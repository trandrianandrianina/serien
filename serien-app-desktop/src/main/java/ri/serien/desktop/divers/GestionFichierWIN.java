/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.fichier.GestionFichierEQU;

/**
 * Gestion des fichiers de description WIN
 * Transforme les fichier de description image de JWAlk pour NIM
 */
public class GestionFichierWIN {
  // Constantes erreurs de chargement
  private final static String ERREUR_FICHIER_INTROUVABLE = "Le fichier est introuvable : ";
  private final static String ERREUR_LECTURE_FICHIER = "Erreur lors de la lecture du fichier : ";
  // private final static String ERREUR_ENREGISTREMENT_FICHIER="Erreur lors de l'enregistrement du fichier : ";
  private final static String ERREUR_LISTE_VIDE = "Analyse impossible car la liste est vide.";
  
  // Variables
  private String nomFichierWin = null;
  private String nomFichierEqu = null;
  private HashMap<String, String> libimage = null;
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  // ---------------------------------------------------------------------------------------------
  // --> Constructeur de la classe <--------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public GestionFichierWIN(String fchwin, String fchequ) {
    nomFichierWin = fchwin;
    nomFichierEqu = fchequ;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Vérifie l'existence d'un fichier <-------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  private boolean isPresent(String fch) {
    File fichier = new File(fch);
    return fichier.exists();
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Lecture du fichier EQU <-----------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public int lectureFichierEQU() {
    GestionFichierEQU gfe = null;
    
    gfe = new GestionFichierEQU(nomFichierEqu);
    if (gfe.lectureFichier() == Constantes.OK) {
      libimage = gfe.getListe(true);
    }
    else {
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Lecture du fichier WIN <-----------------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public int lectureFichierWin(int lectficequ) {
    BufferedReader f = null;
    String chaine = null;
    ArrayList<String> vliste = null;
    
    // On charge le fichier EQU d'origine si on le souhaite
    if (lectficequ == Constantes.TRUE) {
      if (lectureFichierEQU() == Constantes.ERREUR) {
        msgErreur = GestionFichierWIN.class.getSimpleName() + Constantes.SEPARATEUR + ERREUR_LECTURE_FICHIER + nomFichierEqu;
        return Constantes.ERREUR;
      }
    }
    
    // On vérifie que le fichier existe
    if (!isPresent(nomFichierWin)) {
      msgErreur = GestionFichierWIN.class.getSimpleName() + Constantes.SEPARATEUR + ERREUR_FICHIER_INTROUVABLE + nomFichierWin;
      return Constantes.ERREUR;
    }
    
    vliste = new ArrayList<String>();
    try {
      f = new BufferedReader(new FileReader(nomFichierWin));
      
      // Lecture du fichier
      chaine = f.readLine();
      while (chaine != null) {
        vliste.add(chaine);
        chaine = f.readLine();
      }
      f.close();
    }
    catch (Exception e) {
      msgErreur = GestionFichierWIN.class.getSimpleName() + Constantes.SEPARATEUR + ERREUR_LECTURE_FICHIER + nomFichierWin;
      msgErreur = msgErreur + Constantes.crlf + GestionFichierWIN.class.getSimpleName() + Constantes.SEPARATEUR + e;
      return Constantes.ERREUR;
    }
    
    // Analyse des données lues
    return analyseData(vliste);
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Initialise le nom du fichier WIN <-------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public void setNomFichierWin(String fch) {
    nomFichierWin = fch;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Initialise le nom du fichier EQU <-------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public void setNomFichierTxt(String fch) {
    nomFichierEqu = fch;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Analyse les données lues dans le fichier <-----------------------------------------------
  // ---------------------------------------------------------------------------------------------
  private int analyseData(ArrayList<String> lst) {
    int i = 0;
    int j = 0;
    int pos = 0;
    int phase = 0;
    String ligne = null;
    String image = null;
    String lib = null;
    char tabLigne[] = null;
    char delimiteur = ' ';
    
    if (lst == null) {
      msgErreur = GestionFichierWIN.class.getSimpleName() + Constantes.SEPARATEUR + ERREUR_LISTE_VIDE;
      return Constantes.ERREUR;
    }
    
    if (libimage == null) {
      libimage = new HashMap<String, String>();
    }
    tabLigne = new char[255];
    for (i = 0; i < lst.size(); i++) {
      ligne = lst.get(i).trim();
      lib = null;
      image = null;
      phase = 0;
      delimiteur = ' ';
      
      // Les erreurs possibles ou lignes ignorées
      if (ligne.equals("")) {
        continue;
      }
      if (ligne.startsWith(";")) {
        continue;
      }
      
      // On traite chaque ligne
      tabLigne = ligne.toCharArray();
      for (j = 0; j < ligne.length(); j++) {
        if ((tabLigne[j] == delimiteur) || (j == ligne.length() - 1)) {
          if ((pos == j) && (phase != 0)) {
            pos = j + 1;
          }
          else {
            if (phase == 0) {
              pos = j + 1;
            }
            else if (phase == 1) {
              image = "images" + Constantes.SEPARATEUR_DOSSIER_CHAR + new String(tabLigne, pos, j - pos);
              pos = j + 1;
              delimiteur = '"';
            }
            else if (phase == 2) {
              lib = new String(tabLigne, pos, j - pos); // A cause des guillemets
            }
            phase++;
          }
        }
      }
      
      // On stocke le résultat
      if (lib != null) {
        libimage.put(lib, image);
      }
    }
    
    return Constantes.OK;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Change les extensions ICO en GIF <------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public int changeExt() {
    for (Entry<String, String> entry : libimage.entrySet()) {
      libimage.put(entry.getKey(), (entry.getValue()).toLowerCase().replaceAll(".ico", ".gif"));
    }
    
    return Constantes.OK;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Ecrit le fichier JWalk vers le format NIM <----------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public int ecritureFichier() {
    GestionFichierEQU gfe = null;
    
    // On créé le fichier
    if (libimage != null) {
      gfe = new GestionFichierEQU(nomFichierEqu);
      gfe.setListe(libimage);
      if (gfe.ecritureFichier() == Constantes.ERREUR) {
        return Constantes.ERREUR;
      }
    }
    
    return Constantes.OK;
  }
  
  // ---------------------------------------------------------------------------------------------
  // --> Retourne le message d'erreur <-----------------------------------------------------------
  // ---------------------------------------------------------------------------------------------
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
