/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowStateListener;
import java.io.File;
import java.util.Locale;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBoxMenuItem;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLayeredPane;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRootPane;
import javax.swing.JTabbedPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import ri.serien.desktop.composant.SNFondEcran;
import ri.serien.desktop.composant.changerbasedonnees.ModeleChangerBaseDeDonnees;
import ri.serien.desktop.composant.changerbasedonnees.VueChangerBaseDeDonnees;
import ri.serien.desktop.divers.apropos.DialogueAPropos;
import ri.serien.desktop.metier.exploitation.consultationedition.SpConsultationEdition;
import ri.serien.desktop.metier.exploitation.ht630.Ht630;
import ri.serien.desktop.metier.exploitation.menus.ModeleMenuFavoris;
import ri.serien.desktop.metier.exploitation.menus.ModeleMenuGeneral;
import ri.serien.desktop.metier.exploitation.menus.ModeleMenuHistorique;
import ri.serien.desktop.metier.exploitation.menus.VueMenuFavoris;
import ri.serien.desktop.metier.exploitation.menus.VueMenuGeneral;
import ri.serien.desktop.metier.exploitation.menus.VueMenuHistorique;
import ri.serien.desktop.metier.exploitation.menus.VueMenuOutils;
import ri.serien.desktop.metier.exploitation.notification.SurveillanceNotification;
import ri.serien.desktop.sessions.SessionJava;
import ri.serien.desktop.sessions.SessionRPG;
import ri.serien.desktop.user.PreferencesUserManager;
import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.notification.EnumAspectNotification;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.collection.ArrayListManager;
import ri.serien.libcommun.outils.collection.ArrayListManager.IArrayListListener;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceMenu;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.primitif.icon.SNIconTexteVertical;
import ri.serien.libswing.composantrpg.autonome.FenetreIndependante;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.outil.capture.CaptureEcran;

/**
 * @author Stéphane Vénéri
 */
public class ClientSerieN extends JFrame {
  // Constantes
  private static final String TITRE_APPLICATION = "S\u00E9rie N";
  private static final String LIB_MENU_WINPLUS = "Changer le libellé";
  
  private static final int MENU_GENERAL_OUEST = 0;
  private static final int MENU_GENERAL_EST = 1;
  private static final int LARGEUR_MENU_GENERAL = 400;
  
  // Variables
  private int positionMenuGeneral = -1;
  private Icon iconAfficherMenuGeneral = null;
  private Icon iconCacherMenuGeneral = null;
  
  private SNFondEcran pnlFond = null;
  private CaptureEcran captureEcran = null;
  private FenetreIndependante gestionSpool = null;
  
  private int compteurCoupures = 0;
  private SessionJava session = null;
  
  // Variables statiques
  private static Thread confirmationPresence = null;
  private static IdSession idSessionMenu = null;
  public static ModeleMenuGeneral modeleMenuGeneral = null;
  public static ModeleMenuHistorique modeleMenuHistorique = null;
  public static ModeleMenuFavoris modeleMenuFavoris = null;
  private static Object[] tabzone = null;
  private static PreferencesUserManager prefsUserManager = new PreferencesUserManager();
  public static ArrayListManager listeSession = new ArrayListManager(true, false, false);
  private static String version = null;
  private static Object[][] touchesRaccourciSession = new Object[][] { { KeyEvent.VK_F1, "CTRL-F1" }, { KeyEvent.VK_F2, "CTRL-F2" },
      { KeyEvent.VK_F3, "CTRL-F3" }, { KeyEvent.VK_F4, "CTRL-F4" }, { KeyEvent.VK_F5, "CTRL-F5" }, { KeyEvent.VK_F6, "CTRL-F6" },
      { KeyEvent.VK_F7, "CTRL-F7" }, { KeyEvent.VK_F8, "CTRL-F8" }, { KeyEvent.VK_F9, "CTRL-F9" }, { KeyEvent.VK_F10, "CTRL-F10" } };
  
  /**
   * Constructeur.
   */
  @SuppressWarnings("static-access")
  public ClientSerieN(final String pVersion) {
    super();
    ClientSerieN.version = pVersion;
    Trace.info("Démarrage de l'application (" + version + ')');
    
    // Activation de la routine de confirmation de présence
    activerConfirmationPresence();
    
    initComponents();
    setName("SerieNDesktop");
    initMenu();
    initialiserListeSession();
    // Récupération du logo de l'application
    initialiserLogo();
    
    // Remettre la fenêtre principale à sa dernière position connue
    prefsUserManager.getPreferences().positionnerFenetre(this);
    
    // Intercepter les touches
    intercepterCtrlG();
    intercepterCtrlQ();
    intercepterCtrlP();
    intercepterCtrlD();
    intercepterCtrlW();
    intercepterCtrlFX();
    intercepterCtrlPrintScreen();
    
    // Initialisation du titre de la fenêtre
    mettreAJourTitreApplication();
    
    // Création de l'objet pnlFondEcran
    pnlFond = new SNFondEcran(this);
    
    tabzone = new Object[3];
    tabzone[0] = tbpMenuGeneral;
    tabzone[1] = listeSession;
    tabzone[2] = pnlFond;
    
    // Nommer ce composant pour QFTest
    tbpMenuGeneral.setName("OngletsMenuGeneral");
    
    // Créer l'onglet menu général
    modeleMenuGeneral = new ModeleMenuGeneral(this);
    VueMenuGeneral vueMenuGeneral = new VueMenuGeneral(modeleMenuGeneral);
    vueMenuGeneral.afficher();
    tbpMenuGeneral.addTab(modeleMenuGeneral.getEtiquette(), vueMenuGeneral);
    
    // Créer l'onglet menu favoris
    modeleMenuFavoris = new ModeleMenuFavoris(this);
    VueMenuFavoris vueMenuFavoris = new VueMenuFavoris(modeleMenuFavoris);
    vueMenuFavoris.afficher();
    tbpMenuGeneral.addTab(modeleMenuFavoris.getEtiquette(), vueMenuFavoris);
    
    // Créer l'onglet menu historique
    modeleMenuHistorique = new ModeleMenuHistorique(this);
    VueMenuHistorique vueMenuHistorique = new VueMenuHistorique(modeleMenuHistorique);
    vueMenuHistorique.afficher();
    tbpMenuGeneral.addTab(modeleMenuHistorique.getEtiquette(), vueMenuHistorique);
    
    // Créer le menu menu outils
    VueMenuOutils vueMenuOutils = new VueMenuOutils(this);
    vueMenuOutils.gestionDesDroitsUtilisateur();
    tbpMenuGeneral.addTab(vueMenuOutils.getEtiquette(), vueMenuOutils);
    
    // Remplir des différents panneaux
    tbpMenuGeneral.setSelectedIndex(prefsUserManager.getPreferences().getNumOngletMenuCoulissant());
    tbpMenuGeneral.setName("tbpMenuGeneral");
    
    // Nommer pour QFTest
    lpSousCouche.setName("lpSousCouche");
    
    // Positionner le menu général sur la sous-couche
    lpSousCouche.setLayer(pnlMenuGeneral, JLayeredPane.DEFAULT_LAYER + 1000);
    lpSousCouche.add(pnlMenuGeneral);
    
    // Positionner des "composants fixes" sur la sous-couche
    lpSousCouche.setLayout(new BorderLayout());
    lpSousCouche.setLayer(pnlFond, JLayeredPane.DEFAULT_LAYER + 1);
    lpSousCouche.add(pnlFond, BorderLayout.CENTER);
    
    // Configurer le bouton menu général
    Font police = UIManager.getFont("Label.font");
    police = police.deriveFont(police.getStyle() | Font.BOLD, police.getSize() + 2f);
    btMenuGeneral.setFont(police);
    lpSousCouche.setLayer(btMenuGeneral, JLayeredPane.DEFAULT_LAYER + 1);
    deplacerMenuGeneral(prefsUserManager.getPreferences().getPositionMenuCoulissant(), false);
    
    fermerMenuCoulissantDebutSession(prefsUserManager.getPreferences().isFermerAutomatiquement(), false);
    ouvrirMenuCoulissantFinSession(prefsUserManager.getPreferences().isOuvrirAutomatiquement(), false);
    
    // Interception de la touche ESC
    configureRootPane(getRootPane());
    
    // Cache le menu qui permet de lancer des requêtes si on est pas Toulouse
    // MI_Requete.setVisible(infoUser.getNomConfig().equalsIgnoreCase("Toulouse"));
    
    // Cache le menu Détacher
    // mi_Detacher.setVisible(false);
    session = new SessionJava(this, null);
    
    // Paramétrer l'affichage des tooltips pendant 8s
    ToolTipManager.sharedInstance().setDismissDelay(8000);
    
    // Démarrage du contrôle des notifications
    SurveillanceNotification.demarrerSurveillance(this);
    
    // Configuration éventuelle des applications tierces (www/others)
    configurerOthers();
  }
  
  /**
   * Affiche l'écran des éditions.
   */
  private void afficheEdition() {
    SpConsultationEdition gestionDesEditions = new SpConsultationEdition(this, null);
    gestionDesEditions.ouvrirSession();
  }
  
  /**
   * Met à jour le titre de la fenêtre.
   */
  public void mettreAJourTitreApplication() {
    String nomBaseDeDonnees = ManagerSessionClient.getInstance().getCurlib().getNom();
    String profil = ManagerSessionClient.getInstance().getProfil().toUpperCase();
    String descriptionBaseDeDonnees = Constantes.normerTexte(ManagerSessionClient.getInstance().getCurlib().getDescription());
    
    String titre = TITRE_APPLICATION + " (" + profil + ")";
    if (!nomBaseDeDonnees.isEmpty()) {
      titre += " - " + ManagerSessionClient.getInstance().getCurlib().getTexte() + " " + descriptionBaseDeDonnees;
      // S'il faut afficher l'établissement et le magasin
      if (ManagerSessionClient.getInstance().getEnvUser().isTitleBarInformations()) {
        titre += " " + ManagerSessionClient.getInstance().getEnvUser().getTexttitlebarinformations();
      }
    }
    setTitle(titre);
  }
  
  /**
   * Demande un rafraichissement des informations suite au changement de la base de données ou à une mise à jour de la base de données.
   */
  public void rafraichirBaseDeDonnees() {
    // Sauvegarde les préférences
    sauvePreference();
    
    // Mise à jour des informations du client desktop et reconnexion à la base de données
    IdClientDesktop idClientDesktop = ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop();
    ManagerServiceSession.connecterBdd(idClientDesktop, ManagerSessionClient.getInstance().getCurlib());
    // Récupération de la base de données avec ses informations à jour
    BDD baseDeDonnees = ManagerServiceSession.recupererBdd(idClientDesktop);
    ManagerSessionClient.getInstance().getEnvUser().setCurlib(baseDeDonnees);
    
    // Met à jour le titre de l'application
    mettreAJourTitreApplication();
    
    // Récupèration la liste des bibliothèques spécifiques qui a peut être changé suite au changement de la curlib
    try {
      ManagerSessionClient.getInstance().getEnvUser()
          .setListeBibSpe(ManagerServiceMenu.chargerListeBibliothequeSpecifiqueEnvironnement(idSessionMenu,
              ManagerSessionClient.getInstance().getEnvUser().getBibliothequeEnvironnement(),
              ManagerSessionClient.getInstance().getCurlib(), ManagerSessionClient.getInstance().getEnvUser().getLetter()));
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
      Trace.erreur("Problème lors de la récupératon des bibliothèques spécifiques de " + ManagerSessionClient.getInstance().getProfil()
          + " pour la curlib " + ManagerSessionClient.getInstance().getCurlib());
    }
    
    // Recharge complétement les menus (notamment à cause des droits utilisateurs)
    modeleMenuGeneral.chargerDonnees();
    modeleMenuGeneral.rafraichir();
    modeleMenuFavoris.chargerDonnees();
    modeleMenuFavoris.rafraichir();
    modeleMenuHistorique.chargerDonnees();
    modeleMenuHistorique.rafraichir();
  }
  
  /**
   * Contrôle la sauvegarde Série N.
   */
  public void controlerSauvegarde() {
    EnvProgramClient infoPrg = new EnvProgramClient();
    infoPrg.setProgram("VEXP73");
    infoPrg.setLibMenu("Contrôle sauvegarde");
    infoPrg.addBib("WEXPAS");
    ajouterSessionRpg(new SessionRPG(infoPrg, this, null));
    
    validate();
    repaint();
  }
  
  /**
   * Contrôler la version de Série N et de la base de données courante.
   */
  public void controlerVersion() {
    if (ManagerServiceSession.controlerVersion(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop()) == 0) {
      return;
    }
    
    ModeleChangerBaseDeDonnees modele = new ModeleChangerBaseDeDonnees(this.getSession(), this);
    VueChangerBaseDeDonnees vue = new VueChangerBaseDeDonnees(modele);
    vue.afficher();
  }
  
  /**
   * Affiche un message indiquant la liste des licences actives qui vont bientôt arrivées en fin de validité.
   */
  public void controlerFinValiditeLicence() {
    String message = Licence.avertirFinValiditeLicence();
    message = Constantes.normerTexte(message);
    if (message.isEmpty()) {
      return;
    }
    DialogueInformation.afficherTexteNonCentre(message);
  }
  
  /**
   * Ajoute une session pour un programme Java dans la barre (la liste des sessions).
   */
  public void ajouterSessionJava(SessionJava pSessionJava) {
    if (pSessionJava == null) {
      return;
    }
    // Insertion de la session dans la barre (la liste des sessions)
    ArrayListManager listeSession = ((ArrayListManager) tabzone[1]);
    listeSession.addObject(pSessionJava);
  }
  
  /**
   * Ajoute une session pour un programme RPG dans la barre (la liste des sessions).
   */
  public void ajouterSessionRpg(SessionRPG pSessionRpg) {
    if (pSessionRpg == null) {
      return;
    }
    // Insertion de la session dans la barre (la liste des sessions)
    ArrayListManager listeSession = ((ArrayListManager) tabzone[1]);
    listeSession.addObject(pSessionRpg);
  }
  
  /**
   * Active une session par son index
   */
  public void selectSession(int index) {
    pnlFond.selectionnerSessionAvecIndex(index);
  }
  
  /**
   * Capturer la fenêtre de Série N.
   */
  public void capturerEcran() {
    // Instancier l'objet permettant de gérer les captures d'écran si ce n'est pas fait
    if (captureEcran == null) {
      captureEcran = new CaptureEcran(this);
    }
    
    captureEcran.capturerFenetre();
    
    // Envoyer l'image vers le clipboard
    captureEcran.envoyerCaptureVersClipboard();
    
    // Proposer à l'utilisateur de sauver la capture dans un fichier local
    captureEcran.sauverCapture();
  }
  
  /**
   * Modifie l'aspect de l'icone des notifications en fonction de la priorité des notifications à lire.
   */
  public void modifierAspectNotificationBarreSession(EnumAspectNotification pAspectNotification) {
    pnlFond.modifierAspectNotificationBarreSession(pAspectNotification);
  }
  
  public static IdSession getIdSessionMenu() {
    return idSessionMenu;
  }
  
  public static void setIdSessionMenu(IdSession pIdSessionMenu) {
    idSessionMenu = pIdSessionMenu;
  }
  
  /**
   * Retourne les JTabbedPane des panneaux.
   */
  public static Object[] getOnglet() {
    return tabzone;
  }
  
  /**
   * Retourne le manager des préférences de l'utilisateur
   */
  public static PreferencesUserManager getPreferencesUserManager() {
    return prefsUserManager;
  }
  
  /**
   * Retourne le nombre de session active (cad le nombre d'onglet avec des programmes).
   */
  public int getSessionActive() {
    if (listeSession == null) {
      return 0;
    }
    return listeSession.getArrayList().size();
  }
  
  /**
   * Retourne la version du client
   */
  public String getVersion() {
    return Constantes.VERSION_CLIENT;
  }
  
  /**
   * Retourne son menuitem associé.
   */
  public JMenuItem getMenuItem(SessionBase pSessionBase, final int pIndex, int pKeycode) {
    JMenuItem miSession = new JMenuItem();
    miSession.setText(pSessionBase.getPanel().getName());
    miSession.setAccelerator(KeyStroke.getKeyStroke(pKeycode, InputEvent.CTRL_DOWN_MASK));
    miSession.setName("mi_Session" + pIndex);
    miSession.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        selectSession(pIndex);
      }
    });
    return miSession;
  }
  
  /**
   * Contrôle la possibilité de fermer l'application.
   * Elle contrôle si des sessions sont encore actives.
   */
  public boolean isPouvoirFermerApplication() {
    if (listeSession.getArrayList().size() > 0) {
      Object[] options = { UIManager.getString("OptionPane.yesButtonText", Locale.getDefault()),
          UIManager.getString("OptionPane.noButtonText", Locale.getDefault()) };
      int rep = JOptionPane.showOptionDialog(this,
          "<HTML><B>Il n'est pas conseillé de fermer l'application lorsque vous avez des onglets ouverts</B></HTML>\n(répondez NON, puis fermer les onglets un par un).\n\nSi l'application ne répond pas alors confirmez la fermeture du programme.",
          "Demande de fermeture de l'application", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
      if (rep == JOptionPane.NO_OPTION) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Afficher le menu général.
   */
  public void afficherMenuGeneral() {
    pnlMenuGeneral.setVisible(true);
    btMenuGeneral.setIcon(iconCacherMenuGeneral);
  }
  
  /**
   * Cacher le menu général.
   */
  public void cacherMenuGeneral() {
    pnlMenuGeneral.setVisible(false);
    btMenuGeneral.setIcon(iconAfficherMenuGeneral);
  }
  
  /**
   * Permuter l'affichage du menu général (l'afficher s'il est caché, le cacher s'il est visible).
   */
  public void permuterMenuGeneral() {
    if (pnlMenuGeneral.isVisible()) {
      cacherMenuGeneral();
    }
    else {
      afficherMenuGeneral();
    }
  }
  
  /**
   * Ferme proprement l'application.
   */
  public void fermerApplication() {
    // Vérifier si on peut ferme l'application (pas de session en cours par exemple)
    if (!isPouvoirFermerApplication()) {
      return;
    }
    
    // Effacer la liste des sessions
    listeSession.clearObject();
    listeSession.getArrayList().clear();
    
    // Vérification que la gestion des spools soit bien fermé
    if (gestionSpool != null) {
      gestionSpool.dispose();
    }
    
    // Arrêt de la surveillance des notifications
    SurveillanceNotification.arreterSurveillance();
    
    // Sauvegarder les préférences de l'utilisateur
    try {
      sauvePreference();
    }
    catch (Exception e) {
      // Ne pas empêcher la fermeture du client Desktop si cela échoue
      DialogueErreur.afficher(e);
    }
    
    // Nettoyer le dossier temporaire
    try {
      FileNG.remove(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur());
    }
    catch (Exception e) {
      // Ne pas empêcher la fermeture du client Desktop si cela échoue
      DialogueErreur.afficher(e);
    }
    
    // Fermer la session menu
    try {
      if (idSessionMenu != null) {
        ManagerSessionClient.getInstance().detruireSessionClient(idSessionMenu);
      }
    }
    catch (Exception e) {
      // Ne pas empêcher la fermeture du client Desktop si cela échoue
      DialogueErreur.afficher(e);
    }
    
    // Fermer la session transfert
    try {
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fermeture();
    }
    catch (Exception e) {
      // Ne pas empêcher la fermeture du client Desktop si cela échoue
      DialogueErreur.afficher(e);
    }
    
    // Tracer l'usage de la mémoire
    Trace.debugMemoire(ClientSerieN.class, "FermerApplication");
    
    // Arrêt du processus de confirmation de présence auprès du serveur
    arreterConfirmationPresence();
    
    // Détruire de client desktop
    try {
      ManagerServiceSession.detruireClientDesktop(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop());
    }
    catch (Exception e) {
      // Ne pas empêcher la fermeture du client Desktop si cela échoue
      DialogueErreur.afficher(e);
    }
    
    // Tracer l'arrêt du logiciel
    Trace.titre("ARRET DE SERIE N DESKTOP");
    
    // Fermeture de l'application
    System.exit(0);
  }
  
  /**
   * Interception de la touche ESC.
   */
  public void configureRootPane(JRootPane rootPane) {
    InputMap inputMap = rootPane.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
    inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "escPressed");
    
    rootPane.getActionMap().put("escPressed", new AbstractAction("escPressed") {
      
      @Override
      public void actionPerformed(ActionEvent actionEvent) {
        ouvrirLesFavoris();
      }
    });
  }
  
  public SessionJava getSession() {
    return session;
  }
  
  public Object[] getTabzone() {
    return tabzone;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise la liste des sessions (SessionPanel)
   */
  private void initialiserListeSession() {
    listeSession.addListener(new IArrayListListener() {
      // On ferme tous les panels (Sessions AS/400)
      @Override
      public void onDataCleared() {
        for (int i = listeSession.getArrayList().size(); --i >= 0;) {
          ((SessionBase) listeSession.getObject(i)).fermerEcran();
        }
      }
      
      // On ouvre un panel (Session AS/400)
      @Override
      public void onDataAdded(Object val) {
        if (val instanceof SessionBase) {
          onDataAddedSessionBase((SessionBase) val);
        }
      }
      
      // On ferme un panel (Session AS/400)
      @Override
      public void onDataRemoved(Object val) {
        if (val instanceof SessionBase) {
          onDataRemovedSessionBase((SessionBase) val);
        }
      }
      
      @Override
      public void onDataRemoved(int indice) {
      }
    });
  }
  
  private void onDataAddedSessionBase(final SessionBase session) {
    Trace.info("Nouvel onglet, nombre: " + listeSession.getArrayList().size());
    
    pnlFond.ajouterSession(session);
    refreshMenuSession();
  }
  
  private void onDataRemovedSessionBase(final SessionBase session) {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        Trace.info("Fermeture onglet, nombre: " + listeSession.getArrayList().size());
        pnlFond.supprimerSession(session);
        refreshMenuSession();
        Trace.info("Fermer une session");
        Trace.debugMemoire(ClientSerieN.class, "initialiserListeSession");
      }
    });
  }
  
  /**
   * Récupère le logo de l'application et l'applique.
   */
  private void initialiserLogo() {
    setIconImage(new ImageIcon(this.getClass().getClassLoader().getResource(Constantes.IMG_ICON_APPLICATION)).getImage());
  }
  
  /**
   * Initialise le menu avec les Look & Feel
   * 
   */
  private void initMenu() {
    // Activation ou pas du debug
    CKBMI_DebugStatus.setSelected(ManagerSessionClient.getInstance().getEnvUser().isDebugStatus());
  }
  
  /**
   * Active le process récurent de confirmation de présence.
   */
  private void activerConfirmationPresence() {
    confirmationPresence = new Thread() {
      @Override
      public void run() {
        // Construction du nom du thread
        String nomThread = String.format("ClientSerieN(%03d)-confirmerPresence", Thread.currentThread().getId());
        setName(nomThread);
        Trace.info("Démarrage du processus de confirmation de présence.");
        
        // Boucle infinie
        while (true) {
          try {
            IdClientDesktop idClientDesktop = ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop();
            ManagerServiceTechnique.confirmerPresenceClient(idClientDesktop);
            Thread.sleep(ManagerSessionClient.DELAI_CONFIRMATION_PRESENCE_EN_SEC * 1000);
          }
          catch (InterruptedException ex) {
            // Très important de réinterrompre
            Thread.currentThread().interrupt();
            break;
          }
          catch (Exception e) {
          }
        }
      }
    };
    confirmationPresence.start();
  }
  
  /**
   * Arrêt de la confirmation de présence.
   */
  private static void arreterConfirmationPresence() {
    if (confirmationPresence != null && confirmationPresence.isAlive()) {
      confirmationPresence.interrupt();
      Trace.info("Arrêt du processus de confirmation de présence.");
    }
  }
  
  /**
   * Sauvegarde des préférences et ed l'historique de l'utilisateur en cours
   */
  private void sauvePreference() {
    // On sauve l'historique
    prefsUserManager.sauveHistorique(modeleMenuHistorique.getListeNumeroPointsMenu());
    
    // On sauve les préférences
    prefsUserManager.getPreferences().setNumOngletMenuCoulissant(tbpMenuGeneral.getSelectedIndex());
    prefsUserManager.sauvePreferencesUser();
  }
  
  /**
   * Rafraichit le menu des raccoucis clavier pour les sessions
   */
  private void refreshMenuSession() {
    // On détruit les menuitems existants concernant les sessions
    Component[] liste = M_Fichier.getMenuComponents();
    for (int i = 0; i < liste.length; i++) {
      if ((liste[i].getName() != null) && liste[i].getName().startsWith("mi_Session")) {
        M_Fichier.remove(liste[i]);
      }
    }
    
    // On reconstruit le menu
    for (int i = listeSession.getArrayList().size(); --i >= 0;) {
      final int index = i;
      if (listeSession.getObject(index) instanceof SessionBase) {
        SessionBase session = ((SessionBase) listeSession.getObject(index));
        M_Fichier.add(getMenuItem(session, index, (Integer) touchesRaccourciSession[index][0]), 0);
      }
    }
    
    M_Fichier.validate();
    M_Fichier.repaint();
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Clic sur préférence.
   * @param e
   */
  private void MI_PreferenceActionPerformed(ActionEvent e) {
    new OptionsDialog(ManagerSessionClient.getInstance().getEnvUser());
  }
  
  /**
   * Clic sur A propos de
   * @param e
   */
  private void MI_AProposdeActionPerformed(ActionEvent e) {
    DialogueAPropos.afficher(getSession(), getSessionActive());
  }
  
  /**
   * Permet d'afficher le menu ou pas en appuyant sur CTRL+G (comme sous VISTA/Seven).
   */
  private void intercepterCtrlG() {
    menuBar.setVisible(false);
    
    KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_DOWN_MASK, false);
    Action action = new AbstractAction() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        menuBar.setVisible(!menuBar.isVisible());
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "CTRL-G");
    getRootPane().getActionMap().put("CTRL-G", action);
  }
  
  /**
   * Remettre les dimensions standards de la fenêtre via un CTRL+W.
   */
  private void intercepterCtrlW() {
    final JFrame fenetre = this;
    KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_W, InputEvent.CTRL_DOWN_MASK, false);
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        prefsUserManager.getPreferences().positionnerFenetreSuivantStandard(fenetre);
        prefsUserManager.sauvePreferencesUser();
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "CTRL-W");
    getRootPane().getActionMap().put("CTRL-W", action);
  }
  
  /**
   * Effectuer une capture de la fenêtre de l'application lors de l'appui sur CTRL + PRINTSCREEN.
   */
  private void intercepterCtrlPrintScreen() {
    KeyStroke printScreenKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_PRINTSCREEN, InputEvent.CTRL_DOWN_MASK, true);
    Action action = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        capturerEcran();
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(printScreenKeyStroke, "CTRL+PRINTSCREEN");
    getRootPane().getActionMap().put("CTRL+PRINTSCREEN", action);
  }
  
  /**
   * Permet d'afficher la gestion des impressions.
   */
  private void intercepterCtrlP() {
    KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_P, InputEvent.CTRL_DOWN_MASK, false);
    Action action = new AbstractAction() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        afficheEdition();
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "CTRL-P");
    getRootPane().getActionMap().put("CTRL-P", action);
  }
  
  /**
   * Permet de fermer l'application.
   */
  private void intercepterCtrlQ() {
    KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK, false);
    Action action = new AbstractAction() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        fermerApplication();
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "CTRL-Q");
    getRootPane().getActionMap().put("CTRL-Q", action);
  }
  
  /**
   * Permet de changer d'onglet grâce aux touches de fonction.
   */
  private void intercepterCtrlFX() {
    for (int i = 0; i < touchesRaccourciSession.length; i++) {
      final int index = i;
      KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke((Integer) touchesRaccourciSession[i][0], InputEvent.CTRL_DOWN_MASK, false);
      Action action = new AbstractAction() {
        
        @Override
        public void actionPerformed(ActionEvent e) {
          pnlFond.selectionnerSessionAvecIndex(index);
        }
      };
      getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, touchesRaccourciSession[i][1]);
      getRootPane().getActionMap().put(touchesRaccourciSession[i][1], action);
    }
  }
  
  /**
   * Permet de détacher le panel actif.
   */
  private void intercepterCtrlD() {
    KeyStroke escapeKeyStroke = KeyStroke.getKeyStroke(KeyEvent.VK_D, InputEvent.CTRL_DOWN_MASK, false);
    Action action = new AbstractAction() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        pnlFond.detacherPanel();
      }
    };
    getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(escapeKeyStroke, "CTRL-D");
    getRootPane().getActionMap().put("CTRL-D", action);
  }
  
  /**
   * Activer ou désactiver le mode débug pour les traces (côtés client et serveur).
   */
  private void CKBMI_DebugStatusActionPerformed(ActionEvent e) {
    boolean debugstatus = CKBMI_DebugStatus.isSelected();
    modeleMenuGeneral.rafraichirArborescences(!debugstatus);
    Trace.setModeDebug(debugstatus);
    ManagerServiceTechnique.activerTraceDebug(debugstatus);
    ManagerSessionClient.getInstance().getEnvUser().setDebugStatus(debugstatus);
  }
  
  /**
   * Déplacer le menu coulissant à gauche ou à droite de la fenêtre.
   */
  private void deplacerMenuGeneral(int pPositionMenuGeneral, boolean sauve) {
    if (pPositionMenuGeneral == positionMenuGeneral) {
      return;
    }
    positionMenuGeneral = pPositionMenuGeneral;
    
    // Configurer le menu général suivant le côté où il sera affiché
    if (positionMenuGeneral == MENU_GENERAL_OUEST) {
      // Changer le menu contextuel
      mi_MenuAGauche.setVisible(false);
      mi_MenuADroite.setVisible(true);
      
      // Changer l'orientation du texte
      iconCacherMenuGeneral = new SNIconTexteVertical(btMenuGeneral, "Fermer", SNIconTexteVertical.ROTATE_LEFT);
      iconAfficherMenuGeneral = new SNIconTexteVertical(btMenuGeneral, "Menu général", SNIconTexteVertical.ROTATE_LEFT);
      
      // Déplacer le bouton permettant d'afficher le menu général
      lpSousCouche.remove(btMenuGeneral);
      lpSousCouche.add(btMenuGeneral, BorderLayout.WEST);
    }
    else {
      // Changer le menu contextuel
      mi_MenuAGauche.setVisible(true);
      mi_MenuADroite.setVisible(false);
      
      // Changer l'orientation du texte
      iconCacherMenuGeneral = new SNIconTexteVertical(btMenuGeneral, "Fermer", SNIconTexteVertical.ROTATE_RIGHT);
      iconAfficherMenuGeneral = new SNIconTexteVertical(btMenuGeneral, "Menu général", SNIconTexteVertical.ROTATE_RIGHT);
      
      // Déplacer le bouton permettant d'afficher le menu général
      lpSousCouche.remove(btMenuGeneral);
      lpSousCouche.add(btMenuGeneral, BorderLayout.EAST);
    }
    
    // Rafraichir l'icône car le texte à changé d'orientation
    if (pnlMenuGeneral.isVisible()) {
      btMenuGeneral.setIcon(iconCacherMenuGeneral);
    }
    else {
      btMenuGeneral.setIcon(iconAfficherMenuGeneral);
    }
    
    // Changer la position du menu général
    redimensionnerMenuGeneral();
    
    // Rafraichir
    validate();
    
    // Stocker l'information dans les préférences de l'utilisateur si besoin
    if (pPositionMenuGeneral != prefsUserManager.getPreferences().getPositionMenuCoulissant()) {
      prefsUserManager.getPreferences().setPositionMenuCoulissant(pPositionMenuGeneral);
      if (!sauve) {
        prefsUserManager.getPreferences().resetModify();
      }
      prefsUserManager.sauvePreferencesUser();
    }
  }
  
  /**
   * Redimensionner le menu coulissant en fonction du composant parent.
   * La largeur du menu général est fixe tandis que la hauteur doit être identique à celle du composant parent.
   */
  private void redimensionnerMenuGeneral() {
    Container parent = pnlMenuGeneral.getParent();
    if (parent == null) {
      throw new MessageErreurException("Impossible de redimensionner le menu général car il n'est pas rattaché à une fenêtre.");
    }
    
    // Changer la dimension du menu général
    pnlMenuGeneral.setSize(new Dimension(LARGEUR_MENU_GENERAL, parent.getHeight()));
    
    // Changer la position du menu général
    if (positionMenuGeneral == MENU_GENERAL_OUEST) {
      pnlMenuGeneral.setLocation(btMenuGeneral.getWidth(), 0);
    }
    else {
      pnlMenuGeneral.setLocation(parent.getWidth() - pnlMenuGeneral.getWidth() - btMenuGeneral.getWidth(), 0);
    }
    
    // Rafraichir
    validate();
  }
  
  /**
   * Permet sauvegarder l'action sur le menu coulissant à l'ouverture d'une session.
   */
  private void fermerMenuCoulissantDebutSession(boolean fermer, boolean sauve) {
    // On stocke l'info dans les préférences de l'utilisateur si besoin
    if (fermer != prefsUserManager.getPreferences().isFermerAutomatiquement()) {
      prefsUserManager.getPreferences().setFermerAutomatiquement(fermer);
      if (!sauve) {
        prefsUserManager.getPreferences().resetModify();
      }
      prefsUserManager.sauvePreferencesUser();
    }
    // On adapte le texte du menu contextuel
    mi_MenuFermerDebutSession.setVisible(!fermer);
    mi_MenuOuvrirDebutSession.setVisible(fermer);
  }
  
  /**
   * Ouvre le menu coulissant sur l'onglet des Favoris
   */
  private void ouvrirLesFavoris() {
    permuterMenuGeneral();
    
    // Active l'onglet "Favoris" si touche ESC
    if (pnlMenuGeneral.isVisible()) {
      tbpMenuGeneral.setSelectedIndex(1);
      tbpMenuGeneral.requestFocus();
    }
  }
  
  /**
   * Permet sauvegarder l'action sur le menu coulissant à la fermeture de la dernière session
   * @param ouvrir
   */
  private void ouvrirMenuCoulissantFinSession(boolean ouvrir, boolean sauve) {
    // On stocke l'info dans les préférences de l'utilisateur si besoin
    if (ouvrir != prefsUserManager.getPreferences().isOuvrirAutomatiquement()) {
      prefsUserManager.getPreferences().setOuvrirAutomatiquement(ouvrir);
      if (!sauve) {
        prefsUserManager.getPreferences().resetModify();
      }
      prefsUserManager.sauvePreferencesUser();
    }
    // On adapte le texte du menu contextuel
    mi_MenuFermerFinSession.setVisible(ouvrir);
    mi_MenuOuvrirFinSession.setVisible(!ouvrir);
  }
  
  /**
   * Supprime tous les logos de tous les établissements sur le poste local
   */
  private void supprimerLogoEtb() {
    if (listeSession.getArrayList().size() > 0) {
      JOptionPane.showMessageDialog(this, "Fermer toutes les sessions avant de supprimer les logos.",
          "Suppression des logos des établissements", JOptionPane.WARNING_MESSAGE);
      return;
    }
    
    File dossierImages =
        new File(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTravail().getAbsolutePath()
            + File.separator + "images");
    FileNG.remove(new File(dossierImages.getAbsolutePath() + File.separatorChar + "logo.png"));
    File[] listelogos = dossierImages.listFiles();
    if (listelogos != null) {
      for (int i = 0; i < listelogos.length; i++) {
        if (listelogos[i].getName().startsWith("logo_") && listelogos[i].getName().endsWith(".png")) {
          FileNG.remove(listelogos[i]);
        }
      }
    }
    else {
      Trace.info("Le dossier " + dossierImages.getAbsolutePath() + " n'a pas été trouvé.");
    }
    
    // Solution Provisoire
    // Permet de lancer le balayage des runtimes sur le poste de travail de l'utilisateur.
    // Cela permet de récupérer le logo et de l'afficher sans quitter l'application
    if (ManagerSessionClient.getInstance().getEnvUser().getTransfertSession() != null) {
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().balayerDossierRuntime();
    }
    // JOptionPane.showMessageDialog(this, "Les logos ont bien été supprimés.", "Suppression des logos des
    // établissements", JOptionPane.INFORMATION_MESSAGE);
  }
  
  /**
   * Configurer les applications tierces (www/others) sur le poste de travail.
   */
  private void configurerOthers() {
    Trace.info("Configuration des applications tierces sur le poste de travail.");
    Ht630.configurerPosteDeTravail(ManagerSessionClient.getInstance().getEnvUser());
    Trace.info("Fin de la configuration des applications tierces sur le poste de travail.");
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Clic sur la croix.
   */
  private void thisWindowClosing(WindowEvent e) {
    fermerApplication();
  }
  
  /**
   * Clic sur Quitter
   */
  private void MI_QuitterActionPerformed(ActionEvent e) {
    fermerApplication();
  }
  
  /**
   * La fenêtre a été minimisée, maximisée ou remise en mode fenêtré.
   */
  private void thisWindowStateChanged(WindowEvent e) {
    prefsUserManager.getPreferences().memoriserPositionFenetre(this);
  }
  
  private void thisComponentResized(ComponentEvent e) {
    prefsUserManager.getPreferences().memoriserPositionFenetre(this);
  }
  
  private void thisComponentMoved(ComponentEvent e) {
    prefsUserManager.getPreferences().memoriserPositionFenetre(this);
  }
  
  private void MI_ViderCacheActionPerformed(ActionEvent e) {
    FileNG.removeContents(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTemp());
  }
  
  private void lpSousCoucheComponentResized(ComponentEvent e) {
    redimensionnerMenuGeneral();
  }
  
  private void btMenuGeneralActionPerformed(ActionEvent e) {
    permuterMenuGeneral();
  }
  
  private void mi_MenuAGaucheActionPerformed(ActionEvent e) {
    deplacerMenuGeneral(MENU_GENERAL_OUEST, true);
  }
  
  private void mi_MenuADroiteActionPerformed(ActionEvent e) {
    deplacerMenuGeneral(MENU_GENERAL_EST, true);
  }
  
  private void mi_MenuFermerDebutSessionActionPerformed(ActionEvent e) {
    fermerMenuCoulissantDebutSession(true, true);
  }
  
  private void mi_MenuOuvrirDebutSessionActionPerformed(ActionEvent e) {
    fermerMenuCoulissantDebutSession(false, true);
  }
  
  private void mi_MenuOuvrirFinSessionActionPerformed(ActionEvent e) {
    ouvrirMenuCoulissantFinSession(true, true);
  }
  
  private void mi_MenuFermerFinSessionActionPerformed(ActionEvent e) {
    ouvrirMenuCoulissantFinSession(false, true);
  }
  
  private void MI_ImprimerActionPerformed(ActionEvent e) {
    afficheEdition();
  }
  
  private void MI_FavorisActionPerformed(ActionEvent e) {
    ouvrirLesFavoris();
  }
  
  private void MI_SupprimerLogoEtbActionPerformed(ActionEvent e) {
    supprimerLogoEtb();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    menuBar = new JMenuBar();
    M_Fichier = new JMenu();
    MI_Favoris = new JMenuItem();
    MI_Imprimer = new JMenuItem();
    MI_Quitter = new JMenuItem();
    M_Aide = new JMenu();
    CKBMI_DebugStatus = new JCheckBoxMenuItem();
    MI_ViderCache = new JMenuItem();
    MI_SupprimerLogoEtb = new JMenuItem();
    MI_Preference = new JMenuItem();
    MI_AProposde = new JMenuItem();
    lpSousCouche = new JLayeredPane();
    pnlMenuGeneral = new JPanel();
    tbpMenuGeneral = new JTabbedPane();
    btMenuGeneral = new JButton();
    BTD = new JPopupMenu();
    mi_MenuAGauche = new JMenuItem();
    mi_MenuADroite = new JMenuItem();
    mi_MenuFermerDebutSession = new JMenuItem();
    mi_MenuOuvrirDebutSession = new JMenuItem();
    mi_MenuOuvrirFinSession = new JMenuItem();
    mi_MenuFermerFinSession = new JMenuItem();
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    addWindowStateListener(new WindowStateListener() {
      @Override
      public void windowStateChanged(WindowEvent e) {
        thisWindowStateChanged(e);
      }
    });
    addComponentListener(new ComponentAdapter() {
      @Override
      public void componentMoved(ComponentEvent e) {
        thisComponentMoved(e);
      }
      
      @Override
      public void componentResized(ComponentEvent e) {
        thisComponentResized(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== menuBar ========
    {
      menuBar.setName("menuBar");
      
      // ======== M_Fichier ========
      {
        M_Fichier.setText("Fichier");
        M_Fichier.setName("M_Fichier");
        M_Fichier.addSeparator();
        
        // ---- MI_Favoris ----
        MI_Favoris.setText("Les favoris");
        MI_Favoris.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0));
        MI_Favoris.setName("MI_Favoris");
        MI_Favoris.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_FavorisActionPerformed(e);
          }
        });
        M_Fichier.add(MI_Favoris);
        
        // ---- MI_Imprimer ----
        MI_Imprimer.setText("Gestion des impressions");
        MI_Imprimer.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_MASK));
        MI_Imprimer.setName("MI_Imprimer");
        MI_Imprimer.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_ImprimerActionPerformed(e);
          }
        });
        M_Fichier.add(MI_Imprimer);
        M_Fichier.addSeparator();
        
        // ---- MI_Quitter ----
        MI_Quitter.setText("Quitter");
        MI_Quitter.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, KeyEvent.CTRL_MASK));
        MI_Quitter.setName("MI_Quitter");
        MI_Quitter.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_QuitterActionPerformed(e);
          }
        });
        M_Fichier.add(MI_Quitter);
      }
      menuBar.add(M_Fichier);
      
      // ======== M_Aide ========
      {
        M_Aide.setText("Aide");
        M_Aide.setName("M_Aide");
        
        // ---- CKBMI_DebugStatus ----
        CKBMI_DebugStatus.setText("Debug");
        CKBMI_DebugStatus.setName("CKBMI_DebugStatus");
        CKBMI_DebugStatus.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            CKBMI_DebugStatusActionPerformed(e);
          }
        });
        M_Aide.add(CKBMI_DebugStatus);
        
        // ---- MI_ViderCache ----
        MI_ViderCache.setText("Supprimer les fichiers temporaires");
        MI_ViderCache.setName("MI_ViderCache");
        MI_ViderCache.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_ViderCacheActionPerformed(e);
          }
        });
        M_Aide.add(MI_ViderCache);
        
        // ---- MI_SupprimerLogoEtb ----
        MI_SupprimerLogoEtb.setText("Supprimer les logos en cache");
        MI_SupprimerLogoEtb.setName("MI_SupprimerLogoEtb");
        MI_SupprimerLogoEtb.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_SupprimerLogoEtbActionPerformed(e);
          }
        });
        M_Aide.add(MI_SupprimerLogoEtb);
        M_Aide.addSeparator();
        
        // ---- MI_Preference ----
        MI_Preference.setText("Pr\u00e9f\u00e9rences");
        MI_Preference.setVisible(false);
        MI_Preference.setName("MI_Preference");
        MI_Preference.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_PreferenceActionPerformed(e);
          }
        });
        M_Aide.add(MI_Preference);
        
        // ---- MI_AProposde ----
        MI_AProposde.setText("A propos de S\u00e9rie N");
        MI_AProposde.setName("MI_AProposde");
        MI_AProposde.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            MI_AProposdeActionPerformed(e);
          }
        });
        M_Aide.add(MI_AProposde);
      }
      menuBar.add(M_Aide);
    }
    setJMenuBar(menuBar);
    
    // ======== lpSousCouche ========
    {
      lpSousCouche.setName("lpSousCouche");
      lpSousCouche.addComponentListener(new ComponentAdapter() {
        @Override
        public void componentResized(ComponentEvent e) {
          lpSousCoucheComponentResized(e);
        }
      });
    }
    contentPane.add(lpSousCouche, BorderLayout.CENTER);
    
    // ======== pnlMenuGeneral ========
    {
      pnlMenuGeneral.setVisible(false);
      pnlMenuGeneral.setName("pnlMenuGeneral");
      pnlMenuGeneral.setLayout(new BorderLayout());
      
      // ======== tbpMenuGeneral ========
      {
        tbpMenuGeneral.setTabPlacement(SwingConstants.BOTTOM);
        tbpMenuGeneral.setPreferredSize(new Dimension(1, 1));
        tbpMenuGeneral.setMinimumSize(new Dimension(1, 1));
        tbpMenuGeneral.setBorder(LineBorder.createGrayLineBorder());
        tbpMenuGeneral.setName("tbpMenuGeneral");
      }
      pnlMenuGeneral.add(tbpMenuGeneral, BorderLayout.CENTER);
    }
    
    // ---- btMenuGeneral ----
    btMenuGeneral.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    btMenuGeneral.setMaximumSize(new Dimension(25, 12));
    btMenuGeneral.setMinimumSize(new Dimension(25, 12));
    btMenuGeneral.setPreferredSize(new Dimension(25, 12));
    btMenuGeneral.setComponentPopupMenu(BTD);
    btMenuGeneral.setName("btMenuGeneral");
    btMenuGeneral.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btMenuGeneralActionPerformed(e);
      }
    });
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- mi_MenuAGauche ----
      mi_MenuAGauche.setText("D\u00e9placer le menu \u00e0 gauche");
      mi_MenuAGauche.setName("mi_MenuAGauche");
      mi_MenuAGauche.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuAGaucheActionPerformed(e);
        }
      });
      BTD.add(mi_MenuAGauche);
      
      // ---- mi_MenuADroite ----
      mi_MenuADroite.setText("D\u00e9placer le menu \u00e0 droite");
      mi_MenuADroite.setName("mi_MenuADroite");
      mi_MenuADroite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuADroiteActionPerformed(e);
        }
      });
      BTD.add(mi_MenuADroite);
      
      // ---- mi_MenuFermerDebutSession ----
      mi_MenuFermerDebutSession.setText("Fermer le menu apr\u00e8s lancement d'une session");
      mi_MenuFermerDebutSession.setName("mi_MenuFermerDebutSession");
      mi_MenuFermerDebutSession.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuFermerDebutSessionActionPerformed(e);
        }
      });
      BTD.add(mi_MenuFermerDebutSession);
      
      // ---- mi_MenuOuvrirDebutSession ----
      mi_MenuOuvrirDebutSession.setText("Laisser ouvert le menu apr\u00e8s lancement d'une session");
      mi_MenuOuvrirDebutSession.setName("mi_MenuOuvrirDebutSession");
      mi_MenuOuvrirDebutSession.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuOuvrirDebutSessionActionPerformed(e);
        }
      });
      BTD.add(mi_MenuOuvrirDebutSession);
      
      // ---- mi_MenuOuvrirFinSession ----
      mi_MenuOuvrirFinSession.setText("Ouvrir le menu \u00e0 la fermeture de la derni\u00e8re session");
      mi_MenuOuvrirFinSession.setName("mi_MenuOuvrirFinSession");
      mi_MenuOuvrirFinSession.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuOuvrirFinSessionActionPerformed(e);
        }
      });
      BTD.add(mi_MenuOuvrirFinSession);
      
      // ---- mi_MenuFermerFinSession ----
      mi_MenuFermerFinSession.setText("Laisser le menu fermer \u00e0 la fermeture de la derni\u00e8re session");
      mi_MenuFermerFinSession.setName("mi_MenuFermerFinSession");
      mi_MenuFermerFinSession.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mi_MenuFermerFinSessionActionPerformed(e);
        }
      });
      BTD.add(mi_MenuFermerFinSession);
    }
    
    pack();
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JMenuBar menuBar;
  private JMenu M_Fichier;
  private JMenuItem MI_Favoris;
  private JMenuItem MI_Imprimer;
  private JMenuItem MI_Quitter;
  private JMenu M_Aide;
  private JCheckBoxMenuItem CKBMI_DebugStatus;
  private JMenuItem MI_ViderCache;
  private JMenuItem MI_SupprimerLogoEtb;
  private JMenuItem MI_Preference;
  private JMenuItem MI_AProposde;
  private JLayeredPane lpSousCouche;
  private JPanel pnlMenuGeneral;
  private JTabbedPane tbpMenuGeneral;
  private JButton btMenuGeneral;
  private JPopupMenu BTD;
  private JMenuItem mi_MenuAGauche;
  private JMenuItem mi_MenuADroite;
  private JMenuItem mi_MenuFermerDebutSession;
  private JMenuItem mi_MenuOuvrirDebutSession;
  private JMenuItem mi_MenuOuvrirFinSession;
  private JMenuItem mi_MenuFermerFinSession;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
