/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import java.awt.Toolkit;

import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class ControlSaisie extends PlainDocument {
  
  public final static int ORIGIN = 0;
  public final static int UPPERCASE = 1;
  public final static int LOWERCASE = 2;
  public final static int DELETEZERO = 3;
  
  private int maxChar = 10;
  private int type = ORIGIN; // 0:rien, 1:uppercase, 2:lowercase
  
  public ControlSaisie() {
    super();
  }
  
  public ControlSaisie(int maxChar) {
    this();
    setMaxChar(maxChar);
  }
  
  public ControlSaisie(int maxChar, int type) {
    this();
    setMaxChar(maxChar);
    setTypeChar(type);
  }
  
  public final void setMaxChar(int maxChar) {
    this.maxChar = maxChar;
  }
  
  public final void setTypeChar(int type) {
    this.type = type;
  }
  
  @Override
  public final void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
    
    if (str == null) {
      return;
    }
    if ((getLength() + str.length()) > maxChar) {
      Toolkit.getDefaultToolkit().beep();
      return;
    }
    if (type == UPPERCASE) {
      str = str.toUpperCase();
    }
    else if (type == LOWERCASE) {
      str = str.toLowerCase();
    }
    else if (type == DELETEZERO) {
      if ((offs == 0) && (str.equals("0"))) {
        return;
      }
    }
    
    super.insertString(offs, str, a);
  }
}
