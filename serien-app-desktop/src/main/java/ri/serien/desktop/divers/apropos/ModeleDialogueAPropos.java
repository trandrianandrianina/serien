/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers.apropos;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import ri.serien.desktop.sessions.SessionJava;
import ri.serien.libcommun.outils.chaine.StringIP;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Gestion des données associées de la boîte de dialogue "A propos".
 */
public class ModeleDialogueAPropos extends AbstractModeleDialogue {
  // -- Variables
  private int nombreSessionActive = 0;
  private List<String> listeParametreJVM = null;
  private boolean afficherDetail = false;
  
  /**
   * Constructeur.
   * 
   * @param pSession Session Java
   * @param pNombreSessionActive Nombre de sessions actives
   */
  public ModeleDialogueAPropos(SessionJava pSession, int pNombreSessionActive) {
    super(pSession);
    nombreSessionActive = pNombreSessionActive;
    chargerDonnees();
  }
  
  // -- Méthodes publiques
  /**
   * Initialiser les données.
   * 
   * @see AbstractModeleDialogue
   */
  @Override
  public void initialiserDonnees() {
    
    // Lister les paramètres de la JVM
    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    if (runtimeMXBean == null) {
      return;
    }
    listeParametreJVM = runtimeMXBean.getInputArguments();
  }
  
  /**
   * Charger les données.
   * 
   * @see AbstractModeleDialogue
   */
  @Override
  public void chargerDonnees() {
  }
  
  /**
   * Retourner la version du logiciel.
   * 
   * @return Version du logiciel.
   */
  public String getVersionLogiciel() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null
        && ManagerSessionClient.getInstance().getEnvUser().getVersionSerieN() != null) {
      return ManagerSessionClient.getInstance().getEnvUser().getVersionSerieN().toString();
    }
    return null;
  }
  
  /**
   * Retourner la version de Java.
   * 
   * @return Version de Java.
   */
  public String getVersionJRE() {
    return System.getProperty("java.version");
  }
  
  /**
   * Retourner le chemin d'accès du dossier racine.
   * 
   * @return Chemin d'acces du dossier racine.
   */
  public String getDossierRacine() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null
        && ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop() != null
        && ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierRacine() != null) {
      return ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierRacine().getAbsolutePath();
    }
    return null;
  }
  
  /**
   * Retourner le chemin d'accès du serveur.
   * 
   * @return Chemin d'accès du serveur.
   */
  public String getDossierRacineServeur() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null
        && ManagerSessionClient.getInstance().getEnvUser().getDossierServeur() != null) {
      return ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierRacine();
    }
    return null;
  }
  
  /**
   * Retourner l'adresse IP du poste.
   * 
   * @return Adresse IP du poste.
   */
  public String getAdresseIP() {
    InetAddress inetAddress;
    try {
      inetAddress = InetAddress.getLocalHost();
      if (inetAddress != null) {
        StringIP stringInetAddress = new StringIP(inetAddress.toString());
        return stringInetAddress.getIP();
      }
    }
    catch (UnknownHostException e) {
      // RAS
    }
    return null;
  }
  
  /**
   * Retourner le serveur hôte.
   * 
   * @return Serveur hôte.
   */
  public String getServeurHote() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null) {
      return ManagerSessionClient.getInstance().getEnvUser().getServeurSGM();
    }
    return null;
  }
  
  /**
   * Retourner le port.
   * 
   * @return Port.
   */
  public String getPort() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null) {
      return String.valueOf(ManagerSessionClient.getInstance().getEnvUser().getPortSGM());
    }
    return null;
  }
  
  /**
   * Retourner l'identifiant de l'appareil.
   * 
   * @return Identifiant de l'appareil.
   */
  public String getDevice() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null) {
      return ManagerSessionClient.getInstance().getEnvUser().getPrefixDevice();
    }
    return null;
  }
  
  /**
   * Retourner le nombre de sessions actives.
   * 
   * @return Nombre de sessions actives.
   */
  public String getNombreSessionActive() {
    return String.valueOf(nombreSessionActive);
  }
  
  /**
   * Retourner la lettre d'environnement.
   * 
   * @return Lettre d'environnement.
   */
  public String getLettreEnvironnement() {
    if (ManagerSessionClient.getInstance().getEnvUser() != null) {
      return String.valueOf(ManagerSessionClient.getInstance().getEnvUser().getLetter());
    }
    return null;
  }
  
  /**
   * Retourner la liste des paramètres de la JVM.
   * 
   * @return liste des paramètres de la JVM.
   */
  public List<String> getListeParametreJVM() {
    return listeParametreJVM;
  }
  
  /**
   * Définir l'état d'affichage des informations détaillées.
   * 
   * @param pAfficherDetail true si les informations détaillées sont affichée, false sinon.
   */
  public void modifierAfficherDetail(boolean pAfficherDetail) {
    if (afficherDetail == pAfficherDetail) {
      return;
    }
    afficherDetail = pAfficherDetail;
    rafraichir();
  }
  
  /**
   * Indique s'il faut afficher les informations détaillées.
   * 
   * @return true=afficher le détail, false=ne pas afficher le détail.
   */
  public boolean isAfficherDetail() {
    return afficherDetail;
  }
}
