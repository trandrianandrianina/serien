/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import javax.swing.JOptionPane;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.interpreteur.Lexical;

public class NotificationProgrammeRPG {
  // Variables
  private Lexical lexique = null;
  private String buffer = null;
  private int codeNotif = -1;
  private String data = null;
  
  /**
   * Constructeur
   * @param alexique
   * @param abuffer
   */
  public NotificationProgrammeRPG(Lexical alexique, String abuffer) {
    lexique = alexique;
    buffer = abuffer;
  }
  
  /**
   * Traitement
   */
  public void treatmentBuffer() {
    extractData();
    switch (codeNotif) {
      case Constantes.NOTIF_DLFILE:
        downloadFile();
        break;
    }
  }
  
  /**
   * Extraction des données du buffer
   */
  private void extractData() {
    codeNotif = -1;
    if ((buffer == null) || (buffer.trim().length() == 0)) {
      return;
    }
    codeNotif = Integer.parseInt(buffer.substring(5, 10));
    data = buffer.substring(15).trim();
  }
  
  /**
   * Téléchargement d'un fichier
   */
  private void downloadFile() {
    new Thread() {
      @Override
      public void run() {
        // getParent().setCursor(new Cursor(Cursor.WAIT_CURSOR));
        String fichier = lexique.RecuperationFichier(data);
        if (fichier == null) {
          JOptionPane.showMessageDialog(lexique.getPanel(), "Problème lors de la récupération du fichier.");
        }
        else {
          lexique.AfficheDocument(fichier);
          // getParent().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        }
      }
    }.start();
  }
}
