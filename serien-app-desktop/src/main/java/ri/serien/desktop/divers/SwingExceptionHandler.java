/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import javax.swing.SwingUtilities;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

/**
 * Afficher les exceptions non capturées dans une boîte de dialogue.
 * Noter que cela ne marche pas sur les boîtes de dialogue modales en Java 6 :
 * https://stackoverflow.com/questions/5794472/why-bother-with-setting-the-sun-awt-exception-handler-property
 */
public class SwingExceptionHandler implements Thread.UncaughtExceptionHandler {
  
  /**
   * Activer le gestionnaire graphique pour les exceptions non capturées.
   */
  public static void activer() {
    Thread.setDefaultUncaughtExceptionHandler(new SwingExceptionHandler());
  }
  
  @Override
  public void uncaughtException(final Thread thread, final Throwable t) {
    Trace.erreur("Exception non capturée");
    if (SwingUtilities.isEventDispatchThread()) {
      DialogueErreur.afficher(t);
    }
    else {
      try {
        SwingUtilities.invokeAndWait(new Runnable() {
          @Override
          public void run() {
            DialogueErreur.afficher(t);
          }
        });
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de l'affichage de la boîte de dialogue");
        Thread.currentThread().interrupt();
      }
    }
  }
}
