/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import ri.serien.desktop.commun.identification.DialogueIdentification;
import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.JarUtils;
import ri.serien.libcommun.outils.parametreclient.ParametresClient;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerClientRMI;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

/**
 * Classe principale de l'application Série N Desktop.
 * 
 */
public class Application {
  // Constantes
  private final static String TITRE_POPUP = "Paramètres de l'application";
  private final static int PORT_RMI = 1099;
  public final static String VERSION = "3.0";
  
  // Variables
  private ParametresClient parametresClient = new ParametresClient();
  // Liste des fichiers obsolètes à supprimer dans le dossier www
  private String[] listFileToRemoveInWeb =
      new String[] { "SerieN.jar", "liste.ini", "jacob-1.17-x64.dll", "jacob-1.17-x86.dll", "jacob.jar", "balloontip-1.2.4.1.jar" };
  
  /**
   * Constructeur
   */
  public Application() {
  }
  
  /**
   * Programme principal
   */
  public static void main(final String[] args) {
    try {
      // Forum sur Sécurity manager: http://forums.sun.com/thread.jspa?threadID=5360172&tstart=0
      // Permet de régler les problèmes de chargement d'images malgrès le fait que les jar soient signés
      System.setSecurityManager(null);
      
      // Forcer le Look&Feel Nimbus s'il est disponible
      try {
        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
          if ("Nimbus".equals(info.getName())) {
            UIManager.setLookAndFeel(info.getClassName());
            break;
          }
        }
      }
      catch (Exception e) {
        // Ce n'est pas grave si on n'y parvient pas.
      }
      
      // Concaténer les arguments trouvés dans une chaine
      final StringBuffer parametres = new StringBuffer();
      for (int i = 0; i < args.length; i++) {
        parametres.append(args[i].trim()).append(' ');
      }
      
      // Lancer l'applciation
      Application app = new Application();
      app.lancerTraitementPrincipal(parametres.toString());
    }
    catch (final Exception e) {
      // Afficher le message d'erreur et stopper l'application
      DialogueErreur.afficherErreurFatale(e);
    }
  }
  
  /**
   * Traitement principal
   */
  private void lancerTraitementPrincipal(String pParametres) {
    // Analyse des paramètres
    parametresClient.setParametres(pParametres);
    
    // Initialisation de l'utilisateur
    EnvUser envUser = new EnvUser();
    envUser.setServeurSGM(parametresClient.getAdresseServeur());
    envUser.setPortSGM(parametresClient.getPortServeur());
    envUser.getDossierDesktop().setDossierRacine(parametresClient.getDossierRacineDesktop());
    ManagerSessionClient.getInstance().setEnvUser(envUser);
    ManagerSessionClient.getInstance().getEnvUser().setDebugStatus(parametresClient.isModeDebug());
    
    // Initialiser le fichier de traces
    Trace.demarrerLogiciel(parametresClient.getDossierRacineDesktop(), "desktop", "SERIE N DESKTOP");
    Trace.setModeDebug(parametresClient.isModeDebug());
    
    // Tracer les informations clients
    tracerInformationsClient();
    
    // Activer le gestionnaire pour les exceptions non capturées uniquement en mode débug pour ne pas perturber les utilisateurs finaux
    // SNC-2538
    if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
      SwingExceptionHandler.activer();
    }
    
    // Nettoyer les fichiers inutiles
    Trace.titre("NETTOYER LES FICHIERS");
    nettoyerDossierWeb();
    nettoyerDossierConfig();
    nettoyerTmp(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTemp(),
        parametresClient.getDelaiConservationFichiers());
    
    // Initialisation RMI
    Trace.titre("INITIALISER CLIENT RMI");
    ManagerClientRMI.configurerServeur(parametresClient.getAdresseServeur(), parametresClient.getPortServeur());
    
    // Vérifier la version du serveur
    Trace.titre("VERIFIER VERSIONS");
    verifierConcordanceVersion();
    
    // Créer l'objet client desktop côté serveur qui permet de rassembler toutes les données de ce client Desktop
    Trace.titre("CREER CLIENT DESKTOP");
    final IdClientDesktop idClientDesktop = ManagerServiceSession.creerClientDesktop();
    ManagerSessionClient.getInstance().getEnvUser().setIdClientDesktop(idClientDesktop);
    ManagerSessionClient.getInstance().getEnvUser().getDossierServeur()
        .setDossierRacine(ManagerServiceSession.getNomDossierRacineServeur());
    
    // Tracer la fin du démarrage du client
    Trace.titre("FIN DEMARRAGE SERIE N DESKTOP");
    
    // Afficher la boite de dialogue permettant l'identification de l'utilisateur (dans la thread graphique EDT)
    SwingUtilities.invokeLater(new Runnable() {
      @Override
      public void run() {
        try {
          boolean connexion = DialogueIdentification.afficher("Connexion à Série N");
          if (connexion) {
            ManagerSessionClient.getInstance().getEnvUser().setTranslationTable();
            ManagerSessionClient.getInstance().getEnvUser().setTranslationImage();
            
            ClientSerieN client = new ClientSerieN(VERSION);
            client.setVisible(true);
            client.controlerSauvegarde();
            client.controlerVersion();
            client.controlerFinValiditeLicence();
          }
          else {
            // Désenregistrer le client Desktop du serveur
            ManagerServiceSession.detruireClientDesktop(idClientDesktop);
            
            // Tracer l'arrêt du logiciel
            Trace.titre("ARRET DE SERIE N DESKTOP");
            
            // Arrêter le logiciel
            System.exit(0);
          }
        }
        catch (Exception e) {
          DialogueErreur.afficher(e);
        }
      }
    });
  }
  
  /**
   * Tracer les informations intéressantes au démarrage du client.
   */
  private void tracerInformationsClient() {
    // Récupérer des informations sur le jar exécuté (localisation, date de dernière modification)
    final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    String cheminJarPrincipal = "";
    File fichierJarPrincipal = null;
    String dateJarPrincipal = null;
    try {
      cheminJarPrincipal = Application.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
      fichierJarPrincipal = new File(cheminJarPrincipal);
      dateJarPrincipal = dateFormat.format(fichierJarPrincipal.lastModified());
    }
    catch (URISyntaxException e) {
      Trace.erreur(e, "Impossible de récupérer le chemin du JAR courant");
      return;
    }
    
    // Tracer la ligne de commande utilisée
    Trace.info("java -jar " + fichierJarPrincipal + " " + parametresClient.getLigneCommande());
    
    // Tracer les paramètres du client
    parametresClient.tracer();
    
    // Afficher l'environnement Java
    Trace.titre("ENVIRONNEMENT JAVA");
    Trace.info("Nom JVM (java.vm.name)                    : " + System.getProperty("java.vm.name"));
    Trace.info("Version JVM (java.version)                : " + System.getProperty("java.version"));
    Trace.info("Java Home (java.home)                     : " + System.getProperty("java.home"));
    Trace.info("Fichier JAR principal                     : " + fichierJarPrincipal);
    Trace.info("Derniere modification du JAR              : " + dateJarPrincipal);
    
    // Afficher le classpath
    Trace.info("Classpath                                 :");
    String classPath = System.getProperty("java.class.path");
    if (classPath != null) {
      String listPath[] = classPath.split(";");
      for (String path : listPath) {
        Trace.info(path);
      }
    }
  }
  
  /**
   * Vérifier la conconcordance entre la version du client desktop et la version du servur.
   * Une MessageErreurException est généré en cas d'écart.
   */
  private void verifierConcordanceVersion() {
    // Récupérer la version du serveur
    String versionServeur = ManagerServiceSession.getVersion();
    
    // Récupérer la version du client
    String versionClient = null;
    try {
      versionClient = JarUtils.getVersionJar(getClass().getClassLoader());
    }
    catch (Exception e) {
      throw new MessageErreurException("Impossible de récupérer la version du client desktop");
    }
    
    // Tracer les deux versions
    Trace.info("Version serien-app-serveur : " + versionServeur);
    Trace.info("Version serien-app-desktop : " + versionClient);
    
    // Comparer les versions
    if (!versionServeur.equals(versionClient)) {
      throw new MessageErreurException("La version du client desktop (" + versionClient
          + ") ne correspond pas à la version du serveur métier (" + versionServeur + "). Le logiciel ne peut pas démarrer.");
    }
  }
  
  /**
   * Supprimer tous les fichiers obsolètes du dossier WWW
   */
  private void nettoyerDossierWeb() {
    // Déterminer le chemin à nettoyer
    File dossierWeb = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierWeb();
    Trace.info("Nettoyer le dossier : " + dossierWeb.getAbsolutePath());
    
    // Lister les fichiers
    List<File> listeFichiers = FileNG.filtreFichier(dossierWeb, null, new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return true;
      }
    });
    if (listeFichiers == null || listeFichiers.isEmpty()) {
      return;
    }
    
    // Supprimer les fichiers indiqués comme obsolètes
    for (File file : listeFichiers) {
      for (String file2remove : listFileToRemoveInWeb) {
        if (file.getName().equals(file2remove)) {
          if (file.delete()) {
            Trace.info("Supprimer le fichier : " + file.getAbsolutePath());
          }
        }
      }
    }
  }
  
  /**
   * Supprimer tous les fichiers obsolètes du dossier config.
   */
  private void nettoyerDossierConfig() {
    // Déterminer le chemin à nettoyer
    File dossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig();
    Trace.info("Nettoyer le dossier : " + dossierConfig.getAbsolutePath());
    
    // Lister les fichiers .xml
    File[] listeFichiers = dossierConfig.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File arg0, String arg1) {
        return arg1.endsWith(".xml");
      }
    });
    if (listeFichiers == null) {
      return;
    }
    
    // Supprimer tous les fichiers xml car ils sont obsolètes pour cette version
    for (File file : listeFichiers) {
      if (file.delete()) {
        Trace.info("Supprimer le fichier : " + file.getAbsolutePath());
      }
    }
  }
  
  /**
   * Supprimer les fichiers et dossiers trop vieux dans le dossier temporaire.
   */
  private void nettoyerTmp(File dossier, int delaideconser) {
    Trace.info("Nettoyer le dossier : " + dossier);
    
    // Sortir si le dossier n'existe pas
    if (dossier == null || !dossier.exists() || delaideconser <= 0) {
      return;
    }
    
    // Calculer la date d'aujourd'hui - le délai de conservation (en milli seconde)
    Date today = new Date();
    long datemax = today.getTime() - (delaideconser * 24 * 60 * 60 * 1000);
    
    // Effacer les fichiers plus anciens
    File[] listeContenu = dossier.listFiles();
    for (int i = 0; i < listeContenu.length; i++) {
      if (listeContenu[i].lastModified() < datemax) {
        FileNG.remove(listeContenu[i]);
        Trace.info("Supprimer le fichier : " + listeContenu[i]);
      }
    }
  }
}
