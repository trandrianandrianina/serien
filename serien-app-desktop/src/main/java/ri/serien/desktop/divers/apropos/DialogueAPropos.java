/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers.apropos;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JList;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import ri.serien.desktop.sessions.SessionJava;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Affichage de l'écran "A propos" du logiciel Série N.
 * 
 * Composé du logo de l'application, du numéro de version et de deux boutons :
 * - Quitter la boîte de dialogue,
 * - Ouvrir une boîte de dialogue de détails sur les différents paramètres de l'application.
 */
public class DialogueAPropos extends AbstractVueDialogue<ModeleDialogueAPropos> {
  
  /**
   * Constructeur.
   * 
   * @param Modèle associé.
   */
  public DialogueAPropos(ModeleDialogueAPropos pModele) {
    super(pModele);
  }
  
  // -- Constantes
  private static final int LARGEUR_APROPOS = 400;
  private static final int HAUTEUR_APROPOS = 250;
  private static final int LARGEUR_APROPOS_DETAIL = 700;
  private static final int HAUTEUR_APROPOS_DETAIL = 700;
  private static final String AFFICHER_DETAIL = "Afficher détails";
  private static final String MASQUER_DETAIL = "Masquer détails";
  
  // -- Variables
  private DefaultListModel modeleListe = new DefaultListModel();
  
  // -- Méthodes publiques
  
  /**
   * Affiche la boîte de dialogue
   * @param pSession Session Java
   * @param pSessionActive Nombre de sessions actives
   */
  public static boolean afficher(SessionJava pSession, int pSessionActive) {
    ModeleDialogueAPropos modele = new ModeleDialogueAPropos(pSession, pSessionActive);
    DialogueAPropos vue = new DialogueAPropos(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);
    
    // Construction de la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBouton(AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(MASQUER_DETAIL, 'd', false);
    
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirVersion();
    rafraichirAffichageDetail();
    rafraichirBoutonDetail();
  }
  
  /**
   * Rafraîchit l'affichage des boutons en fonction de si le détail des informations est affiché ou non.
   */
  private void rafraichirBoutonDetail() {
    boolean afficherDetail = getModele().isAfficherDetail();
    
    snBarreBouton.getBouton(AFFICHER_DETAIL).setVisible(!afficherDetail);
    snBarreBouton.getBouton(AFFICHER_DETAIL).setEnabled(!afficherDetail);
    
    snBarreBouton.getBouton(MASQUER_DETAIL).setVisible(afficherDetail);
    snBarreBouton.getBouton(MASQUER_DETAIL).setEnabled(afficherDetail);
  }
  
  /**
   * Rafraîchir l'affichage du panel contenant les informations détaillées à propos du logiciel.
   */
  private void rafraichirAffichageDetail() {
    boolean afficherDetail = getModele().isAfficherDetail();
    
    pnlDetailsJVM.setVisible(afficherDetail);
    pnlVersion.setVisible(!afficherDetail);
    
    if (afficherDetail) {
      // Modifie la taille de la fenêtre pour afficher toutes les informations.
      setSize(LARGEUR_APROPOS_DETAIL, HAUTEUR_APROPOS_DETAIL);
      // Affiche le panneau de détails et masque le panneau contenant la version et le logo de l'application.
      // Le logo est masqué pour un gain de place lorsque les informations détaillées sont affichées.
      // Rafraîchit l'affichage des composants.
      rafraichirVersionLogiciel();
      rafraichirVersionJRE();
      rafraichirServeurHote();
      rafraichirPort();
      rafraichirDevice();
      rafraichirSessionActive();
      rafraichirLettreEnvironnement();
      rafraichirDossierRacine();
      rafraichirDossierRacineServeur();
      rafraichirAdresseIP();
      rafraichirListeParametresJVM();
    }
    else {
      // Modifie la taille de la fenêtre lorsque les informations détaillées sont masquées.
      setSize(LARGEUR_APROPOS, HAUTEUR_APROPOS);
    }
  }
  
  /**
   * Rafraichir l'affichage du libellé contenant la version du logiciel.
   */
  private void rafraichirVersion() {
    if (getModele().getVersionLogiciel() != null) {
      lbVersionSerieN.setText(getModele().getVersionLogiciel());
    }
    else {
      lbVersionSerieN.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de la version du logiciel.
   */
  private void rafraichirVersionLogiciel() {
    if (getModele().getVersionLogiciel() != null) {
      lbVersionLogiciel.setText(getModele().getVersionLogiciel());
    }
    else {
      lbVersionLogiciel.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de la version de Java.
   */
  private void rafraichirVersionJRE() {
    if (getModele().getVersionJRE() != null) {
      lbVersionJRE.setText(getModele().getVersionJRE());
    }
    else {
      lbVersionJRE.setText("");
    }
    
  }
  
  /**
   * Rafraichir le libellé d'affichage du serveur hôte.
   */
  private void rafraichirServeurHote() {
    if (getModele().getServeurHote() != null) {
      lbServeurHote.setText(getModele().getServeurHote());
    }
    else {
      lbServeurHote.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage du port.
   */
  private void rafraichirPort() {
    if (getModele().getPort() != null) {
      lbPort.setText(getModele().getPort());
    }
    else {
      lbPort.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de l'identifiant de l'appareil.
   */
  private void rafraichirDevice() {
    if (getModele().getDevice() != null) {
      lbDevice.setText(getModele().getDevice());
    }
    else {
      lbDevice.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage du nombre de sessions actives.
   */
  private void rafraichirSessionActive() {
    if (getModele().getNombreSessionActive() != null) {
      lbSessionsActives.setText(getModele().getNombreSessionActive());
    }
    else {
      lbSessionsActives.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de la lettre d'environnement.
   */
  private void rafraichirLettreEnvironnement() {
    if (getModele().getLettreEnvironnement() != null) {
      lbLettreEnvironnement.setText(getModele().getLettreEnvironnement());
    }
    else {
      lbLettreEnvironnement.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage du chemin d'accès au dossier racine.
   */
  private void rafraichirDossierRacine() {
    if (getModele().getDossierRacine() != null) {
      lbDossierRacine.setText(getModele().getDossierRacine());
    }
    else {
      lbDossierRacine.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage du chemin d'accès au dossier racine du serveur.
   */
  private void rafraichirDossierRacineServeur() {
    if (getModele().getDossierRacineServeur() != null) {
      lbDossierRacineServeur.setText(getModele().getDossierRacineServeur());
    }
    else {
      lbDossierRacineServeur.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de l'adresse IP.
   */
  private void rafraichirAdresseIP() {
    if (getModele().getAdresseIP() != null) {
      lbAdresseIP.setText(getModele().getAdresseIP());
    }
    else {
      lbAdresseIP.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé d'affichage de la liste des paramètres de la JVM.
   */
  private void rafraichirListeParametresJVM() {
    if (getModele().getListeParametreJVM() == null) {
      lbListeParametreJVM.setVisible(false);
      listeParametreJVM.setVisible(false);
    }
    modeleListe.clear();
    for (String parametreJVM : getModele().getListeParametreJVM()) {
      modeleListe.addElement(parametreJVM);
    }
    lbListeParametreJVM.setVisible(true);
    listeParametreJVM.setVisible(true);
    listeParametreJVM.setModel(modeleListe);
  }
  
  /**
   * Gérer les clics sur les boutons.
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecValidation();
      }
      
      else if (pSNBouton.isBouton(AFFICHER_DETAIL)) {
        getModele().modifierAfficherDetail(true);
      }
      else if (pSNBouton.isBouton(MASQUER_DETAIL)) {
        getModele().modifierAfficherDetail(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlAPropos = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlVersion = new SNPanel();
    lbLogoSerieN = new SNLabelChamp();
    lbLibelleVersionSerieN = new SNLabelChamp();
    lbVersionSerieN = new SNLabelChamp();
    pnlDetailsJVM = new SNPanel();
    pnlDetails = new SNPanel();
    lbLibelleVersionLogiciel = new SNLabelChamp();
    lbVersionLogiciel = new SNLabelUnite();
    lbLibelleVersionJRE = new SNLabelChamp();
    lbVersionJRE = new SNLabelUnite();
    lbLibelleServeurHote = new SNLabelChamp();
    lbServeurHote = new SNLabelUnite();
    lbLibellePort = new SNLabelChamp();
    lbPort = new SNLabelUnite();
    lbLibelleDevice = new SNLabelChamp();
    lbDevice = new SNLabelUnite();
    lbLibelleSessionsActives = new SNLabelChamp();
    lbSessionsActives = new SNLabelUnite();
    lbLibelleLettreEnvironnement = new SNLabelChamp();
    lbLettreEnvironnement = new SNLabelUnite();
    lbLibelleDossierRacine = new SNLabelChamp();
    lbDossierRacine = new SNLabelUnite();
    lbLibelleDossierRacineServeur = new SNLabelChamp();
    lbDossierRacineServeur = new SNLabelUnite();
    lbLibelleAdresseIP = new SNLabelChamp();
    lbAdresseIP = new SNLabelUnite();
    lbListeParametreJVM = new SNLabelTitre();
    scrollPane1 = new JScrollPane();
    listeParametreJVM = new JList();
    
    // ======== this ========
    setBackground(new Color(238, 238, 210));
    setTitle("A propos de S\u00e9rie N");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlAPropos ========
    {
      pnlAPropos.setName("pnlAPropos");
      pnlAPropos.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlAPropos.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlVersion ========
        {
          pnlVersion.setName("pnlVersion");
          pnlVersion.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlVersion.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlVersion.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlVersion.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlVersion.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbLogoSerieN ----
          lbLogoSerieN.setIcon(new ImageIcon(getClass().getResource("/images/Logo S\u00e9rie N fond transparent 200x100.png")));
          lbLogoSerieN.setHorizontalAlignment(SwingConstants.CENTER);
          lbLogoSerieN.setPreferredSize(new Dimension(150, 100));
          lbLogoSerieN.setMinimumSize(new Dimension(150, 100));
          lbLogoSerieN.setMaximumSize(new Dimension(150, 100));
          lbLogoSerieN.setName("lbLogoSerieN");
          pnlVersion.add(lbLogoSerieN, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLibelleVersionSerieN ----
          lbLibelleVersionSerieN.setText("Version du logiciel :");
          lbLibelleVersionSerieN.setPreferredSize(new Dimension(200, 30));
          lbLibelleVersionSerieN
              .setFont(lbLibelleVersionSerieN.getFont().deriveFont(lbLibelleVersionSerieN.getFont().getStyle() | Font.BOLD));
          lbLibelleVersionSerieN.setName("lbLibelleVersionSerieN");
          pnlVersion.add(lbLibelleVersionSerieN, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbVersionSerieN ----
          lbVersionSerieN.setText("text");
          lbVersionSerieN.setMinimumSize(new Dimension(70, 30));
          lbVersionSerieN.setPreferredSize(new Dimension(70, 30));
          lbVersionSerieN.setHorizontalAlignment(SwingConstants.LEADING);
          lbVersionSerieN.setName("lbVersionSerieN");
          pnlVersion.add(lbVersionSerieN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlVersion, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlDetailsJVM ========
        {
          pnlDetailsJVM.setBackground(new Color(238, 238, 210));
          pnlDetailsJVM.setName("pnlDetailsJVM");
          pnlDetailsJVM.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDetailsJVM.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDetailsJVM.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDetailsJVM.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDetailsJVM.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          
          // ======== pnlDetails ========
          {
            pnlDetails.setName("pnlDetails");
            pnlDetails.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDetails.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlDetails.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlDetails.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlDetails.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbLibelleVersionLogiciel ----
            lbLibelleVersionLogiciel.setText("Version du logiciel :");
            lbLibelleVersionLogiciel.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleVersionLogiciel.setName("lbLibelleVersionLogiciel");
            pnlDetails.add(lbLibelleVersionLogiciel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbVersionLogiciel ----
            lbVersionLogiciel.setName("lbVersionLogiciel");
            pnlDetails.add(lbVersionLogiciel, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleVersionJRE ----
            lbLibelleVersionJRE.setText("Version JRE (Java) :");
            lbLibelleVersionJRE.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleVersionJRE.setName("lbLibelleVersionJRE");
            pnlDetails.add(lbLibelleVersionJRE, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbVersionJRE ----
            lbVersionJRE.setName("lbVersionJRE");
            pnlDetails.add(lbVersionJRE, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleServeurHote ----
            lbLibelleServeurHote.setText("Serveur h\u00f4te :");
            lbLibelleServeurHote.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleServeurHote.setName("lbLibelleServeurHote");
            pnlDetails.add(lbLibelleServeurHote, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbServeurHote ----
            lbServeurHote.setName("lbServeurHote");
            pnlDetails.add(lbServeurHote, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibellePort ----
            lbLibellePort.setText("Port :");
            lbLibellePort.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibellePort.setName("lbLibellePort");
            pnlDetails.add(lbLibellePort, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPort ----
            lbPort.setName("lbPort");
            pnlDetails.add(lbPort, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleDevice ----
            lbLibelleDevice.setText("Device (pr\u00e9fixe) :");
            lbLibelleDevice.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleDevice.setName("lbLibelleDevice");
            pnlDetails.add(lbLibelleDevice, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDevice ----
            lbDevice.setName("lbDevice");
            pnlDetails.add(lbDevice, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleSessionsActives ----
            lbLibelleSessionsActives.setText("Nombre de sessions actives :");
            lbLibelleSessionsActives.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleSessionsActives.setName("lbLibelleSessionsActives");
            pnlDetails.add(lbLibelleSessionsActives, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbSessionsActives ----
            lbSessionsActives.setName("lbSessionsActives");
            pnlDetails.add(lbSessionsActives, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleLettreEnvironnement ----
            lbLibelleLettreEnvironnement.setText("Lettre d'environnement :");
            lbLibelleLettreEnvironnement.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleLettreEnvironnement.setName("lbLibelleLettreEnvironnement");
            pnlDetails.add(lbLibelleLettreEnvironnement, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbLettreEnvironnement ----
            lbLettreEnvironnement.setName("lbLettreEnvironnement");
            pnlDetails.add(lbLettreEnvironnement, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleDossierRacine ----
            lbLibelleDossierRacine.setText("Dossier racine :");
            lbLibelleDossierRacine.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleDossierRacine.setPreferredSize(new Dimension(250, 30));
            lbLibelleDossierRacine.setName("lbLibelleDossierRacine");
            pnlDetails.add(lbLibelleDossierRacine, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDossierRacine ----
            lbDossierRacine.setName("lbDossierRacine");
            pnlDetails.add(lbDossierRacine, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleDossierRacineServeur ----
            lbLibelleDossierRacineServeur.setText("Dossier racine serveur :");
            lbLibelleDossierRacineServeur.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleDossierRacineServeur.setName("lbLibelleDossierRacineServeur");
            pnlDetails.add(lbLibelleDossierRacineServeur, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbDossierRacineServeur ----
            lbDossierRacineServeur.setName("lbDossierRacineServeur");
            pnlDetails.add(lbDossierRacineServeur, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelleAdresseIP ----
            lbLibelleAdresseIP.setText("Adresse IP du poste :");
            lbLibelleAdresseIP.setFont(new Font("sansserif", Font.BOLD, 14));
            lbLibelleAdresseIP.setName("lbLibelleAdresseIP");
            pnlDetails.add(lbLibelleAdresseIP, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbAdresseIP ----
            lbAdresseIP.setText(" ");
            lbAdresseIP.setName("lbAdresseIP");
            pnlDetails.add(lbAdresseIP, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDetailsJVM.add(pnlDetails, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbListeParametreJVM ----
          lbListeParametreJVM.setText("Param\u00e8tres JVM");
          lbListeParametreJVM.setName("lbListeParametreJVM");
          pnlDetailsJVM.add(lbListeParametreJVM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- listeParametreJVM ----
            listeParametreJVM.setFont(new Font("sansserif", Font.PLAIN, 14));
            listeParametreJVM.setBackground(Color.white);
            listeParametreJVM.setName("listeParametreJVM");
            scrollPane1.setViewportView(listeParametreJVM);
          }
          pnlDetailsJVM.add(scrollPane1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlDetailsJVM, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlAPropos.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlAPropos, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlAPropos;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlVersion;
  private SNLabelChamp lbLogoSerieN;
  private SNLabelChamp lbLibelleVersionSerieN;
  private SNLabelChamp lbVersionSerieN;
  private SNPanel pnlDetailsJVM;
  private SNPanel pnlDetails;
  private SNLabelChamp lbLibelleVersionLogiciel;
  private SNLabelUnite lbVersionLogiciel;
  private SNLabelChamp lbLibelleVersionJRE;
  private SNLabelUnite lbVersionJRE;
  private SNLabelChamp lbLibelleServeurHote;
  private SNLabelUnite lbServeurHote;
  private SNLabelChamp lbLibellePort;
  private SNLabelUnite lbPort;
  private SNLabelChamp lbLibelleDevice;
  private SNLabelUnite lbDevice;
  private SNLabelChamp lbLibelleSessionsActives;
  private SNLabelUnite lbSessionsActives;
  private SNLabelChamp lbLibelleLettreEnvironnement;
  private SNLabelUnite lbLettreEnvironnement;
  private SNLabelChamp lbLibelleDossierRacine;
  private SNLabelUnite lbDossierRacine;
  private SNLabelChamp lbLibelleDossierRacineServeur;
  private SNLabelUnite lbDossierRacineServeur;
  private SNLabelChamp lbLibelleAdresseIP;
  private SNLabelUnite lbAdresseIP;
  private SNLabelTitre lbListeParametreJVM;
  private JScrollPane scrollPane1;
  private JList listeParametreJVM;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
