/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.divers;

import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;

import ri.serien.libcommun.outils.session.EnvUser;

/**
 * Boite de dialogue gérant les options pour l'interface de l'utilisateur courant
 */
class OptionsDialog extends JDialog implements ActionListener {
  
  // Constantes erreurs
  // private final static String ERREUR_RENCONTREE = "Erreur pendant l'ex\u00e9cution : ";
  
  // Déclaration des composants
  public JPopupMenu popup;
  
  // Composants graphiques
  JLabel l_langue = null;
  JLabel l_police = null;
  JTextField t_langue = null;
  JTextField t_police = null;
  JButton b_validation = new JButton();
  JButton b_annuler = new JButton();
  
  // Variables
  private EnvUser infoUser = null;
  
  /**
   * Constructeur
   */
  public OptionsDialog(EnvUser infos) {
    
    // Initialisation des variables
    infoUser = infos;
    prompt();
  }
  
  /**
   * Affiche l'écran Prompt
   */
  public void prompt() {
    int x = 0;
    int y = 0;
    // File ii = null;
    
    // Ajout des composants pour l'identification de la session vers la
    setSize(320, 240);
    JPanel mire = new JPanel();
    setContentPane(mire);
    mire.setLayout(null);
    
    // La police
    Font font = new Font("Arial", Font.PLAIN, 12);
    
    l_langue = new JLabel("Langue");
    l_langue.setBounds(50, 50, 70, 20);
    mire.add(l_langue);
    l_langue.setFont(font);
    l_langue.setForeground(new Color(0, 0, 130));
    
    l_police = new JLabel("Taille Police");
    l_police.setBounds(50, 100, 70, 20);
    mire.add(l_police);
    l_police.setFont(font);
    l_police.setForeground(new Color(0, 0, 130));
    
    t_langue = new JTextField(infoUser.getLangue());
    t_langue.setBounds(150, 50, 20, 20);
    mire.add(t_langue);
    t_langue.setFont(font);
    t_langue.setForeground(new Color(0, 0, 130));
    
    t_police = new JTextField(Integer.toString(infoUser.getDeltaPersoTailleFont()));
    t_police.setBounds(150, 100, 20, 20);
    mire.add(t_police);
    t_police.setFont(font);
    t_police.setForeground(new Color(0, 0, 130));
    // TODO SV: ajouter un la gestion du growicon (deja vue ailleurs)
    // ii = new File("..\\images\\oui64.png");
    // if (!ii.exists())
    // {
    b_validation.setText("Connecter");
    b_validation.setForeground(new Color(0, 0, 130));
    b_validation.setBounds(220, 150, 80, 20);
    /*  }
      else
      {
      b_validation.setIconeGrowOver("..\\images\\oui64.png");
      b_validation.setBounds(220, 115, 92, 92);
      }
      */
    b_validation.addActionListener(this);
    mire.add(b_validation);
    
    // ii = new File("..\\images\\non64.png");
    // if (!ii.exists())
    // {
    b_annuler.setText("Annuler");
    b_annuler.setForeground(new Color(0, 0, 130));
    b_annuler.setBounds(120, 150, 80, 20);
    /*  }
      else
      {
      b_annuler.setIconeGrowOver("..\\images\\non64.png");
      b_annuler.setBounds(120, 115, 92, 92);
      }
      */
    b_annuler.addActionListener(this);
    mire.add(b_annuler);
    
    // Positionnement de la fenêtre au centre de l'écran
    x = Toolkit.getDefaultToolkit().getScreenSize().width;
    y = Toolkit.getDefaultToolkit().getScreenSize().height;
    setLocation(((x / 2) - (getWidth() / 2)), ((y / 2) - (getHeight() / 2)));
    
    setTitle("Options");
    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    setModal(true);
    setResizable(false);
    
    setVisible(true);
  }
  
  /**
   * Gestion des évènements sur le panel
   */
  @Override
  public void actionPerformed(ActionEvent evt) {
    Object source = evt.getSource();
    
    if (source == b_validation) {
      // Initialisation des infos utilisteur en fonction de l'environnement choisi
      infoUser.setDeltaPersoTailleFont(Integer.parseInt(t_police.getText()));
      infoUser.setLangue(t_langue.getText());
      setVisible(false);
    }
    
    if (source == b_annuler) {
      infoUser = null;
      setVisible(false);
    }
  }
  
}
