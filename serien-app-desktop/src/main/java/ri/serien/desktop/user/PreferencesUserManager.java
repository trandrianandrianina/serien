/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.user;

import java.io.File;
import java.io.FilenameFilter;
import java.lang.reflect.Type;
import java.util.Date;
import java.util.LinkedHashMap;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;

/**
 * Permet de gérer (enregistrement, lecture) les préférences de l'utilisateur ainsi l'historique des menus
 * On lit 2 fois le fichier, le local et celui à distance pour comparer les num
 */
public class PreferencesUserManager {
  // Constantes
  private static final String PREF_PREFERENCES = "preferences_";
  private static final String PREF_HISTO = "historique_";
  private static final int PREFERENCES = 0;
  private static final int HISTORIQUE = 1;
  private static final String EXTENSION = ".pref";
  
  // Variables
  private PreferencesUser prefsUser = null;
  private Gson gson = new Gson();
  
  /**
   * Retourne les préférences de l'utilisateur
   * @return the prefsUser
   */
  public PreferencesUser getPreferences() {
    if (prefsUser == null) {
      if (!chargePreferencesUser()) {
        prefsUser = new PreferencesUser();
      }
    }
    return prefsUser;
  }
  
  /**
   * Sauvegarde les préférences d'un utilisateur sur disque
   */
  public void sauvePreferencesUser() {
    prefsUser = getPreferences();
    if (!prefsUser.isModify()) {
      return;
    }
    // Sauvegarde du fichier sur disque
    String nomDossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig().getAbsolutePath();
    String nomfichier = nomDossierConfig + File.separatorChar + PREF_PREFERENCES
        + ManagerSessionClient.getInstance().getProfil().toLowerCase() + EXTENSION;
    try {
      prefsUser.incrementeNumero(); // A chaque sauvegarde on incrémente le numéro
      
      // On enregistre le fichier au format JSON
      String chaine = gson.toJson(prefsUser, PreferencesUser.class);
      GestionFichierTexte gft = new GestionFichierTexte(nomfichier);
      gft.initUnicode();
      gft.setContenuFichier(chaine);
      gft.ecritureFichier();
      gft.dispose();
      
      // Envoi vers le serveur
      File preferenceFile = new File(nomfichier);
      nomDossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierConfig();
      String dossierServeur = nomDossierConfig + ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getSeparateur();
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierAUploader(preferenceFile, dossierServeur);
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierUploade(preferenceFile, dossierServeur);
      prefsUser.resetModify();
    }
    catch (Exception e) {
      Trace.erreur(e, "Problème rencontré lors de la sauvegarde du fichier " + nomfichier + " des préférences utilisateur.");
    }
  }
  
  /**
   * Charge les préférences d'un utilisateur à partir d'un fichier.
   * Nouvel algo qui doit mieux gérer les coupures réseaux.
   */
  private boolean chargePreferencesUser() {
    // On charge le fichier de préférence local s'il existe
    File prefs = getFichierConfig(ManagerSessionClient.getInstance().getProfil(), PREFERENCES);
    prefsUser = deserialiserPreferencesUser(prefs);
    // On construit le nom du fichier dans le cas où il serait null
    if (prefs == null) {
      prefs = new File(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig().getAbsolutePath()
          + File.separatorChar + PREF_PREFERENCES + ManagerSessionClient.getInstance().getProfil().toLowerCase() + EXTENSION);
    }
    
    // On charge le fichier distant
    FileNG nomfichierpreftemp = recupererPreferencesSurServeur();
    PreferencesUser prefsUserTemp = deserialiserPreferencesUser(nomfichierpreftemp);
    
    // On compare les 2 fichiers afin de connaitre le plus récent des 2
    if ((prefsUser != null) && (prefsUserTemp != null)) {
      if (prefsUser.getNumero() < prefsUserTemp.getNumero()) {
        nomfichierpreftemp.copyTo(prefs.getAbsolutePath());
        nomfichierpreftemp.delete();
        prefsUser = prefsUserTemp;
      }
    }
    // Le fichier des préférences existe à distance
    else if (prefsUserTemp != null) {
      nomfichierpreftemp.copyTo(prefs.getAbsolutePath());
      nomfichierpreftemp.delete();
      prefsUser = prefsUserTemp;
    }
    // Le fichier des préférences n'existe pas non plus en local
    else if (prefsUser == null) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Récupération du fichier des préférences de l'utilisateur courant sur le serveur.
   */
  private FileNG recupererPreferencesSurServeur() {
    String nomDossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierConfig();
    String nomfichier = nomDossierConfig + ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getSeparateur()
        + PREF_PREFERENCES + ManagerSessionClient.getInstance().getProfil().toLowerCase() + EXTENSION;
    
    // Construction du nom complet du fichier des préférences utilisateur et suppression dans le dossier temporaire
    String nomDossierTempUser =
        ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierTempUtilisateur().getAbsolutePath();
    FileNG nomFichierTempUser = new FileNG(nomDossierTempUser + File.separatorChar + nomfichier.replace('/', '_'));
    nomFichierTempUser.delete();
    
    // Récupération du fichier sur le serveur
    ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierArecup(nomfichier);
    
    if (!nomFichierTempUser.exists()) {
      return null;
    }
    
    return nomFichierTempUser;
  }
  
  /**
   * Convertion d'un fichier JSON en objet PreferencesUser.
   */
  private PreferencesUser deserialiserPreferencesUser(File pFichierPreferences) {
    if (pFichierPreferences == null) {
      return null;
    }
    PreferencesUser preferencesUser = null;
    try {
      GestionFichierTexte gft = new GestionFichierTexte(pFichierPreferences.getAbsolutePath());
      gft.initUnicode();
      String chaine = gft.getContenuFichierString(false);
      gft.dispose();
      preferencesUser = gson.fromJson(chaine, PreferencesUser.class);
      preferencesUser.resetModify();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du chargement du fichier des préférences " + pFichierPreferences.getAbsolutePath());
    }
    return preferencesUser;
  }
  
  /**
   * Retourne l'historique de l'utilisateur.
   */
  public LinkedHashMap<Integer, Date> getHistorique() {
    return chargeHistorique();
  }
  
  /**
   * Sauve l'historique d'un utilisateur sur disque.
   */
  public void sauveHistorique(LinkedHashMap<Integer, Date> pListeNumeroPointDeMenu) {
    if ((pListeNumeroPointDeMenu == null) || pListeNumeroPointDeMenu.isEmpty()) {
      return;
    }
    
    String nomDossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig().getAbsolutePath();
    String nomfichier =
        nomDossierConfig + File.separatorChar + PREF_HISTO + ManagerSessionClient.getInstance().getProfil().toLowerCase() + EXTENSION;
    try {
      // On enregistre le fichier au format JSON
      String chaine = gson.toJson(pListeNumeroPointDeMenu);
      GestionFichierTexte gft = new GestionFichierTexte(nomfichier);
      gft.setContenuFichier(chaine);
      gft.ecritureFichier();
      gft.dispose();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la sauvegarde du fichier de l'historique " + nomfichier);
    }
  }
  
  /**
   * Charge le fichier des historiques s'il existe.
   */
  public LinkedHashMap<Integer, Date> chargeHistorique() {
    File histo = getFichierConfig(ManagerSessionClient.getInstance().getProfil().toLowerCase(), HISTORIQUE);
    if (histo == null) {
      return null;
    }
    
    GestionFichierTexte gft = new GestionFichierTexte(histo.getAbsolutePath());
    try {
      String chaine = gft.getContenuFichierString(false);
      gft.dispose();
      Type listType = new TypeToken<LinkedHashMap<Integer, Date>>() {}.getType();
      LinkedHashMap<Integer, Date> liste = gson.fromJson(chaine, listType);
      return liste;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du chargement du fichier de l'historique " + gft.getNomFichier());
    }
    return null;
  }
  
  /**
   * Retourne le fichier préférence de l'utilisateur, null sinon
   */
  private File getFichierConfig(String profil, int type) {
    // On liste le contenu du dossier de configuration en filtrant sur l'extension
    File[] contenuDossier =
        ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig().listFiles(new FilenameFilter() {
          @Override
          public boolean accept(File pDossier, String pNomFichier) {
            return pNomFichier.toLowerCase().endsWith(EXTENSION);
          }
        });
    if (contenuDossier == null) {
      return null;
    }
    profil = profil.toLowerCase();
    
    // On alimente le bon préfixe
    String prefixe = null;
    switch (type) {
      case PREFERENCES:
        prefixe = PREF_PREFERENCES;
        break;
      case HISTORIQUE:
        prefixe = PREF_HISTO;
        break;
      default:
        return null;
    }
    
    // On recherche le bon fichier
    for (int i = 0; i < contenuDossier.length; i++) {
      if (contenuDossier[i].isFile() && contenuDossier[i].getName().startsWith(prefixe + profil)) {
        return contenuDossier[i];
      }
    }
    return null;
  }
  
}
