/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.user;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.outils.encodage.Base64;

/**
 * Assure la persistance des logins et des mots de passe pour chaque sous-environnement.
 * 
 * Cela permet aux utilisateurs de ne pas resaisir leur login lorsqu'ils se connectent. Sur la plage d'adresse du réseau de Toulouse,
 * le mot de passe est également mémorisé.
 * - Les noms des environnements sont stockés dans la liste listeNomSousEnvironnement.
 * - Les utilisateurs et le mot de passe sont stockés dans la liste listeDataSousEnvironnement.
 * 
 * L'objet de cette classe est sérialisé en json. Si le nom des attributs change, les données ne seront pas relues le premier coup.
 * Ce n'est pas génant, l'utilisateur devra simplement remémoriser une fois ses informations de connexion. Voici un exemple :
 * {"nomSousEnvironnementDefaut":"Arinfo","listeNomSousEnvironnement":["Defaut","Arinfo"],
 * "listeDataSousEnvironnement":["UklERVZUSE58dGhhcmR1","UklERVZTVnxnYXIxOTc1"]}
 */
public class PreferencesId {
  // Préfixe des logins utilisés par Résolution Informatique sur l'environnement de développement
  private static final String PREFIXE_LOGIN_RI = "RIDEV";
  
  // Variables à conserver sous forme de types primitifs pour la sérialisation
  private String nomSousEnvironnementDefaut = null;
  private List<String> listeNomSousEnvironnement = null;
  private ArrayList<String> listeDataSousEnvironnement = null;
  private String deviceImpression = null;
  
  /**
   * Retourner l'indice de stockage dans les listes d'un sous-environnement.
   */
  private int getIndiceSousEnvironnement(IdSousEnvironnement pIdEnvironnement) {
    if (pIdEnvironnement == null || listeNomSousEnvironnement == null) {
      return -1;
    }
    for (int i = 0; i < listeNomSousEnvironnement.size(); i++) {
      String nomSousEnvironnement = listeNomSousEnvironnement.get(i);
      if (nomSousEnvironnement.equals(pIdEnvironnement.getNom())) {
        return i;
      }
    }
    return -1;
  }
  
  /**
   * Renseigner l'identifiant du sous-système par défaut.
   * C'est juste le nom (chaîne de caractère) qui est stocké pour la sérialisation.
   */
  public void setIdSousEnvironnementDefaut(IdSousEnvironnement pIdSousSystemeDefaut) {
    if (pIdSousSystemeDefaut != null) {
      nomSousEnvironnementDefaut = pIdSousSystemeDefaut.getNom();
    }
    else {
      nomSousEnvironnementDefaut = null;
    }
  }
  
  /**
   * Identifiant du sous-système par défaut.
   * C'est juste le nom (chaîne de caractère) qui est stocké pour la sérialisation.
   */
  public IdSousEnvironnement getIdSousEnvironnementDefaut() {
    if (nomSousEnvironnementDefaut != null && !nomSousEnvironnementDefaut.isEmpty()) {
      return IdSousEnvironnement.getInstance(nomSousEnvironnementDefaut);
    }
    else {
      return null;
    }
  }
  
  /**
   * Renseigner l'utilisateur et le mot de passe pour un sous-environnement.
   */
  public void setData(IdSousEnvironnement pIdEnvironnement, String pNomUtilisateur, String pMotDePasse) {
    if (pIdEnvironnement == null || pNomUtilisateur == null || pMotDePasse == null) {
      // Pas d'erreur car c'est juste pour la sauvegarde des données de connexion
      return;
    }
    
    // Créer les listes si elle n'existe pas
    if (listeNomSousEnvironnement == null) {
      listeNomSousEnvironnement = new ArrayList<String>();
    }
    if (listeDataSousEnvironnement == null) {
      listeDataSousEnvironnement = new ArrayList<String>();
    }
    
    // Sauvegarder le mot de passe uniquement si le profil commence par RIDEV
    String motDePasseSauve = "";
    if (pNomUtilisateur.startsWith(PREFIXE_LOGIN_RI)) {
      motDePasseSauve = pMotDePasse;
    }
    
    // Rechercher l'indice de stockage du sous-environnement
    int indice = getIndiceSousEnvironnement(pIdEnvironnement);
    if (indice == -1) {
      // Ajouter des éléments aux listes si le sous-environnment n'a pas été trouvé
      listeNomSousEnvironnement.add(pIdEnvironnement.getNom());
      listeDataSousEnvironnement.add(Base64.encodeToString((pNomUtilisateur + '|' + motDePasseSauve).getBytes(), false));
    }
    else {
      listeDataSousEnvironnement.set(indice, Base64.encodeToString((pNomUtilisateur + '|' + motDePasseSauve).getBytes(), false));
    }
  }
  
  /**
   * Retourne l'utilisateur et le mot de passe pour un sous-environnement
   */
  private String[] getData(IdSousEnvironnement pIdEnvironnement) {
    if (pIdEnvironnement == null || listeNomSousEnvironnement == null || listeDataSousEnvironnement == null) {
      // Pas d'erreur car c'est juste pour la sauvegarde des données de connexion
      return new String[] { "", "" };
    }
    
    // Rechercher l'indice de stockage du sous-environnement
    int indice = getIndiceSousEnvironnement(pIdEnvironnement);
    if (indice != -1) {
      // Retourner le profil et le mot de passe si environnement trouvé
      try {
        String chaine = listeDataSousEnvironnement.get(indice);
        chaine = new String(Base64.decode(chaine));
        if (chaine.length() > 0) {
          return chaine.split("\\|");
        }
      }
      catch (Exception e) {
      }
    }
    return new String[] { "", "" };
  }
  
  /**
   * Récupérer l'utilisateur enregistré pour un sous-environnement.
   */
  public String getUtilisateur(IdSousEnvironnement pIdEnvironnement) {
    String[] id = getData(pIdEnvironnement);
    if (id != null && id.length >= 1 && id[0] != null) {
      return id[0];
    }
    return "";
  }
  
  /**
   * Récupérer le mot de passe enregistré pour un sous-environnement.
   */
  public String getMotDePasse(IdSousEnvironnement pIdEnvironnement) {
    // Ne récupèrer le mot de passe que pour les logins commençant par RIDEV (utilisés sur le serveur de développement).
    if (!getUtilisateur(pIdEnvironnement).startsWith(PREFIXE_LOGIN_RI)) {
      return "";
    }
    
    // Récupérer le mot de passe
    String[] id = getData(pIdEnvironnement);
    if (id != null && id.length >= 2) {
      return id[1];
    }
    return "";
    
  }
  
  /**
   * Device d'impression.
   */
  public String getDeviceImpression() {
    if (deviceImpression == null) {
      return "";
    }
    return deviceImpression;
  }
  
  /**
   * Renseigner le device d'impression.
   */
  public void setDeviceImpression(String deviceImpression) {
    this.deviceImpression = deviceImpression;
  }
  
}
