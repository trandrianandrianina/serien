/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.user;

import java.awt.Frame;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libswing.composantrpg.autonome.PositionFenetre;

public class PreferencesUser {
  // Numéro qui est incrémenté à chaque modification (pour la récup sur un autre poste)
  private int numero = 0;
  // Côté d'apparition du menu à gauche
  private int positionMenuCoulissant = 0;
  // Mémorisation du dernier onglet ouvert
  private int numOngletMenuCoulissant = 0;
  private LinkedHashMap<Integer, String> listeNumerosOrdreFavoris = null;
  // Fermer le menu lors du lancement d'une session
  private boolean fermerAutomatiquement = true;
  // Ouvrir le menu à la fermeture de la dernière session
  private boolean ouvrirAutomatiquement = false;
  // Dossier où sont stockés les fichiers pour Report One (fichiers publics)
  private String dossierReportOne = null;
  // Dossier où sont stockés les fichiers pour Report One (fichiers privés)
  private String dossierReportOneprive = null;
  // Liste des index des modules à afficher dans l'onglets menu
  private int indexModulesOuvertureDefaut = 0;
  // Contient la liste des ID des listes que l'on a personnalisé (ID: FORMAT_RECORD_LISTE_TITRE)
  private ArrayList<Integer> listeIDListes = null;
  // Contient l'ordre des colonnes pour les listes de listeIDListes
  private ArrayList<int[]> listeOrdreColonnes = null;
  // Détermine si la fonction recherche dans les menus doit être ouverte ou pas systématiquement
  private boolean rechercheMenuTjrsOuverte = false;
  // Liste des imprimantes que l'utilisateur a paramétré pour son menu contextuel (Imprimer avec)
  private ArrayList<String> listeSelectionImprimantes = null;
  // Sauvegarde du type et de la résolution de la fenêtre
  private PositionFenetre positionFenetre = null;
  // Liste des outq que l'utilisateur a paramétré pour son menu contextuel (Déplacer vers)
  private ArrayList<String> listeSelectionOutq = null;
  // Liste des colonnes du GVC à ne pas afficher
  private int[] listeColonnesGVCACacher = null;
  // Indique s'il y a eu une modification ou pas des données
  private transient boolean modify = false;
  
  /**
   * @param numero the numero to set
   */
  public void setNumero(int numero) {
    this.numero = numero;
  }
  
  /**
   * @return the numero
   */
  public int getNumero() {
    return numero;
  }
  
  public void incrementeNumero() {
    this.numero++;
  }
  
  /**
   * @param positionMenuCoulissant the positionMenuCoulissant to set
   */
  public void setPositionMenuCoulissant(int posMenuCoulissant) {
    positionMenuCoulissant = posMenuCoulissant;
    modify = true;
  }
  
  /**
   * @return the positionMenuCoulissant
   */
  public int getPositionMenuCoulissant() {
    return positionMenuCoulissant;
  }
  
  /**
   * @param numOngletMenuCoulissant the numOngletMenuCoulissant to set
   */
  public void setNumOngletMenuCoulissant(int numOngletMenuCoulissant) {
    if (numOngletMenuCoulissant != this.numOngletMenuCoulissant) {
      this.numOngletMenuCoulissant = numOngletMenuCoulissant;
      modify = true;
    }
  }
  
  /**
   * @return the numOngletMenuCoulissant
   */
  public int getNumOngletMenuCoulissant() {
    return numOngletMenuCoulissant;
  }
  
  /**
   * @param libelleWinPlus the libelleWinPlus to set
   */
  /*
  public void setLibelleWinPlus(String libelleWinPlus) {
    LibelleWinPlus = libelleWinPlus;
    modify = true;
  }*/
  
  /**
   * @return the libelleWinPlus
   */
  /*
  public String getLibelleWinPlus() {
    return LibelleWinPlus;
  }*/
  
  /**
   * @param menuWinPlus the menuWinPlus to set
   */
  /*
  public void setMenuWinPlus(MenuDetail[] menuWinPlus) {
    this.menuWinPlus = menuWinPlus;
    modify = true;
  }*/
  
  /**
   * @return the menuWinPlus
   */
  /*
  public MenuDetail[] getMenuWinPlus() {
    return menuWinPlus;
  }*/
  
  /**
   * @param fermerAutomatiquement the fermerAutomatiquement to set
   */
  public void setFermerAutomatiquement(boolean fermerAutomatiquement) {
    this.fermerAutomatiquement = fermerAutomatiquement;
    modify = true;
  }
  
  /**
   * @return the fermerAutomatiquement
   */
  public boolean isFermerAutomatiquement() {
    return fermerAutomatiquement;
  }
  
  /**
   * @param ouvrirAutomatiquement the ouvrirAutomatiquement to set
   */
  public void setOuvrirAutomatiquement(boolean ouvrirAutomatiquement) {
    this.ouvrirAutomatiquement = ouvrirAutomatiquement;
    modify = true;
  }
  
  /**
   * @return the ouvrirAutomatiquement
   */
  public boolean isOuvrirAutomatiquement() {
    return ouvrirAutomatiquement;
  }
  
  /**
   * @param dossierReportOne the dossierReportOne to set
   */
  public void setDossierReportOne(String dossierReportOne) {
    this.dossierReportOne = dossierReportOne;
    modify = true;
  }
  
  /**
   * @return the dossierReportOne
   */
  public String getDossierReportOne() {
    return dossierReportOne;
  }
  
  /**
   * @param dossierReportOne the dossierReportOneprive to set
   */
  public void setDossierReportOneprive(String dossierReportOne) {
    this.dossierReportOneprive = dossierReportOne;
    modify = true;
  }
  
  /**
   * @return the dossierReportOneprive
   */
  public String getDossierReportOneprive() {
    return dossierReportOneprive;
  }
  
  /**
   * @param indexModulesOuvertureDefaut the indexModulesOuvertureDefaut to set
   */
  public void setIndexModulesOuvertureDefaut(int indexModulesOuvertureDefaut) {
    this.indexModulesOuvertureDefaut = indexModulesOuvertureDefaut;
    modify = true;
  }
  
  /**
   * @return the indexModulesOuvertureDefaut
   */
  public int getIndexModulesOuvertureDefaut() {
    return indexModulesOuvertureDefaut;
  }
  
  /**
   * @param listeIDListes the listeIDListes to set
   */
  public void setListeIDListes(ArrayList<Integer> listeIDListes) {
    this.listeIDListes = listeIDListes;
  }
  
  /**
   * @return the listeIDListes
   */
  public ArrayList<Integer> getListeIDListes() {
    return listeIDListes;
  }
  
  /**
   * @param listeOrdreColonnes the listeOrdreColonnes to set
   */
  public void setListeOrdreColonnes(ArrayList<int[]> listeOrdreColonnes) {
    this.listeOrdreColonnes = listeOrdreColonnes;
    modify = true;
  }
  
  /**
   * @return the listeOrdreColonnes
   */
  public ArrayList<int[]> getListeOrdreColonnes() {
    return listeOrdreColonnes;
  }
  
  /**
   * Retourne si une valeur a été modifié ou pas
   * @return
   */
  public boolean isModify() {
    return modify;
  }
  
  /**
   * Remet à false la variable, utilisé après un chargement de la classe via une sérialisation
   */
  public void resetModify() {
    modify = false;
  }
  
  /**
   * @return le rechercheMenuTjrsOuverte
   */
  public boolean isRechercheMenuTjrsOuverte() {
    return rechercheMenuTjrsOuverte;
  }
  
  /**
   * @param rechercheMenuTjrsOuverte le rechercheMenuTjrsOuverte à définir
   */
  public void setRechercheMenuTjrsOuverte(boolean rechercheMenuTjrsOuverte) {
    this.rechercheMenuTjrsOuverte = rechercheMenuTjrsOuverte;
    modify = true;
  }
  
  /**
   * @return le listeSelectionImprimantes
   */
  public ArrayList<String> getListeSelectionImprimantes() {
    return listeSelectionImprimantes;
  }
  
  /**
   * @param listeSelectionImprimantes le listeSelectionImprimantes à définir
   */
  public void setListeSelectionImprimantes(ArrayList<String> listeSelectionImprimantes) {
    this.listeSelectionImprimantes = listeSelectionImprimantes;
    modify = true;
  }
  
  /**
   * Mémoriser la position de fenêtre dans les préférences utilisateurs.
   */
  public void memoriserPositionFenetre(Frame pFrame) {
    if (positionFenetre == null) {
      positionFenetre = new PositionFenetre();
    }
    positionFenetre.memoriserPosition(pFrame);
    modify = true;
  }
  
  /**
   * Modifier la position de la fenêtre en mettant une taille standard.
   */
  public void positionnerFenetreSuivantStandard(Frame pFrame) {
    if (positionFenetre == null) {
      positionFenetre = new PositionFenetre();
    }
    positionFenetre.positionnerFenetreSuivantStandard(pFrame);
    modify = true;
  }
  
  /**
   * Modifier la position de la fenêtre à partir des préférences utilisateurs.
   */
  public void positionnerFenetre(Frame pFrame) {
    if (positionFenetre == null) {
      positionFenetre = new PositionFenetre();
    }
    positionFenetre.positionnerFenetre(pFrame);
    modify = true;
  }
  
  /**
   * @return le listeSelectionOutq
   */
  public ArrayList<String> getListeSelectionOutq() {
    return listeSelectionOutq;
  }
  
  /**
   * @param listeSelectionOutq le listeSelectionOutq à définir
   */
  public void setListeSelectionOutq(ArrayList<String> listeSelectionOutq) {
    this.listeSelectionOutq = listeSelectionOutq;
    modify = true;
  }
  
  /**
   * @return le listeColonnesGVCACacher
   */
  public int[] getListeColonnesGVCACacher() {
    return listeColonnesGVCACacher;
  }
  
  /**
   * @param listeColonnesGVCACacher le listeColonnesGVCACacher à définir
   */
  public void setListeColonnesGVCACacher(int[] listeColonnesGVCACacher) {
    this.listeColonnesGVCACacher = listeColonnesGVCACacher;
    modify = true;
  }
  
  public LinkedHashMap<Integer, String> getListeNumerosOrdreFavoris() {
    return listeNumerosOrdreFavoris;
  }
  
  public void setListeNumerosOrdreFavoris(LinkedHashMap<Integer, String> listeNumerosOrdreFavoris) {
    this.listeNumerosOrdreFavoris = listeNumerosOrdreFavoris;
    modify = true;
  }
}
