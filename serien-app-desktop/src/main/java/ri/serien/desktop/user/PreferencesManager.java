/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.user;

import java.io.File;

import com.google.gson.Gson;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Permet d'enregistrer des préférence pour une application
 */
public class PreferencesManager {
  // Variables
  private static File fichierPreferences = null;
  private Object preferences = null; // Préférences diverses
  private Class<?> classePreference = null;
  private Gson gson = new Gson();
  
  /**
   * Constructeur
   * @param classe
   */
  public PreferencesManager(Class<?> classe) {
    classePreference = classe;
  }
  
  /**
   * Constructeur
   * @param classe
   */
  public PreferencesManager(Class<?> classe, String chemincomplet) {
    this(classe);
    setNomFichier(chemincomplet);
  }
  
  /**
   * Initialise le nom du fichier
   * @param nomfichier
   */
  public void setNomFichier(String nomfichier) {
    if (nomfichier == null) {
      return;
    }
    
    nomfichier = nomfichier.trim();
    
    // Pour la conversion vers la nouvelle version XML -> JSON (à terme on pourra supprimer ce bloc)
    if (!nomfichier.endsWith(".xml")) {
      fichierPreferences = new File(nomfichier + ".xml");
      if (fichierPreferences.exists()) {
        return;
      }
    }
    if (!nomfichier.endsWith(".json")) {
      fichierPreferences = new File(nomfichier + ".json");
    }
  }
  
  /**
   * Retourne les préférences de l'utilisateur
   * @return the prefsUser
   */
  public Object getPreferences() {
    if (preferences == null) {
      if (!chargePreferences()) {
        instancieClasse();
      }
    }
    return preferences;
  }
  
  /**
   * Initialise l'objet preference
   * @param preferences
   */
  public void setPreferences(PreferencesId preferences) {
    this.preferences = preferences;
  }
  
  /**
   * Sauvegarder les préférences de l'utilisateur sur disque.
   */
  public void sauvePreferences() {
    // Ne rien faire si le fichier des préférences n'est pas initialisé
    // Pas d'erreur car ce n'est pas essentiel
    if (fichierPreferences == null) {
      Trace.alerte("Le fichier des préférences utilisateurs n'est pas initialisé.");
      return;
    }
    
    // Créer le dossier contenant le fichier préférence si celui-ci n'existe pas
    File dossier = fichierPreferences.getParentFile();
    if (!dossier.exists()) {
      dossier.mkdirs();
    }
    
    // Sauvegarder le fichier préférence sur disque
    try {
      // Remplacer le fichier XML (ancien format) par un fichier JSON
      if (fichierPreferences.getName().endsWith(".xml")) {
        fichierPreferences.delete();
        String chaine = fichierPreferences.getAbsolutePath().replaceAll(".xml", ".json");
        fichierPreferences = new File(chaine);
      }
      
      // Enregistrer le fichier au format JSON
      String chaine = gson.toJson(getPreferences(), PreferencesId.class);
      GestionFichierTexte gft = new GestionFichierTexte(fichierPreferences);
      gft.setContenuFichier(chaine);
      gft.ecritureFichier();
      gft.dispose();
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Charge les préférences de l'utilisateur à partir d'un fichier
   */
  private boolean chargePreferences() {
    if (fichierPreferences == null) {
      return false;
    }
    
    try {
      if (fichierPreferences.getName().endsWith(".xml")) {
        preferences = classePreference.cast(XMLTools.decodeFromFile(fichierPreferences.getAbsolutePath()));
      }
      else {
        GestionFichierTexte gft = new GestionFichierTexte(fichierPreferences);
        String chaine = gft.getContenuFichierString(false);
        gft.dispose();
        preferences = gson.fromJson(chaine, PreferencesId.class);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }
  
  /**
   * Supprime le fichier des préférences
   */
  public static void removeFile() {
    if (fichierPreferences == null) {
      return;
    }
    fichierPreferences.delete();
  }
  
  /**
   * Instancie la classe des préférences
   */
  private void instancieClasse() {
    try {
      // Class cl = Class.forName(classePreference.getName());
      Class<?> classe = PreferencesManager.class.getClassLoader().loadClass(classePreference.getName());
      preferences = classe.newInstance();
    }
    catch (Exception e) {
    }
  }
}
