/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.changerbasedonnees;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionRPG;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleChangerBaseDeDonnees extends AbstractModeleDialogue {
  // Variables
  private ClientSerieN clientSerieN = null;
  private BDD actuelleBaseDeDonnees = null;
  private BDD nouvelleBaseDeDonnees = null;
  private ListeBDD listeBaseDeDonnees = null;
  private Version versionSerieN = null;
  private Message message = null;
  
  /**
   * Constructeur.
   */
  public ModeleChangerBaseDeDonnees(SessionBase pSession, ClientSerieN pClientSerieN) {
    super(pSession);
    clientSerieN = pClientSerieN;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    if (ManagerSessionClient.getInstance().getEnvUser() == null) {
      throw new MessageErreurException("Les informations de l'utilisateur sont invalides.");
    }
    
    // Récupération de la version du logiciel
    versionSerieN = ManagerSessionClient.getInstance().getEnvUser().getVersionSerieN();
    if (versionSerieN == null) {
      throw new MessageErreurException("La version de Série N est invalide.");
    }
    
    // Initialisation de la base de données actuelle
    actuelleBaseDeDonnees = ManagerSessionClient.getInstance().getCurlib();
    nouvelleBaseDeDonnees = ManagerSessionClient.getInstance().getCurlib();
  }
  
  @Override
  public void chargerDonnees() {
    chargerListeBaseDeDonnees();
    // Initialisation de la base courante
    if (listeBaseDeDonnees != null && !listeBaseDeDonnees.isEmpty() && nouvelleBaseDeDonnees == null) {
      nouvelleBaseDeDonnees = listeBaseDeDonnees.get(0);
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    mettreAJourInformationUtilisateur();
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifie la base de données de travail.
   */
  public void modifierBaseDeDonnees(BDD pBaseDonnees) {
    if (pBaseDonnees == null) {
      throw new MessageErreurException("Impossible de modifier la base de données car elle est invalide.");
    }
    if (pBaseDonnees.equals(nouvelleBaseDeDonnees)) {
      return;
    }
    nouvelleBaseDeDonnees = pBaseDonnees;
    message = null;
    rafraichir();
  }
  
  /**
   * Retourne si la version de la base de données sélectionnée est identique à la version du logiciel.
   */
  public boolean isVersionIdentique() {
    if (nouvelleBaseDeDonnees == null) {
      return false;
    }
    if (nouvelleBaseDeDonnees.getVersion() == null) {
      throw new MessageErreurException("La version de la base de données est invalide.");
    }
    // Comparaison des versions de Série N et de la base de données sélectionnée
    if (versionSerieN.compareTo(nouvelleBaseDeDonnees.getVersion()) == 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la version de la base de données sélectionnée doit être mise à jour.
   */
  public boolean isMettreAJourBaseDeDonnees() {
    if (nouvelleBaseDeDonnees == null) {
      return false;
    }
    if (nouvelleBaseDeDonnees.getVersion() == null) {
      throw new MessageErreurException("La version de la base de données est invalide.");
    }
    // Comparaison des versions de Série N et de la base de données sélectionnée
    if (versionSerieN.compareTo(nouvelleBaseDeDonnees.getVersion()) == 1) {
      message = Message.getMessageImportant("La base de données doit être migrée pour être utilisable avec la version " + versionSerieN
          + " du logiciel. Noter que les versions précédentes du logiciel ne pourront plus y accéder une fois migrée.");
      return true;
    }
    else if (versionSerieN.compareTo(nouvelleBaseDeDonnees.getVersion()) == -1) {
      message = Message
          .getMessageImportant("La base de données sélectionnée ne peut pas être ouverte car la version de Série N lui est antérieure.");
    }
    return false;
  }
  
  /**
   * Lance la mise à jour de la base de données sélectionnées.
   */
  public void mettreAJourBaseDeDonnees() {
    if (!isMettreAJourBaseDeDonnees()) {
      return;
    }
    // Changement de la curlib et fermeture de la boite de dialogue
    quitterAvecValidation();
    
    // Appel du programme qui lance la mise à jour de la base de données
    EnvProgramClient infoPrg = new EnvProgramClient();
    infoPrg.setProgram("NIVFCHM");
    infoPrg.setLibMenu("Programme Lancement des REC");
    infoPrg.addBib("WEXPAS");
    clientSerieN.ajouterSessionRpg(new SessionRPG(infoPrg, clientSerieN, null));
    // La mise à jour des informations concernant la base de données du desktop sont effectuées après la fermeture du programme RPG des
    // Recs
  }
  
  // -- Méthodes privées
  
  /**
   * Charge la liste des bases de données potentiellement utilisables.
   */
  private void chargerListeBaseDeDonnees() {
    CritereBibliotheque criteres = new CritereBibliotheque();
    criteres.setFiltrePrefixe("FM*");
    criteres.setFiltreBaseDeDonneesUniquement(true);
    listeBaseDeDonnees = ManagerServiceTechnique
        .chargerListeBaseDeDonneesOS400(ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().getIdSession(), criteres);
  }
  
  /**
   * Mise à jour de l'utilisateur et des informations de la nouvelle base de données.
   */
  private void mettreAJourInformationUtilisateur() {
    // La base de données n'a pas changée
    if (Constantes.equals(actuelleBaseDeDonnees, nouvelleBaseDeDonnees)) {
      return;
    }
    
    // Récupération de la description de la base de données sélectionnée, cette récupération est effectuée après la sélection
    // car c'est une opération qui peut prendre beaucoup de temps lors du listage des bases de données (> 70 secondes)
    String description = ManagerServiceTechnique.chargerDescriptionBibliotheque(
        ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().getIdSession(), nouvelleBaseDeDonnees.getId());
    nouvelleBaseDeDonnees.setDescription(description);
    
    // Modification de la curlib avec le base de données sélectionnée
    ManagerSessionClient.getInstance().getEnvUser().setCurlib(nouvelleBaseDeDonnees);
    clientSerieN.rafraichirBaseDeDonnees();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des bases de données.
   */
  public ListeBDD getListeBaseDeDonnees() {
    return listeBaseDeDonnees;
  }
  
  /**
   * Retourne la base de données actuelles.
   */
  public BDD getActuelleDeBaseDonnees() {
    return actuelleBaseDeDonnees;
  }
  
  /**
   * Initialise une nouvelle base de données.
   */
  public void setNouvelleBaseDeDonnees(BDD pBaseDeDonnees) {
    nouvelleBaseDeDonnees = pBaseDeDonnees;
  }
  
  /**
   * Retourne la nouvelle base de données.
   */
  public BDD getNouvelleBaseDeDonnees() {
    return nouvelleBaseDeDonnees;
  }
  
  /**
   * Retourne un message.
   */
  public Message getMessage() {
    return message;
  }
  
}
