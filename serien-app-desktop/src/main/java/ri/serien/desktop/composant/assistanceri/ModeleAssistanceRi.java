/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.assistanceri;

import java.io.File;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleAssistanceRi extends AbstractModeleDialogue {
  // Variables
  private StringBuilder log = new StringBuilder();
  private Bibliotheque bibliotheque = null;
  private String dossiertempo = null;
  private String dossierrt = null;
  private FileNG fichierSAVF = null;
  private boolean restaurationPossible = false;
  
  /**
   * Constructeur.
   */
  public ModeleAssistanceRi(SessionBase pSession) {
    super(pSession);
  }
  
  // -- Méthodes héritées
  
  @Override
  public void initialiserDonnees() {
    effacerVariables();
    bibliotheque = new Bibliotheque(IdBibliotheque.getInstance("SERIEMFTP"), EnumTypeBibliotheque.NON_DEFINI);
    dossiertempo = ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierTemp() + '/';
    dossierrt = ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierTravail() + '/';
    
  }
  
  @Override
  public void chargerDonnees() {
    // TODO Auto-generated method stub
  }
  
  // -- Méthodes publiques
  
  public void fermerEcran() {
    super.quitterAvecValidation();
    
    // Fermeture de la session
    getSession().fermerEcran();
  }
  
  /**
   * Modifie la bibliothèque de restauration.
   */
  public void modifierBibliotheque(String pNomBibliotheque) {
    pNomBibliotheque = Constantes.normerTexte(pNomBibliotheque);
    if (pNomBibliotheque.isEmpty()) {
      throw new MessageErreurException("Le nom de la bibilothèque est incorrecte.");
    }
    if (bibliotheque.getNom().equals(pNomBibliotheque)) {
      return;
    }
    bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(pNomBibliotheque), EnumTypeBibliotheque.NON_DEFINI);
    rafraichir();
  }
  
  /**
   * Copie le fichier dans l'IFS, le dossier destination sera choisi en fonction de leur type.
   */
  public void copierFichier(File[] pListeFichier) {
    effacerVariables();
    FileNG fichier = controlerFichierATraiter(pListeFichier);
    if (fichier == null) {
      return;
    }
    
    // S'il s'agit d'un fichier SAVF dans le dossier temporaire
    if (isFichierSAVF(fichier)) {
      ajouterTexteLog("Transfert du fichier " + fichier + " vers " + dossiertempo);
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierAUploader(fichier, dossiertempo);
      ajouterTexteLog("Transfert du fichier dans l'IFS terminé.");
      fichierSAVF = fichier;
      restaurationPossible = true;
    }
    // S'il s'agit d'un fichier JAR, il sera copier dans le dossier des runtimes
    else {
      ajouterTexteLog("Transfert du fichier " + fichier + " vers " + dossierrt);
      ManagerSessionClient.getInstance().getEnvUser().getTransfertSession().fichierAUploader(fichier, dossierrt);
      ajouterTexteLog("Transfert du fichier dans l'IFS terminé.");
      ajouterTexteLog("Vous devez arrêter complètement Série N et le relancer afin de récupérer le correctif.");
    }
    
    rafraichir();
  }
  
  /**
   * Retourne le nom de la bibliothèque de restauration.
   */
  public String getNomBibliotheque() {
    if (bibliotheque == null) {
      return null;
    }
    return bibliotheque.getNom();
  }
  
  /**
   * Retourne si l'utilisateur à le droit de modifier la bibliothèque de restauration.
   */
  public boolean isUtilisateurATousLesDroits() {
    EnvUser envUser = ManagerSessionClient.getInstance().getEnvUser();
    if (envUser == null) {
      return false;
    }
    return envUser.getUSSPOR() == EnvUser.TOUS_LES_DROITS;
  }
  
  /**
   * Copie le fichier SAVF de l'ISF vers le 5250 puis restaure la bibliothèque.
   */
  public void restaurer() {
    if (fichierSAVF == null || fichierSAVF.getName().trim().isEmpty()) {
      throw new MessageErreurException("Le fichier SAVF est incorrect.");
    }
    
    // On soumet la restauration afin de ne pas géler l'interface
    ajouterTexteLog("Démarrage de la restauration");
    // Copie du fichier SAVF de l'IFS vers le 5250
    String cheminCompletsavf = dossiertempo + fichierSAVF.getName();
    ManagerServiceTechnique.copierFichierSavf(getSession().getIdSession(), cheminCompletsavf, bibliotheque);
    ajouterTexteLog("\tLe fichier " + fichierSAVF.getName() + " a été copié dans la bibliothèque " + bibliotheque.getNom());
    rafraichir();
    
    // On restaure le SAVF
    ManagerServiceTechnique.restaurerFichierSavf(getSession().getIdSession(), bibliotheque, fichierSAVF.getNamewoExtension(false));
    ajouterTexteLog("\tLe fichier " + fichierSAVF.getName() + " a bien été restauré.");
    ajouterTexteLog("Fin de la restauration");
    
    restaurationPossible = false;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  private void effacerVariables() {
    fichierSAVF = null;
    if (log == null) {
      log = new StringBuilder();
    }
    log.setLength(0);
  }
  
  /**
   * Contrôle les fichiers à traiter.
   */
  private FileNG controlerFichierATraiter(File[] pListeFichier) {
    // Contrôle générique
    if (pListeFichier == null || pListeFichier.length == 0) {
      throw new MessageErreurException("La liste ne contient aucun fichier à copier.");
    }
    if (pListeFichier.length > 1) {
      throw new MessageErreurException("Vous ne pouvez copier qu'un seul fichier à la fois.");
    }
    FileNG fichier = new FileNG(pListeFichier[0]);
    if (!fichier.isFile() || !(isFichierSAVF(fichier) || isFichierJAR(fichier))) {
      throw new MessageErreurException("Le fichier que vous tentez de copier n'est pas un fichier SAVF ou JAR.");
    }
    if (fichier.length() == 0) {
      throw new MessageErreurException("Le fichier que vous tentez de copier à une taille de 0 octet.");
    }
    
    // Contrôle spécifique pour un SAVF
    if (isFichierSAVF(fichier)) {
      // Contrôle s'il s'agit d'un patch pour Série N
      if (!fichier.getName().toUpperCase().startsWith("SM") && !fichier.getName().toUpperCase().startsWith("DM")) {
        // Si l'utilisateur à des droits restreints alors la copie est interdite
        if (!isUtilisateurATousLesDroits()) {
          throw new MessageErreurException(
              "Le fichier que vous tentez de restaurer n'a pas un nom correct, il n'a pas été généré par la Gestion Bib.Transferts. de Série N");
        }
        // Si l'utilisateur à les droits, une demande de confirmation est quand même formulée
        else {
          if (!DialogueConfirmation.afficher(getSession(), "Confirmation de copie d'un fichier SAVF",
              "Le fichier SAVF que vous souhaitez restaurer n'a pas été généré par la Gestion Bib.Transferts. de Série N.\n"
                  + "Etes-vous sur de vouloir continuer ?")) {
            return null;
          }
        }
      }
    }
    
    return fichier;
  }
  
  /**
   * Ajoute un texte au log.
   */
  private void ajouterTexteLog(String pTexte) {
    log.append(pTexte).append('\n');
  }
  
  /**
   * Détermine si c'est un fichier SAVF ou pas.
   */
  private boolean isFichierSAVF(File pFichier) {
    if (pFichier == null) {
      return false;
    }
    return pFichier.getName().toLowerCase().lastIndexOf(".savf") != -1;
  }
  
  /**
   * Détermine si c'est un fichier JAR ou pas.
   */
  private boolean isFichierJAR(File pFichier) {
    if (pFichier == null) {
      return false;
    }
    return pFichier.getName().toLowerCase().lastIndexOf(".jar") != -1;
  }
  
  // -- Accesseurs
  
  public boolean isRestaurationPossible() {
    return restaurationPossible;
  }
  
  public String getTexteLog() {
    if (log == null) {
      return "";
    }
    return log.toString();
  }
  
}
