/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.assistanceri;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXHyperlink;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.FileDrop;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueAssistanceRI extends AbstractVueDialogue<ModeleAssistanceRi> {
  // Constantes
  private static final String BOUTON_RESTAURER = "Restaurer";
  
  /**
   * Constructeur.
   */
  public VueAssistanceRI(ModeleAssistanceRi pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    tfBibliotheque.setLongueur(10);
    
    dragndopListener(lbZoneReception);
    try {
      URL url = new URL("https://resolution-informatique.atlassian.net/servicedesk/customer/portals");
      xhlAssistance.setURI(url.toURI());
    }
    catch (Exception e) {
    }
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_RESTAURER, 'r', false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirBibliotheque();
    rafraichirLog();
    rafraichirBoutonRestaurer();
  }
  
  private void rafraichirBibliotheque() {
    String nomBibliotheque = Constantes.normerTexte(getModele().getNomBibliotheque());
    tfBibliotheque.setText(nomBibliotheque);
    tfBibliotheque.setEditable(getModele().isUtilisateurATousLesDroits());
  }
  
  private void rafraichirLog() {
    String log = getModele().getTexteLog();
    if (log == null) {
      tpLog.setText("");
    }
    else {
      tpLog.setText(log);
    }
  }
  
  private void rafraichirBoutonRestaurer() {
    snBarreBouton.activerBouton(BOUTON_RESTAURER, getModele().isRestaurationPossible());
  }
  
  /**
   * Traite l'action du drag and drop.
   */
  private void dragndopListener(final JLabel pComposantCible) {
    new FileDrop(null, pComposantCible, new FileDrop.Listener() {
      @Override
      public void filesDropped(File[] pListeFichier) {
        try {
          getModele().copierFichier(pListeFichier);
        }
        catch (Exception exception) {
          DialogueErreur.afficher(exception);
        }
      }
    });
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_RESTAURER)) {
        getModele().restaurer();
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().fermerEcran();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfBibliothequeFocusLost(FocusEvent e) {
    try {
      getModele().modifierBibliotheque(tfBibliotheque.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlContenu = new SNPanelContenu();
    pnlExplication = new SNPanelTitre();
    lbExplication = new SNLabelChamp();
    pnlEtape1 = new SNPanelTitre();
    lbExplicationEtape1 = new SNLabelChamp();
    lbZoneReception = new JLabel();
    pnlEtape2 = new SNPanelTitre();
    pnlBibliotheque = new SNPanel();
    lbBibliotheque = new SNLabelChamp();
    tfBibliotheque = new SNTexte();
    scrollPane1 = new JScrollPane();
    tpLog = new JTextPane();
    xhlAssistance = new JXHyperlink();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Assistance S\u00e9rie N");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setResizable(false);
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setMinimumSize(new Dimension(600, 720));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setPreferredSize(new Dimension(600, 720));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlExplication ========
        {
          pnlExplication.setTitre("Explications");
          pnlExplication.setName("pnlExplication");
          pnlExplication.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlExplication.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlExplication.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlExplication.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlExplication.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbExplication ----
          lbExplication.setText(
              "<html>Cet outil permet de mettre en place des correctifs pour S\u00e9rie N tr\u00e8s simplement.<br>S'il s'agit d'un fichier JAR, il sera copi\u00e9 directement dans le dossier ad\u00e9quat de l'IFS, vous n'aurez rien d'autre \u00e0 faire qu'\u00e0 arr\u00eater et red\u00e9marrer S\u00e9rie N.<br>S'il s'agit d'un fichier SAVF, il sera copi\u00e9 dans la biblioth\u00e8que indiqu\u00e9e \u00e0 l'\u00e9tape 2, puis restaur\u00e9 dans une biblioth\u00e8que ayant comme nom celui du SAVF. Il faudra copier manuellement les objets dans leurs biblioth\u00e8ques respectives avec une session 5250.</html>");
          lbExplication.setVerticalTextPosition(SwingConstants.TOP);
          lbExplication.setPreferredSize(new Dimension(150, 140));
          lbExplication.setMinimumSize(new Dimension(150, 140));
          lbExplication.setMaximumSize(new Dimension(150, 140));
          lbExplication.setName("lbExplication");
          pnlExplication.add(lbExplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlExplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlEtape1 ========
        {
          pnlEtape1.setTitre("Etape 1");
          pnlEtape1.setName("pnlEtape1");
          pnlEtape1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtape1.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtape1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtape1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtape1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbExplicationEtape1 ----
          lbExplicationEtape1.setText(
              "<html>Veuillez d\u00e9poser le fichier SAVF ou JAR dans la zone verte via un drag'n drop avec votre souris.</html>");
          lbExplicationEtape1.setHorizontalAlignment(SwingConstants.LEFT);
          lbExplicationEtape1.setForeground(Color.black);
          lbExplicationEtape1.setPreferredSize(new Dimension(150, 40));
          lbExplicationEtape1.setName("lbExplicationEtape1");
          pnlEtape1.add(lbExplicationEtape1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbZoneReception ----
          lbZoneReception.setBorder(LineBorder.createBlackLineBorder());
          lbZoneReception.setIcon(new ImageIcon(getClass().getResource("/images/green.png")));
          lbZoneReception.setHorizontalAlignment(SwingConstants.CENTER);
          lbZoneReception.setBackground(new Color(0, 204, 0));
          lbZoneReception.setOpaque(true);
          lbZoneReception.setName("lbZoneReception");
          pnlEtape1.add(lbZoneReception, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEtape1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlEtape2 ========
        {
          pnlEtape2.setTitre("Etape 2");
          pnlEtape2.setName("pnlEtape2");
          pnlEtape2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtape2.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlEtape2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtape2.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtape2.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ======== pnlBibliotheque ========
          {
            pnlBibliotheque.setName("pnlBibliotheque");
            pnlBibliotheque.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBibliotheque.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlBibliotheque.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlBibliotheque.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBibliotheque.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbBibliotheque ----
            lbBibliotheque.setText("Biblioth\u00e8que o\u00f9 sera envoy\u00e9 le SAVF");
            lbBibliotheque.setPreferredSize(new Dimension(250, 30));
            lbBibliotheque.setName("lbBibliotheque");
            pnlBibliotheque.add(lbBibliotheque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfBibliotheque ----
            tfBibliotheque.setName("tfBibliotheque");
            tfBibliotheque.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfBibliothequeFocusLost(e);
              }
            });
            pnlBibliotheque.add(tfBibliotheque, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEtape2.add(pnlBibliotheque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- tpLog ----
            tpLog.setEditable(false);
            tpLog.setName("tpLog");
            scrollPane1.setViewportView(tpLog);
          }
          pnlEtape2.add(scrollPane1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlEtape2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- xhlAssistance ----
        xhlAssistance.setText("Site d'assistance RI");
        xhlAssistance.setFont(xhlAssistance.getFont().deriveFont(xhlAssistance.getFont().getSize() + 2f));
        xhlAssistance.setName("xhlAssistance");
        pnlContenu.add(xhlAssistance, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlExplication;
  private SNLabelChamp lbExplication;
  private SNPanelTitre pnlEtape1;
  private SNLabelChamp lbExplicationEtape1;
  private JLabel lbZoneReception;
  private SNPanelTitre pnlEtape2;
  private SNPanel pnlBibliotheque;
  private SNLabelChamp lbBibliotheque;
  private SNTexte tfBibliotheque;
  private JScrollPane scrollPane1;
  private JTextPane tpLog;
  private JXHyperlink xhlAssistance;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
