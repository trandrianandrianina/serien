/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.libswing.composantrpg.autonome.LaunchViewer;
import ri.serien.libswing.composantrpg.autonome.RiFileChooser;

/**
 * On liste les fichiers Excel (Report One) d'un dossier définit
 */
public class ListerMyReport extends JDialog {
  // Constantes
  private static final String EXTENSION_XLS = ".xls";
  private static final String EXTENSION_XLSM = ".xlsm";
  private static final String EXTENSION_XLSX = ".xlsx";
  
  // Variables
  private ClientSerieN fenetre = null;
  private File[] listeRO = null;
  private File[] listeROprive = null;
  
  /**
   * Constructeur
   * @param owner
   * @param iUser
   */
  public ListerMyReport(Frame owner) {
    super(owner);
    fenetre = (ClientSerieN) owner;
    initComponents();
    
    // Récupère le dossier pour Report One public dans les préférences et charge la liste
    String dossier = ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOne();
    if (dossier != null) {
      tf_dossierRO.setText(dossier);
    }
    chargeListe(dossier);
    
    // Récupère le dossier pour Report One privé dans les préférences et charge la liste
    dossier = ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOneprive();
    if (dossier != null) {
      tf_dossierROprive.setText(dossier);
    }
    chargeListeprive(dossier);
    
    setVisible(true);
  }
  
  /**
   * Ouvre une boite de dialogue afin de sélectionner un dossier
   */
  private File getSelectionDossier(String origine) {
    RiFileChooser selectionDossier = new RiFileChooser(fenetre, origine);
    return selectionDossier.choisirDossier(null);
  }
  
  /**
   * Initialise la liste des fichiers Report One
   * @param dossierRO
   */
  private void initListeDossier(File dossier) {
    ArrayList<File> liste = new ArrayList<File>();
    liste = getListeRO(dossier, liste);
    if (liste.isEmpty()) {
      return;
    }
    listeRO = new File[liste.size()];
    liste.toArray(listeRO);
    liste.clear();
    // listeRO = getListeRO(dossier);
    // if (listeRO == null) return;
    
    // int longdos = (int)dossier.getAbsolutePath().length();
    // On va remplir la table avec les fichiers trouvés
    String[][] data = new String[listeRO.length][1];
    for (int i = 0; i < listeRO.length; i++) {
      data[i][0] = listeRO[i].getName();
    }
    String[] titre = { "Fichiers publics" };
    tbl_ReportOne.setModel(new DefaultTableModel(data, titre) {
      @Override
      public Class<?> getColumnClass(int columnIndex) {
        return String.class;
      }
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    });
  }
  
  /**
   * Initialise la liste des fichiers Report One
   * @param dossierRO
   */
  private void initListeDossierprive(File dossier) {
    ArrayList<File> liste = new ArrayList<File>();
    liste = getListeRO(dossier, liste);
    if (liste.isEmpty()) {
      return;
    }
    listeROprive = new File[liste.size()];
    liste.toArray(listeROprive);
    liste.clear();
    // listeROprive = getListeRO(dossier);
    // if (listeROprive == null) return;
    
    // int longdos = (int)dossier.getAbsolutePath().length();
    // On va remplir la table avec les fichiers trouvés
    String[][] data = new String[listeROprive.length][1];
    for (int i = 0; i < listeROprive.length; i++) {
      data[i][0] = listeROprive[i].getName();
    }
    String[] titre = { "Fichiers privées" };
    tbl_ReportOneprive.setModel(new DefaultTableModel(data, titre) {
      @Override
      public Class<?> getColumnClass(int columnIndex) {
        return String.class;
      }
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    });
    
  }
  
  /**
   * Charge la liste à partir d'un dossier
   * @param dossier
   */
  private void chargeListe(String dossier) {
    if ((dossier != null) && (!dossier.trim().equals(""))) {
      File fdossier = new File(dossier.trim());
      if (fdossier.exists()) {
        initListeDossier(fdossier);
      }
    }
  }
  
  /**
   * Charge la liste privé à partir d'un dossier
   * @param dossier
   */
  private void chargeListeprive(String dossier) {
    if ((dossier != null) && (!dossier.trim().equals(""))) {
      File fdossier = new File(dossier.trim());
      if (fdossier.exists()) {
        initListeDossierprive(fdossier);
      }
    }
  }
  
  /**
   * Retourne la liste des fichiers
   * @param dossier
   * @return
   * 
   *         private File[] getListeRO(File dossier)
   *         {
   *         if ((dossier == null) || (!dossier.exists())) return null;
   * 
   *         return dossier.listFiles(new FilenameFilter()
   *         {
   *         public boolean accept(File dir, String name)
   *         {
   *         return name.toLowerCase().endsWith(EXTENSION_XLS) || name.toLowerCase().endsWith(EXTENSION_XLSM) ||
   *         name.toLowerCase().endsWith(EXTENSION_XLSX);
   *         }
   *         });
   *         }
   */
  
  /**
   * Liste tous les fichiers d'un répertoire de manière récursive
   * @param repertoire
   * @param tousLesFichiers
   * @return
   */
  private ArrayList<File> getListeRO(File dossier, ArrayList<File> tousLesFichiers) {
    if (tousLesFichiers == null) {
      tousLesFichiers = new ArrayList<File>();
    }
    if (dossier.exists()) {
      if (dossier.isDirectory()) {
        final File[] list = dossier.listFiles(new FilenameFilter() {
          @Override
          public boolean accept(File dir, String name) {
            return dir.isDirectory() || name.toLowerCase().endsWith(EXTENSION_XLS) || name.toLowerCase().endsWith(EXTENSION_XLSM)
                || name.toLowerCase().endsWith(EXTENSION_XLSX);
          }
        });
        if (list != null) {
          for (int i = 0; i < list.length; i++) {
            if (list[i].isDirectory()) {
              getListeRO(list[i], tousLesFichiers);
            }
            else {
              tousLesFichiers.add(list[i]);
            }
          }
        }
      }
    }
    return tousLesFichiers;
  }
  
  /**
   * Sauve les préférences de l'utilisateur
   */
  private void sauvePreference() {
    if (!tf_dossierRO.getText().trim().equals(ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOne())) {
      ClientSerieN.getPreferencesUserManager().getPreferences().setDossierReportOne(tf_dossierRO.getText().trim());
    }
    if (!tf_dossierROprive.getText().trim()
        .equals(ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOneprive())) {
      ClientSerieN.getPreferencesUserManager().getPreferences().setDossierReportOneprive(tf_dossierROprive.getText().trim());
    }
  }
  
  /**
   * Double clic qui met à jour les tops des listes avec la valeur souhaitée
   * @param table
   * @param wtop
   * @param valeur
   */
  public void doubleClicSelection(MouseEvent e) {
    if ((tbl_ReportOne.getSelectedRow() >= 0) && (e.getClickCount() == 2)) {
      int[] selected = tbl_ReportOne.getSelectedRows();
      lanceReportOne(selected[0], listeRO);
    }
  }
  
  /**
   * Double clic qui met à jour les tops des listes avec la valeur souhaitée
   * @param table
   * @param wtop
   * @param valeur
   */
  public void doubleClicSelectionprive(MouseEvent e) {
    if ((tbl_ReportOneprive.getSelectedRow() >= 0) && (e.getClickCount() == 2)) {
      int[] selected = tbl_ReportOneprive.getSelectedRows();
      lanceReportOne(selected[0], listeROprive);
    }
  }
  
  /**
   * Lance l'application associée au fichier
   * @param indice
   */
  private void lanceReportOne(int indice, File[] liste) {
    LaunchViewer lv = new LaunchViewer(liste[indice]);
    if (!lv.lancerViewer()) {
      JOptionPane.showMessageDialog(this, lv.getMsgErreur(), "Affichage du document", JOptionPane.WARNING_MESSAGE);
    }
  }
  
  // --------------------> Evènementiel <-------------------------------------
  
  private void bt_RafraichirActionPerformed(ActionEvent e) {
    chargeListe(tf_dossierRO.getText());
    chargeListeprive(tf_dossierROprive.getText());
  }
  
  private void bt_ParcourirActionPerformed(ActionEvent e) {
    File dossierRO = getSelectionDossier(ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOne());
    tf_dossierRO.setText(dossierRO.getAbsolutePath());
    chargeListe(tf_dossierRO.getText());
  }
  
  private void bt_ParcourirpriveActionPerformed(ActionEvent e) {
    File dossierRO = getSelectionDossier(ClientSerieN.getPreferencesUserManager().getPreferences().getDossierReportOneprive());
    tf_dossierROprive.setText(dossierRO.getAbsolutePath());
    chargeListeprive(tf_dossierROprive.getText());
  }
  
  private void tbl_ReportOneMouseClicked(MouseEvent e) {
    doubleClicSelection(e);
  }
  
  private void tbl_ReportOnepriveMouseClicked(MouseEvent e) {
    doubleClicSelectionprive(e);
  }
  
  private void bt_FermerActionPerformed(ActionEvent e) {
    setVisible(false);
    dispose();
  }
  
  private void thisWindowClosed(WindowEvent e) {
    sauvePreference();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    tabbedPane1 = new JTabbedPane();
    p_Public = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    tf_dossierRO = new JTextField();
    bt_Parcourir = new JButton();
    panel2 = new JPanel();
    scrollPane1 = new JScrollPane();
    tbl_ReportOne = new JTable();
    p_Prive = new JPanel();
    panel6 = new JPanel();
    label2 = new JLabel();
    tf_dossierROprive = new JTextField();
    bt_Parcourirprive = new JButton();
    panel7 = new JPanel();
    scrollPane2 = new JScrollPane();
    tbl_ReportOneprive = new JTable();
    p_Pied = new JPanel();
    bt_Rafraichir = new JButton();
    bt_Fermer = new JButton();
    
    // ======== this ========
    setTitle("Liste des fichiers MyReport");
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setResizable(false);
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosed(WindowEvent e) {
        thisWindowClosed(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== tabbedPane1 ========
    {
      tabbedPane1.setName("tabbedPane1");
      
      // ======== p_Public ========
      {
        p_Public.setName("p_Public");
        p_Public.setLayout(new BorderLayout(5, 5));
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Dossier \u00e0 scanner"));
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- label1 ----
          label1.setText("Dossier");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(new Rectangle(new Point(20, 40), label1.getPreferredSize()));
          
          // ---- tf_dossierRO ----
          tf_dossierRO.setName("tf_dossierRO");
          panel1.add(tf_dossierRO);
          tf_dossierRO.setBounds(75, 35, 310, tf_dossierRO.getPreferredSize().height);
          
          // ---- bt_Parcourir ----
          bt_Parcourir.setText("Parcourir");
          bt_Parcourir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_Parcourir.setName("bt_Parcourir");
          bt_Parcourir.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_ParcourirActionPerformed(e);
            }
          });
          panel1.add(bt_Parcourir);
          bt_Parcourir.setBounds(new Rectangle(new Point(395, 35), bt_Parcourir.getPreferredSize()));
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_Public.add(panel1, BorderLayout.SOUTH);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Liste des fichiers trouv\u00e9s"));
          panel2.setName("panel2");
          panel2.setLayout(new BorderLayout());
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- tbl_ReportOne ----
            tbl_ReportOne.setModel(new DefaultTableModel(new Object[][] { { null }, }, new String[] { "Vide" }));
            tbl_ReportOne.setName("tbl_ReportOne");
            tbl_ReportOne.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tbl_ReportOneMouseClicked(e);
              }
            });
            scrollPane1.setViewportView(tbl_ReportOne);
          }
          panel2.add(scrollPane1, BorderLayout.CENTER);
        }
        p_Public.add(panel2, BorderLayout.CENTER);
      }
      tabbedPane1.addTab("Public", p_Public);
      
      // ======== p_Prive ========
      {
        p_Prive.setName("p_Prive");
        p_Prive.setLayout(new BorderLayout(5, 5));
        
        // ======== panel6 ========
        {
          panel6.setBorder(new TitledBorder("Dossier \u00e0 scanner"));
          panel6.setName("panel6");
          panel6.setLayout(null);
          
          // ---- label2 ----
          label2.setText("Dossier");
          label2.setName("label2");
          panel6.add(label2);
          label2.setBounds(new Rectangle(new Point(20, 40), label2.getPreferredSize()));
          
          // ---- tf_dossierROprive ----
          tf_dossierROprive.setName("tf_dossierROprive");
          panel6.add(tf_dossierROprive);
          tf_dossierROprive.setBounds(75, 35, 310, tf_dossierROprive.getPreferredSize().height);
          
          // ---- bt_Parcourirprive ----
          bt_Parcourirprive.setText("Parcourir");
          bt_Parcourirprive.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_Parcourirprive.setName("bt_Parcourirprive");
          bt_Parcourirprive.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_ParcourirpriveActionPerformed(e);
            }
          });
          panel6.add(bt_Parcourirprive);
          bt_Parcourirprive.setBounds(new Rectangle(new Point(395, 35), bt_Parcourirprive.getPreferredSize()));
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel6.getComponentCount(); i++) {
              Rectangle bounds = panel6.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel6.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel6.setMinimumSize(preferredSize);
            panel6.setPreferredSize(preferredSize);
          }
        }
        p_Prive.add(panel6, BorderLayout.SOUTH);
        
        // ======== panel7 ========
        {
          panel7.setBorder(new TitledBorder("Liste des fichiers trouv\u00e9s"));
          panel7.setName("panel7");
          panel7.setLayout(new BorderLayout());
          
          // ======== scrollPane2 ========
          {
            scrollPane2.setName("scrollPane2");
            
            // ---- tbl_ReportOneprive ----
            tbl_ReportOneprive.setModel(new DefaultTableModel(new Object[][] { { null }, }, new String[] { "Vide" }));
            tbl_ReportOneprive.setName("tbl_ReportOneprive");
            tbl_ReportOneprive.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tbl_ReportOnepriveMouseClicked(e);
              }
            });
            scrollPane2.setViewportView(tbl_ReportOneprive);
          }
          panel7.add(scrollPane2, BorderLayout.CENTER);
        }
        p_Prive.add(panel7, BorderLayout.CENTER);
      }
      tabbedPane1.addTab("Priv\u00e9", p_Prive);
    }
    contentPane.add(tabbedPane1, BorderLayout.CENTER);
    
    // ======== p_Pied ========
    {
      p_Pied.setName("p_Pied");
      p_Pied.setLayout(new FlowLayout(FlowLayout.RIGHT));
      
      // ---- bt_Rafraichir ----
      bt_Rafraichir.setText("Rafraichir");
      bt_Rafraichir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Rafraichir.setPreferredSize(new Dimension(85, 28));
      bt_Rafraichir.setName("bt_Rafraichir");
      bt_Rafraichir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_RafraichirActionPerformed(e);
        }
      });
      p_Pied.add(bt_Rafraichir);
      
      // ---- bt_Fermer ----
      bt_Fermer.setText("Fermer");
      bt_Fermer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      bt_Fermer.setPreferredSize(new Dimension(85, 28));
      bt_Fermer.setName("bt_Fermer");
      bt_Fermer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          bt_FermerActionPerformed(e);
        }
      });
      p_Pied.add(bt_Fermer);
    }
    contentPane.add(p_Pied, BorderLayout.SOUTH);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tabbedPane1;
  private JPanel p_Public;
  private JPanel panel1;
  private JLabel label1;
  private JTextField tf_dossierRO;
  private JButton bt_Parcourir;
  private JPanel panel2;
  private JScrollPane scrollPane1;
  private JTable tbl_ReportOne;
  private JPanel p_Prive;
  private JPanel panel6;
  private JLabel label2;
  private JTextField tf_dossierROprive;
  private JButton bt_Parcourirprive;
  private JPanel panel7;
  private JScrollPane scrollPane2;
  private JTable tbl_ReportOneprive;
  private JPanel p_Pied;
  private JButton bt_Rafraichir;
  private JButton bt_Fermer;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
