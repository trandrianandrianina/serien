/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.changerbasedonnees;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.WindowConstants;

import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Cette boite de dialogue permet de changer de base données.
 */
public class VueChangerBaseDeDonnees extends AbstractVueDialogue<ModeleChangerBaseDeDonnees> {
  // Constantes
  private static final String BOUTON_MIGRER_BDD = "Migrer la base de données";
  
  /**
   * Constructeur.
   */
  public VueChangerBaseDeDonnees(ModeleChangerBaseDeDonnees pModele) {
    super(pModele);
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(500, 280);
    setResizable(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_MIGRER_BDD, 'm', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirBoutonValider();
    rafraichirBaseDonneesActuelle();
    rafraichirListeBaseDonnees();
    rafraichirBoutonMettreAJourBd();
    rafraichirMessage();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir la base de données actuelle.
   */
  private void rafraichirBaseDonneesActuelle() {
    Bibliotheque baseDonnees = getModele().getActuelleDeBaseDonnees();
    if (baseDonnees == null) {
      tfBdActuelle.setText("");
    }
    else {
      tfBdActuelle.setText(baseDonnees.toString());
    }
  }
  
  /**
   * Rafraichir la liste des bases de données disponibles.
   */
  private void rafraichirListeBaseDonnees() {
    List<BDD> listeBaseDeDonnees = getModele().getListeBaseDeDonnees();
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      return;
    }
    DefaultComboBoxModel model = (DefaultComboBoxModel) cbNouvelleBd.getModel();
    model.removeAllElements();
    for (BDD baseDonnees : listeBaseDeDonnees) {
      model.addElement(baseDonnees);
    }
    BDD nouvelleBd = getModele().getNouvelleBaseDeDonnees();
    if (nouvelleBd != null) {
      cbNouvelleBd.setSelectedItem(nouvelleBd);
    }
  }
  
  /**
   * Rafraichir le message.
   */
  private void rafraichirMessage() {
    lbMessage.setMessage(getModele().getMessage());
  }
  
  /**
   * Rafraichir le bouton Valider
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isVersionIdentique();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Afficher le bouton mettre à jour base de données.
   */
  private void rafraichirBoutonMettreAJourBd() {
    boolean actif = getModele().isMettreAJourBaseDeDonnees();
    snBarreBouton.activerBouton(BOUTON_MIGRER_BDD, actif);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_MIGRER_BDD)) {
        getModele().mettreAJourBaseDeDonnees();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbNouvelleBdItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbNouvelleBd.getSelectedItem() instanceof Bibliotheque) {
        getModele().modifierBaseDeDonnees((BDD) cbNouvelleBd.getSelectedItem());
      }
      else {
        getModele().modifierBaseDeDonnees(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanel();
    pnlContenu = new SNPanelContenu();
    lbTexteDbActuelle = new SNLabelChamp();
    tfBdActuelle = new SNTexte();
    lbTexteBdNouvelle = new SNLabelChamp();
    lbMessage = new SNMessage();
    cbNouvelleBd = new SNComboBox();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Changer de base de donn\u00e9es");
    setModal(true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setResizable(false);
    setBackground(new Color(238, 238, 210));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ---- lbTexteDbActuelle ----
        lbTexteDbActuelle.setText("Base de donn\u00e9es actuelle");
        lbTexteDbActuelle.setMaximumSize(new Dimension(200, 30));
        lbTexteDbActuelle.setMinimumSize(new Dimension(200, 30));
        lbTexteDbActuelle.setPreferredSize(new Dimension(200, 30));
        lbTexteDbActuelle.setName("lbTexteDbActuelle");
        pnlContenu.add(lbTexteDbActuelle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfBdActuelle ----
        tfBdActuelle.setFont(tfBdActuelle.getFont().deriveFont(tfBdActuelle.getFont().getStyle() | Font.BOLD));
        tfBdActuelle.setEditable(false);
        tfBdActuelle.setEnabled(false);
        tfBdActuelle.setName("tfBdActuelle");
        pnlContenu.add(tfBdActuelle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTexteBdNouvelle ----
        lbTexteBdNouvelle.setText("Nouvelle base de donn\u00e9es");
        lbTexteBdNouvelle.setForeground(Color.black);
        lbTexteBdNouvelle.setName("lbTexteBdNouvelle");
        pnlContenu.add(lbTexteBdNouvelle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbMessage ----
        lbMessage.setForeground(Color.red);
        lbMessage.setName("lbMessage");
        pnlContenu.add(lbMessage, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- cbNouvelleBd ----
        cbNouvelleBd.setPreferredSize(new Dimension(180, 30));
        cbNouvelleBd.setName("cbNouvelleBd");
        cbNouvelleBd.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbNouvelleBdItemStateChanged(e);
          }
        });
        pnlContenu.add(cbNouvelleBd, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbTexteDbActuelle;
  private SNTexte tfBdActuelle;
  private SNLabelChamp lbTexteBdNouvelle;
  private SNMessage lbMessage;
  private SNComboBox cbNouvelleBd;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
