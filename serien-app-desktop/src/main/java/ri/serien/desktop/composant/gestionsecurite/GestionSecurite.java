/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.gestionsecurite;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ButtonGroup;
import javax.swing.JDialog;
import javax.swing.WindowConstants;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.metier.exploitation.menus.manager.GestionModelesMenus;
import ri.serien.desktop.sessions.SessionRPG;
import ri.serien.libcommun.outils.collection.ArrayListManager;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;

/**
 * Cette classe permet de lancer les programmes qui gèrent la sécurité utilisateur des différents modules.
 */
public class GestionSecurite extends JDialog {
  // Constantes
  private static final String BOUTON_PERSONNALISER_MENUS = "Personnaliser menus";
  
  // Variables
  private ClientSerieN fenetre = null;
  
  /**
   * Constructeur.
   */
  public GestionSecurite(Frame owner) {
    super(owner);
    fenetre = (ClientSerieN) owner;
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_PERSONNALISER_MENUS, 'p', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    setVisible(true);
  }
  
  /**
   * Lance le programme de sécurité du module correspondant
   */
  private void lanceSecurite() {
    ArrayListManager listeSession = (ArrayListManager) ClientSerieN.getOnglet()[1];
    
    EnvProgramClient infoPrg = new EnvProgramClient();
    if (rbComptabilite.isSelected()) {
      infoPrg.setProgram("SECCGM");
      infoPrg.setLibMenu("Sécurité de la comptabilité");
      infoPrg.addBib("WEXPAS");
      infoPrg.addBib("WCGMAS");
    }
    else if (rbGescom.isSelected()) {
      infoPrg.setProgram("SECGVM");
      infoPrg.setLibMenu("Sécurité de la gescom");
      infoPrg.addBib("WEXPAS");
      infoPrg.addBib("WGVMAS");
    }
    else if (rbImmobilisations.isSelected()) {
      infoPrg.setProgram("SECGIM");
      infoPrg.setLibMenu("Sécurité des immobilisations");
      infoPrg.addBib("WEXPAS");
      infoPrg.addBib("WGIMAS");
    }
    else if (rbEffets.isSelected()) {
      infoPrg.setProgram("SECGEM");
      infoPrg.setLibMenu("Sécurité des effets");
      infoPrg.addBib("WEXPAS");
      infoPrg.addBib("WGEMAS");
    }
    else if (rbExploitation.isSelected()) {
      infoPrg.setProgram("SECEXP");
      infoPrg.setLibMenu("Sécurité de l'exploitation");
      infoPrg.addBib("WEXPAS");
    }
    else {
      return;
    }
    
    // Lancement du programme dans le panel
    listeSession.addObject(new SessionRPG(infoPrg, fenetre, null));
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_PERSONNALISER_MENUS)) {
        ArrayListManager ongletPrg = (ArrayListManager) ClientSerieN.getOnglet()[1];
        ongletPrg.addObject(new GestionModelesMenus(fenetre));
        
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lanceSecurite();
      }
      // else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
      // }
      // Fermeture de la boite de dialogue
      setVisible(false);
      dispose();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlCouleurFond = new SNPanel();
    pnlContenu = new SNPanelContenu();
    pnlChoixModule = new SNPanelTitre();
    rbComptabilite = new SNRadioButton();
    rbEffets = new SNRadioButton();
    rbGescom = new SNRadioButton();
    rbExploitation = new SNRadioButton();
    rbImmobilisations = new SNRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Gestion de la s\u00e9curit\u00e9");
    setModal(true);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setResizable(false);
    setMinimumSize(new Dimension(520, 290));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setOpaque(true);
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new BorderLayout());
        
        // ======== pnlChoixModule ========
        {
          pnlChoixModule.setTitre("Choix du module");
          pnlChoixModule.setName("pnlChoixModule");
          pnlChoixModule.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlChoixModule.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlChoixModule.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlChoixModule.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlChoixModule.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- rbComptabilite ----
          rbComptabilite.setText("Comptabilit\u00e9");
          rbComptabilite.setPreferredSize(new Dimension(200, 30));
          rbComptabilite.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbComptabilite.setName("rbComptabilite");
          pnlChoixModule.add(rbComptabilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbEffets ----
          rbEffets.setText("Effets");
          rbEffets.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbEffets.setName("rbEffets");
          pnlChoixModule.add(rbEffets, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- rbGescom ----
          rbGescom.setText("Gescom");
          rbGescom.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbGescom.setName("rbGescom");
          pnlChoixModule.add(rbGescom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbExploitation ----
          rbExploitation.setText("Exploitation");
          rbExploitation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbExploitation.setName("rbExploitation");
          pnlChoixModule.add(rbExploitation, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- rbImmobilisations ----
          rbImmobilisations.setText("Immobilisations");
          rbImmobilisations.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbImmobilisations.setName("rbImmobilisations");
          pnlChoixModule.add(rbImmobilisations, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlChoixModule, BorderLayout.CENTER);
      }
      pnlCouleurFond.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbComptabilite);
    buttonGroup1.add(rbEffets);
    buttonGroup1.add(rbGescom);
    buttonGroup1.add(rbExploitation);
    buttonGroup1.add(rbImmobilisations);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlCouleurFond;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlChoixModule;
  private SNRadioButton rbComptabilite;
  private SNRadioButton rbEffets;
  private SNRadioButton rbGescom;
  private SNRadioButton rbExploitation;
  private SNRadioButton rbImmobilisations;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
