/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Locale;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.WindowConstants;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Fenêtre générique pour les informations satellites
 */
public class SessionIndependante extends JFrame {
  // Variables
  private JFrame fenetreMere = null;
  private SessionBase session = null;
  
  /**
   * Constructeur
   */
  public SessionIndependante(JFrame mere) {
    super();
    initComponents();
    
    fenetreMere = mere;
  }
  
  /**
   * Redéfinit le bouton par défaut
   * @param bouton
   */
  public void setDefautBouton(JButton bouton) {
    if (bouton != null) {
      getRootPane().setDefaultButton((bouton));
    }
  }
  
  /**
   * Initialise le panel principal
   * @param principal
   */
  public void setSessionPanel(final SessionBase pSession, int w, int h, boolean opaque) {
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        if (!canIClose()) {
          return;
        }
        ClientSerieN.listeSession.removeObject(pSession);
      }
      
      @Override
      public void windowClosed(WindowEvent e) {
      }
    });
    // setDefautBouton(((iRiPanel)principal).getDefautBouton());
    getContentPane().add(pSession.getPanel(), BorderLayout.CENTER);
    this.session = pSession;
    // if (!opaque) principal.setBackground(Constantes.COULEUR_CONTENU);
    setSize(w, h);
    setVisible(true);
  }
  
  /**
   * Retourne le panel principal
   * @return
   */
  public SessionBase getSession() {
    return session;
  }
  
  /**
   * Ferme la fenêtre de la session
   */
  public void fermerFenetre() {
    setVisible(false);
    dispose();
  }
  
  // -- Méthodes privées -----------------------------------------------------
  
  private boolean canIClose() {
    Object[] options = { UIManager.getString("OptionPane.yesButtonText", Locale.getDefault()),
        UIManager.getString("OptionPane.noButtonText", Locale.getDefault()) };
    int rep = JOptionPane.showOptionDialog(this,
        "<HTML><B>Il n'est pas conseillé de fermer cette fenêtre</B></HTML>\n(répondez NON, puis fermer la fenêtre avec le bouton Retour, Valider ou Fermer si c'est possible).\n\nSi l'application ne répond pas alors confirmez la fermeture de ce programme.",
        "Demande de fermeture du programme", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE, null, options, options[1]);
    if (rep == JOptionPane.NO_OPTION) {
      return false;
    }
    return true;
  }
  
  // -- Evènementiels --------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    
    // ======== this ========
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setIconImage(new ImageIcon(getClass().getResource("/images/ri_logo.png")).getImage());
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    pack();
    setLocationRelativeTo(null);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
