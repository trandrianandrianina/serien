/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant.assistanceri;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

public class SpAssistanceRi extends SessionJava {
  
  /**
   * Constructeur.
   */
  public SpAssistanceRi(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    // Ces méthodes doivent obligatoirement être appelées ici sinon la méthode ouvrirSession n'est pas appelée car le vue.afficher()
    // est bloquant
    setBoiteDialogue(true);
    ouvrirSession();
    
    ModeleAssistanceRi modele = new ModeleAssistanceRi(this);
    VueAssistanceRI vue = new VueAssistanceRI(modele);
    vue.afficher();
  }
  
}
