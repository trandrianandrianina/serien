/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.composant;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.FlowLayout;
import java.awt.KeyEventPostProcessor;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.imageio.ImageIO;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JToggleButton;
import javax.swing.KeyStroke;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;

import org.jdesktop.swingx.JXPanel;
import org.jdesktop.swingx.painter.ImagePainter;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;
import ri.serien.desktop.sessions.SessionRPG;
import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.notification.EnumAspectNotification;
import ri.serien.libcommun.exploitation.notification.EnumPrioriteNotification;
import ri.serien.libcommun.exploitation.notification.EnumStatutNotification;
import ri.serien.libcommun.exploitation.notification.ParametreMenuNotification;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.dialoguestandard.information.ModeleDialogueInformation;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonIcone;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonNotification;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonNotificationALire;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonNotificationImportanteALire;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBarreOutil;
import ri.serien.libswing.moteur.graphique.SNBarreSession;
import ri.serien.libswing.moteur.graphique.SNBoutonSession;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Cette classe construit le panel qui sert de support aux écrans RPG ou Java. Le panel est constitué d'une barre de session
 * en bas du panel. Si aucune session n'est ouverte la barre de sessions est cachée et une image de fond est affiché.
 */
public class SNFondEcran extends JXPanel {
  // Constantes
  private static final int MARGE_BASSE_PANEL_PIED = 2;
  public static final Dimension TAILLE = new Dimension(1280, 800);
  
  // Variables
  private boolean afficherBarreSession = false;
  private ClientSerieN clientSerieN = null;
  private SNBoutonNotification btNotificationDefaut = null;
  private SNBoutonNotificationALire btNotificationALire = null;
  private SNBoutonNotificationImportanteALire btNotificationImportanteALire = null;
  private EnumAspectNotification aspectNotification = EnumAspectNotification.DEFAUT;
  private SNBoutonIcone btNotification = null;
  
  /**
   * Constructeur.
   */
  public SNFondEcran() {
    super();
    initComponents();
  }
  
  /**
   * Constructeur.
   */
  public SNFondEcran(ClientSerieN pClientSerieN) {
    this();
    clientSerieN = pClientSerieN;
    initialiserComposant();
  }
  
  // -- Méthode publiques
  
  /**
   * Ajoute un écran d'une session.
   */
  public void ajouterSession(final SessionBase pSessionBase) {
    // Contrôle du nombre de session
    if (ClientSerieN.listeSession.getArrayList().size() > ManagerSessionClient.NOMBRE_MAX_SESSION) {
      ClientSerieN.listeSession.getArrayList().remove(pSessionBase);
      ModeleDialogueInformation modele = new ModeleDialogueInformation();
      modele.setMessage("Vous ne pouvez pas ouvrir plus de " + ManagerSessionClient.NOMBRE_MAX_SESSION + " sessions.");
      DialogueInformation vue = new DialogueInformation(modele);
      vue.afficher();
      return;
    }
    
    // S'il s'agit d'une boite de dialogue il n'y a pas de bouton à créer pour la barre de session ou de cacher le panel du dessous
    if (pSessionBase.isBoiteDialogue()) {
      return;
    }
    
    // Vérification s'il est nécessaire d'afficher la barre de session
    if (afficherBarreSession) {
      if (getComponentCount() > 0 && getComponent(1) instanceof JPanel) {
        pSessionBase.setVisibleDialog(false);
      }
      remove(1);
      validate();
      repaint();
    }
    
    // S'il s'agit d'un panel classique
    afficherBarreSession = true;
    pnlPied.setBackground(SNCharteGraphique.COULEUR_BARRE_SESSION);
    pnlBarreSession.setVisible(true);
    
    add(pSessionBase.getPanel(), BorderLayout.CENTER);
    
    // Mettre uniquement la première lettre en majuscule dans le libellé du bouton
    String libelle = Constantes.capitaliserPremiereLettre(pSessionBase.getPanel().getName());
    
    // Créer le bouton de session
    SNBoutonSession bouton = new SNBoutonSession(pSessionBase.getIdSession(), libelle, null);
    bouton.setComponentPopupMenu(pmOptionOFrame);
    bouton.addActionListener(new ActionListener() {
      
      @Override
      public void actionPerformed(ActionEvent e) {
        remove(1);
        add(pSessionBase.getPanel(), BorderLayout.CENTER);
        // Rafraichir le panel
        validate();
        repaint();
      }
    });
    bouton.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        JToggleButton bouton = ((JToggleButton) e.getSource());
        bouton.setToolTipText(pSessionBase.getToolTipTxt(bouton.isSelected()));
      }
    });
    
    // Insertion du bouton de session dans la barre de session
    pnlBarreSession.ajouterBouton(bouton);
    pSessionBase.startConnexion();
    
    // Rafraichir le panel
    validate();
    repaint();
  }
  
  /**
   * Supprime l'écran d'une session.
   */
  public void supprimerSession(final SessionBase pSessionBase) {
    // Vérifier qu'il ne s'agit pas d'une boite de dialogue comme celle de l'AssistanceRI
    if (!pSessionBase.isBoiteDialogue()) {
      // Tester s'il s'agit d'un panel Java (comptoir par exemple)
      if (pSessionBase instanceof SessionJava) {
        // Tester si le panel est dans la fenêtre principale
        if (((SessionJava) pSessionBase).getSessionIndependante() == null) {
          // Contrôle qu'il y ait bien un panel à enlever
          if (getComponentCount() > 1) {
            remove(1);
            pnlBarreSession.supprimerBouton(pSessionBase.getIdSession());
          }
        }
        // Sinon il s'agit d'une boite de dialogue du type Gestion des impressions par exemple
        else {
          ((SessionJava) pSessionBase).getSessionIndependante().fermerFenetre();
        }
      }
      // Tester s'il s'agit d'un panel RPG (Série N classique)
      else if (pSessionBase instanceof SessionRPG) {
        // Si le panel est dans la fenêtre principale
        if (((SessionRPG) pSessionBase).getSessionIndependante() == null) {
          // Contrôle qu'il y ait bien un panel à enlever
          if (getComponentCount() > 1) {
            remove(1);
            pnlBarreSession.supprimerBouton(pSessionBase.getIdSession());
          }
        }
        // Sinon il s'agit d'une boite de dialogue du type Gestion des impressions par exemple
        else {
          ((SessionRPG) pSessionBase).getSessionIndependante().fermerFenetre();
        }
      }
    }
    pSessionBase.dispose();
    // Bourrin mais efficace...
    System.gc();
    
    // Vérification s'il y a d'autres sessions dans l'onglet
    int indiceSession = getIndiceSessionDansOnglet();
    if (indiceSession > -1) {
      // Ajout du panel à l'écran
      add(((SessionBase) ClientSerieN.listeSession.getObject(indiceSession)).getPanel(), BorderLayout.CENTER);
      // Récupération de l'identifiant de la session à afficher
      IdSession idSession = ((SessionBase) ClientSerieN.listeSession.getObject(indiceSession)).getIdSession();
      // Activation du bouton de la session dans barre de sessions
      pnlBarreSession.selectionnerSession(idSession);
    }
    else {
      if (ClientSerieN.getPreferencesUserManager().getPreferences().isOuvrirAutomatiquement()) {
        clientSerieN.afficherMenuGeneral();
      }
      requestFocus();
    }
    afficherBarreSession = pnlBarreSession.isVisible();
    if (!afficherBarreSession) {
      pnlPied.setBackground(Color.WHITE);
    }
    validate();
    repaint();
  }
  
  /**
   * Détache un onglet/un panel
   */
  public void detacherPanel() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        // Récupérer la session en cours
        SessionBase session = retrouverSessionAvecBouton();
        if (session == null) {
          return;
        }
        
        // Le panel est retiré de la fenêtre principale
        remove(1);
        pnlBarreSession.supprimerBouton(session.getIdSession());
        
        // On créer la nouvelle fenêtre afin d'y intégrer la session
        SessionIndependante si = new SessionIndependante(clientSerieN);
        si.setTitle(clientSerieN.getTitle());
        si.setSessionPanel(session, session.getPanel().getWidth(), session.getPanel().getHeight(), false);
        if (session instanceof SessionJava) {
          ((SessionJava) session).setSessionIndependante(si);
        }
        else if (session instanceof SessionRPG) {
          ((SessionRPG) session).setSessionIndependante(si);
        }
        
        // Affiche une autre session de l'onglet s'il y en a une
        int indice = getIndiceSessionDansOnglet();
        if (indice > -1) {
          add(((SessionBase) ClientSerieN.listeSession.getObject(indice)).getPanel(), BorderLayout.CENTER);
          IdSession idSession = ((SessionBase) ClientSerieN.listeSession.getObject(indice)).getIdSession();
          pnlBarreSession.selectionnerSession(idSession);
        }
        else {
          afficherBarreSession = false;
          pnlPied.setBackground(Color.WHITE);
          pnlBarreSession.setVisible(false);
        }
        validate();
        repaint();
      }
    });
  }
  
  /**
   * Sélectionne une session par son index.
   */
  public void selectionnerSessionAvecIndex(int pIndex) {
    if (pIndex > ManagerSessionClient.NOMBRE_MAX_SESSION) {
      return;
    }
    if (ClientSerieN.listeSession.getArrayList().size() > pIndex) {
      SessionBase session = ((SessionBase) ClientSerieN.listeSession.getObject(pIndex));
      remove(1);
      add(session.getPanel(), BorderLayout.CENTER);
      pnlBarreSession.selectionnerSession(session.getIdSession());
      
      // Rafraichir le panel
      validate();
      repaint();
    }
  }
  
  /**
   * Modifie l'aspect de l'icone des notifications en fonction de la priorité des notifications à lire.
   */
  public void modifierAspectNotificationBarreSession(EnumAspectNotification pAspectNotification) {
    if (pAspectNotification == null) {
      return;
    }
    if (aspectNotification == pAspectNotification) {
      return;
    }
    aspectNotification = pAspectNotification;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir.
   */
  private void rafraichir() {
    rafraichirBoutonNotification();
  }
  
  /**
   * Rafraichir le bouton des notifications.
   */
  private void rafraichirBoutonNotification() {
    // Suppression du bouton des notifications
    if (btNotification != null) {
      pnlBarreOutil.supprimerBouton(btNotification);
    }
    
    // Gestion des différents cas de priorité
    switch (aspectNotification) {
      case IMPORTANT_NON_LUE:
        btNotification = btNotificationImportanteALire;
        break;
      
      case NORMAL_NON_LUE:
        btNotification = btNotificationALire;
        break;
      
      case DEFAUT:
        btNotification = btNotificationDefaut;
        break;
    }
    
    // Ajoute le bouton des notifications
    pnlBarreOutil.ajouterBouton(btNotification);
  }
  
  /**
   * Configurer le panel.
   */
  private void initialiserComposant() {
    // Définir le nom du composant
    setName("pnlFond");
    
    // Créer les 3 boutons correspondant aux aspects des notifications
    btNotificationImportanteALire = new SNBoutonNotificationImportanteALire();
    // L'évènement à executer lors de l'appuie sur le bouton "Afficher notifications importantes à lire"
    btNotificationImportanteALire.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        afficherNotification(EnumStatutNotification.NON_LUE, null);
      }
    });
    btNotificationALire = new SNBoutonNotificationALire();
    // L'évènement à executer lors de l'appuie sur le bouton "Afficher notifications à lire"
    btNotificationALire.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        afficherNotification(EnumStatutNotification.NON_LUE, null);
      }
    });
    btNotificationDefaut = new SNBoutonNotification();
    // L'évènement à executer lors de l'appuie sur le bouton "Afficher notifications"
    btNotificationDefaut.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        afficherNotification(null, null);
      }
    });
    
    // Définir la couleur de fond
    setBackground(Color.WHITE);
    
    // Définir la taille par défaut
    setPreferredSize(TAILLE);
    
    // Gestion de la couleur du pied en fonction de la visiblité de la barre de session
    if (afficherBarreSession) {
      pnlPied.setBackground(SNCharteGraphique.COULEUR_BARRE_SESSION);
      pnlBarreSession.setVisible(true);
    }
    else {
      pnlPied.setBackground(Color.WHITE);
      pnlBarreSession.setVisible(false);
    }
    
    // Ajouter l'image de fond et l'icone de l'application
    initialiserImageFond();
    
    // Contrôle l'affichage du menu contextuel qui permet de forcer la fermeture d'une session
    controleForceFermeturePopupMenu();
    
    rafraichir();
  }
  
  /**
   * Retourne l'indice de la dernière session intégrée à l'onglet.
   */
  private int getIndiceSessionDansOnglet() {
    for (int i = ClientSerieN.listeSession.getArrayList().size(); --i >= 0;) {
      // Contrôle s'il s'agit d'une boite de dialogue
      if (((SessionBase) ClientSerieN.listeSession.getArrayList().get(i)).isBoiteDialogue()) {
        continue;
      }
      // Contrôle s'il s'agit d'une fenêtre indépendante
      if (ClientSerieN.listeSession.getArrayList().get(i) instanceof SessionJava) {
        if (((SessionJava) ClientSerieN.listeSession.getArrayList().get(i)).getSessionIndependante() == null) {
          return i;
        }
      }
      else if (ClientSerieN.listeSession.getArrayList().get(i) instanceof SessionRPG) {
        if (((SessionRPG) ClientSerieN.listeSession.getArrayList().get(i)).getSessionIndependante() == null) {
          return i;
        }
      }
    }
    return -1;
  }
  
  /**
   * Récupère l'image de fond.
   */
  private void initialiserImageFond() {
    try {
      ImagePainter imagefond = new ImagePainter(ImageIO.read(this.getClass().getClassLoader().getResource(Constantes.IMG_FOND)));
      setBackgroundPainter(imagefond);
    }
    catch (Exception e) {
      throw new MessageErreurException("Impossible de charger l'image de fond.");
    }
  }
  
  /**
   * Contrôle l'affichage ou non du menu contextuel pour le forçage de la fermeture d'une session
   */
  private void controleForceFermeturePopupMenu() {
    pmOptionOFrame.addPopupMenuListener(new PopupMenuListener() {
      @Override
      public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        // Les menus sont grisés si la touche CTRL n'est pas enfoncée
        KeyboardFocusManager kbm = KeyboardFocusManager.getCurrentKeyboardFocusManager();
        kbm.addKeyEventPostProcessor(new KeyEventPostProcessor() {
          @Override
          public boolean postProcessKeyEvent(KeyEvent e) {
            if (e.getModifiersEx() == InputEvent.CTRL_DOWN_MASK) {
              if (e.getID() == KeyEvent.KEY_PRESSED) {
                miForcerFermeture.setEnabled(true);
              }
            }
            if (e.getID() == KeyEvent.KEY_RELEASED) {
              miForcerFermeture.setEnabled(false);
            }
            return false;
          }
        });
      }
      
      @Override
      public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
      }
      
      @Override
      public void popupMenuCanceled(PopupMenuEvent e) {
      }
    });
  }
  
  /**
   * Retourne la session associée au toggle bouton.
   */
  private SessionBase retrouverSessionAvecBouton() {
    // Avec le raccourci clavier
    if (pmOptionOFrame.getInvoker() == null) {
      for (Object sp : ClientSerieN.listeSession.getArrayList()) {
        if (sp instanceof SessionJava) {
          if (((SessionJava) sp).getSessionIndependante() == null && ((SessionBase) sp).isEtatActif()) {
            return (SessionBase) sp;
          }
        }
        else if (sp instanceof SessionRPG) {
          if (((SessionRPG) sp).getSessionIndependante() == null && ((SessionBase) sp).isEtatActif()) {
            return (SessionBase) sp;
          }
        }
      }
    }
    // Avec le bouton droit
    else {
      IdSession idSession = ((SNBoutonSession) pmOptionOFrame.getInvoker()).getIdSession();
      for (Object sp : ClientSerieN.listeSession.getArrayList()) {
        if (((SessionBase) sp).getIdSession().equals(idSession)) {
          return (SessionBase) sp;
        }
      }
    }
    
    return null;
  }
  
  /**
   * Ajoute le point de menu de la session en cours dans les Favoris.
   */
  private void ajouterFavoris() {
    EventQueue.invokeLater(new Runnable() {
      @Override
      public void run() {
        // On retire le panel de la session de la fenêtre principale
        SessionBase session = retrouverSessionAvecBouton();
        if (session.getInfoPrg() != null) {
          MenuDetail menu = session.getInfoPrg().getPointDeMenu();
          ClientSerieN.modeleMenuFavoris.ajouterPointMenu(menu);
        }
      }
    });
  }
  
  /**
   * Lance la consultation des notifications.
   */
  private void afficherNotification(EnumStatutNotification pStatut, EnumPrioriteNotification pPriorite) {
    MenuDetail pointDeMenu = ClientSerieN.modeleMenuGeneral.retournerPointDeMenu(EnumPointMenu.NOTIFICATION.getNumero());
    if (pointDeMenu == null) {
      throw new MessageErreurException("Le point de menu " + EnumPointMenu.NOTIFICATION + " n'a pas été trouvé.");
    }
    ParametreMenuNotification parametres = new ParametreMenuNotification();
    parametres.setModeUtilisation(EnumModeUtilisation.CONSULTATION);
    parametres.setStatut(pStatut);
    parametres.setPriorite(pPriorite);
    
    ClientSerieN.modeleMenuGeneral.executerPointDeMenu(pointDeMenu, parametres);
  }
  
  // -- Accesseurs
  
  public boolean isAfficherBarreSession() {
    return afficherBarreSession;
  }
  
  // -- Méthodes évènementielles
  
  private void pmOptionOFramePopupMenuWillBecomeVisible(PopupMenuEvent e) {
    miAjouterFavoris.setEnabled(ClientSerieN.modeleMenuFavoris.isAjoutPossible());
  }
  
  /**
   * Détacher la fenêtre de session de la barre d'onglets.
   */
  private void miDetacherActionPerformed(ActionEvent e) {
    try {
      detacherPanel();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Ajouter le point de menu de la session aux favoris.
   */
  private void miAjouterFavorisActionPerformed(ActionEvent e) {
    try {
      ajouterFavoris();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Capturer l'écran de Série N.
   */
  private void miCapturerFenetreActionPerformed(ActionEvent e) {
    try {
      if (clientSerieN != null) {
        clientSerieN.capturerEcran();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Forcer la fermeture de la session.
   */
  private void miForcerFermetureActionPerformed(ActionEvent e) {
    try {
      retrouverSessionAvecBouton().fermerEcran();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPied = new JPanel();
    pnlBarreSession = new SNBarreSession();
    pnlBarreOutil = new SNBarreOutil();
    pmOptionOFrame = new JPopupMenu();
    miDetacher = new JMenuItem();
    miAjouterFavoris = new JMenuItem();
    miCapturerFenetre = new JMenuItem();
    miForcerFermeture = new JMenuItem();
    
    // ======== this ========
    setPreferredSize(new Dimension(800, 640));
    setBackground(Color.white);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPied ========
    {
      pnlPied.setBackground(Color.white);
      pnlPied.setName("pnlPied");
      pnlPied.setLayout(new BorderLayout(0, 2));
      
      // ---- pnlBarreSession ----
      pnlBarreSession.setPreferredSize(new Dimension(0, 40));
      pnlBarreSession.setName("pnlBarreSession");
      pnlPied.add(pnlBarreSession, BorderLayout.CENTER);
      
      // ======== pnlBarreOutil ========
      {
        pnlBarreOutil.setPreferredSize(new Dimension(36, 40));
        pnlBarreOutil.setOpaque(false);
        pnlBarreOutil.setMinimumSize(new Dimension(36, 40));
        pnlBarreOutil.setName("pnlBarreOutil");
        pnlBarreOutil.setLayout(new FlowLayout(FlowLayout.RIGHT));
      }
      pnlPied.add(pnlBarreOutil, BorderLayout.EAST);
    }
    add(pnlPied, BorderLayout.SOUTH);
    
    // ======== pmOptionOFrame ========
    {
      pmOptionOFrame.setName("pmOptionOFrame");
      pmOptionOFrame.addPopupMenuListener(new PopupMenuListener() {
        @Override
        public void popupMenuCanceled(PopupMenuEvent e) {
        }
        
        @Override
        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        }
        
        @Override
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
          pmOptionOFramePopupMenuWillBecomeVisible(e);
        }
      });
      
      // ---- miDetacher ----
      miDetacher.setText("D\u00e9tacher l'onglet");
      miDetacher.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_D, KeyEvent.CTRL_MASK));
      miDetacher.setName("miDetacher");
      miDetacher.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miDetacherActionPerformed(e);
        }
      });
      pmOptionOFrame.add(miDetacher);
      
      // ---- miAjouterFavoris ----
      miAjouterFavoris.setText("Ajouter aux favoris");
      miAjouterFavoris.setName("miAjouterFavoris");
      miAjouterFavoris.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAjouterFavorisActionPerformed(e);
        }
      });
      pmOptionOFrame.add(miAjouterFavoris);
      
      // ---- miCapturerFenetre ----
      miCapturerFenetre.setText("Capturer la fen\u00eatre");
      miCapturerFenetre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_PRINTSCREEN, KeyEvent.CTRL_MASK));
      miCapturerFenetre.setName("miCapturerFenetre");
      miCapturerFenetre.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miCapturerFenetreActionPerformed(e);
        }
      });
      pmOptionOFrame.add(miCapturerFenetre);
      
      // ---- miForcerFermeture ----
      miForcerFermeture.setText("Forcer la fermeture ");
      miForcerFermeture.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      miForcerFermeture.setEnabled(false);
      miForcerFermeture.setName("miForcerFermeture");
      miForcerFermeture.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miForcerFermetureActionPerformed(e);
        }
      });
      pmOptionOFrame.add(miForcerFermeture);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPied;
  private SNBarreSession pnlBarreSession;
  private SNBarreOutil pnlBarreOutil;
  private JPopupMenu pmOptionOFrame;
  private JMenuItem miDetacher;
  private JMenuItem miAjouterFavoris;
  private JMenuItem miCapturerFenetre;
  private JMenuItem miForcerFermeture;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
