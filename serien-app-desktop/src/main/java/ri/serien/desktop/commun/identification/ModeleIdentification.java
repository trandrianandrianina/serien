/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.commun.identification;

import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionIdentification;
import ri.serien.desktop.user.PreferencesId;
import ri.serien.desktop.user.PreferencesManager;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierByte;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue d'identification.
 */
public class ModeleIdentification extends AbstractModeleDialogue {
  // Constantes
  private static final String NOM_LOGO_STANDARD = "logo.jpg";
  
  // Variables
  private SessionIdentification sessionIdentification = null;
  private ListeSousEnvironnement listeSousEnvironnement = null;
  private PreferencesManager preferencesManager = new PreferencesManager(PreferencesId.class);
  private PreferencesId preferencesId = null;
  private Icon logo = null;
  private IdSousEnvironnement idSousEnvironnement = null;
  private SousEnvironnement sousEnvironnement = null;
  private String utilisateur = null;
  private String motDePasse = null;
  private boolean enregistrerUtilisateur = false;
  private Message message = null;
  
  /**
   * Cette boîte de dialogue nécessite un IdSession.
   */
  public ModeleIdentification() {
    super(null);
  }
  
  /**
   * Initialiser les données avant la phase de chargement des données.
   * Voir InterfaceModele.
   */
  @Override
  public void initialiserDonnees() {
    // Tester les droits sur le répertoire home de l'utilisateur
    if (!ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().isCanWrite()) {
      throw new MessageErreurException("Vous n'avez pas les droits d'écriture sur "
          + ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getNomDossierRacine());
    }
    
    // Essayer d'afficher en premier lieu le logo standard
    // Si on le trouve pas, le logo est caché en attendant de charger le logo issu du modèle pour éviter un effet de clignotement
    String nomDossierRT = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierRT().getAbsolutePath();
    File file = new File(nomDossierRT + File.separatorChar + "logo.jpg");
    if (file.exists()) {
      logo = new ImageIcon(file.getAbsolutePath());
    }
    else {
      logo = null;
    }
  }
  
  /**
   * Initialiser les données avant la phase de chargement des données.
   * Charger les données du modèle.
   */
  @Override
  public void chargerDonnees() {
    // Créer une session pour l'identification
    sessionIdentification = new SessionIdentification();
    
    chargerListeSousEnvironnement();
    chargerPreferenceUtilisateur();
    chargerLogo();
  }
  
  /**
   * Quitter la boite de dialogue en validant la saisie. Les données modifiées sont conservées.
   * Cette méthode fixe le mode de sortie avec validation et demande un rafraîchissement pour déclencher la fermeture de la vue.
   * Si la méthode est surchargéee, il est recommandé d'appelé cette méthode parente à la fin.
   */
  @Override
  public void quitterAvecValidation() {
    // Contrôler la saisie des informations de connexion
    // Pas de MessageErreurException dans ce cas mais affichage d'un message dans la boîte de dialogue.
    if (idSousEnvironnement == null) {
      genererMessageErreur("Veuillez sélectionner un sous-environnement.");
      return;
    }
    if (utilisateur == null || utilisateur.isEmpty()) {
      genererMessageErreur("Veuillez saisir un utilisateur.");
      return;
    }
    // Obligatoire car sinon la classe Authentification dans le serveur considère que c'est bon
    if (motDePasse == null || motDePasse.isEmpty()) {
      genererMessageErreur("Veuillez saisir un mot de passe.");
      return;
    }
    IdSousEnvironnement idSousEnvironnementCorrige = listeSousEnvironnement.controlerUtilisateur(idSousEnvironnement, utilisateur);
    if (idSousEnvironnementCorrige == null) {
      genererMessageErreur("Cet utilisateur n'est pas autorisé dans le sous-environnement sélectionné.");
      return;
    }
    else if (!idSousEnvironnementCorrige.equals(idSousEnvironnement)) {
      idSousEnvironnement = idSousEnvironnementCorrige;
      genererMessageErreur("Vous n'avez pas sélectionné le bon sous-environnement.");
      return;
    }
    
    try {
      // Initialiser les informations utilisateur en fonction du sous-environnement choisi
      ManagerSessionClient.getInstance().getEnvUser().setIdSousEnvironnement(idSousEnvironnement);
      ManagerSessionClient.getInstance().getEnvUser().setProfil(utilisateur);
      ManagerSessionClient.getInstance().getEnvUser().setMotDePasse(motDePasse);
      listeSousEnvironnement.initialiserEnvUser(idSousEnvironnement, ManagerSessionClient.getInstance().getEnvUser());
      ManagerSessionClient.getInstance().getEnvUser().setDossierTravail(sousEnvironnement.getFolder());
      
      // Contrôler la validité de l'utilisateur et du mot de passe
      if (!ManagerServiceSession.connecterClientDesktop(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop(),
          ManagerSessionClient.getInstance().getEnvUser())) {
        throw new MessageErreurException("Utilisateur ou mot de passe incorrect.");
      }
      
      // Récupérer les données utilisateurs mise à jour lors de la connexion au serveur
      EnvUser envUser = ManagerServiceSession.getEnvUser(ManagerSessionClient.getInstance().getEnvUser().getIdClientDesktop());
      ManagerSessionClient.getInstance().setEnvUser(envUser);
      
      // Ouvrir une session client
      ClientSerieN.setIdSessionMenu(ManagerSessionClient.getInstance().creerSessionClient());
      ManagerServiceSession.ouvrirSession(ClientSerieN.getIdSessionMenu(), EnumTypeSession.SESSION_MENU, null);
      
      // Contrôler les paramètres importants du profil pour une utilisation avec la version graphique
      ManagerSessionClient.getInstance().getEnvUser().controlerProfil();
      
      // Récupérer les ficheirs RunTimes
      sessionIdentification.demanderListeRT();
    }
    catch (MessageErreurException e) {
      genererMessageErreur(e.getMessage());
      return;
    }
    
    // Mémoriser le dernier sous-environnement utilisé (dans tous les cas)
    preferencesId.setIdSousEnvironnementDefaut(idSousEnvironnement);
    
    // Enregistrer l'identifiant si l'utilisateur a coché la case
    if (enregistrerUtilisateur) {
      preferencesId.setData(idSousEnvironnement, ManagerSessionClient.getInstance().getProfil(), motDePasse);
    }
    
    // Sauver les préférences
    preferencesManager.sauvePreferences();
    
    // Fermer la session
    sessionIdentification.fermerSession();
    
    // Fermer la fenêtre
    super.quitterAvecValidation();
  }
  
  /**
   * Quitter la boite de dialogue an annulant la saisie. Les données modifiées ne sont pas conservées.
   * Cette méthode fixe le mode de sortie avec annulation et demande un rafraîchissement pour déclencher la fermeture de la vue.
   * Si la méthode est surchargéee, il est recommandé d'appelé cette méthode parente à la fin.
   */
  @Override
  public void quitterAvecAnnulation() {
    // Tracer
    Trace.info("L'utilisateur a annulé la boîte de dialogue d'identification.");
    
    // Fermer la session
    sessionIdentification.fermerSession();
    
    // Fermer la fenêtre
    super.quitterAvecAnnulation();
  }
  
  /**
   * Charger la liste des sous-environnements.
   */
  private void chargerListeSousEnvironnement() {
    listeSousEnvironnement = ManagerServiceSession.listerSousEnvironnement(sessionIdentification.getIdSession());
    
    // Tester s'il n'y a pas de sous-environnements disponibles
    if (listeSousEnvironnement == null || listeSousEnvironnement.getListe().isEmpty()) {
      throw new MessageErreurException(
          "Pas de sous-environnements disponibles. Vérifier que les sous-environnements rattachés sont bien démarrés.");
    }
  }
  
  /**
   * Charger les préférences de l'utilisateur.
   */
  private void chargerPreferenceUtilisateur() {
    // Lire les préférences de l'utilisateur
    String nomDossierConfig = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierConfig().getAbsolutePath();
    preferencesManager.setNomFichier(nomDossierConfig + File.separatorChar + "preferencesId");
    preferencesId = (PreferencesId) preferencesManager.getPreferences();
    
    // Créer des préférences par défaut s'il n'existe pas de fichier preferenceId.json dans le dossier config
    if (preferencesId == null) {
      preferencesId = new PreferencesId();
      preferencesManager.setPreferences(preferencesId);
    }
    
    ManagerSessionClient.getInstance().getEnvUser().setDeviceImpression(preferencesId.getDeviceImpression());
    
    // Initialiser le sous-environnement avec celui par défaut si aucun n'est définit
    if (idSousEnvironnement == null && preferencesId.getIdSousEnvironnementDefaut() != null) {
      idSousEnvironnement = preferencesId.getIdSousEnvironnementDefaut();
      // Contrôle que l'id des préférences soit bien présent dans la liste actuelle des sous environnements
      if (listeSousEnvironnement.getSousEnvironnement(idSousEnvironnement) == null) {
        idSousEnvironnement = null;
      }
    }
    // Sinon, sélectionner le premier sous-environnement de la liste
    if (idSousEnvironnement == null && listeSousEnvironnement.getListe().size() >= 1) {
      idSousEnvironnement = listeSousEnvironnement.getListe().get(0).getId();
    }
    // Afficher un message d'erreur si on ne parvient pas à sélectionner un sous-environnement par défaut
    if (idSousEnvironnement == null) {
      throw new MessageErreurException("Aucun sous-environnement n'est définit par défaut.");
    }
    sousEnvironnement = listeSousEnvironnement.getSousEnvironnement(idSousEnvironnement);
    
    // Charger le login et le mot de passe en fonction du sous-environnement
    utilisateur = preferencesId.getUtilisateur(idSousEnvironnement);
    motDePasse = preferencesId.getMotDePasse(idSousEnvironnement);
  }
  
  /**
   * Déterminer le logo à afficher dans la boîte de dialogue.
   * Charger l'image du logo à partir du serveur pour la mettre sur le poste de travail.
   */
  private void chargerLogo() {
    logo = null;
    
    // Rechercher en premier lieu le logo spécifique à un sous-environnement
    if (telechargerLogoDuServeur("logo_" + idSousEnvironnement.getNom().toLowerCase() + ".jpg")) {
      return;
    }
    
    // Rechercher en second lieu le logo standard "logo.jpg"
    if (telechargerLogoDuServeur(NOM_LOGO_STANDARD)) {
      return;
    }
    
    // Rechercher en dernier lieu le logo sur le poste de travail
    String nomDossierRT = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierRT().getAbsolutePath();
    File file = new File(nomDossierRT + File.separatorChar + NOM_LOGO_STANDARD);
    if (file.exists()) {
      logo = new ImageIcon(file.getAbsolutePath());
    }
  }
  
  /**
   * Télécharger un logo à partir du serveur.
   */
  private boolean telechargerLogoDuServeur(String nomLogo) {
    // Définir l'emplacement du logo en local
    String nomDossierRT = ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getDossierRT().getAbsolutePath();
    File file = new File(nomDossierRT + File.separatorChar + NOM_LOGO_STANDARD);
    
    // Uploader la dernière image du logo sur le poste de travail
    String separateurServeur = "" + ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getSeparateur();
    String nomDossierRuntimeServeur =
        ManagerSessionClient.getInstance().getEnvUser().getDossierServeur().getNomDossierRT() + separateurServeur;
    String cheminServeurLogo = nomDossierRuntimeServeur.replaceAll(separateurServeur, Constantes.SEPARATEUR_DOSSIER) + nomLogo;
    GestionFichierByte gfb = ManagerServiceTechnique.demanderFichier(sessionIdentification.getIdSession(), cheminServeurLogo);
    if (gfb == null || gfb.getContenuFichier() == null) {
      return false;
    }
    
    // Renseigner le logo
    logo = new ImageIcon(gfb.getContenuFichier());
    
    // Sauver le fichier
    gfb.changeNomFichier(file.getAbsolutePath());
    gfb.ecritureFichier();
    return true;
  }
  
  /**
   * Modifier le sous-environnement.
   */
  public void modifierSousEnvironnement(IdSousEnvironnement pIdSousEnvironnement) {
    if (Constantes.equals(idSousEnvironnement, pIdSousEnvironnement)) {
      return;
    }
    // Les informations concernant le sous environnement
    idSousEnvironnement = pIdSousEnvironnement;
    if (idSousEnvironnement != null && listeSousEnvironnement != null) {
      sousEnvironnement = listeSousEnvironnement.getSousEnvironnement(idSousEnvironnement);
    }
    
    // Recharger les informations qui dépendent du sous-environnement
    chargerLogo();
    chargerPreferenceUtilisateur();
    rafraichir();
  }
  
  /**
   * Modifier le compte de l'utilisateur.
   */
  public void modifierUtilisateur(String pUtilisateur) {
    if (Constantes.equals(pUtilisateur, utilisateur)) {
      return;
    }
    utilisateur = pUtilisateur;
    rafraichir();
  }
  
  /**
   * Modifier le mot de passe de l'utilisateur.
   */
  public void modifierMotDePasse(String pMotDePasse) {
    if (Constantes.equals(pMotDePasse, motDePasse)) {
      return;
    }
    motDePasse = pMotDePasse;
    rafraichir();
  }
  
  /**
   * Modifier l'option de sauvegarde des informations de connexion.
   */
  public void modifierEnregistrerUtilisateur(boolean pEnregistrerUtilisateur) {
    if (enregistrerUtilisateur == pEnregistrerUtilisateur) {
      return;
    }
    enregistrerUtilisateur = pEnregistrerUtilisateur;
    rafraichir();
  }
  
  /**
   * Génère un message d'erreur.
   */
  public void genererMessageErreur(String pMessageErreur) {
    pMessageErreur = Constantes.normerTexte(pMessageErreur);
    if (pMessageErreur.isEmpty()) {
      message = null;
    }
    else {
      message = Message.getMessageImportant(pMessageErreur);
    }
    rafraichir();
  }
  
  // -- Accesseurs
  
  /**
   * Message d'erreur.
   */
  public Message getMessage() {
    return message;
  }
  
  /**
   * Compte de l'utilisateur à connecter.
   */
  public String getUtilisateur() {
    return utilisateur;
  }
  
  /**
   * Mot de passe de l'utilisateur à connecter.
   */
  public String getMotDePasse() {
    return motDePasse;
  }
  
  /**
   * Nom du sous-environnement.
   */
  public IdSousEnvironnement getIdSousEnvironnement() {
    return idSousEnvironnement;
  }
  
  /**
   * Configuration RealTime.
   */
  public ListeSousEnvironnement getListeSousEnvironnement() {
    return listeSousEnvironnement;
  }
  
  /**
   * Logo affiché à gauche de la fenêtre.
   */
  public Icon getLogo() {
    return logo;
  }
  
  /**
   * Indiquer s'il faut enregistrer les paramètres de connexion de l'utilisateur.
   */
  public boolean isEnregistrerUtilisateur() {
    return enregistrerUtilisateur;
  }
  
  /**
   * Vérifier si une tentative de connextion est possible avec les informations saisies.
   */
  public boolean isConnectionPossible() {
    if (utilisateur == null || utilisateur.isEmpty() || motDePasse == null || motDePasse.isEmpty()) {
      return false;
    }
    return true;
  }
  
}
