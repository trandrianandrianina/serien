/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.commun.identification;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Arrays;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue d'identification.
 */
public class DialogueIdentification extends AbstractVueDialogue<ModeleIdentification> {
  /**
   * Constructeur.
   */
  public DialogueIdentification(ModeleIdentification pModele) {
    super(pModele);
  }
  
  /**
   * Afficher la boîte de dialogue qui permet de s'identifier.
   * 
   * @param Titre de la boîte de dialogue.
   * @return true=l'utilisateur s'est authentifié avec succès, false=sinon.
   */
  public static boolean afficher(String pTitre) {
    // Afficher la boîte de dialogue
    ModeleIdentification modeleIdentification = new ModeleIdentification();
    DialogueIdentification vueIdentification = new DialogueIdentification(modeleIdentification);
    vueIdentification.afficher();
    return modeleIdentification.isSortieAvecValidation();
  }
  
  @Override
  public void initialiserComposants() {
    // Initialiser les composants graphiques
    initComponents();
    
    // Configurer la saisie possible (10 caractères alphanumérique en majuscules)
    tfUtilisateur.init(10, true, true);
    
    // Configurer la barre de bouton
    snBarreBouton.ajouterBouton(EnumBouton.CONNECTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirLogo();
    rafraichirSousEnvironnement();
    rafraichirUtilisateur();
    rafraichirMotDePasse();
    rafraichirEnregistrerUtilisateur();
    rafraichirMessage();
    rafraichirBoutonConnecter();
    
    // Positionner le curseur au bon endroit
    if (getFocusOwner() != tfUtilisateur && getFocusOwner() != pfMotDePasse) {
      if (tfUtilisateur.getText().trim().isEmpty()) {
        tfUtilisateur.requestFocus();
      }
      else {
        pfMotDePasse.requestFocus();
      }
    }
  }
  
  private void rafraichirLogo() {
    if (getModele().getLogo() != null) {
      lbLogo.setIcon(getModele().getLogo());
      lbLogo.setVisible(true);
    }
    else {
      lbLogo.setIcon(null);
      lbLogo.setVisible(false);
    }
  }
  
  private void rafraichirSousEnvironnement() {
    cbSousEnvironnement.setEnabled(getModele().isDonneesChargees());
    
    cbSousEnvironnement.removeAllItems();
    ListeSousEnvironnement listeSousEnvironnement = getModele().getListeSousEnvironnement();
    if (listeSousEnvironnement != null && listeSousEnvironnement.getListe().size() > 1) {
      for (SousEnvironnement sousEnvironnement : listeSousEnvironnement.getListe()) {
        cbSousEnvironnement.addItem(sousEnvironnement.getId());
      }
      if (getModele().getIdSousEnvironnement() != null) {
        cbSousEnvironnement.setSelectedItem(getModele().getIdSousEnvironnement());
      }
      lbSousEnvironnement.setVisible(true);
      cbSousEnvironnement.setVisible(true);
    }
    else {
      lbSousEnvironnement.setVisible(false);
      cbSousEnvironnement.setVisible(false);
    }
  }
  
  private void rafraichirUtilisateur() {
    tfUtilisateur.setEnabled(getModele().isDonneesChargees());
    
    String valeur = "";
    if (getModele().getUtilisateur() != null) {
      valeur = Constantes.normerTexte(getModele().getUtilisateur());
    }
    
    if (!tfUtilisateur.getText().equals(valeur)) {
      tfUtilisateur.setText(Constantes.normerTexte(getModele().getUtilisateur()));
    }
  }
  
  private void rafraichirMotDePasse() {
    pfMotDePasse.setEnabled(getModele().isDonneesChargees());
    
    String valeur = "";
    if (getModele().getMotDePasse() != null) {
      valeur = Constantes.normerTexte(getModele().getMotDePasse());
    }
    
    String password = Arrays.toString(pfMotDePasse.getPassword());
    if (!password.equals(valeur)) {
      pfMotDePasse.setText(Constantes.normerTexte(getModele().getMotDePasse()));
    }
  }
  
  private void rafraichirEnregistrerUtilisateur() {
    chkEnregistrer.setEnabled(getModele().isDonneesChargees());
    chkEnregistrer.setSelected(getModele().isEnregistrerUtilisateur());
  }
  
  private void rafraichirMessage() {
    Message message = getModele().getMessage();
    snMessage.setMessage(message);
  }
  
  private void rafraichirBoutonConnecter() {
    snBarreBouton.activerBouton(EnumBouton.CONNECTER, getModele().isDonneesChargees() && getModele().isConnectionPossible());
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONNECTER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void cbSousEnvironnementItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        if (cbSousEnvironnement.getSelectedItem() instanceof IdSousEnvironnement) {
          getModele().modifierSousEnvironnement((IdSousEnvironnement) cbSousEnvironnement.getSelectedItem());
        }
        else {
          getModele().modifierSousEnvironnement(null);
        }
      }
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void tfUtilisateurFocusGained(FocusEvent e) {
    tfUtilisateur.selectAll();
  }
  
  private void tfUtilisateurKeyReleased(KeyEvent e) {
    try {
      getModele().modifierUtilisateur(tfUtilisateur.getText());
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void pfMotDePasseFocusGained(FocusEvent e) {
    pfMotDePasse.selectAll();
  }
  
  private void pfMotDePasseKeyReleased(KeyEvent e) {
    try {
      getModele().modifierMotDePasse(new String(pfMotDePasse.getPassword()));
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void chkEnregistrerItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierEnregistrerUtilisateur(chkEnregistrer.isSelected());
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void thisWindowClosing(WindowEvent e) {
    try {
      getModele().quitterAvecAnnulation();
    }
    catch (Exception exception) {
      getModele().genererMessageErreur(exception.getMessage());
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    lbLogo = new JLabel();
    pnlContenu = new JPanel();
    lbUtilisateur = new JLabel();
    lbMotDePasse = new JLabel();
    tfUtilisateur = new XRiTextField();
    pfMotDePasse = new JPasswordField();
    chkEnregistrer = new JCheckBox();
    lbSousEnvironnement = new JLabel();
    cbSousEnvironnement = new JComboBox();
    snMessage = new SNMessage();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Identification");
    setResizable(false);
    setModal(true);
    setFont(new Font("Tahoma", Font.PLAIN, 13));
    setMinimumSize(new Dimension(650, 350));
    setBackground(Color.white);
    setAlwaysOnTop(true);
    setName("this");
    addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        thisWindowClosing(e);
      }
    });
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout(10, 10));
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(Color.white);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
      
      // ---- lbLogo ----
      lbLogo.setIcon(new ImageIcon(getClass().getResource("/images/logo.jpg")));
      lbLogo.setAlignmentY(0.0F);
      lbLogo.setMinimumSize(new Dimension(250, 250));
      lbLogo.setHorizontalAlignment(SwingConstants.CENTER);
      lbLogo.setHorizontalTextPosition(SwingConstants.CENTER);
      lbLogo.setFocusable(false);
      lbLogo.setRequestFocusEnabled(false);
      lbLogo.setName("lbLogo");
      pnlPrincipal.add(lbLogo,
          new GridBagConstraints(0, 0, 1, 2, 0.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(Color.white);
        pnlContenu.setPreferredSize(new Dimension(350, 220));
        pnlContenu.setMinimumSize(new Dimension(350, 220));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 40, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ---- lbUtilisateur ----
        lbUtilisateur.setText("Utilisateur");
        lbUtilisateur.setFont(lbUtilisateur.getFont().deriveFont(lbUtilisateur.getFont().getSize() + 2f));
        lbUtilisateur.setPreferredSize(new Dimension(140, 30));
        lbUtilisateur.setMaximumSize(new Dimension(140, 30));
        lbUtilisateur.setMinimumSize(new Dimension(140, 30));
        lbUtilisateur.setHorizontalAlignment(SwingConstants.TRAILING);
        lbUtilisateur.setName("lbUtilisateur");
        pnlContenu.add(lbUtilisateur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 10), 0, 0));
        
        // ---- lbMotDePasse ----
        lbMotDePasse.setText("Mot de passe");
        lbMotDePasse.setFont(lbMotDePasse.getFont().deriveFont(lbMotDePasse.getFont().getSize() + 2f));
        lbMotDePasse.setMaximumSize(new Dimension(140, 30));
        lbMotDePasse.setMinimumSize(new Dimension(140, 30));
        lbMotDePasse.setPreferredSize(new Dimension(140, 30));
        lbMotDePasse.setHorizontalAlignment(SwingConstants.TRAILING);
        lbMotDePasse.setName("lbMotDePasse");
        pnlContenu.add(lbMotDePasse, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 10), 0, 0));
        
        // ---- tfUtilisateur ----
        tfUtilisateur.setFont(tfUtilisateur.getFont().deriveFont(tfUtilisateur.getFont().getSize() + 2f));
        tfUtilisateur.setMinimumSize(new Dimension(200, 30));
        tfUtilisateur.setPreferredSize(new Dimension(200, 30));
        tfUtilisateur.setName("tfUtilisateur");
        tfUtilisateur.addFocusListener(new FocusAdapter() {
          @Override
          public void focusGained(FocusEvent e) {
            tfUtilisateurFocusGained(e);
          }
        });
        tfUtilisateur.addKeyListener(new KeyAdapter() {
          @Override
          public void keyReleased(KeyEvent e) {
            tfUtilisateurKeyReleased(e);
          }
        });
        pnlContenu.add(tfUtilisateur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));
        
        // ---- pfMotDePasse ----
        pfMotDePasse.setFont(pfMotDePasse.getFont().deriveFont(pfMotDePasse.getFont().getSize() + 2f));
        pfMotDePasse.setPreferredSize(new Dimension(200, 30));
        pfMotDePasse.setMinimumSize(new Dimension(200, 30));
        pfMotDePasse.setName("pfMotDePasse");
        pfMotDePasse.addFocusListener(new FocusAdapter() {
          @Override
          public void focusGained(FocusEvent e) {
            pfMotDePasseFocusGained(e);
          }
        });
        pfMotDePasse.addKeyListener(new KeyAdapter() {
          @Override
          public void keyReleased(KeyEvent e) {
            pfMotDePasseKeyReleased(e);
          }
        });
        pnlContenu.add(pfMotDePasse, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));
        
        // ---- chkEnregistrer ----
        chkEnregistrer.setText("Enregistrer l'utilisateur");
        chkEnregistrer.setFont(chkEnregistrer.getFont().deriveFont(chkEnregistrer.getFont().getSize() + 2f));
        chkEnregistrer.setMaximumSize(new Dimension(2147483647, 2147483647));
        chkEnregistrer.setMinimumSize(new Dimension(200, 30));
        chkEnregistrer.setPreferredSize(new Dimension(200, 30));
        chkEnregistrer.setName("chkEnregistrer");
        chkEnregistrer.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            chkEnregistrerItemStateChanged(e);
          }
        });
        pnlContenu.add(chkEnregistrer, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 10, 0), 0, 0));
        
        // ---- lbSousEnvironnement ----
        lbSousEnvironnement.setText("Sous-environnement");
        lbSousEnvironnement.setFont(lbSousEnvironnement.getFont().deriveFont(lbSousEnvironnement.getFont().getSize() + 2f));
        lbSousEnvironnement.setPreferredSize(new Dimension(150, 30));
        lbSousEnvironnement.setMinimumSize(new Dimension(150, 30));
        lbSousEnvironnement.setMaximumSize(new Dimension(150, 30));
        lbSousEnvironnement.setHorizontalAlignment(SwingConstants.TRAILING);
        lbSousEnvironnement.setName("lbSousEnvironnement");
        pnlContenu.add(lbSousEnvironnement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 10, 10), 0, 0));
        
        // ---- cbSousEnvironnement ----
        cbSousEnvironnement.setFont(cbSousEnvironnement.getFont().deriveFont(cbSousEnvironnement.getFont().getSize() + 2f));
        cbSousEnvironnement.setPreferredSize(new Dimension(200, 30));
        cbSousEnvironnement.setMinimumSize(new Dimension(200, 30));
        cbSousEnvironnement.setBackground(Color.white);
        cbSousEnvironnement.setName("cbSousEnvironnement");
        cbSousEnvironnement.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            cbSousEnvironnementItemStateChanged(e);
          }
        });
        pnlContenu.add(cbSousEnvironnement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 10, 0), 0, 0));
        
        // ---- snMessage ----
        snMessage.setName("snMessage");
        pnlContenu.add(snMessage, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu,
          new GridBagConstraints(1, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton,
          new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    setSize(650, 350);
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JLabel lbLogo;
  private JPanel pnlContenu;
  private JLabel lbUtilisateur;
  private JLabel lbMotDePasse;
  private XRiTextField tfUtilisateur;
  private JPasswordField pfMotDePasse;
  private JCheckBox chkEnregistrer;
  private JLabel lbSousEnvironnement;
  private JComboBox cbSousEnvironnement;
  private SNMessage snMessage;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
