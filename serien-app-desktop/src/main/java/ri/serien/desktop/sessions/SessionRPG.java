/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.sessions;

import java.awt.Cursor;
import java.awt.EventQueue;
import java.util.LinkedList;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import javax.swing.event.AncestorEvent;

import ri.serien.desktop.composant.SessionIndependante;
import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.divers.NotificationProgrammeRPG;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composantrpg.autonome.DialogErrorCPF;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;
import ri.serien.libswing.moteur.interpreteur.FluxRecord;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Session correspondant à un écran gérée par un programme RPG.
 */
public class SessionRPG extends SessionBase {
  // Variables
  protected ClientSerieN clientSerieN = null;
  private SessionIndependante sessionIndependante = null;
  private LinkedList<ODialog> listeoDialog = new LinkedList<ODialog>();
  private SNPanelEcranRPG ecranprec;
  private SNPanelEcranRPG ecran = null;
  private ODialog dialogue = null;
  private FluxRecord fluxRpg = null;
  private String nomProgramme = "";
  private boolean isNewDialog = false;
  private boolean arretencours = false;
  private boolean arretRPGpropre = false;
  
  /**
   * Constructeur.
   */
  public SessionRPG(EnvProgramClient pEnvProgramClient, ClientSerieN pFenetre, Object pParametreMenu) {
    super(pEnvProgramClient);
    clientSerieN = pFenetre;
    setParametreMenu(pParametreMenu);
    panelPanel.setName(envProgramClient.getLibMenu());
    
    // Permet de lancer le sablier pendant les transactions
    panelPanel.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    
    Trace.debugMemoire(SessionRPG.class, "SessionRPG");
  }
  
  // -- Méthodes publiques
  
  /**
   * Ouvrir une session graphique (système des onglets).
   */
  public void ouvrirSession() {
    if (clientSerieN == null) {
      throw new MessageErreurException("La fenêtre n'est pas initialisée.");
    }
    
    // Fermeture du menu général
    clientSerieN.cacherMenuGeneral();
    // Création de la session graphiquement
    clientSerieN.ajouterSessionRpg(this);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    super.dispose();
    
    dialogue = null;
    if (listeoDialog != null) {
      for (int i = 0; i < listeoDialog.size(); i++) {
        listeoDialog.get(i).dispose();
      }
      listeoDialog.clear();
    }
    
    ecran = null;
    ecranprec = null;
    fluxRpg.dispose();
  }
  
  /**
   * Met à jour les informations de la base de données utilisateur.
   */
  @Override
  public void rafraichirBaseDeDonnees() {
    if (clientSerieN == null) {
      return;
    }
    clientSerieN.rafraichirBaseDeDonnees();
  }
  
  /**
   * Fermeture de la session.
   */
  @Override
  public void fermerEcran() {
    // On ferme toutes les boites de dialogue encore ouvertes (style tt_cours)
    for (int i = 0; i < listeoDialog.size(); i++) {
      listeoDialog.get(i).dispose();
      listeoDialog.remove(i);
    }
    // Permet de remettre le curseur normal
    panelPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    ClientSerieN.listeSession.removeObject(thisSessionPanel);
    
    // Destruction de la session client
    try {
      ManagerSessionClient.getInstance().detruireSessionClient(idSession);
    }
    catch (Exception e) {
      // Pas de message car lors de la destruction ce n'est pas important et cela perturbe l'utilisateur final
    }
  }
  
  /**
   * Affiche ou cache les popups liées à cette session.
   */
  @Override
  public void setVisibleDialog(boolean visible) {
    // On modifie la visibilité des popups
    for (int i = 0; i < listeoDialog.size(); i++) {
      listeoDialog.get(i).setVisible(visible);
    }
  }
  
  @Override
  public String getToolTipTxt(boolean selected) {
    return getPointMenu();
  }
  
  /**
   * Démarre la connexion de la session
   */
  @Override
  public void startConnexion() {
    // Attention l'ordre est important à cause de l'idSession
    initialiserSession();
    fluxRpg = new FluxRecord(idSession, Constantes.DATAQLONG, envProgramClient.getEnvUser());
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        receptionBuffer();
      }
    }, "Td:SessionPanelRPG-id=" + idSession).start();
  }
  
  @Override
  public void lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu) {
    if (pIdEnregistrementMneMnp == null) {
      throw new MessageErreurException("L'identifiant du point de menu à lancer est invalide.");
    }
    
    // Récupérer le point de menu
    MenuDetail pointMenu = ClientSerieN.modeleMenuGeneral.retournerPointDeMenu(pIdEnregistrementMneMnp.getNumero());
    
    // Lancer le point de menu
    if (pointMenu == null) {
      String message = "L'identifiant du point de menu à lancer ne correspond à aucun point de menu connu.";
      String menuPersonnalise = envProgramClient.getEnvUser().getMenuPersonnalise();
      // Personnalisation du message si le profil a un menu personnalisé
      if (!menuPersonnalise.isEmpty()) {
        message += "\nVotre profil utilise un menu personnalisé nommé \"" + menuPersonnalise
            + "\". Or ce menu personnalisé n'a pas accès au point de menu qui doit être lancé.";
        // Ajout du point de menu dans le message
        if (pParametreMenu != null) {
          message += "\nVeuillez ajouter le point de menu \"" + pParametreMenu.toString() + "\" à votre menu personnalisé.";
        }
      }
      throw new MessageErreurException(message);
    }
    ClientSerieN.modeleMenuGeneral.executerPointDeMenu(pointMenu, pParametreMenu);
  }
  
  // -- Méthodes protégées
  
  /**
   * Initialiser la session côté serveur.
   */
  protected void initialiserSession() {
    envProgramClient.setIdSession(idSession);
    
    String message = envProgramClient.getEnvUser().getId() + Constantes.SEPARATEUR_CHAINE_CHAR
        + envProgramClient.getEnvUser().getDemandeSession(envProgramClient.getDemandeSession());
    ManagerServiceSession.ouvrirSession(idSession, EnumTypeSession.SESSION_RPG, message);
  }
  
  /**
   * Evènement déclenché lorsque la session est montrée.
   */
  @Override
  protected void sessionAncestorAdded(AncestorEvent e) {
    etatActif = true;
    setVisibleDialog(etatActif);
    if (ecran != null) {
      ecran.requestFocus();
    }
    
    panelPanel.validate();
    panelPanel.repaint();
  }
  
  /**
   * Evènement déclenché lorsque la session est cachée.
   */
  @Override
  protected void sessionAncestorRemoved(AncestorEvent e) {
    etatActif = false;
    if (sessionIndependante != null) {
      return;
    }
    
    etatActif = false;
    // On cache les boites de dialogue s'il y en a
    setVisibleDialog(etatActif);
    
    panelPanel.validate();
    panelPanel.repaint();
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne le point de menu de la session.
   */
  private String getPointMenu() {
    if (envProgramClient == null) {
      return "";
    }
    return envProgramClient.getPointMenu();
  }
  
  private void recoitNotificationRPG(String message) {
    NotificationProgrammeRPG notif = new NotificationProgrammeRPG(getLexique(), message);
    notif.treatmentBuffer();
  }
  
  private void receptionBuffer() {
    String idmessage = "";
    String msg = ManagerServiceTechnique.dialoguerAvecProgramme(idSession, null, Constantes.DELAI_ATTENTE_DATAQ_PROGRAMME);
    while (msg != null) {
      idmessage = "";
      if (msg.length() > Constantes.LONGUEUR_CODE_RPGMSG) {
        idmessage = msg.substring(0, Constantes.LONGUEUR_CODE_RPGMSG);
      }
      if (idmessage.equals(Constantes.RPGMSG_BUFFER)) {
        traitementBufferRPG(msg);
      }
      else if (idmessage.equals(Constantes.RPGMSG_ERR)) {
        afficheErreurDuRPG(msg);
      }
      else if (idmessage.equals(Constantes.RPGMSG_NOTIFICATION)) {
        recoitNotificationRPG(msg);
      }
      else if (idmessage.equals(Constantes.RPGMSG_ENDJOB)) {
        arretRPGpropre = true;
        msg = null;
      }
      else if (idmessage.equals(Constantes.RPGMSG_JOBENDED)) {
        msg = null;
      }
      else if (idmessage.equals(Constantes.RPGMSG_DELAI_ATTENTE_EXPIRE)) {
        msg = "";
      }
      if (!arretencours) {
        msg = ManagerServiceTechnique.recevoirMessageDuProgramme(idSession, Constantes.DELAI_ATTENTE_DATAQ_PROGRAMME);
      }
    }
    stopperSessionCliente();
  }
  
  private void afficheErreurDuRPG(String message) {
    // Récupération des infos du fichier
    String[] infos = Constantes.splitString(message, Constantes.SEPARATEUR_CHAINE_CHAR);
    
    DialogErrorCPF dialogErrorNew = new DialogErrorCPF(clientSerieN, "Problème avec le programme (MSGW ou LCK) sur le serveur", infos);
    message = dialogErrorNew.getReponse();
    
    if (message == null) {
      fermerEcran();
    }
    else if (message.equals("")) {
      // Ne rien faire dans ce cas
    }
    else { // Erreur avec demande de réponse
      try {
        ManagerServiceTechnique.repondreMessageErreur(idSession, message);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    
  }
  
  private void stopperSessionCliente() {
    Trace.info("Demande de fermeture de la session client par le programme RPG.");
    if (!arretencours) {
      arretencours = true;
    }
    fermerEcran();
    
    if (arretRPGpropre) {
      Trace.debug(SessionRPG.class, "stopperSessionCliente", "Réception d'un message 99999 envoyé par le RPG.");
    }
    else {
      Trace.erreur(
          "Il n'y a pas eu de message 99999 envoyé par le RPG : " + "soit le programme RPG est mal fait car il n'envoi pas le 99999, "
              + "soit un 4+F4 immed dans WRKACTJOB, " + "soit une erreur lors du l'execution du programme RPG.");
    }
  }
  
  private void traitementBufferRPG(String buffer) {
    boolean isSamePanel = false;
    
    // Permet de lancer le sablier pendant les transactions
    panelPanel.setCursor(new Cursor(Cursor.WAIT_CURSOR));
    
    Trace.debug(SessionRPG.class, "traitementBufferRPG", "idSession", +getNumeroSession());
    Trace.debugMemoire(SessionRPG.class, "traitementBufferRPG");
    
    // Réception du buffer envoyé par le RPG
    if (fluxRpg.setRecord(buffer) == Constantes.ERREUR) {
      JOptionPane.showMessageDialog(null, fluxRpg.getMsgErreur(), "Programme en cours de construction...", JOptionPane.ERROR_MESSAGE);
      fermerEcran();
      return;
    }
    
    // Initialisation de la fenêtre débug
    envProgramClient.setDebugFluxRpg(fluxRpg, panelPanel.getName(), true);
    nomProgramme = fluxRpg.getNomPrg();
    
    // Chargement du panel si besoin depuis le fichier
    isSamePanel = (idPanelCourant == fluxRpg.getIdClassName());
    if (!isSamePanel) {
      idPanelCourant = fluxRpg.getIdClassName();
      ecranprec = ecran;
      
      // On recherche d'abord dans l'historique
      ecran = fluxRpg.getoFrame(idPanelCourant, envProgramClient);
      if (ecran != null) {
        ecran.noRefresh = false;
        ecran.setSession(thisSessionPanel);
      }
      else {
        // Permet de remettre le curseur normal
        panelPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        // On affiche un message d'erreur et on ferme la session
        JOptionPane.showMessageDialog(null, fluxRpg.getMsgErreur(), "Programme en cours de construction...", JOptionPane.ERROR_MESSAGE);
        fermerEcran();
        ecran = null;
        return;
        
      }
    }
    
    // On affiche la frame si on peut (commander depuis le RPG)
    if (fluxRpg.isPanelAffichage()) {
      
      // Initialisation des variables du panel (permet un rafraichissement normal grâce à l'invoker)
      try {
        if (ecranprec != null && ecran.isDialog()) {
          ecranprec.noRefresh = true;
          
          // SwingUtilities.invokeAndWait remit à cause du lock chez Schroeder sur VCGM11 L8/L5 (FM999, 002
          // 60 X 010213, 606320, 10 egfegfe 111111) mais soucis chez MJ poste de Stéphan sur la popup des bons clients
          SwingUtilities.invokeAndWait(new Runnable() {
            @Override
            public void run() {
              // Mise à jour des records de ecranprec avec ceux de FluxRecord
              fluxRpg.majRecords(ecranprec.listeRecord, ecranprec.getIdClassName());
              // Mise à jour des données du panel
              ((ioFrame) ecranprec).setData();
            }
          });
        }
        
        // Mise à jour des records de l'ecran avec ceux de FluxRecord (Ne peut pas être dans le setData de oFrame)
        fluxRpg.majRecords(ecran.listeRecord, ecran.getIdClassName());
        // Mise à jour des données du panel
        ((ioFrame) ecran).setData();
        // Traitement à effectuer lorsque le setData() est terminé (notamment la gestion du F1 automatique en cas d'erreur)
        ecran.traitementApresSetData(ecranprec);
        ecran.searchRequestComponent();
      }
      catch (Exception e) {
        DialogueErreur.afficher(e);
      }
      
      int i = 0;
      int lg = 0;
      // Si c'est un panel normal
      if (!ecran.isDialog()) {
        // Nettoyage de la liste des Popups si nécessaire
        while (listeoDialog.size() > 0) {
          listeoDialog.get(0).dispose();
          listeoDialog.remove(0);
        }
        
        SwingUtilities.invokeLater(new Runnable() {
          @Override
          public void run() {
            panelPanel.removeAll();
            JScrollPane scrollPane = new JScrollPane(ecran);
            scrollPane.setBorder(null);
            panelPanel.add(scrollPane);
            panelPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
            // Permet l'actualisation du panel sinon faut agrandir la fenêtre avec la souris
            panelPanel.revalidate();
            ecran.activeRequestComponent4Frame();
          }
        });
      }
      // Sinon c'est une boite de dialogue
      else {
        // Création de la fenêtre de la popup
        // On stocke les boites de dialogue si elles ont un WABLC à 2 car elles ne sont pas bloquantes
        // mais il faut les fermer manuellement (cf tt_cours)
        // Reste le probleme de la modalité si modal blocage buffer et sinon le mec peut cliquer partout
        // On détruit les boites de dialogue qui doivent l'être
        i = 0;
        while (i < listeoDialog.size()) {
          if (listeoDialog.get(i).getBlocageEcran() == FluxRecord.WRITETEMPO) {
            listeoDialog.get(i).dispose();
            listeoDialog.remove(i);
          }
          else {
            i++;
          }
        }
        // Création de la fenêtre de la popup si besoin
        isNewDialog = true;
        lg = listeoDialog.size();
        for (i = 0; i < lg; i++) {
          if (listeoDialog.get(i).getIdClassName() == fluxRpg.getIdClassName()) {
            dialogue = listeoDialog.get(i);
            dialogue.setBlocageEcran(fluxRpg.getBlocageEcran());
            dialogue.setEcran(ecran);
            isNewDialog = false;
            break;
          }
        }
        if (isNewDialog) {
          dialogue = new ODialog(fluxRpg.getIdClassName(), ecran, (JFrame) panelPanel.getTopLevelAncestor(), fluxRpg.getBlocageEcran());
          listeoDialog.add(dialogue);
        }
        
        // Permet de remettre le curseur normal pour la popup
        ecran.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        // Evite que la popup perso ne passe derrière si elle est lancée depuis le setdata - 18/03/2015
        // Il le faut sinon ça peut coincer dans certains cas
        if (!ecran.usePopupPerso) {
          EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
              dialogue.affiche();
            }
          });
        }
      }
      // Gestion du focus sur le panel actif et du bouton par défaut
      if (ecran.usePopupPerso) {
        ecran.oFramePopupPerso.setRenvoyable(true);
      }
      ecran.setRenvoyable(true);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la fenêtre cliente
   */
  public ClientSerieN getFenetre() {
    return clientSerieN;
  }
  
  /**
   * @return le sessionIndependante
   */
  public SessionIndependante getSessionIndependante() {
    return sessionIndependante;
  }
  
  /**
   * @param sessionIndependante le sessionIndependante à définir
   */
  public void setSessionIndependante(SessionIndependante sessionIndependante) {
    this.sessionIndependante = sessionIndependante;
  }
  
}
