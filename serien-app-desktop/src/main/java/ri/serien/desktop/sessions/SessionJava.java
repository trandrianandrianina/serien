/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.sessions;

import java.awt.Cursor;

import javax.swing.event.AncestorEvent;

import ri.serien.desktop.composant.SessionIndependante;
import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Session correspondant à un écran gérée par un programme Java.
 */
public class SessionJava extends SessionBase {
  // Variables
  protected ClientSerieN clientSerieN = null;
  private SessionIndependante sessionIndependante = null;
  
  /**
   * Constructeur.
   */
  public SessionJava(ClientSerieN pClientSerieN, Object pParametreMenu) {
    clientSerieN = pClientSerieN;
    setParametreMenu(pParametreMenu);
  }
  
  // -- Méthodes publiques
  
  /**
   * Ouvrir une session graphique (système des onglets).
   */
  public void ouvrirSession() {
    if (clientSerieN == null) {
      throw new MessageErreurException("La fenêtre n'est pas initialisée.");
    }
    
    // Fermeture du menu général
    if (ClientSerieN.getPreferencesUserManager().getPreferences().isFermerAutomatiquement()) {
      clientSerieN.cacherMenuGeneral();
    }
    
    // Création de la session graphiquement
    clientSerieN.ajouterSessionJava(this);
  }
  
  @Override
  public void startConnexion() {
  }
  
  @Override
  public void setVisibleDialog(boolean visible) {
  }
  
  /**
   * Affichage de l'infobulle au passage de la souris sur l'onglet de la session en cours.
   */
  @Override
  public String getToolTipTxt(boolean selected) {
    // Aucune infobulle n'est affichée.
    return null;
  }
  
  /**
   * Met à jour les informations de la base de données utilisateur.
   */
  @Override
  public void rafraichirBaseDeDonnees() {
    if (clientSerieN == null) {
      return;
    }
    clientSerieN.rafraichirBaseDeDonnees();
  }
  
  /**
   * Ferme la session en cours.
   */
  @Override
  public void fermerEcran() {
    new Thread(new Runnable() { // <- Obligatoire sinon le timer de fading ne fonctionne pas
      @Override
      public void run() {
        // Permet de remettre le curseur normal
        panelPanel.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        ClientSerieN.listeSession.removeObject(thisSessionPanel);
      }
    }, "id=" + idSession).start();
    
    // Destruction de la session client
    try {
      ManagerSessionClient.getInstance().detruireSessionClient(idSession);
    }
    catch (Exception e) {
      // Pas de message car lors de la destruction ce n'est pas important et cela perturbe l'utilisateur final
    }
  }
  
  /**
   * Lance un nouveau point de menu dans une autre session.
   */
  @Override
  public void lancerPointMenu(IdEnregistrementMneMnp pIdEnregistrementMneMnp, Object pParametreMenu) {
    if (pIdEnregistrementMneMnp == null) {
      throw new MessageErreurException("L'identifiant du point de menu à lancer est invalide.");
    }
    
    // Récupérer le point de menu
    MenuDetail pointMenu = ClientSerieN.modeleMenuGeneral.retournerPointDeMenu(pIdEnregistrementMneMnp.getNumero());
    
    // Lancer le point de menu
    if (pointMenu == null) {
      throw new MessageErreurException("L'identifiant du point de menu à lancer ne correspond à aucun point de menu connu.");
    }
    ClientSerieN.modeleMenuGeneral.executerPointDeMenu(pointMenu, pParametreMenu);
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    super.dispose();
  }
  
  // -- Méthodes protégées
  
  /**
   * Initialiser la session côté serveur.
   */
  protected boolean initialiserSession() {
    // Initialise la session côté serveur
    String message = ManagerSessionClient.getInstance().getEnvUser().getId() + Constantes.SEPARATEUR_CHAINE_CHAR
        + ManagerSessionClient.getInstance().getEnvUser().getDemandeSession(
            "|||0|3|" + ManagerSessionClient.getInstance().getEnvUser().getBibliothequeEnvironnementClient() + "|QGPL|QTEMP|");
    ManagerServiceSession.ouvrirSession(idSession, EnumTypeSession.SESSION_JAVA, message);
    return true;
  }
  
  /**
   * Evènement déclenché lorsque la session est montrée
   */
  @Override
  protected void sessionAncestorAdded(AncestorEvent e) {
    etatActif = true;
    setVisibleDialog(etatActif);
    if (panelPanel != null) {
      panelPanel.requestFocus();
    }
    
    panelPanel.validate();
    panelPanel.repaint();
  }
  
  /**
   * Evènement déclenché lorsque la session est cachée
   */
  @Override
  protected void sessionAncestorRemoved(AncestorEvent e) {
    etatActif = false;
    if (sessionIndependante != null) {
      return;
    }
    
    etatActif = false;
    
    // On cache les boites de dialogue s'il y en a
    setVisibleDialog(etatActif);
    
    panelPanel.validate();
    panelPanel.repaint();
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Retourne la fenêtre cliente
   */
  public ClientSerieN getFenetre() {
    return clientSerieN;
  }
  
  /**
   * @return le sessionIndependante
   */
  public SessionIndependante getSessionIndependante() {
    return sessionIndependante;
  }
  
  /**
   * @param sessionIndependante le sessionIndependante à définir
   */
  public void setSessionIndependante(SessionIndependante sessionIndependante) {
    this.sessionIndependante = sessionIndependante;
  }
}
