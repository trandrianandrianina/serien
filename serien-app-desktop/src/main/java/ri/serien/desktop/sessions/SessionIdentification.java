/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.sessions;

import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceSession;

/**
 * Session d'identification.
 */
public class SessionIdentification {
  // Variables
  private IdSession idSession = null;
  
  /**
   * Constructeur par défaut.
   */
  public SessionIdentification() {
    // Créer la session
    idSession = ManagerSessionClient.getInstance().creerSessionClient();
  }
  
  /**
   * Retourne l'id de la session.
   */
  public IdSession getIdSession() {
    return idSession;
  }
  
  public void demanderListeRT() {
    // Récupérer l'utiliasteur associé à la session
    EnvUser envUser = ManagerSessionClient.getInstance().getEnvUser();
    
    // Récupérer le sous-système pour avoir la liste des fichiers RunTimes
    Trace.info("Récupérer la liste des fichiers runtimes du serveur.");
    SousEnvironnement sousEnvironnementServeur =
        ManagerServiceSession.recupererSousEnvironnement(idSession, envUser.getIdSousEnvironnement());
    sousEnvironnementServeur.setHost(envUser.getHost());
    sousEnvironnementServeur.setPrefixeDevice(envUser.getPrefixDevice());
    sousEnvironnementServeur.setNomSousSysteme(envUser.getNomSousSysteme());
    sousEnvironnementServeur.setProgram(envUser.getProgram());
    sousEnvironnementServeur.setBibliothequeEnvironnementClient(envUser.getBibliothequeEnvironnementClient());
    sousEnvironnementServeur.setBibliothequeEnvironnement(envUser.getBibliothequeEnvironnement());
    sousEnvironnementServeur.setFolder(envUser.getFolder());
    sousEnvironnementServeur.setTitleBarInformations(envUser.isTitleBarInformations());
    sousEnvironnementServeur.setTypeMenu(envUser.getTypemenu());
    sousEnvironnementServeur.setControleProfilActif(false);
    
    // Lancer le balayage des fichiers runtimes sur le client pour le sous-système choisi
    Trace.info("Balayage des Runtimes sur le poste : " + envUser.getFolder() + " pour la config " + envUser.getIdSousEnvironnement());
    SousEnvironnement sousEnvironnementClient = new SousEnvironnement(envUser.getIdSousEnvironnement());
    sousEnvironnementClient.setHost(envUser.getHost());
    sousEnvironnementClient.setPrefixeDevice(envUser.getPrefixDevice());
    sousEnvironnementClient.setNomSousSysteme(envUser.getNomSousSysteme());
    sousEnvironnementClient.setProgram(envUser.getProgram());
    sousEnvironnementClient.setBibliothequeEnvironnementClient(envUser.getBibliothequeEnvironnementClient());
    sousEnvironnementClient.setBibliothequeEnvironnement(envUser.getBibliothequeEnvironnement());
    sousEnvironnementClient.setFolder(envUser.getFolder());
    sousEnvironnementClient.setTitleBarInformations(envUser.isTitleBarInformations());
    sousEnvironnementClient.setTypeMenu(envUser.getTypemenu());
    sousEnvironnementClient.setControleProfilActif(false);
    sousEnvironnementClient.setDossierRacine(envUser.getDossierDesktop().getNomDossierRacine());
    
    if (!sousEnvironnementClient.isPresentDossierRuntime()) {
      sousEnvironnementClient.creerDossierRuntime();
    }
    sousEnvironnementClient.balayerDossierRuntime();
    
    envUser.initTransfertSession(sousEnvironnementServeur, sousEnvironnementClient);
  }
  
  /**
   * Ferme la session identification.
   */
  public void fermerSession() {
    ManagerSessionClient.getInstance().detruireSessionClient(idSession);
  }
}
