/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueDetailLigneArticle extends AbstractVueDialogue<ModeleDetailLigneArticle> {
  private VueOngletGeneral ongletGeneral = null;
  private VueOngletNegociation ongletNegociation = null;
  private VueOngletInformationsTechniques ongletMemo = null;
  private VueOngletStock ongletStock = null;
  private VueOngletHistorique ongletHistorique = null;
  private VueOngletDimension ongletDimensions = null;
  private VueOngletLiens ongletLiens = null;
  
  /**
   * Constructeur.
   */
  public VueDetailLigneArticle(ModeleDetailLigneArticle pModele) {
    super(pModele);
    
    ongletGeneral = new VueOngletGeneral(getModele());
    ongletNegociation = new VueOngletNegociation(getModele());
    ongletMemo = new VueOngletInformationsTechniques(getModele());
    ongletStock = new VueOngletStock(getModele());
    ongletHistorique = new VueOngletHistorique(getModele());
    ongletDimensions = new VueOngletDimension(getModele());
    ongletLiens = new VueOngletLiens(getModele());
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    ongletGeneral.initialiserComposants();
    ongletNegociation.initialiserComposants();
    ongletMemo.initialiserComposants();
    ongletStock.initialiserComposants();
    ongletHistorique.initialiserComposants();
    ongletDimensions.initialiserComposants();
    ongletLiens.initialiserComposants();
    
    if (getModele().getArticle() == null) {
      throw new MessageErreurException("Aucun article de sélectionné");
    }
    
    setTitle("Détail de la ligne article " + getModele().getArticle().getId().toString());
    
    /**
     * Pourquoi pas de détail pour les palettes ?
     * else if (getModele().getArticle().isArticlePalette()) {
     * throw new MessageErreurException("Erreur lors de l'affichage du détail d'un article de type palette.");
     * }
     **/
    
    // Ajouter l'onglet informations générales
    pnlOnglets.addTab("Général", null, ongletGeneral, "Informations générales");
    if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_GENERAL) {
      pnlOnglets.setSelectedComponent(ongletGeneral);
    }
    
    // Ajouter l'onglet négociation
    if (getModele().getArticle() != null) {
      pnlOnglets.addTab("Négociations", null, ongletNegociation, "Négociations pour cette commande");
      if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_NEGOCIATION) {
        pnlOnglets.setSelectedComponent(ongletNegociation);
      }
    }
    
    // Ajouter l'onglet informations techniques si ce n'est pas un article transport
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleTransport()) {
      pnlOnglets.addTab("Infos techniques", null, ongletMemo, "Informations techniques pour cette commande");
      if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES) {
        pnlOnglets.setSelectedComponent(ongletMemo);
      }
    }
    
    // Ajouter l'onglet stock si ce n'est pas un article transport
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleTransport()) {
      pnlOnglets.addTab("Stocks", null, ongletStock, "Etat des stocks pour cet article");
      if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_STOCK) {
        pnlOnglets.setSelectedComponent(ongletStock);
      }
    }
    
    // Ajouter l'onglet historique si ce n'est pas un article transport
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleTransport()) {
      pnlOnglets.addTab("Historique", null, ongletHistorique, "Historique des commandes");
      if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_HISTORIQUE) {
        pnlOnglets.setSelectedComponent(ongletHistorique);
      }
    }
    
    // Ajouter l'onglet dimensions si c'est un article découpable si ce n'est pas un article transport
    if (getModele().getArticle() != null && getModele().getArticle().isArticleDecoupable()
        && !getModele().getArticle().isArticleTransport()) {
      pnlOnglets.addTab("Dimensions", null, ongletDimensions, "Dimensions de l'article");
      if (getModele().getOngleActif() == ModeleDetailLigneArticle.ONGLET_DIMENSIONS) {
        pnlOnglets.setSelectedComponent(ongletDimensions);
      }
    }
    
    // Ajouter l'onglet liens si ce n'est pas un article transport
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleTransport()) {
      pnlOnglets.addTab("Liens", null, ongletLiens, "Liens documents");
    }
  }
  
  @Override
  public void rafraichir() {
    switch (getModele().getOngleActif()) {
      case ModeleDetailLigneArticle.ONGLET_GENERAL:
        if (getPanelOnglets().indexOfComponent(ongletGeneral) != -1) {
          pnlOnglets.setSelectedComponent(ongletGeneral);
          ongletGeneral.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_NEGOCIATION:
        if (getPanelOnglets().indexOfComponent(ongletNegociation) != -1) {
          pnlOnglets.setSelectedComponent(ongletNegociation);
          ongletNegociation.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES:
        if (getPanelOnglets().indexOfComponent(ongletMemo) != -1) {
          pnlOnglets.setSelectedComponent(ongletMemo);
          ongletMemo.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_STOCK:
        if (getPanelOnglets().indexOfComponent(ongletStock) != -1) {
          pnlOnglets.setSelectedComponent(ongletStock);
          ongletStock.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_HISTORIQUE:
        if (getPanelOnglets().indexOfComponent(ongletHistorique) != -1) {
          pnlOnglets.setSelectedComponent(ongletHistorique);
          ongletHistorique.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_DIMENSIONS:
        if (getPanelOnglets().indexOfComponent(ongletDimensions) != -1) {
          pnlOnglets.setSelectedComponent(ongletDimensions);
          ongletDimensions.rafraichir();
        }
        break;
      case ModeleDetailLigneArticle.ONGLET_LIENS:
        if (getPanelOnglets().indexOfComponent(ongletLiens) != -1) {
          pnlOnglets.setSelectedComponent(ongletLiens);
          ongletLiens.rafraichir();
        }
        break;
      
    }
  }
  
  /**
   * Récupérer le code correspondant à l'onglet sélectionné. Cela retourne l'onglet général si aucun n'est sélectionné.
   */
  private int getCodeOngletSelectionne() {
    if (pnlOnglets.getSelectedComponent() == ongletGeneral) {
      return ModeleDetailLigneArticle.ONGLET_GENERAL;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletNegociation) {
      return ModeleDetailLigneArticle.ONGLET_NEGOCIATION;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletMemo) {
      return ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletStock) {
      return ModeleDetailLigneArticle.ONGLET_STOCK;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletHistorique) {
      return ModeleDetailLigneArticle.ONGLET_HISTORIQUE;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletDimensions) {
      return ModeleDetailLigneArticle.ONGLET_DIMENSIONS;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletLiens) {
      return ModeleDetailLigneArticle.ONGLET_LIENS;
    }
    return ModeleDetailLigneArticle.ONGLET_GENERAL;
  }
  
  // -- Méthodes interractives
  
  private void pnlOngletsStateChanged(ChangeEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierOngletActif(getCodeOngletSelectionne());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlOnglets = new JTabbedPane();
    
    // ======== this ========
    setTitle("D\u00e9tail de la ligne article");
    setBackground(new Color(239, 239, 222));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(1230, 600));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlOnglets ========
    {
      pnlOnglets.setFont(pnlOnglets.getFont().deriveFont(pnlOnglets.getFont().getSize() + 3f));
      pnlOnglets.setBackground(new Color(239, 239, 222));
      pnlOnglets.setPreferredSize(new Dimension(1230, 540));
      pnlOnglets.setMinimumSize(new Dimension(1230, 540));
      pnlOnglets.setMaximumSize(new Dimension(1230, 540));
      pnlOnglets.setOpaque(true);
      pnlOnglets.setName("pnlOnglets");
      pnlOnglets.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          pnlOngletsStateChanged(e);
        }
      });
    }
    contentPane.add(pnlOnglets, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JTabbedPane pnlOnglets;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
  public JTabbedPane getPanelOnglets() {
    return pnlOnglets;
  }
  
  public void setPanelOnglets(JTabbedPane panelOnglets) {
    this.pnlOnglets = panelOnglets;
  }
  
  public VueOngletDimension getOngletDimensions() {
    return ongletDimensions;
  }
  
  public void setOngletDimensions(VueOngletDimension ongletDimensions) {
    this.ongletDimensions = ongletDimensions;
  }
}
