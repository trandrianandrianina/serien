/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.retourarticle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.personnalisation.motifretour.MotifRetour;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue de retour d'un article.
 */
public class VueRetourArticle extends AbstractVueDialogue<ModeleRetourArticle> {
  // Constantes
  private static final String BOUTON_AFFICHER_HISTORIQUE = "Afficher historique";
  private static final String BOUTON_AFFICHER_DOCUMENT_COURS = "Documents en cours";
  private final static String[] TITRE_LISTE_DOCUMENTS_HT = new String[] { "", "Document", "Date", "Quantit\u00e9 livr\u00e9e",
      "Quantit\u00e9 retourn\u00e9e", "Quantit\u00e9 restante", "Prix unitaire HT" };
  private final static String[] TITRE_LISTE_DOCUMENTS_TTC = new String[] { "", "Document", "Date", "Quantit\u00e9 livr\u00e9e",
      "Quantit\u00e9 retourn\u00e9e", "Quantit\u00e9 restante", "Prix unitaire TTC" };
  private String[] titresColonnes = TITRE_LISTE_DOCUMENTS_HT;
  
  // Variables
  private String[][] donnees = null;
  private JTableRetourArticleBooleanRenderer celluleCaseACocher = new JTableRetourArticleBooleanRenderer();
  
  /**
   * Constructeur.
   */
  public VueRetourArticle(ModeleRetourArticle pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialiser les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // La table contenant la liste des articles
    scpListeArticles.getViewport().setBackground(Color.WHITE);
    final int[] dimension = new int[] { 50, 90, 80, 100, 100, 100 };
    final int[] justification =
        new int[] { NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE };
    
    if (getModele().isTTC()) {
      titresColonnes = TITRE_LISTE_DOCUMENTS_TTC;
    }
    DefaultTableModel modeleTable = new DefaultTableModel() {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
      
      Class<?>[] columnTypes =
          new Class<?>[] { Boolean.class, String.class, String.class, String.class, String.class, String.class, String.class };
      
      @Override
      public Class<?> getColumnClass(int column) {
        return columnTypes[column];
      }
      
      @Override
      public int getColumnCount() {
        return titresColonnes.length;
      }
      
      @Override
      public String getColumnName(int column) {
        return titresColonnes[column];
      }
      
      @Override
      public Object getValueAt(int row, int column) {
        return donnees[row][column];
      }
      
      @Override
      public void setValueAt(Object value, int row, int col) {
        if (col == 0) {
          if ((Boolean) value) {
            getModele().selectionnerDocument(row);
          }
          else {
            getModele().deselectionnerDocument(row);
          }
          fireTableCellUpdated(row, col);
        }
      }
    };
    tblListeArticles.personnaliserAspectCellules(new JTableRetourArticleStringRenderer(null), modeleTable, dimension, justification, 14);
    tblListeArticles.getColumnModel().getColumn(0).setCellRenderer(celluleCaseACocher);
    // La largeur de la première colonne est fixé ici dans le cas où la liste serait vide
    TableColumnModel cm = tblListeArticles.getColumnModel();
    cm.getColumn(0).setResizable(false);
    cm.getColumn(0).setMinWidth(50);
    cm.getColumn(0).setMaxWidth(50);
    // Permet de forcer la saisie d'une cellule si l'on clique sur un autre composant (le bouton valider par exemple)
    tblListeArticles.forcerSaisieCellule(true);
    
    // Sélection au clavier ou en dehors de la case à cocher
    tblListeArticles.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeArticlesValueChanged(e);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_HISTORIQUE, 'h', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENT_COURS, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraîchir l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirPanelArticleRetourne();
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirPrixRetour();
    rafraichirMotifRetour();
    
    rafraichirTitreListe();
    rafraichirListe();
    
    rafraichirBoutonValider();
    rafraichirBoutonHistorique();
    rafraichirBoutonDocumentCours();
    
    // Positionnement du focus
    tblListeArticles.requestFocusInWindow();
  }
  
  private void rafraichirTitre() {
    String titre = "Retour d'articles";
    if (getModele().isAffichageEnCours()) {
      titre = titre + " - Documents en cours";
    }
    else if (getModele().isAffichageHistorique()) {
      titre = titre + " - Historique";
    }
    
    setTitle(titre);
  }
  
  private void rafraichirPanelArticleRetourne() {
    String titre = getModele().getLibellePanelArticle();
    if (titre != null) {
      TitledBorder pnlArticleRetourneBorder = new TitledBorder(getModele().getLibellePanelArticle());
      pnlArticleRetourneBorder.setTitleFont(new Font("sansserif", Font.BOLD, 14));
      pnlArticleRetourne.setBorder(pnlArticleRetourneBorder);
    }
    else {
      TitledBorder pnlArticleRetourneBorder = new TitledBorder("Retour d'un article");
      pnlArticleRetourneBorder.setTitleFont(new Font("sansserif", Font.BOLD, 14));
      pnlArticleRetourne.setBorder(pnlArticleRetourneBorder);
    }
  }
  
  private void rafraichirCodeArticle() {
    Article article = getModele().getArticle();
    if (article != null && article.getId() != null && article.getId().getCodeArticle() != null) {
      tfCodeArticle.setText(article.getId().getCodeArticle());
    }
    else {
      tfCodeArticle.setText("");
    }
  }
  
  private void rafraichirLibelleArticle() {
    Article article = getModele().getArticle();
    if (article != null && article.getLibelleComplet() != null) {
      tfLibelleArticle.setText(article.getLibelleComplet());
    }
    else {
      tfLibelleArticle.setText("");
    }
  }
  
  private void rafraichirPrixRetour() {
    BigDecimal prixRetour = getModele().getPrixRetourPersonnalise();
    if (prixRetour != null) {
      tfPrixRetour.setText(Constantes.formater(prixRetour, true));
    }
    else {
      tfPrixRetour.setText("");
    }
  }
  
  /**
   * Rafraîchir la liste des motifs de retour.
   */
  private void rafraichirMotifRetour() {
    cbMotifRetour.removeAllItems();
    if (getModele().getListeMotifs() == null) {
      return;
    }
    // Chargement de la boite à choix
    for (MotifRetour motif : getModele().getListeMotifs()) {
      if (motif.isRetour()) {
        cbMotifRetour.addItem(motif);
      }
    }
    
    cbMotifRetour.setSelectedItem(getModele().getMotifRetour());
  }
  
  /**
   * Rafraîchir le titre de la liste des lignes, en particulier en indiquant les dates de recherche.
   */
  private void rafraichirTitreListe() {
    String titre = "";
    // Construction du titre
    if (getModele().getListelignes() == null || getModele().getListelignes().isEmpty()) {
      titre = "Aucun document n'a été trouvé";
    }
    else if (getModele().getListelignes().size() == 1) {
      titre = "Issus du document";
    }
    else {
      titre = "Issus des documents";
    }
    
    // Ajout de la plage de la date si elle est renseignée
    if (getModele().getDateDebut() != null) {
      titre = titre + " (du " + DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, getModele().getDateDebut()) + " au "
          + DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, getModele().getDateFin()) + ")";
    }
    
    pnlListeArticles.setTitre(titre.toString());
  }
  
  /**
   * Rafraîchir la liste.
   */
  private void rafraichirListe() {
    ListeLigneVente listeLigneVente = getModele().getListelignes();
    if (listeLigneVente == null || listeLigneVente.isEmpty()) {
      return;
    }
    
    // Préparer les données pour le tableau
    donnees = new String[listeLigneVente.size()][tblListeArticles.getColumnCount()];
    
    for (int ligne = 0; ligne < listeLigneVente.size(); ligne++) {
      // Les données
      listeLigneVente.get(ligne).setClientTTC(getModele().isTTC());
      
      // On ne peut pas sélectionner les lignes dont la quantité est inférieure à la quantité à retourner (on grise la case)
      if (getModele().getQuantiteRetournee() != null && listeLigneVente.get(ligne).getQuantiteUCV() != null
          && listeLigneVente.get(ligne).getQuantiteUCV().compareTo(getModele().getQuantiteRetournee().abs()) < 0) {
        donnees[ligne][0] = "DESABLE";
      }
      else if (getModele().getIndiceLigneSelectionnee() == ligne) {
        donnees[ligne][0] = "TRUE";
      }
      else {
        donnees[ligne][0] = "FALSE";
      }
      
      donnees[ligne][1] = listeLigneVente.get(ligne).getIdDocumentVente().toString();
      donnees[ligne][2] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, listeLigneVente.get(ligne).getDateLivraisonPrevue());
      donnees[ligne][3] = getModele().retournerQuantiteOrigine(listeLigneVente.get(ligne));
      donnees[ligne][4] = Constantes.formater(listeLigneVente.get(ligne).getQuantiteDejaRetounee(), false);
      donnees[ligne][5] = getModele().retournerQuantiteRestante(listeLigneVente.get(ligne));
      // Affichage du prix net saisi
      donnees[ligne][6] = Constantes.formater(listeLigneVente.get(ligne).getPrixNet(), true);
    }
    
    celluleCaseACocher.setDonnees(donnees);
    tblListeArticles.mettreAJourDonnees(donnees);
  }
  
  /**
   * Gestion activation des boutons
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isValidationPossible();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Gestion de l'activation du bouton historique
   */
  private void rafraichirBoutonHistorique() {
    boolean actif = getModele().isAffichageEnCours();
    // Si la liste des historiques est vide, le bouton "Afficher historique" reste affiché
    if (getModele().isHistoriqueVide()) {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_HISTORIQUE, true);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_HISTORIQUE, actif);
    }
  }
  
  /**
   * Gestion de l'activaion du bouton document en cours
   */
  private void rafraichirBoutonDocumentCours() {
    boolean actif = getModele().isAffichageHistorique();
    // Si la liste des historique est vide le bouton n'est pas affiché
    if (getModele().isHistoriqueVide()) {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT_COURS, false);
    }
    else {
      snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT_COURS, actif);
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_HISTORIQUE)) {
        getModele().chargerHistorique();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENT_COURS)) {
        getModele().chargerDocumentEnCours();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMotifRetourItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMotif((MotifRetour) cbMotifRetour.getSelectedItem());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPrixRetourFocusLost(FocusEvent e) {
    try {
      getModele().modifierPrixRetour(tfPrixRetour.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeArticlesMouseClicked(MouseEvent e) {
    try {
      if (tblListeArticles.getSelectedRowCount() > 0) {
        int indexvisuel = tblListeArticles.getSelectedRow();
        if (indexvisuel >= 0) {
          int indexreel = indexvisuel;
          if (tblListeArticles.getRowSorter() != null) {
            indexreel = tblListeArticles.getRowSorter().convertRowIndexToModel(indexvisuel);
          }
          getModele().selectionnerDocument(indexreel);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Récupère le document sélectionné.
   */
  private void validerListeSelection() {
    int indexvisuel = tblListeArticles.getSelectedRow();
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeArticles.getRowSorter() != null) {
        indexreel = tblListeArticles.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().selectionnerDocument(indexreel);
      getModele().modifierPrixRetour(tfPrixRetour.getText());
    }
  }
  
  /**
   * Selection dans la liste autre qu'avec la souris
   */
  private void tblListeArticlesValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs() && !e.getValueIsAdjusting()) {
        return;
      }
      validerListeSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    pnlArticleRetourne = new SNPanelTitre();
    lbCodeArticle = new SNLabelChamp();
    tfCodeArticle = new SNTexte();
    tfLibelleArticle = new RiTextArea();
    lbLibelleArticle = new SNLabelChamp();
    pnlListeArticles = new SNPanelTitre();
    scpListeArticles = new JScrollPane();
    tblListeArticles = new NRiTable();
    pnlConditionsRetour = new SNPanelTitre();
    lbMotifRetour = new SNLabelChamp();
    cbMotifRetour = new SNComboBox();
    lbPrixRetour = new SNLabelChamp();
    tfPrixRetour = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(900, 610));
    setForeground(Color.black);
    setTitle("Retour d'articles");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setPreferredSize(new Dimension(20, 290));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 123, 0, 93, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
        
        // ======== pnlArticleRetourne ========
        {
          pnlArticleRetourne.setOpaque(false);
          pnlArticleRetourne.setTitre("Retour d'articles");
          pnlArticleRetourne.setName("pnlArticleRetourne");
          pnlArticleRetourne.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlArticleRetourne.getLayout()).columnWidths = new int[] { 0, 150, 0 };
          ((GridBagLayout) pnlArticleRetourne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlArticleRetourne.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlArticleRetourne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCodeArticle ----
          lbCodeArticle.setText("Code article");
          lbCodeArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbCodeArticle.setPreferredSize(new Dimension(100, 30));
          lbCodeArticle.setName("lbCodeArticle");
          pnlArticleRetourne.add(lbCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfCodeArticle ----
          tfCodeArticle.setEditable(false);
          tfCodeArticle.setPreferredSize(new Dimension(160, 35));
          tfCodeArticle.setMinimumSize(new Dimension(160, 35));
          tfCodeArticle.setEnabled(false);
          tfCodeArticle.setName("tfCodeArticle");
          pnlArticleRetourne.add(tfCodeArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfLibelleArticle ----
          tfLibelleArticle.setBackground(Color.white);
          tfLibelleArticle.setLineWrap(true);
          tfLibelleArticle.setWrapStyleWord(true);
          tfLibelleArticle.setMinimumSize(new Dimension(620, 45));
          tfLibelleArticle.setPreferredSize(new Dimension(620, 45));
          tfLibelleArticle.setEditable(false);
          tfLibelleArticle.setEnabled(false);
          tfLibelleArticle.setFont(tfLibelleArticle.getFont().deriveFont(tfLibelleArticle.getFont().getSize() + 3f));
          tfLibelleArticle.setName("tfLibelleArticle");
          pnlArticleRetourne.add(tfLibelleArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- lbLibelleArticle ----
          lbLibelleArticle.setText("Libell\u00e9 article");
          lbLibelleArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLibelleArticle.setPreferredSize(new Dimension(100, 30));
          lbLibelleArticle.setName("lbLibelleArticle");
          pnlArticleRetourne.add(lbLibelleArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlArticleRetourne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlListeArticles ========
        {
          pnlListeArticles.setTitre("Issus du document");
          pnlListeArticles.setName("pnlListeArticles");
          pnlListeArticles.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlListeArticles.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlListeArticles.getLayout()).rowHeights = new int[] { 249, 0 };
          ((GridBagLayout) pnlListeArticles.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlListeArticles.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scpListeArticles ========
          {
            scpListeArticles.setPreferredSize(new Dimension(1050, 424));
            scpListeArticles.setName("scpListeArticles");
            
            // ---- tblListeArticles ----
            tblListeArticles.setShowVerticalLines(true);
            tblListeArticles.setShowHorizontalLines(true);
            tblListeArticles.setBackground(Color.white);
            tblListeArticles.setRowHeight(20);
            tblListeArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblListeArticles.setSelectionBackground(new Color(57, 105, 138));
            tblListeArticles.setGridColor(new Color(204, 204, 204));
            tblListeArticles.setName("tblListeArticles");
            tblListeArticles.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblListeArticlesMouseClicked(e);
              }
            });
            scpListeArticles.setViewportView(tblListeArticles);
          }
          pnlListeArticles.add(scpListeArticles, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlListeArticles, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlConditionsRetour ========
        {
          pnlConditionsRetour.setOpaque(false);
          pnlConditionsRetour.setTitre("Conditions de retour");
          pnlConditionsRetour.setName("pnlConditionsRetour");
          pnlConditionsRetour.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConditionsRetour.getLayout()).columnWidths = new int[] { 0, 0, 0, 145, 0 };
          ((GridBagLayout) pnlConditionsRetour.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlConditionsRetour.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlConditionsRetour.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbMotifRetour ----
          lbMotifRetour.setText("Motif de retour");
          lbMotifRetour.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbMotifRetour.setPreferredSize(new Dimension(100, 30));
          lbMotifRetour.setName("lbMotifRetour");
          pnlConditionsRetour.add(lbMotifRetour, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbMotifRetour ----
          cbMotifRetour.setMinimumSize(new Dimension(380, 35));
          cbMotifRetour.setPreferredSize(new Dimension(380, 35));
          cbMotifRetour.setName("cbMotifRetour");
          cbMotifRetour.addItemListener(e -> cbMotifRetourItemStateChanged(e));
          pnlConditionsRetour.add(cbMotifRetour, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixRetour ----
          lbPrixRetour.setText("Prix unitaire de retour");
          lbPrixRetour.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbPrixRetour.setName("lbPrixRetour");
          pnlConditionsRetour.add(lbPrixRetour, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPrixRetour ----
          tfPrixRetour.setBackground(Color.white);
          tfPrixRetour.setMinimumSize(new Dimension(110, 35));
          tfPrixRetour.setPreferredSize(new Dimension(110, 35));
          tfPrixRetour.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixRetour.setName("tfPrixRetour");
          tfPrixRetour.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPrixRetourFocusLost(e);
            }
          });
          pnlConditionsRetour.add(tfPrixRetour, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlConditionsRetour, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 10, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlArticleRetourne;
  private SNLabelChamp lbCodeArticle;
  private SNTexte tfCodeArticle;
  private RiTextArea tfLibelleArticle;
  private SNLabelChamp lbLibelleArticle;
  private SNPanelTitre pnlListeArticles;
  private JScrollPane scpListeArticles;
  private NRiTable tblListeArticles;
  private SNPanelTitre pnlConditionsRetour;
  private SNLabelChamp lbMotifRetour;
  private SNComboBox cbMotifRetour;
  private SNLabelChamp lbPrixRetour;
  private SNTexte tfPrixRetour;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
