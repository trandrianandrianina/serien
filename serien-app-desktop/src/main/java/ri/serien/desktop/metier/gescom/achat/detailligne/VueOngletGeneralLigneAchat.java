/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet général de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletGeneralLigneAchat extends JPanel implements InterfaceVueOnglet {
  // Variables
  private ModeleDetailLigneAchat modele = null;
  
  public VueOngletGeneralLigneAchat(ModeleDetailLigneAchat pModele) {
    modele = pModele;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    tfLibelleArticle.setLongueurMaximumTexte(120);
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    tfQuantiteCommandeUCachat.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    tfQuantiteCommandeUCachat.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Positionnement du focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleDetailLigneAchat.FOCUS_QUANTITE:
        tfQuantiteCommandeUCachat.requestFocusInWindow();
    }
  }
  
  @Override
  public void rafraichir() {
    // Rafraichir les données générales
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirUniteCA();
    rafraichirQuantiteCommandeUCA();
    rafraichirQuantiteUA();
    rafraichirMessageUCS();
    
    //
    // Rafraichir les éléments de tarif
    rafraichirPrixBrutHT();
    rafraichirPrixNetHT();
    rafraichirPrixPort();
    rafraichirPrixRevientFournisseur();
    rafraichirMontantHT();
    
    // Rafraichir le récapitulatif des quantités
    rafraichirPanelRecapitulatifQuantites();
    rafraichirQuantiteInitiale();
    rafraichirQuantiteReliquat();
    rafraichirQuantiteTraitee();
    rafraichirMontantInitial();
    rafraichirMontantReliquat();
    rafraichirMontantTraite();
    
    // Rafraichir le stock
    rafraichirPanelStock();
    rafraichirPanelApprovisionnementStock();
    rafraichirStockPhysique();
    rafraichirStockAttendu();
    rafraichirStockCommande();
    rafraichirStockDisponible();
    rafraichirStockConso();
    rafraichirStockDelai();
    rafraichirStockMini();
    rafraichirStockIdeal();
  }
  
  private void rafraichirCodeArticle() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getIdArticle() != null && ligneAchat.getIdArticle().getCodeArticle() != null) {
      tfCodeArticle.setText(ligneAchat.getIdArticle().getCodeArticle());
    }
    else {
      tfCodeArticle.setText("");
    }
  }
  
  private void rafraichirLibelleArticle() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getLibelleArticle() != null) {
      tfLibelleArticle.setText(ligneAchat.getLibelleArticle());
    }
    else {
      tfLibelleArticle.setText("");
    }
  }
  
  private void rafraichirUniteCA() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getIdUCA() != null) {
      tfUniteCA.setText(ligneAchat.getPrixAchat().getIdUCA().getCode());
    }
    else {
      tfUniteCA.setText("");
    }
  }
  
  private void rafraichirMessageUCS() {
    lbUCAparUCS.setText(modele.getMessageUCS());
    lbUCAparUCS.setVisible(true);
    
  }
  
  private void rafraichirQuantiteCommandeUCA() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getQuantiteReliquatUCA() != null) {
      tfQuantiteCommandeUCachat.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false));
    }
    else {
      tfQuantiteCommandeUCachat.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfQuantiteCommandeUCachat.setEnabled(true);
    }
    else {
      tfQuantiteCommandeUCachat.setEnabled(false);
    }
  }
  
  private void rafraichirQuantiteUA() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    Article article = modele.getArticle();
    if (ligneAchat != null && article != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getIdUA() != null
        && !ligneAchat.getPrixAchat().getIdUCA().equals(ligneAchat.getPrixAchat().getIdUA())) {
      BigDecimal nbrUAparUCA = ligneAchat.getPrixAchat().getNombreUAParUCA();
      if (nbrUAparUCA == null || nbrUAparUCA.equals(BigDecimal.ZERO)) {
        nbrUAparUCA = BigDecimal.ONE;
      }
      BigDecimal conditionnement = BigDecimal.ONE;
      if (ligneAchat.getPrixAchat().getQuantiteInitialeUCA() != null
          && ligneAchat.getPrixAchat().getQuantiteInitialeUCA().compareTo(BigDecimal.ZERO) > 0) {
        conditionnement = nbrUAparUCA;
      }
      lbUAparUCA.setText(String.format("Conditionnement %s %s/%s soit en UA", Constantes.formater(conditionnement, false),
          ligneAchat.getPrixAchat().getIdUA().getCode(), ligneAchat.getPrixAchat().getIdUCA().getCode()));
      tfQuantiteCommandeeUA.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUA(), false));
      
      if (ligneAchat.getPrixAchat().getIdUA() != null) {
        tfUniteA.setText(ligneAchat.getPrixAchat().getIdUA().getCode());
      }
      else {
        tfUniteA.setText("");
      }
      
      tfQuantiteCommandeeUA.setVisible(true);
      tfUniteA.setVisible(true);
      
      if (modele.isDocumentModifiable()) {
        tfQuantiteCommandeeUA.setEnabled(true);
      }
      else {
        tfQuantiteCommandeeUA.setEnabled(false);
      }
    }
    else {
      lbUAparUCA.setText("");
      lbUAparUCA.setVisible(false);
      tfQuantiteCommandeeUA.setVisible(false);
      tfUniteA.setVisible(false);
    }
  }
  
  private void rafraichirPrixBrutHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null && prixAchat.getPrixAchatBrutHT() != null) {
      tfPrixAchatBrut.setText(Constantes.formater(prixAchat.getPrixAchatBrutHT(), true));
    }
    else {
      tfPrixAchatBrut.setText("");
    }
  }
  
  private void rafraichirPrixNetHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null && prixAchat.getPrixAchatNetHT() != null) {
      tfPrixAchatNet.setText(Constantes.formater(prixAchat.getPrixAchatNetHT(), true));
    }
    else {
      tfPrixAchatNet.setText("");
    }
  }
  
  private void rafraichirPrixPort() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null && prixAchat.getMontantPortHT() != null) {
      tfPrixPort.setText(Constantes.formater(prixAchat.getMontantPortHT(), true));
    }
    else {
      tfPrixPort.setText("");
    }
  }
  
  private void rafraichirPrixRevientFournisseur() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getPrixRevientFournisseurHT() != null) {
      tfPrixDeRevientFournisseur.setText(Constantes.formater(ligneAchat.getPrixAchat().getPrixRevientFournisseurHT(), true));
    }
    else {
      tfPrixDeRevientFournisseur.setText("");
    }
  }
  
  private void rafraichirMontantHT() {
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getMontantReliquatHT() != null) {
      tfMontantTotal.setText(Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true));
    }
    else {
      tfMontantTotal.setText("");
    }
  }
  
  private void rafraichirPanelRecapitulatifQuantites() {
    boolean actif = true;
    // Le panel n'est pas affiché:
    // Si le document est en cours de création
    if (modele.isDocumentEnCoursCreation()) {
      actif = false;
    }
    // Si la boite de dialogue est affichée à partir de la recherche article
    else if (modele.isAppelDepuisRechercheArticle()) {
      actif = false;
    }
    
    pnlQuantitesUCA.setVisible(actif);
  }
  
  private void rafraichirQuantiteInitiale() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getQuantiteInitialeUCA() != null) {
      // Tant que la quantité traitée vaut 0, la quantité initiale est renseignée avec la quantité en reliquat
      if (ligneAchat.getPrixAchat().getMontantTraiteHT().compareTo(BigDecimal.ZERO) == 0) {
        tfGeneralQuantiteInitiale.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false));
      }
      // Sinon la quantité initiale est renseignée avec la quantité initiale
      else {
        tfGeneralQuantiteInitiale.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false));
      }
    }
    else {
      tfGeneralQuantiteInitiale.setText("");
    }
  }
  
  private void rafraichirQuantiteReliquat() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getQuantiteReliquatUCA() != null) {
      tfGeneralQuantiteReliquat.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false));
    }
    else {
      tfGeneralQuantiteReliquat.setText("");
    }
  }
  
  private void rafraichirQuantiteTraitee() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getQuantiteTraiteeUCA() != null) {
      tfGeneralQuantiteTraitee.setText(Constantes.formater(ligneAchat.getPrixAchat().getQuantiteTraiteeUCA(), false));
    }
    else {
      tfGeneralQuantiteTraitee.setText("");
    }
  }
  
  private void rafraichirMontantInitial() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getMontantInitialHT() != null) {
      // Tant que le montant traité vaut 0, le montant initial est renseigné avec le montant reliquat
      if (ligneAchat.getPrixAchat().getMontantTraiteHT().compareTo(BigDecimal.ZERO) == 0) {
        tfGeneralMontantInitialHT.setText(Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true));
      }
      // Sinon le montant initial est renseigné avec le montant initial
      else {
        tfGeneralMontantInitialHT.setText(Constantes.formater(ligneAchat.getPrixAchat().getMontantInitialHT(), true));
      }
    }
    else {
      tfGeneralMontantInitialHT.setText("");
    }
  }
  
  private void rafraichirMontantReliquat() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getMontantReliquatHT() != null) {
      tfGeneralMontantReliquatHT.setText(Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true));
    }
    else {
      tfGeneralMontantReliquatHT.setText("");
    }
  }
  
  private void rafraichirMontantTraite() {
    if (!pnlQuantitesUCA.isVisible()) {
      return;
    }
    
    LigneAchatArticle ligneAchat = (LigneAchatArticle) modele.getLigneAchat();
    if (ligneAchat != null && ligneAchat.getPrixAchat() != null && ligneAchat.getPrixAchat().getMontantTraiteHT() != null) {
      tfGeneralMontantTraiteHT.setText(Constantes.formater(ligneAchat.getPrixAchat().getMontantTraiteHT(), true));
    }
    else {
      tfGeneralMontantTraiteHT.setText("");
    }
  }
  
  private void rafraichirPanelStock() {
    Article article = modele.getArticle();
    if (article == null || article.isArticleSpecial()) {
      pnlStocksGeneral.setVisible(false);
    }
  }
  
  private void rafraichirPanelApprovisionnementStock() {
    Article article = modele.getArticle();
    if (article == null || article.isArticleSpecial()) {
      pnlApprovisionnementStocks.setVisible(false);
    }
  }
  
  private void rafraichirStockPhysique() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockPhysique.setText(Constantes.formater(listeStock.getQuantitePhysiqueTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantitePhysiqueTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockPhysique.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockPhysique.setText("0");
    }
  }
  
  private void rafraichirStockCommande() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockCommande.setText(Constantes.formater(listeStock.getQuantiteCommandeeTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteCommandeeTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockCommande.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockCommande.setText("0");
    }
  }
  
  private void rafraichirStockAttendu() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockAttendu.setText(Constantes.formater(listeStock.getQuantiteAttendueTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteAttendueTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockAttendu.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockAttendu.setText("0");
    }
  }
  
  private void rafraichirStockDisponible() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockDisponible.setText(Constantes.formater(listeStock.getQuantiteDisponibleAchatTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteDisponibleAchatTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockDisponible.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockDisponible.setText("0");
    }
  }
  
  private void rafraichirStockConso() {
    Article article = modele.getArticle();
    if (article != null && article.getConsommationMoyenneUCA() != null) {
      tfGeneralStockConso.setText(Constantes.formater(article.getConsommationMoyenneUCA(), false));
    }
    else {
      tfGeneralStockConso.setText("0");
    }
  }
  
  private void rafraichirStockDelai() {
    tfGeneralStockDelai.setText(Constantes.convertirIntegerEnTexte(modele.getDelaiApprovisionnement(), 0));
  }
  
  private void rafraichirStockMini() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockMini.setText(Constantes.formater(listeStock.getQuantiteMinimaleTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteMinimaleTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockMini.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockMini.setText("0");
    }
  }
  
  private void rafraichirStockIdeal() {
    ListeStockComptoir listeStock = modele.getListeStock();
    if (listeStock != null) {
      tfGeneralStockIdeal.setText(Constantes.formater(listeStock.getQuantiteIdealeTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteIdealeTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockIdeal.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockIdeal.setText("0");
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeUCFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUCA(tfQuantiteCommandeUCachat.getText());
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeeUVFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUA(tfQuantiteCommandeeUA.getText());
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlGeneral = new JPanel();
    panel1 = new JPanel();
    pnlInfosTarifaires = new JPanel();
    lbPrixAchatNet = new JLabel();
    tfPrixAchatNet = new XRiTextField();
    lbPrixPort = new JLabel();
    tfPrixPort = new XRiTextField();
    lbPrixDeRevientStandard = new JLabel();
    tfPrixDeRevientFournisseur = new XRiTextField();
    lbPrixAchatNet2 = new JLabel();
    tfPrixAchatBrut = new XRiTextField();
    pnlMontant = new JPanel();
    lbVide = new JLabel();
    tfMontantTotal = new XRiTextField();
    panel3 = new JPanel();
    lbCodeArticle = new JLabel();
    tfCodeArticle = new XRiTextField();
    lbLibelleArticle = new JLabel();
    tfLibelleArticle = new RiTextArea();
    lbQuantiteUCA = new JLabel();
    tfQuantiteCommandeUCachat = new XRiTextField();
    tfUniteCA = new XRiTextField();
    lbUAparUCA = new JLabel();
    tfQuantiteCommandeeUA = new XRiTextField();
    tfUniteA = new XRiTextField();
    lbUCAparUCS = new JLabel();
    panel2 = new JPanel();
    pnlQuantitesUCA = new JPanel();
    lbGeneralQuantiteInitiale = new JLabel();
    tfGeneralQuantiteInitiale = new XRiTextField();
    lbGeneralMontantInitialHT = new JLabel();
    tfGeneralMontantInitialHT = new XRiTextField();
    lbGeneralQuantiteReliquat = new JLabel();
    tfGeneralQuantiteReliquat = new XRiTextField();
    lbGeneralMontantReliquatHT = new JLabel();
    tfGeneralMontantReliquatHT = new XRiTextField();
    lbGeneralQuantiteTraitee = new JLabel();
    tfGeneralQuantiteTraitee = new XRiTextField();
    lbGeneralMontantTraiteHT = new JLabel();
    tfGeneralMontantTraiteHT = new XRiTextField();
    pnlStocksGeneral = new JPanel();
    lbStockPhysique = new JLabel();
    tfGeneralStockPhysique = new XRiTextField();
    lbStockCommande = new JLabel();
    tfGeneralStockCommande = new XRiTextField();
    lbStockAttendu = new JLabel();
    tfGeneralStockAttendu = new XRiTextField();
    lbStockDisponible = new JLabel();
    tfGeneralStockDisponible = new XRiTextField();
    pnlApprovisionnementStocks = new JPanel();
    lbStockConso = new JLabel();
    tfGeneralStockConso = new XRiTextField();
    lbStockDelai = new JLabel();
    tfGeneralStockDelai = new XRiTextField();
    lbStockMini = new JLabel();
    lbStockIdeal = new JLabel();
    tfGeneralStockMini = new XRiTextField();
    tfGeneralStockIdeal = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlGeneral ========
    {
      pnlGeneral.setOpaque(false);
      pnlGeneral.setBackground(new Color(238, 238, 210));
      pnlGeneral.setName("pnlGeneral");
      pnlGeneral.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 183, 0, 0, 0 };
      ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setMinimumSize(new Dimension(900, 100));
        panel1.setPreferredSize(new Dimension(900, 100));
        panel1.setName("panel1");
        panel1.setLayout(new GridBagLayout());
        ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlInfosTarifaires ========
        {
          pnlInfosTarifaires.setBorder(new TitledBorder("Informations tarifaires"));
          pnlInfosTarifaires.setOpaque(false);
          pnlInfosTarifaires.setPreferredSize(new Dimension(640, 100));
          pnlInfosTarifaires.setMinimumSize(new Dimension(640, 100));
          pnlInfosTarifaires.setName("pnlInfosTarifaires");
          pnlInfosTarifaires.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPrixAchatNet ----
          lbPrixAchatNet.setText("Prix d'achat net HT");
          lbPrixAchatNet.setFont(lbPrixAchatNet.getFont().deriveFont(lbPrixAchatNet.getFont().getSize() + 2f));
          lbPrixAchatNet.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixAchatNet.setMinimumSize(new Dimension(150, 25));
          lbPrixAchatNet.setMaximumSize(new Dimension(150, 25));
          lbPrixAchatNet.setPreferredSize(new Dimension(150, 25));
          lbPrixAchatNet.setName("lbPrixAchatNet");
          pnlInfosTarifaires.add(lbPrixAchatNet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixAchatNet ----
          tfPrixAchatNet.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixAchatNet.setFont(tfPrixAchatNet.getFont().deriveFont(tfPrixAchatNet.getFont().getSize() + 2f));
          tfPrixAchatNet.setPreferredSize(new Dimension(90, 30));
          tfPrixAchatNet.setMinimumSize(new Dimension(90, 30));
          tfPrixAchatNet.setEnabled(false);
          tfPrixAchatNet.setName("tfPrixAchatNet");
          pnlInfosTarifaires.add(tfPrixAchatNet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixPort ----
          lbPrixPort.setText("Port HT");
          lbPrixPort.setFont(lbPrixPort.getFont().deriveFont(lbPrixPort.getFont().getSize() + 2f));
          lbPrixPort.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixPort.setMinimumSize(new Dimension(50, 25));
          lbPrixPort.setMaximumSize(new Dimension(50, 25));
          lbPrixPort.setPreferredSize(new Dimension(50, 25));
          lbPrixPort.setName("lbPrixPort");
          pnlInfosTarifaires.add(lbPrixPort, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixPort ----
          tfPrixPort.setFont(tfPrixPort.getFont().deriveFont(tfPrixPort.getFont().getSize() + 2f));
          tfPrixPort.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixPort.setPreferredSize(new Dimension(90, 30));
          tfPrixPort.setMinimumSize(new Dimension(90, 30));
          tfPrixPort.setEnabled(false);
          tfPrixPort.setName("tfPrixPort");
          pnlInfosTarifaires.add(tfPrixPort, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixDeRevientStandard ----
          lbPrixDeRevientStandard.setText("Prix de revient fournisseur HT");
          lbPrixDeRevientStandard.setFont(lbPrixDeRevientStandard.getFont().deriveFont(lbPrixDeRevientStandard.getFont().getSize() + 2f));
          lbPrixDeRevientStandard.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixDeRevientStandard.setPreferredSize(new Dimension(200, 25));
          lbPrixDeRevientStandard.setMinimumSize(new Dimension(200, 25));
          lbPrixDeRevientStandard.setMaximumSize(new Dimension(200, 25));
          lbPrixDeRevientStandard.setName("lbPrixDeRevientStandard");
          pnlInfosTarifaires.add(lbPrixDeRevientStandard, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfPrixDeRevientFournisseur ----
          tfPrixDeRevientFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixDeRevientFournisseur
              .setFont(tfPrixDeRevientFournisseur.getFont().deriveFont(tfPrixDeRevientFournisseur.getFont().getSize() + 2f));
          tfPrixDeRevientFournisseur.setPreferredSize(new Dimension(90, 30));
          tfPrixDeRevientFournisseur.setMinimumSize(new Dimension(90, 30));
          tfPrixDeRevientFournisseur.setEnabled(false);
          tfPrixDeRevientFournisseur.setName("tfPrixDeRevientFournisseur");
          pnlInfosTarifaires.add(tfPrixDeRevientFournisseur, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- lbPrixAchatNet2 ----
          lbPrixAchatNet2.setText("Prix d'achat brut HT");
          lbPrixAchatNet2.setFont(lbPrixAchatNet2.getFont().deriveFont(lbPrixAchatNet2.getFont().getSize() + 2f));
          lbPrixAchatNet2.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixAchatNet2.setPreferredSize(new Dimension(150, 25));
          lbPrixAchatNet2.setMinimumSize(new Dimension(150, 25));
          lbPrixAchatNet2.setMaximumSize(new Dimension(150, 25));
          lbPrixAchatNet2.setName("lbPrixAchatNet2");
          pnlInfosTarifaires.add(lbPrixAchatNet2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixAchatBrut ----
          tfPrixAchatBrut.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixAchatBrut.setFont(tfPrixAchatBrut.getFont().deriveFont(tfPrixAchatBrut.getFont().getSize() + 2f));
          tfPrixAchatBrut.setPreferredSize(new Dimension(90, 30));
          tfPrixAchatBrut.setMinimumSize(new Dimension(90, 30));
          tfPrixAchatBrut.setEnabled(false);
          tfPrixAchatBrut.setName("tfPrixAchatBrut");
          pnlInfosTarifaires.add(tfPrixAchatBrut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        }
        panel1.add(pnlInfosTarifaires, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMontant ========
        {
          pnlMontant.setBorder(new TitledBorder("Montant HT"));
          pnlMontant.setOpaque(false);
          pnlMontant.setMinimumSize(new Dimension(210, 100));
          pnlMontant.setPreferredSize(new Dimension(210, 100));
          pnlMontant.setName("pnlMontant");
          pnlMontant.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMontant.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlMontant.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMontant.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMontant.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbVide ----
          lbVide.setText(" ");
          lbVide.setMaximumSize(new Dimension(3, 20));
          lbVide.setMinimumSize(new Dimension(3, 20));
          lbVide.setPreferredSize(new Dimension(3, 20));
          lbVide.setName("lbVide");
          pnlMontant.add(lbVide, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfMontantTotal ----
          tfMontantTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMontantTotal.setFont(new Font("sansserif", Font.BOLD, 15));
          tfMontantTotal.setMinimumSize(new Dimension(180, 35));
          tfMontantTotal.setPreferredSize(new Dimension(180, 35));
          tfMontantTotal.setEnabled(false);
          tfMontantTotal.setName("tfMontantTotal");
          pnlMontant.add(tfMontantTotal, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        panel1.add(pnlMontant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlGeneral.add(panel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 10, 10, 10), 0, 0));
      
      // ======== panel3 ========
      {
        panel3.setOpaque(false);
        panel3.setMinimumSize(new Dimension(945, 160));
        panel3.setPreferredSize(new Dimension(945, 160));
        panel3.setName("panel3");
        panel3.setLayout(new GridBagLayout());
        ((GridBagLayout) panel3.getLayout()).columnWidths = new int[] { 184, 0, 0, 0, 280, 113, 0, 0 };
        ((GridBagLayout) panel3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) panel3.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) panel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCodeArticle ----
        lbCodeArticle.setText("Code article");
        lbCodeArticle.setFont(
            lbCodeArticle.getFont().deriveFont(lbCodeArticle.getFont().getStyle() & ~Font.BOLD, lbCodeArticle.getFont().getSize() + 2f));
        lbCodeArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbCodeArticle.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodeArticle.setName("lbCodeArticle");
        panel3.add(lbCodeArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfCodeArticle ----
        tfCodeArticle.setFont(tfCodeArticle.getFont().deriveFont(tfCodeArticle.getFont().getSize() + 3f));
        tfCodeArticle.setEditable(false);
        tfCodeArticle.setPreferredSize(new Dimension(160, 35));
        tfCodeArticle.setMinimumSize(new Dimension(160, 35));
        tfCodeArticle.setEnabled(false);
        tfCodeArticle.setName("tfCodeArticle");
        panel3.add(tfCodeArticle, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbLibelleArticle ----
        lbLibelleArticle.setText("Libell\u00e9 article");
        lbLibelleArticle.setFont(lbLibelleArticle.getFont().deriveFont(lbLibelleArticle.getFont().getStyle() & ~Font.BOLD,
            lbLibelleArticle.getFont().getSize() + 2f));
        lbLibelleArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLibelleArticle.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLibelleArticle.setName("lbLibelleArticle");
        panel3.add(lbLibelleArticle, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfLibelleArticle ----
        tfLibelleArticle.setFont(tfLibelleArticle.getFont().deriveFont(tfLibelleArticle.getFont().getSize() + 1f));
        tfLibelleArticle.setBackground(Color.white);
        tfLibelleArticle.setLineWrap(true);
        tfLibelleArticle.setWrapStyleWord(true);
        tfLibelleArticle.setEditable(false);
        tfLibelleArticle.setFocusable(false);
        tfLibelleArticle.setPreferredSize(new Dimension(610, 45));
        tfLibelleArticle.setMinimumSize(new Dimension(610, 45));
        tfLibelleArticle.setEnabled(false);
        tfLibelleArticle.setName("tfLibelleArticle");
        panel3.add(tfLibelleArticle, new GridBagConstraints(1, 3, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbQuantiteUCA ----
        lbQuantiteUCA.setText("Quantit\u00e9 en UCA");
        lbQuantiteUCA.setFont(
            lbQuantiteUCA.getFont().deriveFont(lbQuantiteUCA.getFont().getStyle() & ~Font.BOLD, lbQuantiteUCA.getFont().getSize() + 2f));
        lbQuantiteUCA.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbQuantiteUCA.setHorizontalAlignment(SwingConstants.RIGHT);
        lbQuantiteUCA.setName("lbQuantiteUCA");
        panel3.add(lbQuantiteUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfQuantiteCommandeUCachat ----
        tfQuantiteCommandeUCachat.setHorizontalAlignment(SwingConstants.RIGHT);
        tfQuantiteCommandeUCachat
            .setFont(tfQuantiteCommandeUCachat.getFont().deriveFont(tfQuantiteCommandeUCachat.getFont().getSize() + 3f));
        tfQuantiteCommandeUCachat.setMinimumSize(new Dimension(105, 30));
        tfQuantiteCommandeUCachat.setPreferredSize(new Dimension(105, 30));
        tfQuantiteCommandeUCachat.setName("tfQuantiteCommandeUCachat");
        tfQuantiteCommandeUCachat.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfquantiteCommandeUCFocusLost(e);
          }
        });
        panel3.add(tfQuantiteCommandeUCachat, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfUniteCA ----
        tfUniteCA.setFont(tfUniteCA.getFont().deriveFont(tfUniteCA.getFont().getSize() + 3f));
        tfUniteCA.setMinimumSize(new Dimension(50, 35));
        tfUniteCA.setPreferredSize(new Dimension(50, 35));
        tfUniteCA.setEnabled(false);
        tfUniteCA.setName("tfUniteCA");
        panel3.add(tfUniteCA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbUAparUCA ----
        lbUAparUCA.setText("UA/UCA");
        lbUAparUCA
            .setFont(lbUAparUCA.getFont().deriveFont(lbUAparUCA.getFont().getStyle() & ~Font.BOLD, lbUAparUCA.getFont().getSize() + 2f));
        lbUAparUCA.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbUAparUCA.setHorizontalAlignment(SwingConstants.RIGHT);
        lbUAparUCA.setPreferredSize(new Dimension(60, 30));
        lbUAparUCA.setMinimumSize(new Dimension(60, 30));
        lbUAparUCA.setName("lbUAparUCA");
        panel3.add(lbUAparUCA, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfQuantiteCommandeeUA ----
        tfQuantiteCommandeeUA.setHorizontalAlignment(SwingConstants.RIGHT);
        tfQuantiteCommandeeUA.setFont(tfQuantiteCommandeeUA.getFont().deriveFont(tfQuantiteCommandeeUA.getFont().getSize() + 3f));
        tfQuantiteCommandeeUA.setPreferredSize(new Dimension(105, 30));
        tfQuantiteCommandeeUA.setMinimumSize(new Dimension(105, 30));
        tfQuantiteCommandeeUA.setName("tfQuantiteCommandeeUA");
        tfQuantiteCommandeeUA.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfquantiteCommandeeUVFocusLost(e);
          }
        });
        panel3.add(tfQuantiteCommandeeUA, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfUniteA ----
        tfUniteA.setFont(tfUniteA.getFont().deriveFont(tfUniteA.getFont().getSize() + 3f));
        tfUniteA.setMinimumSize(new Dimension(50, 35));
        tfUniteA.setPreferredSize(new Dimension(50, 35));
        tfUniteA.setEnabled(false);
        tfUniteA.setName("tfUniteA");
        panel3.add(tfUniteA, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbUCAparUCS ----
        lbUCAparUCS.setText("UCA/UCS");
        lbUCAparUCS.setFont(
            lbUCAparUCS.getFont().deriveFont(lbUCAparUCS.getFont().getStyle() & ~Font.BOLD, lbUCAparUCS.getFont().getSize() + 2f));
        lbUCAparUCS.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbUCAparUCS.setHorizontalAlignment(SwingConstants.LEFT);
        lbUCAparUCS.setMinimumSize(new Dimension(60, 30));
        lbUCAparUCS.setPreferredSize(new Dimension(60, 30));
        lbUCAparUCS.setName("lbUCAparUCS");
        panel3.add(lbUCAparUCS, new GridBagConstraints(1, 1, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
      }
      pnlGeneral.add(panel3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
          new Insets(15, 10, 5, 10), 0, 0));
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setMinimumSize(new Dimension(700, 180));
        panel2.setPreferredSize(new Dimension(700, 180));
        panel2.setName("panel2");
        panel2.setLayout(new GridBagLayout());
        ((GridBagLayout) panel2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) panel2.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) panel2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) panel2.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlQuantitesUCA ========
        {
          pnlQuantitesUCA.setOpaque(false);
          pnlQuantitesUCA.setBorder(new TitledBorder("Quantit\u00e9s UCA"));
          pnlQuantitesUCA.setComponentPopupMenu(null);
          pnlQuantitesUCA.setPreferredSize(new Dimension(390, 180));
          pnlQuantitesUCA.setMinimumSize(new Dimension(390, 180));
          pnlQuantitesUCA.setName("pnlQuantitesUCA");
          pnlQuantitesUCA.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlQuantitesUCA.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlQuantitesUCA.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlQuantitesUCA.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlQuantitesUCA.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbGeneralQuantiteInitiale ----
          lbGeneralQuantiteInitiale.setText("Initiale");
          lbGeneralQuantiteInitiale.setFont(lbGeneralQuantiteInitiale.getFont().deriveFont(
              lbGeneralQuantiteInitiale.getFont().getStyle() & ~Font.BOLD, lbGeneralQuantiteInitiale.getFont().getSize() + 2f));
          lbGeneralQuantiteInitiale.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralQuantiteInitiale.setComponentPopupMenu(null);
          lbGeneralQuantiteInitiale.setPreferredSize(new Dimension(50, 30));
          lbGeneralQuantiteInitiale.setMinimumSize(new Dimension(50, 30));
          lbGeneralQuantiteInitiale.setMaximumSize(new Dimension(50, 30));
          lbGeneralQuantiteInitiale.setName("lbGeneralQuantiteInitiale");
          pnlQuantitesUCA.add(lbGeneralQuantiteInitiale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralQuantiteInitiale ----
          tfGeneralQuantiteInitiale.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralQuantiteInitiale.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralQuantiteInitiale.setFont(tfGeneralQuantiteInitiale.getFont().deriveFont(
              tfGeneralQuantiteInitiale.getFont().getStyle() & ~Font.ITALIC, tfGeneralQuantiteInitiale.getFont().getSize() + 2f));
          tfGeneralQuantiteInitiale.setComponentPopupMenu(null);
          tfGeneralQuantiteInitiale.setEditable(false);
          tfGeneralQuantiteInitiale.setMinimumSize(new Dimension(100, 30));
          tfGeneralQuantiteInitiale.setPreferredSize(new Dimension(100, 30));
          tfGeneralQuantiteInitiale.setEnabled(false);
          tfGeneralQuantiteInitiale.setName("tfGeneralQuantiteInitiale");
          pnlQuantitesUCA.add(tfGeneralQuantiteInitiale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbGeneralMontantInitialHT ----
          lbGeneralMontantInitialHT.setText("Montant HT");
          lbGeneralMontantInitialHT.setFont(lbGeneralMontantInitialHT.getFont().deriveFont(
              lbGeneralMontantInitialHT.getFont().getStyle() & ~Font.BOLD, lbGeneralMontantInitialHT.getFont().getSize() + 2f));
          lbGeneralMontantInitialHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralMontantInitialHT.setComponentPopupMenu(null);
          lbGeneralMontantInitialHT.setPreferredSize(new Dimension(75, 30));
          lbGeneralMontantInitialHT.setMaximumSize(new Dimension(75, 30));
          lbGeneralMontantInitialHT.setMinimumSize(new Dimension(75, 30));
          lbGeneralMontantInitialHT.setName("lbGeneralMontantInitialHT");
          pnlQuantitesUCA.add(lbGeneralMontantInitialHT, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralMontantInitialHT ----
          tfGeneralMontantInitialHT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralMontantInitialHT.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralMontantInitialHT
              .setFont(tfGeneralMontantInitialHT.getFont().deriveFont(Font.PLAIN, tfGeneralMontantInitialHT.getFont().getSize() + 3f));
          tfGeneralMontantInitialHT.setComponentPopupMenu(null);
          tfGeneralMontantInitialHT.setEditable(false);
          tfGeneralMontantInitialHT.setMinimumSize(new Dimension(100, 30));
          tfGeneralMontantInitialHT.setPreferredSize(new Dimension(100, 30));
          tfGeneralMontantInitialHT.setEnabled(false);
          tfGeneralMontantInitialHT.setName("tfGeneralMontantInitialHT");
          pnlQuantitesUCA.add(tfGeneralMontantInitialHT, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbGeneralQuantiteReliquat ----
          lbGeneralQuantiteReliquat.setText("En reliquat");
          lbGeneralQuantiteReliquat.setFont(lbGeneralQuantiteReliquat.getFont().deriveFont(
              lbGeneralQuantiteReliquat.getFont().getStyle() & ~Font.BOLD, lbGeneralQuantiteReliquat.getFont().getSize() + 2f));
          lbGeneralQuantiteReliquat.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralQuantiteReliquat.setComponentPopupMenu(null);
          lbGeneralQuantiteReliquat.setPreferredSize(new Dimension(75, 30));
          lbGeneralQuantiteReliquat.setMinimumSize(new Dimension(75, 30));
          lbGeneralQuantiteReliquat.setMaximumSize(new Dimension(75, 30));
          lbGeneralQuantiteReliquat.setName("lbGeneralQuantiteReliquat");
          pnlQuantitesUCA.add(lbGeneralQuantiteReliquat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralQuantiteReliquat ----
          tfGeneralQuantiteReliquat.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralQuantiteReliquat.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralQuantiteReliquat.setFont(tfGeneralQuantiteReliquat.getFont().deriveFont(
              tfGeneralQuantiteReliquat.getFont().getStyle() & ~Font.ITALIC, tfGeneralQuantiteReliquat.getFont().getSize() + 2f));
          tfGeneralQuantiteReliquat.setComponentPopupMenu(null);
          tfGeneralQuantiteReliquat.setEditable(false);
          tfGeneralQuantiteReliquat.setMinimumSize(new Dimension(100, 30));
          tfGeneralQuantiteReliquat.setPreferredSize(new Dimension(100, 30));
          tfGeneralQuantiteReliquat.setEnabled(false);
          tfGeneralQuantiteReliquat.setName("tfGeneralQuantiteReliquat");
          pnlQuantitesUCA.add(tfGeneralQuantiteReliquat, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbGeneralMontantReliquatHT ----
          lbGeneralMontantReliquatHT.setText("Montant HT");
          lbGeneralMontantReliquatHT.setFont(lbGeneralMontantReliquatHT.getFont().deriveFont(
              lbGeneralMontantReliquatHT.getFont().getStyle() & ~Font.BOLD, lbGeneralMontantReliquatHT.getFont().getSize() + 2f));
          lbGeneralMontantReliquatHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralMontantReliquatHT.setComponentPopupMenu(null);
          lbGeneralMontantReliquatHT.setPreferredSize(new Dimension(75, 30));
          lbGeneralMontantReliquatHT.setMinimumSize(new Dimension(75, 30));
          lbGeneralMontantReliquatHT.setMaximumSize(new Dimension(75, 30));
          lbGeneralMontantReliquatHT.setName("lbGeneralMontantReliquatHT");
          pnlQuantitesUCA.add(lbGeneralMontantReliquatHT, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralMontantReliquatHT ----
          tfGeneralMontantReliquatHT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralMontantReliquatHT.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralMontantReliquatHT
              .setFont(tfGeneralMontantReliquatHT.getFont().deriveFont(Font.PLAIN, tfGeneralMontantReliquatHT.getFont().getSize() + 3f));
          tfGeneralMontantReliquatHT.setComponentPopupMenu(null);
          tfGeneralMontantReliquatHT.setEditable(false);
          tfGeneralMontantReliquatHT.setMinimumSize(new Dimension(100, 30));
          tfGeneralMontantReliquatHT.setPreferredSize(new Dimension(100, 30));
          tfGeneralMontantReliquatHT.setEnabled(false);
          tfGeneralMontantReliquatHT.setName("tfGeneralMontantReliquatHT");
          pnlQuantitesUCA.add(tfGeneralMontantReliquatHT, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbGeneralQuantiteTraitee ----
          lbGeneralQuantiteTraitee.setText("Trait\u00e9e");
          lbGeneralQuantiteTraitee.setFont(lbGeneralQuantiteTraitee.getFont()
              .deriveFont(lbGeneralQuantiteTraitee.getFont().getStyle() & ~Font.BOLD, lbGeneralQuantiteTraitee.getFont().getSize() + 2f));
          lbGeneralQuantiteTraitee.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralQuantiteTraitee.setComponentPopupMenu(null);
          lbGeneralQuantiteTraitee.setPreferredSize(new Dimension(75, 30));
          lbGeneralQuantiteTraitee.setMinimumSize(new Dimension(75, 30));
          lbGeneralQuantiteTraitee.setMaximumSize(new Dimension(75, 30));
          lbGeneralQuantiteTraitee.setName("lbGeneralQuantiteTraitee");
          pnlQuantitesUCA.add(lbGeneralQuantiteTraitee, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfGeneralQuantiteTraitee ----
          tfGeneralQuantiteTraitee.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralQuantiteTraitee.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralQuantiteTraitee
              .setFont(tfGeneralQuantiteTraitee.getFont().deriveFont(Font.PLAIN, tfGeneralQuantiteTraitee.getFont().getSize() + 3f));
          tfGeneralQuantiteTraitee.setComponentPopupMenu(null);
          tfGeneralQuantiteTraitee.setEditable(false);
          tfGeneralQuantiteTraitee.setMinimumSize(new Dimension(100, 30));
          tfGeneralQuantiteTraitee.setPreferredSize(new Dimension(100, 30));
          tfGeneralQuantiteTraitee.setEnabled(false);
          tfGeneralQuantiteTraitee.setName("tfGeneralQuantiteTraitee");
          pnlQuantitesUCA.add(tfGeneralQuantiteTraitee, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbGeneralMontantTraiteHT ----
          lbGeneralMontantTraiteHT.setText("Montant HT");
          lbGeneralMontantTraiteHT.setFont(lbGeneralMontantTraiteHT.getFont()
              .deriveFont(lbGeneralMontantTraiteHT.getFont().getStyle() & ~Font.BOLD, lbGeneralMontantTraiteHT.getFont().getSize() + 2f));
          lbGeneralMontantTraiteHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbGeneralMontantTraiteHT.setComponentPopupMenu(null);
          lbGeneralMontantTraiteHT.setPreferredSize(new Dimension(150, 30));
          lbGeneralMontantTraiteHT.setName("lbGeneralMontantTraiteHT");
          pnlQuantitesUCA.add(lbGeneralMontantTraiteHT, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfGeneralMontantTraiteHT ----
          tfGeneralMontantTraiteHT.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralMontantTraiteHT.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralMontantTraiteHT
              .setFont(tfGeneralMontantTraiteHT.getFont().deriveFont(Font.PLAIN, tfGeneralMontantTraiteHT.getFont().getSize() + 3f));
          tfGeneralMontantTraiteHT.setComponentPopupMenu(null);
          tfGeneralMontantTraiteHT.setEditable(false);
          tfGeneralMontantTraiteHT.setMinimumSize(new Dimension(100, 30));
          tfGeneralMontantTraiteHT.setPreferredSize(new Dimension(100, 30));
          tfGeneralMontantTraiteHT.setEnabled(false);
          tfGeneralMontantTraiteHT.setName("tfGeneralMontantTraiteHT");
          pnlQuantitesUCA.add(tfGeneralMontantTraiteHT, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        panel2.add(pnlQuantitesUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlStocksGeneral ========
        {
          pnlStocksGeneral.setOpaque(false);
          pnlStocksGeneral.setBorder(new TitledBorder("Stocks en US"));
          pnlStocksGeneral.setComponentPopupMenu(null);
          pnlStocksGeneral.setPreferredSize(new Dimension(240, 180));
          pnlStocksGeneral.setName("pnlStocksGeneral");
          pnlStocksGeneral.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlStocksGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlStocksGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlStocksGeneral.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlStocksGeneral.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbStockPhysique ----
          lbStockPhysique.setText("Physique");
          lbStockPhysique.setFont(lbStockPhysique.getFont().deriveFont(lbStockPhysique.getFont().getStyle() & ~Font.BOLD,
              lbStockPhysique.getFont().getSize() + 2f));
          lbStockPhysique.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockPhysique.setComponentPopupMenu(null);
          lbStockPhysique.setName("lbStockPhysique");
          pnlStocksGeneral.add(lbStockPhysique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralStockPhysique ----
          tfGeneralStockPhysique.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockPhysique.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockPhysique.setFont(tfGeneralStockPhysique.getFont()
              .deriveFont(tfGeneralStockPhysique.getFont().getStyle() & ~Font.ITALIC, tfGeneralStockPhysique.getFont().getSize() + 2f));
          tfGeneralStockPhysique.setComponentPopupMenu(null);
          tfGeneralStockPhysique.setEditable(false);
          tfGeneralStockPhysique.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockPhysique.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockPhysique.setEnabled(false);
          tfGeneralStockPhysique.setName("tfGeneralStockPhysique");
          pnlStocksGeneral.add(tfGeneralStockPhysique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbStockCommande ----
          lbStockCommande.setText("Command\u00e9");
          lbStockCommande.setFont(lbStockCommande.getFont().deriveFont(lbStockCommande.getFont().getStyle() & ~Font.BOLD,
              lbStockCommande.getFont().getSize() + 2f));
          lbStockCommande.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockCommande.setComponentPopupMenu(null);
          lbStockCommande.setName("lbStockCommande");
          pnlStocksGeneral.add(lbStockCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralStockCommande ----
          tfGeneralStockCommande.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockCommande.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockCommande.setFont(tfGeneralStockCommande.getFont()
              .deriveFont(tfGeneralStockCommande.getFont().getStyle() & ~Font.ITALIC, tfGeneralStockCommande.getFont().getSize() + 2f));
          tfGeneralStockCommande.setComponentPopupMenu(null);
          tfGeneralStockCommande.setEditable(false);
          tfGeneralStockCommande.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockCommande.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockCommande.setEnabled(false);
          tfGeneralStockCommande.setName("tfGeneralStockCommande");
          pnlStocksGeneral.add(tfGeneralStockCommande, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbStockAttendu ----
          lbStockAttendu.setText("Attendu");
          lbStockAttendu.setFont(lbStockAttendu.getFont().deriveFont(lbStockAttendu.getFont().getStyle() & ~Font.BOLD,
              lbStockAttendu.getFont().getSize() + 2f));
          lbStockAttendu.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockAttendu.setComponentPopupMenu(null);
          lbStockAttendu.setName("lbStockAttendu");
          pnlStocksGeneral.add(lbStockAttendu, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralStockAttendu ----
          tfGeneralStockAttendu.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockAttendu.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockAttendu.setFont(tfGeneralStockAttendu.getFont()
              .deriveFont(tfGeneralStockAttendu.getFont().getStyle() & ~Font.ITALIC, tfGeneralStockAttendu.getFont().getSize() + 2f));
          tfGeneralStockAttendu.setComponentPopupMenu(null);
          tfGeneralStockAttendu.setEditable(false);
          tfGeneralStockAttendu.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockAttendu.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockAttendu.setEnabled(false);
          tfGeneralStockAttendu.setName("tfGeneralStockAttendu");
          pnlStocksGeneral.add(tfGeneralStockAttendu, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbStockDisponible ----
          lbStockDisponible.setText("Disponible achat");
          lbStockDisponible.setFont(lbStockDisponible.getFont().deriveFont(lbStockDisponible.getFont().getStyle() & ~Font.BOLD,
              lbStockDisponible.getFont().getSize() + 2f));
          lbStockDisponible.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockDisponible.setComponentPopupMenu(null);
          lbStockDisponible.setName("lbStockDisponible");
          pnlStocksGeneral.add(lbStockDisponible, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfGeneralStockDisponible ----
          tfGeneralStockDisponible.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockDisponible.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockDisponible.setFont(tfGeneralStockDisponible.getFont().deriveFont(
              tfGeneralStockDisponible.getFont().getStyle() & ~Font.ITALIC, tfGeneralStockDisponible.getFont().getSize() + 2f));
          tfGeneralStockDisponible.setComponentPopupMenu(null);
          tfGeneralStockDisponible.setEditable(false);
          tfGeneralStockDisponible.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockDisponible.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockDisponible.setEnabled(false);
          tfGeneralStockDisponible.setName("tfGeneralStockDisponible");
          pnlStocksGeneral.add(tfGeneralStockDisponible, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        panel2.add(pnlStocksGeneral, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlApprovisionnementStocks ========
        {
          pnlApprovisionnementStocks.setOpaque(false);
          pnlApprovisionnementStocks.setBorder(new TitledBorder("Approvisionnement stocks en UCA"));
          pnlApprovisionnementStocks.setComponentPopupMenu(null);
          pnlApprovisionnementStocks.setPreferredSize(new Dimension(290, 180));
          pnlApprovisionnementStocks.setMinimumSize(new Dimension(290, 180));
          pnlApprovisionnementStocks.setName("pnlApprovisionnementStocks");
          pnlApprovisionnementStocks.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlApprovisionnementStocks.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlApprovisionnementStocks.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlApprovisionnementStocks.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlApprovisionnementStocks.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbStockConso ----
          lbStockConso.setText("Consommation mensuelle");
          lbStockConso.setFont(
              lbStockConso.getFont().deriveFont(lbStockConso.getFont().getStyle() & ~Font.BOLD, lbStockConso.getFont().getSize() + 2f));
          lbStockConso.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockConso.setComponentPopupMenu(null);
          lbStockConso.setName("lbStockConso");
          pnlApprovisionnementStocks.add(lbStockConso, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralStockConso ----
          tfGeneralStockConso.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockConso.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockConso.setFont(tfGeneralStockConso.getFont().deriveFont(tfGeneralStockConso.getFont().getStyle() & ~Font.ITALIC,
              tfGeneralStockConso.getFont().getSize() + 2f));
          tfGeneralStockConso.setComponentPopupMenu(null);
          tfGeneralStockConso.setEditable(false);
          tfGeneralStockConso.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockConso.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockConso.setEnabled(false);
          tfGeneralStockConso.setName("tfGeneralStockConso");
          pnlApprovisionnementStocks.add(tfGeneralStockConso, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbStockDelai ----
          lbStockDelai.setText("D\u00e9lai en jours");
          lbStockDelai.setFont(
              lbStockDelai.getFont().deriveFont(lbStockDelai.getFont().getStyle() & ~Font.BOLD, lbStockDelai.getFont().getSize() + 2f));
          lbStockDelai.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockDelai.setComponentPopupMenu(null);
          lbStockDelai.setName("lbStockDelai");
          pnlApprovisionnementStocks.add(lbStockDelai, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfGeneralStockDelai ----
          tfGeneralStockDelai.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockDelai.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockDelai.setFont(tfGeneralStockDelai.getFont().deriveFont(tfGeneralStockDelai.getFont().getStyle() & ~Font.ITALIC,
              tfGeneralStockDelai.getFont().getSize() + 2f));
          tfGeneralStockDelai.setComponentPopupMenu(null);
          tfGeneralStockDelai.setEditable(false);
          tfGeneralStockDelai.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockDelai.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockDelai.setEnabled(false);
          tfGeneralStockDelai.setName("tfGeneralStockDelai");
          pnlApprovisionnementStocks.add(tfGeneralStockDelai, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbStockMini ----
          lbStockMini.setText("Stock minimum");
          lbStockMini.setFont(
              lbStockMini.getFont().deriveFont(lbStockMini.getFont().getStyle() & ~Font.BOLD, lbStockMini.getFont().getSize() + 2f));
          lbStockMini.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockMini.setComponentPopupMenu(null);
          lbStockMini.setName("lbStockMini");
          pnlApprovisionnementStocks.add(lbStockMini, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbStockIdeal ----
          lbStockIdeal.setText("Stock id\u00e9al");
          lbStockIdeal.setFont(
              lbStockIdeal.getFont().deriveFont(lbStockIdeal.getFont().getStyle() & ~Font.BOLD, lbStockIdeal.getFont().getSize() + 2f));
          lbStockIdeal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbStockIdeal.setComponentPopupMenu(null);
          lbStockIdeal.setName("lbStockIdeal");
          pnlApprovisionnementStocks.add(lbStockIdeal, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfGeneralStockMini ----
          tfGeneralStockMini.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockMini.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockMini.setFont(tfGeneralStockMini.getFont().deriveFont(Font.PLAIN, tfGeneralStockMini.getFont().getSize() + 3f));
          tfGeneralStockMini.setComponentPopupMenu(null);
          tfGeneralStockMini.setEditable(false);
          tfGeneralStockMini.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockMini.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockMini.setEnabled(false);
          tfGeneralStockMini.setName("tfGeneralStockMini");
          pnlApprovisionnementStocks.add(tfGeneralStockMini, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfGeneralStockIdeal ----
          tfGeneralStockIdeal.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tfGeneralStockIdeal.setHorizontalAlignment(SwingConstants.RIGHT);
          tfGeneralStockIdeal.setFont(tfGeneralStockIdeal.getFont().deriveFont(Font.PLAIN, tfGeneralStockIdeal.getFont().getSize() + 3f));
          tfGeneralStockIdeal.setComponentPopupMenu(null);
          tfGeneralStockIdeal.setEditable(false);
          tfGeneralStockIdeal.setMinimumSize(new Dimension(100, 30));
          tfGeneralStockIdeal.setPreferredSize(new Dimension(100, 30));
          tfGeneralStockIdeal.setEnabled(false);
          tfGeneralStockIdeal.setName("tfGeneralStockIdeal");
          pnlApprovisionnementStocks.add(tfGeneralStockIdeal, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        panel2.add(pnlApprovisionnementStocks, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlGeneral.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 10, 5, 10), 0, 0));
    }
    add(pnlGeneral, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private JPanel pnlGeneral;
  private JPanel panel1;
  private JPanel pnlInfosTarifaires;
  private JLabel lbPrixAchatNet;
  private XRiTextField tfPrixAchatNet;
  private JLabel lbPrixPort;
  private XRiTextField tfPrixPort;
  private JLabel lbPrixDeRevientStandard;
  private XRiTextField tfPrixDeRevientFournisseur;
  private JLabel lbPrixAchatNet2;
  private XRiTextField tfPrixAchatBrut;
  private JPanel pnlMontant;
  private JLabel lbVide;
  private XRiTextField tfMontantTotal;
  private JPanel panel3;
  private JLabel lbCodeArticle;
  private XRiTextField tfCodeArticle;
  private JLabel lbLibelleArticle;
  private RiTextArea tfLibelleArticle;
  private JLabel lbQuantiteUCA;
  private XRiTextField tfQuantiteCommandeUCachat;
  private XRiTextField tfUniteCA;
  private JLabel lbUAparUCA;
  private XRiTextField tfQuantiteCommandeeUA;
  private XRiTextField tfUniteA;
  private JLabel lbUCAparUCS;
  private JPanel panel2;
  private JPanel pnlQuantitesUCA;
  private JLabel lbGeneralQuantiteInitiale;
  private XRiTextField tfGeneralQuantiteInitiale;
  private JLabel lbGeneralMontantInitialHT;
  private XRiTextField tfGeneralMontantInitialHT;
  private JLabel lbGeneralQuantiteReliquat;
  private XRiTextField tfGeneralQuantiteReliquat;
  private JLabel lbGeneralMontantReliquatHT;
  private XRiTextField tfGeneralMontantReliquatHT;
  private JLabel lbGeneralQuantiteTraitee;
  private XRiTextField tfGeneralQuantiteTraitee;
  private JLabel lbGeneralMontantTraiteHT;
  private XRiTextField tfGeneralMontantTraiteHT;
  private JPanel pnlStocksGeneral;
  private JLabel lbStockPhysique;
  private XRiTextField tfGeneralStockPhysique;
  private JLabel lbStockCommande;
  private XRiTextField tfGeneralStockCommande;
  private JLabel lbStockAttendu;
  private XRiTextField tfGeneralStockAttendu;
  private JLabel lbStockDisponible;
  private XRiTextField tfGeneralStockDisponible;
  private JPanel pnlApprovisionnementStocks;
  private JLabel lbStockConso;
  private XRiTextField tfGeneralStockConso;
  private JLabel lbStockDelai;
  private XRiTextField tfGeneralStockDelai;
  private JLabel lbStockMini;
  private JLabel lbStockIdeal;
  private XRiTextField tfGeneralStockMini;
  private XRiTextField tfGeneralStockIdeal;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
