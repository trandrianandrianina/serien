/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Ajouter un bon de cour à un bon ou une facture d'enlèvement
 */
public class VueConsultationBonCour extends AbstractVueDialogue<ModeleConsultationBonCour> {
  
  /**
   * Constructeur.
   */
  public VueConsultationBonCour(ModeleConsultationBonCour pModele) {
    super(pModele);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construire l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setTitle("Bon de cour");
    
    // Initialisation des états de bon de cour
    cbEtat.removeAllItems();
    for (EnumEtatBonCour enumEtat : EnumEtatBonCour.values()) {
      cbEtat.addItem(enumEtat);
    }
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichir l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirNumeroBon();
    rafraichirEtat();
    rafraichirMagasinier();
    rafraichirNumeroCarnet();
    rafraichirDocument();
    rafraichirDateDocument();
    rafraichirClient();
    
    snBarreBouton.getBouton(EnumBouton.ANNULER).requestFocusInWindow();
  }
  
  /**
   * Rafraîchir le numéro de bon.
   */
  private void rafraichirNumeroBon() {
    BonCour bonCourSelectionne = getModele().getBonCourSelectionne();
    tfNumeroBonCour.setText(Constantes.convertirIntegerEnTexte(bonCourSelectionne.getId().getNumero(), 0));
  }
  
  /**
   * Rafraîchir l'état du bon.
   */
  private void rafraichirEtat() {
    BonCour bonCourSelectionne = getModele().getBonCourSelectionne();
    cbEtat.setSelectedItem(bonCourSelectionne.getEtatBonCour());
  }
  
  /**
   * Rafraîchir le magasinier.
   */
  private void rafraichirMagasinier() {
    Vendeur magasinier = getModele().getMagasinier();
    snVendeur.setSession(getModele().getSession());
    snVendeur.setIdEtablissement(getModele().getIdEtablissement());
    snVendeur.charger(false);
    snVendeur.setSelection(magasinier);
  }
  
  /**
   * Rafraîchir le numéro de carnet.
   */
  private void rafraichirNumeroCarnet() {
    BonCour bonCourSelectionne = getModele().getBonCourSelectionne();
    tfNumeroCarnet.setText(Constantes.convertirIntegerEnTexte(bonCourSelectionne.getId().getIdCarnetBonCour().getNumero(), 0));
  }
  
  /**
   * Rafraîchir le document de vente.
   */
  private void rafraichirDocument() {
    DocumentVente document = getModele().getDocumentVente();
    if (document != null) {
      tfDocument.setText(document.getId().getTexte());
    }
  }
  
  /**
   * Rafraîchir la date de création du document de vente.
   */
  private void rafraichirDateDocument() {
    DocumentVente document = getModele().getDocumentVente();
    if (document != null) {
      snDateCreationDocument.setDate(document.getDateCreation());
    }
  }
  
  /**
   * Rafraîchir le client.
   */
  private void rafraichirClient() {
    Client client = getModele().getClient();
    snClient.setSession(getModele().getSession());
    snClient.setIdEtablissement(getModele().getIdEtablissement());
    snClient.charger(false);
    snClient.setSelection(client);
  }
  
  // -- Méthodes événementielles
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbNumeroBon = new SNLabelChamp();
    tfNumeroBonCour = new SNTexte();
    lbEtat = new SNLabelChamp();
    cbEtat = new SNComboBox();
    lbMagasinier = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbNumeroCarnet = new SNLabelChamp();
    tfNumeroCarnet = new SNTexte();
    lbDocument = new SNLabelChamp();
    tfDocument = new SNTexte();
    lbDate = new SNLabelChamp();
    snDateCreationDocument = new SNDate();
    lbClient = new SNLabelChamp();
    snClient = new SNClient();
    
    // ======== this ========
    setTitle("Bon de cour");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(640, 190));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setBackground(new Color(238, 238, 210));
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumeroBon ----
        lbNumeroBon.setText("Num\u00e9ro de bon de cour");
        lbNumeroBon.setMinimumSize(new Dimension(200, 30));
        lbNumeroBon.setPreferredSize(new Dimension(200, 30));
        lbNumeroBon.setMaximumSize(new Dimension(200, 30));
        lbNumeroBon.setName("lbNumeroBon");
        pnlContenu.add(lbNumeroBon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNumeroBonCour ----
        tfNumeroBonCour.setHorizontalAlignment(SwingConstants.TRAILING);
        tfNumeroBonCour.setEnabled(false);
        tfNumeroBonCour.setName("tfNumeroBonCour");
        pnlContenu.add(tfNumeroBonCour, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbEtat ----
        lbEtat.setText("Etat");
        lbEtat.setName("lbEtat");
        pnlContenu.add(lbEtat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbEtat ----
        cbEtat.setMinimumSize(new Dimension(150, 30));
        cbEtat.setPreferredSize(new Dimension(150, 30));
        cbEtat.setEnabled(false);
        cbEtat.setName("cbEtat");
        pnlContenu.add(cbEtat, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMagasinier ----
        lbMagasinier.setText("Magasinier");
        lbMagasinier.setName("lbMagasinier");
        pnlContenu.add(lbMagasinier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snVendeur ----
        snVendeur.setEnabled(false);
        snVendeur.setName("snVendeur");
        pnlContenu.add(snVendeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroCarnet ----
        lbNumeroCarnet.setText("Num\u00e9ro de carnet");
        lbNumeroCarnet.setMinimumSize(new Dimension(200, 30));
        lbNumeroCarnet.setPreferredSize(new Dimension(200, 30));
        lbNumeroCarnet.setMaximumSize(new Dimension(200, 30));
        lbNumeroCarnet.setName("lbNumeroCarnet");
        pnlContenu.add(lbNumeroCarnet, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNumeroCarnet ----
        tfNumeroCarnet.setHorizontalAlignment(SwingConstants.TRAILING);
        tfNumeroCarnet.setEnabled(false);
        tfNumeroCarnet.setName("tfNumeroCarnet");
        pnlContenu.add(tfNumeroCarnet, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDocument ----
        lbDocument.setText("Document");
        lbDocument.setName("lbDocument");
        pnlContenu.add(lbDocument, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfDocument ----
        tfDocument.setHorizontalAlignment(SwingConstants.TRAILING);
        tfDocument.setEnabled(false);
        tfDocument.setName("tfDocument");
        pnlContenu.add(tfDocument, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDate ----
        lbDate.setText("Date du document");
        lbDate.setName("lbDate");
        pnlContenu.add(lbDate, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDateCreationDocument ----
        snDateCreationDocument.setEnabled(false);
        snDateCreationDocument.setName("snDateCreationDocument");
        pnlContenu.add(snDateCreationDocument, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbClient ----
        lbClient.setText("Client");
        lbClient.setName("lbClient");
        pnlContenu.add(lbClient, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snClient ----
        snClient.setEnabled(false);
        snClient.setName("snClient");
        pnlContenu.add(snClient, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelChamp lbNumeroBon;
  private SNTexte tfNumeroBonCour;
  private SNLabelChamp lbEtat;
  private SNComboBox cbEtat;
  private SNLabelChamp lbMagasinier;
  private SNVendeur snVendeur;
  private SNLabelChamp lbNumeroCarnet;
  private SNTexte tfNumeroCarnet;
  private SNLabelChamp lbDocument;
  private SNTexte tfDocument;
  private SNLabelChamp lbDate;
  private SNDate snDateCreationDocument;
  private SNLabelChamp lbClient;
  private SNClient snClient;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
