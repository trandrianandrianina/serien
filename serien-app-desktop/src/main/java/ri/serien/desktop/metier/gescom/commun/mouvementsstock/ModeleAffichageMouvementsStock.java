/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.mouvementsstock;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.LigneMouvement;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle pour l'affichage de la liste des mouvements de stock.
 */
public class ModeleAffichageMouvementsStock extends AbstractModelePanel {
  // Variables
  private ListeMagasin listeMagasin = null;
  private ListeUnite listeUnite = null;
  private ListeVendeur listeVendeur = null;
  private ListeClientBase listeClientBase = null;
  private ListeFournisseurBase listeFournisseurBase = null;
  private Article article = null;
  private EnumTypeStockDisponible typeStockDisponible = null;
  private Magasin magasin = null;
  private ListeLigneMouvement listeLigneMouvement = null;
  private IdMagasin idMagasinLigne = null;
  
  /**
   * Constructeur.
   */
  public ModeleAffichageMouvementsStock(SessionBase pSession, Article pArticle, EnumTypeStockDisponible pTypeStockDisponible,
      IdMagasin pIdMagasin) {
    super(pSession);
    article = pArticle;
    typeStockDisponible = pTypeStockDisponible;
    idMagasinLigne = pIdMagasin;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Charger la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), article.getId().getIdEtablissement());
    
    // Charger la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
    
    // Charger les mouvements de stock pour le magasin sur lequel on travaille ou à défaut pour le premier magasin de la liste
    if (listeMagasin != null && !listeMagasin.isEmpty()) {
      if (idMagasinLigne != null) {
        magasin = listeMagasin.get(idMagasinLigne);
      }
      else {
        magasin = listeMagasin.get(0);
      }
      // Charger la liste des vendeurs
      listeVendeur = new ListeVendeur();
      listeVendeur = listeVendeur.charger(getIdSession(), article.getId().getIdEtablissement());
      
      chargerLigneMouvement();
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Charger tous les identifiants des lignes mouvements pour le magasin et l'article en cours.
   */
  private void chargerLigneMouvement() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeLigneMouvement != null) {
      taillePage = listeLigneMouvement.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente
    listeLigneMouvement = null;
    
    // Vérifier les pré-requis
    if (magasin == null) {
      throw new MessageErreurException("Le magasin n'est valide.");
    }
    if (article == null) {
      throw new MessageErreurException("L'article n'est valide.");
    }
    
    // Charger la liste des identifiants des mouvements de stock
    List<IdLigneMouvement> listeIdLigneMouvement =
        ManagerServiceStock.chargerListeIdLigneMouvement(getIdSession(), article.getId(), magasin);
    listeLigneMouvement = ListeLigneMouvement.creerListeNonChargee(listeIdLigneMouvement);
    
    // Aller à la première page
    listeLigneMouvement.setNombreDecimaleUniteStock(getNombreDecimaleUniteStock());
    listeLigneMouvement.chargerPremierePage(getIdSession(), taillePage);
    chargerListeTiers();
  }
  
  /**
   * Charger la liste des clients bases et des fournisseurs bases correspondant aux mouvements chargés
   */
  private void chargerListeTiers() {
    if (listeClientBase == null) {
      listeClientBase = new ListeClientBase();
    }
    if (listeFournisseurBase == null) {
      listeFournisseurBase = new ListeFournisseurBase();
    }
    ListeIdClient listeIdClient = new ListeIdClient();
    List<IdFournisseur> listeIdFournisseur = new ArrayList<IdFournisseur>();
    for (LigneMouvement mouvement : listeLigneMouvement) {
      if (mouvement.getIdClient() != null && !listeIdClient.contains(mouvement.getIdClient())) {
        listeIdClient.add(mouvement.getIdClient());
      }
      else if (mouvement.getIdFournisseur() != null && !listeIdFournisseur.contains(mouvement.getIdFournisseur())) {
        listeIdFournisseur.add(mouvement.getIdFournisseur());
      }
    }
    if (listeIdClient.size() > 0) {
      listeClientBase = ManagerServiceClient.chargerListeClientBase(getIdSession(), listeIdClient);
    }
    if (listeIdFournisseur.size() > 0) {
      listeFournisseurBase = ManagerServiceFournisseur.chargerListeFournisseurBase(getIdSession(), listeIdFournisseur);
    }
  }
  
  /**
   * Change le magasin à afficher.
   */
  public void modifierMagasin(Magasin pMagasin) {
    if (Constantes.equals(pMagasin, magasin)) {
      return;
    }
    magasin = pMagasin;
    chargerLigneMouvement();
    rafraichir();
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageDocumentsAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les lignes manquantes
    if (listeLigneMouvement != null) {
      listeLigneMouvement.setNombreDecimaleUniteStock(getNombreDecimaleUniteStock());
      listeLigneMouvement.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      chargerListeTiers();
    }
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifier le mouvement de stock sélectionné.
   */
  public void modifierSelection(int pLigneSelectionnee) {
    // Tester si la sélection a changé
    if (listeLigneMouvement != null && listeLigneMouvement.getIndexSelection() == pLigneSelectionnee) {
      return;
    }
    
    // Sélectionner le mouvement de stock
    listeLigneMouvement.selectionner(pLigneSelectionnee);
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Renvoyer le nom du client à partir de son identifiant
   */
  public String getNomClient(IdClient pIdClient) {
    if (pIdClient == null) {
      return "";
    }
    return listeClientBase.getClientBaseParId(pIdClient).toString();
  }
  
  /**
   * Renvoyer le nom du fournisseur à partir de son identifiant
   */
  public String getNomFournisseur(IdFournisseur pIdFournisseur) {
    if (pIdFournisseur == null) {
      return "";
    }
    return listeFournisseurBase.get(pIdFournisseur).toString();
  }
  
  /**
   * Renvoyer le nom du magasinier à partir de son identifiant
   */
  public String getNomMagasinier(IdVendeur pIdVendeur) {
    if (pIdVendeur == null) {
      return "";
    }
    
    Vendeur vendeur = listeVendeur.get(pIdVendeur);
    
    return vendeur.getNomAvecCode();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la précision d'une unité donnée
   */
  public int getNombreDecimaleUniteStock() {
    return listeUnite.getPrecisionUnite(article.getIdUS());
  }
  
  public ListeLigneMouvement getListeMouvementStock() {
    return listeLigneMouvement;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  public EnumTypeStockDisponible getTypeStockDisponible() {
    return typeStockDisponible;
  }
  
  public Magasin getMagasin() {
    return magasin;
  }
}
