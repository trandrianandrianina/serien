/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.commentaires;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.commun.EnumContexteMetier;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne commentaire.
 */
public class VueDetailCommentaire extends AbstractVueDialogue<ModeleDetailCommentaire> {
  private static final String BOUTON_SUPPRIMER_COMMENTAIRE = "Supprimer ce commentaire";
  
  /**
   * Constructeur.
   */
  public VueDetailCommentaire(ModeleDetailCommentaire pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    tfCodeCommentaire.init(IdArticle.LONGUEUR_CODE_ARTICLE, true, true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_COMMENTAIRE, 's', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    
    rafraichirTexte();
    rafraichirPanelEdition();
    rafraichirConserverCommentaire();
    rafraichirCopierSurDocumentAchats();
    rafraichirCodeCommentaire();
    
    if (getModele().getTexteCommentaire() != null) {
      if (getModele().getTexteCommentaire().isEmpty()) {
        taTexteCommentaire.requestFocus();
      }
      else if (getModele().isCommentaireSauvegardable() && ckConserverCommentaire.isSelected()) {
        tfCodeCommentaire.requestFocus();
      }
    }
    
    rafraichirBoutonValider();
    rafraichirBoutonSupprimerCommentaire();
  }
  
  private void rafraichirTexte() {
    taTexteCommentaire.setText(getModele().getTexteCommentaire());
    if (getModele().getMode() != ModeleDetailCommentaire.MODE_CONSULTER_LIGNE_COMMENTAIRE) {
      taTexteCommentaire.setEnabled(true);
    }
    else {
      taTexteCommentaire.setEnabled(false);
    }
  }
  
  private void rafraichirPanelEdition() {
    pnlEdition.setVisible(getModele().isOptionsEditionVisible());
    if (getModele().isOptionsEditionVisible()) {
      rafraichirEditionSurDoc();
      rafraichirEditionTousDoc();
    }
  }
  
  private void rafraichirEditionSurDoc() {
    rbEditionSurDoc.setSelected(!getModele().isEditionTousDocument());
  }
  
  private void rafraichirEditionTousDoc() {
    rbEditionTousDoc.setSelected(getModele().isEditionTousDocument());
  }
  
  private void rafraichirConserverCommentaire() {
    ckConserverCommentaire.setVisible(
        getModele().isCommentaireSauvegardable() && (getModele().getMode() != ModeleDetailCommentaire.MODE_CONSULTER_LIGNE_COMMENTAIRE));
    ckConserverCommentaire.setSelected(getModele().isConserverCommentaire());
  }
  
  private void rafraichirCopierSurDocumentAchats() {
    ckCopierSurDocumentAchat
        .setVisible(getModele().getContexte().equals(EnumContexteMetier.CONTEXTE_VENTE) && getModele().isDirectUsine());
    ckCopierSurDocumentAchat.setSelected(getModele().isCopierSurDocumentAchats());
  }
  
  private void rafraichirCodeCommentaire() {
    tfCodeCommentaire.setText(getModele().getCodeCommentaire());
    if (getModele().isCommentaireSauvegardable()) {
      lbCodeCommentaire.setVisible(getModele().isConserverCommentaire());
      tfCodeCommentaire.setVisible(getModele().isConserverCommentaire());
    }
    else {
      lbCodeCommentaire.setVisible(false);
      tfCodeCommentaire.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le bouton valider.
   * Cette méthode est appelée à chaque frappe de touche dans les champs "Texte commentaire" et "Code commentaire". Cela permet
   * de griser ou non le bouton "Valider" dynamiquement en fonction de la saisie.
   */
  private void rafraichirBoutonValider() {
    boolean actif = true;
    if (taTexteCommentaire.getText().trim().isEmpty()) {
      // Impossible de valider si le texte du commentaire est vide
      actif = false;
    }
    else if (ckConserverCommentaire.isSelected() && tfCodeCommentaire.getText().trim().isEmpty()) {
      actif = false;
    }
    else if (getModele().getMode() == ModeleDetailCommentaire.MODE_CONSULTER_LIGNE_COMMENTAIRE) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Rafraichir le bouton supprimer le commentaire.
   */
  private void rafraichirBoutonSupprimerCommentaire() {
    boolean actif = true;
    if (getModele().getMode() != ModeleDetailCommentaire.MODE_AJOUTER_ARTICLE_COMMENTAIRE) {
      // Le commentaire n'est supprimable que si on manuipule un article commentaire
      actif = false;
    }
    else if (tfCodeCommentaire.getText().trim().isEmpty()) {
      // Le code ne doit pas être vide
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER_COMMENTAIRE, actif);
  }
  
  // -- Méthodes interractives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER_COMMENTAIRE)) {
        getModele().quitterAvecSuppression();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void taTexteCommentaireKeyReleased(KeyEvent e) {
    try {
      // Rafraichir uniquement les boutons en cours de saisie
      rafraichirBoutonValider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void taTexteCommentaireFocusLost(FocusEvent e) {
    try {
      getModele().modifierTexteCommentaire(taTexteCommentaire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfCodeCommentaireKeyReleased(KeyEvent e) {
    try {
      // Rafraichir uniquement les boutons en cours de saisie
      rafraichirBoutonValider();
      rafraichirBoutonSupprimerCommentaire();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfCodeCommentaireFocusLost(FocusEvent e) {
    try {
      getModele().modifierCodeCommentaire(tfCodeCommentaire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckConserverCommentaireItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierConserverCommentaire(ckConserverCommentaire.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbEditionSurDocActionPerformed(ActionEvent e) {
    try {
      getModele().modifierEditionTousDocuments(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbEditionTousDocActionPerformed(ActionEvent e) {
    try {
      getModele().modifierEditionTousDocuments(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckCopierSurDocumentAchatStateChanged(ChangeEvent e) {
    try {
      getModele().modifierCopierSurDocumentAchat(ckCopierSurDocumentAchat.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    lbTexteCommentaire = new SNLabelTitre();
    scpDefilement = new JScrollPane();
    taTexteCommentaire = new RiTextArea();
    pnlEdition = new JPanel();
    rbEditionSurDoc = new JRadioButton();
    rbEditionTousDoc = new JRadioButton();
    pnlConservation = new SNPanel();
    ckConserverCommentaire = new XRiCheckBox();
    lbCodeCommentaire = new SNLabelChamp();
    tfCodeCommentaire = new XRiTextField();
    ckCopierSurDocumentAchat = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setTitle("D\u00e9tail d'un commentaire");
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(850, 355));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTexteCommentaire ----
        lbTexteCommentaire.setText("Texte du commentaire");
        lbTexteCommentaire.setPreferredSize(new Dimension(152, 30));
        lbTexteCommentaire.setName("lbTexteCommentaire");
        pnlContenu.add(lbTexteCommentaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpDefilement ========
        {
          scpDefilement.setName("scpDefilement");
          
          // ---- taTexteCommentaire ----
          taTexteCommentaire.setBorder(new BevelBorder(BevelBorder.LOWERED));
          taTexteCommentaire.setWrapStyleWord(true);
          taTexteCommentaire.setLineWrap(true);
          taTexteCommentaire.setFont(taTexteCommentaire.getFont().deriveFont(taTexteCommentaire.getFont().getSize() + 3f));
          taTexteCommentaire.setOpaque(true);
          taTexteCommentaire.setName("taTexteCommentaire");
          taTexteCommentaire.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              taTexteCommentaireFocusLost(e);
            }
          });
          taTexteCommentaire.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              taTexteCommentaireKeyReleased(e);
            }
          });
          scpDefilement.setViewportView(taTexteCommentaire);
        }
        pnlContenu.add(scpDefilement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlEdition ========
        {
          pnlEdition.setBorder(new TitledBorder(""));
          pnlEdition.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlEdition.setOpaque(false);
          pnlEdition.setMinimumSize(new Dimension(600, 45));
          pnlEdition.setPreferredSize(new Dimension(600, 45));
          pnlEdition.setMaximumSize(new Dimension(600, 45));
          pnlEdition.setName("pnlEdition");
          pnlEdition.setLayout(new GridLayout());
          
          // ---- rbEditionSurDoc ----
          rbEditionSurDoc.setText("Edition uniquement sur ce document");
          rbEditionSurDoc.setFont(rbEditionSurDoc.getFont().deriveFont(rbEditionSurDoc.getFont().getSize() + 2f));
          rbEditionSurDoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbEditionSurDoc.setName("rbEditionSurDoc");
          rbEditionSurDoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbEditionSurDocActionPerformed(e);
            }
          });
          pnlEdition.add(rbEditionSurDoc);
          
          // ---- rbEditionTousDoc ----
          rbEditionTousDoc.setText("Edition sur tous les documents");
          rbEditionTousDoc.setFont(rbEditionTousDoc.getFont().deriveFont(rbEditionTousDoc.getFont().getSize() + 2f));
          rbEditionTousDoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          rbEditionTousDoc.setName("rbEditionTousDoc");
          rbEditionTousDoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              rbEditionTousDocActionPerformed(e);
            }
          });
          pnlEdition.add(rbEditionTousDoc);
        }
        pnlContenu.add(pnlEdition, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlConservation ========
        {
          pnlConservation.setName("pnlConservation");
          pnlConservation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConservation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlConservation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlConservation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlConservation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- ckConserverCommentaire ----
          ckConserverCommentaire.setText("<html>Conserver ce commentaire</html>");
          ckConserverCommentaire.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckConserverCommentaire.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckConserverCommentaire.setMinimumSize(new Dimension(250, 30));
          ckConserverCommentaire.setPreferredSize(new Dimension(250, 30));
          ckConserverCommentaire.setName("ckConserverCommentaire");
          ckConserverCommentaire.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              ckConserverCommentaireItemStateChanged(e);
            }
          });
          pnlConservation.add(ckConserverCommentaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbCodeCommentaire ----
          lbCodeCommentaire.setMaximumSize(new Dimension(200, 30));
          lbCodeCommentaire.setMinimumSize(new Dimension(200, 30));
          lbCodeCommentaire.setPreferredSize(new Dimension(200, 30));
          lbCodeCommentaire.setText("Code du commentaire");
          lbCodeCommentaire.setName("lbCodeCommentaire");
          pnlConservation.add(lbCodeCommentaire, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfCodeCommentaire ----
          tfCodeCommentaire.setFont(tfCodeCommentaire.getFont().deriveFont(tfCodeCommentaire.getFont().getSize() + 2f));
          tfCodeCommentaire.setMinimumSize(new Dimension(300, 30));
          tfCodeCommentaire.setPreferredSize(new Dimension(300, 30));
          tfCodeCommentaire.setMaximumSize(new Dimension(300, 30));
          tfCodeCommentaire.setName("tfCodeCommentaire");
          tfCodeCommentaire.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfCodeCommentaireFocusLost(e);
            }
          });
          tfCodeCommentaire.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfCodeCommentaireKeyReleased(e);
            }
          });
          pnlConservation.add(tfCodeCommentaire, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- ckCopierSurDocumentAchat ----
          ckCopierSurDocumentAchat.setText("<html>Copier sur le document d\u2019achats</html>");
          ckCopierSurDocumentAchat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckCopierSurDocumentAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckCopierSurDocumentAchat.setMinimumSize(new Dimension(250, 30));
          ckCopierSurDocumentAchat.setPreferredSize(new Dimension(250, 30));
          ckCopierSurDocumentAchat.setName("ckCopierSurDocumentAchat");
          ckCopierSurDocumentAchat.addChangeListener(new ChangeListener() {
            @Override
            public void stateChanged(ChangeEvent e) {
              ckCopierSurDocumentAchatStateChanged(e);
            }
          });
          pnlConservation.add(ckCopierSurDocumentAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlConservation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- btgMode ----
    btgMode.add(rbEditionSurDoc);
    btgMode.add(rbEditionTousDoc);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbTexteCommentaire;
  private JScrollPane scpDefilement;
  private RiTextArea taTexteCommentaire;
  private JPanel pnlEdition;
  private JRadioButton rbEditionSurDoc;
  private JRadioButton rbEditionTousDoc;
  private SNPanel pnlConservation;
  private XRiCheckBox ckConserverCommentaire;
  private SNLabelChamp lbCodeCommentaire;
  private XRiTextField tfCodeCommentaire;
  private XRiCheckBox ckCopierSurDocumentAchat;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
