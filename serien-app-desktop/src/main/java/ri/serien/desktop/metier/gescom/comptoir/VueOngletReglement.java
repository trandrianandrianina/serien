/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.ImageIcon;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextPane;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.reglement.Acompte;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

public class VueOngletReglement extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final String BOUTON_DIFFERER_REGLEMENT = "Différer le règlement";
  private static final String BOUTON_DIFFERER_AVOIR = "Différer l'avoir";
  private static final String BOUTON_REGLER_COMPTANT = "Régler comptant";
  private static final String BOUTON_CREER_CLIENT_COMPTANT = "Créer un client comptant";
  private static final String BOUTON_EFFACER_REGLEMENT = "Effacer réglements";
  private static final String[] TITRE_LISTE_FACTURES =
      new String[] { "N° facture", "Type", "Date", "Montant HT", "Montant TTC", "Reste à payer", "R\u00e9f\u00e9rence longue" };
  
  // Variables
  private ModeleComptoir modele = null;
  protected boolean executerEvenements = false;
  
  /**
   * Constructeur.
   */
  public VueOngletReglement(ModeleComptoir acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Création de la table contenant la liste des factures et des avoirs non réglés
    scpFacturesNonReglees.getViewport().setBackground(Color.WHITE);
    tblFacturesNonReglees.personnaliserAspect(TITRE_LISTE_FACTURES, new int[] { 80, 80, 100, 120, 120, 120, 250 },
        new int[] { 80, 80, 100, 140, 140, 140, 2000 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE,
            NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE },
        14);
    tblFacturesNonReglees.personnaliserAspectCellules(new JTableCellFactureNonRegleeRenderer());
    
    // Ajout d'un évènement afin de gérer la pagination "intelligente" avec la scrollbar des devis client
    scpFacturesNonReglees.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent arg0) {
        // Si l'écran n'est pas initialisé, on ignore cet évènement
        if (!executerEvenements) {
          return;
        }
        // Traitement à effectuer si l'évènement est activé
        int longueurBarre = scpFacturesNonReglees.getVerticalScrollBar().getModel().getExtent();
        if (((scpFacturesNonReglees.getVerticalScrollBar().getValue() + longueurBarre) == scpFacturesNonReglees.getVerticalScrollBar()
            .getMaximum())) {
          Trace.info("Charger les documents factures non reglees A FAIRE");
        }
      }
    });
    
    // Raccourcis clavier
    ckSelectionnerToutesFactures.setMnemonic(KeyEvent.VK_T);
    btReglerParCheque1.setMnemonic(KeyEvent.VK_H);
    btReglerParCarteBancaire1.setMnemonic(KeyEvent.VK_B);
    btReglerEnEspeces1.setMnemonic(KeyEvent.VK_S);
    btReglerParVirement1.setMnemonic(KeyEvent.VK_V);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_DIFFERER_REGLEMENT, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DIFFERER_AVOIR, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_REGLER_COMPTANT, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_CLIENT_COMPTANT, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_EFFACER_REGLEMENT, 'e', false);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    executerEvenements = true;
    
    // Panneau règlement
    rafraichirMontantTotalARegler();
    rafraichirMontantAcompte();
    rafraichirMontantDejaRegle();
    rafraichirMontantResteARegler();
    
    // Panneau saisie des paiements
    rafraichirMontantReglement();
    rafraichirMontantARendre();
    rafraichirDescriptionReglements();
    rafraichirBoutonReglerParCarteBancaire();
    rafraichirBoutonReglerParEspece();
    rafraichirBoutonReglerParCheque();
    rafraichirBoutonReglerParVirement();
    
    // Panneau liste des factures
    rafraichirPanneauFacture();
    
    // Boutons
    rafraichirBoutonEffacerReglement();
    rafraichirBoutonReglerComptant();
    rafraichirBoutonCreerClientComptant();
    rafraichirBoutonDiffererReglement();
    rafraichirBoutonDiffererAvoir();
    
    // Positionner le focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleComptoir.FOCUS_REGLEMENTS_MONTANT_REGLEMENT:
        tfMontantReglement.requestFocus();
        break;
    }
    
    executerEvenements = false;
  }
  
  private void rafraichirMontantTotalARegler() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null) {
      zsTotalARegler.setText(Constantes.formater(calculReglement.getMontantTotal(), true));
    }
    else {
      zsTotalARegler.setText("");
    }
  }
  
  private void rafraichirMontantAcompte() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null && modele.isAcompteARegler()) {
      Acompte acompte = calculReglement.getAcompte();
      if (acompte != null && acompte.getMontant().compareTo(BigDecimal.ZERO) != 0) {
        lbMontantAcompte.setVisible(true);
        zsMontantAcompte.setVisible(true);
        zsMontantAcompte.setText(Constantes.formater(acompte.getMontant(), true));
        
        String libelleAcompte;
        if (modele.isAcompteObligatoire()) {
          libelleAcompte = "Acompte obligatoire<br>";
        }
        else {
          libelleAcompte = "Acompte facultatif<br>";
        }
        BigDecimal pourcentageAcompte = calculReglement.getPourcentageAcompte();
        if (pourcentageAcompte != null && pourcentageAcompte.compareTo(BigDecimal.ZERO) != 0) {
          libelleAcompte += " de " + Constantes.formater(pourcentageAcompte, true) + "%";
        }
        lbMontantAcompte.setText("<html>" + libelleAcompte + "</html>");
      }
      else {
        lbMontantAcompte.setVisible(false);
        lbMontantAcompte.setText("");
        zsMontantAcompte.setVisible(false);
        zsMontantAcompte.setText("");
      }
    }
    else {
      lbMontantAcompte.setVisible(false);
      lbMontantAcompte.setText("");
      zsMontantAcompte.setVisible(false);
      zsMontantAcompte.setText("");
    }
  }
  
  private void rafraichirMontantDejaRegle() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null) {
      zsMontantDejaRegle.setText(Constantes.formater(calculReglement.getMontantRegle(), true));
    }
    else {
      zsMontantDejaRegle.setText("");
    }
  }
  
  private void rafraichirMontantResteARegler() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null) {
      zsResteARegler.setText(Constantes.formater(calculReglement.getResteAReglerSurTotal(), true));
    }
    else {
      zsResteARegler.setText("");
    }
  }
  
  private void rafraichirMontantReglement() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null && !modele.isDiffererAvoir()) {
      if (modele.isReglementPossible()) {
        tfMontantReglement.setText(Constantes.formater(calculReglement.getMontantARegler(), true));
      }
      else {
        tfMontantReglement.setText("");
      }
      tfMontantReglement.setEnabled(modele.isReglementPossible());
    }
    else {
      tfMontantReglement.setText("");
      tfMontantReglement.setEnabled(false);
    }
  }
  
  private void rafraichirMontantARendre() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null) {
      if (!calculReglement.isRemboursement()) {
        zsMontantARendre.setText(Constantes.formater(calculReglement.getMontantARendre(), true));
        zsMontantARendre.setVisible(true);
        lbMontantARendre.setVisible(true);
      }
      else {
        zsMontantARendre.setText("");
        zsMontantARendre.setVisible(false);
        lbMontantARendre.setVisible(false);
      }
    }
    else {
      zsMontantARendre.setText("");
      zsMontantARendre.setEnabled(false);
      zsMontantARendre.setVisible(true);
      lbMontantARendre.setVisible(true);
    }
  }
  
  private void rafraichirDescriptionReglements() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (calculReglement != null) {
      tpDescriptionReglements.setText(calculReglement.getNote());
    }
    else {
      tpDescriptionReglements.setText("");
    }
  }
  
  private void rafraichirBoutonReglerParCarteBancaire() {
    boolean actif = false;
    
    // Interdire le paiement s'il s'agit d'un avoir que l'on a différé
    if (modele.isDiffererAvoir()) {
      actif = false;
    }
    // Autoriser le paiement si le règlement est possible
    else if (modele.isReglementPossible()) {
      actif = true;
    }
    
    btReglerParCarteBancaire1.setEnabled(actif);
  }
  
  private void rafraichirBoutonReglerParEspece() {
    boolean actif = false;
    
    // Interdire le paiement s'il s'agit d'un avoir que l'on a différé
    if (modele.isDiffererAvoir()) {
      actif = false;
    }
    // Autoriser le paiement si le règlement est possible
    else if (modele.isReglementPossible()) {
      actif = true;
    }
    
    btReglerEnEspeces1.setEnabled(actif);
  }
  
  private void rafraichirBoutonReglerParCheque() {
    boolean actif = false;
    // Interdire le paiement s'il s'agit d'un avoir que l'on a différé
    if (modele.isDiffererAvoir()) {
      actif = false;
    }
    // Interdire le paiement si c'est interdit
    else if (modele.isChequeInterdit()) {
      actif = false;
    }
    // Autoriser le paiement si le règlement est possible
    else if (modele.isReglementPossible()) {
      actif = true;
    }
    
    btReglerParCheque1.setEnabled(actif);
  }
  
  private void rafraichirBoutonReglerParVirement() {
    boolean actif = false;
    
    // Contôle les droits de l'utilisateur (il doit avoir les droits direction pour effectuer des virements)
    if (!modele.isUtilisateurADroitDirection()) {
      btReglerParVirement1.setVisible(false);
      lbParVirement.setVisible(false);
      return;
    }
    
    // Si l'utilisateur a les droits direction
    btReglerParVirement1.setVisible(true);
    lbParVirement.setVisible(true);
    // Interdire le paiement s'il s'agit d'un avoir que l'on a différé
    if (modele.isDiffererAvoir()) {
      actif = false;
    }
    // Autoriser le paiement si le règlement est possible
    else if (modele.isReglementPossible()) {
      actif = true;
    }
    
    btReglerParVirement1.setEnabled(actif);
  }
  
  private void rafraichirPanneauFacture() {
    CalculReglement calculReglement = modele.getCalculReglement();
    if (modele.getListeFactureNonReglee() != null && !modele.getListeFactureNonReglee().isEmpty()
        && !calculReglement.isEntierementRegle()) {
      pnlFacturesNonReglees.setVisible(true);
      
      rafraichirSelectionToutesFactures();
      rafraichirListeFacturesNonReglees();
    }
    else {
      pnlFacturesNonReglees.setVisible(false);
    }
  }
  
  private void rafraichirSelectionToutesFactures() {
    // Cocher la case si toutes les factures sont sélectionnées
    ListeDocumentVenteBase listeFactureNonReglee = modele.getListeFactureNonReglee();
    if (listeFactureNonReglee != null && listeFactureNonReglee.isSelectionComplete()) {
      ckSelectionnerToutesFactures.setSelected(true);
    }
    else {
      ckSelectionnerToutesFactures.setSelected(false);
    }
  }
  
  /**
   * Met à jour les documents de type factures non réglées.
   */
  private void rafraichirListeFacturesNonReglees() {
    // Récupérer la liste des factures en cours (non réglées)
    ListeDocumentVenteBase listeFactureNonReglee = modele.getListeFactureNonReglee();
    
    // Masquer le tableau
    if (listeFactureNonReglee == null || listeFactureNonReglee.isEmpty()) {
      pnlFacturesNonReglees.setVisible(false);
      return;
    }
    pnlFacturesNonReglees.setVisible(true);
    
    // Remplir le tableau de données
    String[][] donnees = null;
    donnees = new String[listeFactureNonReglee.size()][TITRE_LISTE_FACTURES.length];
    for (int ligne = 0; ligne < listeFactureNonReglee.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeFactureNonReglee.get(ligne);
      LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
      if (ligneFacture != null) {
        // Identifiant du document de vente
        donnees[ligne][0] = documentVenteBase.getId().toString();
        
        // Type de document: facture ou avoir
        donnees[ligne][1] = ligneFacture.getLibelleTypeDocument();
        
        // Date de création
        donnees[ligne][2] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
        
        // Total HT
        donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        
        // Total TTC
        donnees[ligne][4] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
        
        // Reste à régler
        donnees[ligne][5] = Constantes.formater(ligneFacture.getResteAPayerTTC(), true);
        
        // Référence longue
        donnees[ligne][6] = documentVenteBase.getReferenceLongue();
      }
    }
    tblFacturesNonReglees.mettreAJourDonnees(donnees);
    
    // Recalcule la longueur de la liste afin d'avoir l'ascenceur uniquement quand c'est nécessaire
    tblFacturesNonReglees.setPreferredSize(new Dimension(tblFacturesNonReglees.getPreferredSize().width,
        listeFactureNonReglee.size() * tblFacturesNonReglees.getRowHeight()));
    
    // Sélectionne les lignes
    List<Integer> listeIndexSelectionne = listeFactureNonReglee.getListeIndexSelection();
    for (Integer index : listeIndexSelectionne) {
      tblFacturesNonReglees.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  private void rafraichirBoutonReglerComptant() {
    boolean actif = true;
    // Ne pas activer le bouton si on est déjà en paiement comptant
    if (modele.isModePaiementComptant()) {
      actif = false;
    }
    // Ne pas activer le bouton si on est en acompte obligatoire
    else if (modele.isAcompteARegler()) {
      actif = false;
    }
    // Ne pas activer le bouton s'il ne reste plus rien à payer
    else if (modele.getCalculReglement() != null && modele.getCalculReglement().isEntierementRegle()) {
      actif = false;
    }
    // Ne pas activer le bouton si le document n'est pas modifiable
    else if (!modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_REGLER_COMPTANT, actif);
  }
  
  private void rafraichirBoutonEffacerReglement() {
    boolean actif = true;
    if (!modele.isPossibleEffacerSaisieReglement() && !modele.isPossibleEffacerToutReglement()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EFFACER_REGLEMENT, actif);
  }
  
  private void rafraichirBoutonCreerClientComptant() {
    boolean actif = true;
    // Ne pas activer le bouton si le document n'est pas modifiable
    if (!modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    // Ne pas activer le bouton si on a pas les droits pour modifier un client en compte
    else if (!modele.isClientEnCompteModifiable()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_CLIENT_COMPTANT, actif);
  }
  
  private void rafraichirBoutonDiffererReglement() {
    boolean actif = true;
    // Ne pas activer le bouton si le réglement différé n'est pas possible
    if (!modele.isDiffererReglementsEstPossible()) {
      actif = false;
    }
    // Ne pas activer le bouton si le paiement vient d'être différé
    else if (modele.isDiffererPaiement()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DIFFERER_REGLEMENT, actif);
  }
  
  private void rafraichirBoutonDiffererAvoir() {
    boolean actif = true;
    // Ne pas activer le bouton si l'avoir a déjà été différé ou si la création de l'avoir en différé n'est pas possible
    if (modele.isDiffererAvoir() || !modele.isDiffererAvoirEstPossible()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DIFFERER_AVOIR, actif);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.annulerDocumentVente();
      }
      else if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        modele.validerOngletReglement();
      }
      else if (pSNBouton.isBouton(BOUTON_EFFACER_REGLEMENT)) {
        modele.effacerReglement();
      }
      else if (pSNBouton.isBouton(BOUTON_DIFFERER_REGLEMENT)) {
        modele.reglerEnDifferer();
      }
      else if (pSNBouton.isBouton(BOUTON_REGLER_COMPTANT)) {
        modele.activerPaiementComptant();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_CLIENT_COMPTANT)) {
        modele.changerClient();
      }
      else if (pSNBouton.isBouton(BOUTON_DIFFERER_AVOIR)) {
        modele.differerAvoir();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btReglerParCheque1ActionPerformed(ActionEvent e) {
    try {
      modele.payerParCheque(tfMontantReglement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btReglerParCarteBancaire1ActionPerformed(ActionEvent e) {
    try {
      modele.payerParCarteBancaire(tfMontantReglement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btReglerEnEspeces1ActionPerformed(ActionEvent e) {
    try {
      modele.payerEnEspeces(tfMontantReglement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btReglerParVirement1ActionPerformed(ActionEvent e) {
    try {
      modele.payerParVirement(tfMontantReglement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReglementActionPerformed(ActionEvent e) {
    try {
      modele.controlerOngletReglement();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckSelectionnerToutesFacturesActionPerformed(ActionEvent e) {
    try {
      modele.selectionnerToutesFacturesNonReglees(ckSelectionnerToutesFactures.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblFacturesNonRegleesMouseClicked(MouseEvent e) {
    try {
      int[] listeSelection = tblFacturesNonReglees.getSelectedRows();
      if (tblFacturesNonReglees.getRowSorter() != null) {
        for (int i = 0; i < listeSelection.length; i++) {
          listeSelection[i] = tblFacturesNonReglees.getRowSorter().convertRowIndexToModel(listeSelection[i]);
        }
      }
      modele.selectionnerFacturesNonReglees(listeSelection);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlMontants = new SNPanelTitre();
    lbTotalARegler = new SNLabelChamp();
    zsTotalARegler = new RiZoneSortie();
    lbMontantDejaRegle = new SNLabelChamp();
    zsMontantDejaRegle = new RiZoneSortie();
    lbResteARegler = new SNLabelChamp();
    zsResteARegler = new RiZoneSortie();
    lbMontantAcompte = new SNLabelChamp();
    zsMontantAcompte = new RiZoneSortie();
    pnlFacturesNonReglees = new SNPanelTitre();
    scpFacturesNonReglees = new JScrollPane();
    tblFacturesNonReglees = new NRiTable();
    ckSelectionnerToutesFactures = new JCheckBox();
    pnlSaisieReglements = new SNPanel();
    pnlModeReglementComptant = new SNPanelTitre();
    lbMontantReglement = new SNLabelChamp();
    tfMontantReglement = new XRiTextField();
    pnlTypesReglement = new JPanel();
    lbParCarteBancaire = new JLabel();
    btReglerParCarteBancaire1 = new SNBoutonLeger();
    lbEnEspeces = new JLabel();
    btReglerEnEspeces1 = new SNBoutonLeger();
    lbParCheque = new JLabel();
    btReglerParCheque1 = new SNBoutonLeger();
    lbParVirement = new JLabel();
    btReglerParVirement1 = new SNBoutonLeger();
    lbMontantARendre = new JLabel();
    zsMontantARendre = new RiZoneSortie();
    pnDescriptionReglements = new SNPanel();
    lbDescriptionReglements = new SNLabelTitre();
    tpDescriptionReglements = new JTextPane();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 650));
    setPreferredSize(new Dimension(1240, 650));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlMontants ========
      {
        pnlMontants.setTitre("R\u00e9capitulatif");
        pnlMontants.setName("pnlMontants");
        pnlMontants.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMontants.getLayout()).columnWidths = new int[] { 155, 150, 0 };
        ((GridBagLayout) pnlMontants.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlMontants.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlMontants.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTotalARegler ----
        lbTotalARegler.setText("Total \u00e0 r\u00e9gler");
        lbTotalARegler.setName("lbTotalARegler");
        pnlMontants.add(lbTotalARegler, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsTotalARegler ----
        zsTotalARegler.setFont(zsTotalARegler.getFont().deriveFont(zsTotalARegler.getFont().getStyle() & ~Font.BOLD,
            zsTotalARegler.getFont().getSize() + 3f));
        zsTotalARegler.setHorizontalAlignment(SwingConstants.RIGHT);
        zsTotalARegler.setMinimumSize(new Dimension(150, 35));
        zsTotalARegler.setPreferredSize(new Dimension(150, 35));
        zsTotalARegler.setMaximumSize(new Dimension(2147483647, 35));
        zsTotalARegler.setName("zsTotalARegler");
        pnlMontants.add(zsTotalARegler, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMontantDejaRegle ----
        lbMontantDejaRegle.setText("Montant d\u00e9j\u00e0 r\u00e9gl\u00e9");
        lbMontantDejaRegle.setName("lbMontantDejaRegle");
        pnlMontants.add(lbMontantDejaRegle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsMontantDejaRegle ----
        zsMontantDejaRegle.setHorizontalAlignment(SwingConstants.RIGHT);
        zsMontantDejaRegle.setFont(zsMontantDejaRegle.getFont().deriveFont(zsMontantDejaRegle.getFont().getSize() + 3f));
        zsMontantDejaRegle.setMinimumSize(new Dimension(150, 35));
        zsMontantDejaRegle.setPreferredSize(new Dimension(150, 35));
        zsMontantDejaRegle.setMaximumSize(new Dimension(2147483647, 35));
        zsMontantDejaRegle.setName("zsMontantDejaRegle");
        pnlMontants.add(zsMontantDejaRegle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbResteARegler ----
        lbResteARegler.setText("Reste \u00e0 r\u00e9gler");
        lbResteARegler.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbResteARegler.setName("lbResteARegler");
        pnlMontants.add(lbResteARegler, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsResteARegler ----
        zsResteARegler.setFont(zsResteARegler.getFont().deriveFont(zsResteARegler.getFont().getStyle() | Font.BOLD,
            zsResteARegler.getFont().getSize() + 3f));
        zsResteARegler.setHorizontalAlignment(SwingConstants.RIGHT);
        zsResteARegler.setMinimumSize(new Dimension(150, 35));
        zsResteARegler.setPreferredSize(new Dimension(150, 35));
        zsResteARegler.setMaximumSize(new Dimension(2147483647, 35));
        zsResteARegler.setName("zsResteARegler");
        pnlMontants.add(zsResteARegler, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMontantAcompte ----
        lbMontantAcompte.setText("Acompte");
        lbMontantAcompte.setName("lbMontantAcompte");
        pnlMontants.add(lbMontantAcompte, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- zsMontantAcompte ----
        zsMontantAcompte.setFont(zsMontantAcompte.getFont().deriveFont(zsMontantAcompte.getFont().getStyle() & ~Font.BOLD,
            zsMontantAcompte.getFont().getSize() + 3f));
        zsMontantAcompte.setHorizontalAlignment(SwingConstants.RIGHT);
        zsMontantAcompte.setMinimumSize(new Dimension(150, 35));
        zsMontantAcompte.setPreferredSize(new Dimension(150, 35));
        zsMontantAcompte.setMaximumSize(new Dimension(2147483647, 35));
        zsMontantAcompte.setName("zsMontantAcompte");
        pnlMontants.add(zsMontantAcompte, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMontants, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlFacturesNonReglees ========
      {
        pnlFacturesNonReglees.setTitre("Liste des factures et des avoirs non r\u00e9gl\u00e9s");
        pnlFacturesNonReglees.setName("pnlFacturesNonReglees");
        pnlFacturesNonReglees.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlFacturesNonReglees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlFacturesNonReglees.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlFacturesNonReglees.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlFacturesNonReglees.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        
        // ======== scpFacturesNonReglees ========
        {
          scpFacturesNonReglees.setPreferredSize(new Dimension(800, 150));
          scpFacturesNonReglees.setName("scpFacturesNonReglees");
          scpFacturesNonReglees.setMinimumSize(new Dimension(800, 150));
          
          // ---- tblFacturesNonReglees ----
          tblFacturesNonReglees.setShowVerticalLines(true);
          tblFacturesNonReglees.setShowHorizontalLines(true);
          tblFacturesNonReglees.setBackground(Color.white);
          tblFacturesNonReglees.setOpaque(false);
          tblFacturesNonReglees.setRowHeight(20);
          tblFacturesNonReglees.setGridColor(new Color(204, 204, 204));
          tblFacturesNonReglees.setMinimumSize(new Dimension(600, 50));
          tblFacturesNonReglees.setPreferredSize(new Dimension(800, 110));
          tblFacturesNonReglees.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblFacturesNonReglees.setMaximumSize(new Dimension(2147483647, 200));
          tblFacturesNonReglees.setPreferredScrollableViewportSize(new Dimension(1000, 400));
          tblFacturesNonReglees.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
          tblFacturesNonReglees.setName("tblFacturesNonReglees");
          tblFacturesNonReglees.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblFacturesNonRegleesMouseClicked(e);
            }
          });
          scpFacturesNonReglees.setViewportView(tblFacturesNonReglees);
        }
        pnlFacturesNonReglees.add(scpFacturesNonReglees, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- ckSelectionnerToutesFactures ----
        ckSelectionnerToutesFactures
            .setText("<html>S\u00e9lectionner <u>t</u>outes les factures (les avoirs ne seront pas impact\u00e9s)</html>");
        ckSelectionnerToutesFactures.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckSelectionnerToutesFactures
            .setFont(ckSelectionnerToutesFactures.getFont().deriveFont(ckSelectionnerToutesFactures.getFont().getSize() + 2f));
        ckSelectionnerToutesFactures.setMinimumSize(new Dimension(150, 30));
        ckSelectionnerToutesFactures.setPreferredSize(new Dimension(150, 30));
        ckSelectionnerToutesFactures.setName("ckSelectionnerToutesFactures");
        ckSelectionnerToutesFactures.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ckSelectionnerToutesFacturesActionPerformed(e);
          }
        });
        pnlFacturesNonReglees.add(ckSelectionnerToutesFactures, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlFacturesNonReglees,
          new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlSaisieReglements ========
      {
        pnlSaisieReglements.setName("pnlSaisieReglements");
        pnlSaisieReglements.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSaisieReglements.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSaisieReglements.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlSaisieReglements.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlSaisieReglements.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== pnlModeReglementComptant ========
        {
          pnlModeReglementComptant.setTitre("Saisie des r\u00e8glements");
          pnlModeReglementComptant.setMinimumSize(new Dimension(798, 275));
          pnlModeReglementComptant.setPreferredSize(new Dimension(782, 275));
          pnlModeReglementComptant.setName("pnlModeReglementComptant");
          pnlModeReglementComptant.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlModeReglementComptant.getLayout()).columnWidths = new int[] { 155, 0, 0 };
          ((GridBagLayout) pnlModeReglementComptant.getLayout()).rowHeights = new int[] { 0, 0, 31, 0 };
          ((GridBagLayout) pnlModeReglementComptant.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlModeReglementComptant.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMontantReglement ----
          lbMontantReglement.setText("R\u00e8glement");
          lbMontantReglement.setImportanceMessage(EnumImportanceMessage.MOYEN);
          lbMontantReglement.setName("lbMontantReglement");
          pnlModeReglementComptant.add(lbMontantReglement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfMontantReglement ----
          tfMontantReglement.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMontantReglement.setFont(tfMontantReglement.getFont().deriveFont(tfMontantReglement.getFont().getStyle() | Font.BOLD,
              tfMontantReglement.getFont().getSize() + 3f));
          tfMontantReglement.setForeground(Color.black);
          tfMontantReglement.setMinimumSize(new Dimension(150, 35));
          tfMontantReglement.setPreferredSize(new Dimension(150, 35));
          tfMontantReglement.setMaximumSize(new Dimension(2147483647, 35));
          tfMontantReglement.setName("tfMontantReglement");
          tfMontantReglement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tfReglementActionPerformed(e);
            }
          });
          pnlModeReglementComptant.add(tfMontantReglement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTypesReglement ========
          {
            pnlTypesReglement.setOpaque(false);
            pnlTypesReglement.setName("pnlTypesReglement");
            pnlTypesReglement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTypesReglement.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTypesReglement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTypesReglement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTypesReglement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbParCarteBancaire ----
            lbParCarteBancaire.setText("<html><center>Carte <u>b</u>ancaire</center></html>");
            lbParCarteBancaire.setHorizontalAlignment(SwingConstants.CENTER);
            lbParCarteBancaire.setFont(lbParCarteBancaire.getFont().deriveFont(lbParCarteBancaire.getFont().getSize() + 2f));
            lbParCarteBancaire.setPreferredSize(new Dimension(100, 30));
            lbParCarteBancaire.setName("lbParCarteBancaire");
            pnlTypesReglement.add(lbParCarteBancaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- btReglerParCarteBancaire1 ----
            btReglerParCarteBancaire1.setIcon(new ImageIcon(getClass().getResource("/images/carte.jpg")));
            btReglerParCarteBancaire1.setMinimumSize(new Dimension(150, 120));
            btReglerParCarteBancaire1.setMaximumSize(new Dimension(150, 120));
            btReglerParCarteBancaire1.setPreferredSize(new Dimension(150, 120));
            btReglerParCarteBancaire1.setName("btReglerParCarteBancaire1");
            btReglerParCarteBancaire1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btReglerParCarteBancaire1ActionPerformed(e);
              }
            });
            pnlTypesReglement.add(btReglerParCarteBancaire1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbEnEspeces ----
            lbEnEspeces.setText("<html><center>E<u>s</u>p\u00e8ces</center></html>");
            lbEnEspeces.setHorizontalAlignment(SwingConstants.CENTER);
            lbEnEspeces.setFont(lbEnEspeces.getFont().deriveFont(lbEnEspeces.getFont().getSize() + 2f));
            lbEnEspeces.setPreferredSize(new Dimension(100, 30));
            lbEnEspeces.setName("lbEnEspeces");
            pnlTypesReglement.add(lbEnEspeces, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- btReglerEnEspeces1 ----
            btReglerEnEspeces1.setIcon(new ImageIcon(getClass().getResource("/images/especes.jpg")));
            btReglerEnEspeces1.setMinimumSize(new Dimension(150, 120));
            btReglerEnEspeces1.setMaximumSize(new Dimension(150, 120));
            btReglerEnEspeces1.setPreferredSize(new Dimension(150, 120));
            btReglerEnEspeces1.setName("btReglerEnEspeces1");
            btReglerEnEspeces1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btReglerEnEspeces1ActionPerformed(e);
              }
            });
            pnlTypesReglement.add(btReglerEnEspeces1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbParCheque ----
            lbParCheque.setText("<html><center>C<u>h</u>\u00e8que</center></html>");
            lbParCheque.setHorizontalAlignment(SwingConstants.CENTER);
            lbParCheque.setFont(lbParCheque.getFont().deriveFont(lbParCheque.getFont().getSize() + 2f));
            lbParCheque.setName("lbParCheque");
            lbParCheque.setPreferredSize(new Dimension(100, 30));
            pnlTypesReglement.add(lbParCheque, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- btReglerParCheque1 ----
            btReglerParCheque1.setIcon(new ImageIcon(getClass().getResource("/images/cheque.jpg")));
            btReglerParCheque1.setMinimumSize(new Dimension(150, 120));
            btReglerParCheque1.setMaximumSize(new Dimension(150, 150));
            btReglerParCheque1.setPreferredSize(new Dimension(150, 120));
            btReglerParCheque1.setName("btReglerParCheque1");
            btReglerParCheque1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btReglerParCheque1ActionPerformed(e);
              }
            });
            pnlTypesReglement.add(btReglerParCheque1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbParVirement ----
            lbParVirement.setText("<html><center><u>V</u>irement</center></html>");
            lbParVirement.setHorizontalAlignment(SwingConstants.CENTER);
            lbParVirement.setFont(lbParVirement.getFont().deriveFont(lbParVirement.getFont().getSize() + 2f));
            lbParVirement.setName("lbParVirement");
            lbParVirement.setPreferredSize(new Dimension(100, 30));
            pnlTypesReglement.add(lbParVirement, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- btReglerParVirement1 ----
            btReglerParVirement1.setIcon(new ImageIcon(getClass().getResource("/images/virement.png")));
            btReglerParVirement1.setMinimumSize(new Dimension(150, 120));
            btReglerParVirement1.setMaximumSize(new Dimension(150, 150));
            btReglerParVirement1.setPreferredSize(new Dimension(150, 120));
            btReglerParVirement1.setName("btReglerParVirement1");
            btReglerParVirement1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                btReglerParVirement1ActionPerformed(e);
              }
            });
            pnlTypesReglement.add(btReglerParVirement1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlModeReglementComptant.add(pnlTypesReglement, new GridBagConstraints(1, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMontantARendre ----
          lbMontantARendre.setText("A rendre");
          lbMontantARendre.setFont(lbMontantARendre.getFont().deriveFont(lbMontantARendre.getFont().getStyle() | Font.BOLD,
              lbMontantARendre.getFont().getSize() + 2f));
          lbMontantARendre.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMontantARendre.setForeground(Color.red);
          lbMontantARendre.setPreferredSize(new Dimension(150, 30));
          lbMontantARendre.setMinimumSize(new Dimension(150, 30));
          lbMontantARendre.setMaximumSize(new Dimension(100, 30));
          lbMontantARendre.setName("lbMontantARendre");
          pnlModeReglementComptant.add(lbMontantARendre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsMontantARendre ----
          zsMontantARendre.setHorizontalAlignment(SwingConstants.RIGHT);
          zsMontantARendre.setText("0");
          zsMontantARendre.setFont(zsMontantARendre.getFont().deriveFont(zsMontantARendre.getFont().getStyle() | Font.BOLD,
              zsMontantARendre.getFont().getSize() + 7f));
          zsMontantARendre.setMinimumSize(new Dimension(150, 35));
          zsMontantARendre.setPreferredSize(new Dimension(150, 35));
          zsMontantARendre.setMaximumSize(new Dimension(2147483647, 35));
          zsMontantARendre.setForeground(Color.red);
          zsMontantARendre.setName("zsMontantARendre");
          pnlModeReglementComptant.add(zsMontantARendre, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlSaisieReglements.add(pnlModeReglementComptant, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnDescriptionReglements ========
        {
          pnDescriptionReglements.setMinimumSize(new Dimension(400, 275));
          pnDescriptionReglements.setPreferredSize(new Dimension(600, 275));
          pnDescriptionReglements.setName("pnDescriptionReglements");
          pnDescriptionReglements.setLayout(new BorderLayout());
          
          // ---- lbDescriptionReglements ----
          lbDescriptionReglements.setText("R\u00e8glements effectu\u00e9s");
          lbDescriptionReglements.setPreferredSize(new Dimension(150, 20));
          lbDescriptionReglements.setMaximumSize(new Dimension(150, 20));
          lbDescriptionReglements.setMinimumSize(new Dimension(150, 25));
          lbDescriptionReglements.setName("lbDescriptionReglements");
          pnDescriptionReglements.add(lbDescriptionReglements, BorderLayout.NORTH);
          
          // ---- tpDescriptionReglements ----
          tpDescriptionReglements.setBorder(new BevelBorder(BevelBorder.LOWERED));
          tpDescriptionReglements.setFont(
              new Font(Font.MONOSPACED, tpDescriptionReglements.getFont().getStyle(), tpDescriptionReglements.getFont().getSize() + 3));
          tpDescriptionReglements.setPreferredSize(new Dimension(600, 260));
          tpDescriptionReglements.setEditable(false);
          tpDescriptionReglements.setMargin(new Insets(5, 5, 5, 5));
          tpDescriptionReglements.setMinimumSize(new Dimension(400, 260));
          tpDescriptionReglements.setName("tpDescriptionReglements");
          pnDescriptionReglements.add(tpDescriptionReglements, BorderLayout.CENTER);
        }
        pnlSaisieReglements.add(pnDescriptionReglements, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSaisieReglements, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlMontants;
  private SNLabelChamp lbTotalARegler;
  private RiZoneSortie zsTotalARegler;
  private SNLabelChamp lbMontantDejaRegle;
  private RiZoneSortie zsMontantDejaRegle;
  private SNLabelChamp lbResteARegler;
  private RiZoneSortie zsResteARegler;
  private SNLabelChamp lbMontantAcompte;
  private RiZoneSortie zsMontantAcompte;
  private SNPanelTitre pnlFacturesNonReglees;
  private JScrollPane scpFacturesNonReglees;
  private NRiTable tblFacturesNonReglees;
  private JCheckBox ckSelectionnerToutesFactures;
  private SNPanel pnlSaisieReglements;
  private SNPanelTitre pnlModeReglementComptant;
  private SNLabelChamp lbMontantReglement;
  private XRiTextField tfMontantReglement;
  private JPanel pnlTypesReglement;
  private JLabel lbParCarteBancaire;
  private SNBoutonLeger btReglerParCarteBancaire1;
  private JLabel lbEnEspeces;
  private SNBoutonLeger btReglerEnEspeces1;
  private JLabel lbParCheque;
  private SNBoutonLeger btReglerParCheque1;
  private JLabel lbParVirement;
  private SNBoutonLeger btReglerParVirement1;
  private JLabel lbMontantARendre;
  private RiZoneSortie zsMontantARendre;
  private SNPanel pnDescriptionReglements;
  private SNLabelTitre lbDescriptionReglements;
  private JTextPane tpDescriptionReglements;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
