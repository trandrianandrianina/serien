/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.retourarticle;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.personnalisation.motifretour.ListeMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.motifretour.MotifRetour;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneRetourArticle;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.metier.vente.lot.ModeleLot;
import ri.serien.libswing.composant.metier.vente.lot.VueLot;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Expérimentation d'un modèle plus propre de modèle/vue - 10/10/2017
 * Ce dernière amène une structure un peu plus "rigide" et sutout le modèle n'implémente plus de classe Swing, il est totalement
 * indépendant de la vue.
 */
public class ModeleRetourArticle extends AbstractModeleDialogue {
  // Variables
  private Article article = null;
  private Client client = null;
  private ListeUnite listeUnite = null;
  private String libellePanelArticle = "";
  private ListeMotifRetour listeMotifs = null;
  private BigDecimal quantiteRetournee = null;
  private BigDecimal prixNetRetour = null;
  private MotifRetour motifRetour = null;
  private LigneVente ligneVenteOrigine = null;
  private DocumentVente documentEnCours = null;
  private ListeLigneRetourArticle listeLigneRetour = null;
  private ListeLigneRetourArticle listeLigneEnCours = null;
  private int indiceLigneSelectionnee = -1;
  private boolean isAffichageHistorique = false;
  private boolean isAffichageEnCours = true;
  private boolean isHistoriqueVide = false;
  
  /**
   * Constructeur.
   */
  public ModeleRetourArticle(SessionBase pSession, BigDecimal pQuantiteRetournee, Article pArticle, Client pClient,
      DocumentVente pDocument) {
    super(pSession);
    quantiteRetournee = pQuantiteRetournee;
    article = pArticle;
    client = pClient;
    documentEnCours = pDocument;
    
    // Retours impossible sur les directs usine
    if (documentEnCours.isDirectUsine()) {
      throw new MessageErreurException("Il est impossible de faire des retours sur un document direct usine.");
    }
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
  }
  
  @Override
  public void chargerDonnees() {
    if (quantiteRetournee == null || article == null) {
      quitterAvecAnnulation();
      return;
    }
    
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
    
    // Construction du titre suivant le nombre d'articles retournés
    if (quantiteRetournee.abs().equals(BigDecimal.ONE)) {
      libellePanelArticle = "Retour d'un article";
    }
    else {
      BigDecimal valeur = quantiteRetournee.abs();
      libellePanelArticle = String.format("Retour de %s articles", Constantes.formater(valeur, false));
    }
    
    // Chargement de la liste des motifs de retour (paramètre AV)
    listeMotifs = ListeMotifRetour.chargerTout(getIdSession(), article.getId().getIdEtablissement());
    if (listeMotifs == null || listeMotifs.isEmpty()) {
      throw new MessageErreurException(
          "La liste des motifs de retour est vide.\nVous ne pouvez pas valider de retour d'articles sans motif."
              + " Aller dans la personnalisation (paramètre AV) des ventes afin de les créer"
              + " ou pour vérifier qu'il y en ait au moins un avec l'option \"Avoir de type retour\" de cochée.");
    }
    
    // Chargement des documents contenant l'article pour le client en cours
    listeLigneRetour =
        ManagerServiceDocumentVente.chargerListeLigneRetourArticle(getIdSession(), quantiteRetournee, article, client, null);
  }
  
  @Override
  public void quitterAvecValidation() {
    // Avant validation on contrôle si la quantité retournée est affectable sur le document choisi
    // Si document origine (suffixe == 0) on compare à la quantité d'origine
    if (ligneVenteOrigine.getId().getSuffixe().intValue() == 0 && quantiteRetournee.abs()
        .compareTo(ligneVenteOrigine.getQuantiteDocumentOrigine().subtract(ligneVenteOrigine.getQuantiteDejaRetounee())) > 0) {
      rafraichir();
      throw new MessageErreurException("La quantité que vous voulez retourner est supérieure à la quantité restante sur ce document.");
    }
    // Si c'est un document issu d'une extraction (suffixe > 0) on compare à la quantité extraite
    else if (ligneVenteOrigine.getId().getSuffixe().intValue() > 0 && quantiteRetournee.abs()
        .compareTo(ligneVenteOrigine.getQuantiteDejaExtraite().subtract(ligneVenteOrigine.getQuantiteDejaRetounee())) > 0) {
      throw new MessageErreurException("La quantité que vous voulez retourner est supérieure à la quantité restante sur ce document.");
    }
    else {
      if (article.isDefiniNombreUVParUCV()) {
        quantiteRetournee = quantiteRetournee.multiply(article.getNombreUVParUCV());
      }
      // Si c'est un article loti
      Article article = new Article(ligneVenteOrigine.getIdArticle());
      article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
      if (article.isArticleLot()) {
        ModeleLot modeleLot = new ModeleLot(getSession(), ligneVenteOrigine.getId(), article, quantiteRetournee);
        VueLot vueLot = new VueLot(modeleLot);
        vueLot.afficher();
        if (!modeleLot.isSaisieLotComplete()) {
          throw new MessageErreurException(
              "Votre saisie est incomplète et ne sera pas enregistrée :\nPour les articles lotis, tous les retours d'articles doivent être affectés à un lot.");
        }
      }
      ManagerServiceDocumentVente.sauverLigneRetourArticle(getIdSession(), quantiteRetournee, prixNetRetour, motifRetour.getId(),
          ligneVenteOrigine, documentEnCours);
      super.quitterAvecValidation();
    }
  }
  
  /**
   * Charge l'historique des retours articles, si le document est vide , retourne l'ancienne liste.
   * Charge un indicateur permettant de savoir si l'historique est vide.
   */
  public void chargerHistorique() {
    ListeLigneRetourArticle listeAncienneLigneRetour = listeLigneRetour;
    listeLigneRetour =
        ManagerServiceDocumentVente.chargerListeLigneRetourArticle(getIdSession(), quantiteRetournee, article, client, listeLigneRetour);
    if (listeLigneRetour == null || listeLigneRetour.isEmpty()) {
      listeLigneRetour = listeAncienneLigneRetour;
      isHistoriqueVide = true;
    }
    else {
      isHistoriqueVide = false;
      // Les indicateurs du mode en cours ne sont changés que si l'historique n'est pas vide
      isAffichageEnCours = false;
      isAffichageHistorique = true;
    }
    rafraichir();
  }
  
  public void chargerDocumentEnCours() {
    listeLigneEnCours = new ListeLigneRetourArticle();
    isAffichageHistorique = false;
    isAffichageEnCours = true;
    listeLigneRetour =
        ManagerServiceDocumentVente.chargerListeLigneRetourArticle(getIdSession(), quantiteRetournee, article, client, listeLigneEnCours);
    rafraichir();
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourner la quantité d'origine d'une ligne
   */
  public String retournerQuantiteOrigine(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return null;
    }
    
    // Si c'est un document d'origine (suffixe à 0) on retourne la quantité du document d'origine
    if (pLigneVente.getId().getSuffixe().intValue() == 0) {
      return Constantes.formater(pLigneVente.getQuantiteDocumentOrigine(), false);
    }
    // Sinon on retourne la quantité extraite car c'est une extraction
    else {
      return Constantes.formater(pLigneVente.getQuantiteDejaExtraite(), false);
    }
  }
  
  /**
   * Retourner la quantité restante possible à retourner
   */
  public String retournerQuantiteRestante(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return null;
    }
    
    // Si c'est un document d'origine (suffixe à 0) on retourne la quantité du document d'origine
    if (pLigneVente.getId().getSuffixe().intValue() == 0) {
      return Constantes.formater(pLigneVente.getQuantiteDocumentOrigine().subtract(pLigneVente.getQuantiteDejaRetounee()), false);
    }
    // Sinon on retourne la quantité extraite car c'est une extraction
    else {
      return Constantes.formater(pLigneVente.getQuantiteDejaExtraite().subtract(pLigneVente.getQuantiteDejaRetounee()), false);
    }
  }
  
  /**
   * Contrôle s'il est possible de valider la boite de dialogue. On doit avoir choisi un document, un motif de retour et un prix.
   */
  public boolean isValidationPossible() {
    if (ligneVenteOrigine == null || motifRetour == null || prixNetRetour == null) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne si la facturation est en TTC.
   * @param true=Facturation TTC, false=Faturation HT.
   */
  public boolean isTTC() {
    return client.isFactureEnTTC();
  }
  
  public void selectionnerDocument(int pIndiceLigne) {
    if (listeLigneRetour == null) {
      return;
    }
    
    indiceLigneSelectionnee = pIndiceLigne;
    ligneVenteOrigine = listeLigneRetour.getListeLigneVente().get(pIndiceLigne);
    if (ligneVenteOrigine == null && client != null) {
      return;
    }
    // On ne peut pas sélectionner les lignes dont la quantité est inférieure à la quantité à retourner
    if (ligneVenteOrigine.getQuantiteUCV().compareTo(getQuantiteRetournee().abs()) < 0) {
      ligneVenteOrigine = null;
      indiceLigneSelectionnee = -1;
      rafraichir();
      return;
    }
    // Mise à jour du prix de retour avec celui de la ligne sélectionnée
    prixNetRetour = ligneVenteOrigine.getPrixNet();
    // Contrôle du prix de retour
    if (prixNetRetour == null) {
      throw new MessageErreurException("Impossible de déterminer le prix de reprise car le prix de vente n'est pas connu");
    }
    
    rafraichir();
  }
  
  public void deselectionnerDocument(int row) {
    ligneVenteOrigine = null;
    indiceLigneSelectionnee = -1;
    rafraichir();
  }
  
  public void modifierMotif(MotifRetour pMotif) {
    if (pMotif == null) {
      return;
    }
    motifRetour = pMotif;
    rafraichir();
  }
  
  /**
   * Modifie le prix retour de l'article.
   */
  public void modifierPrixRetour(String pPrixSaisi) {
    pPrixSaisi = Constantes.normerTexte(pPrixSaisi);
    if (pPrixSaisi.isEmpty()) {
      rafraichir();
      return;
    }
    BigDecimal prixRetourSaisi = Constantes.convertirTexteEnBigDecimal(pPrixSaisi);
    if (Constantes.equals(prixRetourSaisi, pPrixSaisi)) {
      return;
    }
    contrôlerPrixNetSaisi(prixRetourSaisi);
    prixNetRetour = prixRetourSaisi;
    
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler le prix net saisi sur un retour article => pas de contôle prix de remboursement complètement libre !.
   * @param pPrixNetSaisi Le prix saisi.
   */
  private void contrôlerPrixNetSaisi(BigDecimal pPrixNetSaisi) {
    if (client.isFactureEnTTC()) {
      if (pPrixNetSaisi.compareTo(BigDecimal.ZERO) < 0 || pPrixNetSaisi.compareTo(ligneVenteOrigine.getPrixNetTTC()) > 0) {
        throw new MessageErreurException(String.format("%s%n%.2f%s", "Le prix de retour doit être inférieur ou égal au prix de vente : ",
            ligneVenteOrigine.getPrixNetTTC(), "\u20AC"));
      }
    }
    else if (pPrixNetSaisi.compareTo(BigDecimal.ZERO) < 0 || pPrixNetSaisi.compareTo(ligneVenteOrigine.getPrixNetHT()) > 0) {
      throw new MessageErreurException(String.format("%s%n%.2f%s", "Le prix de retour doit être inférieur ou égal au prix de vente : ",
          ligneVenteOrigine.getPrixNetHT(), "\u20AC"));
    }
  }
  
  // -- Accesseurs
  
  public String getLibellePanelArticle() {
    return libellePanelArticle;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public ListeMotifRetour getListeMotifs() {
    return listeMotifs;
  }
  
  public MotifRetour getMotifRetour() {
    return motifRetour;
  }
  
  public ListeLigneVente getListelignes() {
    if (listeLigneRetour == null) {
      return null;
    }
    return listeLigneRetour.getListeLigneVente();
    
  }
  
  public LigneVente getLigneOrigine() {
    return ligneVenteOrigine;
  }
  
  public int getNombreDecimalesUCS() {
    return listeUnite.getPrecisionUnite(article.getIdUCV());
  }
  
  public Date getDateDebut() {
    if (listeLigneRetour == null) {
      return null;
    }
    return listeLigneRetour.getDateDebut();
  }
  
  public Date getDateFin() {
    if (listeLigneRetour == null) {
      return null;
    }
    return listeLigneRetour.getDateFin();
  }
  
  public int getIndiceLigneSelectionnee() {
    return indiceLigneSelectionnee;
  }
  
  public BigDecimal getPrixRetourPersonnalise() {
    return prixNetRetour;
  }
  
  public BigDecimal getQuantiteRetournee() {
    return quantiteRetournee;
  }
  
  /**
   * Indique si le retour article affiche l'historique ou non.
   */
  public boolean isAffichageHistorique() {
    return isAffichageHistorique;
  }
  
  /**
   * Indique si le retour article affiche les document en cours ou non.
   */
  public boolean isAffichageEnCours() {
    return isAffichageEnCours;
  }
  
  /**
   * Indique si l'historique est vide.
   */
  public boolean isHistoriqueVide() {
    return isHistoriqueVide;
  }
  
}
