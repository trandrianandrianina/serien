/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.duplicationdocument;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialoge qui propose la quantité correspondant au conditionnement supérieur.
 */
public class ModeleDupliquerDocument extends AbstractModeleDialogue {
  // Variables
  private VueDupliquerDocument vue = null;
  private IdDocumentVente idDocumentOrigine = null;
  private boolean isRecalculerPrix = true;
  
  /**
   * Constructeur.
   */
  public ModeleDupliquerDocument(SessionBase pSession, IdDocumentVente pIdDocumentOrigine) {
    super(pSession);
    idDocumentOrigine = pIdDocumentOrigine;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    
  }
  
  // Méthodes privées
  
  // -- Accesseurs
  /**
   * Vue de la boite de dialogue
   */
  public VueDupliquerDocument getVue() {
    return vue;
  }
  
  /**
   * Message de duplication
   */
  public Message getMessageDupplication() {
    return Message.getMessageMoyen("Vous souhaitez dupliquer le document numéro " + idDocumentOrigine);
  }
  
  /**
   * Message sur l'action utilisateur
   */
  public Message getMessageAction() {
    return Message.getMessageMoyen("Souhaitez-vous : ");
  }
  
  /**
   * Renvoyer le mode de mise à jour des prix
   */
  public boolean isRecalculerPrix() {
    return isRecalculerPrix;
  }
  
  /**
   * Modifier le mode de mise à jour des prix
   */
  public void setRecalculerPrix(boolean pIsRecalculerPrix) {
    isRecalculerPrix = pIsRecalculerPrix;
  }
  
}
