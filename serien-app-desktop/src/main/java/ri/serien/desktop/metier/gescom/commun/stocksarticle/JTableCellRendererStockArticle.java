/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.stocksarticle;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

public class JTableCellRendererStockArticle extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  BigDecimal valeurDispo = BigDecimal.ZERO;
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    try {
      
      redimensionnerColonnes(table);
      
      if (column == 0) {
        ((JLabel) component).setHorizontalAlignment(JLabel.LEFT);
      }
      
      if (column == 1) {
        ((JLabel) component).setHorizontalAlignment(JLabel.LEFT);
      }
      
      if (column == 2) {
        ((JLabel) component).setHorizontalAlignment(JLabel.RIGHT);
      }
      
      if (column == 3) {
        ((JLabel) component).setHorizontalAlignment(JLabel.RIGHT);
      }
      
      if (column == 5) {
        ((JLabel) component).setHorizontalAlignment(JLabel.RIGHT);
      }
      
      if (column == 6) {
        ((JLabel) component).setHorizontalAlignment(JLabel.RIGHT);
      }
      
      // Colonne stock dispo : valeur nulle ou négative en rouge
      if (column == 4) {
        // Stockage valeur disponible
        valeurDispo = Constantes.convertirTexteEnBigDecimal((String) value);
        
        if (valeurDispo.compareTo(BigDecimal.ZERO) <= 0) {
          ((JLabel) component).setForeground(Color.RED);
        }
        else {
          ((JLabel) component).setForeground(Color.BLACK);
        }
      }
      
      // Colonne stock maximum : valeur inférieure au dispo en rouge
      else if (column == 7) {
        BigDecimal valeurMaxi = Constantes.convertirTexteEnBigDecimal((String) value);
        
        if (valeurMaxi.compareTo(BigDecimal.ZERO) != 0 && valeurMaxi.compareTo(valeurDispo) <= 0) {
          ((JLabel) component).setForeground(Color.RED);
        }
      }
      else {
        ((JLabel) component).setForeground(Color.BLACK);
      }
      
    }
    catch (
    
    Exception e) {
      DialogueErreur.afficher(e);
    }
    return component;
  }
  
  /**
   * Redimensionne et justifie les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(100);
    cm.getColumn(1).setMinWidth(100);
    cm.getColumn(2).setMinWidth(100);
    cm.getColumn(2).setMaxWidth(100);
    cm.getColumn(3).setMinWidth(100);
    cm.getColumn(3).setMaxWidth(100);
    cm.getColumn(4).setMinWidth(100);
    cm.getColumn(4).setMaxWidth(100);
    cm.getColumn(5).setMinWidth(100);
    cm.getColumn(5).setMaxWidth(100);
    cm.getColumn(6).setMinWidth(100);
    cm.getColumn(6).setMaxWidth(100);
    cm.getColumn(7).setMinWidth(100);
    cm.getColumn(7).setMaxWidth(100);
  }
  
}
