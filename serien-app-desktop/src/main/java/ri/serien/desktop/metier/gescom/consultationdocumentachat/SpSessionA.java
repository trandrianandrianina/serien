/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

/**
 * Classe appelée par le point de menu.
 */
public class SpSessionA extends SessionJava {
  /**
   * Constructeur.
   */
  public SpSessionA(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleConsultationDocumentAchat modele = new ModeleConsultationDocumentAchat(this);
    VueConsultationDocumentAchat vue = new VueConsultationDocumentAchat(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
  }
}
