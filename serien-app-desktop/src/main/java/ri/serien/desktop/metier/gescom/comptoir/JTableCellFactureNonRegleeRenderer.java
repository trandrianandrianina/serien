/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import ri.serien.libcommun.outils.Constantes;

public class JTableCellFactureNonRegleeRenderer extends DefaultTableCellRenderer {
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Pour les cellules de type label
    JLabel label = (JLabel) c;
    switch (column) {
      case 0:
        label.setHorizontalAlignment(CENTER);
        initialiserCouleurBase(label, isSelected);
        break;
      case 1:
        label.setHorizontalAlignment(CENTER);
        initialiserCouleurBase(label, isSelected);
        break;
      case 2:
        label.setHorizontalAlignment(CENTER);
        initialiserCouleurBase(label, isSelected);
        break;
      case 3:
        label.setHorizontalAlignment(RIGHT);
        modifierCouleurTexteMontant(label, (String) value, isSelected);
        break;
      case 4:
        label.setHorizontalAlignment(RIGHT);
        modifierCouleurTexteMontant(label, (String) value, isSelected);
        break;
      case 5:
        label.setHorizontalAlignment(RIGHT);
        modifierCouleurTexteMontant(label, (String) value, isSelected);
        break;
      case 6:
        label.setHorizontalAlignment(LEFT);
        initialiserCouleurBase(label, isSelected);
        break;
    }
    
    return c;
  }
  
  /**
   * Change la couleur du texte si la valeur est un nombre négatif.
   */
  private void modifierCouleurTexteMontant(JLabel pLabel, String pValue, boolean pSelected) {
    if (pValue == null || pLabel == null) {
      return;
    }
    BigDecimal montant = Constantes.convertirTexteEnBigDecimal(pValue);
    if (montant != null && montant.compareTo(BigDecimal.ZERO) < 0) {
      pLabel.setForeground(Color.RED);
    }
    else {
      if (pSelected) {
        pLabel.setForeground(Color.WHITE);
      }
      else {
        pLabel.setForeground(Color.BLACK);
      }
    }
  }
  
  /**
   * Initialise les couleurs des cellules qui ne correspondent pas à des montants.
   */
  private void initialiserCouleurBase(JLabel pLabel, boolean pSelected) {
    if (pLabel == null) {
      return;
    }
    if (pSelected) {
      pLabel.setForeground(Color.WHITE);
    }
    else {
      pLabel.setForeground(Color.BLACK);
    }
  }
}
