/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

/**
 * Classe appelée par le point de menu.
 */
public class SpSessionV extends SessionJava {
  /**
   * Constructeur.
   */
  public SpSessionV(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleConsultationDocumentVente modele = new ModeleConsultationDocumentVente(this);
    VueConsultationDocumentVente vue = new VueConsultationDocumentVente(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
  }
}
