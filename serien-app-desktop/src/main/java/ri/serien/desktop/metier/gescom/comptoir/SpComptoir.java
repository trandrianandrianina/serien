/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

public class SpComptoir extends SessionJava {
  /**
   * Constructeur.
   */
  public SpComptoir(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleComptoir modele = new ModeleComptoir(this);
    VueComptoir vue = new VueComptoir(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
    vue.setVisible(true);
  }
}
