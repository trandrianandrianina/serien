/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.lot.SNBoutonLot;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composant.primitif.saisie.snmontant.SNMontant;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet général de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletGeneral extends JPanel implements InterfaceVueOnglet {
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détails";
  private static final String BOUTON_MASQUER_DETAIL = "Masquer détails";
  
  // Variables
  private ModeleDetailLigneArticle modele = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public VueOngletGeneral(ModeleDetailLigneArticle pComptoir) {
    modele = pComptoir;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes héritées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialiser les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    tfLibelleArticle.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    tfLibelleArticle.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    // Création de l'évènement qui va déclencher l'affichage de la boite de dialogue
    tfMontantTotal.getComposantMontant().addMouseListener(new MouseListener() {
      @Override
      public void mouseReleased(MouseEvent e) {
      }
      
      @Override
      public void mousePressed(MouseEvent e) {
      }
      
      @Override
      public void mouseExited(MouseEvent e) {
      }
      
      @Override
      public void mouseEntered(MouseEvent e) {
      }
      
      @Override
      public void mouseClicked(MouseEvent e) {
        tfMontantTotalMouseClicked(e);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_DETAIL, 'd', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    this.setBackground(new Color(239, 239, 222));
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes rafraichir
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rafraîchir l'affichage des composants graphiques.
   */
  @Override
  public void rafraichir() {
    // Rafraichir les données générales
    rafraichirLibelleLigne();
    rafraichirFournisseur();
    rafraichirUniteConditionnement();
    rafraichirQuantiteUC();
    rafraichirLibelleUVparUC();
    rafraichirMessageUCS();
    rafraichirQuantiteUV();
    rafraichirUniteVente();
    rafraichirLongueur();
    rafraichirUniteLongueur();
    rafraichirLargeur();
    rafraichirUniteLargeur();
    rafraichirHauteur();
    rafraichirUniteHauteur();
    rafraichirBoutonLot();
    
    // Rafraichir les dimensions
    pnlDimensions.setVisible(modele.isLongueurVisible() || modele.isLargeurVisible() || modele.isHauteurVisible());
    
    // Rafraichir le prix flash
    rafraichirPanelPrixFlash();
    rafraichirPrixFlash();
    rafraichirUtilisateurPrixFlash();
    rafraichirDatePrixFlash();
    
    // Rafraichir les éléments de tarif
    rafraichirPrixNet();
    rafraichirPrixBase();
    rafraichirPrixRevient();
    rafraichirOriginePrix();
    rafraichirMontantPrixVente();
    rafraichirMontantArticleGratuit();
    rafraichirRemise();
    rafraichirIndiceMarge();
    rafraichirMarge();
    
    // Rafraichir le stock
    rafraichirPanelStock();
    rafraichirStockPhysique();
    rafraichirStockAttendu();
    rafraichirStockCommande();
    rafraichirStockDisponible();
    
    // Positionnement du focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleDetailLigneArticle.FOCUS_QUANTITE:
        tfQuantiteCommandeUC.requestFocusInWindow();
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL, !modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    snBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL, modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    
    modele.recalculMontantTotal();
  }
  
  /**
   * Rafraîchir l'affichage du message UCS.
   */
  private void rafraichirMessageUCS() {
    
    if (modele.getMessageUCS() != null) {
      lbUCAparUCS.setText("");
      lbUCAparUCS.setText(modele.getMessageUCS());
      lbUCAparUCS.setVisible(true);
    }
    else {
      lbUCAparUCS.setText("");
      lbUCAparUCS.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir l'affichage du libellé de ligne.
   */
  private void rafraichirLibelleLigne() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getLibelle() != null) {
      tfLibelleArticle.setText(ligne.getLibelle() + " (" + ligne.getIdArticle().getCodeArticle() + ")");
    }
    else {
      tfLibelleArticle.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage du fournisseur.
   */
  private void rafraichirFournisseur() {
    Fournisseur fournisseur = modele.getFournisseur();
    if (fournisseur != null) {
      tfFournisseur.setText(fournisseur.getNomFournisseur() + " (" + fournisseur.getId().toString() + ")");
    }
    else {
      tfFournisseur.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'unité de conditionnement.
   */
  private void rafraichirUniteConditionnement() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getIdUniteConditionnementVente() != null) {
      tfUniteC.setText(ligne.getIdUniteConditionnementVente().getCode());
    }
    else {
      tfUniteC.setText("");
    }
  }
  
  /**
   * Rafraîchir la quantité d'UC.
   */
  private void rafraichirQuantiteUC() {
    LigneVente ligne = modele.getLigneVente();
    Article article = modele.getArticle();
    
    if (ligne != null && ligne.getIdUniteConditionnementVente() != null && article != null) {
      if (Constantes.equals(ligne.getIdUniteConditionnementVente(), article.getIdUCV()) || article.getIdUCV() == null) {
        tfQuantiteCommandeUC.setText(Constantes.formater(ligne.getQuantiteUCV(), false));
      }
      else {
        tfQuantiteCommandeUC.setText(Constantes.formater(ligne.getQuantiteUCV().divide(article.getNombreUSParUCS(true)), false));
      }
    }
    else {
      tfQuantiteCommandeUC.setText("");
    }
    
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfQuantiteCommandeUC.setEnabled(true);
    }
    else {
      tfQuantiteCommandeUC.setEnabled(false);
    }
  }
  
  /**
   * Rafraîchir la quantité d'UV.
   */
  private void rafraichirQuantiteUV() {
    LigneVente ligne = modele.getLigneVente();
    if (!ligne.isUniteVenteIdentique()) {
      if (modele.getArticle().isArticleDecoupable()) {
        tfQuantiteCommandeeUV.setText(Constantes.formater(modele.getDimension(), true));
      }
      else {
        tfQuantiteCommandeeUV.setText(Constantes.formater(modele.getLigneVente().getQuantiteUV(), false));
      }
      tfQuantiteCommandeeUV.setVisible(true);
      lbQuantiteUV.setVisible(true);
    }
    else {
      tfQuantiteCommandeeUV.setText("");
      tfQuantiteCommandeeUV.setVisible(false);
      lbQuantiteUV.setVisible(false);
    }
    tfQuantiteCommandeeUV.setEnabled(tfQuantiteCommandeeUV.isVisible() && !pnlDimensions.isVisible());
    
    // La zone est rendu non saisissable :
    // - si les quantités articles du document ne sont pas modifiables
    // - ou si on est en dimension (il faut passer par l'onglet Dimension)
    if (modele.isModifierQuantitePossible() && !modele.getArticle().isArticleDecoupable()) {
      tfQuantiteCommandeeUV.setEnabled(true);
    }
    else {
      tfQuantiteCommandeeUV.setEnabled(false);
    }
  }
  
  /**
   * Rafraîchir le libellé UV par UC.
   */
  private void rafraichirLibelleUVparUC() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne.getIdUniteVente() != null && ligne.getIdUniteConditionnementVente() != null && !ligne.isUniteVenteIdentique()) {
      BigDecimal conditionnement = ligne.getNombreUVParUCV().setScale(ligne.getNombreDecimaleUV());
      if (conditionnement != null && conditionnement.compareTo(BigDecimal.ZERO) == 0) {
        conditionnement = BigDecimal.ONE;
      }
      
      lbUVparUC.setText("Conditionnement " + Constantes.formater(conditionnement, false) + " " + ligne.getIdUniteVente().getCode() + "/"
          + ligne.getIdUniteConditionnementVente().getCode());
    }
    else {
      lbUVparUC.setText("");
    }
    lbUVparUC.setVisible(!ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraîchir l'UV.
   */
  private void rafraichirUniteVente() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getIdUniteVente() != null) {
      tfUniteV.setText(ligne.getIdUniteVente().getCode());
    }
    else {
      tfUniteV.setText("");
    }
    tfUniteV.setVisible(ligne != null && !ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraîchir l'affichage de la longueur.
   */
  private void rafraichirLongueur() {
    lbLongueur.setVisible(modele.isLongueurVisible());
    tfLongueur.setVisible(modele.isLongueurVisible());
    if (tfLongueur.isVisible()) {
      tfLongueur.setText(Constantes.formater(modele.getArticle().getLongueur(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfLongueur.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'unité de longueur.
   */
  private void rafraichirUniteLongueur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLongueur() != null) {
      lbUniteLongueur.setText(article.getCodeUniteLongueur());
    }
    else {
      lbUniteLongueur.setText("");
    }
    lbUniteLongueur.setVisible(modele.isLongueurVisible());
  }
  
  /**
   * Rafraîchir l'affichage de la largeur.
   */
  private void rafraichirLargeur() {
    lbLargeur.setVisible(modele.isLargeurVisible());
    tfLargeur.setVisible(modele.isLargeurVisible());
    if (tfLargeur.isVisible()) {
      tfLargeur.setText(Constantes.formater(modele.getArticle().getLargeur(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfLargeur.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'unité de largeur.
   */
  private void rafraichirUniteLargeur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLargeur() != null) {
      lbUniteLargeur.setText(article.getCodeUniteLargeur());
    }
    else {
      lbUniteLargeur.setText("");
    }
    lbUniteLargeur.setVisible(modele.isLargeurVisible());
  }
  
  /**
   * Rafraîchir l'affichage de la hauteur.
   */
  private void rafraichirHauteur() {
    lbHauteur.setVisible(modele.isHauteurVisible());
    tfHauteur.setVisible(modele.isHauteurVisible());
    if (tfHauteur.isVisible()) {
      tfHauteur.setText(Constantes.formater(modele.getArticle().getHauteur(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfHauteur.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'unité de hauteur.
   */
  private void rafraichirUniteHauteur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteHauteur() != null) {
      lbUniteHauteur.setText(article.getCodeUniteHauteur());
    }
    else {
      lbUniteHauteur.setText("");
    }
    lbUniteHauteur.setVisible(modele.isHauteurVisible());
  }
  
  /**
   * Rafraîchir l'affichag du prix flash.
   */
  private void rafraichirPrixFlash() {
    LigneVente ligne = modele.getLigneVente();
    PrixFlash prixFlash = null;
    if (ligne != null) {
      prixFlash = ligne.getPrixFlash();
    }
    if (prixFlash != null && prixFlash.getPrixVenteFlash() != null) {
      tfPrixFlash.setText(Constantes.formater(prixFlash.getPrixVenteFlash(), true));
    }
    else {
      tfPrixFlash.setText("");
    }
  }
  
  /**
   * 
   * Rafraichir le bouton de saisie des lots pour les articles lotis.
   *
   */
  private void rafraichirBoutonLot() {
    Article article = modele.getArticle();
    if (modele.isGestionParLot() && !modele.getDocumentVente().isDevis() && article.isArticleLot()) {
      snBoutonLot.setVisible(true);
    }
    else {
      snBoutonLot.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'utilisateur prix flash.
   */
  private void rafraichirUtilisateurPrixFlash() {
    LigneVente ligne = modele.getLigneVente();
    PrixFlash prixFlash = null;
    if (ligne != null) {
      prixFlash = ligne.getPrixFlash();
    }
    if (prixFlash != null && prixFlash.getCodeUtilisateurFlash() != null) {
      tfUtilisateurFlash.setText(prixFlash.getCodeUtilisateurFlash());
    }
    else {
      tfUtilisateurFlash.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de la date prix flash.
   */
  private void rafraichirDatePrixFlash() {
    LigneVente ligne = modele.getLigneVente();
    PrixFlash prixFlash = null;
    if (ligne != null) {
      prixFlash = ligne.getPrixFlash();
    }
    if (prixFlash != null && prixFlash.getDatePrixFlash() != null) {
      snDatePrixFlash.setDate(prixFlash.getDatePrixFlash());
    }
    else {
      snDatePrixFlash.setDate(null);
    }
  }
  
  /**
   * Rafraîchir l'affichage du panel de prix flash.
   */
  private void rafraichirPanelPrixFlash() {
    LigneVente ligne = modele.getLigneVente();
    pnlFlash.setVisible(ligne != null && ligne.avoirPrixFlash());
  }
  
  /**
   * Rafraîchir l'affichage du prix public (en mode négoce) ou du prix de base (en mode classique).
   */
  private void rafraichirPrixBase() {
    LigneVente ligneVente = modele.getLigneVente();
    // Mode négoce
    if (modele.isModeNegoce()) {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
        lbPrixBase.setText("Prix public TTC");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
        lbPrixBase.setText("Prix public HT");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBase.setText("Prix public");
        lbPrixBase.setText("");
      }
    }
    // Mode classique
    else {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
        lbPrixBase.setText("Prix base TTC");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
        lbPrixBase.setText("Prix base HT");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBase.setText("Prix base");
        lbPrixBase.setText("");
      }
    }
  }
  
  /**
   * Rafraîchir l'affichage du prix net.
   */
  private void rafraichirPrixNet() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixNetTTC() != null && modele.isAffichageTTC()) {
      lbPrixNet.setText("Prix net TTC");
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixNetHT() != null && !modele.isAffichageTTC()) {
      lbPrixNet.setText("Prix net HT");
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
      
    }
    else {
      lbPrixNet.setText("Prix net");
      tfPrixNet.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage du prix de revient.
   */
  private void rafraichirPrixRevient() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixDeRevientLigneTTC() != null && modele.isAffichageTTC()) {
      lbPrixDeRevientLigne.setText("Prix de revient TTC");
      tfPrixDeRevientLigne.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixDeRevientLigneHT() != null && !modele.isAffichageTTC()) {
      lbPrixDeRevientLigne.setText("Prix de revient HT");
      tfPrixDeRevientLigne.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneHT(), true));
    }
    else {
      lbPrixDeRevientLigne.setText("Prix de revient");
      tfPrixDeRevientLigne.setText("");
    }
    
    lbPrixDeRevientLigne.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    tfPrixDeRevientLigne.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
  }
  
  /**
   * Rafraîchir l'afichage du montant du prix de vente.
   */
  private void rafraichirMontantPrixVente() {
    // La bordure doit etre en TitleBorder car on change le titre dynamiquement
    if (modele.isModeGratuit()) {
      tfMontantTotal.setVisible(false);
      return;
    }
    
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getMontantTTC() != null && modele.isAffichageTTC()) {
      tfMontantTotal.setVisible(true);
      pnlMontant.setTitre("Montant TTC");
      tfMontantTotal.setMontant(Constantes.formater(ligneVente.getMontantTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getMontantHT() != null && !modele.isAffichageTTC()) {
      tfMontantTotal.setVisible(true);
      pnlMontant.setTitre("Montant HT");
      tfMontantTotal.setMontant(Constantes.formater(ligneVente.getMontantHT(), true));
    }
    else {
      tfMontantTotal.setVisible(true);
      pnlMontant.setTitre("Montant");
      tfMontantTotal.setMontant("");
    }
  }
  
  /**
   * Rafraîchir l'affichage du montant article gratuit.
   */
  private void rafraichirMontantArticleGratuit() {
    if (modele.getTypeGratuit() != null) {
      pnlMontant.setTitre("Ligne gratuite");
      tfMontantGratuit.setText(modele.getTypeGratuit().getTexte());
      tfMontantGratuit.setVisible(true);
    }
    else {
      tfMontantGratuit.setText("");
      tfMontantGratuit.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'origine prix.
   */
  private void rafraichirOriginePrix() {
    LigneVente ligneVente = modele.getLigneVente();
    if (modele.isModeGratuit()) {
      tfOrigine.setText("Gratuit");
    }
    else if (modele.getArticle().isArticlePalette()) {
      tfOrigine.setText("Vente");
    }
    else if (ligneVente != null && ligneVente.getTexteOriginePrixVente() != null) {
      tfOrigine.setText(ligneVente.getTexteOriginePrixVente());
    }
    else {
      tfOrigine.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de la remise.
   */
  private void rafraichirRemise() {
    LigneVente ligneVente = modele.getLigneVente();
    BigPercentage tauxRemise = null;
    if (ligneVente != null) {
      tauxRemise = ligneVente.getTauxRemiseUnitaire(modele.isModeNegoce());
    }
    if (tauxRemise != null && !modele.isModeGratuit()) {
      tfRemise.setText(Constantes.formater(tauxRemise, true));
    }
    else {
      tfRemise.setText("");
    }
  }
  
  /**
   * Rafraîchir l'affichage de l'indice marge.
   */
  private void rafraichirIndiceMarge() {
    if (modele.getIndiceDeMarge() != null && !modele.isModeGratuit()) {
      tfIndiceDeMarge.setText(Constantes.formater(modele.getIndiceDeMarge(), true));
    }
    else {
      tfIndiceDeMarge.setText("");
    }
    
    lbIndiceDeMarge.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    tfIndiceDeMarge.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
  }
  
  /**
   * Rafraîchir l'affichage de la marge (taux de marque).
   */
  private void rafraichirMarge() {
    if (modele.getTauxDeMarque() != null && !modele.isModeGratuit()) {
      tfMarge.setText(Constantes.formater(modele.getTauxDeMarque(), true));
    }
    else {
      tfMarge.setText("");
    }
    
    // Masquage des marges
    lbMarge.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    tfMarge.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
    lbSymbolePourcentageMarge.setVisible(modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge());
  }
  
  /**
   * Visible si l'article est géré en stock
   */
  private void rafraichirPanelStock() {
    pnlStocksGeneral.setVisible(modele.getArticle().getNonStocke() == 0);
  }
  
  /**
   * Rafraîchir l'affichage du stock physique.
   */
  private void rafraichirStockPhysique() {
    ListeStockComptoir listeStock = modele.getListeStockUnMagasin();
    if (listeStock != null) {
      tfGeneralStockPhysique.setText(Constantes.formater(listeStock.getQuantitePhysiqueTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantitePhysiqueTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockPhysique.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockPhysique.setText("0");
    }
  }
  
  /**
   * Rafraîchir l'affichage du stock commande.
   */
  private void rafraichirStockCommande() {
    ListeStockComptoir listeStock = modele.getListeStockUnMagasin();
    if (listeStock != null) {
      tfGeneralStockCommande.setText(Constantes.formater(listeStock.getQuantiteCommandeeTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteCommandeeTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockCommande.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockCommande.setText("0");
    }
  }
  
  /**
   * Rafraîchir l'affichage du stock attendu.
   */
  private void rafraichirStockAttendu() {
    ListeStockComptoir listeStock = modele.getListeStockUnMagasin();
    if (listeStock != null) {
      tfGeneralStockAttendu.setText(Constantes.formater(listeStock.getQuantiteAttendueTotale(), false));
      // Si total inférieur ou égal à 0 on le met en rouge
      if (listeStock.getQuantiteAttendueTotale().compareTo(BigDecimal.ZERO) <= 0) {
        tfGeneralStockAttendu.setForeground(Color.RED);
      }
    }
    else {
      tfGeneralStockAttendu.setText("0");
    }
  }
  
  /**
   * Rafraîchir l'affichage stock disponible.
   */
  private void rafraichirStockDisponible() {
    BigDecimal disponibleVente = null;
    ListeStockComptoir listeStock = modele.getListeStockUnMagasin();
    if (listeStock != null) {
      disponibleVente = listeStock.getQuantiteDisponibleVenteTotale();
    }
    if (disponibleVente == null) {
      tfGeneralStockDisponible.setText("0");
    }
    else {
      tfGeneralStockDisponible.setText(Constantes.formater(disponibleVente, false));
    }
    
    if (disponibleVente == null || disponibleVente.compareTo(BigDecimal.ZERO) <= 0) {
      tfGeneralStockDisponible.setForeground(Color.RED);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Gestion des clic bouton.
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        modele.modifierAfficherDetails(true);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL)) {
        modele.modifierAfficherDetails(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Gestion de la perte de focus sur la quantité de commande UC.
   * @param e
   */
  private void tfquantiteCommandeUCFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUC(tfQuantiteCommandeUC.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Gestion de la perte de focus sur la quantité de commande UV.
   * @param e
   */
  private void tfquantiteCommandeeUVFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUV(tfQuantiteCommandeeUV.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Gestion du clic sur le champ de saisie du montant total.
   * 
   * @param e
   */
  private void tfMontantTotalMouseClicked(MouseEvent e) {
    try {
      // Pour que l'action soit exécutée il faut un CTRL + clic souris dans le composant
      if (!e.isControlDown()) {
        return;
      }
      modele.afficherDetailCalculMontant();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfMontantGratuitMouseClicked(MouseEvent e) {
    try {
      // Pour que l'action soit exécutée il faut un CTRL + clic souris dans le composant
      if (!e.isControlDown()) {
        return;
      }
      modele.afficherDetailCalculMontant();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Gestion du clic sur le bouton "+" permettant la saisie d'un lot.
   * 
   * @param e
   */
  private void snBoutonLotActionPerformed(ActionEvent e) {
    try {
      modele.saisirLot();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlDetailHaut = new SNPanel();
    pnlDetailLigne = new SNPanel();
    lbQuantiteUC = new SNLabelChamp();
    snBoutonLot = new SNBoutonLot();
    tfQuantiteCommandeUC = new SNTexte();
    tfUniteC = new SNTexte();
    lbUCAparUCS = new SNLabelChamp();
    lbQuantiteUV = new SNLabelChamp();
    tfQuantiteCommandeeUV = new SNTexte();
    tfUniteV = new SNTexte();
    lbUVparUC = new SNLabelChamp();
    lbArticle = new SNLabelChamp();
    tfLibelleArticle = new SNTexte();
    lbFournisseur = new SNLabelChamp();
    tfFournisseur = new SNTexte();
    pnlConteneur = new SNPanel();
    pnlStocksGeneral = new SNPanelTitre();
    lbEnStock = new SNLabelChamp();
    tfGeneralStockPhysique = new SNTexte();
    lbCommande = new SNLabelChamp();
    tfGeneralStockCommande = new SNTexte();
    lbAttendu = new SNLabelChamp();
    tfGeneralStockAttendu = new SNTexte();
    lbDisponible = new SNLabelChamp();
    tfGeneralStockDisponible = new SNTexte();
    pnlDimensions = new SNPanelTitre();
    lbLongueur = new SNLabelChamp();
    tfLongueur = new SNTexte();
    lbLargeur = new SNLabelChamp();
    tfLargeur = new SNTexte();
    lbUniteLongueur = new SNLabelChamp();
    lbUniteLargeur = new SNLabelChamp();
    lbHauteur = new SNLabelChamp();
    tfHauteur = new SNTexte();
    lbUniteHauteur = new SNLabelChamp();
    pnlFlash = new SNPanelTitre();
    lbPrixFlash = new SNLabelChamp();
    tfPrixFlash = new SNTexte();
    lbUtilisateurFlash = new SNLabelChamp();
    tfUtilisateurFlash = new SNTexte();
    lbDateFlash = new SNLabelChamp();
    snDatePrixFlash = new SNDate();
    pnlTarif = new SNPanel();
    pnlInfosTarifaires = new SNPanelTitre();
    lbPrixBase = new SNLabelChamp();
    tfPrixBase = new SNTexte();
    lbTarif = new SNLabelChamp();
    tfOrigine = new SNTexte();
    lbRemise = new SNLabelChamp();
    tfRemise = new SNTexte();
    lbSymbolePourcentageRemise = new SNLabelChamp();
    lbPrixNet = new SNLabelChamp();
    tfPrixNet = new SNTexte();
    lbIndiceDeMarge = new SNLabelChamp();
    tfIndiceDeMarge = new SNTexte();
    lbMarge = new SNLabelChamp();
    tfMarge = new SNTexte();
    lbPrixDeRevientLigne = new SNLabelChamp();
    lbSymbolePourcentageMarge = new SNLabelChamp();
    tfPrixDeRevientLigne = new SNTexte();
    pnlMontant = new SNPanelTitre();
    tfMontantTotal = new SNMontant();
    tfMontantGratuit = new SNTexte();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setMaximumSize(new Dimension(1230, 530));
      pnlContenu.setMinimumSize(new Dimension(1230, 530));
      pnlContenu.setPreferredSize(new Dimension(1230, 530));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
      
      // ======== pnlDetailHaut ========
      {
        pnlDetailHaut.setName("pnlDetailHaut");
        pnlDetailHaut.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDetailHaut.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDetailHaut.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDetailHaut.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDetailHaut.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlDetailLigne ========
        {
          pnlDetailLigne.setName("pnlDetailLigne");
          pnlDetailLigne.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDetailLigne.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDetailLigne.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlDetailLigne.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDetailLigne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbQuantiteUC ----
          lbQuantiteUC.setText("Quantit\u00e9 en UCV");
          lbQuantiteUC.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbQuantiteUC.setHorizontalAlignment(SwingConstants.RIGHT);
          lbQuantiteUC.setName("lbQuantiteUC");
          pnlDetailLigne.add(lbQuantiteUC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snBoutonLot ----
          snBoutonLot.setMaximumSize(new Dimension(30, 30));
          snBoutonLot.setMinimumSize(new Dimension(30, 30));
          snBoutonLot.setPreferredSize(new Dimension(30, 30));
          snBoutonLot.setName("snBoutonLot");
          snBoutonLot.addActionListener(e -> snBoutonLotActionPerformed(e));
          pnlDetailLigne.add(snBoutonLot, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfQuantiteCommandeUC ----
          tfQuantiteCommandeUC.setHorizontalAlignment(SwingConstants.RIGHT);
          tfQuantiteCommandeUC.setName("tfQuantiteCommandeUC");
          tfQuantiteCommandeUC.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfquantiteCommandeUCFocusLost(e);
            }
          });
          pnlDetailLigne.add(tfQuantiteCommandeUC, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfUniteC ----
          tfUniteC.setEnabled(false);
          tfUniteC.setPreferredSize(new Dimension(36, 30));
          tfUniteC.setMinimumSize(new Dimension(36, 30));
          tfUniteC.setMaximumSize(new Dimension(36, 30));
          tfUniteC.setName("tfUniteC");
          pnlDetailLigne.add(tfUniteC, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbUCAparUCS ----
          lbUCAparUCS.setText("UCA/UCS");
          lbUCAparUCS.setHorizontalAlignment(SwingConstants.LEFT);
          lbUCAparUCS.setName("lbUCAparUCS");
          pnlDetailLigne.add(lbUCAparUCS, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbQuantiteUV ----
          lbQuantiteUV.setText("Quantit\u00e9 en UV");
          lbQuantiteUV.setName("lbQuantiteUV");
          pnlDetailLigne.add(lbQuantiteUV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfQuantiteCommandeeUV ----
          tfQuantiteCommandeeUV.setHorizontalAlignment(SwingConstants.RIGHT);
          tfQuantiteCommandeeUV.setName("tfQuantiteCommandeeUV");
          tfQuantiteCommandeeUV.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfquantiteCommandeeUVFocusLost(e);
            }
          });
          pnlDetailLigne.add(tfQuantiteCommandeeUV, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfUniteV ----
          tfUniteV.setEnabled(false);
          tfUniteV.setMinimumSize(new Dimension(36, 30));
          tfUniteV.setMaximumSize(new Dimension(36, 30));
          tfUniteV.setPreferredSize(new Dimension(36, 30));
          tfUniteV.setName("tfUniteV");
          pnlDetailLigne.add(tfUniteV, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbUVparUC ----
          lbUVparUC.setText("UV/UC");
          lbUVparUC.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbUVparUC.setHorizontalAlignment(SwingConstants.LEFT);
          lbUVparUC.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbUVparUC.setPreferredSize(new Dimension(275, 30));
          lbUVparUC.setMinimumSize(new Dimension(275, 30));
          lbUVparUC.setMaximumSize(new Dimension(275, 30));
          lbUVparUC.setName("lbUVparUC");
          pnlDetailLigne.add(lbUVparUC, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbArticle.setName("lbArticle");
          pnlDetailLigne.add(lbArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLibelleArticle ----
          tfLibelleArticle.setEditable(false);
          tfLibelleArticle.setBackground(new Color(243, 243, 236));
          tfLibelleArticle.setEnabled(false);
          tfLibelleArticle.setName("tfLibelleArticle");
          pnlDetailLigne.add(tfLibelleArticle, new GridBagConstraints(2, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFournisseur ----
          lbFournisseur.setText("Fournisseur");
          lbFournisseur.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFournisseur.setName("lbFournisseur");
          pnlDetailLigne.add(lbFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfFournisseur ----
          tfFournisseur.setEditable(false);
          tfFournisseur.setEnabled(false);
          tfFournisseur.setMinimumSize(new Dimension(150, 30));
          tfFournisseur.setPreferredSize(new Dimension(150, 30));
          tfFournisseur.setMaximumSize(new Dimension(150, 30));
          tfFournisseur.setName("tfFournisseur");
          pnlDetailLigne.add(tfFournisseur, new GridBagConstraints(2, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetailHaut.add(pnlDetailLigne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlConteneur ========
        {
          pnlConteneur.setOpaque(false);
          pnlConteneur.setName("pnlConteneur");
          pnlConteneur.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConteneur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlConteneur.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlConteneur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlConteneur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlStocksGeneral ========
          {
            pnlStocksGeneral.setOpaque(false);
            pnlStocksGeneral.setComponentPopupMenu(null);
            pnlStocksGeneral.setTitre("Stocks en US");
            pnlStocksGeneral.setName("pnlStocksGeneral");
            pnlStocksGeneral.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlStocksGeneral.getLayout()).columnWidths = new int[] { 57, 0, 0 };
            ((GridBagLayout) pnlStocksGeneral.getLayout()).rowHeights = new int[] { 24, 35, 30, 21 };
            ((GridBagLayout) pnlStocksGeneral.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEnStock ----
            lbEnStock.setText("Physique");
            lbEnStock.setHorizontalAlignment(SwingConstants.RIGHT);
            lbEnStock.setComponentPopupMenu(null);
            lbEnStock.setName("lbEnStock");
            pnlStocksGeneral.add(lbEnStock, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfGeneralStockPhysique ----
            tfGeneralStockPhysique.setEnabled(false);
            tfGeneralStockPhysique.setHorizontalAlignment(SwingConstants.TRAILING);
            tfGeneralStockPhysique.setName("tfGeneralStockPhysique");
            pnlStocksGeneral.add(tfGeneralStockPhysique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCommande ----
            lbCommande.setText("Command\u00e9");
            lbCommande.setHorizontalAlignment(SwingConstants.RIGHT);
            lbCommande.setComponentPopupMenu(null);
            lbCommande.setName("lbCommande");
            pnlStocksGeneral.add(lbCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfGeneralStockCommande ----
            tfGeneralStockCommande.setEnabled(false);
            tfGeneralStockCommande.setHorizontalAlignment(SwingConstants.TRAILING);
            tfGeneralStockCommande.setName("tfGeneralStockCommande");
            pnlStocksGeneral.add(tfGeneralStockCommande, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbAttendu ----
            lbAttendu.setComponentPopupMenu(null);
            lbAttendu.setText("Attendu");
            lbAttendu.setName("lbAttendu");
            pnlStocksGeneral.add(lbAttendu, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfGeneralStockAttendu ----
            tfGeneralStockAttendu.setEnabled(false);
            tfGeneralStockAttendu.setHorizontalAlignment(SwingConstants.TRAILING);
            tfGeneralStockAttendu.setName("tfGeneralStockAttendu");
            pnlStocksGeneral.add(tfGeneralStockAttendu, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDisponible ----
            lbDisponible.setComponentPopupMenu(null);
            lbDisponible.setHorizontalTextPosition(SwingConstants.LEADING);
            lbDisponible.setText("Disponible vente");
            lbDisponible.setName("lbDisponible");
            pnlStocksGeneral.add(lbDisponible, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfGeneralStockDisponible ----
            tfGeneralStockDisponible.setEnabled(false);
            tfGeneralStockDisponible.setHorizontalAlignment(SwingConstants.TRAILING);
            tfGeneralStockDisponible.setName("tfGeneralStockDisponible");
            pnlStocksGeneral.add(tfGeneralStockDisponible, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlConteneur.add(pnlStocksGeneral, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlDimensions ========
          {
            pnlDimensions.setOpaque(false);
            pnlDimensions.setTitre("Dimensions");
            pnlDimensions.setName("pnlDimensions");
            pnlDimensions.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlDimensions.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlDimensions.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlDimensions.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlDimensions.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbLongueur ----
            lbLongueur.setText("Longueur");
            lbLongueur.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLongueur.setName("lbLongueur");
            pnlDimensions.add(lbLongueur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfLongueur ----
            tfLongueur.setHorizontalAlignment(SwingConstants.RIGHT);
            tfLongueur.setEnabled(false);
            tfLongueur.setName("tfLongueur");
            pnlDimensions.add(tfLongueur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbLargeur ----
            lbLargeur.setText("Largeur");
            lbLargeur.setHorizontalAlignment(SwingConstants.RIGHT);
            lbLargeur.setName("lbLargeur");
            pnlDimensions.add(lbLargeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfLargeur ----
            tfLargeur.setHorizontalAlignment(SwingConstants.RIGHT);
            tfLargeur.setEnabled(false);
            tfLargeur.setName("tfLargeur");
            pnlDimensions.add(tfLargeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbUniteLongueur ----
            lbUniteLongueur.setText("UL");
            lbUniteLongueur.setMaximumSize(new Dimension(50, 30));
            lbUniteLongueur.setMinimumSize(new Dimension(50, 30));
            lbUniteLongueur.setPreferredSize(new Dimension(50, 30));
            lbUniteLongueur.setHorizontalAlignment(SwingConstants.LEFT);
            lbUniteLongueur.setName("lbUniteLongueur");
            pnlDimensions.add(lbUniteLongueur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbUniteLargeur ----
            lbUniteLargeur.setText("Ul");
            lbUniteLargeur.setMaximumSize(new Dimension(50, 30));
            lbUniteLargeur.setMinimumSize(new Dimension(50, 30));
            lbUniteLargeur.setPreferredSize(new Dimension(50, 30));
            lbUniteLargeur.setHorizontalAlignment(SwingConstants.LEFT);
            lbUniteLargeur.setName("lbUniteLargeur");
            pnlDimensions.add(lbUniteLargeur, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbHauteur ----
            lbHauteur.setText("Hauteur");
            lbHauteur.setHorizontalAlignment(SwingConstants.RIGHT);
            lbHauteur.setName("lbHauteur");
            pnlDimensions.add(lbHauteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfHauteur ----
            tfHauteur.setHorizontalAlignment(SwingConstants.RIGHT);
            tfHauteur.setEnabled(false);
            tfHauteur.setName("tfHauteur");
            pnlDimensions.add(tfHauteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteHauteur ----
            lbUniteHauteur.setText("UH");
            lbUniteHauteur.setMaximumSize(new Dimension(50, 30));
            lbUniteHauteur.setMinimumSize(new Dimension(50, 30));
            lbUniteHauteur.setPreferredSize(new Dimension(50, 30));
            lbUniteHauteur.setHorizontalAlignment(SwingConstants.LEFT);
            lbUniteHauteur.setName("lbUniteHauteur");
            pnlDimensions.add(lbUniteHauteur, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlConteneur.add(pnlDimensions, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          
          // ======== pnlFlash ========
          {
            pnlFlash.setOpaque(false);
            pnlFlash.setTitre("Prix flash");
            pnlFlash.setName("pnlFlash");
            pnlFlash.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFlash.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFlash.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFlash.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFlash.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPrixFlash ----
            lbPrixFlash.setText("Prix");
            lbPrixFlash.setHorizontalAlignment(SwingConstants.RIGHT);
            lbPrixFlash.setName("lbPrixFlash");
            pnlFlash.add(lbPrixFlash, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfPrixFlash ----
            tfPrixFlash.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixFlash.setEnabled(false);
            tfPrixFlash.setName("tfPrixFlash");
            pnlFlash.add(tfPrixFlash, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbUtilisateurFlash ----
            lbUtilisateurFlash.setText("Utilisateur");
            lbUtilisateurFlash.setHorizontalAlignment(SwingConstants.RIGHT);
            lbUtilisateurFlash.setName("lbUtilisateurFlash");
            pnlFlash.add(lbUtilisateurFlash, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfUtilisateurFlash ----
            tfUtilisateurFlash.setHorizontalAlignment(SwingConstants.LEFT);
            tfUtilisateurFlash.setEnabled(false);
            tfUtilisateurFlash.setName("tfUtilisateurFlash");
            pnlFlash.add(tfUtilisateurFlash, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDateFlash ----
            lbDateFlash.setText("Date");
            lbDateFlash.setHorizontalAlignment(SwingConstants.RIGHT);
            lbDateFlash.setName("lbDateFlash");
            pnlFlash.add(lbDateFlash, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snDatePrixFlash ----
            snDatePrixFlash.setEnabled(false);
            snDatePrixFlash.setName("snDatePrixFlash");
            pnlFlash.add(snDatePrixFlash, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlConteneur.add(pnlFlash, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetailHaut.add(pnlConteneur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDetailHaut,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTarif ========
      {
        pnlTarif.setName("pnlTarif");
        pnlTarif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlInfosTarifaires ========
        {
          pnlInfosTarifaires.setOpaque(false);
          pnlInfosTarifaires.setTitre("Informations tarifaires");
          pnlInfosTarifaires.setName("pnlInfosTarifaires");
          pnlInfosTarifaires.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPrixBase ----
          lbPrixBase.setText("Prix public");
          lbPrixBase.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixBase.setMaximumSize(new Dimension(100, 30));
          lbPrixBase.setMinimumSize(new Dimension(100, 30));
          lbPrixBase.setPreferredSize(new Dimension(100, 30));
          lbPrixBase.setName("lbPrixBase");
          pnlInfosTarifaires.add(lbPrixBase, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixBase ----
          tfPrixBase.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixBase.setEnabled(false);
          tfPrixBase.setName("tfPrixBase");
          pnlInfosTarifaires.add(tfPrixBase, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbTarif ----
          lbTarif.setText("Origine");
          lbTarif.setHorizontalAlignment(SwingConstants.CENTER);
          lbTarif.setMaximumSize(new Dimension(95, 30));
          lbTarif.setMinimumSize(new Dimension(95, 30));
          lbTarif.setPreferredSize(new Dimension(95, 30));
          lbTarif.setName("lbTarif");
          pnlInfosTarifaires.add(lbTarif, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfOrigine ----
          tfOrigine.setHorizontalAlignment(SwingConstants.CENTER);
          tfOrigine.setEnabled(false);
          tfOrigine.setName("tfOrigine");
          pnlInfosTarifaires.add(tfOrigine, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbRemise ----
          lbRemise.setText("Remise");
          lbRemise.setHorizontalAlignment(SwingConstants.CENTER);
          lbRemise.setMinimumSize(new Dimension(95, 30));
          lbRemise.setMaximumSize(new Dimension(95, 30));
          lbRemise.setPreferredSize(new Dimension(95, 30));
          lbRemise.setName("lbRemise");
          pnlInfosTarifaires.add(lbRemise, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRemise ----
          tfRemise.setHorizontalAlignment(SwingConstants.RIGHT);
          tfRemise.setEnabled(false);
          tfRemise.setName("tfRemise");
          pnlInfosTarifaires.add(tfRemise, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSymbolePourcentageRemise ----
          lbSymbolePourcentageRemise.setText("% ");
          lbSymbolePourcentageRemise.setHorizontalAlignment(SwingConstants.LEFT);
          lbSymbolePourcentageRemise
              .setFont(lbSymbolePourcentageRemise.getFont().deriveFont(lbSymbolePourcentageRemise.getFont().getSize() + 2f));
          lbSymbolePourcentageRemise.setComponentPopupMenu(null);
          lbSymbolePourcentageRemise.setMaximumSize(new Dimension(30, 30));
          lbSymbolePourcentageRemise.setMinimumSize(new Dimension(30, 30));
          lbSymbolePourcentageRemise.setPreferredSize(new Dimension(30, 30));
          lbSymbolePourcentageRemise.setName("lbSymbolePourcentageRemise");
          pnlInfosTarifaires.add(lbSymbolePourcentageRemise, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixNet ----
          lbPrixNet.setText("Prix net");
          lbPrixNet.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixNet.setPreferredSize(new Dimension(95, 30));
          lbPrixNet.setMinimumSize(new Dimension(95, 30));
          lbPrixNet.setMaximumSize(new Dimension(95, 30));
          lbPrixNet.setName("lbPrixNet");
          pnlInfosTarifaires.add(lbPrixNet, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfPrixNet ----
          tfPrixNet.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixNet.setEnabled(false);
          tfPrixNet.setName("tfPrixNet");
          pnlInfosTarifaires.add(tfPrixNet, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbIndiceDeMarge ----
          lbIndiceDeMarge.setText("Indice");
          lbIndiceDeMarge.setHorizontalAlignment(SwingConstants.CENTER);
          lbIndiceDeMarge.setMaximumSize(new Dimension(95, 30));
          lbIndiceDeMarge.setMinimumSize(new Dimension(95, 30));
          lbIndiceDeMarge.setPreferredSize(new Dimension(95, 30));
          lbIndiceDeMarge.setName("lbIndiceDeMarge");
          pnlInfosTarifaires.add(lbIndiceDeMarge, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfIndiceDeMarge ----
          tfIndiceDeMarge.setHorizontalAlignment(SwingConstants.RIGHT);
          tfIndiceDeMarge.setEnabled(false);
          tfIndiceDeMarge.setName("tfIndiceDeMarge");
          pnlInfosTarifaires.add(tfIndiceDeMarge, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbMarge ----
          lbMarge.setText("Marge");
          lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
          lbMarge.setMaximumSize(new Dimension(100, 30));
          lbMarge.setMinimumSize(new Dimension(100, 30));
          lbMarge.setPreferredSize(new Dimension(100, 30));
          lbMarge.setName("lbMarge");
          pnlInfosTarifaires.add(lbMarge, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfMarge ----
          tfMarge.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMarge.setEnabled(false);
          tfMarge.setName("tfMarge");
          pnlInfosTarifaires.add(tfMarge, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPrixDeRevientLigne ----
          lbPrixDeRevientLigne.setText("Prix de revient");
          lbPrixDeRevientLigne.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixDeRevientLigne.setPreferredSize(new Dimension(125, 30));
          lbPrixDeRevientLigne.setMaximumSize(new Dimension(125, 30));
          lbPrixDeRevientLigne.setMinimumSize(new Dimension(125, 30));
          lbPrixDeRevientLigne.setName("lbPrixDeRevientLigne");
          pnlInfosTarifaires.add(lbPrixDeRevientLigne, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbSymbolePourcentageMarge ----
          lbSymbolePourcentageMarge.setText("% ");
          lbSymbolePourcentageMarge.setHorizontalAlignment(SwingConstants.LEFT);
          lbSymbolePourcentageMarge
              .setFont(lbSymbolePourcentageMarge.getFont().deriveFont(lbSymbolePourcentageMarge.getFont().getSize() + 2f));
          lbSymbolePourcentageMarge.setComponentPopupMenu(null);
          lbSymbolePourcentageMarge.setMaximumSize(new Dimension(30, 30));
          lbSymbolePourcentageMarge.setMinimumSize(new Dimension(30, 30));
          lbSymbolePourcentageMarge.setPreferredSize(new Dimension(30, 30));
          lbSymbolePourcentageMarge.setName("lbSymbolePourcentageMarge");
          pnlInfosTarifaires.add(lbSymbolePourcentageMarge, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPrixDeRevientLigne ----
          tfPrixDeRevientLigne.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixDeRevientLigne.setEnabled(false);
          tfPrixDeRevientLigne.setName("tfPrixDeRevientLigne");
          pnlInfosTarifaires.add(tfPrixDeRevientLigne, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlInfosTarifaires, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlMontant ========
        {
          pnlMontant.setOpaque(false);
          pnlMontant.setTitre("Montant");
          pnlMontant.setMaximumSize(new Dimension(338, 80));
          pnlMontant.setAutoscrolls(true);
          pnlMontant.setName("pnlMontant");
          pnlMontant.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMontant.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlMontant.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMontant.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMontant.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- tfMontantTotal ----
          tfMontantTotal.setEnabled(false);
          tfMontantTotal.setForeground(Color.black);
          tfMontantTotal.setFont(new Font("sansserif", Font.BOLD, 14));
          tfMontantTotal.setPreferredSize(new Dimension(150, 30));
          tfMontantTotal.setMinimumSize(new Dimension(150, 30));
          tfMontantTotal.setMaximumSize(new Dimension(150, 30));
          tfMontantTotal.setName("tfMontantTotal");
          pnlMontant.add(tfMontantTotal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHEAST,
              GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- tfMontantGratuit ----
          tfMontantGratuit.setPreferredSize(new Dimension(150, 30));
          tfMontantGratuit.setMinimumSize(new Dimension(150, 30));
          tfMontantGratuit.setMaximumSize(new Dimension(150, 30));
          tfMontantGratuit.setEnabled(false);
          tfMontantGratuit.setName("tfMontantGratuit");
          tfMontantGratuit.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tfMontantGratuitMouseClicked(e);
            }
          });
          pnlMontant.add(tfMontantGratuit, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST,
              GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarif.add(pnlMontant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTarif,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlDetailHaut;
  private SNPanel pnlDetailLigne;
  private SNLabelChamp lbQuantiteUC;
  private SNBoutonLot snBoutonLot;
  private SNTexte tfQuantiteCommandeUC;
  private SNTexte tfUniteC;
  private SNLabelChamp lbUCAparUCS;
  private SNLabelChamp lbQuantiteUV;
  private SNTexte tfQuantiteCommandeeUV;
  private SNTexte tfUniteV;
  private SNLabelChamp lbUVparUC;
  private SNLabelChamp lbArticle;
  private SNTexte tfLibelleArticle;
  private SNLabelChamp lbFournisseur;
  private SNTexte tfFournisseur;
  private SNPanel pnlConteneur;
  private SNPanelTitre pnlStocksGeneral;
  private SNLabelChamp lbEnStock;
  private SNTexte tfGeneralStockPhysique;
  private SNLabelChamp lbCommande;
  private SNTexte tfGeneralStockCommande;
  private SNLabelChamp lbAttendu;
  private SNTexte tfGeneralStockAttendu;
  private SNLabelChamp lbDisponible;
  private SNTexte tfGeneralStockDisponible;
  private SNPanelTitre pnlDimensions;
  private SNLabelChamp lbLongueur;
  private SNTexte tfLongueur;
  private SNLabelChamp lbLargeur;
  private SNTexte tfLargeur;
  private SNLabelChamp lbUniteLongueur;
  private SNLabelChamp lbUniteLargeur;
  private SNLabelChamp lbHauteur;
  private SNTexte tfHauteur;
  private SNLabelChamp lbUniteHauteur;
  private SNPanelTitre pnlFlash;
  private SNLabelChamp lbPrixFlash;
  private SNTexte tfPrixFlash;
  private SNLabelChamp lbUtilisateurFlash;
  private SNTexte tfUtilisateurFlash;
  private SNLabelChamp lbDateFlash;
  private SNDate snDatePrixFlash;
  private SNPanel pnlTarif;
  private SNPanelTitre pnlInfosTarifaires;
  private SNLabelChamp lbPrixBase;
  private SNTexte tfPrixBase;
  private SNLabelChamp lbTarif;
  private SNTexte tfOrigine;
  private SNLabelChamp lbRemise;
  private SNTexte tfRemise;
  private SNLabelChamp lbSymbolePourcentageRemise;
  private SNLabelChamp lbPrixNet;
  private SNTexte tfPrixNet;
  private SNLabelChamp lbIndiceDeMarge;
  private SNTexte tfIndiceDeMarge;
  private SNLabelChamp lbMarge;
  private SNTexte tfMarge;
  private SNLabelChamp lbPrixDeRevientLigne;
  private SNLabelChamp lbSymbolePourcentageMarge;
  private SNTexte tfPrixDeRevientLigne;
  private SNPanelTitre pnlMontant;
  private SNMontant tfMontantTotal;
  private SNTexte tfMontantGratuit;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
