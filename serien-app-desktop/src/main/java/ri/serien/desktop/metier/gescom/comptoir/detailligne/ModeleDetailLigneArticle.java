/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ri.serien.desktop.metier.gescom.commun.attenduscommandes.ModeleAffichageAttendusCommandes;
import ri.serien.desktop.metier.gescom.commun.attenduscommandes.VueAffichageAttendusCommandes;
import ri.serien.desktop.metier.gescom.commun.mouvementsstock.ModeleAffichageMouvementsStock;
import ri.serien.desktop.metier.gescom.commun.mouvementsstock.VueAffichageMouvementsStock;
import ri.serien.desktop.metier.gescom.commun.stocksarticle.ModeleAffichageStockArticle;
import ri.serien.desktop.metier.gescom.commun.stocksarticle.VueAffichageStockArticle;
import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeDecoupeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.CriteresRechercheDocumentsHistoriqueVentesClientArticle;
import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.gescom.commun.client.LigneMouvement;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumTypeConditionAchat;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.IdTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.TypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.EnumOriginePrixVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.EnumTypePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.FormulePrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrechantier.EnumTypeChantier;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametretarif.ParametreTarif;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.composant.metier.vente.lot.ModeleLot;
import ri.serien.libswing.composant.metier.vente.lot.VueLot;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.DialogueDetailPrixVente;
import ri.serien.libswing.composant.metier.vente.prixvente.detailprixvente.ModeleDialogueDetailPrixVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue d'affichage du détail d'une ligne de vente.
 * 
 * Cette boîte de dialogue peut être appelée sur une ligne de vente en cours de création ou pour modifier une ligne de vente existante.
 */
public class ModeleDetailLigneArticle extends AbstractModeleDialogue {
  // Constantes
  public static final int NE_PAS_AFFICHER = -1;
  public static final int NOMBRE_DECIMALE_DIMENSION = 2;
  private static final int NOMBRE_DECIMALE_COEFFICIENT = 4;
  
  public static final int ONGLET_GENERAL = 0;
  public static final int ONGLET_NEGOCIATION = 1;
  public static final int ONGLET_INFORMATIONSTECHNIQUES = 2;
  public static final int ONGLET_STOCK = 3;
  public static final int ONGLET_HISTORIQUE = 4;
  public static final int ONGLET_DIMENSIONS = 5;
  public static final int ONGLET_LIENS = 6;
  
  public static final int SOUS_ONGLET_ETAT_STOCKS = 0;
  public static final int SOUS_ONGLET_MOUVEMENTS = 1;
  public static final int SOUS_ONGLET_ATTENDUS = 2;
  
  public static final int TAILLE_LIGNE_ITC = 120;
  public static final int NOMBRE_LIGNES_ITC = 10;
  public static final boolean HT = false;
  public static final boolean TTC = true;
  public static final boolean MODE_NEGOCIATION_VENTE = false;
  public static final boolean MODE_NEGOCIATION_ACHAT = true;
  
  public static final int DIMENSION_LONGUEUR = 0;
  public static final int DIMENSION_SURFACE = 1;
  public static final int DIMENSION_VOLUME = 2;
  
  public static final int NOMBREMAXURL = 5;
  
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_LIBELLE_ARTICLE = 1;
  public static final int FOCUS_QUANTITE = 2;
  public static final int FOCUS_DIMENSION_NOMBRE = 3;
  
  public static final int TYPE_SAISIE_PORT_AUCUN = 0;
  public static final int TYPE_SAISIE_PORT_MONTANT_ET_POIDS = 1;
  public static final int TYPE_SAISIE_PORT_POURCENTAGE = 2;
  
  private static final String TYPE_UNITE_DIMENSION_M = "";
  private static final String TYPE_UNITE_DIMENSION_m = "1";
  private static final String TYPE_UNITE_DIMENSION_CM = "2";
  private static final String TYPE_UNITE_DIMENSION_MM = "3";
  
  private static final String PREFIXE_LIBELLE_ARTICLE_DECOUPE = " (D\u00e9coup\u00e9  : ";
  private static final Pattern PATTERN_QUANTITE_SAISIE_UC = Pattern.compile("(-?[0-9.,]{0,})([pPmMvVcC]{0,})");
  
  // Variables d'état
  private int ongletActif = ONGLET_GENERAL;
  private int sousOngletActif = SOUS_ONGLET_ETAT_STOCKS;
  private int composantAyantLeFocus = FOCUS_QUANTITE;
  private boolean nePasEnregistrer = false;
  private Message messageConditionChantier = null;
  
  // Etats de chargement des données
  private boolean ongletGeneralCharge = false;
  private boolean ongletStockCharge = false;
  private boolean ongletHistoriqueCharge = false;
  private boolean ongletNegociationVenteCharge = false;
  private boolean ongletNegociationAchatCharge = false;
  private boolean ongletInformationsTechniquesCharge = false;
  private boolean ongletDimensionsCharge = false;
  private boolean ongletLiensCharge = false;
  private boolean sousOngletEtatStocks = false;
  private boolean sousOngletMouvements = false;
  private boolean sousOngletAttendus = false;
  
  // Données générales
  private final EnumModeDetailLigneVente modeDetailLigneVente;
  private ModeleComptoir modeleComptoir = null;
  private Etablissement etablissement = null;
  private DocumentVente documentVente = null;
  private LigneVente ligneVente = null;
  private LigneVente ligneVenteOrigine = null;
  private Client client = null;
  private Fournisseur fournisseur = null;
  private Article article = null;
  private ListeUnite listeUnite = null;
  private ColonneTarif[] listeColonnesTarif = null;
  private ColonneTarif[] listeColonnesTarifClassique = null;
  private ListeTypeGratuit listeTypeGratuit = null;
  private String messageUCS = "";
  
  // Données négociation
  private Date dateTarif = null;
  private CalculPrixVente calculPrixVente = null;
  private BigDecimal indiceDeMargeOrigine = null;
  private BigDecimal tauxDeMarqueOrigine = null;
  private BigDecimal indiceDeMarge = null;
  private BigDecimal tauxDeMarque = null;
  private boolean affichageTTC = HT;
  private boolean modeNegociation = MODE_NEGOCIATION_VENTE;
  private boolean detailMargeVisible = true;
  private ConditionAchat conditionAchatLigne = null;
  private int typeSaisiePort = TYPE_SAISIE_PORT_AUCUN;
  private BigDecimal sauvegardePRSConditionAchat = BigDecimal.ZERO;
  
  // Données informations techniques
  private boolean sauverMemoITC = false;
  private Memo memoITC = null;
  private String[] listeURL = new String[5];
  
  // Données stock
  private ListeStockComptoir listeStockUnMagasin = null;
  private LigneMouvement[] listeMouvementStock = null;
  private List<LigneAttenduCommande> listeLigneAttenduCommande = null;
  private int indexFiltreTypeDocuments = 0;
  
  // Données historique
  private List<DocumentHistoriqueVenteClientArticle> listeHistorique = new ArrayList<DocumentHistoriqueVenteClientArticle>();
  
  // Données dimensions
  private EnumTypeDecoupeArticle typeDimension = EnumTypeDecoupeArticle.IMPOSSIBLE;
  private BigDecimal longueur = BigDecimal.ZERO;
  private BigDecimal largeur = BigDecimal.ZERO;
  private BigDecimal hauteur = BigDecimal.ZERO;
  private BigDecimal dimension = BigDecimal.ZERO;
  private int nombreDecoupe = 0;
  private boolean longueurVisible = false;
  private boolean largeurVisible = false;
  private boolean hauteurVisible = false;
  
  // Données liens
  private LienLigne lienOrigine = null;
  
  // Composants inclus
  private ModeleAffichageStockArticle modeleStock = null;
  private VueAffichageStockArticle vueStock = null;
  private ModeleAffichageMouvementsStock modeleMouvements = null;
  private VueAffichageMouvementsStock vueMouvements = null;
  private ModeleAffichageAttendusCommandes modeleAttendus = null;
  private VueAffichageAttendusCommandes vueAttendus = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public ModeleDetailLigneArticle(SessionBase pSession, EnumModeDetailLigneVente pModeDetailLigneVente, ModeleComptoir pModeleParent,
      DocumentVente pDocumentVente, LigneVente pLigneVente, Client pClient, Article pArticle, int pOnglet) {
    super(pSession);
    modeDetailLigneVente = pModeDetailLigneVente;
    modeleComptoir = pModeleParent;
    ongletActif = pOnglet;
    documentVente = pDocumentVente;
    client = pClient;
    ligneVente = pLigneVente;
    // Duplication de la ligne afin de travailler sur la ligne dupliquée
    if (ligneVente != null) {
      ligneVenteOrigine = ligneVente.clone();
    }
    article = pArticle;
    affichageTTC = client.isFactureEnTTC();
    if (article.isArticleGeneriqueReprise()) {
      throw new MessageErreurException("Cette ligne ne peut pas être affichée.\nL'article n'existe plus dans votre base de données.");
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards à surcharger
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserDonnees() {
  }
  
  /**
   * Charger les données de l'onglet actif.
   * Cette méthode est appelée à chaque changement d'onglets, il faut donc veiller à ne pas recharger les données à chaque fois.
   */
  @Override
  public void chargerDonnees() {
    sauverMemoITC = false;
    
    // Charger les types de gratuit
    if (listeTypeGratuit == null) {
      CriteresRechercheTypeGratuit criteres = new CriteresRechercheTypeGratuit();
      criteres.setTypeRecherche(CriteresRechercheTypeGratuit.RECHERCHE_TYPE_GRATUIT);
      listeTypeGratuit = ManagerServiceParametre.chargerListeTypeGratuit(getIdSession(), criteres);
    }
    
    // CalculPrixVente est instancié ici car une modification de la quantité ou une négociation nécessite un recalcul de prix
    if (calculPrixVente == null) {
      ParametreChargement parametreChargement = new ParametreChargement();
      parametreChargement.setIdEtablissement(ligneVente.getId().getIdEtablissement());
      parametreChargement.setIdLigneDocumentVente(ligneVente.getId());
      parametreChargement.setIdMagasin(ligneVente.getIdMagasin());
      parametreChargement.setIdArticle(ligneVente.getIdArticle());
      parametreChargement.setIdClientFacture(client.getId());
      parametreChargement.setIdDocumentVente(documentVente.getId());
      parametreChargement.setIdChantier(documentVente.getIdChantier());
      calculPrixVente = ManagerServiceArticle.calculerPrixVente(getIdSession(), parametreChargement);
      
      // Calcul de l'indice de marge et du taux de marque de la ligne de vente d'origine
      indiceDeMargeOrigine = ligneVenteOrigine.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
      tauxDeMarqueOrigine = ligneVenteOrigine.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
      // Calcul de l'indice de marge et du taux de marque de la ligne de vente négociée
      indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
      tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    }
    
    if (article.isArticleStandard()) {
      ligneVente.setAutoriserPrixNetInferieurPrixRevient(ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
          EnumDroitSecurite.IS_SUPERVISEUR_GESTION_DES_BONS_ET_FACTURES));
    }
    else {
      ligneVente.setAutoriserPrixNetInferieurPrixRevient(true);
    }
    
    // L'article est chargé pour tous les onglets
    article = lireArticle(ligneVente.getIdArticle());
    if (fournisseur == null && article.getIdFournisseur() != null) {
      fournisseur = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), article.getIdFournisseur());
    }
    
    if (listeUnite == null) {
      listeUnite = new ListeUnite();
      listeUnite = listeUnite.charger(getIdSession());
    }
    
    // Charger la condition d'achat
    if (conditionAchatLigne == null && fournisseur != null) {
      // Charger la condition d'achats associée à la ligne de vente
      conditionAchatLigne = ManagerServiceDocumentVente.chargerConditionAchat(getIdSession(), ligneVente);
      
      // Charger la condition d'achat de l'article si on ne trouve pas de condition d'achats associé à la ligne de vente
      if (conditionAchatLigne == null) {
        conditionAchatLigne = ManagerServiceArticle.chargerConditionAchat(getIdSession(), ligneVente.getIdArticle(), new Date());
      }
      
      // Si le document est modifiable, mise à jour de la condition d'achats si elle a été chargée
      if (documentVente.isModifiable() && conditionAchatLigne != null) {
        // Associer la condition d'achats à la ligne du document de ventes
        conditionAchatLigne.setIdEtablissement(documentVente.getId().getIdEtablissement());
        conditionAchatLigne.setCodeERL(documentVente.getId().getCodeEntete().getCode());
        conditionAchatLigne.setNumeroBon(documentVente.getId().getNumero());
        conditionAchatLigne.setSuffixeBon(documentVente.getId().getSuffixe());
        conditionAchatLigne.setNumeroLigne(ligneVente.getId().getNumeroLigne());
        conditionAchatLigne.setIdArticle(ligneVente.getIdArticle());
        conditionAchatLigne.setIdFournisseur(fournisseur.getId());
        conditionAchatLigne.setNombreDecimalesUA(listeUnite.getPrecisionUnite(ligneVente.getIdUniteVente()));
        conditionAchatLigne.setNombreDecimalesUCA(listeUnite.getPrecisionUnite(ligneVente.getIdUniteConditionnementVente()));
        
        // Sauvegarder le prix de revient standard afin de voir si on a modifié la condtion d'achat
        sauvegardePRSConditionAchat = conditionAchatLigne.getPrixRevientStandardHT();
      }
    }
    
    // Initialiser le type de saisie du port à partir de la condition d'achats
    if (conditionAchatLigne == null) {
      typeSaisiePort = TYPE_SAISIE_PORT_AUCUN;
    }
    else if (conditionAchatLigne.getPourcentagePort() != null
        && conditionAchatLigne.getPourcentagePort().compareTo(BigDecimal.ZERO) > 0) {
      typeSaisiePort = TYPE_SAISIE_PORT_POURCENTAGE;
    }
    else {
      typeSaisiePort = TYPE_SAISIE_PORT_MONTANT_ET_POIDS;
    }
    
    // Si l'article est un article à dimension variable
    typeDimension = article.getTypeDecoupe();
    if (article.isArticleDecoupable() && !ongletDimensionsCharge) {
      ongletActif = ModeleDetailLigneArticle.ONGLET_DIMENSIONS;
    }
    longueurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.LONGUEUR) >= 0;
    largeurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.VOLUME) >= 0;
    hauteurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.VOLUME) == 0;
    
    // Chargement de la liste des prix
    chargerListeColonnesTarif();
    
    switch (ongletActif) {
      case ModeleDetailLigneArticle.ONGLET_GENERAL:
        chargerOngletGeneral();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_STOCK:
        chargerOngletStock();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_HISTORIQUE:
        chargerOngletHistorique();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_NEGOCIATION:
        chargerOngletNegociation();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES:
        chargerOngletInformationsTechniques();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_DIMENSIONS:
        chargerOngletDimensions();
        break;
      
      case ModeleDetailLigneArticle.ONGLET_LIENS:
        chargerOngletLiens();
        break;
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    if (!enregistrerLigneArticle()) {
      return;
    }
    
    // Mise à jour de l'article commentaire s'il y en a un
    sauverNegociationAchat();
    if (sauverMemoITC) {
      memoITC.sauver(getIdSession());
    }
    
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    // Restauration de la ligne originale
    ligneVente = ligneVenteOrigine;
    super.quitterAvecAnnulation();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées dédiées au chargement des données
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner les données d'un article.
   */
  private Article lireArticle(IdArticle pIdArticle) {
    // Lecture des informations complètes de l'article
    if (article == null) {
      article = new Article(pIdArticle);
      article = ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
    }
    else {
      article = ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
    }
    return article;
  }
  
  /**
   * Chargement de la liste des colonnes.
   */
  private void chargerListeColonnesTarif() {
    if (listeColonnesTarif != null) {
      return;
    }
    ParametreTarif parametreTarif = calculPrixVente.getParametreTarif();
    if (parametreTarif == null) {
      return;
    }
    
    // Mise à jour de la date d'application du tarif
    dateTarif = parametreTarif.getDateTarif();
    
    // Chargement des colonnes tarifs
    List<ColonneTarif> listeColonneTarif = new ArrayList<ColonneTarif>();
    for (int numeroColonneTarif = 1; numeroColonneTarif <= parametreTarif.getNombreColonneTarif(); numeroColonneTarif++) {
      BigDecimal prixHT = parametreTarif.getPrixBaseHT(numeroColonneTarif);
      // Si le prix est égal à 0 alors il est ignoré
      if (prixHT.compareTo(BigDecimal.ZERO) > 0) {
        listeColonneTarif.add(new ColonneTarif(numeroColonneTarif, prixHT,
            OutilCalculPrix.calculerPrixTTC(prixHT, ligneVente.getPourcentageTVA(), Constantes.DEUX_DECIMALES), Constantes.DEUX_DECIMALES,
            affichageTTC));
      }
    }
    
    // Initialisation de la colonne tarif courante
    listeColonnesTarif = listeColonneTarif.toArray(new ColonneTarif[listeColonneTarif.size()]);
    
    // Création de la liste des colonnes tarif pour la saisie en mode classique.
    listeColonneTarif.add(0, null);
    listeColonnesTarifClassique = listeColonneTarif.toArray(new ColonneTarif[listeColonneTarif.size()]);
  }
  
  /**
   * Charger les données de l'onglet général.
   */
  private void chargerOngletGeneral() {
    if (ongletGeneralCharge) {
      return;
    }
    ongletGeneralCharge = true;
    
    // Vérifier les pré-requis
    if (documentVente.getIdMagasin() == null) {
      throw new MessageErreurException("Impossible de récupérer l'identifiant du magasin de sortie");
    }
    
    // Récupérer le tarif du jour
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yy");
    String laDateDuJour = simpleDateFormat.format(new Date());
    try {
      dateTarif = simpleDateFormat.parse(laDateDuJour);
    }
    catch (ParseException e) {
      throw new MessageErreurException("Impossible de récupérer la date du jour");
    }
    
    // Charger le stock
    CritereStockComptoir critereStock = new CritereStockComptoir();
    critereStock.setIdMagasin(documentVente.getIdMagasin());
    critereStock.setCodeArticle(article.getId().getCodeArticle());
    critereStock.setNbDecimales(getPrecisionUniteStock());
    listeStockUnMagasin = ManagerServiceStock.chargerListeStockComptoir(getIdSession(), critereStock);
    
    // Charger le prix flash
    PrixFlash prixFlash =
        ManagerServiceDocumentVente.chargerPrixFlash(getIdSession(), ligneVente.getId(), ligneVente.getIdArticle(), client.getId());
    ligneVente.setPrixFlash(prixFlash);
    
    // S'il s'agit d'un article découpable les quantités doivent être ajustées à l'aide des dimensions
    calculerDimensionDecoupe();
    
    construireMessageUCS();
  }
  
  /**
   * Charger les données de l'onglet négociation vente.
   */
  private void chargerOngletNegociation() {
    if (ongletNegociationVenteCharge) {
      return;
    }
    ongletNegociationVenteCharge = true;
    
    // Message si condition chantier : la condition chantier prime sur tout, donc pas de négotiation dans ce cas.
    if (ligneVente.getOriginePrixVente() != EnumOriginePrixVente.CHANTIER) {
      messageConditionChantier = null;
    }
    else if (ligneVente.getTypeConditionChantier() != null) {
      if (ligneVente.getTypeConditionChantier() == EnumTypeChantier.QUANTITATIVE) {
        messageConditionChantier = Message.getMessageImportant("Cette ligne de vente bénéficie d'une condition quantitative chantier :\n"
            + "ce prix s'applique en fonction de la quantité commandée et interdit toute négociation supplémentaire.");
      }
      else {
        messageConditionChantier = Message.getMessageImportant("Cette ligne de vente bénéficie d'une condition chantier :\n"
            + "ce prix s'applique dans tous les cas et interdit toute négociation supplémentaire.");
      }
    }
    
    // Contrôle que le prix de revient du prix de vente initial soit différent de 0
    if (ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) == 0 && article != null && article.getCoefficientUSParUV() != null
        && conditionAchatLigne != null) {
      ligneVente.setPrixDeRevientLigneHT(conditionAchatLigne.getPrixRevientStandardHTEnUV(article.getCoefficientUSParUV()));
    }
    
    int colonne = ligneVente.getNumeroColonneTarif();
    if (article.isArticlePalette()) {
      if (ligneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) >= 0) {
        colonne = 1;
      }
      else {
        colonne = 2;
      }
    }
    else {
      if (colonne == 0) {
        if (client.getNumeroColonneTarif() != null && client.getNumeroColonneTarif() > 0) {
          colonne = client.getNumeroColonneTarif();
        }
        else {
          colonne = 1;
        }
      }
    }
  }
  
  /**
   * Charger les données de l'onglet négociation achat.
   */
  private void chargerOngletNegociationAchat() {
    if (ongletNegociationAchatCharge) {
      return;
    }
    ongletNegociationAchatCharge = true;
  }
  
  /**
   * Charger les données de l'onglet mémo.
   */
  private void chargerOngletInformationsTechniques() {
    if (ongletInformationsTechniquesCharge) {
      return;
    }
    ongletInformationsTechniquesCharge = true;
    listeURL = chargerURL(article);
    memoITC = Memo.charger(getIdSession(), article.getId(), EnumTypeMemo.INFORMATION_TECHNICOCOMMERCIALE);
    
    // Mise à jour des quantités pour le cas où l'on valide directement l'onglet Informations sans passer par l'onglet général
    // Cela corrige la quantité en UV quand la quantité en UCV a été arrondie (unité sans décimales)
    if (!ongletGeneralCharge) {
      BigDecimal quantite = ligneVente.getQuantiteUCV();
      ligneVente.modifierQuantiteUV(calculPrixVente, quantite);
      if (article.getNombreUVParUCV() != null && article.getNombreUVParUCV().compareTo(BigDecimal.ZERO) > 0) {
        ligneVente.modifierQuantiteUV(calculPrixVente, quantite.multiply(article.getNombreUVParUCV()));
      }
      else {
        ligneVente.setQuantiteUV(quantite);
      }
    }
  }
  
  /**
   * Charge les URL associées à un article.
   * Si une URL n'est pas valide elle est ignorée.
   */
  private String[] chargerURL(Article aArticle) {
    String[] listeURLlues = ManagerServiceArticle.chargerListeURLArticle(getIdSession(), article);
    
    for (int i = 0; i < NOMBREMAXURL; i++) {
      if (i >= listeURLlues.length) {
        listeURL[i] = "";
      }
      else {
        if (getSession().getLexique().isValidURL(listeURLlues[i])) {
          listeURL[i] = listeURLlues[i];
        }
        else {
          listeURL[i] = "";
        }
      }
    }
    return listeURL;
  }
  
  /**
   * Charger les données de l'onglet stock.
   */
  private void chargerOngletStock() {
    if (sousOngletActif == SOUS_ONGLET_MOUVEMENTS) {
      // Les mouvements de stock
      chargerSousOngletMouvements();
    }
    else if (sousOngletActif == SOUS_ONGLET_ATTENDUS) {
      // Les attendus commandé
      chargerSousOngletAttendus();
    }
    else {
      chargerSousOngletEtatStock();
    }
  }
  
  /**
   * Charger les données du sous onglet etat stock.
   */
  private void chargerSousOngletEtatStock() {
    if (sousOngletEtatStocks) {
      return;
    }
    
    if (ligneVente.getIdArticle() == null) {
      throw new MessageErreurException("Impossible de récupérer l'identifiant de l'article de la ligne de vente.");
    }
    
    sousOngletEtatStocks = true;
    
    // Les états des stocks
    modeleStock = new ModeleAffichageStockArticle(getSession(), article, EnumTypeStockDisponible.TYPE_VENTE);
    vueStock = new VueAffichageStockArticle(modeleStock);
    vueStock.afficher();
  }
  
  /**
   * Charger les données du sous onglet mouvements stock.
   */
  private void chargerSousOngletMouvements() {
    if (sousOngletMouvements) {
      return;
    }
    sousOngletMouvements = true;
    // Les mouvements de stock
    modeleMouvements =
        new ModeleAffichageMouvementsStock(getSession(), article, EnumTypeStockDisponible.TYPE_VENTE, ligneVente.getIdMagasin());
    vueMouvements = new VueAffichageMouvementsStock(modeleMouvements);
    vueMouvements.afficher();
  }
  
  /**
   * Charger les données du sous onglet attendus/commandés.
   */
  private void chargerSousOngletAttendus() {
    if (sousOngletAttendus) {
      return;
    }
    sousOngletAttendus = true;
    // Les mouvements de stock
    modeleAttendus = new ModeleAffichageAttendusCommandes(getSession(), article, ligneVente.getIdMagasin());
    vueAttendus = new VueAffichageAttendusCommandes(modeleAttendus);
    vueAttendus.afficher();
  }
  
  /**
   * Charger les données de l'onglet historique.
   */
  private void chargerOngletHistorique() {
    if (ongletHistoriqueCharge) {
      return;
    }
    ongletHistoriqueCharge = true;
    List<DocumentHistoriqueVenteClientArticle> listeHistoriqueTemp = null;
    // init onglet histo ventes
    CriteresRechercheDocumentsHistoriqueVentesClientArticle criteresvca = new CriteresRechercheDocumentsHistoriqueVentesClientArticle();
    criteresvca.setNbrLignesParPage(15);
    criteresvca.setIdEtablissement(client.getId().getIdEtablissement());
    int numeroPage = 1;
    while (listeHistoriqueTemp == null || listeHistoriqueTemp.size() == 15) {
      criteresvca.setNumeroPage(numeroPage);
      listeHistoriqueTemp = ManagerServiceClient.chargerDocumentHistoriqueVenteClientArticle(getIdSession(), client.getId(),
          ligneVente.getIdArticle(), criteresvca);
      numeroPage++;
      if (listeHistoriqueTemp != null) {
        for (int i = 0; i < listeHistoriqueTemp.size(); i++) {
          if (listeHistoriqueTemp.get(i) != null) {
            listeHistorique.add(listeHistoriqueTemp.get(i));
          }
        }
      }
    }
  }
  
  /**
   * Charger les données de l'onglet dimensions.
   */
  private void chargerOngletDimensions() {
    if (ongletDimensionsCharge) {
      return;
    }
    
    typeDimension = article.getTypeDecoupe();
    longueur = article.getLongueur();
    largeur = article.getLargeur();
    hauteur = article.getHauteur();
    nombreDecoupe = ligneVente.getNombreDecoupe();
    
    longueurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.LONGUEUR) >= 0;
    largeurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.VOLUME) >= 0;
    hauteurVisible = typeDimension.compareTo(EnumTypeDecoupeArticle.VOLUME) == 0;
    
    if (ligneVente.getMesure1().compareTo(BigDecimal.ZERO) != 0) {
      longueur = ligneVente.getMesure1();
      longueur = longueur.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    if (ligneVente.getMesure2().compareTo(BigDecimal.ZERO) != 0) {
      largeur = ligneVente.getMesure2();
      largeur = largeur.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    if (ligneVente.getMesure3().compareTo(BigDecimal.ZERO) != 0) {
      hauteur = ligneVente.getMesure3();
      hauteur = hauteur.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    ongletDimensionsCharge = true;
    calculerDimensionDecoupe();
    
    composantAyantLeFocus = FOCUS_DIMENSION_NOMBRE;
    
  }
  
  /**
   * Charger les données de l'onglet liens documents
   */
  private void chargerOngletLiens() {
    if (ongletLiensCharge) {
      return;
    }
    if (documentVente.isFacture()) {
      IdDocumentVente idDocumentOrigine = IdDocumentVente.getInstanceGenerique(ligneVente.getId().getIdEtablissement(),
          ligneVente.getId().getCodeEntete(), ligneVente.getId().getNumero(), ligneVente.getId().getSuffixe(), 0);
      DocumentVente documentOrigine = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), idDocumentOrigine);
      lienOrigine =
          new LienLigne(IdLienLigne.getInstanceVente(ligneVente.getId().getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_VENTE,
              ligneVente.getId().getCodeEntete().getCode(), ligneVente.getId().getNumero(), ligneVente.getId().getSuffixe(), 0,
              ligneVente.getId().getNumeroLigne(), idDocumentOrigine, ligneVente.getId().getNumeroLigne(), false));
      lienOrigine.setDocumentVente(documentOrigine);
    }
    else {
      lienOrigine = new LienLigne(IdLienLigne.getInstanceVente(documentVente.getId().getIdEtablissement(),
          EnumTypeLienLigne.LIEN_LIGNE_VENTE, documentVente.getId().getEntete(), documentVente.getId().getNumero(),
          documentVente.getId().getSuffixe(), documentVente.getId().getNumeroFacture(), ligneVente.getId().getNumeroLigne(),
          documentVente.getId(), ligneVente.getId().getNumeroLigne(), false));
      lienOrigine.setDocumentVente(documentVente);
    }
    lienOrigine.setLigne(ligneVente);
    
    ongletLiensCharge = true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles communes à plusieurs onglets
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Changer l'onglet actif.
   */
  public void modifierOngletActif(int pSaisie) {
    if (pSaisie == ongletActif) {
      return;
    }
    ongletActif = pSaisie;
    chargerDonnees();
    rafraichir();
  }
  
  /**
   * Modifier la quantité commandée en UC.
   */
  public void modifierQuantiteCommandeeUC(String saisie) {
    BigDecimal quantite = BigDecimal.ZERO;
    String nombreTrouve = "";
    String lettreTrouve = "";
    
    // On controle que le contenu de pValeur corresponde à une saisie valide (chiffre et/ou lettre)
    Matcher m = PATTERN_QUANTITE_SAISIE_UC.matcher(saisie);
    // Analyse des données saisies dans la cellule quantité
    if (m.find()) {
      nombreTrouve = Constantes.normerTexte(m.group(1));
      lettreTrouve = Constantes.normerTexte(m.group(2));
    }
    
    if (lettreTrouve.isEmpty()) {
      lettreTrouve = "";
      quantite = BigDecimal.ZERO;
    }
    else {
      lettreTrouve = lettreTrouve.toLowerCase();
      quantite = BigDecimal.ONE;
    }
    if (!nombreTrouve.isEmpty()) {
      quantite = Constantes.convertirTexteEnBigDecimal(nombreTrouve);
    }
    
    // On controle que la quantité ne passe pas de positive à négative et inversement car les traitements sont trop complexe au niveau des
    // tarifs donc on interdit, l'utilisateur doit supprimer sa ligne et saisir la bonne quantité
    if (quantite.compareTo(BigDecimal.ZERO) < 0 && ligneVente.getQuantiteUCV().compareTo(BigDecimal.ZERO) > 0) {
      throw new MessageErreurException("Vous avez saisi une quantité négative, c'est à dire un retour d'article."
          + "\n\nCette opération est impossible dans le détail de ligne article."
          + "\n\nPour procéder à un retour, saisissez votre quantité négative dans le tableau de résultat de la recherche d'article.");
    }
    else if (quantite.compareTo(BigDecimal.ZERO) > 0 && ligneVente.getQuantiteUCV().compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("Vous avez saisi une quantité positive sur une ligne de retour d'article."
          + "\n\nCette opération est impossible dans le détail de ligne article."
          + "\n\nPour procéder à une vente, créez une nouvelle ligne et supprimez la ligne de retour si nécessaire.");
    }
    
    boolean saisieEnUS = lettreTrouve.equals("p") || lettreTrouve.equals("v") || lettreTrouve.equals("c");
    
    // Cas de la déconsignation des palettes
    if (quantite.compareTo(BigDecimal.ZERO) < 0 && article.isArticlePalette()) {
      ligneVente.setQuantiteUV(quantite);
    }
    // Cas d'un article découpable
    else if (article.isArticleDecoupable()) {
      calculerDimensionDecoupe();
    }
    // Cas d'un article non découpable
    else {
      if (saisieEnUS) {
        BigDecimal quantiteCommandeUc = quantite.multiply(article.getNombreUSParUCS(true));
        ligneVente.setQuantiteUCV(quantiteCommandeUc);
      }
      else {
        if (lettreTrouve.equals("m")) {
          modifierQuantiteCommandeeUV(nombreTrouve);
        }
        else {
          // Si l'UCV de l'article n'est pas scindable on arrondi la quantité
          if (article.isUniteConditionnementVenteNonScindable()) {
            quantite = quantite.setScale(getNombreDecimalesUC(), RoundingMode.HALF_UP);
          }
          ligneVente.setQuantiteUCV(quantite);
        }
      }
      recalculMontantTotal();
    }
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    
    construireMessageUCS();
    rafraichir();
  }
  
  /**
   * Modifier la quantité commandée en UV.
   */
  public void modifierQuantiteCommandeeUV(String saisie) {
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(saisie);
    if (quantite.compareTo(ligneVente.getQuantiteUV()) == 0) {
      return;
    }
    
    BigDecimal quantiteCommandeeUV = quantite;
    
    // Si article non découpable et UCV non scindable et quantité saisie en UV on arrondi à la quantité en UC supérieure
    BigDecimal division[] = null;
    // Controle que le conditionnment ne vaut pas 0 afin d'éviter une division par zéro
    if (article.isDefiniNombreUVParUCV()) {
      division = quantiteCommandeeUV.divideAndRemainder(article.getNombreUVParUCV());
    }
    if (division != null && !article.isArticleDecoupable() && article.isUniteConditionnementVenteNonScindable()
        && division[1].compareTo(BigDecimal.ZERO) != 0) {
      quantiteCommandeeUV = division[0].add(BigDecimal.ONE).multiply(article.getNombreUVParUCV())
          .setScale(ligneVente.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    }
    
    ligneVente.modifierQuantiteUV(calculPrixVente, quantiteCommandeeUV);
    recalculMontantTotal();
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification de l'affichage ou non des marges.
   */
  public void modifierAfficherDetails(boolean pAfficherDetails) {
    if (detailMargeVisible == pAfficherDetails) {
      return;
    }
    detailMargeVisible = pAfficherDetails;
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet général
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Afficher la boîte de dialogue de détail du calcul du montant.
   */
  public void afficherDetailCalculMontant() {
    // Charger les données et calculer le prix de vente
    if (calculPrixVente == null) {
      throw new MessageErreurException("Aucun calcul de prix de vente n'est présent.");
    }
    
    // Déterminer le prix à metttre en évidence
    EnumTypePrixVente typePrixVente = EnumTypePrixVente.MONTANT_HT;
    if (isAffichageTTC()) {
      typePrixVente = EnumTypePrixVente.MONTANT_TTC;
    }
    
    // Afficher la boite de dialogue du détail du calcul d'un prix de vente
    ModeleDialogueDetailPrixVente modele = new ModeleDialogueDetailPrixVente(getSession(), calculPrixVente, typePrixVente);
    DialogueDetailPrixVente vue = new DialogueDetailPrixVente(modele);
    vue.afficher();
  }
  
  /**
   * 
   * Saisir les lots sur les articles lotis.
   *
   */
  public void saisirLot() {
    if (article.isArticleLot()) {
      ModeleLot modeleLot = new ModeleLot(getSession(), ligneVente.getId(), article, getQuantiteUV());
      if (documentVente.isAvoir()) {
        modeleLot.setIdDocumentOrigine(modeleComptoir.getIdDocumentOrigineAvoir());
      }
      VueLot vueLot = new VueLot(modeleLot);
      vueLot.afficher();
      
      // Sortie avec validation
      if (modeleLot.isSortieAvecValidation()) {
        // Modification de la quantité vendue sur ligne si on l'a adaptée au nombre de lots saisis
        if (modeleLot.getQuantiteCommande().compareTo(getQuantiteUV()) < 0) {
          ligneVente.setQuantiteUCV(modeleLot.getQuantiteCommande());
        }
        // mise à jour de la ligne pour indiquer si la saisie des lots est complète
        if (modeleLot.isSaisieLotComplete()) {
          ligneVente.setTopNumSerieOuLot(40);
        }
        else {
          ligneVente.setTopNumSerieOuLot(30);
        }
        rafraichir();
      }
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet négociation
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifie l'affichge soit en HT soit en TTC.
   */
  public void modifierAffichageTTC(boolean pAffichageTTC) {
    if (affichageTTC == pAffichageTTC) {
      return;
    }
    affichageTTC = pAffichageTTC;
    // On indique à la liste des colonnes tarif le type d'affichage encours
    if (listeColonnesTarif != null) {
      for (ColonneTarif colonne : listeColonnesTarif) {
        colonne.setAffichageTTC(affichageTTC);
      }
    }
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le mode de négociation
   */
  public void modifierModeNegociation(boolean pModeNegociation) {
    if (modeNegociation == pModeNegociation) {
      return;
    }
    modeNegociation = pModeNegociation;
    
    if (pModeNegociation == MODE_NEGOCIATION_ACHAT) {
      chargerOngletNegociationAchat();
    }
    else if (pModeNegociation == MODE_NEGOCIATION_VENTE) {
      // Modifier le prix de revient standard de la négociation vente
      calculerPrixRevientStandardHTEnUV();
    }
    rafraichir();
  }
  
  /**
   * Modifier le prix base.
   */
  public void modifierPrixBase(String pSaisie) {
    // Dans le cas où le prix base saisi a été effacé
    if (Constantes.normerTexte(pSaisie).isEmpty()) {
      // Les prix base initiaux et la provenance sont remis
      calculPrixVente.getParametreLigneVente().setPrixBaseHT(ligneVenteOrigine.getPrixBaseHT());
      calculPrixVente.getParametreLigneVente().setPrixBaseTTC(ligneVenteOrigine.getPrixBaseTTC());
      calculPrixVente.getParametreLigneVente().setProvenancePrixBase(ligneVenteOrigine.getProvenancePrixBase());
      calculPrixVente.calculer();
      ligneVente.initialiserPrixVente(calculPrixVente);
    }
    // Dans le cas où un prix base a été saisi
    else {
      BigDecimal prixBaseSaisi = Constantes.convertirTexteEnBigDecimal(pSaisie);
      
      // Conversion du prix saisi en TTC car le document est en TTC mais l'affichage en HT
      if (documentVente.isTTC() && !affichageTTC) {
        prixBaseSaisi =
            OutilCalculPrix.calculerPrixTTC(prixBaseSaisi, ligneVente.getPourcentageTVA(), calculPrixVente.getArrondiNombreDecimale());
      }
      // Conversion du prix saisi en HT car le document est en HT mais l'affichage en TTC
      else if (!documentVente.isTTC() && affichageTTC) {
        prixBaseSaisi = OutilCalculPrix.calculerPrixHTAvecPrixTTC(prixBaseSaisi, ligneVente.getPourcentageTVA(),
            calculPrixVente.getArrondiNombreDecimale());
      }
      ligneVente.modifierPrixBase(calculPrixVente, prixBaseSaisi, documentVente.isTTC(), isModeNegoce());
    }
    
    // Calcul de l'indice de marge et du taux de marque de la ligne de vente négociée
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
    tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la colonne tarif.
   */
  public void modifierNumeroColonne(ColonneTarif pColonneTarif) {
    // Vérifier la présence des pré-requis : ligne de vente et calcul prix vente
    if (ligneVente == null || calculPrixVente == null) {
      return;
    }
    
    // Convertir la valeur saisie
    Integer numeroColonneTarif = null;
    if (pColonneTarif != null) {
      numeroColonneTarif = pColonneTarif.getNumeroColonne();
    }
    
    // Vérifier si la valeur a changé
    if (Constantes.equals(numeroColonneTarif, ligneVente.getNumeroColonneTarif())) {
      return;
    }
    
    // Tester si on est en mode négoce
    if (isModeNegoce()) {
      // Effacer les provenances
      calculPrixVente.mettreAZeroProvenance();
      
      // Mettre à jour le numéro de la colonne tarif et sa provenance
      if (pColonneTarif != null) {
        calculPrixVente.getParametreLigneVente().setNumeroColonneTarif(numeroColonneTarif);
        calculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
        
        // Faire un premier calcul du prix de vente afin de récupérer les prix de base de la colonne tarif
        calculPrixVente.calculer();
        FormulePrixVente formulePrixVenteStandard = calculPrixVente.getFormulePrixVenteStandard();
        if (formulePrixVenteStandard != null) {
          calculPrixVente.getParametreLigneVente().setPrixNetSaisiHT(formulePrixVenteStandard.getPrixBaseColonneTarifHT());
          calculPrixVente.getParametreLigneVente().setPrixNetSaisiTTC(formulePrixVenteStandard.getPrixBaseColonneTarifTTC());
          calculPrixVente.getParametreLigneVente().setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
        }
      }
      else {
        // Dans le cas où la colonne tarif saisie a été effacé
        // La colonne tarif initiale et la provenance sont remis
        calculPrixVente.getParametreLigneVente().setNumeroColonneTarif(null);
        calculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(null);
      }
    }
    else {
      // Mettre à jour le numéro de la colonne tarif et sa provenance
      if (pColonneTarif != null) {
        calculPrixVente.getParametreLigneVente().setNumeroColonneTarif(numeroColonneTarif);
        calculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
      }
      else {
        // Dans le cas où la colonne tarif saisie a été effacé
        // La colonne tarif initiale et la provenance sont remis
        calculPrixVente.getParametreLigneVente().setNumeroColonneTarif(ligneVenteOrigine.getNumeroColonneTarif());
        calculPrixVente.getParametreLigneVente().setProvenanceColonneTarif(ligneVenteOrigine.getProvenanceColonneTarif());
      }
    }
    
    // Recalculer le prix de vente
    calculPrixVente.calculer();
    
    // Mettre à jour la ligne de vente à partir du calcul
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Calcul de l'indice de marge et du taux de marque de la ligne de vente négociée
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
    tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le prix net.
   */
  public void modifierPrixNet(String pSaisie) {
    // Vérifier la présence des pré-requis : ligne de vente et calcul prix vente
    if (ligneVente == null || calculPrixVente == null) {
      return;
    }
    
    // Convertir la valeur saisie
    BigDecimal prixNetSaisi = null;
    if (pSaisie != null && !pSaisie.trim().isEmpty()) {
      prixNetSaisi = Constantes.convertirTexteEnBigDecimal(pSaisie);
      
      // Convertir le prix saisi en TTC car le document est en TTC mais l'affichage en HT
      if (documentVente.isTTC() && !affichageTTC) {
        prixNetSaisi =
            OutilCalculPrix.calculerPrixTTC(prixNetSaisi, ligneVente.getPourcentageTVA(), calculPrixVente.getArrondiNombreDecimale());
      }
      // Convertir le prix saisi en HT car le document est en HT mais l'affichage en TTC
      else if (!documentVente.isTTC() && affichageTTC) {
        prixNetSaisi = OutilCalculPrix.calculerPrixHTAvecPrixTTC(prixNetSaisi, ligneVente.getPourcentageTVA(),
            calculPrixVente.getArrondiNombreDecimale());
      }
    }
    
    // Vérifier si la valeur a changé
    if (OutilCalculPrix.equals(documentVente.isTTC(), prixNetSaisi, ligneVenteOrigine.getPrixNetSaisiHT(), prixNetSaisi,
        ligneVenteOrigine.getPrixNetSaisiTTC())) {
      return;
    }
    
    // Effacer les provenances en mode négoce
    if (isModeNegoce()) {
      calculPrixVente.mettreAZeroProvenance();
    }
    
    if (prixNetSaisi == null) {
      // Remettre les prix net initiaux et la provenance dans le cas où le prix net saisi a été effacé
      calculPrixVente.getParametreLigneVente().setPrixNetSaisiHT(ligneVenteOrigine.getPrixNetSaisiHT());
      calculPrixVente.getParametreLigneVente().setPrixNetSaisiTTC(ligneVenteOrigine.getPrixNetSaisiTTC());
      calculPrixVente.getParametreLigneVente().setPrixNetCalculeHT(ligneVenteOrigine.getPrixNetCalculeHT());
      calculPrixVente.getParametreLigneVente().setPrixNetCalculeTTC(ligneVenteOrigine.getPrixNetCalculeTTC());
      calculPrixVente.getParametreLigneVente().setProvenancePrixNet(ligneVenteOrigine.getProvenancePrixNet());
    }
    else {
      // Modifier les paramètres de calcul dans le cas où un prix net a été saisi
      if (documentVente.isTTC()) {
        ligneVente.controlerValiditePrixNetSaisiTTC(prixNetSaisi, false);
        calculPrixVente.getParametreLigneVente().setPrixNetSaisiTTC(prixNetSaisi);
      }
      else {
        ligneVente.controlerValiditePrixNetSaisiHT(prixNetSaisi, false);
        calculPrixVente.getParametreLigneVente().setPrixNetSaisiHT(prixNetSaisi);
      }
      calculPrixVente.getParametreLigneVente().setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    }
    
    // Recalculer le prix de vente
    calculPrixVente.calculer();
    
    // Mettre à jour la ligne de vente à partir du calcul
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Calcul de l'indice de marge et du taux de marque de la ligne de vente négociée
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
    tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise.
   */
  public void modifierRemise(String pSaisie) {
    // Dans le cas où le taux de remise saisie a été effacé
    if (Constantes.normerTexte(pSaisie).isEmpty()) {
      // En mode négoce, rien n'est fait
      if (isModeNegoce()) {
        return;
      }
      // Les taux de remises initiaux et la provenance sont remis
      calculPrixVente.getParametreLigneVente().setTauxRemise1(ligneVenteOrigine.getTauxRemise1());
      calculPrixVente.getParametreLigneVente().setTauxRemise2(ligneVenteOrigine.getTauxRemise2());
      calculPrixVente.getParametreLigneVente().setTauxRemise3(ligneVenteOrigine.getTauxRemise3());
      calculPrixVente.getParametreLigneVente().setTauxRemise4(ligneVenteOrigine.getTauxRemise4());
      calculPrixVente.getParametreLigneVente().setTauxRemise5(ligneVenteOrigine.getTauxRemise5());
      calculPrixVente.getParametreLigneVente().setTauxRemise6(ligneVenteOrigine.getTauxRemise6());
      calculPrixVente.getParametreLigneVente().setAssietteTauxRemise(ligneVenteOrigine.getAssietteTauxRemise());
      calculPrixVente.getParametreLigneVente().setProvenanceTauxRemise(ligneVenteOrigine.getProvenanceTauxRemise());
      calculPrixVente.calculer();
      ligneVente.initialiserPrixVente(calculPrixVente);
    }
    // Dans le cas où un taux de remise a été saisi
    else {
      // Contrôle de la valeur saisie
      BigDecimal tauxRemiseSaisi = Constantes.convertirTexteEnBigDecimal(pSaisie);
      ligneVente.modifierTauxRemise(calculPrixVente, tauxRemiseSaisi, documentVente.isTTC(), isModeNegoce());
    }
    
    // Calcul de l'indice de marge et du taux de marque de la ligne de vente négociée
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
    tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier l'indice de marge.
   */
  public void modifierIndiceDeMarge(String pSaisie) {
    // Contrôle de la valeur saisie
    BigDecimal indiceMarge = Constantes.convertirTexteEnBigDecimal(pSaisie);
    indiceMarge = ligneVente.controlerValiditeIndiceDeMarge(indiceMarge, false);
    
    // Calcul du prix net à l'aide du nouvel indice
    if (ligneVente.getPrixDeRevientLigneHT() == null || ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) == 0) {
      indiceDeMarge = null;
    }
    else {
      BigDecimal prixNetHT = ArrondiPrix.appliquer(
          ligneVente.getPrixDeRevientLigneHT().multiply(indiceMarge).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32),
          calculPrixVente.getArrondiNombreDecimale());
      ligneVente.modifierPrixNet(calculPrixVente, prixNetHT, affichageTTC, true);
      // Calcul de l'indice de marge et du taux de marque suite au recalcul du prix net
      indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
      tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    }
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la marge (ou taux de marque).
   */
  public void modifierMarge(String pSaisie) {
    BigDecimal tauxMarque = Constantes.convertirTexteEnBigDecimal(pSaisie);
    tauxMarque = ligneVente.controlerValiditeTauxDeMarque(tauxMarque, false);
    
    // Calcule un nouveau prix net HT à partir du nouvel indice
    if (ligneVente.getPrixDeRevientLigneHT() != null && Constantes.VALEUR_CENT.subtract(tauxMarque).compareTo(BigDecimal.ZERO) != 0) {
      BigDecimal prixNetHT = ArrondiPrix.appliquer(Constantes.VALEUR_CENT.multiply(ligneVente.getPrixDeRevientLigneHT())
          .divide(Constantes.VALEUR_CENT.subtract(tauxMarque), MathContext.DECIMAL32), calculPrixVente.getArrondiNombreDecimale());
      ligneVente.modifierPrixNet(calculPrixVente, prixNetHT, false, true);
      // Calcul de l'indice de marge et du taux de marque suite au recalcul du prix net
      indiceDeMarge = ligneVente.calculerIndiceDeMarge(calculPrixVente.getArrondiNombreDecimale());
      tauxDeMarque = ligneVente.calculerTauxDeMarque(calculPrixVente.getArrondiNombreDecimale());
    }
    else {
      tauxDeMarque = null;
    }
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le mode gratuit.
   * 
   * Le caractère gratuit d'une ligne de vente n'est pas persisté sous forme d'un indicateur à part entière. C'est la présence d'un
   * type de gratuit qui permet de définir que la ligne de vente est gratuite. Cette méthode sélectionne donc le premier type de gratuit
   * disponible dans la liste des types de gratuit lorsque le mode gratuit est activé. Lorsqu'il est désactivé, cela efface le type de
   * gratuit de la ligne de vente.
   * 
   * @param pModeGratuit true=gratuit, false=non gratuit.
   */
  public void modifierModeGratuit(boolean pModeGratuit) {
    // Tester si la valeur a changé
    if (pModeGratuit == ligneVente.isLigneGratuite()) {
      return;
    }
    
    // Définir le type de gratuit pour que la ligne de vente devienne gratuite
    IdTypeGratuit idTypeGratuit = null;
    if (pModeGratuit) {
      // Vérifier que les types de gratuits sont paramétrés
      if (listeTypeGratuit == null || listeTypeGratuit.isEmpty()) {
        throw new MessageErreurException(
            "Les types de gratuits ne sont pas paramétrés. Rendez-vous dans la personnalisation des ventes (personnalisation TG) et créez "
                + "le ou les types appropriés.");
      }
      
      // Sélectionner le premier type de gratuit par défaut
      idTypeGratuit = listeTypeGratuit.get(0).getId();
    }
    else {
      idTypeGratuit = IdTypeGratuit.NON_GRATUIT;
    }
    
    // Modifier les paramètres du calcul de prix de vente et recalculer le nouveau prix
    ParametreLigneVente parametreLigneVente = calculPrixVente.getParametreLigneVente();
    parametreLigneVente.setIdTypeGratuit(idTypeGratuit);
    calculPrixVente.calculer();
    
    // Mettre à jour les données de tarif de vente de la ligne d evente
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Reclaculer le montant total
    recalculMontantTotal();
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le type de gratuit.
   * @param pTypeGratuit Type de gratuit.
   */
  public void modifierTypeGratuit(TypeGratuit pTypeGratuit) {
    // Transformer la valeur saisie
    IdTypeGratuit idTypeGratuit = null;
    if (pTypeGratuit != null) {
      idTypeGratuit = pTypeGratuit.getId();
    }
    else {
      idTypeGratuit = IdTypeGratuit.NON_GRATUIT;
    }
    
    // Tester si la valeur a changé
    if (Constantes.equals(idTypeGratuit, ligneVente.getIdTypeGratuit())) {
      return;
    }
    
    // Modifier les paramètres du calcul de prix de vente et recalculer le nouveau prix
    ParametreLigneVente parametreLigneVente = calculPrixVente.getParametreLigneVente();
    parametreLigneVente.setIdTypeGratuit(idTypeGratuit);
    calculPrixVente.calculer();
    
    // Mettre à jour les données de tarif de vente de la ligne d evente
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Rafraichir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier prix achat brut.
   */
  public void modifierPrixAchatBrut(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le prix d'achat brut car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPrixAchatBrutHT(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  //
  /**
   * Modifier la remise 1 fournisseur.
   */
  public void modifierRemiseFrs1(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier la remise 1 du fournisseur car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPourcentageRemise1(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  //
  /**
   * Modifier la remise 2 fournisseur.
   */
  public void modifierRemiseFrs2(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier la remise 2 du fournisseur car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPourcentageRemise2(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 3 fournisseur.
   */
  public void modifierRemiseFrs3(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier la remise 3 du fournisseur car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPourcentageRemise3(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 4 fournisseur.
   */
  public void modifierRemiseFrs4(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier la remise 4 du fournisseur car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPourcentageRemise4(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la taxe ou majoration.
   */
  public void modifierTaxeMajoration(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier la taxe de majoration car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setPourcentageTaxeOuMajoration(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage de majoration.
   */
  public void modifierFraisExploitation(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier les frais d'exploitation car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setFraisExploitation(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur de conditionnement.
   */
  public void modifierValeurConditionnementAchat(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le montant du conditionnement car il n'y a pas de condition d'achats");
    }
    
    conditionAchatLigne.setMontantConditionnement(Constantes.convertirTexteEnBigDecimal(pSaisie));
    calculerPrixRevientStandardHTEnUV();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le type de saisie du port
   * Le port est soit non saisissable, soit saisi en montant et poids, soti saisi en pourcentage.
   */
  public void modifierTypeSaisiePort(int pTypeSaisiePort) {
    // Vérifier la présence d'une condition d'achat
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le type de saisie du port car il n'y a pas de condition d'achats");
    }
    
    // Vérifier si la valeur a changé
    if (pTypeSaisiePort == typeSaisiePort) {
      return;
    }
    
    // Mettre à jour la valeur
    typeSaisiePort = pTypeSaisiePort;
    
    // Remettre à zéro les valeurs qui ne sont pas concernées par le nouveau type de saisie
    if (pTypeSaisiePort == TYPE_SAISIE_PORT_MONTANT_ET_POIDS) {
      conditionAchatLigne.setPourcentagePort(BigDecimal.ZERO);
    }
    else if (pTypeSaisiePort == TYPE_SAISIE_PORT_POURCENTAGE) {
      conditionAchatLigne.setMontantAuPoidsPortHT(BigDecimal.ZERO);
      conditionAchatLigne.setPoidsPort(BigDecimal.ZERO);
    }
    else {
      conditionAchatLigne.setPourcentagePort(BigDecimal.ZERO);
      conditionAchatLigne.setPoidsPort(BigDecimal.ZERO);
      conditionAchatLigne.setPourcentagePort(BigDecimal.ZERO);
    }
    
    // Mettre à jour le prix de revient de la ligne
    calculerPrixRevientStandardHTEnUV();
    
    // Rafraichir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur des frais de port.
   */
  public void modifierMontantPort(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le montant du port car il n'y a pas de condition d'achats");
    }
    
    // Vérifier que le mode de saisie en cours permet cette saisie
    if (typeSaisiePort != TYPE_SAISIE_PORT_MONTANT_ET_POIDS) {
      throw new MessageErreurException("Impossible de modifier le montant du port car ce mode de saisie n'est pas actif");
    }
    
    // Vérifier si la valeur a changé
    BigDecimal montantPort = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (montantPort.compareTo(conditionAchatLigne.getMontantAuPoidsPortHT()) == 0) {
      return;
    }
    
    // Mettre à jour la valeur
    conditionAchatLigne.setMontantAuPoidsPortHT(montantPort);
    
    // Mettre à jour le prix de revient de la ligne
    calculerPrixRevientStandardHTEnUV();
    
    // Rafraichir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le poids des frais de port.
   */
  public void modifierPoidsPort(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le poids du port car il n'y a pas de condition d'achats");
    }
    
    // Vérifier que le mode de saisie en cours permet cette saisie
    if (typeSaisiePort != TYPE_SAISIE_PORT_MONTANT_ET_POIDS) {
      throw new MessageErreurException("Impossible de modifier le poids du port car ce mode de saisie n'est pas actif");
    }
    
    // Vérifier si la valeur a changé
    BigDecimal poidsPort = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (poidsPort.compareTo(conditionAchatLigne.getPoidsPort()) == 0) {
      return;
    }
    
    // Mettre à jour la valeur
    conditionAchatLigne.setPoidsPort(poidsPort);
    
    // Mettre à jour le prix de revient de la ligne
    calculerPrixRevientStandardHTEnUV();
    
    // Rafraichir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage des frais de port.
   */
  public void modifierPourcentagePort(String pSaisie) {
    // Vérifier la présence d'une condition d'achats
    if (conditionAchatLigne == null) {
      throw new MessageErreurException("Impossible de modifier le pourcentage de frais de port car il n'y a pas de condition d'achats");
    }
    
    // Vérifier que le mode de saisie en cours permet cette saisie
    if (typeSaisiePort != TYPE_SAISIE_PORT_POURCENTAGE) {
      throw new MessageErreurException("Impossible de modifier le pourcentage du port car ce mode de saisie n'est pas actif");
    }
    
    // Vérifier si la valeur a changé
    BigDecimal pourcentagePort = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (pourcentagePort.compareTo(conditionAchatLigne.getPourcentagePort()) == 0) {
      return;
    }
    
    // Mettre à jour la valeur
    conditionAchatLigne.setPourcentagePort(pourcentagePort);
    
    // Mettre à jour le prix de revient de la ligne
    calculerPrixRevientStandardHTEnUV();
    
    // Rafraichir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet informations techniques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Ouvre l'url du bouton à l'indice passé
   */
  public void ouvrirURL(int indiceBouton) {
    getSession().getLexique().ouvrirURL(getListeURL()[indiceBouton]);
  }
  
  /**
   * Modifier le mémo ITC de l'article.
   */
  public void modifierMemoITC(String pTexteSaisi) {
    if (memoITC == null && (pTexteSaisi == null || pTexteSaisi.isEmpty())) {
      return;
    }
    if (memoITC != null && Constantes.equals(pTexteSaisi, memoITC.getTexte())) {
      return;
    }
    
    // Créer le mémo s'il n'existe pas
    if (memoITC == null) {
      IdMemo idMemo = IdMemo.getInstancePourArticle(article.getId(), 1);
      memoITC = new Memo(idMemo);
      memoITC.setType(EnumTypeMemo.INFORMATION_TECHNICOCOMMERCIALE);
    }
    
    // Modifier le texte
    memoITC.setTexte(pTexteSaisi);
    sauverMemoITC = true;
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier top mémo obligatoire
   */
  public void modifierTopMemo(boolean obligatoire) {
    article.setAffichageMemoObligatoire(obligatoire);
    ManagerServiceArticle.sauverTopMemoObligatoire(getIdSession(), article);
  }
  
  /**
   * Enregistrer mémo ITC comme de nouvelles lignes commentaire.
   */
  public void copierMemoITCversCommentaire() {
    if (memoITC == null) {
      return;
    }
    String[] texteEnTable = memoITC.getTexte().split("\\n");
    int numeroLigne[] = new int[texteEnTable.length];
    for (int i = 0; i < texteEnTable.length; i++) {
      IdLigneVente idLigneVente = IdLigneVente.getInstance(documentVente.getId().getIdEtablissement(),
          documentVente.getId().getCodeEntete(), documentVente.getId().getNumero(), documentVente.getId().getSuffixe(), 0);
      
      LigneVente ligneVente = new LigneVente(idLigneVente);
      ligneVente.setLibelle(texteEnTable[i]);
      boolean isEditionSurFacture = false;
      if (client.isClientComptant()) {
        isEditionSurFacture = true;
      }
      numeroLigne[i] =
          ManagerServiceDocumentVente.sauverLigneVenteCommentaire(getIdSession(), ligneVente, documentVente, isEditionSurFacture, false);
    }
    quitterAvecValidation();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet stock
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Defini le sous onglet stock actif
   */
  public void modifierSousOngletStock(int selectedIndex) {
    sousOngletActif = selectedIndex;
    chargerOngletStock();
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet historique
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le type de documents à afficher dans l'historique.
   */
  public void modifierFiltreTypeDocuments(int saisie) {
    if (saisie == indexFiltreTypeDocuments) {
      return;
    }
    
    indexFiltreTypeDocuments = saisie;
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet dimensions
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifie le nombre d'éléments pour un article à dimension variable.
   */
  public void modifierNombre(String pNombre) {
    int valeur = Constantes.convertirTexteEnInteger(pNombre);
    if (valeur == nombreDecoupe) {
      return;
    }
    nombreDecoupe = valeur;
    calculerDimensionDecoupe();
    rafraichir();
  }
  
  /**
   * Modifie la longueur de l'article à dimension variable.
   */
  public void modifierLongueur(String pLongueur) {
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pLongueur);
    if (saisie.compareTo(longueur) == 0) {
      return;
    }
    
    // Interdire les saisies supérieures à la dimension initiale de l'article
    BigDecimal longueurEnMetre = Article.convertirEnMetres(saisie, article.getTypeUniteLongueur(), false);
    article.controlerLongueurDecoupe(longueurEnMetre);
    
    // Mettre à jour les dimensions
    longueur = saisie.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    calculerDimensionDecoupe();
    
    // Rafraichir
    rafraichir();
  }
  
  /**
   * Modifie la largeur de l'article à dimension variable.
   */
  public void modifierLargeur(String pLargeur) {
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pLargeur);
    if (saisie.compareTo(largeur) == 0) {
      return;
    }
    
    // Interdire les saisies supérieures à la dimension initiale de l'article
    BigDecimal largeurEnMetre = Article.convertirEnMetres(saisie, article.getTypeUniteLargeur(), false);
    article.controlerLargeurDecoupe(largeurEnMetre);
    
    // Mettre à jour les dimensions
    largeur = saisie.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    calculerDimensionDecoupe();
    
    // Rafraichir
    rafraichir();
  }
  
  /**
   * Modifie la hauteur de l'article à dimension variable.
   */
  public void modifierHauteur(String pHauteur) {
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pHauteur);
    if (saisie.compareTo(hauteur) == 0) {
      return;
    }
    
    // Interdire les saisies supérieures à la dimension initiale de l'article
    BigDecimal hauteurEnMetre = Article.convertirEnMetres(saisie, article.getTypeUniteHauteur(), false);
    article.controlerHauteurDecoupe(hauteurEnMetre);
    
    // Mettre à jour les dimensions
    hauteur = saisie.setScale(article.getNombreDecimaleUV(), RoundingMode.HALF_UP);
    calculerDimensionDecoupe();
    
    // Rafraichir
    rafraichir();
  }
  
  /**
   * Interdit l'enregistrement d'une ligne en erreur
   */
  public void empecherValidation() {
    nePasEnregistrer = true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes événementielles de l'onglet liens
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Recalcul du montant total sur onglet général.
   * Devrait être privée <= Bug à corriger.
   */
  public void recalculMontantTotal() {
    if (ligneVente == null) {
      return;
    }
    if (article.isArticleDecoupable()) {
      calculerDimensionDecoupe();
    }
    else {
      ligneVente.modifierQuantiteUV(calculPrixVente, ligneVente.getQuantiteUV());
    }
  }
  
  /**
   * Calcul dimensions de la découpe.
   */
  private void calculerDimensionDecoupe() {
    if (typeDimension.equals(EnumTypeDecoupeArticle.IMPOSSIBLE)) {
      return;
    }
    
    // Conversion des dimensions en mètres avant d'effectuer les calculs
    BigDecimal longueurEnMetre = Article.convertirEnMetres(longueur, article.getTypeUniteLongueur(), false);
    BigDecimal largeurEnMetre = Article.convertirEnMetres(largeur, article.getTypeUniteLargeur(), false);
    BigDecimal hauteurEnMetre = Article.convertirEnMetres(hauteur, article.getTypeUniteHauteur(), false);
    
    // Calcul de la dimension
    BigDecimal dimensionUnitaire = BigDecimal.ZERO;
    switch (typeDimension) {
      case LONGUEUR:
        dimensionUnitaire = longueurEnMetre.setScale(getNombreDecimalesUV(), RoundingMode.HALF_UP);
        break;
      
      case SURFACE:
        // On calcule la dimension pour une unité pour les problèmes d'arrondi
        dimensionUnitaire = longueurEnMetre.multiply(largeurEnMetre).setScale(getNombreDecimalesUV(), RoundingMode.HALF_UP);
        break;
      
      case VOLUME:
        // On calcule la dimension pour une unité pour les problèmes d'arrondi
        dimensionUnitaire =
            longueurEnMetre.multiply(largeurEnMetre).multiply(hauteurEnMetre).setScale(getNombreDecimalesUV(), RoundingMode.HALF_UP);
        break;
      
      default:
        throw new MessageErreurException("Impossible de calculer la dimension de la découpe sur le type de dimension : " + typeDimension);
    }
    // Calcul de la dimension totale à partir de la quantité totale
    dimension = new BigDecimal(nombreDecoupe).multiply(dimensionUnitaire).setScale(getNombreDecimalesUV(), RoundingMode.HALF_UP);
    ligneVente.modifierQuantiteUV(calculPrixVente, dimension);
  }
  
  /**
   * Mise à jour des données de la ligne article concernant un article à dimension variable.
   */
  private void miseAJourDimensions() {
    if (!article.isArticleDecoupable()) {
      return;
    }
    
    calculerDimensionDecoupe();
    
    // Mise à jour des données concernant les dimensions
    ligneVente.setMesure1(longueur);
    ligneVente.setMesure2(largeur);
    ligneVente.setMesure3(hauteur);
    BigDecimal nombreArticlesDecoupes = new BigDecimal(nombreDecoupe);
    ligneVente.setQuantiteArticlesDecoupe(nombreArticlesDecoupes, article);
    
    // Mise à jour du libellé de la ligne de vente
    BigDecimal dimensionUnitaire = BigDecimal.ZERO;
    if (nombreArticlesDecoupes.compareTo(BigDecimal.ZERO) > 0) {
      dimensionUnitaire = dimension.divide(nombreArticlesDecoupes, getNombreDecimalesUV(), RoundingMode.HALF_UP);
    }
    if (ligneVente.getLibelle().contains(PREFIXE_LIBELLE_ARTICLE_DECOUPE)) {
      String ancien = ligneVente.getLibelle().substring(0, ligneVente.getLibelle().indexOf(PREFIXE_LIBELLE_ARTICLE_DECOUPE));
      ligneVente.setLibelle(ancien + PREFIXE_LIBELLE_ARTICLE_DECOUPE + Constantes.formater(dimensionUnitaire, true) + " "
          + ligneVente.getIdUniteVente().getCode() + ")");
      
    }
    else {
      ligneVente.setLibelle(ligneVente.getLibelle() + PREFIXE_LIBELLE_ARTICLE_DECOUPE + Constantes.formater(dimensionUnitaire, true) + " "
          + ligneVente.getIdUniteVente().getCode() + ")");
    }
  }
  
  /**
   * Enregistre les modifications sur la ligne article.
   */
  private boolean enregistrerLigneArticle() {
    if (nePasEnregistrer) {
      nePasEnregistrer = false;
      rafraichir();
      return false;
    }
    
    // Mise à jour des dimension si l'article est découpable
    miseAJourDimensions();
    
    if (ligneVente.isLigneCommentaire() || ligneVente.isLignePalette()) {
      return true;
    }
    
    // Contrôle de la ligne de vente
    AlertesLigneDocument alerteLigne = controlerLigneDocument(ligneVente);
    if (alerteLigne.getMessages().trim().isEmpty()) {
      return true;
    }
    else if ((alerteLigne.getMessages().contains("Prix de Revient") || alerteLigne.getMessages().contains("% de Marge"))
        && modeleComptoir.isVenteSousPRV()) {
      return true;
    }
    else {
      // Dans le cas d'un message d'erreur suite au contrôle de la ligne, la ligne de vente est écrasée par la ligne de vente d'origine
      ligneVente = ligneVenteOrigine.clone();
      
      rafraichir();
      return false;
    }
  }
  
  /**
   * Contrôle les lignes d'un document.
   */
  private AlertesLigneDocument controlerLigneDocument(LigneVente pLigneVente) {
    AlertesLigneDocument alerte = new AlertesLigneDocument();
    try {
      alerte = ManagerServiceDocumentVente.controlerLigneDocumentVente(getIdSession(), pLigneVente, modeleComptoir.getDateTraitement());
    }
    catch (MessageErreurException e) {
      alerte.setMessages(e.getMessage());
      if (!modeleComptoir.isVenteSousPRV() && alerte.getMessages().contains("Prix de Revient")) {
        modeleComptoir.payerSousPRV(pLigneVente);
        return alerte;
      }
      else if (modeleComptoir.isVenteSousPRV()
          && (alerte.getMessages().contains("% Remise >") || alerte.getMessages().contains("Prix de Revient"))) {
        return alerte;
      }
      throw e;
    }
    return alerte;
  }
  
  /**
   * Met à jour les données de la négociation d'achat
   */
  private void sauverNegociationAchat() {
    if (!ongletNegociationVenteCharge || conditionAchatLigne == null) {
      return;
    }
    
    // Sauvegarder la condition d'achats à la ligne uniquement si le PRS a changé
    if (sauvegardePRSConditionAchat.compareTo(conditionAchatLigne.getPrixRevientStandardHT()) == 0) {
      return;
    }
    
    // Sauver la condition d'achats pour la ligne du documents de ventes
    conditionAchatLigne.setTypeCondition(EnumTypeConditionAchat.NEGOCIATION_ACHAT_SUR_DOCUMENT_VENTE);
    conditionAchatLigne.setNombreDecimalesUA(getNombreDecimalesUV());
    conditionAchatLigne.setNombreDecimalesUCA(getNombreDecimalesUC());
    ManagerServiceArticle.sauverNegociationAchat(getIdSession(), conditionAchatLigne);
  }
  
  /**
   * Modifier le prix de revient HT de la ligne (en UV) à partir du prix de revient standard de la condition d'achats (en US).
   */
  private void calculerPrixRevientStandardHTEnUV() {
    if (article == null) {
      throw new MessageErreurException("Impossible de calculer le prix de revient en UV car l'article est invalide.");
    }
    
    if (conditionAchatLigne != null) {
      BigDecimal prixRevientStandardHT = conditionAchatLigne.getPrixRevientStandardHTEnUV(article.getCoefficientUSParUV());
      ligneVente.setPrixDeRevientLigneHT(prixRevientStandardHT);
      ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(prixRevientStandardHT, ligneVente.getPourcentageTVA(),
          calculPrixVente.getArrondiNombreDecimale()));
    }
  }
  
  /**
   * Construit un message informatif sur l'équivalence entre quantités commandées et quantités en unité de commande de stock
   */
  private void construireMessageUCS() {
    messageUCS = "";
    LigneVente ligne = getLigneVente();
    boolean verificationConditionnementStock = article.isDefiniNombreUSParUCS();
    BigDecimal conditionnementVente = getArticle().getNombreUVParUCV();
    
    // Si le conditionnement de vente=0 alors on le mets à 1
    if (conditionnementVente == null || conditionnementVente.compareTo(BigDecimal.ZERO) == 0) {
      conditionnementVente = BigDecimal.ONE;
    }
    // Permet de savoir combien d'unite de vente sont cChevauchements sur une fenêtre d'options d'éditionontenu dans l'unite de stock
    // Ex:50U(UV) dans 1carton(US)
    BigDecimal nombreUCVparUS = getArticle().getCoefficientUSParUV().multiply(conditionnementVente);
    
    // Permet de mettre nombreUCVparUS sans chiffre à virgule
    nombreUCVparUS = nombreUCVparUS.setScale(0, RoundingMode.HALF_EVEN);
    
    // Permet de savoir combien d'unite de conditionnement de vente est contenu dans l'unite de conditionnement de stock
    // Ex:50boite(UCV) dans 1 palette(UCS)
    BigDecimal nombreUCVparUCS = nombreUCVparUS.multiply(getArticle().getNombreUSParUCS(true));
    if (ligne != null && article != null && ligne.getIdUniteConditionnementVente() != null && verificationConditionnementStock != false) {
      
      // On divise la quantité saisie par le nombre UCV par UCS sous deux valeur:quotient et reste
      // EX: 15 (valeur donner ds serie N) / 9.99(UCV par UCS) = 1.501 qui vas donner colonne 0 = 1 colonne 1=5.01
      BigDecimal[] division = ligne.getQuantiteUCV().divideAndRemainder(nombreUCVparUCS);
      
      // Permet de faire une phrase cohérente suivant le contenu
      String ponctuationMessage = "";
      
      // Si on a une valeur supérieur UCV au nombre D'UCV contenu dans une UCS on affiche le détail
      // EX: 1 palette = 50 U si la valeur rentrée est 55 alors on vas afficher le détail (c'est à dire 1PA et 5U)
      if (division[0].compareTo(BigDecimal.ZERO) > 0) {
        if (listeUnite != null && article != null && article.getIdUCS() != null) {
          // Vas afficher le nombre d'UCS
          messageUCS = "Soit " + Constantes.formater(division[0], false) + " " + listeUnite.getLibelleUnite(article.getIdUCS());
          ponctuationMessage = ", ";
          if (division[1].compareTo(BigDecimal.ZERO) > 0) {
            // Vas afficher le reste
            messageUCS += " et " + Constantes.formater(division[1], false) + " " + listeUnite.getLibelleUnite(article.getIdUCV());
          }
        }
      }
      messageUCS += ponctuationMessage + "Conditionnement " + nombreUCVparUCS.setScale(getNombreDecimalesUS(), RoundingMode.HALF_UP) + " "
          + getArticle().getIdUCV() + "/" + getArticle().getIdUCS();
    }
    else {
      messageUCS = null;
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'onglet actif.
   * @return Index de l'onglet actif.
   */
  public int getOngleActif() {
    return ongletActif;
  }
  
  /**
   * Retourner le composant qui détient le focus.
   * @return Indice du composant avec le focus.
   */
  public int getComposantAyantLeFocus() {
    int valeur = composantAyantLeFocus;
    composantAyantLeFocus = -1;
    return valeur;
  }
  
  /**
   * Retourner la ligne de vente affichée dans la boîte de dialogue.
   * @return Ligne de vente.
   */
  public LigneVente getLigneVente() {
    return ligneVente;
  }
  
  /**
   * Retourner la ligne de vente d'origine (avant modifications).
   * @return Ligne de vente.
   */
  public LigneVente getLigneVenteOrigine() {
    return ligneVenteOrigine;
  }
  
  /**
   * Retourner le document de vente de la ligne de vente affichée dans la boîte de dialogue
   * @return Document de vente.
   */
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
  /**
   * Retourner l'article de la ligne de vente affichée dans la boîte de dialogue
   * @return Article.
   */
  public Article getArticle() {
    return article;
  }
  
  /**
   * Retourner le fournisseur de la ligne de vente affichée dans la boîte de dialogue
   * @return Fournisseur.
   */
  public Fournisseur getFournisseur() {
    return fournisseur;
  }
  
  /**
   * Retourner le client de la ligne de vente affichée dans la boîte de dialogue
   * @return Client.
   */
  public Client getClient() {
    return client;
  }
  
  /**
   * Retourner la liste des stocks par magasins.
   * @return Liste des tocks par magasin.
   */
  public ListeStockComptoir getListeStockUnMagasin() {
    return listeStockUnMagasin;
  }
  
  /**
   * Retourner la liste des attendus et commandés.
   * @return Liste des attendus et commandés.
   */
  public List<LigneAttenduCommande> getListeLigneAttenduCommande() {
    return listeLigneAttenduCommande;
  }
  
  /**
   * Retourner la liste des mouvements de stock
   * @return Liste des mouvements de stock.
   */
  public LigneMouvement[] getListeMouvementStock() {
    return listeMouvementStock;
  }
  
  /**
   * Retourner la liste des documents historiques.
   * @return Liste des documents historiques.
   */
  public List<DocumentHistoriqueVenteClientArticle> getListeHistorique() {
    return listeHistorique;
  }
  
  /**
   * Retourner l'index du type de documents
   */
  public int getIndexFiltreTypeDocuments() {
    return indexFiltreTypeDocuments;
  }
  
  /**
   * Retourner la liste des URL.
   * @return Liste d'URL.
   */
  public String[] getListeURL() {
    return listeURL;
  }
  
  /**
   * Retourner si l'affichage est en TTC ou ou HT.
   * @return ture=affichage TTC, false=affichage HT.
   */
  public boolean isAffichageTTC() {
    return affichageTTC;
  }
  
  /**
   * Retourner le mode de négociation.
   * @return MODE_NEGOCIATION_VENTE ou MODE_NEGOCIATION_ACHAT.
   */
  public boolean getModeNegociation() {
    return modeNegociation;
  }
  
  /**
   * Indiquer si les informations détailéles sur les marges sont visibles.
   * @return true=informations sur les marges visibles, false=sinon.
   */
  public boolean isDetailMargeVisible() {
    return detailMargeVisible;
  }
  
  /**
   * Retourner la quantité de la ligne de ventge en UV.
   * @return Quanaité en UV.
   */
  public BigDecimal getQuantiteUV() {
    return ligneVente.getQuantiteUV();
  }
  
  /**
   * Retourner la condition d'achat de la ligne
   */
  public ConditionAchat getConditionAchatLigne() {
    return conditionAchatLigne;
  }
  
  /**
   * Retourner la liste des colonnes du tarif.
   * @return Liste des colonnes de tarif.
   */
  public ColonneTarif[] getListeColonnesTarif() {
    return listeColonnesTarif;
  }
  
  /**
   * Retourner la liste des colonnes du tarif pour la saisie en mode classique.
   * Elle contient un premier élément à null.
   * @return Liste des colonnes de tarif.
   */
  public ColonneTarif[] getListeColonnesTarifClassique() {
    return listeColonnesTarifClassique;
  }
  
  /**
   * Retourner le type de dimensions d'un article à dimension variable.
   * @return Type de dimensions.
   */
  public EnumTypeDecoupeArticle getTypeDimension() {
    return typeDimension;
  }
  
  /**
   * Retourne le libellé du type de la dimension.
   */
  public String getLibelleTypeDimension() {
    return typeDimension.getLibelle();
  }
  
  /**
   * Retourner la longueur d'un article à dimension variable.
   * @return Longueur.
   */
  public BigDecimal getLongueur() {
    return longueur;
  }
  
  /**
   * Retourner la largeur d'un article à dimension variable.
   * @return Longueur.
   */
  public BigDecimal getLargeur() {
    return largeur;
  }
  
  /**
   * Retourner la hauteur d'un article à dimension variable.
   * @return Hauteur.
   */
  public BigDecimal getHauteur() {
    return hauteur;
  }
  
  /**
   * Retourner le nombre de découpes d'un article à dimension variable.
   * @return Nombre de découpes.
   */
  public int getNombreDecoupe() {
    return nombreDecoupe;
  }
  
  /**
   * Retourner la dimension d'un article à dimension variable.
   * @return Dimension.
   */
  public BigDecimal getDimension() {
    return dimension;
  }
  
  /**
   * Retourner la vue stocks
   */
  public VueAffichageStockArticle getVueStock() {
    return vueStock;
  }
  
  /**
   * Retourner la vue mouvements de stocks
   */
  public VueAffichageMouvementsStock getVueMouvements() {
    return vueMouvements;
  }
  
  /**
   * Retourner la vue attendus commandés
   */
  public VueAffichageAttendusCommandes getVueAttendus() {
    return vueAttendus;
  }
  
  /**
   * Retourner l'index du sous onglet de stocks actif
   */
  public int getSousOngletActif() {
    return sousOngletActif;
  }
  
  /**
   * Retourner si on est en mode longueur visible
   */
  public boolean isLongueurVisible() {
    return longueurVisible;
  }
  
  /**
   * Retourner si on est en mode largeur visible
   */
  public boolean isLargeurVisible() {
    return largeurVisible;
  }
  
  /**
   * Retourner si on est en mode hauteur visible
   */
  public boolean isHauteurVisible() {
    return hauteurVisible;
  }
  
  /**
   * Retourner le lien d'origine
   */
  public LienLigne getLienOrigine() {
    return lienOrigine;
  }
  
  /**
   * Retourner le messages UCS
   */
  public String getMessageUCS() {
    return messageUCS;
  }
  
  /**
   * Retourner le messages condition chantier
   */
  public Message getMessageConditionChantier() {
    return messageConditionChantier;
  }
  
  /**
   * Indiquer si la ligne de vente est gratuite.
   * @return true=ligne gratuite, false=ligne non gratuite.
   */
  public boolean isModeGratuit() {
    return ligneVente != null && ligneVente.getIdTypeGratuit() != null && ligneVente.getIdTypeGratuit().isGratuit();
  }
  
  /**
   * Retourner le type de gratuit.
   * @return Type de gratuit.
   */
  public TypeGratuit getTypeGratuit() {
    if (ligneVente == null || ligneVente.getIdTypeGratuit() == null || listeTypeGratuit == null) {
      return null;
    }
    return listeTypeGratuit.get(ligneVente.getIdTypeGratuit());
  }
  
  /**
   * Type de saisie des frais de port.
   */
  public int getTypeSaisiePort() {
    return typeSaisiePort;
  }
  
  /**
   * L'indice de marge de la ligne d'origine.
   * @return L'indice de marge.
   */
  public BigDecimal getIndiceDeMargeOrigine() {
    return indiceDeMargeOrigine;
  }
  
  /**
   * Le taux de marque de la ligne d'origine.
   * @return Le taux de marque.
   */
  public BigDecimal getTauxDeMarqueOrigine() {
    return tauxDeMarqueOrigine;
  }
  
  /**
   * L'indice de marge de la ligne négociée.
   * @return L'indice de marge.
   */
  public BigDecimal getIndiceDeMarge() {
    if (indiceDeMarge == null) {
      return BigDecimal.ZERO;
    }
    return indiceDeMarge;
  }
  
  /**
   * Le taux de marque de la ligne négociée.
   * @return Le taux de marque.
   */
  public BigDecimal getTauxDeMarque() {
    return tauxDeMarque;
  }
  
  /**
   * La date du tarif.
   * @return La date.
   */
  public Date getDateTarif() {
    return dateTarif;
  }
  
  /**
   * Retourner le mode de calcul de prix en cours (Négoce ou Classique).
   * @return
   */
  public boolean isModeNegoce() {
    return modeleComptoir.isModeNegoce();
  }
  
  /**
   * Retourne si la modification des quantités articles du document sont possibles.
   */
  public boolean isModifierQuantitePossible() {
    return modeleComptoir.isModifierQuantitePossible();
  }
  
  /**
   * Retourne si l'utilisateur est autorisé à créer des lignes gratuites.
   */
  public boolean isGratuitPossible() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_SUPERVISEUR_GESTION_DES_BONS_ET_FACTURES));
  }
  
  /**
   * Renvoyer le nombre de décimales de l'UCV
   * 
   * Si l'unité de conditionnement est scindable, le nombre de décimales est forcé à la valeur des unités scindables et pas au nombre de
   * décimales défini pour l'unité dans la personnalisation UN.
   */
  public int getNombreDecimalesUC() {
    if (article.getIdUCV() == null) {
      return getPrecisionUnite(article.getIdUV());
    }
    if (article.getIdUCV() == null || article.getIdUCV().equals(article.getIdUV()) || article.isUniteConditionnementVenteNonScindable()) {
      return getPrecisionUnite(article.getIdUCV());
    }
    else {
      return Article.NOMBRE_DECIMALE_UCV_SCINDABLE;
    }
  }
  
  /**
   * Renvoyer le nombre de décimales de l'UV
   */
  private int getNombreDecimalesUV() {
    return ligneVente.getNombreDecimaleUV();
  }
  
  /**
   * Renvoyer le nombre de décimales de l'US
   */
  public int getNombreDecimalesUS() {
    return getPrecisionUnite(article.getIdUCS());
  }
  
  /**
   * Retourne la précision d'une unité donnée
   */
  private int getPrecisionUnite(IdUnite pId) {
    if (listeUnite == null) {
      return 0;
    }
    return listeUnite.getPrecisionUnite(pId);
  }
  
  /**
   * Retourne la précision d'une unité de stock
   */
  public int getPrecisionUniteStock() {
    if (listeUnite != null && article != null && article.getIdUS() != null) {
      return listeUnite.getPrecisionUnite(article.getIdUS());
    }
    return 0;
  }
  
  /**
   * Retourner la colonne tarif de la ligne de vente.
   * @return Colonne tarif.
   */
  public ColonneTarif getColonneTarif() {
    if (ligneVente == null || ligneVente.getNumeroColonneTarif() == null || listeColonnesTarif == null) {
      return null;
    }
    int index = ligneVente.getNumeroColonneTarif() - 1;
    if (index >= 0 && index < listeColonnesTarif.length) {
      return listeColonnesTarif[index];
    }
    return null;
  }
  
  /**
   * Retourne le texte du mémo.
   */
  public String getTexteMemoITC() {
    if (memoITC != null) {
      return memoITC.getTexte();
    }
    return "";
  }
  
  /**
   * Lecture de la sécurité sur le droit à négocier les achats.
   */
  public boolean isAutoriseNegocierAchat() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_NEGOCIATION_ACHAT));
  }
  
  /**
   * Lecture de la sécurité sur le droit à négocier les achats avec les frais d'exploitation.
   */
  public boolean isAutoriseFraisExploitation() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_NEGOCIATION_ACHAT_AVEC_FRAIS_EXPLOITATION);
  }
  
  /**
   * Lecture de la sécurité sur la visualisation des marges.
   */
  public boolean isAutoriseVisualiserMarge() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_VISUALISATION_MARGES);
  }
  
  /**
   * Retourner si la gestion par lot est activée.
   * @return boolean
   */
  public boolean isGestionParLot() {
    return modeleComptoir.isGestionParLot();
  }
}
