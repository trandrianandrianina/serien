/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

/**
 * Mode de fonctionnement de la boîte de dialogue de détail d'une ligne de vente.
 * 
 * Cette énumération est utilisée dans la boîte de dialogue qui affiche le détail d'une ligne de vente. Cette boîte de dialogue est
 * utilisée lors de la création d'une ligne de vente ou lors de la modification d'une ligne de vente. Cette énumération permet de
 * connaître son mode de fonctionnement.
 */
public enum EnumModeDetailLigneVente {
  CREATION,
  MODIFICATION;
}
