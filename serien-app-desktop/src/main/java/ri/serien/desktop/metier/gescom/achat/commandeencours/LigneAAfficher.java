/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.commandeencours;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;

/**
 * Description d'une ligne à afficher dans le tableau des commandes d'achat en cours pour un article.
 */
public class LigneAAfficher {
  // Variables
  private IdLigneAchat idLigneAchat = null;
  private boolean charge = false;
  private ListeAcheteur listeAcheteur = null;
  
  // Variables à afficher
  private String numeroDocument = null;
  private String dateCommande = null;
  private String dateLivraison = null;
  private String quantiteUCA = null;
  private String prixAchatNet = null;
  private String prixRevientHT = null;
  private String nomAcheteur = null;
  
  /**
   * Constructeur.
   */
  public LigneAAfficher(IdLigneAchat pId) {
    idLigneAchat = pId;
  }
  
  // -- Méthodes public
  
  /**
   * Initialise les données.
   */
  public void initialiserDonnees(DocumentAchat pDocumentAchat, LigneAchat pLigneAchat, Fournisseur pFournisseur,
      ListeAcheteur pListeAcheteur) {
    listeAcheteur = pListeAcheteur;
    
    // Initialisation des informations provenant du document
    if (pDocumentAchat == null) {
      return;
    }
    numeroDocument = pDocumentAchat.getId().toString();
    dateCommande = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, pDocumentAchat.getDateCreation());
    dateLivraison = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, pDocumentAchat.getDateLivraisonPrevue());
    nomAcheteur = getNomAcheteur(pDocumentAchat.getIdAcheteur());
    
    // Initialisation des informations provenant de la ligne
    if (pLigneAchat == null) {
      return;
    }
    
    PrixAchat prixAchat = ((LigneAchatArticle) pLigneAchat).getPrixAchat();
    if (prixAchat != null) {
      quantiteUCA = Constantes.formater(prixAchat.getQuantiteTraiteeUCA(), false);
      prixAchatNet = Constantes.formater(prixAchat.getPrixAchatNetHT(), true);
      prixRevientHT = Constantes.formater(prixAchat.getPrixRevientFournisseurHT(), true);
    }
    
    // Arrivé ici alors la ligne est complètement chargée
    charge = true;
  }
  
  // -- Méthodes privées
  
  private String getNomAcheteur(IdAcheteur pIdAcheteur) {
    if (pIdAcheteur == null || listeAcheteur == null) {
      return "";
    }
    Acheteur acheteur = listeAcheteur.retournerAcheteurParId(pIdAcheteur);
    if (acheteur == null) {
      return "";
    }
    return acheteur.getNom();
  }
  
  private String getLibelleTypePrixAchat(Fournisseur pFournisseur) {
    if (pFournisseur == null || pFournisseur.getTypePrixAchat() == null) {
      return null;
    }
    return pFournisseur.getTypePrixAchat().getLibelle();
  }
  
  // -- Accesseurs
  
  public boolean isCharge() {
    return charge;
  }
  
  public IdLigneAchat getIdLigneAchat() {
    return idLigneAchat;
  }
  
  public String getNumeroDocument() {
    return Constantes.normerTexte(numeroDocument);
  }
  
  public String getDateCommande() {
    return Constantes.normerTexte(dateCommande);
  }
  
  public String getDateLivraison() {
    return Constantes.normerTexte(dateLivraison);
  }
  
  public String getQuantiteUCA() {
    return Constantes.normerTexte(quantiteUCA);
  }
  
  public String getPrixAchatNet() {
    return Constantes.normerTexte(prixAchatNet);
  }
  
  public String getPrixRevientHT() {
    return Constantes.normerTexte(prixRevientHT);
  }
  
  public String getNomAcheteur() {
    return Constantes.normerTexte(nomAcheteur);
  }
}
