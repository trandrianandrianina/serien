/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran détail de la consultation de documents d'achat.
 */
public class VueDetailConsultationDocumentAchat extends AbstractVuePanel<ModeleConsultationDocumentAchat> {
  // Constantes
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détail";
  private static final String BOUTON_MASQUER_DETAIL = "Masquer détail";
  
  private static final String[] TITRE_COLONNE_COMMANDE =
      new String[] { "Initiale", "Reliquat", "Traitée", "UCA", "Article", "Prix HT", "Qt\u00e9 UA", "UA", "Montant HT", };
  private static final String[] TITRE_COLONNE_COMMANDE_DETAIL = new String[] { "Initiale", "Reliquat", "Traitée", "UCA", "Article",
      "Prix brut HT", "Remise", "Taxe ou majo", "Conditionnement", "Prix net HT", "Port", "PRF", "Qt\u00e9 UA", "UA", "Montant HT" };
  
  private static final String[] TITRE_COLONNE_RECEPTION =
      new String[] { "Quantit\u00e9 r\u00e9ceptionn\u00e9e", "UCA", "Article", "Prix HT", "Qt\u00e9 UA", "UA", "Montant HT", };
  private static final String[] TITRE_COLONNE_RECEPTION_DETAIL = new String[] { "Quantit\u00e9 r\u00e9ceptionn\u00e9e", "UCA", "Article",
      "Prix brut HT", "Remise", "Taxe ou majo", "Conditionnement", "Prix net HT", "Port", "PRF", "Qt\u00e9 UA", "UA", "Montant HT" };
  
  private static final String[] TITRE_COLONNE_FACTURE =
      new String[] { "Quantit\u00e9 factur\u00e9e", "UCA", "Article", "Prix HT", "Qt\u00e9 UA", "UA", "Montant HT", };
  private static final String[] TITRE_COLONNE_FACTURE_DETAIL = new String[] { "Quantit\u00e9 factur\u00e9e", "UCA", "Article",
      "Prix brut HT", "Remise", "Taxe ou majo", "Conditionnement", "Prix net HT", "Port", "PRF", "Qt\u00e9 UA", "UA", "Montant HT" };
  
  // Variables
  private DefaultTableModel tableModelCommande = null;
  private DefaultTableModel tableModelReception = null;
  private DefaultTableModel tableModelFacture = null;
  private DefaultTableModel tableModelCommandeDetail = null;
  private DefaultTableModel tableModelReceptionDetail = null;
  private DefaultTableModel tableModelFactureDetail = null;
  private JTableCellRendererLigneCommande rendererCommande = new JTableCellRendererLigneCommande();
  private JTableCellRendererLigneReception rendererReception = new JTableCellRendererLigneReception();
  private JTableCellRendererLigneFacture rendererFacture = new JTableCellRendererLigneFacture();
  private JTableCellRendererLigneCommandeDetail rendererCommandeDetail = new JTableCellRendererLigneCommandeDetail();
  private JTableCellRendererLigneReceptionDetail rendererReceptionDetail = new JTableCellRendererLigneReceptionDetail();
  private JTableCellRendererLigneFactureDetail rendererFactureDetail = new JTableCellRendererLigneFactureDetail();
  
  /**
   * Constructeur.
   */
  public VueDetailConsultationDocumentAchat(ModeleConsultationDocumentAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    tableModelCommande = new DefaultTableModel(null, TITRE_COLONNE_COMMANDE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    tableModelReception = new DefaultTableModel(null, TITRE_COLONNE_RECEPTION) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    tableModelFacture = new DefaultTableModel(null, TITRE_COLONNE_FACTURE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    
    tableModelCommandeDetail = new DefaultTableModel(null, TITRE_COLONNE_COMMANDE_DETAIL) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    tableModelReceptionDetail = new DefaultTableModel(null, TITRE_COLONNE_RECEPTION_DETAIL) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    tableModelFactureDetail = new DefaultTableModel(null, TITRE_COLONNE_FACTURE_DETAIL) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    
    initialiserListeLigneArticle();
    scpLigneArticle.setBackground(Color.white);
    scpLigneArticle.getViewport().setBackground(Color.WHITE);
    
    // Configurer la barre de boutons
    snBarreBouton.setModeNavigation(true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    requestFocus();
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    if (!getModele().isEcranDetail()) {
      return;
    }
    // Rafraîchir entête
    rafraichirDateCreation();
    rafraichirNomMagasin();
    rafraichirNomAcheteur();
    rafraichirReferenceInterne();
    rafraichirReferenceCommande();
    rafraichirReferenceLivraison();
    rafraichirDateLivraisonPrevue();
    rafraichirDateConfirmation();
    rafraichirDateReception();
    rafraichirNumeroFacture();
    rafraichirDocumentOrigine();
    
    // Rafraîchir l'adresse du fournisseur
    rafraichirNomFournisseur();
    rafraichirComplementNomFournisseur();
    rafraichirRueFournisseur();
    rafraichirLocalisationFournisseur();
    rafraichirCodePostalFournisseur();
    rafraichirVilleFournisseur();
    
    // Rafraîchir les informations de contact
    rafraichirNumeroFournisseurContact();
    rafraichirNomContact();
    rafraichirMailContact();
    rafraichirTelephoneContact();
    rafraichirFaxContact();
    
    // Rafraîchir totaux
    rafraichirPoidsTotal();
    rafraichirTotalHT();
    rafraichirTotalTVA();
    rafraichirTotalTTC();
    rafraichirMessageReglement();
    
    // Rafraîchir les lignes du document d'achat
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null) {
      // Mode affichage normal ligne
      if (getModele().getModeAffichageTableDetail() == ModeleConsultationDocumentAchat.MODE_AFFICHAGE_DETAIL_TABLE_STANDARD) {
        if (getModele().getDocumentAchatDetail().isCommande()) {
          rafraichirListeLigneCommande();
        }
        else if (getModele().getDocumentAchatDetail().isReception()) {
          rafraichirListeLigneReception();
        }
        else if (getModele().getDocumentAchatDetail().isFacture()) {
          rafraichirListeLigneFacture();
        }
      }
      // Mode affichage détail ligne
      else {
        if (getModele().getDocumentAchatDetail().isCommande()) {
          rafraichirListeLigneCommandeDetail();
        }
        else if (getModele().getDocumentAchatDetail().isReception()) {
          rafraichirListeLigneReceptionDetail();
        }
        else if (getModele().getDocumentAchatDetail().isFacture()) {
          rafraichirListeLigneFactureDetail();
        }
      }
    }
    
    // Rafraîchir les boutons
    rafraichirBoutonNavigation();
    rafraichirBoutonAfficherDetail();
    rafraichirBoutonMasquerDetail();
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser la structure de la liste des lignes articles.
   */
  private void initialiserListeLigneArticle() {
    if (getModele().getDocumentAchatDetail() == null) {
      return;
    }
    
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    int[] dimension = null;
    int[] dimensionMax = null;
    int[] justification = null;
    String[] listeColonne = null;
    
    if (documentAchat.isCommande()) {
      listeColonne = TITRE_COLONNE_COMMANDE;
      dimension = new int[] { 60, 60, 60, 30, 250, 100, 80, 30, 100 };
      dimensionMax = new int[] { 60, 60, 60, 30, -1, 100, 80, 30, 100 };
      justification = new int[] { NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE,
          NRiTable.CENTRE, NRiTable.DROITE };
    }
    else if (documentAchat.isReception()) {
      listeColonne = TITRE_COLONNE_RECEPTION;
      dimension = new int[] { 150, 30, 250, 100, 80, 30, 100 };
      dimensionMax = new int[] { 60, 30, -1, 100, 80, 30, 100 };
      justification = new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.DROITE };
    }
    else if (documentAchat.isFacture()) {
      listeColonne = TITRE_COLONNE_FACTURE;
      dimension = new int[] { 150, 30, 250, 100, 80, 30, 100 };
      dimensionMax = new int[] { 60, 30, -1, 100, 80, 30, 100 };
      justification = new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.DROITE };
    }
    
    tblLigneArticle.personnaliserAspect(listeColonne, dimension, dimensionMax, justification, 14);
    tblLigneArticle.getTableHeader().setFont(new Font(tblLigneArticle.getTableHeader().getFont().getName(), Font.PLAIN, 14));
  }
  
  private void rafraichirDateCreation() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getDateCreation() != null) {
      calDateValeur.setText(Constantes.convertirDateHeureEnTexte(documentAchat.getDateCreation()));
    }
    else {
      calDateValeur.setText("");
    }
  }
  
  private void rafraichirNomMagasin() {
    Magasin magasinSortie = getModele().getMagasinSortieDetail();
    if (magasinSortie != null) {
      lbMagasinValeur.setText(magasinSortie.getNomAvecCode());
    }
    else {
      lbMagasinValeur.setText("");
    }
  }
  
  private void rafraichirNomAcheteur() {
    if (getModele().getAcheteurDetail() != null) {
      lbAcheteurValeur.setText(getModele().getAcheteurDetail().getNom());
    }
    else {
      lbAcheteurValeur.setText("");
    }
  }
  
  private void rafraichirReferenceInterne() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getReferenceInterne() != null) {
      lbReferenceLongueValeur.setText(documentAchat.getReferenceInterne());
    }
    else {
      lbReferenceLongueValeur.setText("");
    }
  }
  
  private void rafraichirReferenceCommande() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getReferenceCommande() != null) {
      lbReferenceCourteValeur.setText(documentAchat.getReferenceCommande());
    }
    else {
      lbReferenceCourteValeur.setText("");
    }
  }
  
  private void rafraichirReferenceLivraison() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && (documentAchat.isReception() || documentAchat.getDateReception() != null)) {
      lbReferenceLivraison.setVisible(true);
      lbReferenceLivraisonValeur.setVisible(true);
      if (documentAchat.getReferenceBonLivraison() != null) {
        lbReferenceLivraisonValeur.setText(documentAchat.getReferenceBonLivraison());
      }
      else {
        lbReferenceLivraisonValeur.setText("");
      }
    }
    else {
      lbReferenceLivraisonValeur.setText("");
      lbReferenceLivraison.setVisible(false);
      lbReferenceLivraisonValeur.setVisible(false);
    }
  }
  
  private void rafraichirNumeroFacture() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.isFacture()) {
      lbNumeroFacture.setText("Date de facturation :");
      lbNumeroFactureValeur.setText(Constantes.convertirDateEnTexte(documentAchat.getDateFacturation()));
      lbNumeroFactureValeur.setVisible(true);
      lbNumeroFacture.setVisible(true);
    }
    else if (documentAchat != null && documentAchat.getNumeroFacture() > 0) {
      lbNumeroFacture.setText("Facture :");
      if (documentAchat.getDateFacturation() == null) {
        lbNumeroFactureValeur.setText(Constantes.convertirIntegerEnTexte(documentAchat.getNumeroFacture(), 0));
      }
      else {
        lbNumeroFactureValeur.setText(Constantes.convertirIntegerEnTexte(documentAchat.getNumeroFacture(), 0) + " du "
            + Constantes.convertirDateEnTexte(documentAchat.getDateFacturation()));
      }
      lbNumeroFactureValeur.setVisible(true);
      lbNumeroFacture.setVisible(true);
    }
    else {
      lbNumeroFactureValeur.setText("");
      lbNumeroFactureValeur.setVisible(false);
      lbNumeroFacture.setVisible(false);
    }
  }
  
  private void rafraichirDateLivraisonPrevue() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getDateLivraisonPrevue() != null) {
      lbDateLivraisonValeur.setText(Constantes.convertirDateEnTexte(documentAchat.getDateLivraisonPrevue()));
      lbDateLivraisonValeur.setVisible(true);
      lbDateLivraison.setVisible(true);
    }
    else {
      lbDateLivraisonValeur.setText("");
      lbDateLivraisonValeur.setVisible(false);
      lbDateLivraison.setVisible(false);
    }
  }
  
  private void rafraichirDateConfirmation() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getDateReception() != null) {
      lbDateConfirmationValeur.setText(Constantes.convertirDateEnTexte(documentAchat.getDateReception()));
      lbDateConfirmationValeur.setVisible(true);
      lbDateConfirmation.setVisible(true);
    }
    else {
      lbDateConfirmationValeur.setText("");
      lbDateConfirmationValeur.setVisible(false);
      lbDateConfirmation.setVisible(false);
    }
  }
  
  private void rafraichirDateReception() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getDateReception() != null) {
      lbDateReceptionValeur.setText(Constantes.convertirDateEnTexte(documentAchat.getDateReception()));
      lbDateReceptionValeur.setVisible(true);
      lbDateReception.setVisible(true);
    }
    else {
      lbDateReceptionValeur.setText("");
      lbDateReceptionValeur.setVisible(false);
      lbDateReception.setVisible(false);
    }
  }
  
  private void rafraichirDocumentOrigine() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getListeIdDocumentOrigine() != null) {
      lbDocumentOrigineValeur.setText(documentAchat.retournerChaineIdDocumentOrigine());
      lbDocumentOrigine.setVisible(true);
    }
    else {
      lbDocumentOrigineValeur.setText("");
      lbDocumentOrigine.setVisible(false);
    }
  }
  
  /**
   * Rafraîhir l'adresse de Livraison.
   */
  private void rafraichirNomFournisseur() {
    Fournisseur fournisseur = getModele().getFournisseurDetail();
    if (fournisseur != null && fournisseur.getNomFournisseur() != null) {
      tfNomLivraison.setText(fournisseur.getNomFournisseur());
      tfNomLivraison.setVisible(true);
    }
    else {
      tfNomLivraison.setText("");
      tfNomLivraison.setVisible(false);
    }
  }
  
  private void rafraichirComplementNomFournisseur() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getAdresseFournisseur().getComplementNom() != null) {
      tfComplementNomLivraison.setText(documentAchat.getAdresseFournisseur().getComplementNom());
      tfComplementNomLivraison.setVisible(true);
    }
    else {
      tfComplementNomLivraison.setText("");
      tfComplementNomLivraison.setVisible(false);
    }
    
  }
  
  private void rafraichirRueFournisseur() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getAdresseFournisseur() != null
        && documentAchat.getAdresseFournisseur().getRue() != null) {
      tfRueLivraison.setText(documentAchat.getAdresseFournisseur().getRue());
      tfRueLivraison.setVisible(true);
    }
    else {
      tfRueLivraison.setText("");
      tfRueLivraison.setVisible(false);
    }
  }
  
  private void rafraichirLocalisationFournisseur() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getAdresseFournisseur() != null
        && documentAchat.getAdresseFournisseur().getLocalisation() != null) {
      tfLocalisationLivraison.setText(documentAchat.getAdresseFournisseur().getLocalisation());
      tfLocalisationLivraison.setVisible(true);
    }
    else {
      tfLocalisationLivraison.setText("");
      tfLocalisationLivraison.setVisible(false);
    }
  }
  
  private void rafraichirCodePostalFournisseur() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getAdresseFournisseur() != null
        && documentAchat.getAdresseFournisseur().getCodePostalFormate() != null) {
      tfCodePostalLivraison.setText(documentAchat.getAdresseFournisseur().getCodePostalFormate());
      tfCodePostalLivraison.setVisible(true);
    }
    else {
      tfCodePostalLivraison.setText("");
      tfCodePostalLivraison.setVisible(false);
    }
  }
  
  private void rafraichirVilleFournisseur() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getAdresseFournisseur() != null
        && documentAchat.getAdresseFournisseur().getVille() != null) {
      tfVilleLivraison.setText(documentAchat.getAdresseFournisseur().getVille());
      tfVilleLivraison.setVisible(true);
    }
    else {
      tfVilleLivraison.setText("");
      tfVilleLivraison.setVisible(false);
    }
  }
  
  private void rafraichirNumeroFournisseurContact() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null) {
      tfNumeroClient.setText(documentAchat.getIdFournisseur().getIndicatifCourt());
      tfNumeroClient.setVisible(true);
    }
    else {
      tfNumeroClient.setText("");
      tfNumeroClient.setVisible(false);
    }
  }
  
  private void rafraichirNomContact() {
    Fournisseur fournisseur = getModele().getFournisseurDetail();
    if (fournisseur != null && fournisseur.getContactPrincipal() != null && fournisseur.getContactPrincipal().getNomComplet() != null) {
      tfNomComplet.setText(fournisseur.getContactPrincipal().getNomComplet());
      tfNomComplet.setVisible(true);
    }
    else {
      tfNomComplet.setText("");
      tfNomComplet.setVisible(false);
    }
  }
  
  private void rafraichirMailContact() {
    Fournisseur fournisseur = getModele().getFournisseurDetail();
    if (fournisseur != null && fournisseur.getContactPrincipal() != null && fournisseur.getContactPrincipal().getEmail() != null) {
      tfEmail.setText(fournisseur.getContactPrincipal().getEmail());
      tfEmail.setVisible(true);
    }
    else {
      tfEmail.setText("");
      tfEmail.setVisible(false);
    }
  }
  
  private void rafraichirTelephoneContact() {
    Fournisseur fournisseur = getModele().getFournisseurDetail();
    if (fournisseur != null && fournisseur.getContactPrincipal() != null
        && fournisseur.getContactPrincipal().getNumeroTelephone1() != null) {
      tfTelephone.setText(fournisseur.getContactPrincipal().getNumeroTelephone1());
      tfTelephone.setVisible(true);
    }
    else {
      tfTelephone.setText("");
      tfTelephone.setVisible(false);
    }
  }
  
  private void rafraichirFaxContact() {
    Fournisseur fournisseur = getModele().getFournisseurDetail();
    if (fournisseur != null && fournisseur.getContactPrincipal() != null && fournisseur.getContactPrincipal().getNumeroFax() != null) {
      tfFax.setText(fournisseur.getContactPrincipal().getNumeroFax());
      tfFax.setVisible(true);
    }
    else {
      tfFax.setText("");
      tfFax.setVisible(false);
    }
  }
  
  private void rafraichirPoidsTotal() {
    // Poour l'instant le poids est caché si la commande a été réceptionnée car le poids a été perdu (il faudra à terme faire quelque
    // chose)
    
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getPoids() != null && !documentAchat.isCommandeReceptionnee()) {
      lbPoidsTotal.setVisible(true);
      lbPoidsTotalValeur.setText(Constantes.formater(documentAchat.getPoids(), false) + " kg");
    }
    else {
      lbPoidsTotal.setVisible(false);
      lbPoidsTotalValeur.setText("");
    }
  }
  
  private void rafraichirTotalHT() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getTotalHTLignes() != null) {
      lbTotalHTValeur.setText(Constantes.formater(documentAchat.getTotalHTLignes(), true));
    }
    else {
      lbTotalHTValeur.setText("");
    }
  }
  
  private void rafraichirTotalTVA() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getTotalTTC() != null && documentAchat.getTotalHTLignes() != null) {
      BigDecimal totalTva = documentAchat.getTotalTTC().subtract(documentAchat.getTotalHTLignes());
      lbTotalTVAValeur.setText(Constantes.formater(totalTva, true));
    }
    else {
      lbTotalTVAValeur.setText("");
    }
  }
  
  private void rafraichirTotalTTC() {
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat != null && documentAchat.getTotalTTC() != null) {
      lbTotalTTCValeur.setText(Constantes.formater(documentAchat.getTotalTTC(), true));
    }
    else {
      lbTotalTTCValeur.setText("");
    }
  }
  
  private void rafraichirMessageReglement() {
    // lbMessageReglement.setText(getModele().genererMessageReglement());
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une commande (mode recherche document).
   */
  private void rafraichirListeLigneCommande() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelCommande.setRowCount(0);
    
    // Remplacer le modèle de la table si besoin
    if (!tblLigneArticle.getModel().equals(tableModelCommande)) {
      tblLigneArticle.setModel(tableModelCommande);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererCommande);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererCommande.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_COMMANDE.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = "";
        donnees[ligne][3] = "";
        donnees[ligne][4] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
        donnees[ligne][8] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        donnees[ligne][1] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false);
        donnees[ligne][2] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteTraiteeUCA(), false);
        if (ligneAchat.getPrixAchat().getIdUCA() != null) {
          donnees[ligne][3] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        }
        donnees[ligne][4] = ligneAchat.getLibelleArticle();
        donnees[ligne][5] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        donnees[ligne][6] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUA(), false);
        if (ligneAchat.getPrixAchat().getIdUA() != null) {
          donnees[ligne][7] = ligneAchat.getPrixAchat().getIdUA().getCode();
        }
        donnees[ligne][8] = Constantes.formater(ligneAchat.getPrixAchat().getMontantInitialHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelCommande.addRow(ligne);
    }
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une réception (mode recherche document).
   */
  private void rafraichirListeLigneReception() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelReception.setRowCount(0);
    
    // Remplacer le modèle de la table si besoin
    if (!tblLigneArticle.getModel().equals(tableModelReception)) {
      tblLigneArticle.setModel(tableModelReception);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererReception);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererReception.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_RECEPTION.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        if (ligneAchat.getPrixAchat().getIdUCA() != null) {
          donnees[ligne][1] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        }
        donnees[ligne][2] = ligneAchat.getLibelleArticle();
        donnees[ligne][3] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        donnees[ligne][4] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUA(), false);
        if (ligneAchat.getPrixAchat().getIdUA() != null) {
          donnees[ligne][5] = ligneAchat.getPrixAchat().getIdUA().getCode();
        }
        donnees[ligne][6] = Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelReception.addRow(ligne);
    }
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une facture.
   */
  private void rafraichirListeLigneFacture() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelFacture.setRowCount(0);
    
    // Remplacer le modèle de la table si besoin
    if (!tblLigneArticle.getModel().equals(tableModelFacture)) {
      tblLigneArticle.setModel(tableModelFacture);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererFacture);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererFacture.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_FACTURE.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        if (ligneAchat.getPrixAchat().getIdUCA() != null) {
          donnees[ligne][1] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        }
        donnees[ligne][2] = ligneAchat.getLibelleArticle();
        donnees[ligne][3] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        donnees[ligne][4] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUA(), false);
        if (ligneAchat.getPrixAchat().getIdUA() != null) {
          donnees[ligne][5] = ligneAchat.getPrixAchat().getIdUA().getCode();
        }
        donnees[ligne][6] = Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelFacture.addRow(ligne);
    }
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une commande (mode détail).
   */
  private void rafraichirListeLigneCommandeDetail() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelCommandeDetail.setRowCount(0);
    
    // Remplacer le modèle de la table
    if (!tblLigneArticle.getModel().equals(tableModelCommandeDetail)) {
      tblLigneArticle.setModel(tableModelCommandeDetail);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererCommandeDetail);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererCommandeDetail.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_COMMANDE_DETAIL.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = "";
        donnees[ligne][3] = "";
        donnees[ligne][4] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
        donnees[ligne][8] = "";
        donnees[ligne][9] = "";
        donnees[ligne][10] = "";
        donnees[ligne][11] = "";
        donnees[ligne][12] = "";
        donnees[ligne][13] = "";
        donnees[ligne][14] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        
        // Quantité UCA
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        donnees[ligne][1] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUCA(), false);
        donnees[ligne][2] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteTraiteeUCA(), false);
        // UCA
        donnees[ligne][3] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        // Libellé article
        donnees[ligne][4] = ligneAchat.getLibelleArticle();
        // Prix achat brut HT
        donnees[ligne][5] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatBrutHT(), true);
        // Remise
        BigDecimal remise = BigDecimal.ZERO;
        remise = ligneAchat.getPrixAchat().getPourcentageRemise1().add(ligneAchat.getPrixAchat().getPourcentageRemise2())
            .add(ligneAchat.getPrixAchat().getPourcentageRemise3()).add(ligneAchat.getPrixAchat().getPourcentageRemise4());
        
        donnees[ligne][6] = Constantes.formater(remise, true) + "%";
        
        // Taxe ou majoration
        donnees[ligne][7] = Constantes.formater(ligneAchat.getPrixAchat().getPourcentageTaxeOuMajoration(), true) + "%";
        // Montant conditionnement
        donnees[ligne][8] = Constantes.formater(ligneAchat.getPrixAchat().getMontantConditionnement(), true);
        // Prix achat net ht
        donnees[ligne][9] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        // Port HT
        donnees[ligne][10] = Constantes.formater(ligneAchat.getPrixAchat().getMontantPortHT(), true);
        // Prix revient fournisseur
        donnees[ligne][11] = Constantes.formater(ligneAchat.getPrixAchat().getPrixRevientFournisseurHT(), true);
        // Quantité UA
        donnees[ligne][12] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUA(), false);
        // Unité Achat
        donnees[ligne][13] = ligneAchat.getPrixAchat().getIdUA().getCode();
        // Montant HT
        donnees[ligne][14] = Constantes.formater(ligneAchat.getPrixAchat().getMontantInitialHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelCommandeDetail.addRow(ligne);
    }
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une réception (mode détail).
   */
  private void rafraichirListeLigneReceptionDetail() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelReceptionDetail.setRowCount(0);
    
    // Remplacer le modèle de la table
    if (!tblLigneArticle.getModel().equals(tableModelReceptionDetail)) {
      tblLigneArticle.setModel(tableModelReceptionDetail);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererReceptionDetail);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererReceptionDetail.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_RECEPTION_DETAIL.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
        donnees[ligne][8] = "";
        donnees[ligne][9] = "";
        donnees[ligne][10] = "";
        donnees[ligne][11] = "";
        donnees[ligne][12] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        
        // Quantité UCA
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        // UCA
        donnees[ligne][1] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        // Libellé article
        donnees[ligne][2] = ligneAchat.getLibelleArticle();
        // Prix achat brut HT
        donnees[ligne][3] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatBrutHT(), true);
        // Remise
        BigDecimal remise = BigDecimal.ZERO;
        remise = ligneAchat.getPrixAchat().getPourcentageRemise1().add(ligneAchat.getPrixAchat().getPourcentageRemise2())
            .add(ligneAchat.getPrixAchat().getPourcentageRemise3()).add(ligneAchat.getPrixAchat().getPourcentageRemise4());
        
        donnees[ligne][4] = Constantes.formater(remise, true) + "%";
        
        // Taxe ou majoration
        donnees[ligne][5] = Constantes.formater(ligneAchat.getPrixAchat().getPourcentageTaxeOuMajoration(), true) + "%";
        // Montant conditionnement
        donnees[ligne][6] = Constantes.formater(ligneAchat.getPrixAchat().getMontantConditionnement(), true);
        // Prix achat net ht
        donnees[ligne][7] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        // Port HT
        donnees[ligne][8] = Constantes.formater(ligneAchat.getPrixAchat().getMontantPortHT(), true);
        // Prix revient fournisseur
        donnees[ligne][9] = Constantes.formater(ligneAchat.getPrixAchat().getPrixRevientFournisseurHT(), true);
        // Quantité UA
        donnees[ligne][10] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUA(), false);
        // Unité Achat
        donnees[ligne][11] = ligneAchat.getPrixAchat().getIdUA().getCode();
        // Montant HT
        donnees[ligne][12] = Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelReceptionDetail.addRow(ligne);
    }
  }
  
  /**
   * Rafraichit la liste des lignes articles d'une facture (mode détail).
   */
  private void rafraichirListeLigneFactureDetail() {
    // Effacer la table
    tblLigneArticle.clearSelection();
    tableModelFactureDetail.setRowCount(0);
    
    // Remplacer le modèle de la table
    if (!tblLigneArticle.getModel().equals(tableModelFactureDetail)) {
      tblLigneArticle.setModel(tableModelFactureDetail);
      tblLigneArticle.setGridColor(new Color(204, 204, 204));
      tblLigneArticle.setDefaultRenderer(Object.class, rendererFactureDetail);
    }
    
    // Récupérer les lignes à afficher
    DocumentAchat documentAchat = getModele().getDocumentAchatDetail();
    if (documentAchat == null || documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererFactureDetail.redimensionnerColonnes(tblLigneArticle);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneAchat listeLigneAchat = documentAchat.getListeLigneAchat();
    String[][] donnees = new String[listeLigneAchat.size()][TITRE_COLONNE_FACTURE_DETAIL.length];
    for (int ligne = 0; ligne < listeLigneAchat.size(); ligne++) {
      if (listeLigneAchat.get(ligne) == null) {
        continue;
      }
      if (listeLigneAchat.get(ligne).isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ((LigneAchatCommentaire) listeLigneAchat.get(ligne)).getTexte();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
        donnees[ligne][8] = "";
        donnees[ligne][9] = "";
        donnees[ligne][10] = "";
        donnees[ligne][11] = "";
        donnees[ligne][12] = "";
      }
      else {
        LigneAchatArticle ligneAchat = (LigneAchatArticle) listeLigneAchat.get(ligne);
        
        // Quantité UCA
        donnees[ligne][0] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
        // UCA
        donnees[ligne][1] = ligneAchat.getPrixAchat().getIdUCA().getCode();
        // Libellé article
        donnees[ligne][2] = ligneAchat.getLibelleArticle();
        // Prix achat brut HT
        donnees[ligne][3] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatBrutHT(), true);
        // Remise
        BigDecimal remise = BigDecimal.ZERO;
        remise = ligneAchat.getPrixAchat().getPourcentageRemise1().add(ligneAchat.getPrixAchat().getPourcentageRemise2())
            .add(ligneAchat.getPrixAchat().getPourcentageRemise3()).add(ligneAchat.getPrixAchat().getPourcentageRemise4());
        
        donnees[ligne][4] = Constantes.formater(remise, true) + "%";
        
        // Taxe ou majoration
        donnees[ligne][5] = Constantes.formater(ligneAchat.getPrixAchat().getPourcentageTaxeOuMajoration(), true) + "%";
        // Montant conditionnement
        donnees[ligne][6] = Constantes.formater(ligneAchat.getPrixAchat().getMontantConditionnement(), true);
        // Prix achat net ht
        donnees[ligne][7] = Constantes.formater(ligneAchat.getPrixAchat().getPrixAchatNetHT(), true);
        // Port HT
        donnees[ligne][8] = Constantes.formater(ligneAchat.getPrixAchat().getMontantPortHT(), true);
        // Prix revient fournisseur
        donnees[ligne][9] = Constantes.formater(ligneAchat.getPrixAchat().getPrixRevientFournisseurHT(), true);
        // Quantité UA
        donnees[ligne][10] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteReliquatUA(), false);
        // Unité Achat
        donnees[ligne][11] = ligneAchat.getPrixAchat().getIdUA().getCode();
        // Montant HT
        donnees[ligne][12] = Constantes.formater(ligneAchat.getPrixAchat().getMontantReliquatHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelFactureDetail.addRow(ligne);
    }
  }
  
  private void rafraichirBoutonAfficherDetail() {
    boolean actif = false;
    if (getModele().getModeAffichageTableDetail() == ModeleConsultationDocumentAchat.MODE_AFFICHAGE_DETAIL_TABLE_STANDARD) {
      actif = true;
    }
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL, actif);
  }
  
  private void rafraichirBoutonMasquerDetail() {
    boolean actif = true;
    if (getModele().getModeAffichageTableDetail() == ModeleConsultationDocumentAchat.MODE_AFFICHAGE_DETAIL_TABLE_STANDARD) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL, actif);
  }
  
  private void rafraichirBoutonNavigation() {
    if (getModele().getIndexLigneSelectionne() < 0) {
      snBarreBouton.setCompteurCourant(getModele().getIndexDocumentSelectionne() + 1);
      snBarreBouton.setCompteurMax(getModele().getListeIdDocumentAchat().size());
    }
    else {
      snBarreBouton.setCompteurCourant(getModele().getIndexLigneSelectionne() + 1);
      snBarreBouton.setCompteurMax(getModele().getListeIdLigneAchat().size());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.NAVIGATION_PRECEDENT)) {
        getModele().afficherDocumentPrecedentDetail();
      }
      else if (pSNBouton.isBouton(EnumBouton.NAVIGATION_SUIVANT)) {
        getModele().afficherDocumentSuivantDetail();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        getModele().permuterAffichageTableDetail();
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL)) {
        getModele().permuterAffichageTableDetail();
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        getModele().retournerRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLigneArticleMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() < 2) {
        return;
      }
      
      int indexvisuel = tblLigneArticle.getSelectedRow();
      if (indexvisuel < 0) {
        return;
      }
      int indexreel = indexvisuel;
      if (tblLigneArticle.getRowSorter() != null) {
        indexreel = tblLigneArticle.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().afficherDetailLigne(indexreel);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlInfosDocument = new JPanel();
    lbDate = new JLabel();
    calDateValeur = new JLabel();
    lbMagasin = new JLabel();
    lbMagasinValeur = new JLabel();
    lbAcheteur = new JLabel();
    lbAcheteurValeur = new JLabel();
    lbReferenceLongue = new JLabel();
    lbReferenceLongueValeur = new JLabel();
    lbReferenceCourte = new JLabel();
    lbReferenceCourteValeur = new JLabel();
    lbReferenceLivraison = new JLabel();
    lbReferenceLivraisonValeur = new JLabel();
    lbDateLivraison = new JLabel();
    lbDateLivraisonValeur = new JLabel();
    lbDateConfirmation = new JLabel();
    lbDateConfirmationValeur = new JLabel();
    lbDateReception = new JLabel();
    lbDateReceptionValeur = new JLabel();
    lbNumeroFacture = new JLabel();
    lbNumeroFactureValeur = new JLabel();
    lbDocumentOrigine = new JLabel();
    lbDocumentOrigineValeur = new JLabel();
    pnlCoordonneesClientLivre = new JPanel();
    tfCiviliteLivraison = new JLabel();
    tfNomLivraison = new JLabel();
    tfComplementNomLivraison = new JLabel();
    tfRueLivraison = new JLabel();
    tfLocalisationLivraison = new JLabel();
    tfVilleLivraison = new JLabel();
    tfCodePostalLivraison = new JLabel();
    pnlContact = new JPanel();
    lbNumeroClient = new JLabel();
    tfNumeroClient = new JLabel();
    lbContact = new JLabel();
    tfNomComplet = new JLabel();
    lbMail = new JLabel();
    tfEmail = new JLabel();
    lbTelephone = new JLabel();
    tfTelephone = new JLabel();
    lbFax = new JLabel();
    tfFax = new JLabel();
    pnlTitreListe = new JPanel();
    lbTitre = new JLabel();
    scpLigneArticle = new JScrollPane();
    tblLigneArticle = new NRiTable();
    pnlChiffrage = new JPanel();
    lbPoidsTotal = new JLabel();
    lbPoidsTotalValeur = new JLabel();
    lbTotalHT = new JLabel();
    lbTotalHTValeur = new JLabel();
    lbTotalTVA = new JLabel();
    lbTotalTVAValeur = new JLabel();
    lbTotalTTC = new JLabel();
    lbTotalTTCValeur = new JLabel();
    pnlReglement = new JPanel();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setBackground(Color.white);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(Color.white);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 716, 87, 310, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 160, 131, 33, 275, 40, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlInfosDocument ========
      {
        pnlInfosDocument.setOpaque(false);
        pnlInfosDocument.setMaximumSize(new Dimension(550, 270));
        pnlInfosDocument.setMinimumSize(new Dimension(550, 270));
        pnlInfosDocument.setPreferredSize(new Dimension(550, 270));
        pnlInfosDocument.setName("pnlInfosDocument");
        pnlInfosDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWidths = new int[] { 165, 408, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowHeights = new int[] { 23, 26, 20, 20, 20, 0, 21, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDate ----
        lbDate.setText("Date de cr\u00e9ation :");
        lbDate.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDate.setFont(new Font("Arial", Font.BOLD, 12));
        lbDate.setMinimumSize(new Dimension(150, 15));
        lbDate.setMaximumSize(new Dimension(150, 15));
        lbDate.setPreferredSize(new Dimension(150, 15));
        lbDate.setName("lbDate");
        pnlInfosDocument.add(lbDate, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 5), 0, 0));
        
        // ---- calDateValeur ----
        calDateValeur.setBackground(new Color(153, 153, 153));
        calDateValeur.setBorder(null);
        calDateValeur.setFont(new Font("Arial", Font.BOLD, 12));
        calDateValeur.setPreferredSize(new Dimension(110, 15));
        calDateValeur.setMinimumSize(new Dimension(110, 15));
        calDateValeur.setMaximumSize(new Dimension(110, 15));
        calDateValeur.setRequestFocusEnabled(false);
        calDateValeur.setFocusable(false);
        calDateValeur.setVerifyInputWhenFocusTarget(false);
        calDateValeur.setAlignmentX(0.5F);
        calDateValeur.setName("calDateValeur");
        pnlInfosDocument.add(calDateValeur, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin :");
        lbMagasin.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMagasin.setFont(new Font("Arial", Font.BOLD, 12));
        lbMagasin.setMinimumSize(new Dimension(150, 15));
        lbMagasin.setMaximumSize(new Dimension(150, 15));
        lbMagasin.setPreferredSize(new Dimension(150, 15));
        lbMagasin.setName("lbMagasin");
        pnlInfosDocument.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 5), 0, 0));
        
        // ---- lbMagasinValeur ----
        lbMagasinValeur.setBackground(new Color(234, 234, 218));
        lbMagasinValeur.setBorder(null);
        lbMagasinValeur.setIconTextGap(6);
        lbMagasinValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbMagasinValeur.setPreferredSize(new Dimension(325, 15));
        lbMagasinValeur.setMinimumSize(new Dimension(325, 15));
        lbMagasinValeur.setAlignmentX(1.0F);
        lbMagasinValeur.setName("lbMagasinValeur");
        pnlInfosDocument.add(lbMagasinValeur, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.NONE, new Insets(10, 0, 5, 0), 0, 0));
        
        // ---- lbAcheteur ----
        lbAcheteur.setText("Acheteur :");
        lbAcheteur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbAcheteur.setFont(new Font("Arial", Font.BOLD, 12));
        lbAcheteur.setMinimumSize(new Dimension(150, 15));
        lbAcheteur.setMaximumSize(new Dimension(150, 15));
        lbAcheteur.setPreferredSize(new Dimension(150, 15));
        lbAcheteur.setName("lbAcheteur");
        pnlInfosDocument.add(lbAcheteur,
            new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbAcheteurValeur ----
        lbAcheteurValeur.setBackground(new Color(234, 234, 218));
        lbAcheteurValeur.setBorder(null);
        lbAcheteurValeur.setIconTextGap(6);
        lbAcheteurValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbAcheteurValeur.setPreferredSize(new Dimension(40, 15));
        lbAcheteurValeur.setMinimumSize(new Dimension(40, 15));
        lbAcheteurValeur.setName("lbAcheteurValeur");
        pnlInfosDocument.add(lbAcheteurValeur, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceLongue ----
        lbReferenceLongue.setText("R\u00e9f\u00e9rence interne :");
        lbReferenceLongue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceLongue.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceLongue.setMinimumSize(new Dimension(150, 15));
        lbReferenceLongue.setMaximumSize(new Dimension(150, 15));
        lbReferenceLongue.setPreferredSize(new Dimension(150, 15));
        lbReferenceLongue.setName("lbReferenceLongue");
        pnlInfosDocument.add(lbReferenceLongue, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceLongueValeur ----
        lbReferenceLongueValeur.setBackground(new Color(234, 234, 218));
        lbReferenceLongueValeur.setBorder(null);
        lbReferenceLongueValeur.setIconTextGap(6);
        lbReferenceLongueValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceLongueValeur.setPreferredSize(new Dimension(400, 15));
        lbReferenceLongueValeur.setMinimumSize(new Dimension(400, 15));
        lbReferenceLongueValeur.setName("lbReferenceLongueValeur");
        pnlInfosDocument.add(lbReferenceLongueValeur, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceCourte ----
        lbReferenceCourte.setText("R\u00e9f\u00e9rence commande :");
        lbReferenceCourte.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceCourte.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceCourte.setMinimumSize(new Dimension(150, 15));
        lbReferenceCourte.setMaximumSize(new Dimension(150, 15));
        lbReferenceCourte.setPreferredSize(new Dimension(150, 15));
        lbReferenceCourte.setName("lbReferenceCourte");
        pnlInfosDocument.add(lbReferenceCourte, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceCourteValeur ----
        lbReferenceCourteValeur.setBackground(new Color(234, 234, 218));
        lbReferenceCourteValeur.setBorder(null);
        lbReferenceCourteValeur.setIconTextGap(6);
        lbReferenceCourteValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceCourteValeur.setPreferredSize(new Dimension(130, 15));
        lbReferenceCourteValeur.setMinimumSize(new Dimension(130, 15));
        lbReferenceCourteValeur.setName("lbReferenceCourteValeur");
        pnlInfosDocument.add(lbReferenceCourteValeur, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceLivraison ----
        lbReferenceLivraison.setText("R\u00e9f\u00e9rence de livraison :");
        lbReferenceLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceLivraison.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceLivraison.setMinimumSize(new Dimension(150, 15));
        lbReferenceLivraison.setMaximumSize(new Dimension(150, 15));
        lbReferenceLivraison.setPreferredSize(new Dimension(150, 15));
        lbReferenceLivraison.setName("lbReferenceLivraison");
        pnlInfosDocument.add(lbReferenceLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceLivraisonValeur ----
        lbReferenceLivraisonValeur.setBackground(new Color(234, 234, 218));
        lbReferenceLivraisonValeur.setBorder(null);
        lbReferenceLivraisonValeur.setIconTextGap(6);
        lbReferenceLivraisonValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceLivraisonValeur.setPreferredSize(new Dimension(130, 15));
        lbReferenceLivraisonValeur.setMinimumSize(new Dimension(130, 15));
        lbReferenceLivraisonValeur.setName("lbReferenceLivraisonValeur");
        pnlInfosDocument.add(lbReferenceLivraisonValeur, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateLivraison ----
        lbDateLivraison.setText("Date de livraison pr\u00e9vue :");
        lbDateLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDateLivraison.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateLivraison.setMinimumSize(new Dimension(150, 15));
        lbDateLivraison.setMaximumSize(new Dimension(150, 15));
        lbDateLivraison.setPreferredSize(new Dimension(150, 15));
        lbDateLivraison.setName("lbDateLivraison");
        pnlInfosDocument.add(lbDateLivraison, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateLivraisonValeur ----
        lbDateLivraisonValeur.setBackground(new Color(153, 153, 153));
        lbDateLivraisonValeur.setBorder(null);
        lbDateLivraisonValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateLivraisonValeur.setPreferredSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setMinimumSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setMaximumSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setRequestFocusEnabled(false);
        lbDateLivraisonValeur.setFocusable(false);
        lbDateLivraisonValeur.setVerifyInputWhenFocusTarget(false);
        lbDateLivraisonValeur.setAlignmentX(0.5F);
        lbDateLivraisonValeur.setName("lbDateLivraisonValeur");
        pnlInfosDocument.add(lbDateLivraisonValeur, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateConfirmation ----
        lbDateConfirmation.setText("Date de confirmation :");
        lbDateConfirmation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDateConfirmation.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateConfirmation.setMinimumSize(new Dimension(150, 15));
        lbDateConfirmation.setMaximumSize(new Dimension(150, 15));
        lbDateConfirmation.setPreferredSize(new Dimension(150, 15));
        lbDateConfirmation.setName("lbDateConfirmation");
        pnlInfosDocument.add(lbDateConfirmation, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateConfirmationValeur ----
        lbDateConfirmationValeur.setBackground(new Color(153, 153, 153));
        lbDateConfirmationValeur.setBorder(null);
        lbDateConfirmationValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateConfirmationValeur.setPreferredSize(new Dimension(110, 15));
        lbDateConfirmationValeur.setMinimumSize(new Dimension(110, 15));
        lbDateConfirmationValeur.setMaximumSize(new Dimension(110, 15));
        lbDateConfirmationValeur.setRequestFocusEnabled(false);
        lbDateConfirmationValeur.setFocusable(false);
        lbDateConfirmationValeur.setVerifyInputWhenFocusTarget(false);
        lbDateConfirmationValeur.setAlignmentX(0.5F);
        lbDateConfirmationValeur.setName("lbDateConfirmationValeur");
        pnlInfosDocument.add(lbDateConfirmationValeur, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateReception ----
        lbDateReception.setText("Date de r\u00e9ception :");
        lbDateReception.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDateReception.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateReception.setMinimumSize(new Dimension(150, 15));
        lbDateReception.setMaximumSize(new Dimension(150, 15));
        lbDateReception.setPreferredSize(new Dimension(150, 15));
        lbDateReception.setName("lbDateReception");
        pnlInfosDocument.add(lbDateReception, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateReceptionValeur ----
        lbDateReceptionValeur.setBackground(new Color(153, 153, 153));
        lbDateReceptionValeur.setBorder(null);
        lbDateReceptionValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateReceptionValeur.setPreferredSize(new Dimension(110, 15));
        lbDateReceptionValeur.setMinimumSize(new Dimension(110, 15));
        lbDateReceptionValeur.setMaximumSize(new Dimension(110, 15));
        lbDateReceptionValeur.setRequestFocusEnabled(false);
        lbDateReceptionValeur.setFocusable(false);
        lbDateReceptionValeur.setVerifyInputWhenFocusTarget(false);
        lbDateReceptionValeur.setAlignmentX(0.5F);
        lbDateReceptionValeur.setName("lbDateReceptionValeur");
        pnlInfosDocument.add(lbDateReceptionValeur, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroFacture ----
        lbNumeroFacture.setText("Facture :");
        lbNumeroFacture.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroFacture.setFont(new Font("Arial", Font.BOLD, 12));
        lbNumeroFacture.setPreferredSize(new Dimension(150, 15));
        lbNumeroFacture.setName("lbNumeroFacture");
        lbNumeroFacture.setMinimumSize(new Dimension(150, 15));
        lbNumeroFacture.setMaximumSize(new Dimension(150, 15));
        pnlInfosDocument.add(lbNumeroFacture, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbNumeroFactureValeur ----
        lbNumeroFactureValeur.setBackground(new Color(234, 234, 218));
        lbNumeroFactureValeur.setBorder(null);
        lbNumeroFactureValeur.setIconTextGap(6);
        lbNumeroFactureValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbNumeroFactureValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbNumeroFactureValeur.setPreferredSize(new Dimension(170, 15));
        lbNumeroFactureValeur.setMinimumSize(new Dimension(170, 15));
        lbNumeroFactureValeur.setName("lbNumeroFactureValeur");
        pnlInfosDocument.add(lbNumeroFactureValeur, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDocumentOrigine ----
        lbDocumentOrigine.setText("Document d'origine :");
        lbDocumentOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDocumentOrigine.setFont(new Font("Arial", Font.BOLD, 12));
        lbDocumentOrigine.setPreferredSize(new Dimension(150, 15));
        lbDocumentOrigine.setName("lbDocumentOrigine");
        lbDocumentOrigine.setMinimumSize(new Dimension(150, 15));
        lbDocumentOrigine.setMaximumSize(new Dimension(150, 15));
        pnlInfosDocument.add(lbDocumentOrigine, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDocumentOrigineValeur ----
        lbDocumentOrigineValeur.setBackground(new Color(153, 153, 153));
        lbDocumentOrigineValeur.setBorder(null);
        lbDocumentOrigineValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDocumentOrigineValeur.setPreferredSize(new Dimension(110, 15));
        lbDocumentOrigineValeur.setMinimumSize(new Dimension(110, 15));
        lbDocumentOrigineValeur.setMaximumSize(new Dimension(110, 15));
        lbDocumentOrigineValeur.setRequestFocusEnabled(false);
        lbDocumentOrigineValeur.setFocusable(false);
        lbDocumentOrigineValeur.setVerifyInputWhenFocusTarget(false);
        lbDocumentOrigineValeur.setAlignmentX(0.5F);
        lbDocumentOrigineValeur.setName("lbDocumentOrigineValeur");
        pnlInfosDocument.add(lbDocumentOrigineValeur, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlInfosDocument, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0, GridBagConstraints.CENTER,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlCoordonneesClientLivre ========
      {
        pnlCoordonneesClientLivre.setBorder(new TitledBorder(new LineBorder(Color.lightGray), "Adresse du fournisseur",
            TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
        pnlCoordonneesClientLivre.setOpaque(false);
        pnlCoordonneesClientLivre.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlCoordonneesClientLivre.setMinimumSize(new Dimension(350, 135));
        pnlCoordonneesClientLivre.setPreferredSize(new Dimension(350, 135));
        pnlCoordonneesClientLivre.setMaximumSize(new Dimension(350, 115));
        pnlCoordonneesClientLivre.setForeground(new Color(153, 153, 153));
        pnlCoordonneesClientLivre.setName("pnlCoordonneesClientLivre");
        pnlCoordonneesClientLivre.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).columnWidths = new int[] { 38, 250, 0 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- tfCiviliteLivraison ----
        tfCiviliteLivraison.setPreferredSize(new Dimension(300, 15));
        tfCiviliteLivraison.setMinimumSize(new Dimension(300, 15));
        tfCiviliteLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCiviliteLivraison.setBorder(null);
        tfCiviliteLivraison.setName("tfCiviliteLivraison");
        pnlCoordonneesClientLivre.add(tfCiviliteLivraison, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(5, 10, 5, 0), 0, 0));
        
        // ---- tfNomLivraison ----
        tfNomLivraison.setBackground(Color.white);
        tfNomLivraison.setFont(new Font("Arial", Font.BOLD, 12));
        tfNomLivraison.setMinimumSize(new Dimension(300, 15));
        tfNomLivraison.setPreferredSize(new Dimension(300, 12));
        tfNomLivraison.setBorder(null);
        tfNomLivraison.setName("tfNomLivraison");
        pnlCoordonneesClientLivre.add(tfNomLivraison, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfComplementNomLivraison ----
        tfComplementNomLivraison.setBackground(Color.white);
        tfComplementNomLivraison.setMinimumSize(new Dimension(300, 15));
        tfComplementNomLivraison.setPreferredSize(new Dimension(300, 12));
        tfComplementNomLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfComplementNomLivraison.setBorder(null);
        tfComplementNomLivraison.setName("tfComplementNomLivraison");
        pnlCoordonneesClientLivre.add(tfComplementNomLivraison, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfRueLivraison ----
        tfRueLivraison.setBackground(Color.white);
        tfRueLivraison.setMinimumSize(new Dimension(300, 15));
        tfRueLivraison.setPreferredSize(new Dimension(300, 12));
        tfRueLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfRueLivraison.setBorder(null);
        tfRueLivraison.setName("tfRueLivraison");
        pnlCoordonneesClientLivre.add(tfRueLivraison, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfLocalisationLivraison ----
        tfLocalisationLivraison.setBackground(Color.white);
        tfLocalisationLivraison.setMinimumSize(new Dimension(300, 15));
        tfLocalisationLivraison.setPreferredSize(new Dimension(300, 12));
        tfLocalisationLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfLocalisationLivraison.setBorder(null);
        tfLocalisationLivraison.setName("tfLocalisationLivraison");
        pnlCoordonneesClientLivre.add(tfLocalisationLivraison, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfVilleLivraison ----
        tfVilleLivraison.setBackground(new Color(171, 148, 79));
        tfVilleLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfVilleLivraison.setMinimumSize(new Dimension(300, 12));
        tfVilleLivraison.setPreferredSize(new Dimension(300, 12));
        tfVilleLivraison.setBorder(null);
        tfVilleLivraison.setName("tfVilleLivraison");
        pnlCoordonneesClientLivre.add(tfVilleLivraison, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- tfCodePostalLivraison ----
        tfCodePostalLivraison.setBackground(Color.white);
        tfCodePostalLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCodePostalLivraison.setMinimumSize(new Dimension(60, 12));
        tfCodePostalLivraison.setPreferredSize(new Dimension(60, 12));
        tfCodePostalLivraison.setBorder(null);
        tfCodePostalLivraison.setName("tfCodePostalLivraison");
        pnlCoordonneesClientLivre.add(tfCodePostalLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesClientLivre, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlContact ========
      {
        pnlContact.setBorder(new TitledBorder(new LineBorder(Color.white), "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("Arial", Font.BOLD, 12)));
        pnlContact.setOpaque(false);
        pnlContact.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlContact.setMinimumSize(new Dimension(350, 110));
        pnlContact.setPreferredSize(new Dimension(350, 110));
        pnlContact.setMaximumSize(new Dimension(350, 110));
        pnlContact.setForeground(new Color(153, 153, 153));
        pnlContact.setBackground(new Color(204, 204, 204));
        pnlContact.setName("pnlContact");
        pnlContact.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 85, 250, 0 };
        ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContact.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumeroClient ----
        lbNumeroClient.setText("Num\u00e9ro fournisseur :");
        lbNumeroClient.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroClient.setFont(new Font("Arial", Font.BOLD, 12));
        lbNumeroClient.setName("lbNumeroClient");
        pnlContact.add(lbNumeroClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 20, 5), 0, 0));
        
        // ---- tfNumeroClient ----
        tfNumeroClient.setBackground(new Color(234, 234, 218));
        tfNumeroClient.setBorder(null);
        tfNumeroClient.setIconTextGap(6);
        tfNumeroClient.setFont(new Font("Arial", Font.PLAIN, 12));
        tfNumeroClient.setPreferredSize(new Dimension(70, 15));
        tfNumeroClient.setMinimumSize(new Dimension(70, 15));
        tfNumeroClient.setAlignmentX(1.0F);
        tfNumeroClient.setMaximumSize(new Dimension(70, 15));
        tfNumeroClient.setName("tfNumeroClient");
        pnlContact.add(tfNumeroClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 20, 0), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact :");
        lbContact.setFont(new Font("Arial", Font.BOLD, 12));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setPreferredSize(new Dimension(70, 12));
        lbContact.setMinimumSize(new Dimension(70, 12));
        lbContact.setMaximumSize(new Dimension(70, 12));
        lbContact.setName("lbContact");
        pnlContact.add(lbContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNomComplet ----
        tfNomComplet.setBackground(Color.white);
        tfNomComplet.setFont(new Font("Arial", Font.PLAIN, 12));
        tfNomComplet.setMinimumSize(new Dimension(200, 12));
        tfNomComplet.setPreferredSize(new Dimension(200, 12));
        tfNomComplet.setBorder(null);
        tfNomComplet.setName("tfNomComplet");
        pnlContact.add(tfNomComplet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMail ----
        lbMail.setText("EMail :");
        lbMail.setFont(new Font("Arial", Font.BOLD, 12));
        lbMail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMail.setPreferredSize(new Dimension(70, 12));
        lbMail.setMinimumSize(new Dimension(70, 12));
        lbMail.setMaximumSize(new Dimension(70, 12));
        lbMail.setName("lbMail");
        pnlContact.add(lbMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("Arial", Font.PLAIN, 12));
        tfEmail.setMinimumSize(new Dimension(300, 15));
        tfEmail.setPreferredSize(new Dimension(300, 15));
        tfEmail.setBorder(null);
        tfEmail.setName("tfEmail");
        pnlContact.add(tfEmail, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone :");
        lbTelephone.setFont(new Font("Arial", Font.BOLD, 12));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setPreferredSize(new Dimension(70, 12));
        lbTelephone.setMinimumSize(new Dimension(70, 12));
        lbTelephone.setMaximumSize(new Dimension(70, 12));
        lbTelephone.setName("lbTelephone");
        pnlContact.add(lbTelephone, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 10, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("Arial", Font.PLAIN, 12));
        tfTelephone.setMinimumSize(new Dimension(100, 12));
        tfTelephone.setPreferredSize(new Dimension(100, 12));
        tfTelephone.setBorder(null);
        tfTelephone.setName("tfTelephone");
        pnlContact.add(tfTelephone, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax :");
        lbFax.setFont(new Font("Arial", Font.BOLD, 12));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlContact.add(lbFax, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 10, 0, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("Arial", Font.PLAIN, 12));
        tfFax.setPreferredSize(new Dimension(100, 20));
        tfFax.setMinimumSize(new Dimension(100, 20));
        tfFax.setBorder(null);
        tfFax.setName("tfFax");
        pnlContact.add(tfFax, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlContact, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHEAST, GridBagConstraints.NONE,
          new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlTitreListe ========
      {
        pnlTitreListe.setOpaque(false);
        pnlTitreListe.setMaximumSize(new Dimension(341, 30));
        pnlTitreListe.setMinimumSize(new Dimension(341, 30));
        pnlTitreListe.setPreferredSize(new Dimension(341, 30));
        pnlTitreListe.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlTitreListe.setName("pnlTitreListe");
        pnlTitreListe.setLayout(null);
        
        // ---- lbTitre ----
        lbTitre.setText("Lignes articles de ce document");
        lbTitre.setFont(new Font("sansserif", Font.BOLD, 14));
        lbTitre.setName("lbTitre");
        pnlTitreListe.add(lbTitre);
        lbTitre.setBounds(5, 5, 336, lbTitre.getPreferredSize().height);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlTitreListe.getComponentCount(); i++) {
            Rectangle bounds = pnlTitreListe.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlTitreListe.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlTitreListe.setMinimumSize(preferredSize);
          pnlTitreListe.setPreferredSize(preferredSize);
        }
      }
      pnlContenu.add(pnlTitreListe, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpLigneArticle ========
      {
        scpLigneArticle.setPreferredSize(new Dimension(1050, 300));
        scpLigneArticle.setBackground(Color.white);
        scpLigneArticle.setMinimumSize(new Dimension(1050, 250));
        scpLigneArticle.setOpaque(true);
        scpLigneArticle.setName("scpLigneArticle");
        
        // ---- tblLigneArticle ----
        tblLigneArticle.setShowVerticalLines(true);
        tblLigneArticle.setShowHorizontalLines(true);
        tblLigneArticle.setBackground(Color.white);
        tblLigneArticle.setRowHeight(20);
        tblLigneArticle.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblLigneArticle.setAutoCreateRowSorter(true);
        tblLigneArticle.setSelectionBackground(new Color(57, 105, 138));
        tblLigneArticle.setGridColor(new Color(204, 204, 204));
        tblLigneArticle.setName("tblLigneArticle");
        tblLigneArticle.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblLigneArticleMouseClicked(e);
          }
        });
        scpLigneArticle.setViewportView(tblLigneArticle);
      }
      pnlContenu.add(scpLigneArticle, new GridBagConstraints(0, 3, 3, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(5, 5, 10, 5), 0, 0));
      
      // ======== pnlChiffrage ========
      {
        pnlChiffrage.setBorder(new LineBorder(Color.white));
        pnlChiffrage.setBackground(Color.white);
        pnlChiffrage.setMaximumSize(new Dimension(1177, 30));
        pnlChiffrage.setName("pnlChiffrage");
        pnlChiffrage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChiffrage.getLayout()).columnWidths = new int[] { 117, 75, 66, 177, 160, 95, 158, 95, 120, 90, 0 };
        ((GridBagLayout) pnlChiffrage.getLayout()).rowHeights = new int[] { 31, 0 };
        ((GridBagLayout) pnlChiffrage.getLayout()).columnWeights =
            new double[] { 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlChiffrage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbPoidsTotal ----
        lbPoidsTotal.setText("Poids total :");
        lbPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPoidsTotal.setFont(new Font("Arial", Font.BOLD, 14));
        lbPoidsTotal.setMaximumSize(new Dimension(100, 17));
        lbPoidsTotal.setMinimumSize(new Dimension(100, 17));
        lbPoidsTotal.setPreferredSize(new Dimension(100, 17));
        lbPoidsTotal.setName("lbPoidsTotal");
        pnlChiffrage.add(lbPoidsTotal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbPoidsTotalValeur ----
        lbPoidsTotalValeur.setBackground(new Color(234, 234, 218));
        lbPoidsTotalValeur.setBorder(null);
        lbPoidsTotalValeur.setIconTextGap(6);
        lbPoidsTotalValeur.setFont(new Font("Arial", Font.PLAIN, 14));
        lbPoidsTotalValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbPoidsTotalValeur.setPreferredSize(new Dimension(100, 22));
        lbPoidsTotalValeur.setMinimumSize(new Dimension(100, 22));
        lbPoidsTotalValeur.setHorizontalTextPosition(SwingConstants.LEADING);
        lbPoidsTotalValeur.setName("lbPoidsTotalValeur");
        pnlChiffrage.add(lbPoidsTotalValeur,
            new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalHT ----
        lbTotalHT.setText("Total HT :");
        lbTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTotalHT.setFont(new Font("Arial", Font.BOLD, 14));
        lbTotalHT.setMaximumSize(new Dimension(100, 17));
        lbTotalHT.setMinimumSize(new Dimension(100, 17));
        lbTotalHT.setPreferredSize(new Dimension(100, 17));
        lbTotalHT.setName("lbTotalHT");
        pnlChiffrage.add(lbTotalHT, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalHTValeur ----
        lbTotalHTValeur.setBackground(new Color(234, 234, 218));
        lbTotalHTValeur.setBorder(null);
        lbTotalHTValeur.setIconTextGap(6);
        lbTotalHTValeur.setFont(new Font("Arial", Font.PLAIN, 14));
        lbTotalHTValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbTotalHTValeur.setPreferredSize(new Dimension(100, 22));
        lbTotalHTValeur.setMinimumSize(new Dimension(100, 22));
        lbTotalHTValeur.setHorizontalTextPosition(SwingConstants.LEADING);
        lbTotalHTValeur.setName("lbTotalHTValeur");
        pnlChiffrage.add(lbTotalHTValeur, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalTVA ----
        lbTotalTVA.setText("Total TVA :");
        lbTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTotalTVA.setFont(new Font("Arial", Font.BOLD, 14));
        lbTotalTVA.setMaximumSize(new Dimension(100, 17));
        lbTotalTVA.setMinimumSize(new Dimension(100, 17));
        lbTotalTVA.setPreferredSize(new Dimension(100, 17));
        lbTotalTVA.setName("lbTotalTVA");
        pnlChiffrage.add(lbTotalTVA, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalTVAValeur ----
        lbTotalTVAValeur.setBackground(new Color(234, 234, 218));
        lbTotalTVAValeur.setBorder(null);
        lbTotalTVAValeur.setIconTextGap(6);
        lbTotalTVAValeur.setFont(new Font("Arial", Font.PLAIN, 14));
        lbTotalTVAValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbTotalTVAValeur.setPreferredSize(new Dimension(100, 22));
        lbTotalTVAValeur.setMinimumSize(new Dimension(100, 22));
        lbTotalTVAValeur.setHorizontalTextPosition(SwingConstants.LEADING);
        lbTotalTVAValeur.setName("lbTotalTVAValeur");
        pnlChiffrage.add(lbTotalTVAValeur,
            new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalTTC ----
        lbTotalTTC.setText("Total TTC :");
        lbTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTotalTTC.setFont(new Font("Arial", Font.BOLD, 14));
        lbTotalTTC.setMaximumSize(new Dimension(100, 17));
        lbTotalTTC.setMinimumSize(new Dimension(100, 17));
        lbTotalTTC.setPreferredSize(new Dimension(100, 17));
        lbTotalTTC.setName("lbTotalTTC");
        pnlChiffrage.add(lbTotalTTC, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbTotalTTCValeur ----
        lbTotalTTCValeur.setBackground(new Color(234, 234, 218));
        lbTotalTTCValeur.setBorder(null);
        lbTotalTTCValeur.setIconTextGap(6);
        lbTotalTTCValeur.setFont(new Font("Arial", Font.PLAIN, 14));
        lbTotalTTCValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbTotalTTCValeur.setPreferredSize(new Dimension(100, 22));
        lbTotalTTCValeur.setMinimumSize(new Dimension(100, 22));
        lbTotalTTCValeur.setHorizontalTextPosition(SwingConstants.LEADING);
        lbTotalTTCValeur.setName("lbTotalTTCValeur");
        pnlChiffrage.add(lbTotalTTCValeur,
            new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlChiffrage, new GridBagConstraints(0, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
          new Insets(10, 10, 15, 10), 0, 0));
      
      // ======== pnlReglement ========
      {
        pnlReglement.setMinimumSize(new Dimension(1000, 30));
        pnlReglement.setPreferredSize(new Dimension(1000, 30));
        pnlReglement.setOpaque(false);
        pnlReglement.setName("pnlReglement");
        pnlReglement.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlReglement.getLayout()).columnWidths = new int[] { 844, 0 };
        ((GridBagLayout) pnlReglement.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlReglement.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlReglement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      }
      pnlContenu.add(pnlReglement, new GridBagConstraints(0, 5, 3, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(10, 10, 10, 80), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private JPanel pnlInfosDocument;
  private JLabel lbDate;
  private JLabel calDateValeur;
  private JLabel lbMagasin;
  private JLabel lbMagasinValeur;
  private JLabel lbAcheteur;
  private JLabel lbAcheteurValeur;
  private JLabel lbReferenceLongue;
  private JLabel lbReferenceLongueValeur;
  private JLabel lbReferenceCourte;
  private JLabel lbReferenceCourteValeur;
  private JLabel lbReferenceLivraison;
  private JLabel lbReferenceLivraisonValeur;
  private JLabel lbDateLivraison;
  private JLabel lbDateLivraisonValeur;
  private JLabel lbDateConfirmation;
  private JLabel lbDateConfirmationValeur;
  private JLabel lbDateReception;
  private JLabel lbDateReceptionValeur;
  private JLabel lbNumeroFacture;
  private JLabel lbNumeroFactureValeur;
  private JLabel lbDocumentOrigine;
  private JLabel lbDocumentOrigineValeur;
  private JPanel pnlCoordonneesClientLivre;
  private JLabel tfCiviliteLivraison;
  private JLabel tfNomLivraison;
  private JLabel tfComplementNomLivraison;
  private JLabel tfRueLivraison;
  private JLabel tfLocalisationLivraison;
  private JLabel tfVilleLivraison;
  private JLabel tfCodePostalLivraison;
  private JPanel pnlContact;
  private JLabel lbNumeroClient;
  private JLabel tfNumeroClient;
  private JLabel lbContact;
  private JLabel tfNomComplet;
  private JLabel lbMail;
  private JLabel tfEmail;
  private JLabel lbTelephone;
  private JLabel tfTelephone;
  private JLabel lbFax;
  private JLabel tfFax;
  private JPanel pnlTitreListe;
  private JLabel lbTitre;
  private JScrollPane scpLigneArticle;
  private NRiTable tblLigneArticle;
  private JPanel pnlChiffrage;
  private JLabel lbPoidsTotal;
  private JLabel lbPoidsTotalValeur;
  private JLabel lbTotalHT;
  private JLabel lbTotalHTValeur;
  private JLabel lbTotalTVA;
  private JLabel lbTotalTVAValeur;
  private JLabel lbTotalTTC;
  private JLabel lbTotalTTCValeur;
  private JPanel pnlReglement;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
