/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.commandeachatliee;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ListSelectionEvent;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueCommandeAchatLiee extends AbstractVueDialogue<ModeleCommandeAchatLiee> {
  // Constantes
  public final static String[] TITRE_LISTE_DOCUMENT = new String[] { "Identifiant", "Fournisseur" };
  private static final String BOUTON_SELECTIONNER_TOUT = "Sélectionner tout";
  
  /**
   * Constructeur.
   */
  public VueCommandeAchatLiee(ModeleCommandeAchatLiee pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(600, 400);
    setResizable(false);
    
    // La table contenant la liste des commandes d'achat
    scpListeCommandeAchat.getViewport().setBackground(Color.WHITE);
    tblListeCommandeAchat.personnaliserAspect(TITRE_LISTE_DOCUMENT, new int[] { 100, 500 }, new int[] { 100, 500 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListeCommandeAchat.getSelectedRowCount() > 0) {
          getModele().selectionnerCommandeAchat(tblListeCommandeAchat.getListeIndiceSelection());
        }
      }
    };
    tblListeCommandeAchat.getSelectionModel().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    tblListeCommandeAchat.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SELECTIONNER_TOUT, 't', true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirListe();
    rafraichirBoutonValider();
    rafraichirBoutonSelectionnerTout();
    
    // Positionnement du focus
    tblListeCommandeAchat.requestFocusInWindow();
  }
  
  // -- Méthodes privées
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListe() {
    ListeDocumentAchat listeDocumentAchat = getModele().getListeDocumentAchat();
    String[][] donnees = null;
    if (listeDocumentAchat != null) {
      donnees = new String[listeDocumentAchat.size()][TITRE_LISTE_DOCUMENT.length];
      for (int ligne = 0; ligne < listeDocumentAchat.size(); ligne++) {
        DocumentAchat documentAchat = listeDocumentAchat.get(ligne);
        if (documentAchat != null) {
          // Identifiant du document d'achat
          donnees[ligne][0] = documentAchat.getId().toString();
          // Fournisseur
          if (documentAchat.getAdresseFournisseur() != null) {
            donnees[ligne][1] = documentAchat.getAdresseFournisseur().getNom();
          }
        }
      }
    }
    tblListeCommandeAchat.mettreAJourDonnees(donnees);
    
    // Réactualiser les lignes sélectionnées
    if (getModele().getListeIndicesDocumentSelectionne() != null && !getModele().getListeIndicesDocumentSelectionne().isEmpty()) {
      for (int indexSelectionne : getModele().getListeIndicesDocumentSelectionne()) {
        tblListeCommandeAchat.getSelectionModel().addSelectionInterval(indexSelectionne, indexSelectionne);
      }
    }
  }
  
  /**
   * Rafraichir le bouton valider
   */
  private void rafraichirBoutonValider() {
    snBarreBouton.getBouton(EnumBouton.VALIDER).setEnabled(
        getModele().getListeDocumentAchatSelectionnes() != null && !getModele().getListeDocumentAchatSelectionnes().isEmpty());
  }
  
  /**
   * Rafraichir le bouton sélectionner tout
   */
  private void rafraichirBoutonSelectionnerTout() {
    ListeDocumentAchat listeDocumentAchat = getModele().getListeDocumentAchat();
    snBarreBouton.getBouton(BOUTON_SELECTIONNER_TOUT).setEnabled(listeDocumentAchat != null && !listeDocumentAchat.isSelectionComplete());
  }
  
  /**
   * 
   * 
   * // -- Méthodes évènementielles
   * /**
   * Evènementiel barre des boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecAnnulation();
      }
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      if (pSNBouton.isBouton(BOUTON_SELECTIONNER_TOUT)) {
        getModele().selectionnerTout();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Lorsqu'on change la ligne sélectionnée dans la table
   */
  private void tblListeCommandeAchatValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Sélection par clics de souris
   * Si on double clic on quitte avec validation de la ligne sélectionnée
   */
  private void tblListeCommandeAchatMouseClicked(MouseEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      // Sur chaque clic
      getModele().selectionnerCommandeAchat(tblListeCommandeAchat.getListeIndiceSelection());
      
      // Double clic : validation de la sélection
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlPrincipal = new SNPanelContenu();
    scpListeCommandeAchat = new JScrollPane();
    tblListeCommandeAchat = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(600, 400));
    setForeground(Color.black);
    setTitle("Liste des commandes d'achats");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setMinimumSize(new Dimension(45, 250));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setBackground(new Color(238, 238, 210));
        pnlPrincipal.setPreferredSize(new Dimension(20, 220));
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 200, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== scpListeCommandeAchat ========
        {
          scpListeCommandeAchat.setPreferredSize(new Dimension(1050, 200));
          scpListeCommandeAchat.setMaximumSize(new Dimension(32767, 200));
          scpListeCommandeAchat.setName("scpListeCommandeAchat");
          
          // ---- tblListeCommandeAchat ----
          tblListeCommandeAchat.setName("tblListeCommandeAchat");
          tblListeCommandeAchat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeCommandeAchatMouseClicked(e);
            }
          });
          scpListeCommandeAchat.setViewportView(tblListeCommandeAchat);
        }
        pnlPrincipal.add(scpListeCommandeAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlPrincipal, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlPrincipal;
  private JScrollPane scpListeCommandeAchat;
  private NRiTable tblListeCommandeAchat;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
