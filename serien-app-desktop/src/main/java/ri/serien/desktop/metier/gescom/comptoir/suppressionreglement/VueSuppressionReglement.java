/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.suppressionreglement;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueSuppressionReglement extends AbstractVueDialogue<ModeleSuppressionReglement> {
  /**
   * Constructeur.
   */
  public VueSuppressionReglement(ModeleSuppressionReglement pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Les raccourcis claviers
    rbEffacerSaisie.setMnemonic(KeyEvent.VK_J);
    rbEffacerTout.setMnemonic(KeyEvent.VK_E);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirEffacerSaisie();
    rafraichirEffacerTout();
    rafraichirBoutonValider();
  }
  
  // -- Méthodes privées
  
  private void rafraichirEffacerSaisie() {
    if (getModele().isAfficherChoixEffacerSaisie()) {
      rbEffacerSaisie.setEnabled(true);
    }
    else {
      rbEffacerSaisie.setEnabled(false);
    }
    rbEffacerSaisie.setSelected(getModele().isEffacerSaisie());
  }
  
  private void rafraichirEffacerTout() {
    if (getModele().isAfficherChoixToutEffacer()) {
      rbEffacerTout.setEnabled(true);
    }
    else {
      rbEffacerTout.setEnabled(false);
    }
    rbEffacerTout.setSelected(getModele().isEffacerTout());
  }
  
  private void rafraichirBoutonValider() {
    boolean actif = true;
    if (!rbEffacerSaisie.isSelected() && !rbEffacerTout.isSelected()) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else {
        if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
          getModele().quitterAvecAnnulation();
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbEffacerSaisieItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierChoixTypeEffacement(ModeleSuppressionReglement.EFFACER_SAISIE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbEffacerToutItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierChoixTypeEffacement(ModeleSuppressionReglement.EFFACER_TOUT);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlChoix = new SNPanelContenu();
    lbQuestion = new SNLabelTitre();
    rbEffacerSaisie = new SNRadioButton();
    rbEffacerTout = new SNRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Confirmation de l'effacement des r\u00e8glements");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(600, 300));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlChoix ========
      {
        pnlChoix.setOpaque(false);
        pnlChoix.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlChoix.setName("pnlChoix");
        pnlChoix.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChoix.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlChoix.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlChoix.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlChoix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbQuestion ----
        lbQuestion.setText("Vous avez demand\u00e9 l'effacement des r\u00e8glements. Souhaitez-vous:");
        lbQuestion.setName("lbQuestion");
        pnlChoix.add(lbQuestion, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbEffacerSaisie ----
        rbEffacerSaisie.setText("<html><u>J</u>uste effacer les r\u00e8glements que vous venir de saisir.</html>");
        rbEffacerSaisie.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbEffacerSaisie.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbEffacerSaisie.setName("rbEffacerSaisie");
        rbEffacerSaisie.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbEffacerSaisieItemStateChanged(e);
          }
        });
        pnlChoix.add(rbEffacerSaisie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbEffacerTout ----
        rbEffacerTout.setText("<html><u>E</u>ffacer les r\u00e8glements saisis dans la journ\u00e9e.</html>");
        rbEffacerTout.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbEffacerTout.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbEffacerTout.setName("rbEffacerTout");
        rbEffacerTout.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbEffacerToutItemStateChanged(e);
          }
        });
        pnlChoix.add(rbEffacerTout, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlChoix, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbEffacerSaisie);
    buttonGroup1.add(rbEffacerTout);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlChoix;
  private SNLabelTitre lbQuestion;
  private SNRadioButton rbEffacerSaisie;
  private SNRadioButton rbEffacerTout;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
