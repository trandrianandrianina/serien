/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.AbstractAction;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet mémo de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletInformationsTechniquesLigneAchat extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final String BT_CREER_COMMENTAIRE = "Créer commentaire";
  private static final String BT_FICHE_TECHNIQUE_1 = "Fiche technique 1";
  private static final String BT_FICHE_TECHNIQUE_2 = "Fiche technique 2";
  private static final String BT_FICHE_TECHNIQUE_3 = "Fiche technique 3";
  private static final String BT_FICHE_TECHNIQUE_4 = "Fiche technique 4";
  private static final String BT_FICHE_TECHNIQUE_5 = "Fiche technique 5";
  
  // Variables
  private ModeleDetailLigneAchat modele = null;
  
  public VueOngletInformationsTechniquesLigneAchat(ModeleDetailLigneAchat pModele) {
    modele = pModele;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Taille max du mémo (10 lignes de 120 caractères)
    taBlocNotesCNA.setLongueurMaximumTexte(Memo.LONGUEUR_TEXTE_MAX);
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    taBlocNotesCNA.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    taBlocNotesCNA.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    taBlocNotesCNA.setEditable(false);
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BT_CREER_COMMENTAIRE, 'c', false);
    snBarreBouton.ajouterBouton(BT_FICHE_TECHNIQUE_1, 'f', false);
    snBarreBouton.ajouterBouton(BT_FICHE_TECHNIQUE_2, '2', false);
    snBarreBouton.ajouterBouton(BT_FICHE_TECHNIQUE_3, '3', false);
    snBarreBouton.ajouterBouton(BT_FICHE_TECHNIQUE_4, '4', false);
    snBarreBouton.ajouterBouton(BT_FICHE_TECHNIQUE_5, '5', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Ajouter les boutons des fiches techniques
    rafraichirBoutonsURL();
    
    // Le mémo
    rafraichirTexteMemo();
    
    snBarreBouton.getBouton(EnumBouton.VALIDER).requestFocusInWindow();
  }
  
  private void rafraichirTexteMemo() {
    if (modele.retournerTexteBlocNoteCNA() != null) {
      taBlocNotesCNA.setText(modele.retournerTexteBlocNoteCNA());
    }
    else {
      taBlocNotesCNA.setText("");
    }
    if (taBlocNotesCNA.isEditable()) {
      taBlocNotesCNA.setCaretPosition(0);
    }
  }
  
  private void rafraichirBoutonsURL() {
    String[] listeURL = modele.getListeURL();
    boolean bt1isActif = false;
    boolean bt2isActif = false;
    boolean bt3isActif = false;
    boolean bt4isActif = false;
    boolean bt5isActif = false;
    if (listeURL != null) {
      for (int i = 0; i < listeURL.length; i++) {
        switch (i) {
          case 0:
            bt1isActif = !Constantes.normerTexte(listeURL[i]).isEmpty();
            snBarreBouton.getBouton(BT_FICHE_TECHNIQUE_1).setText(modele.getNomDomaine(listeURL[i]));
            break;
          case 1:
            bt2isActif = !Constantes.normerTexte(listeURL[i]).isEmpty();
            snBarreBouton.getBouton(BT_FICHE_TECHNIQUE_2).setText(modele.getNomDomaine(listeURL[i]));
            break;
          case 2:
            bt3isActif = !Constantes.normerTexte(listeURL[i]).isEmpty();
            snBarreBouton.getBouton(BT_FICHE_TECHNIQUE_3).setText(modele.getNomDomaine(listeURL[i]));
            break;
          case 3:
            bt4isActif = !Constantes.normerTexte(listeURL[i]).isEmpty();
            snBarreBouton.getBouton(BT_FICHE_TECHNIQUE_4).setText(modele.getNomDomaine(listeURL[i]));
            break;
          case 4:
            bt5isActif = !Constantes.normerTexte(listeURL[i]).isEmpty();
            snBarreBouton.getBouton(BT_FICHE_TECHNIQUE_5).setText(modele.getNomDomaine(listeURL[i]));
            break;
        }
      }
    }
    
    snBarreBouton.activerBouton(BT_FICHE_TECHNIQUE_1, bt1isActif);
    snBarreBouton.activerBouton(BT_FICHE_TECHNIQUE_2, bt2isActif);
    snBarreBouton.activerBouton(BT_FICHE_TECHNIQUE_3, bt3isActif);
    snBarreBouton.activerBouton(BT_FICHE_TECHNIQUE_4, bt4isActif);
    snBarreBouton.activerBouton(BT_FICHE_TECHNIQUE_5, bt5isActif);
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BT_CREER_COMMENTAIRE)) {
        modele.copierITCversCommentaire();
      }
      else if (pSNBouton.isBouton(BT_FICHE_TECHNIQUE_1)) {
        modele.ouvrirURL(0);
      }
      else if (pSNBouton.isBouton(BT_FICHE_TECHNIQUE_2)) {
        modele.ouvrirURL(1);
      }
      else if (pSNBouton.isBouton(BT_FICHE_TECHNIQUE_3)) {
        modele.ouvrirURL(2);
      }
      else if (pSNBouton.isBouton(BT_FICHE_TECHNIQUE_4)) {
        modele.ouvrirURL(3);
      }
      else if (pSNBouton.isBouton(BT_FICHE_TECHNIQUE_5)) {
        modele.ouvrirURL(4);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void taMemoItcFocusLost(FocusEvent e) {
    try {
      modele.modifierITC(taBlocNotesCNA.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlMemo = new JPanel();
    scpMemoItc = new JScrollPane();
    taBlocNotesCNA = new RiTextArea();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlMemo ========
    {
      pnlMemo.setOpaque(false);
      pnlMemo.setMinimumSize(new Dimension(970, 420));
      pnlMemo.setPreferredSize(new Dimension(970, 420));
      pnlMemo.setName("pnlMemo");
      pnlMemo.setLayout(null);
      
      // ======== scpMemoItc ========
      {
        scpMemoItc.setName("scpMemoItc");
        
        // ---- taBlocNotesCNA ----
        taBlocNotesCNA.setBorder(new BevelBorder(BevelBorder.LOWERED));
        taBlocNotesCNA.setWrapStyleWord(true);
        taBlocNotesCNA.setLineWrap(true);
        taBlocNotesCNA.setFont(taBlocNotesCNA.getFont().deriveFont(taBlocNotesCNA.getFont().getSize() + 3f));
        taBlocNotesCNA.setEditable(false);
        taBlocNotesCNA.setName("taBlocNotesCNA");
        taBlocNotesCNA.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            taMemoItcFocusLost(e);
          }
        });
        scpMemoItc.setViewportView(taBlocNotesCNA);
      }
      pnlMemo.add(scpMemoItc);
      scpMemoItc.setBounds(10, 10, 945, 325);
      
      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for (int i = 0; i < pnlMemo.getComponentCount(); i++) {
          Rectangle bounds = pnlMemo.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = pnlMemo.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        pnlMemo.setMinimumSize(preferredSize);
        pnlMemo.setPreferredSize(preferredSize);
      }
    }
    add(pnlMemo, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private JPanel pnlMemo;
  private JScrollPane scpMemoItc;
  private RiTextArea taBlocNotesCNA;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
