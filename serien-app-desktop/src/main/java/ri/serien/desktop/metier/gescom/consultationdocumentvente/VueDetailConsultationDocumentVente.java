/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran détail de la consultation de documents de ventes.
 */
public class VueDetailConsultationDocumentVente extends AbstractVuePanel<ModeleConsultationDocumentVente> {
  // Constantes
  private static final String BOUTON_AFFICHER_PRIX = "Afficher prix";
  private static final String BOUTON_MASQUER_PRIX = "Masquer prix";
  private static final String BOUTON_AFFICHER_LIGNE = "Afficher détail ligne";
  private static final String BOUTON_AFFICHER_DOCUMENT = "Afficher les documents";
  
  private static final String[] TITRE_COLONNE_STANDARD =
      new String[] { "Qt\u00e9", "UC", "Article", "Etat article", "Prix HT", "Qt\u00e9 UV", "UV", "Montant HT", };
  private static final String[] TITRE_COLONNE_DETAIL = new String[] { "Qt\u00e9", "UC", "Article", "Prix base", "Remise", "Prix net",
      "Qt\u00e9 UV", "UV", "Indice", "Marge", "Prix revient", "Montant HT" };
  private static final String[] TITRE_LISTE_REMBOURSEMENT = new String[] { "Numéro", "Montant HT", "Réference" };
  
  // Variables
  private DefaultTableModel tableModelStandard = null;
  private DefaultTableModel tableModelDetail = null;
  private JTableCellRendererDocumentVenteDetail rendererStandard = new JTableCellRendererDocumentVenteDetail();
  private JTableCellRendererDocumentVenteDetailPrix rendererDetail = new JTableCellRendererDocumentVenteDetailPrix();
  
  /**
   * Constructeur.
   */
  public VueDetailConsultationDocumentVente(ModeleConsultationDocumentVente pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    tableModelStandard = new DefaultTableModel(null, TITRE_COLONNE_STANDARD) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    
    // Création de la liste des lignes du document
    tableModelDetail = new DefaultTableModel(null, TITRE_COLONNE_DETAIL) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    int[] dimension = new int[] { 60, 30, 280, 100, 80, 30, 100 };
    int[] dimensionMax = new int[] { 60, 30, -1, 100, 80, 30, 100 };
    int[] justification = new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE,
        NRiTable.CENTRE, NRiTable.DROITE };
    tblLignesArticlesDocument.personnaliserAspect(TITRE_COLONNE_STANDARD, dimension, dimensionMax, justification, 14);
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    
    // Création de la liste des remboursements
    scpListeRemboursement.getViewport().setBackground(Color.WHITE);
    tblListeRemboursement.personnaliserAspect(TITRE_LISTE_REMBOURSEMENT, new int[] { 80, 100, 300 }, new int[] { 80, 100, 300 },
        new int[] { NRiTable.CENTRE, NRiTable.DROITE, NRiTable.GAUCHE }, 14);
    
    // Configurer la barre de boutons
    snBarreBouton.setModeNavigation(true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENT, 'a', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_PRIX, 'p', true);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_PRIX, 'p', false);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_LIGNE, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir entête
    rafraichirDateCreation();
    rafraichirNomMagasin();
    rafraichirNomRepresentant();
    rafraichirNomVendeur();
    rafraichirReferenceLongue();
    rafraichirReferenceCourte();
    rafraichirReferenceChantier();
    rafraichirNumeroFacture();
    rafraichirDateLivraisonEnlevement();
    rafraichirDateValidite();
    rafraichirDocumentAchatLie();
    
    // Rafraîchir l'information de livraison et d'enlèvement
    rafraichirInformationLivraisonEnlevement();
    
    // Rafraîchir l'adresse de facturation
    rafraichirCiviliteFacturation();
    rafraichirNomFacturation();
    rafraichirComplementNomFacturation();
    rafraichirRueFacturation();
    rafraichirLocalisationFacturation();
    rafraichirCodePostalFacturation();
    rafraichirVilleFacturation();
    
    // Rafraîchir l'adresse de livraison
    rafraichirCiviliteLivraison();
    rafraichirNomLivraison();
    rafraichirComplementNomLivraison();
    rafraichirRueLivraison();
    rafraichirLocalisationLivraison();
    rafraichirCodePostalLivraison();
    rafraichirVilleLivraison();
    
    // Rafraîchir les informations de contact
    rafraichirNumeroClientContact();
    rafraichirNomContact();
    rafraichirMailContact();
    rafraichirTelephoneContact();
    rafraichirFaxContact();
    
    // Rafraîchir totaux
    rafraichirPoidsTotal();
    rafraichirPourcentageMargeTotale();
    rafraichirTotalHT();
    rafraichirTotalTVA();
    rafraichirTotalTTC();
    rafraichirMessageReglement();
    rafraichirListeAvoir();
    
    // Rafraîchir les lignes du document de ventes
    if (getModele().getModeAffichageTable() == ModeleConsultationDocumentVente.MODE_AFFICHAGE_TABLE_STANDARD) {
      rafraichirListeStandard();
    }
    else {
      rafraichirListeDetail();
    }
    
    // Rafraîchir les boutons
    rafraichirBoutonNavigation();
    rafraichirBoutonAfficherLigne();
    rafraichirBoutonAfficherPrix();
    rafraichirBoutonMasquerPrix();
    rafraichirBoutonAfficherDocument();
  }
  
  private void rafraichirDateCreation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getDateCreation() != null) {
      calDateValeur.setText(Constantes.convertirDateHeureEnTexte(documentVente.getDateCreation()));
    }
    else {
      calDateValeur.setText("");
    }
  }
  
  private void rafraichirNomMagasin() {
    Magasin magasinSortie = getModele().getMagasinSortie();
    if (magasinSortie != null) {
      lbMagasinValeur.setText(magasinSortie.getNomAvecCode());
    }
    else {
      lbMagasinValeur.setText("");
    }
  }
  
  private void rafraichirNomRepresentant() {
    if (getModele().getRepresentant() != null) {
      lbRepresentantValeur.setText(getModele().getRepresentant().getNomAvecCode());
    }
    else {
      lbRepresentantValeur.setText("");
    }
  }
  
  private void rafraichirNomVendeur() {
    if (getModele().getVendeur() != null) {
      lbVendeurValeur.setText(getModele().getVendeur().getNomAvecCode());
    }
    else {
      lbVendeurValeur.setText("");
    }
  }
  
  private void rafraichirReferenceLongue() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getReferenceLongue() != null) {
      lbReferenceLongueValeur.setText(documentVente.getReferenceLongue());
    }
    else {
      lbReferenceLongueValeur.setText("");
    }
  }
  
  private void rafraichirReferenceCourte() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getReferenceCourte() != null) {
      lbReferenceCourteValeur.setText(documentVente.getReferenceCourte());
    }
    else {
      lbReferenceCourteValeur.setText("");
    }
  }
  
  private void rafraichirReferenceChantier() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getIdChantier() != null) {
      lbReferenceChantier.setVisible(true);
      lbReferenceChantierValeur.setText(getModele().chargerChantier(documentVente.getIdChantier()).getLibelle());
    }
    else {
      lbReferenceChantier.setVisible(false);
      lbReferenceChantierValeur.setText("");
    }
    
  }
  
  private void rafraichirNumeroFacture() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getId().getNumeroFacture() != null) {
      lbNumeroFactureValeur.setText(Constantes.convertirIntegerEnTexte(documentVente.getId().getNumeroFacture(), 0) + " du "
          + Constantes.convertirDateEnTexte(documentVente.getDateFacturation()));
      lbNumeroFactureValeur.setVisible(true);
      lbNumeroFacture.setVisible(true);
    }
    else {
      lbNumeroFactureValeur.setText("");
      lbNumeroFactureValeur.setVisible(false);
      lbNumeroFacture.setVisible(false);
    }
  }
  
  private void rafraichirDateLivraisonEnlevement() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getDateExpeditionBon() != null) {
      lbDateLivraisonValeur.setText(Constantes.convertirDateEnTexte(documentVente.getDateExpeditionBon()));
      if (documentVente.isEnlevement()) {
        lbDateLivraison.setText("Date d'enlèvement :");
      }
      else {
        lbDateLivraison.setText("Date de livraison :");
      }
      lbDateLivraisonValeur.setVisible(true);
      lbDateLivraison.setVisible(true);
    }
    else {
      lbDateLivraisonValeur.setText("");
      lbDateLivraison.setText("");
      lbDateLivraisonValeur.setVisible(false);
      lbDateLivraison.setVisible(false);
    }
  }
  
  private void rafraichirDateValidite() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.isDevis()) {
      if (documentVente.getDateValiditeDevis() != null) {
        Date aujourdhui = new Date();
        if (documentVente.getDateValiditeDevis().compareTo(aujourdhui) < 0) {
          lbDateValiditeValeur.setText(Constantes.convertirDateEnTexte(documentVente.getDateValiditeDevis()) + " (dépassée)");
        }
        else {
          lbDateValiditeValeur.setText(Constantes.convertirDateEnTexte(documentVente.getDateValiditeDevis()));
        }
      }
      else {
        lbDateValiditeValeur.setText("Pas de limite");
      }
      
      lbDateValiditeValeur.setVisible(true);
      lbDateValidite.setVisible(true);
    }
    else {
      lbDateValiditeValeur.setText("");
      lbDateValiditeValeur.setVisible(true);
      lbDateValidite.setVisible(true);
    }
  }
  
  private void rafraichirDocumentAchatLie() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getIdDocumentAchatLie() != null) {
      lbBonAchatLieValeur.setText(documentVente.getIdDocumentAchatLie().toString());
      lbBonAchatLie.setVisible(true);
    }
    else {
      lbBonAchatLieValeur.setText("");
      lbBonAchatLie.setVisible(false);
    }
  }
  
  private void rafraichirInformationLivraisonEnlevement() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && !documentVente.getTexteLivraisonEnlevement().isEmpty()) {
      String texteLivraisonEnlevement = documentVente.getTexteLivraisonEnlevement();
      taInformationLivraisonEnlevement.setText(texteLivraisonEnlevement);
      taInformationLivraisonEnlevement.setCaretPosition(0);
      taInformationLivraisonEnlevement.setVisible(true);
      scpInformationLivraisonEnlevement.setVisible(true);
      lbInformationLivraisonEnlevement.setVisible(true);
    }
    else {
      taInformationLivraisonEnlevement.setText("");
      taInformationLivraisonEnlevement.setVisible(false);
      scpInformationLivraisonEnlevement.setVisible(false);
      lbInformationLivraisonEnlevement.setVisible(false);
    }
  }
  
  private void rafraichirCiviliteFacturation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    Client client = getModele().getClient();
    if (client != null && client.isParticulier() && documentVente != null && documentVente.getAdresseFacturation() != null
        && documentVente.getAdresseFacturation().getIdCivilite() != null) {
      tfCiviliteFacturation.setText(documentVente.getAdresseFacturation().getIdCivilite().getCode());
      tfCiviliteFacturation.setVisible(true);
    }
    else {
      tfCiviliteFacturation.setText("");
      tfCiviliteFacturation.setVisible(false);
    }
  }
  
  private void rafraichirNomFacturation() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNomFacturation.setText("");
      tfNomFacturation.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      DocumentVente documentVente = getModele().getDocumentVente();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        tfNomFacturation.setText(contact.getNom());
        tfNomFacturation.setVisible(true);
      }
      else if (documentVente != null && documentVente.getAdresseFacturation() != null
          && documentVente.getAdresseFacturation().getNom() != null) {
        tfNomFacturation.setText(documentVente.getAdresseFacturation().getNom());
        tfNomFacturation.setVisible(true);
      }
      else {
        tfNomFacturation.setText("");
        tfNomFacturation.setVisible(false);
      }
    }
    
  }
  
  /**
   * Rafraîhir l'adresse de facturation.
   */
  private void rafraichirComplementNomFacturation() {
    Client client = getModele().getClient();
    if (client == null) {
      tfComplementNomFacturation.setText("");
      tfComplementNomFacturation.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      DocumentVente documentVente = getModele().getDocumentVente();
      if (client.isParticulier() && contact != null && contact.getPrenom() != null) {
        tfComplementNomFacturation.setText(contact.getPrenom());
        tfComplementNomFacturation.setVisible(true);
      }
      else if (documentVente != null && documentVente.getAdresseFacturation() != null
          && documentVente.getAdresseFacturation().getComplementNom() != null) {
        tfComplementNomFacturation.setText(documentVente.getAdresseFacturation().getComplementNom());
        tfComplementNomFacturation.setVisible(true);
      }
      else {
        tfComplementNomFacturation.setText("");
        tfComplementNomFacturation.setVisible(false);
      }
    }
  }
  
  private void rafraichirRueFacturation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getAdresseFacturation() != null
        && documentVente.getAdresseFacturation().getRue() != null) {
      tfRueFacturation.setText(documentVente.getAdresseFacturation().getRue());
      tfRueFacturation.setVisible(true);
    }
    else {
      tfRueFacturation.setText("");
      tfRueFacturation.setVisible(false);
    }
  }
  
  private void rafraichirLocalisationFacturation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getAdresseFacturation() != null
        && documentVente.getAdresseFacturation().getLocalisation() != null) {
      tfLocalisationFacturation.setText(documentVente.getAdresseFacturation().getLocalisation());
      tfLocalisationFacturation.setVisible(true);
    }
    else {
      tfLocalisationFacturation.setText("");
      tfLocalisationFacturation.setVisible(false);
    }
  }
  
  private void rafraichirCodePostalFacturation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getAdresseFacturation() != null
        && documentVente.getAdresseFacturation().getCodePostalFormate() != null) {
      tfCodePostalFacturation.setText(documentVente.getAdresseFacturation().getCodePostalFormate());
      tfCodePostalFacturation.setVisible(true);
    }
    else {
      tfCodePostalFacturation.setText("");
      tfCodePostalFacturation.setVisible(false);
    }
  }
  
  private void rafraichirVilleFacturation() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getAdresseFacturation() != null
        && documentVente.getAdresseFacturation().getVille() != null) {
      tfVilleFacturation.setText(documentVente.getAdresseFacturation().getVille());
      tfVilleFacturation.setVisible(true);
    }
    else {
      tfVilleFacturation.setText("");
      tfVilleFacturation.setVisible(false);
    }
  }
  
  private void rafraichirCiviliteLivraison() {
    DocumentVente documentVente = getModele().getDocumentVente();
    Client client = getModele().getClient();
    if (client != null && client.isParticulier() && documentVente != null && documentVente.getTransport() != null
        && documentVente.getTransport().getAdresseLivraison() != null
        && documentVente.getTransport().getAdresseLivraison().getIdCivilite() != null) {
      tfCiviliteLivraison.setText(documentVente.getTransport().getAdresseLivraison().getIdCivilite().getCode());
      tfCiviliteLivraison.setVisible(true);
    }
    else {
      tfCiviliteLivraison.setText("");
      tfCiviliteLivraison.setVisible(false);
    }
  }
  
  private void rafraichirNomLivraison() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNomLivraison.setText("");
      tfNomLivraison.setVisible(false);
    }
    else {
      DocumentVente documentVente = getModele().getDocumentVente();
      if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
          && documentVente.getTransport().getAdresseLivraison().getNom() != null) {
        tfNomLivraison.setText(documentVente.getTransport().getAdresseLivraison().getNom());
        tfNomLivraison.setVisible(true);
      }
      else {
        tfNomLivraison.setText("");
        tfNomLivraison.setVisible(false);
      }
    }
  }
  
  /**
   * Rafraîhir l'adresse de Livraison.
   */
  private void rafraichirComplementNomLivraison() {
    Client client = getModele().getClient();
    if (client == null) {
      tfComplementNomLivraison.setText("");
      tfComplementNomLivraison.setVisible(false);
    }
    else {
      DocumentVente documentVente = getModele().getDocumentVente();
      if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
          && documentVente.getTransport().getAdresseLivraison().getComplementNom() != null) {
        
        tfComplementNomLivraison.setText(documentVente.getTransport().getAdresseLivraison().getComplementNom());
        tfComplementNomLivraison.setVisible(true);
      }
      else {
        tfComplementNomLivraison.setText("");
        tfComplementNomLivraison.setVisible(false);
      }
    }
  }
  
  private void rafraichirRueLivraison() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
        && documentVente.getTransport().getAdresseLivraison().getRue() != null) {
      tfRueLivraison.setText(documentVente.getTransport().getAdresseLivraison().getRue());
      tfRueLivraison.setVisible(true);
    }
    else {
      tfRueLivraison.setText("");
      tfRueLivraison.setVisible(false);
    }
  }
  
  private void rafraichirLocalisationLivraison() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
        && documentVente.getTransport().getAdresseLivraison().getLocalisation() != null) {
      tfLocalisationLivraison.setText(documentVente.getTransport().getAdresseLivraison().getLocalisation());
      tfLocalisationLivraison.setVisible(true);
    }
    else {
      tfLocalisationLivraison.setText("");
      tfLocalisationLivraison.setVisible(false);
    }
  }
  
  private void rafraichirCodePostalLivraison() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
        && documentVente.getTransport().getAdresseLivraison().getCodePostalFormate() != null) {
      tfCodePostalLivraison.setText(documentVente.getTransport().getAdresseLivraison().getCodePostalFormate());
      tfCodePostalLivraison.setVisible(true);
    }
    else {
      tfCodePostalLivraison.setText("");
      tfCodePostalLivraison.setVisible(false);
    }
  }
  
  private void rafraichirVilleLivraison() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null
        && documentVente.getTransport().getAdresseLivraison().getVille() != null) {
      tfVilleLivraison.setText(documentVente.getTransport().getAdresseLivraison().getVille());
      tfVilleLivraison.setVisible(true);
    }
    else {
      tfVilleLivraison.setText("");
      tfVilleLivraison.setVisible(false);
    }
  }
  
  private void rafraichirNumeroClientContact() {
    Client client = getModele().getClient();
    if (client != null) {
      tfNumeroClient.setText(client.getId().toString());
      tfNumeroClient.setVisible(true);
    }
    else {
      tfNumeroClient.setText("");
      tfNumeroClient.setVisible(false);
    }
  }
  
  private void rafraichirNomContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNomComplet.setText("");
      tfNomComplet.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNomComplet() != null) {
        tfNomComplet.setText(contact.getNomComplet());
        tfNomComplet.setVisible(true);
      }
      else {
        tfNomComplet.setText("");
        tfNomComplet.setVisible(false);
      }
    }
  }
  
  private void rafraichirMailContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfEmail.setText("");
      tfEmail.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getEmail() != null) {
        tfEmail.setText(contact.getEmail());
        tfEmail.setVisible(true);
      }
      else {
        tfEmail.setText("");
        tfEmail.setVisible(false);
      }
    }
  }
  
  private void rafraichirTelephoneContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfTelephone.setText("");
      tfTelephone.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroTelephone1() != null) {
        tfTelephone.setText(contact.getNumeroTelephone1());
        tfTelephone.setVisible(true);
      }
      else if (client.getNumeroTelephone() != null) {
        tfTelephone.setText(client.getNumeroTelephone());
        tfTelephone.setVisible(true);
      }
      else {
        tfTelephone.setText("");
        tfTelephone.setVisible(false);
      }
    }
  }
  
  private void rafraichirFaxContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfFax.setText("");
      tfFax.setVisible(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroFax() != null) {
        tfFax.setText(contact.getNumeroFax());
        tfFax.setVisible(true);
      }
      else if (client.getNumeroFax() != null) {
        tfFax.setText(client.getNumeroFax());
        tfFax.setVisible(true);
      }
      else {
        tfFax.setText("");
        tfFax.setVisible(false);
      }
    }
  }
  
  private void rafraichirPoidsTotal() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getPoidsTotalKg() != null) {
      lbPoidsTotalValeur.setText(Constantes.formater(documentVente.getPoidsTotalKg(), false) + " kg");
    }
    else {
      lbPoidsTotalValeur.setText("");
    }
  }
  
  private void rafraichirPourcentageMargeTotale() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getPourcentageMargeTotale() != null) {
      lbMargeTotaleValeur.setText(Constantes.formater(documentVente.getPourcentageMargeTotale(), true) + " %");
    }
    else {
      lbMargeTotaleValeur.setText("");
    }
  }
  
  private void rafraichirTotalHT() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTotalHT() != null) {
      lbTotalHTValeur.setText(Constantes.formater(documentVente.getTotalHT(), true));
    }
    else {
      lbTotalHTValeur.setText("");
    }
  }
  
  private void rafraichirTotalTVA() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTotalTVA() != null) {
      lbTotalTVAValeur.setText(Constantes.formater(documentVente.getTotalTVA(), true));
    }
    else {
      lbTotalTVAValeur.setText("");
    }
  }
  
  private void rafraichirTotalTTC() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getTotalTTC() != null) {
      lbTotalTTCValeur.setText(Constantes.formater(documentVente.getTotalTTC(), true));
    }
    else {
      lbTotalTTCValeur.setText("");
    }
  }
  
  private void rafraichirMessageReglement() {
    lbMessageReglement.setText(getModele().genererMessageReglement());
  }
  
  private void rafraichirListeStandard() {
    // Effacer la table
    tblLignesArticlesDocument.clearSelection();
    tableModelStandard.setRowCount(0);
    
    // Remplacer le modèle de la table si besoin
    if (!tblLignesArticlesDocument.getModel().equals(tableModelStandard)) {
      tblLignesArticlesDocument.setModel(tableModelStandard);
      tblLignesArticlesDocument.setGridColor(new Color(204, 204, 204));
      tblLignesArticlesDocument.setDefaultRenderer(Object.class, rendererStandard);
    }
    
    // Récupérer les lignes à afficher
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente == null || documentVente.getListeLigneVente() == null || documentVente.getListeLigneVente().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererStandard.redimensionnerColonnes(tblLignesArticlesDocument);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneVente listeLigneVente = documentVente.getListeLigneVente();
    String[][] donnees = new String[listeLigneVente.size()][TITRE_COLONNE_STANDARD.length];
    for (int ligne = 0; ligne < listeLigneVente.size(); ligne++) {
      LigneVente ligneVente = listeLigneVente.get(ligne);
      if (ligneVente == null) {
        continue;
      }
      
      if (ligneVente.isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ligneVente.getLibelle();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
      }
      else {
        donnees[ligne][0] = Constantes.formater(ligneVente.getQuantiteUCV(), false);
        if (ligneVente.getIdUniteConditionnementVente() != null) {
          donnees[ligne][1] = ligneVente.getIdUniteConditionnementVente().getCode();
        }
        donnees[ligne][2] = ligneVente.getLibelle();
        donnees[ligne][3] = getModele().retournerEtatArticle(ligneVente);
        donnees[ligne][4] = Constantes.formater(ligneVente.getPrixNetCalcule(), true);
        donnees[ligne][5] = Constantes.formater(ligneVente.getQuantiteUV(), false);
        if (ligneVente.getIdUniteVente() != null) {
          donnees[ligne][6] = ligneVente.getIdUniteVente().getCode();
        }
        donnees[ligne][7] = Constantes.formater(ligneVente.getMontantHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelStandard.addRow(ligne);
    }
  }
  
  private void rafraichirListeAvoir() {
    ListeDocumentVenteBase listeDocumentAvoir = getModele().getListeDocumentAvoir();
    if (listeDocumentAvoir == null) {
      pnlListeRemboursement.setVisible(false);
      return;
    }
    pnlListeRemboursement.setVisible(true);
    String[][] donnees = null;
    donnees = new String[listeDocumentAvoir.size()][TITRE_LISTE_REMBOURSEMENT.length];
    for (int ligne = 0; ligne < listeDocumentAvoir.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeDocumentAvoir.get(ligne);
      if (documentVenteBase == null) {
        continue;
      }
      // Identifiant du document de vente
      String numeroDocument = "0";
      if (documentVenteBase.getId() != null && documentVenteBase.getId().getNumeroFacture() != null) {
        numeroDocument = Constantes.convertirIntegerEnTexte(documentVenteBase.getId().getNumeroFacture(), 0);
      }
      else {
        numeroDocument = documentVenteBase.getId().toString();
      }
      donnees[ligne][0] = numeroDocument;
      
      // Montant du remboursement
      donnees[ligne][1] = Constantes.formater(documentVenteBase.getTotalHT(), true);
      
      // Référence longue
      donnees[ligne][2] = documentVenteBase.getReferenceLongue();
    }
    tblListeRemboursement.mettreAJourDonnees(donnees);
  }
  
  private void rafraichirListeDetail() {
    // Effacer la table
    tblLignesArticlesDocument.clearSelection();
    tableModelDetail.setRowCount(0);
    
    // Remplacer le modèle de la table
    if (!tblLignesArticlesDocument.getModel().equals(tableModelDetail)) {
      tblLignesArticlesDocument.setModel(tableModelDetail);
      tblLignesArticlesDocument.setGridColor(new Color(204, 204, 204));
      tblLignesArticlesDocument.setDefaultRenderer(Object.class, rendererDetail);
    }
    
    // Récupérer les lignes à afficher
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente == null || documentVente.getListeLigneVente() == null || documentVente.getListeLigneVente().isEmpty()) {
      // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
      rendererDetail.redimensionnerColonnes(tblLignesArticlesDocument);
      return;
    }
    
    // Générer les données pour le tableau
    ListeLigneVente listeLigneVente = documentVente.getListeLigneVente();
    String[][] donnees = new String[listeLigneVente.size()][TITRE_COLONNE_DETAIL.length];
    for (int ligne = 0; ligne < listeLigneVente.size(); ligne++) {
      LigneVente ligneVente = listeLigneVente.get(ligne);
      if (ligneVente == null) {
        continue;
      }
      
      if (ligneVente.isLigneCommentaire()) {
        donnees[ligne][0] = "*";
        donnees[ligne][1] = "";
        donnees[ligne][2] = ligneVente.getLibelle();
        donnees[ligne][3] = "";
        donnees[ligne][4] = "";
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
        donnees[ligne][7] = "";
        donnees[ligne][8] = "";
        donnees[ligne][9] = "";
        donnees[ligne][10] = "";
        donnees[ligne][11] = "";
      }
      else {
        // Quantité UC
        donnees[ligne][0] = Constantes.formater(ligneVente.getQuantiteUCV(), false);
        // UC
        donnees[ligne][1] = ligneVente.getIdUniteConditionnementVente().getCode();
        // Libellé article
        donnees[ligne][2] = ligneVente.getLibelle();
        // Prix de base
        donnees[ligne][3] = Constantes.formater(ligneVente.getPrixBaseHT(), true);
        // Remise
        donnees[ligne][4] = Constantes.formater(ligneVente.getTauxRemise1(), true) + "%";
        
        // Prix net
        donnees[ligne][5] = Constantes.formater(ligneVente.getPrixNetHT(), true);
        // Quantité UV
        donnees[ligne][6] = Constantes.formater(ligneVente.getQuantiteUV(), false);
        // UV
        if (ligneVente.getIdUniteVente() != null) {
          donnees[ligne][7] = ligneVente.getIdUniteVente().getCode();
        }
        // Coefficient
        donnees[ligne][8] = Constantes.formater(ligneVente.calculerIndiceDeMarge(Constantes.DEUX_DECIMALES), true);
        // Marge
        donnees[ligne][9] = Constantes.formater(ligneVente.calculerTauxDeMarque(Constantes.DEUX_DECIMALES), true) + "%";
        // PRV
        donnees[ligne][10] = Constantes.formater(ligneVente.getPrixDeRevientLigneHT(), true);
        // Montant HT
        donnees[ligne][11] = Constantes.formater(ligneVente.getMontantHT(), true);
      }
    }
    
    // Mettre à jour la table
    for (String[] ligne : donnees) {
      tableModelDetail.addRow(ligne);
    }
  }
  
  private void rafraichirBoutonAfficherLigne() {
    boolean actif = tblLignesArticlesDocument.getSelectedRow() > -1;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_LIGNE, actif);
  }
  
  private void rafraichirBoutonAfficherPrix() {
    boolean actif = false;
    if (getModele().getModeAffichageTable() == ModeleConsultationDocumentVente.MODE_AFFICHAGE_TABLE_STANDARD) {
      actif = true;
    }
    snBarreBouton.activerBouton(BOUTON_AFFICHER_PRIX, actif);
  }
  
  private void rafraichirBoutonMasquerPrix() {
    boolean actif = true;
    if (getModele().getModeAffichageTable() == ModeleConsultationDocumentVente.MODE_AFFICHAGE_TABLE_STANDARD) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_MASQUER_PRIX, actif);
  }
  
  private void rafraichirBoutonAfficherDocument() {
    boolean actif = true;
    if (getModele().getIdDocumentVenteSelectionne() == null) {
      actif = false;
    }
    else {
      boolean isFacture = getModele().getIdDocumentVenteSelectionne().isIdFacture();
      if (!isFacture) {
        actif = false;
      }
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT, actif);
  }
  
  private void rafraichirBoutonNavigation() {
    
    if (getModele().getIndexLigneSelectionne() < 0) {
      snBarreBouton.setCompteurCourant(getModele().getIndexDocumentSelectionne() + 1);
      if (getModele().getListeIdDocumentVente() != null) {
        snBarreBouton.setCompteurMax(getModele().getListeIdDocumentVente().size());
      }
    }
    else {
      snBarreBouton.setCompteurCourant(getModele().getIndexLigneSelectionne() + 1);
      if (getModele().getListeIdLigneVente() != null) {
        snBarreBouton.setCompteurMax(getModele().getListeIdLigneVente().size());
      }
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.NAVIGATION_PRECEDENT)) {
        getModele().afficherDocumentPrecedent();
      }
      else if (pSNBouton.isBouton(EnumBouton.NAVIGATION_SUIVANT)) {
        getModele().afficherDocumentSuivant();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_LIGNE)) {
        getModele().afficherDetailCompletLigne();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_PRIX)) {
        getModele().permuterAffichageTable();
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_PRIX)) {
        getModele().permuterAffichageTable();
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        getModele().valider();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENT)) {
        getModele().afficherDocumentStocke();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLignesArticlesDocumentMouseClicked(MouseEvent e) {
    try {
      rafraichirBoutonAfficherLigne();
      rafraichirBoutonAfficherPrix();
      rafraichirBoutonMasquerPrix();
      DocumentVente documentVente = getModele().getDocumentVente();
      int indexvisuel = tblLignesArticlesDocument.getSelectedRow();
      
      if (indexvisuel >= 0) {
        int indexreel = indexvisuel;
        if (tblLignesArticlesDocument.getRowSorter() != null) {
          indexreel = tblLignesArticlesDocument.getRowSorter().convertRowIndexToModel(indexvisuel);
        }
        getModele().setLigneSelectionnee(documentVente.getListeLigneVente().get(indexreel));
      }
      if (e.getClickCount() == 2) {
        getModele().afficherDetailCompletLigne();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlInfosDocument = new JPanel();
    lbDate = new JLabel();
    calDateValeur = new JLabel();
    lbMagasin = new JLabel();
    lbMagasinValeur = new JLabel();
    lbRepresentant = new JLabel();
    lbRepresentantValeur = new JLabel();
    lbVendeur = new JLabel();
    lbVendeurValeur = new JLabel();
    lbReferenceLongue = new JLabel();
    lbReferenceLongueValeur = new JLabel();
    lbReferenceCourte = new JLabel();
    lbReferenceCourteValeur = new JLabel();
    lbReferenceChantier = new JLabel();
    lbReferenceChantierValeur = new JLabel();
    lbInformationLivraisonEnlevement = new JLabel();
    scpInformationLivraisonEnlevement = new JScrollPane();
    taInformationLivraisonEnlevement = new JTextArea();
    lbDateLivraison = new JLabel();
    lbDateLivraisonValeur = new JLabel();
    lbNumeroFacture = new JLabel();
    lbNumeroFactureValeur = new JLabel();
    lbDateValidite = new JLabel();
    lbDateValiditeValeur = new JLabel();
    lbBonAchatLie = new JLabel();
    lbBonAchatLieValeur = new JLabel();
    pnlCoordonneesClientFacture = new JPanel();
    tfCiviliteFacturation = new JLabel();
    tfNomFacturation = new JLabel();
    tfComplementNomFacturation = new JLabel();
    tfRueFacturation = new JLabel();
    tfLocalisationFacturation = new JLabel();
    tfVilleFacturation = new JLabel();
    tfCodePostalFacturation = new JLabel();
    pnlCoordonneesClientLivre = new JPanel();
    tfCiviliteLivraison = new JLabel();
    tfNomLivraison = new JLabel();
    tfComplementNomLivraison = new JLabel();
    tfRueLivraison = new JLabel();
    tfLocalisationLivraison = new JLabel();
    tfVilleLivraison = new JLabel();
    tfCodePostalLivraison = new JLabel();
    pnlContact = new JPanel();
    lbNumeroClient = new JLabel();
    tfNumeroClient = new JLabel();
    lbContact = new JLabel();
    tfNomComplet = new JLabel();
    lbMail = new JLabel();
    tfEmail = new JLabel();
    lbTelephone = new JLabel();
    tfTelephone = new JLabel();
    lbFax = new JLabel();
    tfFax = new JLabel();
    pnlTitreListe = new JPanel();
    lbTitre = new JLabel();
    scpListeDocuments = new JScrollPane();
    tblLignesArticlesDocument = new NRiTable();
    pnlPied = new JPanel();
    pnlListeRemboursement = new JPanel();
    lbTitreRemboursement = new SNLabelTitre();
    scpListeRemboursement = new JScrollPane();
    tblListeRemboursement = new NRiTable();
    pnlInfosDiverses = new JPanel();
    lbPoidsTotal = new SNLabelChamp();
    lbPoidsTotalValeur = new SNLabelChamp();
    lbMargeTotale = new SNLabelChamp();
    lbMargeTotaleValeur = new SNLabelChamp();
    pnlTotaux = new JPanel();
    lbTotalHT = new SNLabelChamp();
    lbTotalHTValeur = new SNLabelChamp();
    lbTotalTVA = new SNLabelChamp();
    lbTotalTVAValeur = new SNLabelChamp();
    lbTotalTTC = new SNLabelChamp();
    lbTotalTTCValeur = new SNLabelChamp();
    lbMessageReglement = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setBackground(Color.white);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(Color.white);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 590, 85, 215, 310, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 160, 131, 33, 275, 35, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlInfosDocument ========
      {
        pnlInfosDocument.setOpaque(false);
        pnlInfosDocument.setMaximumSize(new Dimension(480, 270));
        pnlInfosDocument.setMinimumSize(new Dimension(480, 270));
        pnlInfosDocument.setPreferredSize(new Dimension(480, 270));
        pnlInfosDocument.setName("pnlInfosDocument");
        pnlInfosDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWidths = new int[] { 165, 347, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowHeights = new int[] { 23, 26, 20, 20, 20, 21, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlInfosDocument.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDate ----
        lbDate.setText("Date de cr\u00e9ation :");
        lbDate.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDate.setFont(new Font("Arial", Font.BOLD, 12));
        lbDate.setName("lbDate");
        pnlInfosDocument.add(lbDate, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 5), 0, 0));
        
        // ---- calDateValeur ----
        calDateValeur.setBackground(new Color(153, 153, 153));
        calDateValeur.setBorder(null);
        calDateValeur.setFont(new Font("Arial", Font.BOLD, 12));
        calDateValeur.setPreferredSize(new Dimension(110, 15));
        calDateValeur.setMinimumSize(new Dimension(110, 15));
        calDateValeur.setMaximumSize(new Dimension(110, 15));
        calDateValeur.setRequestFocusEnabled(false);
        calDateValeur.setFocusable(false);
        calDateValeur.setVerifyInputWhenFocusTarget(false);
        calDateValeur.setAlignmentX(0.5F);
        calDateValeur.setName("calDateValeur");
        pnlInfosDocument.add(calDateValeur, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin :");
        lbMagasin.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMagasin.setFont(new Font("Arial", Font.BOLD, 12));
        lbMagasin.setName("lbMagasin");
        pnlInfosDocument.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
            new Insets(10, 0, 5, 5), 0, 0));
        
        // ---- lbMagasinValeur ----
        lbMagasinValeur.setBackground(new Color(234, 234, 218));
        lbMagasinValeur.setBorder(null);
        lbMagasinValeur.setIconTextGap(6);
        lbMagasinValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbMagasinValeur.setPreferredSize(new Dimension(325, 15));
        lbMagasinValeur.setMinimumSize(new Dimension(325, 15));
        lbMagasinValeur.setAlignmentX(1.0F);
        lbMagasinValeur.setName("lbMagasinValeur");
        pnlInfosDocument.add(lbMagasinValeur, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.NONE, new Insets(10, 0, 5, 0), 0, 0));
        
        // ---- lbRepresentant ----
        lbRepresentant.setText("Repr\u00e9sentant :");
        lbRepresentant.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRepresentant.setFont(new Font("Arial", Font.BOLD, 12));
        lbRepresentant.setName("lbRepresentant");
        pnlInfosDocument.add(lbRepresentant,
            new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbRepresentantValeur ----
        lbRepresentantValeur.setBackground(new Color(234, 234, 218));
        lbRepresentantValeur.setBorder(null);
        lbRepresentantValeur.setIconTextGap(6);
        lbRepresentantValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbRepresentantValeur.setPreferredSize(new Dimension(40, 15));
        lbRepresentantValeur.setMinimumSize(new Dimension(40, 15));
        lbRepresentantValeur.setName("lbRepresentantValeur");
        pnlInfosDocument.add(lbRepresentantValeur, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbVendeur ----
        lbVendeur.setText("Vendeur :");
        lbVendeur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbVendeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbVendeur.setName("lbVendeur");
        pnlInfosDocument.add(lbVendeur, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbVendeurValeur ----
        lbVendeurValeur.setBackground(new Color(234, 234, 218));
        lbVendeurValeur.setBorder(null);
        lbVendeurValeur.setIconTextGap(6);
        lbVendeurValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbVendeurValeur.setPreferredSize(new Dimension(40, 15));
        lbVendeurValeur.setMinimumSize(new Dimension(40, 15));
        lbVendeurValeur.setName("lbVendeurValeur");
        pnlInfosDocument.add(lbVendeurValeur, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceLongue ----
        lbReferenceLongue.setText("R\u00e9f\u00e9rence longue :");
        lbReferenceLongue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceLongue.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceLongue.setMinimumSize(new Dimension(145, 19));
        lbReferenceLongue.setName("lbReferenceLongue");
        pnlInfosDocument.add(lbReferenceLongue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceLongueValeur ----
        lbReferenceLongueValeur.setBackground(new Color(234, 234, 218));
        lbReferenceLongueValeur.setBorder(null);
        lbReferenceLongueValeur.setIconTextGap(6);
        lbReferenceLongueValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceLongueValeur.setPreferredSize(new Dimension(300, 15));
        lbReferenceLongueValeur.setMinimumSize(new Dimension(300, 15));
        lbReferenceLongueValeur.setName("lbReferenceLongueValeur");
        pnlInfosDocument.add(lbReferenceLongueValeur, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceCourte ----
        lbReferenceCourte.setText("R\u00e9f\u00e9rence courte :");
        lbReferenceCourte.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceCourte.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceCourte.setName("lbReferenceCourte");
        pnlInfosDocument.add(lbReferenceCourte, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceCourteValeur ----
        lbReferenceCourteValeur.setBackground(new Color(234, 234, 218));
        lbReferenceCourteValeur.setBorder(null);
        lbReferenceCourteValeur.setIconTextGap(6);
        lbReferenceCourteValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceCourteValeur.setPreferredSize(new Dimension(130, 15));
        lbReferenceCourteValeur.setMinimumSize(new Dimension(130, 15));
        lbReferenceCourteValeur.setName("lbReferenceCourteValeur");
        pnlInfosDocument.add(lbReferenceCourteValeur, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceChantier ----
        lbReferenceChantier.setText("R\u00e9f\u00e9rence chantier :");
        lbReferenceChantier.setHorizontalAlignment(SwingConstants.RIGHT);
        lbReferenceChantier.setFont(new Font("Arial", Font.BOLD, 12));
        lbReferenceChantier.setName("lbReferenceChantier");
        pnlInfosDocument.add(lbReferenceChantier, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbReferenceChantierValeur ----
        lbReferenceChantierValeur.setBackground(new Color(234, 234, 218));
        lbReferenceChantierValeur.setBorder(null);
        lbReferenceChantierValeur.setIconTextGap(6);
        lbReferenceChantierValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbReferenceChantierValeur.setPreferredSize(new Dimension(300, 15));
        lbReferenceChantierValeur.setMinimumSize(new Dimension(300, 15));
        lbReferenceChantierValeur.setName("lbReferenceChantierValeur");
        pnlInfosDocument.add(lbReferenceChantierValeur, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbInformationLivraisonEnlevement ----
        lbInformationLivraisonEnlevement.setText("Observations :");
        lbInformationLivraisonEnlevement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbInformationLivraisonEnlevement.setFont(new Font("Arial", Font.BOLD, 12));
        lbInformationLivraisonEnlevement.setName("lbInformationLivraisonEnlevement");
        pnlInfosDocument.add(lbInformationLivraisonEnlevement, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== scpInformationLivraisonEnlevement ========
        {
          scpInformationLivraisonEnlevement.setMaximumSize(new Dimension(200, 50));
          scpInformationLivraisonEnlevement.setMinimumSize(new Dimension(200, 50));
          scpInformationLivraisonEnlevement.setPreferredSize(new Dimension(200, 50));
          scpInformationLivraisonEnlevement.setViewportBorder(null);
          scpInformationLivraisonEnlevement.setFont(new Font("Arial", Font.PLAIN, 12));
          scpInformationLivraisonEnlevement.setBackground(Color.white);
          scpInformationLivraisonEnlevement.setBorder(new LineBorder(new Color(225, 225, 225)));
          scpInformationLivraisonEnlevement.setName("scpInformationLivraisonEnlevement");
          
          // ---- taInformationLivraisonEnlevement ----
          taInformationLivraisonEnlevement.setBorder(null);
          taInformationLivraisonEnlevement.setMinimumSize(new Dimension(200, 840));
          taInformationLivraisonEnlevement.setPreferredSize(new Dimension(200, 840));
          taInformationLivraisonEnlevement.setFont(new Font("Arial", Font.PLAIN, 12));
          taInformationLivraisonEnlevement.setMaximumSize(new Dimension(200, 840));
          taInformationLivraisonEnlevement.setEditable(false);
          taInformationLivraisonEnlevement.setWrapStyleWord(true);
          taInformationLivraisonEnlevement.setLineWrap(true);
          taInformationLivraisonEnlevement.setBackground(Color.white);
          taInformationLivraisonEnlevement.setName("taInformationLivraisonEnlevement");
          scpInformationLivraisonEnlevement.setViewportView(taInformationLivraisonEnlevement);
        }
        pnlInfosDocument.add(scpInformationLivraisonEnlevement, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateLivraison ----
        lbDateLivraison.setText("Date de livraison :");
        lbDateLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDateLivraison.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateLivraison.setName("lbDateLivraison");
        pnlInfosDocument.add(lbDateLivraison, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateLivraisonValeur ----
        lbDateLivraisonValeur.setBackground(new Color(153, 153, 153));
        lbDateLivraisonValeur.setBorder(null);
        lbDateLivraisonValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateLivraisonValeur.setPreferredSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setMinimumSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setMaximumSize(new Dimension(110, 15));
        lbDateLivraisonValeur.setRequestFocusEnabled(false);
        lbDateLivraisonValeur.setFocusable(false);
        lbDateLivraisonValeur.setVerifyInputWhenFocusTarget(false);
        lbDateLivraisonValeur.setAlignmentX(0.5F);
        lbDateLivraisonValeur.setName("lbDateLivraisonValeur");
        pnlInfosDocument.add(lbDateLivraisonValeur, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroFacture ----
        lbNumeroFacture.setText("Facture :");
        lbNumeroFacture.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroFacture.setFont(new Font("Arial", Font.BOLD, 12));
        lbNumeroFacture.setName("lbNumeroFacture");
        pnlInfosDocument.add(lbNumeroFacture, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbNumeroFactureValeur ----
        lbNumeroFactureValeur.setBackground(new Color(234, 234, 218));
        lbNumeroFactureValeur.setBorder(null);
        lbNumeroFactureValeur.setIconTextGap(6);
        lbNumeroFactureValeur.setFont(new Font("Arial", Font.PLAIN, 12));
        lbNumeroFactureValeur.setHorizontalAlignment(SwingConstants.LEFT);
        lbNumeroFactureValeur.setPreferredSize(new Dimension(70, 15));
        lbNumeroFactureValeur.setMinimumSize(new Dimension(70, 15));
        lbNumeroFactureValeur.setName("lbNumeroFactureValeur");
        pnlInfosDocument.add(lbNumeroFactureValeur, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateValidite ----
        lbDateValidite.setText("Date de validit\u00e9 :");
        lbDateValidite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDateValidite.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateValidite.setName("lbDateValidite");
        pnlInfosDocument.add(lbDateValidite, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDateValiditeValeur ----
        lbDateValiditeValeur.setBackground(new Color(153, 153, 153));
        lbDateValiditeValeur.setBorder(null);
        lbDateValiditeValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbDateValiditeValeur.setPreferredSize(new Dimension(110, 15));
        lbDateValiditeValeur.setMinimumSize(new Dimension(110, 15));
        lbDateValiditeValeur.setMaximumSize(new Dimension(110, 15));
        lbDateValiditeValeur.setRequestFocusEnabled(false);
        lbDateValiditeValeur.setFocusable(false);
        lbDateValiditeValeur.setVerifyInputWhenFocusTarget(false);
        lbDateValiditeValeur.setAlignmentX(0.5F);
        lbDateValiditeValeur.setName("lbDateValiditeValeur");
        pnlInfosDocument.add(lbDateValiditeValeur, new GridBagConstraints(1, 10, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbBonAchatLie ----
        lbBonAchatLie.setText("Bon d'achat li\u00e9 :");
        lbBonAchatLie.setHorizontalAlignment(SwingConstants.RIGHT);
        lbBonAchatLie.setFont(new Font("Arial", Font.BOLD, 12));
        lbBonAchatLie.setName("lbBonAchatLie");
        pnlInfosDocument.add(lbBonAchatLie, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbBonAchatLieValeur ----
        lbBonAchatLieValeur.setBackground(new Color(153, 153, 153));
        lbBonAchatLieValeur.setBorder(null);
        lbBonAchatLieValeur.setFont(new Font("Arial", Font.BOLD, 12));
        lbBonAchatLieValeur.setPreferredSize(new Dimension(110, 15));
        lbBonAchatLieValeur.setMinimumSize(new Dimension(110, 15));
        lbBonAchatLieValeur.setMaximumSize(new Dimension(110, 15));
        lbBonAchatLieValeur.setRequestFocusEnabled(false);
        lbBonAchatLieValeur.setFocusable(false);
        lbBonAchatLieValeur.setVerifyInputWhenFocusTarget(false);
        lbBonAchatLieValeur.setAlignmentX(0.5F);
        lbBonAchatLieValeur.setName("lbBonAchatLieValeur");
        pnlInfosDocument.add(lbBonAchatLieValeur, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInfosDocument,
          new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlCoordonneesClientFacture ========
      {
        pnlCoordonneesClientFacture.setBorder(new TitledBorder(new LineBorder(Color.lightGray), "Adresse de facturation",
            TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
        pnlCoordonneesClientFacture.setOpaque(false);
        pnlCoordonneesClientFacture.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlCoordonneesClientFacture.setMinimumSize(new Dimension(310, 135));
        pnlCoordonneesClientFacture.setPreferredSize(new Dimension(310, 135));
        pnlCoordonneesClientFacture.setMaximumSize(new Dimension(310, 115));
        pnlCoordonneesClientFacture.setForeground(new Color(153, 153, 153));
        pnlCoordonneesClientFacture.setName("pnlCoordonneesClientFacture");
        pnlCoordonneesClientFacture.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClientFacture.getLayout()).columnWidths = new int[] { 85, 250, 0 };
        ((GridBagLayout) pnlCoordonneesClientFacture.getLayout()).rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClientFacture.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClientFacture.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- tfCiviliteFacturation ----
        tfCiviliteFacturation.setPreferredSize(new Dimension(300, 15));
        tfCiviliteFacturation.setMinimumSize(new Dimension(300, 15));
        tfCiviliteFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCiviliteFacturation.setBorder(null);
        tfCiviliteFacturation.setName("tfCiviliteFacturation");
        pnlCoordonneesClientFacture.add(tfCiviliteFacturation, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(5, 10, 5, 0), 0, 0));
        
        // ---- tfNomFacturation ----
        tfNomFacturation.setBackground(Color.white);
        tfNomFacturation.setFont(new Font("Arial", Font.BOLD, 12));
        tfNomFacturation.setMinimumSize(new Dimension(300, 15));
        tfNomFacturation.setPreferredSize(new Dimension(300, 12));
        tfNomFacturation.setBorder(null);
        tfNomFacturation.setName("tfNomFacturation");
        pnlCoordonneesClientFacture.add(tfNomFacturation, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfComplementNomFacturation ----
        tfComplementNomFacturation.setBackground(Color.white);
        tfComplementNomFacturation.setMinimumSize(new Dimension(300, 15));
        tfComplementNomFacturation.setPreferredSize(new Dimension(300, 12));
        tfComplementNomFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfComplementNomFacturation.setBorder(null);
        tfComplementNomFacturation.setName("tfComplementNomFacturation");
        pnlCoordonneesClientFacture.add(tfComplementNomFacturation, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfRueFacturation ----
        tfRueFacturation.setBackground(Color.white);
        tfRueFacturation.setMinimumSize(new Dimension(300, 15));
        tfRueFacturation.setPreferredSize(new Dimension(300, 12));
        tfRueFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfRueFacturation.setBorder(null);
        tfRueFacturation.setName("tfRueFacturation");
        pnlCoordonneesClientFacture.add(tfRueFacturation, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfLocalisationFacturation ----
        tfLocalisationFacturation.setBackground(Color.white);
        tfLocalisationFacturation.setMinimumSize(new Dimension(300, 15));
        tfLocalisationFacturation.setPreferredSize(new Dimension(300, 12));
        tfLocalisationFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfLocalisationFacturation.setBorder(null);
        tfLocalisationFacturation.setName("tfLocalisationFacturation");
        pnlCoordonneesClientFacture.add(tfLocalisationFacturation, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfVilleFacturation ----
        tfVilleFacturation.setBackground(new Color(171, 148, 79));
        tfVilleFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfVilleFacturation.setMinimumSize(new Dimension(300, 12));
        tfVilleFacturation.setPreferredSize(new Dimension(300, 12));
        tfVilleFacturation.setBorder(null);
        tfVilleFacturation.setName("tfVilleFacturation");
        pnlCoordonneesClientFacture.add(tfVilleFacturation, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- tfCodePostalFacturation ----
        tfCodePostalFacturation.setBackground(Color.white);
        tfCodePostalFacturation.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCodePostalFacturation.setMinimumSize(new Dimension(60, 12));
        tfCodePostalFacturation.setPreferredSize(new Dimension(60, 12));
        tfCodePostalFacturation.setBorder(null);
        tfCodePostalFacturation.setName("tfCodePostalFacturation");
        pnlCoordonneesClientFacture.add(tfCodePostalFacturation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesClientFacture,
          new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlCoordonneesClientLivre ========
      {
        pnlCoordonneesClientLivre.setBorder(new TitledBorder(new LineBorder(Color.lightGray), "Adresse de livraison",
            TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Arial", Font.BOLD, 12)));
        pnlCoordonneesClientLivre.setOpaque(false);
        pnlCoordonneesClientLivre.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlCoordonneesClientLivre.setMinimumSize(new Dimension(310, 135));
        pnlCoordonneesClientLivre.setPreferredSize(new Dimension(310, 135));
        pnlCoordonneesClientLivre.setMaximumSize(new Dimension(310, 115));
        pnlCoordonneesClientLivre.setForeground(new Color(153, 153, 153));
        pnlCoordonneesClientLivre.setName("pnlCoordonneesClientLivre");
        pnlCoordonneesClientLivre.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).columnWidths = new int[] { 85, 250, 0 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).rowHeights = new int[] { 20, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClientLivre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- tfCiviliteLivraison ----
        tfCiviliteLivraison.setPreferredSize(new Dimension(300, 15));
        tfCiviliteLivraison.setMinimumSize(new Dimension(300, 15));
        tfCiviliteLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCiviliteLivraison.setBorder(null);
        tfCiviliteLivraison.setName("tfCiviliteLivraison");
        pnlCoordonneesClientLivre.add(tfCiviliteLivraison, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(5, 10, 5, 0), 0, 0));
        
        // ---- tfNomLivraison ----
        tfNomLivraison.setBackground(Color.white);
        tfNomLivraison.setFont(new Font("Arial", Font.BOLD, 12));
        tfNomLivraison.setMinimumSize(new Dimension(300, 15));
        tfNomLivraison.setPreferredSize(new Dimension(300, 12));
        tfNomLivraison.setBorder(null);
        tfNomLivraison.setName("tfNomLivraison");
        pnlCoordonneesClientLivre.add(tfNomLivraison, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfComplementNomLivraison ----
        tfComplementNomLivraison.setBackground(Color.white);
        tfComplementNomLivraison.setMinimumSize(new Dimension(300, 15));
        tfComplementNomLivraison.setPreferredSize(new Dimension(300, 12));
        tfComplementNomLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfComplementNomLivraison.setBorder(null);
        tfComplementNomLivraison.setName("tfComplementNomLivraison");
        pnlCoordonneesClientLivre.add(tfComplementNomLivraison, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfRueLivraison ----
        tfRueLivraison.setBackground(Color.white);
        tfRueLivraison.setMinimumSize(new Dimension(300, 15));
        tfRueLivraison.setPreferredSize(new Dimension(300, 12));
        tfRueLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfRueLivraison.setBorder(null);
        tfRueLivraison.setName("tfRueLivraison");
        pnlCoordonneesClientLivre.add(tfRueLivraison, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfLocalisationLivraison ----
        tfLocalisationLivraison.setBackground(Color.white);
        tfLocalisationLivraison.setMinimumSize(new Dimension(300, 15));
        tfLocalisationLivraison.setPreferredSize(new Dimension(300, 12));
        tfLocalisationLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfLocalisationLivraison.setBorder(null);
        tfLocalisationLivraison.setName("tfLocalisationLivraison");
        pnlCoordonneesClientLivre.add(tfLocalisationLivraison, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 5, 0), 0, 0));
        
        // ---- tfVilleLivraison ----
        tfVilleLivraison.setBackground(new Color(171, 148, 79));
        tfVilleLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfVilleLivraison.setMinimumSize(new Dimension(300, 12));
        tfVilleLivraison.setPreferredSize(new Dimension(300, 12));
        tfVilleLivraison.setBorder(null);
        tfVilleLivraison.setName("tfVilleLivraison");
        pnlCoordonneesClientLivre.add(tfVilleLivraison, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- tfCodePostalLivraison ----
        tfCodePostalLivraison.setBackground(Color.white);
        tfCodePostalLivraison.setFont(new Font("Arial", Font.PLAIN, 12));
        tfCodePostalLivraison.setMinimumSize(new Dimension(60, 12));
        tfCodePostalLivraison.setPreferredSize(new Dimension(60, 12));
        tfCodePostalLivraison.setBorder(null);
        tfCodePostalLivraison.setName("tfCodePostalLivraison");
        pnlCoordonneesClientLivre.add(tfCodePostalLivraison, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 10, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesClientLivre, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlContact ========
      {
        pnlContact.setBorder(new TitledBorder(new LineBorder(Color.white), "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("Arial", Font.BOLD, 12)));
        pnlContact.setOpaque(false);
        pnlContact.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlContact.setMinimumSize(new Dimension(310, 110));
        pnlContact.setPreferredSize(new Dimension(310, 110));
        pnlContact.setMaximumSize(new Dimension(310, 110));
        pnlContact.setForeground(new Color(153, 153, 153));
        pnlContact.setBackground(new Color(204, 204, 204));
        pnlContact.setName("pnlContact");
        pnlContact.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContact.getLayout()).columnWidths = new int[] { 85, 250, 0 };
        ((GridBagLayout) pnlContact.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContact.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContact.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumeroClient ----
        lbNumeroClient.setText("Num\u00e9ro client :");
        lbNumeroClient.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroClient.setFont(new Font("Arial", Font.BOLD, 12));
        lbNumeroClient.setName("lbNumeroClient");
        pnlContact.add(lbNumeroClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 20, 5), 0, 0));
        
        // ---- tfNumeroClient ----
        tfNumeroClient.setBackground(new Color(234, 234, 218));
        tfNumeroClient.setBorder(null);
        tfNumeroClient.setIconTextGap(6);
        tfNumeroClient.setFont(new Font("Arial", Font.PLAIN, 12));
        tfNumeroClient.setPreferredSize(new Dimension(70, 15));
        tfNumeroClient.setMinimumSize(new Dimension(70, 15));
        tfNumeroClient.setAlignmentX(1.0F);
        tfNumeroClient.setMaximumSize(new Dimension(70, 15));
        tfNumeroClient.setName("tfNumeroClient");
        pnlContact.add(tfNumeroClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 20, 0), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact :");
        lbContact.setFont(new Font("Arial", Font.BOLD, 12));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setPreferredSize(new Dimension(70, 12));
        lbContact.setMinimumSize(new Dimension(70, 12));
        lbContact.setMaximumSize(new Dimension(70, 12));
        lbContact.setName("lbContact");
        pnlContact.add(lbContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNomComplet ----
        tfNomComplet.setBackground(Color.white);
        tfNomComplet.setFont(new Font("Arial", Font.PLAIN, 12));
        tfNomComplet.setMinimumSize(new Dimension(200, 12));
        tfNomComplet.setPreferredSize(new Dimension(200, 12));
        tfNomComplet.setBorder(null);
        tfNomComplet.setName("tfNomComplet");
        pnlContact.add(tfNomComplet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMail ----
        lbMail.setText("EMail :");
        lbMail.setFont(new Font("Arial", Font.BOLD, 12));
        lbMail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMail.setPreferredSize(new Dimension(70, 12));
        lbMail.setMinimumSize(new Dimension(70, 12));
        lbMail.setMaximumSize(new Dimension(70, 12));
        lbMail.setName("lbMail");
        pnlContact.add(lbMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("Arial", Font.PLAIN, 12));
        tfEmail.setMinimumSize(new Dimension(300, 15));
        tfEmail.setPreferredSize(new Dimension(300, 15));
        tfEmail.setBorder(null);
        tfEmail.setName("tfEmail");
        pnlContact.add(tfEmail, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone :");
        lbTelephone.setFont(new Font("Arial", Font.BOLD, 12));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setPreferredSize(new Dimension(70, 12));
        lbTelephone.setMinimumSize(new Dimension(70, 12));
        lbTelephone.setMaximumSize(new Dimension(70, 12));
        lbTelephone.setName("lbTelephone");
        pnlContact.add(lbTelephone, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 10, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("Arial", Font.PLAIN, 12));
        tfTelephone.setMinimumSize(new Dimension(100, 12));
        tfTelephone.setPreferredSize(new Dimension(100, 12));
        tfTelephone.setBorder(null);
        tfTelephone.setName("tfTelephone");
        pnlContact.add(tfTelephone, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax :");
        lbFax.setFont(new Font("Arial", Font.BOLD, 12));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlContact.add(lbFax, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 10, 0, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("Arial", Font.PLAIN, 12));
        tfFax.setPreferredSize(new Dimension(100, 20));
        tfFax.setMinimumSize(new Dimension(100, 20));
        tfFax.setBorder(null);
        tfFax.setName("tfFax");
        pnlContact.add(tfFax, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlContact, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTitreListe ========
      {
        pnlTitreListe.setOpaque(false);
        pnlTitreListe.setMaximumSize(new Dimension(341, 30));
        pnlTitreListe.setMinimumSize(new Dimension(341, 30));
        pnlTitreListe.setPreferredSize(new Dimension(341, 30));
        pnlTitreListe.setFont(new Font("Arial", Font.PLAIN, 11));
        pnlTitreListe.setName("pnlTitreListe");
        pnlTitreListe.setLayout(null);
        
        // ---- lbTitre ----
        lbTitre.setText("Lignes articles de ce document");
        lbTitre.setFont(new Font("sansserif", Font.BOLD, 14));
        lbTitre.setName("lbTitre");
        pnlTitreListe.add(lbTitre);
        lbTitre.setBounds(5, 5, 336, lbTitre.getPreferredSize().height);
        
        { // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < pnlTitreListe.getComponentCount(); i++) {
            Rectangle bounds = pnlTitreListe.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = pnlTitreListe.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          pnlTitreListe.setMinimumSize(preferredSize);
          pnlTitreListe.setPreferredSize(preferredSize);
        }
      }
      pnlContenu.add(pnlTitreListe, new GridBagConstraints(0, 2, 4, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeDocuments ========
      {
        scpListeDocuments.setPreferredSize(new Dimension(1050, 300));
        scpListeDocuments.setBackground(Color.white);
        scpListeDocuments.setMinimumSize(new Dimension(1050, 250));
        scpListeDocuments.setOpaque(true);
        scpListeDocuments.setName("scpListeDocuments");
        
        // ---- tblLignesArticlesDocument ----
        tblLignesArticlesDocument.setShowVerticalLines(true);
        tblLignesArticlesDocument.setShowHorizontalLines(true);
        tblLignesArticlesDocument.setBackground(Color.white);
        tblLignesArticlesDocument.setRowHeight(20);
        tblLignesArticlesDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblLignesArticlesDocument.setAutoCreateRowSorter(true);
        tblLignesArticlesDocument.setSelectionBackground(new Color(57, 105, 138));
        tblLignesArticlesDocument.setGridColor(new Color(204, 204, 204));
        tblLignesArticlesDocument.setName("tblLignesArticlesDocument");
        tblLignesArticlesDocument.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblLignesArticlesDocumentMouseClicked(e);
          }
        });
        scpListeDocuments.setViewportView(tblLignesArticlesDocument);
      }
      pnlContenu.add(scpListeDocuments, new GridBagConstraints(0, 3, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(5, 5, 10, 5), 0, 0));
      
      // ======== pnlPied ========
      {
        pnlPied.setBorder(new LineBorder(Color.white));
        pnlPied.setBackground(Color.white);
        pnlPied.setMaximumSize(new Dimension(1177, 30));
        pnlPied.setOpaque(false);
        pnlPied.setName("pnlPied");
        pnlPied.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPied.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlPied.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlPied.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlPied.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlListeRemboursement ========
        {
          pnlListeRemboursement.setPreferredSize(new Dimension(480, 458));
          pnlListeRemboursement.setMinimumSize(new Dimension(480, 59));
          pnlListeRemboursement.setOpaque(false);
          pnlListeRemboursement.setName("pnlListeRemboursement");
          pnlListeRemboursement.setLayout(new BorderLayout());
          
          // ---- lbTitreRemboursement ----
          lbTitreRemboursement.setText("Liste des retours et des avoirs");
          lbTitreRemboursement.setOpaque(true);
          lbTitreRemboursement.setName("lbTitreRemboursement");
          pnlListeRemboursement.add(lbTitreRemboursement, BorderLayout.NORTH);
          
          // ======== scpListeRemboursement ========
          {
            scpListeRemboursement.setName("scpListeRemboursement");
            
            // ---- tblListeRemboursement ----
            tblListeRemboursement.setOpaque(false);
            tblListeRemboursement.setRowSelectionAllowed(false);
            tblListeRemboursement.setName("tblListeRemboursement");
            scpListeRemboursement.setViewportView(tblListeRemboursement);
          }
          pnlListeRemboursement.add(scpListeRemboursement, BorderLayout.CENTER);
        }
        pnlPied.add(pnlListeRemboursement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 5, 0, 5), 0, 0));
        
        // ======== pnlInfosDiverses ========
        {
          pnlInfosDiverses.setOpaque(false);
          pnlInfosDiverses.setName("pnlInfosDiverses");
          pnlInfosDiverses.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfosDiverses.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPoidsTotal ----
          lbPoidsTotal.setText("Poids total :");
          lbPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPoidsTotal.setFont(new Font("Arial", Font.BOLD, 13));
          lbPoidsTotal.setMaximumSize(new Dimension(80, 30));
          lbPoidsTotal.setMinimumSize(new Dimension(80, 30));
          lbPoidsTotal.setPreferredSize(new Dimension(80, 30));
          lbPoidsTotal.setName("lbPoidsTotal");
          pnlInfosDiverses.add(lbPoidsTotal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPoidsTotalValeur ----
          lbPoidsTotalValeur.setBackground(new Color(234, 234, 218));
          lbPoidsTotalValeur.setBorder(null);
          lbPoidsTotalValeur.setIconTextGap(6);
          lbPoidsTotalValeur.setFont(new Font("Arial", Font.PLAIN, 14));
          lbPoidsTotalValeur.setHorizontalAlignment(SwingConstants.LEFT);
          lbPoidsTotalValeur.setPreferredSize(new Dimension(100, 30));
          lbPoidsTotalValeur.setMinimumSize(new Dimension(100, 30));
          lbPoidsTotalValeur.setHorizontalTextPosition(SwingConstants.LEADING);
          lbPoidsTotalValeur.setMaximumSize(new Dimension(100, 30));
          lbPoidsTotalValeur.setName("lbPoidsTotalValeur");
          pnlInfosDiverses.add(lbPoidsTotalValeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMargeTotale ----
          lbMargeTotale.setText("Marge totale :");
          lbMargeTotale.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMargeTotale.setFont(new Font("Arial", Font.BOLD, 13));
          lbMargeTotale.setPreferredSize(new Dimension(100, 30));
          lbMargeTotale.setMinimumSize(new Dimension(100, 30));
          lbMargeTotale.setMaximumSize(new Dimension(100, 30));
          lbMargeTotale.setName("lbMargeTotale");
          pnlInfosDiverses.add(lbMargeTotale, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMargeTotaleValeur ----
          lbMargeTotaleValeur.setBackground(new Color(234, 234, 218));
          lbMargeTotaleValeur.setBorder(null);
          lbMargeTotaleValeur.setIconTextGap(6);
          lbMargeTotaleValeur.setFont(new Font("Arial", Font.PLAIN, 14));
          lbMargeTotaleValeur.setHorizontalAlignment(SwingConstants.LEFT);
          lbMargeTotaleValeur.setPreferredSize(new Dimension(100, 30));
          lbMargeTotaleValeur.setMinimumSize(new Dimension(100, 30));
          lbMargeTotaleValeur.setHorizontalTextPosition(SwingConstants.LEADING);
          lbMargeTotaleValeur.setMaximumSize(new Dimension(100, 30));
          lbMargeTotaleValeur.setName("lbMargeTotaleValeur");
          pnlInfosDiverses.add(lbMargeTotaleValeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlPied.add(pnlInfosDiverses, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setOpaque(false);
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTotalHT ----
          lbTotalHT.setText("Total HT :");
          lbTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalHT.setFont(new Font("Arial", Font.BOLD, 13));
          lbTotalHT.setMaximumSize(new Dimension(80, 30));
          lbTotalHT.setMinimumSize(new Dimension(80, 30));
          lbTotalHT.setPreferredSize(new Dimension(80, 30));
          lbTotalHT.setName("lbTotalHT");
          pnlTotaux.add(lbTotalHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalHTValeur ----
          lbTotalHTValeur.setBackground(new Color(234, 234, 218));
          lbTotalHTValeur.setBorder(null);
          lbTotalHTValeur.setIconTextGap(6);
          lbTotalHTValeur.setFont(new Font("Arial", Font.PLAIN, 14));
          lbTotalHTValeur.setHorizontalAlignment(SwingConstants.LEFT);
          lbTotalHTValeur.setPreferredSize(new Dimension(100, 30));
          lbTotalHTValeur.setMinimumSize(new Dimension(100, 30));
          lbTotalHTValeur.setHorizontalTextPosition(SwingConstants.LEADING);
          lbTotalHTValeur.setMaximumSize(new Dimension(100, 30));
          lbTotalHTValeur.setName("lbTotalHTValeur");
          pnlTotaux.add(lbTotalHTValeur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTotalTVA ----
          lbTotalTVA.setText("Total TVA :");
          lbTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTVA.setFont(new Font("Arial", Font.BOLD, 13));
          lbTotalTVA.setMaximumSize(new Dimension(80, 30));
          lbTotalTVA.setMinimumSize(new Dimension(80, 30));
          lbTotalTVA.setPreferredSize(new Dimension(80, 30));
          lbTotalTVA.setName("lbTotalTVA");
          pnlTotaux.add(lbTotalTVA, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalTVAValeur ----
          lbTotalTVAValeur.setBackground(new Color(234, 234, 218));
          lbTotalTVAValeur.setBorder(null);
          lbTotalTVAValeur.setIconTextGap(6);
          lbTotalTVAValeur.setFont(new Font("Arial", Font.PLAIN, 14));
          lbTotalTVAValeur.setHorizontalAlignment(SwingConstants.LEFT);
          lbTotalTVAValeur.setPreferredSize(new Dimension(100, 30));
          lbTotalTVAValeur.setMinimumSize(new Dimension(100, 30));
          lbTotalTVAValeur.setHorizontalTextPosition(SwingConstants.LEADING);
          lbTotalTVAValeur.setMaximumSize(new Dimension(100, 30));
          lbTotalTVAValeur.setName("lbTotalTVAValeur");
          pnlTotaux.add(lbTotalTVAValeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTotalTTC ----
          lbTotalTTC.setText("Total TTC :");
          lbTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTTC.setFont(new Font("Arial", Font.BOLD, 13));
          lbTotalTTC.setPreferredSize(new Dimension(80, 30));
          lbTotalTTC.setMinimumSize(new Dimension(80, 30));
          lbTotalTTC.setMaximumSize(new Dimension(80, 30));
          lbTotalTTC.setName("lbTotalTTC");
          pnlTotaux.add(lbTotalTTC, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalTTCValeur ----
          lbTotalTTCValeur.setBackground(new Color(234, 234, 218));
          lbTotalTTCValeur.setBorder(null);
          lbTotalTTCValeur.setIconTextGap(6);
          lbTotalTTCValeur.setFont(new Font("Arial", Font.PLAIN, 14));
          lbTotalTTCValeur.setHorizontalAlignment(SwingConstants.LEFT);
          lbTotalTTCValeur.setPreferredSize(new Dimension(100, 30));
          lbTotalTTCValeur.setMinimumSize(new Dimension(100, 30));
          lbTotalTTCValeur.setHorizontalTextPosition(SwingConstants.LEADING);
          lbTotalTTCValeur.setMaximumSize(new Dimension(100, 30));
          lbTotalTTCValeur.setName("lbTotalTTCValeur");
          pnlTotaux.add(lbTotalTTCValeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMessageReglement ----
          lbMessageReglement.setText("Informations r\u00e8glement");
          lbMessageReglement.setHorizontalAlignment(SwingConstants.LEFT);
          lbMessageReglement.setFont(new Font("Arial", Font.BOLD, 13));
          lbMessageReglement.setMaximumSize(new Dimension(225, 40));
          lbMessageReglement.setMinimumSize(new Dimension(325, 40));
          lbMessageReglement.setPreferredSize(new Dimension(325, 40));
          lbMessageReglement.setName("lbMessageReglement");
          pnlTotaux.add(lbMessageReglement, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPied.add(pnlTotaux, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPied,
          new GridBagConstraints(0, 4, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private JPanel pnlInfosDocument;
  private JLabel lbDate;
  private JLabel calDateValeur;
  private JLabel lbMagasin;
  private JLabel lbMagasinValeur;
  private JLabel lbRepresentant;
  private JLabel lbRepresentantValeur;
  private JLabel lbVendeur;
  private JLabel lbVendeurValeur;
  private JLabel lbReferenceLongue;
  private JLabel lbReferenceLongueValeur;
  private JLabel lbReferenceCourte;
  private JLabel lbReferenceCourteValeur;
  private JLabel lbReferenceChantier;
  private JLabel lbReferenceChantierValeur;
  private JLabel lbInformationLivraisonEnlevement;
  private JScrollPane scpInformationLivraisonEnlevement;
  private JTextArea taInformationLivraisonEnlevement;
  private JLabel lbDateLivraison;
  private JLabel lbDateLivraisonValeur;
  private JLabel lbNumeroFacture;
  private JLabel lbNumeroFactureValeur;
  private JLabel lbDateValidite;
  private JLabel lbDateValiditeValeur;
  private JLabel lbBonAchatLie;
  private JLabel lbBonAchatLieValeur;
  private JPanel pnlCoordonneesClientFacture;
  private JLabel tfCiviliteFacturation;
  private JLabel tfNomFacturation;
  private JLabel tfComplementNomFacturation;
  private JLabel tfRueFacturation;
  private JLabel tfLocalisationFacturation;
  private JLabel tfVilleFacturation;
  private JLabel tfCodePostalFacturation;
  private JPanel pnlCoordonneesClientLivre;
  private JLabel tfCiviliteLivraison;
  private JLabel tfNomLivraison;
  private JLabel tfComplementNomLivraison;
  private JLabel tfRueLivraison;
  private JLabel tfLocalisationLivraison;
  private JLabel tfVilleLivraison;
  private JLabel tfCodePostalLivraison;
  private JPanel pnlContact;
  private JLabel lbNumeroClient;
  private JLabel tfNumeroClient;
  private JLabel lbContact;
  private JLabel tfNomComplet;
  private JLabel lbMail;
  private JLabel tfEmail;
  private JLabel lbTelephone;
  private JLabel tfTelephone;
  private JLabel lbFax;
  private JLabel tfFax;
  private JPanel pnlTitreListe;
  private JLabel lbTitre;
  private JScrollPane scpListeDocuments;
  private NRiTable tblLignesArticlesDocument;
  private JPanel pnlPied;
  private JPanel pnlListeRemboursement;
  private SNLabelTitre lbTitreRemboursement;
  private JScrollPane scpListeRemboursement;
  private NRiTable tblListeRemboursement;
  private JPanel pnlInfosDiverses;
  private SNLabelChamp lbPoidsTotal;
  private SNLabelChamp lbPoidsTotalValeur;
  private SNLabelChamp lbMargeTotale;
  private SNLabelChamp lbMargeTotaleValeur;
  private JPanel pnlTotaux;
  private SNLabelChamp lbTotalHT;
  private SNLabelChamp lbTotalHTValeur;
  private SNLabelChamp lbTotalTVA;
  private SNLabelChamp lbTotalTVAValeur;
  private SNLabelChamp lbTotalTTC;
  private SNLabelChamp lbTotalTTCValeur;
  private SNLabelChamp lbMessageReglement;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
