/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.negociationglobale;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.desktop.metier.gescom.comptoir.detailligne.ColonneTarif;
import ri.serien.libcommun.gescom.vente.negociation.NegociationVenteGlobale;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueNegociationGlobaleDocument extends AbstractVueDialogue<ModeleNegociationGlobaleDocument> {
  // Constantes
  private static final String BOUTON_AFFICHER_TTC = "Afficher en TTC";
  private static final String BOUTON_AFFICHER_HT = "Afficher en HT";
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détails";
  private static final String BOUTON_MASQUER_DETAIL = "Masquer détails";
  private static final String LIBELLE_TOTAL_PUBLIC_TTC = "Total public TTC";
  private static final String LIBELLE_TOTAL_PUBLIC_HT = "Total public HT";
  private static final String LIBELLE_TOTAL_BASE_TTC = "Total base TTC";
  private static final String LIBELLE_TOTAL_BASE_HT = "Total base HT";
  private static final String LIBELLE_TOTAL_NET_TTC = "Total net TTC";
  private static final String LIBELLE_TOTAL_NET_HT = "Total net HT";
  
  // Variables
  private boolean valeurEstSaisie = false;
  
  /**
   * Constructeur.
   */
  public VueNegociationGlobaleDocument(ModeleNegociationGlobaleDocument pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_TTC, 'a', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_HT, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_DETAIL, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    tfRemise.requestFocusInWindow();
  }
  
  @Override
  public void rafraichir() {
    rafraichirBoutonTTC();
    rafraichirBoutonHT();
    rafraichirBoutonAfficherDetail();
    rafraichirBoutonMasquerDetail();
    
    // Prix initial
    rafraichirTotalBaseInitial();
    rafraichirListeColonneTarifaireInitiale();
    rafraichirTotalNetInitial();
    rafraichirRemiseInitial();
    rafraichirDetailInitial();
    
    // Prix négocié
    rafraichirTotalBase();
    rafraichirListeColonneTarifaire();
    rafraichirTotalNet();
    rafraichirRemise();
    rafraichirDetail();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraîchir le bouton TTC.
   */
  private void rafraichirBoutonTTC() {
    // Interdiction de changer de mode pour le moment
    boolean actif = false;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_TTC, actif);
  }
  
  /**
   * Rafraîchir le bouton HT.
   */
  private void rafraichirBoutonHT() {
    // Interdiction de changer de mode pour le moment
    boolean actif = false;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_HT, actif);
  }
  
  /**
   * Rafraîchir le bouton qui affiche le détail
   */
  private void rafraichirBoutonAfficherDetail() {
    boolean actif = !getModele().isAfficherDetails() && getModele().isAutoriseVisualiserMarge();
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL, actif);
  }
  
  /**
   * Rafraîchir le bouton qui masque le détail.
   */
  private void rafraichirBoutonMasquerDetail() {
    boolean actif = false;
    actif = getModele().isAfficherDetails() && getModele().isAutoriseVisualiserMarge();
    snBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL, actif);
  }
  
  /**
   * Rafraichir le champ contenant le prix public au prix de base initial.
   */
  private void rafraichirTotalBaseInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    // En mode négoce
    if (getModele().isModeNegoce()) {
      if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
        tfTotalBaseInitial.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
        lbTotalBaseInitial.setText(LIBELLE_TOTAL_PUBLIC_TTC);
      }
      else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
        tfTotalBaseInitial.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
        lbTotalBaseInitial.setText(LIBELLE_TOTAL_PUBLIC_HT);
      }
      else {
        tfTotalBaseInitial.setText("");
      }
    }
    // En mode classique
    else {
      if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
        tfTotalBaseInitial.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
        lbTotalBaseInitial.setText(LIBELLE_TOTAL_BASE_TTC);
      }
      else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
        tfTotalBaseInitial.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
        lbTotalBaseInitial.setText(LIBELLE_TOTAL_BASE_HT);
      }
      else {
        tfTotalBaseInitial.setText("");
      }
    }
  }
  
  private void rafraichirTotalNetInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalNetTTC() != null) {
      tfTotalNetInitial.setText(Constantes.formater(negociation.getTotalNetTTC(), true));
      lbTotalNetInitial.setText(LIBELLE_TOTAL_NET_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalNetHT() != null) {
      tfTotalNetInitial.setText(Constantes.formater(negociation.getTotalNetHT(), true));
      lbTotalNetInitial.setText(LIBELLE_TOTAL_NET_HT);
    }
    else {
      tfTotalNetInitial.setText("");
    }
  }
  
  private void rafraichirRemiseInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    if (negociation != null && negociation.getTauxRemise() != null) {
      tfRemiseInitiale.setText(Constantes.formater(negociation.getTauxRemise(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfRemiseInitiale.setText("");
    }
  }
  
  private void rafraichirDetailInitial() {
    if (getModele().isAutoriseVisualiserMarge() && getModele().isAfficherDetails()) {
      pnlTarifOrigineMarge.setVisible(true);
      rafraichirIndiceInitial();
      rafraichirMargeInitial();
      rafraichirPrixRevientInitial();
    }
    else {
      pnlTarifOrigineMarge.setVisible(false);
    }
  }
  
  private void rafraichirIndiceInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    if (negociation != null && negociation.getIndiceMarge() != null) {
      tfIndiceDeMargeInitial.setText(Constantes.formater(negociation.getIndiceMarge(), true));
    }
    else {
      tfIndiceDeMargeInitial.setText("");
    }
  }
  
  private void rafraichirMargeInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    if (negociation != null && negociation.getTauxMarge() != null) {
      tfMargeInitiale.setText(Constantes.formater(negociation.getTauxMarge(), true));
    }
    else {
      tfMargeInitiale.setText("");
    }
  }
  
  private void rafraichirPrixRevientInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleOrigine();
    if (negociation != null && negociation.getTotalRevientHT() != null) {
      tfPrixRevientInitial.setText(Constantes.formater(negociation.getTotalRevientHT(), true));
    }
    else {
      tfPrixRevientInitial.setText("");
    }
  }
  
  private void rafraichirTotalBase() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    // En mode négoce
    if (getModele().isModeNegoce()) {
      if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
        tfTotalBase.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
        lbTotalBase.setText(LIBELLE_TOTAL_PUBLIC_TTC);
      }
      else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
        tfTotalBase.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
        lbTotalBase.setText(LIBELLE_TOTAL_PUBLIC_HT);
      }
      else {
        tfTotalBase.setText("");
      }
    }
    else {
      if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
        tfTotalBase.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
        lbTotalBase.setText(LIBELLE_TOTAL_BASE_TTC);
      }
      else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
        tfTotalBase.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
        lbTotalBase.setText(LIBELLE_TOTAL_BASE_HT);
      }
      else {
        tfTotalBase.setText("");
      }
    }
  }
  
  private void rafraichirTotalNet() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalNetTTC() != null) {
      tfTotalNet.setText(Constantes.formater(negociation.getTotalNetTTC(), true));
      lbTotalNet.setText(LIBELLE_TOTAL_NET_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalNetHT() != null) {
      tfTotalNet.setText(Constantes.formater(negociation.getTotalNetHT(), true));
      lbTotalNet.setText(LIBELLE_TOTAL_NET_HT);
    }
    else {
      tfTotalNet.setText("");
    }
  }
  
  private void rafraichirRemise() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    if (negociation != null && negociation.getTauxRemise() != null) {
      tfRemise.setText(Constantes.formater(negociation.getTauxRemise(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfRemise.setText("");
    }
  }
  
  private void rafraichirDetail() {
    if (getModele().isAutoriseVisualiserMarge() && getModele().isAfficherDetails()) {
      pnlTarifNegocieMarge.setVisible(true);
      rafraichirIndice();
      rafraichirMarge();
      rafraichirPrixRevient();
    }
    else {
      pnlTarifNegocieMarge.setVisible(false);
    }
  }
  
  private void rafraichirIndice() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    if (negociation != null && negociation.getIndiceMarge() != null) {
      tfIndiceDeMarge.setText(Constantes.formater(negociation.getIndiceMarge(), true));
    }
    else {
      tfIndiceDeMarge.setText("");
    }
  }
  
  private void rafraichirMarge() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    if (negociation != null && negociation.getTauxMarge() != null) {
      tfMarge.setText(Constantes.formater(negociation.getTauxMarge(), true));
    }
    else {
      tfMarge.setText("");
    }
  }
  
  private void rafraichirPrixRevient() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteGlobaleNegocie();
    if (negociation != null && negociation.getTotalRevientHT() != null) {
      tfPrixRevient.setText(Constantes.formater(negociation.getTotalRevientHT(), true));
    }
    else {
      tfPrixRevient.setText("");
    }
  }
  
  /**
   * Rafraichir la liste des données de l'origine
   */
  private void rafraichirListeColonneTarifaireInitiale() {
    ColonneTarif[] listeColonne = getModele().getListeColonnesTarif();
    if (listeColonne == null) {
      tfOrigineInitial.setText("");
      return;
    }
    
    if (getModele().getNegociationVenteGlobaleOrigine() != null) {
      int index = getModele().getNegociationVenteGlobaleOrigine().getNumeroColonne();
      if (index < listeColonne.length) {
        tfOrigineInitial.setText(listeColonne[index].toString());
      }
    }
  }
  
  /**
   * Rafraichir la liste des données de l'origine et sélectionner la valeur active.
   */
  private void rafraichirListeColonneTarifaire() {
    cbOrigine.removeAllItems();
    if (getModele().getListeColonnesTarif() != null) {
      cbOrigine.setModel(new DefaultComboBoxModel(getModele().getListeColonnesTarif()));
    }
    else {
      cbOrigine.setModel(new DefaultComboBoxModel());
    }
    
    if (getModele().getNegociationVenteGlobaleNegocie() != null) {
      int index = getModele().getNegociationVenteGlobaleNegocie().getNumeroColonne();
      if (index < cbOrigine.getItemCount()) {
        cbOrigine.setSelectedIndex(index);
      }
    }
  }
  
  /**
   * Teste si la touche du clavier utilisée peut entrainer une modification de valeur.
   */
  private boolean testerModificationValeur(int pKeyCode, boolean pValeurEstModifiee) {
    // Si la valeur a été déjà modifiée alors on retourne vrai
    if (pValeurEstModifiee) {
      return true;
    }
    if ((pKeyCode == KeyEvent.VK_TAB) || (pKeyCode == KeyEvent.VK_LEFT) || (pKeyCode == KeyEvent.VK_RIGHT)
        || (pKeyCode == KeyEvent.VK_UP)) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_TTC)) {
        getModele().modifierAffichageTTC(true);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_HT)) {
        getModele().modifierAffichageTTC(false);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL)) {
        getModele().modifierAffichageDetails(false);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        getModele().modifierAffichageDetails(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbColonneTarifItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierColonne((ColonneTarif) cbOrigine.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemise1FocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (valeurEstSaisie) {
        getModele().modifierRemise(tfRemise.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTotalNetFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierPrixNet(tfTotalNet.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfIndiceDeMargeFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierIndiceDeMarge(tfIndiceDeMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfMargeFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierMarge(tfMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfTotalNetKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfIndiceDeMargeKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfMargeKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlContenu = new SNPanelContenu();
    pnlTarifOrigine = new SNPanelTitre();
    lbTotalBaseInitial = new SNLabelChamp();
    lbOrigineInitiale = new SNLabelChamp();
    lbTotalNetInitial = new SNLabelChamp();
    tfTotalBaseInitial = new SNTexte();
    tfOrigineInitial = new SNTexte();
    pnlRemiseIni = new SNPanel();
    lbRemiseInitiale = new SNLabelChamp();
    tfRemiseInitiale = new SNTexte();
    lbUniteRemise = new SNLabelUnite();
    tfTotalNetInitial = new SNTexte();
    pnlTarifOrigineMarge = new SNPanelTitre();
    lbIndiceDeMargeInitial = new SNLabelChamp();
    lbMargeInitiale = new SNLabelChamp();
    lbPrixRevientInitial = new SNLabelChamp();
    tfIndiceDeMargeInitial = new SNTexte();
    tfMargeInitiale = new SNTexte();
    lbUniteMargeInitiale = new SNLabelUnite();
    tfPrixRevientInitial = new SNTexte();
    pnlTarifNegocie = new SNPanelTitre();
    lbTotalBase = new SNLabelChamp();
    lbOrigine = new SNLabelChamp();
    lbTotalNet = new SNLabelChamp();
    tfTotalBase = new SNTexte();
    cbOrigine = new SNComboBox();
    pnlRemiseNego = new SNPanel();
    lbRemise = new SNLabelChamp();
    tfRemise = new SNTexte();
    lbUniteRemise2 = new SNLabelUnite();
    tfTotalNet = new SNTexte();
    pnlTarifNegocieMarge = new SNPanelTitre();
    lbIndiceDeMarge = new SNLabelChamp();
    lbMarge = new SNLabelChamp();
    lbPrixRevient = new SNLabelChamp();
    tfIndiceDeMarge = new SNTexte();
    tfMarge = new SNTexte();
    lbUniteMarge = new SNLabelUnite();
    tfPrixRevient = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("N\u00e9gociation globale");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(910, 350));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setOpaque(false);
        pnlContenu.setAlignmentX(0.0F);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlTarifOrigine ========
        {
          pnlTarifOrigine.setTitre("Prix initial");
          pnlTarifOrigine.setName("pnlTarifOrigine");
          pnlTarifOrigine.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifOrigine.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifOrigine.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifOrigine.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifOrigine.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTotalBaseInitial ----
          lbTotalBaseInitial.setText("Total public");
          lbTotalBaseInitial.setHorizontalAlignment(SwingConstants.CENTER);
          lbTotalBaseInitial.setComponentPopupMenu(null);
          lbTotalBaseInitial.setMaximumSize(new Dimension(100, 30));
          lbTotalBaseInitial.setMinimumSize(new Dimension(100, 30));
          lbTotalBaseInitial.setPreferredSize(new Dimension(125, 30));
          lbTotalBaseInitial.setName("lbTotalBaseInitial");
          pnlTarifOrigine.add(lbTotalBaseInitial, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbOrigineInitiale ----
          lbOrigineInitiale.setText("Origine");
          lbOrigineInitiale.setHorizontalAlignment(SwingConstants.CENTER);
          lbOrigineInitiale.setComponentPopupMenu(null);
          lbOrigineInitiale.setMaximumSize(new Dimension(100, 30));
          lbOrigineInitiale.setMinimumSize(new Dimension(100, 30));
          lbOrigineInitiale.setPreferredSize(new Dimension(100, 30));
          lbOrigineInitiale.setName("lbOrigineInitiale");
          pnlTarifOrigine.add(lbOrigineInitiale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalNetInitial ----
          lbTotalNetInitial.setText("Total net");
          lbTotalNetInitial.setHorizontalAlignment(SwingConstants.CENTER);
          lbTotalNetInitial.setComponentPopupMenu(null);
          lbTotalNetInitial.setMaximumSize(new Dimension(100, 30));
          lbTotalNetInitial.setMinimumSize(new Dimension(100, 30));
          lbTotalNetInitial.setPreferredSize(new Dimension(100, 30));
          lbTotalNetInitial.setName("lbTotalNetInitial");
          pnlTarifOrigine.add(lbTotalNetInitial, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfTotalBaseInitial ----
          tfTotalBaseInitial.setHorizontalAlignment(SwingConstants.RIGHT);
          tfTotalBaseInitial.setComponentPopupMenu(null);
          tfTotalBaseInitial.setPreferredSize(new Dimension(100, 30));
          tfTotalBaseInitial.setEditable(false);
          tfTotalBaseInitial.setEnabled(false);
          tfTotalBaseInitial.setName("tfTotalBaseInitial");
          pnlTarifOrigine.add(tfTotalBaseInitial, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfOrigineInitial ----
          tfOrigineInitial.setHorizontalAlignment(SwingConstants.CENTER);
          tfOrigineInitial.setComponentPopupMenu(null);
          tfOrigineInitial.setPreferredSize(new Dimension(100, 30));
          tfOrigineInitial.setEditable(false);
          tfOrigineInitial.setEnabled(false);
          tfOrigineInitial.setName("tfOrigineInitial");
          pnlTarifOrigine.add(tfOrigineInitial, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlRemiseIni ========
          {
            pnlRemiseIni.setName("pnlRemiseIni");
            pnlRemiseIni.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemiseIni.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseIni.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseIni.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemiseIni.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbRemiseInitiale ----
            lbRemiseInitiale.setText("Remise");
            lbRemiseInitiale.setHorizontalAlignment(SwingConstants.CENTER);
            lbRemiseInitiale.setComponentPopupMenu(null);
            lbRemiseInitiale.setMaximumSize(new Dimension(100, 30));
            lbRemiseInitiale.setMinimumSize(new Dimension(100, 30));
            lbRemiseInitiale.setPreferredSize(new Dimension(100, 30));
            lbRemiseInitiale.setName("lbRemiseInitiale");
            pnlRemiseIni.add(lbRemiseInitiale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfRemiseInitiale ----
            tfRemiseInitiale.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemiseInitiale.setComponentPopupMenu(null);
            tfRemiseInitiale.setPreferredSize(new Dimension(100, 30));
            tfRemiseInitiale.setEditable(false);
            tfRemiseInitiale.setEnabled(false);
            tfRemiseInitiale.setName("tfRemiseInitiale");
            pnlRemiseIni.add(tfRemiseInitiale, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemise ----
            lbUniteRemise.setText("% ");
            lbUniteRemise.setHorizontalAlignment(SwingConstants.LEFT);
            lbUniteRemise.setComponentPopupMenu(null);
            lbUniteRemise.setPreferredSize(new Dimension(30, 30));
            lbUniteRemise.setMaximumSize(new Dimension(30, 30));
            lbUniteRemise.setMinimumSize(new Dimension(30, 30));
            lbUniteRemise.setName("lbUniteRemise");
            pnlRemiseIni.add(lbUniteRemise, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarifOrigine.add(pnlRemiseIni, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfTotalNetInitial ----
          tfTotalNetInitial.setHorizontalAlignment(SwingConstants.RIGHT);
          tfTotalNetInitial.setComponentPopupMenu(null);
          tfTotalNetInitial.setPreferredSize(new Dimension(100, 30));
          tfTotalNetInitial.setEditable(false);
          tfTotalNetInitial.setEnabled(false);
          tfTotalNetInitial.setName("tfTotalNetInitial");
          pnlTarifOrigine.add(tfTotalNetInitial, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTarifOrigine, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlTarifOrigineMarge ========
        {
          pnlTarifOrigineMarge.setOpaque(false);
          pnlTarifOrigineMarge.setTitre("Marge initiale");
          pnlTarifOrigineMarge.setMinimumSize(new Dimension(400, 120));
          pnlTarifOrigineMarge.setPreferredSize(new Dimension(400, 120));
          pnlTarifOrigineMarge.setMaximumSize(new Dimension(400, 120));
          pnlTarifOrigineMarge.setName("pnlTarifOrigineMarge");
          pnlTarifOrigineMarge.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifOrigineMarge.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifOrigineMarge.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifOrigineMarge.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifOrigineMarge.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbIndiceDeMargeInitial ----
          lbIndiceDeMargeInitial.setText("Indice");
          lbIndiceDeMargeInitial.setHorizontalAlignment(SwingConstants.CENTER);
          lbIndiceDeMargeInitial.setComponentPopupMenu(null);
          lbIndiceDeMargeInitial.setPreferredSize(new Dimension(100, 30));
          lbIndiceDeMargeInitial.setMinimumSize(new Dimension(100, 30));
          lbIndiceDeMargeInitial.setMaximumSize(new Dimension(100, 30));
          lbIndiceDeMargeInitial.setName("lbIndiceDeMargeInitial");
          pnlTarifOrigineMarge.add(lbIndiceDeMargeInitial, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMargeInitiale ----
          lbMargeInitiale.setText("Marge");
          lbMargeInitiale.setHorizontalAlignment(SwingConstants.CENTER);
          lbMargeInitiale.setComponentPopupMenu(null);
          lbMargeInitiale.setPreferredSize(new Dimension(100, 30));
          lbMargeInitiale.setMinimumSize(new Dimension(100, 30));
          lbMargeInitiale.setMaximumSize(new Dimension(100, 30));
          lbMargeInitiale.setName("lbMargeInitiale");
          pnlTarifOrigineMarge.add(lbMargeInitiale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixRevientInitial ----
          lbPrixRevientInitial.setText("Prix de revient HT");
          lbPrixRevientInitial.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixRevientInitial.setComponentPopupMenu(null);
          lbPrixRevientInitial.setPreferredSize(new Dimension(130, 30));
          lbPrixRevientInitial.setMinimumSize(new Dimension(100, 30));
          lbPrixRevientInitial.setMaximumSize(new Dimension(100, 30));
          lbPrixRevientInitial.setName("lbPrixRevientInitial");
          pnlTarifOrigineMarge.add(lbPrixRevientInitial, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfIndiceDeMargeInitial ----
          tfIndiceDeMargeInitial.setHorizontalAlignment(SwingConstants.RIGHT);
          tfIndiceDeMargeInitial.setComponentPopupMenu(null);
          tfIndiceDeMargeInitial.setPreferredSize(new Dimension(100, 30));
          tfIndiceDeMargeInitial.setEditable(false);
          tfIndiceDeMargeInitial.setEnabled(false);
          tfIndiceDeMargeInitial.setName("tfIndiceDeMargeInitial");
          pnlTarifOrigineMarge.add(tfIndiceDeMargeInitial, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfMargeInitiale ----
          tfMargeInitiale.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMargeInitiale.setComponentPopupMenu(null);
          tfMargeInitiale.setPreferredSize(new Dimension(100, 30));
          tfMargeInitiale.setEditable(false);
          tfMargeInitiale.setEnabled(false);
          tfMargeInitiale.setName("tfMargeInitiale");
          pnlTarifOrigineMarge.add(tfMargeInitiale, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteMargeInitiale ----
          lbUniteMargeInitiale.setText("%");
          lbUniteMargeInitiale.setHorizontalAlignment(SwingConstants.LEFT);
          lbUniteMargeInitiale.setComponentPopupMenu(null);
          lbUniteMargeInitiale.setPreferredSize(new Dimension(20, 30));
          lbUniteMargeInitiale.setMaximumSize(new Dimension(20, 30));
          lbUniteMargeInitiale.setMinimumSize(new Dimension(20, 30));
          lbUniteMargeInitiale.setName("lbUniteMargeInitiale");
          pnlTarifOrigineMarge.add(lbUniteMargeInitiale, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPrixRevientInitial ----
          tfPrixRevientInitial.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixRevientInitial.setComponentPopupMenu(null);
          tfPrixRevientInitial.setPreferredSize(new Dimension(100, 30));
          tfPrixRevientInitial.setEditable(false);
          tfPrixRevientInitial.setEnabled(false);
          tfPrixRevientInitial.setName("tfPrixRevientInitial");
          pnlTarifOrigineMarge.add(tfPrixRevientInitial, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTarifOrigineMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTarifNegocie ========
        {
          pnlTarifNegocie.setTitre("Prix n\u00e9goci\u00e9");
          pnlTarifNegocie.setName("pnlTarifNegocie");
          pnlTarifNegocie.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifNegocie.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTotalBase ----
          lbTotalBase.setText("Total public");
          lbTotalBase.setHorizontalAlignment(SwingConstants.CENTER);
          lbTotalBase.setComponentPopupMenu(null);
          lbTotalBase.setMaximumSize(new Dimension(100, 30));
          lbTotalBase.setMinimumSize(new Dimension(100, 30));
          lbTotalBase.setPreferredSize(new Dimension(125, 30));
          lbTotalBase.setName("lbTotalBase");
          pnlTarifNegocie.add(lbTotalBase, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbOrigine ----
          lbOrigine.setText("Origine");
          lbOrigine.setHorizontalAlignment(SwingConstants.CENTER);
          lbOrigine.setComponentPopupMenu(null);
          lbOrigine.setMaximumSize(new Dimension(100, 30));
          lbOrigine.setMinimumSize(new Dimension(100, 30));
          lbOrigine.setPreferredSize(new Dimension(100, 30));
          lbOrigine.setName("lbOrigine");
          pnlTarifNegocie.add(lbOrigine, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalNet ----
          lbTotalNet.setText("Total net");
          lbTotalNet.setHorizontalAlignment(SwingConstants.CENTER);
          lbTotalNet.setComponentPopupMenu(null);
          lbTotalNet.setMaximumSize(new Dimension(100, 30));
          lbTotalNet.setMinimumSize(new Dimension(100, 30));
          lbTotalNet.setPreferredSize(new Dimension(100, 30));
          lbTotalNet.setName("lbTotalNet");
          pnlTarifNegocie.add(lbTotalNet, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfTotalBase ----
          tfTotalBase.setHorizontalAlignment(SwingConstants.RIGHT);
          tfTotalBase.setComponentPopupMenu(null);
          tfTotalBase.setPreferredSize(new Dimension(100, 30));
          tfTotalBase.setEnabled(false);
          tfTotalBase.setName("tfTotalBase");
          pnlTarifNegocie.add(tfTotalBase, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbOrigine ----
          cbOrigine.setPreferredSize(new Dimension(100, 30));
          cbOrigine.setName("cbOrigine");
          cbOrigine.addItemListener(e -> cbColonneTarifItemStateChanged(e));
          pnlTarifNegocie.add(cbOrigine, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlRemiseNego ========
          {
            pnlRemiseNego.setName("pnlRemiseNego");
            pnlRemiseNego.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemiseNego.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseNego.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRemiseNego.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemiseNego.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbRemise ----
            lbRemise.setText("Remise");
            lbRemise.setHorizontalAlignment(SwingConstants.CENTER);
            lbRemise.setComponentPopupMenu(null);
            lbRemise.setMaximumSize(new Dimension(100, 30));
            lbRemise.setMinimumSize(new Dimension(100, 30));
            lbRemise.setPreferredSize(new Dimension(100, 30));
            lbRemise.setName("lbRemise");
            pnlRemiseNego.add(lbRemise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfRemise ----
            tfRemise.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemise.setComponentPopupMenu(null);
            tfRemise.setPreferredSize(new Dimension(100, 30));
            tfRemise.setName("tfRemise");
            tfRemise.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemise1FocusLost(e);
              }
            });
            tfRemise.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfRemiseKeyPressed(e);
              }
            });
            pnlRemiseNego.add(tfRemise, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemise2 ----
            lbUniteRemise2.setText("% ");
            lbUniteRemise2.setHorizontalAlignment(SwingConstants.LEFT);
            lbUniteRemise2.setComponentPopupMenu(null);
            lbUniteRemise2.setPreferredSize(new Dimension(30, 30));
            lbUniteRemise2.setMaximumSize(new Dimension(30, 30));
            lbUniteRemise2.setMinimumSize(new Dimension(30, 30));
            lbUniteRemise2.setName("lbUniteRemise2");
            pnlRemiseNego.add(lbUniteRemise2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarifNegocie.add(pnlRemiseNego, new GridBagConstraints(2, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfTotalNet ----
          tfTotalNet.setHorizontalAlignment(SwingConstants.RIGHT);
          tfTotalNet.setComponentPopupMenu(null);
          tfTotalNet.setPreferredSize(new Dimension(100, 30));
          tfTotalNet.setName("tfTotalNet");
          tfTotalNet.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfTotalNetFocusLost(e);
            }
          });
          tfTotalNet.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
              tfTotalNetKeyPressed(e);
            }
          });
          pnlTarifNegocie.add(tfTotalNet, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTarifNegocie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlTarifNegocieMarge ========
        {
          pnlTarifNegocieMarge.setBackground(new Color(222, 222, 206));
          pnlTarifNegocieMarge.setOpaque(false);
          pnlTarifNegocieMarge.setTitre("Marge n\u00e9goci\u00e9e");
          pnlTarifNegocieMarge.setPreferredSize(new Dimension(400, 120));
          pnlTarifNegocieMarge.setMinimumSize(new Dimension(400, 120));
          pnlTarifNegocieMarge.setMaximumSize(new Dimension(400, 120));
          pnlTarifNegocieMarge.setName("pnlTarifNegocieMarge");
          pnlTarifNegocieMarge.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbIndiceDeMarge ----
          lbIndiceDeMarge.setText("Indice");
          lbIndiceDeMarge.setHorizontalAlignment(SwingConstants.CENTER);
          lbIndiceDeMarge.setComponentPopupMenu(null);
          lbIndiceDeMarge.setMaximumSize(new Dimension(100, 30));
          lbIndiceDeMarge.setMinimumSize(new Dimension(100, 30));
          lbIndiceDeMarge.setPreferredSize(new Dimension(100, 30));
          lbIndiceDeMarge.setName("lbIndiceDeMarge");
          pnlTarifNegocieMarge.add(lbIndiceDeMarge, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMarge ----
          lbMarge.setText("Marge");
          lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
          lbMarge.setComponentPopupMenu(null);
          lbMarge.setMaximumSize(new Dimension(100, 30));
          lbMarge.setMinimumSize(new Dimension(100, 30));
          lbMarge.setPreferredSize(new Dimension(100, 30));
          lbMarge.setName("lbMarge");
          pnlTarifNegocieMarge.add(lbMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixRevient ----
          lbPrixRevient.setText("Prix de revient HT");
          lbPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
          lbPrixRevient.setComponentPopupMenu(null);
          lbPrixRevient.setMaximumSize(new Dimension(100, 30));
          lbPrixRevient.setMinimumSize(new Dimension(100, 30));
          lbPrixRevient.setPreferredSize(new Dimension(130, 30));
          lbPrixRevient.setName("lbPrixRevient");
          pnlTarifNegocieMarge.add(lbPrixRevient, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- tfIndiceDeMarge ----
          tfIndiceDeMarge.setHorizontalAlignment(SwingConstants.RIGHT);
          tfIndiceDeMarge.setComponentPopupMenu(null);
          tfIndiceDeMarge.setPreferredSize(new Dimension(100, 30));
          tfIndiceDeMarge.setName("tfIndiceDeMarge");
          tfIndiceDeMarge.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfIndiceDeMargeFocusLost(e);
            }
          });
          tfIndiceDeMarge.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
              tfIndiceDeMargeKeyPressed(e);
            }
          });
          pnlTarifNegocieMarge.add(tfIndiceDeMarge, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfMarge ----
          tfMarge.setHorizontalAlignment(SwingConstants.RIGHT);
          tfMarge.setComponentPopupMenu(null);
          tfMarge.setPreferredSize(new Dimension(100, 30));
          tfMarge.setName("tfMarge");
          tfMarge.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfMargeFocusLost(e);
            }
          });
          tfMarge.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
              tfMargeKeyPressed(e);
            }
          });
          pnlTarifNegocieMarge.add(tfMarge, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteMarge ----
          lbUniteMarge.setText("%");
          lbUniteMarge.setHorizontalAlignment(SwingConstants.LEFT);
          lbUniteMarge.setComponentPopupMenu(null);
          lbUniteMarge.setPreferredSize(new Dimension(20, 30));
          lbUniteMarge.setMaximumSize(new Dimension(20, 30));
          lbUniteMarge.setMinimumSize(new Dimension(20, 30));
          lbUniteMarge.setName("lbUniteMarge");
          pnlTarifNegocieMarge.add(lbUniteMarge, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPrixRevient ----
          tfPrixRevient.setHorizontalAlignment(SwingConstants.RIGHT);
          tfPrixRevient.setComponentPopupMenu(null);
          tfPrixRevient.setPreferredSize(new Dimension(100, 30));
          tfPrixRevient.setEnabled(false);
          tfPrixRevient.setName("tfPrixRevient");
          pnlTarifNegocieMarge.add(tfPrixRevient, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTarifNegocieMarge, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlTarifOrigine;
  private SNLabelChamp lbTotalBaseInitial;
  private SNLabelChamp lbOrigineInitiale;
  private SNLabelChamp lbTotalNetInitial;
  private SNTexte tfTotalBaseInitial;
  private SNTexte tfOrigineInitial;
  private SNPanel pnlRemiseIni;
  private SNLabelChamp lbRemiseInitiale;
  private SNTexte tfRemiseInitiale;
  private SNLabelUnite lbUniteRemise;
  private SNTexte tfTotalNetInitial;
  private SNPanelTitre pnlTarifOrigineMarge;
  private SNLabelChamp lbIndiceDeMargeInitial;
  private SNLabelChamp lbMargeInitiale;
  private SNLabelChamp lbPrixRevientInitial;
  private SNTexte tfIndiceDeMargeInitial;
  private SNTexte tfMargeInitiale;
  private SNLabelUnite lbUniteMargeInitiale;
  private SNTexte tfPrixRevientInitial;
  private SNPanelTitre pnlTarifNegocie;
  private SNLabelChamp lbTotalBase;
  private SNLabelChamp lbOrigine;
  private SNLabelChamp lbTotalNet;
  private SNTexte tfTotalBase;
  private SNComboBox cbOrigine;
  private SNPanel pnlRemiseNego;
  private SNLabelChamp lbRemise;
  private SNTexte tfRemise;
  private SNLabelUnite lbUniteRemise2;
  private SNTexte tfTotalNet;
  private SNPanelTitre pnlTarifNegocieMarge;
  private SNLabelChamp lbIndiceDeMarge;
  private SNLabelChamp lbMarge;
  private SNLabelChamp lbPrixRevient;
  private SNTexte tfIndiceDeMarge;
  private SNTexte tfMarge;
  private SNLabelUnite lbUniteMarge;
  private SNTexte tfPrixRevient;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
