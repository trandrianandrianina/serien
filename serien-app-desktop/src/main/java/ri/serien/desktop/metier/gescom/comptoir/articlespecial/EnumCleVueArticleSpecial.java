/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.articlespecial;

import ri.serien.libswing.moteur.mvc.InterfaceCleVue;

/**
 * Clé des vues de la boîte de dialogue de création d'un article spécial.
 */
public enum EnumCleVueArticleSpecial implements InterfaceCleVue {
  ONGLET_GENERAL(0, "Général"),
  ONGLET_NEGOCIATION_ACHAT(1, "Négociation achat");
  
  private final Integer index;
  private final String libelle;
  
  /**
   * Constructeur.
   *
   * @param pIndex Indice de la vue.
   * @param pLibelle Libellé de la vue.
   */
  EnumCleVueArticleSpecial(Integer pIndex, String pLibelle) {
    index = pIndex;
    libelle = pLibelle;
  }
  
  /**
   * Indice de la vue.
   */
  @Override
  public Integer getIndex() {
    return index;
  }
  
  /**
   * Libellé de la vue.
   */
  @Override
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Clé de la vue résumé dans une chaîne de caractères.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle + "(" + index + ")");
  }
  
  /**
   * Retourner l'objet énum par son indice.
   */
  static public EnumCleVueArticleSpecial valueOfByCode(Integer pIndex) {
    for (EnumCleVueArticleSpecial value : values()) {
      if (pIndex == value.getIndex()) {
        return value;
      }
    }
    return null;
  }
  
}
