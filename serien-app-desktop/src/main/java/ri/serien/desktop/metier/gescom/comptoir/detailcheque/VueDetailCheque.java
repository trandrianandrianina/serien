/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailcheque;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.WindowConstants;

import ri.serien.libcommun.gescom.vente.reglement.IdBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.Reglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snbanqueclient.SNBanqueClient;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composant.primitif.saisie.SNNombreDecimal;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueDetailCheque extends AbstractVueDialogue<ModeleDetailCheque> {
  /**
   * Constructeur.
   */
  public VueDetailCheque(ModeleDetailCheque pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Initialiser le format du numéro de chèque
    tfMontantCheque.setLongueur(Reglement.LONGUEUR_MONTANT);
    tfNumeroCheque.setLongueur(Reglement.LONGUEUR_NUMERO_CHEQUE);
    snBanqueClient.setModeSaisiAutorise(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirMontant();
    rafraichirNumeroCheque();
    rafraichirNomBanque();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMontant() {
    Reglement reglement = getModele().getReglement();
    if (reglement == null) {
      tfMontantCheque.setText("");
      return;
    }
    tfMontantCheque.setText(Constantes.formater(reglement.getMontant(), true));
    tfMontantCheque.setEnabled(false);
  }
  
  private void rafraichirNumeroCheque() {
    if (getModele().getNumeroCheque() == null) {
      tfNumeroCheque.setText("");
      return;
    }
    tfNumeroCheque.setText(getModele().getNumeroCheque());
  }
  
  private void rafraichirNomBanque() {
    Reglement reglement = getModele().getReglement();
    if (reglement == null) {
      return;
    }
    IdBanqueClient idBanqueClient = getModele().getIdBanqueClient();
    snBanqueClient.setSession(getModele().getSession());
    snBanqueClient.setIdEtablissement(reglement.getId().getIdEtablissement());
    snBanqueClient.charger(false);
    
    snBanqueClient.setIdSelection(idBanqueClient);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroChequeFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroCheque(tfNumeroCheque.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snBanqueClientValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        // Récupération d'une banque sélectionnée
        IdBanqueClient idBanqueClient = snBanqueClient.getIdSelection();
        if (idBanqueClient != null) {
          getModele().modifierBanqueClient(snBanqueClient.getIdSelection());
        }
        // Récupération d'une banque saisie
        else {
          String nomBanque = Constantes.normerTexte(snBanqueClient.getTexteSaisi());
          if (!nomBanque.isEmpty()) {
            idBanqueClient = IdBanqueClient.getInstance(nomBanque);
            getModele().modifierBanqueClient(idBanqueClient);
          }
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    snPanelContenu = new SNPanelContenu();
    lbMontantCheque = new SNLabelChamp();
    tfMontantCheque = new SNNombreDecimal();
    lbNumeroCheque = new SNLabelChamp();
    tfNumeroCheque = new SNIdentifiant();
    lbNomBanque = new SNLabelChamp();
    snBanqueClient = new SNBanqueClient();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail du ch\u00e8que");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(540, 230));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== snPanelContenu ========
      {
        snPanelContenu.setName("snPanelContenu");
        snPanelContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) snPanelContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) snPanelContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) snPanelContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) snPanelContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMontantCheque ----
        lbMontantCheque.setText("Montant du ch\u00e8que");
        lbMontantCheque.setName("lbMontantCheque");
        snPanelContenu.add(lbMontantCheque, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfMontantCheque ----
        tfMontantCheque.setPreferredSize(new Dimension(150, 30));
        tfMontantCheque.setName("tfMontantCheque");
        snPanelContenu.add(tfMontantCheque, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNumeroCheque ----
        lbNumeroCheque.setText("Num\u00e9ro du ch\u00e8que");
        lbNumeroCheque.setName("lbNumeroCheque");
        snPanelContenu.add(lbNumeroCheque, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNumeroCheque ----
        tfNumeroCheque.setPreferredSize(new Dimension(150, 30));
        tfNumeroCheque.setName("tfNumeroCheque");
        tfNumeroCheque.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNumeroChequeFocusLost(e);
          }
        });
        snPanelContenu.add(tfNumeroCheque, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNomBanque ----
        lbNomBanque.setText("Nom de la banque");
        lbNomBanque.setName("lbNomBanque");
        snPanelContenu.add(lbNomBanque, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snBanqueClient ----
        snBanqueClient.setName("snBanqueClient");
        snBanqueClient.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snBanqueClientValueChanged(e);
          }
        });
        snPanelContenu.add(snBanqueClient, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(snPanelContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu snPanelContenu;
  private SNLabelChamp lbMontantCheque;
  private SNNombreDecimal tfMontantCheque;
  private SNLabelChamp lbNumeroCheque;
  private SNIdentifiant tfNumeroCheque;
  private SNLabelChamp lbNomBanque;
  private SNBanqueClient snBanqueClient;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
