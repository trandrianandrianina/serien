/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.correctionadresse;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adresse.EnumErreurAdresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Demander une confirmation.
 * La boîte de dialogue pose une question à laquelle l'utilisateur doit répondre par "Oui" ou "Non".
 */
public class VueErreurAdresse extends AbstractVueDialogue<ModeleErreurAdresse> {
  
  private static final String LIBELLE_NOM = "Nom";
  private static final String LIBELLE_PRENOM = "Pr\u00e9nom";
  private static final String LIBELLE_RAISON_SOCIALE = "Raison sociale";
  private static final String LIBELLE_COMPLEMENT = "Compl\u00e9ment";
  private static final String LIBELLE_RESTE_ENCOURS = "Reste";
  private static final String LIBELLE_DEPASSEMENT_ENCOURS = "Dépassement";
  
  /**
   * Constructeur.
   */
  public VueErreurAdresse(ModeleErreurAdresse pModele) {
    super(pModele);
    setBackground(SNCharteGraphique.COULEUR_FOND);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Message d'erreur
    rafraichirMessageErreur();
    
    // Rafraichir les informations clients
    rafraichirCiviliteClient();
    rafraichirNomClient();
    rafraichirComplementNomClient();
    rafraichirLocalisationClient();
    rafraichirRueClient();
    rafraichirCodePostalClient();
    rafraichirVilleClient();
    rafraichirContactClient();
    rafraichirTelephoneClient();
    rafraichirFaxClient();
    rafraichirMailClient();
  }
  
  private void rafraichirMessageErreur() {
    if (getModele().getListeErreur().size() == 1) {
      lbLibelleErreur.setMessage(Message.getMessageImportant("Une erreur a été détectée sur les coordonnées du client."));
    }
    else {
      lbLibelleErreur.setMessage(
          Message.getMessageImportant(getModele().getListeErreur().size() + " erreurs ont été détectées sur les coordonnées du client."));
    }
    
    lbMessageErreur.setMessage(Message.getMessageImportant(getModele().getErreurEnCours().getLibelle()));
    
    if (getModele().getListeErreur().size() > 0) {
      lbLibelleErreur.setVisible(true);
      lbMessageErreur.setVisible(true);
    }
    else {
      lbLibelleErreur.setVisible(false);
      lbMessageErreur.setVisible(false);
    }
  }
  
  private void rafraichirCiviliteClient() {
    Client client = getModele().getClient();
    snCivilite.setSession(getModele().getSession());
    if (client.isProfessionel()) {
      lbCivilite.setVisible(false);
      snCivilite.setVisible(false);
    }
    else {
      snCivilite.setIdEtablissement(client.getId().getIdEtablissement());
      snCivilite.setAucunAutorise(true);
      snCivilite.charger(false);
      // Sélectionner la civilité
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getIdCivilite() != null) {
        snCivilite.setIdSelection(contact.getIdCivilite());
      }
      else {
        snCivilite.setSelection(null);
      }
    }
    
    lbCivilite.setVisible(true);
    snCivilite.setVisible(true);
    snCivilite.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_CIVILITE));
    if (snCivilite.isEnabled()) {
      snCivilite.requestFocusInWindow();
    }
  }
  
  private void rafraichirNomClient() {
    Client client = getModele().getClient();
    Adresse adresse = getModele().getAdresse();
    
    if (client == null || adresse == null) {
      return;
    }
    else {
      if (client.isParticulier()) {
        lbNom.setText(LIBELLE_NOM);
      }
      else {
        lbNom.setText(LIBELLE_RAISON_SOCIALE);
      }
      
      Contact contact = client.getContactPrincipal();
      
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        tfNom.setText(contact.getNom());
      }
      else if (adresse.getNom() != null) {
        tfNom.setText(adresse.getNom());
      }
      else {
        tfNom.setText("");
      }
      
      tfNom.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_NOM));
      if (tfNom.isEnabled()) {
        tfNom.requestFocusInWindow();
      }
    }
  }
  
  private void rafraichirComplementNomClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      if (client.isParticulier()) {
        lbComplementNom.setText(LIBELLE_PRENOM);
      }
      else {
        lbComplementNom.setText(LIBELLE_COMPLEMENT);
      }
      
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getPrenom() != null) {
        tfComplementNom.setText(contact.getPrenom());
      }
      else if (adresse != null && adresse.getComplementNom() != null) {
        tfComplementNom.setText(adresse.getComplementNom());
      }
      else {
        tfComplementNom.setText("");
      }
      
      tfComplementNom.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_PRENOM));
      if (tfComplementNom.isEnabled()) {
        tfComplementNom.requestFocusInWindow();
      }
    }
    
  }
  
  private void rafraichirLocalisationClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null) {
        tfLocalisation.setText(adresse.getLocalisation());
      }
      else {
        tfLocalisation.setText("");
      }
      tfLocalisation.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_LOCALISATION));
      if (tfLocalisation.isEnabled()) {
        tfLocalisation.requestFocusInWindow();
      }
    }
  }
  
  private void rafraichirRueClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null) {
        tfRue.setText(adresse.getRue());
      }
      else {
        tfRue.setText("");
      }
      
      tfRue.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_RUE));
      if (tfRue.isEnabled()) {
        tfRue.requestFocusInWindow();
      }
    }
  }
  
  private void rafraichirCodePostalClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null) {
        tfCodePostal.setText(adresse.getCodePostalFormate());
      }
      else {
        tfCodePostal.setText("");
      }
      tfCodePostal.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_CODE_POSTAL)
          || getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_VILLE_ERRONEE));
    }
    if (tfCodePostal.isEnabled()) {
      tfCodePostal.requestFocusInWindow();
    }
  }
  
  private void rafraichirVilleClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      String ville = null;
      if (client.getAdresse() != null) {
        ville = client.getAdresse().getVille();
      }
      
      // On le force en éditable afin de pouvoir initialiser le texte sinon setSelectedItem ne fonctionne pas correctement
      // A améliorer plus tard
      boolean editable = cbVille.isEditable();
      cbVille.setEditable(true);
      cbVille.setSelectedItem(ville);
      
      if (getModele().getListeCommune() != null && getModele().getListeCommune().getListeNomCommune() != null) {
        List<String> listeNomCommune = getModele().getListeCommune().getListeNomCommune();
        cbVille.setModel(new DefaultComboBoxModel(listeNomCommune.toArray()));
        if (listeNomCommune.size() > 0 && (ville == null || ville.isEmpty())) {
          cbVille.setSelectedIndex(0);
        }
        else {
          cbVille.setSelectedItem(ville);
        }
      }
      cbVille.setEditable(editable);
      cbVille.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_VILLE_ERRONEE)
          || getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_VILLE_MANQUANTE));
      cbVille.setEditable(cbVille.isEnabled());
    }
    if (cbVille.isEnabled()) {
      cbVille.requestFocusInWindow();
    }
  }
  
  private void rafraichirContactClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else if (client.isClientComptant()) {
      lbContact.setVisible(false);
      tfContact.setVisible(false);
      tfContact.setText("");
      tfContact.setEnabled(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNomComplet() != null) {
        tfContact.setText(contact.getNomComplet());
      }
      else {
        tfContact.setText("");
      }
      
      lbContact.setVisible(true);
      tfContact.setVisible(true);
      tfContact.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_CONTACT));
      if (tfContact.isEnabled()) {
        tfContact.requestFocusInWindow();
      }
    }
  }
  
  private void rafraichirTelephoneClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroTelephone1() != null) {
        tfTelephone.setText(contact.getNumeroTelephone1());
      }
      else if (client.getNumeroTelephone() != null) {
        tfTelephone.setText(client.getNumeroTelephone());
      }
      else {
        tfTelephone.setText("");
      }
      
      tfTelephone.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_TELEPHONE));
      if (tfTelephone.isEnabled()) {
        tfTelephone.requestFocusInWindow();
      }
    }
  }
  
  private void rafraichirFaxClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else if (client.isClientComptant()) {
      lbFax.setVisible(false);
      tfFax.setVisible(false);
      tfFax.setText("");
      tfFax.setEnabled(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroFax() != null) {
        tfFax.setText(contact.getNumeroFax());
      }
      else if (client.getNumeroFax() != null) {
        tfFax.setText(client.getNumeroFax());
      }
      else {
        tfFax.setText("");
      }
      
      lbFax.setVisible(true);
      tfFax.setVisible(true);
      tfFax.setEnabled(false);
    }
    tfFax.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_FAX));
    if (tfFax.isEnabled()) {
      tfFax.requestFocusInWindow();
    }
  }
  
  private void rafraichirMailClient() {
    Client client = getModele().getClient();
    if (client == null) {
      return;
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getEmail() != null) {
        tfEmail.setText(contact.getEmail());
      }
      else {
        tfEmail.setText("");
      }
      
      tfEmail.setEnabled(getModele().getErreurEnCours().equals(EnumErreurAdresse.ERREUR_MAIL));
      if (tfEmail.isEnabled()) {
        tfEmail.requestFocusInWindow();
      }
    }
  }
  
  // -- Méthodes interractives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        // On ne sort avec validation que si l'on a traité toutes les erreurs
        if (getModele().getListeErreur().size() == 0) {
          getModele().quitterAvecValidation();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierNomClient(tfNom.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfComplementNomFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierComplementNomClient(tfComplementNom.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRueFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierRueClient(tfRue.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLocalisationFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierLocalisationClient(tfLocalisation.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfCodePostalFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierCodePostalClient(tfCodePostal.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomCompletFocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void tfTelephoneFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierTelephoneClient(tfTelephone.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfEmailFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierMailClient(tfEmail.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFaxFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierFaxClient(tfFax.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbVilleItemStateChanged(ItemEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierVilleClient((String) cbVille.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbVilleFocusLost(FocusEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierVilleClient((String) cbVille.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCiviliteValueChanged(SNComposantEvent e) {
    try {
      if (isDonneesChargees()) {
        getModele().modifierCiviliteClient(snCivilite.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbLibelleErreur = new SNMessage();
    lbMessageErreur = new SNMessage();
    pnlCoordonneesClient = new SNPanelTitre();
    lbCivilite = new JLabel();
    snCivilite = new SNCivilite();
    lbNom = new JLabel();
    tfNom = new SNTexte();
    lbComplementNom = new JLabel();
    tfComplementNom = new SNTexte();
    lbLocalisation = new JLabel();
    tfRue = new SNTexte();
    lbRue = new JLabel();
    tfLocalisation = new SNTexte();
    lbVille = new JLabel();
    cbVille = new XRiComboBox();
    lbCodePostal = new JLabel();
    tfCodePostal = new SNTexte();
    lbContact = new JLabel();
    tfContact = new SNTexte();
    lbFax = new JLabel();
    tfFax = new SNTexte();
    lbEmail = new JLabel();
    tfEmail = new SNTexte();
    lbTelephone = new JLabel();
    tfTelephone = new SNTexte();
    
    // ======== this ========
    setTitle("Modifier les coordonn\u00e9es client erron\u00e9es");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(600, 300));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setBackground(new Color(238, 238, 210));
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setOpaque(true);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
        
        // ---- lbLibelleErreur ----
        lbLibelleErreur.setText("Erreur bloc adresse");
        lbLibelleErreur.setName("lbLibelleErreur");
        pnlContenu.add(lbLibelleErreur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbMessageErreur ----
        lbMessageErreur.setText("MessageErreur");
        lbMessageErreur.setName("lbMessageErreur");
        pnlContenu.add(lbMessageErreur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlCoordonneesClient ========
        {
          pnlCoordonneesClient.setBorder(new TitledBorder(null, "Coordonn\u00e9es client", TitledBorder.LEADING,
              TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlCoordonneesClient.setOpaque(false);
          pnlCoordonneesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
          pnlCoordonneesClient.setName("pnlCoordonneesClient");
          pnlCoordonneesClient.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 130, 0 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCivilite ----
          lbCivilite.setText("Civilit\u00e9");
          lbCivilite.setPreferredSize(new Dimension(100, 30));
          lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCivilite.setMinimumSize(new Dimension(100, 30));
          lbCivilite.setMaximumSize(new Dimension(100, 30));
          lbCivilite.setName("lbCivilite");
          pnlCoordonneesClient.add(lbCivilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snCivilite ----
          snCivilite.setName("snCivilite");
          snCivilite.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snCiviliteValueChanged(e);
            }
          });
          pnlCoordonneesClient.add(snCivilite, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbNom ----
          lbNom.setText("Raison sociale");
          lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNom.setPreferredSize(new Dimension(100, 30));
          lbNom.setMinimumSize(new Dimension(100, 30));
          lbNom.setMaximumSize(new Dimension(100, 30));
          lbNom.setName("lbNom");
          pnlCoordonneesClient.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNom ----
          tfNom.setBackground(Color.white);
          tfNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfNom.setEditable(false);
          tfNom.setMinimumSize(new Dimension(300, 30));
          tfNom.setPreferredSize(new Dimension(300, 30));
          tfNom.setEnabled(false);
          tfNom.setName("tfNom");
          tfNom.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNomFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfNom, new GridBagConstraints(1, 1, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 1, 1));
          
          // ---- lbComplementNom ----
          lbComplementNom.setText("Compl\u00e9ment");
          lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
          lbComplementNom.setPreferredSize(new Dimension(100, 30));
          lbComplementNom.setMinimumSize(new Dimension(100, 30));
          lbComplementNom.setMaximumSize(new Dimension(100, 30));
          lbComplementNom.setName("lbComplementNom");
          pnlCoordonneesClient.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfComplementNom ----
          tfComplementNom.setBackground(Color.white);
          tfComplementNom.setMinimumSize(new Dimension(300, 30));
          tfComplementNom.setPreferredSize(new Dimension(300, 30));
          tfComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfComplementNom.setEnabled(false);
          tfComplementNom.setName("tfComplementNom");
          tfComplementNom.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfComplementNomFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfComplementNom, new GridBagConstraints(1, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLocalisation ----
          lbLocalisation.setText("Adresse 1");
          lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLocalisation.setInheritsPopupMenu(false);
          lbLocalisation.setMaximumSize(new Dimension(100, 30));
          lbLocalisation.setMinimumSize(new Dimension(100, 30));
          lbLocalisation.setPreferredSize(new Dimension(100, 30));
          lbLocalisation.setName("lbLocalisation");
          pnlCoordonneesClient.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfRue ----
          tfRue.setBackground(Color.white);
          tfRue.setMinimumSize(new Dimension(300, 30));
          tfRue.setPreferredSize(new Dimension(300, 30));
          tfRue.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfRue.setEnabled(false);
          tfRue.setName("tfRue");
          tfRue.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfRueFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfRue, new GridBagConstraints(1, 3, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRue ----
          lbRue.setText("Adresse 2");
          lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRue.setName("lbRue");
          pnlCoordonneesClient.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLocalisation ----
          tfLocalisation.setBackground(Color.white);
          tfLocalisation.setMinimumSize(new Dimension(300, 30));
          tfLocalisation.setPreferredSize(new Dimension(300, 30));
          tfLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfLocalisation.setEnabled(false);
          tfLocalisation.setName("tfLocalisation");
          tfLocalisation.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfLocalisationFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfLocalisation, new GridBagConstraints(1, 4, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbVille ----
          lbVille.setText("Ville");
          lbVille.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbVille.setHorizontalAlignment(SwingConstants.RIGHT);
          lbVille.setName("lbVille");
          pnlCoordonneesClient.add(lbVille, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbVille ----
          cbVille.setBackground(new Color(171, 148, 79));
          cbVille.setFont(new Font("sansserif", Font.PLAIN, 14));
          cbVille.setMinimumSize(new Dimension(200, 30));
          cbVille.setPreferredSize(new Dimension(200, 30));
          cbVille.setEditable(true);
          cbVille.setEnabled(false);
          cbVille.setName("cbVille");
          cbVille.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbVilleItemStateChanged(e);
            }
          });
          cbVille.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              cbVilleFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(cbVille, new GridBagConstraints(3, 5, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodePostal ----
          lbCodePostal.setText("Code postal");
          lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodePostal.setName("lbCodePostal");
          pnlCoordonneesClient.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfCodePostal ----
          tfCodePostal.setBackground(Color.white);
          tfCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfCodePostal.setMinimumSize(new Dimension(55, 30));
          tfCodePostal.setPreferredSize(new Dimension(55, 30));
          tfCodePostal.setEnabled(false);
          tfCodePostal.setName("tfCodePostal");
          tfCodePostal.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfCodePostalFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfCodePostal, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbContact ----
          lbContact.setText("Contact");
          lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
          lbContact.setDisplayedMnemonicIndex(2);
          lbContact.setName("lbContact");
          pnlCoordonneesClient.add(lbContact, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfContact ----
          tfContact.setBackground(Color.white);
          tfContact.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfContact.setMinimumSize(new Dimension(300, 30));
          tfContact.setPreferredSize(new Dimension(300, 30));
          tfContact.setEnabled(false);
          tfContact.setName("tfContact");
          tfContact.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNomCompletFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfContact, new GridBagConstraints(1, 6, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFax ----
          lbFax.setText("Fax");
          lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFax.setMaximumSize(new Dimension(100, 30));
          lbFax.setMinimumSize(new Dimension(100, 30));
          lbFax.setPreferredSize(new Dimension(100, 30));
          lbFax.setName("lbFax");
          pnlCoordonneesClient.add(lbFax, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfFax ----
          tfFax.setBackground(Color.white);
          tfFax.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfFax.setPreferredSize(new Dimension(100, 30));
          tfFax.setMinimumSize(new Dimension(100, 30));
          tfFax.setEnabled(false);
          tfFax.setName("tfFax");
          tfFax.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfFaxFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfFax, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEmail ----
          lbEmail.setText("Email");
          lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
          lbEmail.setName("lbEmail");
          pnlCoordonneesClient.add(lbEmail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfEmail ----
          tfEmail.setBackground(Color.white);
          tfEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfEmail.setMinimumSize(new Dimension(250, 30));
          tfEmail.setPreferredSize(new Dimension(250, 30));
          tfEmail.setEnabled(false);
          tfEmail.setName("tfEmail");
          tfEmail.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfEmailFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfEmail, new GridBagConstraints(1, 7, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbTelephone ----
          lbTelephone.setText("T\u00e9l\u00e9phone");
          lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTelephone.setInheritsPopupMenu(false);
          lbTelephone.setMaximumSize(new Dimension(100, 30));
          lbTelephone.setMinimumSize(new Dimension(100, 30));
          lbTelephone.setPreferredSize(new Dimension(100, 30));
          lbTelephone.setName("lbTelephone");
          pnlCoordonneesClient.add(lbTelephone, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfTelephone ----
          tfTelephone.setBackground(Color.white);
          tfTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfTelephone.setMinimumSize(new Dimension(100, 30));
          tfTelephone.setPreferredSize(new Dimension(100, 30));
          tfTelephone.setEnabled(false);
          tfTelephone.setName("tfTelephone");
          tfTelephone.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfTelephoneFocusLost(e);
            }
          });
          pnlCoordonneesClient.add(tfTelephone, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlCoordonneesClient, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNMessage lbLibelleErreur;
  private SNMessage lbMessageErreur;
  private SNPanelTitre pnlCoordonneesClient;
  private JLabel lbCivilite;
  private SNCivilite snCivilite;
  private JLabel lbNom;
  private SNTexte tfNom;
  private JLabel lbComplementNom;
  private SNTexte tfComplementNom;
  private JLabel lbLocalisation;
  private SNTexte tfRue;
  private JLabel lbRue;
  private SNTexte tfLocalisation;
  private JLabel lbVille;
  private XRiComboBox cbVille;
  private JLabel lbCodePostal;
  private SNTexte tfCodePostal;
  private JLabel lbContact;
  private SNTexte tfContact;
  private JLabel lbFax;
  private SNTexte tfFax;
  private JLabel lbEmail;
  private SNTexte tfEmail;
  private JLabel lbTelephone;
  private SNTexte tfTelephone;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
