/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.correctionadresse;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.exploitation.personnalisation.civilite.Civilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adresse.EnumErreurAdresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.vente.commune.Commune;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue de signalement d'erreur sur un bloc adresse et de correction des informations erronnées.
 */
public class ModeleErreurAdresse extends AbstractModeleDialogue {
  
  // variables
  private Client client = null;
  private Adresse adresse = null;
  private List<EnumErreurAdresse> listeErreur = null;
  private EnumErreurAdresse erreurEnCours = null;
  private ListeCommune listeCommune = null;
  
  /**
   * Constructeur.
   */
  public ModeleErreurAdresse(SessionBase pSession, Client pClient, List<EnumErreurAdresse> pListeErreur) {
    super(pSession);
    client = pClient;
    adresse = pClient.getAdresse();
    listeErreur = pListeErreur;
    
    if (listeErreur == null || listeErreur.size() == 0) {
      quitterAvecAnnulation();
    }
    
    if (client.getContactPrincipal() == null) {
      quitterAvecAnnulation();
      throw new MessageErreurException(
          "Il est impossible d'utiliser ce client car il n'a pas de contact lié.\n Merci de contacter l'assistance");
    }
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    if (client == null || listeErreur == null) {
      listeErreur = new ArrayList<EnumErreurAdresse>();
      listeErreur.add(EnumErreurAdresse.ERREUR_NON_GEREE);
    }
    else {
      erreurEnCours = listeErreur.get(0);
    }
  }
  
  @Override
  public void chargerDonnees() {
    CritereCommune critereCommune = new CritereCommune();
    critereCommune.setCodePostal(client.getAdresse().getCodePostal());
    listeCommune = ListeCommune.charger(getIdSession(), critereCommune);
    
  }
  
  /**
   * Modifier la civilité du client.
   */
  public void modifierCiviliteClient(Civilite pCivilite) {
    // Tester si la valeur a changé
    IdCivilite idCiviliteClient = null;
    if (pCivilite != null) {
      idCiviliteClient = pCivilite.getId();
    }
    else {
      return;
    }
    
    if (Constantes.equals(client.getContactPrincipal().getIdCivilite(), idCiviliteClient)) {
      return;
    }
    
    // Mettre à jour le client
    client.getContactPrincipal().setIdCivilite(idCiviliteClient);
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier le nom du client.
   */
  public void modifierNomClient(String pNomClient) {
    if (pNomClient.trim().isEmpty()) {
      return;
    }
    
    // Tester si la valeur a changé
    pNomClient = Constantes.normerTexte(pNomClient);
    if (pNomClient.equals(adresse.getNom()) && pNomClient.equals(client.getContactPrincipal().getNom())) {
      return;
    }
    
    // Mettre à jour le client
    adresse.setNom(pNomClient);
    if (client.getContactPrincipal() != null) {
      client.getContactPrincipal().setNom(pNomClient);
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier le complément de nom du client et le prénom du contact.
   */
  public void modifierComplementNomClient(String pComplementNomClient) {
    if (pComplementNomClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pComplementNomClient = Constantes.normerTexte(pComplementNomClient);
    if (pComplementNomClient.equals(adresse.getNom()) && pComplementNomClient.equals(client.getContactPrincipal().getNom())) {
      return;
    }
    
    // Mettre à jour le client
    adresse.setComplementNom(pComplementNomClient);
    if (client.getContactPrincipal() != null) {
      client.getContactPrincipal().setPrenom(pComplementNomClient);
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier la rue du client.
   */
  public void modifierRueClient(String pRueClient) {
    if (pRueClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pRueClient = Constantes.normerTexte(pRueClient);
    if (pRueClient.equals(adresse.getRue())) {
      return;
    }
    
    // Mettre à jour le client
    adresse.setRue(pRueClient);
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier la localisation du client.
   */
  public void modifierLocalisationClient(String pLocalisationClient) {
    if (pLocalisationClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pLocalisationClient = Constantes.normerTexte(pLocalisationClient);
    if (pLocalisationClient.equals(adresse.getLocalisation())) {
      return;
    }
    
    // Mettre à jour le client
    adresse.setLocalisation(pLocalisationClient);
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier le code postal du client.
   */
  public void modifierCodePostalClient(String pCodePostalClient) {
    if (pCodePostalClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    int codePostal = Constantes.convertirTexteEnInteger(pCodePostalClient);
    if (codePostal == adresse.getCodePostal()) {
      return;
    }
    
    // Charger les villes ayant ce code postal
    if (pCodePostalClient.length() == 5) {
      // listeCommune = ListeCommune.chargerPourCodePostal(getSession().getIdSession(), pCodePostalClient);
    }
    else {
      listeCommune = null;
    }
    
    // Mettre à jour le client
    adresse.setCodePostal(codePostal);
    
    CritereCommune critereCommune = new CritereCommune();
    critereCommune.setCodePostal(client.getAdresse().getCodePostal());
    listeCommune = ListeCommune.charger(getIdSession(), critereCommune);
    if (listeCommune.size() > 0) {
      adresse.setVille(listeCommune.get(0).getNom());
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier la ville du client.
   */
  public void modifierVilleClient(String pVilleClient) {
    if (pVilleClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pVilleClient = Constantes.normerTexte(pVilleClient.toUpperCase());
    if (pVilleClient.equals(adresse.getVille())) {
      return;
    }
    
    // Mettre à jour le client
    adresse.setVille(pVilleClient);
    listeCommune = ListeCommune.charger(getIdSession());
    if (listeCommune.size() > 0) {
      Commune commune = listeCommune.getCommuneParVille(pVilleClient);
      if (commune != null) {
        adresse.setCodePostal(commune.getCodePostal());
      }
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier le mail du client.
   */
  public void modifierMailClient(String pMailClient) {
    if (pMailClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pMailClient = Constantes.normerTexte(pMailClient);
    if (pMailClient.equals(client.getContactPrincipal().getEmail())) {
      return;
    }
    
    // Mettre à jour le client
    if (client.getContactPrincipal() != null) {
      client.getContactPrincipal().setEmail(pMailClient);
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   * Modifier le numéro de téléphone du client.
   */
  public void modifierTelephoneClient(String pTelephoneClient) {
    if (pTelephoneClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pTelephoneClient = Constantes.normerTexte(pTelephoneClient);
    if (pTelephoneClient.equals(client.getNumeroTelephone())
        && pTelephoneClient.equals(client.getContactPrincipal().getNumeroTelephone1())) {
      return;
    }
    
    // Mettre à jour le client
    client.setNumeroTelephone(pTelephoneClient);
    if (client.getContactPrincipal() != null) {
      client.getContactPrincipal().setNumeroTelephone1(pTelephoneClient);
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  public void modifierFaxClient(String pFaxClient) {
    if (pFaxClient.trim().isEmpty()) {
      return;
    }
    // Tester si la valeur a changé
    pFaxClient = Constantes.normerTexte(pFaxClient);
    if (pFaxClient.equals(client.getNumeroFax()) && pFaxClient.equals(client.getContactPrincipal().getNumeroFax())) {
      return;
    }
    
    // Mettre à jour le client
    client.setNumeroFax(pFaxClient);
    if (client.getContactPrincipal() != null) {
      client.getContactPrincipal().setNumeroFax(pFaxClient);
    }
    
    // Rafraichir
    gererListeErreur();
    rafraichir();
  }
  
  /**
   *
   */
  private void gererListeErreur() {
    if (listeErreur != null && listeErreur.size() > 0) {
      listeErreur.remove(erreurEnCours);
      if (!listeErreur.isEmpty()) {
        erreurEnCours = listeErreur.get(0);
      }
    }
  }
  
  /**
   * Sauver les informations du client dans la base.
   */
  private void sauverClient() {
    // Vérifier les pré-requis
    if (client == null) {
      quitterAvecAnnulation();
      throw new MessageErreurException("Impossible de sauver le client car il est invalide.");
    }
    if (client.getContactPrincipal() == null) {
      quitterAvecAnnulation();
      throw new MessageErreurException("Impossible de sauver le client car il n'a pas de contact lié. Merci de contacter l'assistance");
    }
    
    // Sauver un client existant
    ManagerServiceClient.sauverClient(getSession().getIdSession(), client);
  }
  
  /**
   * On sauve avant de sortir
   * On ne peut quitter avec validation que si toutes les erreurs ont été traitées
   */
  @Override
  public void quitterAvecValidation() {
    if (listeErreur.size() == 0) {
      sauverClient();
      super.quitterAvecValidation();
    }
    else {
      rafraichir();
    }
  }
  
  // -- Accesseurs
  
  /**
   * Client en cours
   */
  public Client getClient() {
    return client;
  }
  
  /**
   * Adresse en erreur
   */
  public Adresse getAdresse() {
    return adresse;
  }
  
  /**
   * Type d'erreur à traiter
   */
  public List<EnumErreurAdresse> getListeErreur() {
    return listeErreur;
  }
  
  /**
   * Liste des communes correspondant au code postal du client
   */
  public ListeCommune getListeCommune() {
    return listeCommune;
  }
  
  /**
   * Erreur en cours de traitement. On traite les erreurs une par une.
   */
  public EnumErreurAdresse getErreurEnCours() {
    return erreurEnCours;
  }
  
}
