/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import ri.serien.desktop.metier.gescom.commun.consultationlignevente.ModeleConsultationLigneVente;
import ri.serien.desktop.metier.gescom.commun.consultationlignevente.VueConsultationLigneVente;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.gescom.commun.representant.Representant;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle de l'écran principal de la consultation de documents de ventes.
 */
public class ModeleConsultationDocumentVente extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Consultation de documents";
  public static final int MODE_AFFICHAGE_DOCUMENT = 0;
  public static final int MODE_AFFICHAGE_LIGNE = 1;
  public static final int NOMBRE_LIGNES_DEVIS = 15;
  public static final int LONGUEUR_MINI_EXPRESSION_RECHERCHEE = 3;
  public static final int MODE_AFFICHAGE_TABLE_STANDARD = 1;
  public static final int MODE_AFFICHAGE_TABLE_DETAIL = 2;
  public static final int NOMBRE_LIGNES_ARTICLES = 15;
  
  // Variables
  private Etablissement etablissement = null;
  private List<IdDocumentVente> listeIdDocumentVente = null;
  private int indexDocumentSelectionne = -1;
  private IdDocumentVente idDocumentSelectionne = null;
  private List<IdLigneVente> listeIdLigneVente = null;
  private int indexLigneSelectionne = -1;
  private IdLigneVente idLigneSelectionne = null;
  
  // Attributs pour la liste de document de vente
  private ListeEtablissement listeEtablissement = null;
  private ListeMagasin listeMagasin = null;
  private ListeModeExpedition listeModeExpedition = null;
  private UtilisateurGescom utilisateurGescom = null;
  private ListeDocumentVenteBase listeDocumentVenteBase = null;
  private ListeLigneVenteBase listeLigneVenteBase = null;
  private Map<LigneVente, DocumentVente> listeLigneVenteCharge;
  private Message titreResultat = null;
  private ClientBase clientBase = null;
  private IdMagasin idMagasin = null;
  private int numeroFacture = 0;
  private int numeroDocument = 0;
  private Date dateDebut = null;
  private Date dateFin = null;
  private IdArticle idArticle = null;
  private Chantier chantier = null;
  private EnumTypeDocumentVente typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
  private Contact contactPrincipal = null;
  private LigneVente ligneVenteSelectionnee = null;
  private String referenceClientDocument = "";
  private EnumTypeCompteClient typeClient = null;
  private boolean isFenetreArticleAffichee = false;
  private int modeAffichage = MODE_AFFICHAGE_DOCUMENT;
  private int indexLigneSelectionnee = -1;
  private ListeChantier listeChantier = null;
  
  // Attributs pour le détail d'un document de vente
  private DocumentVente documentVente = null;
  private Magasin magasinSortie = null;
  private ListeRepresentant listeRepresentant = null;
  private Representant representant = null;
  private ListeVendeur listeVendeur = null;
  private Vendeur vendeur = null;
  private Client client = null;
  private String nomclient = "";
  private ListeUnite listeUnite = null;
  private int modeAffichageTable = MODE_AFFICHAGE_TABLE_STANDARD;
  private ListeModeExpedition listeModesExpedition = null;
  private IdDocumentVente idDocumentVente = null;
  private ListeDocumentVenteBase listeDocumentAvoir = null;
  
  /**
   * Constructeur.
   */
  public ModeleConsultationDocumentVente(SessionBase pSession) {
    super(pSession);
    // Renseigner le titre de l'onglet
    getSession().getPanel().setName(TITRE);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
    // Se mettre en mode liste par défaut
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Initialisation pour la liste de documents
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    // Effacer le message
    titreResultat = null;
    // Renseigner les dates de début et de fin
    if (dateFin == null) {
      dateFin = new Date();
      dateDebut = Constantes.getDateDebutMoisPrecedent(dateFin);
    }
    
    // Initialisation pour le détail d'un document
    magasinSortie = null;
    representant = null;
    vendeur = null;
    client = null;
    listeDocumentAvoir = null;
    
  }
  
  /**
   * Récupére les informations nécessaires.
   */
  @Override
  public void chargerDonnees() {
    // Chargement de données pour la liste de documents
    // Charger la liste des modes d'expédition
    if (listeModeExpedition == null) {
      listeModeExpedition = ListeModeExpedition.charger(getIdSession());
      if (listeModeExpedition == null) {
        throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé dans la base de données courante.");
      }
    }
    
    // Charger l'utilisateur
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    // Charger la liste des magasins
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
      listeMagasin = listeMagasin.charger(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    }
    
    // Charger le magasin courant
    if (idMagasin == null) {
      idMagasin = utilisateurGescom.getIdMagasin();
    }
    
    // Charger l'établissement courant
    if (etablissement == null) {
      listeEtablissement = ListeEtablissement.charger(getIdSession());
      etablissement = listeEtablissement.getEtablissementParId(utilisateurGescom.getIdEtablissementPrincipal());
      setEtablissement(etablissement);
      chargerDonneesLieesEtablissement(etablissement.getId());
    }
    
  }
  
  /**
   * Fermer la boîte de dialogue en validant les modifications.
   * Les données modifiées sont conservées.
   */
  public void quitterAvecValidation() {
    getSession().fermerEcran();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  private void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), etablissement.getId());
    
    // Récupération de la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), pIdEtablissement);
    
    // Controler la validité du code magasin associé à l'utilisateur.
    idMagasin = utilisateurGescom.getIdMagasin();
    if (listeMagasin == null) {
      initialiserRecherche();
      throw new MessageErreurException("Il n'est pas possible de choisir cet établissement car il n'a pas de magasin associé défini");
    }
  }
  
  /**
   * Initialiser le résultat de la recherche (remet les valeurs par défaut aux filtres).
   */
  public void initialiserRecherche() {
    // Initialiser les filtres
    numeroDocument = 0;
    numeroFacture = 0;
    referenceClientDocument = "";
    typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    typeClient = null;
    clientBase = null;
    idArticle = null;
    chantier = null;
    dateFin = new Date();
    dateDebut = Constantes.getDateDebutMoisPrecedent(dateFin);
    
    // Effacer les données
    listeDocumentVenteBase = null;
    listeLigneVenteBase = null;
    setListeIdDocumentVente(null);
    setIndexDocumentSelectionne(-1);
    
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Recherche documents
   */
  public void rechercher() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeDocumentVenteBase != null) {
      taillePage = listeDocumentVenteBase.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeDocumentVenteBase = null;
    setIndexDocumentSelectionne(-1);
    rafraichir();
    
    // Renseigner les critères de recherche
    CritereDocumentVente critereDocumentVente = new CritereDocumentVente();
    critereDocumentVente.setIdEtablissement(idMagasin.getIdEtablissement());
    if (clientBase != null) {
      critereDocumentVente.setIdClient(clientBase.getId());
    }
    critereDocumentVente.setTypeDocumentVente(typeDocumentVente);
    critereDocumentVente.setNumeroFacture(numeroFacture);
    critereDocumentVente.setNumeroDocument(numeroDocument);
    if (idArticle != null) {
      critereDocumentVente.setCodeArticle(idArticle.getCodeArticle());
    }
    if (chantier != null) {
      critereDocumentVente.setChantier(chantier);
    }
    critereDocumentVente.setDateCreationFin(dateFin);
    critereDocumentVente.setDateCreationDebut(dateDebut);
    critereDocumentVente.setIdMagasin(idMagasin);
    critereDocumentVente.setTexteRechercheGenerique(referenceClientDocument);
    critereDocumentVente.setTypeClient(typeClient);
    
    // Charger la liste des identifiants des documents de vente correspondant au résultat de la recherche
    List<IdDocumentVente> listeIdDocumentVente =
        ManagerServiceDocumentVente.chargerListeIdDocumentVente(getIdSession(), critereDocumentVente);
    setListeIdDocumentVente(listeIdDocumentVente);
    
    // Créer le résulat de la recherche
    listeDocumentVenteBase = ListeDocumentVenteBase.creerListeNonChargee(listeIdDocumentVente);
    
    // Charger la première page
    ListeDocumentVenteBase listeDocumentVenteBaseCharge = listeDocumentVenteBase.chargerPremierePage(getIdSession(), taillePage);
    chargerComplement(listeDocumentVenteBaseCharge);
    
    // Construire le titre
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Recherche lignes
   */
  public void rechercherLignes() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeLigneVenteBase != null) {
      taillePage = listeLigneVenteBase.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeLigneVenteBase = null;
    setIndexLigneSelectionne(-1);
    rafraichir();
    
    // Critères de recherche des lignes
    CritereLigneVente criteresLigne = new CritereLigneVente();
    criteresLigne.setNumeroDocument(numeroDocument);
    criteresLigne.setTexteRechercheGenerique(referenceClientDocument);
    criteresLigne.ajouterTypeDocumentVente(typeDocumentVente);
    criteresLigne.setSansHistorique(true);
    criteresLigne.setTypeClient(typeClient);
    if (clientBase != null) {
      criteresLigne.setIdClientFacture(clientBase.getId());
    }
    criteresLigne.setIdArticle(idArticle);
    criteresLigne.setIdEtablissement(etablissement.getId());
    criteresLigne.setIdMagasin(idMagasin);
    criteresLigne.setDateCreationDebut(dateDebut);
    criteresLigne.setDateCreationFin(dateFin);
    criteresLigne.setChantier(chantier);
    
    // Charger les identifiants des lignes de ventes
    ListeIdLigneVente listeIdLigneVente = ManagerServiceDocumentVente.chargerListeIdLigneVente(getIdSession(), criteresLigne);
    // Si pas trouvé sur numéro de document on cherche dans les documents archivés
    if ((listeIdLigneVente == null || listeIdLigneVente.size() == 0) && numeroDocument > 0 && numeroDocument <= 999999) {
      criteresLigne.setIdDocument(IdDocumentVente.getInstanceGenerique(etablissement.getId(),
          EnumCodeEnteteDocumentVente.COMMANDE_OU_BON_ORIGINE, numeroDocument, 0, 0));
      listeIdLigneVente = ManagerServiceDocumentVente.chargerListeIdLigneVente(getIdSession(), criteresLigne);
    }
    listeLigneVenteBase = ListeLigneVenteBase.creerListeNonChargee(listeIdLigneVente);
    
    if (listeIdLigneVente != null) {
      setListeIdLigneVente(listeIdLigneVente);
      
      // Charger la première page
      listeLigneVenteBase.chargerPremierePage(getIdSession(), taillePage);
    }
    
    // Construire le titre
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Construction du titre de la liste suivant le résultat de recherche
   */
  private void construireTitre() {
    if (isModeAffichageDocument()) {
      if (listeDocumentVenteBase == null) {
        titreResultat = Message.getMessageNormal("Documents de ventes correspondant à votre recherche");
      }
      else if (listeDocumentVenteBase.size() == 1) {
        titreResultat = Message.getMessageNormal("Document de ventes correspondant à votre recherche (1)");
      }
      else if (listeDocumentVenteBase.size() > 1) {
        titreResultat =
            Message.getMessageNormal("Documents de ventes correspondant à votre recherche (" + listeDocumentVenteBase.size() + ")");
      }
      else {
        titreResultat = Message.getMessageImportant("Aucun document de ventes ne correspond à votre recherche");
      }
    }
    else {
      if (listeLigneVenteBase == null) {
        titreResultat = Message.getMessageNormal("Lignes de ventes correspondant à votre recherche");
      }
      else if (listeLigneVenteBase.size() == 1) {
        titreResultat = Message.getMessageNormal("Ligne de ventes correspondant à votre recherche (1)");
      }
      else if (listeLigneVenteBase.size() > 1) {
        titreResultat = Message.getMessageNormal("Lignes de ventes correspondant à votre recherche (" + listeLigneVenteBase.size() + ")");
      }
      else {
        titreResultat = Message.getMessageImportant("Aucune ligne de ventes ne correspond à votre recherche");
      }
    }
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageDocumentVenteAffiche(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeDocumentVenteBase != null) {
      ListeDocumentVenteBase listeDocumentVenteBaseCharge =
          listeDocumentVenteBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
      // Charger les informations complémentaires
      chargerComplement(listeDocumentVenteBaseCharge);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  public void modifierPlageLigneVenteAffiche(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeLigneVenteBase != null) {
      ListeLigneVenteBase listeLigneVenteBaseCharge =
          listeLigneVenteBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
      // Charger les informations complémentaires
      chargerComplementLigne(listeLigneVenteBaseCharge);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  public void chargerLignesArticlesDocument(DocumentVente pDocument) {
    lireLignesListeDocument(pDocument);
  }
  
  /**
   * Charger les informations complémentaires d'une liste de document de ventes.
   */
  private void chargerComplement(ListeDocumentVenteBase listeDocumentVenteBaseCharge) {
    if (listeDocumentVenteBaseCharge == null) {
      return;
    }
    
    // Lister les identifiants des clients à charger
    ListeIdClient listeIdClient = new ListeIdClient();
    for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
      IdClient idClient = documentVenteBase.getIdClientFacture();
      if (idClient != null && !listeIdClient.contains(idClient)) {
        listeIdClient.add(documentVenteBase.getIdClientFacture());
      }
    }
    
    // Charger les clients en complément
    if (listeIdClient.size() > 0) {
      ListeClientBase listeClientBase = ManagerServiceClient.chargerListeClientBase(getIdSession(), listeIdClient);
      
      // Mettre le client dans le complément du document de ventes
      for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
        ClientBase clientBase = listeClientBase.getClientBaseParId(documentVenteBase.getIdClientFacture());
        documentVenteBase.setComplement(clientBase);
      }
    }
  }
  
  /**
   * Charger les informations complémentaires d'une liste de lignes de ventes.
   */
  private void chargerComplementLigne(ListeLigneVenteBase pListeLigneVenteBaseCharge) {
    if (pListeLigneVenteBaseCharge == null) {
      return;
    }
  }
  
  /**
   * Modifie le type de documents à rechercher selon le choix de la combo.
   */
  public void modifierTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    if (Constantes.equals(typeDocumentVente, pTypeDocumentVente)) {
      return;
    }
    
    if (pTypeDocumentVente != null) {
      typeDocumentVente = pTypeDocumentVente;
    }
    else {
      typeDocumentVente = EnumTypeDocumentVente.NON_DEFINI;
    }
    
    rafraichir();
  }
  
  /**
   * Modifie le magasin des documents à rechercher selon le choix de la combo.
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    idMagasin = pIdMagasin;
    rafraichir();
  }
  
  /**
   * Modifie l'établissement des documents à rechercher selon le choix de la combo.
   */
  public void modifierEtablissement(Etablissement pEtablissement) {
    if (Constantes.equals(etablissement, pEtablissement)) {
      return;
    }
    etablissement = pEtablissement;
    setEtablissement(etablissement);
    chargerDonneesLieesEtablissement(etablissement.getId());
    
    // La méthode rafraichir() est appelée par initialiserRecherche
    initialiserRecherche();
  }
  
  /**
   * Modifie le numéro du document à rechercher.
   */
  public void modifierNumeroDocument(String pNumeroDocument) {
    // Convertir la saisie en nombre
    int numero = 0;
    try {
      pNumeroDocument = Constantes.normerTexte(pNumeroDocument);
      if (!pNumeroDocument.isEmpty()) {
        if (pNumeroDocument.length() > 7) {
          throw new MessageErreurException("Le numéro saisi est trop long");
        }
        else {
          numero = Integer.parseInt(pNumeroDocument);
        }
      }
    }
    catch (NumberFormatException e) {
      numero = 0;
    }
    
    if (numeroDocument == numero) {
      return;
    }
    
    numeroDocument = numero;
    numeroFacture = numero;
    
    if (numero > 0) {
      rechercher();
    }
    rafraichir();
  }
  
  /**
   * Modifie l'article à rechercher.
   */
  public void modifierArticle(IdArticle pIdArticle) {
    if (pIdArticle != null && Constantes.equals(pIdArticle, idArticle)) {
      return;
    }
    idArticle = pIdArticle;
    
    rafraichir();
  }
  
  /**
   * Modifie le chantier à rechercher.
   */
  public void modifierChantier(Chantier pChantier) {
    chantier = pChantier;
    rafraichir();
  }
  
  /**
   * Modifie le client à rechercher
   */
  public void modifierClient(ClientBase pClientBase) {
    if (Constantes.equals(clientBase, pClientBase)) {
      return;
    }
    
    clientBase = pClientBase;
    rafraichir();
  }
  
  /**
   * Modifie la référence à rechercher
   */
  public void modifierReferenceClientDocument(String pReferenceClientDocument) {
    pReferenceClientDocument = Constantes.normerTexte(pReferenceClientDocument);
    if (Constantes.equals(referenceClientDocument, pReferenceClientDocument)) {
      return;
    }
    referenceClientDocument = pReferenceClientDocument;
    rafraichir();
  }
  
  /**
   * Modifier la date de début.
   */
  public void modifierDateCreationDebut(Date pDate) {
    if (Constantes.equals(pDate, dateDebut)) {
      return;
    }
    dateDebut = pDate;
  }
  
  /**
   * Modifier la date de fin.
   */
  public void modifierDateCreationFin(Date pDate) {
    if ((pDate == null) || Constantes.equals(pDate, dateFin)) {
      return;
    }
    dateFin = pDate;
  }
  
  /**
   * Modifier le type de recherche pour les clients
   */
  public void modifierTypeClient(EnumTypeCompteClient pType) {
    if (Constantes.equals(pType, typeClient)) {
      return;
    }
    typeClient = pType;
    rafraichir();
  }
  
  /**
   * Modifier le type d'affichage de la liste de résultats
   */
  public void modifierModeAffichage(int pModeAffichage) {
    if (modeAffichage == pModeAffichage) {
      return;
    }
    modeAffichage = pModeAffichage;
    rafraichir();
  }
  
  /**
   * Modifier le document sélectionné par l'utilisateur
   */
  public void modifierDocumentSelectionne(int pIndexDocumentSelectionne) {
    if (pIndexDocumentSelectionne == getIndexDocumentSelectionne()) {
      return;
    }
    
    setIndexDocumentSelectionne(pIndexDocumentSelectionne);
    rafraichir();
  }
  
  /**
   * Modifier la ligne sélectionnée par l'utilisateur
   */
  public void modifierLigneSelectionne(int pIndexLigneSelectionne) {
    if (pIndexLigneSelectionne == getIndexLigneSelectionne()) {
      return;
    }
    
    setIndexLigneSelectionne(pIndexLigneSelectionne);
    rafraichir();
  }
  
  /**
   * retourne la provenance de l'article : rien=normal, spécial, direct usine.
   */
  public String retournerProvenanceArticle(LigneVente pLigne, DocumentVente pDocument) {
    String provenance = "";
    if (pLigne.isLigneArticleSpecial()) {
      provenance = "Article spécial";
    }
    else if (pDocument.isDirectUsine()) {
      provenance = "Direct usine";
    }
    return provenance;
  }
  
  /**
   * Lecture des lignes du document pour la liste
   */
  private void lireLignesListeDocument(DocumentVente pDocumentVente) {
    if (idArticle == null) {
      return;
    }
    CritereLigneVente criteres = new CritereLigneVente();
    criteres.setNumeroDocument(pDocumentVente.getId().getNumero());
    criteres.setIdArticle(idArticle);
    List<IdLigneVente> listeIdLigneVente = ManagerServiceDocumentVente.chargerIdLigneVenteParCodeArticle(getIdSession(), criteres);
    
    if (listeIdLigneVente == null) {
      return;
    }
    
    ListeLigneVente listeLigneVente = new ListeLigneVente();
    criteres = new CritereLigneVente();
    for (IdLigneVente idLigneDocument : listeIdLigneVente) {
      criteres.setNumeroLigne(idLigneDocument.getNumeroLigne());
      
      ListeLigneVente lignesArticle =
          ManagerServiceDocumentVente.chargerListeLigneVente(getIdSession(), pDocumentVente.getId(), criteres, pDocumentVente.isTTC());
      
      listeLigneVente.add(lignesArticle.get(0));
    }
    
    pDocumentVente.setListeLignesArticles(listeLigneVente);
  }
  
  /**
   * Mettre à jour la structure de stockage des lignes à afficher
   */
  private void modifierListeLigne() {
    Comparator<LigneVente> keyComparator = new Comparator<LigneVente>() {
      @Override
      public int compare(LigneVente int1, LigneVente int2) {
        return int2.getId().getNumero().compareTo(int1.getId().getNumero());
      }
    };
    if (listeLigneVenteCharge == null) {
      listeLigneVenteCharge = new TreeMap<LigneVente, DocumentVente>(keyComparator);
    }
    // chargement de la hashMap qui contient les lignes
    for (int i = 0; i < listeDocumentVenteBase.size(); i++) {
      if (listeDocumentVenteBase.get(i).isCharge()) {
        DocumentVente document = new DocumentVente(listeDocumentVenteBase.get(i).getId());
        document.setIdMagasin(listeDocumentVenteBase.get(i).getIdMagasin());
        document.setDateCreation(listeDocumentVenteBase.get(i).getDateCreation());
        document.setIdModeExpedition(listeDocumentVenteBase.get(i).geIdModeExpedition());
        if (document.getListeLigneVente() == null || document.getListeLigneVente().size() == 0) {
          chargerLignesArticlesDocument(document);
        }
        for (int j = 0; j < document.getListeLigneVente().size(); j++) {
          LigneVente ligneVente = document.getListeLigneVente().get(j);
          if (ligneVente != null) {
            listeLigneVenteCharge.put(ligneVente, document);
          }
        }
      }
    }
  }
  
  /**
   * Chargement de données pour le détail d'un document
   */
  private void initialiserDonnéesDetailDocument() {
    // Lister les unités
    if (listeUnite == null) {
      listeUnite = new ListeUnite();
      listeUnite = listeUnite.charger(getIdSession());
    }
    
    // Récupération de la liste des modes d'expédition
    if (listeModesExpedition == null) {
      listeModesExpedition = ListeModeExpedition.charger(getIdSession());
      if (listeModesExpedition == null) {
        throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé dans la bibliothèque courante.");
      }
    }
    
    // Récupérer l'id du document de ventes à charger
    idDocumentVente = null;
    if (getIdDocumentVenteSelectionne() != null) {
      idDocumentVente = getIdDocumentVenteSelectionne();
    }
    else {
      IdLigneVente idLigne = getIdLigneVenteSelectionne();
      if (idLigne != null) {
        idDocumentVente = IdDocumentVente.getInstanceHorsFacture(idLigne.getIdEtablissement(), idLigne.getCodeEntete(),
            idLigne.getNumero(), idLigne.getSuffixe());
      }
    }
    
    if (idDocumentVente == null) {
      throw new MessageErreurException("Aucun document de vente n'est sélectionné.");
    }
    
    // Charger le document de ventes
    documentVente = chargerDocument(idDocumentVente);
    if (documentVente == null) {
      throw new MessageErreurException("Impossible de charger le document de ventes " + idDocumentVente);
    }
    
    // Charger le magasin
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
      listeMagasin = listeMagasin.charger(getIdSession(), documentVente.getId().getIdEtablissement());
    }
    if (documentVente.getIdMagasin() != null || listeMagasin != null) {
      magasinSortie = listeMagasin.getMagasinParId(documentVente.getIdMagasin());
    }
    
    // Charger le représentant
    if (listeRepresentant == null) {
      listeRepresentant = new ListeRepresentant();
      listeRepresentant = listeRepresentant.charger(getIdSession(), documentVente.getId().getIdEtablissement());
    }
    representant = listeRepresentant.get(documentVente.getIdRepresentant1());
    
    // Charger le vendeur
    if (listeVendeur == null) {
      listeVendeur = new ListeVendeur();
      listeVendeur = listeVendeur.charger(getIdSession(), documentVente.getId().getIdEtablissement());
    }
    vendeur = listeVendeur.get(documentVente.getIdVendeur());
    
    // Charger le client facturé
    client = ManagerServiceClient.chargerClient(getIdSession(), documentVente.getIdClientFacture());
    
    // Renseigner le titre
    if (documentVente.isFacture() && documentVente.getId().getNumeroFacture() == null) {
      setTitreEcran(documentVente.getTitre());
    }
    else {
      setTitreEcran(documentVente.getTitre());
    }
  }
  
  /**
   * Afficher le détail du document sélectionné
   */
  public void afficherDetailDocument() {
    if (getIdDocumentVenteSelectionne() == null && getIdLigneVenteSelectionne() == null) {
      throw new MessageErreurException("Aucun documents de vente n'est sélectionné.");
    }
    initialiserDonnéesDetailDocument();
    setIdLigneSelectionne(null);
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    rafraichir();
  }
  
  /**
   * Afficher le détail du document de la ligne sélectionnée
   */
  public void afficherDetailLigne() {
    if (getIdDocumentVenteSelectionne() == null && getIdLigneVenteSelectionne() == null) {
      throw new MessageErreurException("Aucun documents de vente n'est sélectionné.");
    }
    initialiserDonnéesDetailDocument();
    setIdDocumentSelectionne(null);
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    rafraichir();
  }
  
  /**
   * Modifier le type de recherche : mode documents ou mode lignes
   */
  public void modifierModeRecherche(int pMode) {
    modeAffichage = pMode;
    construireTitre();
    rafraichir();
  }
  
  /**
   * Retourne la quantité restant à prendre.
   */
  public BigDecimal retournerQuantiteRestante(LigneVente pLigne) {
    if (pLigne != null) {
      if (pLigne.isLigneCommentaire()) {
        if ((documentVente.isFacture() && pLigne.getTopPersonnalisation5().isEmpty())) {
          return BigDecimal.ZERO;
        }
        return BigDecimal.ONE;
      }
      // La valeur n'est pas juste dans certains cas comme avecc les plaques de platres (problème d'arrondi)
      BigDecimal reste = pLigne.getQuantiteDocumentOrigineUC().subtract(pLigne.getQuantiteDejaExtraiteUC());
      if (reste.compareTo(pLigne.getQuantiteDocumentOrigineUC()) <= 0) {
        return reste;
      }
    }
    return BigDecimal.ZERO;
  }
  
  /**
   * Retourne l'état de livraison, commande ou facturation d'une ligne
   */
  public String retournerEtatArticle(LigneVente pLigne) {
    StringBuffer etatArticle = new StringBuffer("");
    if (documentVente.isDevis()) {
      return "";
    }
    if (pLigne.isLigneArticleSpecial()) {
      etatArticle.append("Article spécial - ");
    }
    if (pLigne.getQuantiteUCV().compareTo(BigDecimal.ZERO) < 0) {
      if (pLigne.isLignePalette()) {
        etatArticle.append("Déconsignation");
      }
      else {
        etatArticle.append("Retour");
      }
    }
    else {
      if (pLigne.isLignePalette()) {
        etatArticle.append("Consignation ");
      }
      else {
        if (documentVente.isCommande()) {
          // livraison
          if (documentVente.isLivraison()) {
            if (documentVente.getEtatExtraction().isPartielle() && retournerQuantiteRestante(pLigne).compareTo(BigDecimal.ZERO) > 0) {
              etatArticle.append("Partiellement livré");
            }
            else if (documentVente.getEtatExtraction().isComplete()
                || retournerQuantiteRestante(pLigne).compareTo(BigDecimal.ZERO) == 0) {
              etatArticle.append("Livré");
            }
            else {
              etatArticle.append("Non livré");
            }
          }
          else {
            // livraison
            if (documentVente.getEtatExtraction().isPartielle() && retournerQuantiteRestante(pLigne).compareTo(BigDecimal.ZERO) > 0) {
              etatArticle.append("Partiellement enlevé");
            }
            else if (documentVente.getEtatExtraction().isComplete()
                || retournerQuantiteRestante(pLigne).compareTo(BigDecimal.ZERO) == 0) {
              etatArticle.append("Enlevé");
            }
            else {
              etatArticle.append("Non enlevé");
            }
          }
        }
        else {
          if (documentVente.isLivraison()) {
            etatArticle.append("Livré");
          }
          else {
            etatArticle.append("Enlevé");
          }
        }
        
        // facturation
        if (documentVente.getEtat().equals(EnumEtatBonDocumentVente.COMPTABILISE)
            || documentVente.getEtat().equals(EnumEtatBonDocumentVente.FACTURE)) {
          etatArticle.append(" - Facturé");
        }
        else {
          etatArticle.append(" - Non facturé");
        }
      }
      
      // ligne achat liée
      if (pLigne.getIdLigneAchatLiee() != null) {
        etatArticle.append(" - Commande d'achat n° " + pLigne.getIdLigneAchatLiee().getIdDocumentAchat().toString());
      }
    }
    return etatArticle.toString();
  }
  
  /**
   * Charge toutes les informations relatives à un document.
   */
  private DocumentVente chargerDocument(IdDocumentVente pIdDocumentVente) {
    // Lecture de l'entête
    DocumentVente documentVente = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), pIdDocumentVente);
    
    // Lecture du règlement sauf sans le cas d'un devis
    if (!documentVente.isDevis()) {
      Date dateTraitementDocument = new Date();
      dateTraitementDocument = getEtablissement().getMoisEnCoursMax();
      ListeReglement listeReglement =
          ManagerServiceDocumentVente.chargerListeReglement(getSession().getIdSession(), documentVente.getId(), dateTraitementDocument);
      documentVente.setListeReglement(listeReglement);
      if (listeReglement != null) {
        listeReglement.initialiserInfoDocument(documentVente);
      }
    }
    
    // Charge la liste des avoirs qui ont pu être créé à partir de ce document
    List<IdDocumentVente> listeIdDocumentAvoir =
        ManagerServiceDocumentVente.chargerListeIdDocumentAvoir(getSession().getIdSession(), pIdDocumentVente);
    if (listeIdDocumentAvoir != null) {
      listeDocumentAvoir = ManagerServiceDocumentVente.chargerListeAvoir(getIdSession(), listeIdDocumentAvoir);
    }
    else {
      listeDocumentAvoir = null;
    }
    
    // Lecture des lignes
    lireLignesDocument(documentVente);
    return documentVente;
  }
  
  /**
   * Charger les informations d'un chantier
   */
  public Chantier chargerChantier(IdChantier pIdChantier) {
    if (pIdChantier == null) {
      return null;
    }
    return ManagerServiceClient.chargerChantier(getIdSession(), pIdChantier);
  }
  
  /**
   * Lecture des lignes du document.
   */
  public void lireLignesDocument(DocumentVente pDocumentVente) {
    ListeLigneVente lignesArticle = null;
    
    // On vide les listes
    pDocumentVente.viderListes();
    CritereLigneVente criteres = new CritereLigneVente();
    
    // Il très important que le paramètre indiquant le type du client (TTC/HT) soit à "false" car les lignes des documents sont
    // affichées systématiquement en HT. Si on met "true" le montant HT de la ligne sera calculé à partir du TTC et donc il y aura une
    // différence.
    lignesArticle = ManagerServiceDocumentVente.chargerListeLigneVente(getIdSession(), pDocumentVente.getId(), criteres, false);
    
    // On ajoute les lignes lues dans la classe Document
    for (LigneVente ligne : lignesArticle) {
      // Initialisation du taux de TVA de la ligne
      ligne.initialiserTauxTVA(pDocumentVente, null);
      
      // On lit s'il y a une ligne d'achat liée à la ligne de vente
      ligne.setIdLigneAchatLiee(ManagerServiceClient.chargerLienLigneVenteLigneAchat(getIdSession(), ligne.getId()));
      
      // En fonction de son état on stocke la ligne dans la liste qui va bien
      if (ligne.isAnnulee()) {
        pDocumentVente.getListeLigneVenteSupprimee().add(ligne);
      }
      else {
        pDocumentVente.getListeLigneVente().add(ligne);
      }
    }
    
    // Traitements post-lecture des lignes
    // Les regroupements
    pDocumentVente.actualiserRegroupement(getIdSession());
  }
  
  /**
   * Génère un message sur l'état des réglements d'un document.
   */
  public String genererMessageReglement() {
    String messageReglement = "";
    if (documentVente != null && documentVente.getListeReglement() != null && documentVente.getListeReglement().getEtatReglement() != null
        && client != null) {
      switch (documentVente.getListeReglement().getEtatReglement()) {
        case REGLE:
          messageReglement = "Document entièrement réglé ";
          break;
        case PARTIELLEMENT_REGLE:
          // Si on a payé avec un avoir supérieur au montant, le restant dû apparait négatif, en fait il faut dire entièrement réglé.
          if (documentVente.getListeReglement().getMontantTotalARegler()
              .subtract(documentVente.getListeReglement().getMontantTotalCalcule()).compareTo(BigDecimal.ZERO) <= 0) {
            messageReglement = "Document entièrement réglé ";
          }
          // S'il y a un montant restant à payer
          else {
            // Calcul du montant restant à payer
            BigDecimal montantRestant = documentVente.getListeReglement().getMontantTotalARegler()
                .subtract(documentVente.getListeReglement().getMontantTotalCalcule());
            messageReglement =
                "<html>Document partiellement réglé.<br>Restant dû en TTC : " + Constantes.formater(montantRestant, true) + "</html>";
          }
          break;
        case NON_REGLE:
          messageReglement = "Document non réglé";
          break;
      }
    }
    else {
      messageReglement = "";
    }
    return messageReglement;
  }
  
  /**
   * Afficher le détail de la ligne sélectionnée
   */
  public void afficherDetailCompletLigne() {
    if (documentVente == null) {
      return;
    }
    if (!ligneVenteSelectionnee.isLigneCommentaire()) {
      ligneVenteSelectionnee = chargerLigne(ligneVenteSelectionnee.getId());
      
      ModeleConsultationLigneVente modele = new ModeleConsultationLigneVente(getSession(), documentVente, ligneVenteSelectionnee);
      VueConsultationLigneVente vue = new VueConsultationLigneVente(modele);
      vue.afficher();
    }
  }
  
  private LigneVente chargerLigne(IdLigneVente pIdLigneVente) {
    for (LigneVente ligneVente : documentVente.getListeLigneVente()) {
      if (ligneVente.getId().equals(pIdLigneVente)) {
        ligneVenteSelectionnee = ligneVente;
        return ligneVenteSelectionnee;
      }
    }
    return null;
  }
  
  /**
   * Permutter l'affichage de la table.
   */
  public void permuterAffichageTable() {
    if (modeAffichageTable == MODE_AFFICHAGE_TABLE_STANDARD) {
      modeAffichageTable = MODE_AFFICHAGE_TABLE_DETAIL;
    }
    else {
      modeAffichageTable = MODE_AFFICHAGE_TABLE_STANDARD;
    }
    rafraichir();
  }
  
  /**
   * Charger le document qui précède le document affiché dans la liste des documents trouvés
   */
  public void afficherDocumentPrecedent() {
    if (isPremierDocument()) {
      return;
    }
    if (getIndexLigneSelectionne() < 0) {
      setIndexDocumentSelectionne(getIndexDocumentSelectionne() - 1);
    }
    else {
      setIndexLigneSelectionne(getIndexLigneSelectionne() - 1);
    }
    initialiserDonnéesDetailDocument();
    rafraichir();
  }
  
  /**
   * Affiche les document stocké liée au document de vente
   */
  public void afficherDocumentStocke() {
    CritereDocumentStocke critere = new CritereDocumentStocke();
    critere.setIdEtablissement(getEtablissement().getId());
    // Vérifie si l'IdDocumlentVenteSelectionne n'est pas à null, si oui, cela veux dire que l'on est en recherche de lignes de vente
    if (getIdDocumentVenteSelectionne() != null) {
      critere.setIdDocumentVente(getIdDocumentVenteSelectionne());
    }
    // Recherche le numéro de facture via le document de vente
    else {
      IdDocumentVente numeroFacture;
      if (documentVente.getId().getNumeroFacture() != null) {
        numeroFacture = IdDocumentVente.getInstancePourFacture(getEtablissement().getId(), documentVente.getId().getNumeroFacture());
      }
      else {
        numeroFacture = IdDocumentVente.getInstancePourFacture(getEtablissement().getId(), documentVente.getNumeroFacture());
      }
      critere.setIdDocumentVente(numeroFacture);
    }
    ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
    VueDocumentStocke vue = new VueDocumentStocke(modele);
    vue.afficher();
  }
  
  /**
   * Charger le document qui suit le document affiché dans la liste des documents trouvés
   */
  public void afficherDocumentSuivant() {
    if (isDernierDocument()) {
      return;
    }
    if (getIndexLigneSelectionne() < 0) {
      setIndexDocumentSelectionne(getIndexDocumentSelectionne() + 1);
    }
    else {
      setIndexLigneSelectionne(getIndexLigneSelectionne() + 1);
    }
    initialiserDonnéesDetailDocument();
    rafraichir();
  }
  
  public void valider() {
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    setTitreEcran(TITRE);
    rafraichir();
  }
  
  // -- Accesseurs
  
  /**
   * Liste des documents de ventes retournée par la recherche.
   */
  public List<IdDocumentVente> getListeIdDocumentVente() {
    return listeIdDocumentVente;
  }
  
  /**
   * Modifier la liste des documents de vente retournée par la recherche.
   */
  public void setListeIdDocumentVente(List<IdDocumentVente> pListeIdDocumentVente) {
    listeIdDocumentVente = pListeIdDocumentVente;
  }
  
  /**
   * Indice du document de ventes sélectionné.
   */
  public int getIndexDocumentSelectionne() {
    return indexDocumentSelectionne;
  }
  
  /**
   * Indice de la ligne sélectionnée.
   */
  public int getIndexLigneSelectionne() {
    return indexLigneSelectionne;
  }
  
  /**
   * Modifier l'index du document sélectionné à partir de son identifiant
   */
  public void setIdDocumentSelectionne(IdDocumentVente pIdDocumentSelectionne) {
    idLigneSelectionne = null;
    idDocumentSelectionne = pIdDocumentSelectionne;
  }
  
  /**
   * Modifier l'indice du document de ventes sélectionné.
   */
  public void setIndexDocumentSelectionne(int pIndexDocumentSelectionne) {
    if (pIndexDocumentSelectionne < 0 || pIndexDocumentSelectionne >= listeIdDocumentVente.size()) {
      indexDocumentSelectionne = -1;
    }
    indexLigneSelectionne = -1;
    indexDocumentSelectionne = pIndexDocumentSelectionne;
  }
  
  /**
   * Identifiant du document de ventes sélectionné.
   */
  public IdDocumentVente getIdDocumentVenteSelectionne() {
    if (listeIdDocumentVente != null && indexDocumentSelectionne >= 0 && indexDocumentSelectionne < listeIdDocumentVente.size()) {
      idDocumentSelectionne = listeIdDocumentVente.get(indexDocumentSelectionne);
    }
    return idDocumentSelectionne;
  }
  
  /**
   * Liste des lignes de ventes retournée par la recherche.
   */
  public List<IdLigneVente> getListeIdLigneVente() {
    return listeIdLigneVente;
  }
  
  /**
   * Modifier la liste des lignes de vente retournée par la recherche.
   */
  public void setListeIdLigneVente(List<IdLigneVente> pListeIdLigneVente) {
    listeIdLigneVente = pListeIdLigneVente;
  }
  
  /**
   * Modifier l'index de la ligne sélectionnée à partir de son identifiant
   */
  public void setIdLigneSelectionne(IdLigneVente pIdLigneSelectionne) {
    idDocumentSelectionne = null;
    idLigneSelectionne = pIdLigneSelectionne;
  }
  
  /**
   * Modifier l'indice de la ligne de ventes sélectionnée.
   */
  public void setIndexLigneSelectionne(int pIndexLigneSelectionne) {
    if (pIndexLigneSelectionne < 0 || pIndexLigneSelectionne >= listeIdLigneVente.size()) {
      indexLigneSelectionne = -1;
    }
    indexDocumentSelectionne = -1;
    indexLigneSelectionne = pIndexLigneSelectionne;
  }
  
  /**
   * Identifiant du document de ventes sélectionné.
   */
  public IdLigneVente getIdLigneVenteSelectionne() {
    if (listeIdLigneVente != null && indexLigneSelectionne >= 0 && indexLigneSelectionne < listeIdLigneVente.size()) {
      idLigneSelectionne = listeIdLigneVente.get(indexLigneSelectionne);
    }
    return idLigneSelectionne;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(Etablissement etablissement) {
    this.etablissement = etablissement;
  }
  
  /**
   * Nombre de résultats retournés par la recherche.
   */
  public int getNombreResultat() {
    if (getListeIdDocumentVente() == null) {
      return 0;
    }
    return getListeIdDocumentVente().size();
  }
  
  /**
   * Message d'information.
   */
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public int getNumeroDocument() {
    return numeroDocument;
  }
  
  public Date getDateDebut() {
    return dateDebut;
  }
  
  public Date getDateFin() {
    return dateFin;
  }
  
  public String getCodeArticle() {
    if (idArticle == null) {
      return "";
    }
    return idArticle.getCodeArticle();
  }
  
  public ListeDocumentVenteBase getListeDocumentVenteBase() {
    return listeDocumentVenteBase;
  }
  
  public ListeLigneVenteBase getListeLigneVenteBase() {
    return listeLigneVenteBase;
  }
  
  public EnumTypeCompteClient getTypeRechercheClient() {
    return typeClient;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public int getNombreLignesArticleTrouvees() {
    if (listeLigneVenteBase != null) {
      return listeLigneVenteBase.size();
    }
    else {
      return 0;
    }
  }
  
  public boolean isModeAffichageDocument() {
    return modeAffichage == MODE_AFFICHAGE_DOCUMENT;
  }
  
  public boolean isModeAffichageLigne() {
    return modeAffichage == MODE_AFFICHAGE_LIGNE;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public String getReferenceClientDocument() {
    return referenceClientDocument;
  }
  
  public EnumTypeDocumentVente getTypeDocument() {
    return typeDocumentVente;
  }
  
  /**
   * Client sélectionné.
   */
  public ClientBase getClientBase() {
    return clientBase;
  }
  
  /**
   * Liste des modes d'expédition.
   */
  public ListeModeExpedition getListeModeExpedition() {
    return listeModeExpedition;
  }
  
  public Map<LigneVente, DocumentVente> getListeLigneVenteCharge() {
    return listeLigneVenteCharge;
  }
  
  public int getIndexLigneSelectionnee() {
    return indexLigneSelectionnee;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public Chantier getChantier() {
    return chantier;
  }
  
  public void setLigneSelectionnee(LigneVente pLigneVenteSelectionnee) {
    this.ligneVenteSelectionnee = pLigneVenteSelectionnee;
  }
  
  /**
   * Retourne la précision d'une unité donnée
   */
  public int retournerPrecisionUnite(IdUnite pId) {
    return listeUnite.getPrecisionUnite(pId);
  }
  
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
  /**
   * Magasin de sortie associé au document de ventes.
   */
  public Magasin getMagasinSortie() {
    return magasinSortie;
  }
  
  /**
   * Représentant associé au document de ventes.
   */
  public Representant getRepresentant() {
    return representant;
  }
  
  /**
   * Vendeur associé au document de ventes.
   */
  public Vendeur getVendeur() {
    return vendeur;
  }
  
  /**
   * Client du document de ventes.
   */
  public Client getClient() {
    return client;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  /**
   * Mode d'affichage de la table contenant les lignes du document.
   */
  public int getModeAffichageTable() {
    return modeAffichageTable;
  }
  
  /**
   * Indique si c'est le premier document de la liste.
   */
  public boolean isPremierDocument() {
    if (getListeIdDocumentVente() != null && getIndexDocumentSelectionne() < 0) {
      return getIndexDocumentSelectionne() == 0;
    }
    else {
      return getIndexLigneSelectionne() == 0;
    }
  }
  
  /**
   * Indique si c'est le dernier document de la liste.
   */
  public boolean isDernierDocument() {
    if (getListeIdDocumentVente() != null && getIndexLigneSelectionne() < 0) {
      return getIndexDocumentSelectionne() >= getListeIdDocumentVente().size() - 1;
    }
    else {
      return getIndexLigneSelectionne() >= getListeIdLigneVente().size() - 1;
    }
  }
  
  public ListeDocumentVenteBase getListeDocumentAvoir() {
    return listeDocumentAvoir;
  }
}
