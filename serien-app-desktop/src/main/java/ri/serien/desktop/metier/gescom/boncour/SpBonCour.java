/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

/**
 * Classe appelée par le point de menu.
 */
public class SpBonCour extends SessionJava {
  /**
   * Constructeur.
   */
  public SpBonCour(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleBonCour modele = new ModeleBonCour(this);
    VueBonCour vue = new VueBonCour(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    
    vue.afficher();
  }
}
