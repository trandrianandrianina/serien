/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.io.Serializable;
import java.math.BigDecimal;

import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.reglement.IdReglement;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Facture.
 */
public class LigneFactureNonReglee implements Serializable {
  // Variables
  private SessionBase session = null;
  private DocumentVenteBase documentVenteBase = null;
  private ListeReglement listeReglement = null;
  private CalculReglement calculReglement = null;
  private IdReglement idReglement = null;
  private boolean estUnAvoir = false;
  
  /**
   * Constructeur.
   */
  public LigneFactureNonReglee(SessionBase pSession) {
    session = pSession;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne le reste à payer calculé à partir de la liste des règlements effectués et validés.
   * Il ne s'agit pas du reste à régler calculé à la volée dans l'onglet règlement du comptoir.
   */
  public BigDecimal getResteAPayerTTC() {
    if (documentVenteBase == null) {
      throw new MessageErreurException("Le document de vente est invalide.");
    }
    BigDecimal totalTTC = documentVenteBase.getTotalTTC();
    if (listeReglement == null || listeReglement.getMontantTotalCalcule() == null) {
      return totalTTC;
    }
    BigDecimal totalDejaReglee = listeReglement.getMontantTotalCalcule();
    return totalTTC.subtract(totalDejaReglee);
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser la classe qui effectue les calculs du règlement à partir des données du document.
   */
  private void initialiserPaiement() {
    // Détermine si le document est une facture ou un avoir à partir de son montant
    if (documentVenteBase.getTotalHT() != null && documentVenteBase.getTotalHT().compareTo(BigDecimal.ZERO) < 0) {
      estUnAvoir = true;
    }
    // Initialisation de la classe calculReglement
    if (documentVenteBase.getTotalTTC() != null && listeReglement != null) {
      calculReglement = new CalculReglement(session, documentVenteBase.getTypeDocumentVente(), documentVenteBase.getTotalTTC(),
          listeReglement, documentVenteBase.getId().getIdEtablissement(), documentVenteBase.getId().getNumero(), null,
          documentVenteBase.isModifiable());
    }
    else {
      calculReglement = new CalculReglement(session, documentVenteBase.getTypeDocumentVente(), BigDecimal.ZERO, null,
          documentVenteBase.getId().getIdEtablissement(), documentVenteBase.getId().getNumero(), null, documentVenteBase.isModifiable());
    }
  }
  
  // -- Accesseurs
  
  /**
   * Facture correspondant au document de ventes de base.
   */
  public DocumentVenteBase getDocumentVenteBase() {
    return documentVenteBase;
  }
  
  /**
   * Modifier la facture correspondant au document de ventes de base.
   */
  public void setDocumentVenteBase(DocumentVenteBase pDocumentVenteBase) {
    documentVenteBase = pDocumentVenteBase;
    initialiserPaiement();
  }
  
  /**
   * Liste des règlements de la facture.
   */
  public ListeReglement getListeReglement() {
    return listeReglement;
  }
  
  /**
   * Modifier la facture du document de ventes.
   */
  public void setListeReglement(ListeReglement pListeReglement) {
    listeReglement = pListeReglement;
    initialiserPaiement();
  }
  
  /**
   * Paiment du document de ventes.
   */
  public CalculReglement getPaiement() {
    return calculReglement;
  }
  
  /**
   * Modifier le paiement de la facture.
   */
  public void setPaiement(CalculReglement paiement) {
    this.calculReglement = paiement;
  }
  
  public String getLibelleTypeDocument() {
    if (estUnAvoir) {
      return "Avoir";
    }
    return "Facture";
  }
  
  public IdReglement getIdReglement() {
    return idReglement;
  }
  
  public void setIdReglement(IdReglement idReglement) {
    this.idReglement = idReglement;
  }
  
  public boolean isEstUnAvoir() {
    return estUnAvoir;
  }
}
