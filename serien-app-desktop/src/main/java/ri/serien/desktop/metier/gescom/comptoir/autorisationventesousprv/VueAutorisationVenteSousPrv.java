/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.autorisationventesousprv;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueAutorisationVenteSousPrv extends AbstractVueDialogue<ModeleAutorisationVenteSousPrv> {
  // Constantes
  private static final String BOUTON_DEMANDER_AUTORISATION = "Demander l'autorisation";
  private static final String BOUTON_CONTROLER_DEBLOCAGE = "Contrôler le déblocage";
  
  /**
   * Constructeur.
   */
  public VueAutorisationVenteSousPrv(ModeleAutorisationVenteSousPrv pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Formatage des zones de saisie
    tfNom.init(Client.TAILLE_ZONE_NOM, true, true);
    tfComplementNom.init(Client.TAILLE_ZONE_NOM, true, true);
    tfRue.init(Client.TAILLE_ZONE_NOM, false, true);
    tfLocalisation.init(Client.TAILLE_ZONE_NOM, false, true);
    tfVille.init(Client.TAILLE_ZONE_VILLE, true, true);
    tfCodePostal.init(Client.TAILLE_ZONE_CODEPOSTAL, true, false);
    
    taLibelleArticle.setBackground(Constantes.CL_ZONE_SORTIE);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_DEMANDER_AUTORISATION, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_CONTROLER_DEBLOCAGE, 'c', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    if (getModele().getLigneVente() == null) {
      throw new MessageErreurException(
          "Impossible de faire une demande d'autorisation de vente sous le prix de revient sur un article sans prix.");
    }
    
    // Rafraichissement des données du document
    rafraichirNumeroDocument();
    rafraichirLibelleArticle();
    rafraichirPrixVente();
    rafraichirPrixRevient();
    
    // Rafraichissement des données du client
    rafraichirCivilite();
    rafraichirNom();
    rafraichirComplementNom();
    rafraichirAdresse1();
    rafraichirAdresse2();
    rafraichirCodePostal();
    rafraichirVille();
    rafraichirNomContact();
    rafraichirEmail();
    rafraichirTelephone();
    rafraichirFax();
    
    rafraichirBoutonAutorisation();
    rafraichirBoutonDeblocage();
    rafraichirBoutonValider();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir le bouton de demande d'autorisation
   */
  private void rafraichirBoutonAutorisation() {
    boolean actif = !getModele().isDemandeDeblocageExiste();
    snBarreBouton.activerBouton(BOUTON_DEMANDER_AUTORISATION, actif);
  }
  
  /**
   * Rafraichir le bouton de controle du deblocage
   */
  private void rafraichirBoutonDeblocage() {
    boolean actif = getModele().isDemandeDeblocageExiste();
    snBarreBouton.activerBouton(BOUTON_CONTROLER_DEBLOCAGE, actif);
  }
  
  /**
   * Rafraichir le bouton de validation
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isAutorisationAcceptee();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  private void rafraichirNumeroDocument() {
    DocumentVente documentVente = getModele().getDocumentVente();
    if (documentVente != null && documentVente.getId().getNumero() > 0) {
      zsNumeroDocument.setText(documentVente.getId().toString());
    }
    else {
      zsNumeroDocument.setText("");
    }
  }
  
  private void rafraichirLibelleArticle() {
    LigneVente ligne = getModele().getLigneVente();
    if (ligne != null && ligne.getLibelle() != null) {
      taLibelleArticle.setText(ligne.getLibelle());
    }
    else {
      taLibelleArticle.setText("");
    }
  }
  
  private void rafraichirPrixVente() {
    LigneVente ligne = getModele().getLigneVente();
    BigDecimal prix = null;
    if (ligne != null) {
      prix = ligne.getPrixNetCalcule();
    }
    if (prix != null) {
      zsPrixDeVente.setText(Constantes.formater(prix, true));
    }
    else {
      zsPrixDeVente.setText("");
    }
  }
  
  private void rafraichirPrixRevient() {
    LigneVente ligneVente = getModele().getLigneVente();
    Client client = getModele().getClient();
    BigDecimal prix = null;
    if (ligneVente != null && client != null) {
      if (client.isFactureEnTTC()) {
        prix = ligneVente.getPrixDeRevientLigneTTC();
      }
      else {
        prix = ligneVente.getPrixDeRevientLigneHT();
      }
    }
    if (prix != null) {
      zsPrixDeRevientStandard.setText(Constantes.formater(prix, true));
    }
    else {
      zsPrixDeRevientStandard.setText("");
    }
  }
  
  private void rafraichirCivilite() {
    Client client = getModele().getClient();
    snCivilite.setSession(getModele().getSession());
    snCivilite.setIdEtablissement(getModele().getClient().getId().getIdEtablissement());
    snCivilite.setAucunAutorise(true);
    snCivilite.charger(false);
    if (client == null) {
      lbCivilite.setVisible(false);
      snCivilite.setSelection(null);
    }
    else {
      Adresse adresse = client.getAdresse();
      
      if (adresse != null) {
        IdCivilite idCivilite = getModele().getClient().getAdresse().getIdCivilite();
        if (idCivilite == null) {
          Contact contact = client.getContactPrincipal();
          if (contact != null) {
            idCivilite = contact.getIdCivilite();
          }
        }
        if (client.isParticulier() && getModele().getListeCivilites() != null) {
          snCivilite.setIdSelection(idCivilite);
        }
      }
      else {
        snCivilite.setSelection(null);
      }
      lbCivilite.setVisible(client.isParticulier());
      snCivilite.setVisible(client.isParticulier());
    }
  }
  
  private void rafraichirNom() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNom.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        lbNom.setText("Nom");
        tfNom.setText(contact.getNom());
      }
      else if (adresse != null && adresse.getNom() != null) {
        lbNom.setText("Raison sociale");
        tfNom.setText(adresse.getNom());
      }
      else {
        tfNom.setText("");
      }
    }
  }
  
  private void rafraichirComplementNom() {
    Client client = getModele().getClient();
    if (client == null) {
      tfComplementNom.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        lbComplementNom.setText("Prénom");
        tfComplementNom.setText(contact.getPrenom());
      }
      else if (adresse != null && adresse.getComplementNom() != null) {
        lbComplementNom.setText("Complément");
        tfComplementNom.setText(adresse.getComplementNom());
      }
      else {
        tfComplementNom.setText("");
      }
    }
  }
  
  private void rafraichirAdresse1() {
    Client client = getModele().getClient();
    if (client == null) {
      tfRue.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getRue() != null) {
        tfRue.setText(getModele().getClient().getAdresse().getRue());
      }
      else {
        tfRue.setText("");
      }
    }
  }
  
  private void rafraichirAdresse2() {
    Client client = getModele().getClient();
    if (client == null) {
      tfLocalisation.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getLocalisation() != null) {
        tfLocalisation.setText(getModele().getClient().getAdresse().getLocalisation());
      }
      else {
        tfLocalisation.setText("");
      }
    }
  }
  
  private void rafraichirCodePostal() {
    Client client = getModele().getClient();
    if (client == null) {
      tfCodePostal.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getCodePostalFormate() != null) {
        tfCodePostal.setText(getModele().getClient().getAdresse().getCodePostalFormate());
      }
      else {
        tfCodePostal.setText("");
      }
    }
  }
  
  private void rafraichirVille() {
    Client client = getModele().getClient();
    if (client == null) {
      tfVille.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getVille() != null) {
        tfVille.setText(getModele().getClient().getAdresse().getVille());
      }
      else {
        tfVille.setText("");
      }
    }
  }
  
  private void rafraichirNomContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNomComplet.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNomComplet() != null) {
        tfNomComplet.setText(contact.getNomComplet());
      }
      else {
        tfNomComplet.setText("");
      }
    }
  }
  
  private void rafraichirEmail() {
    Client client = getModele().getClient();
    if (client == null) {
      tfEmail.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getEmail() != null) {
        tfEmail.setText(contact.getEmail());
      }
      else {
        tfEmail.setText("");
      }
    }
  }
  
  private void rafraichirTelephone() {
    Client client = getModele().getClient();
    if (client == null) {
      tfTelephone.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroTelephone1() != null) {
        tfTelephone.setText(contact.getNumeroTelephone1());
      }
      else if (client.getNumeroTelephone() != null) {
        tfTelephone.setText(client.getNumeroTelephone());
      }
      else {
        tfTelephone.setText("");
      }
    }
  }
  
  private void rafraichirFax() {
    Client client = getModele().getClient();
    if (client == null) {
      tfFax.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroFax() != null) {
        tfFax.setText(contact.getNumeroFax());
      }
      else if (client.getNumeroFax() != null) {
        tfFax.setText(client.getNumeroFax());
      }
      else {
        tfFax.setText("");
      }
    }
  }
  
  // -- Méthodes interactives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_DEMANDER_AUTORISATION)) {
        getModele().demanderAutorisation();
      }
      else if (pSNBouton.isBouton(BOUTON_CONTROLER_DEBLOCAGE)) {
        getModele().controlerDeblocage();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomFocusLost(FocusEvent e) {
    try {
      getModele().modifierNomClient(tfNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfComplementNomFocusLost(FocusEvent e) {
    try {
      getModele().modifierComplementNomClient(tfComplementNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRueFocusLost(FocusEvent e) {
    try {
      getModele().modifierRueClient(tfRue.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLocalisationFocusLost(FocusEvent e) {
    try {
      getModele().modifierLocalisationClient(tfLocalisation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfCodePostalFocusLost(FocusEvent e) {
    try {
      getModele().modifierCodePostalClient(tfCodePostal.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfVilleFocusLost(FocusEvent e) {
    try {
      getModele().modifierVilleClient(tfVille.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomCompletFocusLost(FocusEvent e) {
    // TODO add your code here
  }
  
  private void tfTelephoneFocusLost(FocusEvent e) {
    try {
      getModele().modifierTelephoneClient(tfTelephone.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfEmailFocusLost(FocusEvent e) {
    try {
      getModele().modifierMailClient(tfEmail.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFaxFocusLost(FocusEvent e) {
    try {
      getModele().modifierFaxClient(tfFax.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCiviliteValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierCiviliteClient(snCivilite.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlMontants = new JPanel();
    lbNumeroDocument = new JLabel();
    zsNumeroDocument = new RiZoneSortie();
    lbArticle = new JLabel();
    taLibelleArticle = new RiTextArea();
    lbPrixDeVente = new JLabel();
    zsPrixDeVente = new RiZoneSortie();
    lbPrixDeRevient = new JLabel();
    zsPrixDeRevientStandard = new RiZoneSortie();
    pnlCoordonneesClient = new JPanel();
    lbCivilite = new JLabel();
    snCivilite = new SNCivilite();
    lbNom = new JLabel();
    tfNom = new XRiTextField();
    lbComplementNom = new JLabel();
    tfComplementNom = new XRiTextField();
    lbLocalisation = new JLabel();
    tfRue = new XRiTextField();
    lbRue = new JLabel();
    tfLocalisation = new XRiTextField();
    l_Ville = new JLabel();
    tfVille = new XRiTextField();
    lbCodePostal = new JLabel();
    tfCodePostal = new XRiTextField();
    lbContact = new JLabel();
    tfNomComplet = new XRiTextField();
    lbTelephone = new JLabel();
    tfTelephone = new XRiTextField();
    lbEmail = new JLabel();
    tfEmail = new XRiTextField();
    lbFax = new JLabel();
    tfFax = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Demande d'autorisation de vente en dessous du prix de revient");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(710, 600));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlMontants ========
      {
        pnlMontants.setBorder(new TitledBorder(""));
        pnlMontants.setOpaque(false);
        pnlMontants.setMinimumSize(new Dimension(350, 170));
        pnlMontants.setPreferredSize(new Dimension(455, 170));
        pnlMontants.setName("pnlMontants");
        pnlMontants.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMontants.getLayout()).columnWidths = new int[] { 144, 134, 119, 0, 0, 0 };
        ((GridBagLayout) pnlMontants.getLayout()).rowHeights = new int[] { 0, 38, 21, 38, 0 };
        ((GridBagLayout) pnlMontants.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlMontants.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNumeroDocument ----
        lbNumeroDocument.setText("Num\u00e9ro document");
        lbNumeroDocument.setFont(lbNumeroDocument.getFont().deriveFont(lbNumeroDocument.getFont().getStyle() & ~Font.BOLD,
            lbNumeroDocument.getFont().getSize() + 2f));
        lbNumeroDocument.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNumeroDocument.setName("lbNumeroDocument");
        pnlMontants.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsNumeroDocument ----
        zsNumeroDocument.setHorizontalAlignment(SwingConstants.RIGHT);
        zsNumeroDocument.setFont(zsNumeroDocument.getFont().deriveFont(zsNumeroDocument.getFont().getSize() + 3f));
        zsNumeroDocument.setMinimumSize(new Dimension(155, 35));
        zsNumeroDocument.setPreferredSize(new Dimension(155, 35));
        zsNumeroDocument.setMaximumSize(new Dimension(2147483647, 35));
        zsNumeroDocument.setName("zsNumeroDocument");
        pnlMontants.add(zsNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle
            .setFont(lbArticle.getFont().deriveFont(lbArticle.getFont().getStyle() & ~Font.BOLD, lbArticle.getFont().getSize() + 2f));
        lbArticle.setHorizontalAlignment(SwingConstants.RIGHT);
        lbArticle.setName("lbArticle");
        pnlMontants.add(lbArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- taLibelleArticle ----
        taLibelleArticle.setFont(taLibelleArticle.getFont().deriveFont(taLibelleArticle.getFont().getSize() + 1f));
        taLibelleArticle.setBackground(new Color(243, 243, 236));
        taLibelleArticle.setLineWrap(true);
        taLibelleArticle.setWrapStyleWord(true);
        taLibelleArticle.setEditable(false);
        taLibelleArticle.setPreferredSize(new Dimension(511, 60));
        taLibelleArticle.setName("taLibelleArticle");
        pnlMontants.add(taLibelleArticle, new GridBagConstraints(1, 1, 3, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixDeVente ----
        lbPrixDeVente.setText("Vente au prix de");
        lbPrixDeVente.setFont(
            lbPrixDeVente.getFont().deriveFont(lbPrixDeVente.getFont().getStyle() | Font.BOLD, lbPrixDeVente.getFont().getSize() + 2f));
        lbPrixDeVente.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPrixDeVente.setName("lbPrixDeVente");
        pnlMontants.add(lbPrixDeVente, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- zsPrixDeVente ----
        zsPrixDeVente.setFont(zsPrixDeVente.getFont().deriveFont(zsPrixDeVente.getFont().getSize() + 3f));
        zsPrixDeVente.setHorizontalAlignment(SwingConstants.RIGHT);
        zsPrixDeVente.setMinimumSize(new Dimension(155, 35));
        zsPrixDeVente.setPreferredSize(new Dimension(155, 35));
        zsPrixDeVente.setMaximumSize(new Dimension(2147483647, 35));
        zsPrixDeVente.setName("zsPrixDeVente");
        pnlMontants.add(zsPrixDeVente, new GridBagConstraints(1, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbPrixDeRevient ----
        lbPrixDeRevient.setText("pour un prix de revient de");
        lbPrixDeRevient.setFont(lbPrixDeRevient.getFont().deriveFont(lbPrixDeRevient.getFont().getStyle() | Font.BOLD,
            lbPrixDeRevient.getFont().getSize() + 2f));
        lbPrixDeRevient.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPrixDeRevient.setName("lbPrixDeRevient");
        pnlMontants.add(lbPrixDeRevient, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- zsPrixDeRevientStandard ----
        zsPrixDeRevientStandard.setFont(zsPrixDeRevientStandard.getFont()
            .deriveFont(zsPrixDeRevientStandard.getFont().getStyle() | Font.BOLD, zsPrixDeRevientStandard.getFont().getSize() + 3f));
        zsPrixDeRevientStandard.setHorizontalAlignment(SwingConstants.RIGHT);
        zsPrixDeRevientStandard.setMinimumSize(new Dimension(155, 35));
        zsPrixDeRevientStandard.setPreferredSize(new Dimension(155, 35));
        zsPrixDeRevientStandard.setMaximumSize(new Dimension(2147483647, 35));
        zsPrixDeRevientStandard.setName("zsPrixDeRevientStandard");
        pnlMontants.add(zsPrixDeRevientStandard, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlPrincipal.add(pnlMontants, BorderLayout.NORTH);
      
      // ======== pnlCoordonneesClient ========
      {
        pnlCoordonneesClient.setBorder(new TitledBorder(null, "Coordonn\u00e9es client", TitledBorder.LEADING,
            TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
        pnlCoordonneesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlCoordonneesClient.setMinimumSize(new Dimension(710, 330));
        pnlCoordonneesClient.setPreferredSize(new Dimension(710, 320));
        pnlCoordonneesClient.setOpaque(false);
        pnlCoordonneesClient.setName("pnlCoordonneesClient");
        pnlCoordonneesClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 0, 130, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCivilite ----
        lbCivilite.setText("Civilit\u00e9");
        lbCivilite.setPreferredSize(new Dimension(36, 19));
        lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCivilite.setName("lbCivilite");
        pnlCoordonneesClient.add(lbCivilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCivilite ----
        snCivilite.setName("snCivilite");
        snCivilite.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCiviliteValueChanged(e);
          }
        });
        pnlCoordonneesClient.add(snCivilite, new GridBagConstraints(1, 0, 6, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNom ----
        lbNom.setText("Nom");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setName("lbNom");
        pnlCoordonneesClient.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNom ----
        tfNom.setBackground(Color.white);
        tfNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNom.setMinimumSize(new Dimension(485, 30));
        tfNom.setPreferredSize(new Dimension(485, 30));
        tfNom.setName("tfNom");
        tfNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfNom, new GridBagConstraints(1, 1, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
        
        // ---- lbComplementNom ----
        lbComplementNom.setText("Pr\u00e9nom");
        lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNom.setName("lbComplementNom");
        pnlCoordonneesClient.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfComplementNom ----
        tfComplementNom.setBackground(Color.white);
        tfComplementNom.setMinimumSize(new Dimension(485, 30));
        tfComplementNom.setPreferredSize(new Dimension(485, 30));
        tfComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfComplementNom.setName("tfComplementNom");
        tfComplementNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfComplementNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfComplementNom, new GridBagConstraints(1, 2, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonneesClient.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfRue ----
        tfRue.setBackground(Color.white);
        tfRue.setMinimumSize(new Dimension(485, 30));
        tfRue.setPreferredSize(new Dimension(485, 30));
        tfRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfRue.setName("tfRue");
        tfRue.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfRueFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfRue, new GridBagConstraints(1, 3, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setName("lbRue");
        pnlCoordonneesClient.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLocalisation ----
        tfLocalisation.setBackground(Color.white);
        tfLocalisation.setMinimumSize(new Dimension(485, 30));
        tfLocalisation.setPreferredSize(new Dimension(485, 30));
        tfLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfLocalisation.setName("tfLocalisation");
        tfLocalisation.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLocalisationFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfLocalisation, new GridBagConstraints(1, 4, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- l_Ville ----
        l_Ville.setText("Ville");
        l_Ville.setFont(new Font("sansserif", Font.PLAIN, 14));
        l_Ville.setHorizontalAlignment(SwingConstants.RIGHT);
        l_Ville.setName("l_Ville");
        pnlCoordonneesClient.add(l_Ville, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfVille ----
        tfVille.setBackground(Color.white);
        tfVille.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfVille.setMinimumSize(new Dimension(365, 30));
        tfVille.setPreferredSize(new Dimension(365, 30));
        tfVille.setName("tfVille");
        tfVille.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfVilleFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfVille, new GridBagConstraints(3, 5, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbCodePostal ----
        lbCodePostal.setText("Code postal");
        lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal.setName("lbCodePostal");
        pnlCoordonneesClient.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfCodePostal ----
        tfCodePostal.setBackground(Color.white);
        tfCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfCodePostal.setMinimumSize(new Dimension(55, 30));
        tfCodePostal.setPreferredSize(new Dimension(55, 30));
        tfCodePostal.setName("tfCodePostal");
        tfCodePostal.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfCodePostalFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfCodePostal, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact");
        lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setDisplayedMnemonicIndex(2);
        lbContact.setName("lbContact");
        pnlCoordonneesClient.add(lbContact, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNomComplet ----
        tfNomComplet.setBackground(Color.white);
        tfNomComplet.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNomComplet.setMinimumSize(new Dimension(215, 30));
        tfNomComplet.setPreferredSize(new Dimension(215, 30));
        tfNomComplet.setName("tfNomComplet");
        tfNomComplet.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNomCompletFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfNomComplet, new GridBagConstraints(1, 6, 3, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setName("lbTelephone");
        pnlCoordonneesClient.add(lbTelephone, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfTelephone.setMinimumSize(new Dimension(140, 30));
        tfTelephone.setPreferredSize(new Dimension(140, 30));
        tfTelephone.setName("tfTelephone");
        tfTelephone.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfTelephoneFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfTelephone, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbEmail ----
        lbEmail.setText("Email");
        lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEmail.setName("lbEmail");
        pnlCoordonneesClient.add(lbEmail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEmail.setMinimumSize(new Dimension(250, 30));
        tfEmail.setPreferredSize(new Dimension(250, 30));
        tfEmail.setName("tfEmail");
        tfEmail.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfEmailFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfEmail, new GridBagConstraints(1, 7, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlCoordonneesClient.add(lbFax, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfFax.setPreferredSize(new Dimension(140, 30));
        tfFax.setMinimumSize(new Dimension(140, 30));
        tfFax.setName("tfFax");
        tfFax.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfFaxFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfFax, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
      }
      pnlPrincipal.add(pnlCoordonneesClient, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlMontants;
  private JLabel lbNumeroDocument;
  private RiZoneSortie zsNumeroDocument;
  private JLabel lbArticle;
  private RiTextArea taLibelleArticle;
  private JLabel lbPrixDeVente;
  private RiZoneSortie zsPrixDeVente;
  private JLabel lbPrixDeRevient;
  private RiZoneSortie zsPrixDeRevientStandard;
  private JPanel pnlCoordonneesClient;
  private JLabel lbCivilite;
  private SNCivilite snCivilite;
  private JLabel lbNom;
  private XRiTextField tfNom;
  private JLabel lbComplementNom;
  private XRiTextField tfComplementNom;
  private JLabel lbLocalisation;
  private XRiTextField tfRue;
  private JLabel lbRue;
  private XRiTextField tfLocalisation;
  private JLabel l_Ville;
  private XRiTextField tfVille;
  private JLabel lbCodePostal;
  private XRiTextField tfCodePostal;
  private JLabel lbContact;
  private XRiTextField tfNomComplet;
  private JLabel lbTelephone;
  private XRiTextField tfTelephone;
  private JLabel lbEmail;
  private XRiTextField tfEmail;
  private JLabel lbFax;
  private XRiTextField tfFax;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
