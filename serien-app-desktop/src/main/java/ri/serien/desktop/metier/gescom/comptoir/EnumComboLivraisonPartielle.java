/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Choix possibles pour la comboBox livraison partielle de l'onglet livraison du comptoir
 */
public enum EnumComboLivraisonPartielle {
  AUTORISE(' ', "Autorisé"),
  NON_AUTORISE('X', "Non autorisé");
  
  private final Character code;
  private final String libelle;
  
  /**
   * Constructeur.
   */
  EnumComboLivraisonPartielle(Character pCode, String pLibelle) {
    code = pCode;
    libelle = pLibelle;
  }
  
  /**
   * Le code sous lequel la valeur est persistée en base de données.
   */
  public Character getCode() {
    return code;
  }
  
  /**
   * Le libellé associé au code.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Retourne le code associé dans une chaîne de caractère.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle);
  }
  
  /**
   * Retourner l'objet énum par son code.
   */
  static public EnumComboLivraisonPartielle valueOfByCode(Character pCode) {
    for (EnumComboLivraisonPartielle value : values()) {
      if (pCode.equals(value.getCode())) {
        return value;
      }
    }
    throw new MessageErreurException("La valeur " + pCode + " est invalide pour cette boite à choix multiples.");
  }
}
