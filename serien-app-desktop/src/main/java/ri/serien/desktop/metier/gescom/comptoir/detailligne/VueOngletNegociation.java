/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.Negociation;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.sntypegratuit.SNTypeGratuit;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet négociation de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletNegociation extends SNPanelFond implements InterfaceVueOnglet {
  // Constantes
  private static final String BOUTON_NEGOCIER_ACHAT = "Négocier l'achat";
  private static final String BOUTON_NEGOCIER_VENTE = "Négocier la vente";
  private static final String BOUTON_AFFICHER_TTC = "Afficher en TTC";
  private static final String BOUTON_AFFICHER_HT = "Afficher en HT";
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détails";
  private static final String BOUTON_MASQUER_DETAIL = "Masquer détails";
  private static final String LIBELLE_AFFICHER_EN_HT = "<html><center><u>A</u>fficher en HT</center></html>";
  private static final String LIBELLE_AFFICHER_EN_TTC = "<html><center><u>A</u>fficher en TTC</center></html>";
  
  // Variables
  private ModeleDetailLigneArticle modele = null;
  private Negociation negociation = null;
  private boolean executerEvenements = false;
  private boolean valeurEstSaisie = false;
  
  /**
   * Constructeur.
   */
  public VueOngletNegociation(ModeleDetailLigneArticle acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    pnlVente.setVisible(!modele.getModeNegociation());
    pnlAchat.setVisible(modele.getModeNegociation());
    
    snTypeGratuit.setSession(modele.getSession());
    snTypeGratuit.setIdEtablissement(modele.getLigneVente().getId().getIdEtablissement());
    snTypeGratuit.charger(false);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_NEGOCIER_ACHAT, 'n', true);
    snBarreBouton.ajouterBouton(BOUTON_NEGOCIER_VENTE, 'n', false);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_TTC, 'a', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_HT, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_MASQUER_DETAIL, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    tfQuantiteCommandeUC.requestFocus();
  }
  
  @Override
  public void rafraichir() {
    executerEvenements = false;
    
    rafraichirBoutonNegocierAchat();
    rafraichirBoutonNegocierVente();
    rafraichirBoutonAfficherDetail();
    rafraichirBoutonMasquerDetail();
    rafraichirBoutonTTC();
    rafraichirBoutonHT();
    
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE) {
      pnlVente.setVisible(true);
      pnlAchat.setVisible(false);
      rafraichirQuantiteUC();
      rafraichirUniteConditionnement();
      rafraichirLibelleUVparUC();
      rafraichirMessageUCS();
      rafraichirQuantiteUV();
      rafraichirUniteVente();
      rafraichirDateTarif();
      if (modele.getArticle().isArticlePalette() || modele.getArticle().isArticleTransport()) {
        pnlTarifInitial.setVisible(false);
        pnlTarifInitialMarge.setVisible(false);
        pnlPrixNegocie.setVisible(false);
        pnlNegocePrixNegocie.setVisible(false);
        pnlClassiquePrixNegocie.setVisible(false);
        pnlTarifNegocieMarge.setVisible(false);
      }
      else {
        // La ligne initiale
        rafraichirPrixBaseInitial();
        rafraichirColonneTarifInitiale();
        rafraichirOrigineInitialePrix();
        rafraichirRemiseInitiale();
        rafraichirPrixNetInitial();
        pnlPrixNegocie.setVisible(true);
        // La ligne négociée
        if (modele.isModeNegoce()) {
          rafraichirNegocePrixPublic();
          rafraichirNegoceColonneTarif();
          rafraichirNegoceRemise();
          rafraichirNegocePrixNet();
        }
        else {
          // Les champs recalculés
          rafraichirClassiquePrixBase();
          rafraichirClassiqueColonneTarif();
          rafraichirClassiqueRemise();
          rafraichirClassiquePrixNet();
        }
        rafraichirDetail();
        pnlVente.requestFocus();
      }
      rafraichirMessageConditionChantier();
      rafraichirGratuit();
    }
    else {
      pnlVente.setVisible(false);
      pnlAchat.setVisible(true);
      
      rafraichirTitreNegociationAchat();
      rafraichirDateApplicationAchat();
      rafraichirQuantiteCommandeeUCA();
      rafraichirUniteCommandeAchat();
      rafraichirLibelleUVparUCAchat();
      rafraichirQuantiteCommandeeUVA();
      rafraichirUniteVenteAchat();
      rafraichirPrixAchatBrutHT();
      rafraichirLibelleUniteAchat();
      rafraichirRemiseFournisseur1();
      rafraichirRemiseFournisseur2();
      rafraichirRemiseFournisseur3();
      rafraichirRemiseFournisseur4();
      rafraichirTaxeMajoration();
      rafraichirValeurConditionnement();
      rafraichirPrixAchatNet();
      rafraichirTypeSaisiePort();
      rafraichirMontantPortHT();
      rafraichirPoidsPort();
      rafraichirPourcentagePort();
      rafraichirPrixRevientFournisseur();
      rafraichirFraisExploitation();
      rafraichirPrixRevientStandard();
    }
    
    // Positionnement du focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleDetailLigneArticle.FOCUS_QUANTITE:
        if (pnlAchat.isVisible()) {
          tfAchatQuantiteUCV.requestFocusInWindow();
        }
        else {
          tfQuantiteCommandeUC.requestFocusInWindow();
        }
        break;
    }
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraîchir le bouton de négo des achats
   */
  private void rafraichirBoutonNegocierAchat() {
    boolean actif = false;
    
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE && modele.isAutoriseNegocierAchat()
        && !modele.getArticle().isArticlePalette() && !modele.getArticle().isArticleTransport() && !modele.isModeGratuit()) {
      actif = true;
    }
    
    snBarreBouton.activerBouton(BOUTON_NEGOCIER_ACHAT, actif);
  }
  
  /**
   * Rafraîchir le bouton de négo des ventes
   */
  private void rafraichirBoutonNegocierVente() {
    boolean actif = false;
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_ACHAT && !modele.getArticle().isArticlePalette()
        && !modele.getArticle().isArticleTransport() && !modele.isModeGratuit()) {
      actif = true;
    }
    snBarreBouton.activerBouton(BOUTON_NEGOCIER_VENTE, actif);
  }
  
  /**
   * Rafraîchir le bouton d'affichage du détail
   */
  private void rafraichirBoutonAfficherDetail() {
    boolean actif = false;
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE && !modele.isModeGratuit()) {
      actif = !modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge();
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL, actif);
  }
  
  /**
   * Rafraîchir le bouton qui masque le détail
   */
  private void rafraichirBoutonMasquerDetail() {
    boolean actif = false;
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE && !modele.getArticle().isArticlePalette()
        && !modele.getArticle().isArticleTransport() && !modele.isModeGratuit()) {
      actif = modele.isDetailMargeVisible() && modele.isAutoriseVisualiserMarge();
    }
    
    snBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL, actif);
  }
  
  /**
   * Rafraîchir le bouton d'affichage du TTC.
   */
  private void rafraichirBoutonTTC() {
    boolean actif = false;
    
    // Le bouton "Afficher en TTC" ne doit pas être affiché si le client est facturé en HT (car le calcul de prix en Java ne calcule pas
    // les prix TTC en mode HT et que cela n'a pas vraiment de sens)
    if (modele.getClient() == null || !modele.getClient().isFactureEnTTC()) {
      actif = false;
    }
    else if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE && !modele.getArticle().isArticlePalette()
        && !modele.getArticle().isArticleTransport() && !modele.isModeGratuit()) {
      actif = !modele.isAffichageTTC();
    }
    snBarreBouton.activerBouton(BOUTON_AFFICHER_TTC, actif);
  }
  
  /**
   * Rafraîchir le bouton HT.
   */
  private void rafraichirBoutonHT() {
    boolean actif = false;
    if (modele.getModeNegociation() == ModeleDetailLigneArticle.MODE_NEGOCIATION_VENTE && !modele.getArticle().isArticlePalette()
        && !modele.getArticle().isArticleTransport() && !modele.isModeGratuit()) {
      actif = modele.isAffichageTTC();
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_HT, actif);
  }
  
  /**
   * Rafraichir le prix public (en mode négoce) ou le prix de base (en mode classique) initial.
   */
  private void rafraichirPrixBaseInitial() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    // Mode négoce
    if (modele.isModeNegoce()) {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
        lbPrixBaseInitial.setText("Prix public TTC");
        zsPrixBaseInitial.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
        lbPrixBaseInitial.setText("Prix public HT");
        zsPrixBaseInitial.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBaseInitial.setText("Prix public");
        zsPrixBaseInitial.setText("0");
      }
    }
    // Mode classique
    else {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
        lbPrixBaseInitial.setText("Prix base TTC");
        zsPrixBaseInitial.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
        lbPrixBaseInitial.setText("Prix base HT");
        zsPrixBaseInitial.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBaseInitial.setText("Prix base");
        zsPrixBaseInitial.setText("0");
      }
    }
  }
  
  /**
   * Rafraichir le numéro de la colonne initiale.
   */
  private void rafraichirColonneTarifInitiale() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    // En mode négoce cette information n'est pas affichée
    if (modele.isModeNegoce()) {
      lbColonneTarifInitiale.setVisible(false);
      tfColonneTarifInitiale.setVisible(false);
    }
    // Mode classique
    else {
      lbColonneTarifInitiale.setVisible(true);
      tfColonneTarifInitiale.setVisible(true);
      if (ligneVente != null) {
        tfColonneTarifInitiale.setText(LigneVente.TEXTE_ORIGINE_PRIX_COLONNE + ' ' + ligneVente.getNumeroColonneTarif());
      }
      else {
        tfColonneTarifInitiale.setText("");
      }
    }
  }
  
  /**
   * Rafraichir l'origine initiale du prix.
   */
  private void rafraichirOrigineInitialePrix() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    if (ligneVente != null && ligneVente.getTexteOriginePrixVente() != null) {
      zsOrigineInitiale.setText(ligneVente.getTexteOriginePrixVente());
    }
    else {
      zsOrigineInitiale.setText("");
    }
  }
  
  /**
   * Rafraichir la remise initiale.
   */
  private void rafraichirRemiseInitiale() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    BigPercentage tauxRemise = null;
    if (ligneVente != null) {
      tauxRemise = ligneVente.getTauxRemiseUnitaire(modele.isModeNegoce());
    }
    if (tauxRemise != null) {
      zsRemiseInitiale.setText(Constantes.formater(tauxRemise, true));
    }
    else {
      zsRemiseInitiale.setText("");
    }
  }
  
  /**
   * Rafraichir le prix net initial.
   */
  private void rafraichirPrixNetInitial() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    if (ligneVente != null && ligneVente.getPrixNetTTC() != null && modele.isAffichageTTC()) {
      lbPrixNetInitial.setText("Prix net TTC");
      zsPrixNetInitial.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixNetHT() != null && !modele.isAffichageTTC()) {
      lbPrixNetInitial.setText("Prix net HT");
      zsPrixNetInitial.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
    }
    else {
      lbPrixNetInitial.setText("Prix net");
      zsPrixNetInitial.setText("");
    }
  }
  
  /**
   * Rafraichir le prix public du prix de vente négocié.
   */
  private void rafraichirNegocePrixPublic() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
      lbNegocePrixPublic.setText("Prix public TTC");
      tfNegocePrixPublic.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
      lbNegocePrixPublic.setText("Prix public HT");
      tfNegocePrixPublic.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
    }
    else {
      lbNegocePrixPublic.setText("Prix public");
      tfNegocePrixPublic.setText("");
    }
  }
  
  /**
   * Rafraichir le prix net
   */
  private void rafraichirNegocePrixNet() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixNetTTC() != null && modele.isAffichageTTC()) {
      lbNegocePrixNet.setText("Prix net TTC");
      tfNegocePrixNet.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixNetHT() != null && !modele.isAffichageTTC()) {
      lbNegocePrixNet.setText("Prix net HT");
      tfNegocePrixNet.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
    }
    else {
      lbNegocePrixNet.setText("Prix net");
      tfNegocePrixNet.setText("");
    }
    
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfNegocePrixNet.setEnabled(true);
    }
    else {
      tfNegocePrixNet.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (ligneVente != null && ligneVente.isDerogation()) {
      tfNegocePrixNet.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfNegocePrixNet.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir l'origine du prix.
   */
  private void rafraichirNegoceColonneTarif() {
    LigneVente ligneVente = modele.getLigneVente();
    
    // Mettre à jour les valeurs de la liste déroulante
    cbNegoceColonneTarif.removeAllItems();
    cbNegoceColonneTarif.addItem("Aucune");
    ColonneTarif[] tableauColonneTarif = modele.getListeColonnesTarif();
    if (tableauColonneTarif != null) {
      for (ColonneTarif colonneTarif : tableauColonneTarif) {
        cbNegoceColonneTarif.addItem(colonneTarif);
      }
    }
    
    // Mettre à jour la colonne tarif
    ColonneTarif colonneTarif = modele.getColonneTarif();
    if (colonneTarif != null) {
      cbNegoceColonneTarif.setSelectedItem(colonneTarif);
    }
    else {
      cbNegoceColonneTarif.setSelectedItem(-1);
    }
    
    // Permettre la modification de la colonne tarif :
    // - si les quantités sont modifiables
    // - si la ligne n'est pas une dérogation
    // - si la ligne ne bénéficie pas d'une condition chantier
    if (modele.isModifierQuantitePossible() && (ligneVente != null && !ligneVente.isDerogation())
        && modele.getMessageConditionChantier() == null) {
      cbNegoceColonneTarif.setEnabled(true);
    }
    else {
      cbNegoceColonneTarif.setEnabled(false);
    }
    
    // Force le rafraichissement de la combo sinon le texte n'est pas mis à jour (il faut passer la souris dessus pour que ce soit fait)
    cbNegoceColonneTarif.repaint();
  }
  
  /**
   * Rafraichir la remise.
   */
  private void rafraichirNegoceRemise() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getTauxRemise1() != null) {
      tfNegoceRemise.setText(Constantes.formater(ligneVente.getTauxRemise1(), true));
    }
    else {
      tfNegoceRemise.setText("");
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfNegoceRemise.setEnabled(true);
    }
    else {
      tfNegoceRemise.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (ligneVente != null && ligneVente.isDerogation()) {
      tfNegoceRemise.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfNegoceRemise.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le prix base du prix de vente négocié.
   */
  private void rafraichirClassiquePrixBase() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && modele.isAffichageTTC()) {
      lbClassiquePrixBase.setText("Prix base TTC");
      tfClassiquePrixBase.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      if (ligneVente.getProvenancePrixBase() == EnumProvenancePrixBase.SAISIE_LIGNE_VENTE) {
        tfClassiquePrixBaseSaisi.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
    }
    else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !modele.isAffichageTTC()) {
      lbClassiquePrixBase.setText("Prix base HT");
      tfClassiquePrixBase.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      if (ligneVente.getProvenancePrixBase() == EnumProvenancePrixBase.SAISIE_LIGNE_VENTE) {
        tfClassiquePrixBaseSaisi.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
    }
    else {
      lbClassiquePrixBase.setText("Prix base");
      tfClassiquePrixBase.setText("");
      tfClassiquePrixBaseSaisi.setText("");
    }
  }
  
  /**
   * Rafraichir la colonne tarif du prix de vente négocié.
   */
  private void rafraichirClassiqueColonneTarif() {
    LigneVente ligneVente = modele.getLigneVente();
    cbClassiqueColonneTarifSaisie.removeAllItems();
    if (modele.getListeColonnesTarifClassique() != null) {
      cbClassiqueColonneTarifSaisie.setModel(new DefaultComboBoxModel(modele.getListeColonnesTarifClassique()));
    }
    else {
      cbClassiqueColonneTarifSaisie.setModel(new DefaultComboBoxModel());
    }
    
    if (ligneVente != null && modele.getColonneTarif() != null) {
      tfClassiqueColonneTarif.setText(modele.getColonneTarif().toString());
      if (ligneVente.getProvenanceColonneTarif() == EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE) {
        cbClassiqueColonneTarifSaisie.setSelectedItem(modele.getColonneTarif());
      }
    }
    else {
      tfClassiqueColonneTarif.setText("");
      cbClassiqueColonneTarifSaisie.setSelectedItem(-1);
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      cbClassiqueColonneTarifSaisie.setEnabled(true);
    }
    else {
      cbClassiqueColonneTarifSaisie.setEnabled(false);
    }
    
    // Si la ligne correspond à une dérogation
    if (ligneVente != null && ligneVente.isDerogation()) {
      cbClassiqueColonneTarifSaisie.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      cbClassiqueColonneTarifSaisie.setEnabled(false);
    }
    
    // Force le rafraichissement de la combo sinon le texte n'est pas mis à jour (il faut passer la souris dessus pour que ce soit fait)
    cbClassiqueColonneTarifSaisie.repaint();
  }
  
  /**
   * Rafraichir l'origine du prix.
   */
  private void rafraichirClassiqueOriginePrix() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getTexteOriginePrixVente() != null) {
      tfClassiqueOrigine.setText(ligneVente.getTexteOriginePrixVente());
    }
    else {
      tfClassiqueOrigine.setText("");
    }
  }
  
  /**
   * Rafraichir la remise du prix de vente négocié.
   */
  private void rafraichirClassiqueRemise() {
    LigneVente ligneVente = modele.getLigneVente();
    BigPercentage tauxRemise = null;
    if (ligneVente != null) {
      tauxRemise = ligneVente.getTauxRemiseUnitaire(modele.isModeNegoce());
    }
    if (ligneVente != null && tauxRemise != null) {
      tfClassiqueRemise.setText(Constantes.formater(tauxRemise, true));
      if (ligneVente.getProvenanceTauxRemise() == EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE) {
        tfClassiqueRemiseSaisie.setText(Constantes.formater(tauxRemise, true));
      }
    }
    else {
      tfClassiqueRemise.setText("");
      tfClassiqueRemiseSaisie.setText("");
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfClassiqueRemiseSaisie.setEnabled(true);
    }
    else {
      tfClassiqueRemiseSaisie.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (ligneVente != null && ligneVente.isDerogation()) {
      tfClassiqueRemiseSaisie.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfClassiqueRemiseSaisie.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le prix net.
   */
  private void rafraichirClassiquePrixNet() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixNetTTC() != null && modele.isAffichageTTC()) {
      lbClassiquePrixNet.setText("Prix net TTC");
      tfClassiquePrixNet.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
      if (ligneVente.getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
        tfClassiquePrixNetSaisi.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
      }
    }
    else if (ligneVente != null && ligneVente.getPrixNetHT() != null && !modele.isAffichageTTC()) {
      lbClassiquePrixNet.setText("Prix net HT");
      tfClassiquePrixNet.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
      if (ligneVente.getProvenancePrixNet() == EnumProvenancePrixNet.SAISIE_LIGNE_VENTE) {
        tfClassiquePrixNetSaisi.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
      }
    }
    else {
      lbClassiquePrixNet.setText("Prix net");
      tfClassiquePrixNet.setText("");
      tfClassiquePrixNetSaisi.setText("");
    }
    
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfClassiquePrixNetSaisi.setEnabled(true);
    }
    else {
      tfClassiquePrixNetSaisi.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (ligneVente != null && ligneVente.isDerogation()) {
      tfClassiquePrixNetSaisi.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfClassiquePrixNetSaisi.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir détail panels.
   */
  private void rafraichirDetail() {
    if (!modele.isAutoriseVisualiserMarge() || !modele.isDetailMargeVisible()) {
      pnlTarifInitialMarge.setVisible(false);
      pnlTarifNegocieMarge.setVisible(false);
    }
    else if (modele.isModeGratuit()) {
      pnlNegocePrixNegocie.setVisible(false);
      pnlClassiquePrixNegocie.setVisible(false);
      pnlTarifNegocieMarge.setVisible(false);
    }
    else {
      pnlTarifInitialMarge.setVisible(true);
      if (modele.isModeNegoce()) {
        pnlNegocePrixNegocie.setVisible(true);
        pnlClassiquePrixNegocie.setVisible(false);
      }
      else {
        pnlNegocePrixNegocie.setVisible(false);
        pnlClassiquePrixNegocie.setVisible(true);
      }
      pnlTarifNegocieMarge.setVisible(true);
      rafraichirIndiceMargeInitial();
      rafraichirMargeInitiale();
      rafraichirPrixRevientInitial();
      rafraichirIndiceMarge();
      rafraichirMarge();
      rafraichirPrixRevient();
    }
  }
  
  /**
   * Rafraichir l'indice de marge initial.
   */
  private void rafraichirIndiceMargeInitial() {
    if (modele.getIndiceDeMargeOrigine() != null) {
      zsIndiceDeMargeInitiale.setText(Constantes.formater(modele.getIndiceDeMargeOrigine(), true));
    }
    else {
      zsIndiceDeMargeInitiale.setText("");
    }
  }
  
  /**
   * Rafraichir l'indice de marge.
   */
  private void rafraichirIndiceMarge() {
    if (modele.getIndiceDeMarge() != null) {
      tfIndiceDeMarge.setText(Constantes.formater(modele.getIndiceDeMarge(), true));
    }
    else {
      tfIndiceDeMarge.setText("");
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfIndiceDeMarge.setEnabled(true);
    }
    else {
      tfIndiceDeMarge.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (modele.getLigneVente() != null && modele.getLigneVente().isDerogation()) {
      tfIndiceDeMarge.setEnabled(false);
    }
    
    // Ou si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfIndiceDeMarge.setEnabled(false);
    }
    
    // Ou si le mode classique est actif
    if (!modele.isModeNegoce()) {
      tfIndiceDeMarge.setEnabled(false);
      pnlEspace.setVisible(true);
    }
    else {
      pnlEspace.setVisible(false);
    }
  }
  
  /**
   * Rafraichir la marge initiale.
   */
  private void rafraichirMargeInitiale() {
    if (modele.getTauxDeMarqueOrigine() != null) {
      zsMargeInitiale.setText(Constantes.formater(modele.getTauxDeMarqueOrigine(), true));
    }
    else {
      zsMargeInitiale.setText("");
    }
  }
  
  /**
   * Rafraichir la marge.
   */
  private void rafraichirMarge() {
    if (modele.getTauxDeMarque() != null) {
      tfMarge.setText(Constantes.formater(modele.getTauxDeMarque(), true));
    }
    else {
      tfMarge.setText("");
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfMarge.setEnabled(true);
    }
    else {
      tfMarge.setEnabled(false);
    }
    
    // Ou si la ligne correspond à une dérogation
    if (modele.getLigneVente() != null && modele.getLigneVente().isDerogation()) {
      tfMarge.setEnabled(false);
    }
    
    // Si la ligne bénéficie d'une condition chantier
    if (modele.getMessageConditionChantier() != null) {
      tfMarge.setEnabled(false);
    }
    
    // Ou si le mode classique est actif
    if (!modele.isModeNegoce()) {
      tfMarge.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le PRV initial.
   */
  private void rafraichirPrixRevientInitial() {
    LigneVente ligneVente = modele.getLigneVenteOrigine();
    if (ligneVente != null && ligneVente.getPrixDeRevientLigneTTC() != null && modele.isAffichageTTC()) {
      lbPrixRevientInitial.setText("<html>Prix de revient<br>standard TTC</html>");
      zsPrixRevientInitial.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixDeRevientLigneHT() != null && !modele.isAffichageTTC()) {
      lbPrixRevientInitial.setText("<html>Prix de revient<br>standard HT</html>");
      zsPrixRevientInitial.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneHT(), true));
    }
    else {
      lbPrixRevientInitial.setText("Prix de revient");
      zsPrixRevientInitial.setText("");
    }
  }
  
  /**
   * Rafraichir le PRV.
   */
  private void rafraichirPrixRevient() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getPrixDeRevientLigneTTC() != null && modele.isAffichageTTC()) {
      lbPrixRevient.setText("<html>Prix de revient<br>standard TTC</html>");
      tfPrixRevient.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixDeRevientLigneHT() != null && !modele.isAffichageTTC()) {
      lbPrixRevient.setText("<html>Prix de revient<br>standard HT</html>");
      tfPrixRevient.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneHT(), true));
      
    }
    else {
      lbPrixRevient.setText("Prix de revient");
      tfPrixRevient.setText("");
    }
  }
  
  /**
   * Rafraîchir les quantités en UCV et en UV.
   */
  private void rafraichirUniteConditionnement() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && ligneVente.getIdUniteConditionnementVente() != null) {
      tfUniteC.setText(ligneVente.getIdUniteConditionnementVente().getCode());
    }
    else {
      tfUniteC.setText("");
    }
  }
  
  /**
   * Rafraichir la quantité en UC.
   */
  private void rafraichirQuantiteUC() {
    LigneVente ligneVente = modele.getLigneVente();
    Article article = modele.getArticle();
    if (ligneVente != null && ligneVente.getIdUniteConditionnementVente() != null && article != null) {
      if (Constantes.equals(ligneVente.getIdUniteConditionnementVente(), article.getIdUCV()) || article.getIdUCV() == null) {
        tfQuantiteCommandeUC.setText(Constantes.formater(ligneVente.getQuantiteUCV(), false));
      }
      else {
        tfQuantiteCommandeUC.setText(Constantes.formater(
            ligneVente.getQuantiteUCV().divide(article.getNombreUSParUCS(true), modele.getNombreDecimalesUS(), RoundingMode.CEILING),
            false));
      }
    }
    else {
      tfQuantiteCommandeUC.setText("");
    }
    
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfQuantiteCommandeUC.setEnabled(true);
    }
    else {
      tfQuantiteCommandeUC.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la quantité en UV.
   */
  private void rafraichirQuantiteUV() {
    LigneVente ligne = modele.getLigneVente();
    if (!ligne.isUniteVenteIdentique()) {
      if (modele.getArticle().isArticleDecoupable()) {
        tfQuantiteCommandeeUV.setText(Constantes.formater(modele.getDimension(), true));
      }
      else {
        tfQuantiteCommandeeUV.setText(Constantes.formater(modele.getLigneVente().getQuantiteUV(), false));
      }
      tfQuantiteCommandeeUV.setVisible(true);
      lbQuantiteUV.setVisible(true);
    }
    else {
      tfQuantiteCommandeeUV.setText("");
      tfQuantiteCommandeeUV.setVisible(false);
      lbQuantiteUV.setVisible(false);
    }
    
    // La zone est rendu non saisissable :
    // - si les quantités articles du document ne sont pas modifiables
    // - ou si on est en dimension (il faut passer par l'onglet Dimension)
    if (modele.isModifierQuantitePossible() && !modele.getArticle().isArticleDecoupable()) {
      tfQuantiteCommandeeUV.setEnabled(true);
    }
    else {
      tfQuantiteCommandeeUV.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le message UCS
   */
  private void rafraichirMessageUCS() {
    
    if (modele.getMessageUCS() != null) {
      lbUCAparUCS.setText("");
      lbUCAparUCS.setText(modele.getMessageUCS());
      lbUCAparUCS.setVisible(true);
    }
    else {
      lbUCAparUCS.setText("");
      lbUCAparUCS.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le plibellé UV par UC
   */
  private void rafraichirLibelleUVparUC() {
    LigneVente ligne = modele.getLigneVente();
    if (!ligne.isUniteVenteIdentique()) {
      BigDecimal conditionnement = ligne.getNombreUVParUCV();
      if (conditionnement != null && conditionnement.compareTo(BigDecimal.ZERO) == 0) {
        conditionnement = BigDecimal.ONE;
      }
      
      if (ligne.getIdUniteVente() != null && ligne.getIdUniteConditionnementVente() != null) {
        lbUVparUC.setText("Conditionnement " + Constantes.formater(conditionnement, false) + " " + ligne.getIdUniteVente().getCode() + "/"
            + ligne.getIdUniteConditionnementVente().getCode());
      }
    }
    else {
      lbUVparUC.setText("");
    }
    lbUVparUC.setVisible(!ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraichir l'unité de vente
   */
  private void rafraichirUniteVente() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getIdUniteVente() != null) {
      tfUniteV.setText(ligne.getIdUniteVente().getCode());
    }
    else {
      tfUniteV.setText("");
    }
    tfUniteV.setVisible(ligne != null && !ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraichir la date du tarif.
   */
  private void rafraichirDateTarif() {
    if (modele.getDateTarif() != null) {
      snDateApplication.setDate(modele.getDateTarif());
    }
    else {
      snDateApplication.setDate(null);
    }
  }
  
  /**
   * Rafraichir le titre de la négociation d'achat
   */
  private void rafraichirTitreNegociationAchat() {
    // Tester la présence d'une condition d'achats
    if (modele.getConditionAchatLigne() != null && modele.getFournisseur() != null) {
      // Indiquer le fournisseur auprès duquel la négociation sera effectuée
      pnlNegociationAchat.setTitre("Négociation achats chez " + modele.getFournisseur().toString());
    }
    else {
      // Signaler que l'article n'a pas de condition d'achats
      pnlNegociationAchat.setTitre("Article sans condition d'achats");
    }
  }
  
  /**
   * Rafraichir la date d'application tarif achat
   */
  private void rafraichirDateApplicationAchat() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    if (conditionAchat != null && conditionAchat.getDateApplication() != null) {
      snDateApplicationTarifAchat.setDate(conditionAchat.getDateApplication());
    }
  }
  
  /**
   * Rafraichir la quantité commandée en UCA
   */
  private void rafraichirQuantiteCommandeeUCA() {
    LigneVente ligne = modele.getLigneVente();
    Article article = modele.getArticle();
    
    if (ligne != null && ligne.getIdUniteConditionnementVente() != null && article != null) {
      if (Constantes.equals(ligne.getIdUniteConditionnementVente(), article.getIdUCV()) || article.getIdUCV() == null) {
        tfAchatQuantiteUCV.setText(Constantes.formater(ligne.getQuantiteUCV(), false));
      }
      else {
        tfAchatQuantiteUCV.setText(Constantes.formater(
            ligne.getQuantiteUCV().divide(article.getNombreUSParUCS(true), modele.getNombreDecimalesUS(), RoundingMode.CEILING), false));
      }
    }
    else {
      tfAchatQuantiteUCV.setText("");
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfAchatQuantiteUCV.setEnabled(true);
    }
    else {
      tfAchatQuantiteUCV.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir l'unité de commande d'achat
   */
  private void rafraichirUniteCommandeAchat() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getIdUniteConditionnementVente() != null) {
      tfAchatUniteQuantiteUCV.setText(ligne.getIdUniteConditionnementVente().getCode());
    }
    else {
      tfAchatUniteQuantiteUCV.setText("");
    }
  }
  
  /**
   * Rafraichir le libellé UV par UCA
   */
  private void rafraichirLibelleUVparUCAchat() {
    LigneVente ligne = modele.getLigneVente();
    if (!ligne.isUniteVenteIdentique()) {
      BigDecimal conditionnement = ligne.getNombreUVParUCV();
      if (conditionnement != null && conditionnement.compareTo(BigDecimal.ZERO) == 0) {
        conditionnement = BigDecimal.ONE;
      }
      
      if (ligne.getIdUniteVente() != null && ligne.getIdUniteConditionnementVente() != null) {
        lbAchatRatioUVparUCV.setText("Conditionnement " + Constantes.formater(conditionnement, false) + " "
            + ligne.getIdUniteVente().getCode() + "/" + ligne.getIdUniteConditionnementVente().getCode() + " soit en UV");
      }
    }
    else {
      lbAchatRatioUVparUCV.setText("");
    }
    lbAchatRatioUVparUCV.setVisible(!ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraichir la quantité en unité vente achat
   */
  private void rafraichirQuantiteCommandeeUVA() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && !ligne.isUniteVenteIdentique()) {
      tfAchatRatioUVParUCV.setText(Constantes.formater(modele.getLigneVente().getQuantiteUV(), false));
      tfAchatRatioUVParUCV.setVisible(true);
    }
    else {
      tfAchatRatioUVParUCV.setText("");
      tfAchatRatioUVParUCV.setVisible(false);
    }
    // La zone est rendu non saisissable si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfAchatRatioUVParUCV.setEnabled(true);
    }
    else {
      tfAchatRatioUVParUCV.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir l'unité de vente achat
   */
  private void rafraichirUniteVenteAchat() {
    LigneVente ligne = modele.getLigneVente();
    if (ligne != null && ligne.getIdUniteVente() != null) {
      tfAchatUniteRatioUVparUCV.setText(ligne.getIdUniteVente().getCode());
    }
    else {
      tfAchatUniteRatioUVparUCV.setText("");
    }
    tfAchatUniteRatioUVparUCV.setVisible(ligne != null && !ligne.isUniteVenteIdentique());
  }
  
  /**
   * Rafraichir le prix d'achat brut HT
   */
  private void rafraichirPrixAchatBrutHT() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    
    if (conditionAchat != null && conditionAchat.getPrixAchatBrutHT() != null) {
      tfPrixAchatBrutHT.setText(Constantes.formater(conditionAchat.getPrixAchatBrutHT(), true));
      if (conditionAchat.getIdUniteAchat() != null) {
        lbUnitePrixAchatBrutHT.setText(conditionAchat.getIdUniteAchat().getCode());
      }
      else {
        lbUnitePrixAchatBrutHT.setText("");
      }
    }
    else {
      tfPrixAchatBrutHT.setText("");
      lbUnitePrixAchatBrutHT.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfPrixAchatBrutHT.setEnabled(true);
    }
    else {
      tfPrixAchatBrutHT.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le libellé de l'unité d'achat
   */
  private void rafraichirLibelleUniteAchat() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    if (conditionAchat != null && conditionAchat.getIdUniteAchat() != null) {
      lbUnitePrixAchatBrutHT.setText("par " + conditionAchat.getIdUniteAchat().getCode());
      lbUnitePrixAchatNetHT.setText("par " + conditionAchat.getIdUniteAchat().getCode());
      lbUnitePrixRevientFournisseurHT.setText("par " + conditionAchat.getIdUniteAchat().getCode());
      lbUnitePrixRevientStandardHT.setText("par " + conditionAchat.getIdUniteAchat().getCode());
    }
    else {
      lbUnitePrixAchatBrutHT.setText("");
      lbUnitePrixAchatNetHT.setText("");
      lbUnitePrixRevientFournisseurHT.setText("");
      lbUnitePrixRevientStandardHT.setText("");
    }
  }
  
  /**
   * Rafraichir la remise fournisseur 1
   */
  private void rafraichirRemiseFournisseur1() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal remise = null;
    
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise1();
    }
    if (remise != null) {
      tfRemiseFournisseur1.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFournisseur1.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfRemiseFournisseur1.setEnabled(true);
    }
    else {
      tfRemiseFournisseur1.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la remise fournisseur 2
   */
  private void rafraichirRemiseFournisseur2() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal remise = null;
    
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise2();
    }
    if (remise != null) {
      tfRemiseFournisseur2.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFournisseur2.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfRemiseFournisseur2.setEnabled(true);
    }
    else {
      tfRemiseFournisseur2.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la remise fournisseur 3
   */
  private void rafraichirRemiseFournisseur3() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal remise = null;
    
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise3();
    }
    if (remise != null) {
      tfRemiseFournisseur3.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFournisseur3.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfRemiseFournisseur3.setEnabled(true);
    }
    else {
      tfRemiseFournisseur3.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la remise fournisseur 4
   */
  private void rafraichirRemiseFournisseur4() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal remise = null;
    
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise4();
    }
    if (remise != null) {
      tfRemiseFournisseur4.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFournisseur4.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfRemiseFournisseur4.setEnabled(true);
    }
    else {
      tfRemiseFournisseur4.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la taxe de majoration
   */
  private void rafraichirTaxeMajoration() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal valeur = null;
    
    if (conditionAchat != null) {
      valeur = conditionAchat.getPourcentageTaxeOuMajoration();
    }
    if (valeur != null) {
      tfTaxeMajoration.setText(Constantes.formater(valeur, true));
    }
    else {
      tfTaxeMajoration.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfTaxeMajoration.setEnabled(true);
    }
    else {
      tfTaxeMajoration.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la valeur du conditionnement
   */
  private void rafraichirValeurConditionnement() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    BigDecimal valeur = null;
    
    if (conditionAchat != null) {
      valeur = conditionAchat.getMontantConditionnement();
    }
    if (valeur != null) {
      tfMontantConditionnement.setText(Constantes.formater(valeur, true));
    }
    else {
      tfMontantConditionnement.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      tfMontantConditionnement.setEnabled(true);
    }
    else {
      tfMontantConditionnement.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le prix d'achat net
   */
  private void rafraichirPrixAchatNet() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    BigDecimal valeur = null;
    
    if (conditionAchat != null) {
      valeur = conditionAchat.getPrixAchatNetHT();
    }
    if (valeur != null) {
      tfPrixAchatNetHT.setText(Constantes.formater(valeur, true));
    }
    else {
      tfPrixAchatNetHT.setText("");
    }
  }
  
  /**
   * Rafraichir le type de saisie du port
   */
  private void rafraichirTypeSaisiePort() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    
    // Mettre à jour les valeurs
    rbMontantPort.setSelected(modele.getTypeSaisiePort() == ModeleDetailLigneArticle.TYPE_SAISIE_PORT_MONTANT_ET_POIDS);
    rbPourcentagePort.setSelected(modele.getTypeSaisiePort() == ModeleDetailLigneArticle.TYPE_SAISIE_PORT_POURCENTAGE);
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
      rbPourcentagePort.setEnabled(true);
      rbMontantPort.setEnabled(true);
    }
    else {
      rbPourcentagePort.setEnabled(false);
      rbMontantPort.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le montant du port
   */
  private void rafraichirMontantPortHT() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    
    // Récupérer le montant du port
    BigDecimal montantPortHT = null;
    if (conditionAchat != null) {
      montantPortHT = conditionAchat.getMontantAuPoidsPortHT();
    }
    
    // Mettre à jour le composant graphique
    if (montantPortHT != null) {
      tfValeurPort.setText(Constantes.formater(montantPortHT, true));
    }
    else {
      tfValeurPort.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    // - c'est le type de saisie montant et poids
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()
        && modele.getTypeSaisiePort() == ModeleDetailLigneArticle.TYPE_SAISIE_PORT_MONTANT_ET_POIDS) {
      tfValeurPort.setEnabled(true);
    }
    else {
      tfValeurPort.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le poids du port
   */
  private void rafraichirPoidsPort() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    
    // Récupérer le poids du port
    BigDecimal poidsPort = null;
    if (conditionAchat != null) {
      poidsPort = conditionAchat.getPoidsPort();
    }
    
    // Mettre à jour le composant graphique
    if (poidsPort != null) {
      tfPoidsPort.setText(Constantes.formater(poidsPort, true));
    }
    else {
      tfPoidsPort.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    // - c'est le type de saisie montant et poids
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()
        && modele.getTypeSaisiePort() == ModeleDetailLigneArticle.TYPE_SAISIE_PORT_MONTANT_ET_POIDS) {
      tfPoidsPort.setEnabled(true);
    }
    else {
      tfPoidsPort.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le pourcentage de port
   */
  private void rafraichirPourcentagePort() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    LigneVente ligne = modele.getLigneVente();
    
    // Récupérer le pourcentage du port
    BigDecimal pourcentagePort = null;
    if (conditionAchat != null) {
      pourcentagePort = conditionAchat.getPourcentagePort();
    }
    
    // Mettre à jour le composant graphique
    if (pourcentagePort != null) {
      tfPourcentagePort.setText(Constantes.formater(pourcentagePort, true));
    }
    else {
      tfPourcentagePort.setText("");
    }
    
    // Rendre saisissable si :
    // - il y a une condition d'achats
    // - la quantité d'article du document est modifiable
    // - ce n'est pas une dérogation
    // - c'est le type de saisie pourcentage
    if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()
        && modele.getTypeSaisiePort() == ModeleDetailLigneArticle.TYPE_SAISIE_PORT_POURCENTAGE) {
      tfPourcentagePort.setEnabled(true);
    }
    else {
      tfPourcentagePort.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le PRV fournisseur
   */
  private void rafraichirPrixRevientFournisseur() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    BigDecimal prixRevientFournisseurHT = null;
    
    if (conditionAchat != null) {
      prixRevientFournisseurHT = conditionAchat.getPrixRevientFournisseurHT();
    }
    if (prixRevientFournisseurHT != null) {
      tfPrixRevientFournisseurHT.setText(Constantes.formater(prixRevientFournisseurHT, true));
    }
    else {
      tfPrixRevientFournisseurHT.setText("");
    }
  }
  
  /**
   * Rafraichir les frais d'exploitation
   */
  private void rafraichirFraisExploitation() {
    if (modele.isAutoriseFraisExploitation()) {
      tfFraisExploitation.setVisible(true);
      ConditionAchat conditionAchat = modele.getConditionAchatLigne();
      LigneVente ligne = modele.getLigneVente();
      BigDecimal valeur = null;
      
      if (conditionAchat != null) {
        valeur = conditionAchat.getFraisExploitation();
      }
      if (valeur != null) {
        tfFraisExploitation.setText(Constantes.formater(valeur, true));
      }
      else {
        tfFraisExploitation.setText("");
      }
      
      // Rendre saisissable si :
      // - il y a une condition d'achats
      // - la quantité d'article du document est modifiable
      // - ce n'est pas une dérogation
      if (conditionAchat != null && modele.isModifierQuantitePossible() && !ligne.isDerogation()) {
        tfFraisExploitation.setEnabled(true);
      }
      else {
        tfFraisExploitation.setEnabled(false);
      }
    }
    else {
      tfFraisExploitation.setVisible(false);
    }
    lbFraisExploitation.setVisible(tfFraisExploitation.isVisible());
    lbPourcentageFraisExploitation.setVisible(tfFraisExploitation.isVisible());
  }
  
  /**
   * Rafraichir le PRV
   */
  private void rafraichirPrixRevientStandard() {
    ConditionAchat conditionAchat = modele.getConditionAchatLigne();
    BigDecimal prixRevientStandardHT = null;
    
    if (conditionAchat != null) {
      prixRevientStandardHT = conditionAchat.getPrixRevientStandardHT();
    }
    if (prixRevientStandardHT != null) {
      tfPrixRevientStandardHT.setText(Constantes.formater(prixRevientStandardHT, true));
    }
    else {
      tfPrixRevientStandardHT.setText("");
    }
  }
  
  /**
   * Rafraichir le message si la ligne a une CNV chantier
   */
  private void rafraichirMessageConditionChantier() {
    // Message
    lbMessageConditionChantier.setMessage(modele.getMessageConditionChantier());
    
    // Visibilité
    boolean isConditionChantier = false;
    if (modele.getMessageConditionChantier() != null) {
      isConditionChantier = true;
    }
    lbMessageConditionChantier.setVisible(isConditionChantier);
  }
  
  /**
   * Teste si la touche du clavier utilisée peut entrainer une modification de valeur.
   */
  private boolean testerModificationValeur(int pKeyCode, boolean pValeurEstModifiee) {
    // Si la valeur a été déjà modifiée alors on retourne vrai
    if (pValeurEstModifiee) {
      return true;
    }
    if ((pKeyCode == KeyEvent.VK_TAB) || (pKeyCode == KeyEvent.VK_LEFT) || (pKeyCode == KeyEvent.VK_RIGHT)
        || (pKeyCode == KeyEvent.VK_UP)) {
      return false;
    }
    return true;
  }
  
  /**
   * Rafraichir panel gratuité
   */
  private void rafraichirGratuit() {
    if (!modele.isGratuitPossible()) {
      pnlGratuit.setVisible(false);
    }
    else {
      pnlGratuit.setVisible(true);
      rafraichirModeGratuit();
      rafraichirTypeGratuit();
    }
  }
  
  /**
   * Rafraichir case à cocher gratuité
   */
  private void rafraichirModeGratuit() {
    // Mettre à jour le composant graphique avec la valeur du modèle
    if (!modele.isModeGratuit()) {
      chkGratuit.setSelected(false);
    }
    else {
      chkGratuit.setSelected(true);
    }
    
    // Lier la possibilité de saisie du champ à la saisie de quantité
    chkGratuit.setEnabled(modele.isModifierQuantitePossible());
  }
  
  /**
   * Rafraichir type gratuité
   */
  private void rafraichirTypeGratuit() {
    // Afficher le champ uniquement si le moge gratuti est actif
    if (!modele.isModeGratuit()) {
      lbTypeGratuit.setVisible(false);
      snTypeGratuit.setVisible(false);
      snTypeGratuit.setSelection(null);
    }
    else {
      lbTypeGratuit.setVisible(true);
      snTypeGratuit.setVisible(true);
      snTypeGratuit.setSelection(modele.getTypeGratuit());
      
      // Lier la possibilité de saisie du champ à la saisie de quantité
      snTypeGratuit.setEnabled(modele.isModifierQuantitePossible());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_NEGOCIER_ACHAT)) {
        modele.modifierModeNegociation(true);
      }
      else if (pSNBouton.isBouton(BOUTON_NEGOCIER_VENTE)) {
        modele.modifierModeNegociation(false);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_TTC)) {
        modele.modifierAffichageTTC(true);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_HT)) {
        modele.modifierAffichageTTC(false);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        modele.modifierAfficherDetails(true);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL)) {
        modele.modifierAfficherDetails(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbNegoceOrigineItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        Object selection = cbNegoceColonneTarif.getSelectedItem();
        if (selection instanceof ColonneTarif) {
          modele.modifierNumeroColonne((ColonneTarif) selection);
        }
        else {
          modele.modifierNumeroColonne(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfNegoceRemiseFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierRemise(tfNegoceRemise.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfNegocePrixNetFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierPrixNet(tfNegocePrixNet.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfIndiceDeMargeFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierIndiceDeMarge(tfIndiceDeMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfMargeFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierMarge(tfMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void btAfficherDetailsActionPerformed(ActionEvent e) {
    try {
      modele.modifierAfficherDetails(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btMasquerDetailsActionPerformed(ActionEvent e) {
    try {
      modele.modifierAfficherDetails(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPrixAchatBrutFocusLost(FocusEvent e) {
    try {
      modele.modifierPrixAchatBrut(tfPrixAchatBrutHT.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseFrs1FocusLost(FocusEvent e) {
    try {
      modele.modifierRemiseFrs1(tfRemiseFournisseur1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseFrs2FocusLost(FocusEvent e) {
    try {
      modele.modifierRemiseFrs2(tfRemiseFournisseur2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseFrs3FocusLost(FocusEvent e) {
    try {
      modele.modifierRemiseFrs3(tfRemiseFournisseur3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseFrs4FocusLost(FocusEvent e) {
    try {
      modele.modifierRemiseFrs4(tfRemiseFournisseur4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfValeurConditionnementAchatFocusLost(FocusEvent e) {
    try {
      modele.modifierValeurConditionnementAchat(tfMontantConditionnement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeUCFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUC(tfQuantiteCommandeUC.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeeUVFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUV(tfQuantiteCommandeeUV.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeUCachatFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUC(tfAchatQuantiteUCV.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfquantiteCommandeeUVachatFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUV(tfAchatRatioUVParUCV.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPortValeurFocusLost(FocusEvent e) {
    try {
      modele.modifierMontantPort(tfValeurPort.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPortPoidsFocusLost(FocusEvent e) {
    try {
      modele.modifierPoidsPort(tfPoidsPort.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNegoceRemiseKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNegocePrixNetKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfIndiceDeMargeKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfMargeKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPortPourcentageFocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentagePort(tfPourcentagePort.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbMontantPortActionPerformed(ActionEvent e) {
    try {
      modele.modifierTypeSaisiePort(ModeleDetailLigneArticle.TYPE_SAISIE_PORT_MONTANT_ET_POIDS);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbCoeffPortActionPerformed(ActionEvent e) {
    try {
      modele.modifierTypeSaisiePort(ModeleDetailLigneArticle.TYPE_SAISIE_PORT_POURCENTAGE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFraisExploitationFocusLost(FocusEvent e) {
    try {
      modele.modifierFraisExploitation(tfFraisExploitation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTaxeMajorationFocusLost(FocusEvent e) {
    try {
      modele.modifierTaxeMajoration(tfTaxeMajoration.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snTypeGratuitValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierTypeGratuit(snTypeGratuit.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void chkGratuitActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeGratuit(chkGratuit.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfClassiquePrixNetSaisiFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierPrixNet(tfClassiquePrixNetSaisi.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfClassiquePrixNetSaisiKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfClassiqueRemiseSaisieFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierRemise(tfClassiqueRemiseSaisie.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfClassiqueRemiseSaisieKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbClassiqueColonneTarifSaisieItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED) {
        modele.modifierNumeroColonne((ColonneTarif) cbClassiqueColonneTarifSaisie.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfClassiquePrixBaseSaisiFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        modele.modifierPrixBase(tfClassiquePrixBaseSaisi.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
    valeurEstSaisie = false;
  }
  
  private void tfClassiquePrixBaseSaisiKeyPressed(KeyEvent e) {
    try {
      // Permet de detecter si la valeur a été saisie ou non
      valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfNegocePrixNetMouseClicked(MouseEvent e) {
    try {
      // Pour que l'action soit exécutée il faut un CTRL + clic souris dans le composant
      if (!e.isControlDown()) {
        return;
      }
      modele.afficherDetailCalculMontant();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanel();
    pnlVente = new SNPanelContenu();
    pnlEnteteVente = new SNPanel();
    lbQuantiteUC = new SNLabelChamp();
    pnlVenteQuantiteUCV = new SNPanel();
    tfQuantiteCommandeUC = new SNTexte();
    tfUniteC = new SNTexte();
    lbUCAparUCS = new SNLabelChamp();
    lbQuantiteUV = new SNLabelChamp();
    pnlVenteRatioUVParUCV = new SNPanel();
    tfQuantiteCommandeeUV = new SNTexte();
    tfUniteV = new SNTexte();
    lbUVparUC = new SNLabelChamp();
    pnlTarif = new SNPanel();
    pnlTarifInitial = new SNPanelTitre();
    pnlInformationPrixInitial = new SNPanel();
    pnlDateApplicationtarif = new SNPanel();
    lbDateApplication = new SNLabelChamp();
    snDateApplication = new SNDate();
    lbPrixBaseInitial = new SNLabelChamp();
    lbColonneTarifInitiale = new SNLabelChamp();
    lbOrigineInitiale = new SNLabelChamp();
    lbRemiseInitiale = new SNLabelChamp();
    lbPrixNetInitial = new SNLabelChamp();
    zsPrixBaseInitial = new SNTexte();
    tfColonneTarifInitiale = new SNTexte();
    zsOrigineInitiale = new SNTexte();
    zsRemiseInitiale = new SNTexte();
    lbSymbolePourcentageRemiseInitiale = new SNLabelChamp();
    zsPrixNetInitial = new SNTexte();
    pnlTarifInitialMarge = new SNPanelTitre();
    lbIndiceDeMargeInitiale = new SNLabelChamp();
    zsIndiceDeMargeInitiale = new SNTexte();
    lbMargeInitiale = new SNLabelChamp();
    lbPrixRevientInitial = new SNLabelChamp();
    zsMargeInitiale = new SNTexte();
    lbSymbolePourcentageMargeInitiale = new SNLabelChamp();
    zsPrixRevientInitial = new SNTexte();
    pnlTarifNegocie = new SNPanelTitre();
    pnlPrixNegocie = new JPanel();
    pnlNegocePrixNegocie = new SNPanel();
    lbNegocePrixPublic = new SNLabelChamp();
    lbNegoceOrigine = new SNLabelChamp();
    lbNegoceRemise = new SNLabelChamp();
    lbNegocePrixNet = new SNLabelChamp();
    tfNegocePrixPublic = new SNTexte();
    cbNegoceColonneTarif = new SNComboBox();
    tfNegoceRemise = new SNTexte();
    lbSymbolePourcentageNegoceRemise = new SNLabelChamp();
    tfNegocePrixNet = new SNTexte();
    pnlClassiquePrixNegocie = new SNPanel();
    lbClassiquePrixBase = new SNLabelChamp();
    lbClassiqueColonneTarif = new SNLabelChamp();
    lbClassiqueOrigine = new SNLabelChamp();
    lbClassiqueRemise = new SNLabelChamp();
    lbClassiquePrixNet = new SNLabelChamp();
    tfClassiquePrixBaseSaisi = new SNTexte();
    cbClassiqueColonneTarifSaisie = new SNComboBox();
    tfClassiqueRemiseSaisie = new SNTexte();
    lbSymbolePourcentageClassiqueRemiseSaisie = new SNLabelChamp();
    tfClassiquePrixNetSaisi = new SNTexte();
    tfClassiquePrixBase = new SNTexte();
    tfClassiqueColonneTarif = new SNTexte();
    tfClassiqueOrigine = new SNTexte();
    tfClassiqueRemise = new SNTexte();
    lbSymbolePourcentageClassiqueRemise = new SNLabelChamp();
    tfClassiquePrixNet = new SNTexte();
    pnlGratuit = new SNPanel();
    chkGratuit = new JCheckBox();
    lbTypeGratuit = new SNLabelChamp();
    snTypeGratuit = new SNTypeGratuit();
    pnlTarifNegocieMarge = new SNPanelTitre();
    lbIndiceDeMarge = new SNLabelChamp();
    tfIndiceDeMarge = new SNTexte();
    lbMarge = new SNLabelChamp();
    lbPrixRevient = new SNLabelChamp();
    pnlEspace = new SNPanel();
    tfMarge = new SNTexte();
    lbSymbolePourcentageMarge = new SNLabelChamp();
    tfPrixRevient = new SNTexte();
    lbMessageConditionChantier = new SNMessage();
    pnlAchat = new SNPanelContenu();
    pnlEnteteAchat = new SNPanel();
    lbAchatQuantiteUCV = new SNLabelChamp();
    pnlQuantiteUCV = new SNPanel();
    tfAchatQuantiteUCV = new SNTexte();
    tfAchatUniteQuantiteUCV = new SNTexte();
    lbAchatRatioUVparUCV = new SNLabelChamp();
    pnlUVParUCV = new SNPanel();
    tfAchatRatioUVParUCV = new SNTexte();
    tfAchatUniteRatioUVparUCV = new SNTexte();
    pnlNegociationAchat = new SNPanelTitre();
    lbRemisesFournisseur = new SNLabelChamp();
    lbPrixAchatBrutHT = new SNLabelChamp();
    pnlPrixAchatBrutHT = new SNPanel();
    tfPrixAchatBrutHT = new SNTexte();
    lbUnitePrixAchatBrutHT = new SNLabelChamp();
    pnlRemisesFournisseur = new SNPanel();
    tfRemiseFournisseur1 = new SNTexte();
    lbUniteRemiseFournisseur1 = new SNLabelChamp();
    tfRemiseFournisseur2 = new SNTexte();
    lbUniteRemiseFournisseur2 = new SNLabelChamp();
    tfRemiseFournisseur3 = new SNTexte();
    lbUniteRemiseFournisseur3 = new SNLabelChamp();
    tfRemiseFournisseur4 = new SNTexte();
    lbUniteRemiseFournisseur4 = new SNLabelChamp();
    lbTaxeMajoration = new SNLabelChamp();
    pnlTaxeMajoration = new SNPanel();
    tfTaxeMajoration = new SNTexte();
    lbPourcentageTaxeMajoration = new SNLabelChamp();
    lbMontantConditionnement = new SNLabelChamp();
    tfMontantConditionnement = new SNTexte();
    lbPrixAchatNetHT = new SNLabelChamp();
    pnlPrixAchatNetHT = new SNPanel();
    tfPrixAchatNetHT = new SNTexte();
    lbUnitePrixAchatNetHT = new SNLabelChamp();
    rbMontantPort = new JRadioButton();
    pnlMontantPort = new SNPanel();
    tfValeurPort = new SNTexte();
    lbPortPoids = new SNLabelChamp();
    tfPoidsPort = new SNTexte();
    rbPourcentagePort = new JRadioButton();
    pnlPourcentagePort = new SNPanel();
    tfPourcentagePort = new SNTexte();
    lbPourcentage5 = new SNLabelChamp();
    lbPrixRevientFournisseurHT = new SNLabelChamp();
    pnlPrixRevientFournisseurHT = new SNPanel();
    tfPrixRevientFournisseurHT = new SNTexte();
    lbUnitePrixRevientFournisseurHT = new SNLabelChamp();
    lbFraisExploitation = new SNLabelChamp();
    pnlFraisExploitation = new SNPanel();
    tfFraisExploitation = new SNTexte();
    lbPourcentageFraisExploitation = new SNLabelChamp();
    lbPrixRevientStandardHT = new SNLabelChamp();
    pnlPrixRevientStandardHT = new SNPanel();
    tfPrixRevientStandardHT = new SNTexte();
    lbUnitePrixRevientStandardHT = new SNLabelChamp();
    lbDateApplicationTarifAchat = new SNLabelChamp();
    snDateApplicationTarifAchat = new SNDate();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(950, 530));
    setPreferredSize(new Dimension(950, 530));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setOpaque(false);
      pnlContenu.setPreferredSize(new Dimension(965, 370));
      pnlContenu.setMinimumSize(new Dimension(945, 370));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new CardLayout());
      
      // ======== pnlVente ========
      {
        pnlVente.setName("pnlVente");
        pnlVente.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlVente.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlVente.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlVente.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlVente.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
        
        // ======== pnlEnteteVente ========
        {
          pnlEnteteVente.setName("pnlEnteteVente");
          pnlEnteteVente.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnteteVente.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlEnteteVente.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEnteteVente.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEnteteVente.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          
          // ---- lbQuantiteUC ----
          lbQuantiteUC.setText("Quantit\u00e9 en UCV");
          lbQuantiteUC.setName("lbQuantiteUC");
          pnlEnteteVente.add(lbQuantiteUC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlVenteQuantiteUCV ========
          {
            pnlVenteQuantiteUCV.setName("pnlVenteQuantiteUCV");
            pnlVenteQuantiteUCV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlVenteQuantiteUCV.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlVenteQuantiteUCV.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlVenteQuantiteUCV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlVenteQuantiteUCV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfQuantiteCommandeUC ----
            tfQuantiteCommandeUC.setHorizontalAlignment(SwingConstants.TRAILING);
            tfQuantiteCommandeUC.setName("tfQuantiteCommandeUC");
            tfQuantiteCommandeUC.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfquantiteCommandeUCFocusLost(e);
              }
            });
            pnlVenteQuantiteUCV.add(tfQuantiteCommandeUC, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfUniteC ----
            tfUniteC.setEditable(false);
            tfUniteC.setEnabled(false);
            tfUniteC.setMinimumSize(new Dimension(36, 30));
            tfUniteC.setPreferredSize(new Dimension(36, 30));
            tfUniteC.setMaximumSize(new Dimension(36, 30));
            tfUniteC.setName("tfUniteC");
            pnlVenteQuantiteUCV.add(tfUniteC, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnteteVente.add(pnlVenteQuantiteUCV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbUCAparUCS ----
          lbUCAparUCS.setText("UCA/UCS");
          lbUCAparUCS.setHorizontalAlignment(SwingConstants.LEFT);
          lbUCAparUCS.setName("lbUCAparUCS");
          pnlEnteteVente.add(lbUCAparUCS, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbQuantiteUV ----
          lbQuantiteUV.setText("Quantit\u00e9 en UV");
          lbQuantiteUV.setName("lbQuantiteUV");
          pnlEnteteVente.add(lbQuantiteUV, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlVenteRatioUVParUCV ========
          {
            pnlVenteRatioUVParUCV.setName("pnlVenteRatioUVParUCV");
            pnlVenteRatioUVParUCV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlVenteRatioUVParUCV.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlVenteRatioUVParUCV.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlVenteRatioUVParUCV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlVenteRatioUVParUCV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfQuantiteCommandeeUV ----
            tfQuantiteCommandeeUV.setHorizontalAlignment(SwingConstants.TRAILING);
            tfQuantiteCommandeeUV.setName("tfQuantiteCommandeeUV");
            tfQuantiteCommandeeUV.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfquantiteCommandeeUVFocusLost(e);
              }
            });
            pnlVenteRatioUVParUCV.add(tfQuantiteCommandeeUV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfUniteV ----
            tfUniteV.setEditable(false);
            tfUniteV.setEnabled(false);
            tfUniteV.setMinimumSize(new Dimension(36, 30));
            tfUniteV.setPreferredSize(new Dimension(36, 30));
            tfUniteV.setMaximumSize(new Dimension(36, 30));
            tfUniteV.setName("tfUniteV");
            pnlVenteRatioUVParUCV.add(tfUniteV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnteteVente.add(pnlVenteRatioUVParUCV, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUVparUC ----
          lbUVparUC.setText("Ratio UV/UCV");
          lbUVparUC.setMaximumSize(new Dimension(300, 30));
          lbUVparUC.setMinimumSize(new Dimension(300, 30));
          lbUVparUC.setPreferredSize(new Dimension(300, 30));
          lbUVparUC.setHorizontalAlignment(SwingConstants.LEFT);
          lbUVparUC.setName("lbUVparUC");
          pnlEnteteVente.add(lbUVparUC, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlVente.add(pnlEnteteVente, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTarif ========
        {
          pnlTarif.setMinimumSize(new Dimension(1015, 250));
          pnlTarif.setMaximumSize(new Dimension(1015, 320));
          pnlTarif.setPreferredSize(new Dimension(1015, 320));
          pnlTarif.setName("pnlTarif");
          pnlTarif.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          
          // ======== pnlTarifInitial ========
          {
            pnlTarifInitial.setTitre("Prix initial");
            pnlTarifInitial.setPreferredSize(new Dimension(650, 150));
            pnlTarifInitial.setBorder(new CompoundBorder(new EmptyBorder(5, 0, 0, 0), new TitledBorder(null, "Prix initial",
                TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14))));
            pnlTarifInitial.setMinimumSize(new Dimension(650, 150));
            pnlTarifInitial.setName("pnlTarifInitial");
            pnlTarifInitial.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifInitial.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTarifInitial.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTarifInitial.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifInitial.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlInformationPrixInitial ========
            {
              pnlInformationPrixInitial.setPreferredSize(new Dimension(620, 100));
              pnlInformationPrixInitial.setMinimumSize(new Dimension(620, 100));
              pnlInformationPrixInitial.setName("pnlInformationPrixInitial");
              pnlInformationPrixInitial.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlInformationPrixInitial.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlInformationPrixInitial.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlInformationPrixInitial.getLayout()).columnWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlInformationPrixInitial.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlDateApplicationtarif ========
              {
                pnlDateApplicationtarif.setName("pnlDateApplicationtarif");
                pnlDateApplicationtarif.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlDateApplicationtarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlDateApplicationtarif.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlDateApplicationtarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlDateApplicationtarif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- lbDateApplication ----
                lbDateApplication.setText("Date d'application du tarif");
                lbDateApplication.setMaximumSize(new Dimension(200, 30));
                lbDateApplication.setMinimumSize(new Dimension(200, 30));
                lbDateApplication.setPreferredSize(new Dimension(200, 30));
                lbDateApplication.setName("lbDateApplication");
                pnlDateApplicationtarif.add(lbDateApplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- snDateApplication ----
                snDateApplication.setComponentPopupMenu(null);
                snDateApplication.setPreferredSize(new Dimension(115, 30));
                snDateApplication.setMinimumSize(new Dimension(115, 30));
                snDateApplication.setMaximumSize(new Dimension(105, 30));
                snDateApplication.setFont(new Font("sansserif", Font.PLAIN, 14));
                snDateApplication.setEnabled(false);
                snDateApplication.setFocusable(false);
                snDateApplication.setName("snDateApplication");
                pnlDateApplicationtarif.add(snDateApplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlInformationPrixInitial.add(pnlDateApplicationtarif, new GridBagConstraints(0, 0, 6, 1, 0.0, 0.0, GridBagConstraints.EAST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbPrixBaseInitial ----
              lbPrixBaseInitial.setText("Prix public");
              lbPrixBaseInitial.setHorizontalAlignment(SwingConstants.CENTER);
              lbPrixBaseInitial.setMaximumSize(new Dimension(100, 30));
              lbPrixBaseInitial.setMinimumSize(new Dimension(100, 30));
              lbPrixBaseInitial.setPreferredSize(new Dimension(100, 30));
              lbPrixBaseInitial.setName("lbPrixBaseInitial");
              pnlInformationPrixInitial.add(lbPrixBaseInitial, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbColonneTarifInitiale ----
              lbColonneTarifInitiale.setText("Colonne tarif");
              lbColonneTarifInitiale.setHorizontalAlignment(SwingConstants.CENTER);
              lbColonneTarifInitiale.setMaximumSize(new Dimension(120, 30));
              lbColonneTarifInitiale.setMinimumSize(new Dimension(120, 30));
              lbColonneTarifInitiale.setPreferredSize(new Dimension(120, 30));
              lbColonneTarifInitiale.setName("lbColonneTarifInitiale");
              pnlInformationPrixInitial.add(lbColonneTarifInitiale, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbOrigineInitiale ----
              lbOrigineInitiale.setText("Origine");
              lbOrigineInitiale.setHorizontalAlignment(SwingConstants.CENTER);
              lbOrigineInitiale.setName("lbOrigineInitiale");
              pnlInformationPrixInitial.add(lbOrigineInitiale, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbRemiseInitiale ----
              lbRemiseInitiale.setText("Remise");
              lbRemiseInitiale.setHorizontalAlignment(SwingConstants.CENTER);
              lbRemiseInitiale.setMaximumSize(new Dimension(100, 30));
              lbRemiseInitiale.setMinimumSize(new Dimension(100, 30));
              lbRemiseInitiale.setPreferredSize(new Dimension(100, 30));
              lbRemiseInitiale.setName("lbRemiseInitiale");
              pnlInformationPrixInitial.add(lbRemiseInitiale, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbPrixNetInitial ----
              lbPrixNetInitial.setText("Prix net");
              lbPrixNetInitial.setHorizontalAlignment(SwingConstants.CENTER);
              lbPrixNetInitial.setMaximumSize(new Dimension(100, 30));
              lbPrixNetInitial.setMinimumSize(new Dimension(100, 30));
              lbPrixNetInitial.setPreferredSize(new Dimension(100, 30));
              lbPrixNetInitial.setName("lbPrixNetInitial");
              pnlInformationPrixInitial.add(lbPrixNetInitial, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- zsPrixBaseInitial ----
              zsPrixBaseInitial.setEditable(false);
              zsPrixBaseInitial.setEnabled(false);
              zsPrixBaseInitial.setHorizontalAlignment(SwingConstants.TRAILING);
              zsPrixBaseInitial.setName("zsPrixBaseInitial");
              pnlInformationPrixInitial.add(zsPrixBaseInitial, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfColonneTarifInitiale ----
              tfColonneTarifInitiale.setEditable(false);
              tfColonneTarifInitiale.setEnabled(false);
              tfColonneTarifInitiale.setMinimumSize(new Dimension(90, 30));
              tfColonneTarifInitiale.setMaximumSize(new Dimension(90, 30));
              tfColonneTarifInitiale.setPreferredSize(new Dimension(90, 30));
              tfColonneTarifInitiale.setName("tfColonneTarifInitiale");
              pnlInformationPrixInitial.add(tfColonneTarifInitiale, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- zsOrigineInitiale ----
              zsOrigineInitiale.setEditable(false);
              zsOrigineInitiale.setEnabled(false);
              zsOrigineInitiale.setMinimumSize(new Dimension(150, 30));
              zsOrigineInitiale.setPreferredSize(new Dimension(150, 30));
              zsOrigineInitiale.setName("zsOrigineInitiale");
              pnlInformationPrixInitial.add(zsOrigineInitiale, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- zsRemiseInitiale ----
              zsRemiseInitiale.setEditable(false);
              zsRemiseInitiale.setEnabled(false);
              zsRemiseInitiale.setHorizontalAlignment(SwingConstants.TRAILING);
              zsRemiseInitiale.setName("zsRemiseInitiale");
              pnlInformationPrixInitial.add(zsRemiseInitiale, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbSymbolePourcentageRemiseInitiale ----
              lbSymbolePourcentageRemiseInitiale.setText("% ");
              lbSymbolePourcentageRemiseInitiale.setMaximumSize(new Dimension(20, 30));
              lbSymbolePourcentageRemiseInitiale.setMinimumSize(new Dimension(20, 30));
              lbSymbolePourcentageRemiseInitiale.setPreferredSize(new Dimension(20, 30));
              lbSymbolePourcentageRemiseInitiale.setHorizontalAlignment(SwingConstants.LEADING);
              lbSymbolePourcentageRemiseInitiale.setName("lbSymbolePourcentageRemiseInitiale");
              pnlInformationPrixInitial.add(lbSymbolePourcentageRemiseInitiale, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
                  GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- zsPrixNetInitial ----
              zsPrixNetInitial.setEditable(false);
              zsPrixNetInitial.setEnabled(false);
              zsPrixNetInitial.setHorizontalAlignment(SwingConstants.TRAILING);
              zsPrixNetInitial.setName("zsPrixNetInitial");
              pnlInformationPrixInitial.add(zsPrixNetInitial, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlTarifInitial.add(pnlInformationPrixInitial, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarif.add(pnlTarifInitial, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlTarifInitialMarge ========
          {
            pnlTarifInitialMarge.setTitre("Marge initiale");
            pnlTarifInitialMarge.setPreferredSize(new Dimension(360, 150));
            pnlTarifInitialMarge.setMinimumSize(new Dimension(360, 150));
            pnlTarifInitialMarge.setName("pnlTarifInitialMarge");
            pnlTarifInitialMarge.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifInitialMarge.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTarifInitialMarge.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTarifInitialMarge.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifInitialMarge.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
            
            // ---- lbIndiceDeMargeInitiale ----
            lbIndiceDeMargeInitiale.setText("Indice");
            lbIndiceDeMargeInitiale.setMaximumSize(new Dimension(100, 30));
            lbIndiceDeMargeInitiale.setMinimumSize(new Dimension(100, 30));
            lbIndiceDeMargeInitiale.setPreferredSize(new Dimension(100, 30));
            lbIndiceDeMargeInitiale.setHorizontalAlignment(SwingConstants.CENTER);
            lbIndiceDeMargeInitiale.setName("lbIndiceDeMargeInitiale");
            pnlTarifInitialMarge.add(lbIndiceDeMargeInitiale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- zsIndiceDeMargeInitiale ----
            zsIndiceDeMargeInitiale.setEditable(false);
            zsIndiceDeMargeInitiale.setEnabled(false);
            zsIndiceDeMargeInitiale.setHorizontalAlignment(SwingConstants.TRAILING);
            zsIndiceDeMargeInitiale.setName("zsIndiceDeMargeInitiale");
            pnlTarifInitialMarge.add(zsIndiceDeMargeInitiale, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbMargeInitiale ----
            lbMargeInitiale.setText("Marge");
            lbMargeInitiale.setMaximumSize(new Dimension(100, 30));
            lbMargeInitiale.setMinimumSize(new Dimension(100, 30));
            lbMargeInitiale.setPreferredSize(new Dimension(100, 30));
            lbMargeInitiale.setHorizontalAlignment(SwingConstants.CENTER);
            lbMargeInitiale.setName("lbMargeInitiale");
            pnlTarifInitialMarge.add(lbMargeInitiale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPrixRevientInitial ----
            lbPrixRevientInitial.setText("<html>Prix de revient<br>standard</html>");
            lbPrixRevientInitial.setMaximumSize(new Dimension(100, 30));
            lbPrixRevientInitial.setMinimumSize(new Dimension(100, 30));
            lbPrixRevientInitial.setPreferredSize(new Dimension(100, 30));
            lbPrixRevientInitial.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixRevientInitial.setName("lbPrixRevientInitial");
            pnlTarifInitialMarge.add(lbPrixRevientInitial, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- zsMargeInitiale ----
            zsMargeInitiale.setEditable(false);
            zsMargeInitiale.setEnabled(false);
            zsMargeInitiale.setHorizontalAlignment(SwingConstants.TRAILING);
            zsMargeInitiale.setName("zsMargeInitiale");
            pnlTarifInitialMarge.add(zsMargeInitiale, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbSymbolePourcentageMargeInitiale ----
            lbSymbolePourcentageMargeInitiale.setText("%");
            lbSymbolePourcentageMargeInitiale.setMaximumSize(new Dimension(20, 30));
            lbSymbolePourcentageMargeInitiale.setMinimumSize(new Dimension(20, 30));
            lbSymbolePourcentageMargeInitiale.setPreferredSize(new Dimension(20, 30));
            lbSymbolePourcentageMargeInitiale.setHorizontalAlignment(SwingConstants.LEADING);
            lbSymbolePourcentageMargeInitiale.setName("lbSymbolePourcentageMargeInitiale");
            pnlTarifInitialMarge.add(lbSymbolePourcentageMargeInitiale, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- zsPrixRevientInitial ----
            zsPrixRevientInitial.setEditable(false);
            zsPrixRevientInitial.setEnabled(false);
            zsPrixRevientInitial.setHorizontalAlignment(SwingConstants.TRAILING);
            zsPrixRevientInitial.setName("zsPrixRevientInitial");
            pnlTarifInitialMarge.add(zsPrixRevientInitial, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarif.add(pnlTarifInitialMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlTarifNegocie ========
          {
            pnlTarifNegocie.setTitre("Prix n\u00e9goci\u00e9");
            pnlTarifNegocie.setPreferredSize(new Dimension(650, 150));
            pnlTarifNegocie.setMinimumSize(new Dimension(650, 115));
            pnlTarifNegocie.setMaximumSize(new Dimension(650, 150));
            pnlTarifNegocie.setName("pnlTarifNegocie");
            pnlTarifNegocie.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
            
            // ======== pnlPrixNegocie ========
            {
              pnlPrixNegocie.setOpaque(false);
              pnlPrixNegocie.setMinimumSize(new Dimension(620, 65));
              pnlPrixNegocie.setPreferredSize(new Dimension(620, 105));
              pnlPrixNegocie.setMaximumSize(new Dimension(620, 105));
              pnlPrixNegocie.setName("pnlPrixNegocie");
              pnlPrixNegocie.setLayout(new CardLayout());
              
              // ======== pnlNegocePrixNegocie ========
              {
                pnlNegocePrixNegocie.setPreferredSize(new Dimension(600, 65));
                pnlNegocePrixNegocie.setMinimumSize(new Dimension(600, 65));
                pnlNegocePrixNegocie.setName("pnlNegocePrixNegocie");
                pnlNegocePrixNegocie.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlNegocePrixNegocie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlNegocePrixNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlNegocePrixNegocie.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlNegocePrixNegocie.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbNegocePrixPublic ----
                lbNegocePrixPublic.setText("Prix public");
                lbNegocePrixPublic.setHorizontalAlignment(SwingConstants.CENTER);
                lbNegocePrixPublic.setMaximumSize(new Dimension(100, 30));
                lbNegocePrixPublic.setMinimumSize(new Dimension(100, 30));
                lbNegocePrixPublic.setPreferredSize(new Dimension(100, 30));
                lbNegocePrixPublic.setName("lbNegocePrixPublic");
                pnlNegocePrixNegocie.add(lbNegocePrixPublic, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbNegoceOrigine ----
                lbNegoceOrigine.setText("Colonne tarif");
                lbNegoceOrigine.setHorizontalAlignment(SwingConstants.CENTER);
                lbNegoceOrigine.setMaximumSize(new Dimension(100, 30));
                lbNegoceOrigine.setMinimumSize(new Dimension(100, 30));
                lbNegoceOrigine.setPreferredSize(new Dimension(100, 30));
                lbNegoceOrigine.setName("lbNegoceOrigine");
                pnlNegocePrixNegocie.add(lbNegoceOrigine, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbNegoceRemise ----
                lbNegoceRemise.setText("Remise");
                lbNegoceRemise.setHorizontalAlignment(SwingConstants.CENTER);
                lbNegoceRemise.setMaximumSize(new Dimension(100, 30));
                lbNegoceRemise.setMinimumSize(new Dimension(100, 30));
                lbNegoceRemise.setPreferredSize(new Dimension(100, 30));
                lbNegoceRemise.setName("lbNegoceRemise");
                pnlNegocePrixNegocie.add(lbNegoceRemise, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbNegocePrixNet ----
                lbNegocePrixNet.setText("Prix net");
                lbNegocePrixNet.setHorizontalAlignment(SwingConstants.CENTER);
                lbNegocePrixNet.setMaximumSize(new Dimension(100, 30));
                lbNegocePrixNet.setMinimumSize(new Dimension(100, 30));
                lbNegocePrixNet.setPreferredSize(new Dimension(100, 30));
                lbNegocePrixNet.setName("lbNegocePrixNet");
                pnlNegocePrixNegocie.add(lbNegocePrixNet, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfNegocePrixPublic ----
                tfNegocePrixPublic.setEditable(false);
                tfNegocePrixPublic.setEnabled(false);
                tfNegocePrixPublic.setHorizontalAlignment(SwingConstants.TRAILING);
                tfNegocePrixPublic.setName("tfNegocePrixPublic");
                pnlNegocePrixNegocie.add(tfNegocePrixPublic, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- cbNegoceColonneTarif ----
                cbNegoceColonneTarif.setMinimumSize(new Dimension(150, 30));
                cbNegoceColonneTarif.setPreferredSize(new Dimension(150, 30));
                cbNegoceColonneTarif.setName("cbNegoceColonneTarif");
                cbNegoceColonneTarif.addItemListener(e -> cbNegoceOrigineItemStateChanged(e));
                pnlNegocePrixNegocie.add(cbNegoceColonneTarif, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfNegoceRemise ----
                tfNegoceRemise.setHorizontalAlignment(SwingConstants.TRAILING);
                tfNegoceRemise.setName("tfNegoceRemise");
                tfNegoceRemise.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfNegoceRemiseFocusLost(e);
                  }
                });
                tfNegoceRemise.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyPressed(KeyEvent e) {
                    tfNegoceRemiseKeyPressed(e);
                  }
                });
                pnlNegocePrixNegocie.add(tfNegoceRemise, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymbolePourcentageNegoceRemise ----
                lbSymbolePourcentageNegoceRemise.setText("% ");
                lbSymbolePourcentageNegoceRemise.setMaximumSize(new Dimension(20, 30));
                lbSymbolePourcentageNegoceRemise.setMinimumSize(new Dimension(20, 30));
                lbSymbolePourcentageNegoceRemise.setPreferredSize(new Dimension(20, 30));
                lbSymbolePourcentageNegoceRemise.setHorizontalAlignment(SwingConstants.LEADING);
                lbSymbolePourcentageNegoceRemise.setName("lbSymbolePourcentageNegoceRemise");
                pnlNegocePrixNegocie.add(lbSymbolePourcentageNegoceRemise, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfNegocePrixNet ----
                tfNegocePrixNet.setHorizontalAlignment(SwingConstants.TRAILING);
                tfNegocePrixNet.setName("tfNegocePrixNet");
                tfNegocePrixNet.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfNegocePrixNetFocusLost(e);
                  }
                });
                tfNegocePrixNet.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyPressed(KeyEvent e) {
                    tfNegocePrixNetKeyPressed(e);
                  }
                });
                tfNegocePrixNet.addMouseListener(new MouseAdapter() {
                  @Override
                  public void mouseClicked(MouseEvent e) {
                    tfNegocePrixNetMouseClicked(e);
                  }
                });
                pnlNegocePrixNegocie.add(tfNegocePrixNet, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrixNegocie.add(pnlNegocePrixNegocie, "Negoce");
              
              // ======== pnlClassiquePrixNegocie ========
              {
                pnlClassiquePrixNegocie.setMinimumSize(new Dimension(620, 105));
                pnlClassiquePrixNegocie.setPreferredSize(new Dimension(620, 105));
                pnlClassiquePrixNegocie.setName("pnlClassiquePrixNegocie");
                pnlClassiquePrixNegocie.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlClassiquePrixNegocie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlClassiquePrixNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlClassiquePrixNegocie.getLayout()).columnWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlClassiquePrixNegocie.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbClassiquePrixBase ----
                lbClassiquePrixBase.setText("Prix base");
                lbClassiquePrixBase.setHorizontalAlignment(SwingConstants.CENTER);
                lbClassiquePrixBase.setMaximumSize(new Dimension(100, 30));
                lbClassiquePrixBase.setMinimumSize(new Dimension(100, 30));
                lbClassiquePrixBase.setPreferredSize(new Dimension(100, 30));
                lbClassiquePrixBase.setName("lbClassiquePrixBase");
                pnlClassiquePrixNegocie.add(lbClassiquePrixBase, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbClassiqueColonneTarif ----
                lbClassiqueColonneTarif.setText("Colonne tarif");
                lbClassiqueColonneTarif.setHorizontalAlignment(SwingConstants.CENTER);
                lbClassiqueColonneTarif.setMaximumSize(new Dimension(120, 30));
                lbClassiqueColonneTarif.setMinimumSize(new Dimension(120, 30));
                lbClassiqueColonneTarif.setPreferredSize(new Dimension(120, 30));
                lbClassiqueColonneTarif.setName("lbClassiqueColonneTarif");
                pnlClassiquePrixNegocie.add(lbClassiqueColonneTarif, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbClassiqueOrigine ----
                lbClassiqueOrigine.setText("Origine");
                lbClassiqueOrigine.setHorizontalAlignment(SwingConstants.CENTER);
                lbClassiqueOrigine.setName("lbClassiqueOrigine");
                pnlClassiquePrixNegocie.add(lbClassiqueOrigine, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbClassiqueRemise ----
                lbClassiqueRemise.setText("Remise");
                lbClassiqueRemise.setHorizontalAlignment(SwingConstants.CENTER);
                lbClassiqueRemise.setMaximumSize(new Dimension(100, 30));
                lbClassiqueRemise.setMinimumSize(new Dimension(100, 30));
                lbClassiqueRemise.setPreferredSize(new Dimension(100, 30));
                lbClassiqueRemise.setName("lbClassiqueRemise");
                pnlClassiquePrixNegocie.add(lbClassiqueRemise, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbClassiquePrixNet ----
                lbClassiquePrixNet.setText("Prix net");
                lbClassiquePrixNet.setHorizontalAlignment(SwingConstants.CENTER);
                lbClassiquePrixNet.setMaximumSize(new Dimension(100, 30));
                lbClassiquePrixNet.setMinimumSize(new Dimension(100, 30));
                lbClassiquePrixNet.setPreferredSize(new Dimension(100, 30));
                lbClassiquePrixNet.setName("lbClassiquePrixNet");
                pnlClassiquePrixNegocie.add(lbClassiquePrixNet, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfClassiquePrixBaseSaisi ----
                tfClassiquePrixBaseSaisi.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiquePrixBaseSaisi.setName("tfClassiquePrixBaseSaisi");
                tfClassiquePrixBaseSaisi.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfClassiquePrixBaseSaisiFocusLost(e);
                  }
                });
                tfClassiquePrixBaseSaisi.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyPressed(KeyEvent e) {
                    tfClassiquePrixBaseSaisiKeyPressed(e);
                  }
                });
                pnlClassiquePrixNegocie.add(tfClassiquePrixBaseSaisi, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- cbClassiqueColonneTarifSaisie ----
                cbClassiqueColonneTarifSaisie.setMinimumSize(new Dimension(100, 30));
                cbClassiqueColonneTarifSaisie.setPreferredSize(new Dimension(100, 30));
                cbClassiqueColonneTarifSaisie.setName("cbClassiqueColonneTarifSaisie");
                cbClassiqueColonneTarifSaisie.addItemListener(e -> cbClassiqueColonneTarifSaisieItemStateChanged(e));
                pnlClassiquePrixNegocie.add(cbClassiqueColonneTarifSaisie, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfClassiqueRemiseSaisie ----
                tfClassiqueRemiseSaisie.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiqueRemiseSaisie.setName("tfClassiqueRemiseSaisie");
                tfClassiqueRemiseSaisie.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfClassiqueRemiseSaisieFocusLost(e);
                  }
                });
                tfClassiqueRemiseSaisie.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyPressed(KeyEvent e) {
                    tfClassiqueRemiseSaisieKeyPressed(e);
                  }
                });
                pnlClassiquePrixNegocie.add(tfClassiqueRemiseSaisie, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbSymbolePourcentageClassiqueRemiseSaisie ----
                lbSymbolePourcentageClassiqueRemiseSaisie.setText("% ");
                lbSymbolePourcentageClassiqueRemiseSaisie.setMaximumSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemiseSaisie.setMinimumSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemiseSaisie.setPreferredSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemiseSaisie.setHorizontalAlignment(SwingConstants.LEADING);
                lbSymbolePourcentageClassiqueRemiseSaisie.setName("lbSymbolePourcentageClassiqueRemiseSaisie");
                pnlClassiquePrixNegocie.add(lbSymbolePourcentageClassiqueRemiseSaisie, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfClassiquePrixNetSaisi ----
                tfClassiquePrixNetSaisi.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiquePrixNetSaisi.setName("tfClassiquePrixNetSaisi");
                tfClassiquePrixNetSaisi.addFocusListener(new FocusAdapter() {
                  @Override
                  public void focusLost(FocusEvent e) {
                    tfClassiquePrixNetSaisiFocusLost(e);
                  }
                });
                tfClassiquePrixNetSaisi.addKeyListener(new KeyAdapter() {
                  @Override
                  public void keyPressed(KeyEvent e) {
                    tfClassiquePrixNetSaisiKeyPressed(e);
                  }
                });
                pnlClassiquePrixNegocie.add(tfClassiquePrixNetSaisi, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfClassiquePrixBase ----
                tfClassiquePrixBase.setEditable(false);
                tfClassiquePrixBase.setEnabled(false);
                tfClassiquePrixBase.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiquePrixBase.setName("tfClassiquePrixBase");
                pnlClassiquePrixNegocie.add(tfClassiquePrixBase, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfClassiqueColonneTarif ----
                tfClassiqueColonneTarif.setEditable(false);
                tfClassiqueColonneTarif.setEnabled(false);
                tfClassiqueColonneTarif.setName("tfClassiqueColonneTarif");
                pnlClassiquePrixNegocie.add(tfClassiqueColonneTarif, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfClassiqueOrigine ----
                tfClassiqueOrigine.setEditable(false);
                tfClassiqueOrigine.setEnabled(false);
                tfClassiqueOrigine.setPreferredSize(new Dimension(150, 30));
                tfClassiqueOrigine.setMaximumSize(new Dimension(150, 30));
                tfClassiqueOrigine.setMinimumSize(new Dimension(150, 30));
                tfClassiqueOrigine.setName("tfClassiqueOrigine");
                pnlClassiquePrixNegocie.add(tfClassiqueOrigine, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfClassiqueRemise ----
                tfClassiqueRemise.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiqueRemise.setEditable(false);
                tfClassiqueRemise.setEnabled(false);
                tfClassiqueRemise.setName("tfClassiqueRemise");
                pnlClassiquePrixNegocie.add(tfClassiqueRemise, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymbolePourcentageClassiqueRemise ----
                lbSymbolePourcentageClassiqueRemise.setText("% ");
                lbSymbolePourcentageClassiqueRemise.setMaximumSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemise.setMinimumSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemise.setPreferredSize(new Dimension(20, 30));
                lbSymbolePourcentageClassiqueRemise.setHorizontalAlignment(SwingConstants.LEADING);
                lbSymbolePourcentageClassiqueRemise.setName("lbSymbolePourcentageClassiqueRemise");
                pnlClassiquePrixNegocie.add(lbSymbolePourcentageClassiqueRemise, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfClassiquePrixNet ----
                tfClassiquePrixNet.setHorizontalAlignment(SwingConstants.TRAILING);
                tfClassiquePrixNet.setEditable(false);
                tfClassiquePrixNet.setEnabled(false);
                tfClassiquePrixNet.setName("tfClassiquePrixNet");
                pnlClassiquePrixNegocie.add(tfClassiquePrixNet, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPrixNegocie.add(pnlClassiquePrixNegocie, "Classique");
            }
            pnlTarifNegocie.add(pnlPrixNegocie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlGratuit ========
            {
              pnlGratuit.setOpaque(false);
              pnlGratuit.setName("pnlGratuit");
              pnlGratuit.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlGratuit.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlGratuit.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlGratuit.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlGratuit.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- chkGratuit ----
              chkGratuit.setText("Article gratuit");
              chkGratuit.setFont(new Font("sansserif", Font.PLAIN, 14));
              chkGratuit.setPreferredSize(new Dimension(105, 30));
              chkGratuit.setMaximumSize(new Dimension(105, 30));
              chkGratuit.setMinimumSize(new Dimension(105, 30));
              chkGratuit.setName("chkGratuit");
              chkGratuit.addActionListener(e -> chkGratuitActionPerformed(e));
              pnlGratuit.add(chkGratuit, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbTypeGratuit ----
              lbTypeGratuit.setText("Type de gratuit\u00e9");
              lbTypeGratuit.setHorizontalAlignment(SwingConstants.RIGHT);
              lbTypeGratuit.setName("lbTypeGratuit");
              pnlGratuit.add(lbTypeGratuit, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snTypeGratuit ----
              snTypeGratuit.setMinimumSize(new Dimension(200, 30));
              snTypeGratuit.setPreferredSize(new Dimension(200, 30));
              snTypeGratuit.setName("snTypeGratuit");
              snTypeGratuit.addSNComposantListener(e -> snTypeGratuitValueChanged(e));
              pnlGratuit.add(snTypeGratuit, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlTarifNegocie.add(pnlGratuit, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarif.add(pnlTarifNegocie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlTarifNegocieMarge ========
          {
            pnlTarifNegocieMarge.setTitre("Marge n\u00e9goci\u00e9e");
            pnlTarifNegocieMarge.setPreferredSize(new Dimension(360, 150));
            pnlTarifNegocieMarge.setMinimumSize(new Dimension(360, 105));
            pnlTarifNegocieMarge.setMaximumSize(new Dimension(360, 150));
            pnlTarifNegocieMarge.setName("pnlTarifNegocieMarge");
            pnlTarifNegocieMarge.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbIndiceDeMarge ----
            lbIndiceDeMarge.setText("Indice");
            lbIndiceDeMarge.setMaximumSize(new Dimension(100, 30));
            lbIndiceDeMarge.setMinimumSize(new Dimension(100, 30));
            lbIndiceDeMarge.setPreferredSize(new Dimension(100, 30));
            lbIndiceDeMarge.setHorizontalAlignment(SwingConstants.CENTER);
            lbIndiceDeMarge.setName("lbIndiceDeMarge");
            pnlTarifNegocieMarge.add(lbIndiceDeMarge, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfIndiceDeMarge ----
            tfIndiceDeMarge.setHorizontalAlignment(SwingConstants.TRAILING);
            tfIndiceDeMarge.setName("tfIndiceDeMarge");
            tfIndiceDeMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfIndiceDeMargeFocusLost(e);
              }
            });
            tfIndiceDeMarge.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfIndiceDeMargeKeyPressed(e);
              }
            });
            pnlTarifNegocieMarge.add(tfIndiceDeMarge, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbMarge ----
            lbMarge.setText("Marge");
            lbMarge.setMaximumSize(new Dimension(100, 30));
            lbMarge.setMinimumSize(new Dimension(100, 30));
            lbMarge.setPreferredSize(new Dimension(100, 30));
            lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
            lbMarge.setName("lbMarge");
            pnlTarifNegocieMarge.add(lbMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPrixRevient ----
            lbPrixRevient.setText("<html>Prix de revient<br>standard</html>");
            lbPrixRevient.setMaximumSize(new Dimension(100, 30));
            lbPrixRevient.setMinimumSize(new Dimension(100, 30));
            lbPrixRevient.setPreferredSize(new Dimension(100, 30));
            lbPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixRevient.setName("lbPrixRevient");
            pnlTarifNegocieMarge.add(lbPrixRevient, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- pnlEspace ----
            pnlEspace.setPreferredSize(new Dimension(100, 30));
            pnlEspace.setMinimumSize(new Dimension(100, 30));
            pnlEspace.setName("pnlEspace");
            pnlTarifNegocieMarge.add(pnlEspace, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfMarge ----
            tfMarge.setHorizontalAlignment(SwingConstants.TRAILING);
            tfMarge.setName("tfMarge");
            tfMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfMargeFocusLost(e);
              }
            });
            tfMarge.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfMargeKeyPressed(e);
              }
            });
            pnlTarifNegocieMarge.add(tfMarge, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbSymbolePourcentageMarge ----
            lbSymbolePourcentageMarge.setText("%");
            lbSymbolePourcentageMarge.setMaximumSize(new Dimension(20, 30));
            lbSymbolePourcentageMarge.setMinimumSize(new Dimension(20, 30));
            lbSymbolePourcentageMarge.setPreferredSize(new Dimension(20, 30));
            lbSymbolePourcentageMarge.setHorizontalAlignment(SwingConstants.LEADING);
            lbSymbolePourcentageMarge.setName("lbSymbolePourcentageMarge");
            pnlTarifNegocieMarge.add(lbSymbolePourcentageMarge, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPrixRevient ----
            tfPrixRevient.setEditable(false);
            tfPrixRevient.setEnabled(false);
            tfPrixRevient.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixRevient.setName("tfPrixRevient");
            pnlTarifNegocieMarge.add(tfPrixRevient, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTarif.add(pnlTarifNegocieMarge, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlVente.add(pnlTarif, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMessageConditionChantier ----
        lbMessageConditionChantier.setText("Message condition chantier");
        lbMessageConditionChantier.setMaximumSize(new Dimension(150, 40));
        lbMessageConditionChantier.setPreferredSize(new Dimension(150, 40));
        lbMessageConditionChantier.setName("lbMessageConditionChantier");
        pnlVente.add(lbMessageConditionChantier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlVente, "Vente");
      
      // ======== pnlAchat ========
      {
        pnlAchat.setName("pnlAchat");
        pnlAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAchat.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlEnteteAchat ========
        {
          pnlEnteteAchat.setOpaque(false);
          pnlEnteteAchat.setName("pnlEnteteAchat");
          pnlEnteteAchat.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnteteAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlEnteteAchat.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEnteteAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEnteteAchat.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- lbAchatQuantiteUCV ----
          lbAchatQuantiteUCV.setText("Quantit\u00e9 en UCV");
          lbAchatQuantiteUCV.setName("lbAchatQuantiteUCV");
          pnlEnteteAchat.add(lbAchatQuantiteUCV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlQuantiteUCV ========
          {
            pnlQuantiteUCV.setOpaque(false);
            pnlQuantiteUCV.setName("pnlQuantiteUCV");
            pnlQuantiteUCV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlQuantiteUCV.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlQuantiteUCV.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlQuantiteUCV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlQuantiteUCV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfAchatQuantiteUCV ----
            tfAchatQuantiteUCV.setHorizontalAlignment(SwingConstants.TRAILING);
            tfAchatQuantiteUCV.setName("tfAchatQuantiteUCV");
            tfAchatQuantiteUCV.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfquantiteCommandeUCachatFocusLost(e);
              }
            });
            pnlQuantiteUCV.add(tfAchatQuantiteUCV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfAchatUniteQuantiteUCV ----
            tfAchatUniteQuantiteUCV.setEditable(false);
            tfAchatUniteQuantiteUCV.setEnabled(false);
            tfAchatUniteQuantiteUCV.setMinimumSize(new Dimension(36, 30));
            tfAchatUniteQuantiteUCV.setPreferredSize(new Dimension(36, 30));
            tfAchatUniteQuantiteUCV.setMaximumSize(new Dimension(36, 30));
            tfAchatUniteQuantiteUCV.setName("tfAchatUniteQuantiteUCV");
            pnlQuantiteUCV.add(tfAchatUniteQuantiteUCV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnteteAchat.add(pnlQuantiteUCV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbAchatRatioUVparUCV ----
          lbAchatRatioUVparUCV.setText("Ratio UV/UCV");
          lbAchatRatioUVparUCV.setMaximumSize(new Dimension(300, 30));
          lbAchatRatioUVparUCV.setMinimumSize(new Dimension(300, 30));
          lbAchatRatioUVparUCV.setPreferredSize(new Dimension(300, 30));
          lbAchatRatioUVparUCV.setName("lbAchatRatioUVparUCV");
          pnlEnteteAchat.add(lbAchatRatioUVparUCV, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlUVParUCV ========
          {
            pnlUVParUCV.setOpaque(false);
            pnlUVParUCV.setName("pnlUVParUCV");
            pnlUVParUCV.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlUVParUCV.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlUVParUCV.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlUVParUCV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlUVParUCV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfAchatRatioUVParUCV ----
            tfAchatRatioUVParUCV.setHorizontalAlignment(SwingConstants.TRAILING);
            tfAchatRatioUVParUCV.setName("tfAchatRatioUVParUCV");
            tfAchatRatioUVParUCV.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfquantiteCommandeeUVachatFocusLost(e);
              }
            });
            pnlUVParUCV.add(tfAchatRatioUVParUCV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfAchatUniteRatioUVparUCV ----
            tfAchatUniteRatioUVparUCV.setEditable(false);
            tfAchatUniteRatioUVparUCV.setEnabled(false);
            tfAchatUniteRatioUVparUCV.setMinimumSize(new Dimension(36, 30));
            tfAchatUniteRatioUVparUCV.setPreferredSize(new Dimension(36, 30));
            tfAchatUniteRatioUVparUCV.setMaximumSize(new Dimension(36, 30));
            tfAchatUniteRatioUVparUCV.setName("tfAchatUniteRatioUVparUCV");
            pnlUVParUCV.add(tfAchatUniteRatioUVparUCV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnteteAchat.add(pnlUVParUCV, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlAchat.add(pnlEnteteAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlNegociationAchat ========
        {
          pnlNegociationAchat.setTitre("N\u00e9gociation achats");
          pnlNegociationAchat.setName("pnlNegociationAchat");
          pnlNegociationAchat.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlNegociationAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlNegociationAchat.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbRemisesFournisseur ----
          lbRemisesFournisseur.setText("Remises fournisseur");
          lbRemisesFournisseur.setMaximumSize(new Dimension(200, 30));
          lbRemisesFournisseur.setMinimumSize(new Dimension(200, 30));
          lbRemisesFournisseur.setPreferredSize(new Dimension(200, 30));
          lbRemisesFournisseur.setName("lbRemisesFournisseur");
          pnlNegociationAchat.add(lbRemisesFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixAchatBrutHT ----
          lbPrixAchatBrutHT.setText("Prix d'achat brut HT");
          lbPrixAchatBrutHT.setMaximumSize(new Dimension(200, 30));
          lbPrixAchatBrutHT.setMinimumSize(new Dimension(200, 30));
          lbPrixAchatBrutHT.setPreferredSize(new Dimension(200, 30));
          lbPrixAchatBrutHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
          lbPrixAchatBrutHT.setName("lbPrixAchatBrutHT");
          pnlNegociationAchat.add(lbPrixAchatBrutHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixAchatBrutHT ========
          {
            pnlPrixAchatBrutHT.setOpaque(false);
            pnlPrixAchatBrutHT.setName("pnlPrixAchatBrutHT");
            pnlPrixAchatBrutHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixAchatBrutHT ----
            tfPrixAchatBrutHT.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixAchatBrutHT.setName("tfPrixAchatBrutHT");
            tfPrixAchatBrutHT.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPrixAchatBrutFocusLost(e);
              }
            });
            pnlPrixAchatBrutHT.add(tfPrixAchatBrutHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixAchatBrutHT ----
            lbUnitePrixAchatBrutHT.setText("UA");
            lbUnitePrixAchatBrutHT.setHorizontalAlignment(SwingConstants.LEADING);
            lbUnitePrixAchatBrutHT.setMinimumSize(new Dimension(50, 30));
            lbUnitePrixAchatBrutHT.setPreferredSize(new Dimension(50, 30));
            lbUnitePrixAchatBrutHT.setName("lbUnitePrixAchatBrutHT");
            pnlPrixAchatBrutHT.add(lbUnitePrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlPrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemisesFournisseur ========
          {
            pnlRemisesFournisseur.setOpaque(false);
            pnlRemisesFournisseur.setName("pnlRemisesFournisseur");
            pnlRemisesFournisseur.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfRemiseFournisseur1 ----
            tfRemiseFournisseur1.setHorizontalAlignment(SwingConstants.TRAILING);
            tfRemiseFournisseur1.setName("tfRemiseFournisseur1");
            tfRemiseFournisseur1.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs1FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFournisseur1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemiseFournisseur1 ----
            lbUniteRemiseFournisseur1.setText("% ");
            lbUniteRemiseFournisseur1.setHorizontalAlignment(SwingConstants.LEADING);
            lbUniteRemiseFournisseur1.setMinimumSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur1.setPreferredSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur1.setName("lbUniteRemiseFournisseur1");
            pnlRemisesFournisseur.add(lbUniteRemiseFournisseur1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFournisseur2 ----
            tfRemiseFournisseur2.setHorizontalAlignment(SwingConstants.TRAILING);
            tfRemiseFournisseur2.setName("tfRemiseFournisseur2");
            tfRemiseFournisseur2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs2FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFournisseur2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemiseFournisseur2 ----
            lbUniteRemiseFournisseur2.setText("% ");
            lbUniteRemiseFournisseur2.setHorizontalAlignment(SwingConstants.LEADING);
            lbUniteRemiseFournisseur2.setMinimumSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur2.setPreferredSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur2.setName("lbUniteRemiseFournisseur2");
            pnlRemisesFournisseur.add(lbUniteRemiseFournisseur2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFournisseur3 ----
            tfRemiseFournisseur3.setHorizontalAlignment(SwingConstants.TRAILING);
            tfRemiseFournisseur3.setName("tfRemiseFournisseur3");
            tfRemiseFournisseur3.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs3FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFournisseur3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemiseFournisseur3 ----
            lbUniteRemiseFournisseur3.setText("% ");
            lbUniteRemiseFournisseur3.setHorizontalAlignment(SwingConstants.LEADING);
            lbUniteRemiseFournisseur3.setMinimumSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur3.setPreferredSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur3.setName("lbUniteRemiseFournisseur3");
            pnlRemisesFournisseur.add(lbUniteRemiseFournisseur3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFournisseur4 ----
            tfRemiseFournisseur4.setHorizontalAlignment(SwingConstants.TRAILING);
            tfRemiseFournisseur4.setName("tfRemiseFournisseur4");
            tfRemiseFournisseur4.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs4FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFournisseur4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteRemiseFournisseur4 ----
            lbUniteRemiseFournisseur4.setText("% ");
            lbUniteRemiseFournisseur4.setHorizontalAlignment(SwingConstants.LEADING);
            lbUniteRemiseFournisseur4.setMinimumSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur4.setPreferredSize(new Dimension(20, 30));
            lbUniteRemiseFournisseur4.setName("lbUniteRemiseFournisseur4");
            pnlRemisesFournisseur.add(lbUniteRemiseFournisseur4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlRemisesFournisseur, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTaxeMajoration ----
          lbTaxeMajoration.setText("Taxe ou majoration");
          lbTaxeMajoration.setName("lbTaxeMajoration");
          pnlNegociationAchat.add(lbTaxeMajoration, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlTaxeMajoration ========
          {
            pnlTaxeMajoration.setName("pnlTaxeMajoration");
            pnlTaxeMajoration.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfTaxeMajoration ----
            tfTaxeMajoration.setHorizontalAlignment(SwingConstants.RIGHT);
            tfTaxeMajoration.setName("tfTaxeMajoration");
            tfTaxeMajoration.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTaxeMajorationFocusLost(e);
              }
            });
            pnlTaxeMajoration.add(tfTaxeMajoration, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentageTaxeMajoration ----
            lbPourcentageTaxeMajoration.setText("% ");
            lbPourcentageTaxeMajoration.setHorizontalAlignment(SwingConstants.LEADING);
            lbPourcentageTaxeMajoration.setMinimumSize(new Dimension(20, 30));
            lbPourcentageTaxeMajoration.setPreferredSize(new Dimension(20, 30));
            lbPourcentageTaxeMajoration.setName("lbPourcentageTaxeMajoration");
            pnlTaxeMajoration.add(lbPourcentageTaxeMajoration, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlTaxeMajoration, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMontantConditionnement ----
          lbMontantConditionnement.setText("Montant conditionnement");
          lbMontantConditionnement.setMaximumSize(new Dimension(200, 30));
          lbMontantConditionnement.setMinimumSize(new Dimension(200, 30));
          lbMontantConditionnement.setPreferredSize(new Dimension(200, 30));
          lbMontantConditionnement.setName("lbMontantConditionnement");
          pnlNegociationAchat.add(lbMontantConditionnement, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfMontantConditionnement ----
          tfMontantConditionnement.setHorizontalAlignment(SwingConstants.TRAILING);
          tfMontantConditionnement.setName("tfMontantConditionnement");
          tfMontantConditionnement.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfValeurConditionnementAchatFocusLost(e);
            }
          });
          pnlNegociationAchat.add(tfMontantConditionnement, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixAchatNetHT ----
          lbPrixAchatNetHT.setText("Prix d'achat net HT");
          lbPrixAchatNetHT.setMaximumSize(new Dimension(200, 30));
          lbPrixAchatNetHT.setMinimumSize(new Dimension(200, 30));
          lbPrixAchatNetHT.setPreferredSize(new Dimension(200, 30));
          lbPrixAchatNetHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
          lbPrixAchatNetHT.setName("lbPrixAchatNetHT");
          pnlNegociationAchat.add(lbPrixAchatNetHT, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixAchatNetHT ========
          {
            pnlPrixAchatNetHT.setOpaque(false);
            pnlPrixAchatNetHT.setName("pnlPrixAchatNetHT");
            pnlPrixAchatNetHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixAchatNetHT ----
            tfPrixAchatNetHT.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixAchatNetHT.setEditable(false);
            tfPrixAchatNetHT.setEnabled(false);
            tfPrixAchatNetHT.setName("tfPrixAchatNetHT");
            pnlPrixAchatNetHT.add(tfPrixAchatNetHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixAchatNetHT ----
            lbUnitePrixAchatNetHT.setText("UA");
            lbUnitePrixAchatNetHT.setHorizontalAlignment(SwingConstants.LEADING);
            lbUnitePrixAchatNetHT.setMinimumSize(new Dimension(50, 30));
            lbUnitePrixAchatNetHT.setPreferredSize(new Dimension(50, 30));
            lbUnitePrixAchatNetHT.setName("lbUnitePrixAchatNetHT");
            pnlPrixAchatNetHT.add(lbUnitePrixAchatNetHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlPrixAchatNetHT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbMontantPort ----
          rbMontantPort.setText("  Montant port HT");
          rbMontantPort.setFont(rbMontantPort.getFont().deriveFont(rbMontantPort.getFont().getSize() + 2f));
          rbMontantPort.setHorizontalAlignment(SwingConstants.RIGHT);
          rbMontantPort.setName("rbMontantPort");
          rbMontantPort.addActionListener(e -> rbMontantPortActionPerformed(e));
          pnlNegociationAchat.add(rbMontantPort, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlMontantPort ========
          {
            pnlMontantPort.setOpaque(false);
            pnlMontantPort.setName("pnlMontantPort");
            pnlMontantPort.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMontantPort.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlMontantPort.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlMontantPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlMontantPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfValeurPort ----
            tfValeurPort.setHorizontalAlignment(SwingConstants.TRAILING);
            tfValeurPort.setName("tfValeurPort");
            tfValeurPort.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortValeurFocusLost(e);
              }
            });
            pnlMontantPort.add(tfValeurPort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPortPoids ----
            lbPortPoids.setText("x poids");
            lbPortPoids.setMaximumSize(new Dimension(50, 30));
            lbPortPoids.setMinimumSize(new Dimension(50, 30));
            lbPortPoids.setPreferredSize(new Dimension(50, 30));
            lbPortPoids.setHorizontalAlignment(SwingConstants.CENTER);
            lbPortPoids.setName("lbPortPoids");
            pnlMontantPort.add(lbPortPoids, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPoidsPort ----
            tfPoidsPort.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPoidsPort.setName("tfPoidsPort");
            tfPoidsPort.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortPoidsFocusLost(e);
              }
            });
            pnlMontantPort.add(tfPoidsPort, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlMontantPort, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbPourcentagePort ----
          rbPourcentagePort.setText("Pourcentage port");
          rbPourcentagePort.setFont(rbPourcentagePort.getFont().deriveFont(rbPourcentagePort.getFont().getSize() + 2f));
          rbPourcentagePort.setHorizontalAlignment(SwingConstants.RIGHT);
          rbPourcentagePort.setName("rbPourcentagePort");
          rbPourcentagePort.addActionListener(e -> rbCoeffPortActionPerformed(e));
          pnlNegociationAchat.add(rbPourcentagePort, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPourcentagePort ========
          {
            pnlPourcentagePort.setOpaque(false);
            pnlPourcentagePort.setName("pnlPourcentagePort");
            pnlPourcentagePort.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPourcentagePort ----
            tfPourcentagePort.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPourcentagePort.setName("tfPourcentagePort");
            tfPourcentagePort.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortPourcentageFocusLost(e);
              }
            });
            pnlPourcentagePort.add(tfPourcentagePort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage5 ----
            lbPourcentage5.setText("% ");
            lbPourcentage5.setHorizontalAlignment(SwingConstants.LEADING);
            lbPourcentage5.setMinimumSize(new Dimension(50, 30));
            lbPourcentage5.setPreferredSize(new Dimension(50, 30));
            lbPourcentage5.setName("lbPourcentage5");
            pnlPourcentagePort.add(lbPourcentage5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlPourcentagePort, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixRevientFournisseurHT ----
          lbPrixRevientFournisseurHT.setText("Prix de revient fournisseur HT");
          lbPrixRevientFournisseurHT.setMaximumSize(new Dimension(200, 30));
          lbPrixRevientFournisseurHT.setMinimumSize(new Dimension(200, 30));
          lbPrixRevientFournisseurHT.setPreferredSize(new Dimension(225, 30));
          lbPrixRevientFournisseurHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
          lbPrixRevientFournisseurHT.setName("lbPrixRevientFournisseurHT");
          pnlNegociationAchat.add(lbPrixRevientFournisseurHT, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixRevientFournisseurHT ========
          {
            pnlPrixRevientFournisseurHT.setOpaque(false);
            pnlPrixRevientFournisseurHT.setName("pnlPrixRevientFournisseurHT");
            pnlPrixRevientFournisseurHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixRevientFournisseurHT ----
            tfPrixRevientFournisseurHT.setEditable(false);
            tfPrixRevientFournisseurHT.setEnabled(false);
            tfPrixRevientFournisseurHT.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixRevientFournisseurHT.setName("tfPrixRevientFournisseurHT");
            pnlPrixRevientFournisseurHT.add(tfPrixRevientFournisseurHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixRevientFournisseurHT ----
            lbUnitePrixRevientFournisseurHT.setText("UA");
            lbUnitePrixRevientFournisseurHT.setHorizontalAlignment(SwingConstants.LEADING);
            lbUnitePrixRevientFournisseurHT.setMinimumSize(new Dimension(50, 30));
            lbUnitePrixRevientFournisseurHT.setPreferredSize(new Dimension(50, 30));
            lbUnitePrixRevientFournisseurHT.setName("lbUnitePrixRevientFournisseurHT");
            pnlPrixRevientFournisseurHT.add(lbUnitePrixRevientFournisseurHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlPrixRevientFournisseurHT, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFraisExploitation ----
          lbFraisExploitation.setText("Frais d'exploitation");
          lbFraisExploitation.setMaximumSize(new Dimension(200, 30));
          lbFraisExploitation.setMinimumSize(new Dimension(200, 30));
          lbFraisExploitation.setPreferredSize(new Dimension(200, 30));
          lbFraisExploitation.setName("lbFraisExploitation");
          pnlNegociationAchat.add(lbFraisExploitation, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlFraisExploitation ========
          {
            pnlFraisExploitation.setOpaque(false);
            pnlFraisExploitation.setName("pnlFraisExploitation");
            pnlFraisExploitation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfFraisExploitation ----
            tfFraisExploitation.setHorizontalAlignment(SwingConstants.TRAILING);
            tfFraisExploitation.setName("tfFraisExploitation");
            tfFraisExploitation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFraisExploitationFocusLost(e);
              }
            });
            pnlFraisExploitation.add(tfFraisExploitation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentageFraisExploitation ----
            lbPourcentageFraisExploitation.setText("% ");
            lbPourcentageFraisExploitation.setHorizontalAlignment(SwingConstants.LEADING);
            lbPourcentageFraisExploitation.setMinimumSize(new Dimension(50, 30));
            lbPourcentageFraisExploitation.setPreferredSize(new Dimension(50, 30));
            lbPourcentageFraisExploitation.setName("lbPourcentageFraisExploitation");
            pnlFraisExploitation.add(lbPourcentageFraisExploitation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlFraisExploitation, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixRevientStandardHT ----
          lbPrixRevientStandardHT.setText("Prix de revient standard HT");
          lbPrixRevientStandardHT.setMaximumSize(new Dimension(200, 30));
          lbPrixRevientStandardHT.setMinimumSize(new Dimension(200, 30));
          lbPrixRevientStandardHT.setPreferredSize(new Dimension(200, 30));
          lbPrixRevientStandardHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
          lbPrixRevientStandardHT.setName("lbPrixRevientStandardHT");
          pnlNegociationAchat.add(lbPrixRevientStandardHT, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlPrixRevientStandardHT ========
          {
            pnlPrixRevientStandardHT.setOpaque(false);
            pnlPrixRevientStandardHT.setName("pnlPrixRevientStandardHT");
            pnlPrixRevientStandardHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixRevientStandardHT ----
            tfPrixRevientStandardHT.setEditable(false);
            tfPrixRevientStandardHT.setEnabled(false);
            tfPrixRevientStandardHT.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixRevientStandardHT.setName("tfPrixRevientStandardHT");
            pnlPrixRevientStandardHT.add(tfPrixRevientStandardHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixRevientStandardHT ----
            lbUnitePrixRevientStandardHT.setText("UA");
            lbUnitePrixRevientStandardHT.setHorizontalAlignment(SwingConstants.LEADING);
            lbUnitePrixRevientStandardHT.setMinimumSize(new Dimension(50, 30));
            lbUnitePrixRevientStandardHT.setPreferredSize(new Dimension(50, 30));
            lbUnitePrixRevientStandardHT.setName("lbUnitePrixRevientStandardHT");
            pnlPrixRevientStandardHT.add(lbUnitePrixRevientStandardHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlNegociationAchat.add(pnlPrixRevientStandardHT, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDateApplicationTarifAchat ----
          lbDateApplicationTarifAchat.setText("Date d'application du tarif d'achat");
          lbDateApplicationTarifAchat.setPreferredSize(new Dimension(250, 30));
          lbDateApplicationTarifAchat.setName("lbDateApplicationTarifAchat");
          pnlNegociationAchat.add(lbDateApplicationTarifAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snDateApplicationTarifAchat ----
          snDateApplicationTarifAchat.setComponentPopupMenu(null);
          snDateApplicationTarifAchat.setPreferredSize(new Dimension(115, 30));
          snDateApplicationTarifAchat.setMinimumSize(new Dimension(115, 30));
          snDateApplicationTarifAchat.setMaximumSize(new Dimension(105, 30));
          snDateApplicationTarifAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          snDateApplicationTarifAchat.setEnabled(false);
          snDateApplicationTarifAchat.setFocusable(false);
          snDateApplicationTarifAchat.setName("snDateApplicationTarifAchat");
          pnlNegociationAchat.add(snDateApplicationTarifAchat, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlAchat.add(pnlNegociationAchat, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlAchat, "Achat");
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- grpPort ----
    ButtonGroup grpPort = new ButtonGroup();
    grpPort.add(rbMontantPort);
    grpPort.add(rbPourcentagePort);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlContenu;
  private SNPanelContenu pnlVente;
  private SNPanel pnlEnteteVente;
  private SNLabelChamp lbQuantiteUC;
  private SNPanel pnlVenteQuantiteUCV;
  private SNTexte tfQuantiteCommandeUC;
  private SNTexte tfUniteC;
  private SNLabelChamp lbUCAparUCS;
  private SNLabelChamp lbQuantiteUV;
  private SNPanel pnlVenteRatioUVParUCV;
  private SNTexte tfQuantiteCommandeeUV;
  private SNTexte tfUniteV;
  private SNLabelChamp lbUVparUC;
  private SNPanel pnlTarif;
  private SNPanelTitre pnlTarifInitial;
  private SNPanel pnlInformationPrixInitial;
  private SNPanel pnlDateApplicationtarif;
  private SNLabelChamp lbDateApplication;
  private SNDate snDateApplication;
  private SNLabelChamp lbPrixBaseInitial;
  private SNLabelChamp lbColonneTarifInitiale;
  private SNLabelChamp lbOrigineInitiale;
  private SNLabelChamp lbRemiseInitiale;
  private SNLabelChamp lbPrixNetInitial;
  private SNTexte zsPrixBaseInitial;
  private SNTexte tfColonneTarifInitiale;
  private SNTexte zsOrigineInitiale;
  private SNTexte zsRemiseInitiale;
  private SNLabelChamp lbSymbolePourcentageRemiseInitiale;
  private SNTexte zsPrixNetInitial;
  private SNPanelTitre pnlTarifInitialMarge;
  private SNLabelChamp lbIndiceDeMargeInitiale;
  private SNTexte zsIndiceDeMargeInitiale;
  private SNLabelChamp lbMargeInitiale;
  private SNLabelChamp lbPrixRevientInitial;
  private SNTexte zsMargeInitiale;
  private SNLabelChamp lbSymbolePourcentageMargeInitiale;
  private SNTexte zsPrixRevientInitial;
  private SNPanelTitre pnlTarifNegocie;
  private JPanel pnlPrixNegocie;
  private SNPanel pnlNegocePrixNegocie;
  private SNLabelChamp lbNegocePrixPublic;
  private SNLabelChamp lbNegoceOrigine;
  private SNLabelChamp lbNegoceRemise;
  private SNLabelChamp lbNegocePrixNet;
  private SNTexte tfNegocePrixPublic;
  private SNComboBox cbNegoceColonneTarif;
  private SNTexte tfNegoceRemise;
  private SNLabelChamp lbSymbolePourcentageNegoceRemise;
  private SNTexte tfNegocePrixNet;
  private SNPanel pnlClassiquePrixNegocie;
  private SNLabelChamp lbClassiquePrixBase;
  private SNLabelChamp lbClassiqueColonneTarif;
  private SNLabelChamp lbClassiqueOrigine;
  private SNLabelChamp lbClassiqueRemise;
  private SNLabelChamp lbClassiquePrixNet;
  private SNTexte tfClassiquePrixBaseSaisi;
  private SNComboBox cbClassiqueColonneTarifSaisie;
  private SNTexte tfClassiqueRemiseSaisie;
  private SNLabelChamp lbSymbolePourcentageClassiqueRemiseSaisie;
  private SNTexte tfClassiquePrixNetSaisi;
  private SNTexte tfClassiquePrixBase;
  private SNTexte tfClassiqueColonneTarif;
  private SNTexte tfClassiqueOrigine;
  private SNTexte tfClassiqueRemise;
  private SNLabelChamp lbSymbolePourcentageClassiqueRemise;
  private SNTexte tfClassiquePrixNet;
  private SNPanel pnlGratuit;
  private JCheckBox chkGratuit;
  private SNLabelChamp lbTypeGratuit;
  private SNTypeGratuit snTypeGratuit;
  private SNPanelTitre pnlTarifNegocieMarge;
  private SNLabelChamp lbIndiceDeMarge;
  private SNTexte tfIndiceDeMarge;
  private SNLabelChamp lbMarge;
  private SNLabelChamp lbPrixRevient;
  private SNPanel pnlEspace;
  private SNTexte tfMarge;
  private SNLabelChamp lbSymbolePourcentageMarge;
  private SNTexte tfPrixRevient;
  private SNMessage lbMessageConditionChantier;
  private SNPanelContenu pnlAchat;
  private SNPanel pnlEnteteAchat;
  private SNLabelChamp lbAchatQuantiteUCV;
  private SNPanel pnlQuantiteUCV;
  private SNTexte tfAchatQuantiteUCV;
  private SNTexte tfAchatUniteQuantiteUCV;
  private SNLabelChamp lbAchatRatioUVparUCV;
  private SNPanel pnlUVParUCV;
  private SNTexte tfAchatRatioUVParUCV;
  private SNTexte tfAchatUniteRatioUVparUCV;
  private SNPanelTitre pnlNegociationAchat;
  private SNLabelChamp lbRemisesFournisseur;
  private SNLabelChamp lbPrixAchatBrutHT;
  private SNPanel pnlPrixAchatBrutHT;
  private SNTexte tfPrixAchatBrutHT;
  private SNLabelChamp lbUnitePrixAchatBrutHT;
  private SNPanel pnlRemisesFournisseur;
  private SNTexte tfRemiseFournisseur1;
  private SNLabelChamp lbUniteRemiseFournisseur1;
  private SNTexte tfRemiseFournisseur2;
  private SNLabelChamp lbUniteRemiseFournisseur2;
  private SNTexte tfRemiseFournisseur3;
  private SNLabelChamp lbUniteRemiseFournisseur3;
  private SNTexte tfRemiseFournisseur4;
  private SNLabelChamp lbUniteRemiseFournisseur4;
  private SNLabelChamp lbTaxeMajoration;
  private SNPanel pnlTaxeMajoration;
  private SNTexte tfTaxeMajoration;
  private SNLabelChamp lbPourcentageTaxeMajoration;
  private SNLabelChamp lbMontantConditionnement;
  private SNTexte tfMontantConditionnement;
  private SNLabelChamp lbPrixAchatNetHT;
  private SNPanel pnlPrixAchatNetHT;
  private SNTexte tfPrixAchatNetHT;
  private SNLabelChamp lbUnitePrixAchatNetHT;
  private JRadioButton rbMontantPort;
  private SNPanel pnlMontantPort;
  private SNTexte tfValeurPort;
  private SNLabelChamp lbPortPoids;
  private SNTexte tfPoidsPort;
  private JRadioButton rbPourcentagePort;
  private SNPanel pnlPourcentagePort;
  private SNTexte tfPourcentagePort;
  private SNLabelChamp lbPourcentage5;
  private SNLabelChamp lbPrixRevientFournisseurHT;
  private SNPanel pnlPrixRevientFournisseurHT;
  private SNTexte tfPrixRevientFournisseurHT;
  private SNLabelChamp lbUnitePrixRevientFournisseurHT;
  private SNLabelChamp lbFraisExploitation;
  private SNPanel pnlFraisExploitation;
  private SNTexte tfFraisExploitation;
  private SNLabelChamp lbPourcentageFraisExploitation;
  private SNLabelChamp lbPrixRevientStandardHT;
  private SNPanel pnlPrixRevientStandardHT;
  private SNTexte tfPrixRevientStandardHT;
  private SNLabelChamp lbUnitePrixRevientStandardHT;
  private SNLabelChamp lbDateApplicationTarifAchat;
  private SNDate snDateApplicationTarifAchat;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
