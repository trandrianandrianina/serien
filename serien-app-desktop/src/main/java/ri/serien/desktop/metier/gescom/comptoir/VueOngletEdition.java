/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;

import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.commun.document.EnumModeEdition;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEdition;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEditionDevis;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEditionFacture;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

public class VueOngletEdition extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final String LIB_ENVOI_MAIL = "Envoi par mail à ";
  private static final String LIB_ENVOI_FAX = "Envoi par fax à ";
  private static final String LIB_ENVOI_SMS = "Envoi par sms à ";
  // Liste
  private static final String[] TITRE_LISTE_COMMANDES_ACHAT = new String[] { "Identifiant", "Date", "Fournisseur", "Traitement" };
  // Boutons
  private static final String BOUTON_GENERER_CNV = "Générer CNV";
  private static final String BOUTON_AFFICHER_DOCUMENT = "Afficher les documents";
  private static final String BOUTON_VISUALISER_ACHAT = "Visualiser achats";
  
  // Variables
  private ModeleComptoir modele = null;
  protected boolean executerEvenements = false;
  
  /**
   * Constructeur.
   */
  public VueOngletEdition(ModeleComptoir acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    pnlEditionDevis.setVisible(false);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_GENERER_CNV, 'g', false);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENT, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_VISUALISER_ACHAT, 'v', false);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snAdresseFournisseur.setSession(modele.getSession());
    snAdresseFournisseur.initialiserRecherche();
    snAdresseFournisseur.setContexteSaisieDocumentAchat(false);
    snAdresseFournisseur.addSNComposantListener(new InterfaceSNComposantListener() {
      @Override
      public void valueChanged(SNComposantEvent pSNComposantEvent) {
        if (executerEvenements) {
          modele.modifierAdresseFournisseur(snAdresseFournisseur.getSelection());
        }
      }
    });
    
    // Initialise l'aspect de la table des commandes d'achat
    scpCommandeAchat.getViewport().setBackground(Color.WHITE);
    tblCommandeAchat.personnaliserAspect(TITRE_LISTE_COMMANDES_ACHAT, new int[] { 80, 90, 500, 200 }, new int[] { 150, 120, 5000, 300 },
        new int[] { NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    
    pnlChiffrage.setVisible(false);
    pnlEditionDevis.setVisible(false);
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    executerEvenements = false;
    // Rafraîchir le panneau chiffrage ou devis
    rafraichirPanelChiffrage();
    rafraichirPanelEditionDevis();
    
    // Panel type d'édition pour factures et avoirs
    rafraichirChoixImpressionFacture();
    rafraichirRadioBoutonImpressionFacture();
    
    // Rafraîchir le choix d'édition
    rafraichirModeEditionVisualiser();
    rafraichirModeEditionImprimer();
    rafraichirModeEditionEnvoyerParMail();
    rafraichirModeEditionEnvoyerParFax();
    
    // Rafraîchir les options compémentaires
    rafraichirOptionComplementaireAchat();
    rafraichirOptionComplementaireEditionDocumentSupplementaire();
    
    // Options facturation
    rafraichirPanneauOptionsFacturation();
    
    // Recherche contact
    rafraichirRechercheContactMail();
    rafraichirRechercheContactFax();
    rafraichirBoutonGenererCNV();
    rafraichirBoutonEditer();
    rafraichirBoutonAfficherDocument();
    
    // Liste achats lié
    rafraichirPanelAchatLie();
    rafraichirTableAchatLie();
    rafraichirBoutonVisualiserAchat();
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  /**
   * Rafraichir le panel de choix du chiffrage (hors factures et devis).
   */
  private void rafraichirPanelChiffrage() {
    if (modele.getOptionsEditionVente() != null) {
      switch (modele.getOptionsEditionVente().getEditionChiffree()) {
        case EDITION_CHIFFREE:
          rbEditionAvecTotaux.setSelected(true);
          break;
        case EDITION_SANS_TOTAUX:
          rbEditionSansTotaux.setSelected(true);
          break;
        case EDITION_NON_CHIFFREE:
          rbEditionNonChiffre.setSelected(true);
          break;
        default:
          break;
      }
      
    }
    // Affichage du panel des options de chiffrage
    if (modele.isAfficherOptionChiffrage()) {
      pnlChiffrage.setVisible(true);
    }
    else {
      pnlChiffrage.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le panel du d'édition des devis.
   */
  private void rafraichirPanelEditionDevis() {
    if (modele.getOptionsEditionVente() != null) {
      rbDevisAvecTotaux.setSelected(modele.getOptionsEditionVente().getTypeEditionDevis().equals(EnumTypeEditionDevis.DEVIS_AVEC_TOTAUX));
      rbDevisSansTotaux.setSelected(modele.getOptionsEditionVente().getTypeEditionDevis().equals(EnumTypeEditionDevis.DEVIS_SANS_TOTAUX));
      rbFactureProforma.setSelected(modele.getOptionsEditionVente().getTypeEditionDevis().equals(EnumTypeEditionDevis.FACTURE_PROFORMA));
    }
    pnlEditionDevis.setVisible(modele.getDocumentVenteEnCours().isDevis());
  }
  
  /**
   * Rafraichir la recherche contact pour l'envoi par mail.
   */
  private void rafraichirRechercheContactMail() {
    snContactMail.setSession(modele.getSession());
    if (modele.getEtablissement() != null) {
      snContactMail.setIdEtablissement(modele.getEtablissement().getId());
    }
    else {
      snContactMail.setIdEtablissement(null);
    }
    snContactMail.setIdClient(modele.getClientCourant().getId());
    snContactMail.charger(false, false);
    snContactMail.setSelection(modele.getContactMail());
  }
  
  /**
   * Rafraichir la recherche contact pour l'envoi par fax.
   */
  private void rafraichirRechercheContactFax() {
    snContactFax.setSession(modele.getSession());
    if (modele.getEtablissement() != null) {
      snContactFax.setIdEtablissement(modele.getEtablissement().getId());
    }
    else {
      snContactFax.setIdEtablissement(null);
    }
    snContactFax.setIdClient(modele.getClientCourant().getId());
    snContactFax.charger(false, false);
    snContactFax.setSelection(modele.getContactFax());
  }
  
  /**
   * Rafraichit le bouton générer CNV.
   */
  private void rafraichirBoutonGenererCNV() {
    boolean actif = true;
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente == null || !documentVente.isDevis()) {
      actif = false;
    }
    if (!ManagerSessionClient.getInstance().verifierDroitGescom(modele.getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_GERER_CONDITION_VENTE)) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_GENERER_CNV, actif);
    
  }
  
  /**
   * Rafraichit le bouton éditer.
   */
  private void rafraichirBoutonEditer() {
    boolean actif = true;
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente == null || (documentVente.isDirectUsineMonoFournisseur() && documentVente.getIdAdresseFournisseur() == null
        && modele.isDirectUsineEnModeMultiFournisseur() && !rbMultiFournisseur.isSelected())) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(EnumBouton.EDITER, actif);
  }
  
  /**
   * Rafraichit le bouton AfficherDocument
   */
  private void rafraichirBoutonAfficherDocument() {
    boolean actif = true;
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente == null || documentVente.isEnCoursCreation() || !documentVente.isFacture()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT, actif);
  }
  
  /**
   * Rafraichit le bouton Visualiser achats
   */
  private void rafraichirBoutonVisualiserAchat() {
    boolean actif = true;
    ListeDocumentAchat listeAchatLie = modele.getListeDocumentAchatLie();
    if (listeAchatLie == null) {
      actif = false;
    }
    else if (tblCommandeAchat.getSelectionModel().isSelectionEmpty()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_VISUALISER_ACHAT, actif);
  }
  
  /**
   * Rafraichir le panel d'affichage des commandes d'achat liées
   */
  private void rafraichirPanelAchatLie() {
    ListeDocumentAchat listeDocumentAchatLie = modele.getListeDocumentAchatLie();
    if (listeDocumentAchatLie == null) {
      pnlCommandesAchat.setVisible(false);
    }
    else {
      pnlCommandesAchat.setVisible(true);
      lbCommandeAchat.setText(modele.mettreEnFormeTitreDocumentAchatLie());
    }
  }
  
  /**
   * Rafraichir la table des commandes d'achats liées au document en cours
   */
  private void rafraichirTableAchatLie() {
    ListeDocumentAchat listeDocumentAchatLie = modele.getListeDocumentAchatLie();
    if (listeDocumentAchatLie == null || listeDocumentAchatLie.size() < 1) {
      return;
    }
    
    // Renseigner les données du tableau
    String[][] donnees = new String[listeDocumentAchatLie.size()][TITRE_LISTE_COMMANDES_ACHAT.length];
    for (int ligne = 0; ligne < listeDocumentAchatLie.size(); ligne++) {
      DocumentAchat documentAchat = listeDocumentAchatLie.get(ligne);
      
      // Identifiant
      donnees[ligne][0] = documentAchat.getId().toString();
      
      // Date de création
      if (documentAchat.getDateCreation() != null) {
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentAchat.getDateCreation());
      }
      
      // Fournisseur (nom et ID)
      if (documentAchat.getIdFournisseur() != null) {
        donnees[ligne][2] =
            documentAchat.getAdresseFournisseur().getNom().trim() + " (" + documentAchat.getIdFournisseur().getTexte() + ")";
      }
      
      // Traitement
      if (documentAchat.getCodeEtat() != null) {
        donnees[ligne][3] = documentAchat.getCodeEtat().getLibelle();
      }
      
    }
    tblCommandeAchat.mettreAJourDonnees(donnees);
    
    // Recalcule la longueur de la liste afin d'avoir l'ascenceur uniquement quand c'est nécessaire
    tblCommandeAchat.setPreferredSize(
        new Dimension(tblCommandeAchat.getPreferredSize().width, listeDocumentAchatLie.size() * tblCommandeAchat.getRowHeight()));
    
    // Sélectionne les lignes
    List<Integer> listeIndexSelectionne = listeDocumentAchatLie.getListeIndexSelection();
    for (Integer index : listeIndexSelectionne) {
      tblCommandeAchat.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Visualiser.
   */
  private void rafraichirModeEditionVisualiser() {
    ListeModeEdition listeModeEditionVente = modele.getListeModeEditionVente();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.VISUALISER);
    if (modeEdition == null || !modeEdition.isActif()) {
      ckVisualiser.setVisible(false);
    }
    else {
      ckVisualiser.setText(modeEdition.getTexte());
      ckVisualiser.setSelected(modeEdition.isSelectionne());
      ckVisualiser.setVisible(true);
    }
  }
  
  /**
   * Rafraichit les options complémentaires concernant les commandes d'achats.
   */
  private void rafraichirOptionComplementaireAchat() {
    if (modele.isDirectUsineEnModeMultiFournisseur() || modele.isGBAarticleSpeciaux()) {
      pnlOptionsComplementairesAchat.setVisible(true);
      rafraichirBoutonRadioFournisseurs();
      rafraichirAdresseFournisseur();
      rafraichirGenerationCommandeAchat();
    }
    else {
      pnlOptionsComplementairesAchat.setVisible(false);
    }
  }
  
  /**
   * Rafraichit les options complémentaires concernant les éditions des documents supplémentaires.
   */
  private void rafraichirOptionComplementaireEditionDocumentSupplementaire() {
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && documentVente.isCommande()) {
      OptionsEditionVente optionEditionVente = modele.getOptionsEditionVente();
      if (optionEditionVente != null) {
        ckBonDePreparation.setSelected(optionEditionVente.isEditionSupplementaireBonDePreparation());
      }
      pnlEditionDocumentSupplementaire.setVisible(true);
    }
    else {
      pnlEditionDocumentSupplementaire.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le panel pour options de facturation.
   */
  private void rafraichirPanneauOptionsFacturation() {
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isBon()
        || modele.getDocumentVenteEnCours().getTotalTTC().compareTo(BigDecimal.ZERO) < 1) {
      pnlOptionFacturation.setVisible(false);
      return;
    }
    Date dateMinimaleFacturation = modele.getDocumentVenteEnCours().getDateMiniFacturation();
    snDateFacturationMinimale.setDate(dateMinimaleFacturation);
    pnlOptionFacturation.setVisible(true);
  }
  
  /**
   * Rafraichit les boutons radios mono ou multi fournisseurs.
   */
  private void rafraichirBoutonRadioFournisseurs() {
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (modele.isGBAarticleSpeciaux()) {
      rbMonoFournisseur.setVisible(false);
      rbMultiFournisseur.setVisible(false);
    }
    else {
      rbMonoFournisseur.setVisible(true);
      rbMultiFournisseur.setVisible(true);
      rbMonoFournisseur.setEnabled(documentVente.isEnCoursCreation());
      rbMultiFournisseur.setEnabled(documentVente.isEnCoursCreation());
      rbMonoFournisseur.setSelected(documentVente.isMonoFournisseur() && modele.getInformationsExtraction() == null);
      rbMultiFournisseur.setSelected(!documentVente.isMonoFournisseur());
    }
  }
  
  /**
   * rafraichir l'adresse du fournisseur.
   */
  private void rafraichirAdresseFournisseur() {
    if (modele.isGBAarticleSpeciaux()) {
      pnlAdresseFournisseur.setVisible(false);
    }
    else {
      pnlAdresseFournisseur.setVisible(true);
      snAdresseFournisseur.setEnabled(rbMonoFournisseur.isSelected() && modele.getDocumentVenteEnCours().isEnCoursCreation());
      
      if (modele.getEtablissement() != null) {
        snAdresseFournisseur.setIdEtablissement(modele.getEtablissement().getId());
      }
      
      DocumentVente documentVente = modele.getDocumentVenteEnCours();
      if (documentVente != null && documentVente.getIdAdresseFournisseur() != null) {
        snAdresseFournisseur.setSelectionParId(documentVente.getIdAdresseFournisseur());
      }
      else {
        snAdresseFournisseur.setSelection(null);
      }
    }
  }
  
  /**
   * Rafraichit la checkbox génération de commande d'achat
   */
  private void rafraichirGenerationCommandeAchat() {
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && modele.isAfficherOptionGenerationCommandeAchat()) {
      ckGenerationCommandeAchat.setSelected(documentVente.isGenerationImmediateCommandeAchat());
      ckGenerationCommandeAchat.setEnabled(modele.getDocumentVenteEnCours().isEnCoursCreation());
    }
    else {
      ckGenerationCommandeAchat.setSelected(false);
      ckGenerationCommandeAchat.setEnabled(false);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Imprimer.
   */
  private void rafraichirModeEditionImprimer() {
    ListeModeEdition listeModeEditionVente = modele.getListeModeEditionVente();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.IMPRIMER);
    if (modeEdition == null || !modeEdition.isActif()) {
      ckImprimer.setVisible(false);
    }
    else {
      ckImprimer.setText(modeEdition.getTexte());
      ckImprimer.setSelected(modeEdition.isSelectionne());
      ckImprimer.setVisible(true);
    }
  }
  
  /**
   * Rafraichit le choix d'édition de facture ou ticket de caisse, si on a coché "imprimer et si le document est une facture ou un avoir.
   */
  private void rafraichirChoixImpressionFacture() {
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente != null && (documentVente.isFacture() || documentVente.isAvoir())) {
      pnlChoixImpressionFacture.setVisible(true);
    }
    else {
      pnlChoixImpressionFacture.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir l'affichage des radio bouton permettant la sélection d'édition de ticket ou de facture.
   */
  private void rafraichirRadioBoutonImpressionFacture() {
    if (!pnlChoixImpressionFacture.isVisible()) {
      return;
    }
    EnumTypeEditionFacture typeEditionFacture = modele.getOptionsEditionFacture();
    switch (typeEditionFacture) {
      case EDITION_FACTURE:
        rbImprimerFacture.setSelected(true);
        rbImprimerTicket.setSelected(false);
        break;
      case EDITION_TICKET:
        rbImprimerFacture.setSelected(false);
        rbImprimerTicket.setSelected(true);
        break;
      default:
        rbImprimerFacture.setSelected(false);
        rbImprimerTicket.setSelected(false);
        break;
    }
  }
  
  /**
   * Rafraichit le mode d'édition Envoyer par mail.
   */
  private void rafraichirModeEditionEnvoyerParMail() {
    ListeModeEdition listeModeEditionVente = modele.getListeModeEditionVente();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_MAIL);
    lbMail.setForeground(Color.BLACK);
    tfEmailDestinataire.setText(modele.getEmailDestinataireEdition());
    if (modeEdition == null || !modeEdition.isActif() || !modeEdition.isSelectionne()) {
      ckEnvoyerParMail.setSelected(false);
      snContactMail.setEnabled(false);
      tfEmailDestinataire.setEnabled(false);
    }
    else {
      ckEnvoyerParMail.setSelected(true);
      ckEnvoyerParMail.setText(modeEdition.getTexte() + " à");
      if (modele.getEmailDestinataireEdition().isEmpty()) {
        lbMail.setForeground(Color.RED);
      }
      snContactMail.setEnabled(true);
      tfEmailDestinataire.setEnabled(true);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Envoyer par fax.
   */
  private void rafraichirModeEditionEnvoyerParFax() {
    ListeModeEdition listeModeEditionVente = modele.getListeModeEditionVente();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_FAX);
    lbFax.setForeground(Color.BLACK);
    tfFaxDestinataire.setText(modele.getFaxDestinataireEdition());
    if (modeEdition == null || !modeEdition.isActif() || !modeEdition.isSelectionne()) {
      ckEnvoyerParFax.setSelected(false);
      snContactFax.setEnabled(false);
      tfFaxDestinataire.setEnabled(false);
    }
    else {
      ckEnvoyerParFax.setSelected(true);
      ckEnvoyerParFax.setText(modeEdition.getTexte() + " au ");
      if (modele.getFaxDestinataireEdition().isEmpty()) {
        lbFax.setForeground(Color.RED);
      }
      snContactFax.setEnabled(true);
      tfFaxDestinataire.setEnabled(true);
    }
  }
  
  // -- Méthodes évènementielles --------------------------------------------
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        modele.validerOngletEdition();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.annulerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_GENERER_CNV)) {
        modele.genererCNV();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENT)) {
        modele.afficherDocumentStocke();
      }
      else if (pSNBouton.isBouton(BOUTON_VISUALISER_ACHAT)) {
        modele.afficherDocumentAchatLie();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Bon de préparation".
   * 
   * @param e
   */
  private void ckBonDePreparationActionPerformed(ActionEvent e) {
    try {
      modele.activerEditionBonDePreparation(ckBonDePreparation.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Edition sans pied".
   * 
   * @param e
   */
  private void rbEditionSansPiedItemStateChanged(ItemEvent e) {
    if (!executerEvenements) {
      return;
    }
    try {
      modele.choisirTypeEdition(EnumTypeEdition.EDITION_SANS_TOTAUX);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Edition chiffrée".
   * 
   * @param e
   */
  private void rbEditionChiffreeItemStateChanged(ItemEvent e) {
    if (!executerEvenements) {
      return;
    }
    try {
      modele.choisirTypeEdition(EnumTypeEdition.EDITION_CHIFFREE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Edition non chiffrée".
   * 
   * @param e
   */
  private void rbEditionNonChiffreItemStateChanged(ItemEvent e) {
    if (!executerEvenements) {
      return;
    }
    try {
      modele.choisirTypeEdition(EnumTypeEdition.EDITION_NON_CHIFFREE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Edition Proforma".
   * 
   * @param e
   */
  private void rbEditionProformaItemStateChanged(ItemEvent e) {
    try {
      modele.choisirTypeEditionDevis(EnumTypeEditionDevis.FACTURE_PROFORMA);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Génération de commande d'achat".
   * 
   * @param e
   */
  private void ckGenerationCommandeAchatActionPerformed(ActionEvent e) {
    try {
      modele.modifierGenerationCommandeAchat(ckGenerationCommandeAchat.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Imprimer".
   * 
   * @param e
   */
  private void ckImprimerActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeEdition(EnumModeEdition.IMPRIMER, ckImprimer.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Envoyer par mail".
   * 
   * @param e
   */
  private void ckEnvoyerParMailActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeEdition(EnumModeEdition.ENVOYER_PAR_MAIL, ckEnvoyerParMail.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Visualiser".
   * 
   * @param e
   */
  private void ckVisualiserActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeEdition(EnumModeEdition.VISUALISER, ckVisualiser.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Envoyer par fax".
   * 
   * @param e
   */
  private void ckEnvoyerParFaxActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeEdition(EnumModeEdition.ENVOYER_PAR_FAX, ckEnvoyerParFax.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de l'adresse email du destinataire.
   * 
   * @param e
   */
  private void tfEmailDestinataireFocusLost(FocusEvent e) {
    try {
      modele.modifierEmailDestinataire(tfEmailDestinataire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Multi fournisseur".
   * 
   * @param e
   */
  private void rbMultiFournisseurActionPerformed(ActionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierMonoFournisseur(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Mono fournisseur".
   * 
   * @param e
   */
  private void rbMonoFournisseurActionPerformed(ActionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierMonoFournisseur(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du contact pour envoi par mail.
   * 
   * @param e
   */
  private void snContactMailValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierEmailDestinataire(snContactMail.getSelection(), true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du contact pour envoi par fax.
   * 
   * @param e
   */
  private void snContactFaxValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierFaxDestinataire(snContactFax.getSelection(), true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du fax.
   * 
   * @param e
   */
  private void tfFaxDestinataireFocusLost(FocusEvent e) {
    try {
      modele.modifierFaxDestinataire(tfFaxDestinataire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option ""facture Proforma.
   * 
   * @param e
   */
  private void rbFactureProformaItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.choisirTypeEditionDevis(EnumTypeEditionDevis.FACTURE_PROFORMA);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Devis sans totaux".
   * 
   * @param e
   */
  private void rbDevisSansTotauxItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.choisirTypeEditionDevis(EnumTypeEditionDevis.DEVIS_SANS_TOTAUX);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Devis avec totaux".
   * 
   * @param e
   */
  private void rbDevisAvecTotauxItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.choisirTypeEditionDevis(EnumTypeEditionDevis.DEVIS_AVEC_TOTAUX);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la date de facturation minimale.
   * 
   * @param e
   */
  private void snDateFacturationMinimaleValueChanged(SNComposantEvent e) {
    try {
      modele.modifierDateFacturationMinimale(snDateFacturationMinimale.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * 
   * Choisir que l'on édite une facture ou un avoir sur un document facture.
   * 
   * @param e
   */
  private void rbImprimerFactureItemStateChanged(ItemEvent e) {
    if (!executerEvenements) {
      return;
    }
    try {
      if (!rbImprimerFacture.isSelected()) {
        return;
      }
      modele.choisirTypeEditionFacture(EnumTypeEditionFacture.EDITION_FACTURE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * 
   * Choisir que l'on édite une facture ou un avoir sur un ticket de caisse.
   * 
   * @param e
   */
  private void rbImprimerTicketItemStateChanged(ItemEvent e) {
    if (!executerEvenements) {
      return;
    }
    try {
      if (!rbImprimerTicket.isSelected()) {
        return;
      }
      modele.choisirTypeEditionFacture(EnumTypeEditionFacture.EDITION_TICKET);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Traitement des actions à la souris sur la liste des documents d'achats liés.
   * 
   * @param e MouseEvent
   */
  private void tblCommandeAchatMouseClicked(MouseEvent e) {
    try {
      int[] listeSelection = tblCommandeAchat.getSelectedRows();
      if (tblCommandeAchat.getRowSorter() != null) {
        for (int i = 0; i < listeSelection.length; i++) {
          listeSelection[i] = tblCommandeAchat.getRowSorter().convertRowIndexToModel(listeSelection[i]);
        }
      }
      modele.selectionnerDocumentAchatLie(listeSelection);
      
      // Double clic
      if (e.getClickCount() == 2) {
        modele.afficherDocumentAchatLie();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlColonnes = new SNPanel();
    pnlGauche = new SNPanel();
    pnlChiffrage = new SNPanelTitre();
    rbEditionAvecTotaux = new JRadioButton();
    rbEditionSansTotaux = new JRadioButton();
    rbEditionNonChiffre = new JRadioButton();
    pnlEditionDevis = new SNPanelTitre();
    rbDevisAvecTotaux = new JRadioButton();
    rbDevisSansTotaux = new JRadioButton();
    rbFactureProforma = new JRadioButton();
    pnlChoixImpressionFacture = new SNPanelTitre();
    rbImprimerFacture = new JRadioButton();
    rbImprimerTicket = new JRadioButton();
    pnlChoixEdition = new SNPanelTitre();
    ckVisualiser = new JCheckBox();
    pnlImprimer = new SNPanel();
    ckImprimer = new JCheckBox();
    pnlEnvoyerParMail = new SNPanel();
    ckEnvoyerParMail = new JCheckBox();
    lbContactMail = new SNLabelChamp();
    snContactMail = new SNContact();
    lbMail = new SNLabelChamp();
    tfEmailDestinataire = new SNTexte();
    pnlEnvoyerParFax = new SNPanel();
    ckEnvoyerParFax = new JCheckBox();
    lbContactFax = new SNLabelChamp();
    snContactFax = new SNContact();
    lbFax = new SNLabelChamp();
    tfFaxDestinataire = new SNTexte();
    pnlDroite = new SNPanel();
    pnlEditionDocumentSupplementaire = new SNPanelTitre();
    ckBonDePreparation = new XRiCheckBox();
    pnlOptionFacturation = new SNPanelTitre();
    lbDateFacturationMinimale = new SNLabelChamp();
    snDateFacturationMinimale = new SNDate();
    pnlOptionsComplementairesAchat = new SNPanelTitre();
    rbMultiFournisseur = new JRadioButton();
    rbMonoFournisseur = new JRadioButton();
    pnlAdresseFournisseur = new JPanel();
    lbAdresseFournisseur = new SNLabelChamp();
    snAdresseFournisseur = new SNAdresseFournisseur();
    ckGenerationCommandeAchat = new JCheckBox();
    pnlCommandesAchat = new SNPanel();
    lbCommandeAchat = new SNLabelTitre();
    scpCommandeAchat = new JScrollPane();
    tblCommandeAchat = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    pnlFax = new JPanel();
    bgTypeEditionDevis = new ButtonGroup();
    groupeFournisseur = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 750));
    setPreferredSize(new Dimension(1240, 750));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlColonnes ========
      {
        pnlColonnes.setName("pnlColonnes");
        pnlColonnes.setLayout(new GridLayout(1, 2));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlChiffrage ========
          {
            pnlChiffrage.setTitre("Chiffrage");
            pnlChiffrage.setName("pnlChiffrage");
            pnlChiffrage.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChiffrage.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlChiffrage.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlChiffrage.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlChiffrage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbEditionAvecTotaux ----
            rbEditionAvecTotaux.setText("Edition avec totaux");
            rbEditionAvecTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionAvecTotaux.setSelected(true);
            rbEditionAvecTotaux.setName("label21");
            rbEditionAvecTotaux.setMinimumSize(new Dimension(250, 30));
            rbEditionAvecTotaux.setPreferredSize(new Dimension(250, 30));
            rbEditionAvecTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionAvecTotaux.addItemListener(e -> rbEditionChiffreeItemStateChanged(e));
            pnlChiffrage.add(rbEditionAvecTotaux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbEditionSansTotaux ----
            rbEditionSansTotaux.setText("Edition sans totaux");
            rbEditionSansTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionSansTotaux.setName("label21");
            rbEditionSansTotaux.setMinimumSize(new Dimension(250, 30));
            rbEditionSansTotaux.setPreferredSize(new Dimension(250, 30));
            rbEditionSansTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionSansTotaux.addItemListener(e -> rbEditionSansPiedItemStateChanged(e));
            pnlChiffrage.add(rbEditionSansTotaux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbEditionNonChiffre ----
            rbEditionNonChiffre.setText("Edition non chiffr\u00e9e");
            rbEditionNonChiffre.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbEditionNonChiffre.setName("label21");
            rbEditionNonChiffre.setMinimumSize(new Dimension(250, 30));
            rbEditionNonChiffre.setPreferredSize(new Dimension(250, 30));
            rbEditionNonChiffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbEditionNonChiffre.addItemListener(e -> rbEditionNonChiffreItemStateChanged(e));
            pnlChiffrage.add(rbEditionNonChiffre, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlChiffrage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlEditionDevis ========
          {
            pnlEditionDevis.setTitre("Type d'\u00e9dition");
            pnlEditionDevis.setName("pnlEditionDevis");
            pnlEditionDevis.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEditionDevis.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEditionDevis.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlEditionDevis.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEditionDevis.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbDevisAvecTotaux ----
            rbDevisAvecTotaux.setText("Devis avec totaux");
            rbDevisAvecTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbDevisAvecTotaux.setSelected(true);
            rbDevisAvecTotaux.setName("label21");
            rbDevisAvecTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbDevisAvecTotaux.setPreferredSize(new Dimension(250, 30));
            rbDevisAvecTotaux.setMinimumSize(new Dimension(150, 30));
            rbDevisAvecTotaux.addItemListener(e -> rbDevisAvecTotauxItemStateChanged(e));
            pnlEditionDevis.add(rbDevisAvecTotaux, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbDevisSansTotaux ----
            rbDevisSansTotaux.setText("Devis sans totaux");
            rbDevisSansTotaux.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbDevisSansTotaux.setName("label21");
            rbDevisSansTotaux.setMinimumSize(new Dimension(150, 30));
            rbDevisSansTotaux.setPreferredSize(new Dimension(250, 30));
            rbDevisSansTotaux.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbDevisSansTotaux.addItemListener(e -> rbDevisSansTotauxItemStateChanged(e));
            pnlEditionDevis.add(rbDevisSansTotaux, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbFactureProforma ----
            rbFactureProforma.setText("Facture proforma");
            rbFactureProforma.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbFactureProforma.setSelected(true);
            rbFactureProforma.setName("label21");
            rbFactureProforma.setMinimumSize(new Dimension(150, 30));
            rbFactureProforma.setPreferredSize(new Dimension(250, 30));
            rbFactureProforma.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbFactureProforma.addItemListener(e -> rbFactureProformaItemStateChanged(e));
            pnlEditionDevis.add(rbFactureProforma, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlEditionDevis, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlChoixImpressionFacture ========
          {
            pnlChoixImpressionFacture.setTitre("Type d'\u00e9dition");
            pnlChoixImpressionFacture.setName("pnlChoixImpressionFacture");
            pnlChoixImpressionFacture.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChoixImpressionFacture.getLayout()).columnWidths = new int[] { 590, 0 };
            ((GridBagLayout) pnlChoixImpressionFacture.getLayout()).rowHeights = new int[] { 30, 0, 0 };
            ((GridBagLayout) pnlChoixImpressionFacture.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlChoixImpressionFacture.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- rbImprimerFacture ----
            rbImprimerFacture.setText("Edition facture");
            rbImprimerFacture.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbImprimerFacture.setMinimumSize(new Dimension(250, 30));
            rbImprimerFacture.setPreferredSize(new Dimension(200, 30));
            rbImprimerFacture.setMaximumSize(new Dimension(250, 30));
            rbImprimerFacture.setName("rbImprimerFacture");
            rbImprimerFacture.addItemListener(e -> rbImprimerFactureItemStateChanged(e));
            pnlChoixImpressionFacture.add(rbImprimerFacture, new GridBagConstraints(0, 0, 1, 1, 0.5, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbImprimerTicket ----
            rbImprimerTicket.setText("Edition ticket de caisse");
            rbImprimerTicket.setFont(new Font("sansserif", Font.PLAIN, 14));
            rbImprimerTicket.setMinimumSize(new Dimension(250, 30));
            rbImprimerTicket.setPreferredSize(new Dimension(200, 30));
            rbImprimerTicket.setMaximumSize(new Dimension(250, 30));
            rbImprimerTicket.setName("rbImprimerTicket");
            rbImprimerTicket.addItemListener(e -> rbImprimerTicketItemStateChanged(e));
            pnlChoixImpressionFacture.add(rbImprimerTicket, new GridBagConstraints(0, 1, 1, 1, 0.5, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlChoixImpressionFacture, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlChoixEdition ========
          {
            pnlChoixEdition.setTitre("Mode d'\u00e9dition");
            pnlChoixEdition.setMinimumSize(new Dimension(508, 325));
            pnlChoixEdition.setPreferredSize(new Dimension(492, 325));
            pnlChoixEdition.setName("pnlChoixEdition");
            pnlChoixEdition.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChoixEdition.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlChoixEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- ckVisualiser ----
            ckVisualiser.setText("Visualiser");
            ckVisualiser.setPreferredSize(new Dimension(200, 30));
            ckVisualiser.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckVisualiser.setFont(ckVisualiser.getFont().deriveFont(ckVisualiser.getFont().getSize() + 2f));
            ckVisualiser.setName("ckVisualiser");
            ckVisualiser.addActionListener(e -> ckVisualiserActionPerformed(e));
            pnlChoixEdition.add(ckVisualiser, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlImprimer ========
            {
              pnlImprimer.setName("pnlImprimer");
              pnlImprimer.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlImprimer.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlImprimer.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlImprimer.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlImprimer.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- ckImprimer ----
              ckImprimer.setText("Imprimer");
              ckImprimer.setPreferredSize(new Dimension(200, 30));
              ckImprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ckImprimer.setFont(ckImprimer.getFont().deriveFont(ckImprimer.getFont().getSize() + 2f));
              ckImprimer.setName("ckImprimer");
              ckImprimer.addActionListener(e -> ckImprimerActionPerformed(e));
              pnlImprimer.add(ckImprimer, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
            }
            pnlChoixEdition.add(pnlImprimer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEnvoyerParMail ========
            {
              pnlEnvoyerParMail.setName("pnlEnvoyerParMail");
              pnlEnvoyerParMail.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- ckEnvoyerParMail ----
              ckEnvoyerParMail.setText("Envoyer par mail \u00e0 ");
              ckEnvoyerParMail.setFont(new Font("sansserif", Font.PLAIN, 14));
              ckEnvoyerParMail.setName("label21");
              ckEnvoyerParMail.setMinimumSize(new Dimension(150, 30));
              ckEnvoyerParMail.setPreferredSize(new Dimension(150, 30));
              ckEnvoyerParMail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ckEnvoyerParMail.setMaximumSize(new Dimension(150, 30));
              ckEnvoyerParMail.addActionListener(e -> ckEnvoyerParMailActionPerformed(e));
              pnlEnvoyerParMail.add(ckEnvoyerParMail, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbContactMail ----
              lbContactMail.setText("Contact");
              lbContactMail.setMaximumSize(new Dimension(75, 30));
              lbContactMail.setMinimumSize(new Dimension(75, 30));
              lbContactMail.setPreferredSize(new Dimension(75, 30));
              lbContactMail.setName("lbContactMail");
              pnlEnvoyerParMail.add(lbContactMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snContactMail ----
              snContactMail.setName("snContactMail");
              snContactMail.addSNComposantListener(e -> snContactMailValueChanged(e));
              pnlEnvoyerParMail.add(snContactMail, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMail ----
              lbMail.setText("Mail");
              lbMail.setMaximumSize(new Dimension(75, 30));
              lbMail.setMinimumSize(new Dimension(75, 30));
              lbMail.setPreferredSize(new Dimension(75, 30));
              lbMail.setName("lbMail");
              pnlEnvoyerParMail.add(lbMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfEmailDestinataire ----
              tfEmailDestinataire.setMinimumSize(new Dimension(400, 30));
              tfEmailDestinataire.setPreferredSize(new Dimension(400, 30));
              tfEmailDestinataire.setName("tfEmailDestinataire");
              tfEmailDestinataire.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tfEmailDestinataireFocusLost(e);
                }
              });
              pnlEnvoyerParMail.add(tfEmailDestinataire, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlChoixEdition.add(pnlEnvoyerParMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlEnvoyerParFax ========
            {
              pnlEnvoyerParFax.setName("pnlEnvoyerParFax");
              pnlEnvoyerParFax.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- ckEnvoyerParFax ----
              ckEnvoyerParFax.setText("Envoyer par fax au");
              ckEnvoyerParFax.setFont(new Font("sansserif", Font.PLAIN, 14));
              ckEnvoyerParFax.setName("label21");
              ckEnvoyerParFax.setMinimumSize(new Dimension(150, 30));
              ckEnvoyerParFax.setPreferredSize(new Dimension(150, 30));
              ckEnvoyerParFax.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ckEnvoyerParFax.setMaximumSize(new Dimension(150, 30));
              ckEnvoyerParFax.addActionListener(e -> ckEnvoyerParFaxActionPerformed(e));
              pnlEnvoyerParFax.add(ckEnvoyerParFax, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbContactFax ----
              lbContactFax.setText("Contact");
              lbContactFax.setMaximumSize(new Dimension(75, 30));
              lbContactFax.setMinimumSize(new Dimension(75, 30));
              lbContactFax.setPreferredSize(new Dimension(75, 30));
              lbContactFax.setName("lbContactFax");
              pnlEnvoyerParFax.add(lbContactFax, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snContactFax ----
              snContactFax.setName("snContactFax");
              snContactFax.addSNComposantListener(e -> snContactFaxValueChanged(e));
              pnlEnvoyerParFax.add(snContactFax, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFax ----
              lbFax.setText("Fax");
              lbFax.setMaximumSize(new Dimension(75, 30));
              lbFax.setMinimumSize(new Dimension(75, 30));
              lbFax.setPreferredSize(new Dimension(75, 30));
              lbFax.setName("lbFax");
              pnlEnvoyerParFax.add(lbFax, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfFaxDestinataire ----
              tfFaxDestinataire.setMinimumSize(new Dimension(400, 30));
              tfFaxDestinataire.setPreferredSize(new Dimension(400, 30));
              tfFaxDestinataire.setName("tfFaxDestinataire");
              tfFaxDestinataire.addFocusListener(new FocusAdapter() {
                @Override
                public void focusLost(FocusEvent e) {
                  tfFaxDestinataireFocusLost(e);
                }
              });
              pnlEnvoyerParFax.add(tfFaxDestinataire, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlChoixEdition.add(pnlEnvoyerParFax, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlChoixEdition, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonnes.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ======== pnlEditionDocumentSupplementaire ========
          {
            pnlEditionDocumentSupplementaire.setTitre("Edition de documents suppl\u00e9mentaires");
            pnlEditionDocumentSupplementaire.setMinimumSize(new Dimension(600, 80));
            pnlEditionDocumentSupplementaire.setPreferredSize(new Dimension(600, 80));
            pnlEditionDocumentSupplementaire.setName("pnlEditionDocumentSupplementaire");
            pnlEditionDocumentSupplementaire.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEditionDocumentSupplementaire.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- ckBonDePreparation ----
            ckBonDePreparation.setText("Avec bon de pr\u00e9paration");
            ckBonDePreparation.setFont(new Font("sansserif", Font.PLAIN, 14));
            ckBonDePreparation.setName("label21");
            ckBonDePreparation.setPreferredSize(new Dimension(200, 30));
            ckBonDePreparation.setMaximumSize(new Dimension(250, 30));
            ckBonDePreparation.setMinimumSize(new Dimension(200, 30));
            ckBonDePreparation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckBonDePreparation.addActionListener(e -> ckBonDePreparationActionPerformed(e));
            pnlEditionDocumentSupplementaire.add(ckBonDePreparation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEditionDocumentSupplementaire, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlOptionFacturation ========
          {
            pnlOptionFacturation.setTitre("Options de facturation");
            pnlOptionFacturation.setPreferredSize(new Dimension(800, 80));
            pnlOptionFacturation.setMaximumSize(new Dimension(2147483647, 80));
            pnlOptionFacturation.setMinimumSize(new Dimension(362, 80));
            pnlOptionFacturation.setName("pnlOptionFacturation");
            pnlOptionFacturation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlOptionFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbDateFacturationMinimale ----
            lbDateFacturationMinimale.setText("Ne sera pas factur\u00e9 avant le");
            lbDateFacturationMinimale.setPreferredSize(new Dimension(230, 30));
            lbDateFacturationMinimale.setMinimumSize(new Dimension(230, 30));
            lbDateFacturationMinimale.setMaximumSize(new Dimension(230, 30));
            lbDateFacturationMinimale.setName("lbDateFacturationMinimale");
            pnlOptionFacturation.add(lbDateFacturationMinimale, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snDateFacturationMinimale ----
            snDateFacturationMinimale.setName("snDateFacturationMinimale");
            snDateFacturationMinimale.addSNComposantListener(e -> snDateFacturationMinimaleValueChanged(e));
            pnlOptionFacturation.add(snDateFacturationMinimale, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionFacturation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 2, 0), 0, 0));
          
          // ======== pnlOptionsComplementairesAchat ========
          {
            pnlOptionsComplementairesAchat.setTitre("Documents achats");
            pnlOptionsComplementairesAchat.setMinimumSize(new Dimension(690, 163));
            pnlOptionsComplementairesAchat.setPreferredSize(new Dimension(690, 167));
            pnlOptionsComplementairesAchat.setName("pnlOptionsComplementairesAchat");
            pnlOptionsComplementairesAchat.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOptionsComplementairesAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- rbMultiFournisseur ----
            rbMultiFournisseur.setText("Multi-fournisseurs");
            rbMultiFournisseur.setPreferredSize(new Dimension(150, 30));
            rbMultiFournisseur.setFont(rbMultiFournisseur.getFont().deriveFont(rbMultiFournisseur.getFont().getSize() + 2f));
            rbMultiFournisseur.setMinimumSize(new Dimension(150, 30));
            rbMultiFournisseur.setMaximumSize(new Dimension(150, 30));
            rbMultiFournisseur.setName("rbMultiFournisseur");
            rbMultiFournisseur.addActionListener(e -> rbMultiFournisseurActionPerformed(e));
            pnlOptionsComplementairesAchat.add(rbMultiFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbMonoFournisseur ----
            rbMonoFournisseur.setText("Mono-fournisseur");
            rbMonoFournisseur.setPreferredSize(new Dimension(150, 30));
            rbMonoFournisseur.setFont(rbMonoFournisseur.getFont().deriveFont(rbMonoFournisseur.getFont().getSize() + 2f));
            rbMonoFournisseur.setMaximumSize(new Dimension(150, 30));
            rbMonoFournisseur.setMinimumSize(new Dimension(150, 30));
            rbMonoFournisseur.setName("rbMonoFournisseur");
            rbMonoFournisseur.addActionListener(e -> rbMonoFournisseurActionPerformed(e));
            pnlOptionsComplementairesAchat.add(rbMonoFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlAdresseFournisseur ========
            {
              pnlAdresseFournisseur.setOpaque(false);
              pnlAdresseFournisseur.setMinimumSize(new Dimension(660, 30));
              pnlAdresseFournisseur.setPreferredSize(new Dimension(660, 30));
              pnlAdresseFournisseur.setName("pnlAdresseFournisseur");
              pnlAdresseFournisseur.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlAdresseFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- lbAdresseFournisseur ----
              lbAdresseFournisseur.setText("Adresse fournisseur");
              lbAdresseFournisseur.setPreferredSize(new Dimension(150, 30));
              lbAdresseFournisseur.setMinimumSize(new Dimension(150, 30));
              lbAdresseFournisseur.setMaximumSize(new Dimension(150, 30));
              lbAdresseFournisseur.setName("lbAdresseFournisseur");
              pnlAdresseFournisseur.add(lbAdresseFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- snAdresseFournisseur ----
              snAdresseFournisseur.setName("snAdresseFournisseur");
              pnlAdresseFournisseur.add(snAdresseFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOptionsComplementairesAchat.add(pnlAdresseFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ckGenerationCommandeAchat ----
            ckGenerationCommandeAchat.setText("G\u00e9n\u00e9ration imm\u00e9diate de la commande d'achat");
            ckGenerationCommandeAchat.setName("chk_GenerationCommandeAchat");
            ckGenerationCommandeAchat
                .setFont(ckGenerationCommandeAchat.getFont().deriveFont(ckGenerationCommandeAchat.getFont().getSize() + 2f));
            ckGenerationCommandeAchat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ckGenerationCommandeAchat.setMaximumSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.setMinimumSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.setPreferredSize(new Dimension(400, 30));
            ckGenerationCommandeAchat.addActionListener(e -> ckGenerationCommandeAchatActionPerformed(e));
            pnlOptionsComplementairesAchat.add(ckGenerationCommandeAchat, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlOptionsComplementairesAchat, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonnes.add(pnlDroite);
      }
      pnlContenu.add(pnlColonnes, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCommandesAchat ========
      {
        pnlCommandesAchat.setName("pnlCommandesAchat");
        pnlCommandesAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommandesAchat.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCommandesAchat.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCommandesAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCommandesAchat.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ---- lbCommandeAchat ----
        lbCommandeAchat.setText("Commandes d'achats");
        lbCommandeAchat.setName("lbCommandeAchat");
        pnlCommandesAchat.add(lbCommandeAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpCommandeAchat ========
        {
          scpCommandeAchat.setBackground(Color.white);
          scpCommandeAchat.setOpaque(true);
          scpCommandeAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpCommandeAchat.setPreferredSize(new Dimension(0, 0));
          scpCommandeAchat.setName("scpCommandeAchat");
          
          // ---- tblCommandeAchat ----
          tblCommandeAchat.setShowVerticalLines(true);
          tblCommandeAchat.setShowHorizontalLines(true);
          tblCommandeAchat.setBackground(Color.white);
          tblCommandeAchat.setRowHeight(20);
          tblCommandeAchat.setGridColor(new Color(204, 204, 204));
          tblCommandeAchat.setMinimumSize(new Dimension(1170, 650));
          tblCommandeAchat.setPreferredSize(new Dimension(1170, 30));
          tblCommandeAchat.setMaximumSize(new Dimension(2147483647, 2147483647));
          tblCommandeAchat.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblCommandeAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblCommandeAchat.setPreferredScrollableViewportSize(new Dimension(2147483647, 2147483647));
          tblCommandeAchat.setOpaque(false);
          tblCommandeAchat.setName("tblCommandeAchat");
          tblCommandeAchat.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblCommandeAchatMouseClicked(e);
            }
          });
          scpCommandeAchat.setViewportView(tblCommandeAchat);
        }
        pnlCommandesAchat.add(scpCommandeAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCommandesAchat,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlFax ========
    {
      pnlFax.setOpaque(false);
      pnlFax.setName("pnlFax");
      pnlFax.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlFax.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlFax.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlFax.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
    }
    
    // ---- btgPiedChiffre ----
    ButtonGroup btgPiedChiffre = new ButtonGroup();
    btgPiedChiffre.add(rbEditionAvecTotaux);
    btgPiedChiffre.add(rbEditionSansTotaux);
    btgPiedChiffre.add(rbEditionNonChiffre);
    
    // ---- bgTypeEditionDevis ----
    bgTypeEditionDevis.add(rbDevisAvecTotaux);
    bgTypeEditionDevis.add(rbDevisSansTotaux);
    bgTypeEditionDevis.add(rbFactureProforma);
    
    // ---- btgEditionFacture ----
    ButtonGroup btgEditionFacture = new ButtonGroup();
    btgEditionFacture.add(rbImprimerFacture);
    btgEditionFacture.add(rbImprimerTicket);
    
    // ---- groupeFournisseur ----
    groupeFournisseur.add(rbMultiFournisseur);
    groupeFournisseur.add(rbMonoFournisseur);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonnes;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlChiffrage;
  private JRadioButton rbEditionAvecTotaux;
  private JRadioButton rbEditionSansTotaux;
  private JRadioButton rbEditionNonChiffre;
  private SNPanelTitre pnlEditionDevis;
  private JRadioButton rbDevisAvecTotaux;
  private JRadioButton rbDevisSansTotaux;
  private JRadioButton rbFactureProforma;
  private SNPanelTitre pnlChoixImpressionFacture;
  private JRadioButton rbImprimerFacture;
  private JRadioButton rbImprimerTicket;
  private SNPanelTitre pnlChoixEdition;
  private JCheckBox ckVisualiser;
  private SNPanel pnlImprimer;
  private JCheckBox ckImprimer;
  private SNPanel pnlEnvoyerParMail;
  private JCheckBox ckEnvoyerParMail;
  private SNLabelChamp lbContactMail;
  private SNContact snContactMail;
  private SNLabelChamp lbMail;
  private SNTexte tfEmailDestinataire;
  private SNPanel pnlEnvoyerParFax;
  private JCheckBox ckEnvoyerParFax;
  private SNLabelChamp lbContactFax;
  private SNContact snContactFax;
  private SNLabelChamp lbFax;
  private SNTexte tfFaxDestinataire;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEditionDocumentSupplementaire;
  private XRiCheckBox ckBonDePreparation;
  private SNPanelTitre pnlOptionFacturation;
  private SNLabelChamp lbDateFacturationMinimale;
  private SNDate snDateFacturationMinimale;
  private SNPanelTitre pnlOptionsComplementairesAchat;
  private JRadioButton rbMultiFournisseur;
  private JRadioButton rbMonoFournisseur;
  private JPanel pnlAdresseFournisseur;
  private SNLabelChamp lbAdresseFournisseur;
  private SNAdresseFournisseur snAdresseFournisseur;
  private JCheckBox ckGenerationCommandeAchat;
  private SNPanel pnlCommandesAchat;
  private SNLabelTitre lbCommandeAchat;
  private JScrollPane scpCommandeAchat;
  private NRiTable tblCommandeAchat;
  private SNBarreBouton snBarreBouton;
  private JPanel pnlFax;
  private ButtonGroup bgTypeEditionDevis;
  private ButtonGroup groupeFournisseur;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
