/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran principal de la consultation de documents de ventes.
 */
public class VueConsultationDocumentVente extends AbstractVuePanel<ModeleConsultationDocumentVente> {
  private VueListeConsultationDocumentVente vueListe;
  private VueDetailConsultationDocumentVente vueDetail;
  
  /**
   * Constructeur.
   */
  public VueConsultationDocumentVente(ModeleConsultationDocumentVente pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de titre
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    pnlBpresentation.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    
    // Ajouter la vue liste
    vueListe = new VueListeConsultationDocumentVente(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.LISTE, vueListe);
    pnlContenu.add(vueListe, EnumCleVueListeDetail.LISTE.getLibelle());
    
    // Ajouter la vue détail
    vueDetail = new VueDetailConsultationDocumentVente(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.DETAIL, vueDetail);
    pnlContenu.add(vueDetail, EnumCleVueListeDetail.DETAIL.getLibelle());
  }
  
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirVue();
  }
  
  private void rafraichirTitre() {
    if (getModele().getTitreEcran() != null) {
      pnlBpresentation.setText(getModele().getTitreEcran());
    }
    else {
      pnlBpresentation.setText("");
    }
  }
  
  private void rafraichirVue() {
    InterfaceCleVue interfaceCleVue = getModele().getCleVueEnfantActive();
    if (interfaceCleVue != null) {
      CardLayout cardLayout = (CardLayout) (pnlContenu.getLayout());
      cardLayout.show(pnlContenu, interfaceCleVue.getLibelle());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    pnlBpresentation = new SNBandeauTitre();
    pnlSud = new JPanel();
    scpContenu = new JScrollPane();
    pnlContenu = new JPanel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Consultation de documents de ventes");
      pnlBpresentation.setName("pnlBpresentation");
      pnlNord.add(pnlBpresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setPreferredSize(new Dimension(1024, 650));
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== scpContenu ========
      {
        scpContenu.setPreferredSize(new Dimension(1024, 650));
        scpContenu.setBorder(null);
        scpContenu.setName("scpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(1024, 650));
          pnlContenu.setBackground(new Color(239, 239, 222));
          pnlContenu.setForeground(Color.black);
          pnlContenu.setMinimumSize(new Dimension(1200, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new CardLayout());
        }
        scpContenu.setViewportView(pnlContenu);
      }
      pnlSud.add(scpContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre pnlBpresentation;
  private JPanel pnlSud;
  private JScrollPane scpContenu;
  private JPanel pnlContenu;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
