/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumOrigineDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.metier.vente.representant.snrepresentant.SNRepresentant;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet client du comptoir.
 */
public class VueOngletClient extends JPanel implements InterfaceVueOnglet {
  private static final String BOUTON_CREER_DEVIS = "Créer devis";
  private static final String BOUTON_CREER_COMMANDE = "Créer commande";
  private static final String BOUTON_CREER_BON = "Créer bon";
  private static final String BOUTON_CREER_FACTURE = "Créer facture";
  private static final String BOUTON_CREER_AVOIR = "Créer avoir";
  private static final String BOUTON_CHANGER_CLIENT = "Changer client";
  private static final String BOUTON_MODIFIER_DOCUMENT = "Modifier document";
  private static final String BOUTON_AFFICHER_DOCUMENT = "Afficher document";
  private static final String BOUTON_CONSULTER_DOCUMENT = "Consulter document";
  private static final String BOUTON_IMPORTER_PRODEVIS = "Importer devis Pro-Devis";
  private static final String BOUTON_DUPLIQUER_DEVIS = "Dupliquer devis";
  private static final String BOUTON_DUPLIQUER_COMMANDE = "Dupliquer commande";
  private static final String BOUTON_DUPLIQUER_BON = "Dupliquer bon";
  private static final String BOUTON_DUPLIQUER_FACTURE = "Dupliquer facture";
  private static final String BOUTON_EXTRAIRE_COMMANDE = "Extraire en commande";
  private static final String BOUTON_EXTRAIRE_BON = "Extraire en bon";
  private static final String BOUTON_EXTRAIRE_FACTURE = "Extraire en facture";
  private static final String BOUTON_AFFICHER_DOCUMENT_LIE = "Afficher documents liés";
  private static final int NOMBRE_LIGNES = 15;
  private static final String[] TITRE_LISTE_DEVIS = new String[] { "Num\u00e9ro", "Date", "Validit\u00e9", "Montant HT", "Montant TTC",
      "Magasin", "R\u00e9f\u00e9rence longue", "D\u00e9livrance", "Direct usine", "Extraction" };
  private static final String[] TITRE_LISTE_COMMANDE = new String[] { "Num\u00e9ro", "Date", "Montant HT", "Montant TTC", "Magasin",
      "R\u00e9f\u00e9rence longue", "R\u00e9f\u00e9rence chantier", "D\u00e9livrance", "Direct usine", "Extraction" };
  private static final String[] TITRE_LISTE_BON = new String[] { "Num\u00e9ro", "Date", "Montant HT", "Montant TTC", "Magasin",
      "R\u00e9f\u00e9rence longue", "R\u00e9f\u00e9rence chantier", "D\u00e9livrance", "Direct usine" };
  private static final String[] TITRE_LISTE_FACTURE = new String[] { "Num\u00e9ro", "Date", "Montant HT", "Montant TTC", "Magasin",
      "R\u00e9f\u00e9rence longue", "D\u00e9livrance", "Direct usine" };
  
  private static final String LIBELLE_NOM = "Nom";
  private static final String LIBELLE_PRENOM = "Pr\u00e9nom";
  private static final String LIBELLE_RAISON_SOCIALE = "Raison sociale";
  private static final String LIBELLE_COMPLEMENT = "Compl\u00e9ment";
  private static final String LIBELLE_RESTE_ENCOURS = "Reste";
  private static final String LIBELLE_DEPASSEMENT_ENCOURS = "Dépassement";
  
  // Onglets des documents client
  private static final int ONGLET_DEVIS = 0;
  private static final int ONGLET_COMMANDES = 1;
  private static final int ONGLET_BONS = 2;
  private static final int ONGLET_FACTURES = 3;
  
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  private static final int NOMBRE_DECIMALE_ENCOURS = 0;
  
  // Variables
  private ModeleComptoir modele = null;
  private int indexChantiersClient = -1;
  protected boolean executerEvenements = false;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Constructeur.
   * @param pComptoir Modele comptoir
   */
  public VueOngletClient(ModeleComptoir pComptoir) {
    modele = pComptoir;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    executerEvenements = false;
    
    // Visiblité des composants
    lbMessageErreur.setText("");
    lbMessageEncours.setText("");
    lbMessageEncours.setVisible(false);
    
    // Formatage des zones de saisie
    tfNom.setLongueur(Client.TAILLE_ZONE_NOM);
    tfComplementNom.setLongueur(Client.TAILLE_ZONE_NOM);
    tfRue.setLongueur(Client.TAILLE_ZONE_NOM);
    tfLocalisation.setLongueur(Client.TAILLE_ZONE_NOM);
    
    tfEncours.setEnabled(false);
    tfEncoursCommande.setEnabled(false);
    tfPlafond.setEnabled(false);
    tfResteEncours.setEnabled(false);
    tfPlafondExceptionnel.setEnabled(false);
    snDatePlafondExceptionnel.setEnabled(false);
    
    // Initialise l'aspect de la table des devis
    scpDevisEnCours.getViewport().setBackground(Color.WHITE);
    tblDevisEnCours
        .personnaliserAspect(TITRE_LISTE_DEVIS, new int[] { 80, 100, 100, 100, 100, 70, 200, 90, 100, 100 },
            new int[] { 90, 120, 120, 150, 150, 70, 5000, 90, 100, 100 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.CENTRE,
                NRiTable.DROITE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE },
            14);
    
    // Initialise l'aspect de la table des commandes
    scpCommandesEnCours.getViewport().setBackground(Color.WHITE);
    
    tblCommandesEnCours
        .personnaliserAspect(TITRE_LISTE_COMMANDE, new int[] { 80, 85, 100, 100, 70, 200, 200, 90, 100, 85 },
            new int[] { 90, 95, 150, 150, 70, 5000, 5000, 100, 100, 85 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE,
                NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE },
            14);
    
    // Initialise l'aspect de la table des bons
    scpLivraisonsEnCours.getViewport().setBackground(Color.WHITE);
    tblLivraisonsEnCours.personnaliserAspect(TITRE_LISTE_BON, new int[] { 80, 85, 100, 100, 70, 200, 200, 90, 100 },
        new int[] { 90, 95, 150, 150, 70, 5000, 5000, 90, 100 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE,
            NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE },
        14);
    
    // Initialise l'aspect de la table des factures
    scpFacturesEnCours.getViewport().setBackground(Color.WHITE);
    tblFacturesEnCours.personnaliserAspect(TITRE_LISTE_FACTURE, new int[] { 80, 85, 100, 100, 70, 200, 90, 100 },
        new int[] { 90, 95, 150, 150, 70, 5000, 90, 100 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE,
            NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE },
        14);
    
    taBlocnotesClient.setPreferredSize(new Dimension(400, 500));
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    taBlocnotesClient.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    taBlocnotesClient.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    // Ajouter un listener sur les changement de sélection
    tblDevisEnCours.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblDevisEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportDevis = scpDevisEnCours.getViewport();
    viewportDevis.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpDevisEnCoursStateChanged(e);
      }
    });
    scpDevisEnCours.setViewport(viewportDevis);
    
    // Ajouter un listener sur les changement de sélection
    tblCommandesEnCours.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblCommandeEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportCommandes = scpCommandesEnCours.getViewport();
    viewportCommandes.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpCommandesEnCoursStateChanged(e);
      }
    });
    scpCommandesEnCours.setViewport(viewportCommandes);
    
    // Ajouter un listener sur les changement de sélection
    tblLivraisonsEnCours.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblLivraisonEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportLivraisons = scpLivraisonsEnCours.getViewport();
    viewportLivraisons.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpLivraisonsEnCoursStateChanged(e);
      }
    });
    scpLivraisonsEnCours.setViewport(viewportLivraisons);
    
    // Ajouter un listener sur les changement de sélection
    tblFacturesEnCours.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblFactureEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportFactures = scpFacturesEnCours.getViewport();
    viewportFactures.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpFacturesEnCoursStateChanged(e);
      }
    });
    scpFacturesEnCours.setViewport(viewportFactures);
    
    // Raccourcis clavier
    btRechercheDocumentsClient.setMnemonic(KeyEvent.VK_R);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER_DEVIS, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_COMMANDE, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_BON, 'b', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_FACTURE, 'f', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_AVOIR, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_CHANGER_CLIENT, 'h', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_DEVIS, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_COMMANDE, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_BON, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_FACTURE, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_COMMANDE, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_BON, 'b', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_FACTURE, 'f', false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_DOCUMENT, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENT, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_CONSULTER_DOCUMENT, 'v', false);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENT_LIE, 'l', false);
    snBarreBouton.ajouterBouton(BOUTON_IMPORTER_PRODEVIS, 'p', false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    executerEvenements = true;
  }
  
  /**
   * Rafraichir l'écran.
   */
  @Override
  public void rafraichir() {
    try {
      executerEvenements = false;
      
      rafraichirMessage();
      
      // Rafraîchir les informations générales
      rafraichirEtablissement();
      rafraichirMagasin();
      rafraichirVendeurs();
      rafraichirRepresentants();
      
      // Rafraichir les informations clients
      rafraichirCiviliteClient();
      rafraichirNomClient();
      rafraichirComplementNomClient();
      rafraichirLocalisationClient();
      rafraichirRueClient();
      rafraichirCodePostalCommuneClient();
      // rafraichirCodePostalClient();
      // rafraichirVilleClient();
      rafraichirContactClient();
      rafraichirTelephoneClient();
      rafraichirFaxClient();
      rafraichirMailClient();
      rafraichirObservationsClient();
      
      // Rafraichir les informations clients
      rafraichirBoutonDetailEncours();
      rafraichirEncours();
      rafraichirEncoursCommande();
      rafraichirPlafondEncours();
      rafraichirResteEncours();
      rafraichirPlafondExceptionnel();
      rafraichirDatePlafondExceptionnel();
      rafraichirMessageEnCours();
      
      // Rafraîchir la liste des documents
      rafraichirTitreListeDocumentVente();
      rafraichirListeDocumentVente();
      rafraichirBoutonRechercheDocumentVente();
      
      // Rafraîchir les boutons
      rafraichirBoutonModifierDocument();
      rafraichirBoutonAfficherDocument();
      rafraichirBoutonCreerDevis();
      rafraichirBoutonCreerCommande();
      rafraichirBoutonCreerBon();
      rafraichirBoutonCreerFacture();
      rafraichirBoutonCreerAvoir();
      rafraichirBoutonChangerClient();
      rafraichirBoutonImporterProDevis();
      rafraichirBoutonDupliquerDevis();
      rafraichirBoutonDupliquerCommande();
      rafraichirBoutonDupliquerBon();
      rafraichirBoutonDupliquerFacture();
      rafraichirBoutonExtraireCommande();
      rafraichirBoutonExtraireBon();
      rafraichirBoutonExtraireFacture();
      rafraichirBoutonAfficherDocumentLie();
      
      // Rafraîchir la sélection client (doit être fait en dernier sinon le temps de rafraichir tous les composants l'utilisateur peut
      // saisir du texte)
      rafraichirSelectionClient();
      
      // Positionner le focus
      switch (modele.getComposantAyantLeFocus()) {
        case ModeleComptoir.FOCUS_CLIENT_RECHERCHE_CLIENT:
          snClient.requestFocus();
          break;
        case ModeleComptoir.FOCUS_ONGLET_COMMANDES:
          if (tbpDocumentsEnCours.isEnabledAt(ONGLET_COMMANDES)) {
            tbpDocumentsEnCours.setSelectedIndex(ONGLET_COMMANDES);
          }
          break;
      }
      
      executerEvenements = true;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Rafraîchir le client sélectionné.
   */
  private void rafraichirSelectionClient() {
    // Composant non saisissable tant que le chargement des données n'est pas terminé
    snClient.setEnabled(modele.isDonneesChargees());
    // Configurer le composant
    snClient.setSession(modele.getSession());
    if (modele.getEtablissement() != null) {
      snClient.setIdEtablissement(modele.getEtablissement().getId());
    }
    else {
      snClient.setIdEtablissement(null);
    }
    
    // Autoriser la recherche de prospects
    snClient.setRechercheProspectAutorisee(false);
    
    // Rafraîchir sa valeur
    if (modele.getClientCourant() != null && modele.getClientCourant().isExistant()) {
      snClient.setSelection(modele.getClientCourant());
    }
    else {
      snClient.setSelection(null);
    }
    
    snClient.setEnabled(!modele.isDocumentEnCoursDeSaisie());
  }
  
  /**
   * Rafraîchir le message associé au client.
   */
  private void rafraichirMessage() {
    if (modele.getMessageClient() != null) {
      lbMessageErreur.setText(modele.getMessageClient());
    }
    else {
      lbMessageErreur.setText("");
    }
  }
  
  /**
   * Rafraîchir la civilité du client.
   */
  private void rafraichirCiviliteClient() {
    snCivilite.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    snCivilite.setSession(modele.getSession());
    if (modele.getEtablissement() != null) {
      snCivilite.setIdEtablissement(modele.getEtablissement().getId());
    }
    snCivilite.setAucunAutorise(true);
    snCivilite.charger(false);
    if (client == null) {
      lbCivilite.setVisible(false);
      snCivilite.setVisible(false);
      snCivilite.setSelection(null);
      snCivilite.setEnabled(modele.isCreationClientComptant());
    }
    else if (client.isProfessionel()) {
      lbCivilite.setVisible(false);
      snCivilite.setVisible(false);
      snCivilite.setSelection(null);
      snCivilite.setEnabled(false);
    }
    else {
      // Sélectionner la civilité
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getIdCivilite() != null) {
        snCivilite.setIdSelection(contact.getIdCivilite());
      }
      else {
        snCivilite.setSelection(null);
      }
      lbCivilite.setVisible(true);
      snCivilite.setVisible(true);
      snCivilite.setEnabled(modele.isCreationClientComptant());
    }
  }
  
  /**
   * Rafraîchir le nom du client.
   */
  private void rafraichirNomClient() {
    tfNom.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfNom.setText("");
      tfNom.setEnabled(modele.isCreationClientComptant());
    }
    else {
      if (client.isParticulier()) {
        lbNom.setText(LIBELLE_NOM);
      }
      else {
        lbNom.setText(LIBELLE_RAISON_SOCIALE);
      }
      
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        tfNom.setText(contact.getNom());
      }
      else if (adresse != null && adresse.getNom() != null) {
        tfNom.setText(adresse.getNom());
      }
      else {
        tfNom.setText("");
      }
      
      tfNom.setEnabled(modele.isCreationClientComptant());
    }
  }
  
  /**
   * Rafraîchir le complément du nom du client.
   */
  private void rafraichirComplementNomClient() {
    tfComplementNom.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfComplementNom.setText("");
      tfComplementNom.setEnabled(modele.isCreationClientComptant());
    }
    else {
      if (client.isParticulier()) {
        lbComplementNom.setText(LIBELLE_PRENOM);
      }
      else {
        lbComplementNom.setText(LIBELLE_COMPLEMENT);
      }
      
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getPrenom() != null) {
        tfComplementNom.setText(contact.getPrenom());
      }
      else if (adresse != null && adresse.getComplementNom() != null) {
        tfComplementNom.setText(adresse.getComplementNom());
      }
      else {
        tfComplementNom.setText("");
      }
      
      tfComplementNom.setEnabled(modele.isCreationClientComptant());
    }
  }
  
  /**
   * Rafraîchir la localisation du client.
   */
  private void rafraichirLocalisationClient() {
    tfLocalisation.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfLocalisation.setText("");
      tfLocalisation.setEnabled(modele.isCreationClientComptant());
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null) {
        tfLocalisation.setText(adresse.getLocalisation());
      }
      else {
        tfLocalisation.setText("");
      }
      tfLocalisation.setEnabled(modele.isCreationClientComptant() || client.isClientComptant());
    }
  }
  
  /**
   * Rafraîchir la rue du client.
   */
  private void rafraichirRueClient() {
    tfRue.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfRue.setText("");
      tfRue.setEnabled(modele.isCreationClientComptant());
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null) {
        tfRue.setText(adresse.getRue());
      }
      else {
        tfRue.setText("");
      }
      
      tfRue.setEnabled(modele.isCreationClientComptant() || !client.isClientEnCompte());
    }
  }
  
  /**
   * Rafraîchir le code postal et la commune du client.
   */
  private void rafraichirCodePostalCommuneClient() {
    snCodePostalCommune.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    snCodePostalCommune.setSession(modele.getSession());
    
    if (client == null || modele.getEtablissement() == null) {
      snCodePostalCommune.setSelection(null);
      snCodePostalCommune.setEnabled(modele.isCreationClientComptant());
      return;
    }
    snCodePostalCommune.setIdEtablissement(modele.getEtablissement().getId());
    
    Adresse adresse = client.getAdresse();
    CodePostalCommune codePostalCommuneClient = null;
    if (adresse != null) {
      codePostalCommuneClient = CodePostalCommune.getInstance(adresse.getCodePostalFormate(), adresse.getVille());
      snCodePostalCommune.charger(false);
      snCodePostalCommune.setSelection(codePostalCommuneClient);
    }
    else {
      snCodePostalCommune.setSelection(null);
    }
    
    snCodePostalCommune.setEnabled(modele.isCreationClientComptant() || !client.isClientEnCompte());
  }
  
  /**
   * Rafraîchir le contact du client.
   */
  private void rafraichirContactClient() {
    tfContact.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      lbContact.setVisible(false);
      tfContact.setVisible(false);
      tfContact.setText("");
      tfContact.setEnabled(false);
    }
    else if (client.isClientComptant()) {
      lbContact.setVisible(false);
      tfContact.setVisible(false);
      tfContact.setText("");
      tfContact.setEnabled(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNomComplet() != null) {
        tfContact.setText(contact.getNomComplet());
      }
      else {
        tfContact.setText("");
      }
      
      lbContact.setVisible(true);
      tfContact.setVisible(true);
      tfContact.setEnabled(modele.isCreationClientComptant() || !client.isClientEnCompte());
    }
  }
  
  /**
   * Rafraîchir le téléphone du client.
   */
  private void rafraichirTelephoneClient() {
    tfTelephone.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfTelephone.setText("");
      tfTelephone.setEnabled(modele.isCreationClientComptant());
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroTelephone1() != null) {
        tfTelephone.setText(contact.getNumeroTelephone1());
      }
      else if (client.getNumeroTelephone() != null) {
        tfTelephone.setText(client.getNumeroTelephone());
      }
      else {
        tfTelephone.setText("");
      }
      
      tfTelephone.setEnabled(modele.isCreationClientComptant() || !client.isClientEnCompte());
    }
  }
  
  /**
   * Rafraîchir le fax du client.
   */
  private void rafraichirFaxClient() {
    tfFax.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      lbFax.setVisible(false);
      tfFax.setText("");
      tfFax.setVisible(false);
      tfFax.setEnabled(false);
    }
    else if (client.isClientComptant()) {
      lbFax.setVisible(false);
      tfFax.setVisible(false);
      tfFax.setText("");
      tfFax.setEnabled(false);
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroFax() != null) {
        tfFax.setText(contact.getNumeroFax());
      }
      else if (client.getNumeroFax() != null) {
        tfFax.setText(client.getNumeroFax());
      }
      else {
        tfFax.setText("");
      }
      
      lbFax.setVisible(true);
      tfFax.setVisible(true);
      tfFax.setEnabled(false);
    }
  }
  
  /**
   * Rafraîchir le mail du client.
   */
  private void rafraichirMailClient() {
    tfEmail.setEnabled(modele.isDonneesChargees());
    Client client = modele.getClientCourant();
    if (client == null) {
      tfEmail.setText("");
      tfEmail.setEnabled(modele.isCreationClientComptant());
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getEmail() != null) {
        tfEmail.setText(contact.getEmail());
      }
      else {
        tfEmail.setText("");
      }
      
      tfEmail.setEnabled(modele.isCreationClientComptant() || !client.isClientEnCompte());
    }
  }
  
  /**
   * Rafraîchir l'établissement.
   */
  private void rafraichirEtablissement() {
    snEtablissement.setEnabled(modele.isDonneesChargees());
    if (modele.getEtablissement() == null) {
      return;
    }
    snEtablissement.setSession(modele.getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(modele.getEtablissement().getId());
    // Si un client a été choisi on ne peut plus change d'établissement
    snEtablissement.setEnabled(modele.getClientCourant() == null || modele.getClientCourant().getId().getNumero() == 0);
  }
  
  /**
   * Rafraîchir le magasin.
   */
  private void rafraichirMagasin() {
    cbMagasin.setEnabled(modele.isDonneesChargees());
    cbMagasin.setSession(modele.getSession());
    if (snEtablissement.getIdSelection() != null) {
      cbMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      cbMagasin.charger(false);
      cbMagasin.setIdSelection(modele.getIdMagasin());
    }
    
    cbMagasin.setEnabled(modele.getDocumentVenteEnCours() == null && !modele.isDocumentEnCoursDeSaisie());
  }
  
  /**
   * Rafraîchir le vendeur.
   */
  private void rafraichirVendeurs() {
    IdVendeur idVendeur = modele.getIdVendeur();
    cbVendeur.setSession(modele.getSession());
    if (snEtablissement.getIdSelection() != null) {
      cbVendeur.setIdEtablissement(snEtablissement.getIdSelection());
      cbVendeur.charger(false);
    }
    
    if (idVendeur != null) {
      cbVendeur.setIdSelection(modele.getIdVendeur());
    }
  }
  
  /**
   * Rafraîchir le représentant.
   */
  private void rafraichirRepresentants() {
    snRepresentant.setEnabled(modele.isDonneesChargees());
    IdRepresentant idRepresentant = modele.getIdRepresentant();
    snRepresentant.setSession(modele.getSession());
    if (snEtablissement.getIdSelection() != null) {
      snRepresentant.setIdEtablissement(snEtablissement.getIdSelection());
      snRepresentant.charger(false);
      
      snRepresentant.setIdSelection(idRepresentant);
    }
    // Le représentant n'est modifiable que dans le cas d'une création de client comptant et si la sécurité 180 ne l'interdit pas
    snRepresentant.setEnabled(modele.getEtape() == ModeleComptoir.ETAPE_CREATION_CLIENT && !modele.isRepresentantNonModifiable());
  }
  
  /**
   * Rafraîchir le bouton détail encours.
   */
  private void rafraichirBoutonDetailEncours() {
    boolean actif = false;
    if (modele.getClientCourant() != null && modele.getClientCourant().isExistant() && modele.getClientCourant().isClientEnCompte()) {
      actif = true;
    }
    btEncoursComptable.setEnabled(actif);
  }
  
  /**
   * Rafraîchir l'encours.
   */
  private void rafraichirEncours() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().getEncours() != null) {
      tfEncours.setText(modele.getEncoursClient().getEncours().toString());
    }
    else {
      tfEncours.setText("");
    }
  }
  
  /**
   * Rafraîchir l'encours des commandes.
   */
  private void rafraichirEncoursCommande() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().getEncoursCommande() != null) {
      tfEncoursCommande.setText(modele.getEncoursClient().getEncoursCommande().toString());
    }
    else {
      tfEncoursCommande.setText("");
    }
  }
  
  /**
   * Rafraîchir le plafond d'encours.
   */
  private void rafraichirPlafondEncours() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().isPlafondPresent()
        && modele.getClientCourant().isClientEnCompte()) {
      tfPlafond.setText(modele.getEncoursClient().getPlafond().toString());
      tfPlafond.setVisible(true);
      lbPlafond.setVisible(true);
    }
    else {
      tfPlafond.setText("");
      tfPlafond.setVisible(false);
      lbPlafond.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir le reste d'encours.
   */
  private void rafraichirResteEncours() {
    // Ne pas afficher le reste ou dépassement si le plafond n'est pas définit
    if (modele.getEncoursClient() != null && modele.getEncoursClient().isPlafondPresent()
        && modele.getClientCourant().isClientEnCompte()) {
      // Afficher le dépassement en rouge
      if (modele.getEncoursClient().getDepassement() > 0) {
        lbResteEncours.setText(LIBELLE_DEPASSEMENT_ENCOURS);
        lbResteEncours.setVisible(true);
        tfResteEncours.setForeground(Color.RED);
        tfResteEncours.setText(modele.getEncoursClient().getDepassement().toString());
        tfResteEncours.setVisible(true);
      }
      // Afficher le reste en noir
      else {
        lbResteEncours.setText(LIBELLE_RESTE_ENCOURS);
        lbResteEncours.setVisible(true);
        tfResteEncours.setForeground(Color.BLACK);
        tfResteEncours.setText((modele.getEncoursClient().getResteAvantPlafond().toString()));
        tfResteEncours.setVisible(true);
      }
    }
    else {
      lbResteEncours.setText("");
      lbResteEncours.setVisible(false);
      tfResteEncours.setForeground(Color.BLACK);
      tfResteEncours.setText("");
      tfResteEncours.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le plafond exceptionnel.
   */
  private void rafraichirPlafondExceptionnel() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().isPlafondExceptionnelPresent()) {
      tfPlafondExceptionnel.setText(modele.getEncoursClient().getPlafondExceptionnel().toString());
      lbPlafondExceptionnel.setVisible(true);
      tfPlafondExceptionnel.setVisible(true);
    }
    else {
      tfPlafondExceptionnel.setText("");
      lbPlafondExceptionnel.setVisible(false);
      tfPlafondExceptionnel.setVisible(false);
    }
  }
  
  /**
   * Rafraichir la date de validité du plafond exceptionnel.
   */
  private void rafraichirDatePlafondExceptionnel() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().isPlafondExceptionnelPresent()) {
      snDatePlafondExceptionnel.setDate(modele.getEncoursClient().getDateLimitePlafondExceptionnel());
      lbDatePlafondExceptionnel.setVisible(true);
      snDatePlafondExceptionnel.setVisible(true);
    }
    else {
      snDatePlafondExceptionnel.setDate(null);
      lbDatePlafondExceptionnel.setVisible(false);
      snDatePlafondExceptionnel.setVisible(false);
    }
    
    if (modele.getEncoursClient() == null) {
      snDatePlafondExceptionnel.setDate(null);
      return;
    }
  }
  
  /**
   * Rafraichir le message adapté suivant l'état de l'encours.
   */
  private void rafraichirMessageEnCours() {
    if (modele.getEncoursClient() != null && modele.getEncoursClient().getMessageEncours() != null
        && !modele.getEncoursClient().getMessageEncours().isEmpty()) {
      lbMessageEncours.setText(modele.getEncoursClient().getMessageEncours());
      lbMessageEncours.setVisible(true);
    }
    else {
      lbMessageEncours.setText("");
      lbMessageEncours.setVisible(false);
    }
  }
  
  /**
   * Rafraîchir les observation client.
   */
  private void rafraichirObservationsClient() {
    if (modele.getClientCourant() == null || modele.getClientCourant().getBlocNote() == null) {
      taBlocnotesClient.setText("");
    }
    else {
      taBlocnotesClient.setText(modele.getClientCourant().getBlocNote().getTexte());
    }
    taBlocnotesClient.setCaretPosition(0);
    
    // Suivant la sécurité de l'utilisateur
    taBlocnotesClient.setEditable(modele.getEtablissement() != null && ManagerSessionClient.getInstance()
        .verifierDroitGescom(modele.getIdSession(), EnumDroitSecurite.IS_AUTORISE_MODIFICATION_BLOC_NOTES_CLIENT));
  }
  
  /**
   * Rafraîchir le titre la liste des documents de vente.
   */
  private void rafraichirTitreListeDocumentVente() {
    if (modele.getClientCourant() != null && modele.getClientCourant().isExistant()) {
      sptDocumentEnCours.setText("Documents en cours " + modele.getPeriodeEnCours());
    }
    else {
      sptDocumentEnCours.setText("Documents en cours ");
    }
  }
  
  /**
   * Rafraîchir les listes des documents de vente.
   */
  private void rafraichirListeDocumentVente() {
    tbpDocumentsEnCours.setEnabled(true);
    
    // Rafraîhir les différents onglets
    rafraichirListeDevis();
    rafraichirListeCommande();
    rafraichirListeBon();
    rafraichirListeFacture();
    
    // Activer le premier onglet non grisé
    if (modele.isActivationOngletDocumentsClient()) {
      modele.setActivationOngletDocumentsClient(false);
      boolean actif = false;
      for (int tab = 0; tab < tbpDocumentsEnCours.getTabCount(); tab++) {
        if (tbpDocumentsEnCours.isEnabledAt(tab)) {
          tbpDocumentsEnCours.setSelectedIndex(tab);
          actif = true;
          break;
        }
      }
      
      // S'il n'y a aucun document alors on active le premier onglet
      if (!actif) {
        tbpDocumentsEnCours.setSelectedIndex(ONGLET_DEVIS);
      }
    }
  }
  
  /**
   * Rafraîchir la liste des devis.
   */
  private void rafraichirListeDevis() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = modele.getModeleDocumentVenteParType();
    
    // Désactiver l'onglet devis s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeDevis() == null
        || modeleDocumentVenteParType.getListeDevis().isEmpty()) {
      tblDevisEnCours.mettreAJourDonnees(null);
      tbpDocumentsEnCours.setEnabledAt(ONGLET_DEVIS, false);
      tbpDocumentsEnCours.setTitleAt(ONGLET_DEVIS, "Devis");
      scpDevisEnCours.getVerticalScrollBar().setValue(0);
      scpDevisEnCours.getViewport().reshape(0, 0, scpDevisEnCours.getViewport().getWidth(), 0);
      return;
    }
    tbpDocumentsEnCours.setEnabledAt(ONGLET_DEVIS, true);
    tbpDocumentsEnCours.setTitleAt(ONGLET_DEVIS, "Devis (" + modeleDocumentVenteParType.getListeDevis().size() + ")");
    
    // Renseigner les données du tableau
    ListeDocumentVenteBase listeDocumentVenteBase = modeleDocumentVenteParType.getListeDevis();
    String[][] donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_DEVIS.length];
    for (int ligne = 0; ligne < listeDocumentVenteBase.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(ligne);
      
      // Identifiant
      donnees[ligne][0] = documentVenteBase.getId().toString();
      
      // Date de création
      if (documentVenteBase.isVerrouille()) {
        donnees[ligne][1] = "Verrouillé";
      }
      else if (documentVenteBase.getDateCreation() != null) {
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
      }
      
      // Date de validité du devis
      if (documentVenteBase.getDateValiditeDevis() != null) {
        donnees[ligne][2] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateValiditeDevis());
      }
      else {
        donnees[ligne][2] = "Pas de limite";
      }
      
      // Total HT
      if (documentVenteBase.getTotalHT() != null) {
        donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
      }
      
      // Total TTC
      if (documentVenteBase.getTotalTTC() != null) {
        donnees[ligne][4] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
      }
      
      // Magasin
      Magasin magasin = modele.getListeMagasinParEtablissement().get(documentVenteBase.getId().getIdEtablissement())
          .getMagasinParId(documentVenteBase.getIdMagasin());
      if (magasin != null) {
        donnees[ligne][5] = magasin.getId().getCode();
      }
      
      // Référence longue
      if (documentVenteBase.getReferenceLongue() != null) {
        donnees[ligne][6] = documentVenteBase.getReferenceLongue();
      }
      
      // Mode d'expédition
      if (documentVenteBase.geIdModeExpedition() != null) {
        ModeExpedition modeExpedition = modele.getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
        if (modeExpedition != null) {
          donnees[ligne][7] = modeExpedition.getLibelle();
        }
      }
      
      // Direct usine
      if (documentVenteBase.isDirectUsine()) {
        donnees[ligne][8] = "Oui";
        if (documentVenteBase.isCommandeAchatGeneree()) {
          donnees[ligne][8] = "Achat généré";
        }
      }
      
      // Etat de l'extraction
      if (documentVenteBase.getEtatExtraction() != null) {
        donnees[ligne][9] = documentVenteBase.getEtatExtraction().getLibelle();
      }
    }
    tblDevisEnCours.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexDevisSelectionne();
    if (index > -1) {
      tblDevisEnCours.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblDevisEnCours.clearSelection();
    }
  }
  
  /**
   * Rafraîchir la liste des commandes.
   */
  private void rafraichirListeCommande() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = modele.getModeleDocumentVenteParType();
    
    // Désactiver l'onglet commande s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeCommande() == null
        || modeleDocumentVenteParType.getListeCommande().isEmpty()) {
      tblCommandesEnCours.mettreAJourDonnees(null);
      tbpDocumentsEnCours.setEnabledAt(ONGLET_COMMANDES, false);
      tbpDocumentsEnCours.setTitleAt(ONGLET_COMMANDES, "Commandes");
      scpCommandesEnCours.getVerticalScrollBar().setValue(0);
      scpCommandesEnCours.getViewport().reshape(0, 0, scpCommandesEnCours.getViewport().getWidth(), 0);
      return;
    }
    tbpDocumentsEnCours.setEnabledAt(ONGLET_COMMANDES, true);
    tbpDocumentsEnCours.setTitleAt(ONGLET_COMMANDES, "Commandes (" + modeleDocumentVenteParType.getListeCommande().size() + ")");
    
    // Renseigner les données du tableau
    ListeDocumentVenteBase listeDocumentVenteBase = modeleDocumentVenteParType.getListeCommande();
    String[][] donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_COMMANDE.length];
    for (int ligne = 0; ligne < listeDocumentVenteBase.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(ligne);
      
      // Identifiant
      donnees[ligne][0] = documentVenteBase.getId().toString();
      
      // Date de création
      if (documentVenteBase.isVerrouille()) {
        donnees[ligne][1] = "Verrouillé";
      }
      else if (documentVenteBase.getDateCreation() != null) {
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
      }
      
      // Total HT
      if (documentVenteBase.getTotalHT() != null) {
        donnees[ligne][2] = Constantes.formater(documentVenteBase.getTotalHT(), true);
      }
      
      // Total TTC
      if (documentVenteBase.getTotalTTC() != null) {
        donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
      }
      
      // Magasin
      Magasin magasin = modele.getListeMagasin().getMagasinParId(documentVenteBase.getIdMagasin());
      if (magasin != null) {
        donnees[ligne][4] = magasin.getId().getCode();
      }
      
      // Référence longue
      if (documentVenteBase.getReferenceLongue() != null) {
        donnees[ligne][5] = documentVenteBase.getReferenceLongue();
      }
      
      // Chantier
      if (documentVenteBase.getReferenceLongue() != null) {
        donnees[ligne][6] = documentVenteBase.getLibelleChantier();
      }
      
      // Mode d'expédition
      if (documentVenteBase.geIdModeExpedition() != null) {
        ModeExpedition modeExpedition = modele.getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
        if (modeExpedition != null) {
          donnees[ligne][7] = modeExpedition.getLibelle();
        }
      }
      
      // Direct usine
      if (documentVenteBase.isDirectUsine()) {
        donnees[ligne][8] = "Oui";
        if (documentVenteBase.isCommandeAchatGeneree()) {
          donnees[ligne][8] = "Achat généré";
        }
      }
      
      // Etat de l'extraction
      if (documentVenteBase.getEtatExtraction() != null) {
        donnees[ligne][9] = documentVenteBase.getEtatExtraction().getLibelle();
      }
    }
    tblCommandesEnCours.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexCommandeSelectionnee();
    if (index > -1) {
      tblCommandesEnCours.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblCommandesEnCours.clearSelection();
    }
  }
  
  /**
   * Rafraîchir la liste des bons.
   */
  private void rafraichirListeBon() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = modele.getModeleDocumentVenteParType();
    
    // Désactiver l'onglet bon s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeBon() == null
        || modeleDocumentVenteParType.getListeBon().isEmpty()) {
      tblLivraisonsEnCours.mettreAJourDonnees(null);
      tbpDocumentsEnCours.setEnabledAt(ONGLET_BONS, false);
      tbpDocumentsEnCours.setTitleAt(ONGLET_BONS, "Bons");
      scpLivraisonsEnCours.getVerticalScrollBar().setValue(0);
      scpLivraisonsEnCours.getViewport().reshape(0, 0, scpLivraisonsEnCours.getViewport().getWidth(), 0);
      return;
    }
    tbpDocumentsEnCours.setEnabledAt(ONGLET_BONS, true);
    tbpDocumentsEnCours.setTitleAt(ONGLET_BONS, "Bons (" + modeleDocumentVenteParType.getListeBon().size() + ")");
    
    // Renseigner les données du tableau
    ListeDocumentVenteBase listeDocumentVenteBase = modeleDocumentVenteParType.getListeBon();
    String[][] donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_BON.length];
    for (int ligne = 0; ligne < listeDocumentVenteBase.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(ligne);
      
      // Identifiant
      donnees[ligne][0] = documentVenteBase.getId().toString();
      
      // Date de création
      if (documentVenteBase.isVerrouille()) {
        donnees[ligne][1] = "Verrouillé";
      }
      else if (documentVenteBase.getDateCreation() != null) {
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
      }
      
      // Total HT
      if (documentVenteBase.getTotalHT() != null) {
        donnees[ligne][2] = Constantes.formater(documentVenteBase.getTotalHT(), true);
      }
      
      // Total TTC
      if (documentVenteBase.getTotalTTC() != null) {
        donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
      }
      
      // Magasin
      Magasin magasin = modele.getListeMagasin().getMagasinParId(documentVenteBase.getIdMagasin());
      if (magasin != null) {
        donnees[ligne][4] = magasin.getId().getCode();
      }
      
      // Référence longue
      if (documentVenteBase.getReferenceLongue() != null) {
        donnees[ligne][5] = documentVenteBase.getReferenceLongue();
      }
      
      // Chantier
      if (documentVenteBase.getReferenceLongue() != null) {
        donnees[ligne][6] = documentVenteBase.getLibelleChantier();
      }
      
      // Mode d'expédition
      if (documentVenteBase.geIdModeExpedition() != null) {
        ModeExpedition modeExpedition = modele.getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
        if (modeExpedition != null) {
          donnees[ligne][7] = modeExpedition.getLibelle();
        }
      }
      
      // Direct usine
      if (documentVenteBase.isDirectUsine()) {
        donnees[ligne][8] = "Oui";
        if (documentVenteBase.isCommandeAchatGeneree()) {
          donnees[ligne][8] = "Achat généré";
        }
      }
    }
    tblLivraisonsEnCours.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexBonSelectionne();
    if (index > -1) {
      tblLivraisonsEnCours.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblLivraisonsEnCours.clearSelection();
    }
  }
  
  /**
   * Rafraîchir la liste des factures.
   */
  private void rafraichirListeFacture() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = modele.getModeleDocumentVenteParType();
    
    // Désactiver l'onglet facture s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeFacture() == null
        || modeleDocumentVenteParType.getListeFacture().isEmpty()) {
      tblFacturesEnCours.mettreAJourDonnees(null);
      tbpDocumentsEnCours.setEnabledAt(ONGLET_FACTURES, false);
      tbpDocumentsEnCours.setTitleAt(ONGLET_FACTURES, "Factures");
      scpFacturesEnCours.getVerticalScrollBar().setValue(0);
      scpFacturesEnCours.getViewport().reshape(0, 0, scpFacturesEnCours.getViewport().getWidth(), 0);
      return;
    }
    tbpDocumentsEnCours.setEnabledAt(ONGLET_FACTURES, true);
    tbpDocumentsEnCours.setTitleAt(ONGLET_FACTURES, "Factures (" + modeleDocumentVenteParType.getListeFacture().size() + ")");
    
    // Renseigner les données du tableau
    ListeDocumentVenteBase listeDocumentVenteBase = modeleDocumentVenteParType.getListeFacture();
    String[][] donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_FACTURE.length];
    for (int ligne = 0; ligne < listeDocumentVenteBase.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(ligne);
      
      // Identifiant
      donnees[ligne][0] = documentVenteBase.getId().toString();
      
      // Date de création
      if (documentVenteBase.isVerrouille()) {
        donnees[ligne][1] = "Verrouillé";
      }
      else if (documentVenteBase.getDateCreation() != null) {
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
      }
      
      // Total HT
      if (documentVenteBase.getTotalHT() != null) {
        donnees[ligne][2] = Constantes.formater(documentVenteBase.getTotalHT(), true);
      }
      
      // Total TTC
      if (documentVenteBase.getTotalTTC() != null) {
        donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
      }
      
      // Magasin
      Magasin magasin = modele.getListeMagasin().getMagasinParId(documentVenteBase.getIdMagasin());
      if (magasin != null) {
        donnees[ligne][4] = magasin.getId().getCode();
      }
      
      // Référence longue
      donnees[ligne][5] = documentVenteBase.getReferenceLongue();
      
      // Mode d'expédition
      if (documentVenteBase.geIdModeExpedition() != null) {
        ModeExpedition modeExpedition = modele.getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
        if (modeExpedition != null) {
          donnees[ligne][6] = modeExpedition.getLibelle();
        }
      }
      
      // Direct usine
      if (documentVenteBase.isDirectUsine()) {
        donnees[ligne][7] = "Oui";
        if (documentVenteBase.isCommandeAchatGeneree()) {
          donnees[ligne][7] = "Achat généré";
        }
      }
    }
    tblFacturesEnCours.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexFactureSelectionnee();
    if (index > -1) {
      tblFacturesEnCours.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblFacturesEnCours.clearSelection();
    }
  }
  
  /**
   * Rafraichir l'aspect de l'écran en fonction du type de client (En compte / Comptant)
   */
  private void rafraichirBoutonRechercheDocumentVente() {
    boolean actif = false;
    if (modele.getClientCourant() != null && modele.getClientCourant().isExistant()) {
      actif = true;
    }
    
    btRechercheDocumentsClient.setEnabled(actif);
    btRechercheDocumentsClient.setVisible(actif);
  }
  
  /**
   * Rafraîchir le bouton "Créer devis".
   */
  private void rafraichirBoutonCreerDevis() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si la création de client comptant est impossible
    else if (!modele.getClientCourant().isExistant() && !modele.getClientCourant().isCreationPossibleClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_DEVIS, actif);
  }
  
  /**
   * Rafraîchir le bouton "Créer commande".
   */
  private void rafraichirBoutonCreerCommande() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si la création de client comptant est impossible
    else if (!modele.getClientCourant().isExistant() && !modele.getClientCourant().isCreationPossibleClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_COMMANDE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Créer bon".
   */
  private void rafraichirBoutonCreerBon() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est comptant
    else if (modele.getClientCourant().isClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_BON, actif);
  }
  
  /**
   * Rafraîchir le bouton "Créer facture".
   */
  private void rafraichirBoutonCreerFacture() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si la création de client comptant est impossible
    else if (!modele.getClientCourant().isExistant() && !modele.getClientCourant().isCreationPossibleClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est en compte
    else if (modele.getClientCourant().isClientEnCompte()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_FACTURE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Créer avoir".
   */
  private void rafraichirBoutonCreerAvoir() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si aucun bon ou facture n'est sélectionné
    else if (documentVenteBase == null || (!documentVenteBase.isBon() && !documentVenteBase.isFacture())) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver si le document est déjà un avoir
    else if (documentVenteBase.getTotalHT().compareTo(BigDecimal.ZERO) < 1) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREER_AVOIR, actif);
  }
  
  /**
   * Rafraîchir le bouton "Changer client".
   */
  private void rafraichirBoutonChangerClient() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si l'utilisateur n'a pas les droits nécessaires
    if (modele.getEtablissement() == null || !ManagerSessionClient.getInstance().verifierDroitGescom(modele.getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_CHANGEMENT_DE_TIERS_SUR_BON)) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getDocumentVenteEnCours() == null) {
      actif = false;
    }
    // Désactiver le bouton si le document en cours n'est pas en cours de création
    else if (!modele.getDocumentVenteEnCours().isEnCoursCreation()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CHANGER_CLIENT, actif);
  }
  
  /**
   * Rafraîchir le bouton "Modifier document".
   */
  private void rafraichirBoutonModifierDocument() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun document de ventes n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document est une facture
    else if (documentVenteBase.isFacture()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est entièrement extrait
    else if (documentVenteBase.getEtatExtraction() == null || documentVenteBase.getEtatExtraction().isComplete()) {
      actif = false;
    }
    // Désactiver le bouton si l'utilisateur n'a pas les droits de modification
    else if (documentVenteBase.isBon() && !modele.isUtilisateurADroitModificationCommande()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER_DOCUMENT, actif);
  }
  
  /**
   * Rafraîchir le bouton "Afficher document".
   */
  private void rafraichirBoutonAfficherDocument() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si aucun document de ventes n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est entièrement extrait
    else if (documentVenteBase.getEtatExtraction() == null || documentVenteBase.getEtatExtraction().isComplete()) {
      actif = false;
    }
    // Désactiver le bouton si l'utilisateur a les droits de modification
    else if (modele.isUtilisateurADroitModificationCommande()) {
      actif = false;
    }
    
    else if (snBarreBouton.getBouton(BOUTON_MODIFIER_DOCUMENT).isVisible()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT, actif);
  }
  
  /**
   * Rafraîchir le bouton "Consulter document".
   */
  private void rafraichirBoutonConsulterDocument() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun document de ventes n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes n'est pas entièrement extrait
    else if (documentVenteBase.getEtatExtraction() == null || !documentVenteBase.getEtatExtraction().isComplete()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CONSULTER_DOCUMENT, actif);
  }
  
  /**
   * Rafraîchir le bouton "Importer ProDevis".
   */
  private void rafraichirBoutonImporterProDevis() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si la création de client comptant est impossible
    else if (!modele.getClientCourant().isExistant() && !modele.getClientCourant().isCreationPossibleClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_IMPORTER_PRODEVIS, actif);
  }
  
  /**
   * Rafraîchir le bouton "Dupliquer devis".
   */
  private void rafraichirBoutonDupliquerDevis() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (documentVenteBase == null || !documentVenteBase.isDevis()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_DEVIS, actif);
  }
  
  /**
   * Rafraîchir le bouton "Dupliquer commande".
   */
  private void rafraichirBoutonDupliquerCommande() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (documentVenteBase == null || !documentVenteBase.isCommande()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_COMMANDE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Dupliquer bon".
   */
  private void rafraichirBoutonDupliquerBon() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (documentVenteBase == null || !documentVenteBase.isBon()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_BON, actif);
  }
  
  /**
   * Rafraîchir le bouton "Dupliquer facture".
   */
  private void rafraichirBoutonDupliquerFacture() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (documentVenteBase == null || !documentVenteBase.isFacture()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_FACTURE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Extraire commande".
   */
  private void rafraichirBoutonExtraireCommande() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (documentVenteBase == null || !documentVenteBase.isDevis()) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_COMMANDE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Extraire bon".
   */
  private void rafraichirBoutonExtraireBon() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est comptant
    else if (modele.getClientCourant().isClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis ou commande n'est sélectionné
    else if (documentVenteBase == null || (!documentVenteBase.isDevis() && !documentVenteBase.isCommande())) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_BON, actif);
  }
  
  /**
   * Rafraîchir le bouton "Extraire facture".
   */
  private void rafraichirBoutonExtraireFacture() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est en compte
    else if (modele.getClientCourant().isClientEnCompte()) {
      actif = false;
    }
    // Désactiver le bouton si le client est un prospect
    else if (modele.getClientCourant().isClientProspect()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (modele.getClientCourant().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (modele.isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis ou commande n'est sélectionné
    else if (documentVenteBase == null || (!documentVenteBase.isDevis() && !documentVenteBase.isCommande())) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_FACTURE, actif);
  }
  
  /**
   * Rafraîchir le bouton "Afficher document lié".
   */
  private void rafraichirBoutonAfficherDocumentLie() {
    boolean actif = true;
    
    // Désactiver le bouton si aucun client n'est sélectionné
    if (modele.getClientCourant() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!modele.getClientCourant().isExistant()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DOCUMENT_LIE, actif);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Barre des boutons
   * @param pSNBouton le bouton activé
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.annulerDocumentVente();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        modele.fermerEcran();
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_DOCUMENT)) {
        modele.modifierDocumentVente(true);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENT)) {
        modele.modifierDocumentVente(false);
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_DEVIS)) {
        modele.creerDevis();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_BON)) {
        modele.creerBon();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_COMMANDE)) {
        modele.creerCommande();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_FACTURE)) {
        modele.creerFacture();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_AVOIR)) {
        if (modele.getDocumentVenteBaseSelectionne() != null) {
          modele.creerAvoir(modele.getDocumentVenteBaseSelectionne().getId());
        }
      }
      else if (pSNBouton.isBouton(BOUTON_CHANGER_CLIENT)) {
        modele.changerClientDocument();
      }
      else if (pSNBouton.isBouton(BOUTON_IMPORTER_PRODEVIS)) {
        modele.importerDevisProDevis();
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_DEVIS)) {
        modele.dupliquerDocumentVente(EnumTypeDocumentVente.DEVIS);
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_COMMANDE)) {
        modele.dupliquerDocumentVente(EnumTypeDocumentVente.COMMANDE);
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_BON)) {
        modele.dupliquerDocumentVente(EnumTypeDocumentVente.BON);
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_FACTURE)) {
        modele.dupliquerDocumentVente(EnumTypeDocumentVente.FACTURE);
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_COMMANDE)) {
        if (tbpDocumentsEnCours.getSelectedIndex() == ONGLET_DEVIS) {
          modele.extraireDocumentVente(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_COMMANDE);
        }
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_BON)) {
        if (tbpDocumentsEnCours.getSelectedIndex() == ONGLET_DEVIS) {
          modele.extraireDocumentVente(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_BON);
        }
        else if (tbpDocumentsEnCours.getSelectedIndex() == ONGLET_COMMANDES) {
          modele.extraireDocumentVente(EnumOrigineDocumentVente.EXTRACTION_COMMANDE_EN_BON);
        }
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_FACTURE)) {
        if (tbpDocumentsEnCours.getSelectedIndex() == ONGLET_DEVIS) {
          modele.extraireDocumentVente(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_FACTURE);
        }
        else if (tbpDocumentsEnCours.getSelectedIndex() == ONGLET_COMMANDES) {
          modele.extraireDocumentVente(EnumOrigineDocumentVente.EXTRACTION_COMMANDE_EN_FACTURE);
        }
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENT_LIE)) {
        modele.afficherDocumentLie();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de l'établissement sélectionné.
   * @param e
   */
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierEtablissement(snEtablissement.getSelection());
        cbMagasin.setIdEtablissement(snEtablissement.getIdSelection());
        cbMagasin.charger(true);
        cbMagasin.setIdSelection(modele.getIdMagasin());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      modele.rafraichir();
    }
  }
  
  /**
   * Traiter la modification du client sélectionné.
   * @param e
   */
  private void snClientValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        if (snClient.getSelection() != null) {
          modele.modifierClient(snClient.getSelection().getId());
        }
        else {
          modele.modifierClient(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      snClient.initialiserRecherche();
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du nom du client.
   * @param e
   */
  private void tfNomFocusLost(FocusEvent e) {
    try {
      modele.modifierNomClient(tfNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du complément de nom du client.
   * @param e
   */
  private void tfComplementNomFocusLost(FocusEvent e) {
    try {
      modele.modifierComplementNomClient(tfComplementNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btBlocNotesMouseClicked(MouseEvent e) {
    try {
      modele.ouvrirPetitBlocNotesDocument();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du bloc note client.
   * @param e
   */
  private void ta_BlocnotesClientFocusLost(FocusEvent e) {
    try {
      modele.modifierBlocNoteClient(taBlocnotesClient.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de la rue du client.
   * @param e
   */
  private void tfRueFocusLost(FocusEvent e) {
    try {
      modele.modifierRueClient(tfRue.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de la localisation du client.
   * @param e
   */
  private void tfLocalisationFocusLost(FocusEvent e) {
    try {
      modele.modifierLocalisationClient(tfLocalisation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du code postal/commune sélectionné.
   * @param e
   */
  private void snCodePostalCommuneValueChanged(SNComposantEvent e) {
    try {
      if (!executerEvenements || modele.getClientCourant() == null || modele.getClientCourant().isClientEnCompte()) {
        return;
      }
      modele.modifierCodePostalCommune(snCodePostalCommune.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du numéro de téléphone du client.
   * @param e
   */
  private void tfTelephoneFocusLost(FocusEvent e) {
    try {
      modele.modifierTelephoneClient(tfTelephone.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de l'email du client.
   * @param e
   */
  private void tfEmailFocusLost(FocusEvent e) {
    try {
      modele.modifierMailClient(tfEmail.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du fax du client.
   * @param e
   */
  private void tfFaxFocusLost(FocusEvent e) {
    try {
      modele.modifierFaxClient(tfFax.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur le bouton permettant de lancer le programme d'encours comptable.
   * @param e
   */
  private void btEncoursComptableActionPerformed(ActionEvent e) {
    try {
      modele.lancerProgrammeEncoursComptable();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur le bouton de recherche de document de vente d'un client.
   * @param e
   */
  private void btRechercheDocumentsClientActionPerformed(ActionEvent e) {
    try {
      modele.rechercherDocumentVenteClient();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action sur le panel d'affichage des documents de vente.
   * @param e
   */
  private void tbpDocumentsEnCoursStateChanged(ChangeEvent e) {
    try {
      if (executerEvenements) {
        rafraichir();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la sélection d'un devis dans la liste.
   * @param e
   */
  private void tblDevisEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.selectionnerDevisClient(tblDevisEnCours.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur la liste des devis.
   * @param e
   */
  private void tblDevisEnCoursMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        modele.modifierDocumentVente(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter une action sur le scrollpane des devis.
   * @param e
   */
  private void scpDevisEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!executerEvenements || tblDevisEnCours == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpDevisEnCours.getViewport().getViewRect();
      int premiereLigne = tblDevisEnCours.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblDevisEnCours.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblDevisEnCours.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.modifierPlageDevis(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la sélection d'une commande dans la liste.
   * @param e
   */
  private void tblCommandeEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.selectionnerCommandeClient(tblCommandesEnCours.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur la liste des commandes.
   * @param e
   */
  private void tblCommandesEnCoursMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        modele.modifierDocumentVente(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter une action sur le scrollpane des commandes.
   * @param e
   */
  private void scpCommandesEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!executerEvenements || tblCommandesEnCours == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpCommandesEnCours.getViewport().getViewRect();
      int premiereLigne = tblCommandesEnCours.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblCommandesEnCours.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblCommandesEnCours.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.modifierPlageCommande(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la sélection d'un bon dans la liste.
   * @param e
   */
  private void tblLivraisonEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.selectionnerBonClient(tblLivraisonsEnCours.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur la liste des bons.
   * @param e
   */
  private void tblLivraisonsEnCoursMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        modele.modifierDocumentVente(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter une action sur le scrollpane des bons.
   * @param e
   */
  private void scpLivraisonsEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!executerEvenements || tblLivraisonsEnCours == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpLivraisonsEnCours.getViewport().getViewRect();
      int premiereLigne = tblLivraisonsEnCours.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblLivraisonsEnCours.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblLivraisonsEnCours.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.modifierPlageBon(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la sélection d'une facture dans la liste.
   * @param e
   */
  private void tblFactureEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.selectionnerFactureClient(tblFacturesEnCours.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action de clic sur la liste des factures.
   * @param e
   */
  private void tblFacturesEnCoursMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        modele.modifierDocumentVente(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter une action sur le scrollpane des factures.
   * @param e
   */
  private void scpFacturesEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!executerEvenements || tblFacturesEnCours == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpFacturesEnCours.getViewport().getViewRect();
      int premiereLigne = tblFacturesEnCours.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblFacturesEnCours.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblFacturesEnCours.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.modifierPlageFacture(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du magasin sélectionné.
   * @param e
   */
  private void cbMagasinValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierMagasin(cbMagasin.getIdSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du représentant sélectionné.
   * @param e
   */
  private void snRepresentantValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierRepresentant(snRepresentant.getIdSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la civilité sélectionnée.
   * @param e
   */
  private void snCiviliteValueChanged(SNComposantEvent e) {
    try {
      if (executerEvenements) {
        modele.modifierCiviliteClient(snCivilite.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes auto-générées (JFormDesigner)
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlSelectionClient = new JPanel();
    lbRechercheClient = new JLabel();
    snClient = new SNClient();
    lbMessageErreur = new JLabel();
    pnlContexte = new JPanel();
    lbEtablissement = new JLabel();
    snEtablissement = new SNEtablissement();
    lbMagasin = new JLabel();
    cbMagasin = new SNMagasin();
    lbVendeur = new JLabel();
    cbVendeur = new SNVendeur();
    lbRepresentant = new JLabel();
    snRepresentant = new SNRepresentant();
    pnlCoordonneesClient = new JPanel();
    lbCivilite = new JLabel();
    snCivilite = new SNCivilite();
    lbNom = new JLabel();
    tfNom = new SNTexte();
    lbComplementNom = new JLabel();
    tfComplementNom = new SNTexte();
    lbLocalisation = new JLabel();
    tfRue = new SNTexte();
    lbRue = new JLabel();
    tfLocalisation = new SNTexte();
    lbCodePostal = new JLabel();
    snCodePostalCommune = new SNCodePostalCommune();
    lbContact = new JLabel();
    tfContact = new SNTexte();
    lbFax = new JLabel();
    tfFax = new SNTexte();
    lbEmail = new JLabel();
    tfEmail = new SNTexte();
    lbTelephone = new JLabel();
    tfTelephone = new SNTexte();
    pnlEncoursClient = new JPanel();
    lbEncours = new JLabel();
    tfEncours = new SNTexte();
    lbEncoursCommande = new JLabel();
    tfEncoursCommande = new SNTexte();
    btEncoursComptable = new SNBoutonDetail();
    lbPlafond = new JLabel();
    tfPlafond = new SNTexte();
    lbResteEncours = new JLabel();
    tfResteEncours = new SNTexte();
    lbPlafondExceptionnel = new JLabel();
    tfPlafondExceptionnel = new SNTexte();
    lbDatePlafondExceptionnel = new JLabel();
    snDatePlafondExceptionnel = new SNDate();
    lbMessageEncours = new JLabel();
    scpBlocNoteClient = new JScrollPane();
    taBlocnotesClient = new JTextArea();
    pnlTitreListeDocument = new JPanel();
    sptDocumentEnCours = new SNLabelTitre();
    btRechercheDocumentsClient = new SNBoutonRecherche();
    tbpDocumentsEnCours = new JTabbedPane();
    pnlDevisEnCours = new JPanel();
    scpDevisEnCours = new JScrollPane();
    tblDevisEnCours = new NRiTable();
    pnlCommandesEnCours = new JPanel();
    scpCommandesEnCours = new JScrollPane();
    tblCommandesEnCours = new NRiTable();
    pnlLivraisonsEnCours = new JPanel();
    scpLivraisonsEnCours = new JScrollPane();
    tblLivraisonsEnCours = new NRiTable();
    pnlFacturesEnCours = new JPanel();
    scpFacturesEnCours = new JScrollPane();
    tblFacturesEnCours = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setPreferredSize(new Dimension(1240, 650));
    setMinimumSize(new Dimension(1240, 650));
    setBackground(new Color(239, 239, 222));
    setAutoscrolls(true);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setOpaque(false);
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlSelectionClient ========
      {
        pnlSelectionClient.setOpaque(false);
        pnlSelectionClient.setName("pnlSelectionClient");
        pnlSelectionClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSelectionClient.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlSelectionClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbRechercheClient ----
        lbRechercheClient.setText("Recherche client");
        lbRechercheClient.setFont(new Font("sansserif", Font.BOLD, 14));
        lbRechercheClient.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRechercheClient.setName("lbRechercheClient");
        pnlSelectionClient.add(lbRechercheClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snClient ----
        snClient.setName("snClient");
        snClient.addSNComposantListener(e -> snClientValueChanged(e));
        pnlSelectionClient.add(snClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMessageErreur ----
        lbMessageErreur.setText("Message");
        lbMessageErreur.setForeground(Color.red);
        lbMessageErreur.setFont(new Font("sansserif", Font.BOLD, 14));
        lbMessageErreur.setPreferredSize(new Dimension(300, 30));
        lbMessageErreur.setMinimumSize(new Dimension(300, 30));
        lbMessageErreur.setMaximumSize(new Dimension(300, 30));
        lbMessageErreur.setName("lbMessageErreur");
        pnlSelectionClient.add(lbMessageErreur, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.ABOVE_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSelectionClient,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlContexte ========
      {
        pnlContexte.setOpaque(false);
        pnlContexte.setMinimumSize(new Dimension(450, 135));
        pnlContexte.setPreferredSize(new Dimension(450, 135));
        pnlContexte.setName("pnlContexte");
        pnlContexte.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContexte.getLayout()).columnWidths = new int[] { 141, 294, 0 };
        ((GridBagLayout) pnlContexte.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContexte.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContexte.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEtablissement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEtablissement.setMinimumSize(new Dimension(150, 30));
        lbEtablissement.setPreferredSize(new Dimension(150, 30));
        lbEtablissement.setName("lbEtablissement");
        pnlContexte.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setName("snEtablissement");
        snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
        pnlContexte.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbMagasin.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMagasin.setName("lbMagasin");
        pnlContexte.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbMagasin ----
        cbMagasin.setName("cbMagasin");
        cbMagasin.addSNComposantListener(e -> cbMagasinValueChanged(e));
        pnlContexte.add(cbMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbVendeur ----
        lbVendeur.setText("Vendeur");
        lbVendeur.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbVendeur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbVendeur.setName("lbVendeur");
        pnlContexte.add(lbVendeur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbVendeur ----
        cbVendeur.setEnabled(false);
        cbVendeur.setName("cbVendeur");
        pnlContexte.add(cbVendeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRepresentant ----
        lbRepresentant.setText("Repr\u00e9sentant");
        lbRepresentant.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRepresentant.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbRepresentant.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRepresentant.setDisplayedMnemonicIndex(1);
        lbRepresentant.setName("lbRepresentant");
        pnlContexte.add(lbRepresentant, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snRepresentant ----
        snRepresentant.setName("snRepresentant");
        snRepresentant.addSNComposantListener(e -> snRepresentantValueChanged(e));
        pnlContexte.add(snRepresentant, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlContexte,
          new GridBagConstraints(1, 0, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCoordonneesClient ========
      {
        pnlCoordonneesClient.setBorder(new TitledBorder(null, "Coordonn\u00e9es client", TitledBorder.LEADING,
            TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
        pnlCoordonneesClient.setOpaque(false);
        pnlCoordonneesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlCoordonneesClient.setName("pnlCoordonneesClient");
        pnlCoordonneesClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 130, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCivilite ----
        lbCivilite.setText("Civilit\u00e9");
        lbCivilite.setPreferredSize(new Dimension(100, 30));
        lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCivilite.setMinimumSize(new Dimension(100, 30));
        lbCivilite.setMaximumSize(new Dimension(100, 30));
        lbCivilite.setName("lbCivilite");
        pnlCoordonneesClient.add(lbCivilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCivilite ----
        snCivilite.setName("snCivilite");
        snCivilite.addSNComposantListener(e -> snCiviliteValueChanged(e));
        pnlCoordonneesClient.add(snCivilite, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbNom ----
        lbNom.setText("Raison sociale");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setPreferredSize(new Dimension(100, 30));
        lbNom.setMinimumSize(new Dimension(100, 30));
        lbNom.setMaximumSize(new Dimension(100, 30));
        lbNom.setName("lbNom");
        pnlCoordonneesClient.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNom ----
        tfNom.setBackground(Color.white);
        tfNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNom.setEditable(false);
        tfNom.setMinimumSize(new Dimension(300, 30));
        tfNom.setPreferredSize(new Dimension(300, 30));
        tfNom.setEnabled(false);
        tfNom.setName("tfNom");
        tfNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfNom, new GridBagConstraints(1, 1, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 1, 1));
        
        // ---- lbComplementNom ----
        lbComplementNom.setText("Compl\u00e9ment");
        lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNom.setPreferredSize(new Dimension(100, 30));
        lbComplementNom.setMinimumSize(new Dimension(100, 30));
        lbComplementNom.setMaximumSize(new Dimension(100, 30));
        lbComplementNom.setName("lbComplementNom");
        pnlCoordonneesClient.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfComplementNom ----
        tfComplementNom.setBackground(Color.white);
        tfComplementNom.setMinimumSize(new Dimension(300, 30));
        tfComplementNom.setPreferredSize(new Dimension(300, 30));
        tfComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfComplementNom.setEnabled(false);
        tfComplementNom.setName("tfComplementNom");
        tfComplementNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfComplementNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfComplementNom, new GridBagConstraints(1, 2, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setInheritsPopupMenu(false);
        lbLocalisation.setMaximumSize(new Dimension(100, 30));
        lbLocalisation.setMinimumSize(new Dimension(100, 30));
        lbLocalisation.setPreferredSize(new Dimension(100, 30));
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonneesClient.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfRue ----
        tfRue.setBackground(Color.white);
        tfRue.setMinimumSize(new Dimension(300, 30));
        tfRue.setPreferredSize(new Dimension(300, 30));
        tfRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfRue.setEnabled(false);
        tfRue.setName("tfRue");
        tfRue.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfRueFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfRue, new GridBagConstraints(1, 3, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setName("lbRue");
        pnlCoordonneesClient.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLocalisation ----
        tfLocalisation.setBackground(Color.white);
        tfLocalisation.setMinimumSize(new Dimension(300, 30));
        tfLocalisation.setPreferredSize(new Dimension(300, 30));
        tfLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfLocalisation.setEnabled(false);
        tfLocalisation.setName("tfLocalisation");
        tfLocalisation.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLocalisationFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfLocalisation, new GridBagConstraints(1, 4, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCodePostal ----
        lbCodePostal.setText("Commune");
        lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal.setName("lbCodePostal");
        pnlCoordonneesClient.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCodePostalCommune ----
        snCodePostalCommune.setName("snCodePostalCommune");
        snCodePostalCommune.addSNComposantListener(e -> snCodePostalCommuneValueChanged(e));
        pnlCoordonneesClient.add(snCodePostalCommune, new GridBagConstraints(1, 5, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact");
        lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setDisplayedMnemonicIndex(2);
        lbContact.setName("lbContact");
        pnlCoordonneesClient.add(lbContact, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfContact ----
        tfContact.setBackground(Color.white);
        tfContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfContact.setMinimumSize(new Dimension(300, 30));
        tfContact.setPreferredSize(new Dimension(300, 30));
        tfContact.setEnabled(false);
        tfContact.setName("tfContact");
        pnlCoordonneesClient.add(tfContact, new GridBagConstraints(1, 6, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setMaximumSize(new Dimension(100, 30));
        lbFax.setMinimumSize(new Dimension(100, 30));
        lbFax.setPreferredSize(new Dimension(100, 30));
        lbFax.setName("lbFax");
        pnlCoordonneesClient.add(lbFax, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfFax.setPreferredSize(new Dimension(100, 30));
        tfFax.setMinimumSize(new Dimension(100, 30));
        tfFax.setEnabled(false);
        tfFax.setName("tfFax");
        tfFax.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfFaxFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfFax, new GridBagConstraints(5, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbEmail ----
        lbEmail.setText("Email");
        lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEmail.setName("lbEmail");
        pnlCoordonneesClient.add(lbEmail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEmail.setMinimumSize(new Dimension(250, 30));
        tfEmail.setPreferredSize(new Dimension(250, 30));
        tfEmail.setEnabled(false);
        tfEmail.setName("tfEmail");
        tfEmail.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfEmailFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfEmail, new GridBagConstraints(1, 7, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setInheritsPopupMenu(false);
        lbTelephone.setMaximumSize(new Dimension(100, 30));
        lbTelephone.setMinimumSize(new Dimension(100, 30));
        lbTelephone.setPreferredSize(new Dimension(100, 30));
        lbTelephone.setName("lbTelephone");
        pnlCoordonneesClient.add(lbTelephone, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfTelephone.setMinimumSize(new Dimension(100, 30));
        tfTelephone.setPreferredSize(new Dimension(100, 30));
        tfTelephone.setEnabled(false);
        tfTelephone.setName("tfTelephone");
        tfTelephone.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfTelephoneFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfTelephone, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesClient,
          new GridBagConstraints(0, 1, 1, 3, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlEncoursClient ========
      {
        pnlEncoursClient.setBorder(new TitledBorder(null, "Encours client", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
            new Font("sansserif", Font.BOLD, 14)));
        pnlEncoursClient.setOpaque(false);
        pnlEncoursClient.setName("pnlEncoursClient");
        pnlEncoursClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEncoursClient.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEncoursClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEncoursClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlEncoursClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEncours ----
        lbEncours.setText("Encours");
        lbEncours.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEncours.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbEncours.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEncours.setMinimumSize(new Dimension(150, 30));
        lbEncours.setPreferredSize(new Dimension(150, 30));
        lbEncours.setMaximumSize(new Dimension(150, 30));
        lbEncours.setName("lbEncours");
        pnlEncoursClient.add(lbEncours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEncours ----
        tfEncours.setHorizontalAlignment(SwingConstants.RIGHT);
        tfEncours.setEnabled(false);
        tfEncours.setMinimumSize(new Dimension(80, 30));
        tfEncours.setPreferredSize(new Dimension(80, 30));
        tfEncours.setMaximumSize(new Dimension(2147483647, 30));
        tfEncours.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEncours.setName("tfEncours");
        pnlEncoursClient.add(tfEncours, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbEncoursCommande ----
        lbEncoursCommande.setText("Encours commande");
        lbEncoursCommande.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEncoursCommande.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbEncoursCommande.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEncoursCommande.setMaximumSize(new Dimension(150, 30));
        lbEncoursCommande.setMinimumSize(new Dimension(150, 30));
        lbEncoursCommande.setPreferredSize(new Dimension(150, 30));
        lbEncoursCommande.setName("lbEncoursCommande");
        pnlEncoursClient.add(lbEncoursCommande, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEncoursCommande ----
        tfEncoursCommande.setHorizontalAlignment(SwingConstants.RIGHT);
        tfEncoursCommande.setEnabled(false);
        tfEncoursCommande.setMinimumSize(new Dimension(80, 30));
        tfEncoursCommande.setPreferredSize(new Dimension(80, 30));
        tfEncoursCommande.setMaximumSize(new Dimension(2147483647, 30));
        tfEncoursCommande.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEncoursCommande.setName("tfEncoursCommande");
        pnlEncoursClient.add(tfEncoursCommande, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- btEncoursComptable ----
        btEncoursComptable.setText("");
        btEncoursComptable.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        btEncoursComptable.setBorder(null);
        btEncoursComptable.setBackground(new Color(239, 239, 222));
        btEncoursComptable.setEnabled(false);
        btEncoursComptable.setName("btEncoursComptable");
        btEncoursComptable.addActionListener(e -> btEncoursComptableActionPerformed(e));
        pnlEncoursClient.add(btEncoursComptable, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPlafond ----
        lbPlafond.setText("Plafond");
        lbPlafond.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbPlafond.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbPlafond.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPlafond.setMinimumSize(new Dimension(150, 30));
        lbPlafond.setPreferredSize(new Dimension(150, 30));
        lbPlafond.setMaximumSize(new Dimension(150, 30));
        lbPlafond.setName("lbPlafond");
        pnlEncoursClient.add(lbPlafond, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPlafond ----
        tfPlafond.setForeground(Color.black);
        tfPlafond.setHorizontalAlignment(SwingConstants.RIGHT);
        tfPlafond.setEnabled(false);
        tfPlafond.setMinimumSize(new Dimension(80, 30));
        tfPlafond.setPreferredSize(new Dimension(80, 30));
        tfPlafond.setMaximumSize(new Dimension(2147483647, 30));
        tfPlafond.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfPlafond.setName("tfPlafond");
        pnlEncoursClient.add(tfPlafond, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbResteEncours ----
        lbResteEncours.setText("Reste");
        lbResteEncours.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbResteEncours.setForeground(Color.black);
        lbResteEncours.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbResteEncours.setHorizontalAlignment(SwingConstants.RIGHT);
        lbResteEncours.setMaximumSize(new Dimension(100, 30));
        lbResteEncours.setMinimumSize(new Dimension(100, 30));
        lbResteEncours.setPreferredSize(new Dimension(100, 30));
        lbResteEncours.setName("lbResteEncours");
        pnlEncoursClient.add(lbResteEncours, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfResteEncours ----
        tfResteEncours.setHorizontalAlignment(SwingConstants.RIGHT);
        tfResteEncours.setForeground(Color.black);
        tfResteEncours.setEnabled(false);
        tfResteEncours.setMinimumSize(new Dimension(80, 30));
        tfResteEncours.setPreferredSize(new Dimension(80, 30));
        tfResteEncours.setMaximumSize(new Dimension(2147483647, 30));
        tfResteEncours.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfResteEncours.setName("tfResteEncours");
        pnlEncoursClient.add(tfResteEncours, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPlafondExceptionnel ----
        lbPlafondExceptionnel.setText("Plafond exceptionnel");
        lbPlafondExceptionnel.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbPlafondExceptionnel.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbPlafondExceptionnel.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPlafondExceptionnel.setMaximumSize(new Dimension(150, 30));
        lbPlafondExceptionnel.setMinimumSize(new Dimension(150, 30));
        lbPlafondExceptionnel.setPreferredSize(new Dimension(150, 30));
        lbPlafondExceptionnel.setName("lbPlafondExceptionnel");
        pnlEncoursClient.add(lbPlafondExceptionnel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfPlafondExceptionnel ----
        tfPlafondExceptionnel.setHorizontalAlignment(SwingConstants.RIGHT);
        tfPlafondExceptionnel.setEnabled(false);
        tfPlafondExceptionnel.setMinimumSize(new Dimension(80, 30));
        tfPlafondExceptionnel.setPreferredSize(new Dimension(80, 30));
        tfPlafondExceptionnel.setMaximumSize(new Dimension(2147483647, 30));
        tfPlafondExceptionnel.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfPlafondExceptionnel.setName("tfPlafondExceptionnel");
        pnlEncoursClient.add(tfPlafondExceptionnel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbDatePlafondExceptionnel ----
        lbDatePlafondExceptionnel.setText("jusqu'au");
        lbDatePlafondExceptionnel.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbDatePlafondExceptionnel.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbDatePlafondExceptionnel.setHorizontalAlignment(SwingConstants.RIGHT);
        lbDatePlafondExceptionnel.setMinimumSize(new Dimension(100, 30));
        lbDatePlafondExceptionnel.setMaximumSize(new Dimension(100, 30));
        lbDatePlafondExceptionnel.setPreferredSize(new Dimension(100, 30));
        lbDatePlafondExceptionnel.setName("lbDatePlafondExceptionnel");
        pnlEncoursClient.add(lbDatePlafondExceptionnel, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDatePlafondExceptionnel ----
        snDatePlafondExceptionnel.setEnabled(false);
        snDatePlafondExceptionnel.setName("snDatePlafondExceptionnel");
        pnlEncoursClient.add(snDatePlafondExceptionnel, new GridBagConstraints(3, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMessageEncours ----
        lbMessageEncours.setText("Message");
        lbMessageEncours.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbMessageEncours.setForeground(Color.red);
        lbMessageEncours.setHorizontalTextPosition(SwingConstants.CENTER);
        lbMessageEncours.setHorizontalAlignment(SwingConstants.LEFT);
        lbMessageEncours.setPreferredSize(new Dimension(300, 30));
        lbMessageEncours.setMinimumSize(new Dimension(300, 30));
        lbMessageEncours.setName("lbMessageEncours");
        pnlEncoursClient.add(lbMessageEncours, new GridBagConstraints(0, 3, 5, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEncoursClient,
          new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpBlocNoteClient ========
      {
        scpBlocNoteClient.setPreferredSize(new Dimension(495, 40));
        scpBlocNoteClient.setMinimumSize(new Dimension(495, 40));
        scpBlocNoteClient.setName("scpBlocNoteClient");
        
        // ---- taBlocnotesClient ----
        taBlocnotesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        taBlocnotesClient.setLineWrap(true);
        taBlocnotesClient.setForeground(Color.red);
        taBlocnotesClient.setName("taBlocnotesClient");
        taBlocnotesClient.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            ta_BlocnotesClientFocusLost(e);
          }
        });
        scpBlocNoteClient.setViewportView(taBlocnotesClient);
      }
      pnlContenu.add(scpBlocNoteClient,
          new GridBagConstraints(1, 3, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTitreListeDocument ========
      {
        pnlTitreListeDocument.setOpaque(false);
        pnlTitreListeDocument.setName("pnlTitreListeDocument");
        pnlTitreListeDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTitreListeDocument.getLayout()).columnWidths = new int[] { 365, 0, 0 };
        ((GridBagLayout) pnlTitreListeDocument.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlTitreListeDocument.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTitreListeDocument.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- sptDocumentEnCours ----
        sptDocumentEnCours.setText("Documents en cours");
        sptDocumentEnCours.setName("sptDocumentEnCours");
        pnlTitreListeDocument.add(sptDocumentEnCours, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- btRechercheDocumentsClient ----
        btRechercheDocumentsClient.setToolTipText("Recherche  (ALT+R)");
        btRechercheDocumentsClient.setEnabled(false);
        btRechercheDocumentsClient.setName("btRechercheDocumentsClient");
        btRechercheDocumentsClient.addActionListener(e -> btRechercheDocumentsClientActionPerformed(e));
        pnlTitreListeDocument.add(btRechercheDocumentsClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTitreListeDocument, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== tbpDocumentsEnCours ========
      {
        tbpDocumentsEnCours.setFont(tbpDocumentsEnCours.getFont().deriveFont(tbpDocumentsEnCours.getFont().getSize() + 4f));
        tbpDocumentsEnCours.setMinimumSize(new Dimension(800, 200));
        tbpDocumentsEnCours.setPreferredSize(new Dimension(800, 200));
        tbpDocumentsEnCours.setBackground(new Color(239, 239, 222));
        tbpDocumentsEnCours.setOpaque(true);
        tbpDocumentsEnCours.setName("tbpDocumentsEnCours");
        tbpDocumentsEnCours.addChangeListener(e -> tbpDocumentsEnCoursStateChanged(e));
        
        // ======== pnlDevisEnCours ========
        {
          pnlDevisEnCours.setOpaque(false);
          pnlDevisEnCours.setName("pnlDevisEnCours");
          pnlDevisEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDevisEnCours.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDevisEnCours.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDevisEnCours.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDevisEnCours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== scpDevisEnCours ========
          {
            scpDevisEnCours.setBackground(Color.white);
            scpDevisEnCours.setOpaque(true);
            scpDevisEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            scpDevisEnCours.setPreferredSize(new Dimension(0, 0));
            scpDevisEnCours.setName("scpDevisEnCours");
            
            // ---- tblDevisEnCours ----
            tblDevisEnCours.setShowVerticalLines(true);
            tblDevisEnCours.setShowHorizontalLines(true);
            tblDevisEnCours.setBackground(Color.white);
            tblDevisEnCours.setRowHeight(20);
            tblDevisEnCours.setGridColor(new Color(204, 204, 204));
            tblDevisEnCours.setMinimumSize(new Dimension(1170, 650));
            tblDevisEnCours.setPreferredSize(new Dimension(1170, 30));
            tblDevisEnCours.setMaximumSize(new Dimension(2147483647, 2147483647));
            tblDevisEnCours.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblDevisEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblDevisEnCours.setPreferredScrollableViewportSize(new Dimension(2147483647, 2147483647));
            tblDevisEnCours.setOpaque(false);
            tblDevisEnCours.setName("tblDevisEnCours");
            tblDevisEnCours.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblDevisEnCoursMouseClicked(e);
              }
            });
            scpDevisEnCours.setViewportView(tblDevisEnCours);
          }
          pnlDevisEnCours.add(scpDevisEnCours, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
        }
        tbpDocumentsEnCours.addTab("Devis", pnlDevisEnCours);
        
        // ======== pnlCommandesEnCours ========
        {
          pnlCommandesEnCours.setOpaque(false);
          pnlCommandesEnCours.setName("pnlCommandesEnCours");
          pnlCommandesEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommandesEnCours.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommandesEnCours.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommandesEnCours.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCommandesEnCours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== scpCommandesEnCours ========
          {
            scpCommandesEnCours.setPreferredSize(new Dimension(0, 0));
            scpCommandesEnCours.setName("scpCommandesEnCours");
            
            // ---- tblCommandesEnCours ----
            tblCommandesEnCours.setShowVerticalLines(true);
            tblCommandesEnCours.setShowHorizontalLines(true);
            tblCommandesEnCours.setBackground(Color.white);
            tblCommandesEnCours.setOpaque(false);
            tblCommandesEnCours.setRowHeight(20);
            tblCommandesEnCours.setGridColor(new Color(204, 204, 204));
            tblCommandesEnCours.setPreferredSize(new Dimension(1170, 30));
            tblCommandesEnCours.setMinimumSize(new Dimension(1170, 1000));
            tblCommandesEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblCommandesEnCours.setName("tblCommandesEnCours");
            tblCommandesEnCours.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblCommandesEnCoursMouseClicked(e);
              }
            });
            scpCommandesEnCours.setViewportView(tblCommandesEnCours);
          }
          pnlCommandesEnCours.add(scpCommandesEnCours, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
        }
        tbpDocumentsEnCours.addTab("Commandes", pnlCommandesEnCours);
        tbpDocumentsEnCours.setBackgroundAt(1, Color.yellow);
        
        // ======== pnlLivraisonsEnCours ========
        {
          pnlLivraisonsEnCours.setOpaque(false);
          pnlLivraisonsEnCours.setName("pnlLivraisonsEnCours");
          pnlLivraisonsEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlLivraisonsEnCours.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlLivraisonsEnCours.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlLivraisonsEnCours.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlLivraisonsEnCours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== scpLivraisonsEnCours ========
          {
            scpLivraisonsEnCours.setPreferredSize(new Dimension(0, 0));
            scpLivraisonsEnCours.setName("scpLivraisonsEnCours");
            
            // ---- tblLivraisonsEnCours ----
            tblLivraisonsEnCours.setShowVerticalLines(true);
            tblLivraisonsEnCours.setShowHorizontalLines(true);
            tblLivraisonsEnCours.setBackground(Color.white);
            tblLivraisonsEnCours.setOpaque(false);
            tblLivraisonsEnCours.setRowHeight(20);
            tblLivraisonsEnCours.setGridColor(new Color(204, 204, 204));
            tblLivraisonsEnCours.setMinimumSize(new Dimension(1170, 120));
            tblLivraisonsEnCours.setPreferredSize(new Dimension(1170, 30));
            tblLivraisonsEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblLivraisonsEnCours.setName("tblLivraisonsEnCours");
            tblLivraisonsEnCours.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblLivraisonsEnCoursMouseClicked(e);
              }
            });
            scpLivraisonsEnCours.setViewportView(tblLivraisonsEnCours);
          }
          pnlLivraisonsEnCours.add(scpLivraisonsEnCours, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
        }
        tbpDocumentsEnCours.addTab("Bons", pnlLivraisonsEnCours);
        
        // ======== pnlFacturesEnCours ========
        {
          pnlFacturesEnCours.setOpaque(false);
          pnlFacturesEnCours.setName("pnlFacturesEnCours");
          pnlFacturesEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFacturesEnCours.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlFacturesEnCours.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlFacturesEnCours.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlFacturesEnCours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== scpFacturesEnCours ========
          {
            scpFacturesEnCours.setPreferredSize(new Dimension(0, 0));
            scpFacturesEnCours.setName("scpFacturesEnCours");
            
            // ---- tblFacturesEnCours ----
            tblFacturesEnCours.setShowVerticalLines(true);
            tblFacturesEnCours.setShowHorizontalLines(true);
            tblFacturesEnCours.setBackground(Color.white);
            tblFacturesEnCours.setOpaque(false);
            tblFacturesEnCours.setRowHeight(20);
            tblFacturesEnCours.setGridColor(new Color(204, 204, 204));
            tblFacturesEnCours.setMinimumSize(new Dimension(1170, 110));
            tblFacturesEnCours.setPreferredSize(new Dimension(1170, 30));
            tblFacturesEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblFacturesEnCours.setSelectionForeground(Color.white);
            tblFacturesEnCours.setName("tblFacturesEnCours");
            tblFacturesEnCours.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblFacturesEnCoursMouseClicked(e);
              }
            });
            scpFacturesEnCours.setViewportView(tblFacturesEnCours);
          }
          pnlFacturesEnCours.add(scpFacturesEnCours, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 0, 0, 0), 0, 0));
        }
        tbpDocumentsEnCours.addTab("Factures", pnlFacturesEnCours);
      }
      pnlContenu.add(tbpDocumentsEnCours,
          new GridBagConstraints(0, 5, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private JPanel pnlSelectionClient;
  private JLabel lbRechercheClient;
  private SNClient snClient;
  private JLabel lbMessageErreur;
  private JPanel pnlContexte;
  private JLabel lbEtablissement;
  private SNEtablissement snEtablissement;
  private JLabel lbMagasin;
  private SNMagasin cbMagasin;
  private JLabel lbVendeur;
  private SNVendeur cbVendeur;
  private JLabel lbRepresentant;
  private SNRepresentant snRepresentant;
  private JPanel pnlCoordonneesClient;
  private JLabel lbCivilite;
  private SNCivilite snCivilite;
  private JLabel lbNom;
  private SNTexte tfNom;
  private JLabel lbComplementNom;
  private SNTexte tfComplementNom;
  private JLabel lbLocalisation;
  private SNTexte tfRue;
  private JLabel lbRue;
  private SNTexte tfLocalisation;
  private JLabel lbCodePostal;
  private SNCodePostalCommune snCodePostalCommune;
  private JLabel lbContact;
  private SNTexte tfContact;
  private JLabel lbFax;
  private SNTexte tfFax;
  private JLabel lbEmail;
  private SNTexte tfEmail;
  private JLabel lbTelephone;
  private SNTexte tfTelephone;
  private JPanel pnlEncoursClient;
  private JLabel lbEncours;
  private SNTexte tfEncours;
  private JLabel lbEncoursCommande;
  private SNTexte tfEncoursCommande;
  private SNBoutonDetail btEncoursComptable;
  private JLabel lbPlafond;
  private SNTexte tfPlafond;
  private JLabel lbResteEncours;
  private SNTexte tfResteEncours;
  private JLabel lbPlafondExceptionnel;
  private SNTexte tfPlafondExceptionnel;
  private JLabel lbDatePlafondExceptionnel;
  private SNDate snDatePlafondExceptionnel;
  private JLabel lbMessageEncours;
  private JScrollPane scpBlocNoteClient;
  private JTextArea taBlocnotesClient;
  private JPanel pnlTitreListeDocument;
  private SNLabelTitre sptDocumentEnCours;
  private SNBoutonRecherche btRechercheDocumentsClient;
  private JTabbedPane tbpDocumentsEnCours;
  private JPanel pnlDevisEnCours;
  private JScrollPane scpDevisEnCours;
  private NRiTable tblDevisEnCours;
  private JPanel pnlCommandesEnCours;
  private JScrollPane scpCommandesEnCours;
  private NRiTable tblCommandesEnCours;
  private JPanel pnlLivraisonsEnCours;
  private JScrollPane scpLivraisonsEnCours;
  private NRiTable tblLivraisonsEnCours;
  private JPanel pnlFacturesEnCours;
  private JScrollPane scpFacturesEnCours;
  private NRiTable tblFacturesEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
