/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.JPanel;

import ri.serien.desktop.metier.gescom.achat.EnumCleVueAchat;
import ri.serien.desktop.metier.gescom.commun.commentaires.ModeleDetailCommentaire;
import ri.serien.desktop.metier.gescom.commun.commentaires.VueDetailCommentaire;
import ri.serien.desktop.metier.gescom.comptoir.articlespecial.ModeleArticleSpecial;
import ri.serien.desktop.metier.gescom.comptoir.articlespecial.VueArticleSpecial;
import ri.serien.desktop.metier.gescom.comptoir.autorisationdiffere.ModeleAutorisationDiffere;
import ri.serien.desktop.metier.gescom.comptoir.autorisationdiffere.VueAutorisationDiffere;
import ri.serien.desktop.metier.gescom.comptoir.autorisationventesousprv.ModeleAutorisationVenteSousPrv;
import ri.serien.desktop.metier.gescom.comptoir.autorisationventesousprv.VueAutorisationVenteSousPrv;
import ri.serien.desktop.metier.gescom.comptoir.blocnotes.ModeleBlocNoteDocument;
import ri.serien.desktop.metier.gescom.comptoir.blocnotes.VueBlocNoteDocument;
import ri.serien.desktop.metier.gescom.comptoir.changementclient.ModeleChangementClient;
import ri.serien.desktop.metier.gescom.comptoir.changementclient.VueChangementClient;
import ri.serien.desktop.metier.gescom.comptoir.changementclientdocument.ModeleChangementClientDocument;
import ri.serien.desktop.metier.gescom.comptoir.changementclientdocument.VueChangementClientDocument;
import ri.serien.desktop.metier.gescom.comptoir.commandeachatliee.ModeleCommandeAchatLiee;
import ri.serien.desktop.metier.gescom.comptoir.commandeachatliee.VueCommandeAchatLiee;
import ri.serien.desktop.metier.gescom.comptoir.correctionadresse.ModeleErreurAdresse;
import ri.serien.desktop.metier.gescom.comptoir.correctionadresse.VueErreurAdresse;
import ri.serien.desktop.metier.gescom.comptoir.creationavoir.ModeleCreerAvoir;
import ri.serien.desktop.metier.gescom.comptoir.creationavoir.VueCreerAvoir;
import ri.serien.desktop.metier.gescom.comptoir.detailcheque.ModeleDetailCheque;
import ri.serien.desktop.metier.gescom.comptoir.detailcheque.VueDetailCheque;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.EnumModeDetailLigneVente;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.VueDetailLigneArticle;
import ri.serien.desktop.metier.gescom.comptoir.duplicationdocument.ModeleDupliquerDocument;
import ri.serien.desktop.metier.gescom.comptoir.duplicationdocument.VueDupliquerDocument;
import ri.serien.desktop.metier.gescom.comptoir.editiondocument.EditionDocument;
import ri.serien.desktop.metier.gescom.comptoir.extractiondocument.ModeleExtractionDocument;
import ri.serien.desktop.metier.gescom.comptoir.extractiondocument.VueExtractionDocument;
import ri.serien.desktop.metier.gescom.comptoir.genererconditionventes.ModeleGenererConditionVentes;
import ri.serien.desktop.metier.gescom.comptoir.genererconditionventes.VueGenererConditionVentes;
import ri.serien.desktop.metier.gescom.comptoir.negociationglobale.ModeleNegociationGlobaleDocument;
import ri.serien.desktop.metier.gescom.comptoir.negociationglobale.VueNegociationGlobaleDocument;
import ri.serien.desktop.metier.gescom.comptoir.prodevis.ModeleImportationDevisProDevis;
import ri.serien.desktop.metier.gescom.comptoir.prodevis.VueImportationDevisProDevis;
import ri.serien.desktop.metier.gescom.comptoir.propositionsurconditionnement.ModeleProposerSurconditionnement;
import ri.serien.desktop.metier.gescom.comptoir.propositionsurconditionnement.VueProposerSurconditionnement;
import ri.serien.desktop.metier.gescom.comptoir.recherchedocumentvente.ModeleRechercheDocumentVente;
import ri.serien.desktop.metier.gescom.comptoir.recherchedocumentvente.VueRechercheDocumentVente;
import ri.serien.desktop.metier.gescom.comptoir.regroupement.ModeleRegroupementLignes;
import ri.serien.desktop.metier.gescom.comptoir.regroupement.VueRegroupementLignes;
import ri.serien.desktop.metier.gescom.comptoir.retourarticle.ModeleRetourArticle;
import ri.serien.desktop.metier.gescom.comptoir.retourarticle.VueRetourArticle;
import ri.serien.desktop.metier.gescom.comptoir.sipe.ModeleImportationPlanDePoseSipe;
import ri.serien.desktop.metier.gescom.comptoir.sipe.VueImportationPlanDePoseSipe;
import ri.serien.desktop.metier.gescom.comptoir.suppressionreglement.ModeleSuppressionReglement;
import ri.serien.desktop.metier.gescom.comptoir.suppressionreglement.VueSuppressionReglement;
import ri.serien.desktop.sessions.SessionJava;
import ri.serien.desktop.sessions.SessionRPG;
import ri.serien.libcommun.commun.EnumContexteMetier;
import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.menu.EnumPointMenu;
import ri.serien.libcommun.exploitation.menu.IdEnregistrementMneMnp;
import ri.serien.libcommun.exploitation.parametreavance.EnumCleParametreAvance;
import ri.serien.libcommun.exploitation.personnalisation.civilite.Civilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ParametreMenuAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.commun.adresse.EnumErreurAdresse;
import ri.serien.libcommun.gescom.commun.adressedocument.EnumCodeEnteteAdresseDocument;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.CriteresRechercheArticles;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleSpecial;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreDirectUsine;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.EnumTypeDecoupeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeImageClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.document.EnumModeEdition;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.representant.IdRepresentant;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.CritereResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.IdResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.IdModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.transporteur.IdTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.transporteur.Transporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.IdTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.TypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.TypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ZoneGeographique;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesDocument;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.commune.Commune;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatBonDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumEtatExtractionDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumModeDelivrement;
import ri.serien.libcommun.gescom.vente.document.EnumOrigineDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEdition;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEditionDevis;
import ri.serien.libcommun.gescom.vente.document.EnumTypeEditionFacture;
import ri.serien.libcommun.gescom.vente.document.EnumTypeQuantite;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.gescom.vente.document.Transport;
import ri.serien.libcommun.gescom.vente.ligne.ActionRegroupement;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceTauxRemise;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.ParametreLigneVente;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.gescom.vente.reglement.Acompte;
import ri.serien.libcommun.gescom.vente.reglement.AcompteParametre;
import ri.serien.libcommun.gescom.vente.reglement.EnumTypeReglement;
import ri.serien.libcommun.gescom.vente.reglement.IdReglement;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.reglement.Reglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.chaine.ListeGeneriquesString;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceCommun;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.attente.SwingWorkerAttente;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.dialoguestandard.information.ModeleDialogueInformation;
import ri.serien.libswing.composant.metier.exploitation.documentlie.listedocumentlie.SNDocumentLie;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.ModeleListeDocumentStocke;
import ri.serien.libswing.composant.metier.exploitation.documentstocke.listedocumentstocke.VueDocumentStocke;
import ri.serien.libswing.composant.metier.vente.adresselivraison.ModeleListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.adresselivraison.VueListeAdresseLivraisonChantier;
import ri.serien.libswing.composant.metier.vente.boncour.ajoutboncour.ModeleAjoutBonCour;
import ri.serien.libswing.composant.metier.vente.boncour.ajoutboncour.VueAjoutBonCour;
import ri.serien.libswing.composant.metier.vente.lot.ModeleLot;
import ri.serien.libswing.composant.metier.vente.lot.VueLot;
import ri.serien.libswing.moteur.interpreteur.EnvProgramClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle du comptoir.
 */
public class ModeleComptoir extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Comptoir";
  
  public static final int ONGLET_CLIENT = 0;
  public static final int ONGLET_LIVRAISONENLEVEMENT = 1;
  public static final int ONGLET_ARTICLE = 2;
  public static final int ONGLET_REGLEMENT = 3;
  public static final int ONGLET_EDITION = 4;
  
  public static final int ONGLET_ADRESSE_INDEFINI = -1;
  public static final int ONGLET_ADRESSE_FACTURATION = 0;
  public static final int ONGLET_ADRESSE_LIVRAISON = 1;
  
  public static final int ETAPE_CREATION_CLIENT = 0;
  public static final int ETAPE_RECHERCHE_CLIENT = 1;
  public static final int ETAPE_SAISIE_ONGLET_CLIENT = 2;
  public static final int ETAPE_SAISIE_ONGLET_LIVRAISON = 3;
  public static final int ETAPE_CREATION_BLOC_ADRESSE_LIVRAISON = 4;
  public static final int ETAPE_RECHERCHE_ARTICLE_STANDARD = 6;
  public static final int ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE = 7;
  public static final int ETAPE_RECHERCHE_ARTICLE_PALETTE = 8;
  public static final int ETAPE_RECHERCHE_ARTICLE_TRANSPORT = 9;
  public static final int ETAPE_SAISIE_ONGLET_REGLEMENT = 10;
  public static final int ETAPE_SAISIE_ONGLET_EDITION = 11;
  public static final int ETAPE_SAISIE_AVOIR = 12;
  
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_CLIENT_RECHERCHE_CLIENT = 1;
  public static final int FOCUS_CLIENT_CIVILITE = 2;
  public static final int FOCUS_ONGLET_COMMANDES = 3;
  public static final int FOCUS_LIVRAISON_REFERENCE_DOCUMENT = 11;
  public static final int FOCUS_LIVRAISON_CIVILITE = 12;
  public static final int FOCUS_LIVRAISON_NOM = 13;
  public static final int FOCUS_ARTICLES_RECHERCHE_ARTICLE = 21;
  public static final int FOCUS_ARTICLES_RESULTAT_RECHERCHE = 22;
  public static final int FOCUS_REGLEMENTS_MONTANT_REGLEMENT = 31;
  public static final int FOCUS_EDITION_ = 41;
  
  public static final String TYPEDOC_DEVIS_ENLEVEMENT = "Devis d'enlèvement";
  public static final String TYPEDOC_DEVIS_LIVRAISON = "Devis livraison";
  public static final String TYPEDOC_DEVIS_DIRECT_USINE = "Devis direct usine";
  public static final String TYPEDOC_FACTURE_ENLEVEMENT = "Facture d'enlèvement";
  public static final String TYPEDOC_FACTURE_LIVRAISON = "Facture livraison";
  public static final String TYPEDOC_FACTURE_DIRECT_USINE = "Facture direct usine";
  public static final String TYPEDOC_AVOIR = "Avoir";
  public static final String TYPEDOC_BON_ENLEVEMENT = "Bon d'enlèvement";
  public static final String TYPEDOC_BON_LIVRAISON = "Bon livraison";
  public static final String TYPEDOC_BON_DIRECT_USINE = "Bon direct usine";
  public static final String TYPEDOC_COMMANDE_ENLEVEMENT = "Commande d'enlèvement";
  public static final String TYPEDOC_COMMANDE_LIVRAISON = "Commande livraison";
  public static final String TYPEDOC_COMMANDE_DIRECT_USINE = "Commande direct usine";
  
  private static final Pattern PATTERN_SAISIE_ARTICLE = Pattern.compile("(-?[0-9.,]{0,})([a-zA-Z]{0,})");
  private static final Pattern PATTERN_SAISIE_ARTICLE_STANDARD = Pattern.compile("([0-9.,]{0,})([dDgGsShHnNiImMvVcC]{0,})");
  private static final Pattern PATTERN_SAISIE_ARTICLE_STANDARD_AVEC_P = Pattern.compile("([0-9.,]{0,})([pc])");
  private static final Pattern PATTERN_SAISIE_RETOUR_ARTICLE_STANDARD = Pattern.compile("(-[0-9.,]{0,})([dDgGsShHnNiImMvV]{0,})");
  private static final Pattern PATTERN_SAISIE_ARTICLE_PALETTE = Pattern.compile("([0-9]{0,})([dDgGsShHiI]{0,})");
  private static final Pattern PATTERN_SAISIE_RETOUR_ARTICLE_PALETTE = Pattern.compile("(-[0-9]+)");
  private static final Pattern PATTERN_SAISIE_ARTICLE_TRANSPORT = Pattern.compile("([0-9]+)");
  
  public static final int TABLE_RESULTAT_RECHERCHE_ARTICLES = 1;
  public static final int TABLE_LIGNES_DOCUMENT = 2;
  
  public static final String[][] GENERIQUESTRING_LIVRAISON_PARTIELLE = { { "", "Autorisée" }, { "X", "Non autorisée" } };
  
  // Variables
  protected JPanel parent = null;
  
  // Navigation
  private int etape = ETAPE_RECHERCHE_CLIENT;
  private int indiceOngletCourant = ONGLET_CLIENT;
  private int indiceOngletAccessible = indiceOngletCourant;
  private int indiceOngletAdresse = ONGLET_ADRESSE_INDEFINI;
  private int composantAyantLeFocus = FOCUS_CLIENT_RECHERCHE_CLIENT;
  
  // Listes
  private ListeEtablissement listeEtablissement = null;
  private ListeModeExpedition listeModeExpedition = null;
  private Map<IdEtablissement, ListeMagasin> listeMagasinParEtablissement = new HashMap<IdEtablissement, ListeMagasin>();
  private ListeMagasin listeMagasin = null;
  private ListeUnite listeUnite = null;
  private ListeCategorieClient listeCategorieClient = null;
  private ListeCivilite listeCivilite = null;
  private ListeTransporteur listeTransporteur = null;
  private ListeVendeur listeVendeur = null;
  private ListeCommune listeCommune = null;
  private ListeCommune listeCommuneLivraison = null;
  private ListeGroupe listeGroupes = null;
  private ListeFamille listeFamille = null;
  private ListeGeneriquesString listeLivraisonPartielle = null;
  private ListeZoneGeographique listeZoneGeographique = null;
  private ListeTypeFacturation listeTypeFacturation = null;
  private ListeFamille listeFamilleTransports = null;
  private ListeModeEdition listeModeEditionVente = ListeModeEdition.getInstanceModeEditionVente();
  private ListeTypeGratuit listeTypeGratuit = null;
  
  // Informations générales
  private boolean modeNegoce = true;
  private UtilisateurGescom utilisateurGescom = null;
  private Etablissement etablissement = null;
  private Date dateTraitement = null;
  private IdMagasin idMagasin = null;
  private IdRepresentant idRepresentant = null;
  
  // Informations document de ventes
  private DocumentVente documentVenteEnCours = null;
  private DocumentVente documentVenteAvantModifications = null;
  private OptionsValidation optionsValidation = null;
  private ModeleRegroupementLignes modeleRegroupement = null;
  private ModeleBlocNoteDocument modeleBlocNoteDocument = null;
  private DescriptionProDevis proDevisOrigine = null;
  private Chantier chantier = null;
  private OptionsEditionVente optionsEdition = null;
  private String emailDestinataireEdition = null;
  private String faxDestinataireEdition = null;
  private Contact contactMail = null;
  private Contact contactFax = null;
  private boolean isEnCoursSaisieAvoirTotal = false;
  private IdDocumentVente idDocumentOrigineAvoir = null;
  
  // Onglet client
  private String messageClient = "";
  private Client clientCourant = null;
  private ListeContact listeContactClient = null;
  private EncoursClient encoursClient = null;
  private EncoursClient encoursDocument = null;
  private ModeleDocumentVenteParType modeleDocumentVenteParType;
  private int indexArticleSelectionne = -1;
  private int indexLigneVenteSelectionnee = -1;
  private boolean activationOngletDocumentsClient = true;
  private InformationsExtraction informationsExtraction = null;
  private List<EnumErreurAdresse> listeErreurAdresse = new ArrayList<EnumErreurAdresse>();
  
  // Onglet article
  private String messageArticle = "";
  private boolean saisieArticlePossible = true;
  private String texteRecherche = "";
  private String dernierTexteRecherche = "";
  private EnumFiltreHorsGamme filtreHorsGamme = EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME;
  private CriteresRechercheArticles criteresRechercheArticles = null;
  private BigPercentage tauxRemiseForcee = null;
  private Integer numeroColonneTarifForcee = null;
  private ListeResultatRechercheArticle listeResultatRechercheArticle = null;
  private ListeResultatRechercheArticlePalette listeResultatRechercheArticlePalette = null;
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private ArrayList<IdLigneVente> listeLigneVenteASelectionner = new ArrayList<IdLigneVente>();
  private BigDecimal nombreDePalettesLies = BigDecimal.ZERO;
  private Boolean importationPlanPoseSipeActive = null;
  
  // Onglet règlement
  private Acompte acompte = null;
  private CalculReglement calculReglement = null;
  private boolean modePaiementComptant = false;
  private AcompteParametre acompteParametre = new AcompteParametre();
  private boolean changementClientAuReglement = false;
  private boolean forcerReglementComptantACauseEncours = false;
  private boolean venteSousPRV = false;
  private BigDecimal montantReglementDiffereAccepte = BigDecimal.ZERO;
  private ListeDocumentVenteBase listeFactureNonReglee = null;
  private ListeDocumentVenteBase listeAvoirUtilise = null;
  private boolean activerReglementComptant = false;
  private boolean isEnCoursModification = false;
  
  private boolean dateLivraisonSouhaiteeInvalide = false;
  private boolean dateLivraisonPrevueInvalide = false;
  
  // Onglet édition
  private ListeDocumentAchat listeDocumentAchatLie = null;
  
  /**
   * Constructeur.
   */
  public ModeleComptoir(SessionBase pSession) {
    super(pSession);
    getSession().getPanel().setName(TITRE);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes du framework MVC
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserDonnees() {
    listeModeEditionVente.setModeDebug(getSession().isModeDebug());
  }
  
  @Override
  public void chargerDonnees() {
    // Récupération des infos de l'utilisateur en gescom
    utilisateurGescom = chargerInformationsGescom(null);
    if (utilisateurGescom == null) {
      throw new MessageErreurException("Les données de l'utilisateur n'ont pas pu être chargées.");
    }
    
    // Charger la liste des établissements
    listeEtablissement = ListeEtablissement.charger(getSession().getIdSession());
    
    // Récupérer l'établissement courant
    etablissement = listeEtablissement.getEtablissementParId(utilisateurGescom.getIdEtablissementPrincipal());
    
    effacerVariables();
    
    // Charger la liste des magasins
    chargerListeMagasinToutEtablissement();
    
    // Chargement de toutes les données liées à l'établissement
    chargerDonneesLieesEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    
    // Récupération de la liste des modes d'expédition
    listeModeExpedition = ListeModeExpedition.charger(getSession().getIdSession());
    if (listeModeExpedition == null) {
      throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé.");
    }
    
    // Charger les types de gratuit
    if (listeTypeGratuit == null) {
      CriteresRechercheTypeGratuit criteres = new CriteresRechercheTypeGratuit();
      criteres.setTypeRecherche(CriteresRechercheTypeGratuit.RECHERCHE_TYPE_GRATUIT);
      listeTypeGratuit = ManagerServiceParametre.chargerListeTypeGratuit(getIdSession(), criteres);
    }
  }
  
  /**
   * Retourne la description générale (DG).
   */
  private Etablissement chargerListeMagasinToutEtablissement() {
    if (listeEtablissement == null) {
      throw new MessageErreurException("Aucune description générale n'est chargée.");
    }
    
    // Chargement des magasins par établissement
    for (Etablissement itrEtablissement : listeEtablissement) {
      ListeMagasin listeMagasin = new ListeMagasin();
      listeMagasin = listeMagasin.charger(getSession().getIdSession(), itrEtablissement.getId());
      listeMagasinParEtablissement.put(itrEtablissement.getId(), listeMagasin);
    }
    return etablissement;
  }
  
  /**
   * Retourne une liste de famille liées au transport
   */
  private ListeFamille chargerFamilleTranport(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    
    CritereFamille criteres = new CritereFamille();
    criteres.setIdEtablissement(pIdEtablissement);
    return ManagerServiceParametre.chargerListeFamille(getSession().getIdSession(), criteres);
  }
  
  /**
   * Controle la validité du code magasin associé à l'utilisateur.
   */
  private void controlerValiditeIdMagasin(IdMagasin pIdMagasin) {
    if (pIdMagasin == null) {
      throw new MessageErreurException("Il n'est pas possible d'utiliser le comptoir car le code magasin de l'utilisateur est à null.");
    }
    if (idMagasin.getCode().isEmpty()) {
      throw new MessageErreurException(
          "Il n'est pas possible d'utiliser le comptoir car l'utilisateur n'est pas associé à un code magasin. "
              + "Aller dans le menu gestion des sécurités pour configurer l'utilisateur.");
    }
    if (listeMagasin.getMagasinParId(pIdMagasin) == null) {
      throw new MessageErreurException(
          String.format("Il n'est pas possible d'utiliser le comptoir car le code magasin associé à " + "l'utilisateur n'existe pas. "
              + "Aller dans le menu gestion des sécurités pour configurer correctement l'utilisateur ou dans les paramètres de Série N "
              + "afin de créer le code magasin %s.", pIdMagasin));
    }
  }
  
  /**
   * Vérifier que l'utilisateur qui rentre dans le comptoir est bien associé à un vendeur valide.
   * Si ce n'est pas le cas, un message d'erreur est affiché et l'utilisateur ne peut pas accèder au comptoir.
   */
  private void controlerVendeur() {
    IdVendeur idVendeur = utilisateurGescom.getIdVendeur();
    if (idVendeur == null || idVendeur.getCode() == null || idVendeur.getCode().isEmpty()) {
      throw new MessageErreurException(
          "Il n'est pas possible d'utiliser le comptoir car l'utilisateur n'est pas associé à un code vendeur. "
              + "Aller dans le menu gestion des sécurités pour configurer l'utilisateur.");
    }
    if (!listeVendeur.isPresent(idVendeur)) {
      throw new MessageErreurException(
          String.format("Il n'est pas possible d'utiliser le comptoir car le code vendeur associé à l'utilisateur n'existe pas. "
              + "Aller dans le menu gestion des sécurités pour configurer correctement l'utilisateur ou dans les personnalisations "
              + "afin de créer le code vendeur %s.", idVendeur));
    }
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  private void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    // Déterminer le mode de calcul de prix du comptoir (négoce ou classique)
    modeNegoce = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS287);
    
    // Récupération de la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getSession().getIdSession(), pIdEtablissement);
    idMagasin = utilisateurGescom.getIdMagasin();
    controlerValiditeIdMagasin(idMagasin);
    
    // Récupération de la liste des vendeurs
    listeVendeur = new ListeVendeur();
    listeVendeur = listeVendeur.charger(getSession().getIdSession(), pIdEtablissement);
    controlerVendeur();
    
    // Récupération de la liste des types de facturation
    listeTypeFacturation = new ListeTypeFacturation();
    listeTypeFacturation = listeTypeFacturation.charger(getSession().getIdSession(), pIdEtablissement);
    
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
    
    // Récupération de la liste des catégories clients
    listeCategorieClient = new ListeCategorieClient();
    listeCategorieClient = listeCategorieClient.charger(getSession().getIdSession(), pIdEtablissement);
    
    // Récupération de la liste des transporteurs
    listeTransporteur = new ListeTransporteur();
    listeTransporteur = listeTransporteur.charger(getSession().getIdSession(), pIdEtablissement);
    
    // Récupération de la liste pour les livraisons patielles
    listeLivraisonPartielle = new ListeGeneriquesString();
    listeLivraisonPartielle.chargerGeneriqueString(GENERIQUESTRING_LIVRAISON_PARTIELLE);
    
    // Récupération du code famille pour les articles transports
    listeFamilleTransports = chargerFamilleTranport(pIdEtablissement);
    
    // Récupération de la liste des groupes
    listeGroupes = new ListeGroupe();
    listeGroupes = listeGroupes.charger(getSession().getIdSession(), pIdEtablissement);
    
    // Récupération de la liste des familles
    listeFamille = new ListeFamille();
    listeFamille = listeFamille.charger(getSession().getIdSession(), pIdEtablissement);
    
    // Récupération de la zone géographique
    listeZoneGeographique = new ListeZoneGeographique();
    listeZoneGeographique = listeZoneGeographique.charger(getSession().getIdSession(), pIdEtablissement);
  }
  
  /**
   * Retourne les informations gescom de l'utilisateur.
   */
  private UtilisateurGescom chargerInformationsGescom(IdEtablissement pIdEtablissement) {
    UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
        ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    // Dans le cas où l'utilisateur est chargé
    if (utilisateurGescom != null) {
      // S'il n'y a pas d'établissement par défaut définit pour l'utilisateur, on envoie un message d'erreur à l'utilisateur
      if (utilisateurGescom.getIdEtablissementPrincipal() == null) {
        throw new MessageErreurException("L'utilisateur n'a pas un établissement définit par défaut.");
      }
      ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    }
    return utilisateurGescom;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes communes à tous les onglets
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialise les variables du modèle.
   */
  public void effacerVariables() {
    effacerVariablesNavigation();
    effacerVariablesClient();
    effacerVariablesDocument();
  }
  
  /**
   * Initialise les variables de navigation.
   */
  private void effacerVariablesNavigation() {
    etape = ETAPE_RECHERCHE_CLIENT;
    composantAyantLeFocus = FOCUS_CLIENT_RECHERCHE_CLIENT;
    indiceOngletCourant = ONGLET_CLIENT;
    indiceOngletAccessible = ONGLET_CLIENT;
    indiceOngletAdresse = ONGLET_ADRESSE_INDEFINI;
  }
  
  /**
   * Initialise les variables du client.
   */
  private void effacerVariablesClient() {
    messageClient = "";
    activationOngletDocumentsClient = true;
    clientCourant = new Client(IdClient.getInstanceAvecCreationId(etablissement.getId()));
    clientCourant.setTypeCompteClient(EnumTypeCompteClient.COMPTANT);
    clientCourant.setTypeImageClient(EnumTypeImageClient.PARTICULIER);
    clientCourant.setFactureEnTTC(true);
    listeContactClient = null;
    encoursClient = null;
    contactMail = null;
    contactFax = null;
    emailDestinataireEdition = null;
    faxDestinataireEdition = null;
    
    modeleDocumentVenteParType = null;
    listeFactureNonReglee = null;
    listeAvoirUtilise = null;
    indexPremiereLigneAffichee = 0;
    
    idRepresentant = null;
    modePaiementComptant = false;
    if (acompte == null) {
      acompteParametre = new AcompteParametre();
    }
    else {
      acompteParametre.effacerVariables();
    }
    listeCommune = null;
    listeCommuneLivraison = null;
    
    saisieArticlePossible = true;
  }
  
  /**
   * Initialise les variables du document.
   */
  private void effacerVariablesDocument() {
    dateTraitement = etablissement.getDateTraitement();
    documentVenteEnCours = null;
    documentVenteAvantModifications = null;
    informationsExtraction = null;
    effacerListeArticleResultatRecherche();
    tauxRemiseForcee = null;
    numeroColonneTarifForcee = null;
    optionsValidation = null;
    acompte = null;
    calculReglement = null;
    dernierTexteRecherche = "";
    chantier = null;
    optionsEdition = new OptionsEditionVente();
    listeDocumentAchatLie = null;
    proDevisOrigine = null;
    indexArticleSelectionne = -1;
    indexLigneVenteSelectionnee = -1;
    
    texteRecherche = "";
    messageArticle = "";
    encoursDocument = null;
    
    changementClientAuReglement = false;
    forcerReglementComptantACauseEncours = false;
    activerReglementComptant = false;
    montantReglementDiffereAccepte = BigDecimal.ZERO;
    listeLigneVenteASelectionner.clear();
    isEnCoursModification = false;
    
    filtreHorsGamme = EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME;
    isEnCoursSaisieAvoirTotal = false;
    
    // Initialisation de base des critères de recherche des articles
    criteresRechercheArticles = new CriteresRechercheArticles();
    criteresRechercheArticles.setTypeRecherche(CriteresRechercheArticles.RECHERCHE_COMPTOIR);
    
    // Initialise par défaut les modes d'édition pour les documents de ventes
    listeModeEditionVente.reinitialiserValeurParDefaut();
    if (clientCourant != null) {
      // Initialisation de l'email
      modifierEmailDestinataire(clientCourant.getContactPrincipal(), false);
      // Initialisation du numéro de fax
      contactFax = clientCourant.getContactPrincipal();
      modifierFaxDestinataire(clientCourant.getContactPrincipal(), false);
    }
  }
  
  /**
   * Efface le contenu du résultat de la recherche articles.
   * Cela efface le contenu du tableau et les données correspondantes : l'index de la première ligne affichée, l'index de la ligne
   * sélectionnée le message d'erreur correspondant à la dernière recherche. Par contre, cela ne doit pas effacer le texte recherché
   * car celui-ci n'est pas lié à la dernière recherche.
   */
  private void effacerListeArticleResultatRecherche() {
    listeResultatRechercheArticle = null;
    listeResultatRechercheArticlePalette = null;
    indexPremiereLigneAffichee = 0;
    indexArticleSelectionne = -1;
    messageArticle = "";
  }
  
  /**
   * Ouvre la fenêtre du petit bloc notes document.
   */
  public void ouvrirPetitBlocNotesDocument() {
    ModeleBlocNoteDocument modele = new ModeleBlocNoteDocument(getSession(), this, documentVenteEnCours);
    VueBlocNoteDocument vue = new VueBlocNoteDocument(modele);
    vue.afficher();
    rafraichir();
  }
  
  /**
   * Initialise la variable du diagramme d'état.
   */
  private void setEtape(int pEtape) {
    etape = pEtape;
    switch (etape) {
      case ETAPE_RECHERCHE_CLIENT:
        setComposantAyantLeFocus(FOCUS_CLIENT_RECHERCHE_CLIENT);
        break;
      case ETAPE_CREATION_CLIENT:
        setComposantAyantLeFocus(FOCUS_CLIENT_CIVILITE);
        initialiserIdRepresentantParDefaut();
        break;
      case ETAPE_SAISIE_ONGLET_CLIENT:
        setComposantAyantLeFocus(FOCUS_NON_INDIQUE);
        break;
      case ETAPE_SAISIE_ONGLET_LIVRAISON:
        setComposantAyantLeFocus(FOCUS_NON_INDIQUE);
        break;
      case ETAPE_CREATION_BLOC_ADRESSE_LIVRAISON:
        if (clientCourant != null && clientCourant.isClientComptant()) {
          setComposantAyantLeFocus(FOCUS_LIVRAISON_CIVILITE);
        }
        else {
          setComposantAyantLeFocus(FOCUS_LIVRAISON_NOM);
        }
        break;
      case ETAPE_RECHERCHE_ARTICLE_STANDARD:
        setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
        break;
      case ETAPE_SAISIE_ONGLET_REGLEMENT:
        setComposantAyantLeFocus(FOCUS_REGLEMENTS_MONTANT_REGLEMENT);
        break;
    }
  }
  
  /**
   * Modifier l'onglet courant.
   */
  public void changerOnglet(int pIndiceOnglet) {
    // Vérifier si l'onglet a changé
    if (indiceOngletCourant == pIndiceOnglet) {
      return;
    }
    
    // Activer l'onglet souhaité
    setIndiceOnglet(pIndiceOnglet);
    
    // Définir l'étape de saisie en focntion de l'onglet courant
    switch (indiceOngletCourant) {
      case ONGLET_CLIENT:
        setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
        break;
      case ONGLET_LIVRAISONENLEVEMENT:
        setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
        break;
      case ONGLET_ARTICLE:
        setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
        break;
      case ONGLET_REGLEMENT:
        setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
        break;
      case ONGLET_EDITION:
        setEtape(ETAPE_SAISIE_ONGLET_EDITION);
        break;
    }
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Met à jour l'indice de l'onglet courant.
   */
  public void setIndiceOnglet(int pIndiceOnglet) {
    // Vérifier si l'onglet a changé
    if (indiceOngletCourant == pIndiceOnglet) {
      return;
    }
    
    // Modifier l'indice
    indiceOngletCourant = pIndiceOnglet;
    if (indiceOngletAccessible < indiceOngletCourant) {
      indiceOngletAccessible = indiceOngletCourant;
    }
    
    // Sauver et recharger le document de ventes si on arrive sur l'onglet article
    // Pour rafraîchir les informations mises à jour par des programmes RPG
    if (indiceOngletCourant == ONGLET_ARTICLE && documentVenteEnCours.isModifiable()) {
      sauverEnteteDocumentVente(documentVenteEnCours);
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    }
    else if (indiceOngletCourant == ONGLET_REGLEMENT) {
      chargerOngletReglement();
    }
    else if (indiceOngletCourant == ONGLET_EDITION) {
      chargerOngletEdition();
    }
  }
  
  /**
   * Met à jour l'indice de l'onglet courant et l'indice de l'onglet accessible.
   */
  public void setIndiceOnglet(int pIndiceOnglet, int pIndiceOngletAccessible) {
    indiceOngletCourant = pIndiceOnglet;
    indiceOngletAccessible = pIndiceOngletAccessible;
  }
  
  /**
   * Fermeture de la getSession() panel.
   */
  public void fermerEcran() {
    // On met le document en attente avant de sortir car sinon il est vérouillé
    if (documentVenteEnCours != null) {
      annulerDocumentVente();
    }
    // On ferme l'écran dans le cas où le document est à null c'est à dire que l'on a annuler le document)
    if (documentVenteEnCours == null) {
      getSession().fermerEcran();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Document de ventes
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Création d'un document.
   */
  public DocumentVente creerDocumentVente(DocumentVente pDocumentVente) {
    // Vérifier si le document de ventes existe déjà
    if (pDocumentVente.isExistant()) {
      throw new MessageErreurException("Impossible de créer le document de ventes car il existe déjà.");
    }
    
    // Créer le document
    IdDocumentVente idDocumentVente =
        ManagerServiceDocumentVente.creerDocumentVente(getSession().getIdSession(), pDocumentVente, dateTraitement);
    pDocumentVente.setId(idDocumentVente);
    
    // On lit l'entête du document qui vient d'être créé afin que le document soit bien à jour (par exemple le champ "ttc")
    return chargerDocumentVenteAvecConservationType(pDocumentVente);
  }
  
  /**
   * Charger toutes les informations d'un document de ventes.
   */
  private DocumentVente chargerDocumentVente(IdDocumentVente pIdDocumentVente) {
    // Charger l'entête du document de ventes
    DocumentVente documentVente = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getSession().getIdSession(), pIdDocumentVente);
    
    // Si modification autorisée
    if (isUtilisateurADroitModificationCommande() && isEnCoursModification && !documentVente.isFacture()) {
      documentVente.setModifiable(true);
    }
    
    // Charger le bloc-notes
    documentVente
        .setPetitBlocNote(ManagerServiceDocumentVente.chargerPetitBlocNoteDocumentVente(getSession().getIdSession(), pIdDocumentVente));
    
    // Charger le chantier associé au document de ventes
    if (documentVente.isPourChantier()) {
      chantier = ManagerServiceClient.chargerChantier(getIdSession(), documentVente.getIdChantier());
    }
    
    // Charger le règlement sauf dans le cas d'un devis
    if (!documentVente.isDevis()) {
      ListeReglement listeReglement =
          ManagerServiceDocumentVente.chargerListeReglement(getSession().getIdSession(), documentVente.getId(), dateTraitement);
      documentVente.setListeReglement(listeReglement);
      if (listeReglement != null) {
        listeReglement.initialiserInfoDocument(documentVente);
      }
    }
    
    // Charger les lignes de ventes
    CritereLigneVente critereLigneVente = new CritereLigneVente();
    ListeLigneVente listeLigneVente = ManagerServiceDocumentVente.chargerListeLigneVente(getSession().getIdSession(), pIdDocumentVente,
        critereLigneVente, documentVente.isTTC());
    
    // Ajouter les lignes au document de ventes
    for (LigneVente ligneVente : listeLigneVente) {
      // Stocker la ligne dans la liste qui va bien (en fonction de son état)
      if (ligneVente.isAnnulee()) {
        documentVente.getListeLigneVenteSupprimee().add(ligneVente);
      }
      else {
        // Gérer les articles ajoutés dans un document de vente où l'on a déjà fait une extraction partielle
        if (Constantes.equals(documentVente.getEtatExtraction(), EnumEtatExtractionDocumentVente.PARTIELLE)) {
          // Mettre à jour la quantité d'origine pour les articles ajoutés
          if (Constantes.equals(ligneVente.getQuantiteDejaExtraite(), BigDecimal.ZERO)) {
            ligneVente.setQuantiteDocumentOrigine(ligneVente.getQuantiteUV());
          }
          else {
            BigDecimal quantiteOrigine = ligneVente.getQuantiteUV().add(ligneVente.getQuantiteDejaExtraite());
            ligneVente.setQuantiteDocumentOrigine(quantiteOrigine);
          }
        }
        documentVente.getListeLigneVente().add(ligneVente);
        
        // Initialiser le tarif dans le prix de vente de chaque ligne
        // Dans le cas d'un retour palette on n'initialise pas le tarif article car il peut y avoir des tarifs différents car on
        // applique le tarif de déconsignation qui avait cours au moment de l'achat de la palette.
        if (!ligneVente.isLigneCommentaire() && !ligneVente.isRetourPalette()) {
          if (clientCourant != null) {
            ligneVente.setClientTTC(clientCourant.isFactureEnTTC());
          }
        }
      }
    }
    
    // Charger les regroupements
    documentVente.actualiserRegroupement(getSession().getIdSession());
    
    // Conservation de l'état et du total TTC du document au moment de son chargement
    if (documentVente.isEnCoursCreation()) {
      documentVente.setEtatOrigine(documentVente.getEtat());
      documentVente.setTotalTTCOrigine(documentVente.getTotalTTC());
      documentVente.setTotalTTCPrecedent(documentVente.getTotalTTC());
    }
    
    return documentVente;
  }
  
  /**
   * Recharger l'entête du document de ventes.
   * C'est utilisé par exemple après modification d'une ligne de ventes.
   * Seul l'entête du document de ventes est rechargé. Les lignes ne le sont pas pour l'instant mais il faudrait améliroer cela.
   */
  private DocumentVente chargerDocumentVenteAvecConservationType(DocumentVente pDocumentVente) {
    // Sauvegarder le type pour ne pas l'écraser à la relecture si on est en création de bon ou facture
    EnumTypeDocumentVente typeDocumentVente = pDocumentVente.getTypeDocumentVente();
    
    // On relit le bon afin de récupérer les informations à rafraichir comme les montants
    pDocumentVente = chargerDocumentVente(pDocumentVente.getId());
    
    // Remettre le type si sauvegardé
    pDocumentVente.setTypeDocumentVente(typeDocumentVente);
    return pDocumentVente;
  }
  
  /**
   * Modifie les informations du document (entête + adresse).
   */
  private void sauverEnteteDocumentVente(DocumentVente pDocumentVente) {
    if (!documentVenteEnCours.isModifiable()) {
      return;
    }
    
    // Vérifier si le document de ventes existe déjà
    if (!pDocumentVente.isExistant()) {
      throw new MessageErreurException("Impossible de sauver le document de ventes car il n'existe pas.");
    }
    
    // Vérifier si le document n'est pas modifiable
    if (!pDocumentVente.isModifiable()) {
      throw new MessageErreurException("Impossible de sauver le document de ventes car il n'est plus dans un état modifiable.");
    }
    
    // Sauver l'entête document de ventes
    ManagerServiceDocumentVente.sauverEnteteDocumentVente(getSession().getIdSession(), pDocumentVente, dateTraitement);
    
    // Sauver les adresses du document de ventes
    ManagerServiceDocumentVente.sauverAdresseDocumentVente(getSession().getIdSession(), pDocumentVente.getId(),
        pDocumentVente.getAdresseFacturation(), pDocumentVente.getTransport().getAdresseLivraison());
    
    // Sauver le mémo du document de ventes
    if (pDocumentVente.getMemoLivraisonEnlevement() != null) {
      pDocumentVente.getMemoLivraisonEnlevement().sauver(getSession().getIdSession());
    }
    
    // Sauver le bloc-note
    if (pDocumentVente.getPetitBlocNote() != null) {
      ManagerServiceDocumentVente.sauverPetitBlocNoteDocumentVente(getIdSession(), pDocumentVente.getId(),
          pDocumentVente.getPetitBlocNote());
    }
    
    // Chiffrer le documents de ventes pour recalculer les totaux
    ManagerServiceDocumentVente.chiffrerDocumentVente(getSession().getIdSession(), pDocumentVente.getId(), true);
    
    try {
      // Contrôle le document
      controlerDocument(true);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
  }
  
  /**
   * Annuler la saisie d'un document.
   */
  public void annulerDocumentVente() {
    // Vérification que le document de ventes est en cours
    if (documentVenteEnCours != null) {
      // Dans le cas d'un document non modifiable aucun traitement n'est effectué
      if (!documentVenteEnCours.isModifiable()) {
        // Effacer les données
        clientCourant = null;
        effacerVariables();
        rafraichir();
        return;
      }
      
      // Tester si un document est en cours de saisie
      if (documentVenteEnCours.isExistant()) {
        // Annuler le document s'il est en cours de création
        if (documentVenteEnCours.isEnCoursCreation()) {
          if (!DialogueConfirmation.afficher(getSession(), "Annulation de la création du document",
              "Vous avez demandé l'annulation du document en cours de création.\n Confirmez-vous cette annulation ?")) {
            return;
          }
          // Supprimer les éventuels bons de cour
          ListeLigneVente listeLigneBonCourASupprimer = new ListeLigneVente();
          for (LigneVente ligne : documentVenteEnCours.getListeLigneVente()) {
            if (ligne.isLigneBonCour()) {
              listeLigneBonCourASupprimer.add(ligne);
            }
          }
          supprimerListeLigneVente(listeLigneBonCourASupprimer);
          ManagerServiceDocumentVente.annulerDocumentVente(getSession().getIdSession(), documentVenteEnCours.getId());
        }
        // Mettre en attente le document s'il existait auparavant
        else if (documentVenteEnCours.isModifiable() && !documentVenteEnCours.isFacture()) {
          if (documentVenteAvantModifications == null || !documentVenteAvantModifications.equalsComplet(documentVenteEnCours)) {
            if (!DialogueConfirmation.afficher(getSession(), "Mise en attente du document",
                "Vous souhaitez quitter le document en cours ?\n Sélectionnez oui pour quitter et enregistrer les modifications. "
                    + "\nSélectionnez non pour revenir en modification sur le document.")) {
              return;
            }
          }
          else {
            try {
              controlerDocument(false);
              rafraichir();
            }
            catch (Exception e) {
              throw new MessageErreurException(e, "");
            }
            // Permet de rafraichir les données correctement dans le cas où une exception est déclenchée
            finally {
              rafraichir();
            }
          }
          
          if (optionsValidation != null) {
            optionsValidation.setCodePropose(OptionsValidation.OPTION_ATTENTE);
            documentVenteEnCours = ManagerServiceDocumentVente.validerDocumentVente(getSession().getIdSession(), documentVenteEnCours,
                optionsValidation, dateTraitement);
          }
        }
      }
    }
    
    // Effacer les données
    clientCourant = null;
    effacerVariables();
    rafraichir();
  }
  
  /**
   * Initialise l'option de validation.
   */
  private void initialiserOptionValidation() {
    if (optionsValidation == null) {
      optionsValidation = new OptionsValidation();
    }
    // Affichage de la liste des codes de validation si la variable ne vient pas d'être instanciée
    else {
      Trace.debug(ModeleComptoir.class, "ValiderDocument",
          "option=" + optionsValidation.getCodePropose() + " " + optionsValidation.getLibellePropose());
      for (String code : optionsValidation.getListeCodes()) {
        Trace.debug(ModeleComptoir.class, "initialiserOptionValidation", "codePossible", code);
      }
    }
    
    if (documentVenteEnCours.isDevis()) {
      optionsValidation.setCodePropose(OptionsValidation.OPTION_DEVIS);
    }
    else if (documentVenteEnCours.isBon()) {
      if (documentVenteEnCours.isEnCoursCreation()) {
        optionsValidation.setCodePropose(OptionsValidation.OPTION_BON_LIVRAISON);
      }
      else {
        optionsValidation.setCodePropose(OptionsValidation.OPTION_ATTENTE);
      }
    }
    else if (documentVenteEnCours.isFacture()) {
      // Si on est en train de créer une facture on met FAC pour valider
      optionsValidation.setCodePropose(OptionsValidation.OPTION_FACTURE);
    }
    else if (documentVenteEnCours.isCommande()) {
      optionsValidation.setCodePropose(OptionsValidation.OPTION_COMMANDE);
    }
    Trace.debug(ModeleComptoir.class, "ValiderDocument",
        "option=" + optionsValidation.getCodePropose() + " " + optionsValidation.getLibellePropose());
  }
  
  /**
   * Controle l'intégrité du document en cours.
   * La variable pActiverAlerte est là pour éviter de bloquer certains traitements comme la suppression d'une ligne de vente
   * ou l'annulation d'un document car dans ces deux cas le controle des alertes ne sert à rien mais par contre il peut bloquer
   * le traitement normal du document.
   */
  private void controlerDocument(boolean pActiverAlerte) {
    initialiserOptionValidation();
    
    optionsValidation = ManagerServiceDocumentVente.controlerFinDocumentVente(getSession().getIdSession(), documentVenteEnCours,
        optionsValidation, dateTraitement);
    
    if (optionsValidation != null && pActiverAlerte) {
      AlertesDocument alertesDocument = optionsValidation.getAlertesDocument();
      
      // Le service retourne les données en fonction du champ CLTNS (Acompte obligatoire de la fiche client)
      // S'il est obligatoire il retourne le pourcentage de la catégorie client en fonction du cas (normal ou détection d'un article
      // spécial)
      if (alertesDocument.isAcompteObligatoireNormal()) {
        acompteParametre.setAcompteObligatoireNormal(alertesDocument.isAcompteObligatoireNormal());
        acompteParametre.setPourcentageNormal(alertesDocument.getPourcentageAcompteNormal());
      }
      if (alertesDocument.isAcompteObligatoireDetectionArticleSpecial()) {
        acompteParametre.setAcompteObligatoireDetectionArticleSpecial(alertesDocument.isAcompteObligatoireDetectionArticleSpecial());
        acompteParametre.setPourcentageDetectionArticleSpecial(alertesDocument.getPourcentageAcompteDetectionArticleSpecial());
      }
      activerPaiementAcompte();
      
      if (alertesDocument.controlerAlertes()) {
        if (alertesDocument.isMargeMiniNonAtteinte()) {
          throw new MessageErreurException("La marge minimale obligatoire n'est pas atteinte");
        }
        if (alertesDocument.isStockNonDisponible() && (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture())) {
          throw new MessageErreurException("Le stock demandé n'est pas disponible");
        }
        if (alertesDocument.isAucuneLigneLivrable() && (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture())) {
          throw new MessageErreurException("Aucune ligne n'est livrable");
        }
        if (alertesDocument.isNombreLignesEnErreur()) {
          throw new MessageErreurException("Il y a " + alertesDocument.getNombreLignesEnErreurAttendu()
              + " lignes en erreur dans ce document. Il est impossible de le valider.");
        }
        if (alertesDocument.isAuMoinsUnLotNonEnStock() && (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture())) {
          throw new MessageErreurException("Au moins un lot n'est pas disponible en stock");
        }
        if (alertesDocument.isErreurPrixSurLigne()) {
          throw new MessageErreurException("Une erreur de prix a été détectée sur une ligne");
        }
        if (alertesDocument.isNumeroLotIncorrect()) {
          throw new MessageErreurException("Un numéro de lot est incorrect");
        }
        if (alertesDocument.isAdresseStockIncorrecte()) {
          throw new MessageErreurException("Une adresse de stock est incorrecte");
        }
        if (alertesDocument.isErreurPrixInferieurPump()) {
          throw new MessageErreurException("L'article " + alertesDocument.getCodeArticlePrixInferieurPump()
              + " présente un prix de base inférieur à son PUMP.\nLe tarif de cet article doit être modifié ou l'article doit"
              + " être supprimé de votre document de vente.");
        }
        if (alertesDocument.isErreurPrixInferieurPrv()) {
          throw new MessageErreurException("L'article " + alertesDocument.getCodeArticlePrixInferieurPrv()
              + " présente un prix de base inférieur à son prix de revient.\nLe tarif de cet article doit être modifié ou l'article doit"
              + " être supprimé de votre document de vente.");
        }
      }
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet client
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Initialise l'identifiant du représentant en création de client comptant à partir du représentant par défaut.
   * Le représentant par défaut est défini sur le magasin.
   */
  private void initialiserIdRepresentantParDefaut() {
    // On contrôle que l'on est bien en création de client comptant
    if (!isCreationClientComptant()) {
      return;
    }
    // On contrôle que l'on est bien à l'étape de création de client
    if (etape != ETAPE_CREATION_CLIENT) {
      return;
    }
    // Si l'identifiant du représentant n'a pas encore été saisi
    if (idRepresentant == null && listeMagasin != null && listeMagasin.get(idMagasin) != null) {
      idRepresentant = listeMagasin.get(idMagasin).getIdRepresentant();
    }
  }
  
  /**
   * Modifier le client sélectionné.
   */
  public void modifierClient(IdClient pIdClient) {
    if (Constantes.equals(clientCourant.getId(), pIdClient)) {
      return;
    }
    
    // Effacer toutes les variables concernant le client
    effacerVariablesClient();
    
    // Charger le client
    if (pIdClient != null) {
      clientCourant.setId(pIdClient);
      
      // Charger le client
      clientCourant = ManagerServiceClient.chargerClient(getSession().getIdSession(), clientCourant.getId());
      ManagerServiceDocumentVente.initialiserPrixClient(getSession().getIdSession(), clientCourant);
      
      // Charger les options d'édition du client
      optionsEdition.setEditionChiffree(clientCourant.getEditionChiffre());
      
      // Charger le blocnotes du client
      IdBlocNote idBlocNote = IdBlocNote.getInstancePourClient(clientCourant.getId());
      BlocNote blocNote = ManagerServiceParametre.chargerBlocNote(getSession().getIdSession(), idBlocNote);
      clientCourant.setBlocNote(blocNote);
      
      // Charger la liste des contacts du client
      CritereContact critereContact = new CritereContact();
      critereContact.setIdClient(clientCourant.getId());
      critereContact.setTypeContact(EnumTypeContact.CLIENT);
      List<IdContact> listeIdContact = ManagerServiceCommun.chargerListeIdContact(getSession().getIdSession(), critereContact);
      listeContactClient = new ListeContact();
      listeContactClient = listeContactClient.charger(getSession().getIdSession(), listeIdContact);
      
      // Initialisation de l'email
      modifierEmailDestinataire(clientCourant.getContactPrincipal(), false);
      // Initialisation du numéro de fax
      modifierFaxDestinataire(clientCourant.getContactPrincipal(), false);
      
      // Charger l'encours du client
      try {
        encoursClient = ManagerServiceClient.chargerEncoursClient(getSession().getIdSession(), clientCourant.getId());
      }
      catch (MessageErreurException e) {
        // Construction du nom
        String nomclient = "";
        if (clientCourant.getAdresse() != null) {
          nomclient = clientCourant.getAdresse().getNom() + " " + clientCourant.getAdresse().getComplementNom() + " ";
        }
        // Modification du message à la volée pour le rendre plus clair
        String message = "Le chargement des données du client " + nomclient + "a rencontré un problème.\n" + e.getMessage()
            + " Vérifier que la fiche de ce client ne soit pas ouverte en mode modification sur une autre de vos sessions ou d'un autre"
            + " utilisateur.";
        throw new MessageErreurException(message);
      }
      
      // Charger les documents de ventes du client
      modeleDocumentVenteParType = new ModeleDocumentVenteParType(getSession().getIdSession());
      modeleDocumentVenteParType.charger(getSession().getIdSession(), clientCourant.getId());
      
      // Initialiser le représentant avec celui du client
      idRepresentant = clientCourant.getIdRepresentant();
      
      // Initialiser le mode de paiement
      if (activerReglementComptant) {
        modePaiementComptant = true;
      }
      else if (clientCourant.isClientComptant() || clientCourant.isReglementComptantObligatoire()) {
        modePaiementComptant = true;
      }
      else {
        modePaiementComptant = false;
      }
      
      // Initialiser l'acompte obligatoire (du cas normal car à ce stade le document est vide)
      CategorieClient categorieClient = listeCategorieClient.get(clientCourant.getIdCategorieClient());
      if (categorieClient != null) {
        acompteParametre.setAcompteObligatoireNormal(categorieClient.isAcompteObligatoire());
        acompteParametre.setPourcentageNormal(new BigDecimal(categorieClient.getPourcentageAcompte()));
        acompteParametre.setAcompteObligatoireDetectionArticleSpecial(categorieClient.isAcompteObligatoireDetectionArticleSpeciaux());
        acompteParametre
            .setPourcentageDetectionArticleSpecial(new BigDecimal(categorieClient.getPourcentageAcompteDetectionArticleSpecial()));
      }
      
      // Afficher un message si le client est interdit
      if (clientCourant.isClientInterdit()) {
        messageClient = "Le client est interdit";
      }
      // Afficher un message si le client a des différés
      else if (modeleDocumentVenteParType != null && !modeleDocumentVenteParType.getListeFacture().isEmpty()) {
        messageClient = "Le client a des factures différées.";
      }
      else {
        messageClient = "";
      }
      
      // Contrôler adresse
      clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
      if (!traiterErreurAdresse(true, true)) {
        return;
      }
    }
    
    setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    rafraichir();
  }
  
  /**
   * Gestion des erreurs sur l'adresse du client
   */
  public boolean traiterErreurAdresse(boolean forcerRafraichissement, boolean forcerSortie) {
    if (clientCourant == null) {
      return false;
    }
    
    if (listeErreurAdresse == null || listeErreurAdresse.size() == 0) {
      return true;
    }
    
    // Associer un contact principal au client
    if (clientCourant.getContactPrincipal() == null) {
      creerContactPrincipalParDefaut();
    }
    
    ModeleErreurAdresse modeleErreur = new ModeleErreurAdresse(getSession(), clientCourant, listeErreurAdresse);
    VueErreurAdresse vueErreur = new VueErreurAdresse(modeleErreur);
    vueErreur.afficher();
    if (modeleErreur.isSortieAvecValidation()) {
      return true;
    }
    else {
      if (forcerSortie) {
        effacerVariables();
        setEtape(ETAPE_RECHERCHE_CLIENT);
      }
      if (forcerRafraichissement) {
        rafraichir();
      }
      return false;
    }
  }
  
  /**
   * Création de contact principal par défaut pour un client
   */
  private Contact creerContactPrincipalParDefaut() {
    IdContact idContact = ManagerServiceCommun.chercherPremiereIdContactLibre(getIdSession());
    Contact contact = new Contact(idContact);
    
    // Si l'adresse n'est pas vide, le définir comme nom de contact par défaut
    if (clientCourant.getAdresse() != null && !Constantes.normerTexte(clientCourant.getAdresse().getNom()).isEmpty()) {
      contact.setNom(clientCourant.getAdresse().getNom());
    }
    // Sinon, le nom du contact sera l'ID du client courant précédé du mot "Contact" et mis entre parenthèse
    else {
      contact.setNom("Contact (" + clientCourant.getId().getTexte() + ")");
    }
    
    // Contact enregistré après la validation de la correction de l'adresse
    clientCourant.setContactPrincipal(contact);
    
    return contact;
  }
  
  /**
   * Modifier la civilité du client.
   */
  public void modifierCiviliteClient(Civilite pCivilite) {
    // Tester si c'est nous sommes sur un client comptant inexistant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    if (!isCreationClientComptant() && etape == ETAPE_CREATION_CLIENT) {
      throw new MessageErreurException("Impossible de modifier la civilité d'un client existant.");
    }
    
    // Tester si la valeur a changé
    IdCivilite idCiviliteClient = null;
    if (pCivilite != null) {
      idCiviliteClient = pCivilite.getId();
    }
    if (Constantes.equals(clientCourant.getContactPrincipal().getIdCivilite(), idCiviliteClient)) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getContactPrincipal().setIdCivilite(idCiviliteClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    rafraichir();
  }
  
  /**
   * Modifier le nom du client.
   */
  public void modifierNomClient(String nomClient) {
    // Tester si c'est nous sommes sur un client comptant inexistant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    if (!isCreationClientComptant() && (etape == ETAPE_CREATION_CLIENT)) {
      throw new MessageErreurException("Impossible de modifier le mom d'un client existant.");
    }
    
    // Tester si la valeur a changé
    nomClient = Constantes.normerTexte(nomClient);
    if (nomClient.equals(clientCourant.getAdresse().getNom()) && nomClient.equals(clientCourant.getContactPrincipal().getNom())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.setTypeCompteClient(EnumTypeCompteClient.COMPTANT);
    clientCourant.setTypeImageClient(EnumTypeImageClient.PARTICULIER);
    clientCourant.setIdCategorieClient(null);
    clientCourant.getAdresse().setNom(nomClient);
    clientCourant.getContactPrincipal().setNom(nomClient);
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le complément du nom du client.
   */
  public void modifierComplementNomClient(String complementNomClient) {
    // Tester si c'est nous sommes sur un client comptant inexistant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    if (!isCreationClientComptant() && (etape == ETAPE_CREATION_CLIENT)) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client existant.");
    }
    
    // Tester si la valeur a changé
    complementNomClient = Constantes.normerTexte(complementNomClient);
    if (complementNomClient.equals(clientCourant.getAdresse().getComplementNom())
        && complementNomClient.equals(clientCourant.getContactPrincipal().getPrenom())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getAdresse().setComplementNom(complementNomClient);
    clientCourant.getContactPrincipal().setPrenom(complementNomClient);
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier la rue du client.
   */
  public void modifierRueClient(String rueClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    rueClient = Constantes.normerTexte(rueClient);
    if (rueClient.equals(clientCourant.getAdresse().getRue())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getAdresse().setRue(rueClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier la localisation du client.
   */
  public void modifierLocalisationClient(String localisationClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    localisationClient = Constantes.normerTexte(localisationClient);
    if (localisationClient.equals(clientCourant.getAdresse().getLocalisation())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getAdresse().setLocalisation(localisationClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le code postal du client.
   */
  public void modifierCodePostalClient(String pCodePostalClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    int codePostal = Constantes.convertirTexteEnInteger(pCodePostalClient);
    if (codePostal == clientCourant.getAdresse().getCodePostal()) {
      return;
    }
    
    // Charger les villes ayant ce code postal
    if (pCodePostalClient.length() == Commune.LONGUEUR_CODEPOSTAL) {
      CritereCommune critereCommune = new CritereCommune();
      critereCommune.setCodePostal(Integer.parseInt(pCodePostalClient));
      listeCommune = ListeCommune.charger(getIdSession(), critereCommune);
    }
    else {
      listeCommune = null;
    }
    
    // Mettre à jour le client
    clientCourant.getAdresse().setCodePostal(codePostal);
    clientCourant.getAdresse().setVille("");
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le code postal et la commune du client.
   */
  public void modifierCodePostalCommune(CodePostalCommune pCodePostalCommune) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (pCodePostalCommune == null) {
      return;
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    if (!clientCourant.getAdresse().getCodePostalFormate().equalsIgnoreCase(pCodePostalCommune.getCodePostal())) {
      clientCourant.getAdresse().setCodePostal(pCodePostalCommune.getCodePostalFormate());
    }
    if (!clientCourant.getAdresse().getVille().equalsIgnoreCase(pCodePostalCommune.getVille())) {
      clientCourant.getAdresse().setVille(pCodePostalCommune.getVille());
    }
    
    rafraichir();
  }
  
  /**
   * Modifier la ville du client.
   */
  public void modifierVilleClient(String villeClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    villeClient = Constantes.normerTexte(villeClient);
    if (villeClient.equals(clientCourant.getAdresse().getVille())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getAdresse().setVille(villeClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le mail du client.
   */
  public void modifierMailClient(String mailClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    mailClient = Constantes.normerTexte(mailClient);
    if (mailClient.equals(clientCourant.getContactPrincipal().getEmail())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.getContactPrincipal().setEmail(mailClient);
    emailDestinataireEdition = mailClient;
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le numéro de téléphone du client.
   */
  public void modifierTelephoneClient(String telephoneClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    telephoneClient = Constantes.normerTexte(telephoneClient);
    if (telephoneClient.equals(clientCourant.getNumeroTelephone())
        && telephoneClient.equals(clientCourant.getContactPrincipal().getNumeroTelephone1())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.setNumeroTelephone(telephoneClient);
    clientCourant.getContactPrincipal().setNumeroTelephone1(telephoneClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le numéro de fax du client.
   */
  public void modifierFaxClient(String faxClient) {
    // Tester si c'est nous sommes sur un client comptant
    if (clientCourant == null || clientCourant.isClientEnCompte()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un client en compte.");
    }
    if (clientCourant.isClientProspect()) {
      throw new MessageErreurException("Impossible de modifier les coordonnées d'un prospect.");
    }
    
    // Tester si la valeur a changé
    faxClient = Constantes.normerTexte(faxClient);
    if (faxClient.equals(clientCourant.getNumeroFax()) && faxClient.equals(clientCourant.getContactPrincipal().getNumeroFax())) {
      return;
    }
    
    // Mettre à jour le client
    clientCourant.setNumeroFax(faxClient);
    clientCourant.getContactPrincipal().setNumeroFax(faxClient);
    
    // Sauver immédiatement les modifications des clients comptants existants
    if (clientCourant.isExistant()) {
      sauverClient();
    }
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier l'établissement.
   */
  public void modifierEtablissement(Etablissement pEtablissement) {
    if (pEtablissement == null || etablissement.equals(pEtablissement)) {
      return;
    }
    
    // Si le chargement se passe bien alors on met à jour l'établissement
    try {
      utilisateurGescom = chargerInformationsGescom(pEtablissement.getId());
      chargerDonneesLieesEtablissement(pEtablissement.getId());
      etablissement = pEtablissement;
    }
    catch (MessageErreurException e) {
      // Dans le cas contraire on revient en arrière
      utilisateurGescom = chargerInformationsGescom(etablissement.getId());
      chargerDonneesLieesEtablissement(etablissement.getId());
      throw e;
    }
    
    rafraichir();
  }
  
  /**
   * Modifier le magasin du client.
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    if (idMagasin.equals(pIdMagasin)) {
      return;
    }
    idMagasin = pIdMagasin;
    rafraichir();
  }
  
  /**
   * Modifier le représentant du client.
   */
  public void modifierRepresentant(IdRepresentant pIdRepresentant) {
    if (isRepresentantNonModifiable()) {
      return;
    }
    if (Constantes.equals(idRepresentant, pIdRepresentant)) {
      return;
    }
    
    idRepresentant = pIdRepresentant;
    
    // Mettre à jour le client
    clientCourant.setIdRepresentant(idRepresentant);
    
    // Rafraichir
    if (!clientCourant.isExistant()) {
      setEtape(ETAPE_CREATION_CLIENT);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Modifier le bloc-notes du client.
   */
  public void modifierBlocNoteClient(String pTexte) {
    if (clientCourant.getBlocNote() != null && clientCourant.getBlocNote().getTexte().equals(pTexte)) {
      return;
    }
    
    // Créer le blocnotes s'il n'existe pas (l'id du blocnote sera renseigné lors de la sauvegarde
    if (clientCourant.getBlocNote() == null) {
      clientCourant.setBlocNote(new BlocNote(IdBlocNote.getInstancePourClient(clientCourant.getId())));
    }
    
    // Modifier le texte du blocnotes
    clientCourant.getBlocNote().setTexte(pTexte);
    
    // Sauver le blocnotes
    // Il ne sera pas sauvé de suite si le client est en cours de création. Dans ce cas, il sera sauvé lorsque le client sera créé.
    if (clientCourant.isExistant()) {
      ManagerServiceParametre.sauverBlocNote(getSession().getIdSession(), clientCourant.getBlocNote());
    }
    
    // Rafraichir
    setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    rafraichir();
  }
  
  /**
   * Modifier la plage de devis affichés.
   */
  public void modifierPlageDevis(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageDevis(pIndexPremiereLigne, pIndexDerniereLigne);
    }
    rafraichir();
  }
  
  /**
   * Modifier la plage de commandes affichéss.
   */
  public void modifierPlageCommande(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageCommande(pIndexPremiereLigne, pIndexDerniereLigne);
    }
    rafraichir();
  }
  
  /**
   * Modifier la plage de bons affichés.
   */
  public void modifierPlageBon(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageBon(pIndexPremiereLigne, pIndexDerniereLigne);
    }
    rafraichir();
  }
  
  /**
   * Modifier la plage de factures affichés.
   */
  public void modifierPlageFacture(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageFacture(pIndexPremiereLigne, pIndexDerniereLigne);
    }
    rafraichir();
  }
  
  /**
   * Modifier le devis sélectionné dans la liste des documents clients.
   */
  public void selectionnerDevisClient(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexDevisSelectionne()) {
      return;
    }
    modeleDocumentVenteParType.setIndexDevisSelectionne(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Modifier la commande sélectionnée dans la liste des documents clients.
   */
  public void selectionnerCommandeClient(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexCommandeSelectionnee()) {
      return;
    }
    modeleDocumentVenteParType.setIndexCommandeSelectionnee(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Modifier le bon sélectionné dans la liste des documents clients.
   */
  public void selectionnerBonClient(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexBonSelectionne()) {
      return;
    }
    modeleDocumentVenteParType.setIndexBonSelectionne(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Modifier le bon sélectionné dans la liste des documents clients.
   */
  public void selectionnerFactureClient(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexFactureSelectionnee()) {
      return;
    }
    modeleDocumentVenteParType.setIndexFactureSelectionnee(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Afficher la boîte de dialogue de recherche avancée des documents de ventes du client.
   */
  public void rechercherDocumentVenteClient() {
    // Afficher la boîte de dialogue
    ModeleRechercheDocumentVente modele = new ModeleRechercheDocumentVente(getSession(), clientCourant, isDocumentEnCoursDeSaisie());
    VueRechercheDocumentVente vue = new VueRechercheDocumentVente(modele);
    vue.afficher();
    
    // Aucun traitement car sortie de la boite de dialogue sans validation
    if (modele.isSortieAvecAnnulation()) {
      return;
    }
    
    // Récupérer le document de ventes sélectionnés
    DocumentVenteBase documentVenteBase = modele.getDocumentVenteBaseSelectionne();
    if (documentVenteBase != null) {
      // Effacement des variables globales
      effacerVariablesDocument();
      EnumOrigineDocumentVente typeExtraction = null;
      
      switch (modele.getRetour()) {
        case ModeleRechercheDocumentVente.RETOUR_EXTRAIRE_EN_FACTURE:
          if (documentVenteBase.isDevis()) {
            typeExtraction = EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_FACTURE;
          }
          else if (documentVenteBase.isCommande()) {
            typeExtraction = EnumOrigineDocumentVente.EXTRACTION_COMMANDE_EN_FACTURE;
          }
          extraireDocumentVente(documentVenteBase.getId(), typeExtraction);
          break;
        
        case ModeleRechercheDocumentVente.RETOUR_EXTRAIRE_EN_BON:
          if (documentVenteBase.isDevis()) {
            typeExtraction = EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_BON;
          }
          else if (documentVenteBase.isCommande()) {
            typeExtraction = EnumOrigineDocumentVente.EXTRACTION_COMMANDE_EN_BON;
          }
          extraireDocumentVente(documentVenteBase.getId(), typeExtraction);
          break;
        
        case ModeleRechercheDocumentVente.RETOUR_EXTRAIRE_EN_COMMANDE:
          if (documentVenteBase.isDevis()) {
            typeExtraction = EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_COMMANDE;
          }
          extraireDocumentVente(documentVenteBase.getId(), typeExtraction);
          break;
        
        case ModeleRechercheDocumentVente.RETOUR_DUPLIQUER_DOCUMENT:
          dupliquerDocumentVente(documentVenteBase.getId(), documentVenteBase.getTypeDocumentVente());
          break;
        
        case ModeleRechercheDocumentVente.RETOUR_MODIFIER_DOCUMENT:
          if (documentVenteBase.getEtatExtraction().equals(EnumEtatExtractionDocumentVente.COMPLETE)) {
            modifierDocumentVente(documentVenteBase.getId(), false);
          }
          else {
            modifierDocumentVente(documentVenteBase.getId(), true);
          }
          break;
        
        case ModeleRechercheDocumentVente.RETOUR_CREER_AVOIR:
          creerAvoir(documentVenteBase.getId());
          break;
        
        default:
          break;
      }
    }
    
    rafraichir();
  }
  
  /**
   * Lance le programme d'encours comptable dans une getSession() à part.
   */
  public void lancerProgrammeEncoursComptable() {
    final Character ps059 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS059);
    
    new Thread(new Runnable() {
      @Override
      public void run() {
        EnvProgramClient infoPrg = new EnvProgramClient();
        infoPrg.setProgram("VGVM33CL");
        // Paramètre ZPAR90
        infoPrg.addParametre(
            String.format("'%s%s%0137d'", "          ", clientCourant.getId().getIndicatif(IdClient.INDICATIF_ETB_NUM_SUF), 0), true);
        // Paramètre ZPAR33
        String numeroFm = ManagerSessionClient.getInstance().getCurlib().getNom().substring(2);
        infoPrg.addParametre(String.format("'%07d%07d%07d%07d%07d%7s%30s%s%s%07d%07d%07d%07d%26s'",
            clientCourant.getEncoursFacture().intValue(), clientCourant.getEncoursExpedie().intValue(),
            clientCourant.getEncoursCommande().intValue(), clientCourant.getPlafondEncoursAutorise(),
            clientCourant.getVenteAssimileExport(), "GVM/" + numeroFm, clientCourant.getAdresse().getNom(), " ", // ORI
            ps059, // PST,59
            clientCourant.getSurPlafondEncoursAutorise(), clientCourant.getDateLimitePourSurPlafond(), 0, // P33BON
            clientCourant.getPlafondMaxEnDeblocage(), " "), true);
        infoPrg.setLibMenu("Encours comptable");
        infoPrg.addBib("WEXPAS");
        infoPrg.addBib("WGVMAS");
        infoPrg.addBib("WGVMX");
        ((SessionJava) getSession()).getFenetre()
            .ajouterSessionRpg(new SessionRPG(infoPrg, ((SessionJava) getSession()).getFenetre(), null));
      }
    }).start();
  }
  
  /**
   * Créer un devis.
   */
  public void creerDevis() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de créer un devis car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de créer un document de ventes car un document est déjà en cours de saisie.");
    }
    
    // Sauver le client s'il est nouveau
    if (!clientCourant.isExistant()) {
      sauverClient();
      
      // Recharger le client que l'on vient de créer pour avoir des données complètes
      modifierClient(clientCourant.getId());
    }
    
    // Vérifier les informations obligatoires du client pour un devis
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, true);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Effacer des variables globales
    effacerVariablesNavigation();
    effacerVariablesDocument();
    
    // Initialiser le nouveau document de ventes
    IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceAvecCreationId(etablissement.getId(), EnumCodeEnteteDocumentVente.DEVIS);
    documentVenteEnCours = new DocumentVente(idDocumentVente, EnumTypeDocumentVente.DEVIS);
    documentVenteEnCours.setId(idDocumentVente);
    documentVenteEnCours.setProfilCreation(ManagerSessionClient.getInstance().getProfil());
    documentVenteEnCours.setIdMagasin(idMagasin);
    documentVenteEnCours.setIdVendeur(utilisateurGescom.getIdVendeur());
    documentVenteEnCours.setIdRepresentant1(idRepresentant);
    documentVenteEnCours.setIdClientFacture(clientCourant.getId());
    documentVenteEnCours.setAdresseFacturation(clientCourant.getAdresse());
    if (listeModeExpedition != null) {
      switch (getModeDelivrementParDefaut()) {
        case AUCUN:
          documentVenteEnCours.setIdModeExpedition(null);
          break;
        case ENLEVEMENT:
          ModeExpedition modeExpedition = listeModeExpedition.getModeExpeditionEnlevement();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *E (Enlèvement) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        case LIVRAISON:
          modeExpedition = listeModeExpedition.getModeExpeditionLivraison();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *L (Livraison) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        
        default:
          break;
      }
    }
    if (clientCourant.getIdTypeFacturation() != null) {
      documentVenteEnCours.setIdTypeFacturation(clientCourant.getIdTypeFacturation());
    }
    else {
      documentVenteEnCours.setIdTypeFacturation(listeTypeFacturation.getIdTypeFacturationParDefaut());
    }
    
    // Créer le document de ventes
    documentVenteEnCours = creerDocumentVente(documentVenteEnCours);
    
    modifierMonoFournisseur(true);
    
    // Changer d'onglet
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    rafraichir();
  }
  
  /**
   * Créer une commande.
   */
  public void creerCommande() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de créer une commande car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de créer un document de ventes car un document est déjà en cours de saisie.");
    }
    
    // Sauver le client s'il est nouveau
    if (!clientCourant.isExistant()) {
      sauverClient();
      
      // Recharger le client que l'on vient de créer poru avoir des données complètes
      modifierClient(clientCourant.getId());
    }
    
    // Vérifier les informations obligatoires du client pour une commande
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, true);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, true);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Effacement des variables globales
    effacerVariablesNavigation();
    effacerVariablesDocument();
    
    // Initialisation du nouveau document
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstanceAvecCreationId(etablissement.getId(), EnumCodeEnteteDocumentVente.COMMANDE_OU_BON);
    documentVenteEnCours = new DocumentVente(idDocumentVente, EnumTypeDocumentVente.COMMANDE);
    documentVenteEnCours.setId(idDocumentVente);
    documentVenteEnCours.setIdMagasin(idMagasin);
    documentVenteEnCours.setProfilCreation(ManagerSessionClient.getInstance().getProfil());
    documentVenteEnCours.setIdVendeur(utilisateurGescom.getIdVendeur());
    documentVenteEnCours.setIdRepresentant1(idRepresentant);
    documentVenteEnCours.setIdClientFacture(clientCourant.getId());
    documentVenteEnCours.setAdresseFacturation(clientCourant.getAdresse());
    if (listeModeExpedition != null) {
      switch (getModeDelivrementParDefaut()) {
        case AUCUN:
          documentVenteEnCours.setIdModeExpedition(null);
          break;
        case ENLEVEMENT:
          ModeExpedition modeExpedition = listeModeExpedition.getModeExpeditionEnlevement();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *E (Enlèvement) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        case LIVRAISON:
          modeExpedition = listeModeExpedition.getModeExpeditionLivraison();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *L (Livraison) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        
        default:
          break;
      }
    }
    if (clientCourant.getIdTypeFacturation() != null) {
      documentVenteEnCours.setIdTypeFacturation(clientCourant.getIdTypeFacturation());
    }
    else {
      documentVenteEnCours.setIdTypeFacturation(listeTypeFacturation.getIdTypeFacturationParDefaut());
    }
    
    // Créer le document de ventes
    documentVenteEnCours = creerDocumentVente(documentVenteEnCours);
    
    modifierMonoFournisseur(true);
    
    // Changer d'onglet
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    rafraichir();
  }
  
  /**
   * Créer un bon.
   */
  public void creerBon() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de créer un bon car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de créer un document de ventes car un document est déjà en cours de saisie.");
    }
    
    // Sauver le client s'il est nouveau
    if (!clientCourant.isExistant()) {
      sauverClient();
      
      // Recharger le client que l'on vient de créer poru avoir des données complètes
      modifierClient(clientCourant.getId());
    }
    
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Effacement des variables globales
    effacerVariablesNavigation();
    effacerVariablesDocument();
    
    // Initialisation du nouveau document
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstanceAvecCreationId(etablissement.getId(), EnumCodeEnteteDocumentVente.COMMANDE_OU_BON);
    documentVenteEnCours = new DocumentVente(idDocumentVente, EnumTypeDocumentVente.BON);
    documentVenteEnCours.setEtat(EnumEtatBonDocumentVente.EXPEDIE);
    documentVenteEnCours.setId(idDocumentVente);
    documentVenteEnCours.setIdMagasin(idMagasin);
    documentVenteEnCours.setProfilCreation(ManagerSessionClient.getInstance().getProfil());
    documentVenteEnCours.setIdVendeur(utilisateurGescom.getIdVendeur());
    documentVenteEnCours.setIdRepresentant1(idRepresentant);
    documentVenteEnCours.setIdClientFacture(clientCourant.getId());
    documentVenteEnCours.setAdresseFacturation(clientCourant.getAdresse());
    if (listeModeExpedition != null) {
      switch (getModeDelivrementParDefaut()) {
        case AUCUN:
          documentVenteEnCours.setIdModeExpedition(null);
          break;
        case ENLEVEMENT:
          ModeExpedition modeExpedition = listeModeExpedition.getModeExpeditionEnlevement();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *E (Enlèvement) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        case LIVRAISON:
          modeExpedition = listeModeExpedition.getModeExpeditionLivraison();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *L (Livraison) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        
        default:
          break;
      }
    }
    if (clientCourant.getIdTypeFacturation() != null) {
      documentVenteEnCours.setIdTypeFacturation(clientCourant.getIdTypeFacturation());
    }
    else {
      documentVenteEnCours.setIdTypeFacturation(listeTypeFacturation.getIdTypeFacturationParDefaut());
    }
    
    // Créer le document de ventes
    documentVenteEnCours = creerDocumentVente(documentVenteEnCours);
    
    modifierMonoFournisseur(true);
    
    // Changer d'onglet
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    rafraichir();
  }
  
  /**
   * Créer une facture.
   */
  public void creerFacture() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de créer une facture car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de créer un document de ventes car un document est déjà en cours de saisie.");
    }
    
    // Sauver le client s'il est nouveau
    if (!clientCourant.isExistant()) {
      sauverClient();
      
      // Recharger le client que l'on vient de créer poru avoir des données complètes
      modifierClient(clientCourant.getId());
    }
    
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Effacement des variables globales
    effacerVariablesNavigation();
    effacerVariablesDocument();
    
    // Initialisation du nouveau document
    IdDocumentVente idDocumentVente =
        IdDocumentVente.getInstanceAvecCreationId(etablissement.getId(), EnumCodeEnteteDocumentVente.COMMANDE_OU_BON);
    documentVenteEnCours = new DocumentVente(idDocumentVente, EnumTypeDocumentVente.FACTURE);
    documentVenteEnCours.setId(idDocumentVente);
    documentVenteEnCours.setIdMagasin(idMagasin);
    documentVenteEnCours.setProfilCreation(ManagerSessionClient.getInstance().getProfil());
    documentVenteEnCours.setIdVendeur(utilisateurGescom.getIdVendeur());
    documentVenteEnCours.setIdRepresentant1(idRepresentant);
    documentVenteEnCours.setIdClientFacture(clientCourant.getId());
    documentVenteEnCours.setAdresseFacturation(clientCourant.getAdresse());
    if (listeModeExpedition != null) {
      switch (getModeDelivrementParDefaut()) {
        case AUCUN:
          documentVenteEnCours.setIdModeExpedition(null);
          break;
        case ENLEVEMENT:
          ModeExpedition modeExpedition = listeModeExpedition.getModeExpeditionEnlevement();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *E (Enlèvement) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        case LIVRAISON:
          modeExpedition = listeModeExpedition.getModeExpeditionLivraison();
          if (modeExpedition == null) {
            throw new MessageErreurException(
                "Vérifier le paramètre 'EX' dans la personnalisation des ventes afin de vous assurer que le mode d'expédition *L (Livraison) soit bien présent.");
          }
          documentVenteEnCours.setIdModeExpedition(modeExpedition.getId());
          break;
        
        default:
          break;
      }
    }
    if (clientCourant.getIdTypeFacturation() != null) {
      documentVenteEnCours.setIdTypeFacturation(clientCourant.getIdTypeFacturation());
    }
    else {
      documentVenteEnCours.setIdTypeFacturation(listeTypeFacturation.getIdTypeFacturationParDefaut());
    }
    
    // Créer le document de ventes
    documentVenteEnCours = creerDocumentVente(documentVenteEnCours);
    
    modifierMonoFournisseur(true);
    
    // Changer d'onglet
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    rafraichir();
  }
  
  /**
   * Créer un avoir.
   */
  public void creerAvoir(IdDocumentVente pIdDocumentVente) {
    // Boîte de dialogue de changement de client pour le document
    ModeleCreerAvoir modeleAvoir = new ModeleCreerAvoir(getSession(), pIdDocumentVente);
    VueCreerAvoir vueAvoir = new VueCreerAvoir(modeleAvoir);
    vueAvoir.afficher();
    
    if (!modeleAvoir.isSortieAvecValidation()) {
      return;
    }
    
    // Création de l'avoir (par duplication)
    IdTypeAvoir idTypeAvoir = null;
    if (modeleAvoir.getTypeAvoir() != null) {
      idTypeAvoir = modeleAvoir.getTypeAvoir().getId();
    }
    IdDocumentVente idAvoir =
        ManagerServiceDocumentVente.creerAvoir(getSession().getIdSession(), modeleAvoir.getIdDocumentOrigine(), idTypeAvoir);
    
    if (idAvoir == null) {
      throw new MessageErreurException("Erreur lors de la création de l'avoir.");
    }
    
    idDocumentOrigineAvoir = pIdDocumentVente;
    
    // Effacer les variables globales
    effacerVariablesDocument();
    
    // Recharge le document pour affichage
    documentVenteEnCours = chargerDocumentVente(idAvoir);
    documentVenteEnCours.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
    isEnCoursSaisieAvoirTotal = true;
    
    // Activer l'onglet Enlèvement/Livraison
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT, ONGLET_EDITION);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Changer le client pour lequel un document est créé, en cours de création
   */
  public void changerClientDocument() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Le client en cours est invalide.");
    }
    if (documentVenteEnCours == null || !documentVenteEnCours.isEnCoursCreation()) {
      throw new MessageErreurException("Un document doit être en cours de création pour utiliser cette option.");
    }
    
    IdClient idClientOrigine = clientCourant.getId();
    
    // Boîte de dialogue de changement de client pour le document
    ModeleChangementClientDocument modele = new ModeleChangementClientDocument(getSession(), documentVenteEnCours);
    VueChangementClientDocument vue = new VueChangementClientDocument(modele);
    vue.afficher();
    
    // Si on n'a pas validé la boite de dialogue on sort
    if (!modele.isSortieAvecValidation()) {
      return;
    }
    // Si on n'a pas changé le client dans la boite de dialogue on ne fait rien
    if (modele.getDocument().getIdClientFacture().equals(idClientOrigine)) {
      return;
    }
    
    // Service de changement de client
    ManagerServiceDocumentVente.changerClientDocument(getIdSession(), documentVenteEnCours.getId(),
        modele.getDocument().getIdClientFacture(), modele.isRecalculerPrix());
    
    // Effacement du texte des informations de livraison
    if (documentVenteEnCours.getMemoLivraisonEnlevement() != null) {
      ManagerServiceParametre.supprimerMemo(getIdSession(), documentVenteEnCours.getMemoLivraisonEnlevement().getId());
    }
    
    // Effacer les variables globales
    effacerVariablesDocument();
    effacerListeArticleResultatRecherche();
    
    // On recharge le document pour affichage
    modifierClient(modele.getDocument().getIdClientFacture());
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(modele.getDocument());
    
    // Initialiser le chantier
    documentVenteEnCours.setIdChantier(null);
    chantier = null;
    
    rafraichir();
  }
  
  /**
   * Importation d'un devis ProDevis.
   */
  public void importerDevisProDevis() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible d'importer un pro-devis car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible d'importer un pro-devis car une saisie est déjà en cours.");
    }
    
    // Vérifier les informations obligatoires du client pour un devis
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, true);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Sauver le client s'il est nouveau
    if (!clientCourant.isExistant()) {
      sauverClient();
      
      // Recharger le client que l'on vient de créer pour avoir des données complètes
      modifierClient(clientCourant.getId());
    }
    
    // Sélectionner le devis à importer
    ModeleImportationDevisProDevis modele = new ModeleImportationDevisProDevis(getSession(), this, clientCourant);
    VueImportationDevisProDevis vue = new VueImportationDevisProDevis(modele);
    vue.afficher();
    if (!modele.isSortieAvecValidation()) {
      return;
    }
    
    // Effacer les variables globales
    effacerVariablesDocument();
    
    // Récupérer le document créé
    IdDocumentVente idDocumentVente = modele.getIdCommandeProDevis();
    if (idDocumentVente == null) {
      throw new MessageErreurException("Erreur lors de la génération de la commande.");
    }
    
    // Charger le document de vente créé
    documentVenteEnCours = chargerDocumentVente(idDocumentVente);
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Erreur lors du chargement du document de ventes :" + idDocumentVente);
    }
    
    // Mémoriser le pro-devis de départ
    proDevisOrigine = modele.getDevisImporte();
    documentVenteAvantModifications = documentVenteEnCours.clone();
    
    // Activer l'onglet Enlèvement/Livraison
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT, ONGLET_EDITION);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le document de ventes sélectionné.
   */
  public void modifierDocumentVente(boolean pModeModification) {
    // Récupérer le document de ventes sélectionné
    DocumentVenteBase documentVenteBase = getDocumentVenteBaseSelectionne();
    
    // Vérifier les pré-requis
    if (documentVenteBase == null) {
      throw new MessageErreurException("Aucun document de ventes n'est sélectionné.");
    }
    if (documentVenteBase.isVerrouille()) {
      throw new MessageErreurException("Impossible d'ouvrir le document de ventes car il est verrouillé.");
    }
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    // Modifier le document de ventes
    modifierDocumentVente(documentVenteBase.getId(), pModeModification);
  }
  
  /**
   * Modifier un document de ventes.
   */
  public void modifierDocumentVente(IdDocumentVente pIdDocumentVente, boolean pModeModification) {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de modifier le document de ventes car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de modifier le document de ventes car une saisie est déjà en cours.");
    }
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("Impossible de modifier le document de ventes car aucun n'est sélectionné.");
    }
    
    // Effacer les variables globales
    effacerVariablesDocument();
    
    // On passe en mode modification si demandé
    isEnCoursModification = pModeModification;
    
    // Charger le document de ventes
    documentVenteEnCours = chargerDocumentVente(pIdDocumentVente);
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Erreur lors du chargement du document de ventes :" + pIdDocumentVente);
    }
    
    if (!documentVenteEnCours.isBon()) {
      Trace.info("1a-Facture:" + documentVenteEnCours.isFacture() + " modifiable:" + documentVenteEnCours.isModifiable() + " extrac:"
          + documentVenteEnCours.getEtatExtraction());
      if (documentVenteEnCours.isFacture() || !documentVenteEnCours.isModifiable()
          || documentVenteEnCours.getEtatExtraction().equals(EnumEtatExtractionDocumentVente.COMPLETE)) {
        isEnCoursModification = false;
        documentVenteEnCours.setModifiable(false);
      }
    }
    else {
      Trace.info(
          "1b-DateExpBon:" + documentVenteEnCours.getDateExpeditionBon() + " DateFact:" + documentVenteEnCours.getDateFacturation());
      if (documentVenteEnCours.getDateExpeditionBon() != null && documentVenteEnCours.getDateFacturation() != null) {
        isEnCoursModification = false;
        documentVenteEnCours.setModifiable(false);
      }
    }
    Trace.info("2-Document modifiable:" + documentVenteEnCours.isModifiable());
    
    // Modifier des informations
    documentVenteAvantModifications = documentVenteEnCours.clone();
    
    // Activation de l'onglet Enlèvement/Livraison
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT, ONGLET_EDITION);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Générer des conditions de ventes à partir de lignes de devis
   */
  public void genererCNV() {
    DocumentVente devisOrigine = documentVenteEnCours;
    if (devisOrigine == null) {
      throw new MessageErreurException("Impossible de créer une condition de vente car aucun document d'origine n'est sélectionné.");
    }
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de créer une condition de vente car le client est invalide.");
    }
    if (!devisOrigine.getId().isIdDevis()) {
      throw new MessageErreurException("Vous ne pouvez créer une condition de vente qu'à partir d'un devis.");
    }
    
    // Boite de dialogue de création des conditions de ventes
    ModeleGenererConditionVentes modeleGenererCNV = new ModeleGenererConditionVentes(getSession(), devisOrigine);
    VueGenererConditionVentes vueGenererCNV = new VueGenererConditionVentes(modeleGenererCNV);
    vueGenererCNV.afficher();
    
  }
  
  /**
   * Dupliquer le document sélectionné.
   */
  public void dupliquerDocumentVente(EnumTypeDocumentVente pTypeDocument) {
    // Contrôle du bloc adresse du client
    if (pTypeDocument.equals(EnumTypeDocumentVente.DEVIS)) {
      clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
      if (!traiterErreurAdresse(true, true)) {
        return;
      }
    }
    
    if (modeleDocumentVenteParType != null) {
      dupliquerDocumentVente(modeleDocumentVenteParType.getIdDocumentVenteSelectionne(), pTypeDocument);
    }
  }
  
  /**
   * Dupliquer le document dont on fournit l'identifiant.
   */
  public void dupliquerDocumentVente(IdDocumentVente pIdDocumentVente, EnumTypeDocumentVente pTypeDocument) {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de dupliquer le document car le client est invalide.");
    }
    if (documentVenteEnCours != null) {
      throw new MessageErreurException("Impossible de dupliquer le document car une saisie est déjà en cours.");
    }
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("Impossible de dupliquer le document car aucun document d'origine n'est sélectionné.");
    }
    
    // Boite de dialogue de duplication pour demande de recalcul des prix
    ModeleDupliquerDocument modeleDupliquer = new ModeleDupliquerDocument(getSession(), pIdDocumentVente);
    VueDupliquerDocument vueDupliquer = new VueDupliquerDocument(modeleDupliquer);
    vueDupliquer.afficher();
    
    if (modeleDupliquer.isSortieAvecValidation()) {
      // Dupliquer le document
      IdDocumentVente idDocumentVente = ManagerServiceDocumentVente.dupliquerDocument(getSession().getIdSession(), pIdDocumentVente,
          modeleDupliquer.isRecalculerPrix());
      
      if (idDocumentVente == null) {
        throw new MessageErreurException("Erreur lors de la duplication du document.");
      }
      
      // Effacer les variables globales
      effacerVariablesDocument();
      
      // Charger le devis qui vient d'être généré
      documentVenteEnCours = chargerDocumentVente(idDocumentVente);
      // Mise à jour des dates du devis
      if (pTypeDocument.equals(EnumTypeDocumentVente.DEVIS)) {
        documentVenteEnCours.setDateDernierTraitementDevis(new Date());
        // Calcul de la nouvelle date de validité
        documentVenteEnCours.setDateValiditeDevis(null);
        documentVenteEnCours.setDateValiditeDevis(getDateValiditeDocument());
        if (documentVenteEnCours.getDateValiditeDevis() == null) {
          documentVenteEnCours.setDateValiditeDevis(new Date());
        }
      }
      
      // Si c'est une duplication de bon il faut le préciser, car sinon pas de différence avec une commande tant qu'on n'a pas validé
      if (pTypeDocument.equals(EnumTypeDocumentVente.BON)) {
        documentVenteEnCours.setTypeDocumentVente(EnumTypeDocumentVente.BON);
      }
      // Si c'est une duplication de facture il faut le préciser, car sinon pas de différence avec une commande tant qu'on n'a pas validé
      else if (pTypeDocument.equals(EnumTypeDocumentVente.FACTURE)) {
        documentVenteEnCours.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
      }
      
      if (documentVenteEnCours == null) {
        throw new MessageErreurException("Erreur lors du chargement du document de ventes après duplication :" + idDocumentVente);
      }
      
      // Modifier des informations
      documentVenteEnCours.setEtat(EnumEtatBonDocumentVente.ATTENTE);
      
      // Contrôle de l'onglet livraison
      controlerOngletLivraison();
      
      // Activer l'onglet Enlèvement/Livraison
      setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT, ONGLET_EDITION);
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    }
    else {
      setEtape(ETAPE_SAISIE_ONGLET_CLIENT);
    }
    rafraichir();
  }
  
  /**
   * Extraire le document de ventes sélectionnés.
   */
  public void extraireDocumentVente(EnumOrigineDocumentVente pTypeExtraction) {
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
    if (!traiterErreurAdresse(true, true)) {
      return;
    }
    
    if (modeleDocumentVenteParType != null) {
      extraireDocumentVente(modeleDocumentVenteParType.getIdDocumentVenteSelectionne(), pTypeExtraction);
    }
  }
  
  /**
   * Extraire un document de ventes dont on fournit l'identifiant.
   */
  public void extraireDocumentVente(IdDocumentVente pIdDocumentVente, EnumOrigineDocumentVente pTypeExtraction) {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de faire l'extraction car le client est invalide.");
    }
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("Impossible de faire l'extraction car aucun document de ventes n'est sélectionné.");
    }
    if (pTypeExtraction == null) {
      throw new MessageErreurException("Impossible de faire l'extraction car le type d'extraction est invalide.");
    }
    
    // Charger le document de ventes sélectionnés
    DocumentVente documentVenteSource = chargerDocumentVente(pIdDocumentVente);
    if (documentVenteSource == null) {
      throw new MessageErreurException("Impossible de faire l'extraction car il n'est pas possible de charger le document de ventes.");
    }
    
    // C'est à cause du service validerDocument/validerExtractionDocument
    documentVenteSource.setEtat(EnumEtatBonDocumentVente.VALIDE);
    documentVenteSource.setTotalTTCOrigine(documentVenteSource.getTotalTTC());
    
    // Contrôler si l'on peut extraire des articles du document
    documentVenteSource.controlerExtractionPossible();
    
    // Chargement de la version originale du document sélectionné
    IdDocumentVente idDocumentVenteSourceOriginal = IdDocumentVente.getInstanceIdOrigine(documentVenteSource.getId());
    DocumentVente documentVenteSourceOriginal = chargerDocumentVente(idDocumentVenteSourceOriginal);
    
    // Initialiser les informations d'extraction
    if (informationsExtraction == null) {
      informationsExtraction = new InformationsExtraction();
    }
    informationsExtraction.effacerVariables();
    informationsExtraction.setTypeExtraction(pTypeExtraction);
    informationsExtraction.setDocumentVenteSource(documentVenteSource);
    informationsExtraction.setDocumentVenteSourceOriginal(documentVenteSourceOriginal);
    
    // Récupérer la date de traitement
    Date dateTraitementDocument = etablissement.getDateTraitement();
    
    // Afficher la boite de dialogue pour l'extraction
    ModeleExtractionDocument modele = new ModeleExtractionDocument(getSession(), this, dateTraitementDocument, informationsExtraction);
    VueExtractionDocument vue = new VueExtractionDocument(modele);
    vue.afficher();
    if (modele.isSortieAvecAnnulation()) {
      return;
    }
    
    // Effacer les variables globales
    // effacerVariablesDocument();
    
    // Sauvegarde temporaire de l'Id du document d'origine qui sera perdu lors du chargement du document
    // TODO J'ai créé la SNC-3900 pour éviter le traitement en dessous
    IdDocumentVente idDocumentOrigine = null;
    if (modele.getDocumentVenteNouveau() != null) {
      idDocumentOrigine = modele.getDocumentVenteNouveau().getIdOrigine();
    }
    
    // Actualiser les prix du document de ventes généré si nécessaire (SNC-3035)
    if (modele.isRecalculerPrix()) {
      ManagerServiceDocumentVente.chiffrerDocumentVente(getSession().getIdSession(), modele.getDocumentVenteNouveau().getId(), false);
    }
    
    // Charger le document généré par l'extraction
    documentVenteEnCours = chargerDocumentVente(modele.getDocumentVenteNouveau().getId());
    
    // Modifier les informations du document de ventes créés
    documentVenteEnCours.setEtat(EnumEtatBonDocumentVente.ATTENTE);
    documentVenteEnCours.setPoidsKg(modele.getDocumentVenteNouveau().getPoidsTotalKg());
    documentVenteEnCours.setVolume(modele.getDocumentVenteNouveau().getVolume());
    documentVenteEnCours.setTypeDocumentVente(modele.getDocumentVenteNouveau().getTypeDocumentVente());
    documentVenteEnCours.setIdAdresseFournisseur(documentVenteSourceOriginal.getIdAdresseFournisseur());
    
    // Copier le chantier d'origine dans le document extrait
    if (documentVenteSource.getIdChantier() != null) {
      documentVenteEnCours.setIdChantier(documentVenteSource.getIdChantier());
      documentVenteEnCours.setLibelleChantier(documentVenteSource.getLibelleChantier());
      chantier = ManagerServiceClient.chargerChantier(getIdSession(), documentVenteEnCours.getIdChantier());
    }
    else {
      documentVenteEnCours.setIdChantier(null);
      documentVenteEnCours.setLibelleChantier(null);
      chantier = null;
    }
    
    // Ajoute une information dans la référence si cette dernière est vide
    if (documentVenteEnCours.getReferenceLongue().trim().isEmpty()) {
      documentVenteEnCours.setReferenceLongue(modele.getReferenceLongue());
    }
    // Stocker l'ID du document d'origine qui a été perdu lors de l'appel du service "chargerDocument"
    // TODO SNC-3900
    documentVenteEnCours.setIdOrigine(idDocumentOrigine);
    
    // Saisir les numéros de lots si le document en contient et que les lots n'ont pas été affectés
    if (isGestionParLot() && isSaisieLotsObligatoire() && !pIdDocumentVente.isIdDevis()) {
      for (LigneVente ligne : documentVenteEnCours.getListeLigneVente()) {
        if (!ligne.isLigneCommentaire()) {
          Article article = new Article(ligne.getIdArticle());
          article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
          if (article.isArticleLot()) {
            if (!article.isArticleSpecial()) {
              documentVenteEnCours.setIdOrigine(pIdDocumentVente);
              ajouterQuantiteSurLot(ligne, article, ligne.getQuantiteUCV());
            }
          }
        }
      }
    }
    
    // Activer de l'onglet Enlèvement/Livraison
    setIndiceOnglet(ONGLET_LIVRAISONENLEVEMENT, ONGLET_EDITION);
    indiceOngletAccessible = ONGLET_LIVRAISONENLEVEMENT;
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    rafraichir();
  }
  
  /**
   * Sauver les informations du client dans la base.
   */
  private void sauverClient() {
    // Vérifier les pré-requis
    if (clientCourant == null) {
      throw new MessageErreurException("Impossible de sauver le client car il est invalide.");
    }
    
    // Sauver un client existant
    if (clientCourant.isExistant()) {
      ManagerServiceClient.sauverClient(getSession().getIdSession(), clientCourant);
    }
    // Sauver un nouveau client
    else {
      // Vérifier que les informations nécessaires sont présentes
      if (!clientCourant.isCreationPossibleClientComptant()) {
        throw new MessageErreurException("Vous devez saisir au minimum le nom, le prénom, la rue, le code postal et la ville du client.");
      }
      
      // Le client
      clientCourant.setId(IdClient.getInstanceAvecCreationId(etablissement.getId()));
      
      // Initialisation des variables numériques à la création d'un nouveau client comptant
      clientCourant.setEncoursCommande(BigDecimal.ZERO);
      clientCourant.setEncoursExpedie(BigDecimal.ZERO);
      clientCourant.setEncoursFacture(BigDecimal.ZERO);
      clientCourant.setFraisFacturePied1(BigDecimal.ZERO);
      clientCourant.setFraisFacturePied2(BigDecimal.ZERO);
      clientCourant.setFraisFacturePied3(BigDecimal.ZERO);
      clientCourant.setTauxEscompte(BigDecimal.ZERO);
      clientCourant.setRemise1(BigDecimal.ZERO);
      clientCourant.setRemise2(BigDecimal.ZERO);
      clientCourant.setRemise3(BigDecimal.ZERO);
      clientCourant.setRemisePied1(BigDecimal.ZERO);
      clientCourant.setRemisePied2(BigDecimal.ZERO);
      clientCourant.setRemisePied3(BigDecimal.ZERO);
      
      clientCourant = ManagerServiceClient.sauverClientComptant(getSession().getIdSession(), clientCourant);
      
      // Le bloc-notes du client
      BlocNote blocNote = clientCourant.getBlocNote();
      if (blocNote != null && !Constantes.normerTexte(blocNote.getTexte()).isEmpty()) {
        blocNote.setId(IdBlocNote.getInstancePourClient(clientCourant.getId()));
        ManagerServiceParametre.sauverBlocNote(getSession().getIdSession(), blocNote);
      }
      
      // Le mode de paiement est comptant car c'est un client comptant qui vient d'être créé
      modePaiementComptant = true;
    }
  }
  
  /**
   * Afficher le composant des documents liés pour le client en cours
   */
  public void afficherDocumentLie() {
    if (clientCourant == null) {
      return;
    }
    
    SNDocumentLie fenetreDocumentLie = new SNDocumentLie(getSession(), clientCourant);
    fenetreDocumentLie.afficher();
  }
  
  /**
   * Effectuer les contrôles et traitement lors de la sortie de l'onglet client.
   */
  private boolean controlerOngletClient() {
    // Ne pas faire de contrôle si le document n'est pas modifiable
    if (!documentVenteEnCours.isModifiable() && !documentVenteEnCours.isFacture()) {
      return true;
    }
    
    // Contrôler la présence d'un document de ventes
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de contrôler l'onglet client car le document de ventes est invalide");
    }
    
    // Contrôler la présence d'un client
    if (clientCourant == null) {
      throw new MessageErreurException("La saisie d'un client est obligatoire.");
    }
    
    // Contrôle du bloc adresse du client
    clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
    if (!traiterErreurAdresse(true, true)) {
      return false;
    }
    return true;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet enlèvement/livraison
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier l'onglet adresse sélectionné.
   */
  public void changerOngletAdresse(int pIndiceOnglet) {
    // Vérifier si l'onglet a changé
    if (indiceOngletAdresse == pIndiceOnglet) {
      return;
    }
    
    // Activer l'onglet souhaité
    indiceOngletAdresse = pIndiceOnglet;
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier le mode d'expédition du document en cours (livraison ou enlèvement).
   */
  public void modifierModeLivraison(IdModeExpedition pIdModeExpedition) {
    // Tester si la valeur a changé.
    if (Constantes.equals(pIdModeExpedition, documentVenteEnCours.geIdModeExpedition())) {
      return;
    }
    
    // Modifier le mode d'expédition
    documentVenteEnCours.setIdModeExpedition(pIdModeExpedition);
    
    // Si l'utilisateur a choisi une livraison
    if (documentVenteEnCours.geIdModeExpedition().isLivraison()) {
      // Renseigner les informations issues du chantier
      if (isAfficherChantier() && chantier != null) {
        ajouterInformationChantier();
      }
      else if (documentVenteEnCours.getTransport().getAdresseLivraison() == null) {
        documentVenteEnCours.copierAdresseFacturationVersAdresseLivraison();
      }
      
      // Remise à blanc du Pris par
      modifierPrisPar("");
      
      // Recherche de la zone géographique
      rechercherZoneGeographique(documentVenteEnCours);
      
      // Afficher l'onglet de l'adresse de livraison
      indiceOngletAdresse = ONGLET_ADRESSE_LIVRAISON;
      
      setEtape(ETAPE_CREATION_BLOC_ADRESSE_LIVRAISON);
    }
    // Si l'utilisateur a choisi un enlèvement
    else {
      documentVenteEnCours.getTransport().setAdresseLivraison(null);
      documentVenteEnCours.getTransport().setIdTransporteur(null);
      // Les dates sont effacées uniquement lorsque le document est en cours de création, car les dates peuvent avoir été renseignés pour
      // d'éventuelles stats de qualité de service (donc en modification nous ne faisons rien)
      if (documentVenteEnCours.isEnCoursCreation()) {
        documentVenteEnCours.getTransport().setDateLivraisonPrevue(null);
        documentVenteEnCours.getTransport().setDateLivraisonSouhaitee(null);
      }
      
      // Afficher l'onglet de l'adresse de facturation
      indiceOngletAdresse = ONGLET_ADRESSE_FACTURATION;
      
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      setComposantAyantLeFocus(FOCUS_LIVRAISON_REFERENCE_DOCUMENT);
    }
    rafraichir();
  }
  
  /**
   * Recherche la zone géographique à partir de l'adresse de livraison.
   */
  private void rechercherZoneGeographique(DocumentVente pDocumentVente) {
    if (pDocumentVente == null || pDocumentVente.getTransport() == null || pDocumentVente.isDirectUsine()) {
      return;
    }
    
    Transport transport = pDocumentVente.getTransport();
    // Evite un message d'erreur de type null si le magasin et/ou l'établissement n'a pas de zone géographique
    if (listeZoneGeographique == null) {
      return;
    }
    
    ZoneGeographique zoneGeographique = listeZoneGeographique.getZoneGeographiqueParCodePostalEtVille(
        transport.getAdresseLivraison().getCodePostal(), transport.getAdresseLivraison().getVille());
    
    if (zoneGeographique == null) {
      transport.setIdZoneGeographique(null);
    }
    else {
      transport.setIdZoneGeographique(zoneGeographique.getId());
    }
  }
  
  /**
   * Modifier la nature du document en direct usine.
   */
  public void modifierDirectUsine(boolean pDirectUsine) {
    // Vérifier si la valeur a changée
    if (pDirectUsine == documentVenteEnCours.isDirectUsine()) {
      return;
    }
    
    int nombreLignes = 0;
    if (documentVenteEnCours.getListeLigneVente() != null) {
      nombreLignes = documentVenteEnCours.getListeLigneVente().size();
    }
    if (!pDirectUsine && nombreLignes > 0 && documentVenteEnCours.possedeArticlesDirectUsine(getSession().getIdSession())) {
      pDirectUsine = true;
      documentVenteEnCours.setDirectUsine(pDirectUsine);
      ModeleDialogueInformation modele = new ModeleDialogueInformation();
      modele.setMessage(
          "Le document comporte au moins un article exclusivement direct usine. Veuillez le retirer avant de décocher cette option.");
      DialogueInformation vue = new DialogueInformation(modele);
      vue.afficher();
      rafraichir();
    }
    
    // Modifier la valeur
    documentVenteEnCours.setDirectUsine(pDirectUsine);
    
    // Activer la génération du document d'achats par défaut pour les commandes directs usines
    if (documentVenteEnCours.isEnCoursCreation() && documentVenteEnCours.isCommande()) {
      documentVenteEnCours.setGenerationImmediateCommandeAchat(pDirectUsine);
    }
    
    // Effacer le résultat de la recherche pour ne pas ajouter des articles de type différent par accident
    effacerListeArticleResultatRecherche();
    
    // Rafraîchir
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la référence du document.
   */
  public void modifierReferenceDocument(String pReferenceDocument) {
    pReferenceDocument = Constantes.normerTexte(pReferenceDocument);
    if (pReferenceDocument.equals(documentVenteEnCours.getReferenceLongue())) {
      return;
    }
    documentVenteEnCours.setReferenceLongue(pReferenceDocument);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la référence courte du document.
   */
  public void modifierReferenceCourteDocument(String pTexteReferenceCourte) {
    pTexteReferenceCourte = Constantes.normerTexte(pTexteReferenceCourte);
    if (pTexteReferenceCourte.equals(documentVenteEnCours.getReferenceCourte())) {
      return;
    }
    documentVenteEnCours.setReferenceCourte(pTexteReferenceCourte);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la date du document.
   */
  public void modifierDateTraitement(Date pDateTraitement) {
    pDateTraitement = Constantes.normerDate(pDateTraitement);
    if (pDateTraitement.equals(dateTraitement)) {
      return;
    }
    
    if (!etablissement.isDateDansPeriodeEnCours(pDateTraitement)) {
      throw new MessageErreurException(
          String.format("La date saisie n'est pas valide car elle est hors de la période en cours qui est comprise entre le"
              + " %1$td/%1$tm/%1$tY et le %2$td/%2$tm/%2$tY.", etablissement.getMoisEnCours(), etablissement.getMoisEnCoursMax()));
    }
    
    dateTraitement = pDateTraitement;
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la date de validité du document.
   */
  public void modifierDateValiditeDocument(Date pDateValiditeDocument) {
    // Contrôle de cohérence de la date de validité
    if (pDateValiditeDocument != null && pDateValiditeDocument.before(dateTraitement)) {
      throw new MessageErreurException(
          String.format("La date de validité saisie est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
    }
    documentVenteEnCours.setDateValiditeDevis(pDateValiditeDocument);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la date de relance du document.
   */
  public void modifierDateRelanceDocument(Date pDateRelanceDocument) {
    documentVenteEnCours.setDateRelanceDevis(pDateRelanceDocument);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Renvoyer la date de validité du document.
   */
  public Date getDateValiditeDocument() {
    if (documentVenteEnCours == null) {
      return null;
    }
    // Si on crée ou modifie un devis et que la date de validité n'est pas renseignée on renvoi la date par défaut du vendeur
    if ((documentVenteEnCours.isEnCoursCreation() || documentVenteEnCours.isModifiable())
        && documentVenteEnCours.getDateValiditeDevis() == null && listeVendeur != null && getIdVendeur() != null) {
      documentVenteEnCours.setDateValiditeDevis(listeVendeur.get(getIdVendeur()).getDateDefautValiditeDevis());
    }
    return documentVenteEnCours.getDateValiditeDevis();
  }
  
  /**
   * Renvoyer la date de relance du document.
   */
  public Date getDateRelanceDocument() {
    if (documentVenteEnCours == null) {
      return null;
    }
    return documentVenteEnCours.getDateRelanceDevis();
  }
  
  /**
   * Modifier le type de facturation.
   */
  public void modifierTypesFacturation(TypeFacturation pTypeFacturation, boolean pRafraichir) {
    // Transformer la saisie
    IdTypeFacturation idTypeFacturation = null;
    if (pTypeFacturation != null) {
      idTypeFacturation = pTypeFacturation.getId();
    }
    
    // Contrôler si la valeur a changée
    if (Constantes.equals(idTypeFacturation, documentVenteEnCours.getIdTypeFacturation())) {
      return;
    }
    
    // Mettre à jour la valeur
    documentVenteEnCours.setIdTypeFacturation(idTypeFacturation);
    
    // Rafraîchir
    if (pRafraichir) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      rafraichir();
    }
  }
  
  /**
   * 
   * Modifier le nom dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierNomFacturation(String pNomFacturation) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pNomFacturation = Constantes.normerTexte(pNomFacturation);
    if ((pNomFacturation == null) || (documentVenteEnCours.getAdresseFacturation() == null)
        || pNomFacturation.equals(documentVenteEnCours.getAdresseFacturation().getNom())) {
      return;
    }
    documentVenteEnCours.getAdresseFacturation().setNom(pNomFacturation);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier le complément de nom dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierComplementNomFacturation(String pComplementNom) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pComplementNom = Constantes.normerTexte(pComplementNom);
    if ((pComplementNom == null) || (documentVenteEnCours.getAdresseFacturation() == null)
        || pComplementNom.equals(documentVenteEnCours.getAdresseFacturation().getComplementNom())) {
      return;
    }
    documentVenteEnCours.getAdresseFacturation().setComplementNom(pComplementNom);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier la rue dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierRueFacturation(String pRue) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pRue = Constantes.normerTexte(pRue);
    if ((pRue == null) || (documentVenteEnCours.getAdresseFacturation() == null)
        || pRue.equals(documentVenteEnCours.getAdresseFacturation().getRue())) {
      return;
    }
    documentVenteEnCours.getAdresseFacturation().setRue(pRue);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier la localisation dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierLocalisationFacturation(String pLocalisation) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pLocalisation = Constantes.normerTexte(pLocalisation);
    if ((pLocalisation == null) || (documentVenteEnCours.getAdresseFacturation() == null)
        || pLocalisation.equals(documentVenteEnCours.getAdresseFacturation().getLocalisation())) {
      return;
    }
    documentVenteEnCours.getAdresseFacturation().setLocalisation(pLocalisation);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier le code postal et la commune dans l'adresse de facturation du document.
   * 
   * @param CodePostalCommune
   */
  public void modifierCodePostalCommuneFacturation(CodePostalCommune pCodePostalCommune) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    if (!documentVenteEnCours.getAdresseFacturation().getCodePostalFormate().equalsIgnoreCase(pCodePostalCommune.getCodePostal())) {
      documentVenteEnCours.getAdresseFacturation().setCodePostal(pCodePostalCommune.getCodePostalFormate());
    }
    if (!documentVenteEnCours.getAdresseFacturation().getVille().equalsIgnoreCase(pCodePostalCommune.getVille())) {
      documentVenteEnCours.getAdresseFacturation().setVille(pCodePostalCommune.getVille());
    }
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier le contact dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierContactFacturation(String pContactFacturation) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pContactFacturation = Constantes.normerTexte(pContactFacturation);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier le téléphone dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierTelephoneFacturation(String pTelephone) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pTelephone = Constantes.normerTexte(pTelephone);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier l'adresse mail dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierEmailFacturation(String pMail) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pMail = Constantes.normerTexte(pMail);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * 
   * Modifier le fax dans l'adresse de facturation du document.
   * 
   * @param String
   */
  public void modifierFaxFacturation(String pFax) {
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de facturation pour ce document.");
    }
    
    pFax = Constantes.normerTexte(pFax);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le nom pour la livraison.
   */
  public void modifierNomLivraison(String nomLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    nomLivraison = Constantes.normerTexte(nomLivraison);
    if ((nomLivraison == null) || (documentVenteEnCours.getTransport() == null)
        || (documentVenteEnCours.getTransport().getAdresseLivraison() == null)
        || nomLivraison.equals(documentVenteEnCours.getTransport().getAdresseLivraison().getNom())) {
      return;
    }
    documentVenteEnCours.getTransport().getAdresseLivraison().setNom(nomLivraison);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le complément de nom pour la livraison.
   */
  public void modifierComplementNomLivraison(String complementNomLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    complementNomLivraison = Constantes.normerTexte(complementNomLivraison);
    if ((complementNomLivraison == null) || (documentVenteEnCours.getTransport() == null)
        || (documentVenteEnCours.getTransport().getAdresseLivraison() == null)
        || complementNomLivraison.equals(documentVenteEnCours.getTransport().getAdresseLivraison().getComplementNom())) {
      return;
    }
    documentVenteEnCours.getTransport().getAdresseLivraison().setComplementNom(complementNomLivraison);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la rue pour la livraison.
   */
  public void modifierRueLivraison(String rueLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    rueLivraison = Constantes.normerTexte(rueLivraison);
    if ((rueLivraison == null) || (documentVenteEnCours.getTransport() == null)
        || (documentVenteEnCours.getTransport().getAdresseLivraison() == null)
        || rueLivraison.equals(documentVenteEnCours.getTransport().getAdresseLivraison().getRue())) {
      return;
    }
    documentVenteEnCours.getTransport().getAdresseLivraison().setRue(rueLivraison);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la localisation pour la livraison.
   */
  public void modifierLocalisationLivraison(String localisationLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    localisationLivraison = Constantes.normerTexte(localisationLivraison);
    if ((localisationLivraison == null) || (documentVenteEnCours.getTransport() == null)
        || (documentVenteEnCours.getTransport().getAdresseLivraison() == null)
        || localisationLivraison.equals(documentVenteEnCours.getTransport().getAdresseLivraison().getLocalisation())) {
      return;
    }
    documentVenteEnCours.getTransport().getAdresseLivraison().setLocalisation(localisationLivraison);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le code postal commune de la livraison.
   */
  public void modifierCodePostalCommuneLivraison(CodePostalCommune pCodePostalCommuneLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    if (!documentVenteEnCours.getTransport().getAdresseLivraison().getCodePostalFormate()
        .equalsIgnoreCase(pCodePostalCommuneLivraison.getCodePostal())) {
      documentVenteEnCours.getTransport().getAdresseLivraison().setCodePostal(pCodePostalCommuneLivraison.getCodePostalFormate());
    }
    if (!documentVenteEnCours.getTransport().getAdresseLivraison().getVille().equalsIgnoreCase(pCodePostalCommuneLivraison.getVille())) {
      documentVenteEnCours.getTransport().getAdresseLivraison().setVille(pCodePostalCommuneLivraison.getVille());
    }
    
    rechercherZoneGeographique(documentVenteEnCours);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier l'adresse de livraison par le composant de liste des adresses de livraison chantier
   */
  public void modifierAdresseLivraison() {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    IdDocumentVente idDocumentChantier = IdDocumentVente.getInstanceHorsFacture(documentVenteEnCours.getIdChantier().getIdEtablissement(),
        EnumCodeEnteteDocumentVente.DEVIS, documentVenteEnCours.getIdChantier().getNumero(),
        documentVenteEnCours.getIdChantier().getSuffixe());
    ModeleListeAdresseLivraisonChantier modeleAdresse =
        new ModeleListeAdresseLivraisonChantier(getSession(), idDocumentChantier, EnumCodeEnteteAdresseDocument.ADRESSE_LIVRAISON_DEVIS);
    VueListeAdresseLivraisonChantier vueAdresse = new VueListeAdresseLivraisonChantier(modeleAdresse);
    modeleAdresse.setAvecBonton(false);
    vueAdresse.afficher();
    if (modeleAdresse.isSortieAvecValidation()) {
      documentVenteEnCours.getTransport().setAdresseLivraison(modeleAdresse.getAdresseSelectionnee().getAdresse());
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    }
    rafraichir();
  }
  
  /**
   * Modifier le contact de livraison.
   */
  public void modifierContactLivraison(String pContactLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    pContactLivraison = Constantes.normerTexte(pContactLivraison);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le numéro de téléphone de livraison.
   */
  public void modifierTelephoneLivraison(String pTelephoneLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    pTelephoneLivraison = Constantes.normerTexte(pTelephoneLivraison);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le numéro de fax de la livraison.
   */
  public void modifierFaxLivraison(String pFaxLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    pFaxLivraison = Constantes.normerTexte(pFaxLivraison);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le mail de la livraison.
   */
  public void modifierMailLivraison(String pMailLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    pMailLivraison = Constantes.normerTexte(pMailLivraison);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le transporteur.
   */
  public void modifierTransporteur(Transporteur pTransporteur) {
    
    // Vérifier les pré-requis
    if (documentVenteEnCours == null || documentVenteEnCours.getTransport() == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible d'obtenir les informations de type transport pour ce document.");
    }
    
    // Transformer la valeur
    IdTransporteur idTransporteur = null;
    if (pTransporteur != null) {
      idTransporteur = pTransporteur.getId();
    }
    
    // Contrôler si la valeur a changé
    if (Constantes.equals(idTransporteur, documentVenteEnCours.getTransport().getIdTransporteur())) {
      return;
    }
    
    // Mettre à jour la valeur
    documentVenteEnCours.getTransport().setIdTransporteur(idTransporteur);
    
    // Gérer les conséquences
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier si la livraison est partielle ou non.
   */
  public void modifierLivraisonPartielle(EnumComboLivraisonPartielle pTypeLivraison) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les informations de livraison en mode enlèvement.");
    }
    
    if ((pTypeLivraison == null) || (documentVenteEnCours.getTransport() == null)) {
      return;
    }
    documentVenteEnCours.setBlocageExpedition(pTypeLivraison.getCode());
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le montant du franco de port.
   */
  public void modifierFranco(String pMontantFranco) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les informations de livraison en mode enlèvement.");
    }
    
    int montantFranco = Constantes.convertirTexteEnInteger(pMontantFranco);
    if (montantFranco == clientCourant.getMontantFrancoPort()) {
      return;
    }
    clientCourant.setMontantFrancoPort(montantFranco);
    sauverClient();
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la date de livraison souhaitée.
   */
  public void modifierDateLivraisonSouhaitee(Date pDateLivraisonSouhaitee) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les informations de livraison en mode enlèvement.");
    }
    
    if ((pDateLivraisonSouhaitee == null) || (documentVenteEnCours.getTransport() == null)
        || pDateLivraisonSouhaitee.equals(documentVenteEnCours.getTransport().getDateLivraisonSouhaitee())) {
      return;
    }
    if (pDateLivraisonSouhaitee.before(dateTraitement)) {
      // Afficher le message d'erreur une fois pour permettre la modification de la date à partir de la datePicker
      if (!dateLivraisonSouhaiteeInvalide) {
        dateLivraisonSouhaiteeInvalide = true;
        throw new MessageErreurException(
            String.format("La date de livraison souhaitée est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
      }
    }
    else {
      dateLivraisonSouhaiteeInvalide = false;
    }
    documentVenteEnCours.getTransport().setDateLivraisonSouhaitee(pDateLivraisonSouhaitee);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier la date de livraison prévue.
   */
  public void modifierDateLivraisonPrevue(Date pDateLivraisonPrevue) {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison()) {
      throw new MessageErreurException("Impossible de saisir les informations de livraison en mode enlèvement.");
    }
    
    if ((pDateLivraisonPrevue == null) || (documentVenteEnCours.getTransport() == null)
        || pDateLivraisonPrevue.equals(documentVenteEnCours.getTransport().getDateLivraisonPrevue())) {
      return;
    }
    if (pDateLivraisonPrevue.before(dateTraitement)) {
      // Afficher le message d'erreur une fois pour permettre la modification de la date à partir de la datePicker
      if (!dateLivraisonPrevueInvalide) {
        dateLivraisonPrevueInvalide = true;
        throw new MessageErreurException(
            String.format("La date de livraison prévue est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
      }
    }
    else {
      dateLivraisonPrevueInvalide = false;
    }
    
    documentVenteEnCours.getTransport().setDateLivraisonPrevue(pDateLivraisonPrevue);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier le contact qui va emporter la marchandise (pris par).
   */
  public void modifierPrisPar(String pContactPrisPar) {
    // Vérifier si la valeur a changé
    pContactPrisPar = Constantes.normerTexte(pContactPrisPar);
    if (pContactPrisPar.equals(documentVenteEnCours.getPrisPar())) {
      return;
    }
    
    // Mettre à jour la valeur
    documentVenteEnCours.setPrisPar(pContactPrisPar);
    
    // Rafraîchir
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier l'immatriculation du véhicule pour la livraison.
   */
  public void modifierImmatriculation(String pImmatriculation) {
    pImmatriculation = Constantes.normerTexte(pImmatriculation);
    if (pImmatriculation.equals(documentVenteEnCours.getImmatriculationVehiculeClient())) {
      return;
    }
    documentVenteEnCours.setImmatriculationVehiculeClient(pImmatriculation);
    
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Modifier les informations de livraison associées au document de ventes.
   */
  public void modifierTexteLivraisonEnlevement(String pTexte) {
    if (Constantes.equals(pTexte, documentVenteEnCours.getTexteLivraisonEnlevement())) {
      return;
    }
    
    documentVenteEnCours.setTexteLivraisonEnlevement(pTexte);
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Ajouter les informations de livraison du chantier dans le document de vente.
   * Il s'agit de de l'adresse de livraison et des informations de livraison
   */
  private void ajouterInformationChantier() {
    // Vérifier les pré-requis
    if (chantier == null) {
      return;
    }
    if (!documentVenteEnCours.isLivraison()) {
      return;
    }
    
    // Vérifier si l'adresse de livraison actuelle est différente de celle du chantier
    if (!chantier.getAdresse().equals(documentVenteEnCours.getTransport().getAdresseLivraison())) {
      // Vérifier si l'utilisateur a modifié l'adresse
      if (documentVenteEnCours.getTransport().getAdresseLivraison() != null
          && !Constantes.equals(documentVenteEnCours.getTransport().getAdresseLivraison(), clientCourant.getAdresse())) {
        // Demander confirmation à l'utilisateur
        ModeleDialogueConfirmation modeleChoixAdresseChantier = new ModeleDialogueConfirmation(getSession(),
            "Modification de l'adresse de livraison", "Le chantier comporte une adresse de livraison mais vous en avez déjà saisi une. "
                + "Souhaitez-vous utiliser l'adresse du chantier comme adresse de livraison ?");
        DialogueConfirmation vueChoixAdresseChantier = new DialogueConfirmation(modeleChoixAdresseChantier);
        vueChoixAdresseChantier.afficher();
        
        // Utiliser l'adresse de livraison du chantier si l'utilisateur répond oui
        if (modeleChoixAdresseChantier.isSortieAvecValidation()) {
          // Il faut cloner l'adresse car l'utilisateur peut la modifier et il ne faut pas que cela soit celle du chantier
          documentVenteEnCours.getTransport().setAdresseLivraison(chantier.getAdresse().clone());
          rechercherZoneGeographique(documentVenteEnCours);
        }
      }
      else {
        // Utiliser l'adresse de livraison du chantier
        // Il faut cloner l'adresse car l'utilisateur peut la modifier et il ne faut pas que cela soit celle du chantier
        documentVenteEnCours.getTransport().setAdresseLivraison(chantier.getAdresse().clone());
        rechercherZoneGeographique(documentVenteEnCours);
      }
    }
    
    // Vérifier si les informations de livraison actuelles sont différentes de celle du chantier
    String informationLivraisonChantier = chargerInformationsLivraison(chantier);
    if (!informationLivraisonChantier.isEmpty()
        && !informationLivraisonChantier.equals(documentVenteEnCours.getTexteLivraisonEnlevement())) {
      // Vérifier si l'utilisateur a déjà saisi des informations de livraison sur le document en cours
      if (!documentVenteEnCours.getTexteLivraisonEnlevement().trim().isEmpty()) {
        // Demander confirmation à l'utilisateur
        ModeleDialogueConfirmation modeleChoixInfosLivraisonChantier =
            new ModeleDialogueConfirmation(getSession(), "Modification des informations de livraison",
                "Le chantier comporte des informations de livraison mais vous en avez déjà saisies. "
                    + "Souhaitez-vous utiliser les informations de livraison du chantier pour le document en cours ?");
        DialogueConfirmation vueChoixInfosLivraisonChantier = new DialogueConfirmation(modeleChoixInfosLivraisonChantier);
        vueChoixInfosLivraisonChantier.afficher();
        
        // Si l'utilisateur répond oui, on utilise les informations de livraison du chantier dans le document de livraison
        if (modeleChoixInfosLivraisonChantier.isSortieAvecValidation()) {
          documentVenteEnCours.setTexteLivraisonEnlevement(chargerInformationsLivraison(chantier));
        }
      }
      else {
        documentVenteEnCours.setTexteLivraisonEnlevement(chargerInformationsLivraison(chantier));
      }
    }
  }
  
  /**
   * Modifier le chantier.
   * @param Chantier pChantier
   */
  public void modifierChantier(Chantier pChantier) {
    // Tester si la valeur a changé
    if (Constantes.equals(pChantier, chantier)) {
      return;
    }
    
    // Modifier le chantier
    chantier = pChantier;
    
    // Modifier le chantier du document de livraison
    if (chantier != null) {
      documentVenteEnCours.setIdChantier(chantier.getId());
      documentVenteEnCours.setLibelleChantier(pChantier.getLibelle());
    }
    else {
      documentVenteEnCours.setIdChantier(null);
      documentVenteEnCours.setLibelleChantier(null);
    }
    
    // Ajouter les informations chantier pour les documents de livraison,
    if (chantier != null && documentVenteEnCours.isLivraison()) {
      ajouterInformationChantier();
    }
    
    // Rafraîchir la vue
    rafraichir();
  }
  
  /**
   * Lecture des informations de livraison du chantier passé en paramètre
   */
  private String chargerInformationsLivraison(Chantier pChantier) {
    // Contrôler les parmaètres
    if (pChantier == null || pChantier.getId() == null) {
      return "";
    }
    
    // Charger le mémo "Livraison" du chantier
    Memo memoInfosLivraison = Memo.charger(getIdSession(), pChantier.getId(), EnumTypeMemo.LIVRAISON);
    if (memoInfosLivraison == null) {
      return "";
    }
    
    // Retourner le texte du mémo
    return memoInfosLivraison.getTexte();
  }
  
  /**
   * Contôler que l'on peut utiliser le composant de sélection des adresses de livraison chantier.
   */
  public boolean isSelectionAdresseChantier() {
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison() || documentVenteEnCours.getIdChantier() == null) {
      return false;
    }
    return true;
  }
  
  /**
   * Effectuer les contrôles et traitement lors de la sortie de l'onglet Livraison/Enlèvement.
   */
  private boolean controlerOngletLivraison() {
    // Contrôler la présence d'un document de ventes
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de contrôler l'onglet livraison/enlèvement car le document de ventes est invalide");
    }
    
    // Ne pas faire de contrôle si le document n'est pas modifiable
    if (!documentVenteEnCours.isModifiable() || (documentVenteEnCours.isFacture() && !documentVenteEnCours.isEnCoursCreation())) {
      return true;
    }
    
    // Contrôler le mode d'expédition
    if (documentVenteEnCours.isModeExpeditionNonDefini()) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("Merci de définir si le document est une livraison ou un enlèvement.");
    }
    
    boolean isClientRefCourteObligatoire = false;
    boolean isClientRefLongueObligatoire = false;
    // Charger si les références sont obligatoires pour le client en cours
    if (clientCourant != null) {
      isClientRefCourteObligatoire = clientCourant.isRefCourteCommandeEstObligatoire();
      isClientRefLongueObligatoire = clientCourant.isRefLongueCommandeEstObligatoire();
    }
    
    // Chargement de la valeur de la PS157 (Référence client obligatoire)
    Character valeurPS157 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS157);
    
    // Si la référence courte n'est pas renseignée
    if (documentVenteEnCours.getReferenceCourte() == null || documentVenteEnCours.getReferenceCourte().equals("")) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      // message si PS157 à 1, 2 ou 3
      if (valeurPS157.equals('1') || valeurPS157.equals('2') || valeurPS157.equals('3')) {
        throw new MessageErreurException("Merci de saisir la référence courte du document (PS157).");
      }
      // message si la référence courte est paramétrée comme obligatoire sur la fiche du client
      else if (isClientRefCourteObligatoire) {
        throw new MessageErreurException(
            "Merci de saisir la référence courte du document.\nElle est obligatoire pour le client en cours.");
      }
    }
    
    // Chargement de la valeur de la PS329 (Gestion référence longue dans document de vente)
    boolean ps329 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS329);
    
    // Si la référence longue n'est pas renseignée
    if (documentVenteEnCours.getReferenceLongue() == null || documentVenteEnCours.getReferenceLongue().equals("")) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      // message si PS329 à 1
      if (ps329) {
        throw new MessageErreurException("Merci de saisir la référence longue du document (PS329).");
      }
      // message si PS157 à 3
      else if (valeurPS157.equals('3')) {
        throw new MessageErreurException("Merci de saisir la référence longue du document (PS157).");
      }
      // message si la référence longue est paramétrée comme obligatoire sur la fiche du client
      else if (isClientRefLongueObligatoire) {
        throw new MessageErreurException(
            "Merci de saisir la référence longue du document.\nElle est obligatoire pour le client en cours.");
      }
    }
    
    // Contrôler le code de facturation
    if (documentVenteEnCours.getIdTypeFacturation() == null) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("Le type de facturation est obligatoire.");
    }
    
    // Contrôler si un chantier est obligatoire
    if (isChantierEstObligatoire() && documentVenteEnCours.getIdChantier() == null) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("Le chantier est obligatoire.");
    }
    
    // Contrôle pour "pris par" obligatoire
    if (isPrisParObligatoire() && documentVenteEnCours.isModifiable()) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("L'information \"Pris par\" est obligatoire.");
    }
    
    // Contrôle pour immatriculation obligatoire
    if (isVehiculeObligatoire() && documentVenteEnCours.isModifiable()) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("L'immatriculation est obligatoire.");
    }
    
    // Contrôle pour date de validité de devis obligatoire
    if (isDateValiditeDevisObligatoire() && documentVenteEnCours.isDevis() && documentVenteEnCours.getDateValiditeDevis() == null) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("La date de validité du devis est obligatoire.");
    }
    
    // Contrôles pour la date de relance de devis
    if (documentVenteEnCours.isDevis()) {
      // Controle si la date de relance est obligatoire
      if (isDateRelanceDevisObligatoire() && documentVenteEnCours.getDateRelanceDevis() == null) {
        setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
        throw new MessageErreurException("La date de relance du devis est obligatoire.");
      }
      if (documentVenteEnCours.getDateRelanceDevis() != null && documentVenteEnCours.getDateRelanceDevis().before(dateTraitement)) {
        setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
        throw new MessageErreurException(
            String.format("La date de relance saisie est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
      }
    }
    
    // Contrôle pour date de commande obligatoire
    if (isDateCommmandeObligatoire() && documentVenteEnCours.isCommande()) {
      setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
      throw new MessageErreurException("La date de commande est obligatoire.");
    }
    
    // Contrôle de la date de livraison souhaitée
    if (dateLivraisonSouhaiteeInvalide) {
      throw new MessageErreurException(
          String.format("La date de livraison souhaitée est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
    }
    
    // Contrôle de la date de livraison prévue
    if (dateLivraisonPrevueInvalide) {
      throw new MessageErreurException(
          String.format("La date de livraison prévue est antérieure à la date du document %1$td/%1$tm/%1$tY", dateTraitement));
    }
    
    return true;
  }
  
  /**
   * Valider l'onglet Livraison.
   */
  public void validerOngletLivraison() {
    // Valider l'onglet livraison
    if (!controlerOngletLivraison()) {
      rafraichir();
      return;
    }
    
    // Changer l'onglet
    setIndiceOnglet(ONGLET_ARTICLE);
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet article
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Modifier le texte de recherche.
   * Cela lance automatiquement une recherche article.
   */
  public void modifierTexteRecherche(String pTexteRecherche) {
    // Même si la valeur est identique à la fois précédente, on lance une recherche.
    texteRecherche = Constantes.normerTexte(pTexteRecherche);
    
    // Lancer une recherche différente suivant le mode
    switch (etape) {
      case ETAPE_RECHERCHE_ARTICLE_PALETTE:
        rechercherArticlePalette();
        return;
      
      case ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
        rechercherArticleCommentaire();
        return;
      
      case ETAPE_RECHERCHE_ARTICLE_TRANSPORT:
        rechercherArticleTransport();
        return;
      
      default:
        rechercherArticleStandard();
        return;
    }
  }
  
  /**
   * Modifier l'option de recherche article gamme/hors gamme.
   */
  public void modifierFiltreHorsGamme(EnumFiltreHorsGamme pFiltreHorsGamme) {
    if (filtreHorsGamme == pFiltreHorsGamme) {
      return;
    }
    
    filtreHorsGamme = pFiltreHorsGamme;
    
    // Relancer une recherche si une recherche est en cours
    if (texteRecherche != null && !texteRecherche.isEmpty()) {
      rechercherArticleStandard();
      return;
    }
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier la zone géographique.
   */
  public void modifierZoneGeographique(ZoneGeographique pZoneGeographique) {
    // Vérifier les pré-requis
    if (documentVenteEnCours == null || !documentVenteEnCours.isLivraison() || documentVenteEnCours.getTransport() == null) {
      throw new MessageErreurException(
          "Le document n'est pas de type livraison.\n" + "Revenez sur l'onglet Enlèvement/Livraison et sélectionner \"Livraison\".");
    }
    
    // Convertir la valeur fournie dans la valeur attendue
    IdZoneGeographique idZoneGeographique = null;
    if (pZoneGeographique != null) {
      idZoneGeographique = pZoneGeographique.getId();
    }
    
    // Vérifier si la valeur a changée
    Transport transport = documentVenteEnCours.getTransport();
    if (Constantes.equals(transport.getIdZoneGeographique(), idZoneGeographique)) {
      return;
    }
    
    // Mémoriser la nouvelle valeur
    transport.setIdZoneGeographique(idZoneGeographique);
    
    // Sauver le document de ventes sinon on perd l'information lors du chiffrage (il y a relecture de l'entête)
    sauverEnteteDocumentVente(documentVenteEnCours);
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    
    // Refaire immédiatement une recherche de transport si on modifie la zone géographique en mode "Recherche transport"
    if (etape == ETAPE_RECHERCHE_ARTICLE_TRANSPORT) {
      rechercherArticleTransport();
    }
    // Rafraichir sinon
    else {
      rafraichir();
    }
  }
  
  /**
   * Modifier le pourcentage de la remise à appliquer au résultat de la recherche.
   * 
   * Il n'est pas possible de saisir simultanément une colonne tarif et une remise forcée. Du coup, la colonne tarifaire est effacée.
   * Les données des articles sont invalidées afin d'être rechargées avec de nouveaux prix.
   * 
   * @param pTauxRemiseForcee Pourcentage de remise sous forme textuelle.
   */
  public void modifierRemiseForcee(String pTauxRemiseForcee) {
    // Convertir la valeur saisie (la saisie de 0 équivaut à la valeur null)
    BigPercentage nouveauTauxRemiseForcee =
        Constantes.convertirTexteEnBigPercentage(pTauxRemiseForcee, EnumFormatPourcentage.POURCENTAGE);
    if (nouveauTauxRemiseForcee.isZero()) {
      nouveauTauxRemiseForcee = null;
    }
    
    // Vérifier si la valeur a changé
    if (Constantes.equals(nouveauTauxRemiseForcee, tauxRemiseForcee)) {
      return;
    }
    
    // Renseigner la nouvelle valuer
    tauxRemiseForcee = nouveauTauxRemiseForcee;
    
    // Effacer la colonne tarifaire forcée car il n'est pas possible de saisir simultanément une colonne tarifaire et une remise forcée
    numeroColonneTarifForcee = null;
    
    // Invalider les lignes du résultat de la recherche pour forcer leur rechargement
    if (listeResultatRechercheArticle != null) {
      for (ResultatRechercheArticle resultatRechercheArticle : listeResultatRechercheArticle) {
        resultatRechercheArticle.setInformationCharge(false);
      }
      
      // Recharger les articles visibles
      chargerPlageResultatRechercheArticle(indexPremiereLigneAffichee, nombreLigneAffichee);
    }
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier le numéro de la colonne tarif forcée.
   * 
   * Il n'est pas possible de saisir simultanément une colonne tarif et une remise forcée. Du coup, la remise forcée est effacée.
   * Les données des articles sont invalidées afin d'être rechargées avec de nouveaux prix.
   * 
   * @param pNumeroColonneTarifForcee Numero de colonne tarif.
   */
  public void modifierNumeroColonneTarifForcee(Integer pNumeroColonneTarifForcee) {
    // Vérifier si la valeur a changé
    if (Constantes.equals(pNumeroColonneTarifForcee, numeroColonneTarifForcee)) {
      return;
    }
    
    // Modifier la colonne tarifaire
    // On ne peut pas saisir une colonne tarifaire inférieure à la colonne tarifaire du client
    if (clientCourant.getNumeroColonneTarif() != null && pNumeroColonneTarifForcee != null && pNumeroColonneTarifForcee > 0
        && clientCourant.getNumeroColonneTarif() > pNumeroColonneTarifForcee) {
      numeroColonneTarifForcee = clientCourant.getNumeroColonneTarif();
    }
    else {
      numeroColonneTarifForcee = pNumeroColonneTarifForcee;
    }
    
    // Effacer la remise forcée car il n'est pas possible de saisir simultanément une colonne tarifaire et une remise forcée
    tauxRemiseForcee = null;
    
    // Invalider les lignes du résultat de la recherche pour forcer leur rechargement
    if (listeResultatRechercheArticle != null) {
      for (ResultatRechercheArticle resultatRechercheArticle : listeResultatRechercheArticle) {
        resultatRechercheArticle.setInformationCharge(false);
      }
      
      // Recharger les articles visibles
      chargerPlageResultatRechercheArticle(indexPremiereLigneAffichee, nombreLigneAffichee);
    }
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Modifier l'article sélectionné dans la liste du résultat de la recherche.
   */
  public void modifierArticleSelectionne(int pIndexLigne) {
    if (pIndexLigne == indexArticleSelectionne) {
      return;
    }
    
    indexArticleSelectionne = pIndexLigne;
    rafraichir();
  }
  
  /**
   * Modifier la ligne de vente sélectionnée.
   */
  public void modifierLigneVenteSelectionnee(int pIndexLigne) {
    if (pIndexLigne == indexLigneVenteSelectionnee) {
      return;
    }
    
    indexLigneVenteSelectionnee = pIndexLigne;
    rafraichir();
  }
  
  /**
   * Extraire la quantité de la saisie.
   * Méthode utilitaire pour récupérer ou déduire la quantité saisie par l'utilisateur.
   */
  private BigDecimal extraireQuantiteSaisie(String pSaisie, Pattern pPattern) {
    // Contrôler les paramètres
    if (pSaisie == null || pPattern == null) {
      return null;
    }
    
    // Controler que la saisie corresponde à une saisie valide (chiffre et/ou lettre)
    Matcher m = pPattern.matcher(pSaisie);
    if (!m.matches()) {
      return null;
    }
    
    // Retourner le nombre saisi s'il y en a un
    String nombreTrouve = Constantes.normerTexte(m.group(1));
    if (nombreTrouve != null && !nombreTrouve.isEmpty()) {
      return Constantes.convertirTexteEnBigDecimal(nombreTrouve);
    }
    
    // Retourner 1 si une lettre a été saisie
    String lettreTrouve = Constantes.normerTexte(m.group(2));
    if (lettreTrouve != null && !lettreTrouve.isEmpty()) {
      return BigDecimal.ONE;
    }
    
    return null;
  }
  
  /**
   * Extraire la lettre de la saisie.
   * Méthode utilitaire pour récupérer la lettre saisie par l'utilisateur.
   */
  private Character extraireLettreSaisie(String pSaisie, Pattern pPattern) {
    // Contrôler les paramètres
    if (pSaisie == null || pPattern == null) {
      return null;
    }
    
    // Controler que la saisie corresponde à une saisie valide (chiffre et/ou lettre)
    Matcher m = pPattern.matcher(pSaisie);
    if (!m.matches()) {
      return null;
    }
    
    // Retourner la lettre saisie s'il y en a une
    String lettreTrouve = Constantes.normerTexte(m.group(2));
    if (lettreTrouve != null && !lettreTrouve.isEmpty()) {
      return lettreTrouve.charAt(0);
    }
    
    return null;
  }
  
  /**
   * Modifier la quantité d'un article dans le résultat de la recherche.
   * 
   * Cette méthode est un aiguillage. Elle se charge de dispatcher le travail à la méthode adaptée à l'article et au mode de saisie.
   * 
   * @param pSaisie Valeur saisie.
   * @param pIndexLigneModifiee Index de la ligne modifiée.
   * @param pIndexLigneInseree Index de la ligne insérée.
   */
  public void modifierQuantiteArticle(String pSaisie, int pIndexLigneModifiee, int pIndexLigneInseree) {
    // Normer la saisie en minuscules afin de simplifier l'analyse
    pSaisie = Constantes.normerTexte(pSaisie).toLowerCase();
    
    // Effacer la recherche en cours si rien n'est saisit dans la quantité
    if (pSaisie.isEmpty()) {
      effacerListeArticleResultatRecherche();
      setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
      texteRecherche = "";
      rafraichir();
      return;
    }
    
    // Supprimer systématiquement la sélection dans la liste de la ligne de vente lors de l'insertion d'une ligne
    indexLigneVenteSelectionnee = -1;
    
    // Récupérer l'identifiant de l'article sélectionné
    IdArticle idArticle = null;
    if (etape == ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      if (listeResultatRechercheArticlePalette != null && listeResultatRechercheArticlePalette.get(pIndexLigneModifiee) != null) {
        idArticle = listeResultatRechercheArticlePalette.get(pIndexLigneModifiee).getId().getIdArticle();
      }
    }
    else {
      if (listeResultatRechercheArticle != null && listeResultatRechercheArticle.get(pIndexLigneModifiee) != null) {
        idArticle = listeResultatRechercheArticle.get(pIndexLigneModifiee).getId().getIdArticle();
      }
    }
    if (idArticle == null) {
      throw new MessageErreurException("Aucune article n'est associé à la ligne sélectionnée.");
    }
    
    // Charger l'article correspondant
    Article article = new Article(idArticle);
    article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
    
    // Pour les directs usines, il n'est pas possible d'ajouter un article sans CNA valide
    if (documentVenteEnCours.isDirectUsine()) {
      ConditionAchat conditionAchat = ManagerServiceArticle.chargerConditionAchat(getIdSession(), article.getId(), dateTraitement);
      if (!article.isArticleCommentaire() && (conditionAchat == null || conditionAchat.getDateApplication().after(dateTraitement))) {
        throw new MessageErreurException("Vous ne pouvez pas ajouter l'article " + article.getId().getCodeArticle()
            + " au document en cours. Dans un document direct usine on ne peut pas ajouter d'article "
            + "sans condition d'achat en cours de validité.\nMerci de vérifiez la condition d'achat de cet article si vous souhaitez "
            + "l'ajouter à ce document.");
      }
    }
    
    // Interpréter la saisie (pour dispatcher)
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_ARTICLE);
    Character lettre = extraireLettreSaisie(pSaisie, PATTERN_SAISIE_ARTICLE);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException("La saisie est invalide : " + pSaisie);
    }
    
    // Tester si c'est un article standard
    if (article.isArticleStandard()) {
      // Tester si c'est une saisie en UCS pour un article standard
      if (Constantes.equals(lettre, 'p') || Constantes.equals(lettre, 'c')) {
        ajouterArticleStandardAvecP(pSaisie, article, pIndexLigneInseree);
      }
      // Tester si c'est un retour d'article standard
      else if (quantite.compareTo(BigDecimal.ZERO) < 0) {
        ajouterRetourArticleStandard(pSaisie, article);
      }
      // C'est un ajout d'article standard
      else {
        ajouterArticleStandard(pSaisie, article, pIndexLigneInseree);
      }
    }
    // Tester si c'est un article commentaire
    else if (article.isArticleCommentaire()) {
      ajouterArticleCommentaire(pSaisie, article, pIndexLigneInseree);
    }
    // Tester si c'est un article palette
    else if (article.isArticlePalette()) {
      // Tester si c'est un retour d'article palette
      if (quantite.compareTo(BigDecimal.ZERO) < 0) {
        ajouterRetourArticlePalette(pSaisie, article, pIndexLigneInseree);
      }
      // C'est un ajout d'article palette
      else {
        ajouterArticlePalette(pSaisie, article, pIndexLigneInseree);
      }
    }
    // Tester si c'est un article transport
    else if (article.isArticleTransport()) {
      ajouterArticleTransport(pSaisie, article, pIndexLigneInseree);
    }
    // Cas non traité
    else {
      throw new MessageErreurException("Il est impossible d'ajouter un article de type " + article.getTypeArticle().getLibelle()
          + " au comptoir.\nVous ne pouvez utiliser que les articles négoce, commentaires, palettes, transports ou spéciaux.\n"
          + "Pour la saisie d'un autre type d'article, merci de créer votre document de ventes par le menu de saisie classique.");
    }
  }
  
  /**
   * Contrôle les lignes d'un document.
   * TODO Voir avec Marc si on le fait en java ou s'il doit le refaire fonctionner en RPG.
   */
  private AlertesLigneDocument controlerLigneDocument(LigneVente pLigneVente) {
    if (pLigneVente.isLigneCommentaire() || pLigneVente.isLignePalette()) {
      return null;
    }
    
    try {
      return ManagerServiceDocumentVente.controlerLigneDocumentVente(getSession().getIdSession(), pLigneVente, dateTraitement);
    }
    catch (MessageErreurException e) {
      if (e.getCause() != null && e.getCause().getLocalizedMessage() != null) {
        AlertesLigneDocument alerte = new AlertesLigneDocument();
        alerte.setMessages(e.getCause().getLocalizedMessage());
        if (!isVenteSousPRV() && alerte.getMessages().contains("Prix de Revient")) {
          payerSousPRV(pLigneVente);
          return alerte;
        }
        else if (!isVenteSousPRV() && alerte.getMessages().contains("% Remise >")) {
          return alerte;
        }
      }
      throw e;
    }
  }
  
  /**
   * Permet de valider prix sous le PRV.
   */
  public void payerSousPRV(LigneVente pLigneVente) {
    setVenteSousPRV(isAutorisationVenteSousPRV(pLigneVente));
    if (!venteSousPRV) {
      return;
    }
    
    rafraichir();
  }
  
  /**
   * Créer une ligne de vente à partir d'un article afin de l'insérer dans un document de vente.
   * @return LigneVente La ligne de vente créée.
   */
  public LigneVente ajouterArticleDansDocumentVente(Article pArticle, BigDecimal pQuantite, EnumTypeQuantite pEnumTypeQuantite,
      int pIndiceTableau) {
    messageArticle = "";
    
    setVenteSousPRV(false);
    
    // Si l'UCV de l'article n'est pas scindable on arrondi la quantité saisie
    if (pEnumTypeQuantite.equals(EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES) && pArticle.isUniteConditionnementVenteNonScindable()) {
      pQuantite = pQuantite.setScale(listeUnite.getPrecisionUnite(pArticle.getIdUCV()), RoundingMode.HALF_UP);
    }
    
    // Ajouter la ligne article au document de ventes
    LigneVente ligneVente = documentVenteEnCours.ajouterLigneArticle(getSession().getIdSession(), pArticle, pQuantite, pEnumTypeQuantite,
        pIndiceTableau, dateTraitement);
    
    // Renseigner les paramètres de la ligne de vente
    ParametreLigneVente parametreLigneVente = new ParametreLigneVente();
    parametreLigneVente.setQuantiteUV(ligneVente.getQuantiteUV());
    if (numeroColonneTarifForcee != null) {
      parametreLigneVente.setNumeroColonneTarif(numeroColonneTarifForcee);
      parametreLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
    }
    if (tauxRemiseForcee != null) {
      parametreLigneVente.setTauxRemise1(tauxRemiseForcee);
      parametreLigneVente.setProvenanceTauxRemise(EnumProvenanceTauxRemise.SAISIE_LIGNE_VENTE);
    }
    
    // Renseigner les données concernant les prix
    // Appel au programme de calcul de prix avec la quantité saisie
    ParametreChargement parametreChargement = new ParametreChargement();
    parametreChargement.setIdEtablissement(documentVenteEnCours.getId().getIdEtablissement());
    parametreChargement.setParametreLigneVente(parametreLigneVente);
    parametreChargement.setIdMagasin(getIdMagasin());
    parametreChargement.setIdArticle(pArticle.getId());
    parametreChargement.setIdClientFacture(documentVenteEnCours.getIdClientFacture());
    parametreChargement.setIdDocumentVente(documentVenteEnCours.getId());
    parametreChargement.setIdChantier(documentVenteEnCours.getIdChantier());
    CalculPrixVente calculPrixVente = ManagerServiceArticle.calculerPrixVente(getIdSession(), parametreChargement);
    
    // Mise à jour des données de prix dans la ligne de ventes
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Proposer un surconditionnement si la quantité commandée en est proche
    proposerSurconditionnement(pArticle, ligneVente);
    
    // Controle des données saisies
    AlertesLigneDocument alertesLigneDocument = controlerLigneDocument(ligneVente);
    if (alertesLigneDocument != null && !alertesLigneDocument.getMessages().trim().isEmpty()) {
      ligneVente.setAlertesLigneDocument(null);
      return ligneVente;
    }
    
    return ligneVente;
  }
  
  /**
   * Propose un surconditionnement si besoin.
   */
  private void proposerSurconditionnement(Article pArticle, LigneVente pLigneVente) {
    if (pArticle == null || pArticle.isArticleSpecial()) {
      return;
    }
    // Lancement de la boite de dialogue qui propose de commander davantage
    ModeleProposerSurconditionnement modele = new ModeleProposerSurconditionnement(getSession(), pArticle, pLigneVente);
    // On teste si la quantité commandée doit déclencher une proposition
    if (modele.isProposable()) {
      VueProposerSurconditionnement vue = new VueProposerSurconditionnement(modele);
      vue.afficher();
      
      // Traitement si l'utilisateur a validé les modifications
      if (modele.isSortieAvecValidation()) {
        // si on a choisi la quantité supérieure
        if (modele.isSaisieSurconditionnement()) {
          pLigneVente.setQuantiteUCV(modele.getQuantiteCalculee());
        }
      }
    }
  }
  
  /**
   * Propose une nouvelle saisie d'article à dimension variable
   */
  private void ecrireNouvelleLigneDimensionVariable(Article pArticle) {
    // Contrôler les données de l'articles
    // pArticle.controler();
    if (pArticle.isArticleStandard()) {
      controlerArticleStandardOuTransport(pArticle);
    }
    
    // Afficher plusieurs fois la boîte de dialogue (une fois par taille)
    LigneVente ligneVente = null;
    ModeleDetailLigneArticle modele = null;
    do {
      // Insérer l'article dans le document de vente
      ligneVente = ajouterArticleDansDocumentVente(pArticle, BigDecimal.ONE, EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES,
          documentVenteEnCours.getListeLigneVente().size() + 1);
      
      // Créer la ligne de ventes
      ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
      
      // Recharger le document de ventes suite aux ajouts de ligne
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      ligneVente = documentVenteEnCours.getLigneVente(ligneVente.getId());
      
      // On instancie une nouvelle boite de dialogue
      modele = new ModeleDetailLigneArticle(getSession(), EnumModeDetailLigneVente.CREATION, this, documentVenteEnCours, ligneVente,
          clientCourant, pArticle, ModeleDetailLigneArticle.ONGLET_DIMENSIONS);
      VueDetailLigneArticle vue = new VueDetailLigneArticle(modele);
      vue.afficher();
      
      // Si on sort en validant, on enregistre les modifications et on relance une nouvelle fenêtre
      if (modele.isSortieAvecValidation() && modele.getNombreDecoupe() > 0) {
        ManagerServiceDocumentVente.modifierLignedocumentVente(getSession().getIdSession(), ligneVente);
      }
      
      rafraichir();
    }
    while (modele.isSortieAvecValidation());
    
    // Supprimer la ligne créée pour rien
    ListeLigneVente listeLigneVente = new ListeLigneVente();
    listeLigneVente.add(documentVenteEnCours.getLigneVente(ligneVente.getId()));
    supprimerListeLigneVente(listeLigneVente);
  }
  
  /**
   * Contrôle l'encours.
   */
  public boolean controlerEncoursClient() {
    // On controle si le document n'est pas un devis et si le paiement n'est pas comptant
    encoursDocument = null;
    saisieArticlePossible = true;
    
    if (!documentVenteEnCours.isDevis() && !isModePaiementComptant()) {
      encoursDocument = ManagerServiceClient.controlerEncoursClient(getSession().getIdSession(), documentVenteEnCours,
          clientCourant.getCollectifComptable());
      
      if (encoursDocument.getAlertesEncoursClient().controlerAlertes()) {
        String message = encoursDocument.getAlertesEncoursClient().getMessages();
        if (encoursDocument.getAlertesEncoursClient().isPlafondDepasseDemandeAutorisationGeneree()) {
          // Il faut griser les onglets Règlements et Edition pour bloquer l'utilisateur
          indiceOngletAccessible = indiceOngletCourant;
          // Construction du message
          message += "\n";
          if (encoursClient.getPlafondExceptionnel() > 0) {
            message += "\nLe plafond exceptionnel pour ce client est de " + encoursClient.getPlafondExceptionnel() + "\u20AC";
          }
          else {
            message += "\nLe plafond maximum pour ce client est de " + encoursDocument.getPlafond() + "\u20AC";
          }
          message += " \nL'encours dépasse ce plafond d'un montant de " + encoursDocument.getDepassement() + "\u20AC";
          if (encoursClient.getEncours() > 0) {
            message += " \n(compte tenu des " + encoursClient.getEncours() + "\u20AC d'encours déjà utilisés)";
          }
        }
        DialogueInformation.afficher(message);
        
        // Gestion saisie suivant PS300 (si valeur 2 on bloque la saisie)
        Character ps300 =
            ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS300);
        if (ps300.equals('2')) {
          saisieArticlePossible = false;
        }
        else {
          saisieArticlePossible = true;
        }
      }
    }
    
    return true;
  }
  
  /**
   * Traitement dans le cas d'une saisie sur une ligne article (modification d'une ligne déjà existante).
   * Il va falloir faire le ménage la dedans...
   * Idée : duppliquer en une méthode creerLigneVente() et une méthode modifierLigneVente() car la méthode selectionnerLigneVente est
   * appelée lors des créations. Possible de faire du ménage ensuite.
   */
  public void selectionnerLigneVente(LigneVente pLigneVente, int pIndiceOngletDetail, int pTableAppelante, Article pArticle,
      boolean pEnAjout) {
    ModeleDetailLigneArticle modele = null;
    boolean validation = false;
    
    if (pArticle == null) {
      pArticle = new Article(pLigneVente.getIdArticle());
      pArticle = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), pArticle);
    }
    
    if (pArticle != null && pArticle.isArticleSpecial()) {
      selectionnerLigneVenteArticleSpecial(pLigneVente, 0);
      return;
    }
    // La boite de dialogue n'est pas affichée (il s'agit juste d'une saisie de quantité dans la liste)
    else if (pIndiceOngletDetail == ModeleDetailLigneArticle.NE_PAS_AFFICHER) {
      validation = true;
    }
    // Affichage de la boite de dialogue du détail de la ligne
    else {
      // Déterminer si la boîte de dialogue est appelée en mode création ou modification
      EnumModeDetailLigneVente modeDetailLigneVente = null;
      if (pTableAppelante == TABLE_RESULTAT_RECHERCHE_ARTICLES) {
        modeDetailLigneVente = EnumModeDetailLigneVente.CREATION;
      }
      else {
        modeDetailLigneVente = EnumModeDetailLigneVente.MODIFICATION;
      }
      
      // Afficher la boîte de dialogue
      modele = new ModeleDetailLigneArticle(getSession(), modeDetailLigneVente, this, documentVenteEnCours, pLigneVente, clientCourant,
          pArticle, pIndiceOngletDetail);
      VueDetailLigneArticle vue = new VueDetailLigneArticle(modele);
      vue.afficher();
      
      // Si le document n'est pas modifiable, on en reste là !
      if (!documentVenteEnCours.isModifiable()) {
        return;
      }
      // On sort de la boite de dialogue en validant et on met à jour la ligne
      if (modele.isSortieAvecValidation() && modele.getQuantiteUV().compareTo(BigDecimal.ZERO) != 0) {
        validation = true;
        
        // Propose un surconditionnement si la quantité commandée en est proche
        proposerSurconditionnement(modele.getArticle(), pLigneVente);
        ManagerServiceDocumentVente.modifierLignedocumentVente(getSession().getIdSession(), pLigneVente);
      }
    }
    
    // Mise à jour des articles du document
    if (modele != null && validation) {
      // Controle des données saisies
      AlertesLigneDocument alertesLigneDocument = controlerLigneDocument(modele.getLigneVente());
      if (alertesLigneDocument != null && alertesLigneDocument.controlerAlertes()) {
        modele.getLigneVente().setAlertesLigneDocument(null);
        throw new MessageErreurException(alertesLigneDocument.getMessages());
      }
    }
    
    // S'il s'agit d'une ligne appartenant à un regroupement on affiche la boite de dialogue des Regroupements
    if (pEnAjout && pLigneVente != null && pLigneVente.isLigneRegroupee()) {
      Regroupement regroupement = documentVenteEnCours.getListeRegroupements().get(pLigneVente.getCodeRegroupement());
      afficherDetailRegroupement(regroupement);
    }
    
    // Insertion d'un article à dimension variable
    if (modele != null && modele.getTypeDimension().compareTo(EnumTypeDecoupeArticle.IMPOSSIBLE) > 0
        && modele.getQuantiteUV().compareTo(BigDecimal.ZERO) > 0 && validation
        && (pIndiceOngletDetail != ModeleDetailLigneArticle.NE_PAS_AFFICHER)) {
      ecrireNouvelleLigneDimensionVariable(modele.getArticle());
    }
    
    // Traitement en sortie de la boite de dialogue
    if (pTableAppelante == TABLE_RESULTAT_RECHERCHE_ARTICLES) {
      if (validation) {
        if (pArticle != null && listeResultatRechercheArticle != null) {
          // Cas du document direct usine (mais pas multi-fournisseur), si c'est le premier article alors on supprime le résultat
          // (pour être sur qu'il n'y ait pas des fournisseurs différents)
          if (documentVenteEnCours.isDirectUsine() && !isDirectUsineEnModeMultiFournisseur()
              && documentVenteEnCours.getListeLigneVente().size() == 1) {
            effacerListeArticleResultatRecherche();
          }
          // Afficher les articles liés à l'article inséré
          else {
            rechercherArticleLiePropose(pArticle);
          }
        }
      }
      else if (pArticle != null) {
        // Suppression des articles visualisés mais pas ajoutés au document : popup détail + annuler (article choisi + ses articles liés)
        CritereArticleLie criteres = new CritereArticleLie(pArticle.getId());
        criteres.setIsArticleValideSeulement(true);
        if (ManagerServiceArticle.verifierExistenceArticleLieAutomatiquement(getSession().getIdSession(), criteres)) {
          ListeArticle listeArticleLieASupprimer =
              ManagerServiceArticle.chargerListeArticleLieAutomatiquement(getSession().getIdSession(), criteres);
          
          // Supprimer l'article principal et ses article sliés
          ListeLigneVente listeLigneVenteASupprimer = new ListeLigneVente();
          listeLigneVenteASupprimer.add(documentVenteEnCours.getLigneVente(pArticle.getId()));
          for (Article articleLieASupprimer : listeArticleLieASupprimer) {
            // Rechercher l'article lié dans le document de vente
            LigneVente ligneVente = documentVenteEnCours.getLigneVente(articleLieASupprimer.getId());
            if (ligneVente != null) {
              listeLigneVenteASupprimer.add(ligneVente);
            }
          }
          supprimerListeLigneVente(listeLigneVenteASupprimer);
          
          // Recherche les articles standards
          rechercherArticleStandard();
        }
        else {
          // Au final, suppression de la ligne créée pour rien
          ListeLigneVente listeLigneVenteASupprimer = new ListeLigneVente();
          listeLigneVenteASupprimer.add(pLigneVente);
          supprimerListeLigneVente(listeLigneVenteASupprimer);
        }
      }
    }
    
    // Contrôle de l'encours
    controlerEncoursClient();
    
    // Conservation du montant TTC du document après toutes opération d'insertion/modification/suppression
    conserverTotalTTC();
    
    // Recalculer les totaux
    try {
      if (validation) {
        controlerDocument(true);
      }
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
    // Permet de rafraichir les données correctement dans le cas où une exception est déclenchée
    finally {
      // Charge les informations d'entête du document de ventes
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      
      // Rafraîchit
      rafraichir();
    }
  }
  
  /**
   * Conserve le total TTC du document.
   */
  public void conserverTotalTTC() {
    // Si le paiement est comptant on met 0 afin de ne pas incrémenter l'encours
    if (modePaiementComptant) {
      documentVenteEnCours.setTotalTTCPrecedent(BigDecimal.ZERO);
    }
    else {
      documentVenteEnCours.setTotalTTCPrecedent(documentVenteEnCours.getTotalTTC());
    }
    Trace.debug(ModeleComptoir.class, "conserverTotalTTC", "TTC", documentVenteEnCours.getTotalTTCOrigine());
  }
  
  /**
   * Supprimer les lignes de ventes correspondant aux lignes indiquées.
   */
  public void supprimerListeLigneVente(int[] pListeIndiceLigneVente) {
    if (documentVenteEnCours == null || documentVenteEnCours.getListeLigneVente() == null) {
      return;
    }
    
    // Générer une liste de lignes de ventes à partir de la liste d'index
    ListeLigneVente listeLigneVenteASupprimer = new ListeLigneVente();
    for (int index : pListeIndiceLigneVente) {
      listeLigneVenteASupprimer.add(documentVenteEnCours.getLigneVente(index));
    }
    
    // Supprimer les lignes
    supprimerListeLigneVente(listeLigneVenteASupprimer);
  }
  
  /**
   * Supprime une liste de lignes articles à partir de leur indice.
   */
  public void supprimerListeLigneVente(ListeLigneVente listeLigneVenteASupprimer) {
    if (documentVenteEnCours == null || documentVenteEnCours.getListeLigneVente() == null || listeLigneVenteASupprimer.isEmpty()) {
      return;
    }
    
    // On controle la liste des lignes articles à supprimer (avec les implications que cela peut avoir sur les regroupements)
    ActionRegroupement action = documentVenteEnCours.supprimerLignesArticles(listeLigneVenteASupprimer);
    if ((action != null) && (action.getAction() == ActionRegroupement.SUPPRIMER)) {
      // Il s'agit de la suppression complète du regroupement
      enleverRegroupement(action.getRegroupement());
    }
    
    // On parcourt la liste à l'envers pour la suppression des lignes articles
    int i = listeLigneVenteASupprimer.size() - 1;
    while (i >= 0) {
      LigneVente ligneVente = listeLigneVenteASupprimer.get(i);
      if (ligneVente != null) {
        // Cas particulier : suppression d'un bon de cour
        if (ligneVente.isLigneBonCour()) {
          Pattern pattern = Pattern.compile("[0-9]+");
          Matcher matcher = pattern.matcher(ligneVente.getLibelle());
          while (matcher.find()) {
            String numero = matcher.group();
            BonCour bonCour =
                ManagerServiceDocumentVente.chargerBonCour(getIdSession(), etablissement.getId(), idMagasin, Integer.parseInt(numero));
            bonCour.setEtatBonCour(EnumEtatBonCour.NON_UTILISE);
            bonCour.setIdDocumentGenere(null);
            bonCour.setDateDelivranceClient(null);
            bonCour.setComplement(null);
            ManagerServiceDocumentVente.modifierBonCour(getIdSession(), bonCour);
          }
        }
        // Cas particulier: suppression des informations de négociation globale
        if (!ligneVente.isLigneCommentaire() && ligneVente.getIdArticle().isArticleSupportRemise()) {
          documentVenteEnCours.setPourcentageRemise1Pied(BigDecimal.ZERO);
          documentVenteEnCours.setRemisePiedExiste(false);
          sauverEnteteDocumentVente(documentVenteEnCours);
          documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
        }
        // Mise à jour des lots : on désaffecte les lots de la ligne supprimée (pour les articles lotis)
        if (isGestionParLot() && (ligneVente.getTopNumSerieOuLot() == 30 || ligneVente.getTopNumSerieOuLot() == 40)) {
          Article article = new Article(ligneVente.getIdArticle());
          article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
          ModeleLot modeleLot = new ModeleLot(getSession(), ligneVente.getId(), article, ligneVente.getQuantiteUCV());
          modeleLot.supprimerSaisieLot();
        }
        
        // Suppression de la ligne article
        ManagerServiceDocumentVente.supprimerLigneDocumentVente(getSession().getIdSession(), ligneVente);
      }
      i--;
    }
    
    Character referenceRegroupement = Character.valueOf(' ');
    if (action != null && action.getAction() == ActionRegroupement.METTRE_A_JOUR) {
      // Il s'agit d'une mise à jour suite à la suppression d'une ou plusieurs lignes
      referenceRegroupement = action.getRegroupement().getReference();
      ecrireRegroupement(action.getRegroupement());
    }
    
    try {
      // Les alertes lors du controle du document sont désactivées lors de la suppression d'une ligne
      controlerDocument(false);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
    // Permet de rafraichir les données correctement dans le cas où une exception est déclenchée
    finally {
      // Recharge le document de ventes
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      
      // Contrôle de l'encours
      controlerEncoursClient();
      
      // Positionne le curseur dans la zone de racherche article
      rafraichir();
      
      // Si une mise à jour d'un regroupement a effectué alors affichage de la boite de dialogue
      if (action != null && action.getAction() == ActionRegroupement.METTRE_A_JOUR) {
        Regroupement regroupement = documentVenteEnCours.getListeRegroupements().get(referenceRegroupement);
        if (regroupement != null) {
          afficherDetailRegroupement(regroupement);
        }
      }
    }
  }
  
  /**
   * Recherche les articles liés à un autre article.
   */
  private void rechercherArticleLiePropose(Article pArticleParent) {
    // Renseigner les critères de recherche
    CritereArticleLie critere = new CritereArticleLie(pArticleParent.getId());
    critere.setIsArticleValideSeulement(true);
    
    // Retourner la liste des articles liés proposés (uniquement) à un article donné
    ListeArticle listeArticlesLies = ManagerServiceArticle.chargerListeArticleLieProposes(getSession().getIdSession(), critere);
    if (listeArticlesLies == null || listeArticlesLies.isEmpty()) {
      return;
    }
    
    // Lister les identifiants des articles dont il faut charger les informations
    List<IdResultatRechercheArticle> listeIdResultatRechercheArticle = new ArrayList<IdResultatRechercheArticle>();
    for (Article article : listeArticlesLies) {
      IdArticle idArticle = IdArticle.getInstance(article.getId().getIdEtablissement(), article.getId().getCodeArticle());
      // TODO voir si nessaire de charger les articles car chargerListeArticleLieProposes fait des trucs
      listeIdResultatRechercheArticle.add(IdResultatRechercheArticle.getInstance(idArticle));
    }
    
    // Renseigner les paramètres nécessaires pour charger les informations des articles
    ParametresLireArticle parametresLireArticle = new ParametresLireArticle();
    parametresLireArticle.setIdEtablissement(etablissement.getId());
    parametresLireArticle.setIdMagasin(idMagasin);
    parametresLireArticle.setIdClient(clientCourant.getId());
    if (documentVenteEnCours != null) {
      parametresLireArticle.setIdDocumentVente(documentVenteEnCours.getId());
      parametresLireArticle.setIdChantier(documentVenteEnCours.getIdChantier());
    }
    
    // Charger les informations des articles
    listeResultatRechercheArticle = ManagerServiceArticle.chargerListeResultatRechercheArticle(getSession().getIdSession(),
        listeIdResultatRechercheArticle, parametresLireArticle);
    
    for (Article articleLie : listeArticlesLies) {
      forcerQuantiteSaisiePalette(articleLie);
    }
    
    messageArticle = "Articles liés proposés pour " + pArticleParent.getLibelleComplet();
    criteresRechercheArticles.setNumeroPage(0);
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RESULTAT_RECHERCHE);
  }
  
  /**
   * Négocier sur l'ensemble du document.
   */
  public void negocierGlobalement() {
    ModeleNegociationGlobaleDocument modele =
        new ModeleNegociationGlobaleDocument(getSession(), this, documentVenteEnCours, clientCourant.getId());
    VueNegociationGlobaleDocument vue = new VueNegociationGlobaleDocument(modele);
    vue.afficher();
    
    // Recharger le document de ventes
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    rafraichir();
  }
  
  /**
   * Retourne si l'on doit afficher la colonne d'origine dans la liste des lignes du document.
   */
  public boolean isAfficherColonneOrigine() {
    return documentVenteEnCours.isAfficherColonneOrigine() || informationsExtraction != null;
  }
  
  /**
   * Formate la liste des lignes articles du document pour l'affichage.
   */
  public String[][] formaterListeArticlesDocument(int pNombreColonnes) {
    int offset = 0;
    
    // En fonction de l'état de traitement du document (modification ou extraction), on ajoute une colonne quantité d'origine
    // La colonne est ajouté en position 1
    if (isAfficherColonneOrigine()) {
      offset++;
    }
    
    // On créé le tableau qui va recevoir les données à afficher dans la liste
    int tailleListeArticles = documentVenteEnCours.getListeLigneVente().size();
    String[][] donnees = new String[tailleListeArticles][pNombreColonnes];
    
    // En fonction du type de la ligne Article, il y a un traitement spécifique
    for (int ligne = 0; ligne < tailleListeArticles; ligne++) {
      LigneVente ligneVente = documentVenteEnCours.getListeLigneVente().get(ligne);
      // Ligne regroupée
      if (ligneVente.isLigneRegroupee()) {
        donnees[ligne] = formaterLigneArticleRegroupee(ligneVente, pNombreColonnes, offset);
      }
      // Ligne commentaire
      else if (ligneVente.isLigneCommentaire()) {
        // Version pourri à voir si ma version n'est pas générique
        // else if ((ligneArticle.getQuantiteCommandeeUC() == 0) && (ligneArticle.getCodeFamille().trim().isEmpty())) {
        donnees[ligne] = formaterLigneArticleCommentaire(ligneVente, pNombreColonnes, offset);
      }
      // Ligne commentaire
      else if (ligneVente.isLigneGratuite()) {
        donnees[ligne] = formaterLigneArticleGratuite(ligneVente, pNombreColonnes, offset);
      }
      // Ligne palette
      else if (ligneVente.isLignePalette()) {
        donnees[ligne] = formaterLigneArticlePalette(ligneVente, pNombreColonnes, offset);
      }
      // Ligne avec quantité négative (lignes quantités négatives == remises ou avoirs)
      else if (ligneVente.getQuantiteUCV().compareTo(BigDecimal.ZERO) < 0) {
        donnees[ligne] = formaterLigneArticleNegative(ligneVente, pNombreColonnes, offset);
      }
      // Ligne normale
      else {
        donnees[ligne] = formaterLigneArticleNormale(ligneVente, pNombreColonnes, offset);
      }
    }
    return donnees;
  }
  
  /**
   * Ajouter un bon de cour au document en cours
   * 
   * Ajoute juste une ligne commentaire pour signifier que l'on saisie une liste d'articles (sous ce commentaire)
   * délivrés au client au parc matériaux. (voir COM-965)
   */
  public void ajouterBonCour(int pLigneInsertion) {
    // Lancer la boite de dialogue qui gère les commentaires
    ModeleAjoutBonCour modeleAjoutBonCour = new ModeleAjoutBonCour(getSession(), documentVenteEnCours, pLigneInsertion);
    VueAjoutBonCour vueAjoutBonCour = new VueAjoutBonCour(modeleAjoutBonCour);
    vueAjoutBonCour.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    if (modeleAjoutBonCour.isSortieAvecAnnulation()) {
      return;
    }
    else if (modeleAjoutBonCour.isSortieAvecValidation()) {
      // Créer la ligne de commentaire
      ecrireLigneCommentaire(modeleAjoutBonCour.retournerCommentaire(), modeleAjoutBonCour.getLigneCommentaireBonCour(),
          documentVenteEnCours, true, false);
      
      // Recharger le document
      if (documentVenteEnCours != null && documentVenteEnCours.getId().getNumero() > 0) {
        documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      }
      rafraichir();
    }
  }
  
  /**
   * Importe les articles d'un plan de pose Sipe.
   * 
   * Affiche une boite de dialogue permettant la sélection du fichier à importer.
   */
  public void importerPlanDePoseSipe() {
    if (documentVenteEnCours == null) {
      return;
    }
    ModeleImportationPlanDePoseSipe modele = new ModeleImportationPlanDePoseSipe(getSession(), documentVenteEnCours.getId());
    VueImportationPlanDePoseSipe vue = new VueImportationPlanDePoseSipe(modele);
    vue.afficher();
    if (!modele.isSortieAvecValidation()) {
      return;
    }
    // Chargement des lignes importées
    documentVenteEnCours = chargerDocumentVente(documentVenteEnCours.getId());
    rafraichir();
  }
  
  /**
   * Faire les opérations nécessaires avant de contrôler les données de l'onglet article.
   */
  private boolean controlerOngletArticle() {
    // Contrôler la présence d'un document de ventes
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de contrôler l'onglet article car le document de ventes est invalide");
    }
    
    // Ne rien contrôler si le document n'est pas modifiable
    if (!documentVenteEnCours.isModifiable() && !documentVenteEnCours.isFacture()) {
      return true;
    }
    
    // Contrôler si tous les lots ont été saisis, dans le cas où cette saisie est obligatoire
    if (isGestionParLot() && isSaisieLotsObligatoire() && !documentVenteEnCours.isDevis()) {
      for (LigneVente ligne : documentVenteEnCours.getListeLigneVente()) {
        // On doit saisir un numéro de lot sur les lignes d'avoir d'articles spéciaux
        if (documentVenteEnCours.isAvoir() && ligne.isLigneArticleSpecial()) {
          Article article = new Article(ligne.getIdArticle());
          article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
          ajouterQuantiteSurLot(ligne, article, ligne.getQuantiteUCV());
        }
        // Si l'affectation sur lots n'est pas complète on ne peut pas sortir
        if (ligne.getTopNumSerieOuLot() == 30) {
          DialogueInformation.afficher("Il est obligatoire de saisir les lots sur l'article " + ligne.getIdArticle().getCodeArticle()
              + "\nOuvrez la fenêtre détail article de cette ligne et cliquez sur le bouton de saisie des lots.");
          return false;
        }
      }
    }
    
    // Contrôler la présence d'un article transport pour un document livraison non direct usine
    if (documentVenteEnCours.isLivraison() && !documentVenteEnCours.isDirectUsine() && !isArticleTransportPresent()
        && isAfficherAlerteArticleTransportNonPresent()) {
      if (!DialogueConfirmation.afficher(getSession(), "Contrôle de la présence d'un article transport",
          "Vous n'avez pas saisi d'article transport pour ce document avec livraison.\n"
              + "Souhaitez-vous continuer sans saisir d'article transport ?")) {
        return false;
      }
    }
    
    // Contrôler si le franco de port n'est pas atteint pour les documents de livraison
    BigDecimal francoPort = BigDecimal.valueOf(clientCourant.getMontantFrancoPort());
    if (documentVenteEnCours.isLivraison() && documentVenteEnCours.isAlerteFrancoPortNonAtteint(getIdSession(), francoPort)) {
      // Demander confirmation à l'utilisateur
      ModeleDialogueConfirmation modeleAlerteFranco = new ModeleDialogueConfirmation(getSession(), "Franco de port non atteint",
          "Le document en cours a un montant HT de " + Constantes.formater(documentVenteEnCours.getTotalHT(), true)
              + " €\nalors que le franco de port du client en cours est de " + clientCourant.getMontantFrancoPort() + " €."
              + "\nSouhaitez-vous ajouter des articles pour atteindre ce franco de port ? ");
      DialogueConfirmation vueAlerteFranco = new DialogueConfirmation(modeleAlerteFranco);
      vueAlerteFranco.afficher();
      if (modeleAlerteFranco.isSortieAvecValidation()) {
        return false;
      }
    }
    
    return true;
  }
  
  /**
   * Valider l'onglet Articles.
   */
  public void validerOngletArticle() {
    // Contrôler les données
    if (!controlerOngletArticle()) {
      rafraichir();
      return;
    }
    
    // Mettre à jour le paiement
    mettreAJourMontantPaiement(documentVenteEnCours);
    
    // Passer directement à l'onglet édition pour un devis
    if (documentVenteEnCours.isDevis()) {
      setIndiceOnglet(ONGLET_EDITION);
      setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    }
    // Passer directement à l'onglet édition pour un client en compte (sauf si règlement comptant)
    else if (clientCourant.isClientEnCompte() && !isModePaiementComptant() && !isAcompteARegler()) {
      setIndiceOnglet(ONGLET_EDITION);
      setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    }
    // Passer directement à l'onglet édition pour un règlement comptant où l'acompte n'est pas obligatoire
    // et qu'il ne s'agisse pas d'un remboursement à cause d'un trop perçu
    else if (isAcompteARegler() && !isAcompteObligatoire()) {
      setIndiceOnglet(ONGLET_EDITION);
      setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    }
    // Passer à l'onglet règlement
    else {
      setIndiceOnglet(ONGLET_REGLEMENT);
      setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    }
    rafraichir();
  }
  
  /**
   * Contrôler si un message d'avertissement doit être affiché dans le cas où aucun article transport n'a été saisi sur une livraison.
   * 
   * On se base sur le paramètre avancé documentvente.controlepresencearticletransport dans la table des paramètres avancés (PSEMPAVM).
   * On renvoie FALSE si :
   * - la valeur du paramètre == false
   * - la valeur du paramètre == 0
   * Sinon on renvoie TRUE.
   */
  private boolean isAfficherAlerteArticleTransportNonPresent() {
    // Valeur définie par le paramètre
    String valeurParametre = ManagerSessionClient.getInstance().getValeurParametreAvance(getIdSession(),
        EnumCleParametreAvance.CONTROLE_PRESENCE_ARTICLE_TRANSPORT);
    
    // Si le paramètre n'est pas défini, le comportement par défaut est d'afficher l'alerte
    if (valeurParametre == null) {
      return true;
    }
    
    // Si la valeur égale à false ou égale à 0 on renvoie false (alerte désactivée)
    if (valeurParametre.trim().equalsIgnoreCase("false") || valeurParametre.trim().equalsIgnoreCase("0")) {
      return false;
    }
    return true;
  }
  
  /**
   * Activer l'importation des plans de pose Sipe.
   * 
   * On se base sur le paramètre avancé documentvente.activeplanposesipe dans la table des paramètres avancés (PSEMPAVM).
   * On renvoie FALSE si :
   * - la valeur du paramètre == false
   * - la valeur du paramètre == 0
   * Sinon on renvoie TRUE.
   */
  public boolean isImportationPlanPoseSipeActive() {
    // Si le contrôle a déjà été effectué, le résultat est renvoyé directement
    if (importationPlanPoseSipeActive != null) {
      return importationPlanPoseSipeActive.booleanValue();
    }
    
    // Le contrôle dans les paramètres avancés n'a pas été effectué au préalable
    // Valeur définie par le paramètre
    String valeurParametre = ManagerSessionClient.getInstance().getValeurParametreAvance(getIdSession(),
        EnumCleParametreAvance.ACTIVE_IMPORTATION_PLAN_POSE_SIPE);
    
    // Si le paramètre n'est pas défini, le comportement par défaut est de ne pas activer l'importation
    if (valeurParametre == null) {
      importationPlanPoseSipeActive = Boolean.FALSE;
      return importationPlanPoseSipeActive.booleanValue();
    }
    
    // Si la valeur égale à false ou égale à 0 on renvoie false (alerte désactivée)
    valeurParametre = Constantes.normerTexte(valeurParametre).toLowerCase();
    if (valeurParametre.equals("false") || valeurParametre.equals("0")) {
      importationPlanPoseSipeActive = Boolean.FALSE;
    }
    else {
      importationPlanPoseSipeActive = Boolean.TRUE;
    }
    
    return importationPlanPoseSipeActive.booleanValue();
  }
  
  /**
   * Retourne si la modification des quantités articles du document est possible.
   */
  public boolean isModifierQuantitePossible() {
    // Pas de modification possible si le document n'est pas modifiable
    if (documentVenteEnCours == null || !documentVenteEnCours.isModifiable()) {
      return false;
    }
    // Pas de modification possible si on est en saisie d'avoir total
    if (isEnCoursSaisieAvoirTotal) {
      return false;
    }
    // Modification possible si c'est un devis
    if (documentVenteEnCours.isDevis()) {
      return true;
    }
    // Modification possible s'il n'y a aucun règlements
    if (calculReglement == null || calculReglement.getListeReglement() == null || calculReglement.getListeReglement().isEmpty()) {
      return true;
    }
    // Dans les autres cas ce n'est pas possible
    return false;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet article - Articles standards
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Passer en mode recherche des articles standards.
   */
  public void activerModeRechercheArticleStandard() {
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
    rafraichir();
  }
  
  /**
   * Retourne l'identifiant du fournisseur pour les directs usine mono fournisseur (voir PS312).
   * 
   * Ce fournisseur est celui du premier article ajouté au document, à condition que cet article soit associé à un fournisseur.
   * Les recherches article suivantes seront ensuite filtrées sur ce premier fournisseur.
   */
  private IdFournisseur retournerFournisseurDirectUsineMonoFournisseur() {
    ListeLigneVente ligneLigneVente = documentVenteEnCours.getListeLigneVente();
    
    // Vérifier que le document comporte des lignes de ventes
    if (!ligneLigneVente.isEmpty()) {
      // Parcourir les lignes de ventes
      for (int i = 0; i < ligneLigneVente.size(); i++) {
        // Tester si la ligne de vente conitnet un identifiant article
        if (ligneLigneVente.get(i).getIdArticle() != null) {
          // Charger les données de l'article
          Article article = new Article(ligneLigneVente.get(i).getIdArticle());
          article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
          
          // Vérifier si l'article comporte un fournisseur
          if (article.getIdFournisseur() != null) {
            return article.getIdFournisseur();
          }
        }
      }
    }
    return null;
  }
  
  /**
   * Lancer la recherche d'articles standards.
   */
  public void rechercherArticleStandard() {
    // Quitter l'onglet article si rien n'est saisi dans la zone de recherche
    if (texteRecherche.isEmpty() && documentVenteEnCours != null && documentVenteEnCours.getListeLigneVente().size() > 0
        && documentVenteEnCours.isArticleHorsCommentairePresent()) {
      validerOngletArticle();
      return;
    }
    
    // Vider le résultat de la recherche précédente
    effacerListeArticleResultatRecherche();
    
    // Renseigner les critères de recherche
    CritereArticle critereArticle = new CritereArticle();
    critereArticle.setIdEtablissement(etablissement.getId());
    critereArticle.setIsArticleValideSeulement(true);
    critereArticle.setHorsGamme(filtreHorsGamme);
    critereArticle.setFiltreSpecial(EnumFiltreArticleSpecial.OPTION_TOUS_LES_ARTICLES_SAUF_SPECIAUX);
    
    // Les articles directs usine sont affichés uniquement sur les documents direct usine
    if (documentVenteEnCours.isDirectUsine()) {
      critereArticle.setDirectUsine(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES);
      // On filtre en plus sur le fournisseur si la PS312 n'est pas renseignée
      if (!isDirectUsineEnModeMultiFournisseur()) {
        critereArticle.setIdFournisseur(retournerFournisseurDirectUsineMonoFournisseur());
      }
    }
    else {
      critereArticle.setDirectUsine(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE);
    }
    
    // Renseigner la recherche générique
    critereArticle.setTexteRechercheGenerique(texteRecherche);
    texteRecherche = critereArticle.getRechercheGenerique().getTexteFinal();
    messageArticle = critereArticle.getRechercheGenerique().getMessage();
    if (critereArticle.getRechercheGenerique().isEnErreur()) {
      rafraichir();
      return;
    }
    
    // Rechercher les identifiants articles correspondants aux critères de recherche
    ListeArticleBase listeArticlesBases = ListeArticleBase.charger(getIdSession(), critereArticle);
    
    // Enlever un caractère au texte recherché si rien n'a été trouvé
    if (listeArticlesBases == null || listeArticlesBases.isEmpty()) {
      if (texteRecherche.length() > RechercheGenerique.LONGUEUR_MINI_TEXTE_RECHERCHE) {
        texteRecherche = texteRecherche.trim().substring(0, texteRecherche.length() - 1);
        messageArticle = "Aucun article n'a été trouvé, essayez avec : " + texteRecherche;
      }
      else {
        messageArticle = "Aucun article n'a été trouvé.";
      }
      setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
      rafraichir();
      return;
    }
    
    // Mémoriser le résultat : conversion des articles de base trouvés en préparation de vente
    listeResultatRechercheArticle = new ListeResultatRechercheArticle();
    for (ArticleBase article : listeArticlesBases) {
      IdResultatRechercheArticle idResultatRechercheArticle = IdResultatRechercheArticle.getInstance(article.getId());
      ResultatRechercheArticle resultatRechercheArticle = new ResultatRechercheArticle(idResultatRechercheArticle);
      resultatRechercheArticle.setLibelle1(article.getLibelle1());
      resultatRechercheArticle.setLibelle2(article.getLibelle2());
      resultatRechercheArticle.setLibelle3(article.getLibelle3());
      resultatRechercheArticle.setLibelle4(article.getLibelle4());
      listeResultatRechercheArticle.add(resultatRechercheArticle);
    }
    
    // Charger les articles affichés
    chargerPlageResultatRechercheArticle(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraîchir l'écran
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RESULTAT_RECHERCHE);
    rafraichir();
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticleStandardAffiche(int pPremiereLigne, int pDerniereLigne) {
    //
    int nombreResultat = 0;
    if (etape == ETAPE_RECHERCHE_ARTICLE_PALETTE && listeResultatRechercheArticlePalette != null) {
      nombreResultat = listeResultatRechercheArticlePalette.size();
    }
    else if (listeResultatRechercheArticle != null) {
      nombreResultat = listeResultatRechercheArticle.size();
    }
    
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= nombreResultat) {
      indexPremiereLigneAffichee = nombreResultat - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= nombreResultat) {
      nombreLigneAffichee = nombreResultat - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacement de l'ascenseur avec la souris
    if (indexArticleSelectionne < indexPremiereLigneAffichee) {
      indexArticleSelectionne = indexPremiereLigneAffichee;
    }
    else if (indexArticleSelectionne > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexArticleSelectionne = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleComptoir.class, "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageResultatRechercheArticle(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * CHarger les données des articles comprise entre deux lignes.
   * Les informations des articles sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageResultatRechercheArticle(int pPremiereLigne, int pNombreLigne) {
    // Quitter immédiatement si on est en recherche palettes car toutes les informations sont déjà chargées pour tous les articles.
    // Si on tente de recharger, on écrase les prix de consignation !
    if (listeResultatRechercheArticle == null || etape == ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      return;
    }
    
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeResultatRechercheArticle.size());
    
    // Lister les identifiants du résultat de la recherche dont il faut charger les informations
    List<IdResultatRechercheArticle> listeIdResultatRechercheArticle = new ArrayList<IdResultatRechercheArticle>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      ResultatRechercheArticle resultatRechercheArticle = listeResultatRechercheArticle.get(i);
      if (!resultatRechercheArticle.isInformationCharge()) {
        listeIdResultatRechercheArticle.add(resultatRechercheArticle.getId());
      }
    }
    
    // Quitter immédiatement si aucun article n'est à charger
    if (listeIdResultatRechercheArticle.isEmpty()) {
      return;
    }
    
    // Renseigner les paramètres nécessaires pour charger les informations des articles
    ParametresLireArticle parametresLireArticle = new ParametresLireArticle();
    parametresLireArticle.setIdEtablissement(etablissement.getId());
    parametresLireArticle.setIdMagasin(idMagasin);
    parametresLireArticle.setIdClient(clientCourant.getId());
    if (documentVenteEnCours != null) {
      parametresLireArticle.setIdDocumentVente(documentVenteEnCours.getId());
      parametresLireArticle.setIdChantier(documentVenteEnCours.getIdChantier());
    }
    parametresLireArticle.setTauxRemiseForcee(tauxRemiseForcee);
    parametresLireArticle.setNumeroColonneTarifForcee(numeroColonneTarifForcee);
    
    // Charger les informations de ventes des articles
    ListeResultatRechercheArticle listeResultatSupplementaire = ManagerServiceArticle
        .chargerListeResultatRechercheArticle(getSession().getIdSession(), listeIdResultatRechercheArticle, parametresLireArticle);
    
    // Mettre les articles chargés au bon endroit dans la liste de résultat
    for (int i = pPremiereLigne; i < iMax; i++) {
      ResultatRechercheArticle resultatRechercheArticle = listeResultatRechercheArticle.get(i);
      if (!resultatRechercheArticle.isInformationCharge()) {
        for (ResultatRechercheArticle resultatSupplementaire : listeResultatSupplementaire) {
          if (resultatRechercheArticle.getId().equals(resultatSupplementaire.getId())) {
            listeResultatRechercheArticle.set(i, resultatSupplementaire);
            Trace.debug(ModeleComptoir.class, "chargerPlageResultatRechercheArticle", "Charger résultat recherche article", "ligne", i,
                "idResultatRechercheArticle", resultatSupplementaire.getId());
            break;
          }
        }
      }
    }
  }
  
  /**
   * Ajouter un article standard au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterArticleStandard(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_STANDARD);
    Character lettre = extraireLettreSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_STANDARD);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException("La saisie est invalide pour l'ajout d'un article standard : " + pSaisie);
    }
    
    // Définir le type de saisie de la quantité
    EnumTypeQuantite enumTypeQuantite = null;
    if (Constantes.equals(lettre, 'm') || Constantes.equals(lettre, 'v')) {
      enumTypeQuantite = EnumTypeQuantite.UNITE_VENTES;
    }
    else {
      enumTypeQuantite = EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES;
    }
    
    // Les articles gérés par numéros de série ne sont pas utilisables au comptoir.
    if (pArticle.isArticleSerie()) {
      throw new MessageErreurException(
          "Un article géré par numéros de série ne peut être ajouté dans un document de ventes au comptoir.");
    }
    
    // Contrôler si les données essentielles de l'article sont présentes
    controlerArticleStandardOuTransport(pArticle);
    
    // Insérer l'article dans le document de ventes
    LigneVente ligneVente = ajouterArticleDansDocumentVente(pArticle, quantite, enumTypeQuantite, pIndexLigneInseree);
    
    // Si c'est un article géré par lots et que l'on n'est pas en devis
    if (!documentVenteEnCours.isDevis() && isGestionParLot() && pArticle.isArticleLot()) {
      ajouterQuantiteSurLot(ligneVente, pArticle, quantite);
    }
    
    // Créer la ligne de ventes
    ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
    
    // Recharger le document de ventes suite à l'ajout de la ligne
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    ligneVente = documentVenteEnCours.getLigneVente(ligneVente.getId());
    
    // Déterminer s'il faut afficher la boîte de dialogue détaillée
    int ongletAAfficher = ModeleDetailLigneArticle.NE_PAS_AFFICHER;
    if (pArticle.isAffichageMemoObligatoire() || Constantes.equals(lettre, 'i')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES;
    }
    else if (Constantes.equals(lettre, 'd') || Constantes.equals(lettre, 'g')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_GENERAL;
    }
    else if (Constantes.equals(lettre, 's')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_STOCK;
    }
    else if (Constantes.equals(lettre, 'h')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_HISTORIQUE;
    }
    else if (Constantes.equals(lettre, 'n')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_NEGOCIATION;
    }
    else if (pArticle.isArticleDecoupable()) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_DIMENSIONS;
    }
    
    // Traiter l'insertion
    selectionnerLigneVente(ligneVente, ongletAAfficher, TABLE_RESULTAT_RECHERCHE_ARTICLES, pArticle, true);
    
    // Rafraichir
    rafraichir();
  }
  
  /**
   * 
   * Ajouter les quantité sur lot pour un article standard géré par lots.
   *
   */
  private void ajouterQuantiteSurLot(LigneVente pLigneVente, Article pArticle, BigDecimal pQuantiteSaisie) {
    if (!isGestionParLot() || documentVenteEnCours.isDevis() || !pArticle.isArticleLot()) {
      return;
    }
    
    ModeleLot modeleLot = new ModeleLot(getSession(), pLigneVente.getId(), pArticle, pQuantiteSaisie);
    if (documentVenteEnCours.isAvoir()) {
      modeleLot.setIdDocumentOrigine(idDocumentOrigineAvoir);
    }
    else if (documentVenteEnCours.getIdOrigine() != null) {
      modeleLot.setIdDocumentOrigine(documentVenteEnCours.getIdOrigine());
    }
    VueLot vueLot = new VueLot(modeleLot);
    vueLot.afficher();
    if (modeleLot.isSortieAvecValidation()) {
      // Modification de la quantité vendue sur ligne si on l'a adaptée au nombre de lots saisis
      if (modeleLot.getQuantiteCommande().compareTo(pQuantiteSaisie) < 0) {
        pLigneVente.setQuantiteUCV(modeleLot.getQuantiteCommande());
      }
      // mise à jour de la ligne pour indiquer si la saisie des lots est complète
      if (modeleLot.isSaisieLotComplete()) {
        pLigneVente.setTopNumSerieOuLot(40);
      }
      else {
        pLigneVente.setTopNumSerieOuLot(30);
      }
    }
  }
  
  /**
   * Ajouter un article standard, avec une quantité suivie de la lettre P, au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterArticleStandardAvecP(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_STANDARD_AVEC_P);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException(
          "La saisie est invalide pour l'ajout d'un article standard par unité de conditionnement de stock : " + pSaisie);
    }
    
    // Tester si une unité de conditionnement de stock est définie
    if (!pArticle.isDefiniNombreUSParUCS()) {
      throw new MessageErreurException("Il n'y a pas d'unité de conditionnement de stock pour cet article.");
    }
    
    // Forcer les quantités de palettes à saisir
    forcerQuantiteSaisiePalette(pArticle);
    
    // Contrôler les données de l'articles
    if (pArticle.isArticleStandard()) {
      controlerArticleStandardOuTransport(pArticle);
    }
    
    // Insérer l'article dans le document
    LigneVente ligneVente =
        ajouterArticleDansDocumentVente(pArticle, quantite, EnumTypeQuantite.UNITE_CONDITIONNEMENT_STOCKS, pIndexLigneInseree);
    
    // Créer la ligne de ventes
    ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
    
    // Recharger le document de ventes suite à l'ajout de la ligne
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    ligneVente = documentVenteEnCours.getLigneVente(ligneVente.getId());
    
    // Afficher l'onglet "Informations techniques" si affichage du mémo ITC obligatoire
    int onglet = ModeleDetailLigneArticle.NE_PAS_AFFICHER;
    if (pArticle.isAffichageMemoObligatoire()) {
      onglet = ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES;
    }
    
    // Traiter l'insertion
    selectionnerLigneVente(ligneVente, onglet, TABLE_RESULTAT_RECHERCHE_ARTICLES, pArticle, true);
    
    // Rafraichir
    rafraichir();
  }
  
  private void forcerQuantiteSaisiePalette(Article pArticle) {
    if (pArticle == null) {
      return;
    }
    for (LigneVente ligneVente : documentVenteEnCours.getListeLigneVente()) {
      if (Constantes.equals(ligneVente.getIdArticle(), pArticle.getId()) && !ligneVente.isLigneCommentaire()
          && pArticle.isDefiniNombreUSParUCS()) {
        nombreDePalettesLies = ligneVente.getQuantiteUCV().divide(pArticle.getNombreUSParUCS(true), MathContext.DECIMAL32);
      }
    }
  }
  
  /**
   * Ajouter un retour d'article standard au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterRetourArticleStandard(String pSaisie, Article pArticle) {
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_RETOUR_ARTICLE_STANDARD);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException("La saisie est invalide pour un retour d'article standard : " + pSaisie);
    }
    
    // Retours impossible sur les directs usine
    if (documentVenteEnCours.isDirectUsine()) {
      throw new MessageErreurException("Il est impossible de faire des retours d'articles sur un document direct usine.");
    }
    
    // Retours possibles seulement sur articles standards et sur bons et factures
    if (!documentVenteEnCours.isBon() && !documentVenteEnCours.isFacture()) {
      throw new MessageErreurException("Il est possible de faire des retours d'articles uniquement sur des bons ou des factures.");
    }
    
    // Afficher la boîte de dialogue de saisie d'un retour
    ModeleRetourArticle modeleRetour = new ModeleRetourArticle(getSession(), quantite, pArticle, clientCourant, documentVenteEnCours);
    VueRetourArticle vueRetour = new VueRetourArticle(modeleRetour);
    vueRetour.afficher();
    
    // Recharger le document de ventes
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article gratuite.
   */
  private String[] formaterLigneArticleGratuite(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    String ligne[] = new String[pNombreColonnes];
    
    // Quantité
    ligne[0] = Constantes.formater(pLigneVente.getQuantiteUCV(), false);
    // Dans le cas où on affiche la colonne des quantités d'origine
    if (pOffset > 0) {
      // Dans le cas d'une modification de document (hors extraction)
      if (informationsExtraction == null) {
        // Récupération de la ligne de vente de la sauvegarde du doucment en cours (avant les dernières modifications)
        LigneVente ancienneligneVente = null;
        if (documentVenteAvantModifications != null) {
          ancienneligneVente = documentVenteAvantModifications.getLigneVente(pLigneVente.getId());
        }
        if (ancienneligneVente != null) {
          ligne[1] = Constantes.formater(ancienneligneVente.getQuantiteDocumentOrigineUC(), false);
        }
        // Utilisation de la ligen en cours si l'ancienne ligne n'a pas été trouvée
        else {
          ligne[1] = Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
        }
      }
      // Dans le cas d'une extraction
      else {
        // Quantité d'origine avec le même nombre de décimales que la quantité en UCV : ce nombre de décimale peut être différent de
        // celui de l'unité pour les articles scindables
        ligne[1] = Constantes.formater(informationsExtraction.retournerQuantiteOrigine(pLigneVente.getId().getNumeroLigne()), false);
      }
    }
    if (pLigneVente.getIdUniteConditionnementVente() != null) {
      ligne[1 + pOffset] = pLigneVente.getIdUniteConditionnementVente().getCode();
    }
    
    // Libellé ligne
    ligne[2 + pOffset] = pLigneVente.getLibelle();
    
    // Origine de prix
    TypeGratuit typeGratuit = null;
    if (listeTypeGratuit != null) {
      typeGratuit = listeTypeGratuit.get(pLigneVente.getIdTypeGratuit());
    }
    if (typeGratuit != null) {
      ligne[5 + pOffset] = typeGratuit.getTexte();
    }
    
    // Montant
    ligne[9 + pOffset] = "Gratuit";
    
    return ligne;
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article normale.
   */
  private String[] formaterLigneArticleNormale(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de vente est incorrecte.");
    }
    
    String ligne[] = new String[pNombreColonnes];
    ligne[0] = Constantes.formater(pLigneVente.getQuantiteUCV(), false);
    // Dans le cas où on affiche la colonne des quantités d'origine
    if (pOffset > 0) {
      // Dans le cas d'une modification de document (hors extraction)
      if (informationsExtraction == null) {
        // Récupération de la ligne de vente de la sauvegarde du doucment en cours (avant les dernières modifications)
        LigneVente ancienneligneVente = null;
        if (documentVenteAvantModifications != null) {
          ancienneligneVente = documentVenteAvantModifications.getLigneVente(pLigneVente.getId());
        }
        if (ancienneligneVente != null) {
          ligne[1] = Constantes.formater(ancienneligneVente.getQuantiteDocumentOrigineUC(), false);
        }
        // Utilisation de la ligen en cours si l'ancienne ligne n'a pas été trouvée
        else {
          ligne[1] = Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
        }
      }
      // Dans le cas d'une extraction
      else {
        // Quantité d'origine avec le même nombre de décimales que la quantité en UCV : ce nombre de décimale peut être différent de
        // celui de l'unité pour les articles scindables
        ligne[1] = Constantes.formater(informationsExtraction.retournerQuantiteOrigine(pLigneVente.getId().getNumeroLigne()), false);
      }
    }
    if (pLigneVente.getIdUniteConditionnementVente() != null) {
      ligne[1 + pOffset] = pLigneVente.getIdUniteConditionnementVente().getCode();
    }
    
    // Liebllé
    ligne[2 + pOffset] = pLigneVente.getLibelle();
    
    // Affichage des montants en TTC ou HT
    if (isClientTTC()) {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseTTC(), true);
    }
    else {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseHT(), true);
    }
    
    // Unité de vente
    if (pLigneVente.getIdUniteVente() != null) {
      ligne[4 + pOffset] = pLigneVente.getIdUniteVente().getCode();
    }
    
    // Origine de prix
    ligne[5 + pOffset] = pLigneVente.getTexteOriginePrixVente();
    
    // Remise
    ligne[6 + pOffset] = Constantes.formater(pLigneVente.getTauxRemiseUnitaire(modeNegoce), true) + " %";
    
    // Affichage des montants en TTC ou HT
    if (isClientTTC()) {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetTTC(), true);
    }
    else {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetHT(), true);
    }
    
    // Quantité en UV
    ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
    
    // Affichage des montants en TTC ou HT
    if (isClientTTC()) {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantTTC(), true);
    }
    else {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantHT(), true);
    }
    
    return ligne;
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article négative.
   */
  private String[] formaterLigneArticleNegative(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de vente est invalide.");
    }
    String ligne[] = new String[pNombreColonnes];
    
    // Quantité en UV
    if (pLigneVente.getQuantiteUCV() != null) {
      ligne[0] = Constantes.formater(pLigneVente.getQuantiteUCV(), false);
    }
    
    // Dans le cas où on affiche la colonne des quantités d'origine
    if (pOffset > 0) {
      if (retournerPartieDecimale(pLigneVente.getQuantiteDocumentOrigineUC()).compareTo(BigDecimal.ZERO) > 0) {
        if (informationsExtraction == null) {
          ligne[1] = Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
        }
        else {
          // Quantité d'origine avec le même nombre de décimales que la quantité en UCV : ce nombre de décimale peut être différent de
          // celui de l'unité pour les articles scindables
          ligne[1] = Constantes.formater(informationsExtraction.retournerQuantiteOrigine(pLigneVente.getId().getNumeroLigne()), false);
        }
      }
    }
    
    // Unité de conditionnement de ventes
    if (pLigneVente.getIdUniteConditionnementVente() != null) {
      ligne[1 + pOffset] = pLigneVente.getIdUniteConditionnementVente().getCode();
    }
    
    // Libellé ligne
    ligne[2 + pOffset] = pLigneVente.getLibelle();
    
    // Prix de base
    if (isClientTTC()) {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseTTC(), true);
    }
    else {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseHT(), true);
    }
    
    //
    ligne[4 + pOffset] = "-";
    
    // Pourcentage de remise
    if (documentVenteEnCours.getPourcentageRemise1Pied(Constantes.DEUX_DECIMALES) != null) {
      ligne[6 + pOffset] = Constantes.formater(documentVenteEnCours.getPourcentageRemise1Pied(Constantes.DEUX_DECIMALES), true) + " %";
    }
    
    // Prix net
    if (isClientTTC()) {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetTTC(), true);
    }
    else {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetHT(), true);
    }
    
    // Quantité en UV
    ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
    
    // Montant
    if (isClientTTC()) {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantTTC(), true);
    }
    else {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantHT(), true);
    }
    
    return ligne;
  }
  
  /**
   * Contrôler la validité d'un article standard ou transport (vérifie l'existence des données indispensables).
   * @param pArticle Article à contrôler.
   */
  public void controlerArticleStandardOuTransport(Article pArticle) {
    if (pArticle == null) {
      throw new MessageErreurException("L'article est invalide.");
    }
    StringBuilder message = new StringBuilder();
    if (pArticle.getLibelleComplet() == null || pArticle.getLibelleComplet().isEmpty()) {
      message.append("Le libellé de l'article n'est pas renseigné.\n");
    }
    if (pArticle.getIdFamille() == null || pArticle.getIdFamille().getCode().isEmpty()) {
      message.append("Le groupe ou la famille de l'article ne sont pas renseignés.\n");
    }
    if (pArticle.getIdUV() == null) {
      message.append("L'unité de vente de l'article n'est pas renseignée.\n");
    }
    if (pArticle.getCodeTVA() == null || pArticle.getCodeTVA() == 0) {
      message.append("Le code TVA de l'article n'est pas renseigné.\n");
    }
    if (message.length() > 0) {
      message.insert(0, "Des erreurs ont été détecté, veuillez corriger la fiche article:\n");
      throw new MessageErreurException(message.toString());
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet article - Articles transport
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Passer en mode recherche des articles transports.
   */
  public void activerModeRechercheArticleTransport() {
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_TRANSPORT);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
    rafraichir();
  }
  
  /**
   * Lance la recherche d'articles transports avec les critères de recherche.
   */
  public void rechercherArticleTransport() {
    // Vérifier les pré-requis
    if (documentVenteEnCours == null || documentVenteEnCours.getTransport() == null) {
      throw new MessageErreurException("Le document de ventes est invalide.");
    }
    
    // Effacer la recherche précédente
    effacerListeArticleResultatRecherche();
    
    // Récupérer la zone géographique
    IdZoneGeographique idZoneGeographique = documentVenteEnCours.getTransport().getIdZoneGeographique();
    
    // Lancer la recherche
    CriteresRechercheArticles critereArticle = new CriteresRechercheArticles();
    critereArticle.setTypeRecherche(CriteresRechercheArticles.RECHERCHE_TYPE_ARTICLES_TRANSPORTS);
    critereArticle.setNumeroPage(1);
    critereArticle.initialiserNombrePagesMax();
    critereArticle.setTexteRecherche(texteRecherche);
    critereArticle.setZoneGeographique(idZoneGeographique);
    critereArticle.setIdChantier(null);
    critereArticle.setIdEtablissement(etablissement.getId());
    critereArticle.setIdMagasin(idMagasin);
    critereArticle.setIsArticleValideSeulement(true);
    critereArticle.setChaineExacte(true);
    critereArticle.setTauxRemiseForcee(tauxRemiseForcee);
    
    // Rechercher les articles transports
    ListeArticle listeArticleTransport = ManagerServiceArticle.chargerListeArticleTransport(getSession().getIdSession(), critereArticle);
    
    // Afficher un message si aucun transport n'a été trouvé
    if (listeArticleTransport == null || listeArticleTransport.isEmpty()) {
      messageArticle = "Aucun article transport n'a été trouvé avec ces critères de recherche.";
      setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
      rafraichir();
      return;
    }
    
    // Création de la liste des lignes des articles transports à afficher
    listeResultatRechercheArticle = new ListeResultatRechercheArticle();
    for (Article article : listeArticleTransport) {
      IdResultatRechercheArticle idResultatRechercheArticle = IdResultatRechercheArticle.getInstance(article.getId());
      ResultatRechercheArticle resultatRechercheArticle = new ResultatRechercheArticle(idResultatRechercheArticle);
      resultatRechercheArticle.setIdUCV(article.getIdUCV());
      resultatRechercheArticle.setLibelle1(article.getLibelle1());
      resultatRechercheArticle.setLibelle2(article.getLibelle2());
      resultatRechercheArticle.setLibelle3(article.getLibelle3());
      resultatRechercheArticle.setLibelle4(article.getLibelle4());
      resultatRechercheArticle.setStock(article.getQuantiteDisponibleVente());
      resultatRechercheArticle.setIdUV(article.getIdUV());
      resultatRechercheArticle.setObservation(article.getObservationCourte());
      listeResultatRechercheArticle.add(resultatRechercheArticle);
    }
    
    // Charger les articles affichés
    chargerPlageResultatRechercheArticle(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraîchir
    setEtape(ETAPE_RECHERCHE_ARTICLE_TRANSPORT);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RESULTAT_RECHERCHE);
    rafraichir();
  }
  
  /**
   * Ajouter un article transport au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterArticleTransport(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_TRANSPORT);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      quantite = BigDecimal.ONE;
    }
    
    // Contrôler si les données essentielles de l'article sont présentes
    controlerArticleStandardOuTransport(pArticle);
    
    // Insérer l'article dans le document de ventes
    LigneVente ligneVente =
        ajouterArticleDansDocumentVente(pArticle, quantite, EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES, pIndexLigneInseree);
    
    // Créer la ligne de ventes
    ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
    
    // Recharger le document de ventes suite à l'ajout de la ligne
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    ligneVente = documentVenteEnCours.getLigneVente(ligneVente.getId());
    
    // Traiter l'ajout d'une ligne de vente
    selectionnerLigneVente(ligneVente, ModeleDetailLigneArticle.NE_PAS_AFFICHER, TABLE_RESULTAT_RECHERCHE_ARTICLES, pArticle, true);
    
    // Revenir au mode recherche standard après ajout d'un article transport
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    rafraichir();
  }
  
  /**
   * Est-ce qu'une ligne de commande contient un article transport ?
   */
  public boolean isArticleTransport(LigneVente pLigneVente) {
    // Tester si la ligne contient un identifiant article
    if (pLigneVente.getIdArticle() == null || !pLigneVente.getIdArticle().isExistant()) {
      return false;
    }
    
    // Charger les données de l'article
    Article article = new Article(pLigneVente.getIdArticle());
    article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
    if (article == null) {
      return false;
    }
    return article.isArticleTransport();
  }
  
  /**
   * Controle qu'il y ait au moins un article transport.
   */
  private boolean isArticleTransportPresent() {
    ListeLigneVente listeLigneVente = documentVenteEnCours.getListeLigneVente();
    if ((listeLigneVente == null) || (listeLigneVente.isEmpty())) {
      return false;
    }
    // On commence par la fin du document (c'est plus logique)
    for (int i = listeLigneVente.size(); --i >= 0;) {
      if (isArticleTransport(listeLigneVente.get(i))) {
        return true;
      }
    }
    return false;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet article - Articles palettes
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Passer en mode recherche des articles palettes.
   */
  public void activerModeRechercheArticlePalette() {
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_PALETTE);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
    rafraichir();
  }
  
  /**
   * Recherche des articles de type palettes.
   */
  private void rechercherArticlePalette() {
    // Effacer la recherche précédente
    effacerListeArticleResultatRecherche();
    
    // Renseigner les critères de recherche
    CritereResultatRechercheArticlePalette critere = new CritereResultatRechercheArticlePalette();
    critere.setIdEtablissement(etablissement.getId());
    critere.setIdMagasin(idMagasin);
    critere.setIdClient(clientCourant.getId());
    critere.setIdDocumentVente(documentVenteEnCours.getId());
    critere.setIdChantier(documentVenteEnCours.getIdChantier());
    critere.setTexteRecherche(texteRecherche.toUpperCase());
    
    // Récupération d'une liste d'articles avec gestion de la pagination
    listeResultatRechercheArticlePalette =
        ManagerServiceArticle.chargerListeResultatRechercheArticlePalette(getSession().getIdSession(), critere);
    
    // Afficher un message si aucune palette n'a été trouvée
    if (listeResultatRechercheArticlePalette == null || listeResultatRechercheArticlePalette.isEmpty()) {
      messageArticle = "Aucun article palette n'a été trouvé pour vos critères.";
      setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
      rafraichir();
      return;
    }
    
    // Rafraichir
    setEtape(ETAPE_RECHERCHE_ARTICLE_PALETTE);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RESULTAT_RECHERCHE);
    rafraichir();
  }
  
  /**
   * Ajouter un article palette au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterArticlePalette(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_PALETTE);
    Character lettre = extraireLettreSaisie(pSaisie, PATTERN_SAISIE_ARTICLE_PALETTE);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException("La saisie est invalide pour un article palette : " + pSaisie);
    }
    
    // Empêcher la vente sous le prix de revient
    setVenteSousPRV(false);
    
    // Ajouter la ligne article au document de ventes
    LigneVente ligneVente = documentVenteEnCours.ajouterLigneArticle(getSession().getIdSession(), pArticle, quantite,
        EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES, pIndexLigneInseree, dateTraitement);
    if (ligneVente == null) {
      throw new MessageErreurException("Erreur lors de l'ajout de l'article palette.");
    }
    
    // Renseigner les quantités
    ligneVente.setQuantiteUCV(quantite);
    ligneVente.setQuantiteUV(quantite);
    
    // Renseigner les paramètres de la ligne de vente
    ParametreLigneVente parametreLigneVente = new ParametreLigneVente();
    parametreLigneVente.setQuantiteUV(ligneVente.getQuantiteUV());
    if (numeroColonneTarifForcee != null) {
      parametreLigneVente.setNumeroColonneTarif(numeroColonneTarifForcee);
      parametreLigneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
    }
    
    // Renseigner les données concernant les prix
    // Appel au programme de calcul de prix avec la quantité saisie
    ParametreChargement parametreChargement = new ParametreChargement();
    parametreChargement.setIdEtablissement(documentVenteEnCours.getId().getIdEtablissement());
    parametreChargement.setParametreLigneVente(parametreLigneVente);
    parametreChargement.setIdMagasin(getIdMagasin());
    parametreChargement.setIdArticle(pArticle.getId());
    parametreChargement.setIdClientFacture(documentVenteEnCours.getIdClientFacture());
    parametreChargement.setIdDocumentVente(documentVenteEnCours.getId());
    parametreChargement.setIdChantier(documentVenteEnCours.getIdChantier());
    CalculPrixVente calculPrixVente = ManagerServiceArticle.calculerPrixVente(getIdSession(), parametreChargement);
    
    // Mise à jour des données de prix dans la ligne de ventes
    ligneVente.initialiserPrixVente(calculPrixVente);
    
    // Sauver la ligne de ventes
    ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
    
    // Recharger le document de ventes
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    ligneVente = documentVenteEnCours.getLigneVente(ligneVente.getId());
    
    // Déterminer s'il faut afficher la boîte de dialogue détaillée
    int ongletAAfficher = ModeleDetailLigneArticle.NE_PAS_AFFICHER;
    if (pArticle.isAffichageMemoObligatoire() || Constantes.equals(lettre, 'i')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES;
    }
    else if (Constantes.equals(lettre, 'd') || Constantes.equals(lettre, 'g')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_GENERAL;
    }
    else if (Constantes.equals(lettre, 's')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_STOCK;
    }
    else if (Constantes.equals(lettre, 'h')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_HISTORIQUE;
    }
    else if (Constantes.equals(lettre, 'i')) {
      ongletAAfficher = ModeleDetailLigneArticle.ONGLET_INFORMATIONSTECHNIQUES;
    }
    
    // Afficher la boîte de dialogue détaillée
    selectionnerLigneVente(ligneVente, ongletAAfficher, TABLE_RESULTAT_RECHERCHE_ARTICLES, pArticle, true);
    
    // Revenir au mode recherche standard après ajout d'une palette
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    rafraichir();
  }
  
  /**
   * Ajouter un retour d'article palette au document de vente.
   * 
   * Lors d'un retour de palette, plusieurs lignes de ventes peuvent être générées (une par prix de déconsignation différent). C'est la
   * raison pour laquelle il n'est pas possible d'afficher la boîte de dialogue de saisie détaillée lors de la saisie initiale car
   * on ne saurait pas quelle ligne afficher.
   * 
   * @param pSaisie Valeur saisie.
   * @param pIndexLigneModifiee Index de la ligne modifiée.
   * @param pIndexLigneInseree Index de la ligne insérée.
   */
  private void ajouterRetourArticlePalette(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Retours impossible sur les directs usine
    if (documentVenteEnCours.isDirectUsine()) {
      throw new MessageErreurException("Il est impossible de faire des retours d'articles sur un document direct usine.");
    }
    
    // Interpréter la saisie
    BigDecimal quantite = extraireQuantiteSaisie(pSaisie, PATTERN_SAISIE_RETOUR_ARTICLE_PALETTE);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) == 0) {
      throw new MessageErreurException("La saisie est invalide pour un retour d'article palette : " + pSaisie);
    }
    
    // Empêcher la vente sous le prix d erevient
    setVenteSousPRV(false);
    
    // Ajouter la ligne article au document de ventes
    LigneVente ligneVente = documentVenteEnCours.ajouterLigneArticle(getSession().getIdSession(), pArticle, quantite,
        EnumTypeQuantite.UNITE_CONDITIONNEMENT_VENTES, pIndexLigneInseree, dateTraitement);
    if (ligneVente == null) {
      throw new MessageErreurException("Erreur lors du retour de l'article palette.");
    }
    
    // Renseigner les quantités
    ligneVente.setQuantiteUCV(quantite);
    ligneVente.setQuantiteUV(quantite);
    
    // Renseigner le taux de TVA (TODO A voir si toujours nécessaire)
    ligneVente.initialiserTauxTVA(documentVenteEnCours, etablissement);
    
    // Sauver la ligne de ventes
    ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), ligneVente);
    
    // Recharger le document de ventes
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    
    // Revenir au mode recherche standard après ajout d'une palette
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    rafraichir();
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article palette.
   */
  private String[] formaterLigneArticlePalette(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de vente est invalide.");
    }
    
    String ligne[] = new String[pNombreColonnes];
    
    // Quantité en UCV
    if (pLigneVente.getQuantiteUCV() != null) {
      ligne[0] = Constantes.formater(pLigneVente.getQuantiteUCV(), false);
    }
    
    // Dans le cas où on affiche la colonne des quantités d'origine
    if (pOffset > 0) {
      if (informationsExtraction == null) {
        ligne[1] = Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
      }
      else {
        // Quantité d'origine avec le même nombre de décimales que la quantité en UCV : ce nombre de décimale peut être différent de
        // celui de l'unité pour les articles scindables
        ligne[1] = Constantes.formater(informationsExtraction.retournerQuantiteOrigine(pLigneVente.getId().getNumeroLigne()), false);
      }
    }
    
    // Unité de conditionnement de ventes
    if (pLigneVente.getIdUniteConditionnementVente() != null) {
      ligne[1 + pOffset] = pLigneVente.getIdUniteConditionnementVente().getCode();
    }
    
    // Libellé
    ligne[2 + pOffset] = pLigneVente.getLibelle();
    
    // Prix net
    if (isClientTTC()) {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixNetTTC(), true);
    }
    else {
      ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixNetHT(), true);
    }
    
    // Unité de vente
    if (pLigneVente.getIdUniteVente() != null) {
      ligne[4 + pOffset] = pLigneVente.getIdUniteVente().getCode();
    }
    
    // Type de prix
    if (pLigneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) >= 0) {
      ligne[5 + pOffset] = "Vente";
    }
    else if (pLigneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) < 0) {
      ligne[5 + pOffset] = "Reprise";
    }
    
    // Le taux de remise
    ligne[6 + pOffset] = "";
    
    // Prix net
    if (isClientTTC()) {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetTTC(), true);
    }
    else {
      ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetHT(), true);
    }
    
    // Quantité en UV
    if (retournerPartieDecimale(pLigneVente.getQuantiteUCV()).compareTo(BigDecimal.ZERO) > 0) {
      ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
    }
    else {
      ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
    }
    
    // Montant
    if (isClientTTC()) {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantTTC(), true);
    }
    else {
      ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantHT(), true);
    }
    
    return ligne;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet Article - Articles spéciaux
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Est-ce qu'une ligne de commande contient un article spécial ?
   */
  public boolean etreArticleSpecial(LigneVente pLigneVente) {
    // Cela n'est pas un article spécial si le code article n'est pas renseigné
    if (pLigneVente.getIdArticle() == null) {
      return false;
    }
    Article article = new Article(pLigneVente.getIdArticle());
    article = ManagerServiceArticle.completerArticleStandard(getSession().getIdSession(), article);
    if (article == null) {
      return false;
    }
    return article.isArticleSpecial();
  }
  
  /**
   * Sélection d'une ligne de vente d'un article spécial.
   * 
   * @param pLigneVente La ligne de vente ou null si création de la ligne.
   * @param pIndiceInsertion Le numéro de la ligne dans la liste des lignes du document de ventes affichée.
   */
  public void selectionnerLigneVenteArticleSpecial(LigneVente pLigneVente, int pIndiceInsertion) {
    boolean modeCreation = false;
    LigneVente ligneVenteOrigine = null;
    
    if (pLigneVente == null) {
      modeCreation = true;
    }
    else {
      // Clonage de la ligne dans les cas où l'utilisateur ferme la boite dialogue avec le bouton "Annuler"
      ligneVenteOrigine = pLigneVente.clone();
    }
    
    // Lancement de la boite de dialogue qui gère les articles spéciaux
    ModeleArticleSpecial modele =
        new ModeleArticleSpecial(getSession(), this, utilisateurGescom, documentVenteEnCours, pLigneVente, clientCourant);
    VueArticleSpecial vue = new VueArticleSpecial(modele);
    vue.afficher();
    
    // Traitement si l'utilisateur a annulé les modifications
    if (modele.isSortieAvecAnnulation()) {
      pLigneVente = ligneVenteOrigine;
      return;
    }
    
    // L'article spécial vient d'être crée
    if (modeCreation) {
      // Insérer l'article dans le document de ventes
      pLigneVente = ajouterArticleDansDocumentVente(modele.getArticle(), modele.getLigneVente().getQuantiteUV(),
          EnumTypeQuantite.UNITE_VENTES, pIndiceInsertion);
      // Mise à jour des données de la nouvelle ligne avec les données de la ligne de vente négociée (à améliorer car c'est une source de
      // bugs)
      pLigneVente.setNombreUVParUCV(modele.getLigneVente().getNombreUVParUCV());
      pLigneVente.setIdAdresseFournisseur(modele.getAdresseFournisseur().getId());
      pLigneVente.setProvenanceColonneTarif(modele.getLigneVente().getProvenanceColonneTarif());
      pLigneVente.setNumeroColonneTarif(modele.getLigneVente().getNumeroColonneTarif());
      
      pLigneVente.setPrixBaseHT(modele.getLigneVente().getPrixBaseHT());
      pLigneVente.setPrixBaseTTC(modele.getLigneVente().getPrixBaseTTC());
      pLigneVente.setProvenancePrixBase(modele.getLigneVente().getProvenancePrixBase());
      
      pLigneVente.setPrixNetSaisiHT(modele.getLigneVente().getPrixNetSaisiHT());
      pLigneVente.setPrixNetSaisiTTC(modele.getLigneVente().getPrixNetSaisiTTC());
      pLigneVente.setPrixNetCalculeHT(modele.getLigneVente().getPrixNetCalculeHT());
      pLigneVente.setPrixNetCalculeTTC(modele.getLigneVente().getPrixNetCalculeTTC());
      pLigneVente.setProvenancePrixNet(modele.getLigneVente().getProvenancePrixNet());
      
      pLigneVente.setPrixDeRevientStandardHT(modele.getLigneVente().getPrixDeRevientStandardHT());
      pLigneVente.setPrixDeRevientStandardTTC(modele.getLigneVente().getPrixDeRevientStandardTTC());
      pLigneVente.setPrixDeRevientLigneHT(modele.getLigneVente().getPrixDeRevientLigneHT());
      pLigneVente.setPrixDeRevientLigneTTC(modele.getLigneVente().getPrixDeRevientLigneTTC());
      pLigneVente.setProvenancePrixDeRevient(modele.getLigneVente().getProvenancePrixDeRevient());
      
      pLigneVente.setMontantHT(modele.getLigneVente().getMontantHT());
      pLigneVente.setMontantTTC(modele.getLigneVente().getMontantTTC());
      
      // Créer la ligne de ventes
      ManagerServiceDocumentVente.sauverLigneVente(getSession().getIdSession(), pLigneVente);
    }
    // L'article spécial vient d'être modifié
    else {
      pLigneVente = modele.getLigneVente();
      AlertesLigneDocument alertesLigneDocument = controlerLigneDocument(pLigneVente);
      if ((alertesLigneDocument != null) && alertesLigneDocument.controlerAlertes()) {
        pLigneVente.setAlertesLigneDocument(null);
        throw new MessageErreurException(alertesLigneDocument.getMessages());
      }
      ManagerServiceDocumentVente.modifierLignedocumentVente(getSession().getIdSession(), pLigneVente);
    }
    
    try {
      // Contrôle le document (permet de recaluler le montant total du document)
      controlerDocument(true);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "");
    }
    // Permet de rafraichir les données correctement dans le cas où une exception est déclenchée
    finally {
      // Recharge le document de ventes
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      
      // Contrôle de l'encours
      controlerEncoursClient();
      
      // Ne modifie pas l'étape de saisie pour laisser le comptoir dans l'état ou il était avant ajout de l'article spécial
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet Article - Articles commentaires
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Passer en mode recherche des articles commentaires.
   */
  public void activerModeRechercheArticleCommentaire() {
    effacerListeArticleResultatRecherche();
    texteRecherche = "";
    setEtape(ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
    rafraichir();
  }
  
  /**
   * Lancer la recherche d'articles commentaires avec les critères de recherche.
   */
  public void rechercherArticleCommentaire() {
    effacerListeArticleResultatRecherche();
    
    // Renseigner les critères de recherche
    CritereArticleCommentaire critere = new CritereArticleCommentaire();
    critere.setIdEtablissement(etablissement.getId());
    critere.setIsArticleValideSeulement(true);
    
    // Renseigner le texte de recherche s'il n'est pas vide
    if (!texteRecherche.isEmpty()) {
      critere.setTexteRechercheGenerique(texteRecherche);
      texteRecherche = critere.getRechercheGenerique().getTexteFinal();
      messageArticle = critere.getRechercheGenerique().getMessage();
      if (critere.getRechercheGenerique().isEnErreur()) {
        setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
        rafraichir();
        return;
      }
    }
    
    // Rechercher les articles commentaires
    ListeArticle listeArticleCommentaire = ManagerServiceArticle.chargerListeArticleCommentaire(getSession().getIdSession(), critere);
    
    // Afficher un message si aucun commentaire n'a été trouvé
    if (listeArticleCommentaire == null || listeArticleCommentaire.isEmpty()) {
      messageArticle = "Aucun commentaire n'a été trouvé pour vos critères.";
      setComposantAyantLeFocus(FOCUS_ARTICLES_RECHERCHE_ARTICLE);
      rafraichir();
      return;
    }
    
    // Création de la liste des lignes des articles commentaires à afficher
    listeResultatRechercheArticle = new ListeResultatRechercheArticle();
    for (Article article : listeArticleCommentaire) {
      IdResultatRechercheArticle idResultatRechercheArticle = IdResultatRechercheArticle.getInstance(article.getId());
      ResultatRechercheArticle resultatRechercheArticle = new ResultatRechercheArticle(idResultatRechercheArticle);
      resultatRechercheArticle.setLibelle1(article.getLibelle1());
      resultatRechercheArticle.setLibelle2(article.getLibelle2());
      resultatRechercheArticle.setLibelle3(article.getLibelle3());
      resultatRechercheArticle.setLibelle4(article.getLibelle4());
      listeResultatRechercheArticle.add(resultatRechercheArticle);
    }
    
    // Rafraichir
    setEtape(ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE);
    setComposantAyantLeFocus(FOCUS_ARTICLES_RESULTAT_RECHERCHE);
    rafraichir();
  }
  
  /**
   * Saisir un nouveau commentaire qui sera ajouté au document.
   * Ce commentaire pourra éventuellement être sauvegardé sous forme d'article commentaire pour une utilisation ultérieure.
   */
  public void creerLigneCommentaire(int pLigneInsertion) {
    // Récupérer le code établissement
    IdEtablissement idEtablissement = IdEtablissement.getInstance(documentVenteEnCours.getId().getCodeEtablissement());
    
    // Lancer la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modele =
        ModeleDetailCommentaire.getInstanceCreationCommentaireLibre(getSession(), idEtablissement, EnumContexteMetier.CONTEXTE_VENTE);
    modele.setDirectUsine(documentVenteEnCours.isDirectUsine());
    VueDetailCommentaire vue = new VueDetailCommentaire(modele);
    vue.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    if (modele.isSortieAvecAnnulation()) {
      return;
    }
    else if (modele.isSortieAvecValidation()) {
      // Créer la ligne de commentaire
      Article articleCommentaire = modele.creerArticleCommentaire();
      LigneVente ligneVente =
          documentVenteEnCours.ajouterLigneArticleCommentaire(getSession().getIdSession(), articleCommentaire, pLigneInsertion);
      if (modele.isCopierSurDocumentAchats()) {
        ligneVente.setGenerationBonAchat(LigneVente.COMMENTAIRE_A_COPIER_DANS_COMMANDE_ACHAT);
      }
      ecrireLigneCommentaire(modele.getTexteCommentaire(), ligneVente, documentVenteEnCours, modele.isEditionTousDocument(),
          modele.isCopierSurDocumentAchats());
      
      // Recharger le document
      if (documentVenteEnCours != null && documentVenteEnCours.getId().getNumero() > 0) {
        documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      }
    }
    
    // Cas particulier, si on ajoute un commentaire et que c'est la recherche commentaire qui est en cours, on relance la recherche
    // afin d'afficher l'éventuel article commentaire ajouté.
    if (etape == ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      rechercherArticleCommentaire();
    }
    
    // Ne pas modifier l'étape pour laisser le comptoir dans l'état ou il était avant ajout de l'article commentaire
    rafraichir();
  }
  
  /**
   * Ajouter un article commentaire au document de vente.
   * 
   * @param pSaisie Saisie effectuée dans le résultat de la recherche.
   * @param pArticle Article concerné par la saisie.
   * @param pIndexLigneInseree Index de la ligne à insérer.
   */
  private void ajouterArticleCommentaire(String pSaisie, Article pArticle, int pIndexLigneInseree) {
    // Contrôler la saisie
    if (pSaisie == null || pSaisie.isEmpty()) {
      return;
    }
    
    // Lancement de la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modele =
        ModeleDetailCommentaire.getInstanceAjouterArticleCommentaire(getSession(), pArticle, EnumContexteMetier.CONTEXTE_VENTE);
    modele.setDirectUsine(documentVenteEnCours.isDirectUsine());
    VueDetailCommentaire vue = new VueDetailCommentaire(modele);
    vue.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    if (modele.isSortieAvecAnnulation()) {
      return;
    }
    else if (modele.isSortieAvecSuppression()) {
      // Ne rien faire. On reste dans le mode recherche commentaire. La liste sera raffraichie sans le commentaire supprimé.
    }
    else if (modele.isSortieAvecValidation()) {
      // Créer la ligne document
      int numeroLigne = documentVenteEnCours.calculerNumeroLigneArticle(pIndexLigneInseree);
      LigneVente ligneVente = documentVenteEnCours.ajouterLigneArticleCommentaire(getSession().getIdSession(), pArticle, numeroLigne);
      if (ligneVente == null) {
        throw new MessageErreurException("Erreur lors de l'ajout de l'article commentaire au document.");
      }
      
      // Insérer la ligne commentaire dans le document
      ecrireLigneCommentaire(modele.getTexteCommentaire(), ligneVente, documentVenteEnCours, modele.isEditionTousDocument(),
          modele.isCopierSurDocumentAchats());
      
      // Recharger le document
      if (documentVenteEnCours.getId().getNumero() > 0) {
        documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      }
    }
    
    // Recharger les commentaires (au cas où si le commentaire à été modifé)
    rechercherArticleCommentaire();
    
    // Rafraichir
    setEtape(ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE);
    rafraichir();
  }
  
  /**
   * 
   * Modifier une ligne commentaire du document.
   * 
   * @param LigneVente pLigneVente
   */
  public void modifierLigneCommentaire(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      throw new MessageErreurException("Erreur lors de la modification du commentaire du document.");
    }
    
    // Lancement de la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modele = ModeleDetailCommentaire.getInstanceModifierLigneDocument(getSession(), pLigneVente);
    modele.setDirectUsine(documentVenteEnCours.isDirectUsine());
    VueDetailCommentaire vue = new VueDetailCommentaire(modele);
    vue.afficher();
    
    if (modele.isSortieAvecAnnulation()) {
      return;
    }
    else if (modele.isSortieAvecSuppression()) {
      // RAS
    }
    else if (modele.isSortieAvecValidation()) {
      // Mettre à jour le texte du commentaire dans la ligne
      pLigneVente.setLibelle(modele.getTexteCommentaire());
      
      // Sauver la ligne
      ecrireLigneCommentaire(modele.getTexteCommentaire(), pLigneVente, documentVenteEnCours, modele.isEditionTousDocument(),
          modele.isCopierSurDocumentAchats());
      
      // Recharger le document
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    }
    
    // Rafraichir
    rafraichir();
  }
  
  /**
   * Ecrire une ligne commentaire.
   */
  private void ecrireLigneCommentaire(String pTexteCommentaire, LigneVente pLigneVente, DocumentVente pDocumentVente,
      boolean pEditionFacture, boolean pConserverSurAchat) {
    // Découpage du commentaire saisi en ligne de 120 caractères de long
    String[] texteEnTable = pTexteCommentaire.split("(?<=\\G.{120})|\\n");
    if (texteEnTable == null || texteEnTable.length == 0) {
      return;
    }
    // Ecriture de la première ligne avec l'identifiant original
    pLigneVente.setLibelle(texteEnTable[0]);
    
    // Si le commentaire est sur une seule ligne
    if (texteEnTable.length == 1) {
      ManagerServiceDocumentVente.sauverLigneVenteCommentaire(getSession().getIdSession(), pLigneVente, pDocumentVente, pEditionFacture,
          pConserverSurAchat);
    }
    else {
      
      int nouveauNumeroLigne = pLigneVente.getId().getNumeroLigne();
      // Ecriture des éventuelles autres lignes avec génration d'un nouvel identifiant
      for (int indiceLigne = 0; indiceLigne < texteEnTable.length; indiceLigne++) {
        IdLigneVente idLigne = IdLigneVente.getInstance(pLigneVente.getId().getIdEtablissement(), pLigneVente.getId().getCodeEntete(),
            pLigneVente.getId().getNumero(), pLigneVente.getId().getSuffixe(), nouveauNumeroLigne);
        LigneVente ligne = new LigneVente(idLigne);
        ligne.setLibelle(texteEnTable[indiceLigne]);
        ManagerServiceDocumentVente.sauverLigneVenteCommentaire(getSession().getIdSession(), ligne, pDocumentVente, pEditionFacture,
            pConserverSurAchat);
        int increment = pDocumentVente.getIndexLigneVente(ligne.getId());
        nouveauNumeroLigne += nouveauNumeroLigne + increment;
      }
    }
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article commentaire.
   */
  private String[] formaterLigneArticleCommentaire(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    String ligne[] = new String[pNombreColonnes];
    
    // Libellé ligne
    ligne[2 + pOffset] = pLigneVente.getLibelle();
    return ligne;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet article - Regroupements
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * 
   * Affiche la boite de dialogue Détail d'un regroupement.
   * 
   * @param Regroupement pRegroupement
   */
  private void afficherDetailRegroupement(Regroupement pRegroupement) {
    if (pRegroupement == null) {
      throw new MessageErreurException("Le paramètre pRegroupement est à null.");
    }
    
    ModeleRegroupementLignes modele =
        new ModeleRegroupementLignes(getSession(), this, pRegroupement, documentVenteEnCours, clientCourant.getId());
    VueRegroupementLignes vue = new VueRegroupementLignes(modele);
    vue.afficher();
    
    if (modele.isSortieAvecValidation()) {
      ecrireRegroupement(modele.getRegroupement());
    }
    
    // Chiffrer le documents de ventes pour recalculer les totaux
    ManagerServiceDocumentVente.chiffrerDocumentVente(getSession().getIdSession(), documentVenteEnCours.getId(), true);
    // Recharger le document de ventes
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    
    // Suppression systématique de la sélection dans la liste de la ligne de vente
    indexLigneVenteSelectionnee = -1;
    rafraichir();
  }
  
  /**
   * Lance la fenêtre de création/modification d'un regroupement de lignes
   */
  public void creerRegroupement(int[] pLignesSelectionnees) {
    int indiceDebut = pLignesSelectionnees[0];
    int indiceFin = pLignesSelectionnees[pLignesSelectionnees.length - 1];
    
    Regroupement regroupement = new Regroupement();
    regroupement.chargerLignesArticles(documentVenteEnCours.getListeLigneVente(), indiceDebut, indiceFin);
    regroupement.setEnCoursCreation(true);
    
    regroupement.setNumeroLigneTitre(documentVenteEnCours.calculerNumeroLigneArticle(indiceDebut));
    regroupement.setNumeroLignePied(documentVenteEnCours.calculerNumeroLigneArticle(indiceFin + 1));
    
    afficherDetailRegroupement(regroupement);
    rafraichir();
  }
  
  /**
   * Créé ou modifie un regroupement.
   */
  private void ecrireRegroupement(Regroupement pRegroupement) {
    if (pRegroupement == null) {
      return;
    }
    
    if (documentVenteEnCours.isExistant()) {
      ManagerServiceDocumentVente.sauverRegroupement(getSession().getIdSession(), pRegroupement);
    }
  }
  
  /**
   * 
   * Enlève un regroupement de lignes articles.
   * 
   * @param Regroupement pRegroupement
   */
  public void enleverRegroupement(Regroupement pRegroupement) {
    if (pRegroupement == null) {
      return;
    }
    
    // On supprime le regroupement auquel appartient la ligne sélectionnée
    ManagerServiceDocumentVente.supprimerRegroupement(getSession().getIdSession(), pRegroupement.getIdLigneVenteDebut());
    
    // On relit les lignes du document et on rafraichit l'écran si besoin
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    rafraichir();
  }
  
  /**
   * 
   * Modifie un regroupement de lignes articles.
   * 
   * @param int pIndexReel
   */
  public void modifierRegroupement(int pIndexReel) {
    ListeLigneVente listeLigneVente = documentVenteEnCours.getListeLigneVente();
    // On teste que l'index de la ligne sélectionnée soit valide
    if ((pIndexReel < 0) || (pIndexReel >= listeLigneVente.size())) {
      return;
    }
    // On vérifie que la ligne sélectionnée soit bien une ligne regroupée
    LigneVente ligneVenteSelectionnee = listeLigneVente.get(pIndexReel);
    if (!ligneVenteSelectionnee.isLigneRegroupee()) {
      return;
    }
    // On stocke dans la liste éphémère des lignes à sélectionner la liste des id des lignes articles du regroupement supprimé
    Regroupement regroupement = documentVenteEnCours.getListeRegroupements().get(ligneVenteSelectionnee.getCodeRegroupement());
    if (regroupement != null) {
      afficherDetailRegroupement(regroupement);
    }
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article regroupée.
   */
  private String[] formaterLigneArticleRegroupee(LigneVente pLigneVente, int pNombreColonnes, int pOffset) {
    String ligne[] = new String[pNombreColonnes];
    if (pLigneVente == null) {
      throw new MessageErreurException("La ligne de vente est invalide.");
    }
    
    if (!pLigneVente.isLigneCommentaire()) {
      
      // Quantité en UCV
      if (pLigneVente.getQuantiteUCV() != null) {
        ligne[0] = Constantes.formater(pLigneVente.getQuantiteUCV(), false);
      }
      
      // Dans le cas où on affiche la colonne des quantités d'origine
      if (pOffset > 0) {
        if (informationsExtraction == null) {
          ligne[1] = Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
        }
        else {
          // Quantité d'origine avec le même nombre de décimales que la quantité en UCV : ce nombre de décimale peut être différent de
          // celui de l'unité pour les articles scindables
          ligne[1] = Constantes.formater(informationsExtraction.retournerQuantiteOrigine(pLigneVente.getId().getNumeroLigne()), false);
        }
      }
      
      // Unité de conditionnement de vente
      if (pLigneVente.getIdUniteConditionnementVente() != null) {
        ligne[1 + pOffset] = pLigneVente.getIdUniteConditionnementVente().getCode();
      }
      
      // Libellé
      if (pLigneVente.getLibelle() != null) {
        ligne[2 + pOffset] = "<html>&emsp;&emsp;" + pLigneVente.getLibelle() + "</html>";
      }
      
      // Prix de base
      if (isClientTTC()) {
        ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseTTC(), true);
      }
      else {
        ligne[3 + pOffset] = Constantes.formater(pLigneVente.getPrixBaseHT(), true);
      }
      
      // Unité de vente
      if (pLigneVente.getIdUniteVente() != null) {
        ligne[4 + pOffset] = pLigneVente.getIdUniteVente().getCode();
      }
      
      // Origine du prix
      ligne[5 + pOffset] = pLigneVente.getTexteOriginePrixVente();
      
      // Remise
      ligne[6 + pOffset] = Constantes.formater(pLigneVente.getTauxRemise1(), true) + " %";
      
      // Prix net
      if (isClientTTC()) {
        ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetTTC(), true);
      }
      else {
        ligne[7 + pOffset] = Constantes.formater(pLigneVente.getPrixNetHT(), true);
      }
      
      // Quantité en UCV
      if (pLigneVente.getQuantiteUCV() != null) {
        if (retournerPartieDecimale(pLigneVente.getQuantiteUCV()).compareTo(BigDecimal.ZERO) > 0) {
          ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
        }
        else {
          ligne[8 + pOffset] = Constantes.formater(pLigneVente.getQuantiteUV(), false);
        }
      }
      
      // Montant
      if (isClientTTC()) {
        ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantTTC(), true);
      }
      else {
        ligne[9 + pOffset] = Constantes.formater(pLigneVente.getMontantHT(), true);
      }
    }
    // ligne commentaire (comprend l'en-tete et le pied)
    else {
      ligne[0] = "";
      // Dans le cas où on affiche la colonne des quantités d'origine
      if (pOffset > 0) {
        ligne[1] = "";
      }
      ligne[1 + pOffset] = "";
      if (pLigneVente.isDebutRegroupement() || pLigneVente.isFinRegroupement()) {
        ligne[2 + pOffset] = "<html><b>" + pLigneVente.getLibelle() + "</b></html>";
      }
      else {
        ligne[2 + pOffset] = "<html>&emsp;<b>" + pLigneVente.getLibelle() + "</b></html>";
      }
      ligne[3 + pOffset] = "";
      ligne[4 + pOffset] = "";
      ligne[5 + pOffset] = "";
      ligne[6 + pOffset] = "";
      if (pLigneVente.getInfoComplementaire() != null) {
        // Si il s'agit du pied de regroupement d'articles, la valeur est le montant total du regroupement
        // et cette valeur doit être formatée
        if (pLigneVente.isFinRegroupement()) {
          ligne[7 + pOffset] = "<html><b>"
              + Constantes.formater(Constantes.convertirTexteEnBigDecimal(pLigneVente.getInfoComplementaire()), true) + "</b></html>";
        }
        else {
          ligne[7 + pOffset] = "<html><b>" + pLigneVente.getInfoComplementaire() + "</b></html>";
        }
      }
      else {
        ligne[7 + pOffset] = "";
      }
      ligne[8 + pOffset] = "";
      ligne[9 + pOffset] = "";
      
    }
    
    return ligne;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet règlement
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Charger les données spécifiques de l'onglet réglement.
   */
  public void chargerOngletReglement() {
    // Charger les règlements des factures non réglées :
    // - si le document en cours n'est pas une commande
    // - uniquement pour les clients comptants
    // - uniquement pour les clients existants (modeleDocumentVenteParType est nul pour les nouveaux clients)
    if (!documentVenteEnCours.isCommande() && clientCourant.isClientComptant() && listeFactureNonReglee == null
        && modeleDocumentVenteParType != null) {
      listeAvoirUtilise = new ListeDocumentVenteBase();
      
      // Chargement des id des factures non réglées (attention: si aucune facture n'est retournée il est possible que ce soit à cause d'un
      // problème avec la comptabilisation automatique)
      CritereDocumentVente critere = new CritereDocumentVente();
      critere.setIdEtablissement(etablissement.getId());
      critere.setIdClient(clientCourant.getId());
      List<IdDocumentVente> listeIdFactureNonReglee = ManagerServiceDocumentVente.chargerListeIdFactureNonReglee(getIdSession(), critere);
      // Supprimer la facture en cours de la liste des identifiants pour éviter le doublon
      if (listeIdFactureNonReglee != null && !listeIdFactureNonReglee.isEmpty()) {
        int indexFactureEncours = listeIdFactureNonReglee.indexOf(documentVenteEnCours.getId());
        if (indexFactureEncours != -1) {
          listeIdFactureNonReglee.remove(indexFactureEncours);
        }
      }
      // Chargement des documents à partir de la liste des identifiants
      if (listeIdFactureNonReglee != null && !listeIdFactureNonReglee.isEmpty()) {
        // Chargement des factures non réglées
        listeFactureNonReglee = ManagerServiceDocumentVente.chargerListeDocumentVenteBase(getIdSession(), listeIdFactureNonReglee);
        if (listeFactureNonReglee == null) {
          listeFactureNonReglee = new ListeDocumentVenteBase();
        }
        
        // Parcourir les factures non réglées
        Iterator<DocumentVenteBase> i = listeFactureNonReglee.iterator();
        while (i.hasNext()) {
          DocumentVenteBase documentVenteBase = i.next();
          
          // Créer une ligne avec les informations sur le règlement de la facture
          LigneFactureNonReglee ligneFacture = new LigneFactureNonReglee(getSession());
          ligneFacture.setDocumentVenteBase(documentVenteBase);
          
          // Charger les règlements
          ListeReglement listeReglement =
              ManagerServiceDocumentVente.chargerListeReglement(getSession().getIdSession(), documentVenteBase.getId(), dateTraitement);
          ligneFacture.setListeReglement(listeReglement);
          
          // Attacher la ligne à la facture en cours de traitement
          documentVenteBase.setComplement(ligneFacture);
          
          // Contrôler que la facture ait un montant sinon elle est supprimée de la liste
          if (documentVenteBase.getTotalHT() == null) {
            i.remove();
          }
        }
      }
    }
    
    // Mettre à jour le paiement
    mettreAJourMontantPaiement(documentVenteEnCours);
    
    // Activer le mode paiement comptant si :
    // - soit le bouton "Régler comptant" a été activé
    // - soit le document n'est pas entièrement réglé et ce n'est pas un bon
    // - soit il y a un acompte à payer
    if (activerReglementComptant) {
      modePaiementComptant = true;
    }
    else if ((calculReglement != null && !calculReglement.isEntierementRegle() && !documentVenteEnCours.isBon()) || isAcompteARegler()) {
      modePaiementComptant = true;
    }
  }
  
  /**
   * Charger les données spécifiques de l'onglet édition.
   */
  public void chargerOngletEdition() {
    // Mettre à jour le paiment
    mettreAJourMontantPaiement(documentVenteEnCours);
    
    // Charger les options d'édition du client
    optionsEdition.setEditionChiffree(clientCourant.getEditionChiffre());
    
    // Charger les documents d'achat liés
    listeDocumentAchatLie = recupererListeCommandeAchatLiee(documentVenteEnCours.getId());
  }
  
  /**
   * Met en forme le titre du panel documents achats liés suivant le nombre de documents dans la liste
   */
  public String mettreEnFormeTitreDocumentAchatLie() {
    if (listeDocumentAchatLie == null) {
      return "";
    }
    if (listeDocumentAchatLie.size() == 1) {
      return "Commande d'achats liée (" + listeDocumentAchatLie.size() + ")";
    }
    else {
      return "Commandes d'achats liées (" + listeDocumentAchatLie.size() + ")";
    }
  }
  
  /**
   * Permet de sélectionner la liste des documents d'achats liés.
   */
  public void selectionnerDocumentAchatLie(int[] pListeIndexReels) {
    if (listeDocumentAchatLie == null) {
      return;
    }
    // Sélection des documents à partir de leur index
    listeDocumentAchatLie.selectionner(pListeIndexReels);
    
    rafraichir();
  }
  
  /**
   * Ouvre une session achat par document d'achat à afficher
   */
  public void afficherDocumentAchatLie() {
    if (listeDocumentAchatLie == null) {
      return;
    }
    for (DocumentAchat documentAchat : listeDocumentAchatLie.getListeSelection()) {
      // Préparation des paramètres pour le programme des commandes d'achats
      final ParametreMenuAchat parametreExecutionAchat = new ParametreMenuAchat();
      parametreExecutionAchat.setNumeroOnglet(EnumCleVueAchat.ONGLET_ARTICLE.getIndex());
      parametreExecutionAchat.setIdDocumentAchat(documentAchat.getId());
      parametreExecutionAchat.setFermetureAutomatique(Boolean.TRUE);
      parametreExecutionAchat.setEtapeDocument(DocumentAchat.ETAPE_MODIFICATION_DOCUMENT);
      // Lancement d'une nouvelle session avec le programme des commandes d'achat
      ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.ACHATS),
          parametreExecutionAchat);
    }
  }
  
  /**
   * Affiche la fenêtre qui permet de saisir un nouveau client pour le règlement.
   */
  public void changerClient() {
    ModeleChangementClient modele = new ModeleChangementClient(getSession(), etablissement, clientCourant, utilisateurGescom);
    VueChangementClient vue = new VueChangementClient(modele);
    vue.afficher();
    
    if (modele.isSortieAvecValidation()) {
      changementClientAuReglement = true;
      clientCourant = modele.getClient();
      
      ManagerServiceDocumentVente.sauverDocumentVenteClientModifie(getSession().getIdSession(), documentVenteEnCours.getId(),
          clientCourant.getId(), true, true);
    }
    
    // On recharge le document après le changement du client
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    rafraichir();
  }
  
  /**
   * Retourner la liste des documents d'achats liés
   */
  private ListeDocumentAchat recupererListeCommandeAchatLiee(IdDocumentVente idDocumentVente) {
    if (idDocumentVente == null) {
      return null;
    }
    return ManagerServiceDocumentAchat.recupererCommandeAchatliee(getSession().getIdSession(), idDocumentVente);
  }
  
  /**
   * Controle un règlement comptant.
   */
  private void controlerReglementComptant() {
    // Pour le règlement différé et les avoirs différés le paiement n'est pas contrôlé
    // if (reglementDiffere || (calculReglement != null && calculReglement.isDiffererAvoir())) {
    if (calculReglement != null && calculReglement.isReglementDiffere()) {
      return;
    }
    
    if (!documentVenteEnCours.isCommande() && !calculReglement.isExisteAuMoinsUnReglement()) {
      throw new MessageErreurException("Aucun règlement n'a été saisi.");
    }
    
    // Tester si c'est une commande
    if (documentVenteEnCours.isCommande()) {
      if (isAcompteARegler() && isAcompteObligatoire() && !calculReglement.isAcompteRegle() && !calculReglement.isEntierementRegle()
          && calculReglement.getResteARegler().compareTo(BigDecimal.ZERO) > 0) {
        throw new MessageErreurException("Le règlement de l'acompte n'est pas complet.\nIl reste à régler: "
            + Constantes.formater(calculReglement.getResteARegler(), true));
      }
    }
    // Tester si le montant est réglé en totalité
    else if (!calculReglement.isEntierementRegle()) {
      throw new MessageErreurException(
          "Le règlement n'est pas complet.\nIl reste à régler: " + Constantes.formater(calculReglement.getResteAReglerSurTotal(), true));
    }
  }
  
  /**
   * Activer le mode de paiement comptant.
   */
  public void activerPaiementComptant() {
    if (modePaiementComptant) {
      return;
    }
    activerReglementComptant = true;
    modePaiementComptant = true;
    mettreAJourMontantPaiement(documentVenteEnCours);
    if (documentVenteEnCours.isBon()) {
      documentVenteEnCours.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
    }
    
    if (indiceOngletCourant == ONGLET_ARTICLE) {
      setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
    }
    else if (indiceOngletCourant == ONGLET_REGLEMENT) {
      setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    }
    rafraichir();
  }
  
  /**
   * Activer le paiement de l'acompte.
   */
  public void activerPaiementAcompte() {
    // Si l'encours est dépassé le pourcentage de l'accompte est 100% le client en compte doit payer la totalité
    if (isEncoursEstDepassePaiementComptantObligatoire()) {
      acompteParametre.setPourcentageNormal(Constantes.VALEUR_CENT);
    }
    else if (!isAcompteARegler()) {
      return;
    }
    mettreAJourMontantPaiement(documentVenteEnCours);
    
    // Si le paiement de l'acompte doit s'effectuer alors le mode paiement comptant est activé
    // Si isSaisieArticlePossible() seulement car sinon l'action paiement comptant doit être sélectionnée explicitement par l'utilisateur
    // pour débloquer l'écran (cas d'un encours dépassé)
    if (isSaisieArticlePossible()) {
      modePaiementComptant = true;
    }
  }
  
  /**
   * Retourne s'il est possible d'effacer le règlement saisie.
   */
  public boolean isPossibleEffacerSaisieReglement() {
    boolean actif = true;
    // Ne pas activer le bouton s'il n'est pas nécessaire de payer
    if (!isModePaiementComptant() && !isAcompteARegler()) {
      actif = false;
    }
    // Activer le bouton si l'avoir est différé
    else if (calculReglement != null && calculReglement.isDiffererAvoir()) {
      actif = true;
    }
    // Ne pas afficher le bouton si les règlements n'ont pas été modifiés
    else if (calculReglement != null && calculReglement.getListeReglement() != null && !calculReglement.getListeReglement().isModifie()) {
      actif = false;
    }
    return actif;
  }
  
  /**
   * Retourne s'il est possible d'effacer tous les règlements d'un document.
   */
  public boolean isPossibleEffacerToutReglement() {
    boolean actif = true;
    // Ne pas afficher le bouton si le document est en création
    if (documentVenteEnCours != null && documentVenteEnCours.isEnCoursCreation()) {
      actif = false;
    }
    // Ne pas activer le bouton s'il n'est pas nécessaire de payer
    else if (calculReglement == null) {
      actif = false;
    }
    // Ne pas afficher le bouton si les règlements ne sont pas modifiables
    else if (!calculReglement.isReglementSupprimable()) {
      actif = false;
    }
    return actif;
  }
  
  /**
   * Affiche la boite de dialogue permettant de faire un choix sur le type d'effacement des règlements.
   */
  public void effacerReglement() {
    if (documentVenteEnCours == null) {
      return;
    }
    ModeleSuppressionReglement modeleSuppressionReglement = new ModeleSuppressionReglement(getSession(), this);
    VueSuppressionReglement vueSuppressionReglement = new VueSuppressionReglement(modeleSuppressionReglement);
    vueSuppressionReglement.afficher();
    
    // L'utilisateur a annulé sa demande
    if (!modeleSuppressionReglement.isSortieAvecValidation()) {
      return;
    }
    
    // A partir du moment où le bouton "Effacer règlement" est activé alors le règlement comptant est activé
    activerReglementComptant = true;
    
    // Seule la saisie est effacée
    if (modeleSuppressionReglement.isEffacerSaisie()) {
      initialiserPaiement();
    }
    // Tous les règlements du document seront effacés
    else if (modeleSuppressionReglement.isEffacerTout()) {
      supprimerReglement();
    }
  }
  
  /**
   * Supprime le règlement d'un document si le règlement a été effectué le même jour et s'il est non remis en banque.
   */
  private void supprimerReglement() {
    if (documentVenteEnCours == null) {
      return;
    }
    
    // Suppression du règlement
    if (documentVenteEnCours.getListeReglement() != null && !documentVenteEnCours.getListeReglement().isEmpty()) {
      ManagerServiceDocumentVente.supprimerReglement(getSession().getIdSession(), documentVenteEnCours.getId());
    }
    
    // Rechargement du document
    documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
    initialiserPaiement();
  }
  
  /**
   * Initialise le mode de paiement.
   */
  private void initialiserPaiement() {
    calculReglement = null;
    // Restauration des règlements originaux s'ils ont été sauvegardés
    if (documentVenteEnCours.getListeReglement() != null) {
      ListeReglement listeReglementRestaures = ListeReglement.restaurerReglementOriginaux(documentVenteEnCours.getListeReglement());
      documentVenteEnCours.setListeReglement(listeReglementRestaures);
      // Le règlement comptant a été activé manuellement donc activation du mode comptant lors de la restauration des règlements
      if (activerReglementComptant) {
        modePaiementComptant = true;
      }
      else if (clientCourant != null && (clientCourant.isClientComptant() || clientCourant.isReglementComptantObligatoire())) {
        modePaiementComptant = true;
      }
      else {
        modePaiementComptant = false;
      }
    }
    
    // Déselectionner toutes les factures non réglées
    if (listeFactureNonReglee != null) {
      listeFactureNonReglee.deselectionnerTout();
      // Remise à null des idReglement dans la liste des factures non réglées
      for (int ligne = 0; ligne < listeFactureNonReglee.size(); ligne++) {
        DocumentVenteBase documentVenteBase = listeFactureNonReglee.get(ligne);
        LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
        ligneFacture.setIdReglement(null);
      }
      listeAvoirUtilise.clear();
      mettreAJourMontantPaiement(documentVenteEnCours);
    }
    
    // Active le paiement de l'acompte ou du reste à régler
    if (isAcompteARegler()) {
      activerPaiementAcompte();
    }
    else {
      if (modePaiementComptant) {
        activerPaiementComptant();
      }
    }
    
    // On remet à jour les données pour le paiement si cela n'a pas déjà été fait dans les traitements précédemments
    if (calculReglement == null) {
      mettreAJourMontantPaiement(documentVenteEnCours);
    }
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Permet de sélectionner ou non toutes les factures non réglées (les avoirs sont ignorés).
   */
  public void selectionnerToutesFacturesNonReglees(boolean pSelectionnerTout) {
    if (listeFactureNonReglee == null) {
      return;
    }
    // Sélectionne ou désélectionne uniquement les factures non réglées en ignorant les avoirs
    for (int ligne = 0; ligne < listeFactureNonReglee.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeFactureNonReglee.get(ligne);
      LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
      // Ce n'est pas un avoir: lecture du suivant
      if (ligneFacture == null || ligneFacture.isEstUnAvoir()) {
        continue;
      }
      listeFactureNonReglee.changerSelection(ligne, pSelectionnerTout);
    }
    mettreAJourMontantPaiement(documentVenteEnCours);
    rafraichir();
  }
  
  /**
   * Permet de sélectionner la liste des factures non réglées.
   */
  public void selectionnerFacturesNonReglees(int[] pListeIndexReels) {
    if (listeFactureNonReglee == null) {
      return;
    }
    // Sélection des documents à partir de leur index
    listeFactureNonReglee.selectionner(pListeIndexReels);
    
    // Traitement des factures différées (les avoirs sont igorés car les traitements sont différents)
    // Les montants des factures différées sont ajoutés au montant du document à régler
    mettreAJourMontantPaiement(documentVenteEnCours);
    
    // Traitement des avoirs différés
    // Parcourt de la liste des avoirs utilisés afin de détecter les avoirs qui ont été déselectionnés
    int index = 0;
    while (index < listeAvoirUtilise.size()) {
      DocumentVenteBase documentVenteBase = listeAvoirUtilise.get(index);
      // L'avoir n'est plus sélectionné, il faut le décompter
      if (!listeFactureNonReglee.isSelectionne(documentVenteBase)) {
        LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
        calculReglement.supprimerReglement(ligneFacture.getIdReglement());
        ligneFacture.setIdReglement(null);
        listeAvoirUtilise.remove(index);
      }
      else {
        index++;
      }
    }
    // Mise à jour de la liste des avoirs utilisés
    listeAvoirUtilise.clear();
    for (int ligne = 0; ligne < listeFactureNonReglee.size(); ligne++) {
      DocumentVenteBase documentVenteBase = listeFactureNonReglee.get(ligne);
      LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
      // Ce n'est pas un avoir: lecture du suivant
      if (ligneFacture == null || !ligneFacture.isEstUnAvoir()) {
        continue;
      }
      // L'avoir n'est pas sélectionné: lecture du suivant
      if (!listeFactureNonReglee.isSelectionne(documentVenteBase.getId())) {
        continue;
      }
      // L'avoir est sélectionné, il est ajouté à la liste des avoirs utilisés
      listeAvoirUtilise.add(listeFactureNonReglee.get(ligne));
      // Si son idReglement est null alors il s'agit d'un nouvel avoir que l'on ajoute aux règlements
      if (ligneFacture.getIdReglement() == null) {
        payerAvecAvoir(ligneFacture);
      }
    }
    
    rafraichir();
  }
  
  /**
   * Calcule le montant total TTC à régler (facture en cours et éventuellement réglements multiples).
   */
  private BigDecimal calculMontantTotalTTC(DocumentVente pDocumentVente) {
    // Retourner le total du document de ventes si aucune autre factures n'a de reste à régler
    if (listeFactureNonReglee == null || listeFactureNonReglee.isSelectionVide()) {
      return pDocumentVente.getTotalTTC();
    }
    
    // Parcourir les factures sélectionnées
    BigDecimal totalTTC = BigDecimal.ZERO;
    for (DocumentVenteBase documentVenteBase : listeFactureNonReglee.getListeSelection()) {
      // Récupérer l'objet complémentaire
      LigneFactureNonReglee ligneFacture = (LigneFactureNonReglee) documentVenteBase.getComplement();
      if (ligneFacture != null && !ligneFacture.isEstUnAvoir()) {
        // Ajouter le montant restant à régler des factures (et non des avoirs) différées sélectionnées au total du document initial
        totalTTC = totalTTC.add(ligneFacture.getResteAPayerTTC());
      }
    }
    
    return pDocumentVente.getTotalTTC().add(totalTTC);
  }
  
  /**
   * Mettre à jour les montants pour le paiement.
   */
  public void mettreAJourMontantPaiement(DocumentVente pDocumentVente) {
    if (pDocumentVente.isDevis()) {
      return;
    }
    
    if (calculReglement == null) {
      // Création de l'acompte si nécessaire
      if (isAcompteARegler()) {
        acompteParametre.setDocumentContientArticleSpecial(pDocumentVente.isContientArticleSpecial());
        acompte = new Acompte(acompteParametre, pDocumentVente.getTotalTTC());
      }
      
      // Initialisation du montant total TTC avec celui du document
      BigDecimal montantTotalTTC = pDocumentVente.getTotalTTC();
      // S'il s'agit d'un avoir créer directement, le montant total TTC est forcé en négatif (puisque remboursement)
      if (isEnCoursSaisieAvoirTotal) {
        montantTotalTTC = montantTotalTTC.negate();
      }
      // Création et initialisation du calcul du règlement
      calculReglement =
          new CalculReglement(getSession(), pDocumentVente.getTypeDocumentVente(), montantTotalTTC, pDocumentVente.getListeReglement(),
              pDocumentVente.getId().getIdEtablissement(), pDocumentVente.getId().getNumero(), acompte, pDocumentVente.isModifiable());
    }
    else {
      // Mise à jour du montant de l'acompte à partir du total du document qui a pu changer
      if (acompte != null) {
        acompteParametre.setDocumentContientArticleSpecial(pDocumentVente.isContientArticleSpecial());
        acompte.setAcompteParametre(acompteParametre);
        acompte.modifierMontantAcompte(calculMontantTotalTTC(pDocumentVente));
      }
      // Mise à jour des données du calcul du règlement
      calculReglement.modifierMontantTotalARegler(calculMontantTotalTTC(pDocumentVente));
    }
  }
  
  /**
   * Retourne si un acompte doit être réglé.
   */
  public boolean isAcompteARegler() {
    return Acompte.isARegler(documentVenteEnCours);
  }
  
  /**
   * Retourne si l'acompte est obligatoire.
   */
  public boolean isAcompteObligatoire() {
    if (acompte != null && acompte.isObligatoire()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si un règlement comptant est possible.
   */
  public boolean isReglementPossible() {
    boolean reglementPossible = true;
    // Le règlement n'est pas possible si le mode de paiement n'est pas comptant
    if (!modePaiementComptant) {
      reglementPossible = false;
    }
    // Le règlement n'est pas possible si le document est entièrement réglé
    else if (calculReglement == null || calculReglement.isEntierementRegle()) {
      reglementPossible = false;
    }
    // Le règlement n'est pas possible si le document n'est pas modifiable et qu'il ne s'agit pas d'une facture
    else if (documentVenteEnCours == null || (!documentVenteEnCours.isFacture() && !documentVenteEnCours.isModifiable())) {
      reglementPossible = false;
    }
    
    return reglementPossible;
  }
  
  /**
   * Paiement comptant par chèque.
   */
  public void payerParCheque(String pMontant) {
    if (!modePaiementComptant && !isAcompteARegler()) {
      throw new MessageErreurException("Impossible de payer par chèque car le mode paiement n'est pas comptant.");
    }
    
    // Création du règlement
    Reglement reglement = new Reglement(IdReglement.getInstanceAvecCreationId(documentVenteEnCours.getId().getIdEtablissement()));
    reglement.setTypeReglement(EnumTypeReglement.CHEQUE);
    reglement.setLibelle(EnumTypeReglement.CHEQUE.getLibelleEnMinuscules());
    reglement.setMontant(Constantes.convertirTexteEnBigDecimal(pMontant));
    
    // Affichage de la boite de dialoque permettant la saisie du détail sur le chèque
    ModeleDetailCheque modeleDetailCheque = new ModeleDetailCheque(getSession(), reglement);
    VueDetailCheque vueDetailCheque = new VueDetailCheque(modeleDetailCheque);
    vueDetailCheque.afficher();
    
    // Traitement du règlement
    calculReglement.ajouterReglement(reglement, documentVenteEnCours.getTypeDocumentVente());
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Paiement comptant par carte bancaire.
   */
  public void payerParCarteBancaire(String pMontant) {
    if (!modePaiementComptant && !isAcompteARegler()) {
      throw new MessageErreurException("Impossible de payer par carte bancaire car le mode paiement n'est pas comptant.");
    }
    
    // Création du règlement
    Reglement reglement = new Reglement(IdReglement.getInstanceAvecCreationId(documentVenteEnCours.getId().getIdEtablissement()));
    reglement.setTypeReglement(EnumTypeReglement.CARTE_BANCAIRE);
    reglement.setLibelle(EnumTypeReglement.CARTE_BANCAIRE.getLibelleEnMinuscules());
    reglement.setMontant(Constantes.convertirTexteEnBigDecimal(pMontant));
    
    calculReglement.ajouterReglement(reglement, documentVenteEnCours.getTypeDocumentVente());
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Paiement comptant en espèces.
   */
  public void payerEnEspeces(String pMontant) {
    if (!modePaiementComptant && !isAcompteARegler()) {
      throw new MessageErreurException("Impossible de payer en espèces car le mode paiement n'est pas comptant.");
    }
    
    BigDecimal montant = Constantes.convertirTexteEnBigDecimal(pMontant);
    
    // Création du règlement
    Reglement reglement = new Reglement(IdReglement.getInstanceAvecCreationId(documentVenteEnCours.getId().getIdEtablissement()));
    reglement.setTypeReglement(EnumTypeReglement.ESPECES);
    reglement.setLibelle(EnumTypeReglement.ESPECES.getLibelleEnMinuscules());
    reglement.setMontant(montant);
    
    calculReglement.ajouterReglement(reglement, documentVenteEnCours.getTypeDocumentVente());
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Paiement comptant par virement.
   */
  public void payerParVirement(String pMontant) {
    if (!modePaiementComptant && !isAcompteARegler()) {
      throw new MessageErreurException("Impossible de payer par virement car le mode paiement n'est pas comptant.");
    }
    
    // Création du règlement
    Reglement reglement = new Reglement(IdReglement.getInstanceAvecCreationId(documentVenteEnCours.getId().getIdEtablissement()));
    reglement.setTypeReglement(EnumTypeReglement.VIREMENT);
    reglement.setLibelle(EnumTypeReglement.VIREMENT.getLibelleEnMinuscules());
    reglement.setMontant(Constantes.convertirTexteEnBigDecimal(pMontant));
    
    calculReglement.ajouterReglement(reglement, documentVenteEnCours.getTypeDocumentVente());
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Paiement avec un avoir.
   */
  public void payerAvecAvoir(LigneFactureNonReglee pLigneFactureNonReglee) {
    if (pLigneFactureNonReglee == null || !pLigneFactureNonReglee.isEstUnAvoir()) {
      throw new MessageErreurException("Impossible de payer avec le document sélectionné car celui-ci est invalide.");
    }
    if (pLigneFactureNonReglee.getIdReglement() != null) {
      throw new MessageErreurException("Cet avoir a déjà été utilisé lors de ce règlement.");
    }
    
    DocumentVenteBase documentVenteBase = pLigneFactureNonReglee.getDocumentVenteBase();
    if (documentVenteBase == null || documentVenteBase.getId() == null) {
      throw new MessageErreurException("Impossible de payer avec l'avoir car celui-ci est invalide.");
    }
    if (!modePaiementComptant && !isAcompteARegler()) {
      throw new MessageErreurException("Impossible de payer avec un avoir car le mode paiement n'est pas comptant.");
    }
    
    // Calcul du montant utilisé sur l'avoir (partiel ou en totalité)
    BigDecimal montantAvoir = pLigneFactureNonReglee.getResteAPayerTTC().abs();
    if (calculReglement.getMontantARegler().compareTo(montantAvoir) < 0) {
      montantAvoir = calculReglement.getMontantARegler();
    }
    // Création du règlement
    Reglement reglement = new Reglement(IdReglement.getInstanceAvecCreationId(documentVenteEnCours.getId().getIdEtablissement()));
    reglement.setTypeReglement(EnumTypeReglement.AVOIR);
    reglement.setLibelle(EnumTypeReglement.AVOIR.getLibelleEnMinuscules() + " n°" + documentVenteBase.getId());
    reglement.setMontant(montantAvoir);
    
    calculReglement.ajouterReglement(reglement, documentVenteEnCours.getTypeDocumentVente());
    pLigneFactureNonReglee.setIdReglement(reglement.getId());
    
    setEtape(ETAPE_SAISIE_ONGLET_REGLEMENT);
    rafraichir();
  }
  
  /**
   * Controle l'autorisation du paiement d'une facture en différé.
   */
  private boolean controlerAutorisationDiffere(boolean pReglementDiffereAccepte) {
    // Vérification des droits de l'utilisateur concernant les demandes d'autorisation pour effectuer des différés
    if (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_A_EFFECTUER_DIFFERE_SANS_DEMANDE_AUTORISATION)) {
      return true;
    }
    if (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_A_EFFECTUER_DIFFERE_ET_A_AUTORISER_DES_DEMANDES)) {
      return true;
    }
    
    // On controle dans le cas où le différé a déjà été accepté et que le montant n'a pas changé
    if (pReglementDiffereAccepte && montantReglementDiffereAccepte.compareTo(calculReglement.getMontantTotal()) == 0) {
      return true;
    }
    
    // Affichage de la boite de dialogue
    ModeleAutorisationDiffere modele = new ModeleAutorisationDiffere(getSession(), this, calculReglement, utilisateurGescom);
    VueAutorisationDiffere vue = new VueAutorisationDiffere(modele);
    vue.afficher();
    
    if (modele.isSortieAvecValidation()) {
      pReglementDiffereAccepte = modele.isAutorisationAcceptee();
    }
    else {
      pReglementDiffereAccepte = false;
    }
    
    if (pReglementDiffereAccepte) {
      // On stocke le montant total pour l'autorisation du différé
      montantReglementDiffereAccepte = calculReglement.getMontantTotal();
    }
    return pReglementDiffereAccepte;
  }
  
  /**
   * Permet de valider un règlement différé.
   */
  public void reglerEnDifferer() {
    // Pour les clients comptants souhaitant une facture différée
    if (isModePaiementComptant() && documentVenteEnCours.isFacture()) {
      boolean reglementDiffere = controlerAutorisationDiffere(isDiffererPaiement());
      if (calculReglement != null) {
        calculReglement.setDiffererPaiement(reglementDiffere);
      }
      if (!reglementDiffere) {
        return;
      }
    }
    
    controlerOngletReglement();
    rafraichir();
  }
  
  /**
   * Permet de valider un avoir différé.
   */
  public void differerAvoir() {
    calculReglement.setDiffererAvoir(true);
    rafraichir();
  }
  
  /**
   * Valider l'onglet Règlement.
   */
  public boolean controlerOngletReglement() {
    // Contrôler la présence d'un document de ventes
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de contrôler l'onglet règlement car le document de ventes est invalide");
    }
    
    // Ne rien contrôler si c'est un devis
    if (documentVenteEnCours.isDevis()) {
      return true;
    }
    
    // Ne rien contrôler si le document n'est pas modifiable et s'il n'est pas une facture (car une facture peut avoir son règlement
    // modifiable)
    if (!documentVenteEnCours.isModifiable() && !documentVenteEnCours.isFacture()) {
      return true;
    }
    
    // Ne rien contrôler si le document est une facture d'un client en compte et qu'il s'agit d'un remboursement
    if (documentVenteEnCours.isFacture() && clientCourant.isClientEnCompte() && calculReglement != null
        && calculReglement.isRemboursement()) {
      return true;
    }
    
    // Si le total TTC du document est égal à zéro, le document est considéré comme réglé
    if (documentVenteEnCours.getTotalTTC().compareTo(BigDecimal.ZERO) == 0) {
      // Si le document ne contient aucun article alors l'onglet article est activé
      if (!documentVenteEnCours.isContientArticle()) {
        setIndiceOnglet(ONGLET_ARTICLE);
        setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
        return false;
      }
      else {
        return true;
      }
    }
    
    // Contrôler le règlement
    if ((calculReglement != null && !calculReglement.isEntierementRegle() && !documentVenteEnCours.isBon()) || isModePaiementComptant()
        || isAcompteARegler()) {
      documentVenteEnCours.setListeReglement(calculReglement.getListeReglement());
      calculReglement.mettreAZeroMontantARendre();
      if (documentVenteEnCours.isBon()) {
        documentVenteEnCours.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
      }
      controlerReglementComptant();
    }
    
    // Contrôler qu'il ne reste plus rien à régler
    if (calculReglement != null) {
      if (!documentVenteEnCours.isCommande() && calculReglement.getResteARegler().compareTo(BigDecimal.ZERO) > 0
          && isModePaiementComptant() && !isDiffererPaiement()) {
        throw new MessageErreurException("Impossible d'éditer le document s'il reste un règlement à effectuer.");
      }
      else if (isAcompteARegler() && isAcompteObligatoire() && !calculReglement.isAcompteRegle()) {
        throw new MessageErreurException("Impossible d'éditer le document si l'acompte obligatoire n'a pas été réglé.");
      }
    }
    
    return true;
  }
  
  /**
   * Quitter l'onglet règlement.
   */
  public void validerOngletReglement() {
    // Valider l'onglet règlement
    if (!controlerOngletReglement()) {
      rafraichir();
      return;
    }
    
    // Passer à l'onglet édition
    setIndiceOnglet(ONGLET_EDITION);
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Onglet édition
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Activer l'édition du bon de préparation.
   */
  public void activerEditionBonDePreparation(boolean pEditerBonDePreparation) {
    if (optionsEdition != null) {
      optionsEdition.setEditionSupplementaireBonDePreparation(pEditerBonDePreparation);
    }
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * Choisir une édition avec totaux.
   */
  public void choisirTypeEdition(EnumTypeEdition pTypeEdition) {
    if (pTypeEdition == optionsEdition.getEditionChiffree()) {
      return;
    }
    optionsEdition.setEditionChiffree(pTypeEdition);
    
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * Choisir un type d'édition de devis.
   */
  public void choisirTypeEditionDevis(EnumTypeEditionDevis pTypeEdition) {
    if (optionsEdition == null || pTypeEdition == optionsEdition.getTypeEditionDevis()) {
      return;
    }
    optionsEdition.setTypeEditionDevis(pTypeEdition);
    
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * 
   * Choisir si l'on édite une facture (ou un avoir) sur une facture ou sur un ticket de caisse.
   * 
   * @param pEditionFacture
   */
  public void choisirTypeEditionFacture(EnumTypeEditionFacture pEditionFacture) {
    if (optionsEdition == null || Constantes.equals(pEditionFacture, optionsEdition.getTypeEditionFacture())) {
      return;
    }
    optionsEdition.setTypeEditionFacture(pEditionFacture);
    
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * Modifier le paramètre isMonoFournisseur et mettre à jour l'id
   * adresse fournisseur du document de vente
   */
  public void modifierMonoFournisseur(boolean pMonoFournisseur) {
    documentVenteEnCours.setMonoFournisseur(pMonoFournisseur);
    if (!documentVenteEnCours.isMonoFournisseur()) {
      documentVenteEnCours.setIdAdresseFournisseur(null);
    }
    rafraichir();
  }
  
  /**
   * Modifier la valeur pour la génération de commande d'achat.
   */
  public void modifierGenerationCommandeAchat(boolean pGenerationCommandeAchat) {
    if (pGenerationCommandeAchat == documentVenteEnCours.isGenerationImmediateCommandeAchat()) {
      return;
    }
    documentVenteEnCours.setGenerationImmediateCommandeAchat(pGenerationCommandeAchat);
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * Modifie la sélection d'un mode d'édition.
   */
  public void modifierModeEdition(EnumModeEdition pEnumModeEdition, boolean pSelectionne) {
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(pEnumModeEdition);
    if (modeEdition == null || modeEdition.isSelectionne() == pSelectionne) {
      return;
    }
    modeEdition.setSelectionne(pSelectionne);
    rafraichir();
  }
  
  /**
   * Modifie l'email du destinataire pour l'édition.
   */
  public void modifierEmailDestinataire(Contact pContact, boolean pRafraichirPanel) {
    if (pContact == null && Constantes.equals(contactMail, pContact)) {
      return;
    }
    contactMail = pContact;
    String email = "";
    // Contrôle du premier email
    if (contactMail.getEmail() != null) {
      email = Constantes.normerTexte(contactMail.getEmail());
    }
    // Si le premier email est vide, contrôle du deuxième
    if (email.isEmpty()) {
      if (contactMail.getEmail2() != null) {
        email = Constantes.normerTexte(contactMail.getEmail2());
      }
    }
    if (email.equals(emailDestinataireEdition)) {
      return;
    }
    emailDestinataireEdition = email;
    
    if (pRafraichirPanel) {
      rafraichir();
    }
  }
  
  /**
   * Modifie l'email du destinataire pour l'édition.
   */
  public void modifierEmailDestinataire(String pEmail) {
    pEmail = Constantes.normerTexte(pEmail);
    if (pEmail.equals(emailDestinataireEdition)) {
      return;
    }
    emailDestinataireEdition = pEmail;
    contactMail = null;
    rafraichir();
  }
  
  /**
   * Modifie le fax du destinataire pour l'édition.
   */
  public void modifierFaxDestinataire(Contact pContact, boolean pRafraichirPanel) {
    if (pContact == null && Constantes.equals(contactFax, pContact)) {
      return;
    }
    contactFax = pContact;
    String numeroFax = "";
    if (contactFax.getNumeroFax() != null) {
      numeroFax = Constantes.normerTexte(contactFax.getNumeroFax());
    }
    if (numeroFax.equals(faxDestinataireEdition)) {
      return;
    }
    faxDestinataireEdition = numeroFax;
    
    if (pRafraichirPanel) {
      rafraichir();
    }
  }
  
  /**
   * Modifie le fax du destinataire pour l'édition.
   */
  public void modifierFaxDestinataire(String pNumeroFax) {
    pNumeroFax = Constantes.normerTexte(pNumeroFax);
    if (pNumeroFax.equals(faxDestinataireEdition)) {
      return;
    }
    faxDestinataireEdition = pNumeroFax;
    contactFax = null;
    rafraichir();
  }
  
  /**
   * Retourne si l'on doit afficher les options de chiffrage.
   */
  public boolean isAfficherOptionChiffrage() {
    // Pas d'affichage si le document est une facture ou un devis
    if (documentVenteEnCours.isFacture() || documentVenteEnCours.isDevis()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Traitement dans le cas où il y a un traitement multiple.
   */
  private void reglementMultiple() {
    // Sortir de suite s'il n'y a aucune facture non réglée de sélectionnée
    if (listeFactureNonReglee == null || listeFactureNonReglee.isSelectionVide()) {
      return;
    }
    
    // Parcourir les factures non réglées sélectionnées
    boolean initliste = true;
    for (DocumentVenteBase documentVenteBase : listeFactureNonReglee.getListeSelection()) {
      // Sauver chaque règlement
      ManagerServiceDocumentVente.sauverDocumentVenteReglementMultiple(getSession().getIdSession(), initliste, documentVenteBase.getId());
      initliste = false;
    }
  }
  
  /**
   * Modifier la date de facturation minimale d'un bon de vente (date avant laquelle un bon ne pourra être facturé).
   */
  public void modifierDateFacturationMinimale(Date pDateFacturationMinimale) {
    pDateFacturationMinimale = Constantes.normerDate(pDateFacturationMinimale);
    if (pDateFacturationMinimale.equals(dateTraitement)) {
      return;
    }
    
    documentVenteEnCours.setDateMiniFacturation(pDateFacturationMinimale);
    
    setEtape(ETAPE_SAISIE_ONGLET_EDITION);
    rafraichir();
  }
  
  /**
   * Lance la validation du document en cours.
   */
  private boolean controlerOngletEdition() {
    // Contrôler la présence d'un document de ventes
    if (documentVenteEnCours == null) {
      throw new MessageErreurException("Impossible de contrôler l'onglet édition car le document de ventes est invalide");
    }
    
    // Controle des données de l'onglet
    for (ModeEdition modeEdition : listeModeEditionVente) {
      // Dans le cas où l'envoi par mail a été sélectionné
      if (EnumModeEdition.ENVOYER_PAR_MAIL.equals(modeEdition.getEnumModeEdition()) && modeEdition.isSelectionne()) {
        // Controle de la validité de l'adresse email du destinataire
        if (!Constantes.controlerEmail(emailDestinataireEdition)) {
          throw new MessageErreurException("L'adresse email du destinataire n'est pas valide.");
        }
        break;
      }
      // Dans le cas où l'envoi par fax a été sélectionné
      if (EnumModeEdition.ENVOYER_PAR_FAX.equals(modeEdition.getEnumModeEdition()) && modeEdition.isSelectionne()) {
        // Controle de la validité du numéro de fax du destinataire
        if (faxDestinataireEdition == null || faxDestinataireEdition.trim().isEmpty()) {
          throw new MessageErreurException("Le numéro de fax du destinataire n'est pas valide.");
        }
        break;
      }
    }
    
    // Ne rien contrôler si le document n'est pas modifiable et s'il n'est pas une facture (car une facture peut avoir son règlement
    // modifiable)
    if (!documentVenteEnCours.isModifiable() && !documentVenteEnCours.isFacture()) {
      return true;
    }
    
    // Contrôle la validité des données du document de vente.
    ManagerServiceDocumentVente.controlerDocumentVente(getSession().getIdSession(), documentVenteEnCours, dateTraitement);
    
    return true;
  }
  
  /**
   * Afficher la commande d'achat générée dans une nouvelle session.
   */
  private void afficherCommandeAchat(final IdDocumentAchat pIdDocumentAchat) {
    // Contrôle que la PS327 soit activée et qu'il s'agisse d'un document direct usine
    if (!isAfficherCommandeAchat()) {
      return;
    }
    if (pIdDocumentAchat == null) {
      throw new MessageErreurException("L'id du document d'achat généré est invalide.");
    }
    
    // Préparation des paramètres pour le programme des commandes d'achats
    final ParametreMenuAchat parametreExecutionAchat = new ParametreMenuAchat();
    parametreExecutionAchat.setNumeroOnglet(EnumCleVueAchat.ONGLET_ARTICLE.getIndex());
    parametreExecutionAchat.setIdDocumentAchat(pIdDocumentAchat);
    parametreExecutionAchat.setFermetureAutomatique(Boolean.TRUE);
    parametreExecutionAchat.setEtapeDocument(DocumentAchat.ETAPE_MODIFICATION_DOCUMENT);
    // Lancement d'une nouvelle session avec le programme des commandes d'achat
    ManagerSessionClient.getInstance().lancerPointMenu(getSession(), IdEnregistrementMneMnp.getInstance(EnumPointMenu.ACHATS),
        parametreExecutionAchat);
  }
  
  /**
   * Attribuer une adresse fournisseur au document de vente en cours
   */
  public void modifierAdresseFournisseur(AdresseFournisseur pAdresseFournisseur) {
    if (documentVenteEnCours != null) {
      if (pAdresseFournisseur != null) {
        documentVenteEnCours.setIdAdresseFournisseur(pAdresseFournisseur.getId());
      }
      else {
        documentVenteEnCours.setIdAdresseFournisseur(null);
      }
    }
    rafraichir();
  }
  
  /**
   * Valide les zones de l'onglet Edition
   */
  public void validerOngletEdition() {
    // Contrôler tous les onglets
    if (!controlerOngletClient()) {
      return;
    }
    if (!controlerOngletLivraison()) {
      return;
    }
    if (!controlerOngletArticle()) {
      return;
    }
    if (!controlerOngletReglement()) {
      rafraichir();
      return;
    }
    if (!controlerOngletEdition()) {
      return;
    }
    
    // Sauver les règlements (s'il ne s'agit pas d'un devis)
    if (!documentVenteEnCours.isDevis() || documentVenteEnCours.isModifiable() || documentVenteEnCours.isFacture()) {
      // Controle du règlement
      if (isModePaiementComptant() || isAcompteARegler()) {
        reglementMultiple();
        
        // Enregistrement des règlements
        if (documentVenteEnCours.getListeReglement() != null && documentVenteEnCours.getListeReglement().isModifie()) {
          ManagerServiceDocumentVente.sauverListeReglement(getIdSession(), documentVenteEnCours.getId(),
              documentVenteEnCours.getListeReglement());
          // Lecture du règlement qui vient d'être sauvé
          ListeReglement listeReglement =
              ManagerServiceDocumentVente.chargerListeReglement(getIdSession(), documentVenteEnCours.getId(), dateTraitement);
          // Initialisation de la classe CalculReglement suite au chargement des règlements afin de ne pas sauvegarder inutilement les
          // règlements (risque de bug avec le service qui sauvegarde en double les montants)
          if (listeReglement != null) {
            listeReglement.initialiserInfoDocument(documentVenteEnCours);
          }
          calculReglement = new CalculReglement(getSession(), documentVenteEnCours.getTypeDocumentVente(),
              documentVenteEnCours.getTotalTTC(), listeReglement, documentVenteEnCours.getId().getIdEtablissement(),
              documentVenteEnCours.getId().getNumero(), acompte, documentVenteEnCours.isModifiable());
          documentVenteEnCours.setListeReglement(listeReglement);
        }
      }
    }
    
    // Modifier le statut du pro-devis
    if (documentVenteEnCours.isModifiable() && proDevisOrigine != null) {
      ManagerServiceDocumentVente.modifierStatutProDevis(getSession().getIdSession(), proDevisOrigine, EnumStatutProDevis.IMPORTE);
    }
    
    // Vérifier que l'utilisateur a sélectionné un mode d'édition ou l'édition d'un document supplémentaire
    if ((listeModeEditionVente == null || listeModeEditionVente.isAucuneSelection())
        && !optionsEdition.isEditionDocumentSupplementaire()) {
      if (!DialogueConfirmation.afficher(getSession(), "Confirmation du choix des modes d'édition",
          "Vous n'avez sélectionné aucun mode d'édition.\nConfirmez-vous la validation du document sans édition ?.")) {
        return;
      }
    }
    
    // On sauve à nouveau l'entête à cause de l'option de génération de bon d'achat...
    if (documentVenteEnCours.isModifiable()) {
      sauverEnteteDocumentVente(documentVenteEnCours);
    }
    
    // Initialiser les options de validation
    initialiserOptionValidation();
    if (optionsValidation == null) {
      throw new MessageErreurException("Impossible d'éditer le document de ventes car les options de validation sont invalides.");
    }
    
    // Valider le document de ventes s'il est modifiable
    if (documentVenteEnCours.isModifiable()) {
      // S'il s'agit d'une extraction, il faut effectuer des traitements sur le document d'origine
      if (informationsExtraction != null && informationsExtraction.getDocumentVenteSource() != null) {
        // On met à blanc l'option de validation c'est le service qui va mettre la bonen valeur
        ManagerServiceDocumentVente.validerExtractionDocumentVente(getSession().getIdSession(),
            informationsExtraction.getDocumentVenteSource(), dateTraitement, new OptionsValidation());
      }
      
      Trace.debug(ModeleComptoir.class, "validerDocument", "option", optionsValidation.getCodePropose());
      documentVenteEnCours = ManagerServiceDocumentVente.validerDocumentVente(getSession().getIdSession(), documentVenteEnCours,
          optionsValidation, dateTraitement);
    }
    
    // Charger le document de vente après sa validation s'il s'agit d'une facture afin de regénérer l'id
    if (documentVenteEnCours.isFacture()) {
      documentVenteEnCours = chargerDocumentVenteAvecConservationType(documentVenteEnCours);
      IdDocumentVente id = IdDocumentVente.getInstancePourFacture(documentVenteEnCours.getId().getIdEtablissement(),
          documentVenteEnCours.getNumeroFacture());
      documentVenteEnCours.setId(id);
      
      // Modifier le numéro de facture sur le bon de cour maintenant qu'il est généré
      for (LigneVente ligne : documentVenteEnCours.getListeLigneVente()) {
        if (ligne.isLigneBonCour()) {
          Pattern pattern = Pattern.compile("[0-9]+");
          Matcher matcher = pattern.matcher(ligne.getLibelle());
          while (matcher.find()) {
            String numeroBonCour = matcher.group();
            BonCour bonCour = ManagerServiceDocumentVente.chargerBonCour(getIdSession(), etablissement.getId(), idMagasin,
                Integer.parseInt(numeroBonCour));
            bonCour.setEtatBonCour(EnumEtatBonCour.UTILISE);
            bonCour.setIdDocumentGenere(documentVenteEnCours.getId());
            ManagerServiceDocumentVente.modifierBonCour(getIdSession(), bonCour);
          }
        }
      }
    }
    
    // Préparer l'édition
    EditionDocument editionDocument = new EditionDocument(getSession(), documentVenteEnCours, dateTraitement, listeModeEditionVente,
        optionsValidation, optionsEdition, listeVendeur);
    editionDocument.setEmailDestinataire(getEmailDestinataireEdition());
    editionDocument.setNumeroFaxDestinataire(getFaxDestinataireEdition());
    
    // Lancer l'édition en tâche de fond
    SwingWorkerAttente.executer(((SessionJava) getSession()).getFenetre(), editionDocument, "Edition en cours ...");
    
    // Dans le cas où l'utilisateur a demandé la génération de la commande d'achat, si la PS327 est activé alors affichage de cette
    // commande dans une nouvelle session
    if (isAfficherCommandeAchat()) {
      // Affichage des commandes d'achat
      ModeleCommandeAchatLiee modeleCommandeAchatLiee = new ModeleCommandeAchatLiee(getSession(), documentVenteEnCours.getId());
      VueCommandeAchatLiee vueCommandeAchatLiee = new VueCommandeAchatLiee(modeleCommandeAchatLiee);
      vueCommandeAchatLiee.afficher();
      
      // Si on sort avec validation
      if (modeleCommandeAchatLiee.isSortieAvecValidation()) {
        // Si des documents sont sélectionnés
        if (modeleCommandeAchatLiee.getListeDocumentAchatSelectionnes() != null
            && !modeleCommandeAchatLiee.getListeDocumentAchatSelectionnes().isEmpty()) {
          // On ouvre une session d'achat par document sélectionné
          for (DocumentAchat documentAchat : modeleCommandeAchatLiee.getListeDocumentAchatSelectionnes()) {
            afficherCommandeAchat(documentAchat.getId());
          }
        }
      }
    }
    
    // Effacer toutes les variables
    effacerVariables();
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourne si l'utilisateur a les droits de saisir des articles hors gammes dans les documents.
   */
  public boolean isArticleHorsGammeAutorise() {
    if (etablissement == null) {
      return false;
    }
    if (documentVenteEnCours.isDevis() || documentVenteEnCours.isCommande()) {
      return true;
    }
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_SAISIE_ARTICLES_HORS_GAMME);
  }
  
  /**
   * Retourne si l'utilisateur a les droits de changer de client (client en compte) sur bon pour un vendeur.
   * Il faut les 2 sécurités (148 + 175) sinon le tiers n'est pas modifié.
   * Il faudrait qu'un Gapeur creuse la question car je ne comprend pas la différence entre les 2 sécurités.
   */
  public boolean isClientEnCompteModifiable() {
    if (etablissement != null && clientCourant != null && clientCourant.isClientEnCompte() && isModePaiementComptant()
        && (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture())) {
      return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
          EnumDroitSecurite.IS_AUTORISE_CHANGEMENT_CLIENT_EN_COMPTE)
          && ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
              EnumDroitSecurite.IS_AUTORISE_CHANGEMENT_DE_TIERS_SUR_BON));
    }
    return false;
  }
  
  /**
   * Retourne si l'utilisateur à les droits direction dans la gescom.
   */
  public boolean isUtilisateurADroitDirection() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AVOIR_DROITS_DIRECTION);
  }
  
  /**
   * Retourne si l'utilisateur à les droits de modification d'une commande.
   * Il faut 2 sécurités (31 + 151) pour que l'utilisateur ait les droits de modification.
   * Voir utilisation en GAP dans VGVM11EX à partir de la ligne 21.14
   */
  public boolean isUtilisateurADroitModificationCommande() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_SUPERVISEUR_GESTION_DES_BONS_ET_FACTURES)
        && ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_A_MODIFIER_BONS));
  }
  
  /**
   * Retourne si l'utilisateur n'est pas autorisé à modifier le représentant d'un client comptant.
   */
  public boolean isRepresentantNonModifiable() {
    return (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_NON_AUTORISE_A_MODIFIER_REPRESENTANT));
  }
  
  /**
   * 
   * Retourner si la gestion par lots est activée.
   * 
   * @return
   */
  public boolean isGestionParLot() {
    // Pas de gestion des lots en direct usine (car pas de gestion en stocks)
    if (documentVenteEnCours.isDirectUsine()) {
      return false;
    }
    // Lecture de la valeur du PS030
    Character ps30 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS030);
    if (ps30 == null || ps30.equals(' ')) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne si la saisie des lots est obligatoire sur les commandes. Cette saisie est obligatoire sur les bons et factures, quelle que
   * soit la valeur de ce paramètre système.
   */
  public boolean isSaisieLotsObligatoire() {
    Character ps86 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS086);
    if (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture()) {
      return true;
    }
    if (documentVenteEnCours.isCommande() && (ps86.equals('1') || ps86.equals('3'))) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la date de validité du devis est obligatoire pour l'utilisateur.
   */
  public boolean isDateValiditeDevisObligatoire() {
    Character ps203 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS203);
    if (ps203.equals('2') || ps203.equals('3')) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la date de relance du devis est obligatoire pour l'utilisateur.
   */
  public boolean isDateRelanceDevisObligatoire() {
    Character ps332 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS332);
    if (ps332.equals('1')) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la gestion des bons de cour est active.
   */
  public boolean isGestionBonCourActive() {
    boolean ps330 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS330);
    return ps330;
  }
  
  /**
   * Retourne si la date de commande est obligatoire pour l'utilisateur.
   */
  public boolean isDateCommmandeObligatoire() {
    Character ps203 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS203);
    if (ps203.equals('1') || ps203.equals('3')) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne le texte affichant les informations client (nom + type).
   */
  public String getTexteInformationsClient() {
    String texte = "";
    if (clientCourant == null || !clientCourant.isExistant() || clientCourant.getAdresse() == null) {
      return texte;
    }
    
    // On formate en fonction du type du client
    if (clientCourant != null && clientCourant.isClientComptant()) {
      if (clientCourant.getAdresse().getIdCivilite() != null) {
        texte = String.format("%s %s %s (%s)", clientCourant.getAdresse().getIdCivilite(), clientCourant.getAdresse().getNom(),
            clientCourant.getAdresse().getComplementNom(), EnumTypeCompteClient.COMPTANT.getLibelle());
      }
      else {
        texte = String.format("%s %s (%s)", clientCourant.getAdresse().getNom(), clientCourant.getAdresse().getComplementNom(),
            EnumTypeCompteClient.COMPTANT.getLibelle());
      }
    }
    else if (clientCourant.isClientEnCompte()) {
      texte = String.format("%s (%s)", clientCourant.getAdresse().getNom(), EnumTypeCompteClient.EN_COMPTE.getLibelle());
    }
    else if (clientCourant.isClientProspect()) {
      texte = String.format("%s (%s)", clientCourant.getAdresse().getNom(), EnumTypeCompteClient.PROSPECT.getLibelle());
    }
    
    // Récupération du nom du magasin s'il y a en à un
    if ((documentVenteEnCours != null) && documentVenteEnCours.getIdMagasin() != null) {
      return texte + " - " + listeMagasin.getMagasinParId(documentVenteEnCours.getIdMagasin()).getNom();
    }
    else {
      return texte;
    }
  }
  
  /**
   * Controle l'autorisation de vente en dessous du prix de revient.
   */
  private boolean isAutorisationVenteSousPRV(LigneVente pLigneVente) {
    // On controle dans le cas où le différé a déjà été accepté et que le montant n'a pas changé
    if (isVenteSousPRV()) {
      return true;
    }
    
    boolean venteSousPRVAccepte = false;
    
    // Affichage de la boite de dialogue
    ModeleAutorisationVenteSousPrv modele = new ModeleAutorisationVenteSousPrv(getSession(), this, pLigneVente, utilisateurGescom);
    VueAutorisationVenteSousPrv vue = new VueAutorisationVenteSousPrv(modele);
    vue.afficher();
    
    if (modele.isSortieAvecValidation()) {
      venteSousPRVAccepte = modele.isAutorisationAcceptee();
    }
    else {
      venteSousPRVAccepte = false;
    }
    
    return venteSousPRVAccepte;
  }
  
  /**
   * Retourne si l'encours est dépassé pour le document en cours et paiement comptant obligatoire.
   */
  public boolean isEncoursEstDepassePaiementComptantObligatoire() {
    if ((encoursDocument != null) && encoursDocument.getAlertesEncoursClient().controlerAlertes()) {
      return encoursDocument.getAlertesEncoursClient().isPlafondDepassePaiementComptantObligatoire();
    }
    return false;
  }
  
  /**
   * Retourne si l'encours est dépassé pour le document en cours.
   */
  public boolean isEncoursEstDepasse() {
    if (encoursDocument != null && encoursDocument.getAlertesEncoursClient().controlerAlertes() && documentVenteEnCours.isModifiable()) {
      return encoursDocument.getAlertesEncoursClient().isPlafondDepasseDemandeAutorisationGeneree()
          || encoursDocument.getAlertesEncoursClient().isPlafondDepassePaiementComptantObligatoire();
    }
    return false;
  }
  
  /**
   * Retourne si une alerte est blocante pour la validation du document.
   * - prix d'un article en dessous du PUMP
   * - prix d'un article en dessous du PRV
   */
  public boolean isErreurBlocanteDocument() {
    if (optionsValidation == null) {
      return false;
    }
    if (optionsValidation.getAlertesDocument().isErreurPrixInferieurPump()
        || optionsValidation.getAlertesDocument().isErreurPrixInferieurPrv()) {
      return true;
    }
    return false;
  }
  
  /**
   * Permet de controler si il existe au moins un regroupement dans le document.
   */
  public boolean isRegroupementExiste() {
    if (documentVenteEnCours == null) {
      return false;
    }
    if (!documentVenteEnCours.isModifiable()) {
      return false;
    }
    return !documentVenteEnCours.getListeRegroupements().isEmpty();
  }
  
  /**
   * Renvoit le mode de délivrance par défaut pour les documents de vente.
   * Ce mode est fixé par la PS331
   */
  public EnumModeDelivrement getModeDelivrementParDefaut() {
    Character ps331 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS331);
    return EnumModeDelivrement.valueOfByCode(ps331);
  }
  
  /**
   * Pour un document de vente Direct usine, retourne si on est en mode "MultiFournisseur" ou non
   * Le mode multi fournisseur s'il est activé autorise soit le choix d'un fournisseur différent du
   * fournisseur principal de l'article soit la possibilité de passer autant de commandes d'achats
   * que de fournisseurs principaux associés aux articles
   * La PS 312 doit contenir '1' pour être multi-fournisseur.
   * Si sa valeur est ' ' les articles du document de vente appartiendront obligatoirement
   * au même fournisseur principal et la commande d'achat sera générée automatiquement auprès de ce fournisseur
   */
  public boolean isDirectUsineEnModeMultiFournisseur() {
    boolean ps312 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS312);
    if (documentVenteEnCours.isDirectUsine() && ps312) {
      return true;
    }
    return false;
  }
  
  /**
   * La PS 300 fixe le comportement du logiciel vis à vis du dépassement d'encours.
   * Si sa valeur est '1' on lance une alerte sans bloquer la saisie de ligne et on ne peut aller plus loin qu'avec autorisation ou
   * paiement comptant
   * Si sa valeur est '2' on bloque la saisie de ligne et on ne peut aller plus loin qu'avec autorisation ou paiement comptant
   */
  public boolean isPaiementComptantPourEncoursDepasse() {
    Character ps300 =
        ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS300);
    if (ps300.equals('1') || ps300.equals('2')) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si la commande d'achat est affichée après l'édition du document de vente.
   * La PS,327 doit contenir '1' pour que la commande d'achat s'affiche.
   * Autre possibilité : une commande standard qui contient au moins un article spécial
   */
  public boolean isAfficherCommandeAchat() {
    // Pour afficher la commande d'achat, les conditions sont :
    
    // Le document doit être en création
    if (!documentVenteEnCours.isEnCoursCreation()) {
      return false;
    }
    
    // Le document doit demander une génération immédiate de la commande d'achat
    if (!documentVenteEnCours.isGenerationImmediateCommandeAchat()) {
      return false;
    }
    
    // La PS327 doit être active
    boolean ps327 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS327);
    if (!ps327) {
      return false;
    }
    
    // Soit le document est un direct usine
    if (documentVenteEnCours.isDirectUsine()) {
      return true;
    }
    // Soit le document contient des articles spéciaux
    if (isGBAarticleSpeciaux()) {
      return true;
    }
    
    // Dans les autres cas, la commande d'achat n'est pas affichée
    return false;
  }
  
  /**
   * Retourner si un règlement différé est possible.
   */
  public boolean isDiffererReglementsEstPossible() {
    if (clientCourant == null) {
      throw new MessageErreurException("Le client est invalide.");
    }
    
    // Vérification si l'utilisateur a les droits d'effectuer un différé
    if (ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_NON_AUTORISE_A_EFFECTUER_DIFFERE)) {
      return false;
    }
    
    // Réglement différé interdit s'il s'agit d'un remboursement
    if (calculReglement == null || calculReglement.isRemboursement()) {
      return false;
    }
    
    // Réglement différé interdit s'il ne reste rien a réglé
    if (calculReglement == null || calculReglement.isEntierementRegle()) {
      return false;
    }
    
    // Le document est une facture qui a été déjà différée (car il s'agit d'une modification de facture)
    if (!documentVenteEnCours.isModifiable() && documentVenteEnCours.isFacture()) {
      // Si la facture a déjà été différé par le passé pas besoin de repasser par une demande d'autorisation de différer
      if (isDiffererPaiement()) {
        return false;
      }
      return true;
    }
    
    // Le document doit être modifiable pour un règlement différé
    if (!documentVenteEnCours.isModifiable()) {
      return false;
    }
    
    // Réglement différé autorisé si le document est une facture ou un bon
    if (!documentVenteEnCours.isFacture() && !documentVenteEnCours.isBon()) {
      return false;
    }
    
    // Réglement différé possible si est n'est pas déjà en paiement comptant
    if (!isModePaiementComptant()) {
      return false;
    }
    
    // Réglement différé interdit si le règlement comptant est obligatoire
    if (clientCourant.isReglementComptantObligatoire()) {
      return false;
    }
    
    // Réglement différé interdit si l'encours de paiement comptant est atteint
    if (isEncoursEstDepassePaiementComptantObligatoire()) {
      return false;
    }
    
    // Réglement différé interdit si le paiement comptant est imposé pour l'encours
    if (forcerReglementComptantACauseEncours) {
      return false;
    }
    
    // Réglement différé interdit si le client a éét changé au règlement
    if (changementClientAuReglement) {
      return false;
    }
    
    // Réglement différé interdit si une autre facture a été sélectionnée pour le règlement
    if (listeFactureNonReglee != null && !listeFactureNonReglee.isSelectionVide()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Retourner si différer un avoir est possible.
   */
  public boolean isDiffererAvoirEstPossible() {
    if (clientCourant == null) {
      throw new MessageErreurException("Le client est invalide.");
    }
    
    // Avoir différé possible s'il s'agit d'un remboursement
    if (calculReglement == null || !calculReglement.isRemboursement()) {
      return false;
    }
    
    // Avoir différé possible le document n'est pas entièrement reglé
    if (calculReglement == null || calculReglement.isEntierementRegle()) {
      return false;
    }
    
    // Avoir différé possible si le document n'est pas modifiable
    if (!documentVenteEnCours.isModifiable()) {
      // S'il ne s'agit pas d'une facture ou d'un bon et que le document est entièrement réglé
      if (!documentVenteEnCours.isFacture() && !documentVenteEnCours.isBon()
          && (calculReglement == null || calculReglement.isEntierementRegle())) {
        return false;
      }
    }
    
    // Avoir différé possible si le document est une facture ou un bon
    if (!documentVenteEnCours.isFacture() && !documentVenteEnCours.isBon()) {
      return false;
    }
    
    // Avoir différé possible si ce n'est pas déjà en paiement comptant
    if (!isModePaiementComptant()) {
      return false;
    }
    
    // Avoir différé interdit si le client a été changé au règlement
    if (changementClientAuReglement) {
      return false;
    }
    
    return true;
  }
  
  public boolean isDiffererAvoir() {
    if (calculReglement == null) {
      return false;
    }
    return calculReglement.isDiffererAvoir();
  }
  
  /**
   * Retourne si les consditions sont remplis pour valider l'onglet articles.
   */
  public boolean isValidationOngletArticlesPossible() {
    // Pour ne pas bloquer les clients comptants
    return !documentVenteEnCours.getListeLigneVente().isEmpty() && (!isEncoursEstDepasse() || isModePaiementComptant());
  }
  
  /**
   * Controle si un document est en cours de création ou modification.
   */
  public boolean isDocumentEnCoursDeSaisie() {
    return documentVenteEnCours != null;
  }
  
  /**
   * Construit le titre en fonction de l'avancement dans la saisie.
   */
  public String getTitre() {
    String titre = TITRE;
    String typedoc = "";
    
    if (documentVenteEnCours == null) {
      return titre;
    }
    
    switch (documentVenteEnCours.getTypeDocumentVente()) {
      case NON_DEFINI:
        typedoc = "";
        break;
      
      case DEVIS:
        if (!documentVenteEnCours.isDirectUsine()) {
          if (documentVenteEnCours.isEnlevement()) {
            typedoc = TYPEDOC_DEVIS_ENLEVEMENT;
          }
          else if (documentVenteEnCours.isLivraison()) {
            typedoc = TYPEDOC_DEVIS_LIVRAISON;
          }
        } // Document direct usine
        else {
          typedoc = TYPEDOC_DEVIS_DIRECT_USINE;
        }
        break;
      
      case FACTURE:
        if (!documentVenteEnCours.isDirectUsine()) {
          if (documentVenteEnCours.isEnlevement()) {
            typedoc = TYPEDOC_FACTURE_ENLEVEMENT;
          }
          else if (documentVenteEnCours.isLivraison()) {
            typedoc = TYPEDOC_FACTURE_LIVRAISON;
          }
        } // Document direct usine
        else {
          typedoc = TYPEDOC_FACTURE_DIRECT_USINE;
        }
        if (documentVenteEnCours.getSigne() < 0) {
          typedoc = TYPEDOC_AVOIR;
        }
        break;
      
      case BON:
        if (!documentVenteEnCours.isDirectUsine()) {
          if (documentVenteEnCours.isEnlevement()) {
            typedoc = TYPEDOC_BON_ENLEVEMENT;
          }
          else if (documentVenteEnCours.isLivraison()) {
            typedoc = TYPEDOC_BON_LIVRAISON;
          }
        } // Document direct usine
        else {
          typedoc = TYPEDOC_BON_DIRECT_USINE;
        }
        if (documentVenteEnCours.getSigne() < 0) {
          typedoc = TYPEDOC_AVOIR;
        }
        break;
      
      case COMMANDE:
        if (!documentVenteEnCours.isDirectUsine()) {
          if (documentVenteEnCours.isEnlevement()) {
            typedoc = TYPEDOC_COMMANDE_ENLEVEMENT;
          }
          else if (documentVenteEnCours.isLivraison()) {
            typedoc = TYPEDOC_COMMANDE_LIVRAISON;
          }
        } // Document direct usine
        else {
          typedoc = TYPEDOC_COMMANDE_DIRECT_USINE;
        }
        break;
      
      default:
        throw new MessageErreurException(
            "Impossible de construire le titre d'un document de ventes de type " + documentVenteEnCours.getTypeDocumentVente());
    }
    
    // On controle si le document a déjà un numéro
    if (!documentVenteEnCours.isExistant()) {
      if (typedoc.length() > 0) {
        titre = String.format("%s - %s", TITRE, typedoc);
      }
    }
    else {
      titre = String.format("%s - %s %s", TITRE, typedoc, documentVenteEnCours.getId().toString());
    }
    return titre;
  }
  
  // -- Méthodes privée
  
  /**
   * Retourne l'id de la zone géographique du document en cours.
   */
  public IdZoneGeographique retournerIdZoneGeographique() {
    if (documentVenteEnCours.getTransport() != null) {
      return documentVenteEnCours.getTransport().getIdZoneGeographique();
    }
    
    return null;
  }
  
  /**
   * Retourne le taux de TVA voulu pour l'établissement
   */
  public BigDecimal getTauxTVA(int pCodeTVA) {
    if (etablissement == null) {
      return BigDecimal.ZERO;
    }
    return etablissement.getTauxTVA(pCodeTVA);
  }
  
  /**
   * Retourne si la saisie du chantier est obligatoire pour le client courant.
   */
  public boolean isChantierEstObligatoire() {
    CategorieClient categorieClient = listeCategorieClient.get(clientCourant.getIdCategorieClient());
    return (clientCourant.isChantierEstObligatoire() || (categorieClient != null && categorieClient.isChantierObligatoire()))
        && isDocumentEnCoursDeSaisie();
  }
  
  /**
   * Retourne si le "pris par" est obligatoire pour le client et le document courant.
   */
  public boolean isPrisParObligatoire() {
    return clientCourant.isPrisParEstObligatoire() && documentVenteEnCours.getPrisPar().trim().isEmpty()
        && !documentVenteEnCours.isDevis() && documentVenteEnCours.isEnlevement() && isDocumentEnCoursDeSaisie();
  }
  
  /**
   * Retourne si le véhicule est obligatoire pour le client courant.
   */
  public boolean isVehiculeObligatoire() {
    return clientCourant.isVehiculeEstObligatoire() && documentVenteEnCours.getImmatriculationVehiculeClient().trim().isEmpty()
        && !documentVenteEnCours.isDevis() && documentVenteEnCours.isEnlevement() && isDocumentEnCoursDeSaisie();
  }
  
  /**
   * Retourne la précision d'une unité
   */
  public int retournerPrecisionUnite(IdUnite pIdUnite) {
    if (pIdUnite == null) {
      return 0;
    }
    
    return listeUnite.getPrecisionUnite(pIdUnite);
  }
  
  /**
   * Retourne la partie décimale d'un flottant.
   */
  private Double retournerPartieDecimale(Double valeur) {
    return valeur % 1;
  }
  
  /**
   * Retourne la partie décimale d'un BigDecimal.
   */
  private BigDecimal retournerPartieDecimale(BigDecimal valeur) {
    return valeur.remainder(BigDecimal.ONE);
  }
  
  /**
   * Affiche l'écran d'affichage des documents stocké
   */
  public void afficherDocumentStocke() {
    CritereDocumentStocke critere = new CritereDocumentStocke();
    critere.setIdEtablissement(getEtablissement().getId());
    critere.setIdDocumentVente(documentVenteEnCours.getId());
    ModeleListeDocumentStocke modele = new ModeleListeDocumentStocke(getSession(), critere);
    VueDocumentStocke vue = new VueDocumentStocke(modele);
    vue.afficher();
  }
  
  // -- Accesseurs et équivalents
  
  /**
   * Indiquer s'il le client en cours est facturé en TTC.
   */
  public boolean isClientTTC() {
    return clientCourant.isFactureEnTTC();
  }
  
  /**
   * Indiquer s'il est possible de créer un nouveau client comptant.
   * Ce n'est possible que si le client courant n'existe pas en base.
   */
  public boolean isCreationClientComptant() {
    return (clientCourant != null && !clientCourant.isExistant());
  }
  
  /**
   * Retourne si le règlement pas chèque pour le client en cours est interdit.
   */
  public boolean isChequeInterdit() {
    if ((clientCourant == null) || (clientCourant.isReglementChequeEstInterdit() == null)) {
      return true;
    }
    return clientCourant.isReglementChequeEstInterdit();
  }
  
  /**
   * Détermine si on affiche l'option de génération d'une commande d'achat.
   */
  public boolean isAfficherOptionGenerationCommandeAchat() {
    boolean retour = false;
    // Affichage de l'option génération commande d'achat s'il s'agit d'une commande et s'il s'agit d'un direct usine
    retour = documentVenteEnCours.isCommande() && documentVenteEnCours.isDirectUsine();
    // Dans le cas où il s'agit d'une commande mais pas d'un direct usine
    if (!retour && documentVenteEnCours.isCommande()) {
      for (LigneVente ligneVente : documentVenteEnCours.getListeLigneVente()) {
        // Si la commande contient au moins un article spécial: l'option génération de commande d'achat est affichée
        retour = etreArticleSpecial(ligneVente);
        if (retour) {
          return true;
        }
      }
    }
    return retour;
  }
  
  /**
   * Détermine si le document est une commande qui contient un article spécial
   */
  public boolean isGBAarticleSpeciaux() {
    boolean retour = false;
    // Dans le cas où il s'agit d'une commande mais pas d'un direct usine
    if (documentVenteEnCours.isCommande() && !documentVenteEnCours.isDirectUsine()) {
      for (LigneVente ligneVente : documentVenteEnCours.getListeLigneVente()) {
        // Si la commande contient au moins un article spécial: l'option génération de commande d'achat est affichée
        retour = etreArticleSpecial(ligneVente);
        if (retour) {
          return true;
        }
      }
    }
    return retour;
  }
  
  /**
   * Détermine si on affiche l'option direct usine
   * Valable surtout dans le cas des retours en arrière (via les onglets).
   */
  public boolean isAfficherOptionDirectUsine() {
    if (documentVenteEnCours != null && !documentVenteEnCours.isDirectUsine()
        && (documentVenteEnCours.isBon() || documentVenteEnCours.isFacture())) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Détermine si on active l'option direct usine d'un document de vente
   */
  public boolean isDirectUsineActif() {
    if (!isAfficherOptionDirectUsine()) {
      return false;
    }
    
    if (documentVenteEnCours == null || documentVenteEnCours.getListeLigneVente() == null || (proDevisOrigine == null
        && documentVenteEnCours.isEnCoursCreation() && (!documentVenteEnCours.isBon() && !documentVenteEnCours.isFacture()))) {
      return true;
    }
    // Dans les autres cas le type direct usine ne peut plus être changé
    return false;
  }
  
  /**
   * Détermine si on doit afficher les informatons du chantier.
   */
  public boolean isAfficherChantier() {
    if (clientCourant != null && clientCourant.isClientComptant()) {
      return false;
    }
    if (documentVenteEnCours == null) {
      return true;
    }
    return clientCourant.isChantierEstObligatoire() || documentVenteEnCours.isDevis() || documentVenteEnCours.isCommande()
        || documentVenteEnCours.isBon();
  }
  
  /**
   * Détermine si le chantier est modifiable
   */
  public boolean isChantierModifiable() {
    if (documentVenteEnCours.isModifiable() && isAfficherChantier() && (chantier == null || documentVenteEnCours.isEnCoursCreation())) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si le client est autorisé à selectionner ses documents clients.
   */
  public boolean isClientAutoriseSelectionDocumentsClient() {
    if ((clientCourant != null) && clientCourant.isClientInterdit()) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne l'indice de l'onglet courant.
   */
  public int getIndiceOnglet() {
    return indiceOngletCourant;
  }
  
  public boolean isOngletClientAccessible() {
    return true;
  }
  
  // Retourne un libellé contenant la période en cours pour les documents de vente entre parenthèses
  public String getPeriodeEnCours() {
    String dateDebutPeriode = Constantes.convertirDateEnTexte((Constantes.getDateDebutMoisPrecedent(new Date())));
    String dateFinPeriode = Constantes.convertirDateEnTexte(new Date());
    return "(du " + dateDebutPeriode + " au " + dateFinPeriode + ")";
  }
  
  public boolean isOngletLivraisonAccessible() {
    return indiceOngletAccessible >= ONGLET_LIVRAISONENLEVEMENT;
  }
  
  public boolean isOngletArticleAccessible() {
    return indiceOngletAccessible >= ONGLET_ARTICLE;
  }
  
  public boolean isOngletReglementAccessible() {
    if ((documentVenteEnCours == null) || documentVenteEnCours.isDevis()) {
      return false;
    }
    return indiceOngletAccessible >= ONGLET_REGLEMENT;
  }
  
  public boolean isOngletEditionAccessible() {
    return indiceOngletAccessible >= ONGLET_EDITION;
  }
  
  public int getEtape() {
    return etape;
  }
  
  /**
   * 
   * Indice de l'onglet actif pour les adresses de livraison ou d'enlèvement.
   * 
   * @return Indice de l'onglet courant (adresse de facturation ou adresse de livraison).
   */
  public int getIndiceOngletAdresse() {
    return indiceOngletAdresse;
  }
  
  public Client getClientCourant() {
    return clientCourant;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public IdRepresentant getIdRepresentant() {
    return idRepresentant;
  }
  
  public String getMessageArticle() {
    if (!messageArticle.isEmpty()) {
      return "<html>" + messageArticle + "</html>";
    }
    return messageArticle;
  }
  
  public String getMessageClient() {
    if (!messageClient.isEmpty()) {
      return "<html>" + messageClient + "</html>";
    }
    return messageClient;
  }
  
  /**
   * Taux de remise forcée.
   * 
   * Le taux de remise forcé est utilisé pour calculer les prix de ventes du résultat de la recherche article. Cela revient à
   * saisir un taux de remise dans la ligne de vente.
   * 
   * @return Taux de remise.
   */
  public BigPercentage getTauxRemiseForcee() {
    return tauxRemiseForcee;
  }
  
  /**
   * Retourner le numéro de la colonne tarif forcée.
   * 
   * Le numéro de colonne tarif forcé est utilisé pour calculer les prix de ventes du résultat de la recherche article.
   * Le numéro de colonne tarif forcé prend le pas sur les colonnes tarifs issues d'ailleurs (CNV, client, ...). Cela revient à
   * saisir une colonne tarif dans le document de vente. On ne peut pas saisir une colonne tarif inférieure à la colonne
   * tarif du client
   * 
   * @return Numéro de colonne tarif.
   */
  public Integer getNumeroColonneTarifForcee() {
    return numeroColonneTarifForcee;
  }
  
  public ListeResultatRechercheArticle getListeResultatRechercheArticle() {
    return listeResultatRechercheArticle;
  }
  
  public ListeResultatRechercheArticlePalette getListeResultatRechercheArticlePalette() {
    return listeResultatRechercheArticlePalette;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeCivilite getListeCivilite() {
    return listeCivilite;
  }
  
  public ListeTransporteur getListeTransporteur() {
    return listeTransporteur;
  }
  
  public ListeVendeur getListeVendeur() {
    return listeVendeur;
  }
  
  public ListeFamille getListeFamille() {
    return listeFamille;
  }
  
  public ListeGeneriquesString getListeLivraisonPartielle() {
    return listeLivraisonPartielle;
  }
  
  public char getLivraisonPartielle() {
    // Si le top n'est pas encore fixé sur le document on lit le top prédéfinit sur la fiche client
    if (documentVenteEnCours.getBlocageExpedition() == null) {
      if (clientCourant != null && !clientCourant.getTopLivraisonPartielle().trim().isEmpty()) {
        return EnumComboLivraisonPartielle.NON_AUTORISE.getCode();
      }
      return EnumComboLivraisonPartielle.AUTORISE.getCode();
    }
    return documentVenteEnCours.getBlocageExpedition();
  }
  
  public UtilisateurGescom getUtilisateurGescom() {
    return utilisateurGescom;
  }
  
  public boolean isModePaiementComptant() {
    return modePaiementComptant;
  }
  
  public String getTexteRecherche() {
    return texteRecherche;
  }
  
  public DocumentVente getDocumentVenteEnCours() {
    return documentVenteEnCours;
  }
  
  /**
   * Document de ventes sélectionné dans le tableau de la liste des document de ventes du client.
   */
  public DocumentVenteBase getDocumentVenteBaseSelectionne() {
    if (modeleDocumentVenteParType != null) {
      return modeleDocumentVenteParType.getDocumentVenteBaseSelectionne();
    }
    return null;
  }
  
  public CalculReglement getCalculReglement() {
    return calculReglement;
  }
  
  public boolean isForcerReglementComptantACauseEncours() {
    return forcerReglementComptantACauseEncours;
  }
  
  public void setForcerReglementComptantACauseEncours(boolean forcerReglementComptantACauseEncours) {
    this.forcerReglementComptantACauseEncours = forcerReglementComptantACauseEncours;
  }
  
  public EncoursClient getEncoursClient() {
    return encoursClient;
  }
  
  public EncoursClient getEncoursDocument() {
    return encoursDocument;
  }
  
  public ListeCommune getListeCommune() {
    if (listeCommune == null && clientCourant != null && clientCourant.getAdresse() != null
        && clientCourant.getAdresse().getCodePostal() != 0) {
      
      CritereCommune critereCommune = new CritereCommune();
      critereCommune.setCodePostal(Integer.parseInt(clientCourant.getAdresse().getCodePostalFormate()));
      listeCommune = ListeCommune.charger(getIdSession(), critereCommune);
      
      clientCourant.controlerAdresse(getIdSession(), listeErreurAdresse, false);
      traiterErreurAdresse(false, true);
      
    }
    return listeCommune;
  }
  
  public ListeCommune getListeCommuneLivraison() {
    if (listeCommuneLivraison == null && documentVenteEnCours != null && documentVenteEnCours.getTransport() != null
        && documentVenteEnCours.getTransport().getAdresseLivraison() != null
        && documentVenteEnCours.getTransport().getAdresseLivraison().getCodePostal() != 0) {
      CritereCommune critereCommune = new CritereCommune();
      critereCommune.setCodePostal(Integer.parseInt(documentVenteEnCours.getTransport().getAdresseLivraison().getCodePostalFormate()));
      listeCommuneLivraison = ListeCommune.charger(getIdSession(), critereCommune);
      
    }
    return listeCommuneLivraison;
  }
  
  public void setComposantAyantLeFocus(int composantAyantLeFocus) {
    this.composantAyantLeFocus = composantAyantLeFocus;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public String getDernierTexteRecherche() {
    return dernierTexteRecherche;
  }
  
  public boolean isActivationOngletDocumentsClient() {
    return activationOngletDocumentsClient;
  }
  
  public void setActivationOngletDocumentsClient(boolean activationOngletDocumentsClient) {
    this.activationOngletDocumentsClient = activationOngletDocumentsClient;
  }
  
  /**
   * Index de l'article sélectionné.
   */
  public int getIndexArticleSelectionne() {
    return indexArticleSelectionne;
  }
  
  /**
   * Index de la ligne de vente sélectionnée.
   */
  public int getIndexLigneVenteSelectionnee() {
    return indexLigneVenteSelectionnee;
  }
  
  public ListeZoneGeographique getListeZoneGeographique() {
    return listeZoneGeographique;
  }
  
  public ArrayList<IdLigneVente> getListeLigneVenteASelectionner() {
    return listeLigneVenteASelectionner;
  }
  
  public BigDecimal getNombreDePalettesLies() {
    return nombreDePalettesLies;
  }
  
  public BigDecimal getNombreDePalettesLies(Integer pNombreDecimales) {
    if (nombreDePalettesLies != null && pNombreDecimales != null) {
      return nombreDePalettesLies = nombreDePalettesLies.setScale(pNombreDecimales, RoundingMode.HALF_UP);
    }
    return nombreDePalettesLies;
  }
  
  public void setNombreDePalettesLies(BigDecimal pNombreDePalettesLies) {
    nombreDePalettesLies = pNombreDePalettesLies;
  }
  
  public ListeContact getListeContactClient() {
    return listeContactClient;
  }
  
  public boolean isVenteSousPRV() {
    return venteSousPRV;
  }
  
  public void setVenteSousPRV(boolean venteSousPRV) {
    this.venteSousPRV = venteSousPRV;
  }
  
  public int getComposantAyantLeFocus() {
    return composantAyantLeFocus;
  }
  
  public EnumFiltreHorsGamme getFiltreHorsGamme() {
    return filtreHorsGamme;
  }
  
  public ModeleDocumentVenteParType getModeleDocumentVenteParType() {
    return modeleDocumentVenteParType;
  }
  
  /**
   * Liste des factures non réglées, affichées dans l'onglet réglement.
   */
  public ListeDocumentVenteBase getListeFactureNonReglee() {
    return listeFactureNonReglee;
  }
  
  /**
   * Identifiant du vendeur associé au document.
   * Si aucun n'est chargé, on présente l'identifiant vendeur de l'utilisateur.
   */
  public IdVendeur getIdVendeur() {
    if (documentVenteEnCours != null) {
      return documentVenteEnCours.getIdVendeur();
    }
    else if (utilisateurGescom != null) {
      return utilisateurGescom.getIdVendeur();
    }
    else {
      return null;
    }
  }
  
  public ListeTypeFacturation getListeTypeFacturation() {
    return listeTypeFacturation;
  }
  
  public Map<IdEtablissement, ListeMagasin> getListeMagasinParEtablissement() {
    return listeMagasinParEtablissement;
  }
  
  public boolean isSaisieArticlePossible() {
    return saisieArticlePossible;
  }
  
  public Chantier getChantier() {
    return chantier;
  }
  
  public ListeModeEdition getListeModeEditionVente() {
    return listeModeEditionVente;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  /**
   * Retourner la date de traitement en cours.
   */
  public Date getDateTraitement() {
    return dateTraitement;
  }
  
  /**
   * Email du destinataire du mail avec le document en pièce jointe.
   */
  public String getEmailDestinataireEdition() {
    if (emailDestinataireEdition == null) {
      return "";
    }
    return emailDestinataireEdition;
  }
  
  /**
   * Numéro de fax du destinataire.
   */
  public String getFaxDestinataireEdition() {
    if (faxDestinataireEdition == null) {
      return "";
    }
    return faxDestinataireEdition;
  }
  
  public OptionsEditionVente getOptionsEditionVente() {
    return optionsEdition;
  }
  
  /**
   * Liste des modes d'expédition.
   */
  public ListeModeExpedition getListeModeExpedition() {
    return listeModeExpedition;
  }
  
  public Contact getContactMail() {
    return contactMail;
  }
  
  public Contact getContactFax() {
    return contactFax;
  }
  
  public InformationsExtraction getInformationsExtraction() {
    return informationsExtraction;
  }
  
  public List<EnumErreurAdresse> getListeErreurAdresse() {
    return listeErreurAdresse;
  }
  
  public boolean isDiffererPaiement() {
    if (calculReglement != null && calculReglement.isDiffererPaiement()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourner le type d'édition d'une facture ou d'un avoir.
   * @return EnumTypeEditionFacture
   */
  public EnumTypeEditionFacture getOptionsEditionFacture() {
    return optionsEdition.getTypeEditionFacture();
  }
  
  /**
   * Retourner la liste des documents d'achats liés au document de vente en cours.
   * @return
   */
  public ListeDocumentAchat getListeDocumentAchatLie() {
    return listeDocumentAchatLie;
  }
  
  /**
   * Retourner le mode de calcul de prix en cours (Négoce ou Classique)
   * @return
   */
  public boolean isModeNegoce() {
    return modeNegoce;
  }
  
  /**
   * 
   * Retourner l'identifiant du document d'origine (pour les avoirs par exemple).
   * 
   * @return IdDocumentVente
   */
  public IdDocumentVente getIdDocumentOrigineAvoir() {
    return idDocumentOrigineAvoir;
  }
  
}
