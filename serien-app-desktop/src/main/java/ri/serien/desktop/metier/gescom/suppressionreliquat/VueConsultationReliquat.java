/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.suppressionreliquat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.List;

import javax.swing.JScrollPane;

import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran affiche les lignes du document sélectionné.
 */
public class VueConsultationReliquat extends AbstractVuePanel<ModeleReliquat> {
  // Constantes
  private static final String BOUTON_SOLDER_RELIQUAT = "Solder le reliquat";
  private final static String[] TITRE_LISTE_LIGNE = new String[] { "Quantit\u00e9", "UC", "Disponible vente", "Code article",
      "Libell\u00e9", "Quantit\u00e9 initiale", "Quantit\u00e9 trait\u00e9e" };
  
  // Variables
  private JTableLignesArticleRenderer ligneArticleCelluleRenderer = new JTableLignesArticleRenderer();
  
  /**
   * Constructeur.
   */
  public VueConsultationReliquat(ModeleReliquat pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Initialise l'aspect de la liste des lignes
    scpListeLignes.getViewport().setBackground(Color.WHITE);
    tblListeLignes
        .personnaliserAspect(
            TITRE_LISTE_LIGNE, new int[] { 70, 40, 120, 110, 360, 120, 190 }, new int[] { 70, 40, 150, 200, 1000, 200, 250 }, new int[] {
                NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE },
            14);
    tblListeLignes.personnaliserAspectCellules(ligneArticleCelluleRenderer);
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BOUTON_SOLDER_RELIQUAT, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.ALLER_ECRAN_PRECEDENT, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    requestFocus();
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirListe();
  }
  
  // -- Méthodes privées
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListe() {
    final List<LigneVente> listeLigneVenteNouveau = getModele().getListeLigneVenteNouveau();
    if (listeLigneVenteNouveau == null) {
      return;
    }
    
    // Préparer les données pour le tableau
    final String[][] donnees = new String[listeLigneVenteNouveau.size()][tblListeLignes.getColumnCount()];
    for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
      LigneVente ligneVente = listeLigneVenteNouveau.get(ligne);
      // Les données
      donnees[ligne][0] = getModele().retournerQuantiteRestanteAAfficher(ligne);
      if (ligneVente.isLigneCommentaire()) {
        if (ligneVente.isDebutRegroupement() || ligneVente.isFinRegroupement()) {
          donnees[ligne][0] = "";
          donnees[ligne][1] = "";
          donnees[ligne][2] = "";
          donnees[ligne][3] = "";
        }
        else {
          donnees[ligne][1] = "*";
          donnees[ligne][2] = "";
          donnees[ligne][3] = "Commentaire";
        }
      }
      else {
        if (ligneVente.getIdUniteConditionnementVente() != null) {
          donnees[ligne][1] = ligneVente.getIdUniteConditionnementVente().getCode();
        }
        donnees[ligne][2] = getModele().retournerQuantiteDisponible(ligne);
        donnees[ligne][3] = ligneVente.getIdArticle().getCodeArticle();
      }
      
      // S'il s'agit du début ou de la fin d'un regroupement
      if (ligneVente.isDebutRegroupement() || ligneVente.isFinRegroupement()) {
        if (ligneVente.getLibelle() != null) {
          donnees[ligne][4] = "<html><i>" + ligneVente.getLibelle() + "</i></html>";
        }
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
      }
      // Les autres types de lignes
      else {
        donnees[ligne][4] = "<html>" + ligneVente.getLibelle() + "</html>";
        donnees[ligne][5] = getModele().retournerQuantiteOrigine(ligneVente);
        donnees[ligne][6] = getModele().retournerQuantiteDejaExtraite(ligneVente);
      }
    }
    ligneArticleCelluleRenderer.setListeLigneVente(listeLigneVenteNouveau);
    tblListeLignes.mettreAJourDonnees(donnees);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ALLER_ECRAN_PRECEDENT)) {
        getModele().afficherRechercheDocument();
      }
      else if (pSNBouton.isBouton(BOUTON_SOLDER_RELIQUAT)) {
        getModele().solderReliquat();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    lbListeLignes = new SNLabelTitre();
    scpListeLignes = new JScrollPane();
    tblListeLignes = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ---- lbListeLignes ----
      lbListeLignes.setText("Liste des lignes");
      lbListeLignes.setName("lbListeLignes");
      pnlContenu.add(lbListeLignes,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeLignes ========
      {
        scpListeLignes.setName("scpListeLignes");
        
        // ---- tblListeLignes ----
        tblListeLignes.setRowSelectionAllowed(false);
        tblListeLignes.setName("tblListeLignes");
        scpListeLignes.setViewportView(tblListeLignes);
      }
      pnlContenu.add(scpListeLignes,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbListeLignes;
  private JScrollPane scpListeLignes;
  private NRiTable tblListeLignes;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
