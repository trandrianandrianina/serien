/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet historique de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletHistorique extends SNPanel implements InterfaceVueOnglet {
  private static final String[] TITRE_LISTE_HISTORIQUE = new String[] { "Type", "Date", "Num\u00e9ro bon", "Etat",
      "R\u00e9cup\u00e9ration", "Quantit\u00e9", "Prix HT", "Vendeur", "Date facture", "Facture" };
  private static final String[] TYPE_DOCUMENT_HISTORIQUE = { "", "DEV", "EXP", "CMD", "FAC" };
  
  private ModeleDetailLigneArticle modele = null;
  
  /**
   * Constructeur.
   */
  public VueOngletHistorique(ModeleDetailLigneArticle acomptoir) {
    modele = acomptoir;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    scpHistoriqueVentesClientArticle.getViewport().setBackground(Color.WHITE);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirTypeDocument();
    rafraichirListe();
  }
  
  private void rafraichirTypeDocument() {
    cbTypeDocumentHistorique.setSelectedIndex(modele.getIndexFiltreTypeDocuments());
  }
  
  private void rafraichirListe() {
    // Sélectionner les lignes à afficher
    ArrayList<DocumentHistoriqueVenteClientArticle> listeDocumentsAffiches = new ArrayList<DocumentHistoriqueVenteClientArticle>();
    List<DocumentHistoriqueVenteClientArticle> historique = modele.getListeHistorique();
    if (historique != null) {
      for (int i = 0; i < historique.size(); i++) {
        DocumentHistoriqueVenteClientArticle ligne = historique.get(i);
        if (ligne == null) {
          continue;
        }
        
        if ((ligne.getTypeDocument().trim().equalsIgnoreCase(TYPE_DOCUMENT_HISTORIQUE[modele.getIndexFiltreTypeDocuments()].trim()))
            || (modele.getIndexFiltreTypeDocuments() == 0)) {
          listeDocumentsAffiches.add(ligne);
        }
      }
    }
    
    // Générer les données du tableau
    String[][] donnees = new String[listeDocumentsAffiches.size()][TITRE_LISTE_HISTORIQUE.length];
    int i = 0;
    for (DocumentHistoriqueVenteClientArticle documentAffiche : listeDocumentsAffiches) {
      donnees[i][0] = documentAffiche.getlibelleTypeDocument();
      donnees[i][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentAffiche.getDateCreation());
      donnees[i][2] = Constantes.convertirIntegerEnTexte(documentAffiche.getNumeroBon(), 0);
      donnees[i][3] = documentAffiche.retournerStatus();
      if (documentAffiche.getModeRecuperation().equals(DocumentHistoriqueVenteClientArticle.MODE_RECUPERATION_ENLEVEMENT)) {
        donnees[i][4] = "Enlèvement";
      }
      else {
        donnees[i][4] = "Livraison";
      }
      donnees[i][5] = Constantes.formater(documentAffiche.getQuantite(), false);
      donnees[i][6] = Constantes.formater(documentAffiche.getPrixCalcule(), true);
      donnees[i][7] = documentAffiche.getCodeVendeur();
      if (documentAffiche.getDateFacture() != null) {
        donnees[i][8] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentAffiche.getDateFacture());
      }
      if (documentAffiche.getNumeroFacture() > 0) {
        donnees[i][9] = documentAffiche.getNumeroFacture() + "";
      }
      i++;
    }
    
    DefaultTableModel tableModel = new DefaultTableModel(donnees, TITRE_LISTE_HISTORIQUE) {
      Class<?>[] columnTypes = new Class<?>[] { String.class, String.class, String.class, String.class, String.class, String.class,
          String.class, String.class, String.class, String.class };
      
      @Override
      public Class<?> getColumnClass(int columnIndex) {
        return columnTypes[columnIndex];
      }
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
      
    };
    
    tblHistoriqueVentesClientArticle.setModel(tableModel);
    // On affine la taille des colonnes
    TableColumnModel cm = tblHistoriqueVentesClientArticle.getColumnModel();
    cm.getColumn(0).setPreferredWidth(90);
    cm.getColumn(1).setPreferredWidth(80);
    cm.getColumn(2).setPreferredWidth(80);
    cm.getColumn(3).setPreferredWidth(100);
    cm.getColumn(4).setPreferredWidth(90);
    cm.getColumn(5).setPreferredWidth(110);
    cm.getColumn(6).setPreferredWidth(120);
    cm.getColumn(7).setPreferredWidth(55);
    cm.getColumn(8).setPreferredWidth(80);
    cm.getColumn(9).setPreferredWidth(80);
    
    // justification colonnes
    DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    cm.getColumn(0).setCellRenderer(centerRenderer);
    cm.getColumn(1).setCellRenderer(centerRenderer);
    cm.getColumn(2).setCellRenderer(rightRenderer);
    cm.getColumn(3).setCellRenderer(centerRenderer);
    cm.getColumn(4).setCellRenderer(centerRenderer);
    cm.getColumn(5).setCellRenderer(rightRenderer);
    cm.getColumn(6).setCellRenderer(rightRenderer);
    cm.getColumn(7).setCellRenderer(centerRenderer);
    cm.getColumn(8).setCellRenderer(rightRenderer);
    cm.getColumn(9).setCellRenderer(rightRenderer);
    
    tblHistoriqueVentesClientArticle.setShowVerticalLines(true);
    tblHistoriqueVentesClientArticle.setShowHorizontalLines(true);
    tblHistoriqueVentesClientArticle.setBackground(Color.white);
    tblHistoriqueVentesClientArticle.setOpaque(false);
    tblHistoriqueVentesClientArticle.setRowHeight(20);
    tblHistoriqueVentesClientArticle.setGridColor(new Color(204, 204, 204));
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeDocumentHistoriqueItemStateChanged(ItemEvent e) {
    try {
      if (e.getStateChange() == ItemEvent.SELECTED) {
        modele.modifierFiltreTypeDocuments(cbTypeDocumentHistorique.getSelectedIndex());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlPrincipal = new SNPanelContenu();
    lbTypeDocumentVente = new SNLabelChamp();
    cbTypeDocumentHistorique = new SNComboBox();
    scpHistoriqueVentesClientArticle = new JScrollPane();
    tblHistoriqueVentesClientArticle = new JTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ---- lbTypeDocumentVente ----
      lbTypeDocumentVente.setText("Type de documents de vente");
      lbTypeDocumentVente.setPreferredSize(new Dimension(200, 30));
      lbTypeDocumentVente.setMinimumSize(new Dimension(200, 30));
      lbTypeDocumentVente.setMaximumSize(new Dimension(200, 30));
      lbTypeDocumentVente.setName("lbTypeDocumentVente");
      pnlPrincipal.add(lbTypeDocumentVente,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- cbTypeDocumentHistorique ----
      cbTypeDocumentHistorique.setModel(new DefaultComboBoxModel(new String[] { "Tous", "Devis", "Bons", "Commandes", "Factures" }));
      cbTypeDocumentHistorique.setMaximumSize(new Dimension(125, 30));
      cbTypeDocumentHistorique.setPreferredSize(new Dimension(125, 30));
      cbTypeDocumentHistorique.setMinimumSize(new Dimension(125, 30));
      cbTypeDocumentHistorique.setName("cbTypeDocumentHistorique");
      cbTypeDocumentHistorique.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          cbTypeDocumentHistoriqueItemStateChanged(e);
        }
      });
      pnlPrincipal.add(cbTypeDocumentHistorique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpHistoriqueVentesClientArticle ========
      {
        scpHistoriqueVentesClientArticle.setName("scpHistoriqueVentesClientArticle");
        
        // ---- tblHistoriqueVentesClientArticle ----
        tblHistoriqueVentesClientArticle.setModel(new DefaultTableModel(
            new Object[][] { { "", "", "", "", "", "", null, null, null }, { "", "", "", "", "", "", null, null, null },
                { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                { null, null, null, null, null, null, null, null, null }, { null, null, null, null, null, null, null, null, null },
                { null, null, null, null, null, null, null, null, null }, },
            new String[] { "Type", "Date", "Num\u00e9ro", "Etat", "Quantit\u00e9", "Prix HT", "Vendeur", "Date facture",
                "Num\u00e9ro facture" }) {
          boolean[] columnEditable = new boolean[] { false, true, true, true, true, true, true, true, true };
          
          @Override
          public boolean isCellEditable(int rowIndex, int columnIndex) {
            return columnEditable[columnIndex];
          }
        });
        {
          TableColumnModel cm = tblHistoriqueVentesClientArticle.getColumnModel();
          cm.getColumn(0).setPreferredWidth(70);
          cm.getColumn(1).setPreferredWidth(70);
          cm.getColumn(2).setPreferredWidth(80);
          cm.getColumn(3).setPreferredWidth(50);
          cm.getColumn(4).setPreferredWidth(100);
          cm.getColumn(5).setPreferredWidth(100);
          cm.getColumn(6).setPreferredWidth(70);
          cm.getColumn(7).setPreferredWidth(80);
          cm.getColumn(8).setPreferredWidth(90);
        }
        tblHistoriqueVentesClientArticle.setShowVerticalLines(true);
        tblHistoriqueVentesClientArticle.setShowHorizontalLines(true);
        tblHistoriqueVentesClientArticle.setBackground(Color.white);
        tblHistoriqueVentesClientArticle.setOpaque(false);
        tblHistoriqueVentesClientArticle.setRowHeight(20);
        tblHistoriqueVentesClientArticle.setGridColor(new Color(204, 204, 204));
        tblHistoriqueVentesClientArticle.setName("tblHistoriqueVentesClientArticle");
        scpHistoriqueVentesClientArticle.setViewportView(tblHistoriqueVentesClientArticle);
      }
      pnlPrincipal.add(scpHistoriqueVentesClientArticle,
          new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlPrincipal;
  private SNLabelChamp lbTypeDocumentVente;
  private SNComboBox cbTypeDocumentHistorique;
  private JScrollPane scpHistoriqueVentesClientArticle;
  private JTable tblHistoriqueVentesClientArticle;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
