/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.contacts;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne commentaire.
 */
public class VueListeContactFournisseur extends AbstractVueDialogue<ModeleListeContactFournisseur> {
  
  private static final String[] TITRE_LISTE_CONTACT = new String[] { "CP", "Nom du contact", "Fonction", "Téléphone", "Mail" };
  private DefaultTableModel tableModelContact = null;
  private JTableCellRendererListeContact listeContactRenderer = null;
  private ImageIcon iconeContactPrincipal = new ImageIcon(getClass().getResource("/images/GVC_valid.png"));
  
  /**
   * Constructeur.
   */
  public VueListeContactFournisseur(ModeleListeContactFournisseur pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    scpListeContact.getViewport().setBackground(Color.WHITE);
    tbListeContact.personnaliserAspect(TITRE_LISTE_CONTACT, new int[] { 50, 220, 120, 120, 280 }, new int[] { 50, 220, 120, 120, 2800 },
        new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    tableModelContact = new DefaultTableModel(null, TITRE_LISTE_CONTACT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    listeContactRenderer = new JTableCellRendererListeContact();
  }
  
  @Override
  public void rafraichir() {
    rafraichirTableau();
  }
  
  /**
   * Rafraichir le tableau affichant la liste des contacts
   */
  private void rafraichirTableau() {
    ListeContact listeContact = getModele().getListeContact();
    Object[][] donnees = null;
    
    if (listeContact != null && !listeContact.isEmpty()) {
      donnees = new Object[listeContact.size()][TITRE_LISTE_CONTACT.length];
      int ligne = 0;
      for (Contact contact : listeContact) {
        if (contact != null && contact.isCharge()) {
          Object cp = null;
          if (getModele().getContactPrincipal() != null && contact.getId().equals(getModele().getContactPrincipal().getId())) {
            cp = "X";
          }
          
          donnees[ligne][0] = cp;
          donnees[ligne][1] = contact.getNomComplet();
          donnees[ligne][2] = getModele().retournerLibelleContact(contact.getIdCategorie());
          donnees[ligne][3] = contact.getNumeroTelephone1();
          donnees[ligne][4] = contact.getEmail();
        }
        ligne++;
      }
    }
    
    tbListeContact.mettreAJourDonnees(donnees);
    
    /*
    tableModelContact = new DefaultTableModel(donnees, TITRE_LISTE_CONTACT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
      
      @Override
      public Class getColumnClass(int column) {
        return getValueAt(0, column).getClass();
      }
    };
    
    // Remplacer le modèle de la table si besoin
    tbListeContact.setModel(tableModelContact);
    
    TableColumnModel cm = tbListeContact.getColumnModel();
    cm.getColumn(0).setPreferredWidth(40);
    cm.getColumn(1).setPreferredWidth(220);
    cm.getColumn(2).setPreferredWidth(120);
    cm.getColumn(3).setPreferredWidth(120);
    cm.getColumn(4).setPreferredWidth(280);
    */
    
  }
  
  // -- Méthodes interractives
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlCommentaire = new SNPanelContenu();
    scpListeContact = new JScrollPane();
    tbListeContact = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(850, 200));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Liste des contacts du fournisseur");
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCommentaire ========
      {
        pnlCommentaire.setOpaque(false);
        pnlCommentaire.setPreferredSize(new Dimension(472, 230));
        pnlCommentaire.setName("pnlCommentaire");
        pnlCommentaire.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
        
        // ======== scpListeContact ========
        {
          scpListeContact.setPreferredSize(new Dimension(452, 200));
          scpListeContact.setBackground(Color.white);
          scpListeContact.setOpaque(true);
          scpListeContact.setName("scpListeContact");
          
          // ---- tbListeContact ----
          tbListeContact
              .setModel(new DefaultTableModel(new Object[][] { { null, "", null, null, null }, { null, null, null, null, null }, },
                  new String[] { " ", "Nom", "Fonction", "Tel\u00e9phone", "Mail" }));
          {
            TableColumnModel cm = tbListeContact.getColumnModel();
            cm.getColumn(0).setMinWidth(50);
            cm.getColumn(0).setMaxWidth(50);
            cm.getColumn(0).setPreferredWidth(50);
            cm.getColumn(1).setMinWidth(200);
            cm.getColumn(1).setMaxWidth(200);
            cm.getColumn(1).setPreferredWidth(200);
            cm.getColumn(2).setMinWidth(80);
            cm.getColumn(2).setMaxWidth(80);
            cm.getColumn(2).setPreferredWidth(80);
            cm.getColumn(3).setMinWidth(80);
            cm.getColumn(3).setMaxWidth(80);
            cm.getColumn(3).setPreferredWidth(80);
            cm.getColumn(4).setMinWidth(250);
            cm.getColumn(4).setPreferredWidth(250);
          }
          tbListeContact.setRowHeight(22);
          tbListeContact.setName("tbListeContact");
          scpListeContact.setViewportView(tbListeContact);
        }
        pnlCommentaire.add(scpListeContact, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlCommentaire, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlCommentaire;
  private JScrollPane scpListeContact;
  private NRiTable tbListeContact;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
