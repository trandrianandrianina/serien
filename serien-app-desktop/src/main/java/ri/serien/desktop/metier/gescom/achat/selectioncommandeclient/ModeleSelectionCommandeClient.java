/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.selectioncommandeclient;

import java.util.ArrayList;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.EnumTypeApprovisionnement;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleSelectionCommandeClient extends AbstractModeleDialogue {
  // Constantes
  public static final int COMMANDE_CLIENT_URGENTE = 0;
  public static final int COMMANDE_DIRECT_USINE = 1;
  
  // Variables
  private ModeleAchat modeleParent = null;
  private IdEtablissement idEtablissement = null;
  private Fournisseur fournisseur = null;
  private IdMagasin idMagasin = null;
  private Message message = null;
  private ArrayList<Integer> listeIndiceLigneSelectionnee = null;
  private ArrayList<DocumentVente> listeDocumentVenteChoisi = null;
  private List<IdLigneVente> listeIdLigneVenteAApprovisionner = null;
  private ListeModeExpedition listeModesExpedition = null;
  private EnumTypeApprovisionnement typeApprovisionnement;
  private boolean multiSelection = true;
  private boolean filtreIdClientFactureActif = false;
  private IdClient idClientFacture = null;
  private IdDocumentVente idDocumentVenteSelectionne = null;
  
  // Variables pour le chargement progressif de la liste
  private ArrayList<LigneAAfficher> listeLigneAAfficher = new ArrayList<LigneAAfficher>();
  private int indexPremiereLigneAffichee = 0;
  // Ne doit pas être initialisée dans le code car cette valeur est recalculée cas cela est nécessaire
  private int nombreLigneAffichee = 0;
  private int indexRechercheDocumentVente = -1;
  
  /**
   * Constructeur.
   */
  public ModeleSelectionCommandeClient(SessionBase pSession, ModeleAchat pModeleParent, EnumTypeApprovisionnement pTypeApprovisionnement,
      Fournisseur pFournisseur, IdMagasin pIdMagasin) {
    super(pSession);
    modeleParent = pModeleParent;
    fournisseur = pFournisseur;
    idMagasin = pIdMagasin;
    typeApprovisionnement = pTypeApprovisionnement;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    switch (typeApprovisionnement) {
      case CLIENT:
        setTitreEcran("Liste des commandes clients non livrées");
        break;
      case DIRECT_USINE:
        setTitreEcran("Liste des commandes directs usines");
        break;
      default:
        setTitreEcran("");
    }
  }
  
  @Override
  public void chargerDonnees() {
    // Contrôle des variables obligatoires
    if (fournisseur == null || fournisseur.getId() == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    if (idMagasin == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    idEtablissement = fournisseur.getId().getIdEtablissement();
    
    // Récupération de la liste des modes d'expédition
    listeModesExpedition = new ListeModeExpedition();
    listeModesExpedition = ListeModeExpedition.charger(getSession().getIdSession());
    if (listeModesExpedition == null) {
      throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé.");
    }
    
    // Lancement de la recherche dès l'affichage de la boite de dialogue
    rechercherDocumentVente();
  }
  
  @Override
  public void quitterAvecValidation() {
    // On récupère la liste des documents de vente à partir des lignes qui ont été sélectionnée
    if (listeDocumentVenteChoisi == null) {
      listeDocumentVenteChoisi = new ArrayList<DocumentVente>();
    }
    else {
      listeDocumentVenteChoisi.clear();
    }
    ArrayList<LigneAAfficher> listeLigneChoisi = retournerListeLigneChoisi();
    if (listeLigneChoisi == null || listeLigneChoisi.isEmpty()) {
      throw new MessageErreurException("Aucun document de vente n'a été choisi.");
    }
    // Constitution de la liste des documents de vente choisi à partir des lignes sélectionnées du tableau
    for (LigneAAfficher ligneChoisi : listeLigneChoisi) {
      listeDocumentVenteChoisi.add(ligneChoisi.getDocumentVente());
    }
    
    // On récupère la liste des id des lignes de ventes appartenant à ce document
    if (listeDocumentVenteChoisi == null || listeDocumentVenteChoisi.isEmpty()) {
      throw new MessageErreurException("Aucun document de vente n'a été choisi.");
    }
    // On créé des lignes (vides) mais contenant leur id pour l'ensemble des documents de vente sélectionnés
    for (DocumentVente documentVente : listeDocumentVenteChoisi) {
      if (documentVente == null) {
        throw new MessageErreurException("Le document de vente est invalide");
      }
      for (IdLigneVente idLigneVente : listeIdLigneVenteAApprovisionner) {
        // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
        IdDocumentVente idDocumentLigneVente = IdDocumentVente.getInstanceHorsFacture(idLigneVente.getIdEtablissement(),
            idLigneVente.getCodeEntete(), idLigneVente.getNumero(), idLigneVente.getSuffixe());
        if (idDocumentLigneVente.equals(documentVente.getId())) {
          documentVente.getListeLigneVente().add(new LigneVente(idLigneVente));
        }
      }
    }
    
    // Traitement normal
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    listeDocumentVenteChoisi = null;
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Afficher la plage de lignes d'achat comprise entre deux lignes.
   * Les informations des lignes d'achats sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticlesAffichees(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeLigneAAfficher == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeLigneAAfficher.size()) {
      indexPremiereLigneAffichee = listeLigneAAfficher.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeLigneAAfficher == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeLigneAAfficher.size()) {
      nombreLigneAffichee = listeLigneAAfficher.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacements de l'ascenseur avec la souris
    if (indexRechercheDocumentVente < indexPremiereLigneAffichee) {
      indexRechercheDocumentVente = indexPremiereLigneAffichee;
    }
    else if (indexRechercheDocumentVente > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheDocumentVente = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleSelectionCommandeClient.class, "modifierPlageLignesAffichees", "indexPremiereLigneVisible",
        indexPremiereLigneAffichee, "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie la liste des indices des lignes séléctionnées dans le tableau.
   */
  public void modifierListeIndiceTableau(ArrayList<Integer> pListeIndiceTableau) {
    boolean lancerNouvelleRecherche = false;
    // On doit lancer une nouvelle recherche :
    // - s'il y a plus d'un seul document de vente
    // - si l'on a activé le filtre sur le client facturé et qu'il s'agit de la première sélection
    // - si on a désélectionné toutes les lignes
    if (listeLigneAAfficher != null && listeLigneAAfficher.size() > 1
        && (filtreIdClientFactureActif && listeIndiceLigneSelectionnee == null) || (pListeIndiceTableau == null)
        || pListeIndiceTableau.isEmpty()) {
      lancerNouvelleRecherche = true;
    }
    // On stocke la liste des indices des lignes sélectionnées dans le tableau
    listeIndiceLigneSelectionnee = pListeIndiceTableau;
    // On prépare la nouvelle recherche
    if (lancerNouvelleRecherche) {
      // Cas où aucune ligne n'est sélectionnée on lance une recherche sans filtre
      if (listeIndiceLigneSelectionnee == null || listeIndiceLigneSelectionnee.isEmpty()) {
        idDocumentVenteSelectionne = null;
        idClientFacture = null;
      }
      // Cas où une ou des lignes ont été sélectionné on lance une recherche avec filtre
      else {
        int indiceLigne = listeIndiceLigneSelectionnee.get(0);
        if (-1 < indiceLigne && indiceLigne < listeLigneAAfficher.size()) {
          DocumentVente document = listeLigneAAfficher.get(indiceLigne).getDocumentVente();
          idDocumentVenteSelectionne = document.getId();
          idClientFacture = document.getIdClientFacture();
        }
      }
      rechercherDocumentVente();
      rafraichir();
    }
  }
  
  /**
   * Permet de vérifier qu'au moins un document de la liste a bien été sélectionné.
   */
  public boolean isVerifierSelectionDocument() {
    return listeIndiceLigneSelectionnee != null && !listeIndiceLigneSelectionnee.isEmpty();
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les variables liées au chargement des données dans le tableau.
   */
  private void effacerVariablesChargement() {
    indexPremiereLigneAffichee = 0;
    indexRechercheDocumentVente = -1;
    listeLigneAAfficher.clear();
    
    listeIndiceLigneSelectionnee = null;
  }
  
  /**
   * Recherche les documents de vente contenant des articles à commander chez le fournisseur courant.
   */
  private void rechercherDocumentVente() {
    message = null;
    effacerVariablesChargement();
    
    // Initialisation les critères de recherche des documents de vente
    CritereLigneVente criteres = new CritereLigneVente();
    criteres.setIdEtablissement(modeleParent.getEtablissement().getId());
    criteres.setIdFournisseur(fournisseur.getId());
    criteres.setIdMagasin(modeleParent.getMagasin().getId());
    switch (typeApprovisionnement) {
      case CLIENT:
        criteres.setCommandeDirectUsine(false);
        break;
      case DIRECT_USINE:
        criteres.setCommandeDirectUsine(true);
        break;
    }
    // Gestion du filtre s'il est en cours
    if (isFiltrageEnCours()) {
      criteres.setIdClientFacture(IdClient.getInstance(idEtablissement, idClientFacture.getNumero(), idClientFacture.getSuffixe()));
    }
    else {
      criteres.setIdClientFacture(null);
    }
    
    // Lancement de la recherche des articles à approvisionner
    listeIdLigneVenteAApprovisionner = ManagerServiceDocumentVente.chargerListeIdLigneVenteAApprovisionner(getIdSession(), criteres);
    
    // Création de la liste des documents de vente pour la liste à afficher
    if (listeIdLigneVenteAApprovisionner == null || listeIdLigneVenteAApprovisionner.isEmpty()) {
      message = Message.getMessageImportant("Aucun document de vente n'a été trouvé.");
      return;
    }
    // On parcourt la liste des id lignes de vente pour ne retenir que les id documents de vente que l'on va afficher
    listeLigneAAfficher.clear();
    IdDocumentVente idDocumentVente = null;
    for (IdLigneVente idLigneVente : listeIdLigneVenteAApprovisionner) {
      // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
      // Idéalement il faudrait une liste de ligne de vente base qui contiendra l'id d u document de vente
      IdDocumentVente idDocumentLigneVente = IdDocumentVente.getInstanceHorsFacture(idLigneVente.getIdEtablissement(),
          idLigneVente.getCodeEntete(), idLigneVente.getNumero(), idLigneVente.getSuffixe());
      if (!idDocumentLigneVente.equals(idDocumentVente)) {
        idDocumentVente = idDocumentLigneVente;
        listeLigneAAfficher.add(new LigneAAfficher(idDocumentVente));
      }
    }
    
    // Si le filtre est actif
    if (filtreIdClientFactureActif) {
      // Si le filtre n'est pas en cours, on affiche un message explicatif du foncionnement
      if (idClientFacture == null) {
        message = Message.getMessageNormal("En sélectionnant un document, la liste filtrera les documents sur ce même client. ");
      }
      // On met à jour le tableau des lignes sélectionés afin de pourvoir resélectionner la ligne qui a servit de référence pour le
      // filtrage
      if (idDocumentVenteSelectionne != null) {
        listeIndiceLigneSelectionnee = new ArrayList<Integer>();
        int indice = 0;
        for (IdLigneVente idLigneVente : listeIdLigneVenteAApprovisionner) {
          // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
          // Idéalement il faudrait une liste de ligne de vente base qui contiendra l'id d u document de vente
          IdDocumentVente idDocumentLigneVente = IdDocumentVente.getInstanceHorsFacture(idLigneVente.getIdEtablissement(),
              idLigneVente.getCodeEntete(), idLigneVente.getNumero(), idLigneVente.getSuffixe());
          // On recherche l'id du document qui a été sélectionné précedemment et qui a servit de référent pour la nouvelle recherche
          if (idDocumentLigneVente.equals(idDocumentVenteSelectionne)) {
            listeIndiceLigneSelectionnee.add(indice);
            indice = -1;
            break;
          }
          indice++;
        }
        // Si elle n'a pas été trouvé on sélectionne la première ligne à défaut d'autre chose
        if (indice > -1) {
          listeIndiceLigneSelectionnee.add(0);
        }
      }
    }
    
    // Charger les lignes du tableau affichés
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
  }
  
  /**
   * Charger les données des lignes du tableau comprises entre deux lignes.
   * Les informations des lignes du tableau sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageLigneTableau(int pPremiereLigne, int pNombreLigne) {
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeLigneAAfficher.size());
    
    // Lister les id des lignes du tableau dont il faut charger les informations
    List<IdDocumentVente> listeIdDocumentVente = new ArrayList<IdDocumentVente>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneAAfficher = listeLigneAAfficher.get(i);
      if (!ligneAAfficher.isCharge()) {
        listeIdDocumentVente.add(ligneAAfficher.getIdDocumentVente());
      }
    }
    
    // Quitter immédiatement si aucun document de vente n'est à charger
    if (listeIdDocumentVente.isEmpty()) {
      return;
    }
    
    // Charger les informations des lignes du tableau
    List<DocumentVente> listeDocumentVenteCharges =
        ManagerServiceDocumentVente.chargerEnteteListeDocumentVente(getIdSession(), listeIdDocumentVente);
    
    // Mettre les lignes du tableau chargées au bon endroit dans la liste
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneAAfficher = listeLigneAAfficher.get(i);
      if (!ligneAAfficher.isCharge()) {
        for (DocumentVente documentCharge : listeDocumentVenteCharges) {
          if (ligneAAfficher.getIdDocumentVente().equals(documentCharge.getId())) {
            listeLigneAAfficher.get(i).initialiserDonnees(documentCharge);
            Trace.debug(ModeleSelectionCommandeClient.class, "chargerPlageLigneTableau", "Charger ligne tableau", "ligne", i,
                "idDocumentVente", documentCharge.getId());
            break;
          }
        }
      }
    }
  }
  
  /**
   * Retourne la liste des objets constituants les lignes sélectionnées du tableau.
   */
  private ArrayList<LigneAAfficher> retournerListeLigneChoisi() {
    if (!isVerifierSelectionDocument()) {
      return null;
    }
    
    // Contrôle que les lignes sélectionnées soient bien chargées
    // Cas d'une sélection complète (Ctrl + A) avec plus de lignes sélectionnées que de lignes affichées
    if (listeIndiceLigneSelectionnee.size() == listeLigneAAfficher.size()
        && listeLigneAAfficher.get(listeLigneAAfficher.size() - 1) != null
        && listeLigneAAfficher.get(listeLigneAAfficher.size() - 1).getDocumentVente() == null) {
      chargerPlageLigneTableau(0, listeLigneAAfficher.size());
    }
    
    // Chargment des lignes sélectionnées
    ArrayList<LigneAAfficher> listeLigneChoisi = new ArrayList<LigneAAfficher>();
    for (Integer indice : listeIndiceLigneSelectionnee) {
      // On s'assure que l'indice d'une des lignes sélectionnée ne soit pas plus grande que le nombre de ligne du tableau des lignes à
      // afficher
      if (indice >= listeLigneAAfficher.size()) {
        continue;
      }
      listeLigneChoisi.add(listeLigneAAfficher.get(indice));
    }
    return listeLigneChoisi;
    
  }
  
  // -- Accesseurs
  
  public boolean isFiltrageEnCours() {
    return filtreIdClientFactureActif && idClientFacture != null;
  }
  
  public ArrayList<LigneAAfficher> getListeAAfficher() {
    return listeLigneAAfficher;
  }
  
  public Message getMessage() {
    return message;
  }
  
  public ArrayList<Integer> getListeIndiceLigneSelectionnee() {
    return listeIndiceLigneSelectionnee;
  }
  
  public ArrayList<DocumentVente> getListeDocumentVenteChoisi() {
    return listeDocumentVenteChoisi;
  }
  
  public boolean isMultiSelection() {
    return multiSelection;
  }
  
  public void setMultiSelection(boolean multiSelection) {
    this.multiSelection = multiSelection;
  }
  
  public void setFiltreIdClientFactureActif(boolean filtreIdClientFactureActif) {
    this.filtreIdClientFactureActif = filtreIdClientFactureActif;
  }
}
