/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererLigneCommandeDetail extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color noir = new Color(0, 0, 0);
  private static Color rouge = new Color(218, 100, 100);
  private static int tailleQuantite = 60;
  private static int tailleUnite = 50;
  private static int tailleLibelle = 180;
  private static int tailleMontant = 90;
  private int tailleMini = 0;
  private int tailleMaxi = 0;
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    TableColumnModel cm = table.getColumnModel();
    Object o = table.getValueAt(row, column);
    if (o != null && component instanceof JLabel) {
      JLabel label = (JLabel) component;
      
      switch (column) {
        case 0:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleQuantite;
          tailleMaxi = tailleQuantite;
          component.setForeground(noir);
          break;
        
        case 1:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = 70;
          tailleMaxi = 70;
          component.setForeground(noir);
          break;
        
        case 2:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleQuantite;
          tailleMaxi = tailleQuantite;
          component.setForeground(noir);
          break;
        
        case 3:
          label.setHorizontalAlignment(CENTER);
          tailleMini = tailleUnite;
          tailleMaxi = tailleUnite;
          component.setForeground(noir);
          break;
        
        case 4:
          label.setHorizontalAlignment(LEFT);
          tailleMini = tailleLibelle;
          tailleMaxi = 3000;
          component.setForeground(noir);
          break;
        
        case 5:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        case 6:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleQuantite;
          tailleMaxi = tailleQuantite;
          component.setForeground(noir);
          break;
        
        case 7:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        case 8:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        case 9:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        case 10:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = 50;
          tailleMaxi = 50;
          component.setForeground(noir);
          break;
        
        case 11:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = 50;
          tailleMaxi = 50;
          if (label.getText().contains("-")) {
            component.setForeground(rouge);
          }
          else {
            component.setForeground(noir);
          }
          break;
        
        case 12:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        case 13:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = 30;
          tailleMaxi = 30;
          component.setForeground(noir);
          break;
        
        case 14:
          label.setHorizontalAlignment(RIGHT);
          tailleMini = tailleMontant;
          tailleMaxi = tailleMontant;
          component.setForeground(noir);
          break;
        
        default:
          break;
      }
      cm.getColumn(column).setMinWidth(tailleMini);
      cm.getColumn(column).setMaxWidth(tailleMaxi);
    }
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(60);
    cm.getColumn(0).setMaxWidth(60);
    cm.getColumn(1).setMinWidth(70);
    cm.getColumn(1).setMaxWidth(70);
    cm.getColumn(2).setMinWidth(60);
    cm.getColumn(2).setMaxWidth(60);
    cm.getColumn(3).setMinWidth(30);
    cm.getColumn(3).setMaxWidth(30);
    cm.getColumn(4).setMinWidth(200);
    cm.getColumn(5).setMinWidth(90);
    cm.getColumn(5).setMaxWidth(90);
    cm.getColumn(6).setMinWidth(90);
    cm.getColumn(6).setMaxWidth(90);
    cm.getColumn(7).setMinWidth(90);
    cm.getColumn(7).setMaxWidth(90);
    cm.getColumn(8).setMinWidth(60);
    cm.getColumn(8).setMaxWidth(60);
    cm.getColumn(9).setMinWidth(30);
    cm.getColumn(9).setMaxWidth(30);
    cm.getColumn(10).setMinWidth(50);
    cm.getColumn(10).setMaxWidth(50);
    cm.getColumn(11).setMinWidth(50);
    cm.getColumn(11).setMaxWidth(50);
    cm.getColumn(12).setMinWidth(50);
    cm.getColumn(12).setMaxWidth(50);
    cm.getColumn(13).setMinWidth(30);
    cm.getColumn(13).setMaxWidth(30);
    cm.getColumn(14).setMinWidth(90);
    cm.getColumn(14).setMaxWidth(90);
  }
}
