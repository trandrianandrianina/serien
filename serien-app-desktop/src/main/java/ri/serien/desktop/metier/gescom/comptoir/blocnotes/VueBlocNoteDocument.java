/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.blocnotes;

import java.awt.AWTException;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueBlocNoteDocument extends AbstractVueDialogue<ModeleBlocNoteDocument> {
  
  // Variable
  private Message messageDecompte = null;
  private Message messageLigne = null;
  public static final int NOMBRE_LIGNES_AFFICHEES = 8;
  // Variable contenant le nombre maximal que peux contenir le champ SQL
  public static final int LONGUEURMAXIMALLIGNE = 30;
  
  /**
   * Constructeur.
   */
  public VueBlocNoteDocument(ModeleBlocNoteDocument pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    taBlocNoteDocument.setDocument(new ColonnesComposantTexte(DocumentVente.LONGUEUR_PETIT_BLOCNOTE));
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    taBlocNoteDocument.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    taBlocNoteDocument.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    taBlocNoteDocument.setText("");
    
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // final AbstractDocument zoneTexte = (AbstractDocument) taBlocNoteDocument.getDocument();
    taBlocNoteDocument.getDocument().addDocumentListener(new DocumentListener() {
      
      @Override
      public void removeUpdate(DocumentEvent e) {
        String tailleTexte = taBlocNoteDocument.getText();
        // Permet de ne pas compter les retour à la ligne
        tailleTexte = tailleTexte.replaceAll("\\n", "");
        messageDecompte = Message.getMessageNormal(tailleTexte.length() + " /240");
        lbDecompte.setMessage(messageDecompte);
        
        messageLigne = Message.getMessageNormal(taBlocNoteDocument.getLineCount() + "/8");
        lbNomeLigne.setMessage(messageLigne);
        if (taBlocNoteDocument.getLineCount() > NOMBRE_LIGNES_AFFICHEES) {
          snBarreBouton.activerBouton(EnumBouton.VALIDER, false);
        }
        else {
          snBarreBouton.activerBouton(EnumBouton.VALIDER, true);
        }
      }
      
      @Override
      public void insertUpdate(DocumentEvent e) {
        int nombrecaractere = getCaractereLigne();
        String tailleTexte = taBlocNoteDocument.getText();
        // Permet de ne pas compter les retour à la ligne
        tailleTexte = tailleTexte.replaceAll("\\n", "");
        messageDecompte = Message.getMessageNormal(tailleTexte.length() + " /240");
        lbDecompte.setMessage(messageDecompte);
        
        messageLigne = Message.getMessageNormal(taBlocNoteDocument.getLineCount() + "/8");
        lbNomeLigne.setMessage(messageLigne);
        if (nombrecaractere == LONGUEURMAXIMALLIGNE) {
          if (taBlocNoteDocument.getLineCount() == NOMBRE_LIGNES_AFFICHEES) {
            try {
              Robot robot = new Robot();
              robot.keyPress(KeyEvent.VK_BACK_SPACE);
            }
            catch (AWTException exception) {
              // XXX Auto-generated catch block
              exception.printStackTrace();
            }
          }
          else {
            try {
              Robot robot = new Robot();
              robot.keyPress(KeyEvent.VK_ENTER);
            }
            catch (AWTException exception) {
              // XXX Auto-generated catch block
              exception.printStackTrace();
            }
          }
        }
        // Empeche l'utilisateur de revenir sur une ligne avec deja 30 caractère,la commande KeyEvent.VK_ENTER étant compter comme un
        // caractère on vérifie si la longueur est supérieur à 31
        else if (nombrecaractere > LONGUEURMAXIMALLIGNE + 1) {
          try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
          }
          catch (AWTException exception) {
            // XXX Auto-generated catch block
            exception.printStackTrace();
          }
        }
        // Bloque à la 18eme ligne
        if (taBlocNoteDocument.getLineCount() > NOMBRE_LIGNES_AFFICHEES) {
          try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
          }
          catch (AWTException exception) {
            // XXX Auto-generated catch block
            exception.printStackTrace();
          }
        }
      }
      
      @Override
      public void changedUpdate(DocumentEvent arg0) {
        int nombrecaractere = getCaractereLigne();
        String tailleTexte = taBlocNoteDocument.getText();
        // Permet de ne pas compter les retour à la ligne
        tailleTexte = tailleTexte.replaceAll("\\n", "");
        messageDecompte = Message.getMessageNormal(tailleTexte.length() + " /240");
        lbDecompte.setMessage(messageDecompte);
        
        messageLigne = Message.getMessageNormal(taBlocNoteDocument.getLineCount() + "/8");
        lbNomeLigne.setMessage(messageLigne);
        if (nombrecaractere == LONGUEURMAXIMALLIGNE) {
          if (taBlocNoteDocument.getLineCount() == NOMBRE_LIGNES_AFFICHEES) {
            try {
              Robot robot = new Robot();
              robot.keyPress(KeyEvent.VK_BACK_SPACE);
            }
            catch (AWTException exception) {
              // XXX Auto-generated catch block
              exception.printStackTrace();
            }
          }
          try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_ENTER);
          }
          catch (AWTException exception) {
            // XXX Auto-generated catch block
            exception.printStackTrace();
          }
        }
        // Empeche l'utilisateur de revenir sur une ligne avec deja 30 caractère,la commande KeyEvent.VK_ENTER étant compter comme un
        // caractère on vérifie si la longueur est supérieur à 31
        else if (nombrecaractere > LONGUEURMAXIMALLIGNE + 1) {
          try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
          }
          catch (AWTException exception) {
            // XXX Auto-generated catch block
            exception.printStackTrace();
          }
        }
        // Bloque à la 18eme ligne
        if (taBlocNoteDocument.getLineCount() > NOMBRE_LIGNES_AFFICHEES) {
          try {
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_BACK_SPACE);
          }
          catch (AWTException exception) {
            // XXX Auto-generated catch block
            exception.printStackTrace();
          }
        }
      }
    });
  }
  
  /**
   * Rafraichissement des données à l'écran.
   */
  @Override
  public void rafraichir() {
    String tailleTexte = taBlocNoteDocument.getText();
    // Permet de ne pas compter les retour à la ligne
    tailleTexte = tailleTexte.replaceAll("\\n", "");
    messageDecompte = Message.getMessageNormal(tailleTexte.length() + " /240");
    lbDecompte.setMessage(messageDecompte);
    messageLigne = Message.getMessageNormal(taBlocNoteDocument.getLineCount() + "/8");
    lbNomeLigne.setMessage(messageLigne);
    rafraichirTexte();
  }
  
  // -- Méthodes privées
  private void rafraichirTexte() {
    String texte = "";
    if (getModele().getBlocNote() != null && getModele().getBlocNote().getTexte() != null) {
      texte = getModele().getBlocNote().getTexte();
    }
    // Permet de récupérée ligne par ligne le texte sans les espace inutile
    String[] texteLigne = texte.split("\n");
    int nombreLigne = texteLigne.length;
    for (int i = 0; i < nombreLigne; i++) {
      // met la 1er ligne en tant que texte, append rajoutant du texte à la fin du document.Cela évite d'avoir une premiere ligne vide
      if (i == 0) {
        taBlocNoteDocument.setText(texteLigne[i].trim());
      }
      else {
        taBlocNoteDocument.append(texteLigne[i].trim());
      }
      // Permet de faire un saut à la ligne à chaque fin de ligne sauf si on arrive à la dernière ligne ou à la limite max
      if (i + 1 != nombreLigne && i + 1 != NOMBRE_LIGNES_AFFICHEES) {
        taBlocNoteDocument.append("\n");
      }
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().modifierTexte(taBlocNoteDocument.getText());
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne le nombre de ligne
   */
  private int getNombreLigne() {
    int nombreLigne = 0;
    int caretOffset = taBlocNoteDocument.getCaretPosition();
    try {
      nombreLigne = taBlocNoteDocument.getLineOfOffset(caretOffset);
    }
    catch (BadLocationException e) {
      // XXX Auto-generated catch block
      e.printStackTrace();
    }
    return nombreLigne;
  }
  
  /**
   * Retourne le nombre de caractère que possede une ligne
   */
  private int getCaractereLigne() {
    int nombreLigne = getNombreLigne();
    int numeroPremierCaractere = 0;
    int numeorDernierCaractere = 0;
    try {
      numeroPremierCaractere = taBlocNoteDocument.getLineStartOffset(nombreLigne);
      numeorDernierCaractere = taBlocNoteDocument.getLineEndOffset(nombreLigne);
    }
    catch (BadLocationException e) {
      // XXX Auto-generated catch block
      e.printStackTrace();
    }
    return numeorDernierCaractere - numeroPremierCaractere;
    
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelEcranRPG();
    pnlContenu = new SNPanelContenu();
    scpBlocNoteDocument = new JScrollPane();
    taBlocNoteDocument = new JTextArea();
    pnlIndicationBlocNote = new SNPanel();
    lbNomeLigne = new SNLabelTitre();
    lbDecompte = new SNLabelTitre();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Bloc-notes du document");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(385, 390));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        
        // ======== scpBlocNoteDocument ========
        {
          scpBlocNoteDocument.setName("scpBlocNoteDocument");
          
          // ---- taBlocNoteDocument ----
          taBlocNoteDocument.setBorder(new BevelBorder(BevelBorder.LOWERED));
          taBlocNoteDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          taBlocNoteDocument.setWrapStyleWord(true);
          taBlocNoteDocument.setLineWrap(true);
          taBlocNoteDocument.setMargin(new Insets(10, 10, 10, 10));
          taBlocNoteDocument.setName("taBlocNoteDocument");
          scpBlocNoteDocument.setViewportView(taBlocNoteDocument);
        }
        pnlContenu.add(scpBlocNoteDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlIndicationBlocNote ========
        {
          pnlIndicationBlocNote.setName("pnlIndicationBlocNote");
          pnlIndicationBlocNote.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlIndicationBlocNote.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlIndicationBlocNote.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlIndicationBlocNote.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlIndicationBlocNote.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbNomeLigne ----
          lbNomeLigne.setText("8/8");
          lbNomeLigne.setName("lbNomeLigne");
          pnlIndicationBlocNote.add(lbNomeLigne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbDecompte ----
          lbDecompte.setText("240/240");
          lbDecompte.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDecompte.setName("lbDecompte");
          pnlIndicationBlocNote.add(lbDecompte, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlIndicationBlocNote, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelEcranRPG pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private JScrollPane scpBlocNoteDocument;
  private JTextArea taBlocNoteDocument;
  private SNPanel pnlIndicationBlocNote;
  private SNLabelTitre lbNomeLigne;
  private SNLabelTitre lbDecompte;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
  public class ColonnesComposantTexte extends PlainDocument {
    // Variables
    private int maxChar = 255;
    
    /**
     * Constructeur.
     */
    public ColonnesComposantTexte(int pMaxChar) {
      super();
      setMaxChar(pMaxChar);
    }
    
    /**
     * Initialise le nombre de caractères maximum.
     */
    public final void setMaxChar(int pMaxChar) {
      this.maxChar = pMaxChar;
    }
    
    @Override
    public final void insertString(int offs, String str, AttributeSet a) throws BadLocationException {
      if (str == null) {
        return;
      }
      
      if ((getLength() + str.length()) > maxChar) {
        Toolkit.getDefaultToolkit().beep();
        return;
      }
      super.insertString(offs, str, a);
    }
  }
}
