/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import ri.serien.desktop.metier.gescom.consultationdocumentachat.detailligneachat.ModeleDetailLigneAchat;
import ri.serien.desktop.metier.gescom.consultationdocumentachat.detailligneachat.VueDetailLigneAchat;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.gescom.commun.representant.Representant;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle de l'écran principal de la consultation de documents d'achat.
 */
public class ModeleConsultationDocumentAchat extends AbstractModelePanel {
  
  // Constantes detail
  public static final int MODE_AFFICHAGE_DETAIL_TABLE_STANDARD = 2;
  public static final int MODE_AFFICHAGE_DETAIL_TABLE_DETAIL = 3;
  public static final int NOMBRE_LIGNES_ARTICLES_DETAIL = 15;
  
  public static final String TYPE_DOCUMENT_TRANSFERT = "Transfert";
  public static final String TYPE_DOCUMENT_COMMANDE = "Commande";
  public static final String TYPE_DOCUMENT_FACTURE = "Facture";
  public static final String TYPE_DOCUMENT_RECEPTION = "Réception";
  
  // Constantes liste
  public static final String TITRE = "Consultation de documents d'achats";
  public static final int MODE_AFFICHAGE_DOCUMENT_LISTE = 0;
  public static final int MODE_AFFICHAGE_LIGNE_LISTE = 1;
  public static final int NOMBRE_LIGNES_DOCUMENTS_LISTE = 15;
  public static final int LONGUEUR_MINI_EXPRESSION_RECHERCHEE_LISTE = 3;
  
  // Variables globales
  private List<IdDocumentAchat> listeIdDocumentAchat = null;
  private List<IdLigneAchat> listeIdLigneAchat = null;
  private int indexDocumentSelectionne = -1;
  private int indexLigneSelectionne = -1;
  private IdDocumentAchat idDocumentSelectionne = null;
  private IdLigneAchat idLigneAchatSelectionnee = null;
  
  // Variables liste
  private UtilisateurGescom utilisateurGescom = null;
  
  private CritereDocumentAchat criteresDocument = new CritereDocumentAchat();
  private CritereLigneAchat criteresLigne = new CritereLigneAchat();
  private ListeDocumentAchat listeDocumentAchat = null;
  private ListeFournisseurBase listeFournisseurBase = null;
  private AdresseFournisseur adresseFournisseurSelectionne = null;
  private ListeLigneAchatBase listeLigneAchat = null;
  
  private ListeMagasin listeMagasin = null;
  private IdMagasin idMagasin = null;
  private int numeroDocument = 0;
  private Date dateDebut = null;
  private Date dateFin = null;
  private EnumTypeDocumentAchat typeDocument = null;
  private IdArticle idArticle = null;
  private LigneAchat ligneAchatSelectionnee = null;
  private int indexRechercheDocument = -1;
  private int indexRechercheLigne = -1;
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private String texteArticleRecherche = "";
  private String referenceDocument = "";
  private boolean isFenetreArticleAffichee = false;
  private int modeAffichage = MODE_AFFICHAGE_DOCUMENT_LISTE;
  private int nombreLignesArticleTrouvees = 0;
  private ListeEtablissement listeEtablissement = null;
  private Etablissement etablissement = null;
  private Message titreResultat = null;
  
  // Variables détail
  private DocumentAchat documentAchatDetail = null;
  private ListeMagasin listeMagasinDetail = null;
  private Magasin magasinSortieDetail = null;
  private ListeRepresentant listeRepresentantDetail = null;
  private Representant representantDetail = null;
  private ListeAcheteur listeAcheteurDetail = null;
  private Acheteur acheteurDetail = null;
  private Fournisseur fournisseurDetail = null;
  private ListeUnite listeUniteDetail = null;
  private LigneAchatArticle ligneAchatSelectionneeDetail = null;
  private int modeAffichageTableDetail = MODE_AFFICHAGE_DETAIL_TABLE_STANDARD;
  private ListeModeExpedition listeModesExpeditionDetail = null;
  
  /**
   * Constructeur.
   */
  public ModeleConsultationDocumentAchat(SessionBase pSession) {
    super(pSession);
    // Renseigner le titre de l'onglet
    getSession().getPanel().setName(ModeleConsultationDocumentAchat.TITRE);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
    // Se mettre en mode liste par défaut
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Initialiser données liste
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    
    // Renseigner les dates de début et de fin
    if (dateFin == null) {
      dateFin = new Date();
      dateDebut = Constantes.getDateDebutMoisPrecedent(dateFin);
    }
    
    // Initialiser données détail
    magasinSortieDetail = null;
    representantDetail = null;
    acheteurDetail = null;
    fournisseurDetail = null;
  }
  
  @Override
  public void chargerDonnees() {
    // Charger données pour affichage liste
    // Charger l'utilisateur
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    // Charger la liste des magasins
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
      listeMagasin = listeMagasin.charger(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    }
    
    // Charger le magasin courant
    if (idMagasin == null) {
      idMagasin = utilisateurGescom.getIdMagasin();
    }
    
    // Charger l'établissement courant
    if (etablissement == null) {
      listeEtablissement = ListeEtablissement.charger(getIdSession());
      etablissement = listeEtablissement.getEtablissementParId(utilisateurGescom.getIdEtablissementPrincipal());
      chargerDonneesLieesEtablissement(etablissement.getId());
    }
    
    // Charger la liste des fournisseurs
    if (listeFournisseurBase == null) {
      listeFournisseurBase = new ListeFournisseurBase();
      listeFournisseurBase = listeFournisseurBase.charger(getIdSession(), etablissement.getId());
    }
    // Charger données pour affichage détail
    // Lister les unités
    if (listeUniteDetail == null) {
      listeUniteDetail = new ListeUnite();
      listeUniteDetail = listeUniteDetail.charger(getIdSession());
    }
    
    // Récupération de la liste des modes d'expédition
    if (listeModesExpeditionDetail == null) {
      listeModesExpeditionDetail = ListeModeExpedition.charger(getIdSession());
      if (listeModesExpeditionDetail == null) {
        throw new MessageErreurException("Aucun mode d'expédition n'a été trouvé dans la bibliothèque courante.");
      }
    }
  }
  
  /**
   * Fermer la boîte de dialogue en validant les modifications.
   * Les données modifiées sont conservées.
   */
  public void quitterAvecValidation() {
    getSession().fermerEcran();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  // Méthodes pour l'écran de détail
  
  /**
   * Construit le titre de l'écran.
   */
  public void construireTitreDetail() {
    if (documentAchatDetail == null) {
      IdDocumentAchat idDocument = null;
      if (idDocumentSelectionne != null) {
        idDocument = idDocumentSelectionne;
      }
      else {
        idDocument = idLigneAchatSelectionnee.getIdDocumentAchat();
      }
      if (idDocument == null) {
        return;
      }
      documentAchatDetail = new DocumentAchat(idDocument);
    }
    
    String nomTypeDocument = TYPE_DOCUMENT_TRANSFERT;
    if (documentAchatDetail.isCommande()) {
      nomTypeDocument = TYPE_DOCUMENT_COMMANDE;
    }
    else if (documentAchatDetail.isFacture()) {
      nomTypeDocument = TYPE_DOCUMENT_FACTURE;
    }
    else if (documentAchatDetail.isReception()) {
      nomTypeDocument = TYPE_DOCUMENT_RECEPTION;
    }
    
    String titre = nomTypeDocument + " d'achats " + documentAchatDetail.getId();
    if (documentAchatDetail.getCodeEtat() != null) {
      titre += " (" + documentAchatDetail.getCodeEtat().getLibelle() + ")";
    }
    setTitreEcran(titre);
  }
  
  /**
   * Retourne la quantité restant à prendre.
   */
  public BigDecimal retournerQuantiteRestanteDetail(LigneVente pLigne) {
    if (pLigne != null) {
      if (pLigne.isLigneCommentaire()) {
        if (pLigne.getTopPersonnalisation5().isEmpty()) {
          return BigDecimal.ZERO;
        }
        return BigDecimal.ONE;
      }
      // La valeur n'est pas juste dans certains cas comme avecc les plaques de platres (problème d'arrondi)
      BigDecimal reste = pLigne.getQuantiteDocumentOrigineUC().subtract(pLigne.getQuantiteDejaExtraiteUC());
      if (reste.compareTo(pLigne.getQuantiteDocumentOrigineUC()) < 0) {
        return reste;
      }
    }
    return BigDecimal.ZERO;
  }
  
  /**
   * Lecture des lignes du document.
   */
  public void lireLignesDocumentDetail(DocumentAchat pDocumentAchat) {
    if (pDocumentAchat == null) {
      return;
    }
    // On vide les listes
    pDocumentAchat.viderListes();
    CritereLigneAchat criteres = new CritereLigneAchat();
    boolean rechercher = true;
    
    // On récupère toutes les lignes du documents (en plusieurs appels du service si nécessaire)
    do {
      criteres.setLignesOrigine(true);
      criteres.setIdDocumentAchat(pDocumentAchat.getId());
      ListeLigneAchat listeLigneAchat = ManagerServiceDocumentAchat.chargerListeLigneAchat(getIdSession(), criteres);
      
      // On ajoute les lignes lues dans la classe Document
      for (LigneAchat ligne : listeLigneAchat) {
        // On vérifie si l'on a parcouru tous les articles du document
        if (ligne == null) {
          rechercher = false;
          break;
        }
        
        // En fonction de son état on stocke la ligne dans la liste qui va bien
        if (ligne.isAnnulee()) {
          pDocumentAchat.getListeLigneAchatSupprimee().add(ligne);
        }
        else {
          pDocumentAchat.getListeLigneAchat().add(ligne);
        }
      }
      if (listeLigneAchat.size() < NOMBRE_LIGNES_ARTICLES_DETAIL) {
        rechercher = false;
      }
    }
    while (rechercher);
  }
  
  /**
   * Afficher le détail de la ligne sélectionnée.
   */
  public void afficherDetailLigne(int pIndexLigne) {
    if (documentAchatDetail == null || pIndexLigne < 0) {
      return;
    }
    
    // Récupération de la ligne du document à partir de son index du tableau
    LigneAchat ligneAchat = documentAchatDetail.getListeLigneAchat().get(pIndexLigne);
    
    // Il n'y a rien à afficher S'il s'agit d'une ligne commentaire
    if (ligneAchat == null || ligneAchat.isLigneCommentaire()) {
      return;
    }
    
    // Chargement des données de la ligne sélectionnée
    ligneAchatSelectionneeDetail = (LigneAchatArticle) retournerLigneSelectionneeDetail(ligneAchat.getId());
    if (ligneAchatSelectionneeDetail == null) {
      return;
    }
    
    // Affichage de la boite de dialogue du détail d'une ligne
    ModeleDetailLigneAchat modele = new ModeleDetailLigneAchat(getSession(), this, ligneAchatSelectionneeDetail);
    VueDetailLigneAchat vue = new VueDetailLigneAchat(modele);
    vue.afficher();
  }
  
  /**
   * Permutter l'affichage de la table.
   */
  public void permuterAffichageTableDetail() {
    if (modeAffichageTableDetail == MODE_AFFICHAGE_DETAIL_TABLE_STANDARD) {
      modeAffichageTableDetail = MODE_AFFICHAGE_DETAIL_TABLE_DETAIL;
    }
    else {
      modeAffichageTableDetail = MODE_AFFICHAGE_DETAIL_TABLE_STANDARD;
    }
    rafraichir();
  }
  
  /**
   * Charger le document qui précède le document affiché dans la liste des documents trouvés
   */
  public void afficherDocumentPrecedentDetail() {
    if (isPremierDocumentDetail()) {
      return;
    }
    if (indexLigneSelectionne < 0) {
      setIndexDocumentSelectionne(indexDocumentSelectionne - 1);
    }
    else {
      setIndexLigneSelectionne(indexLigneSelectionne - 1);
    }
    chargerDonnees();
    chargerDocumentSelectionne();
    rafraichir();
  }
  
  /**
   * Charger le document qui suit le document affiché dans la liste des documents trouvés
   */
  public void afficherDocumentSuivantDetail() {
    if (isDernierDocumentDetail()) {
      return;
    }
    if (indexLigneSelectionne < 0) {
      setIndexDocumentSelectionne(indexDocumentSelectionne + 1);
    }
    else {
      setIndexLigneSelectionne(indexLigneSelectionne + 1);
    }
    chargerDonnees();
    chargerDocumentSelectionne();
    rafraichir();
  }
  
  /**
   * Retourner à l'écran de recherche.
   */
  public void retournerRecherche() {
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    rafraichir();
  }
  
  /**
   * Retourne la précision d'une unité donnée.
   */
  public int retournerPrecisionUniteDetail(IdUnite pId) {
    return listeUniteDetail.getPrecisionUnite(pId);
  }
  
  // -- Méthodes publiques
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  private void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), etablissement.getId());
    
    // Récupération de la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), pIdEtablissement);
    
    // Controler la validité du code magasin associé à l'utilisateur.
    idMagasin = utilisateurGescom.getIdMagasin();
    if (idMagasin == null || idMagasin.getCode().isEmpty() || listeMagasin.getMagasinParId(idMagasin) == null) {
      throw new MessageErreurException("Il n'est pas possible de choisir cet établissement car il n'a pas de magasin associé défini");
    }
    
    // Charger la liste des fournisseurs
    if (listeFournisseurBase == null) {
      listeFournisseurBase = new ListeFournisseurBase();
    }
    listeFournisseurBase = listeFournisseurBase.charger(getIdSession(), etablissement.getId());
    
  }
  
  /**
   * Recherche documents
   */
  public void rechercherDocument() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeDocumentAchat != null) {
      taillePage = listeDocumentAchat.getTaillePage();
    }
    
    // Renseigner les critères de recherche
    criteresDocument.setIdEtablissement(etablissement.getId());
    criteresDocument.initialiserNombrePagesMax();
    criteresDocument.setNbrLignesParPage(NOMBRE_LIGNES_DOCUMENTS_LISTE);
    criteresDocument.setNumeroDocument(numeroDocument);
    
    if (idArticle != null) {
      criteresDocument.setCodeArticle(idArticle.getCodeArticle());
    }
    else {
      criteresDocument.setCodeArticle("");
    }
    criteresDocument.setDateCreationFin(dateFin);
    criteresDocument.setDateCreationDebut(dateDebut);
    criteresDocument.setIdMagasin(idMagasin);
    criteresDocument.setTexteRechercheGenerique(referenceDocument);
    criteresDocument.setAvecDocumentAchatEntierementReceptionne(true);
    if (adresseFournisseurSelectionne != null) {
      criteresDocument.setIdFournisseur(adresseFournisseurSelectionne.getId().getIdFournisseur());
    }
    
    // Charger la liste des identifiants des documents d'achat correspondant au résultat de la recherche
    listeIdDocumentAchat = ManagerServiceDocumentAchat.chargerListeIdDocumentAchat(getIdSession(), criteresDocument);
    
    listeDocumentAchat = ListeDocumentAchat.creerListeNonChargee(listeIdDocumentAchat);
    
    listeDocumentAchat.chargerPremierePage(getIdSession(), taillePage);
    construireTitre();
    rafraichir();
  }
  
  /**
   * Recherche lignes
   */
  public void rechercherLigne() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeLigneAchat != null) {
      taillePage = listeLigneAchat.getTaillePage();
    }
    
    // Renseigner les critères de recherche
    criteresLigne.setIdEtablissement(etablissement.getId());
    criteresLigne.setIdMagasin(idMagasin);
    criteresLigne.setDateCreationDebut(dateDebut);
    criteresLigne.setDateCreationFin(dateFin);
    criteresLigne.setNumeroDocumentAchat(numeroDocument);
    if (idArticle != null) {
      criteresLigne.setIdArticle(idArticle);
    }
    else {
      criteresLigne.setIdArticle(null);
    }
    criteresLigne.setTexteRechercheGenerique(referenceDocument);
    
    if (adresseFournisseurSelectionne != null) {
      criteresLigne.setIdFournisseur(adresseFournisseurSelectionne.getId().getIdFournisseur());
    }
    criteresLigne.setExclureLigneCommentaire(true);
    
    // Charger la liste des identifiants des documents d'achat correspondant au résultat de la recherche
    listeIdLigneAchat = ManagerServiceDocumentAchat.chargerListeIdLignesAchat(getIdSession(), criteresLigne);
    
    listeLigneAchat = ListeLigneAchatBase.creerListeNonChargee(listeIdLigneAchat);
    
    listeLigneAchat.chargerPremierePage(getIdSession(), taillePage);
    
    construireTitre();
    rafraichir();
  }
  
  /**
   * Construction du titre de la liste suivant le résultat de recherche
   */
  private void construireTitre() {
    if (isModeAffichageDocument()) {
      if (listeDocumentAchat == null) {
        titreResultat = Message.getMessageNormal("Documents d'achat correspondant à votre recherche");
      }
      else if (listeDocumentAchat.size() == 1) {
        titreResultat = Message.getMessageNormal("Document d'achat correspondant à votre recherche (1)");
      }
      else if (listeDocumentAchat.size() > 1) {
        titreResultat = Message.getMessageNormal("Documents d'achat correspondant à votre recherche (" + listeDocumentAchat.size() + ")");
      }
      else {
        titreResultat = Message.getMessageImportant("Aucun document d'achat ne correspond à votre recherche");
      }
    }
    else {
      if (listeLigneAchat == null) {
        titreResultat = Message.getMessageNormal("Lignes d'achat correspondant à votre recherche");
      }
      else if (listeLigneAchat.size() == 1) {
        titreResultat = Message.getMessageNormal("Ligne d'achat correspondant à votre recherche (1)");
      }
      else if (listeLigneAchat.size() > 1) {
        titreResultat = Message.getMessageNormal("Lignes d'achat correspondant à votre recherche (" + listeLigneAchat.size() + ")");
      }
      else {
        titreResultat = Message.getMessageImportant("Aucune ligne d'achat ne correspond à votre recherche");
      }
    }
  }
  
  public void initialiserRecherche() {
    // Effacer filtres
    numeroDocument = 0;
    referenceDocument = "";
    
    adresseFournisseurSelectionne = null;
    texteArticleRecherche = "";
    typeDocument = null;
    idArticle = null;
    dateFin = new Date();
    dateDebut = Constantes.getDateDebutMoisPrecedent(dateFin);
    
    // Effacer les données
    listeDocumentAchat = null;
    idArticle = null;
    
    listeIdDocumentAchat = null;
    listeIdLigneAchat = null;
    
    // Effacer les critères de recherche
    criteresDocument = new CritereDocumentAchat();
    criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
    criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.RECEPTION);
    criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.FACTURE);
    
    criteresLigne = new CritereLigneAchat();
    
    listeDocumentAchat = null;
    listeLigneAchat = null;
    
    construireTitre();
    rafraichir();
  }
  
  /**
   * Modifie le magasin des documents à rechercher selon le choix de la combo.
   */
  public void modifierMagasin(int pIndex) {
    idMagasin = listeMagasin.get(pIndex).getId();
    rafraichir();
  }
  
  /**
   * Modifie l'établissement des documents à rechercher selon le choix de la combo.
   */
  public void modifierEtablissement(Etablissement pEtablissement) {
    if (pEtablissement == null || Constantes.equals(etablissement, pEtablissement)) {
      return;
    }
    etablissement = pEtablissement;
    chargerDonneesLieesEtablissement(etablissement.getId());
    
    // La méthode rafraichir() est appelée par initialiserRecherche
    initialiserRecherche();
  }
  
  /**
   * Modifie le numéro du document à rechercher.
   */
  public void modifierNumeroDocument(String pNumeroDocument) {
    // Convertir la saisie en nombre
    int numero = 0;
    
    try {
      pNumeroDocument = Constantes.normerTexte(pNumeroDocument);
      if (!pNumeroDocument.isEmpty()) {
        if (pNumeroDocument.length() > 7) {
          throw new MessageErreurException("Le numéro saisi est trop long");
        }
        else {
          numero = Integer.parseInt(pNumeroDocument);
        }
      }
    }
    catch (NumberFormatException e) {
      numero = 0;
    }
    
    if (numeroDocument == numero) {
      return;
    }
    
    numeroDocument = numero;
    
    if (numero > 0) {
      rechercherDocument();
    }
    rafraichir();
  }
  
  /**
   * Modifie l'article à rechercher.
   */
  public void modifierArticle(IdArticle pIdArticle) {
    if (Constantes.equals(pIdArticle, idArticle)) {
      return;
    }
    idArticle = pIdArticle;
    
    rafraichir();
  }
  
  /**
   * Modifie le fournisseur à rechercher
   */
  public void modifierFournisseur(AdresseFournisseur pAdresseFournisseur) {
    if (Constantes.equals(adresseFournisseurSelectionne, pAdresseFournisseur)) {
      return;
    }
    
    adresseFournisseurSelectionne = pAdresseFournisseur;
    rafraichir();
  }
  
  /**
   * Modifie la référence à rechercher
   */
  public void modifierReferenceDocument(String pReferenceClientDocument) {
    pReferenceClientDocument = Constantes.normerTexte(pReferenceClientDocument);
    if (Constantes.equals(referenceDocument, pReferenceClientDocument)) {
      return;
    }
    referenceDocument = pReferenceClientDocument;
    rafraichir();
  }
  
  /**
   * Modifier la date de début.
   */
  public void modifierDateCreationDebut(Date pDate) {
    if ((pDate == null) || Constantes.equals(pDate, dateDebut)) {
      return;
    }
    dateDebut = pDate;
  }
  
  /**
   * Modifier la date de fin.
   */
  public void modifierDateCreationFin(Date pDate) {
    if ((pDate == null) || Constantes.equals(pDate, dateFin)) {
      return;
    }
    dateFin = pDate;
  }
  
  /**
   * Modifier le type d'affichage de la liste de résultats
   */
  public void modifierModeAffichage(int pModeAffichage) {
    if (modeAffichage == pModeAffichage) {
      return;
    }
    modeAffichage = pModeAffichage;
    nombreLignesArticleTrouvees = 0;
    calculerNombreLignesTrouvees();
    rafraichir();
  }
  
  /**
   * Modifie le type de document à afficher.
   */
  public void modifierTypeDocument(EnumTypeDocumentAchat pTypeDocumentAchat) {
    criteresDocument.viderListeTypeDocument();
    criteresLigne.viderListeTypeDocument();
    typeDocument = pTypeDocumentAchat;
    if (pTypeDocumentAchat != null) {
      criteresDocument.ajouterTypeDocumentAchat(pTypeDocumentAchat);
      criteresLigne.ajouterTypeDocumentAchat(pTypeDocumentAchat);
    }
    else {
      criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
      criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.RECEPTION);
      criteresDocument.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.FACTURE);
      criteresLigne.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
      criteresLigne.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.RECEPTION);
      criteresLigne.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.FACTURE);
    }
  }
  
  /**
   * Modifier le document sélectionné par l'utilisateur
   */
  public void modifierDocumentSelectionne(int pIndexDocumentSelectionne) {
    setIndexDocumentSelectionne(pIndexDocumentSelectionne);
    rafraichir();
  }
  
  /**
   * Modifier la ligne sélectionnée par l'utilisateur
   */
  public void modifierLigneSelectionne(int pIndexLigneSelectionne) {
    if (pIndexLigneSelectionne == indexDocumentSelectionne) {
      return;
    }
    
    setIndexLigneSelectionne(pIndexLigneSelectionne);
    rafraichir();
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageDocumentsAffiches(int pPremiereLigne, int pDerniereLigne) {
    // Charger les données visibles
    if (listeDocumentAchat != null) {
      listeDocumentAchat.chargerPage(getIdSession(), pPremiereLigne, pDerniereLigne);
    }
    
    // Charger les données des articles visibles
    // chargerPlageDocuments(pPremiereLigne, pDerniereLigne);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Afficher la plage de lignes comprise entre deux lignes.
   * Les informations des lignes sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageLigneAffiches(int pPremiereLigne, int pDerniereLigne) {
    // Charger les données visibles
    if (listeLigneAchat != null) {
      listeLigneAchat.chargerPage(getIdSession(), pPremiereLigne, pDerniereLigne);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Charge les lignes du document
   */
  public void chargerLignesArticlesDocument() {
    calculerNombreLignesTrouvees();
    if (listeDocumentAchat != null && !listeDocumentAchat.isEmpty()) {
      for (int i = 0; i < listeDocumentAchat.size(); i++) {
        DocumentAchat documentAchat = listeDocumentAchat.get(i);
        if (documentAchat.isCharge() && (documentAchat.getListeLigneAchat() == null || documentAchat.getListeLigneAchat().isEmpty())) {
          lireLignesDocument(documentAchat);
        }
      }
    }
  }
  
  /**
   * Lecture des lignes du document pour le code article donné
   */
  private void lireLignesDocument(DocumentAchat pDocumentAchat) {
    if (pDocumentAchat == null || idArticle == null || etablissement == null) {
      return;
    }
    ListeLigneAchat listeLigneAchat = new ListeLigneAchat();
    
    CritereLigneAchat criteresLigne = new CritereLigneAchat();
    criteresLigne.setIdDocumentAchat(pDocumentAchat.getId());
    criteresLigne.setIdEtablissement(etablissement.getId());
    criteresLigne.setIdArticle(idArticle);
    
    List<IdLigneAchat> listeIdLigne = ManagerServiceDocumentAchat.chargerListeIdLignesAchat(getIdSession(), criteresLigne);
    listeLigneAchat = listeLigneAchat.charger(getIdSession(), listeIdLigne);
    
    pDocumentAchat.setListeLignesArticles(listeLigneAchat);
  }
  
  /**
   * Calculerle nombre de lignes contenant l'article
   */
  private void calculerNombreLignesTrouvees() {
    if (listeDocumentAchat == null) {
      nombreLignesArticleTrouvees = 0;
      return;
    }
    nombreLignesArticleTrouvees = 0;
    for (DocumentAchat documentAchat : listeDocumentAchat) {
      if (documentAchat != null && etablissement != null && idArticle != null) {
        CritereLigneAchat criteresLigne = new CritereLigneAchat();
        criteresLigne.setIdEtablissement(etablissement.getId());
        criteresLigne.setIdArticle(idArticle);
        List<IdLigneAchat> listeIdLigne = ManagerServiceDocumentAchat.chargerListeIdLignesAchat(getIdSession(), criteresLigne);
        if (listeIdLigne != null) {
          nombreLignesArticleTrouvees += listeIdLigne.size();
        }
      }
    }
    
  }
  
  /**
   * Afficher le détail du document sélectionné
   */
  public void afficherDetailDocument() {
    if (indexDocumentSelectionne < 0) {
      throw new MessageErreurException("Aucun documents d'achat n'est sélectionné.");
    }
    chargerDocumentSelectionne();
    idLigneAchatSelectionnee = null;
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    rafraichir();
  }
  
  /**
   * Afficher le détail du document correspondant à la ligne sélectionnée
   */
  public void afficherDetailLigne() {
    if (indexLigneSelectionne < 0) {
      throw new MessageErreurException("Aucun documents d'achat n'est sélectionné.");
    }
    chargerDocumentSelectionne();
    idDocumentSelectionne = null;
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    rafraichir();
  }
  
  /**
   * Modifier le type de recherche : mode documents ou mode lignes
   */
  public void modifierModeRecherche(int pMode) {
    modeAffichage = pMode;
    construireTitre();
    rafraichir();
  }
  
  // -- Accesseurs
  
  // Accesseurs globaux
  /**
   * Liste des documents d'achats retournée par la recherche.
   */
  public List<IdDocumentAchat> getListeIdDocumentAchat() {
    return listeIdDocumentAchat;
  }
  
  /**
   * Modifier la liste des documents d'achat retournée par la recherche.
   */
  public void setListeIdDocumentAchat(List<IdDocumentAchat> pListeIdDocument) {
    listeIdDocumentAchat = pListeIdDocument;
  }
  
  public List<IdLigneAchat> getListeIdLigneAchat() {
    return listeIdLigneAchat;
  }
  
  /**
   * Modifier la liste des lignes d'achat retournée par la recherche.
   */
  public void setListeIdLigneAchat(List<IdLigneAchat> pListeIdLigneAchat) {
    listeIdLigneAchat = pListeIdLigneAchat;
  }
  
  /**
   * Indice du document d'achat sélectionné.
   */
  public int getIndexDocumentSelectionne() {
    return indexDocumentSelectionne;
  }
  
  /**
   * Modifier l'indice du document d'achat sélectionné.
   */
  public void setIndexDocumentSelectionne(int pIndexDocumentSelectionne) {
    if (pIndexDocumentSelectionne < 0 || pIndexDocumentSelectionne >= listeIdDocumentAchat.size()) {
      indexDocumentSelectionne = -1;
    }
    indexLigneSelectionne = -1;
    indexDocumentSelectionne = pIndexDocumentSelectionne;
    idDocumentSelectionne = listeIdDocumentAchat.get(indexDocumentSelectionne);
  }
  
  public int getIndexLigneSelectionne() {
    return indexLigneSelectionne;
  }
  
  public void setIndexLigneSelectionne(int pIndexLigneSelectionne) {
    if (pIndexLigneSelectionne < 0 || pIndexLigneSelectionne >= listeIdLigneAchat.size()) {
      indexDocumentSelectionne = -1;
    }
    indexDocumentSelectionne = -1;
    indexLigneSelectionne = pIndexLigneSelectionne;
    idLigneAchatSelectionnee = listeIdLigneAchat.get(indexLigneSelectionne);
  }
  
  /**
   * Identifiant du document d'achat sélectionné.
   */
  public IdDocumentAchat getIdDocumentAchatSelectionne() {
    if (listeIdDocumentAchat == null || indexDocumentSelectionne < 0 || indexDocumentSelectionne >= listeIdDocumentAchat.size()) {
      return null;
    }
    idDocumentSelectionne = listeIdDocumentAchat.get(indexDocumentSelectionne);
    return idDocumentSelectionne;
  }
  
  /**
   * Identifiant de la ligne d'achat sélectionné.
   */
  public IdLigneAchat getIdLigneAchatSelectionne() {
    if (listeIdLigneAchat == null || indexLigneSelectionne < 0 || indexLigneSelectionne >= listeIdLigneAchat.size()) {
      return null;
    }
    
    idLigneAchatSelectionnee = listeIdLigneAchat.get(indexLigneSelectionne);
    return idLigneAchatSelectionnee;
  }
  
  /**
   * Mettre à jour l'identifiant du document sélectionné
   */
  public void setIdDocumentSelectionne(IdDocumentAchat pIdDocumentSelectionne) {
    idDocumentSelectionne = pIdDocumentSelectionne;
  }
  
  /**
   * Mettre à jour l'identifiant de la ligne sélectionnée
   */
  public void setIdLigneAchatSelectionnee(IdLigneAchat pIdLigneAchatSelectionnee) {
    idLigneAchatSelectionnee = pIdLigneAchatSelectionnee;
  }
  
  // Accesseurs pour l'écran de détail
  
  public DocumentAchat getDocumentAchatDetail() {
    return documentAchatDetail;
  }
  
  /**
   * Magasin de sortie associé au document d'achat.
   */
  public Magasin getMagasinSortieDetail() {
    return magasinSortieDetail;
  }
  
  /**
   * Représentant associé au document d'achat.
   */
  public Representant getRepresentantDetail() {
    return representantDetail;
  }
  
  /**
   * Vendeur associé au document d'achat.
   */
  public Acheteur getAcheteurDetail() {
    return acheteurDetail;
  }
  
  /**
   * Fournisseur du document.
   */
  public Fournisseur getFournisseurDetail() {
    return fournisseurDetail;
  }
  
  public ListeUnite getListeUniteDetail() {
    return listeUniteDetail;
  }
  
  /**
   * Mode d'affichage de la table contenant les lignes du document.
   */
  public int getModeAffichageTableDetail() {
    return modeAffichageTableDetail;
  }
  
  /**
   * Indique si c'est le premier document de la liste.
   */
  public boolean isPremierDocumentDetail() {
    if (listeIdDocumentAchat != null) {
      return indexDocumentSelectionne == 0;
    }
    else {
      return indexLigneSelectionne == 0;
    }
  }
  
  /**
   * Indique si c'est le dernier document de la liste.
   */
  public boolean isDernierDocumentDetail() {
    if (listeIdDocumentAchat != null) {
      return indexDocumentSelectionne >= listeIdDocumentAchat.size() - 1;
    }
    else {
      return indexLigneSelectionne >= listeIdLigneAchat.size() - 1;
    }
  }
  
  /**
   * Nombre de résultats retournés par la recherche.
   */
  public int getNombreResultat() {
    if (listeIdDocumentAchat == null) {
      return 0;
    }
    return listeIdDocumentAchat.size();
  }
  
  /**
   * Message d'information.
   */
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public int getNumeroDocument() {
    return numeroDocument;
  }
  
  public Date getDateDebut() {
    return dateDebut;
  }
  
  public Date getDateFin() {
    return dateFin;
  }
  
  public EnumTypeDocumentAchat getTypeDocument() {
    return typeDocument;
  }
  
  public String getCodeArticle() {
    if (idArticle == null) {
      return "";
    }
    return idArticle.getCodeArticle();
  }
  
  public void setIndexRechercheDocument(int selectedRow) {
    indexRechercheDocument = selectedRow;
  }
  
  public void setIndexRechercheLigne(int selectedRow) {
    indexRechercheLigne = selectedRow;
  }
  
  public ListeDocumentAchat getListeDocumentAchat() {
    return listeDocumentAchat;
  }
  
  public ListeLigneAchatBase getListeLigneAchat() {
    return listeLigneAchat;
  }
  
  public String getTexteArticleRecherche() {
    return texteArticleRecherche;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public int getNombreLignesArticleTrouvees() {
    return nombreLignesArticleTrouvees;
  }
  
  public boolean isModeAffichageDocument() {
    return modeAffichage == MODE_AFFICHAGE_DOCUMENT_LISTE;
  }
  
  public boolean isModeAffichageLigne() {
    return modeAffichage == MODE_AFFICHAGE_LIGNE_LISTE;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public String getReferenceClientDocument() {
    return referenceDocument;
  }
  
  public ListeFournisseurBase getListeFournisseurBase() {
    return listeFournisseurBase;
  }
  
  public String getNomFournisseur(IdFournisseur pIdFournisseur) {
    if (listeFournisseurBase == null || listeFournisseurBase.get(pIdFournisseur) == null) {
      return "";
    }
    return listeFournisseurBase.get(pIdFournisseur).toString();
  }
  
  /**
   * Fournisseur.
   */
  public AdresseFournisseur getAdresseFournisseur() {
    return adresseFournisseurSelectionne;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public boolean isEcranDetail() {
    return Constantes.equals(getCleVueEnfantActive(), EnumCleVueListeDetail.DETAIL);
  }
  
  // -- Méthodes privées
  
  private void chargerDocumentSelectionne() {
    // Récupérer l'id du document d'achat à charger
    IdDocumentAchat idDocument = null;
    if (idDocumentSelectionne != null) {
      idDocument = idDocumentSelectionne;
    }
    else {
      idDocument = idLigneAchatSelectionnee.getIdDocumentAchat();
    }
    
    if (idDocument == null) {
      throw new MessageErreurException("Aucun document d'achat n'est sélectionné.");
    }
    
    // Charger le document d'achat
    documentAchatDetail = chargerDocumentDetail(idDocument);
    if (documentAchatDetail == null) {
      throw new MessageErreurException("Impossible de charger le document d'achats " + idDocument);
    }
    
    // Charger le magasin
    if (listeMagasinDetail == null) {
      listeMagasinDetail = new ListeMagasin();
      listeMagasinDetail = listeMagasinDetail.charger(getIdSession(), documentAchatDetail.getId().getIdEtablissement());
    }
    if (documentAchatDetail.getIdMagasinSortie() != null || listeMagasinDetail != null) {
      magasinSortieDetail = listeMagasinDetail.getMagasinParId(documentAchatDetail.getIdMagasinSortie());
    }
    
    // Charger le représentant
    if (listeRepresentantDetail == null) {
      listeRepresentantDetail = new ListeRepresentant();
      listeRepresentantDetail = listeRepresentantDetail.charger(getIdSession(), documentAchatDetail.getId().getIdEtablissement());
    }
    // representant = listeRepresentant.retournerRepresentantParId(documentAchat.getIdRepresentant1());
    
    // Charger le vendeur
    if (listeAcheteurDetail == null) {
      listeAcheteurDetail = new ListeAcheteur();
      listeAcheteurDetail = listeAcheteurDetail.charger(getIdSession(), documentAchatDetail.getId().getIdEtablissement());
    }
    acheteurDetail = listeAcheteurDetail.retournerAcheteurParId(documentAchatDetail.getIdAcheteur());
    
    // Charger le fournisseur
    fournisseurDetail = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), documentAchatDetail.getIdFournisseur());
  }
  
  /**
   * Charge toutes les informations relatives à un document.
   */
  private DocumentAchat chargerDocumentDetail(IdDocumentAchat pIdDocumentAchat) {
    if (pIdDocumentAchat == null) {
      throw new MessageErreurException("Document d'achat invalide.");
    }
    // Lecture de l'entête
    DocumentAchat documentAchat = ManagerServiceDocumentAchat.chargerDocumentAchat(getIdSession(), pIdDocumentAchat, null, true);
    
    // Lecture des lignes
    lireLignesDocumentDetail(documentAchat);
    
    return documentAchat;
  }
  
  /**
   * Récupère les données d'une ligne à partir des lignes chargées du document d'achat.
   */
  private LigneAchat retournerLigneSelectionneeDetail(IdLigneAchat pIdLigne) {
    for (LigneAchat ligne : documentAchatDetail.getListeLigneAchat()) {
      if (Constantes.equals(ligne.getId(), pIdLigne)) {
        ligneAchatSelectionneeDetail = (LigneAchatArticle) ligne;
        return ligneAchatSelectionneeDetail;
      }
    }
    return null;
  }
}
