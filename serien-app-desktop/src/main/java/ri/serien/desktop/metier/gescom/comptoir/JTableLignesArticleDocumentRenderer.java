/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;

public class JTableLignesArticleDocumentRenderer extends DefaultTableCellRenderer {
  // Variables
  private ListeLigneVente listeLigneVente;
  
  // -- Méthodes publiques
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component cellule = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Justification des colonnes
    if (table.getModel().getColumnCount() == 10) {
      justifierColonnesNormal(cellule, column);
    }
    else {
      justifierColonnesEnModif(cellule, column);
    }
    
    return cellule;
  }
  
  // -- Méthodes privées
  
  /**
   * Justification pour lorsque le document est en création.
   */
  private void justifierColonnesNormal(Component pCellule, int pColonne) {
    JLabel label = (JLabel) pCellule;
    switch (pColonne) {
      case 0:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 1:
        label.setHorizontalAlignment(CENTER);
        break;
      case 2:
        label.setHorizontalAlignment(LEFT);
        break;
      case 3:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 4:
        label.setHorizontalAlignment(CENTER);
        break;
      case 5:
        label.setHorizontalAlignment(CENTER);
        break;
      case 6:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 7:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 8:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 9:
        label.setHorizontalAlignment(RIGHT);
        break;
    }
  }
  
  /**
   * Justification pour lorsque le document est en modification ou extraction.
   */
  private void justifierColonnesEnModif(Component pCellule, int pColonne) {
    JLabel label = (JLabel) pCellule;
    switch (pColonne) {
      case 0:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 1:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 2:
        label.setHorizontalAlignment(CENTER);
        break;
      case 3:
        label.setHorizontalAlignment(LEFT);
        break;
      case 4:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 5:
        label.setHorizontalAlignment(CENTER);
        break;
      case 6:
        label.setHorizontalAlignment(CENTER);
        break;
      case 7:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 8:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 9:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 10:
        label.setHorizontalAlignment(RIGHT);
        break;
    }
  }
  
  // -- Accesseurs
  
  public void setListeLigneVente(ListeLigneVente pListeLigneVente) {
    this.listeLigneVente = pListeLigneVente;
  }
  
}
