/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.regroupement;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.gescom.vente.negociation.NegociationVenteGlobale;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * @author Stéphane Vénéri
 */
public class VueRegroupementLignes extends AbstractVueDialogue<ModeleRegroupementLignes> {
  // Constantes
  private static final String BOUTON_DEGROUPER = "Dégrouper";
  private static final String BOUTON_AFFICHER_TTC = "Afficher en TTC";
  private static final String BOUTON_AFFICHER_HT = "Afficher en HT";
  private static final String BOUTON_MASQUER_DETAIL = "Masquer détails";
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détails";
  private static final String LIBELLE_TOTAL_TTC = "Total public TTC";
  private static final String LIBELLE_TOTAL_HT = "Total public HT";
  private static final String LIBELLE_TOTAL_NET_TTC = "Total net TTC";
  private static final String LIBELLE_TOTAL_NET_HT = "Total net HT";
  
  // Variables
  private boolean valeurEstSaisie = false;
  
  /**
   * Constructeur.
   */
  public VueRegroupementLignes(ModeleRegroupementLignes pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(980, 430);
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Configurer la barre de boutons
    snBarreBoutonGeneral.ajouterBouton(BOUTON_DEGROUPER, 'g', true);
    snBarreBoutonGeneral.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBoutonGeneral.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBoutonGeneral.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBoutonNegociation.ajouterBouton(BOUTON_AFFICHER_TTC, 'a', false);
    snBarreBoutonNegociation.ajouterBouton(BOUTON_AFFICHER_HT, 'a', false);
    snBarreBoutonNegociation.ajouterBouton(BOUTON_MASQUER_DETAIL, 'd', false);
    snBarreBoutonNegociation.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', false);
    snBarreBoutonNegociation.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBoutonNegociation.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBoutonNegociation.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir les données de l'onglet général.
    rafraichirTexteTitre();
    rafraichirTexteTotal();
    rafraichirChoixDetailArticle();
    rafraichirChoixEditionPrix();
    
    // Rafraîchir le prix initial
    rafraichirTotalPublicInitial();
    rafraichirTotalNetInitial();
    rafraichirRemiseInitial();
    rafraichirDetailInitial();
    
    // Rafraîchir le prix négocié
    rafraichirTotalPublic();
    rafraichirTotalNet();
    rafraichirRemise();
    rafraichirDetail();
    
    // Rafraîchir les boutons
    rafraichirBoutonTTC();
    rafraichirBoutonHT();
    rafraichirBoutonMasquerDetail();
    rafraichirBoutonAfficherDetail();
    rafraichirBoutonValider();
    rafraichirBoutonDegrouper();
    
    // Positionnement du focus
    switch (getModele().getComposantAyantLeFocus()) {
      case ModeleRegroupementLignes.FOCUS_GENERAL_TITRE:
        tfTexteTitre.requestFocus();
        break;
      case ModeleRegroupementLignes.FOCUS_NEGO_VENTE_REMISE:
        tfRemise.requestFocus();
        break;
    }
  }
  
  /**
   * Rafraîchir le bouton TTC
   */
  private void rafraichirBoutonTTC() {
    boolean actif = !getModele().isAffichageTTC();
    // Le bouton "Afficher en TTC" ne doit pas être affiché si le client est facturé en HT (car le calcul de prix en Java ne calcule pas
    // les prix TTC en mode HT et que cela n'a pas vraiment de sens)
    if (getModele().getDocumentVente() == null || !getModele().getDocumentVente().isTTC()) {
      actif = false;
    }
    snBarreBoutonNegociation.activerBouton(BOUTON_AFFICHER_TTC, actif);
  }
  
  /**
   * Rafraîchir le bouton HT
   */
  private void rafraichirBoutonHT() {
    boolean actif = getModele().isAffichageTTC();
    snBarreBoutonNegociation.activerBouton(BOUTON_AFFICHER_HT, actif);
  }
  
  /**
   * Rafraîchir le bouton Masquer le détail
   */
  private void rafraichirBoutonMasquerDetail() {
    boolean actif = getModele().isAfficherDetails() && getModele().isAutoriseVisualiserMarge();
    snBarreBoutonNegociation.activerBouton(BOUTON_MASQUER_DETAIL, actif);
  }
  
  /**
   * Rafraîchir le bouton Afficher le détail
   */
  private void rafraichirBoutonAfficherDetail() {
    boolean actif = !getModele().isAfficherDetails() && getModele().isAutoriseVisualiserMarge();
    snBarreBoutonNegociation.activerBouton(BOUTON_AFFICHER_DETAIL, actif);
  }
  
  /**
   * Rafraîchir le bouton Afficher les boutons Valider
   */
  private void rafraichirBoutonValider() {
    boolean validerVisible = getModele().getTexteTotal() != null && !getModele().getTexteTotal().trim().isEmpty()
        && getModele().getTexteTitre() != null && !getModele().getTexteTitre().trim().isEmpty();
    snBarreBoutonGeneral.activerBouton(EnumBouton.VALIDER, validerVisible);
    snBarreBoutonNegociation.activerBouton(EnumBouton.VALIDER, validerVisible);
  }
  
  /**
   * Rafraîchir le bouton Dégrouper
   */
  private void rafraichirBoutonDegrouper() {
    snBarreBoutonGeneral.activerBouton(BOUTON_DEGROUPER, !getModele().getRegroupement().isEnCoursCreation());
  }
  
  private void rafraichirTexteTitre() {
    String valeur = "";
    if (getModele().getTexteTitre() != null) {
      valeur = getModele().getTexteTitre();
    }
    if (!valeur.equals(tfTexteTitre.getText())) {
      tfTexteTitre.setText(valeur);
    }
    tfTexteTitre.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirTexteTotal() {
    String valeur = "";
    if (getModele().getTexteTotal() != null) {
      valeur = getModele().getTexteTotal();
    }
    if (!valeur.equals(tfTexteTotal.getText())) {
      tfTexteTotal.setText(valeur);
    }
    tfTexteTotal.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirChoixDetailArticle() {
    ckDetailArticles.setSelected(getModele().isEditionDetail());
    ckDetailArticles.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirChoixEditionPrix() {
    ckPrixArticles.setSelected(getModele().isEditionPrix());
    ckPrixArticles.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirTotalPublicInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
      zsTotalOrigine.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
      lbTotalOrigine.setText(LIBELLE_TOTAL_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
      zsTotalOrigine.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
      lbTotalOrigine.setText(LIBELLE_TOTAL_HT);
    }
    else {
      zsTotalOrigine.setText("");
    }
    
    zsTotalOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirTotalNetInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalNetTTC() != null) {
      zsTotalNetOrigine.setText(Constantes.formater(negociation.getTotalNetTTC(), true));
      lbTotalNetOrigine.setText(LIBELLE_TOTAL_NET_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalNetHT() != null) {
      zsTotalNetOrigine.setText(Constantes.formater(negociation.getTotalNetHT(), true));
      lbTotalNetOrigine.setText(LIBELLE_TOTAL_NET_HT);
    }
    else {
      zsTotalNetOrigine.setText("");
    }
    zsTotalNetOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirRemiseInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && negociation.getTauxRemise() != null) {
      zsRemiseOrigine.setText(Constantes.formater(negociation.getTauxRemise(Constantes.DEUX_DECIMALES), true));
    }
    else {
      zsRemiseOrigine.setText("");
    }
    zsRemiseOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirDetailInitial() {
    if (getModele().isAutoriseVisualiserMarge() && getModele().isAfficherDetails()) {
      pnlTarifOrigineMarge.setVisible(true);
      rafraichirIndiceInitial();
      rafraichirMargeInitial();
      rafraichirPrixRevientInitial();
    }
    else {
      pnlTarifOrigineMarge.setVisible(false);
    }
  }
  
  private void rafraichirIndiceInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && negociation.getIndiceMarge() != null) {
      zsIndiceMargeOrigine.setText(Constantes.formater(negociation.getIndiceMarge(), true));
    }
    else {
      zsIndiceMargeOrigine.setText("");
    }
    zsIndiceMargeOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirMargeInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && negociation.getTauxMarge() != null) {
      zsMargeOrigine.setText(Constantes.formater(negociation.getTauxMarge(), true));
    }
    else {
      zsMargeOrigine.setText("");
    }
    zsMargeOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirPrixRevientInitial() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementOrigine();
    if (negociation != null && negociation.getTotalRevientHT() != null) {
      zsPrixRevientOrigine.setText(Constantes.formater(negociation.getTotalRevientHT(), true));
    }
    else {
      zsPrixRevientOrigine.setText("");
    }
    zsPrixRevientOrigine.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirTotalPublic() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalBaseTTC() != null) {
      tfTotal.setText(Constantes.formater(negociation.getTotalBaseTTC(), true));
      lbTotal.setText(LIBELLE_TOTAL_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalBaseHT() != null) {
      tfTotal.setText(Constantes.formater(negociation.getTotalBaseHT(), true));
      lbTotal.setText(LIBELLE_TOTAL_HT);
    }
    else {
      tfTotal.setText("");
    }
    tfTotal.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirTotalNet() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && getModele().isAffichageTTC() && negociation.getTotalNetTTC() != null) {
      tfTotalNet.setText(Constantes.formater(negociation.getTotalNetTTC(), true));
      lbTotalNet.setText(LIBELLE_TOTAL_NET_TTC);
    }
    else if (negociation != null && !getModele().isAffichageTTC() && negociation.getTotalNetHT() != null) {
      tfTotalNet.setText(Constantes.formater(negociation.getTotalNetHT(), true));
      lbTotalNet.setText(LIBELLE_TOTAL_NET_HT);
    }
    else {
      tfTotalNet.setText("");
    }
    tfTotalNet.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirRemise() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && negociation.getTauxRemise() != null) {
      tfRemise.setText(Constantes.formater(negociation.getTauxRemise(Constantes.DEUX_DECIMALES), true));
    }
    else {
      tfRemise.setText("");
    }
    tfRemise.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirDetail() {
    if (getModele().isAutoriseVisualiserMarge() && getModele().isAfficherDetails()) {
      pnlTarifNegocieMarge.setVisible(true);
      rafraichirIndice();
      rafraichirMarge();
      rafraichirPrixRevient();
    }
    else {
      pnlTarifNegocieMarge.setVisible(false);
    }
  }
  
  private void rafraichirIndice() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && negociation.getIndiceMarge() != null) {
      tfIndiceMarge.setText(Constantes.formater(negociation.getIndiceMarge(), true));
    }
    else {
      tfIndiceMarge.setText("");
    }
    tfIndiceMarge.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirMarge() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && negociation.getTauxMarge() != null) {
      tfMarge.setText(Constantes.formater(negociation.getTauxMarge(), true));
    }
    else {
      tfMarge.setText("");
    }
    tfMarge.setEnabled(getModele().isDonneesChargees());
  }
  
  private void rafraichirPrixRevient() {
    NegociationVenteGlobale negociation = getModele().getNegociationVenteRegroupementNegocie();
    if (negociation != null && negociation.getTotalRevientHT() != null) {
      tfPrixRevient.setText(Constantes.formater(negociation.getTotalRevientHT(), true));
    }
    else {
      tfPrixRevient.setText("");
    }
    tfPrixRevient.setEnabled(getModele().isDonneesChargees());
  }
  
  /**
   * Teste si la touche du clavier utilisée peut entrainer une modification de valeur.
   */
  private boolean testerModificationValeur(int pKeyCode, boolean pValeurEstModifiee) {
    // Si la valeur a été déjà modifiée alors on retourne vrai
    if (pValeurEstModifiee) {
      return true;
    }
    if ((pKeyCode == KeyEvent.VK_TAB) || (pKeyCode == KeyEvent.VK_LEFT) || (pKeyCode == KeyEvent.VK_RIGHT)
        || (pKeyCode == KeyEvent.VK_UP)) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes évênementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        if (isEvenementsActifs()) {
          getModele().quitterAvecValidation();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_DEGROUPER)) {
        getModele().quitterAvecSuppression();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_TTC)) {
        getModele().modifierAffichageTTC(true);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_HT)) {
        getModele().modifierAffichageTTC(false);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL)) {
        getModele().modifierAffichageDetails(false);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        getModele().modifierAffichageDetails(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpPrincipalStateChanged(ChangeEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().changerOnglet(tbpPrincipal.getSelectedIndex());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteTitreKeyReleased(KeyEvent e) {
    try {
      getModele().modifierTexteTitre(tfTexteTitre.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTexteTotalKeyReleased(KeyEvent e) {
    try {
      getModele().modifierTexteTotal(tfTexteTotal.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckDetailArticlesActionPerformed(ActionEvent e) {
    try {
      getModele().modifierEditionDetailArticles(ckDetailArticles.isSelected());
      ckPrixArticles.setVisible(ckDetailArticles.isSelected());
      rafraichir();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckDetailPrixActionPerformed(ActionEvent e) {
    try {
      getModele().modifierEditionPrixArticles(ckPrixArticles.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemise1FocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierRemise(tfRemise.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTotalNetFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierPrixNet(tfTotalNet.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfIndiceFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierIndiceDeMarge(tfIndiceMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfMargeFocusLost(FocusEvent e) {
    try {
      if (valeurEstSaisie) {
        getModele().modifierMarge(tfMarge.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRemiseKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfTotalNetKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfIndiceKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  private void tfMargeKeyPressed(KeyEvent e) {
    // Permet de detecter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    tbpPrincipal = new JTabbedPane();
    pnlGeneral = new JPanel();
    pnlPrincipalGeneral = new JPanel();
    lbTitre = new JLabel();
    tfTexteTitre = new JTextField();
    lbTexteTotal = new JLabel();
    tfTexteTotal = new JTextField();
    ckDetailArticles = new JCheckBox();
    ckPrixArticles = new JCheckBox();
    snBarreBoutonGeneral = new SNBarreBouton();
    pnlNegociation = new JPanel();
    pnlPrincipalNegociation = new JPanel();
    pnlTarifOrigine = new JPanel();
    lbTotalOrigine = new JLabel();
    zsTotalOrigine = new RiZoneSortie();
    lbRemiseOrigine = new JLabel();
    zsRemiseOrigine = new RiZoneSortie();
    lbRemiseOrigineSymbolePourcentage = new JLabel();
    lbTotalNetOrigine = new JLabel();
    zsTotalNetOrigine = new RiZoneSortie();
    pnlTarifOrigineMarge = new JPanel();
    lbIndiceOrigine = new JLabel();
    zsIndiceMargeOrigine = new RiZoneSortie();
    lbMargeOrigine = new JLabel();
    zsMargeOrigine = new RiZoneSortie();
    lbMargeOrigineSymbolePourcentage = new JLabel();
    lbPrixRevientOrigine = new JLabel();
    zsPrixRevientOrigine = new RiZoneSortie();
    pnlTarifNegocie = new JPanel();
    lbTotal = new JLabel();
    tfTotal = new RiZoneSortie();
    lbRemise = new JLabel();
    tfRemise = new XRiTextField();
    lbRemiseSymbolePourcentage = new JLabel();
    lbTotalNet = new JLabel();
    tfTotalNet = new XRiTextField();
    pnlTarifNegocieMarge = new JPanel();
    lbIndice = new JLabel();
    tfIndiceMarge = new XRiTextField();
    lbMarge = new JLabel();
    tfMarge = new XRiTextField();
    lbMargeSymbolePourcentage = new JLabel();
    lbPrixRevient = new JLabel();
    tfPrixRevient = new RiZoneSortie();
    snBarreBoutonNegociation = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Regroupement de lignes");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setMinimumSize(new Dimension(800, 450));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== tbpPrincipal ========
    {
      tbpPrincipal.setFont(tbpPrincipal.getFont().deriveFont(tbpPrincipal.getFont().getSize() + 3f));
      tbpPrincipal.setOpaque(true);
      tbpPrincipal.setBackground(new Color(238, 238, 210));
      tbpPrincipal.setName("tbpPrincipal");
      tbpPrincipal.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          tbpPrincipalStateChanged(e);
        }
      });
      
      // ======== pnlGeneral ========
      {
        pnlGeneral.setBackground(new Color(238, 238, 210));
        pnlGeneral.setOpaque(false);
        pnlGeneral.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlGeneral.setName("pnlGeneral");
        pnlGeneral.setLayout(new BorderLayout());
        
        // ======== pnlPrincipalGeneral ========
        {
          pnlPrincipalGeneral.setBackground(new Color(238, 238, 210));
          pnlPrincipalGeneral.setOpaque(false);
          pnlPrincipalGeneral.setName("pnlPrincipalGeneral");
          pnlPrincipalGeneral.setLayout(null);
          
          // ---- lbTitre ----
          lbTitre.setText("Titre du regroupement");
          lbTitre.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbTitre.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTitre.setFont(lbTitre.getFont().deriveFont(lbTitre.getFont().getSize() + 2f));
          lbTitre.setName("lbTitre");
          pnlPrincipalGeneral.add(lbTitre);
          lbTitre.setBounds(5, 25, 145, 30);
          
          // ---- tfTexteTitre ----
          tfTexteTitre.setFont(tfTexteTitre.getFont().deriveFont(tfTexteTitre.getFont().getSize() + 2f));
          tfTexteTitre.setName("tfTexteTitre");
          tfTexteTitre.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfTexteTitreKeyReleased(e);
            }
          });
          pnlPrincipalGeneral.add(tfTexteTitre);
          tfTexteTitre.setBounds(155, 25, 775, 30);
          
          // ---- lbTexteTotal ----
          lbTexteTotal.setText("Texte pour le total");
          lbTexteTotal.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbTexteTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTexteTotal.setFont(lbTexteTotal.getFont().deriveFont(lbTexteTotal.getFont().getSize() + 2f));
          lbTexteTotal.setName("lbTexteTotal");
          pnlPrincipalGeneral.add(lbTexteTotal);
          lbTexteTotal.setBounds(5, 65, 145, 30);
          
          // ---- tfTexteTotal ----
          tfTexteTotal.setFont(tfTexteTotal.getFont().deriveFont(tfTexteTotal.getFont().getSize() + 2f));
          tfTexteTotal.setName("tfTexteTotal");
          tfTexteTotal.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfTexteTotalKeyReleased(e);
            }
          });
          pnlPrincipalGeneral.add(tfTexteTotal);
          tfTexteTotal.setBounds(155, 65, 775, 30);
          
          // ---- ckDetailArticles ----
          ckDetailArticles.setText("Edition du d\u00e9tail des articles");
          ckDetailArticles.setFont(ckDetailArticles.getFont().deriveFont(ckDetailArticles.getFont().getSize() + 2f));
          ckDetailArticles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckDetailArticles.setName("ckDetailArticles");
          ckDetailArticles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckDetailArticlesActionPerformed(e);
            }
          });
          pnlPrincipalGeneral.add(ckDetailArticles);
          ckDetailArticles.setBounds(155, 110, 235, 30);
          
          // ---- ckPrixArticles ----
          ckPrixArticles.setText("Edition des prix par articles");
          ckPrixArticles.setFont(ckPrixArticles.getFont().deriveFont(ckPrixArticles.getFont().getSize() + 2f));
          ckPrixArticles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckPrixArticles.setName("ckPrixArticles");
          ckPrixArticles.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckDetailPrixActionPerformed(e);
            }
          });
          pnlPrincipalGeneral.add(ckPrixArticles);
          ckPrixArticles.setBounds(155, 150, 235, 30);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlPrincipalGeneral.getComponentCount(); i++) {
              Rectangle bounds = pnlPrincipalGeneral.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlPrincipalGeneral.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlPrincipalGeneral.setMinimumSize(preferredSize);
            pnlPrincipalGeneral.setPreferredSize(preferredSize);
          }
        }
        pnlGeneral.add(pnlPrincipalGeneral, BorderLayout.CENTER);
        
        // ---- snBarreBoutonGeneral ----
        snBarreBoutonGeneral.setName("snBarreBoutonGeneral");
        pnlGeneral.add(snBarreBoutonGeneral, BorderLayout.SOUTH);
      }
      tbpPrincipal.addTab("G\u00e9n\u00e9ral", pnlGeneral);
      
      // ======== pnlNegociation ========
      {
        pnlNegociation.setBackground(new Color(238, 238, 210));
        pnlNegociation.setOpaque(false);
        pnlNegociation.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlNegociation.setName("pnlNegociation");
        pnlNegociation.setLayout(new BorderLayout());
        
        // ======== pnlPrincipalNegociation ========
        {
          pnlPrincipalNegociation.setBackground(new Color(238, 238, 210));
          pnlPrincipalNegociation.setOpaque(false);
          pnlPrincipalNegociation.setName("pnlPrincipalNegociation");
          pnlPrincipalNegociation.setLayout(null);
          
          // ======== pnlTarifOrigine ========
          {
            pnlTarifOrigine.setOpaque(false);
            pnlTarifOrigine.setBorder(new TitledBorder("Prix initial"));
            pnlTarifOrigine.setName("pnlTarifOrigine");
            pnlTarifOrigine.setLayout(null);
            
            // ---- lbTotalOrigine ----
            lbTotalOrigine.setText("Total public");
            lbTotalOrigine.setFont(lbTotalOrigine.getFont().deriveFont(lbTotalOrigine.getFont().getStyle() & ~Font.BOLD,
                lbTotalOrigine.getFont().getSize() + 2f));
            lbTotalOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbTotalOrigine.setComponentPopupMenu(null);
            lbTotalOrigine.setName("lbTotalOrigine");
            pnlTarifOrigine.add(lbTotalOrigine);
            lbTotalOrigine.setBounds(30, 25, 105, 21);
            
            // ---- zsTotalOrigine ----
            zsTotalOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsTotalOrigine.setFont(zsTotalOrigine.getFont().deriveFont(zsTotalOrigine.getFont().getSize() + 2f));
            zsTotalOrigine.setComponentPopupMenu(null);
            zsTotalOrigine.setName("zsTotalOrigine");
            pnlTarifOrigine.add(zsTotalOrigine);
            zsTotalOrigine.setBounds(30, 55, 105, 30);
            
            // ---- lbRemiseOrigine ----
            lbRemiseOrigine.setText("Remise");
            lbRemiseOrigine.setFont(lbRemiseOrigine.getFont().deriveFont(lbRemiseOrigine.getFont().getStyle() & ~Font.BOLD,
                lbRemiseOrigine.getFont().getSize() + 2f));
            lbRemiseOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbRemiseOrigine.setComponentPopupMenu(null);
            lbRemiseOrigine.setName("lbRemiseOrigine");
            pnlTarifOrigine.add(lbRemiseOrigine);
            lbRemiseOrigine.setBounds(219, 25, 60, 21);
            
            // ---- zsRemiseOrigine ----
            zsRemiseOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsRemiseOrigine.setFont(zsRemiseOrigine.getFont().deriveFont(zsRemiseOrigine.getFont().getSize() + 2f));
            zsRemiseOrigine.setComponentPopupMenu(null);
            zsRemiseOrigine.setName("zsRemiseOrigine");
            pnlTarifOrigine.add(zsRemiseOrigine);
            zsRemiseOrigine.setBounds(219, 55, 60, 30);
            
            // ---- lbRemiseOrigineSymbolePourcentage ----
            lbRemiseOrigineSymbolePourcentage.setText("% ");
            lbRemiseOrigineSymbolePourcentage.setHorizontalAlignment(SwingConstants.LEFT);
            lbRemiseOrigineSymbolePourcentage.setFont(
                lbRemiseOrigineSymbolePourcentage.getFont().deriveFont(lbRemiseOrigineSymbolePourcentage.getFont().getSize() + 2f));
            lbRemiseOrigineSymbolePourcentage.setComponentPopupMenu(null);
            lbRemiseOrigineSymbolePourcentage.setName("lbRemiseOrigineSymbolePourcentage");
            pnlTarifOrigine.add(lbRemiseOrigineSymbolePourcentage);
            lbRemiseOrigineSymbolePourcentage.setBounds(290, 55, 20, 30);
            
            // ---- lbTotalNetOrigine ----
            lbTotalNetOrigine.setText("Total net");
            lbTotalNetOrigine.setFont(lbTotalNetOrigine.getFont().deriveFont(lbTotalNetOrigine.getFont().getStyle() & ~Font.BOLD,
                lbTotalNetOrigine.getFont().getSize() + 2f));
            lbTotalNetOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbTotalNetOrigine.setComponentPopupMenu(null);
            lbTotalNetOrigine.setName("lbTotalNetOrigine");
            pnlTarifOrigine.add(lbTotalNetOrigine);
            lbTotalNetOrigine.setBounds(365, 25, 105, 21);
            
            // ---- zsTotalNetOrigine ----
            zsTotalNetOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsTotalNetOrigine.setFont(zsTotalNetOrigine.getFont().deriveFont(zsTotalNetOrigine.getFont().getSize() + 2f));
            zsTotalNetOrigine.setComponentPopupMenu(null);
            zsTotalNetOrigine.setName("zsTotalNetOrigine");
            pnlTarifOrigine.add(zsTotalNetOrigine);
            zsTotalNetOrigine.setBounds(365, 55, 105, 30);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlTarifOrigine.getComponentCount(); i++) {
                Rectangle bounds = pnlTarifOrigine.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlTarifOrigine.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlTarifOrigine.setMinimumSize(preferredSize);
              pnlTarifOrigine.setPreferredSize(preferredSize);
            }
          }
          pnlPrincipalNegociation.add(pnlTarifOrigine);
          pnlTarifOrigine.setBounds(20, 35, 495, 110);
          
          // ======== pnlTarifOrigineMarge ========
          {
            pnlTarifOrigineMarge.setOpaque(false);
            pnlTarifOrigineMarge.setBorder(new TitledBorder("Marge initiale"));
            pnlTarifOrigineMarge.setName("pnlTarifOrigineMarge");
            pnlTarifOrigineMarge.setLayout(null);
            
            // ---- lbIndiceOrigine ----
            lbIndiceOrigine.setText("Indice");
            lbIndiceOrigine.setFont(lbIndiceOrigine.getFont().deriveFont(lbIndiceOrigine.getFont().getStyle() & ~Font.BOLD,
                lbIndiceOrigine.getFont().getSize() + 2f));
            lbIndiceOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbIndiceOrigine.setComponentPopupMenu(null);
            lbIndiceOrigine.setName("lbIndiceOrigine");
            pnlTarifOrigineMarge.add(lbIndiceOrigine);
            lbIndiceOrigine.setBounds(10, 25, 98, 21);
            
            // ---- zsIndiceMargeOrigine ----
            zsIndiceMargeOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsIndiceMargeOrigine.setFont(zsIndiceMargeOrigine.getFont().deriveFont(zsIndiceMargeOrigine.getFont().getSize() + 2f));
            zsIndiceMargeOrigine.setComponentPopupMenu(null);
            zsIndiceMargeOrigine.setName("zsIndiceMargeOrigine");
            pnlTarifOrigineMarge.add(zsIndiceMargeOrigine);
            zsIndiceMargeOrigine.setBounds(30, 55, 60, 30);
            
            // ---- lbMargeOrigine ----
            lbMargeOrigine.setText("Marge");
            lbMargeOrigine.setFont(lbMargeOrigine.getFont().deriveFont(lbMargeOrigine.getFont().getStyle() & ~Font.BOLD,
                lbMargeOrigine.getFont().getSize() + 2f));
            lbMargeOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbMargeOrigine.setComponentPopupMenu(null);
            lbMargeOrigine.setName("lbMargeOrigine");
            pnlTarifOrigineMarge.add(lbMargeOrigine);
            lbMargeOrigine.setBounds(146, 25, 68, 21);
            
            // ---- zsMargeOrigine ----
            zsMargeOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsMargeOrigine.setFont(zsMargeOrigine.getFont().deriveFont(zsMargeOrigine.getFont().getSize() + 2f));
            zsMargeOrigine.setComponentPopupMenu(null);
            zsMargeOrigine.setName("zsMargeOrigine");
            pnlTarifOrigineMarge.add(zsMargeOrigine);
            zsMargeOrigine.setBounds(150, 55, 60, 30);
            
            // ---- lbMargeOrigineSymbolePourcentage ----
            lbMargeOrigineSymbolePourcentage.setText("%");
            lbMargeOrigineSymbolePourcentage.setHorizontalAlignment(SwingConstants.LEFT);
            lbMargeOrigineSymbolePourcentage.setFont(
                lbMargeOrigineSymbolePourcentage.getFont().deriveFont(lbMargeOrigineSymbolePourcentage.getFont().getSize() + 2f));
            lbMargeOrigineSymbolePourcentage.setComponentPopupMenu(null);
            lbMargeOrigineSymbolePourcentage.setName("lbMargeOrigineSymbolePourcentage");
            pnlTarifOrigineMarge.add(lbMargeOrigineSymbolePourcentage);
            lbMargeOrigineSymbolePourcentage.setBounds(215, 55, 40, 30);
            
            // ---- lbPrixRevientOrigine ----
            lbPrixRevientOrigine.setText("Prix de revient HT");
            lbPrixRevientOrigine.setFont(lbPrixRevientOrigine.getFont().deriveFont(lbPrixRevientOrigine.getFont().getStyle() & ~Font.BOLD,
                lbPrixRevientOrigine.getFont().getSize() + 2f));
            lbPrixRevientOrigine.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixRevientOrigine.setComponentPopupMenu(null);
            lbPrixRevientOrigine.setName("lbPrixRevientOrigine");
            pnlTarifOrigineMarge.add(lbPrixRevientOrigine);
            lbPrixRevientOrigine.setBounds(280, 25, 115, 21);
            
            // ---- zsPrixRevientOrigine ----
            zsPrixRevientOrigine.setHorizontalAlignment(SwingConstants.RIGHT);
            zsPrixRevientOrigine.setFont(zsPrixRevientOrigine.getFont().deriveFont(zsPrixRevientOrigine.getFont().getSize() + 2f));
            zsPrixRevientOrigine.setComponentPopupMenu(null);
            zsPrixRevientOrigine.setName("zsPrixRevientOrigine");
            pnlTarifOrigineMarge.add(zsPrixRevientOrigine);
            zsPrixRevientOrigine.setBounds(285, 55, 105, 30);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlTarifOrigineMarge.getComponentCount(); i++) {
                Rectangle bounds = pnlTarifOrigineMarge.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlTarifOrigineMarge.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlTarifOrigineMarge.setMinimumSize(preferredSize);
              pnlTarifOrigineMarge.setPreferredSize(preferredSize);
            }
          }
          pnlPrincipalNegociation.add(pnlTarifOrigineMarge);
          pnlTarifOrigineMarge.setBounds(525, 35, 415, 110);
          
          // ======== pnlTarifNegocie ========
          {
            pnlTarifNegocie.setBackground(new Color(222, 222, 206));
            pnlTarifNegocie.setBorder(new TitledBorder("Prix n\u00e9goci\u00e9"));
            pnlTarifNegocie.setOpaque(false);
            pnlTarifNegocie.setName("pnlTarifNegocie");
            pnlTarifNegocie.setLayout(null);
            
            // ---- lbTotal ----
            lbTotal.setText("Total public");
            lbTotal.setFont(lbTotal.getFont().deriveFont(lbTotal.getFont().getStyle() & ~Font.BOLD, lbTotal.getFont().getSize() + 2f));
            lbTotal.setHorizontalAlignment(SwingConstants.CENTER);
            lbTotal.setComponentPopupMenu(null);
            lbTotal.setName("lbTotal");
            pnlTarifNegocie.add(lbTotal);
            lbTotal.setBounds(30, 25, 105, 21);
            
            // ---- tfTotal ----
            tfTotal.setHorizontalAlignment(SwingConstants.RIGHT);
            tfTotal.setFont(tfTotal.getFont().deriveFont(tfTotal.getFont().getSize() + 2f));
            tfTotal.setComponentPopupMenu(null);
            tfTotal.setName("tfTotal");
            pnlTarifNegocie.add(tfTotal);
            tfTotal.setBounds(30, 55, 105, 30);
            
            // ---- lbRemise ----
            lbRemise.setText("Remise");
            lbRemise
                .setFont(lbRemise.getFont().deriveFont(lbRemise.getFont().getStyle() & ~Font.BOLD, lbRemise.getFont().getSize() + 2f));
            lbRemise.setHorizontalAlignment(SwingConstants.CENTER);
            lbRemise.setComponentPopupMenu(null);
            lbRemise.setName("lbRemise");
            pnlTarifNegocie.add(lbRemise);
            lbRemise.setBounds(219, 25, 60, 21);
            
            // ---- tfRemise ----
            tfRemise.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemise.setFont(tfRemise.getFont().deriveFont(tfRemise.getFont().getSize() + 2f));
            tfRemise.setComponentPopupMenu(null);
            tfRemise.setName("tfRemise");
            tfRemise.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemise1FocusLost(e);
              }
            });
            tfRemise.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfRemiseKeyPressed(e);
              }
            });
            pnlTarifNegocie.add(tfRemise);
            tfRemise.setBounds(219, 55, 60, 30);
            
            // ---- lbRemiseSymbolePourcentage ----
            lbRemiseSymbolePourcentage.setText("% ");
            lbRemiseSymbolePourcentage.setHorizontalAlignment(SwingConstants.LEFT);
            lbRemiseSymbolePourcentage
                .setFont(lbRemiseSymbolePourcentage.getFont().deriveFont(lbRemiseSymbolePourcentage.getFont().getSize() + 2f));
            lbRemiseSymbolePourcentage.setComponentPopupMenu(null);
            lbRemiseSymbolePourcentage.setName("lbRemiseSymbolePourcentage");
            pnlTarifNegocie.add(lbRemiseSymbolePourcentage);
            lbRemiseSymbolePourcentage.setBounds(285, 55, 20, 30);
            
            // ---- lbTotalNet ----
            lbTotalNet.setText("Total net");
            lbTotalNet.setFont(
                lbTotalNet.getFont().deriveFont(lbTotalNet.getFont().getStyle() & ~Font.BOLD, lbTotalNet.getFont().getSize() + 2f));
            lbTotalNet.setHorizontalAlignment(SwingConstants.CENTER);
            lbTotalNet.setComponentPopupMenu(null);
            lbTotalNet.setName("lbTotalNet");
            pnlTarifNegocie.add(lbTotalNet);
            lbTotalNet.setBounds(365, 25, 105, 21);
            
            // ---- tfTotalNet ----
            tfTotalNet.setHorizontalAlignment(SwingConstants.RIGHT);
            tfTotalNet.setFont(tfTotalNet.getFont().deriveFont(tfTotalNet.getFont().getSize() + 2f));
            tfTotalNet.setComponentPopupMenu(null);
            tfTotalNet.setName("tfTotalNet");
            tfTotalNet.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfTotalNetFocusLost(e);
              }
            });
            tfTotalNet.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfTotalNetKeyPressed(e);
              }
            });
            pnlTarifNegocie.add(tfTotalNet);
            tfTotalNet.setBounds(365, 55, 105, 30);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlTarifNegocie.getComponentCount(); i++) {
                Rectangle bounds = pnlTarifNegocie.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlTarifNegocie.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlTarifNegocie.setMinimumSize(preferredSize);
              pnlTarifNegocie.setPreferredSize(preferredSize);
            }
          }
          pnlPrincipalNegociation.add(pnlTarifNegocie);
          pnlTarifNegocie.setBounds(20, 180, 495, 110);
          
          // ======== pnlTarifNegocieMarge ========
          {
            pnlTarifNegocieMarge.setBackground(new Color(222, 222, 206));
            pnlTarifNegocieMarge.setBorder(new TitledBorder("Marge n\u00e9goci\u00e9e"));
            pnlTarifNegocieMarge.setOpaque(false);
            pnlTarifNegocieMarge.setName("pnlTarifNegocieMarge");
            pnlTarifNegocieMarge.setLayout(null);
            
            // ---- lbIndice ----
            lbIndice.setText("Indice");
            lbIndice
                .setFont(lbIndice.getFont().deriveFont(lbIndice.getFont().getStyle() & ~Font.BOLD, lbIndice.getFont().getSize() + 2f));
            lbIndice.setHorizontalAlignment(SwingConstants.CENTER);
            lbIndice.setComponentPopupMenu(null);
            lbIndice.setName("lbIndice");
            pnlTarifNegocieMarge.add(lbIndice);
            lbIndice.setBounds(10, 25, 98, 21);
            
            // ---- tfIndiceMarge ----
            tfIndiceMarge.setHorizontalAlignment(SwingConstants.RIGHT);
            tfIndiceMarge.setFont(tfIndiceMarge.getFont().deriveFont(tfIndiceMarge.getFont().getSize() + 2f));
            tfIndiceMarge.setComponentPopupMenu(null);
            tfIndiceMarge.setName("tfIndiceMarge");
            tfIndiceMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfIndiceFocusLost(e);
              }
            });
            tfIndiceMarge.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfIndiceKeyPressed(e);
              }
            });
            pnlTarifNegocieMarge.add(tfIndiceMarge);
            tfIndiceMarge.setBounds(30, 55, 60, 30);
            
            // ---- lbMarge ----
            lbMarge.setText("Marge");
            lbMarge.setFont(lbMarge.getFont().deriveFont(lbMarge.getFont().getStyle() & ~Font.BOLD, lbMarge.getFont().getSize() + 2f));
            lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
            lbMarge.setComponentPopupMenu(null);
            lbMarge.setName("lbMarge");
            pnlTarifNegocieMarge.add(lbMarge);
            lbMarge.setBounds(145, 25, 68, 21);
            
            // ---- tfMarge ----
            tfMarge.setHorizontalAlignment(SwingConstants.RIGHT);
            tfMarge.setFont(tfMarge.getFont().deriveFont(tfMarge.getFont().getSize() + 2f));
            tfMarge.setComponentPopupMenu(null);
            tfMarge.setName("tfMarge");
            tfMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfMargeFocusLost(e);
              }
            });
            tfMarge.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                tfMargeKeyPressed(e);
              }
            });
            pnlTarifNegocieMarge.add(tfMarge);
            tfMarge.setBounds(150, 55, 60, 30);
            
            // ---- lbMargeSymbolePourcentage ----
            lbMargeSymbolePourcentage.setText("%");
            lbMargeSymbolePourcentage.setHorizontalAlignment(SwingConstants.LEFT);
            lbMargeSymbolePourcentage
                .setFont(lbMargeSymbolePourcentage.getFont().deriveFont(lbMargeSymbolePourcentage.getFont().getSize() + 2f));
            lbMargeSymbolePourcentage.setComponentPopupMenu(null);
            lbMargeSymbolePourcentage.setName("lbMargeSymbolePourcentage");
            pnlTarifNegocieMarge.add(lbMargeSymbolePourcentage);
            lbMargeSymbolePourcentage.setBounds(215, 55, 40, 30);
            
            // ---- lbPrixRevient ----
            lbPrixRevient.setText("Prix de revient HT");
            lbPrixRevient.setFont(lbPrixRevient.getFont().deriveFont(lbPrixRevient.getFont().getStyle() & ~Font.BOLD,
                lbPrixRevient.getFont().getSize() + 2f));
            lbPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixRevient.setComponentPopupMenu(null);
            lbPrixRevient.setName("lbPrixRevient");
            pnlTarifNegocieMarge.add(lbPrixRevient);
            lbPrixRevient.setBounds(280, 25, 115, 21);
            
            // ---- tfPrixRevient ----
            tfPrixRevient.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixRevient.setFont(tfPrixRevient.getFont().deriveFont(tfPrixRevient.getFont().getSize() + 2f));
            tfPrixRevient.setComponentPopupMenu(null);
            tfPrixRevient.setName("tfPrixRevient");
            pnlTarifNegocieMarge.add(tfPrixRevient);
            tfPrixRevient.setBounds(285, 55, 105, 30);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < pnlTarifNegocieMarge.getComponentCount(); i++) {
                Rectangle bounds = pnlTarifNegocieMarge.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = pnlTarifNegocieMarge.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              pnlTarifNegocieMarge.setMinimumSize(preferredSize);
              pnlTarifNegocieMarge.setPreferredSize(preferredSize);
            }
          }
          pnlPrincipalNegociation.add(pnlTarifNegocieMarge);
          pnlTarifNegocieMarge.setBounds(525, 180, 415, 110);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < pnlPrincipalNegociation.getComponentCount(); i++) {
              Rectangle bounds = pnlPrincipalNegociation.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = pnlPrincipalNegociation.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            pnlPrincipalNegociation.setMinimumSize(preferredSize);
            pnlPrincipalNegociation.setPreferredSize(preferredSize);
          }
        }
        pnlNegociation.add(pnlPrincipalNegociation, BorderLayout.CENTER);
        
        // ---- snBarreBoutonNegociation ----
        snBarreBoutonNegociation.setName("snBarreBoutonNegociation");
        pnlNegociation.add(snBarreBoutonNegociation, BorderLayout.SOUTH);
      }
      tbpPrincipal.addTab("N\u00e9gociation", pnlNegociation);
    }
    contentPane.add(tbpPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JTabbedPane tbpPrincipal;
  private JPanel pnlGeneral;
  private JPanel pnlPrincipalGeneral;
  private JLabel lbTitre;
  private JTextField tfTexteTitre;
  private JLabel lbTexteTotal;
  private JTextField tfTexteTotal;
  private JCheckBox ckDetailArticles;
  private JCheckBox ckPrixArticles;
  private SNBarreBouton snBarreBoutonGeneral;
  private JPanel pnlNegociation;
  private JPanel pnlPrincipalNegociation;
  private JPanel pnlTarifOrigine;
  private JLabel lbTotalOrigine;
  private RiZoneSortie zsTotalOrigine;
  private JLabel lbRemiseOrigine;
  private RiZoneSortie zsRemiseOrigine;
  private JLabel lbRemiseOrigineSymbolePourcentage;
  private JLabel lbTotalNetOrigine;
  private RiZoneSortie zsTotalNetOrigine;
  private JPanel pnlTarifOrigineMarge;
  private JLabel lbIndiceOrigine;
  private RiZoneSortie zsIndiceMargeOrigine;
  private JLabel lbMargeOrigine;
  private RiZoneSortie zsMargeOrigine;
  private JLabel lbMargeOrigineSymbolePourcentage;
  private JLabel lbPrixRevientOrigine;
  private RiZoneSortie zsPrixRevientOrigine;
  private JPanel pnlTarifNegocie;
  private JLabel lbTotal;
  private RiZoneSortie tfTotal;
  private JLabel lbRemise;
  private XRiTextField tfRemise;
  private JLabel lbRemiseSymbolePourcentage;
  private JLabel lbTotalNet;
  private XRiTextField tfTotalNet;
  private JPanel pnlTarifNegocieMarge;
  private JLabel lbIndice;
  private XRiTextField tfIndiceMarge;
  private JLabel lbMarge;
  private XRiTextField tfMarge;
  private JLabel lbMargeSymbolePourcentage;
  private JLabel lbPrixRevient;
  private RiZoneSortie tfPrixRevient;
  private SNBarreBouton snBarreBoutonNegociation;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
