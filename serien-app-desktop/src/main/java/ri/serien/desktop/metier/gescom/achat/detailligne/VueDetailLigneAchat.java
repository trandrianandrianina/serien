/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueDetailLigneAchat extends AbstractVueDialogue<ModeleDetailLigneAchat> {
  private VueOngletGeneralLigneAchat ongletGeneral = null;
  private VueOngletNegociationLigneAchat ongletNegociation = null;
  private VueOngletInformationsTechniquesLigneAchat ongletMemo = null;
  private VueOngletStockLigneAchat ongletStock = null;
  private VueOngletHistoriqueAchat ongletHistorique = null;
  private VueOngletLiens ongletLiens = null;
  
  /**
   * Constructeur.
   */
  public VueDetailLigneAchat(ModeleDetailLigneAchat pModele) {
    super(pModele);
    
    ongletGeneral = new VueOngletGeneralLigneAchat(getModele());
    ongletNegociation = new VueOngletNegociationLigneAchat(getModele());
    ongletMemo = new VueOngletInformationsTechniquesLigneAchat(getModele());
    ongletStock = new VueOngletStockLigneAchat(getModele());
    ongletHistorique = new VueOngletHistoriqueAchat(getModele());
    ongletLiens = new VueOngletLiens(getModele());
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    ongletGeneral.initialiserComposants();
    ongletNegociation.initialiserComposants();
    ongletMemo.initialiserComposants();
    ongletStock.initialiserComposants();
    ongletHistorique.initialiserComposants();
    ongletLiens.initialiserComposants();
    
    if (getModele().getArticle() == null) {
      throw new MessageErreurException("Aucun article de sélectionné");
    }
    
    // Ajouter l'onglet informations générales
    pnlOnglets.addTab("Général", null, ongletGeneral, "Informations générales");
    if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_GENERAL) {
      pnlOnglets.setSelectedComponent(ongletGeneral);
    }
    
    // Ajouter l'onglet négociation si ce n'est pas un article palette ou un article spécial
    if (getModele().getArticle() != null && !getModele().getArticle().isArticlePalette()
        && !getModele().getArticle().isArticleSpecial()) {
      pnlOnglets.addTab("Négociations", null, ongletNegociation, "Négociations pour cette commande");
      if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_NEGOCIATION) {
        pnlOnglets.setSelectedComponent(ongletNegociation);
      }
    }
    
    // Ajouter l'onglet informations techniques si ce n'est pas un article spécial
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleSpecial()) {
      pnlOnglets.addTab("Informations", null, ongletMemo, "Informations techniques pour cette commande");
      if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_INFORMATIONSTECHNIQUES) {
        pnlOnglets.setSelectedComponent(ongletMemo);
      }
    }
    
    // Ajouter l'onglet stock
    // si ce n'est pas un article spécial
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleSpecial()) {
      // On n'affiche pas le stock pour les lignes qui ne sont pas associées à un magasin (article non géré en stock par exemple)
      if (getModele().getLigneAchatArticle() != null && getModele().getLigneAchatArticle().getIdMagasin() != null) {
        pnlOnglets.addTab("Stocks", null, ongletStock, "Etat des stocks pour cet article");
        if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_STOCK) {
          pnlOnglets.setSelectedComponent(ongletStock);
        }
      }
    }
    
    // Ajouter l'onglet historique
    // si ce n'est pas un article spécial
    if (getModele().getArticle() != null && !getModele().getArticle().isArticleSpecial()) {
      // On n'affiche pas l'histroique pour les lignes qui ne sont pas associées à un magasin (article non géré en stock par exemple)
      if (getModele().getLigneAchatArticle() != null && getModele().getLigneAchatArticle().getIdMagasin() != null) {
        pnlOnglets.addTab("Historique", null, ongletHistorique, "Historique des réceptions");
        if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_HISTORIQUE) {
          pnlOnglets.setSelectedComponent(ongletHistorique);
        }
      }
    }
    
    // Ajouter l'onglet liens
    pnlOnglets.addTab("Liens", null, ongletLiens, "Liens entre documents");
    if (getModele().getOngleActif() == ModeleDetailLigneAchat.ONGLET_LIENS) {
      pnlOnglets.setSelectedComponent(ongletLiens);
    }
    
  }
  
  @Override
  public void rafraichir() {
    switch (getModele().getOngleActif()) {
      case ModeleDetailLigneAchat.ONGLET_GENERAL:
        if (getPanelOnglets().indexOfComponent(ongletGeneral) != -1) {
          pnlOnglets.setSelectedComponent(ongletGeneral);
          ongletGeneral.rafraichir();
        }
        break;
      case ModeleDetailLigneAchat.ONGLET_NEGOCIATION:
        if (getPanelOnglets().indexOfComponent(ongletNegociation) != -1) {
          pnlOnglets.setSelectedComponent(ongletNegociation);
          ongletNegociation.rafraichir();
        }
        break;
      case ModeleDetailLigneAchat.ONGLET_INFORMATIONSTECHNIQUES:
        if (getPanelOnglets().indexOfComponent(ongletMemo) != -1) {
          pnlOnglets.setSelectedComponent(ongletMemo);
          ongletMemo.rafraichir();
        }
        break;
      case ModeleDetailLigneAchat.ONGLET_STOCK:
        if (getPanelOnglets().indexOfComponent(ongletStock) != -1) {
          pnlOnglets.setSelectedComponent(ongletStock);
          ongletStock.rafraichir();
        }
        break;
      case ModeleDetailLigneAchat.ONGLET_HISTORIQUE:
        if (getPanelOnglets().indexOfComponent(ongletHistorique) != -1) {
          pnlOnglets.setSelectedComponent(ongletHistorique);
          ongletHistorique.rafraichir();
        }
        break;
      case ModeleDetailLigneAchat.ONGLET_LIENS:
        if (getPanelOnglets().indexOfComponent(ongletLiens) != -1) {
          pnlOnglets.setSelectedComponent(ongletLiens);
          ongletLiens.rafraichir();
        }
        break;
    }
  }
  
  /**
   * Récupérer le code correspondant à l'onglet sélectionné. Cela retourne l'onglet général si aucun n'est sélectionné.
   */
  private int getCodeOngletSelectionne() {
    if (pnlOnglets.getSelectedComponent() == ongletGeneral) {
      return ModeleDetailLigneAchat.ONGLET_GENERAL;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletNegociation) {
      return ModeleDetailLigneAchat.ONGLET_NEGOCIATION;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletMemo) {
      return ModeleDetailLigneAchat.ONGLET_INFORMATIONSTECHNIQUES;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletStock) {
      return ModeleDetailLigneAchat.ONGLET_STOCK;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletHistorique) {
      return ModeleDetailLigneAchat.ONGLET_HISTORIQUE;
    }
    else if (pnlOnglets.getSelectedComponent() == ongletLiens) {
      return ModeleDetailLigneAchat.ONGLET_LIENS;
    }
    
    return ModeleDetailLigneAchat.ONGLET_GENERAL;
  }
  
  // -- Méthodes interractives
  
  private void pnlOngletsStateChanged(ChangeEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierOngletActif(getCodeOngletSelectionne());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlOnglets = new JTabbedPane();
    
    // ======== this ========
    setTitle("D\u00e9tail de la ligne article");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(975, 590));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlOnglets ========
    {
      pnlOnglets.setFont(pnlOnglets.getFont().deriveFont(pnlOnglets.getFont().getSize() + 3f));
      pnlOnglets.setBackground(new Color(238, 238, 210));
      pnlOnglets.setOpaque(true);
      pnlOnglets.setPreferredSize(new Dimension(190, 200));
      pnlOnglets.setName("pnlOnglets");
      pnlOnglets.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          pnlOngletsStateChanged(e);
        }
      });
    }
    contentPane.add(pnlOnglets, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JTabbedPane pnlOnglets;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
  public JTabbedPane getPanelOnglets() {
    return pnlOnglets;
  }
  
  public void setPanelOnglets(JTabbedPane panelOnglets) {
    this.pnlOnglets = panelOnglets;
  }
  
}
