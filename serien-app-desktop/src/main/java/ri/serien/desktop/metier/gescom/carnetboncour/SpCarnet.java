/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.carnetboncour;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

public class SpCarnet extends SessionJava {
  /**
   * Constructeur.
   */
  public SpCarnet(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleCarnetBonCour modele = new ModeleCarnetBonCour(this);
    VueCarnetBonCour vue = new VueCarnetBonCour(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
  }
}
