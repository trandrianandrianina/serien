/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisionnementstock;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Description d'une ligne à afficher dans le tableau des commandes d'achat en cours pour un article.
 */
public class LigneTableau {
  // Variables de travail
  private IdMagasin idMagasin = null;
  private IdArticle idArticle = null;
  private Article article = null;
  private boolean charge = false;
  private LigneAchatArticle ligneAchatAssociee = null;
  private String stockIdeal = null;
  private String stockMinimum = null;
  private boolean stockDisponibleInferieurAuMinimum = false;
  
  // Variables à afficher
  private String quantiteCommandeeUCA = null;
  private String uniteCommandeUCA = null;
  private String libelle = null;
  private String typeArticle = null;
  private String stockPhysique = null;
  private String stockCommande = null;
  private String stockAttendu = null;
  private String stockDisponible = null;
  
  /**
   * Constructeur.
   */
  public LigneTableau(IdMagasin pIdMagasin, IdArticle pIdArticle) {
    idMagasin = pIdMagasin;
    idArticle = pIdArticle;
  }
  
  // -- Méthodes public
  
  /**
   * Initialise les données.
   */
  public void initialiserDonnees(Article pArticle) {
    // Initialisation des informations provenant de l'article
    if (pArticle == null) {
      return;
    }
    article = pArticle;
    
    // Alimentation des variables qui seront affichées dans le tableau
    quantiteCommandeeUCA = "0";
    uniteCommandeUCA = "";
    libelle = Constantes.normerTexte(article.getLibelleComplet());
    
    // Renseigner le type d'articles
    if (article.isHorsGamme()) {
      typeArticle = Article.LIBELLE_HORS_GAMME;
    }
    else if (article.getTypeArticle() != null) {
      typeArticle = article.getTypeArticle().getLibelle();
    }
    else {
      typeArticle = "";
    }
    
    stockPhysique = Constantes.formater(article.getQuantitePhysique(), false);
    stockCommande = Constantes.formater(article.getQuantiteReservee(), false);
    stockAttendu = Constantes.formater(article.getQuantiteAttendueUCA(), false);
    stockIdeal = Constantes.formater(article.getQuantiteStockIdealUCA(), false);
    stockMinimum = Constantes.formater(article.getQuantiteStockMinimumUCA(), false);
    // Calcul du stock disponible
    BigDecimal stockdisponible = article.calculerStockDisponible();
    if (stockdisponible == null) {
      stockDisponible = "";
    }
    else {
      stockDisponible = Constantes.formater(stockdisponible, false);
      stockDisponibleInferieurAuMinimum = stockdisponible.compareTo(article.getQuantiteStockMinimumUCA()) < 0;
    }
    // On propose la quantité à commander
    if (article.getPrixAchat() != null) {
      // La quantité à commander est calculée par le service RPG listeArticlesEnRupture
      BigDecimal quantiteUCA = article.getPrixAchat().getQuantiteReliquatUCA();
      quantiteCommandeeUCA = Constantes.formater(quantiteUCA, false);
      if (article.getPrixAchat().getIdUCA() != null) {
        uniteCommandeUCA = article.getPrixAchat().getIdUCA().getCode();
      }
    }
    
    // Si on est arrivé ici alors la ligne est complètement chargée
    charge = true;
  }
  
  /**
   * Contrôle la quantité commandée saisie.
   */
  public void controlerQuantite(String pQuantiteSaisie) {
    if (pQuantiteSaisie == null) {
      return;
    }
    
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pQuantiteSaisie);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("La quantit\u00e9 saisie est invalide, elle doit être sup\u00e9rieure à 0.");
    }
    // On contrôle que la quantité commandée saisie n'entraîne pas un stock disponible supérieur au stock maximum
    BigDecimal stockdisponible = article.calculerStockDisponible();
    if (stockdisponible == null) {
      throw new MessageErreurException("Le stock disponible de cet article ne peut être calcul\u00e9.");
    }
    if (article.getQuantiteStockMaximumUCA() == null) {
      throw new MessageErreurException("Le stock maximum est invalide.");
    }
    
    // On ajoute un contrôle si le stock maximum est supérieur à 0
    // Le stock disponible doit être inférieur ou égal au stock maximum
    BigDecimal quantiteMax = article.getQuantiteStockMaximumUCA();
    if (quantiteMax != null && quantiteMax.compareTo(BigDecimal.ZERO) > 0) {
      stockdisponible = stockdisponible.add(quantite);
      if (stockdisponible.compareTo(article.getQuantiteStockMaximumUCA()) > 0) {
        throw new MessageErreurException("La quantité saisie (" + Constantes.formater(quantite, false)
            + ") est trop grande ce qui entraîne un stock disponible (" + Constantes.formater(stockdisponible, false)
            + ") supérieur au stock maximum (" + Constantes.formater(quantiteMax, false) + ").");
      }
    }
  }
  
  /**
   * Modifie la quantité commandée.
   */
  public void modifierQuantite(BigDecimal pQuantiteSaisie) {
    quantiteCommandeeUCA = "0";
    if (pQuantiteSaisie == null) {
      return;
    }
    
    // On met à jour la quantité saisie dans la classe PrixAchat
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getIdUCA() != null) {
      article.getPrixAchat().setQuantiteReliquatUCA(pQuantiteSaisie, true);
      quantiteCommandeeUCA = Constantes.formater(article.getPrixAchat().getQuantiteReliquatUCA(), false);
    }
    else {
      quantiteCommandeeUCA = Constantes.formater(pQuantiteSaisie, false);
    }
  }
  
  /**
   * Vérifie si l'article à une quantité surpérieure à 0.
   */
  public boolean isArticleACommander() {
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getQuantiteReliquatUCA() != null) {
      return article.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) > 0;
    }
    return false;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public boolean isCharge() {
    return charge;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public void setIdMagasin(IdMagasin idMagasin) {
    this.idMagasin = idMagasin;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public BigDecimal getQuantiteCommandeeUCABigDecimal() {
    return Constantes.convertirTexteEnBigDecimal(Constantes.normerTexte(quantiteCommandeeUCA));
  }
  
  public String getQuantiteCommandeeUCA() {
    return quantiteCommandeeUCA;
  }
  
  public String getUniteCommandeUCA() {
    return uniteCommandeUCA;
  }
  
  public String getTypeArticle() {
    return typeArticle;
  }
  
  public String getStockPhysique() {
    return stockPhysique;
  }
  
  public String getStockCommande() {
    return stockCommande;
  }
  
  public String getStockAttendu() {
    return stockAttendu;
  }
  
  public String getStockDisponible() {
    return stockDisponible;
  }
  
  public LigneAchatArticle getLigneAchatAssociee() {
    return ligneAchatAssociee;
  }
  
  public void setLigneAchatAssociee(LigneAchatArticle ligneAchatAssociee) {
    this.ligneAchatAssociee = ligneAchatAssociee;
  }
  
  public String getStockIdeal() {
    return stockIdeal;
  }
  
  public String getStockMinimum() {
    return stockMinimum;
  }
  
  public boolean isStockDisponibleInferieurAuMinimum() {
    return stockDisponibleInferieurAuMinimum;
  }
}
