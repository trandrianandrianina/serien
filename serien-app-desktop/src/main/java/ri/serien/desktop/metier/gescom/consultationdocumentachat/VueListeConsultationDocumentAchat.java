/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatBase;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran liste de la consultation de documents de achat.
 */
public class VueListeConsultationDocumentAchat extends AbstractVuePanel<ModeleConsultationDocumentAchat> {
  // Constantes
  private static final String BOUTON_AFFICHER_LIGNE = "Afficher lignes avec l'article";
  private static final String BOUTON_AFFICHER_DOCUMENT = "Afficher documents";
  
  private static final String[] TITRE_LISTE_DOCUMENT = new String[] { "Num\u00e9ro", "Date", "Type", "Montant HT", "Montant TTC",
      "Fournisseur", "R\u00e9f\u00e9rence interne", "Acheteur", "Facture", "" };
  private static final String[] TITRE_LISTE_LIGNE =
      new String[] { "Num\u00e9ro", "Qt\u00e9 UCA", "UCA", "Libell\u00e9", "Qt\u00e9 UA", "UA", "Montant HT" };
  
  // Variables
  private DefaultTableModel tableModelDocument = null;
  private DefaultTableModel tableModelLigne = null;
  private JTableCellRendererDocumentAchat listeDocumentsRenderer = new JTableCellRendererDocumentAchat();
  private JTableCellRendererLigneAchat listeLigneRenderer = new JTableCellRendererLigneAchat();
  
  /**
   * Constructeur.
   */
  public VueListeConsultationDocumentAchat(ModeleConsultationDocumentAchat pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Champs de saisie
    tfNumeroDocument.setLongueur(IdDocumentAchat.LONGUEUR_NUMERO_COMPLET);
    tfNumeroLigne.setLongueur(IdDocumentAchat.LONGUEUR_NUMERO_COMPLET);
    
    tableModelDocument = new DefaultTableModel(null, TITRE_LISTE_DOCUMENT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    tableModelLigne = new DefaultTableModel(null, TITRE_LISTE_LIGNE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    // Initialise l'aspect de la table des documents
    scpListeDocument.getViewport().setBackground(Color.WHITE);
    scpListeDocument.setBackground(Color.white);
    // Initialise l'aspect de la table des lignes
    scpListeLigne.getViewport().setBackground(Color.WHITE);
    scpListeLigne.setBackground(Color.white);
    
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (getModele().isModeAffichageDocument()) {
          if (tblListeDocument.getSelectedRowCount() == 0) {
            validerRechercheDocument();
          }
          else {
            validerListeSelectionDocument();
          }
        }
        else {
          if (tblListeLigne.getSelectedRowCount() == 0) {
            validerRechercheLigne();
          }
          else {
            validerListeSelectionLigne();
          }
        }
      }
    };
    
    this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(SNCharteGraphique.TOUCHE_ENTREE, "enter");
    this.getActionMap().put("enter", nouvelleAction);
    tblListeDocument.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    tblListeDocument.getTableHeader().setFont(new Font(tblListeDocument.getTableHeader().getFont().getName(), Font.PLAIN, 14));
    tblListeLigne.getTableHeader().setFont(new Font(tblListeLigne.getTableHeader().getFont().getName(), Font.PLAIN, 14));
    
    tblListeDocument.getTableHeader().addMouseListener(new MouseAdapter() {
      
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlageDocumentsAffiches(0, tblListeDocument.getModel().getRowCount());
        rafraichir();
      }
    });
    
    tblListeLigne.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlageLigneAffiches(0, tblListeDocument.getModel().getRowCount());
        rafraichir();
      }
    });
    
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleActionLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (getModele().isModeAffichageDocument()) {
          if (tblListeDocument.getSelectedRowCount() == 0) {
            validerRechercheDocument();
          }
          else {
            validerListeSelectionDocument();
          }
        }
        else {
          if (tblListeLigne.getSelectedRowCount() == 0) {
            validerRechercheLigne();
          }
          else {
            validerListeSelectionLigne();
          }
        }
      }
    };
    
    this.getActionMap().put("enter", nouvelleActionLigne);
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHaut = retournerActionComposant(tblListeDocument, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBas = retournerActionComposant(tblListeDocument, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHaut = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHaut.actionPerformed(ae);
      }
    };
    Action nouvelleActionFlecheBas = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBas.actionPerformed(ae);
      }
    };
    tblListeDocument.modifierAction(nouvelleActionFlecheHaut, SNCharteGraphique.TOUCHE_HAUT);
    tblListeDocument.modifierAction(nouvelleActionFlecheBas, SNCharteGraphique.TOUCHE_BAS);
    
    tblListeLigne.modifierAction(nouvelleActionLigne, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHautLigne = retournerActionComposant(tblListeLigne, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBasLigne = retournerActionComposant(tblListeLigne, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHautLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHautLigne.actionPerformed(ae);
      }
    };
    Action nouvelleActionFlecheBasLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBasLigne.actionPerformed(ae);
      }
    };
    tblListeLigne.modifierAction(nouvelleActionFlecheHautLigne, SNCharteGraphique.TOUCHE_HAUT);
    tblListeLigne.modifierAction(nouvelleActionFlecheBasLigne, SNCharteGraphique.TOUCHE_BAS);
    
    snPlageDateDocument.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDateLigne.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpListeDocument.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeDocumentsStateChanged(e);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigne = scpListeLigne.getViewport();
    viewportLigne.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeLigneStateChanged(e);
      }
    });
    
    // Configurer la barre de boutons de recherche
    snBarreRechercheDocument.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        traiterClicBoutonDocument(pSNBouton);
      }
    });
    
    snBarreRechercheLigne.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        traiterClicBoutonLigne(pSNBouton);
      }
    });
    
    // Fournisseur
    snAdresseFournisseurDocument.setContexteSaisieDocumentAchat(true);
    snAdresseFournisseurDocument.initialiserRecherche();
    snAdresseFournisseurDocument.addSNComposantListener(new InterfaceSNComposantListener() {
      @Override
      public void valueChanged(SNComposantEvent pSNComposantEvent) {
        getModele().modifierFournisseur(snAdresseFournisseurDocument.getSelection());
      }
    });
    snAdresseFournisseurLigne.setContexteSaisieDocumentAchat(true);
    snAdresseFournisseurLigne.initialiserRecherche();
    snAdresseFournisseurLigne.addSNComposantListener(new InterfaceSNComposantListener() {
      @Override
      public void valueChanged(SNComposantEvent pSNComposantEvent) {
        getModele().modifierFournisseur(snAdresseFournisseurLigne.getSelection());
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirModeRecherche();
    
    // Rafraichissement documents
    if (getModele().isModeAffichageDocument()) {
      // Rafraîchir filtre colonne 1
      rafraichirNumeroDocument();
      rafraichirReferenceClientDocument();
      rafraichirTypeDocument();
      rafraichirFournisseur();
      rafraichirCodePostal();
      rafraichirVille();
      rafraichirArticleDocument();
      
      // Rafraîchir filtre colonne 2
      rafraichirEtablissement();
      rafraichirMagasin();
      rafraichirDateCreationDocument();
    }
    else {
      // Rafraîchir filtre colonne 1
      rafraichirNumeroLigne();
      rafraichirReferenceClientLigne();
      rafraichirTypeDocumentLigne();
      rafraichirFournisseurLigne();
      rafraichirCodePostalLigne();
      rafraichirVilleLigne();
      rafraichirArticleLigne();
      
      // Rafraîchir filtre colonne 2
      rafraichirEtablissementLigne();
      rafraichirMagasinLigne();
      rafraichirDateCreationLigne();
    }
    
    // Rafraîchir tableau
    rafraichirTableauResultat();
    
    // titre résultats liste
    rafraichirTitreResultat();
    
    // Rafraîchir boutons
    rafraichirBoutonConsulter();
    rafraichirBoutonDocument();
    rafraichirBoutonLigne();
    
    // Gérer le focus
    if ((getModele().getListeDocumentAchat() != null) && (!getModele().getListeDocumentAchat().isEmpty())) {
      tblListeDocument.requestFocusInWindow();
    }
  }
  
  private void rafraichirModeRecherche() {
    rbRechercheDocument.setSelected(getModele().isModeAffichageDocument());
    rbRechercheLigne.setSelected(getModele().isModeAffichageLigne());
    pnlDocument.setVisible(getModele().isModeAffichageDocument());
    pnlLigne.setVisible(getModele().isModeAffichageLigne());
  }
  
  private void rafraichirNumeroDocument() {
    String valeur = "";
    if (getModele().getNumeroDocument() > 0) {
      valeur = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocument(), 0);
    }
    tfNumeroDocument.setText(valeur);
    tfNumeroDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirNumeroLigne() {
    String valeur = "";
    if (getModele().getNumeroDocument() > 0) {
      valeur = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocument(), 0);
    }
    tfNumeroLigne.setText(valeur);
    tfNumeroLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirReferenceClientDocument() {
    String valeur = "";
    if (getModele().getReferenceClientDocument() != null) {
      valeur = getModele().getReferenceClientDocument();
    }
    tfReferenceClientDocument.setText(valeur);
    tfReferenceClientDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirReferenceClientLigne() {
    String valeur = "";
    if (getModele().getReferenceClientDocument() != null) {
      valeur = getModele().getReferenceClientDocument();
    }
    tfReferenceClientLigne.setText(valeur);
    tfReferenceClientLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTypeDocument() {
    EnumTypeDocumentAchat type = getModele().getTypeDocument();
    cbTypeDocument.removeAllItems();
    cbTypeDocument.addItem("Tous");
    cbTypeDocument.addItem(EnumTypeDocumentAchat.COMMANDE);
    cbTypeDocument.addItem(EnumTypeDocumentAchat.RECEPTION);
    cbTypeDocument.addItem(EnumTypeDocumentAchat.FACTURE);
    if (type != null) {
      cbTypeDocument.setSelectedItem(type);
    }
  }
  
  private void rafraichirTypeDocumentLigne() {
    EnumTypeDocumentAchat type = getModele().getTypeDocument();
    cbTypeDocumentLigne.removeAllItems();
    cbTypeDocumentLigne.addItem("Tous");
    cbTypeDocumentLigne.addItem(EnumTypeDocumentAchat.COMMANDE);
    cbTypeDocumentLigne.addItem(EnumTypeDocumentAchat.RECEPTION);
    cbTypeDocumentLigne.addItem(EnumTypeDocumentAchat.FACTURE);
    if (type != null) {
      cbTypeDocumentLigne.setSelectedItem(type);
    }
  }
  
  private void rafraichirFournisseur() {
    snAdresseFournisseurDocument.setEnabled(getModele().isDonneesChargees());
    snAdresseFournisseurDocument.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snAdresseFournisseurDocument.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snAdresseFournisseurDocument.setIdEtablissement(null);
    }
    if (getModele().getAdresseFournisseur() == null) {
      snAdresseFournisseurDocument.initialiserRecherche();
    }
    else {
      snAdresseFournisseurDocument.setSelectionParId(getModele().getAdresseFournisseur().getId());
    }
    snAdresseFournisseurDocument.charger(false, false);
  }
  
  private void rafraichirFournisseurLigne() {
    snAdresseFournisseurLigne.setEnabled(getModele().isDonneesChargees());
    snAdresseFournisseurLigne.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snAdresseFournisseurLigne.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snAdresseFournisseurLigne.setIdEtablissement(null);
    }
    if (getModele().getAdresseFournisseur() == null) {
      snAdresseFournisseurLigne.initialiserRecherche();
    }
    else {
      snAdresseFournisseurLigne.setSelectionParId(getModele().getAdresseFournisseur().getId());
    }
    snAdresseFournisseurLigne.charger(false, false);
  }
  
  private void rafraichirCodePostal() {
    AdresseFournisseur fournisseur = getModele().getAdresseFournisseur();
    if (fournisseur == null || fournisseur.getAdresse() == null) {
      tfCodePostalDocument.setText("");
    }
    else {
      if (fournisseur.getAdresse().getCodePostal() != 0) {
        tfCodePostalDocument.setText("" + fournisseur.getAdresse().getCodePostal());
      }
      else {
        tfCodePostalDocument.setText("");
      }
    }
    tfCodePostalDocument.setEnabled(false);
  }
  
  private void rafraichirCodePostalLigne() {
    AdresseFournisseur fournisseur = getModele().getAdresseFournisseur();
    if (fournisseur == null) {
      tfCodePostalLigne.setText("");
    }
    else {
      if (fournisseur.getAdresse().getCodePostal() != 0) {
        tfCodePostalLigne.setText("" + fournisseur.getAdresse().getCodePostal());
      }
      else {
        tfCodePostalLigne.setText("");
      }
    }
    tfCodePostalLigne.setEnabled(false);
  }
  
  private void rafraichirVille() {
    AdresseFournisseur fournisseur = getModele().getAdresseFournisseur();
    if (fournisseur == null || fournisseur.getAdresse() == null) {
      tfVilleDocument.setText("");
    }
    else {
      if (fournisseur.getAdresse().getVille() != null) {
        tfVilleDocument.setText(fournisseur.getAdresse().getVille());
      }
      else {
        tfVilleDocument.setText("");
      }
    }
    tfVilleDocument.setEnabled(false);
  }
  
  private void rafraichirVilleLigne() {
    AdresseFournisseur fournisseur = getModele().getAdresseFournisseur();
    if (fournisseur == null || fournisseur.getAdresse() == null) {
      tfVilleLigne.setText("");
    }
    else {
      if (fournisseur.getAdresse().getVille() != null) {
        tfVilleLigne.setText(fournisseur.getAdresse().getVille());
      }
      else {
        tfVilleLigne.setText("");
      }
    }
    tfVilleLigne.setEnabled(false);
  }
  
  private void rafraichirArticleDocument() {
    snArticleStandardDocument.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snArticleStandardDocument.setIdEtablissement(getModele().getEtablissement().getId());
      snArticleStandardDocument.charger(false);
    }
    
    if (getModele().getIdArticle() == null) {
      snArticleStandardDocument.initialiserRecherche();
    }
    
    snArticleStandardDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirArticleLigne() {
    snArticleStandardLigne.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snArticleStandardLigne.setIdEtablissement(getModele().getEtablissement().getId());
      snArticleStandardLigne.charger(false);
    }
    
    if (getModele().getIdArticle() == null) {
      snArticleStandardLigne.initialiserRecherche();
    }
    
    snArticleStandardLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirEtablissement() {
    // Charger la liste des établissements le premier coup
    if (snEtablissementDocument.isEmpty() && getModele().getListeEtablissement() != null) {
      snEtablissementDocument.setListe(getModele().getListeEtablissement());
    }
    
    if (getModele().getEtablissement() != null) {
      snEtablissementDocument.setSelection(getModele().getEtablissement());
      
    }
    
    snEtablissementDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirEtablissementLigne() {
    // Charger la liste des établissements le premier coup
    if (snEtablissementLigne.isEmpty() && getModele().getListeEtablissement() != null) {
      snEtablissementLigne.setListe(getModele().getListeEtablissement());
    }
    
    if (getModele().getEtablissement() != null) {
      snEtablissementLigne.setSelection(getModele().getEtablissement());
      
    }
    
    snEtablissementLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasin() {
    cbMagasinDocument.removeAllItems();
    if (getModele().getListeMagasin() != null) {
      for (Magasin magagasin : getModele().getListeMagasin()) {
        cbMagasinDocument.addItem(magagasin);
      }
    }
    
    Magasin magasin = null;
    if (getModele().getListeMagasin() != null && getModele().getIdMagasin() != null) {
      magasin = getModele().getListeMagasin().getMagasinParId(getModele().getIdMagasin());
    }
    cbMagasinDocument.setSelectedItem(magasin);
    
    cbMagasinDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasinLigne() {
    cbMagasinLigne.removeAllItems();
    if (getModele().getListeMagasin() != null) {
      for (Magasin magagasin : getModele().getListeMagasin()) {
        cbMagasinLigne.addItem(magagasin);
      }
    }
    
    Magasin magasin = null;
    if (getModele().getListeMagasin() != null && getModele().getIdMagasin() != null) {
      magasin = getModele().getListeMagasin().getMagasinParId(getModele().getIdMagasin());
    }
    cbMagasinLigne.setSelectedItem(magasin);
    
    cbMagasinLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirDateCreationDocument() {
    snPlageDateDocument.setEnabled(isDonneesChargees());
    snPlageDateDocument.setDateDebut(getModele().getDateDebut());
    snPlageDateDocument.setDateFin(getModele().getDateFin());
    
  }
  
  private void rafraichirDateCreationLigne() {
    snPlageDateLigne.setEnabled(isDonneesChargees());
    snPlageDateLigne.setDateDebut(getModele().getDateDebut());
    snPlageDateLigne.setDateFin(getModele().getDateFin());
    
  }
  
  private void rafraichirTitreResultat() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultatDocument.setMessage(getModele().getTitreResultat());
      lblTitreResultatLigne.setMessage(getModele().getTitreResultat());
    }
    else {
      lblTitreResultatDocument.setText("");
      lblTitreResultatLigne.setText("");
    }
  }
  
  private void rafraichirTableauResultat() {
    if (getModele().isModeAffichageDocument()) {
      tableModelDocument.setRowCount(0);
      rafraichirListeDocument();
    }
    else {
      tableModelLigne.setRowCount(0);
      rafraichirListeLigne();
    }
  }
  
  /**
   * valide les données sur documents en validation
   */
  private void validerRechercheDocument() {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
      getModele().modifierReferenceDocument(tfReferenceClientDocument.getText());
      snAdresseFournisseurDocument.charger(true, false);
      getModele().rechercherDocument();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * valide les données sur lignes en validation
   */
  private void validerRechercheLigne() {
    try {
      getModele().modifierNumeroDocument(tfNumeroLigne.getText());
      getModele().modifierReferenceDocument(tfReferenceClientLigne.getText());
      snAdresseFournisseurLigne.charger(true, false);
      getModele().rechercherLigne();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge les données dans la liste des documents.
   */
  public void rafraichirListeDocument() {
    ListeDocumentAchat listeDocumentAchat = getModele().getListeDocumentAchat();
    
    String[][] donnees = null;
    
    if (listeDocumentAchat != null) {
      donnees = new String[listeDocumentAchat.size()][TITRE_LISTE_DOCUMENT.length];
      
      for (int i = 0; i < listeDocumentAchat.size(); i++) {
        DocumentAchat document = listeDocumentAchat.get(i);
        if (document.isCharge()) {
          String[] ligne = donnees[i];
          
          // Numéro du document
          ligne[0] = document.getId().toString();
          
          // Date de création
          ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, document.getDateCreation());
          
          // Type de document
          ligne[2] = document.retournerTypeDocument().getLibelle();
          
          // Montant du document HT
          ligne[3] = Constantes.formater(document.getTotalHTLignes(), true);
          
          // Montant du document TTC
          ligne[4] = Constantes.formater(document.getTotalTTC(), true);
          
          // Nom du fournisseur
          ligne[5] = getModele().getNomFournisseur(document.getIdFournisseur());
          
          // Référence longue
          ligne[6] = document.getReferenceInterne();
          
          // Code vendeur
          if (document.getIdAcheteur() != null) {
            ligne[7] = document.getIdAcheteur().getCode();
          }
          
          // Numéro de facture
          ligne[8] = Constantes.convertirIntegerEnTexte(document.getNumeroFacture(), 6);
          
          // Etat du document (vert = entièrement traité, orange = partiellement traité, blanc = pas traité du tout).
          ligne[9] = document.getCodeEtat().getLibelle();
        }
      }
    }
    
    // Mettre à jour les données
    tableModelDocument.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelDocument.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeDocument.getModel().equals(tableModelDocument)) {
      tblListeDocument.setModel(tableModelDocument);
      tblListeDocument.setGridColor(new Color(204, 204, 204));
      tblListeDocument.setDefaultRenderer(Object.class, listeDocumentsRenderer);
      tblListeDocument.trierColonnes();
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeDocument.getRowCount() == 0) {
      listeDocumentsRenderer.redimensionnerColonnes(tblListeDocument);
    }
    
    int index = getModele().getIndexDocumentSelectionne();
    if (index > -1 && index < tblListeDocument.getRowCount()) {
      tblListeDocument.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  /**
   * Charge les données dans la liste des lignes.
   */
  private void rafraichirListeLigne() {
    // Convertir les données à afficher dans le format attendu par le tableau
    ListeLigneAchatBase listeLigneAchat = getModele().getListeLigneAchat();
    String[][] donnees = null;
    if (listeLigneAchat != null) {
      donnees = new String[listeLigneAchat.size()][TITRE_LISTE_LIGNE.length];
      for (int i = 0; i < listeLigneAchat.size(); i++) {
        LigneAchatBase ligneAchat = listeLigneAchat.get(i);
        if (ligneAchat.isCharge() && !ligneAchat.isLigneCommentaire()) {
          String[] ligne = donnees[i];
          
          // Identifiant de la ligne
          ligne[0] = ligneAchat.getId().toString();
          
          // Quantité UCA
          ligne[1] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUCA(), false);
          
          // Code UCA
          ligne[2] = ligneAchat.getPrixAchat().getIdUCA().getCode();
          
          // Libellé article
          ligne[3] = ligneAchat.getLibelleArticle();
          
          // Quantité en UA
          ligne[4] = Constantes.formater(ligneAchat.getPrixAchat().getQuantiteInitialeUA(), false);
          
          // Code UA
          ligne[5] = ligneAchat.getPrixAchat().getIdUA().getCode();
          
          // Montant HT
          ligne[6] = Constantes.formater(ligneAchat.getPrixAchat().getMontantInitialHT(), true);
        }
      }
    }
    
    // Mettre à jour les données
    tableModelLigne.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelLigne.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeLigne.getModel().equals(tableModelLigne)) {
      tblListeLigne.setModel(tableModelLigne);
      tblListeLigne.setGridColor(new Color(204, 204, 204));
      tblListeLigne.setDefaultRenderer(Object.class, listeLigneRenderer);
      tblListeLigne.trierColonnes();
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeLigne.getRowCount() == 0) {
      listeLigneRenderer.redimensionnerColonnes(tblListeLigne);
    }
    
    int index = getModele().getIndexDocumentSelectionne();
    if (index > -1 && index < tblListeLigne.getRowCount()) {
      tblListeLigne.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  /**
   * Rafraichir le bouton de consultation
   */
  private void rafraichirBoutonConsulter() {
    boolean actif = false;
    if (getModele().isModeAffichageDocument()) {
      actif = isDonneesChargees() && tblListeDocument.getSelectedRow() >= 0;
    }
    else {
      actif = isDonneesChargees() && tblListeLigne.getSelectedRow() >= 0;
    }
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, actif);
  }
  
  /**
   * Rafraichir le bouton d'affichage de la ligne
   */
  private void rafraichirBoutonLigne() {
    
  }
  
  /**
   * Rafraichit le bouton d'affichage du document
   */
  private void rafraichirBoutonDocument() {
    
  }
  
  /**
   * Récupère le document sélectionné.
   */
  private void validerListeSelectionDocument() {
    int indexvisuel = tblListeDocument.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeDocument.getRowSorter() != null) {
        indexreel = tblListeDocument.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierDocumentSelectionne(indexreel);
      getModele().afficherDetailDocument();
    }
  }
  
  /**
   * Récupère la ligne sélectionnée.
   */
  private void validerListeSelectionLigne() {
    int indexvisuel = tblListeLigne.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeLigne.getRowSorter() != null) {
        indexreel = tblListeLigne.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierLigneSelectionne(indexreel);
      getModele().afficherDetailLigne();
    }
  }
  
  /**
   * Selectionne la ligne sur laquelle on a double cliqué.
   */
  private void selectionnerLigne(MouseEvent e) {
    rafraichirBoutonConsulter();
    rafraichirBoutonDocument();
    rafraichirBoutonLigne();
    
    if (e.getClickCount() == 2) {
      if (getModele().isModeAffichageDocument()) {
        validerListeSelectionDocument();
      }
      else {
        validerListeSelectionLigne();
      }
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        if (getModele().isModeAffichageDocument()) {
          validerListeSelectionDocument();
        }
        else {
          validerListeSelectionLigne();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        if (!snAdresseFournisseurDocument.hasFocus()) {
          if (getModele().isModeAffichageDocument()) {
            getModele().rechercherDocument();
          }
          else {
            getModele().rechercherLigne();
          }
        }
      }
      
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().modifierModeAffichage(ModeleConsultationDocumentAchat.MODE_AFFICHAGE_DOCUMENT_LISTE);
        getModele().initialiserRecherche();
        tableModelLigne.setRowCount(0);
        
        rafraichirListeDocument();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void traiterClicBoutonDocument(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercherDocument();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void traiterClicBoutonLigne(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercherLigne();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSeletionnee = tblListeDocument.getSelectedRow();
      if (indexLigneSeletionnee == -1) {
        return;
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeLigne == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeLigne.getViewport().getViewRect();
      int premiereLigne = tblListeLigne.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeLigne.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeLigne.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageLigneAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDocument == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDocument.getViewport().getViewRect();
      int premiereLigne = tblListeDocument.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDocument.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDocument.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDocumentsAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || (e.getStateChange() != ItemEvent.SELECTED)) {
        return;
      }
      getModele().modifierMagasin(cbMagasinDocument.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
    try {
      getModele().modifierReferenceDocument(tfReferenceClientDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
    try {
      selectionnerLigne(e);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissementDocument.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
    try {
      if (cbTypeDocument.getSelectedItem() instanceof EnumTypeDocumentAchat) {
        getModele().modifierTypeDocument((EnumTypeDocumentAchat) cbTypeDocument.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocument(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && (e.getStateChange() == ItemEvent.SELECTED)) {
        getModele().modifierModeRecherche(ModeleConsultationDocumentAchat.MODE_AFFICHAGE_DOCUMENT_LISTE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && (e.getStateChange() == ItemEvent.SELECTED)) {
        getModele().modifierModeRecherche(ModeleConsultationDocumentAchat.MODE_AFFICHAGE_LIGNE_LISTE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroLigneFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroLigne.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
    try {
      getModele().modifierReferenceDocument(tfReferenceClientLigne.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
    try {
      if (cbTypeDocumentLigne.getSelectedItem() instanceof EnumTypeDocumentAchat) {
        getModele().modifierTypeDocument((EnumTypeDocumentAchat) cbTypeDocumentLigne.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocument(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierEtablissement(snEtablissementLigne.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinLigneItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || (e.getStateChange() != ItemEvent.SELECTED)) {
        return;
      }
      getModele().modifierMagasin(cbMagasinLigne.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snArticleStandardLigneValueChanged(SNComposantEvent e) {
    try {
      if (snArticleStandardLigne.getSelection() == null) {
        getModele().modifierArticle(null);
      }
      else {
        getModele().modifierArticle(snArticleStandardLigne.getSelection().getId());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snArticleStandardDocumentValueChanged(SNComposantEvent e) {
    try {
      if (snArticleStandardDocument.getSelection() == null) {
        getModele().modifierArticle(null);
      }
      else {
        getModele().modifierArticle(snArticleStandardDocument.getSelection().getId());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snFournisseurDocumentValueChanged(SNComposantEvent e) {
    try {
      if (snAdresseFournisseurDocument.getSelection() == null) {
        getModele().modifierFournisseur(null);
      }
      else {
        getModele().modifierFournisseur(snAdresseFournisseurDocument.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snFournisseurLigneValueChanged(SNComposantEvent e) {
    try {
      if (snAdresseFournisseurLigne.getSelection() == null) {
        getModele().modifierFournisseur(null);
      }
      else {
        getModele().modifierFournisseur(snAdresseFournisseurLigne.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPlageDateDocumentValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateCreationDebut(snPlageDateDocument.getDateDebut());
      getModele().modifierDateCreationFin(snPlageDateDocument.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateCreationDebut(snPlageDateLigne.getDateDebut());
      getModele().modifierDateCreationFin(snPlageDateLigne.getDateFin());
    }
    catch (
    
    Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlChoixTypeRecherche = new SNPanelTitre();
    rbRechercheDocument = new SNRadioButton();
    rbRechercheLigne = new SNRadioButton();
    pnlContenu = new JPanel();
    pnlDocument = new JPanel();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbNumeroDocument = new SNLabelChamp();
    tfNumeroDocument = new SNIdentifiant();
    lbReferenceClientDocument = new SNLabelChamp();
    tfReferenceClientDocument = new SNTexte();
    lbTypeDocument = new SNLabelChamp();
    cbTypeDocument = new SNComboBox();
    lbFournisseur = new SNLabelChamp();
    snAdresseFournisseurDocument = new SNAdresseFournisseur();
    lbVille = new SNLabelChamp();
    tfCodePostalDocument = new SNTexte();
    tfVilleDocument = new SNTexte();
    lbArticle = new SNLabelChamp();
    snArticleStandardDocument = new SNArticle();
    pnlFiltre2 = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissementDocument = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    cbMagasinDocument = new SNComboBox();
    ldDateCreation = new SNLabelChamp();
    snPlageDateDocument = new SNPlageDate();
    snBarreRechercheDocument = new SNBarreRecherche();
    lblTitreResultatDocument = new SNLabelTitre();
    scpListeDocument = new JScrollPane();
    tblListeDocument = new NRiTable();
    pnlLigne = new JPanel();
    pnlFiltre3 = new JPanel();
    pnlFiltre4 = new JPanel();
    lbNumeroDocument2 = new SNLabelChamp();
    tfNumeroLigne = new SNIdentifiant();
    lbReferenceClientDocument2 = new SNLabelChamp();
    tfReferenceClientLigne = new SNTexte();
    lbTypeDocument2 = new SNLabelChamp();
    cbTypeDocumentLigne = new SNComboBox();
    lbFournisseur2 = new SNLabelChamp();
    snAdresseFournisseurLigne = new SNAdresseFournisseur();
    lbVille2 = new SNLabelChamp();
    tfCodePostalLigne = new SNTexte();
    tfVilleLigne = new SNTexte();
    lbArticle2 = new SNLabelChamp();
    snArticleStandardLigne = new SNArticle();
    pnlFiltre5 = new JPanel();
    lbEtablissement2 = new SNLabelChamp();
    snEtablissementLigne = new SNEtablissement();
    lbMagasin2 = new SNLabelChamp();
    cbMagasinLigne = new SNComboBox();
    ldDateCreation2 = new SNLabelChamp();
    snPlageDateLigne = new SNPlageDate();
    snBarreRechercheLigne = new SNBarreRecherche();
    lblTitreResultatLigne = new SNLabelTitre();
    scpListeLigne = new JScrollPane();
    tblListeLigne = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlChoixTypeRecherche ========
    {
      pnlChoixTypeRecherche.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 0, 10),
          new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14))));
      pnlChoixTypeRecherche.setName("pnlChoixTypeRecherche");
      pnlChoixTypeRecherche.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- rbRechercheDocument ----
      rbRechercheDocument.setText("Recherche de documents d'achat");
      rbRechercheDocument.setPreferredSize(new Dimension(350, 30));
      rbRechercheDocument.setMinimumSize(new Dimension(350, 30));
      rbRechercheDocument.setName("rbRechercheDocument");
      rbRechercheDocument.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheDocumentItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheDocument,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- rbRechercheLigne ----
      rbRechercheLigne.setText("Recherche de lignes d'achat");
      rbRechercheLigne.setPreferredSize(new Dimension(350, 30));
      rbRechercheLigne.setMinimumSize(new Dimension(350, 30));
      rbRechercheLigne.setName("rbRechercheLigne");
      rbRechercheLigne.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheLigneItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheLigne,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlChoixTypeRecherche, BorderLayout.PAGE_START);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new CardLayout());
      
      // ======== pnlDocument ========
      {
        pnlDocument.setOpaque(false);
        pnlDocument.setName("pnlDocument");
        pnlDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDocument.getLayout()).columnWidths = new int[] { 1205, 0 };
        ((GridBagLayout) pnlDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlDocument.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setOpaque(false);
          pnlFiltre.setBorder(new TitledBorder(""));
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre1 ========
          {
            pnlFiltre1.setOpaque(false);
            pnlFiltre1.setName("pnlFiltre1");
            pnlFiltre1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 188, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument ----
            lbNumeroDocument.setText("Num\u00e9ro document");
            lbNumeroDocument.setName("lbNumeroDocument");
            pnlFiltre1.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroDocument ----
            tfNumeroDocument.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroDocument.setName("tfNumeroDocument");
            tfNumeroDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbReferenceClientDocument ----
            lbReferenceClientDocument.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocument.setName("lbReferenceClientDocument");
            pnlFiltre1.add(lbReferenceClientDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientDocument ----
            tfReferenceClientDocument.setName("tfReferenceClientDocument");
            tfReferenceClientDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfReferenceClientDocument, new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTypeDocument ----
            lbTypeDocument.setText("Type document");
            lbTypeDocument.setName("lbTypeDocument");
            pnlFiltre1.add(lbTypeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocument ----
            cbTypeDocument.setMinimumSize(new Dimension(150, 30));
            cbTypeDocument.setPreferredSize(new Dimension(150, 30));
            cbTypeDocument.setName("cbTypeDocument");
            cbTypeDocument.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentItemStateChanged(e);
              }
            });
            pnlFiltre1.add(cbTypeDocument, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlFiltre1.add(lbFournisseur, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAdresseFournisseurDocument ----
            snAdresseFournisseurDocument.setName("snAdresseFournisseurDocument");
            pnlFiltre1.add(snAdresseFournisseurDocument, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVille ----
            lbVille.setText("Ville");
            lbVille.setName("lbVille");
            pnlFiltre1.add(lbVille, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfCodePostalDocument ----
            tfCodePostalDocument.setEnabled(false);
            tfCodePostalDocument.setName("tfCodePostalDocument");
            pnlFiltre1.add(tfCodePostalDocument, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfVilleDocument ----
            tfVilleDocument.setEnabled(false);
            tfVilleDocument.setName("tfVilleDocument");
            pnlFiltre1.add(tfVilleDocument, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setName("lbArticle");
            pnlFiltre1.add(lbArticle, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleStandardDocument ----
            snArticleStandardDocument.setName("snArticleStandardDocument");
            snArticleStandardDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleStandardDocumentValueChanged(e);
              }
            });
            pnlFiltre1.add(snArticleStandardDocument, new GridBagConstraints(1, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre1);
          
          // ======== pnlFiltre2 ========
          {
            pnlFiltre2.setOpaque(false);
            pnlFiltre2.setName("pnlFiltre2");
            pnlFiltre2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlFiltre2.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissementDocument ----
            snEtablissementDocument.setName("snEtablissementDocument");
            snEtablissementDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlFiltre2.add(snEtablissementDocument, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlFiltre2.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasinDocument ----
            cbMagasinDocument.setName("cbMagasinDocument");
            cbMagasinDocument.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbMagasinItemStateChanged(e);
              }
            });
            pnlFiltre2.add(cbMagasinDocument, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ldDateCreation ----
            ldDateCreation.setText("Date de cr\u00e9ation");
            ldDateCreation.setName("ldDateCreation");
            pnlFiltre2.add(ldDateCreation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateDocument ----
            snPlageDateDocument.setName("snPlageDateDocument");
            snPlageDateDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPlageDateDocumentValueChanged(e);
              }
            });
            pnlFiltre2.add(snPlageDateDocument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRechercheDocument ----
            snBarreRechercheDocument.setName("snBarreRechercheDocument");
            pnlFiltre2.add(snBarreRechercheDocument, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre2);
        }
        pnlDocument.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultatDocument ----
        lblTitreResultatDocument.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultatDocument.setName("lblTitreResultatDocument");
        pnlDocument.add(lblTitreResultatDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocument ========
        {
          scpListeDocument.setPreferredSize(new Dimension(1050, 424));
          scpListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeDocument.setName("scpListeDocument");
          
          // ---- tblListeDocument ----
          tblListeDocument.setShowVerticalLines(true);
          tblListeDocument.setShowHorizontalLines(true);
          tblListeDocument.setBackground(Color.white);
          tblListeDocument.setRowHeight(20);
          tblListeDocument.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocument.setAutoCreateRowSorter(true);
          tblListeDocument.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocument.setGridColor(new Color(204, 204, 204));
          tblListeDocument.setName("tblListeDocument");
          tblListeDocument.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocument.setViewportView(tblListeDocument);
        }
        pnlDocument.add(scpListeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDocument, "card1");
      
      // ======== pnlLigne ========
      {
        pnlLigne.setOpaque(false);
        pnlLigne.setName("pnlLigne");
        pnlLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlLigne.getLayout()).columnWidths = new int[] { 1205, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlLigne.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlLigne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre3 ========
        {
          pnlFiltre3.setOpaque(false);
          pnlFiltre3.setBorder(new TitledBorder(""));
          pnlFiltre3.setName("pnlFiltre3");
          pnlFiltre3.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre4 ========
          {
            pnlFiltre4.setOpaque(false);
            pnlFiltre4.setName("pnlFiltre4");
            pnlFiltre4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWidths = new int[] { 0, 0, 188, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument2 ----
            lbNumeroDocument2.setText("Num\u00e9ro document");
            lbNumeroDocument2.setName("lbNumeroDocument2");
            pnlFiltre4.add(lbNumeroDocument2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroLigne ----
            tfNumeroLigne.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroLigne.setName("tfNumeroLigne");
            tfNumeroLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfNumeroLigne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbReferenceClientDocument2 ----
            lbReferenceClientDocument2.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocument2.setName("lbReferenceClientDocument2");
            pnlFiltre4.add(lbReferenceClientDocument2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientLigne ----
            tfReferenceClientLigne.setName("tfReferenceClientLigne");
            tfReferenceClientLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfReferenceClientLigne, new GridBagConstraints(1, 1, 3, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbTypeDocument2 ----
            lbTypeDocument2.setText("Type document");
            lbTypeDocument2.setName("lbTypeDocument2");
            pnlFiltre4.add(lbTypeDocument2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocumentLigne ----
            cbTypeDocumentLigne.setMinimumSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setPreferredSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setName("cbTypeDocumentLigne");
            cbTypeDocumentLigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentLigneItemStateChanged(e);
              }
            });
            pnlFiltre4.add(cbTypeDocumentLigne, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbFournisseur2 ----
            lbFournisseur2.setText("Fournisseur");
            lbFournisseur2.setName("lbFournisseur2");
            pnlFiltre4.add(lbFournisseur2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snAdresseFournisseurLigne ----
            snAdresseFournisseurLigne.setName("snAdresseFournisseurLigne");
            pnlFiltre4.add(snAdresseFournisseurLigne, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVille2 ----
            lbVille2.setText("Ville");
            lbVille2.setName("lbVille2");
            pnlFiltre4.add(lbVille2, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfCodePostalLigne ----
            tfCodePostalLigne.setEnabled(false);
            tfCodePostalLigne.setName("tfCodePostalLigne");
            pnlFiltre4.add(tfCodePostalLigne, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfVilleLigne ----
            tfVilleLigne.setEnabled(false);
            tfVilleLigne.setName("tfVilleLigne");
            pnlFiltre4.add(tfVilleLigne, new GridBagConstraints(2, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbArticle2 ----
            lbArticle2.setText("Article");
            lbArticle2.setName("lbArticle2");
            pnlFiltre4.add(lbArticle2, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleStandardLigne ----
            snArticleStandardLigne.setName("snArticleStandardLigne");
            snArticleStandardLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleStandardLigneValueChanged(e);
              }
            });
            pnlFiltre4.add(snArticleStandardLigne, new GridBagConstraints(1, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre4);
          
          // ======== pnlFiltre5 ========
          {
            pnlFiltre5.setOpaque(false);
            pnlFiltre5.setName("pnlFiltre5");
            pnlFiltre5.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement2 ----
            lbEtablissement2.setText("Etablissement");
            lbEtablissement2.setName("lbEtablissement2");
            pnlFiltre5.add(lbEtablissement2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissementLigne ----
            snEtablissementLigne.setName("snEtablissementLigne");
            snEtablissementLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snEtablissementLigne, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasin2 ----
            lbMagasin2.setText("Magasin");
            lbMagasin2.setName("lbMagasin2");
            pnlFiltre5.add(lbMagasin2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasinLigne ----
            cbMagasinLigne.setName("cbMagasinLigne");
            cbMagasinLigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbMagasinLigneItemStateChanged(e);
              }
            });
            pnlFiltre5.add(cbMagasinLigne, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ldDateCreation2 ----
            ldDateCreation2.setText("Date de cr\u00e9ation");
            ldDateCreation2.setName("ldDateCreation2");
            pnlFiltre5.add(ldDateCreation2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateLigne ----
            snPlageDateLigne.setName("snPlageDateLigne");
            snPlageDateLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPlageDateLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snPlageDateLigne, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRechercheLigne ----
            snBarreRechercheLigne.setName("snBarreRechercheLigne");
            pnlFiltre5.add(snBarreRechercheLigne, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre5);
        }
        pnlLigne.add(pnlFiltre3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultatLigne ----
        lblTitreResultatLigne.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultatLigne.setName("lblTitreResultatLigne");
        pnlLigne.add(lblTitreResultatLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeLigne ========
        {
          scpListeLigne.setPreferredSize(new Dimension(500, 424));
          scpListeLigne.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeLigne.setMaximumSize(new Dimension(32767, 240));
          scpListeLigne.setName("scpListeLigne");
          
          // ---- tblListeLigne ----
          tblListeLigne.setShowVerticalLines(true);
          tblListeLigne.setShowHorizontalLines(true);
          tblListeLigne.setBackground(Color.white);
          tblListeLigne.setRowHeight(20);
          tblListeLigne.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeLigne.setAutoCreateRowSorter(true);
          tblListeLigne.setSelectionBackground(new Color(57, 105, 138));
          tblListeLigne.setGridColor(new Color(204, 204, 204));
          tblListeLigne.setMaximumSize(new Dimension(200, 400));
          tblListeLigne.setPreferredScrollableViewportSize(new Dimension(200, 400));
          tblListeLigne.setPreferredSize(new Dimension(400, 400));
          tblListeLigne.setName("tblListeLigne");
          tblListeLigne.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeLigne.setViewportView(tblListeLigne);
        }
        pnlLigne.add(scpListeLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlLigne, "card2");
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.PAGE_END);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbRechercheDocument);
    buttonGroup1.add(rbRechercheLigne);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelTitre pnlChoixTypeRecherche;
  private SNRadioButton rbRechercheDocument;
  private SNRadioButton rbRechercheLigne;
  private JPanel pnlContenu;
  private JPanel pnlDocument;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbNumeroDocument;
  private SNIdentifiant tfNumeroDocument;
  private SNLabelChamp lbReferenceClientDocument;
  private SNTexte tfReferenceClientDocument;
  private SNLabelChamp lbTypeDocument;
  private SNComboBox cbTypeDocument;
  private SNLabelChamp lbFournisseur;
  private SNAdresseFournisseur snAdresseFournisseurDocument;
  private SNLabelChamp lbVille;
  private SNTexte tfCodePostalDocument;
  private SNTexte tfVilleDocument;
  private SNLabelChamp lbArticle;
  private SNArticle snArticleStandardDocument;
  private JPanel pnlFiltre2;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissementDocument;
  private SNLabelChamp lbMagasin;
  private SNComboBox cbMagasinDocument;
  private SNLabelChamp ldDateCreation;
  private SNPlageDate snPlageDateDocument;
  private SNBarreRecherche snBarreRechercheDocument;
  private SNLabelTitre lblTitreResultatDocument;
  private JScrollPane scpListeDocument;
  private NRiTable tblListeDocument;
  private JPanel pnlLigne;
  private JPanel pnlFiltre3;
  private JPanel pnlFiltre4;
  private SNLabelChamp lbNumeroDocument2;
  private SNIdentifiant tfNumeroLigne;
  private SNLabelChamp lbReferenceClientDocument2;
  private SNTexte tfReferenceClientLigne;
  private SNLabelChamp lbTypeDocument2;
  private SNComboBox cbTypeDocumentLigne;
  private SNLabelChamp lbFournisseur2;
  private SNAdresseFournisseur snAdresseFournisseurLigne;
  private SNLabelChamp lbVille2;
  private SNTexte tfCodePostalLigne;
  private SNTexte tfVilleLigne;
  private SNLabelChamp lbArticle2;
  private SNArticle snArticleStandardLigne;
  private JPanel pnlFiltre5;
  private SNLabelChamp lbEtablissement2;
  private SNEtablissement snEtablissementLigne;
  private SNLabelChamp lbMagasin2;
  private SNComboBox cbMagasinLigne;
  private SNLabelChamp ldDateCreation2;
  private SNPlageDate snPlageDateLigne;
  private SNBarreRecherche snBarreRechercheLigne;
  private SNLabelTitre lblTitreResultatLigne;
  private JScrollPane scpListeLigne;
  private NRiTable tblListeLigne;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
