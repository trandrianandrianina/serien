/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.carnetboncour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueListeCarnetBonCour extends AbstractVuePanel<ModeleCarnetBonCour> {
  // Constantes
  private static final String BOUTON_CREER_CARNET = "Créer carnet";
  private static final String BOUTON_MODIFIER_CARNET = "Modifier carnet";
  private static final String[] TITRE_LISTE_CARNET =
      new String[] { "Num\u00e9ro", "Date", "Magasinier", "Nombre de pages", "Bon de début", "Bon de fin" };
  
  // Variables
  private DefaultTableModel tableModelDocument = null;
  
  // Classe interne qui permet de gérer les flèches dans la table tblListe
  private class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;
    
    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      validerSelectionLigne(e, actionOriginale);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueListeCarnetBonCour(ModeleCarnetBonCour pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Champs de saisie
    tfNumeroCarnet.setLongueur(IdCarnetBonCour.LONGUEUR_NUMERO);
    tfNumeroBonCour.setLongueur(IdBonCour.LONGUEUR_NUMERO);
    
    // La liste
    scpListeCarnets.getViewport().setBackground(Color.WHITE);
    tblListeCarnets.personnaliserAspect(TITRE_LISTE_CARNET, new int[] { 60, 75, 600, 80, 60, 60 },
        new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE }, 14);
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListeCarnets, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListeCarnets.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListeCarnets.getActionMap().put(actionEnter, actionCelluleEnter);
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListeCarnets.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeCarnetsStateChanged(e);
      }
    });
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BOUTON_CREER_CARNET, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_CARNET, 'm', false);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    tfNumeroCarnet.requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir filtre colonne de gauche
    rafraichirNumeroCarnet();
    rafraichirMagasinier();
    rafraichirNumeroBonDeCour();
    
    // Rafraîchir filtre colonne de droite
    rafraichirEtablissement();
    rafraichirMagasin();
    
    // Rafraîchir tableau
    rafraichirTitreResultatDocument();
    rafraichirTableauResultatDocument();
    
    // Rafraîchir boutons
    rafraichirBoutonModifier();
    rafraichirBoutonConsulter();
  }
  
  private void rafraichirNumeroCarnet() {
    String numero = "";
    if (getModele().getNumeroCarnetARecherher() != null && getModele().getNumeroCarnetARecherher() > 0) {
      numero = Constantes.convertirIntegerEnTexte(getModele().getNumeroCarnetARecherher(), 0);
    }
    tfNumeroCarnet.setText(numero);
    tfNumeroCarnet.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasinier() {
    cbMagasinier.setSession(getModele().getSession());
    cbMagasinier.setIdEtablissement(getModele().getIdEtablissement());
    cbMagasinier.setTousAutorise(true);
    cbMagasinier.setListe(getModele().getListeMagasinier());
    cbMagasinier.setIdSelection(getModele().getIdMagasinierARechercher());
    cbMagasinier.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirNumeroBonDeCour() {
    String numero = "";
    if (getModele().getNumeroBonDeCourARecherher() != null && getModele().getNumeroBonDeCourARecherher() > 0) {
      numero = Constantes.convertirIntegerEnTexte(getModele().getNumeroBonDeCourARecherher(), 0);
    }
    tfNumeroBonCour.setText(numero);
    tfNumeroBonCour.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirEtablissement() {
    // Charger la liste des établissements le premier coup
    ListeEtablissement listeEtablissement = getModele().getListeEtablissement();
    if (snEtablissement.isEmpty() && listeEtablissement != null) {
      snEtablissement.setListe(listeEtablissement);
    }
    if (getModele().getIdEtablissement() != null) {
      snEtablissement.setIdSelection(getModele().getIdEtablissement());
    }
    snEtablissement.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasin() {
    ListeMagasin listeMagasin = getModele().getListeMagasin();
    cbMagasin.setListe(listeMagasin);
    cbMagasin.setIdSelection(getModele().getIdMagasin());
    
    cbMagasin.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTitreResultatDocument() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultat.setMessage(getModele().getTitreResultat());
    }
    else {
      lblTitreResultat.setText("Carnets correspondant à votre recherche");
    }
  }
  
  private void rafraichirTableauResultatDocument() {
    ListeCarnetBonCour listeCarnet = getModele().getListeCarnet();
    String[][] donnees = null;
    
    if (listeCarnet != null) {
      donnees = new String[listeCarnet.size()][TITRE_LISTE_CARNET.length];
      for (int i = 0; i < listeCarnet.size(); i++) {
        CarnetBonCour carnet = listeCarnet.get(i);
        String[] ligne = donnees[i];
        
        // Numéro du carnet
        ligne[0] = Constantes.convertirIntegerEnTexte(carnet.getId().getNumero(), 0);
        
        // Date de création
        if (carnet.getDateRemise() != null) {
          ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, carnet.getDateRemise());
        }
        else {
          ligne[1] = "";
        }
        
        // Magasinier
        String valeur = getModele().retournerNomMagasinier(carnet.getIdMagasinier());
        if (valeur != null) {
          ligne[2] = valeur;
        }
        else {
          ligne[2] = "";
        }
        
        // Nombre de page
        if (carnet.getNombrePages() != null) {
          ligne[3] = Constantes.convertirIntegerEnTexte(carnet.getNombrePages(), 0);
        }
        else {
          ligne[3] = "";
        }
        
        // Numéro du premier bon de cour
        if (carnet.getPremierNumero() != null) {
          ligne[4] = Constantes.convertirIntegerEnTexte(carnet.getPremierNumero(), 0);
        }
        else {
          ligne[4] = "";
        }
        
        // Numéro du dernier bon de cour
        if (carnet.getDernierNumero() != null) {
          ligne[5] = Constantes.convertirIntegerEnTexte(carnet.getDernierNumero(), 0);
        }
        else {
          ligne[5] = "";
        }
      }
    }
    tblListeCarnets.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours
    int index = getModele().getIndexCarnetSelectionne();
    if (index > -1 && index < tblListeCarnets.getRowCount()) {
      tblListeCarnets.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  /**
   * Afficher le bouton Modifier.
   */
  private void rafraichirBoutonModifier() {
    snBarreBouton.activerBouton(BOUTON_MODIFIER_CARNET, isDonneesChargees() && tblListeCarnets.getSelectedRow() >= 0);
  }
  
  /**
   * Afficher le bouton Consulter.
   */
  private void rafraichirBoutonConsulter() {
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, isDonneesChargees() && tblListeCarnets.getSelectedRow() >= 0);
  }
  
  /**
   * Récupère le carnet sélectionné.
   */
  private void validerListeSelectionCarnet() {
    getModele().modifierCarnetSelectionne(tblListeCarnets.getIndiceSelection());
    getModele().afficherDetailCarnetPourConsultation();
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale) {
    try {
      validerListeSelectionCarnet();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_CREER_CARNET)) {
        getModele().afficherDetailCarnetPourCreation();
      }
      if (pSNBouton.isBouton(BOUTON_MODIFIER_CARNET)) {
        getModele().afficherDetailCarnetPourModification();
      }
      else if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        validerListeSelectionCarnet();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher(true);
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSeletionnee = tblListeCarnets.getSelectedRow();
      if (indexLigneSeletionnee == -1) {
        return;
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeCarnetsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeCarnets == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeCarnets.getViewport().getViewRect();
      int premiereLigne = tblListeCarnets.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeCarnets.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeCarnets.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageCarnetAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat des documents.
   */
  private void tblListeCarnetselectionChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierCarnetSelectionne(tblListeCarnets.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroCarnetFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroCarnetARechercher(tfNumeroCarnet.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinierValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasinierARechercher(cbMagasinier.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroBonCourFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroBonDeCourARechercher(tfNumeroBonCour.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeCarnetsMouseClicked(MouseEvent e) {
    try {
      getModele().modifierCarnetSelectionne(tblListeCarnets.getSelectedRow());
      
      if (e.getClickCount() == 2) {
        validerListeSelectionCarnet();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissement.getSelection().getId());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void cbMagasinValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasin(cbMagasin.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeCarnets == null) {
        return;
      }
      Trace.debug(VueListeCarnetBonCour.class, "scpListeDocumentsStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeCarnets.getViewport().getViewRect();
      int premiereLigne = tblListeCarnets.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeCarnets.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeCarnets.getRowCount() - 1;
      }
      
      // Charges les lignes d'achat concernées si besoin
      getModele().modifierPlageCarnetAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroCarnetActionPerformed(ActionEvent e) {
    try {
      getModele().modifierNumeroCarnetARechercher(tfNumeroCarnet.getText());
      getModele().rechercher(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroBonCourActionPerformed(ActionEvent e) {
    try {
      getModele().modifierNumeroBonDeCourARechercher(tfNumeroBonCour.getText());
      getModele().rechercher(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlFiltre = new SNPanel();
    pnlFiltreGauche = new SNPanel();
    lbNumeroCarnet = new SNLabelChamp();
    tfNumeroCarnet = new SNIdentifiant();
    lbMagasinier = new SNLabelChamp();
    cbMagasinier = new SNVendeur();
    lbNumeroBonCour = new SNLabelChamp();
    tfNumeroBonCour = new SNIdentifiant();
    pnlFiltreDroite = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    cbMagasin = new SNMagasin();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    scpListeCarnets = new JScrollPane();
    tblListeCarnets = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlFiltre ========
      {
        pnlFiltre.setOpaque(false);
        pnlFiltre.setBorder(new TitledBorder(""));
        pnlFiltre.setName("pnlFiltre");
        pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
        
        // ======== pnlFiltreGauche ========
        {
          pnlFiltreGauche.setOpaque(false);
          pnlFiltreGauche.setName("pnlFiltreGauche");
          pnlFiltreGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNumeroCarnet ----
          lbNumeroCarnet.setText("Num\u00e9ro de carnet");
          lbNumeroCarnet.setName("lbNumeroCarnet");
          pnlFiltreGauche.add(lbNumeroCarnet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroCarnet ----
          tfNumeroCarnet.setToolTipText("Filtrer sur un num\u00e9ro du carnet des bons de cour");
          tfNumeroCarnet.setName("tfNumeroCarnet");
          tfNumeroCarnet.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroCarnetFocusLost(e);
            }
          });
          tfNumeroCarnet.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tfNumeroCarnetActionPerformed(e);
            }
          });
          pnlFiltreGauche.add(tfNumeroCarnet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasinier ----
          lbMagasinier.setText("Magasinier");
          lbMagasinier.setName("lbMagasinier");
          pnlFiltreGauche.add(lbMagasinier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbMagasinier ----
          cbMagasinier.setToolTipText("Filtrer sur le magasinier");
          cbMagasinier.setName("cbMagasinier");
          cbMagasinier.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              cbMagasinierValueChanged(e);
            }
          });
          pnlFiltreGauche.add(cbMagasinier, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNumeroBonCour ----
          lbNumeroBonCour.setText("Num\u00e9ro de bon de cour");
          lbNumeroBonCour.setPreferredSize(new Dimension(180, 30));
          lbNumeroBonCour.setName("lbNumeroBonCour");
          pnlFiltreGauche.add(lbNumeroBonCour, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroBonCour ----
          tfNumeroBonCour.setToolTipText("Filtrer sur un num\u00e9ro du bon de cour");
          tfNumeroBonCour.setName("tfNumeroBonCour");
          tfNumeroBonCour.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroBonCourFocusLost(e);
            }
          });
          tfNumeroBonCour.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tfNumeroBonCourActionPerformed(e);
            }
          });
          pnlFiltreGauche.add(tfNumeroBonCour, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltreGauche);
        
        // ======== pnlFiltreDroite ========
        {
          pnlFiltreDroite.setOpaque(false);
          pnlFiltreDroite.setName("pnlFiltreDroite");
          pnlFiltreDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltreDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlFiltreDroite.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlFiltreDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlFiltreDroite.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbMagasin ----
          cbMagasin.setName("cbMagasin");
          cbMagasin.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              cbMagasinValueChanged(e);
            }
          });
          pnlFiltreDroite.add(cbMagasin, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          pnlFiltreDroite.add(snBarreRecherche, new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltreDroite);
      }
      pnlContenu.add(pnlFiltre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lblTitreResultat ----
      lblTitreResultat.setText("Carnets correspondant \u00e0 votre recherche");
      lblTitreResultat.setName("lblTitreResultat");
      pnlContenu.add(lblTitreResultat,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeCarnets ========
      {
        scpListeCarnets.setPreferredSize(new Dimension(1050, 424));
        scpListeCarnets.setFont(new Font("sansserif", Font.PLAIN, 14));
        scpListeCarnets.setName("scpListeCarnets");
        
        // ---- tblListeCarnets ----
        tblListeCarnets.setShowVerticalLines(true);
        tblListeCarnets.setShowHorizontalLines(true);
        tblListeCarnets.setBackground(Color.white);
        tblListeCarnets.setRowHeight(20);
        tblListeCarnets.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblListeCarnets.setAutoCreateRowSorter(true);
        tblListeCarnets.setSelectionBackground(new Color(57, 105, 138));
        tblListeCarnets.setGridColor(new Color(204, 204, 204));
        tblListeCarnets.setName("tblListeCarnets");
        tblListeCarnets.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblListeCarnetsMouseClicked(e);
          }
        });
        scpListeCarnets.setViewportView(tblListeCarnets);
      }
      pnlContenu.add(scpListeCarnets,
          new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlFiltre;
  private SNPanel pnlFiltreGauche;
  private SNLabelChamp lbNumeroCarnet;
  private SNIdentifiant tfNumeroCarnet;
  private SNLabelChamp lbMagasinier;
  private SNVendeur cbMagasinier;
  private SNLabelChamp lbNumeroBonCour;
  private SNIdentifiant tfNumeroBonCour;
  private SNPanel pnlFiltreDroite;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin cbMagasin;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JScrollPane scpListeCarnets;
  private NRiTable tblListeCarnets;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
