/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;

import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue détail de la configuration d'un catalogue fournisseur.
 * 
 * On y détermine le fournisseur, un nom, le fichier CSV catalogue du fournisseur et l'association entre des champs de l'ERP et les champs
 * du fichier CSV
 */
public class VueDetailConfigurationCatalogue extends AbstractVuePanel<ModeleConfigurationCatalogue> {
  private static final String BOUTON_SUPPRIMER = "Supprimer";
  
  /**
   * Constructeur.
   */
  public VueDetailConfigurationCatalogue(ModeleConfigurationCatalogue pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    // Configurer la barre de boutons
    snBarreBouton.setModeNavigation(true);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirFournisseur();
    rafraichirFichierCSV();
    rafraichirLibelle();
    rafraichirChamps();
    
    rafraichirBoutonFermer();
    rafraichirBoutonValider();
    rafraichirBoutonAnnuler();
    rafraichirBoutonPrecedent();
    rafraichirBoutonSuivant();
    rafraichirBoutonNavigation();
  }
  
  /**
   * Rafraichir la liste des fournisseurs.
   */
  private void rafraichirFournisseur() {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    // Charger la liste des fournisseurs
    snFournisseur.setSession(getModele().getSession());
    if (configurationCatalogue != null && configurationCatalogue.getIdEtablissement() != null) {
      snFournisseur.setIdEtablissement(configurationCatalogue.getIdEtablissement());
    }
    else {
      snFournisseur.setIdEtablissement(getModele().getIdEtablissement());
    }
    snFournisseur.charger(false);
    
    // Sélectionner le fournisseur
    if (configurationCatalogue != null) {
      snFournisseur.setSelectionParId(configurationCatalogue.getIdFournisseur());
    }
    else {
      snFournisseur.setSelectionParId(null);
    }
    
    // Gérer l'activation du champ
    snFournisseur.setEnabled(isDonneesChargees());
  }
  
  /**
   * Rafraichir la liste des fichiers CSV.
   */
  private void rafraichirFichierCSV() {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    cbFichier.setEnabled(false);
    cbFichier.removeAllItems();
    cbFichier.addItem(" ");
    if (getModele().getListeFichierCSV() != null) {
      for (String fichierCSV : getModele().getListeFichierCSV()) {
        cbFichier.addItem(fichierCSV);
      }
    }
    
    if (configurationCatalogue != null && configurationCatalogue.getFichierCSV() != null) {
      cbFichier.setSelectedItem(configurationCatalogue.getFichierCSV());
    }
    if (isDonneesChargees()) {
      cbFichier.setEnabled(true);
    }
  }
  
  /**
   * Rafraichir le libellé de la configuration de catalogue
   */
  private void rafraichirLibelle() {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    tfLibelleConfig.setEnabled(isDonneesChargees());
    
    String valeur = "";
    if (configurationCatalogue != null && configurationCatalogue.getLibelle() != null) {
      valeur = configurationCatalogue.getLibelle();
    }
    
    if (!tfLibelleConfig.getText().equals(valeur)) {
      tfLibelleConfig.setText(valeur);
    }
  }
  
  /**
   * Rafraichir le panel des champs catalogue
   */
  private void rafraichirChamps() {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    pnlChampsCatalogue.removeAll();
    if (configurationCatalogue != null) {
      if (configurationCatalogue.getFichierCSV() != null && getModele().getListeChampERP() != null) {
        int ligne = 0;
        for (final ChampERP champERP : getModele().getListeChampERP()) {
          
          JPanel panel = new JPanel();
          panel.setName("panel" + ligne);
          panel.setOpaque(false);
          panel.setLayout(new GridBagLayout());
          ((GridBagLayout) panel.getLayout()).columnWidths = new int[] { 0, 40, 250, 400, 32, 500 };
          ((GridBagLayout) panel.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) panel.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) panel.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          if (ligne == 0) {
            JLabel lbColonne1 = new JLabel();
            lbColonne1.setText("Zone ERP");
            lbColonne1.setName("lbColonne1");
            lbColonne1.setPreferredSize(new Dimension(250, 30));
            panel.add(lbColonne1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 10), 0, 0));
            JLabel lbColonne2 = new JLabel();
            lbColonne2.setText("Zone fichier CSV");
            lbColonne2.setName("lbColonne2");
            lbColonne2.setPreferredSize(new Dimension(400, 30));
            panel.add(lbColonne2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 10), 0, 0));
            JLabel lbColonne3 = new JLabel();
            lbColonne3.setText("Exemple valeur catalogue");
            lbColonne3.setName("lbColonne3");
            lbColonne3.setPreferredSize(new Dimension(500, 30));
            panel.add(lbColonne3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 10), 0, 0));
          }
          ligne++;
          
          // Indice de la colonne du fichier CSV
          JLabel lbIndice = new JLabel();
          lbIndice.setText(afficherIndiceColonneCSV(ligne));
          lbIndice.setHorizontalAlignment(SwingConstants.RIGHT);
          lbIndice.setName("label" + ligne);
          panel.add(lbIndice, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 7, 10), 0, 0));
          
          // Nom du champ ERP
          RiZoneSortie riZoneSortie = new RiZoneSortie();
          riZoneSortie.setText(champERP.getLibelleLong());
          riZoneSortie.setFont(new Font("sansserif", Font.PLAIN, 14));
          riZoneSortie.setPreferredSize(new Dimension(250, 30));
          riZoneSortie.setMinimumSize(new Dimension(250, 30));
          riZoneSortie.setMaximumSize(new Dimension(250, 30));
          riZoneSortie.setName("riZoneSortie" + ligne);
          panel.add(riZoneSortie, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 7, 10), 0, 0));
          
          if (configurationCatalogue.getListeChampCatalogue() != null) {
            XRiComboBox cbZonesCatalogues = afficherComboChampsCatalogue(champERP);
            cbZonesCatalogues.setBackground(new Color(171, 148, 79));
            cbZonesCatalogues.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbZonesCatalogues.setName("cbZonesCatalogues" + ligne);
            cbZonesCatalogues.setPreferredSize(new Dimension(400, 30));
            cbZonesCatalogues.setMinimumSize(new Dimension(400, 30));
            cbZonesCatalogues.setMaximumSize(new Dimension(400, 30));
            panel.add(cbZonesCatalogues, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 7, 10), 0, 0));
            
            SNBoutonDetail riBoutonDetail = new SNBoutonDetail();
            riBoutonDetail.setName("riBoutonDetail" + ligne);
            riBoutonDetail.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riBoutonDetail1ActionPerformed(e, champERP);
              }
            });
            panel.add(riBoutonDetail, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 7, 10), 0, 0));
            
            JLabel lbExemple = new JLabel();
            lbExemple.setText(afficherUneValeurExemple(champERP));
            lbExemple.setName("lbExemple" + ligne);
            panel.add(lbExemple, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 7, 10), 0, 0));
            
            // afficherFormuleChampsCatalogue
            if (configurationCatalogue.getListeChampERP() != null) {
              ChampERP champ = configurationCatalogue.getListeChampERP().recupererUnChampParId(champERP.getId());
              if (champ != null && champ.getListeChampCatalogue() != null && champ.isModeExpert()) {
                JLabel lbFormule = new JLabel();
                lbFormule.setText(champ.retournerLaFormuleAssociation());
                lbFormule.setName("lbFormule" + ligne);
                lbFormule.setPreferredSize(new Dimension(400, 15));
                panel.add(lbFormule, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 10, 10), 0, 0));
              }
            }
          }
          
          pnlChampsCatalogue.add(panel, new GridBagConstraints(1, ligne, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
        }
      }
    }
  }
  
  private void rafraichirBoutonFermer() {
    boolean actif = !getModele().isConfigurationModifiee();
    snBarreBouton.activerBouton(EnumBouton.FERMER, actif);
  }
  
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isConfigurationModifiee();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  private void rafraichirBoutonAnnuler() {
    snBarreBouton.activerBouton(EnumBouton.ANNULER, getModele().isConfigurationModifiee());
  }
  
  private void rafraichirBoutonPrecedent() {
    snBarreBouton.activerBouton(EnumBouton.NAVIGATION_PRECEDENT, !getModele().isConfigurationModifiee());
  }
  
  private void rafraichirBoutonSuivant() {
    snBarreBouton.activerBouton(EnumBouton.NAVIGATION_SUIVANT, !getModele().isConfigurationModifiee());
  }
  
  /**
   * Rafraichir les compteurs de listes de configuration et d'index dans la liste
   */
  private void rafraichirBoutonNavigation() {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    ListeConfigurationCatalogue listeConfigurationCatalogue = getModele().getListeConfigurationCatalogue();
    if (listeConfigurationCatalogue != null && configurationCatalogue != null) {
      snBarreBouton.setCompteurCourant(listeConfigurationCatalogue.retournerIndexConfigurationCatalogue(configurationCatalogue) + 1);
      snBarreBouton.setCompteurMax(listeConfigurationCatalogue.size());
    }
  }
  
  /**
   * Afficher une Combo Box de Champs catalogue afin de correspondre à un champ ERP
   */
  private XRiComboBox afficherComboChampsCatalogue(final ChampERP pChampERP) {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    if (pChampERP == null) {
      return null;
    }
    final XRiComboBox cbChampsCatalogue = new XRiComboBox();
    cbChampsCatalogue.addItem("");
    for (ChampCatalogue champCatalogue : configurationCatalogue.getListeChampCatalogue()) {
      cbChampsCatalogue.addItem(champCatalogue);
    }
    cbChampsCatalogue.addItemListener(new ItemListener() {
      @Override
      public void itemStateChanged(ItemEvent e) {
        cbChampsCatalogueItemStateChanged(pChampERP, cbChampsCatalogue, e);
      }
    });
    
    if (configurationCatalogue.getListeChampERP() != null) {
      ChampERP champERP = configurationCatalogue.getListeChampERP().recupererUnChampParId(pChampERP.getId());
      if (champERP != null) {
        if (champERP.getListeChampCatalogue() != null && champERP.getListeChampCatalogue().size() == 1) {
          cbChampsCatalogue.setSelectedItem(champERP.getListeChampCatalogue().get(0));
        }
      }
    }
    
    return cbChampsCatalogue;
  }
  
  /**
   * Attribuer un champ Catalogue à un champ ERP grâce à la sélection d'un champ Catalogue dans la combo Box
   */
  private void cbChampsCatalogueItemStateChanged(ChampERP pChampERP, XRiComboBox pCcomboChampsCatalogue, ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        ChampERP champERP = pChampERP.clone();
        if (pCcomboChampsCatalogue.getSelectedItem() instanceof ChampCatalogue) {
          ChampCatalogue champCatalogue = (ChampCatalogue) pCcomboChampsCatalogue.getSelectedItem();
          ChampCatalogue nouveauChamp = champCatalogue.clone();
          getModele().ajouterAssociationChamp(champERP, nouveauChamp);
        }
        else {
          getModele().retirerAssociationChamp(pChampERP);
        }
        rafraichirBoutonAnnuler();
        rafraichirBoutonFermer();
        rafraichirBoutonPrecedent();
        rafraichirBoutonSuivant();
        rafraichirBoutonValider();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourner une valeur exemple du champ catalogue du fichier csv associé à la configuration catalogue
   * La première ligne étant dédiée
   */
  private String afficherUneValeurExemple(ChampERP pChampERP) {
    ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
    
    if (pChampERP == null) {
      return "";
    }
    
    String retour = "";
    
    if (configurationCatalogue.getListeChampERP() != null) {
      ChampERP champERP = configurationCatalogue.getListeChampERP().recupererUnChampParId(pChampERP.getId());
      if (champERP != null) {
        if (champERP.getListeChampCatalogue() != null) {
          for (ChampCatalogue champCatalogue : champERP.getListeChampCatalogue()) {
            if (champCatalogue.getValeurExemple() != null) {
              int longueurValeurEx = champCatalogue.getValeurExemple().length();
              int debutDecoupe = champCatalogue.getDebutDecoupe();
              int finDecoupe = champCatalogue.getFinDecoupe();
              if (debutDecoupe < 1 || finDecoupe < debutDecoupe) {
                return retour;
              }
              if (debutDecoupe >= longueurValeurEx) {
                return retour;
              }
              if (finDecoupe > longueurValeurEx) {
                finDecoupe = longueurValeurEx;
              }
              
              retour += champCatalogue.getValeurExemple().substring(debutDecoupe - 1, finDecoupe);
            }
          }
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Afficher l'indice de colonne du fichier CSV typé "CSV"
   */
  private String afficherIndiceColonneCSV(int pIndice) {
    String retour = "" + pIndice;
    String[] tabLettres = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
        "W", "X", "Y", "Z" };
    if (pIndice > tabLettres.length) {
      int premiereLettre = -1;
      if (pIndice % tabLettres.length == 0) {
        premiereLettre = (pIndice / tabLettres.length) - 1;
      }
      else {
        premiereLettre = pIndice / tabLettres.length;
      }
      retour = tabLettres[premiereLettre - 1];
      int deuxiemeLettre = pIndice - (tabLettres.length * premiereLettre);
      if (deuxiemeLettre != 0) {
        retour += tabLettres[deuxiemeLettre - 1];
      }
      else {
        retour += tabLettres[tabLettres.length - 1];
      }
    }
    else {
      retour = tabLettres[pIndice - 1];
    }
    
    return retour;
  }
  
  // Methodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().annuler();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().annuler();
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().valider();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        getModele().supprimer();
      }
      else if (pSNBouton.isBouton(EnumBouton.NAVIGATION_PRECEDENT)) {
        getModele().afficherConfigurationPrecedente();
      }
      else if (pSNBouton.isBouton(EnumBouton.NAVIGATION_SUIVANT)) {
        getModele().afficherConfigurationSuivante();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btAnnulerActionPerformed(ActionEvent e) {
    try {
      getModele().annuler();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  protected void btValiderActionPerformed(ActionEvent e) {
    try {
      getModele().valider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbFichierActionPerformed(ActionEvent e) {
    try {
      if (isEvenementsActifs()) {
        if (cbFichier.getSelectedItem() instanceof String) {
          getModele().modifierFichierCSV((String) cbFichier.getSelectedItem());
        }
        else {
          getModele().modifierFichierCSV(null);
        }
        rafraichirBoutonFermer();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snFournisseurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFournisseur(snFournisseur.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btSupprimerActionPerformed(ActionEvent e) {
    try {
      getModele().supprimer();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLibelleConfigKeyReleased(KeyEvent e) {
    try {
      getModele().modifierLibelle(tfLibelleConfig.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riBoutonDetail1ActionPerformed(ActionEvent e, ChampERP pChampERP) {
    try {
      getModele().afficherModeExpert(pChampERP);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new JPanel();
    pnlEntete = new JPanel();
    pnlEntete1 = new JPanel();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    lbLibelle = new SNLabelChamp();
    tfLibelleConfig = new XRiTextField();
    lbFichier = new SNLabelChamp();
    cbFichier = new SNComboBox();
    pnlEntete2 = new JPanel();
    lbTitre = new SNLabelTitre();
    scpListeConfigs = new JScrollPane();
    pnlChampsCatalogue = new JPanel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlEntete ========
        {
          pnlEntete.setOpaque(false);
          pnlEntete.setName("pnlEntete");
          pnlEntete.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlEntete1 ========
          {
            pnlEntete1.setOpaque(false);
            pnlEntete1.setName("pnlEntete1");
            pnlEntete1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEntete1.getLayout()).columnWidths = new int[] { 155, 350, 0 };
            ((GridBagLayout) pnlEntete1.getLayout()).rowHeights = new int[] { 33, 33, 28, 0 };
            ((GridBagLayout) pnlEntete1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEntete1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlEntete1.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            snFournisseur.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snFournisseurValueChanged(e);
              }
            });
            pnlEntete1.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLibelle ----
            lbLibelle.setText("Libell\u00e9");
            lbLibelle.setName("lbLibelle");
            pnlEntete1.add(lbLibelle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfLibelleConfig ----
            tfLibelleConfig.setMinimumSize(new Dimension(12, 30));
            tfLibelleConfig.setPreferredSize(new Dimension(12, 30));
            tfLibelleConfig.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfLibelleConfig.setName("tfLibelleConfig");
            tfLibelleConfig.addKeyListener(new KeyAdapter() {
              @Override
              public void keyReleased(KeyEvent e) {
                tfLibelleConfigKeyReleased(e);
              }
            });
            pnlEntete1.add(tfLibelleConfig, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFichier ----
            lbFichier.setText("Fichier CSV");
            lbFichier.setName("lbFichier");
            pnlEntete1.add(lbFichier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbFichier ----
            cbFichier.setName("cbFichier");
            cbFichier.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                cbFichierActionPerformed(e);
              }
            });
            pnlEntete1.add(cbFichier, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEntete.add(pnlEntete1);
          
          // ======== pnlEntete2 ========
          {
            pnlEntete2.setOpaque(false);
            pnlEntete2.setName("pnlEntete2");
            pnlEntete2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEntete2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEntete2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEntete2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlEntete2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          }
          pnlEntete.add(pnlEntete2);
        }
        pnlContenu.add(pnlEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTitre ----
        lbTitre.setText("Correspondance des champs du fichier");
        lbTitre.setName("lbTitre");
        pnlContenu.add(lbTitre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeConfigs ========
        {
          scpListeConfigs.setPreferredSize(new Dimension(1050, 500));
          scpListeConfigs.setBackground(new Color(238, 238, 210));
          scpListeConfigs.setOpaque(true);
          scpListeConfigs.setName("scpListeConfigs");
          
          // ======== pnlChampsCatalogue ========
          {
            pnlChampsCatalogue.setBackground(new Color(238, 238, 210));
            pnlChampsCatalogue.setBorder(new EtchedBorder());
            pnlChampsCatalogue.setName("pnlChampsCatalogue");
            pnlChampsCatalogue.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlChampsCatalogue.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlChampsCatalogue.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlChampsCatalogue.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlChampsCatalogue.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          }
          scpListeConfigs.setViewportView(pnlChampsCatalogue);
        }
        pnlContenu.add(scpListeConfigs, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private JPanel pnlContenu;
  private JPanel pnlEntete;
  private JPanel pnlEntete1;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private SNLabelChamp lbLibelle;
  private XRiTextField tfLibelleConfig;
  private SNLabelChamp lbFichier;
  private SNComboBox cbFichier;
  private JPanel pnlEntete2;
  private SNLabelTitre lbTitre;
  private JScrollPane scpListeConfigs;
  private JPanel pnlChampsCatalogue;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
