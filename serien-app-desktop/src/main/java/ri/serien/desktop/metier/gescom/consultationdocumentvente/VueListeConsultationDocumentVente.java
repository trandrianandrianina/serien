/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.ButtonGroup;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.RowSorter;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.ModeleChantier;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueListeConsultationDocumentVente extends AbstractVuePanel<ModeleConsultationDocumentVente> {
  // Constantes
  private static final String[] TITRE_LISTE_DOCUMENT = new String[] { "Num\u00e9ro", "Date", "Type", "Montant HT", "Montant TTC",
      "Client", "R\u00e9f\u00e9rence longue", "Vendeur", "R\u00e9f\u00e9rence chantier", "" };
  private static final String[] TITRE_LISTE_LIGNE = new String[] { "Num\u00e9ro", "Qt\u00e9 UC", "UC", "Libell\u00e9", "Prix HT",
      "Qt\u00e9 UV", "UV", "Origine", "Remise", "Prix net HT", "Montant HT" };
  
  // Variables
  private DefaultTableModel tableModelDocument = null;
  private DefaultTableModel tableModelLigne = null;
  private JTableCellRendererDocumentVente listeDocumentsRenderer = new JTableCellRendererDocumentVente();
  JTableCellRendererLigne listeLigneRenderer = new JTableCellRendererLigne();
  
  /**
   * Constructeur.
   */
  public VueListeConsultationDocumentVente(ModeleConsultationDocumentVente pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Champs de saisie
    tfNumeroDocument.setLongueur(IdDocumentVente.LONGUEUR_NUMERO_COMPLET);
    
    tableModelDocument = new DefaultTableModel(null, TITRE_LISTE_DOCUMENT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    tableModelLigne = new DefaultTableModel(null, TITRE_LISTE_LIGNE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    // Initialise l'aspect de la table des documents
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    scpListeDocuments.setBackground(Color.white);
    
    // Initialise l'aspect de la table des lignes
    scpListeLigne.getViewport().setBackground(Color.WHITE);
    scpListeLigne.setBackground(Color.white);
    
    // Permet de redéfinir la touche Enter sur la liste documents
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (tblListeDocuments.getSelectedRowCount() == 0) {
          validerRecherche();
        }
        else {
          validerListeSelectionDocument();
        }
      }
    };
    this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(SNCharteGraphique.TOUCHE_ENTREE, "enter");
    this.getActionMap().put("enter", nouvelleAction);
    tblListeDocuments.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Permet de redéfinir la touche Enter sur la liste lignes
    Action nouvelleActionLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (tblListeLigne.getSelectedRowCount() == 0) {
          validerRecherche();
        }
        else {
          validerListeSelectionLigne();
        }
      }
    };
    this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(SNCharteGraphique.TOUCHE_ENTREE, "enter");
    this.getActionMap().put("enter", nouvelleActionLigne);
    tblListeLigne.modifierAction(nouvelleActionLigne, SNCharteGraphique.TOUCHE_ENTREE);
    
    tblListeDocuments.getTableHeader().setFont(new Font(tblListeDocuments.getTableHeader().getFont().getName(), Font.PLAIN, 14));
    
    tblListeDocuments.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlageDocumentVenteAffiche(0, tblListeDocuments.getModel().getRowCount());
      }
    });
    
    tblListeLigne.getTableHeader().setFont(new Font(tblListeDocuments.getTableHeader().getFont().getName(), Font.PLAIN, 14));
    
    tblListeLigne.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlageLigneVenteAffiche(0, tblListeLigne.getModel().getRowCount());
      }
    });
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHaut = retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBas = retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHaut = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHaut.actionPerformed(ae);
      }
    };
    Action nouvelleActionFlecheBas = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBas.actionPerformed(ae);
      }
    };
    tblListeDocuments.modifierAction(nouvelleActionFlecheHaut, SNCharteGraphique.TOUCHE_HAUT);
    tblListeDocuments.modifierAction(nouvelleActionFlecheBas, SNCharteGraphique.TOUCHE_BAS);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeDocuments.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeDocumentSelectionChanged(e);
      }
    });
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHautLigne = retournerActionComposant(tblListeLigne, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBasLigne = retournerActionComposant(tblListeLigne, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHautLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHautLigne.actionPerformed(ae);
      }
    };
    Action nouvelleActionFlecheBasLigne = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBasLigne.actionPerformed(ae);
      }
    };
    tblListeLigne.modifierAction(nouvelleActionFlecheHautLigne, SNCharteGraphique.TOUCHE_HAUT);
    tblListeLigne.modifierAction(nouvelleActionFlecheBasLigne, SNCharteGraphique.TOUCHE_BAS);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeLigne.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeLigneSelectionChanged(e);
      }
    });
    
    // Renseigner les types de documents de vente
    cbTypeDocument.removeAllItems();
    cbTypeDocument.addItem("Tous");
    cbTypeDocument.addItem(EnumTypeDocumentVente.DEVIS);
    cbTypeDocument.addItem(EnumTypeDocumentVente.COMMANDE);
    cbTypeDocument.addItem(EnumTypeDocumentVente.BON);
    cbTypeDocument.addItem(EnumTypeDocumentVente.FACTURE);
    
    cbTypeDocumentLigne.removeAllItems();
    cbTypeDocumentLigne.addItem("Tous");
    cbTypeDocumentLigne.addItem(EnumTypeDocumentVente.DEVIS);
    cbTypeDocumentLigne.addItem(EnumTypeDocumentVente.COMMANDE);
    cbTypeDocumentLigne.addItem(EnumTypeDocumentVente.BON);
    cbTypeDocumentLigne.addItem(EnumTypeDocumentVente.FACTURE);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    scpListeDocuments.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent e) {
        scpListeDocumentsStateChanged(null);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigne = scpListeLigne.getViewport();
    viewportLigne.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeLigneStateChanged(e);
      }
    });
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonDocument(pSNBouton);
      }
    });
    
    snBarreRechercheLigne.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonLigne(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonDocument(pSNBouton);
      }
    });
    
    snPlageDateDocument.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDateLigne.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    
    rafraichirModeRecherche();
    
    // Rafraichir le mode document
    if (getModele().isModeAffichageDocument()) {
      // Rafraîchir filtre colonne 1
      rafraichirNumeroDocument();
      rafraichirReferenceClientDocument();
      rafraichirTypeDocument();
      rafraichirTypeClientDocument();
      rafraichirNomCLientDocument();
      rafraichirCodePostalDocument();
      rafraichirVilleDocument();
      rafraichirArticleDocument();
      rafraichirChantierDocument();
      
      // Rafraîchir filtre colonne 2
      rafraichirEtablissementDocument();
      rafraichirMagasinDocument();
      rafraichirDateCreationDocument();
      
      // Rafraîchir tableau
      rafraichirTitreResultatDocument();
      rafraichirTableauResultatDocument();
    }
    // Rafraichir le mode ligne
    else {
      // Rafraîchir filtre colonne 1
      rafraichirNumeroDocumentLigne();
      rafraichirReferenceClientLigne();
      rafraichirTypeLigne();
      rafraichirNomCLientLigne();
      rafraichirCodePostalLigne();
      rafraichirVilleLigne();
      rafraichirArticleLigne();
      rafraichirChantierLigne();
      
      // Rafraîchir filtre colonne 2
      rafraichirEtablissementLigne();
      rafraichirMagasinLigne();
      rafraichirDateCreationLigne();
      
      // Rafraîchir tableau
      rafraichirTitreResultatLigne();
      rafraichirTableauResultatLigne();
    }
    
    // Rafraîchir boutons
    rafraichirBoutonConsulter();
    
    // Gérer le focus
    if ((getModele().getListeDocumentVenteBase() != null) && (!getModele().getListeDocumentVenteBase().isEmpty())) {
      tblListeDocuments.requestFocusInWindow();
    }
  }
  
  private void rafraichirModeRecherche() {
    rbRechercheDocument.setSelected(getModele().isModeAffichageDocument());
    rbRechercheLigne.setSelected(getModele().isModeAffichageLigne());
    pnlContenuDocument.setVisible(getModele().isModeAffichageDocument());
    pnlContenuLigne.setVisible(getModele().isModeAffichageLigne());
  }
  
  private void rafraichirNumeroDocument() {
    String valeur = "";
    if (getModele().getNumeroDocument() > 0) {
      valeur = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocument(), 0);
    }
    tfNumeroDocument.setText(valeur);
    tfNumeroDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirReferenceClientDocument() {
    String valeur = "";
    if (getModele().getReferenceClientDocument() != null) {
      valeur = getModele().getReferenceClientDocument();
    }
    tfReferenceClientDocument.setText(valeur);
    tfReferenceClientDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTypeDocument() {
    if (getModele().getTypeDocument() != EnumTypeDocumentVente.NON_DEFINI) {
      cbTypeDocument.setSelectedItem(getModele().getTypeDocument());
    }
    else {
      cbTypeDocument.setSelectedItem("Tous");
    }
    cbTypeDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTypeClientDocument() {
    // Remplir la liste déroulante la premièr efois
    if (cbTypeClient.getItemCount() == 0) {
      cbTypeClient.addItem("Tous");
      for (int i = 0; i < EnumTypeCompteClient.values().length; i++) {
        cbTypeClient.addItem(EnumTypeCompteClient.values()[i]);
      }
    }
    
    // Sélectionner la bonne valeur
    if (getModele().getTypeRechercheClient() != null) {
      cbTypeClient.setSelectedItem(getModele().getTypeRechercheClient());
    }
    else {
      cbTypeClient.setSelectedIndex(0);
    }
    
    // Désactiver le composant si l'écran n'est pas chargé
    cbTypeClient.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirNomCLientDocument() {
    snClient.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snClient.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snClient.setIdEtablissement(null);
    }
    snClient.setTypeCompteClient(getModele().getTypeRechercheClient());
    snClient.charger(false);
    snClient.setSelection(getModele().getClientBase());
  }
  
  private void rafraichirCodePostalDocument() {
    ClientBase client = getModele().getClientBase();
    if (client == null) {
      tfCodePostal.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getCodePostalFormate() != null) {
        tfCodePostal.setText(adresse.getCodePostalFormate());
      }
      else {
        tfCodePostal.setText("");
      }
    }
    tfCodePostal.setEnabled(false);
  }
  
  private void rafraichirVilleDocument() {
    ClientBase client = getModele().getClientBase();
    if (client == null) {
      tfVille.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getCodePostalFormate() != null) {
        tfVille.setText(adresse.getVille());
      }
      else {
        tfVille.setText("");
      }
    }
    tfVille.setEnabled(false);
  }
  
  private void rafraichirArticleDocument() {
    snArticleDocument.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snArticleDocument.setIdEtablissement(getModele().getEtablissement().getId());
      snArticleDocument.charger(false);
    }
    
    if (getModele().getIdArticle() == null) {
      snArticleDocument.initialiserRecherche();
    }
    
    snArticleDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirChantierDocument() {
    snChantierDocument.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snChantierDocument.setIdEtablissement(getModele().getEtablissement().getId());
      if (getModele().getClientBase() != null) {
        snChantierDocument.setIdClient(getModele().getClientBase().getId());
      }
      else {
        snChantierDocument.setIdClient(null);
      }
      snChantierDocument.setModeComposant(ModeleChantier.MODE_FILTRE_RECHERCHE);
      snChantierDocument.charger(false);
      snChantierDocument.setSelection(getModele().getChantier());
    }
  }
  
  private void rafraichirChantierLigne() {
    snChantierLigne.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snChantierLigne.setIdEtablissement(getModele().getEtablissement().getId());
      if (getModele().getClientBase() != null) {
        snChantierLigne.setIdClient(getModele().getClientBase().getId());
      }
      else {
        snChantierLigne.setIdClient(null);
      }
      snChantierLigne.setModeComposant(ModeleChantier.MODE_FILTRE_RECHERCHE);
      snChantierLigne.charger(false);
      snChantierLigne.setSelection(getModele().getChantier());
    }
  }
  
  private void rafraichirEtablissementDocument() {
    // Charger la liste des établissements le premier coup
    if (snEtablissement.isEmpty() && getModele().getListeEtablissement() != null) {
      snEtablissement.setListe(getModele().getListeEtablissement());
    }
    
    if (getModele().getEtablissement() != null) {
      snEtablissement.setSelection(getModele().getEtablissement());
      snClient.setIdEtablissement(getModele().getEtablissement().getId());
    }
    
    snEtablissement.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasinDocument() {
    cbMagasin.setSession(getModele().getSession());
    if (snEtablissement.getIdSelection() != null) {
      cbMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      cbMagasin.charger(true);
      cbMagasin.setIdSelection(getModele().getIdMagasin());
    }
    cbMagasin.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirDateCreationDocument() {
    snPlageDateDocument.setEnabled(isDonneesChargees());
    snPlageDateDocument.setDateDebut(getModele().getDateDebut());
    snPlageDateDocument.setDateFin(getModele().getDateFin());
  }
  
  private void rafraichirDateCreationLigne() {
    snPlageDateLigne.setEnabled(isDonneesChargees());
    snPlageDateLigne.setDateDebut(getModele().getDateDebut());
    snPlageDateLigne.setDateFin(getModele().getDateFin());
  }
  
  private void rafraichirTitreResultatDocument() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultat.setMessage(getModele().getTitreResultat());
    }
    else if (getModele().isModeAffichageDocument()) {
      lblTitreResultat.setText("Documents correspondant à votre recherche (" + getModele().getNombreResultat() + ")");
    }
    else if (getModele().isModeAffichageLigne()) {
      lblTitreResultat.setText("Lignes correspondant à votre recherche (" + getModele().getNombreLignesArticleTrouvees() + ")");
    }
    else {
      lblTitreResultat.setText("");
    }
  }
  
  private void rafraichirTableauResultatDocument() {
    tableModelDocument.setRowCount(0);
    rafraichirListeDocument();
  }
  
  /**
   * valide les données sur en validation
   */
  private void validerRecherche() {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
      getModele().modifierReferenceClientDocument(tfReferenceClientDocument.getText());
      if (snArticleDocument.getSelection() != null) {
        getModele().modifierArticle(snArticleDocument.getSelection().getId());
      }
      getModele().rechercher();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListeDocument() {
    String[][] donnees = null;
    
    // Convertir les données à afficher dans le format attendu par le tableau
    ListeDocumentVenteBase listeDocumentVenteBase = getModele().getListeDocumentVenteBase();
    if (listeDocumentVenteBase != null && !listeDocumentVenteBase.isEmpty()) {
      donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE_DOCUMENT.length];
      for (int i = 0; i < listeDocumentVenteBase.size(); i++) {
        DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(i);
        String[] ligne = donnees[i];
        
        // Numéro du document
        ligne[0] = documentVenteBase.getId().toString();
        
        // Date de création
        if (documentVenteBase.isVerrouille()) {
          ligne[1] = "Verrouillé";
        }
        else if (documentVenteBase.getDateCreation() != null) {
          if (documentVenteBase.isFacture()) {
            ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateFacturation());
          }
          else {
            ligne[1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
          }
        }
        
        // Type du document et mode d'expédition
        String type = "";
        if (documentVenteBase.getTypeDocumentVente() != null) {
          type += documentVenteBase.getTypeDocumentVente().getLibelle();
        }
        if (documentVenteBase.geIdModeExpedition() != null) {
          ModeExpedition modeExpedition =
              getModele().getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
          if (modeExpedition != null) {
            type += " - " + modeExpedition.getLibelle();
          }
        }
        ligne[2] = type;
        
        // Montant du document HT
        if (documentVenteBase.getTotalHT() != null) {
          ligne[3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        }
        
        // Montant du document TTC
        if (documentVenteBase.getTotalTTC() != null) {
          ligne[4] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
        }
        
        // Nom du client
        if (documentVenteBase.getComplement() instanceof ClientBase) {
          ClientBase clientBase = (Client) documentVenteBase.getComplement();
          if (clientBase.getAdresse() != null && clientBase.getAdresse().getNom() != null) {
            ligne[5] = clientBase.getAdresse().getNom();
            if (clientBase.getTypeCompteClient() != null) {
              ligne[5] += " (" + clientBase.getTypeCompteClient().getLibelle() + ")";
            }
          }
        }
        
        // Référence longue
        ligne[6] = documentVenteBase.getReferenceLongue();
        
        // Code vendeur
        if (documentVenteBase.getIdVendeur() != null) {
          ligne[7] = documentVenteBase.getIdVendeur().getCode();
        }
        
        // Libellé chantier
        if (documentVenteBase.getIdChantier() != null) {
          ligne[8] = documentVenteBase.getLibelleChantier();
        }
        
        // Etat du document (vert = entièrement traité, orange = partiellement traité, blanc = pas traité du tout).
        if (documentVenteBase.getLibelleEtat() != null) {
          ligne[9] = documentVenteBase.getLibelleEtat();
        }
      }
    }
    else {
      // Se positionner en haut du tableau si la liste est vide
      scpListeDocuments.getViewport().reshape(0, 0, scpListeDocuments.getViewport().getWidth(), 0);
      scpListeDocuments.getVerticalScrollBar().setValue(0);
      tblListeDocuments.getRowSorter().setSortKeys(null);
    }
    
    // Mettre à jour les données
    tableModelDocument.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelDocument.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeDocuments.getModel().equals(tableModelDocument)) {
      tblListeDocuments.setModel(tableModelDocument);
      tblListeDocuments.setGridColor(new Color(204, 204, 204));
      tblListeDocuments.setDefaultRenderer(Object.class, listeDocumentsRenderer);
      tblListeDocuments.trierColonnes();
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeDocuments.getRowCount() == 0) {
      listeDocumentsRenderer.redimensionnerColonnes(tblListeDocuments);
    }
    
    // Sélectionner la ligne en cours
    int index = getModele().getIndexDocumentSelectionne();
    if (index > -1 && index < tblListeDocuments.getRowCount()) {
      tblListeDocuments.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  private void rafraichirListeLigne() {
    String[][] donnees = null;
    // Convertir les données à afficher dans le format attendu par le tableau
    
    ListeLigneVenteBase listeLigneVente = getModele().getListeLigneVenteBase();
    
    if (listeLigneVente != null) {
      
      donnees = new String[getModele().getNombreLignesArticleTrouvees()][TITRE_LISTE_LIGNE.length];
      
      // On rempli le tableau
      for (int i = 0; i < listeLigneVente.size(); i++) {
        LigneVenteBase ligneVenteBase = listeLigneVente.get(i);
        String[] ligne = donnees[i];
        
        // Numéro du document
        if (ligneVenteBase.getNumeroFacture() != null && ligneVenteBase.getNumeroFacture() > 0) {
          ligne[0] = ligneVenteBase.getNumeroFacture() + "-" + ligneVenteBase.getId().getNumeroLigne();
        }
        else {
          ligne[0] = ligneVenteBase.getId().getIndicatifSimplifie();
        }
        
        // Quantité article en unité de conditionnement de vente
        ligne[1] = Constantes.formater(ligneVenteBase.getQuantiteUCV(), false);
        
        // Unité de conditionnement de vente
        if (ligneVenteBase.getIdUniteConditionnementVente() != null) {
          ligne[2] = ligneVenteBase.getIdUniteConditionnementVente().getCode();
        }
        
        // Libellé article
        ligne[3] = ligneVenteBase.getLibelle();
        
        // Prix unitaire HT de l'article
        ligne[4] = Constantes.formater(ligneVenteBase.getPrixBaseHT(), true);
        
        // Quantité article en unité de vente
        ligne[5] = Constantes.formater(ligneVenteBase.getQuantiteUV(), false);
        
        // Unité de vente
        if (ligneVenteBase.getIdUniteVente() != null) {
          ligne[6] = ligneVenteBase.getIdUniteVente().getCode();
        }
        
        // Origine du prix
        if (ligneVenteBase.getOriginePrixVente() == null) {
          ligne[7] = "";
        }
        else {
          ligne[7] = ligneVenteBase.getOriginePrixVente().getLibelle();
        }
        
        // Remise
        if (ligneVenteBase.getTauxRemise1() != null) {
          ligne[8] = Constantes.formater(ligneVenteBase.getTauxRemise1(), true) + " %";
        }
        
        // Prix net de l'article
        ligne[9] = Constantes.formater(ligneVenteBase.getPrixNetHT(), true);
        
        // Montant
        ligne[10] = Constantes.formater(ligneVenteBase.getMontantHT(), true);
      }
    }
    
    // Mettre à jour les données
    tableModelLigne.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelLigne.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeLigne.getModel().equals(tableModelLigne)) {
      tblListeLigne.setModel(tableModelLigne);
      tblListeLigne.setGridColor(new Color(204, 204, 204));
      tblListeLigne.setDefaultRenderer(Object.class, listeLigneRenderer);
      tblListeLigne.trierColonnes();
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeLigne.getRowCount() == 0) {
      listeLigneRenderer.redimensionnerColonnes(tblListeLigne);
    }
    
    // Sélectionner la ligne en cours
    int index = getModele().getIndexLigneSelectionne();
    if (index > -1 && index < tblListeLigne.getRowCount()) {
      tblListeLigne.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  private void rafraichirNumeroDocumentLigne() {
    String valeur = "";
    if (getModele().getNumeroDocument() > 0) {
      valeur = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocument(), 0);
    }
    tfNumeroDocumentLigne.setText(valeur);
    tfNumeroDocumentLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirReferenceClientLigne() {
    String valeur = "";
    if (getModele().getReferenceClientDocument() != null) {
      valeur = getModele().getReferenceClientDocument();
    }
    tfReferenceClientLigne.setText(valeur);
    tfReferenceClientLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTypeLigne() {
    if (getModele().getTypeDocument() != EnumTypeDocumentVente.NON_DEFINI) {
      cbTypeDocumentLigne.setSelectedItem(getModele().getTypeDocument());
    }
    else {
      cbTypeDocumentLigne.setSelectedItem("Tous");
    }
    cbTypeDocumentLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirNomCLientLigne() {
    snClientLigne.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snClientLigne.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snClientLigne.setIdEtablissement(null);
    }
    snClientLigne.setTypeCompteClient(getModele().getTypeRechercheClient());
    snClientLigne.charger(false);
    snClientLigne.setSelection(getModele().getClientBase());
  }
  
  private void rafraichirCodePostalLigne() {
    ClientBase client = getModele().getClientBase();
    if (client == null) {
      tfCodePostalLigne.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getCodePostalFormate() != null) {
        tfCodePostalLigne.setText(adresse.getCodePostalFormate());
      }
      else {
        tfCodePostalLigne.setText("");
      }
    }
    tfCodePostalLigne.setEnabled(false);
  }
  
  private void rafraichirVilleLigne() {
    ClientBase client = getModele().getClientBase();
    if (client == null) {
      tfVilleLigne.setText("");
    }
    else {
      Adresse adresse = client.getAdresse();
      if (adresse != null && adresse.getCodePostalFormate() != null) {
        tfVilleLigne.setText(adresse.getVille());
      }
      else {
        tfVilleLigne.setText("");
      }
    }
    tfVilleLigne.setEnabled(false);
  }
  
  private void rafraichirArticleLigne() {
    snArticleLigne.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snArticleLigne.setIdEtablissement(getModele().getEtablissement().getId());
      snArticleLigne.charger(false);
    }
    snArticleLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirEtablissementLigne() {
    // Charger la liste des établissements le premier coup
    if (snEtablissementLigne.isEmpty() && getModele().getListeEtablissement() != null) {
      snEtablissementLigne.setListe(getModele().getListeEtablissement());
    }
    
    if (getModele().getEtablissement() != null) {
      snEtablissementLigne.setSelection(getModele().getEtablissement());
      snClientLigne.setIdEtablissement(getModele().getEtablissement().getId());
    }
    
    snEtablissementLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasinLigne() {
    cbMagasinLigne.setSession(getModele().getSession());
    cbMagasinLigne.setIdEtablissement(snEtablissement.getIdSelection());
    cbMagasinLigne.charger(true);
    cbMagasinLigne.setIdSelection(getModele().getIdMagasin());
    cbMagasinLigne.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTitreResultatLigne() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultatLigne.setMessage(getModele().getTitreResultat());
    }
    else if (getModele().isModeAffichageDocument()) {
      lblTitreResultatLigne.setText("Documents correspondant à votre recherche (" + getModele().getNombreResultat() + ")");
    }
    else if (getModele().isModeAffichageLigne()) {
      lblTitreResultatLigne.setText("Lignes correspondant à votre recherche (" + getModele().getNombreLignesArticleTrouvees() + ")");
    }
    else {
      lblTitreResultatLigne.setText("");
    }
  }
  
  private void rafraichirTableauResultatLigne() {
    tableModelLigne.setRowCount(0);
    rafraichirListeLigne();
  }
  
  /**
   * Afficher le bouton Consulter
   */
  private void rafraichirBoutonConsulter() {
    if (getModele().isModeAffichageDocument()) {
      snBarreBouton.activerBouton(EnumBouton.CONSULTER, isDonneesChargees() && tblListeDocuments.getSelectedRow() >= 0);
    }
    else if (getModele().isModeAffichageLigne()) {
      snBarreBouton.activerBouton(EnumBouton.CONSULTER, isDonneesChargees() && tblListeLigne.getSelectedRow() >= 0);
    }
  }
  
  /**
   * Récupère le document sélectionné.
   */
  private void validerListeSelectionDocument() {
    int indexvisuel = tblListeDocuments.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeDocuments.getRowSorter() != null) {
        indexreel = tblListeDocuments.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierDocumentSelectionne(indexreel);
      getModele().afficherDetailDocument();
    }
  }
  
  /**
   * Récupère le document sélectionné.
   */
  private void validerListeSelectionLigne() {
    int indexvisuel = tblListeLigne.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeLigne.getRowSorter() != null) {
        indexreel = tblListeLigne.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierLigneSelectionne(indexreel);
      getModele().afficherDetailLigne();
    }
  }
  
  /**
   * Selectionne la ligne sur laquelle on a double cliqué.
   */
  private void selectionnerLigne(MouseEvent e) {
    // Rafraîchir boutons
    rafraichirBoutonConsulter();
    
    if (e.getClickCount() == 2) {
      if (getModele().isModeAffichageDocument()) {
        validerListeSelectionDocument();
      }
      else if (getModele().isModeAffichageLigne()) {
        validerListeSelectionLigne();
      }
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  private void btTraiterClicBoutonDocument(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        if (getModele().isModeAffichageDocument()) {
          validerListeSelectionDocument();
        }
        else if (getModele().isModeAffichageLigne()) {
          validerListeSelectionLigne();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        if (!snClient.hasFocus()) {
          getModele().rechercher();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
        tableModelDocument.setRowCount(0);
        RowSorter<?> rs = tblListeDocuments.getRowSorter();
        rs.setSortKeys(null);
        rafraichirListeDocument();
        tableModelLigne.setRowCount(0);
        RowSorter<?> rsl = tblListeLigne.getRowSorter();
        rsl.setSortKeys(null);
        rafraichirListeLigne();
        snClient.initialiserRecherche();
        snArticleDocument.initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btTraiterClicBoutonLigne(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        validerListeSelectionDocument();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        if (!snClient.hasFocus()) {
          getModele().rechercherLignes();
          
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
        tableModelDocument.setRowCount(0);
        RowSorter<?> rs = tblListeDocuments.getRowSorter();
        rs.setSortKeys(null);
        rafraichirListeDocument();
        tableModelLigne.setRowCount(0);
        RowSorter<?> rsl = tblListeLigne.getRowSorter();
        rsl.setSortKeys(null);
        rafraichirListeLigne();
        snClient.initialiserRecherche();
        snArticleLigne.initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void keyFleches(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSeletionnee = tblListeDocuments.getSelectedRow();
      if (indexLigneSeletionnee == -1) {
        return;
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDocuments == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDocuments.getViewport().getViewRect();
      int premiereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDocuments.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDocumentVenteAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpListeLigneStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeLigne == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeLigne.getViewport().getViewRect();
      int premiereLigne = tblListeLigne.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeLigne.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeLigne.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageLigneVenteAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat des documents.
   */
  private void tblListeDocumentSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierDocumentSelectionne(tblListeDocuments.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat des lignes.
   */
  private void tblListeLigneSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierLigneSelectionne(tblListeLigne.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroDocumentLigneFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierNumeroDocument(tfNumeroDocumentLigne.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfArticleFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierArticle(snArticleDocument.getSelection().getId());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfArticleLigneActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierArticle(snArticleLigne.getSelection().getId());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      if (cbTypeDocument.getSelectedItem() != null && cbTypeDocument.getSelectedItem() instanceof EnumTypeDocumentVente) {
        getModele().modifierTypeDocumentVente((EnumTypeDocumentVente) cbTypeDocument.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeDocumentLigneItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      if (cbTypeDocumentLigne.getSelectedItem() != null && cbTypeDocumentLigne.getSelectedItem() instanceof EnumTypeDocumentVente) {
        getModele().modifierTypeDocumentVente((EnumTypeDocumentVente) cbTypeDocumentLigne.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinLigneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasin(cbMagasinLigne.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasin(cbMagasin.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceClientDocumentFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierReferenceClientDocument(tfReferenceClientDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfReferenceClientLigneFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierReferenceClientDocument(tfReferenceClientLigne.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      selectionnerLigne(e);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbTypeClientItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbTypeClient.getSelectedItem() instanceof EnumTypeCompteClient) {
        getModele().modifierTypeClient((EnumTypeCompteClient) cbTypeClient.getSelectedItem());
      }
      else {
        getModele().modifierTypeClient(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissement.getSelection());
      cbMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      cbMagasin.charger(true);
      cbMagasin.setIdSelection(getModele().getIdMagasin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementLigneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissementLigne.getSelection());
      cbMagasinLigne.setIdEtablissement(snEtablissementLigne.getIdSelection());
      cbMagasinLigne.charger(true);
      cbMagasinLigne.setIdSelection(getModele().getIdMagasin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snClientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierClient(snClient.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snClientLigneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierClient(snClientLigne.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRechercheDocumentItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierModeRecherche(ModeleConsultationDocumentVente.MODE_AFFICHAGE_DOCUMENT);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRechercheLigneItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierModeRecherche(ModeleConsultationDocumentVente.MODE_AFFICHAGE_LIGNE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snArticleDocumentValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (snArticleDocument.getSelection() != null) {
        getModele().modifierArticle(snArticleDocument.getSelection().getId());
      }
      else {
        getModele().modifierArticle(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snArticleLigneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (snArticleLigne.getSelection() != null) {
        getModele().modifierArticle(snArticleLigne.getSelection().getId());
      }
      else {
        getModele().modifierArticle(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snChantierDocumentValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierChantier(snChantierDocument.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snChantierLigneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierChantier(snChantierLigne.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeLigneMouseClicked(MouseEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      selectionnerLigne(e);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snPlageDateDocumentValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateCreationDebut(snPlageDateDocument.getDateDebut());
      getModele().modifierDateCreationFin(snPlageDateDocument.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snPlageDateLigneValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateCreationDebut(snPlageDateLigne.getDateDebut());
      getModele().modifierDateCreationFin(snPlageDateLigne.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlChoixTypeRecherche = new SNPanelTitre();
    rbRechercheDocument = new SNRadioButton();
    rbRechercheLigne = new SNRadioButton();
    pnlGeneral = new JPanel();
    pnlContenuDocument = new JPanel();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbNumeroDocument = new SNLabelChamp();
    tfNumeroDocument = new SNIdentifiant();
    lbReferenceClientDocument = new SNLabelChamp();
    tfReferenceClientDocument = new SNTexte();
    lbTypeDocument = new SNLabelChamp();
    cbTypeDocument = new SNComboBox();
    lbTypeClient = new SNLabelChamp();
    cbTypeClient = new XRiComboBox();
    lbClient = new SNLabelChamp();
    snClient = new SNClient();
    lbVille = new SNLabelChamp();
    pnlCommune = new SNPanel();
    tfCodePostal = new SNTexte();
    tfVille = new SNTexte();
    lbChantier = new SNLabelChamp();
    snChantierDocument = new SNChantier();
    lbArticle = new SNLabelChamp();
    snArticleDocument = new SNArticle();
    pnlFiltre2 = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    cbMagasin = new SNMagasin();
    ldDateCreation = new SNLabelChamp();
    snPlageDateDocument = new SNPlageDate();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    scpListeDocuments = new JScrollPane();
    tblListeDocuments = new NRiTable();
    pnlContenuLigne = new JPanel();
    pnlFiltre3 = new JPanel();
    pnlFiltre4 = new JPanel();
    lbNumeroDocumentLigne = new SNLabelChamp();
    tfNumeroDocumentLigne = new SNTexte();
    lbReferenceClientDocumentLigne = new SNLabelChamp();
    tfReferenceClientLigne = new SNTexte();
    lbTypeDocumentLigne = new SNLabelChamp();
    cbTypeDocumentLigne = new SNComboBox();
    lbClientLigne = new SNLabelChamp();
    snClientLigne = new SNClient();
    lbVilleLigne = new SNLabelChamp();
    pnlCommuneLigne = new SNPanel();
    tfCodePostalLigne = new SNTexte();
    tfVilleLigne = new SNTexte();
    lbChantierLigne = new SNLabelChamp();
    snChantierLigne = new SNChantier();
    lbArticleLigne = new SNLabelChamp();
    snArticleLigne = new SNArticle();
    pnlFiltre5 = new JPanel();
    lbEtablissementLigne = new SNLabelChamp();
    snEtablissementLigne = new SNEtablissement();
    lbMagasinLigne = new SNLabelChamp();
    cbMagasinLigne = new SNMagasin();
    ldDateCreationLigne = new SNLabelChamp();
    snPlageDateLigne = new SNPlageDate();
    snBarreRechercheLigne = new SNBarreRecherche();
    lblTitreResultatLigne = new SNLabelTitre();
    scpListeLigne = new JScrollPane();
    tblListeLigne = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    buttonGroup2 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlChoixTypeRecherche ========
    {
      pnlChoixTypeRecherche.setBorder(new CompoundBorder(new EmptyBorder(10, 10, 0, 10),
          new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14))));
      pnlChoixTypeRecherche.setName("pnlChoixTypeRecherche");
      pnlChoixTypeRecherche.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlChoixTypeRecherche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
      
      // ---- rbRechercheDocument ----
      rbRechercheDocument.setText("Recherche de documents de vente");
      rbRechercheDocument.setPreferredSize(new Dimension(350, 30));
      rbRechercheDocument.setMinimumSize(new Dimension(350, 30));
      rbRechercheDocument.setName("rbRechercheDocument");
      rbRechercheDocument.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheDocumentItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheDocument,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
      
      // ---- rbRechercheLigne ----
      rbRechercheLigne.setText("Recherche de lignes de vente");
      rbRechercheLigne.setPreferredSize(new Dimension(350, 30));
      rbRechercheLigne.setMinimumSize(new Dimension(350, 30));
      rbRechercheLigne.setName("rbRechercheLigne");
      rbRechercheLigne.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          rbRechercheLigneItemStateChanged(e);
        }
      });
      pnlChoixTypeRecherche.add(rbRechercheLigne,
          new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlChoixTypeRecherche, BorderLayout.NORTH);
    
    // ======== pnlGeneral ========
    {
      pnlGeneral.setOpaque(false);
      pnlGeneral.setName("pnlGeneral");
      pnlGeneral.setLayout(new CardLayout());
      
      // ======== pnlContenuDocument ========
      {
        pnlContenuDocument.setBackground(new Color(239, 239, 222));
        pnlContenuDocument.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenuDocument.setOpaque(false);
        pnlContenuDocument.setName("pnlContenuDocument");
        pnlContenuDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenuDocument.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenuDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setOpaque(false);
          pnlFiltre.setBorder(new TitledBorder(""));
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre1 ========
          {
            pnlFiltre1.setOpaque(false);
            pnlFiltre1.setName("pnlFiltre1");
            pnlFiltre1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocument ----
            lbNumeroDocument.setText("Num\u00e9ro document");
            lbNumeroDocument.setName("lbNumeroDocument");
            pnlFiltre1.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroDocument ----
            tfNumeroDocument.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroDocument.setName("tfNumeroDocument");
            tfNumeroDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbReferenceClientDocument ----
            lbReferenceClientDocument.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocument.setName("lbReferenceClientDocument");
            pnlFiltre1.add(lbReferenceClientDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientDocument ----
            tfReferenceClientDocument.setName("tfReferenceClientDocument");
            tfReferenceClientDocument.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientDocumentFocusLost(e);
              }
            });
            pnlFiltre1.add(tfReferenceClientDocument, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeDocument ----
            lbTypeDocument.setText("Type document");
            lbTypeDocument.setName("lbTypeDocument");
            pnlFiltre1.add(lbTypeDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocument ----
            cbTypeDocument.setMinimumSize(new Dimension(150, 30));
            cbTypeDocument.setPreferredSize(new Dimension(150, 30));
            cbTypeDocument.setName("cbTypeDocument");
            cbTypeDocument.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentItemStateChanged(e);
              }
            });
            pnlFiltre1.add(cbTypeDocument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeClient ----
            lbTypeClient.setText("Type de client");
            lbTypeClient.setName("lbTypeClient");
            pnlFiltre1.add(lbTypeClient, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeClient ----
            cbTypeClient.setBackground(new Color(171, 148, 79));
            cbTypeClient.setFont(cbTypeClient.getFont().deriveFont(cbTypeClient.getFont().getSize() + 2f));
            cbTypeClient.setPreferredSize(new Dimension(150, 30));
            cbTypeClient.setMinimumSize(new Dimension(150, 30));
            cbTypeClient.setName("cbTypeClient");
            cbTypeClient.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeClientItemStateChanged(e);
              }
            });
            pnlFiltre1.add(cbTypeClient, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClient ----
            lbClient.setText("Client");
            lbClient.setName("lbClient");
            pnlFiltre1.add(lbClient, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClient ----
            snClient.setName("snClient");
            snClient.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snClientValueChanged(e);
              }
            });
            pnlFiltre1.add(snClient, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVille ----
            lbVille.setText("Ville");
            lbVille.setName("lbVille");
            pnlFiltre1.add(lbVille, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCommune ========
            {
              pnlCommune.setName("pnlCommune");
              pnlCommune.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCommune.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCommune.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCommune.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCommune.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- tfCodePostal ----
              tfCodePostal.setEnabled(false);
              tfCodePostal.setName("tfCodePostal");
              pnlCommune.add(tfCodePostal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfVille ----
              tfVille.setEnabled(false);
              tfVille.setName("tfVille");
              pnlCommune.add(tfVille, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFiltre1.add(pnlCommune, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbChantier ----
            lbChantier.setText("Chantier");
            lbChantier.setName("lbChantier");
            pnlFiltre1.add(lbChantier, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snChantierDocument ----
            snChantierDocument.setName("snChantierDocument");
            snChantierDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snChantierDocumentValueChanged(e);
              }
            });
            pnlFiltre1.add(snChantierDocument, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setName("lbArticle");
            pnlFiltre1.add(lbArticle, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleDocument ----
            snArticleDocument.setName("snArticleDocument");
            snArticleDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleDocumentValueChanged(e);
              }
            });
            pnlFiltre1.add(snArticleDocument, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltre1);
          
          // ======== pnlFiltre2 ========
          {
            pnlFiltre2.setOpaque(false);
            pnlFiltre2.setName("pnlFiltre2");
            pnlFiltre2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlFiltre2.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlFiltre2.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlFiltre2.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasin ----
            cbMagasin.setName("cbMagasin");
            cbMagasin.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                cbMagasinValueChanged(e);
              }
            });
            pnlFiltre2.add(cbMagasin, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- ldDateCreation ----
            ldDateCreation.setText("Date de cr\u00e9ation");
            ldDateCreation.setName("ldDateCreation");
            pnlFiltre2.add(ldDateCreation, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateDocument ----
            snPlageDateDocument.setName("snPlageDateDocument");
            snPlageDateDocument.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPlageDateDocumentValueChanged(e);
              }
            });
            pnlFiltre2.add(snPlageDateDocument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlFiltre2.add(snBarreRecherche, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlFiltre.add(pnlFiltre2);
        }
        pnlContenuDocument.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultat ----
        lblTitreResultat.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultat.setName("lblTitreResultat");
        pnlContenuDocument.add(lblTitreResultat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocuments ========
        {
          scpListeDocuments.setPreferredSize(new Dimension(1050, 424));
          scpListeDocuments.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeDocuments.setName("scpListeDocuments");
          
          // ---- tblListeDocuments ----
          tblListeDocuments.setShowVerticalLines(true);
          tblListeDocuments.setShowHorizontalLines(true);
          tblListeDocuments.setBackground(Color.white);
          tblListeDocuments.setRowHeight(20);
          tblListeDocuments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocuments.setAutoCreateRowSorter(true);
          tblListeDocuments.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocuments.setGridColor(new Color(204, 204, 204));
          tblListeDocuments.setName("tblListeDocuments");
          tblListeDocuments.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocuments.setViewportView(tblListeDocuments);
        }
        pnlContenuDocument.add(scpListeDocuments, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlGeneral.add(pnlContenuDocument, "card1");
      
      // ======== pnlContenuLigne ========
      {
        pnlContenuLigne.setBackground(new Color(238, 238, 210));
        pnlContenuLigne.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenuLigne.setOpaque(false);
        pnlContenuLigne.setName("pnlContenuLigne");
        pnlContenuLigne.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenuLigne.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenuLigne.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
        ((GridBagLayout) pnlContenuLigne.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenuLigne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlFiltre3 ========
        {
          pnlFiltre3.setOpaque(false);
          pnlFiltre3.setBorder(new TitledBorder(""));
          pnlFiltre3.setName("pnlFiltre3");
          pnlFiltre3.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre4 ========
          {
            pnlFiltre4.setOpaque(false);
            pnlFiltre4.setName("pnlFiltre4");
            pnlFiltre4.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre4.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre4.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbNumeroDocumentLigne ----
            lbNumeroDocumentLigne.setText("Num\u00e9ro document");
            lbNumeroDocumentLigne.setName("lbNumeroDocumentLigne");
            pnlFiltre4.add(lbNumeroDocumentLigne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfNumeroDocumentLigne ----
            tfNumeroDocumentLigne.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
            tfNumeroDocumentLigne.setName("tfNumeroDocumentLigne");
            tfNumeroDocumentLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfNumeroDocumentLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfNumeroDocumentLigne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbReferenceClientDocumentLigne ----
            lbReferenceClientDocumentLigne.setText("R\u00e9f\u00e9rence document");
            lbReferenceClientDocumentLigne.setName("lbReferenceClientDocumentLigne");
            pnlFiltre4.add(lbReferenceClientDocumentLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfReferenceClientLigne ----
            tfReferenceClientLigne.setName("tfReferenceClientLigne");
            tfReferenceClientLigne.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfReferenceClientLigneFocusLost(e);
              }
            });
            pnlFiltre4.add(tfReferenceClientLigne, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTypeDocumentLigne ----
            lbTypeDocumentLigne.setText("Type document");
            lbTypeDocumentLigne.setName("lbTypeDocumentLigne");
            pnlFiltre4.add(lbTypeDocumentLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbTypeDocumentLigne ----
            cbTypeDocumentLigne.setMinimumSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setPreferredSize(new Dimension(150, 30));
            cbTypeDocumentLigne.setName("cbTypeDocumentLigne");
            cbTypeDocumentLigne.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeDocumentLigneItemStateChanged(e);
              }
            });
            pnlFiltre4.add(cbTypeDocumentLigne, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbClientLigne ----
            lbClientLigne.setText("Client");
            lbClientLigne.setName("lbClientLigne");
            pnlFiltre4.add(lbClientLigne, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snClientLigne ----
            snClientLigne.setName("snClientLigne");
            snClientLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snClientLigneValueChanged(e);
              }
            });
            pnlFiltre4.add(snClientLigne, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVilleLigne ----
            lbVilleLigne.setText("Ville");
            lbVilleLigne.setName("lbVilleLigne");
            pnlFiltre4.add(lbVilleLigne, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCommuneLigne ========
            {
              pnlCommuneLigne.setName("pnlCommuneLigne");
              pnlCommuneLigne.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCommuneLigne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCommuneLigne.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCommuneLigne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCommuneLigne.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- tfCodePostalLigne ----
              tfCodePostalLigne.setEnabled(false);
              tfCodePostalLigne.setName("tfCodePostalLigne");
              pnlCommuneLigne.add(tfCodePostalLigne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfVilleLigne ----
              tfVilleLigne.setEnabled(false);
              tfVilleLigne.setName("tfVilleLigne");
              pnlCommuneLigne.add(tfVilleLigne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlFiltre4.add(pnlCommuneLigne, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbChantierLigne ----
            lbChantierLigne.setText("Chantier");
            lbChantierLigne.setName("lbChantierLigne");
            pnlFiltre4.add(lbChantierLigne, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snChantierLigne ----
            snChantierLigne.setName("snChantierLigne");
            snChantierLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snChantierLigneValueChanged(e);
              }
            });
            pnlFiltre4.add(snChantierLigne, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticleLigne ----
            lbArticleLigne.setText("Article");
            lbArticleLigne.setName("lbArticleLigne");
            pnlFiltre4.add(lbArticleLigne, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snArticleLigne ----
            snArticleLigne.setName("snArticleLigne");
            snArticleLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snArticleLigneValueChanged(e);
              }
            });
            pnlFiltre4.add(snArticleLigne, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre4);
          
          // ======== pnlFiltre5 ========
          {
            pnlFiltre5.setOpaque(false);
            pnlFiltre5.setName("pnlFiltre5");
            pnlFiltre5.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlFiltre5.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre5.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissementLigne ----
            lbEtablissementLigne.setText("Etablissement");
            lbEtablissementLigne.setName("lbEtablissementLigne");
            pnlFiltre5.add(lbEtablissementLigne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissementLigne ----
            snEtablissementLigne.setName("snEtablissementLigne");
            snEtablissementLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snEtablissementLigne, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMagasinLigne ----
            lbMagasinLigne.setText("Magasin");
            lbMagasinLigne.setName("lbMagasinLigne");
            pnlFiltre5.add(lbMagasinLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbMagasinLigne ----
            cbMagasinLigne.setName("cbMagasinLigne");
            cbMagasinLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                cbMagasinLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(cbMagasinLigne, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- ldDateCreationLigne ----
            ldDateCreationLigne.setText("Date de cr\u00e9ation");
            ldDateCreationLigne.setName("ldDateCreationLigne");
            pnlFiltre5.add(ldDateCreationLigne, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snPlageDateLigne ----
            snPlageDateLigne.setName("snPlageDateLigne");
            snPlageDateLigne.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snPlageDateLigneValueChanged(e);
              }
            });
            pnlFiltre5.add(snPlageDateLigne, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRechercheLigne ----
            snBarreRechercheLigne.setName("snBarreRechercheLigne");
            pnlFiltre5.add(snBarreRechercheLigne, new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre3.add(pnlFiltre5);
        }
        pnlContenuLigne.add(pnlFiltre3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultatLigne ----
        lblTitreResultatLigne.setText("Documents correspondant \u00e0 votre recherche");
        lblTitreResultatLigne.setName("lblTitreResultatLigne");
        pnlContenuLigne.add(lblTitreResultatLigne, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeLigne ========
        {
          scpListeLigne.setPreferredSize(new Dimension(1050, 424));
          scpListeLigne.setFont(new Font("sansserif", Font.PLAIN, 14));
          scpListeLigne.setName("scpListeLigne");
          
          // ---- tblListeLigne ----
          tblListeLigne.setShowVerticalLines(true);
          tblListeLigne.setShowHorizontalLines(true);
          tblListeLigne.setBackground(Color.white);
          tblListeLigne.setRowHeight(20);
          tblListeLigne.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeLigne.setAutoCreateRowSorter(true);
          tblListeLigne.setSelectionBackground(new Color(57, 105, 138));
          tblListeLigne.setGridColor(new Color(204, 204, 204));
          tblListeLigne.setName("tblListeLigne");
          tblListeLigne.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeLigneMouseClicked(e);
            }
          });
          scpListeLigne.setViewportView(tblListeLigne);
        }
        pnlContenuLigne.add(scpListeLigne, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlGeneral.add(pnlContenuLigne, "card2");
    }
    add(pnlGeneral, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup2 ----
    buttonGroup2.add(rbRechercheDocument);
    buttonGroup2.add(rbRechercheLigne);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelTitre pnlChoixTypeRecherche;
  private SNRadioButton rbRechercheDocument;
  private SNRadioButton rbRechercheLigne;
  private JPanel pnlGeneral;
  private JPanel pnlContenuDocument;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbNumeroDocument;
  private SNIdentifiant tfNumeroDocument;
  private SNLabelChamp lbReferenceClientDocument;
  private SNTexte tfReferenceClientDocument;
  private SNLabelChamp lbTypeDocument;
  private SNComboBox cbTypeDocument;
  private SNLabelChamp lbTypeClient;
  private XRiComboBox cbTypeClient;
  private SNLabelChamp lbClient;
  private SNClient snClient;
  private SNLabelChamp lbVille;
  private SNPanel pnlCommune;
  private SNTexte tfCodePostal;
  private SNTexte tfVille;
  private SNLabelChamp lbChantier;
  private SNChantier snChantierDocument;
  private SNLabelChamp lbArticle;
  private SNArticle snArticleDocument;
  private JPanel pnlFiltre2;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin cbMagasin;
  private SNLabelChamp ldDateCreation;
  private SNPlageDate snPlageDateDocument;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JScrollPane scpListeDocuments;
  private NRiTable tblListeDocuments;
  private JPanel pnlContenuLigne;
  private JPanel pnlFiltre3;
  private JPanel pnlFiltre4;
  private SNLabelChamp lbNumeroDocumentLigne;
  private SNTexte tfNumeroDocumentLigne;
  private SNLabelChamp lbReferenceClientDocumentLigne;
  private SNTexte tfReferenceClientLigne;
  private SNLabelChamp lbTypeDocumentLigne;
  private SNComboBox cbTypeDocumentLigne;
  private SNLabelChamp lbClientLigne;
  private SNClient snClientLigne;
  private SNLabelChamp lbVilleLigne;
  private SNPanel pnlCommuneLigne;
  private SNTexte tfCodePostalLigne;
  private SNTexte tfVilleLigne;
  private SNLabelChamp lbChantierLigne;
  private SNChantier snChantierLigne;
  private SNLabelChamp lbArticleLigne;
  private SNArticle snArticleLigne;
  private JPanel pnlFiltre5;
  private SNLabelChamp lbEtablissementLigne;
  private SNEtablissement snEtablissementLigne;
  private SNLabelChamp lbMagasinLigne;
  private SNMagasin cbMagasinLigne;
  private SNLabelChamp ldDateCreationLigne;
  private SNPlageDate snPlageDateLigne;
  private SNBarreRecherche snBarreRechercheLigne;
  private SNLabelTitre lblTitreResultatLigne;
  private JScrollPane scpListeLigne;
  private NRiTable tblListeLigne;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup buttonGroup2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
