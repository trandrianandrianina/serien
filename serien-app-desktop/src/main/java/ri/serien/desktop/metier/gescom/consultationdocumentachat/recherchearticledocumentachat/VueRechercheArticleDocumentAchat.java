/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat.recherchearticledocumentachat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueRechercheArticleDocumentAchat extends AbstractVueDialogue<ModeleRechercheArticleDocumentAchat> {
  // Constantes
  public final static String[] TITRE_LISTE = new String[] { "Code article", "Libell\u00e9" };
  private final static String TITRE_RESULTATS = "Articles correspondants à votre recherche (";
  
  // Variables
  private DefaultTableModel tableModelArticle = null;
  private JTableCellRendererArticlesDocumentAchat listeArticlesRenderer = new JTableCellRendererArticlesDocumentAchat();
  
  /**
   * Constructeur.
   */
  public VueRechercheArticleDocumentAchat(ModeleRechercheArticleDocumentAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1000, 400);
    setResizable(false);
    
    tableModelArticle = new DefaultTableModel(null, TITRE_LISTE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    
    scpListeArticles.getViewport().setBackground(Color.WHITE);
    tblListeArticles.personnaliserAspect(TITRE_LISTE, new int[] { 100, 500 }, new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE }, 13);
    tblListeArticles.personnaliserAspectCellules(new JTableRechercheArticleDocumentAchatRenderer());
    
    // Permet de redéfinir la touche Enter sur la liste
    // Action originalAction = retournerActionComposant(tblListeDocuments, enter);
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListeArticles.getSelectedRowCount() > 0) {
          validerListeSelection();
        }
      }
    };
    tblListeArticles.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessage();
    rafraichirArticle();
    rafraichirTitreListeArticle();
    
    rafraichirListe();
    
    if (getModele().getListeResultatRechercheArticles() != null && getModele().getListeResultatRechercheArticles().size() > 0) {
      tblListeArticles.requestFocus();
    }
    rafraichirBoutonValider();
  }
  
  public void rafraichirBoutonValider() {
    boolean actif = tblListeArticles.getSelectedRow() >= 0;
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessage() {
    if (getModele().getMessageErreur() != null) {
      lbMessages.setText(getModele().getMessageErreur());
    }
    else {
      lbMessages.setText("");
    }
  }
  
  private void rafraichirArticle() {
    if (getModele().getTexteRecherche() != null) {
      tfArticle.setText(getModele().getTexteRecherche());
    }
    else {
      tfArticle.setText("");
    }
  }
  
  private void rafraichirTitreListeArticle() {
    List<Article> liste = getModele().getListeResultatRechercheArticles();
    if (liste != null && liste.size() > 1) {
      lbTitreListeArticles.setText("Articles correspondants à votre recherche (" + liste.size() + ")");
    }
    else if (liste != null && liste.size() == 1) {
      lbTitreListeArticles.setText("Article correspondant à votre recherche");
    }
    else {
      lbTitreListeArticles.setText("Aucun article ne correspond à votre recherche");
    }
  }
  
  /**
   * Charge les données dans la liste.
   */
  public void rafraichirListe() {
    // Convertir les données à afficher dans le format attendu par le tableau
    List<Article> listeArticles = getModele().getListeResultatRechercheArticles();
    String[][] codesArticles = null;
    if (listeArticles != null) {
      codesArticles = new String[listeArticles.size()][TITRE_LISTE.length];
      for (int i = 0; i < listeArticles.size(); i++) {
        Article article = listeArticles.get(i);
        String[] ligne = codesArticles[i];
        
        // Code article
        ligne[0] = article.getId().getCodeArticle();
        
        // Libéllé article
        ligne[1] = article.getLibelleComplet();
        
        codesArticles[i][0] = article.getId().getCodeArticle();
      }
    }
    
    // Mettre à jour les données
    tableModelArticle.setRowCount(0);
    if (codesArticles != null) {
      for (String[] ligne : codesArticles) {
        tableModelArticle.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (tblListeArticles.getModel() != tableModelArticle) {
      tblListeArticles.setModel(tableModelArticle);
      tblListeArticles.setGridColor(new Color(204, 204, 204));
      tblListeArticles.setDefaultRenderer(Object.class, listeArticlesRenderer);
    }
  }
  
  /**
   * Récupère le client sélectionné.
   */
  private void validerListeSelection() {
    int indexvisuel = tblListeArticles.getSelectedRow();
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeArticles.getRowSorter() != null) {
        indexreel = tblListeArticles.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().selectionnerArticle(getModele().getListeResultatRechercheArticles().get(indexreel));
      dispose();
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        validerListeSelection();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeClientsMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        validerListeSelection();
      }
      rafraichirBoutonValider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfArticleFocusLost(FocusEvent e) {
    try {
      getModele().setTexteRecherche(tfArticle.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btLancerRechercheActionPerformed(ActionEvent e) {
    try {
      getModele().lancerRecherche();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    pnlRecherche = new JPanel();
    lbArticle = new JLabel();
    tfArticle = new XRiTextField();
    lbMessages = new JLabel();
    snBarreRecherche = new SNBarreRecherche();
    lbTitreListeArticles = new JLabel();
    scpListeArticles = new JScrollPane();
    tblListeArticles = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1000, 600));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Recherche d'article");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 51, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 65, 0, 250, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlRecherche ========
        {
          pnlRecherche.setOpaque(false);
          pnlRecherche.setBorder(new TitledBorder(""));
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecherche.getLayout()).columnWidths = new int[] { 0, 197, 279, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbArticle.setPreferredSize(new Dimension(100, 30));
          lbArticle.setMinimumSize(new Dimension(100, 30));
          lbArticle.setName("lbArticle");
          pnlRecherche.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfArticle ----
          tfArticle.setFont(tfArticle.getFont().deriveFont(tfArticle.getFont().getSize() + 2f));
          tfArticle.setPreferredSize(new Dimension(330, 30));
          tfArticle.setName("tfArticle");
          tfArticle.setMinimumSize(new Dimension(330, 30));
          tfArticle.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfArticleFocusLost(e);
            }
          });
          pnlRecherche.add(tfArticle, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMessages ----
          lbMessages.setText("MESSAGES");
          lbMessages.setFont(new Font(Font.SANS_SERIF, Font.BOLD, 13));
          lbMessages.setForeground(new Color(255, 0, 51));
          lbMessages.setMinimumSize(new Dimension(400, 30));
          lbMessages.setPreferredSize(new Dimension(400, 30));
          lbMessages.setName("lbMessages");
          pnlRecherche.add(lbMessages, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setMinimumSize(new Dimension(300, 70));
          snBarreRecherche.setPreferredSize(new Dimension(300, 70));
          snBarreRecherche.setName("snBarreRecherche");
          pnlRecherche.add(snBarreRecherche, new GridBagConstraints(2, 1, 1, 1, 0.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTitreListeArticles ----
        lbTitreListeArticles.setText("Articles correspondants \u00e0 votre recherche");
        lbTitreListeArticles.setFont(lbTitreListeArticles.getFont().deriveFont(lbTitreListeArticles.getFont().getStyle() | Font.BOLD,
            lbTitreListeArticles.getFont().getSize() + 2f));
        lbTitreListeArticles.setName("lbTitreListeArticles");
        pnlContenu.add(lbTitreListeArticles, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeArticles ========
        {
          scpListeArticles.setPreferredSize(new Dimension(1050, 424));
          scpListeArticles.setName("scpListeArticles");
          
          // ---- tblListeArticles ----
          tblListeArticles.setShowVerticalLines(true);
          tblListeArticles.setShowHorizontalLines(true);
          tblListeArticles.setBackground(Color.white);
          tblListeArticles.setRowHeight(20);
          tblListeArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeArticles.setAutoCreateRowSorter(true);
          tblListeArticles.setSelectionBackground(new Color(57, 105, 138));
          tblListeArticles.setGridColor(new Color(204, 204, 204));
          tblListeArticles.setName("tblListeArticles");
          tblListeArticles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeClientsMouseClicked(e);
            }
          });
          scpListeArticles.setViewportView(tblListeArticles);
        }
        pnlContenu.add(scpListeArticles, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JPanel pnlRecherche;
  private JLabel lbArticle;
  private XRiTextField tfArticle;
  private JLabel lbMessages;
  private SNBarreRecherche snBarreRecherche;
  private JLabel lbTitreListeArticles;
  private JScrollPane scpListeArticles;
  private NRiTable tblListeArticles;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
