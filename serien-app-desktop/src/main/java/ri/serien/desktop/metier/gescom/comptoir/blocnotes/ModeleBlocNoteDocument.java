/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.blocnotes;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleBlocNoteDocument extends AbstractModeleDialogue {
  // Variables
  private ModeleComptoir parent = null;
  private DocumentVente documentvente = null;
  private BlocNote blocNote = null;
  
  /**
   * Constructeur.
   */
  public ModeleBlocNoteDocument(SessionBase pSession, ModeleComptoir pParent, DocumentVente pDocumentVente) {
    super(pSession);
    parent = pParent;
    documentvente = pDocumentVente;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Lecture du bloc-note du document
    lirePetitBlocNote();
  }
  
  @Override
  public void quitterAvecValidation() {
    enregistrerBlocNote();
    super.quitterAvecValidation();
  }
  
  // Méthodes publiques
  
  /**
   * Modifie le texte saisie.
   */
  public void modifierTexte(String pTexte) {
    if (pTexte == null) {
      pTexte = "";
    }
    
    if (blocNote == null) {
      IdBlocNote idBlocNote = IdBlocNote.getInstancePourDocumentVente(documentvente.getId());
      blocNote = new BlocNote(idBlocNote);
    }
    
    blocNote.setTexte(pTexte);
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Lire le petit bloc-note.
   */
  private void lirePetitBlocNote() {
    if (documentvente.isExistant()) {
      blocNote = ManagerServiceDocumentVente.chargerPetitBlocNoteDocumentVente(getIdSession(), documentvente.getId());
      
      documentvente.setPetitBlocNote(blocNote);
      if (blocNote != null) {
        blocNote.setTexte(blocNote.getTexte().trim());
      }
    }
    else {
      if (documentvente.getPetitBlocNote() != null) {
        modifierTexte(documentvente.getPetitBlocNote().getTexte());
      }
    }
  }
  
  /**
   * Enregistre le petit bloc-note.
   */
  public void enregistrerBlocNote() {
    documentvente.setPetitBlocNote(blocNote);
    if (documentvente.isExistant()) {
      ManagerServiceDocumentVente.sauverPetitBlocNoteDocumentVente(getIdSession(), documentvente.getId(),
          documentvente.getPetitBlocNote());
    }
  }
  
  // -- Accesseurs
  
  public BlocNote getBlocNote() {
    return blocNote;
  }
}
