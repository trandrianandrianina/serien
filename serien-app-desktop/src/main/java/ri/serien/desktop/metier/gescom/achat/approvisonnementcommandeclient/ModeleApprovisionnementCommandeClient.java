/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisonnementcommandeclient;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumTypeApprovisionnement;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleApprovisionnementCommandeClient extends AbstractModeleDialogue {
  // Variables
  private ModeleAchat modeleParent = null;
  private IdEtablissement idEtablissement = null;
  private Fournisseur fournisseur = null;
  private IdMagasin idMagasin = null;
  private ArrayList<DocumentVente> listeDocumentVenteChoisi = null;
  private Message message = null;
  private int indiceLigneAAfficherSelectionnee = -1;
  private EnumTypeApprovisionnement typeApprovisiosnnement;
  
  // Variables pour le chargement progressif de la liste
  private ListeLigneATraiter listeLigneATraiter = new ListeLigneATraiter();
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheArticle = -1;
  
  /**
   * Constructeur.
   */
  public ModeleApprovisionnementCommandeClient(SessionBase pSession, ModeleAchat pModeleParent,
      EnumTypeApprovisionnement pTypeApprovisionnment, Fournisseur pFournisseur, IdMagasin pIdMagasin,
      ArrayList<DocumentVente> pListeDocumentVenteChoisi) {
    super(pSession);
    modeleParent = pModeleParent;
    fournisseur = pFournisseur;
    idMagasin = pIdMagasin;
    listeDocumentVenteChoisi = pListeDocumentVenteChoisi;
    typeApprovisiosnnement = pTypeApprovisionnment;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    switch (typeApprovisiosnnement) {
      case CLIENT:
        setTitreEcran("Liste des lignes des commandes clients non livrées");
        break;
      case DIRECT_USINE:
        setTitreEcran("Liste des lignes des commandes directs usines");
        break;
      default:
        setTitreEcran("");
    }
  }
  
  @Override
  public void chargerDonnees() {
    // Contrôle des variables obligatoires
    if (fournisseur == null || fournisseur.getId() == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    if (idMagasin == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    idEtablissement = fournisseur.getId().getIdEtablissement();
    
    // Lancement de la recherche dès l'affichage de la boite de dialogue
    rechercherArticlesAApprovisionner();
  }
  
  @Override
  public void quitterAvecValidation() {
    // Finir de charger les lignes qui n'ont pas été visionnées avant leur transformation en ligne d'achat afin d'avoir toutes les
    // données nécessaires
    chargerPlageLigneTableau(0, listeLigneATraiter.getNombreLigneAAfficher());
    
    for (LigneATraiter ligneATraiter : listeLigneATraiter) {
      // Conversion de la ligne à afficher (mais non affichée ) de type commentaire en ligne d'achat commentaire
      if (ligneATraiter.isLigneCommentaire()) {
        convertirLigneAAfficherEnLigneAchatCommentaire(ligneATraiter);
      }
      // Contrôle que les lignes à afficher qui ont une quantité renseignée et supérieure à zéro soient bien transformées en ligne d'achat
      // si cela n'a pas déjà fait. Les lignes sont insérées en fin systématiquement car le service RPG insére parfois des lignes
      // intermédaires (comme le nom du client) ce qui pose problème si on se base uniquement sur l'indice de la ligne affichée
      else {
        convertirLigneAAfficherEnLigneAchat(ligneATraiter, true);
      }
    }
    
    // Chargement du document d'achat
    modeleParent.setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    
    // Traitement normal
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    // Suppression éventuelle des lignes d'achat qui auraient pu être crée (lors de l'affichage du détail)
    List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
    for (LigneATraiter ligneATraiter : listeLigneATraiter) {
      if (ligneATraiter.getLigneAchatAssociee() != null) {
        listeLigneASupprimer.add(ligneATraiter.getLigneAchatAssociee());
      }
    }
    // Appel du service qui va supprimer ces lignes
    if (!listeLigneASupprimer.isEmpty()) {
      ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
      // Chargement du document
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    }
    
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Afficher la plage de lignes d'achat comprise entre deux lignes.
   * Les informations des lignes d'achats sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticlesAffichees(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeLigneATraiter == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeLigneATraiter.size()) {
      indexPremiereLigneAffichee = listeLigneATraiter.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeLigneATraiter == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeLigneATraiter.size()) {
      nombreLigneAffichee = listeLigneATraiter.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacements de l'ascenseur avec la souris
    if (indexRechercheArticle < indexPremiereLigneAffichee) {
      indexRechercheArticle = indexPremiereLigneAffichee;
    }
    else if (indexRechercheArticle > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheArticle = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleApprovisionnementCommandeClient.class, "modifierPlageLignesAffichees", "indexPremiereLigneVisible",
        indexPremiereLigneAffichee, "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans la liste.
   */
  public void modifierIndiceTableau(int pIndiceLigneSelectionnee) {
    indiceLigneAAfficherSelectionnee = pIndiceLigneSelectionnee;
  }
  
  /**
   * Modifie la quantité d'une ligne de la liste à traiter.
   */
  public void modifierQuantiteUCA(int pIndiceTableau, String pSaisie) {
    if (!isQuantiteModifiee(pIndiceTableau, pSaisie)) {
      return;
    }
    message = null;
    indiceLigneAAfficherSelectionnee = pIndiceTableau;
    LigneATraiter ligneATraiter = retournerLigneATraiter();
    if (ligneATraiter == null || ligneATraiter.getArticle() == null) {
      return;
    }
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    // Traitement de la quantité saisie si celle-ci vaut 0
    if (saisie.compareTo(BigDecimal.ZERO) == 0) {
      // Suppression de la ligne d'achat si celle-ci existe
      if (ligneATraiter.getLigneAchatAssociee() != null) {
        List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
        listeLigneASupprimer.add(ligneATraiter.getLigneAchatAssociee());
        ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
        ligneATraiter.setLigneAchatAssociee(null);
      }
      ligneATraiter.modifierQuantite(saisie);
      rafraichir();
      return;
    }
    
    // On contrôle la quantité saisie et on met à jour la quantité UCA de la ligne du tableau que l'on affiche
    try {
      ligneATraiter.controlerQuantite(pSaisie);
      ligneATraiter.modifierQuantite(saisie);
    }
    catch (Exception e) {
      message = Message.getMessageImportant(e.getMessage());
      rafraichir();
      return;
    }
    
    rafraichir();
  }
  
  /**
   * Vérifie qu'au moins un article a été commandé.
   */
  public boolean isVerifierArticleCommande() {
    if (listeLigneATraiter == null || modeleParent.getDocumentAchatEnCours() == null) {
      return false;
    }
    // Parcours des lignes à afficher à la recherche d'une quantité commandée supérieure à 0
    for (LigneATraiter ligneAAfficher : listeLigneATraiter) {
      if (ligneAAfficher.isLigneCommentaire()) {
        continue;
      }
      BigDecimal valeur = ligneAAfficher.getQuantiteCommandeeUCABigDecimal();
      if (valeur != null && valeur.compareTo(BigDecimal.ZERO) > 0) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Affiche le détail d'un article.
   */
  public void afficherDetail() {
    LigneATraiter ligneTableau = retournerLigneATraiter();
    if (ligneTableau == null) {
      rafraichir();
      return;
    }
    
    BigDecimal quantiteCommandeeOrigine = ligneTableau.getQuantiteCommandeeUCABigDecimal();
    // Contrôle que la quantité du tableau soit supérieure à 0
    if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) <= 0) {
      ligneTableau.modifierQuantite(BigDecimal.ONE);
    }
    
    LigneAchatArticle ligneAchatArticle = ligneTableau.getLigneAchatAssociee();
    if (ligneAchatArticle == null) {
      ligneAchatArticle = (LigneAchatArticle) convertirLigneAAfficherEnLigneAchat(ligneTableau, false);
    }
    
    boolean validation = modeleParent.selectionnerLigneArticleStandard(ligneAchatArticle, ModeleDetailLigneArticle.ONGLET_GENERAL,
        ModeleAchat.TABLE_LIGNES_DOCUMENT, null, false, false, false);
    
    // Met à jour la quantité UCA de la ligne du tableau que l'on affiche dans le cas on l'on a validé la quanitté dans le détail
    if (validation) {
      ligneTableau.modifierQuantite(ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA());
    }
    else {
      ligneTableau.modifierQuantite(quantiteCommandeeOrigine);
      // Si la quantité d'origine était à zéro à l'origine alors il faut supprimer le ligne de commande que l'on a créé pour pouvoir
      // afficher le détail
      if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) == 0) {
        List<IdLigneAchat> listeIdLigneAchat = new ArrayList<IdLigneAchat>();
        listeIdLigneAchat.add(ligneTableau.getLigneAchatAssociee().getId());
        modeleParent.setDocumentAchatEnCours(DocumentAchat.supprimerListeLigneAchat(getIdSession(),
            modeleParent.getDocumentAchatEnCours(), listeIdLigneAchat, ManagerSessionClient.getInstance().getProfil()));
        ligneTableau.setLigneAchatAssociee(null);
      }
    }
    rafraichir();
  }
  
  /**
   * Détermine si l'on doit afficher le bouton "Afficher détail".
   */
  public boolean isAfficherBoutonDetail() {
    // Si aucune ligne n'est sélectionnée on n'affiche pas le bouton
    if (indiceLigneAAfficherSelectionnee == -1) {
      return false;
    }
    // Contrôle si la ligne sélectionnée correspond à un article spécial
    LigneATraiter ligneATraiter = retournerLigneATraiter();
    if (ligneATraiter == null) {
      return false;
    }
    Article article = ligneATraiter.getArticle();
    return article != null && !article.isArticleSpecial();
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les variables liées au chargement des données dans le tableau.
   */
  private void effacerVariablesChargement() {
    indexPremiereLigneAffichee = 0;
    indexRechercheArticle = -1;
    listeLigneATraiter.clear();
    
    indiceLigneAAfficherSelectionnee = -1;
  }
  
  /**
   * Recherche les articles à approvisionner.
   * Les articles palettes et transports sont exclus de cette liste.
   */
  private void rechercherArticlesAApprovisionner() {
    message = null;
    listeLigneATraiter.clear();
    
    // Vérifier que la liste des documents de vente n'est pas vide
    if (listeDocumentVenteChoisi == null) {
      message = Message.getMessageImportant("La liste des documents de vente choisis est invalide.");
      return;
    }
    
    // Construire la liste des lignes à afficher à partir des documents sélectionnés par l'utilisateur
    int indiceLigne = 0;
    for (DocumentVente documentVenteChoisi : listeDocumentVenteChoisi) {
      // Vérification qu'il n'y ait pas un document vide dans la liste
      if (documentVenteChoisi == null) {
        continue;
      }
      
      // Chargement de toutes les lignes du document choisi dans le but de récupérer les lignes commentaires
      CritereLigneVente critereLigneVente = new CritereLigneVente();
      ListeLigneVente listeLigneVente = ManagerServiceDocumentVente.chargerListeLigneVente(getIdSession(), documentVenteChoisi.getId(),
          critereLigneVente, documentVenteChoisi.isTTC());
      if (listeLigneVente == null) {
        continue;
      }
      for (LigneVente ligneVente : listeLigneVente) {
        // Chargement des lignes commentaires (ceux uniquement à éditer dans la commande d'achat) et des lignes à afficher contenues dans
        // les documents sélectionnés
        LigneVente ligneVenteAAfficher = documentVenteChoisi.getListeLigneVente().get(ligneVente.getId());
        if (ligneVenteAAfficher != null || ligneVente.isLigneCommentaireACopier()) {
          listeLigneATraiter.add(new LigneATraiter(ligneVente, indiceLigne, documentVenteChoisi.isLivraison()));
          indiceLigne++;
        }
      }
    }
    
    // Contrôler que des lignes de vente sont présentes
    if (listeLigneATraiter.isEmpty()) {
      message = Message.getMessageImportant("Aucune ligne de vente n'a été trouvé.");
      return;
    }
    
    // Charger les lignes du tableau affichés
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
  }
  
  /**
   * Charger les données des lignes du tableau comprises entre deux lignes.
   * Les informations des lignes du tableau sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageLigneTableau(int pPremiereLigne, int pNombreLigne) {
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    List<LigneATraiter> listeAAfficher = listeLigneATraiter.getListeLigneAAfficher();
    if (listeAAfficher == null) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeAAfficher.size());
    
    // Lister les id des lignes du tableau dont il faut charger les informations
    List<IdLigneVente> listeIdLigneVente = new ArrayList<IdLigneVente>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneATraiter ligneTableau = listeAAfficher.get(i);
      if (!ligneTableau.isCharge()) {
        listeIdLigneVente.add(ligneTableau.getIdLigneVente());
      }
    }
    
    // Quitter immédiatement si aucune ligne vente n'est à charger
    if (listeIdLigneVente.isEmpty()) {
      return;
    }
    
    // Charger les informations des lignes du tableau
    ParametresLireArticle parametres = new ParametresLireArticle();
    parametres.setIdEtablissement(idEtablissement);
    parametres.setIdFournisseur(fournisseur.getId());
    parametres.setIdMagasin(idMagasin);
    List<ReapprovisionnementClient> listeReapproClientCharges =
        ManagerServiceDocumentVente.chargerListeArticlesAApprovisionner(getIdSession(), listeIdLigneVente, parametres);
    
    // Mettre les lignes du tableau chargées au bon endroit dans la liste
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneATraiter ligneTableau = listeAAfficher.get(i);
      if (!ligneTableau.isCharge()) {
        for (ReapprovisionnementClient reapproClientCharge : listeReapproClientCharges) {
          if (ligneTableau.getIdLigneVente().equals(reapproClientCharge.getIdLigneVente())) {
            listeAAfficher.get(i).initialiserDonnees(reapproClientCharge);
            break;
          }
        }
      }
    }
    
    // Efface le contenu de la liste qui est devenu inutile
    listeAAfficher.clear();
  }
  
  /**
   * Retourne la ligne sélectionnée de la liste des lignes à traiter.
   * Cette méthode permet d'ignorer les lignes commentaires.
   */
  private LigneATraiter retournerLigneATraiter() {
    if (indiceLigneAAfficherSelectionnee < 0 || indiceLigneAAfficherSelectionnee >= listeLigneATraiter.size()) {
      return null;
    }
    int indice = 0;
    int indiceLigneAAfficher = 0;
    LigneATraiter ligneAAfficher = null;
    while (indiceLigneAAfficher <= indiceLigneAAfficherSelectionnee) {
      if (listeLigneATraiter.get(indice).isLigneCommentaire()) {
        indice++;
        continue;
      }
      ligneAAfficher = listeLigneATraiter.get(indice);
      indice++;
      indiceLigneAAfficher++;
    }
    return ligneAAfficher;
  }
  
  /**
   * Retourne la ligne d'achat article correspondant à l'id article.
   */
  private LigneAchatArticle retournerLigneAchatArticle(IdLigneAchat pIdLigneAchat) {
    if (pIdLigneAchat == null) {
      return null;
    }
    ListeLigneAchat listeLignesDocument = modeleParent.getDocumentAchatEnCours().getListeLigneAchat();
    // Récupération de la ligne achat article
    LigneAchat ligneAchat = listeLignesDocument.get(pIdLigneAchat);
    // Il est fortement possible que l'on ait annuler lors de la demande de confirmation (affichage des commandes en cours)
    if (ligneAchat == null) {
      return null;
    }
    LigneAchatArticle ligneAchatArticle = (LigneAchatArticle) ligneAchat;
    return ligneAchatArticle;
  }
  
  /**
   * Créer une ligne article d'achat à partir d'une ligne de vente.
   */
  private LigneAchat convertirLigneAAfficherEnLigneAchat(LigneATraiter pLigneATraiter, boolean pInsererEnFin) {
    if (pLigneATraiter == null || pLigneATraiter.isLigneCommentaire()) {
      return null;
    }
    
    // Contrôle que la quantité du tableau soit supérieure à 0
    BigDecimal valeur = pLigneATraiter.getQuantiteCommandeeUCABigDecimal();
    if (valeur.compareTo(BigDecimal.ZERO) <= 0) {
      return null;
    }
    
    // Créer une ligne article dans le cas d'un ajout de ligne
    if (pLigneATraiter.getLigneAchatAssociee() == null) {
      // Déterminer l'indice d'insertion dans la liste des lignes
      int indiceInsertion = pLigneATraiter.getIndiceLigneDansListeATraiter();
      if (pInsererEnFin) {
        indiceInsertion = -1;
      }
      // Création de la ligne
      IdLigneAchat idLigneAchat = modeleParent.creerLigneAchat(pLigneATraiter.getArticle(), indiceInsertion,
          pLigneATraiter.getQuantiteCommandeeUCABigDecimal(), pLigneATraiter.getIdLigneVente());
      // Chargement des lignes du document achat afin de récupérer la ligne proprement
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
      LigneAchat ligneAchat = modeleParent.getDocumentAchatEnCours().retournerLigneAchatAvecId(idLigneAchat);
      LigneAchatArticle ligneAchatArticle = (LigneAchatArticle) ligneAchat;
      // Récupération de la ligne achat article
      ligneAchat.getPrixAchat().setQuantiteReliquatUCA(valeur, true);
      pLigneATraiter.setLigneAchatAssociee(ligneAchatArticle);
      
      return ligneAchat;
    }
    // Dans le cas d'une mise à jour d'une ligne achat déjà existante
    else {
      LigneAchatArticle ligneAchat = pLigneATraiter.getLigneAchatAssociee();
      ligneAchat.getPrixAchat().setQuantiteReliquatUCA(valeur, true);
      // S'il s'agit d'une commande direct usine le type de la ligne doit être ligne spéciale non stockée (code ERL == 'S')
      if (modeleParent.getDocumentAchatEnCours().isDirectUsine() && !ligneAchat.isLigneCommentaire()) {
        ligneAchat.setTypeLigne(EnumTypeLigneAchat.SPECIALE_NON_STOCKEE);
      }
      return ligneAchat;
    }
  }
  
  /**
   * Créer une ligne achat commentaire à partir d'une ligne commentaire vente.
   */
  private LigneAchat convertirLigneAAfficherEnLigneAchatCommentaire(LigneATraiter pLigneATraiter) {
    if (pLigneATraiter == null || !pLigneATraiter.isLigneCommentaire()) {
      return null;
    }
    
    // Si la ligne commentaire a été demandée éditable dans les achats
    String commentaire = pLigneATraiter.getLigneVente().getLibelle();
    // Initialisation de la ligne commentaire
    IdLigneAchat idLigneAchat = modeleParent.creerLigneAchatCommentaire(commentaire, pLigneATraiter.getIndiceLigneDansListeATraiter());
    LigneAchatCommentaire ligneAchatCommentaire = new LigneAchatCommentaire(idLigneAchat);
    ligneAchatCommentaire.setTexte(commentaire);
    
    // Ajout de la ligne dans la liste des lignes du document d'achat (nécessaire pour générer les identifiants des lignes)
    // Pas de rechergement complet car non nécessaire pour les lignes commentaires
    modeleParent.getDocumentAchatEnCours().getListeLigneAchat().add(ligneAchatCommentaire);
    
    return ligneAchatCommentaire;
  }
  
  /**
   * Contrôle si la quantitée saisie est différente de la valeur d'origine.
   */
  private boolean isQuantiteModifiee(int pIndiceTableau, String pSaisie) {
    indiceLigneAAfficherSelectionnee = pIndiceTableau;
    LigneATraiter ligneTableau = retournerLigneATraiter();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      return false;
    }
    pSaisie = Constantes.normerTexte(pSaisie);
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    return saisie.compareTo(ligneTableau.getQuantiteCommandeeUCABigDecimal()) != 0;
  }
  
  /**
   * Indique si l'article de la ligne de vente est un article transport.
   */
  private boolean isArticleTransport(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return false;
    }
    
    IdArticle idArticle = pLigneVente.getIdArticle();
    if (idArticle == null) {
      return false;
    }
    
    Article article = ManagerServiceArticle.lireArticle(getIdSession(), idArticle);
    if (article == null) {
      return false;
    }
    
    return article.isArticleTransport();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des lignes à afficher.
   * 
   * @return
   */
  public List<LigneATraiter> getListeLigneAAfficher() {
    return listeLigneATraiter.getListeLigneAAfficher();
  }
  
  /**
   * Retourne un message à afficher.
   * 
   * @return
   */
  public Message getMessage() {
    return message;
  }
  
  /**
   * Retourne l'indice courant de la liste affichée.
   * 
   * @return
   */
  public int getIndiceLigneAAfficherSelectionnee() {
    return indiceLigneAAfficherSelectionnee;
  }
  
}
