/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.regroupement;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.negociation.NegociationVenteGlobale;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleRegroupementLignes extends AbstractModeleDialogue {
  // Constantes
  public static final int ONGLET_GENERAL = 0;
  public static final int ONGLET_NEGOCIATION = 1;
  
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_GENERAL_TITRE = 1;
  public static final int FOCUS_NEGO_VENTE_REMISE = 2;
  
  // Variables
  private int composantAyantLeFocus = FOCUS_NON_INDIQUE;
  private ModeleComptoir modeleParent = null;
  private Regroupement regroupement = null;
  private DocumentVente documentVente = null;
  private IdClient idClient = null;
  private String texteTitre = "";
  private String texteTotal = "";
  private boolean editionDetail = false;
  private boolean editionPrix = false;
  private boolean affichageTTC = false;
  private boolean afficherDetails = true;
  private NegociationVenteGlobale negociationVenteRegroupementOrigine = null;
  private NegociationVenteGlobale negociationVenteRegroupementNegocie = null;
  private Date maintenant = null;
  
  /**
   * Constructeur.
   */
  public ModeleRegroupementLignes(SessionBase pSession, ModeleComptoir pModeleParent, Regroupement pRegroupement,
      DocumentVente pDocumentVente, IdClient pIdClient) {
    super(pSession);
    modeleParent = pModeleParent;
    regroupement = pRegroupement;
    documentVente = pDocumentVente;
    if (documentVente != null) {
      affichageTTC = documentVente.isTTC();
    }
    idClient = pIdClient;
    composantAyantLeFocus = FOCUS_GENERAL_TITRE;
  }
  
  // Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    if (regroupement != null) {
      texteTitre = regroupement.getTexteTitre();
      texteTotal = regroupement.getTexteTotal();
      editionDetail = regroupement.isEditionDetailArticles();
      editionPrix = regroupement.isEditionPrixArticles();
      
      if (texteTitre == null) {
        texteTitre = "Regroupement ";
      }
      
      if (texteTotal == null) {
        texteTotal = "Total regroupement ";
      }
      
      // La négociation
      // On recherche le tarif pour chaque article (provisoire)
      maintenant = new Date();
      
      ListeLigneVente listeLigneVente = regroupement.getListeLigneVente();
      if (listeLigneVente == null) {
        throw new MessageErreurException("La liste des lignes du regroupement est invalide.");
      }
      
      // Initialisation de la classe négociation vente d'origine
      negociationVenteRegroupementOrigine = new NegociationVenteGlobale(getIdSession(), affichageTTC, isModeNegoce());
      negociationVenteRegroupementOrigine.initialiser(listeLigneVente);
      
      // Initialisation de la classe négociation vente modifiée
      negociationVenteRegroupementNegocie = new NegociationVenteGlobale(getIdSession(), affichageTTC, isModeNegoce());
      negociationVenteRegroupementNegocie.initialiser(listeLigneVente);
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    regroupement.setTexteTitre(texteTitre);
    regroupement.setTexteTotal(texteTotal);
    regroupement.setEditionDetailArticles(editionDetail);
    regroupement.setEditionPrixArticles(editionPrix);
    // Met à jour des lignes de ventes négociées
    for (LigneVente ligneVente : regroupement.getListeLigneVente()) {
      LigneVente ligneVenteNegociee = negociationVenteRegroupementNegocie.getLigneVente(ligneVente.getId());
      if (ligneVenteNegociee != null) {
        if (!controlerLigneDocument(ligneVenteNegociee)) {
          return;
        }
        ManagerServiceDocumentVente.modifierLignedocumentVente(getIdSession(), ligneVenteNegociee);
      }
    }
    
    super.quitterAvecValidation();
  }
  
  /**
   * Supprime un regroupement de lignes articles.
   */
  @Override
  public void quitterAvecSuppression() {
    if (regroupement == null) {
      return;
    }
    ListeLigneVente listeLigneVente = regroupement.getListeLigneVente();
    ArrayList<IdLigneVente> listeIdLignes = new ArrayList<IdLigneVente>();
    
    if (listeLigneVente == null || listeLigneVente.isEmpty()) {
      return;
    }
    
    for (LigneVente ligne : listeLigneVente) {
      listeIdLignes.add(ligne.getId());
    }
    
    if (regroupement != null) {
      regroupement.retourneListeIdLignesHorsEnteteEtPied(listeIdLignes);
    }
    // On supprime le regroupement auquel appartient la ligne sélectionnée
    if (!listeIdLignes.isEmpty()) {
      ManagerServiceDocumentVente.supprimerRegroupement(getIdSession(), listeIdLignes.get(0));
    }
    
    super.quitterAvecSuppression();
  }
  
  // Méthodes publiques
  
  /**
   * Indique l'indice de l'onglet que l'on souhaite activer.
   */
  public void changerOnglet(int pIndiceOnglet) {
    if (pIndiceOnglet > ONGLET_NEGOCIATION) {
      return;
    }
    
    // Activation de l'onglet souhaité
    switch (pIndiceOnglet) {
      case ONGLET_GENERAL:
        composantAyantLeFocus = FOCUS_GENERAL_TITRE;
        break;
      case ONGLET_NEGOCIATION:
        composantAyantLeFocus = FOCUS_NEGO_VENTE_REMISE;
        break;
    }
    rafraichir();
  }
  
  /**
   * Modifie le titre du regroupement.
   */
  public void modifierTexteTitre(String pSaisie) {
    if (Constantes.equals(pSaisie, texteTitre)) {
      return;
    }
    texteTitre = pSaisie;
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifie le texte du total du regroupement.
   */
  public void modifierTexteTotal(String pSaisie) {
    if (Constantes.equals(pSaisie, texteTotal)) {
      return;
    }
    texteTotal = pSaisie;
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifie les options d'édition des détails articles.
   */
  public void modifierEditionDetailArticles(boolean pSelection) {
    if (pSelection == editionDetail) {
      return;
    }
    editionDetail = pSelection;
    if (!editionDetail) {
      modifierEditionPrixArticles(pSelection);
    }
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifie les options d'édition des prix articles.
   */
  public void modifierEditionPrixArticles(boolean pSelection) {
    if (pSelection == editionPrix) {
      return;
    }
    editionPrix = pSelection;
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification de la remise.
   */
  public void modifierRemise(String pSaisie) {
    BigDecimal taux = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (taux.compareTo(negociationVenteRegroupementNegocie.getTauxRemise()) == 0) {
      return;
    }
    negociationVenteRegroupementNegocie.modifierTauxRemise(taux);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification du prix net HT ou TTC.
   */
  public void modifierPrixNet(String pSaisie) {
    BigDecimal montant = Constantes.convertirTexteEnBigDecimal(pSaisie);
    // Affichage TTC
    if (isAffichageTTC()) {
      if (montant.compareTo(negociationVenteRegroupementNegocie.getTotalNetTTC()) == 0) {
        return;
      }
      else {
        negociationVenteRegroupementNegocie.modifierTotalNetTTC(montant);
      }
    }
    // Affichage HT
    else if (montant.compareTo(negociationVenteRegroupementNegocie.getTotalNetHT()) == 0) {
      return;
    }
    else {
      negociationVenteRegroupementNegocie.modifierTotalNetHT(montant);
    }
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification de l'indice.
   */
  public void modifierIndiceDeMarge(String pSaisie) {
    BigDecimal indice = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (indice.compareTo(negociationVenteRegroupementNegocie.getIndiceMarge()) == 0) {
      return;
    }
    negociationVenteRegroupementNegocie.modifierIndiceMarge(indice);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification de la marge.
   */
  public void modifierMarge(String pSaisie) {
    BigDecimal tauxmarge = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (tauxmarge.compareTo(negociationVenteRegroupementNegocie.getTauxMarge()) == 0) {
      return;
    }
    negociationVenteRegroupementNegocie.modifierTauxMarge(tauxmarge);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modification de l'affichage ou non des marges.
   */
  public void modifierAffichageDetails(boolean pAfficherDetails) {
    if (afficherDetails == pAfficherDetails) {
      return;
    }
    afficherDetails = pAfficherDetails;
    rafraichir();
  }
  
  /**
   * Modification de l'affichage en TTC ou HT.
   */
  public void modifierAffichageTTC(boolean pAfficherTTC) {
    if (affichageTTC == pAfficherTTC) {
      return;
    }
    affichageTTC = pAfficherTTC;
    rafraichir();
  }
  
  /**
   * Lecture de la sécurité sur la visualisation des marges
   */
  public boolean isAutoriseVisualiserMarge() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_VISUALISATION_MARGES);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôle la ligne d'un document
   */
  private boolean controlerLigneDocument(LigneVente pLigneVente) {
    AlertesLigneDocument alerte =
        ManagerServiceDocumentVente.controlerLigneDocumentVente(getIdSession(), pLigneVente, modeleParent.getDateTraitement());
    if (!alerte.getMessages().trim().isEmpty()) {
      alerte.setMessages("Erreur");
      return false;
    }
    return true;
  }
  
  // -- Accesseurs
  
  public ModeleComptoir getParentModel() {
    return modeleParent;
  }
  
  public Regroupement getRegroupement() {
    return regroupement;
  }
  
  public void setRegroupement(Regroupement regroupement) {
    this.regroupement = regroupement;
  }
  
  public String getTexteTitre() {
    return texteTitre;
  }
  
  public String getTexteTotal() {
    return texteTotal;
  }
  
  public boolean isEditionDetail() {
    return editionDetail;
  }
  
  public void setEditionDetail(boolean editionDetail) {
    this.editionDetail = editionDetail;
  }
  
  public boolean isEditionPrix() {
    return editionPrix;
  }
  
  public void setEditionPrix(boolean editionPrix) {
    this.editionPrix = editionPrix;
  }
  
  public NegociationVenteGlobale getNegociationVenteRegroupementOrigine() {
    return negociationVenteRegroupementOrigine;
  }
  
  public NegociationVenteGlobale getNegociationVenteRegroupementNegocie() {
    return negociationVenteRegroupementNegocie;
  }
  
  public boolean isAffichageTTC() {
    return affichageTTC;
  }
  
  public boolean isAfficherDetails() {
    return afficherDetails;
  }
  
  /**
   * Composant ayant le focus.
   */
  public int getComposantAyantLeFocus() {
    return composantAyantLeFocus;
  }
  
  /**
   * Retourner le document de ventes.
   * @return
   */
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
  /**
   * Retourne le mode en cours Négoce ou Classique.
   * @return
   */
  public boolean isModeNegoce() {
    return modeleParent.isModeNegoce();
  }
}
