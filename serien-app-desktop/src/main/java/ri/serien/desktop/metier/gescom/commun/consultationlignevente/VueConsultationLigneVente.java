/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.consultationlignevente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.affichagelienligne.SNLienLigne;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue qui permet de consulter le détail d'une ligne de vente.
 */
public class VueConsultationLigneVente extends AbstractVueDialogue<ModeleConsultationLigneVente> {
  /**
   * Constructeur.
   */
  public VueConsultationLigneVente(ModeleConsultationLigneVente pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snLignesLiees.setBackground(Color.WHITE);
  }
  
  @Override
  public void rafraichir() {
    // Informations ligne
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirQuantiteUCV();
    rafraichirQuantiteUV();
    // Liens
    rafraichirLiens();
    // Prix
    rafraichirPrixBase();
    rafraichirPrixNet();
    rafraichirOriginePrix();
    rafraichirRemise();
    rafraichirIndiceDeMarge();
    rafraichirMarge();
    rafraichirPrixRevient();
    rafraichirMontant();
    // Prix d'achat
    rafraichirDateApplicationAchat();
    rafraichirUniteCommandeAchat();
    rafraichirPrixAchatBrutHT();
    rafraichirLibelleUniteAchat();
    rafraichirRemiseFournisseur1();
    rafraichirRemiseFournisseur2();
    rafraichirRemiseFournisseur3();
    rafraichirRemiseFournisseur4();
    rafraichirFraisExploitation();
    rafraichirValeurConditionnement();
    rafraichirValeurPort();
    rafraichirPoids();
    rafraichirPourcentagePort();
    rafraichirPrixRevientStandard();
    
    // Si l'utilisateur n'a pas le droit de voir les marges :
    pnlTarifNegocieMarge.setVisible(getModele().isAutoriseVisualiserMarge());
    pnlNegociationAchat.setVisible(getModele().isAutoriseVisualiserMarge());
  }
  
  // Méthodes privées
  
  private void rafraichirCodeArticle() {
    IdArticle idArticle = getModele().getIdArticle();
    if (idArticle != null && idArticle.getCodeArticle() != null) {
      tfCodeArticle.setText(idArticle.getCodeArticle());
    }
    else {
      tfCodeArticle.setText("");
    }
  }
  
  private void rafraichirLibelleArticle() {
    if (getModele().getLibelle() != null) {
      tfLibelleArticle.setText(getModele().getLibelle());
    }
    else {
      tfLibelleArticle.setText("");
    }
  }
  
  private void rafraichirQuantiteUCV() {
    String texteQuantiteUCV = "";
    
    // Ajouter la quantité en UCV
    texteQuantiteUCV = Constantes.formater(getModele().getQuantiteUCV(), false);
    
    // Ajouter l'unité
    IdUnite idUnite = getModele().getIdUCV();
    if (idUnite != null && idUnite.getCode() != null) {
      texteQuantiteUCV += " " + idUnite.getCode();
    }
    
    tfQuantiteUCV.setText(texteQuantiteUCV);
  }
  
  private void rafraichirQuantiteUV() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getNombreUVParUCV() != null && !ligneVente.isUniteVenteIdentique()) {
      // Arrondir la quantité en UV (inutile en théorie mais il faut corriger le pb de la précision stockée à la ligne)
      BigDecimal quantiteUV = ligneVente.getQuantiteUV();
      
      // Ajouter la quantité en UV
      String texteQuantiteUV = Constantes.formater(quantiteUV, false);
      
      // Ajouter l'unité de ventes (UV)
      if (getModele().getIdUniteV() != null) {
        texteQuantiteUV += " " + getModele().getIdUniteV().getCode();
      }
      
      // Récupérer le conditionnement
      BigDecimal nombreUVParUCV = BigDecimal.ONE;
      if (ligneVente.getNombreUVParUCV().compareTo(BigDecimal.ZERO) > 0) {
        nombreUVParUCV = ligneVente.getNombreUVParUCV();
      }
      
      // Générer le texte sur le conditionnement
      texteQuantiteUV += " (conditionnement de " + Constantes.formater(nombreUVParUCV, false) + " " + getModele().getIdUniteV() + " / "
          + getModele().getIdUCV() + ")";
      
      tfQuantiteUV.setText(texteQuantiteUV);
      tfQuantiteUV.setVisible(true);
    }
    else {
      tfQuantiteUV.setText("");
      tfQuantiteUV.setVisible(false);
    }
  }
  
  private void rafraichirLiens() {
    LienLigne lienOrigine = getModele().getLienOrigine();
    if (lienOrigine != null) {
      snLignesLiees.setVisible(true);
      snLignesLiees.setSession(getModele().getSession());
      snLignesLiees.setLienOrigine(lienOrigine);
      snLignesLiees.charger(true);
    }
    // Si aucun lien on cache le panel
    else {
      snLignesLiees.setVisible(false);
    }
  }
  
  private void rafraichirPrixBase() {
    LigneVente ligneVente = getModele().getLigneVente();
    // Mode négoce
    if (getModele().isModeNegoce()) {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && getModele().isAffichageTTC()) {
        lbPrixBase.setText("Prix public TTC");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !getModele().isAffichageTTC()) {
        lbPrixBase.setText("Prix public HT");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBase.setText("Prix public");
        tfPrixBase.setText("");
      }
    }
    // Mode classique
    else {
      if (ligneVente != null && ligneVente.getPrixBaseTTC() != null && getModele().isAffichageTTC()) {
        lbPrixBase.setText("Prix base TTC");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseTTC(), true));
      }
      else if (ligneVente != null && ligneVente.getPrixBaseHT() != null && !getModele().isAffichageTTC()) {
        lbPrixBase.setText("Prix base HT");
        tfPrixBase.setText(Constantes.formater(ligneVente.getPrixBaseHT(), true));
      }
      else {
        lbPrixBase.setText("Prix base");
        tfPrixBase.setText("0");
      }
    }
    
    // La police
    lbPrixBase.setFont(new Font("sansserif", Font.BOLD, 13));
  }
  
  private void rafraichirPrixNet() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getPrixNetTTC() != null && getModele().isAffichageTTC()) {
      lbPrixNet.setText("Prix net TTC");
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixNetHT() != null && !getModele().isAffichageTTC()) {
      lbPrixNet.setText("Prix net HT");
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
      
    }
    else {
      lbPrixNet.setText("Prix net");
      tfPrixNet.setText("");
    }
    lbPrixNet.setFont(new Font("sansserif", Font.BOLD, 13));
  }
  
  private void rafraichirOriginePrix() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getTexteOriginePrixVente() != null) {
      tfOrigineTarifLigneArticle.setText(ligneVente.getTexteOriginePrixVente());
    }
    else {
      tfOrigineTarifLigneArticle.setText("");
    }
  }
  
  private void rafraichirRemise() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getTauxRemise1() != null) {
      tfRemise.setText(Constantes.formater(ligneVente.getTauxRemise1(), true) + "%");
    }
    else {
      tfRemise.setText("");
    }
  }
  
  private void rafraichirIndiceDeMarge() {
    if (getModele().getIndiceDeMarge() != null) {
      tfIndice.setText(Constantes.formater(getModele().getIndiceDeMarge(), true));
    }
    else {
      tfIndice.setText("");
    }
  }
  
  private void rafraichirMarge() {
    if (getModele().getTauxDeMarque() != null) {
      tfMarge.setText(Constantes.formater(getModele().getTauxDeMarque(), true) + "%");
    }
    else {
      tfMarge.setText("");
    }
  }
  
  private void rafraichirPrixRevient() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getPrixDeRevientLigneTTC() != null && getModele().isAffichageTTC()) {
      lbPrixRevient.setText("Prix de revient TTC");
      tfPrixRevient.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getPrixDeRevientLigneHT() != null && !getModele().isAffichageTTC()) {
      lbPrixRevient.setText("Prix de revient");
      tfPrixRevient.setText(Constantes.formater(ligneVente.getPrixDeRevientLigneHT(), true));
      
    }
    else {
      lbPrixRevient.setText("Prix de revient");
      tfPrixRevient.setText("");
    }
    lbPrixRevient.setFont(new Font("sansserif", Font.BOLD, 13));
  }
  
  private void rafraichirMontant() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getMontantTTC() != null && getModele().isAffichageTTC()) {
      lbMontantTotal.setText("Montant TTC de la ligne");
      tfMontantTotal.setText(Constantes.formater(ligneVente.getMontantTTC(), true));
    }
    else if (ligneVente != null && ligneVente.getMontantHT() != null && !getModele().isAffichageTTC()) {
      lbMontantTotal.setText("Montant HT de la ligne");
      tfMontantTotal.setText(Constantes.formater(ligneVente.getMontantHT(), true));
      
    }
    else {
      lbMontantTotal.setText("Montant de la ligne");
      tfMontantTotal.setText("");
    }
    lbMontantTotal.setFont(new Font("sansserif", Font.BOLD, 13));
  }
  
  private void rafraichirDateApplicationAchat() {
    ConditionAchat conditionAchat = getModele().getConditionAchatLigne();
    if (conditionAchat != null && conditionAchat.getDateApplication() != null) {
      calDateApplicationTarifAchat.setText(Constantes.convertirDateEnTexte(conditionAchat.getDateApplication()));
    }
  }
  
  private void rafraichirUniteCommandeAchat() {
    LigneVente ligne = getModele().getLigneVente();
    if (ligne != null && ligne.getIdUniteConditionnementVente() != null) {
      lbUnitePrixAchatBrutHT.setText(ligne.getIdUniteConditionnementVente().getCode());
    }
    else {
      lbUnitePrixAchatBrutHT.setText("");
    }
  }
  
  private void rafraichirPrixAchatBrutHT() {
    ConditionAchat conditionAchat = getModele().getConditionAchatLigne();
    
    if (conditionAchat != null && conditionAchat.getPrixAchatBrutHT() != null) {
      tfPrixAchatBrutHT.setText(Constantes.formater(conditionAchat.getPrixAchatBrutHT(), true));
      if (conditionAchat.getIdUniteAchat() != null) {
        lbUnitePrixAchatBrutHT.setText(conditionAchat.getIdUniteAchat().getCode());
      }
      else {
        lbUnitePrixAchatBrutHT.setText("");
      }
    }
    else {
      tfPrixAchatBrutHT.setText("");
      lbUnitePrixAchatBrutHT.setText("");
    }
    
  }
  
  private void rafraichirLibelleUniteAchat() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    if (negociation != null && negociation.getIdUniteAchat() != null) {
      lbUnitePrixAchatBrutHT.setText("par " + negociation.getIdUniteAchat().getCode());
      lbUnitePrixRevientStandardHT.setText("par " + negociation.getIdUniteAchat().getCode());
    }
    else {
      lbUnitePrixAchatBrutHT.setText("");
      lbUnitePrixRevientStandardHT.setText("");
    }
  }
  
  private void rafraichirRemiseFournisseur1() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal remise = null;
    
    if (negociation != null) {
      remise = negociation.getPourcentageRemise1();
    }
    if (remise != null) {
      tfRemiseFournisseur1.setText(Constantes.formater(remise, true) + " %");
    }
    else {
      tfRemiseFournisseur1.setText("");
    }
    
  }
  
  private void rafraichirRemiseFournisseur2() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal remise = null;
    
    if (negociation != null) {
      remise = negociation.getPourcentageRemise2();
    }
    if (remise != null && remise.compareTo(BigDecimal.ZERO) > 0) {
      tfRemiseFournisseur2.setText(Constantes.formater(remise, true) + " %");
    }
    else {
      tfRemiseFournisseur2.setText("");
    }
  }
  
  private void rafraichirRemiseFournisseur3() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal remise = null;
    
    if (negociation != null) {
      remise = negociation.getPourcentageRemise3();
    }
    if (remise != null && remise.compareTo(BigDecimal.ZERO) > 0) {
      tfRemiseFournisseur3.setText(Constantes.formater(remise, true) + " %");
    }
    else {
      tfRemiseFournisseur3.setText("");
    }
  }
  
  private void rafraichirRemiseFournisseur4() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal remise = null;
    
    if (negociation != null) {
      remise = negociation.getPourcentageRemise4();
    }
    if (remise != null && remise.compareTo(BigDecimal.ZERO) > 0) {
      tfRemiseFournisseur4.setText(Constantes.formater(remise, true) + " %");
    }
    else {
      tfRemiseFournisseur4.setText("");
    }
  }
  
  private void rafraichirFraisExploitation() {
    if (getModele().isAutoriseFraisExploitation()) {
      tfFraisExploitation.setVisible(true);
      ConditionAchat negociation = getModele().getConditionAchatLigne();
      BigDecimal valeur = null;
      
      if (negociation != null) {
        valeur = negociation.getFraisExploitation();
      }
      if (valeur != null) {
        tfFraisExploitation.setText(Constantes.formater(valeur, true) + " %");
      }
      else {
        tfFraisExploitation.setText("");
      }
    }
    else {
      tfFraisExploitation.setVisible(false);
    }
    lbFraisExploitation.setVisible(tfFraisExploitation.isVisible());
  }
  
  private void rafraichirValeurConditionnement() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal valeur = null;
    
    if (negociation != null) {
      valeur = negociation.getMontantConditionnement();
    }
    if (valeur != null) {
      tfMontantConditionnement.setText(Constantes.formater(valeur, true));
    }
    else {
      tfMontantConditionnement.setText("");
    }
  }
  
  private void rafraichirValeurPort() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal valeur = null;
    
    if (negociation != null) {
      valeur = negociation.getMontantAuPoidsPortHT();
    }
    if (valeur != null) {
      tfValeurPort.setText(Constantes.formater(valeur, true));
    }
    else {
      tfValeurPort.setText("");
    }
  }
  
  private void rafraichirPoids() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal poids = null;
    
    if (negociation != null) {
      poids = negociation.getPoidsPort();
    }
    if (poids != null) {
      tfPoidsPort.setText(Constantes.formater(poids, false));
    }
    else {
      tfPoidsPort.setText("");
    }
  }
  
  private void rafraichirPourcentagePort() {
    ConditionAchat negociation = getModele().getConditionAchatLigne();
    BigDecimal coefficient = null;
    
    if (negociation != null) {
      coefficient = negociation.getPourcentagePort();
    }
    if (coefficient != null) {
      tfPourcentagePort.setText(Constantes.formater(coefficient, true) + " %");
      
    }
    else {
      tfPourcentagePort.setText("");
    }
  }
  
  private void rafraichirPrixRevientStandard() {
    ConditionAchat conditionAchat = getModele().getConditionAchatLigne();
    LigneVente ligne = getModele().getLigneVente();
    BigDecimal prixRevientStandardHT = null;
    
    if (conditionAchat != null) {
      prixRevientStandardHT = conditionAchat.getPrixRevientStandardHT();
    }
    if (prixRevientStandardHT != null) {
      tfPrixRevientStandardHT.setText(Constantes.formater(prixRevientStandardHT, true));
    }
    else {
      tfPrixRevientStandardHT.setText("");
    }
    // ou si la ligne correspond à une dérogation
    if (ligne.isDerogation()) {
      tfPrixRevientStandardHT.setEnabled(false);
    }
  }
  
  // -- Méthodes interractives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelContenu();
    pnlCommentaire = new JPanel();
    pnlEntete = new JPanel();
    lbCodeArticle = new JLabel();
    tfCodeArticle = new JLabel();
    lbLibelleArticle = new JLabel();
    tfLibelleArticle = new JLabel();
    lbQuantiteUCV = new JLabel();
    tfQuantiteUCV = new JLabel();
    lbQuantiteUV = new JLabel();
    tfQuantiteUV = new JLabel();
    tabbedPane1 = new JTabbedPane();
    snLignesLiees = new SNLienLigne();
    sNPanelContenu1 = new SNPanelContenu();
    pnlTarif = new SNPanel();
    pnlTarifNegocie = new SNPanelTitre();
    lbPrixBase = new SNLabelChamp();
    tfPrixBase = new SNLabelChamp();
    lbOrigine = new SNLabelChamp();
    tfOrigineTarifLigneArticle = new SNLabelChamp();
    lbRemise = new SNLabelChamp();
    tfRemise = new SNLabelChamp();
    lbPrixNet = new SNLabelChamp();
    lbMontantTotal = new SNLabelChamp();
    tfPrixNet = new SNLabelChamp();
    tfMontantTotal = new SNLabelChamp();
    pnlTarifNegocieMarge = new SNPanelTitre();
    lbIndiceDeMarge = new SNLabelChamp();
    lbMarge = new SNLabelChamp();
    lbPrixRevient = new SNLabelChamp();
    tfIndice = new SNLabelChamp();
    tfMarge = new SNLabelChamp();
    tfPrixRevient = new SNLabelChamp();
    pnlNegociationAchat = new SNPanelTitre();
    lbRemisesFournisseur = new SNLabelChamp();
    lbPrixAchatBrutHT = new SNLabelChamp();
    pnlPrixAchatBrutHT = new SNPanel();
    tfPrixAchatBrutHT = new SNLabelChamp();
    lbUnitePrixAchatBrutHT = new SNLabelChamp();
    pnlRemisesFournisseur = new SNPanel();
    tfRemiseFournisseur1 = new SNLabelChamp();
    tfRemiseFournisseur2 = new SNLabelChamp();
    tfRemiseFournisseur3 = new SNLabelChamp();
    tfRemiseFournisseur4 = new SNLabelChamp();
    lbMontantConditionnement = new SNLabelChamp();
    tfMontantConditionnement = new SNLabelChamp();
    rbMontantPort = new SNLabelChamp();
    pnlMontantPort = new SNPanel();
    tfValeurPort = new SNLabelChamp();
    lbPortPoids = new SNLabelChamp();
    tfPoidsPort = new SNLabelUnite();
    rbPourcentagePort = new SNLabelChamp();
    pnlPourcentagePort = new SNPanel();
    tfPourcentagePort = new SNLabelChamp();
    lbFraisExploitation = new SNLabelChamp();
    pnlFraisExploitation = new SNPanel();
    tfFraisExploitation = new SNLabelChamp();
    lbPrixRevientStandardHT = new SNLabelChamp();
    pnlPrixRevientStandardHT = new SNPanel();
    tfPrixRevientStandardHT = new SNLabelChamp();
    lbUnitePrixRevientStandardHT = new SNLabelChamp();
    lbDateApplicationTarifAchat = new SNLabelChamp();
    calDateApplicationTarifAchat = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail d'une ligne article");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(980, 640));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(Color.white);
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCommentaire ========
      {
        pnlCommentaire.setOpaque(false);
        pnlCommentaire.setName("pnlCommentaire");
        pnlCommentaire.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlEntete ========
        {
          pnlEntete.setOpaque(false);
          pnlEntete.setName("pnlEntete");
          pnlEntete.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEntete.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEntete.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlEntete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCodeArticle ----
          lbCodeArticle.setText("Code article : ");
          lbCodeArticle.setFont(new Font("sansserif", Font.BOLD, 13));
          lbCodeArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbCodeArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodeArticle.setPreferredSize(new Dimension(150, 20));
          lbCodeArticle.setMinimumSize(new Dimension(150, 20));
          lbCodeArticle.setMaximumSize(new Dimension(150, 20));
          lbCodeArticle.setName("lbCodeArticle");
          pnlEntete.add(lbCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfCodeArticle ----
          tfCodeArticle.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfCodeArticle.setBorder(null);
          tfCodeArticle.setHorizontalTextPosition(SwingConstants.LEADING);
          tfCodeArticle.setMaximumSize(new Dimension(400, 20));
          tfCodeArticle.setMinimumSize(new Dimension(400, 20));
          tfCodeArticle.setPreferredSize(new Dimension(400, 20));
          tfCodeArticle.setName("tfCodeArticle");
          pnlEntete.add(tfCodeArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLibelleArticle ----
          lbLibelleArticle.setText("Libell\u00e9 article :");
          lbLibelleArticle.setFont(new Font("sansserif", Font.BOLD, 13));
          lbLibelleArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLibelleArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLibelleArticle.setPreferredSize(new Dimension(150, 20));
          lbLibelleArticle.setMinimumSize(new Dimension(150, 20));
          lbLibelleArticle.setMaximumSize(new Dimension(150, 20));
          lbLibelleArticle.setName("lbLibelleArticle");
          pnlEntete.add(lbLibelleArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- tfLibelleArticle ----
          tfLibelleArticle.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfLibelleArticle.setBackground(Color.white);
          tfLibelleArticle.setBorder(null);
          tfLibelleArticle.setHorizontalTextPosition(SwingConstants.LEADING);
          tfLibelleArticle.setPreferredSize(new Dimension(800, 20));
          tfLibelleArticle.setMaximumSize(new Dimension(840, 20));
          tfLibelleArticle.setMinimumSize(new Dimension(80, 20));
          tfLibelleArticle.setName("tfLibelleArticle");
          pnlEntete.add(tfLibelleArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbQuantiteUCV ----
          lbQuantiteUCV.setText("Quantit\u00e9 en UCV :");
          lbQuantiteUCV.setFont(new Font("sansserif", Font.BOLD, 13));
          lbQuantiteUCV.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbQuantiteUCV.setHorizontalAlignment(SwingConstants.RIGHT);
          lbQuantiteUCV.setMinimumSize(new Dimension(150, 20));
          lbQuantiteUCV.setPreferredSize(new Dimension(150, 20));
          lbQuantiteUCV.setMaximumSize(new Dimension(150, 20));
          lbQuantiteUCV.setName("lbQuantiteUCV");
          pnlEntete.add(lbQuantiteUCV, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 10), 0, 0));
          
          // ---- tfQuantiteUCV ----
          tfQuantiteUCV.setHorizontalAlignment(SwingConstants.LEFT);
          tfQuantiteUCV.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfQuantiteUCV.setBorder(null);
          tfQuantiteUCV.setHorizontalTextPosition(SwingConstants.LEADING);
          tfQuantiteUCV.setMaximumSize(new Dimension(150, 20));
          tfQuantiteUCV.setMinimumSize(new Dimension(150, 20));
          tfQuantiteUCV.setPreferredSize(new Dimension(150, 20));
          tfQuantiteUCV.setName("tfQuantiteUCV");
          pnlEntete.add(tfQuantiteUCV, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbQuantiteUV ----
          lbQuantiteUV.setText("Quantit\u00e9 en UV :");
          lbQuantiteUV.setFont(new Font("sansserif", Font.BOLD, 13));
          lbQuantiteUV.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbQuantiteUV.setHorizontalAlignment(SwingConstants.RIGHT);
          lbQuantiteUV.setMinimumSize(new Dimension(150, 20));
          lbQuantiteUV.setPreferredSize(new Dimension(150, 20));
          lbQuantiteUV.setMaximumSize(new Dimension(150, 20));
          lbQuantiteUV.setName("lbQuantiteUV");
          pnlEntete.add(lbQuantiteUV, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 10), 0, 0));
          
          // ---- tfQuantiteUV ----
          tfQuantiteUV.setHorizontalAlignment(SwingConstants.LEFT);
          tfQuantiteUV.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfQuantiteUV.setBorder(null);
          tfQuantiteUV.setHorizontalTextPosition(SwingConstants.LEADING);
          tfQuantiteUV.setMaximumSize(new Dimension(400, 20));
          tfQuantiteUV.setMinimumSize(new Dimension(400, 20));
          tfQuantiteUV.setPreferredSize(new Dimension(400, 20));
          tfQuantiteUV.setInheritsPopupMenu(false);
          tfQuantiteUV.setName("tfQuantiteUV");
          pnlEntete.add(tfQuantiteUV, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCommentaire.add(pnlEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== tabbedPane1 ========
        {
          tabbedPane1.setFont(new Font("sansserif", Font.PLAIN, 14));
          tabbedPane1.setName("tabbedPane1");
          
          // ---- snLignesLiees ----
          snLignesLiees.setBackground(Color.white);
          snLignesLiees.setOpaque(true);
          snLignesLiees.setPreferredSize(new Dimension(1060, 280));
          snLignesLiees.setName("snLignesLiees");
          tabbedPane1.addTab("Liens", snLignesLiees);
          
          // ======== sNPanelContenu1 ========
          {
            sNPanelContenu1.setName("sNPanelContenu1");
            sNPanelContenu1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) sNPanelContenu1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ======== pnlTarif ========
            {
              pnlTarif.setName("pnlTarif");
              pnlTarif.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlTarifNegocie ========
              {
                pnlTarifNegocie.setTitre("Prix de vente");
                pnlTarifNegocie.setName("pnlTarifNegocie");
                pnlTarifNegocie.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlTarifNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlTarifNegocie.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbPrixBase ----
                lbPrixBase.setText("Prix public");
                lbPrixBase.setHorizontalAlignment(SwingConstants.CENTER);
                lbPrixBase.setMaximumSize(new Dimension(100, 30));
                lbPrixBase.setMinimumSize(new Dimension(100, 30));
                lbPrixBase.setPreferredSize(new Dimension(100, 30));
                lbPrixBase.setFont(new Font("sansserif", Font.BOLD, 13));
                lbPrixBase.setName("lbPrixBase");
                pnlTarifNegocie.add(lbPrixBase, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfPrixBase ----
                tfPrixBase.setHorizontalAlignment(SwingConstants.CENTER);
                tfPrixBase.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPrixBase.setName("tfPrixBase");
                pnlTarifNegocie.add(tfPrixBase, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbOrigine ----
                lbOrigine.setText("Origine");
                lbOrigine.setHorizontalAlignment(SwingConstants.CENTER);
                lbOrigine.setMaximumSize(new Dimension(100, 30));
                lbOrigine.setMinimumSize(new Dimension(100, 30));
                lbOrigine.setPreferredSize(new Dimension(100, 30));
                lbOrigine.setFont(new Font("sansserif", Font.BOLD, 13));
                lbOrigine.setName("lbOrigine");
                pnlTarifNegocie.add(lbOrigine, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfOrigineTarifLigneArticle ----
                tfOrigineTarifLigneArticle.setMinimumSize(new Dimension(150, 30));
                tfOrigineTarifLigneArticle.setPreferredSize(new Dimension(150, 30));
                tfOrigineTarifLigneArticle.setHorizontalAlignment(SwingConstants.CENTER);
                tfOrigineTarifLigneArticle.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfOrigineTarifLigneArticle.setName("tfOrigineTarifLigneArticle");
                pnlTarifNegocie.add(tfOrigineTarifLigneArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbRemise ----
                lbRemise.setText("Remise");
                lbRemise.setHorizontalAlignment(SwingConstants.CENTER);
                lbRemise.setMaximumSize(new Dimension(100, 30));
                lbRemise.setMinimumSize(new Dimension(100, 30));
                lbRemise.setPreferredSize(new Dimension(100, 30));
                lbRemise.setFont(new Font("sansserif", Font.BOLD, 13));
                lbRemise.setName("lbRemise");
                pnlTarifNegocie.add(lbRemise, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- tfRemise ----
                tfRemise.setHorizontalAlignment(SwingConstants.CENTER);
                tfRemise.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfRemise.setName("tfRemise");
                pnlTarifNegocie.add(tfRemise, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbPrixNet ----
                lbPrixNet.setText("Prix net");
                lbPrixNet.setHorizontalAlignment(SwingConstants.CENTER);
                lbPrixNet.setMaximumSize(new Dimension(100, 30));
                lbPrixNet.setMinimumSize(new Dimension(100, 30));
                lbPrixNet.setPreferredSize(new Dimension(100, 30));
                lbPrixNet.setFont(new Font("sansserif", Font.BOLD, 13));
                lbPrixNet.setName("lbPrixNet");
                pnlTarifNegocie.add(lbPrixNet, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbMontantTotal ----
                lbMontantTotal.setText("Montant HT de la ligne");
                lbMontantTotal.setFont(new Font("sansserif", Font.BOLD, 13));
                lbMontantTotal.setHorizontalAlignment(SwingConstants.CENTER);
                lbMontantTotal.setName("lbMontantTotal");
                pnlTarifNegocie.add(lbMontantTotal, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfPrixNet ----
                tfPrixNet.setHorizontalAlignment(SwingConstants.CENTER);
                tfPrixNet.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPrixNet.setName("tfPrixNet");
                pnlTarifNegocie.add(tfPrixNet, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfMontantTotal ----
                tfMontantTotal.setHorizontalAlignment(SwingConstants.CENTER);
                tfMontantTotal.setMaximumSize(new Dimension(100, 30));
                tfMontantTotal.setMinimumSize(new Dimension(100, 30));
                tfMontantTotal.setPreferredSize(new Dimension(100, 30));
                tfMontantTotal.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfMontantTotal.setName("tfMontantTotal");
                pnlTarifNegocie.add(tfMontantTotal, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlTarif.add(pnlTarifNegocie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== pnlTarifNegocieMarge ========
              {
                pnlTarifNegocieMarge.setTitre("Marge");
                pnlTarifNegocieMarge.setName("pnlTarifNegocieMarge");
                pnlTarifNegocieMarge.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlTarifNegocieMarge.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbIndiceDeMarge ----
                lbIndiceDeMarge.setText("Indice");
                lbIndiceDeMarge.setMaximumSize(new Dimension(100, 30));
                lbIndiceDeMarge.setMinimumSize(new Dimension(100, 30));
                lbIndiceDeMarge.setPreferredSize(new Dimension(100, 30));
                lbIndiceDeMarge.setHorizontalAlignment(SwingConstants.CENTER);
                lbIndiceDeMarge.setFont(new Font("sansserif", Font.BOLD, 13));
                lbIndiceDeMarge.setName("lbIndiceDeMarge");
                pnlTarifNegocieMarge.add(lbIndiceDeMarge, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbMarge ----
                lbMarge.setText("Marge");
                lbMarge.setMaximumSize(new Dimension(100, 30));
                lbMarge.setMinimumSize(new Dimension(100, 30));
                lbMarge.setPreferredSize(new Dimension(100, 30));
                lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
                lbMarge.setFont(new Font("sansserif", Font.BOLD, 13));
                lbMarge.setName("lbMarge");
                pnlTarifNegocieMarge.add(lbMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbPrixRevient ----
                lbPrixRevient.setText("<html>Prix de revient<br>standard</html>");
                lbPrixRevient.setMaximumSize(new Dimension(100, 30));
                lbPrixRevient.setMinimumSize(new Dimension(100, 30));
                lbPrixRevient.setPreferredSize(new Dimension(100, 30));
                lbPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
                lbPrixRevient.setFont(new Font("sansserif", Font.BOLD, 13));
                lbPrixRevient.setName("lbPrixRevient");
                pnlTarifNegocieMarge.add(lbPrixRevient, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- tfIndice ----
                tfIndice.setHorizontalAlignment(SwingConstants.CENTER);
                tfIndice.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfIndice.setName("tfIndice");
                pnlTarifNegocieMarge.add(tfIndice, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfMarge ----
                tfMarge.setHorizontalAlignment(SwingConstants.CENTER);
                tfMarge.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfMarge.setName("tfMarge");
                pnlTarifNegocieMarge.add(tfMarge, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPrixRevient ----
                tfPrixRevient.setHorizontalAlignment(SwingConstants.CENTER);
                tfPrixRevient.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPrixRevient.setName("tfPrixRevient");
                pnlTarifNegocieMarge.add(tfPrixRevient, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlTarif.add(pnlTarifNegocieMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            sNPanelContenu1.add(pnlTarif, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlNegociationAchat ========
            {
              pnlNegociationAchat.setTitre("Prix d'achat");
              pnlNegociationAchat.setName("pnlNegociationAchat");
              pnlNegociationAchat.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlNegociationAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlNegociationAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              
              // ---- lbRemisesFournisseur ----
              lbRemisesFournisseur.setText("Remises fournisseur :");
              lbRemisesFournisseur.setMaximumSize(new Dimension(200, 30));
              lbRemisesFournisseur.setMinimumSize(new Dimension(200, 30));
              lbRemisesFournisseur.setPreferredSize(new Dimension(200, 30));
              lbRemisesFournisseur.setFont(new Font("sansserif", Font.BOLD, 13));
              lbRemisesFournisseur.setName("lbRemisesFournisseur");
              pnlNegociationAchat.add(lbRemisesFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbPrixAchatBrutHT ----
              lbPrixAchatBrutHT.setText("Prix d'achat brut HT :");
              lbPrixAchatBrutHT.setMaximumSize(new Dimension(200, 30));
              lbPrixAchatBrutHT.setMinimumSize(new Dimension(200, 30));
              lbPrixAchatBrutHT.setPreferredSize(new Dimension(200, 30));
              lbPrixAchatBrutHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
              lbPrixAchatBrutHT.setFont(new Font("sansserif", Font.BOLD, 13));
              lbPrixAchatBrutHT.setName("lbPrixAchatBrutHT");
              pnlNegociationAchat.add(lbPrixAchatBrutHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlPrixAchatBrutHT ========
              {
                pnlPrixAchatBrutHT.setOpaque(false);
                pnlPrixAchatBrutHT.setName("pnlPrixAchatBrutHT");
                pnlPrixAchatBrutHT.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPrixAchatBrutHT ----
                tfPrixAchatBrutHT.setHorizontalAlignment(SwingConstants.TRAILING);
                tfPrixAchatBrutHT.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPrixAchatBrutHT.setMaximumSize(new Dimension(100, 30));
                tfPrixAchatBrutHT.setMinimumSize(new Dimension(100, 30));
                tfPrixAchatBrutHT.setPreferredSize(new Dimension(100, 30));
                tfPrixAchatBrutHT.setName("tfPrixAchatBrutHT");
                pnlPrixAchatBrutHT.add(tfPrixAchatBrutHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbUnitePrixAchatBrutHT ----
                lbUnitePrixAchatBrutHT.setText("UA");
                lbUnitePrixAchatBrutHT.setHorizontalAlignment(SwingConstants.LEADING);
                lbUnitePrixAchatBrutHT.setMinimumSize(new Dimension(50, 30));
                lbUnitePrixAchatBrutHT.setPreferredSize(new Dimension(50, 30));
                lbUnitePrixAchatBrutHT.setFont(new Font("sansserif", Font.BOLD, 13));
                lbUnitePrixAchatBrutHT.setName("lbUnitePrixAchatBrutHT");
                pnlPrixAchatBrutHT.add(lbUnitePrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlNegociationAchat.add(pnlPrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlRemisesFournisseur ========
              {
                pnlRemisesFournisseur.setOpaque(false);
                pnlRemisesFournisseur.setName("pnlRemisesFournisseur");
                pnlRemisesFournisseur.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfRemiseFournisseur1 ----
                tfRemiseFournisseur1.setHorizontalAlignment(SwingConstants.TRAILING);
                tfRemiseFournisseur1.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfRemiseFournisseur1.setMaximumSize(new Dimension(100, 30));
                tfRemiseFournisseur1.setMinimumSize(new Dimension(100, 30));
                tfRemiseFournisseur1.setPreferredSize(new Dimension(100, 30));
                tfRemiseFournisseur1.setName("tfRemiseFournisseur1");
                pnlRemisesFournisseur.add(tfRemiseFournisseur1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfRemiseFournisseur2 ----
                tfRemiseFournisseur2.setHorizontalAlignment(SwingConstants.TRAILING);
                tfRemiseFournisseur2.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfRemiseFournisseur2.setMaximumSize(new Dimension(100, 30));
                tfRemiseFournisseur2.setMinimumSize(new Dimension(100, 30));
                tfRemiseFournisseur2.setPreferredSize(new Dimension(100, 30));
                tfRemiseFournisseur2.setName("tfRemiseFournisseur2");
                pnlRemisesFournisseur.add(tfRemiseFournisseur2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfRemiseFournisseur3 ----
                tfRemiseFournisseur3.setHorizontalAlignment(SwingConstants.TRAILING);
                tfRemiseFournisseur3.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfRemiseFournisseur3.setMaximumSize(new Dimension(100, 30));
                tfRemiseFournisseur3.setMinimumSize(new Dimension(100, 30));
                tfRemiseFournisseur3.setPreferredSize(new Dimension(100, 30));
                tfRemiseFournisseur3.setName("tfRemiseFournisseur3");
                pnlRemisesFournisseur.add(tfRemiseFournisseur3, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfRemiseFournisseur4 ----
                tfRemiseFournisseur4.setHorizontalAlignment(SwingConstants.TRAILING);
                tfRemiseFournisseur4.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfRemiseFournisseur4.setMaximumSize(new Dimension(100, 30));
                tfRemiseFournisseur4.setMinimumSize(new Dimension(100, 30));
                tfRemiseFournisseur4.setPreferredSize(new Dimension(100, 30));
                tfRemiseFournisseur4.setName("tfRemiseFournisseur4");
                pnlRemisesFournisseur.add(tfRemiseFournisseur4, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlNegociationAchat.add(pnlRemisesFournisseur, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMontantConditionnement ----
              lbMontantConditionnement.setText("Montant conditionnement :");
              lbMontantConditionnement.setMaximumSize(new Dimension(200, 30));
              lbMontantConditionnement.setMinimumSize(new Dimension(200, 30));
              lbMontantConditionnement.setPreferredSize(new Dimension(200, 30));
              lbMontantConditionnement.setFont(new Font("sansserif", Font.BOLD, 13));
              lbMontantConditionnement.setName("lbMontantConditionnement");
              pnlNegociationAchat.add(lbMontantConditionnement, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfMontantConditionnement ----
              tfMontantConditionnement.setHorizontalAlignment(SwingConstants.TRAILING);
              tfMontantConditionnement.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfMontantConditionnement.setMaximumSize(new Dimension(100, 30));
              tfMontantConditionnement.setMinimumSize(new Dimension(100, 30));
              tfMontantConditionnement.setPreferredSize(new Dimension(100, 30));
              tfMontantConditionnement.setName("tfMontantConditionnement");
              pnlNegociationAchat.add(tfMontantConditionnement, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- rbMontantPort ----
              rbMontantPort.setText("  Montant port HT :");
              rbMontantPort.setFont(new Font("sansserif", Font.BOLD, 13));
              rbMontantPort.setHorizontalAlignment(SwingConstants.RIGHT);
              rbMontantPort.setName("rbMontantPort");
              pnlNegociationAchat.add(rbMontantPort, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlMontantPort ========
              {
                pnlMontantPort.setOpaque(false);
                pnlMontantPort.setName("pnlMontantPort");
                pnlMontantPort.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlMontantPort.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlMontantPort.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlMontantPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlMontantPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfValeurPort ----
                tfValeurPort.setHorizontalAlignment(SwingConstants.TRAILING);
                tfValeurPort.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfValeurPort.setMaximumSize(new Dimension(100, 30));
                tfValeurPort.setMinimumSize(new Dimension(100, 30));
                tfValeurPort.setPreferredSize(new Dimension(100, 30));
                tfValeurPort.setName("tfValeurPort");
                pnlMontantPort.add(tfValeurPort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbPortPoids ----
                lbPortPoids.setText("x poids");
                lbPortPoids.setMaximumSize(new Dimension(50, 30));
                lbPortPoids.setMinimumSize(new Dimension(50, 30));
                lbPortPoids.setPreferredSize(new Dimension(50, 30));
                lbPortPoids.setHorizontalAlignment(SwingConstants.CENTER);
                lbPortPoids.setFont(new Font("sansserif", Font.BOLD, 13));
                lbPortPoids.setName("lbPortPoids");
                pnlMontantPort.add(lbPortPoids, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfPoidsPort ----
                tfPoidsPort.setHorizontalAlignment(SwingConstants.TRAILING);
                tfPoidsPort.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPoidsPort.setMaximumSize(new Dimension(100, 30));
                tfPoidsPort.setMinimumSize(new Dimension(100, 30));
                tfPoidsPort.setPreferredSize(new Dimension(100, 30));
                tfPoidsPort.setHorizontalTextPosition(SwingConstants.LEADING);
                tfPoidsPort.setName("tfPoidsPort");
                pnlMontantPort.add(tfPoidsPort, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlNegociationAchat.add(pnlMontantPort, new GridBagConstraints(1, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- rbPourcentagePort ----
              rbPourcentagePort.setText("Pourcentage port :");
              rbPourcentagePort.setFont(new Font("sansserif", Font.BOLD, 13));
              rbPourcentagePort.setHorizontalAlignment(SwingConstants.RIGHT);
              rbPourcentagePort.setName("rbPourcentagePort");
              pnlNegociationAchat.add(rbPourcentagePort, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlPourcentagePort ========
              {
                pnlPourcentagePort.setOpaque(false);
                pnlPourcentagePort.setName("pnlPourcentagePort");
                pnlPourcentagePort.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPourcentagePort ----
                tfPourcentagePort.setHorizontalAlignment(SwingConstants.TRAILING);
                tfPourcentagePort.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfPourcentagePort.setMaximumSize(new Dimension(100, 30));
                tfPourcentagePort.setMinimumSize(new Dimension(100, 30));
                tfPourcentagePort.setPreferredSize(new Dimension(100, 30));
                tfPourcentagePort.setName("tfPourcentagePort");
                pnlPourcentagePort.add(tfPourcentagePort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlNegociationAchat.add(pnlPourcentagePort, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbFraisExploitation ----
              lbFraisExploitation.setText("Frais d'exploitation :");
              lbFraisExploitation.setMaximumSize(new Dimension(200, 30));
              lbFraisExploitation.setMinimumSize(new Dimension(200, 30));
              lbFraisExploitation.setPreferredSize(new Dimension(200, 30));
              lbFraisExploitation.setFont(new Font("sansserif", Font.BOLD, 13));
              lbFraisExploitation.setName("lbFraisExploitation");
              pnlNegociationAchat.add(lbFraisExploitation, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlFraisExploitation ========
              {
                pnlFraisExploitation.setOpaque(false);
                pnlFraisExploitation.setName("pnlFraisExploitation");
                pnlFraisExploitation.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlFraisExploitation.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlFraisExploitation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfFraisExploitation ----
                tfFraisExploitation.setHorizontalAlignment(SwingConstants.TRAILING);
                tfFraisExploitation.setFont(new Font("sansserif", Font.PLAIN, 13));
                tfFraisExploitation.setMaximumSize(new Dimension(100, 30));
                tfFraisExploitation.setMinimumSize(new Dimension(100, 30));
                tfFraisExploitation.setPreferredSize(new Dimension(100, 30));
                tfFraisExploitation.setName("tfFraisExploitation");
                pnlFraisExploitation.add(tfFraisExploitation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              }
              pnlNegociationAchat.add(pnlFraisExploitation, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbPrixRevientStandardHT ----
              lbPrixRevientStandardHT.setText("Prix de revient standard HT :");
              lbPrixRevientStandardHT.setMaximumSize(new Dimension(200, 30));
              lbPrixRevientStandardHT.setMinimumSize(new Dimension(200, 30));
              lbPrixRevientStandardHT.setPreferredSize(new Dimension(200, 30));
              lbPrixRevientStandardHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
              lbPrixRevientStandardHT.setFont(new Font("sansserif", Font.BOLD, 13));
              lbPrixRevientStandardHT.setName("lbPrixRevientStandardHT");
              pnlNegociationAchat.add(lbPrixRevientStandardHT, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ======== pnlPrixRevientStandardHT ========
              {
                pnlPrixRevientStandardHT.setOpaque(false);
                pnlPrixRevientStandardHT.setName("pnlPrixRevientStandardHT");
                pnlPrixRevientStandardHT.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- tfPrixRevientStandardHT ----
                tfPrixRevientStandardHT.setMaximumSize(new Dimension(100, 30));
                tfPrixRevientStandardHT.setMinimumSize(new Dimension(100, 30));
                tfPrixRevientStandardHT.setPreferredSize(new Dimension(100, 30));
                tfPrixRevientStandardHT.setName("tfPrixRevientStandardHT");
                pnlPrixRevientStandardHT.add(tfPrixRevientStandardHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbUnitePrixRevientStandardHT ----
                lbUnitePrixRevientStandardHT.setText("UA");
                lbUnitePrixRevientStandardHT.setHorizontalAlignment(SwingConstants.LEADING);
                lbUnitePrixRevientStandardHT.setMinimumSize(new Dimension(50, 30));
                lbUnitePrixRevientStandardHT.setPreferredSize(new Dimension(50, 30));
                lbUnitePrixRevientStandardHT.setFont(new Font("sansserif", Font.BOLD, 13));
                lbUnitePrixRevientStandardHT.setName("lbUnitePrixRevientStandardHT");
                pnlPrixRevientStandardHT.add(lbUnitePrixRevientStandardHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlNegociationAchat.add(pnlPrixRevientStandardHT, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDateApplicationTarifAchat ----
              lbDateApplicationTarifAchat.setText("Date d'application du tarif d'achat :");
              lbDateApplicationTarifAchat.setFont(new Font("sansserif", Font.BOLD, 13));
              lbDateApplicationTarifAchat.setName("lbDateApplicationTarifAchat");
              pnlNegociationAchat.add(lbDateApplicationTarifAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- calDateApplicationTarifAchat ----
              calDateApplicationTarifAchat.setMaximumSize(new Dimension(80, 30));
              calDateApplicationTarifAchat.setMinimumSize(new Dimension(80, 30));
              calDateApplicationTarifAchat.setPreferredSize(new Dimension(80, 30));
              calDateApplicationTarifAchat.setName("calDateApplicationTarifAchat");
              pnlNegociationAchat.add(calDateApplicationTarifAchat, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            }
            sNPanelContenu1.add(pnlNegociationAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          }
          tabbedPane1.addTab("Informations tarifaires", sNPanelContenu1);
        }
        pnlCommentaire.add(tabbedPane1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlCommentaire, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlPrincipal;
  private JPanel pnlCommentaire;
  private JPanel pnlEntete;
  private JLabel lbCodeArticle;
  private JLabel tfCodeArticle;
  private JLabel lbLibelleArticle;
  private JLabel tfLibelleArticle;
  private JLabel lbQuantiteUCV;
  private JLabel tfQuantiteUCV;
  private JLabel lbQuantiteUV;
  private JLabel tfQuantiteUV;
  private JTabbedPane tabbedPane1;
  private SNLienLigne snLignesLiees;
  private SNPanelContenu sNPanelContenu1;
  private SNPanel pnlTarif;
  private SNPanelTitre pnlTarifNegocie;
  private SNLabelChamp lbPrixBase;
  private SNLabelChamp tfPrixBase;
  private SNLabelChamp lbOrigine;
  private SNLabelChamp tfOrigineTarifLigneArticle;
  private SNLabelChamp lbRemise;
  private SNLabelChamp tfRemise;
  private SNLabelChamp lbPrixNet;
  private SNLabelChamp lbMontantTotal;
  private SNLabelChamp tfPrixNet;
  private SNLabelChamp tfMontantTotal;
  private SNPanelTitre pnlTarifNegocieMarge;
  private SNLabelChamp lbIndiceDeMarge;
  private SNLabelChamp lbMarge;
  private SNLabelChamp lbPrixRevient;
  private SNLabelChamp tfIndice;
  private SNLabelChamp tfMarge;
  private SNLabelChamp tfPrixRevient;
  private SNPanelTitre pnlNegociationAchat;
  private SNLabelChamp lbRemisesFournisseur;
  private SNLabelChamp lbPrixAchatBrutHT;
  private SNPanel pnlPrixAchatBrutHT;
  private SNLabelChamp tfPrixAchatBrutHT;
  private SNLabelChamp lbUnitePrixAchatBrutHT;
  private SNPanel pnlRemisesFournisseur;
  private SNLabelChamp tfRemiseFournisseur1;
  private SNLabelChamp tfRemiseFournisseur2;
  private SNLabelChamp tfRemiseFournisseur3;
  private SNLabelChamp tfRemiseFournisseur4;
  private SNLabelChamp lbMontantConditionnement;
  private SNLabelChamp tfMontantConditionnement;
  private SNLabelChamp rbMontantPort;
  private SNPanel pnlMontantPort;
  private SNLabelChamp tfValeurPort;
  private SNLabelChamp lbPortPoids;
  private SNLabelUnite tfPoidsPort;
  private SNLabelChamp rbPourcentagePort;
  private SNPanel pnlPourcentagePort;
  private SNLabelChamp tfPourcentagePort;
  private SNLabelChamp lbFraisExploitation;
  private SNPanel pnlFraisExploitation;
  private SNLabelChamp tfFraisExploitation;
  private SNLabelChamp lbPrixRevientStandardHT;
  private SNPanel pnlPrixRevientStandardHT;
  private SNLabelChamp tfPrixRevientStandardHT;
  private SNLabelChamp lbUnitePrixRevientStandardHT;
  private SNLabelChamp lbDateApplicationTarifAchat;
  private SNLabelChamp calDateApplicationTarifAchat;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
