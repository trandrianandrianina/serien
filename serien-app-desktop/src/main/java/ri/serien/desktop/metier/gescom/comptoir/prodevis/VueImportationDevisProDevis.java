/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.prodevis;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;

import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.autonome.OutilGraphique;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueImportationDevisProDevis extends AbstractVueDialogue<ModeleImportationDevisProDevis> {
  // Constantes
  private static final String BOUTON_SUPPRIMER_FICHIER = "Supprimer fichier";
  private static final String[] TITRE_LISTE_DOCUMENTS =
      new String[] { "Date", "Num\u00e9ro Pro-Devis", "Nom du client", "Date d'import", "Nombre total d'articles", "Montant HT" };
  
  // Variables
  
  /**
   * Constructeur.
   */
  public VueImportationDevisProDevis(ModeleImportationDevisProDevis pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1155, 400);
    setResizable(false);
    
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    tblListeDocuments.personnaliserAspect(TITRE_LISTE_DOCUMENTS, new int[] { 80, 100, 200, 80, 150, 80 },
        new int[] { NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE }, 14);
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListeDocuments.getSelectedRowCount() > 0) {
          validerListeSelection();
        }
      }
    };
    tblListeDocuments.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHaut = OutilGraphique.retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBas = OutilGraphique.retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHaut = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHaut.actionPerformed(ae);
        // Active le bouton Valider si besoin
        rafraichirBoutonSupprimerFichier();
        rafraichirBoutonValider();
      }
    };
    Action nouvelleActionFlecheBas = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBas.actionPerformed(ae);
        // Active le bouton Valider si besoin
        rafraichirBoutonSupprimerFichier();
        rafraichirBoutonValider();
      }
    };
    tblListeDocuments.modifierAction(nouvelleActionFlecheHaut, SNCharteGraphique.TOUCHE_HAUT);
    tblListeDocuments.modifierAction(nouvelleActionFlecheBas, SNCharteGraphique.TOUCHE_BAS);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER_FICHIER, 's', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirListe();
    rafraichirBoutonSupprimerFichier();
    rafraichirBoutonValider();
  }
  
  /**
   * Afficher le bouton supprimer le fichier
   */
  private void rafraichirBoutonSupprimerFichier() {
    // Si une ligne est sélectionnée on dégrise le bouton supprimer et le bouton valider
    int indexvisuel = tblListeDocuments.getSelectedRow();
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER_FICHIER, indexvisuel >= 0);
  }
  
  /**
   * Afficher le bouton valider
   */
  private void rafraichirBoutonValider() {
    // Si une ligne est sélectionnée on dégrise le bouton supprimer et le bouton valider
    int indexvisuel = tblListeDocuments.getSelectedRow();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, indexvisuel >= 0);
  }
  
  // -- Méthodes privées
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListe() {
    List<DescriptionProDevis> liste = getModele().getListeDescriptions();
    String[][] donnees = null;
    
    if (liste != null) {
      donnees = new String[liste.size()][TITRE_LISTE_DOCUMENTS.length];
      for (int ligne = 0; ligne < liste.size(); ligne++) {
        if (liste.get(ligne) != null) {
          donnees[ligne][0] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, liste.get(ligne).getDateCreation());
          donnees[ligne][1] = liste.get(ligne).getNumero();
          donnees[ligne][2] = liste.get(ligne).getNomClient() + " " + liste.get(ligne).getPrenomClient();
          donnees[ligne][3] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, liste.get(ligne).getDateImport());
          donnees[ligne][4] = Constantes.convertirIntegerEnTexte(liste.get(ligne).getNombreTotalDArticles(), 0);
          donnees[ligne][5] = Constantes.formater(liste.get(ligne).getMontantTotalHT(), true);
        }
      }
    }
    tblListeDocuments.mettreAJourDonnees(donnees);
  }
  
  /**
   * Récupère le document sélectionné.
   */
  private void validerListeSelection() {
    int indexvisuel = tblListeDocuments.getSelectedRow();
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeDocuments.getRowSorter() != null) {
        indexreel = tblListeDocuments.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().importerDevisSelectionne(indexreel);
    }
  }
  
  /**
   * Selectionne la ligne sur laquelle on a double cliqué.
   */
  private void selectionnerLigne(MouseEvent e) {
    rafraichirBoutonSupprimerFichier();
    rafraichirBoutonValider();
    
    if (e.getClickCount() == 2) {
      validerListeSelection();
    }
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        validerListeSelection();
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER_FICHIER)) {
        getModele().supprimerFichier(tblListeDocuments.getSelectedRow());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
    try {
      selectionnerLigne(e);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    scpListeDocuments = new JScrollPane();
    tblListeDocuments = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1155, 400));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un devis Pro-Devis");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 1139, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 297, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
        
        // ======== scpListeDocuments ========
        {
          scpListeDocuments.setPreferredSize(new Dimension(1050, 424));
          scpListeDocuments.setName("scpListeDocuments");
          
          // ---- tblListeDocuments ----
          tblListeDocuments.setShowVerticalLines(true);
          tblListeDocuments.setShowHorizontalLines(true);
          tblListeDocuments.setBackground(Color.white);
          tblListeDocuments.setRowHeight(20);
          tblListeDocuments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocuments.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocuments.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblListeDocuments.setName("tblListeDocuments");
          tblListeDocuments.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocuments.setViewportView(tblListeDocuments);
        }
        pnlContenu.add(scpListeDocuments, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(10, 10, 10, 10), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JScrollPane scpListeDocuments;
  private NRiTable tblListeDocuments;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
