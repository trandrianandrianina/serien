/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.controleconditionnementfournisseur;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modele de la boîte de dialogue de contrôle du respect du conditionnement fournisseur
 */
public class ModeleControleConditionnementFournisseur extends AbstractModeleDialogue {
  // Constantes
  public static final int CHOIX_QUANTITE_SAISIE = 0;
  public static final int CHOIX_QUANTITE_INFERIEURE = 1;
  public static final int CHOIX_QUANTITE_SUPERIEURE = 2;
  
  // Variables
  private BigDecimal quantite = null;
  private Article article = null;
  private ListeUnite listeUnite = null;
  private int choixQuantite = CHOIX_QUANTITE_SAISIE;
  private BigDecimal quantiteChoisie = BigDecimal.ZERO;
  private BigDecimal conditionnement = BigDecimal.ZERO;
  private BigDecimal partieEntiere = BigDecimal.ZERO;
  private BigDecimal reste = BigDecimal.ZERO;
  private BigDecimal quantiteInferieure = BigDecimal.ZERO;
  private BigDecimal quantiteSuperieure = BigDecimal.ZERO;
  private boolean isInferieurConditionnement = false;
  
  /**
   * Constructeur.
   */
  public ModeleControleConditionnementFournisseur(SessionBase pSession, Article pArticle, BigDecimal pQuantite) {
    super(pSession);
    article = pArticle;
    quantite = pQuantite;
    quantiteChoisie = quantite;
    chargerDonnees();
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    
  }
  
  @Override
  public void chargerDonnees() {
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
    
    // On calcule le conditionnement fournisseur en UCA
    conditionnement = article.getQuantiteConditionnementFournisseurUS().multiply(article.getPrixAchat().getNombreUSParUCA());
    
    // On calcule si la quantité achetée est parfaitement divisible par le conditionnement fournisseur (reste == 0)
    BigDecimal[] resultat = quantite.divideAndRemainder(conditionnement);
    if (resultat[1].compareTo(BigDecimal.ZERO) == 0) {
      quitterAvecAnnulation();
    }
    else {
      partieEntiere = resultat[0].setScale(listeUnite.getPrecisionUnite(article.getPrixAchat().getIdUCA()), RoundingMode.HALF_UP);
      reste = resultat[1].setScale(listeUnite.getPrecisionUnite(article.getPrixAchat().getIdUCA()), RoundingMode.HALF_UP);
    }
    
    calculerQuantite();
  }
  
  // Méthodes publiques
  /**
   * Modifier le type de quantité sélectionné par l'utilisateur
   */
  public void modifierChoixQuantite(int pChoixQuantiteSaisie) {
    choixQuantite = pChoixQuantiteSaisie;
  }
  
  // Méthodes privées
  /**
   * Calcule les quantités suivant le conditionnement fournisseur.
   */
  private void calculerQuantite() {
    // Inférieure
    if (partieEntiere.compareTo(BigDecimal.ZERO) == 0) {
      isInferieurConditionnement = true;
      reste = conditionnement.subtract(reste);
      quantiteInferieure =
          conditionnement.setScale(listeUnite.getPrecisionUnite(article.getPrixAchat().getIdUCA()), RoundingMode.HALF_UP);
    }
    else {
      quantiteInferieure = partieEntiere.multiply(conditionnement)
          .setScale(listeUnite.getPrecisionUnite(article.getPrixAchat().getIdUCA()), RoundingMode.HALF_UP);
    }
    
    // Supérieure
    quantiteSuperieure = partieEntiere.add(BigDecimal.ONE).multiply(conditionnement)
        .setScale(listeUnite.getPrecisionUnite(article.getPrixAchat().getIdUCA()), RoundingMode.HALF_UP);
  }
  
  // -- Accesseurs
  /**
   * Construire et renvoyer le message
   */
  public Message getMessage() {
    String libelleUnite = " " + listeUnite.getLibelleUnite(article.getPrixAchat().getIdUCA());
    return Message.getMessageNormal("Le conditionnement fournisseur pour cet article est de "
        + Constantes.formater(conditionnement, false) + libelleUnite + ".\n Vous avez saisi une quantité de "
        + Constantes.formater(quantite, false) + libelleUnite + " qui n'en est pas un multiple (écart de "
        + Constantes.formater(reste, false) + libelleUnite + ").\n Confirmez vous cette saisie ?");
  }
  
  /**
   * Construire et renvoyer le texte du radio bouton quantité saisie
   */
  public String getLibelleRbQuantiteSaisie() {
    return "Conserver la quantité saisie (" + Constantes.formater(quantite, false) + article.getPrixAchat().getIdUCA() + ")";
  }
  
  /**
   * Construire et renvoyer le texte du radio bouton quantité inférieure
   */
  public String getLibelleRbQuantiteInferieure() {
    
    return "Choisir le conditionnement inférieur (" + Constantes.formater(quantiteInferieure, false) + article.getPrixAchat().getIdUCA()
        + ")";
  }
  
  /**
   * Construire et renvoyer le texte du radio bouton quantité supérieure
   */
  public String getLibelleRbQuantiteSuperieure() {
    
    return "Choisir le conditionnement supérieur (" + Constantes.formater(quantiteSuperieure, false) + article.getPrixAchat().getIdUCA()
        + ")";
  }
  
  /**
   * Renvoi le type de quantité choisie (inchangée, inférieure ou supérieure)
   */
  public int getChoixQuantite() {
    return choixQuantite;
  }
  
  /**
   * Renvoi si la quantité saisie est inférieure au conditionnement fournisseur
   */
  public boolean isInferieurConditionnement() {
    return isInferieurConditionnement;
  }
  
  /**
   * Renvoi la quantité sélectionnée par l'utilisateur
   */
  public BigDecimal getQuantiteChoisie() {
    switch (choixQuantite) {
      case CHOIX_QUANTITE_INFERIEURE:
        return quantiteInferieure;
      
      case CHOIX_QUANTITE_SUPERIEURE:
        return quantiteSuperieure;
      
      default:
        return quantite;
    }
  }
  
}
