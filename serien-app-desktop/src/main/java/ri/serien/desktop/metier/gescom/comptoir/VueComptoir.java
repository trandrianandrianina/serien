/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Ecran comptoir.
 * Point de menu "[GVM241] Gestion des ventes -> Documents de ventes -> Comptoir et facturation -> Comptoir".
 */
public class VueComptoir extends AbstractVuePanel<ModeleComptoir> {
  // Variables
  protected ClientSerieN fenetre = null;
  protected SNBandeauTitre bandeau = null;
  
  // Variables
  private VueOngletClient ongletClient = null;
  private VueOngletLivraison ongletLivraison = null;
  private VueOngletArticle ongletArticle = null;
  private VueOngletReglement ongletReglement = null;
  private VueOngletEdition ongletEdition = null;
  private ImageIcon iconeBNplein = new ImageIcon(getClass().getResource("/images/blocNotes.png"));
  private ImageIcon iconeBNvide = new ImageIcon(getClass().getResource("/images/blocNotesVide.png"));
  
  /**
   * Constructeur.
   */
  public VueComptoir(ModeleComptoir pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    ongletClient = new VueOngletClient(getModele());
    ongletLivraison = new VueOngletLivraison(getModele());
    ongletArticle = new VueOngletArticle(getModele());
    ongletReglement = new VueOngletReglement(getModele());
    ongletEdition = new VueOngletEdition(getModele());
    
    tbpGeneral.insertTab("Client", null, ongletClient, "Client", ModeleComptoir.ONGLET_CLIENT);
    tbpGeneral.insertTab("Livraison/Enlèvement", null, ongletLivraison, "Livraison/Enlèvement",
        ModeleComptoir.ONGLET_LIVRAISONENLEVEMENT);
    tbpGeneral.insertTab("Articles", null, ongletArticle, "Articles", ModeleComptoir.ONGLET_ARTICLE);
    tbpGeneral.insertTab("Règlements", null, ongletReglement, "Règlements", ModeleComptoir.ONGLET_REGLEMENT);
    tbpGeneral.insertTab("Edition", null, ongletEdition, "Edition", ModeleComptoir.ONGLET_EDITION);
    
    ongletClient.initialiserComposants();
    ongletLivraison.initialiserComposants();
    ongletArticle.initialiserComposants();
    ongletReglement.initialiserComposants();
    ongletEdition.initialiserComposants();
    
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    initialiserAspectTitre(pnlBpresentation, "");
    
    // Les raccourcis claviers
    /*    tabbedPane_General.setMnemonicAt(getModele().ONGLET_CLIENT, KeyEvent.VK_F1);
    tabbedPane_General.setMnemonicAt(getModele().ONGLET_LIVRAISONENLEVEMENT, KeyEvent.VK_F2);
    tabbedPane_General.setMnemonicAt(getModele().ONGLET_ARTICLES, KeyEvent.VK_F3);
    tabbedPane_General.setMnemonicAt(getModele().ONGLET_REGLEMENTS, KeyEvent.VK_F4);
    tabbedPane_General.setMnemonicAt(getModele().ONGLET_EDITION, KeyEvent.VK_F5);
    */
    
    // Grise ou cache les objets graphiques non accessibles au départ
    for (int tab = ModeleComptoir.ONGLET_LIVRAISONENLEVEMENT; tab < tbpGeneral.getTabCount(); tab++) {
      tbpGeneral.setEnabledAt(tab, false);
    }
  }
  
  protected void initialiserAspectTitre(SNBandeauTitre pbandeauTitre, String atitre) {
    bandeau = pbandeauTitre;
    bandeau.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    setTitre(atitre);
  }
  
  /**
   * Mise à jour du titre.
   * @param texte
   */
  public void setTitre(String texte) {
    if (texte != null) {
      bandeau.setText(texte.trim());
    }
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraichir le bandeau
    if (getModele().getEtablissement() != null) {
      pnlBpresentation.setIdEtablissement(getModele().getEtablissement().getId());
    }
    setTitre(getModele().getTitre());
    
    // Accessibilité des onglets
    tbpGeneral.setEnabledAt(ModeleComptoir.ONGLET_CLIENT, getModele().isOngletClientAccessible());
    tbpGeneral.setEnabledAt(ModeleComptoir.ONGLET_LIVRAISONENLEVEMENT, getModele().isOngletLivraisonAccessible());
    tbpGeneral.setEnabledAt(ModeleComptoir.ONGLET_ARTICLE, getModele().isOngletArticleAccessible());
    tbpGeneral.setEnabledAt(ModeleComptoir.ONGLET_REGLEMENT, getModele().isOngletReglementAccessible());
    tbpGeneral.setEnabledAt(ModeleComptoir.ONGLET_EDITION, getModele().isOngletEditionAccessible());
    
    // Activer l'onglet actif
    tbpGeneral.setSelectedIndex(getModele().getIndiceOnglet());
    
    // Rafraichi les informations client et le bloc note document
    rafraichirInformations();
    
    // Rafraichir l'onglet actif
    switch (tbpGeneral.getSelectedIndex()) {
      case ModeleComptoir.ONGLET_CLIENT:
        ongletClient.rafraichir();
        break;
      case ModeleComptoir.ONGLET_LIVRAISONENLEVEMENT:
        ongletLivraison.rafraichir();
        break;
      case ModeleComptoir.ONGLET_ARTICLE:
        ongletArticle.rafraichir();
        break;
      case ModeleComptoir.ONGLET_REGLEMENT:
        ongletReglement.rafraichir();
        break;
      case ModeleComptoir.ONGLET_EDITION:
        ongletEdition.rafraichir();
        break;
    }
  }
  
  /**
   * Met à jour les informations du client et l'icone liée au bloc-notes document.
   */
  private void rafraichirInformations() {
    lbInfosClient.setText(getModele().getTexteInformationsClient());
    
    if (this.isVisible()) {
      // Bouton lié au bloc-notes du document
      if (getModele().getDocumentVenteEnCours() != null) {
        
        btBlocNotesArticles.setVisible(true);
        
        if (btBlocNotesArticles.isVisible()) {
          if (getModele().getDocumentVenteEnCours().isPetitBlocNoteVide()) {
            btBlocNotesArticles.setIcon(iconeBNvide);
            btBlocNotesArticles.setToolTipText("Le bloc-notes document est vide");
          }
          else {
            btBlocNotesArticles.setIcon(iconeBNplein);
            btBlocNotesArticles.setToolTipText("Voir le bloc-notes document");
          }
        }
      }
      else {
        btBlocNotesArticles.setVisible(false);
      }
    }
  }
  
  // -- Méthodes évènementielles
  
  private void bt_SortirComptoirClientActionPerformed(ActionEvent e) {
    try {
      getModele().fermerEcran();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpGeneralStateChanged(ChangeEvent e) {
    try {
      getModele().changerOnglet(tbpGeneral.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btBlocNotesMouseClicked(MouseEvent e) {
    try {
      getModele().ouvrirPetitBlocNotesDocument();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    pnlBpresentation = new SNBandeauTitre();
    pnlSud = new JPanel();
    scpContenu = new JScrollPane();
    pnlContenu = new JPanel();
    pnlInformationsClient = new JPanel();
    lbInfosClient = new JLabel();
    btBlocNotesArticles = new JLabel();
    tbpGeneral = new JTabbedPane();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Comptoir - ");
      pnlBpresentation.setName("pnlBpresentation");
      pnlNord.add(pnlBpresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setPreferredSize(new Dimension(1024, 650));
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== scpContenu ========
      {
        scpContenu.setPreferredSize(new Dimension(1024, 650));
        scpContenu.setBorder(null);
        scpContenu.setName("scpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(1024, 650));
          pnlContenu.setBackground(new Color(238, 238, 210));
          pnlContenu.setForeground(Color.black);
          pnlContenu.setMinimumSize(new Dimension(1200, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new BorderLayout(0, -30));
          
          // ======== pnlInformationsClient ========
          {
            pnlInformationsClient.setMinimumSize(new Dimension(235, 30));
            pnlInformationsClient.setPreferredSize(new Dimension(235, 30));
            pnlInformationsClient.setOpaque(false);
            pnlInformationsClient.setName("pnlInformationsClient");
            pnlInformationsClient.setLayout(new FlowLayout(FlowLayout.RIGHT));
            
            // ---- lbInfosClient ----
            lbInfosClient.setText("Informations client");
            lbInfosClient.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbInfosClient.setForeground(new Color(125, 125, 94));
            lbInfosClient.setHorizontalAlignment(SwingConstants.RIGHT);
            lbInfosClient.setName("lbInfosClient");
            pnlInformationsClient.add(lbInfosClient);
            
            // ---- btBlocNotesArticles ----
            btBlocNotesArticles.setIcon(new ImageIcon(getClass().getResource("/images/blocNotesVide.png")));
            btBlocNotesArticles.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            btBlocNotesArticles.setName("btBlocNotesArticles");
            btBlocNotesArticles.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                btBlocNotesMouseClicked(e);
              }
            });
            pnlInformationsClient.add(btBlocNotesArticles);
          }
          pnlContenu.add(pnlInformationsClient, BorderLayout.NORTH);
          
          // ======== tbpGeneral ========
          {
            tbpGeneral.setFont(
                tbpGeneral.getFont().deriveFont(tbpGeneral.getFont().getStyle() & ~Font.BOLD, tbpGeneral.getFont().getSize() + 5f));
            tbpGeneral.setBackground(new Color(212, 133, 54));
            tbpGeneral.setPreferredSize(new Dimension(1245, 670));
            tbpGeneral.setMinimumSize(new Dimension(1245, 670));
            tbpGeneral.setFocusCycleRoot(true);
            tbpGeneral.setFocusTraversalPolicyProvider(true);
            tbpGeneral.setName("tbpGeneral");
            tbpGeneral.addChangeListener(new ChangeListener() {
              @Override
              public void stateChanged(ChangeEvent e) {
                tbpGeneralStateChanged(e);
              }
            });
          }
          pnlContenu.add(tbpGeneral, BorderLayout.CENTER);
        }
        scpContenu.setViewportView(pnlContenu);
      }
      pnlSud.add(scpContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre pnlBpresentation;
  private JPanel pnlSud;
  private JScrollPane scpContenu;
  private JPanel pnlContenu;
  private JPanel pnlInformationsClient;
  private JLabel lbInfosClient;
  private JLabel btBlocNotesArticles;
  private JTabbedPane tbpGeneral;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
