/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisionnementstock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.ModeleTableEditable;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueApprovisionnementStock extends AbstractVueDialogue<ModeleApprovisionnementStock> {
  // Constantes
  private static final String[] TITRE_LISTE = new String[] { "Quantit\u00e9", "UCA", "Libell\u00e9", "Physique", "Command\u00e9",
      "Attendu", "Disponible", "Minimum", "Id\u00e9al" };
  private static final String BOUTON_AFFICHER_DETAIL_ARTICLE = "Afficher détail article";
  private static final String BOUTON_AFFICHER_DETAIL_CALCUL = "Afficher détail calcul";
  
  // Variables
  
  // Classe interne qui permet de gérer les flèches dans la table tblListe
  class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;
    
    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      validerQuantiteLigne(e, actionOriginale);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueApprovisionnementStock(ModeleApprovisionnementStock pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    // setSize(700, 400);
    setResizable(false);
    
    final boolean[] colonnesEditables = new boolean[] { true, false, false, false, false, false, false, false, false };
    final int[] dimension = new int[] { 80, 50, 500, 80, 80, 80, 80, 80, 80 };
    final int[] justification = new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE,
        NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE };
    ModeleTableEditable modeleTable = new ModeleTableEditable(TITRE_LISTE, colonnesEditables);
    tblListe.personnaliserAspect(modeleTable, dimension, justification, 14);
    tblListe.setSelectAllForEdit(true);
    // Permet de forcer la saisie d'une cellule si l'on clique sur un autre composant (le bouton valider par exemple)
    tblListe.forcerSaisieCellule(true);
    // Permet de prendre en compte la valeur d'une cellule lorsque l'on "valide" en donnant le focus à une autre cellule
    tblListe.getModel().addTableModelListener(new TableModelListener() {
      @Override
      public void tableChanged(TableModelEvent e) {
        if (e.getType() == TableModelEvent.UPDATE) {
          int ligne = e.getFirstRow();
          if (ligne == -1) {
            return;
          }
          String valeur = (String) tblListe.getModel().getValueAt(ligne, e.getColumn());
          getModele().modifierQuantiteUCA(e.getFirstRow(), valeur);
        }
      }
    });
    
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListe.getActionMap().put(actionEnter, actionCelluleEnter);
    // Modification de la gestion des évènements sur la touche TAB de la jtable
    final Action originalActionTab = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_TAB);
    String actionTab = "actionTab";
    ActionCellule actionCelluleTab = new ActionCellule(actionTab, originalActionTab);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_TAB, actionTab);
    tblListe.getActionMap().put(actionTab, actionCelluleTab);
    // Modification de la gestion des évènements sur la touche HAUT de la jtable
    final Action originalActionHaut = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_HAUT);
    String actionHaut = "actionHaut";
    ActionCellule actionCelluleHaut = new ActionCellule(actionHaut, originalActionHaut);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_HAUT, actionHaut);
    tblListe.getActionMap().put(actionHaut, actionCelluleHaut);
    // Modification de la gestion des évènements sur la touche BAS de la jtable
    final Action originalActionBas = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_BAS);
    String actionBas = "actionBas";
    ActionCellule actionCelluleBas = new ActionCellule(actionBas, originalActionBas);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_BAS, actionBas);
    tblListe.getActionMap().put(actionBas, actionCelluleBas);
    // Modification de la gestion des évènements sur la touche GAUCHE de la jtable
    final Action originalActionGauche = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_GAUCHE);
    String actionGauche = "actionGauche";
    ActionCellule actionCelluleGauche = new ActionCellule(actionGauche, originalActionGauche);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_GAUCHE, actionGauche);
    tblListe.getActionMap().put(actionGauche, actionCelluleGauche);
    // Modification de la gestion des évènements sur la touche DROITE de la jtable
    final Action originalActionDroite = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_DROITE);
    String actionDroite = "actionDroite";
    ActionCellule actionCelluleDroite = new ActionCellule(actionDroite, originalActionDroite);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_DROITE, actionDroite);
    tblListe.getActionMap().put(actionDroite, actionCelluleDroite);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListe.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeSelectionChanged(e);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListe.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeStateChanged(e);
      }
    });
    viewportLigneAchat.setBackground(Color.WHITE);
    
    // Raccourcis clavier
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL_ARTICLE, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL_CALCUL, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessageErreur();
    rafraichirListe();
    rafraichirBoutonValider();
    rafraichirBoutonDetailArticle();
    rafraichirBoutonDetailCalcul();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessageErreur() {
    Message message = getModele().getMessage();
    if (message == null) {
      lbMessageErreur.setText("");
      lbMessageErreur.setVisible(false);
    }
    else {
      lbMessageErreur.setText(message.getTexte());
      lbMessageErreur.setVisible(true);
      if (message.isImportanceHaute()) {
        lbMessageErreur.setForeground(Color.RED);
      }
      else {
        lbMessageErreur.setForeground(Color.BLACK);
      }
    }
  }
  
  private void rafraichirListe() {
    ArrayList<LigneTableau> liste = getModele().getListeChargement();
    String[][] donnees = null;
    
    if (liste != null) {
      donnees = new String[liste.size()][TITRE_LISTE.length];
      for (int ligne = 0; ligne < liste.size(); ligne++) {
        donnees[ligne][0] = liste.get(ligne).getQuantiteCommandeeUCA();
        donnees[ligne][1] = liste.get(ligne).getUniteCommandeUCA();
        if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
          donnees[ligne][2] = liste.get(ligne).getLibelle() + " (" + liste.get(ligne).getIdArticle().getCodeArticle() + ")";
        }
        else {
          donnees[ligne][2] = liste.get(ligne).getLibelle();
        }
        donnees[ligne][3] = liste.get(ligne).getStockPhysique();
        donnees[ligne][4] = liste.get(ligne).getStockCommande();
        donnees[ligne][5] = liste.get(ligne).getStockAttendu();
        donnees[ligne][6] = liste.get(ligne).getStockDisponible();
        donnees[ligne][7] = liste.get(ligne).getStockMinimum();
        donnees[ligne][8] = liste.get(ligne).getStockIdeal();
      }
    }
    tblListe.mettreAJourDonnees(donnees);
    
    // Remettre la ligne sélectionné avant le rafraichissement
    int index = getModele().getIndiceTableau();
    if (index > -1 && index < tblListe.getRowCount()) {
      tblListe.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  /**
   * Rafraichir l'affichage du bouton Valider
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isVerifierArticleCommande();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Rafraichir l'affichage du bouton détail de l'article
   */
  private void rafraichirBoutonDetailArticle() {
    boolean actif = tblListe.getSelectedRowCount() > 0;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL_ARTICLE, actif);
  }
  
  /**
   * Rafraichir l'affichage du bouton détail du calcul
   */
  private void rafraichirBoutonDetailCalcul() {
    boolean actif = tblListe.getSelectedRowCount() > 0;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL_CALCUL, actif);
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerQuantiteLigne(ActionEvent e, Action actionOriginale) {
    try {
      int indexLigneSeletionnee = tblListe.getIndiceSelection();
      if (indexLigneSeletionnee == -1) {
        return;
      }
      if (tblListe.editCellAt(indexLigneSeletionnee, 0)) {
        // Lire la valeur sur la ligne sélectionnée avant de déclencher le traitement de l'action par le tableau car celui-ci efface
        // la valeur saisie dans certains cas (déplacement en fin de tableau).
        String valeur = (String) tblListe.getValueAt(indexLigneSeletionnee, 0);
        
        // Déclencher le traitement de l'action par le tableau
        actionOriginale.actionPerformed(e);
        
        // Informer le modèle qu'une valeur a été saisie pour un article
        getModele().modifierQuantiteUCA(indexLigneSeletionnee, valeur);
      }
      else {
        actionOriginale.actionPerformed(e);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  /**
   * La ligne sélectionnée a changée dans le tableau.
   */
  private void tblListeSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierIndiceTableau(tblListe.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL_ARTICLE)) {
        getModele().afficherDetailArticle();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL_CALCUL)) {
        getModele().afficherDetailCalcul();
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpListeStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListe == null) {
        return;
      }
      Trace.debug(VueApprovisionnementStock.class, "scpListeStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListe.getViewport().getViewRect();
      int premiereLigne = tblListe.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListe.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListe.getRowCount() - 1;
      }
      
      // Charges les lignes d'achat concernées si besoin
      getModele().modifierPlageArticlesAffichees(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeMouseClicked(MouseEvent e) {
    try {
      // Informer le modèle que l'indice de la ligne sélectionnée a changé
      getModele().modifierIndiceTableau(tblListe.getIndiceSelection());
      // On affiche le détail de la ligne double cliquée
      if (e.getClickCount() == 2) {
        getModele().afficherDetailArticle();
        return;
      }
      rafraichirBoutonDetailArticle();
      rafraichirBoutonDetailCalcul();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlPrincipal = new SNPanelContenu();
    lbMessageErreur = new JLabel();
    scpListe = new JScrollPane();
    tblListe = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1300, 400));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Liste des articles g\u00e9r\u00e9s en stock \u00e0 approvisionner");
    setModal(true);
    setFont(new Font(Font.DIALOG, Font.PLAIN, 14));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setForeground(Color.black);
      pnlCouleurFond.setBorder(null);
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 1139, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 297, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ---- lbMessageErreur ----
        lbMessageErreur.setFont(lbMessageErreur.getFont().deriveFont(lbMessageErreur.getFont().getStyle() | Font.BOLD,
            lbMessageErreur.getFont().getSize() + 2f));
        lbMessageErreur.setMaximumSize(new Dimension(457, 60));
        lbMessageErreur.setMinimumSize(new Dimension(457, 30));
        lbMessageErreur.setPreferredSize(new Dimension(457, 30));
        lbMessageErreur.setForeground(Color.red);
        lbMessageErreur.setName("lbMessageErreur");
        pnlPrincipal.add(lbMessageErreur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListe ========
        {
          scpListe.setPreferredSize(new Dimension(800, 424));
          scpListe.setName("scpListe");
          
          // ---- tblListe ----
          tblListe.setShowVerticalLines(true);
          tblListe.setShowHorizontalLines(true);
          tblListe.setBackground(Color.white);
          tblListe.setRowHeight(20);
          tblListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListe.setSelectionBackground(new Color(57, 105, 138));
          tblListe.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblListe.setOpaque(false);
          tblListe.setGridColor(new Color(204, 204, 204));
          tblListe.setName("tblListe");
          tblListe.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeMouseClicked(e);
            }
          });
          scpListe.setViewportView(tblListe);
        }
        pnlPrincipal.add(scpListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlPrincipal, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlPrincipal;
  private JLabel lbMessageErreur;
  private JScrollPane scpListe;
  private NRiTable tblListe;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
