/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.stocksarticle;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.StockComptoir;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

public class ModeleAffichageStockArticle extends AbstractModelePanel {
  // Constantes
  public static final int NE_PAS_AFFICHER = -1;
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  private static final int NOMBRE_DECIMALE_COEFFICIENT = 4;
  
  // Variables
  private Article article = null;
  private ListeStockComptoir listeStock = null;
  private ListeEtablissement listeEtablissement = null;
  private ListeMagasin listeMagasin = null;
  private ListeUnite listeUnite = null;
  private Message message = null;
  
  private EnumTypeStockDisponible enumTypeStockDisponible = null;
  
  /**
   * Constructeur.
   */
  public ModeleAffichageStockArticle(SessionBase pSession, Article pArticle, EnumTypeStockDisponible pEnumTypeStock) {
    super(pSession);
    article = pArticle;
    enumTypeStockDisponible = pEnumTypeStock;
    initialiserDonnees();
    chargerDonnees();
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    
    if (article == null) {
      throw new MessageErreurException("L'article transmis est incorrect.");
    }
    
    // Charger la liste des établissements
    if (listeEtablissement == null) {
      listeEtablissement = ListeEtablissement.charger(getSession().getIdSession());
    }
    
    // Charger la liste des magasins de tous les établissements
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
      for (ri.serien.libcommun.gescom.commun.etablissement.Etablissement etablissement : listeEtablissement) {
        listeMagasin.addAll(listeMagasin.charger(getIdSession(), etablissement.getId()));
      }
      
    }
    
    // Récupération de la liste des unités
    if (listeUnite == null) {
      listeUnite = new ListeUnite();
      listeUnite = listeUnite.charger(getIdSession());
    }
    
    // Si les stocks n'ont pas été chargés alors on appelle le service dédié
    if (listeStock == null) {
      listeStock = ListeStockComptoir.chargerPourArticle(getIdSession(), null, article.getId().getCodeArticle());
    }
    
    // On vérifie s'il y a du surstock pour cet article
    if (listeStock != null && listeStock.isArticleEnSurStock()) {
      message = Message.getMessageImportant("Les stocks physiques affich\u00e9s en rouge indiquent que l'article est en surstock.");
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne la précision d'une unité donnée
   */
  public int getPrecisionUniteStock() {
    return listeUnite.getPrecisionUnite(article.getIdUS());
  }
  
  /**
   * Formate et retourne la quantité physique d'un article.
   */
  public String getFormaterStockPhysique(StockComptoir pStock) {
    if (pStock == null) {
      throw new MessageErreurException("Le stock est incorrect.");
    }
    String quantitePhysique = Constantes.formater(pStock.getQuantitePhysique(), false);
    // On vérifie si l'article est en sur stock si c'est le cas alors la valeur est affichée en rouge
    if (pStock.isArticleEnSurStock()) {
      quantitePhysique = "<html><span style='color:#FF0000'>" + quantitePhysique + "</span></html>";
    }
    return quantitePhysique;
  }
  
  // -- Accesseurs
  
  public ListeStockComptoir getListeStock() {
    return listeStock;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  public Message getMessage() {
    return message;
  }
  
  public EnumTypeStockDisponible getEnumTypeStockDisponible() {
    return enumTypeStockDisponible;
  }
  
}
