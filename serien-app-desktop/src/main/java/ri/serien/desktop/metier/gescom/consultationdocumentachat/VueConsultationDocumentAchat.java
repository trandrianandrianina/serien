/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran principal de la consultation de documents d'achat.
 */
public class VueConsultationDocumentAchat extends AbstractVuePanel<ModeleConsultationDocumentAchat> {
  // Variables
  private VueListeConsultationDocumentAchat vueRecherche;
  private VueDetailConsultationDocumentAchat vueDetail;
  
  /**
   * Constructeur.
   */
  public VueConsultationDocumentAchat(ModeleConsultationDocumentAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de titre
    bpBandeauPresentation.setCapitaliserPremiereLettre(false);
    bpBandeauPresentation.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    
    // Instancier la vue liste
    vueRecherche = new VueListeConsultationDocumentAchat(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.LISTE, vueRecherche);
    pnlContenu.add(vueRecherche, EnumCleVueListeDetail.LISTE.getLibelle());
    
    // Initialiser la vue détail
    vueDetail = new VueDetailConsultationDocumentAchat(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.DETAIL, vueDetail);
    pnlContenu.add(vueDetail, EnumCleVueListeDetail.DETAIL.getLibelle());
  }
  
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirVue();
  }
  
  private void rafraichirTitre() {
    if (getModele().getCleVueEnfantActive() != null) {
      // Obligatoire afin que ce soit pris en compte lors du rafraichissement sinon le titre est mis à jour après le chargée donnéees
      if (getModele().getCleVueEnfantActive().equals(EnumCleVueListeDetail.DETAIL)) {
        getModele().construireTitreDetail();
      }
    }
    if (getModele().getTitreEcran() != null) {
      bpBandeauPresentation.setText(getModele().getTitreEcran());
    }
    else {
      bpBandeauPresentation.setText("");
    }
  }
  
  private void rafraichirVue() {
    InterfaceCleVue interfaceCleVue = getModele().getCleVueEnfantActive();
    if (interfaceCleVue != null) {
      CardLayout cardLayout = (CardLayout) (pnlContenu.getLayout());
      cardLayout.show(pnlContenu, interfaceCleVue.getLibelle());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    bpBandeauPresentation = new SNBandeauTitre();
    pnlSud = new JPanel();
    scpContenu = new JScrollPane();
    pnlContenu = new JPanel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- bpBandeauPresentation ----
      bpBandeauPresentation.setText("Consultation de documents d'achats");
      bpBandeauPresentation.setName("bpBandeauPresentation");
      pnlNord.add(bpBandeauPresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setPreferredSize(new Dimension(1024, 650));
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== scpContenu ========
      {
        scpContenu.setPreferredSize(new Dimension(1024, 650));
        scpContenu.setBorder(null);
        scpContenu.setName("scpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(1024, 650));
          pnlContenu.setBackground(new Color(239, 239, 222));
          pnlContenu.setForeground(Color.black);
          pnlContenu.setMinimumSize(new Dimension(1024, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new CardLayout());
        }
        scpContenu.setViewportView(pnlContenu);
      }
      pnlSud.add(scpContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre bpBandeauPresentation;
  private JPanel pnlSud;
  private JScrollPane scpContenu;
  private JPanel pnlContenu;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
