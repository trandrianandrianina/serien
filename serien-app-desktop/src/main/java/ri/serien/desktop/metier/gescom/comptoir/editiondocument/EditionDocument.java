/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.editiondocument;

import java.util.Date;

import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.DestinataireMail;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeDestinataireReponse;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdDestinataireMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.InterfaceSoumission;

/**
 * Cette classe permet de gérer les différents traitement pour les modes d'édition qui ont été sélectionné pour un document de vente.
 * ATTENTION Développement en cours ...
 */
public class EditionDocument implements InterfaceSoumission {
  // Variables
  private DocumentVente documentVente = null;
  private SessionBase session = null;
  private Date dateTraitement = null;
  private ListeModeEdition listeModeEdition = null;
  private OptionsValidation optionsValidation = null;
  private OptionsEditionVente optionsEdition = null;
  private String emailDestinataire = null;
  private String numeroFax = null;
  private ListeVendeur listeVendeur = null;
  
  /**
   * Constructeur.
   */
  public EditionDocument(SessionBase pSession, DocumentVente pDocumentVente, Date pDateTraitement, ListeModeEdition pListeModeEdition,
      OptionsValidation pOptionsValidation, OptionsEditionVente pOptionsEdition, ListeVendeur pListeVendeur) {
    session = pSession;
    documentVente = pDocumentVente;
    dateTraitement = pDateTraitement;
    listeModeEdition = pListeModeEdition;
    optionsValidation = pOptionsValidation;
    optionsEdition = pOptionsEdition;
    listeVendeur = pListeVendeur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Edition du document.
   */
  public void editer() {
    if (documentVente == null) {
      throw new MessageErreurException("Le document de vente est invalide.");
    }
    if (optionsValidation == null) {
      throw new MessageErreurException("Les options de validation sont invalides.");
    }
    
    // On parcourt la liste des modes d'édition qui ont été sélectionné
    for (ModeEdition modeEdition : listeModeEdition) {
      // Si le mode d'édition n'est pas actif ou qu'il n'a pas été sélectionné on passe au suivant
      if (!modeEdition.isActif() || !modeEdition.isSelectionne()) {
        continue;
      }
      // Si le mode d'édition a déjà été traité on passe au suivant
      if (modeEdition.isTraite()) {
        continue;
      }
      
      try {
        switch (modeEdition.getEnumModeEdition()) {
          case IMPRIMER:
            imprimer(modeEdition);
            break;
          case ENVOYER_PAR_FAX:
            faxer(modeEdition);
            break;
          case ENVOYER_PAR_MAIL:
            mailer(modeEdition);
            break;
          case VISUALISER:
            visualiser(modeEdition);
            break;
          default:
            break;
        }
      }
      catch (MessageErreurException exception) {
        DialogueErreur.afficher(exception);
      }
      
      // Efface les variables pour NewSim après chaque utilisation
      optionsEdition.effacerVariableNewSim();
    }
    
    try {
      // Editer les documents supplémentaires
      editerDocumentSupplementaire();
    }
    catch (MessageErreurException exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Demande l'exécution en tâche de fond.
   */
  @Override
  public void executer() {
    editer();
  }
  
  // -- Méthodes privées
  
  /**
   * Imprime un document à partir de son spool sur une imprimante reconnue par l'AS400 (ne concerne pas les éditions NewSim V2).
   * Il faut une affectation d'impression avec un code état sans le @ pour que cela fonctionne, par exemple DEV.
   */
  private void imprimer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    
    // Pour l'instant seules les factures sont stockées dans la base documentaire
    boolean stockageBaseDocumentaire = false;
    EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
    String indicatifIdDocumentPourBaseDocumentaire = null;
    String indicatifIdClientPourBaseDocumentaire = null;
    if (documentVente.isFacture()) {
      stockageBaseDocumentaire = true;
      typeDocumentStocke = EnumTypeDocumentStocke.FACTURE_VENTES;
      indicatifIdDocumentPourBaseDocumentaire = documentVente.getId().getIndicatif(IdDocumentVente.INDICATIF_FACTURE);
      indicatifIdClientPourBaseDocumentaire = documentVente.getIdClientFacture().getIndicatif(IdClient.INDICATIF_ETB_NUM_SUF);
    }
    
    // Initialisation de l'option d'édition
    optionsEdition.initCodeEdition(documentVente.getTypeDocumentVente(), optionsValidation.getCodePropose(), pModeEdition);
    optionsEdition.setStockageBaseDocumentaire(stockageBaseDocumentaire);
    optionsEdition.setTypeDocumentStocke(typeDocumentStocke);
    optionsEdition.setIndicatifDocument(indicatifIdDocumentPourBaseDocumentaire);
    optionsEdition.setIndicatifClient(indicatifIdClientPourBaseDocumentaire);
    
    // Appel du service qui va générer l'édition
    ManagerServiceDocumentVente.editerDocumentVente(session.getIdSession(), documentVente.getId(), documentVente.isModifiable(),
        dateTraitement, optionsEdition, session.getLdaSerien());
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Visualise un document.
   * Un fichier PDF est généré, téléchargé sur le poste de travail et enfin affiché avec l'application adéquat.
   * Il faut une affectation d'impression avec un code état avec le @ pour que cela fonctionne, par exemple DEV@.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void visualiser(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    
    // Pour l'instant seules les factures sont stockées dans la base documentaire
    boolean stockageBaseDocumentaire = false;
    EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
    String indicatifIdDocumentPourBaseDocumentaire = null;
    String indicatifIdClientPourBaseDocumentaire = null;
    if (documentVente.isFacture()) {
      stockageBaseDocumentaire = true;
      typeDocumentStocke = EnumTypeDocumentStocke.FACTURE_VENTES;
      indicatifIdDocumentPourBaseDocumentaire = documentVente.getId().getIndicatif(IdDocumentVente.INDICATIF_FACTURE);
      indicatifIdClientPourBaseDocumentaire = documentVente.getIdClientFacture().getIndicatif(IdClient.INDICATIF_ETB_NUM_SUF);
    }
    
    // L'option d'édition est de type mail car elle permet de sélectionner l'affectation d'impression qui va générer un fichier PDF
    // C'est une limitation de Série N sur laquelle il va falloir travailler
    optionsEdition.initCodeEdition(documentVente.getTypeDocumentVente(), optionsValidation.getCodePropose(), pModeEdition);
    optionsEdition.setStockageBaseDocumentaire(stockageBaseDocumentaire);
    optionsEdition.setTypeDocumentStocke(typeDocumentStocke);
    optionsEdition.setIndicatifDocument(indicatifIdDocumentPourBaseDocumentaire);
    optionsEdition.setIndicatifClient(indicatifIdClientPourBaseDocumentaire);
    
    // Appel du service qui va générer l'édition
    String cheminPDF = ManagerServiceDocumentVente.editerDocumentVente(session.getIdSession(), documentVente.getId(),
        documentVente.isModifiable(), dateTraitement, optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(".pdf")) {
      throw new MessageErreurException(
          "Vérifier vos affectations d'impression car ce n'est pas un fichier PDF qui a été généré pour le mode d'édition \""
              + pModeEdition.getEnumModeEdition().getLibelle() + "\".");
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException("Une erreur s'est produite pendant la génération de l'édition pour le mode d'édition \""
          + pModeEdition.getEnumModeEdition().getLibelle() + "\".");
    }
    // Transférer le PDF sur le poste client
    String pdf = session.getLexique().RecuperationFichier(cheminPDF);
    
    // Ouvrir le PDF avec l'application associée
    session.getLexique().AfficheDocument(pdf);
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Edite les documents supplémentaires.
   */
  private void editerDocumentSupplementaire() {
    if (!optionsEdition.isEditionDocumentSupplementaire()) {
      return;
    }
    
    // L'option d'édition est de type mail car elle permet de sélectionner l'affectation d'impression qui va générer un fichier PDF
    // C'est une limitation de Série N sur laquelle il va falloir travailler
    optionsEdition.initCodeEditionDocumentSupplementaire(documentVente.getTypeDocumentVente(), optionsValidation.getCodePropose());
    
    // Appel du service qui va générer l'édition
    String cheminPDF = ManagerServiceDocumentVente.editerDocumentVente(session.getIdSession(), documentVente.getId(),
        documentVente.isModifiable(), dateTraitement, optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(".pdf") || cheminPDF.isEmpty()) {
      return;
    }
    // Transférer le PDF sur le poste client
    String pdf = session.getLexique().RecuperationFichier(cheminPDF);
    
    // Ouvrir le PDF avec l'application associée
    session.getLexique().AfficheDocument(pdf);
  }
  
  /**
   * Envoi un document par mail.
   * Un fichier PDF est généré.
   * Il faut une affectation d'impression avec un code état avec le @ pour que cela fonctionne, par exemple DEV@.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void mailer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    if (emailDestinataire == null) {
      throw new MessageErreurException("L'email du destinataire est incorrect.");
    }
    
    // Création du mail
    Mail mail = null;
    boolean stockageBaseDocumentaire = false;
    EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
    String indicatifIdDocumentPourBaseDocumentaire = null;
    String indicatifIdClientPourBaseDocumentaire = null;
    
    // Préparation du destinataire
    String indicatifDocumentPourMail = null;
    if (documentVente.getId() != null) {
      indicatifDocumentPourMail = documentVente.getId().toString();
    }
    
    // Détermination du type de mail à générer et si le document doit être stocké dans la base documentaire
    EnumTypeMail enumTypeMail = EnumTypeMail.MAIL_DOCUMENT_EN_PIECE_JOINTE;
    if (documentVente.isDevis()) {
      enumTypeMail = EnumTypeMail.MAIL_DEVIS_PIECE_JOINTE;
    }
    else if (documentVente.isBon()) {
      enumTypeMail = EnumTypeMail.MAIL_BON_PIECE_JOINTE;
    }
    else if (documentVente.isCommande()) {
      enumTypeMail = EnumTypeMail.MAIL_ACCUSE_RECEPTION_COMMANDE_PIECE_JOINTE;
    }
    else if (documentVente.isFacture()) {
      enumTypeMail = EnumTypeMail.MAIL_FACTURE_PIECE_JOINTE;
      // Pour l'instant seules les factures sont stockées dans la base documentaire
      stockageBaseDocumentaire = true;
      typeDocumentStocke = EnumTypeDocumentStocke.FACTURE_VENTES;
      indicatifIdDocumentPourBaseDocumentaire = documentVente.getId().getIndicatif(IdDocumentVente.INDICATIF_FACTURE);
      indicatifIdClientPourBaseDocumentaire = documentVente.getIdClientFacture().getIndicatif(IdClient.INDICATIF_ETB_NUM_SUF);
    }
    
    // Recherche s'il y a un destinataire pour la réponse au mail en fonction du type de mail
    IdTypeMail idTypeMail = IdTypeMail.getInstance(documentVente.getId().getIdEtablissement(), enumTypeMail);
    String emailDestinataireReponse = recupererAdresseEmailReponse(idTypeMail);
    try {
      // Préparation du mail
      mail = preparerMail(emailDestinataire, idTypeMail, indicatifDocumentPourMail, emailDestinataireReponse);
    }
    catch (MessageErreurException e) {
      Trace.erreur("Le mail n'a pas pu être créé.");
    }
    
    if (mail == null) {
      throw new MessageErreurException("Erreur lors de la préparation du mail.");
    }
    
    // Initialisation de l'option d'édition
    optionsEdition.initCodeEdition(documentVente.getTypeDocumentVente(), optionsValidation.getCodePropose(), pModeEdition);
    optionsEdition.setIdMail(mail.getId());
    optionsEdition.setStockageBaseDocumentaire(stockageBaseDocumentaire);
    optionsEdition.setTypeDocumentStocke(typeDocumentStocke);
    optionsEdition.setIndicatifDocument(indicatifIdDocumentPourBaseDocumentaire);
    optionsEdition.setIndicatifClient(indicatifIdClientPourBaseDocumentaire);
    
    // Appel du service qui va générer l'édition puis envoyer le mail
    String cheminPDF = ManagerServiceDocumentVente.editerDocumentVente(session.getIdSession(), documentVente.getId(),
        documentVente.isModifiable(), dateTraitement, optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(EnumTypeDocument.PDF.getExtension())) {
      throw new MessageErreurException(
          "Vérifier vos affectations d'impression car ce n'est pas un fichier PDF qui a été généré pour le mode d'édition \""
              + pModeEdition.getTexte() + "\".");
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException(
          "Une erreur s'est produite pendant la génération de l'édition pour le mode d'édition \"" + pModeEdition.getTexte() + "\".");
    }
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Envoi un document par fax.
   * Un fichier PDF est généré.
   * Il faut une affectation d'impression avec un code état avec le 'X' pour que cela fonctionne, par exemple DEVX.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void faxer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    if (Constantes.normerTexte(numeroFax).isEmpty()) {
      throw new MessageErreurException("Le numéro de fax est incorrect.");
    }
    
    // Création du mail
    Mail mail = null;
    try {
      String indicatifDocumentPourMail = null;
      if (documentVente.getId() != null) {
        indicatifDocumentPourMail = documentVente.getId().toString();
      }
      
      // Préparation du mail pour le fax
      IdTypeMail idTypeMail = IdTypeMail.getInstance(documentVente.getId().getIdEtablissement(), EnumTypeMail.MAIL_ENVOI_FAX);
      mail = preparerMail(getNettoyerNumeroFax(numeroFax), idTypeMail, indicatifDocumentPourMail, null);
    }
    catch (MessageErreurException e) {
      Trace.erreur("Le mail n'a pas pu être créé.");
    }
    
    if (mail == null) {
      throw new MessageErreurException("Erreur lors de la préparation du mail.");
    }
    
    // Pour l'instant seules les factures sont stockées dans la base documentaire
    boolean stockageBaseDocumentaire = false;
    EnumTypeDocumentStocke typeDocumentStocke = EnumTypeDocumentStocke.NON_DEFINI;
    String indicatifIdDocumentPourBaseDocumentaire = null;
    String indicatifIdClientPourBaseDocumentaire = null;
    if (documentVente.isFacture()) {
      stockageBaseDocumentaire = true;
      typeDocumentStocke = EnumTypeDocumentStocke.FACTURE_VENTES;
      indicatifIdDocumentPourBaseDocumentaire = documentVente.getId().getIndicatif(IdDocumentVente.INDICATIF_FACTURE);
      indicatifIdClientPourBaseDocumentaire = documentVente.getIdClientFacture().getIndicatif(IdClient.INDICATIF_ETB_NUM_SUF);
    }
    
    // Initialisation de l'option d'édition
    optionsEdition.initCodeEdition(documentVente.getTypeDocumentVente(), optionsValidation.getCodePropose(), pModeEdition);
    optionsEdition.setIdFaxMail(mail.getId());
    optionsEdition.setStockageBaseDocumentaire(stockageBaseDocumentaire);
    optionsEdition.setTypeDocumentStocke(typeDocumentStocke);
    optionsEdition.setIndicatifDocument(indicatifIdDocumentPourBaseDocumentaire);
    optionsEdition.setIndicatifClient(indicatifIdClientPourBaseDocumentaire);
    
    // Appel du service qui va générer l'édition
    String cheminPDF = ManagerServiceDocumentVente.editerDocumentVente(session.getIdSession(), documentVente.getId(),
        documentVente.isModifiable(), dateTraitement, optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(EnumTypeDocument.PDF.getExtension())) {
      throw new MessageErreurException(
          "Vérifier vos affectations d'impression car ce n'est pas un fichier PDF qui a été généré pour le mode d'édition \""
              + pModeEdition.getTexte() + "\".");
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException(
          "Une erreur s'est produite pendant la génération de l'édition pour le mode d'édition \"" + pModeEdition.getTexte() + "\".");
    }
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Prépare un mail avec les données du document de vente.
   */
  private Mail preparerMail(String pEmail, IdTypeMail pTypeMail, String pIndicatifDocument, String pEmailReponse) {
    pEmail = Constantes.normerTexte(pEmail);
    if (pEmail.isEmpty()) {
      throw new MessageErreurException("L'email du destinataire est incorrect.");
    }
    
    Mail mail = new Mail(IdMail.getInstanceAvecCreationId(documentVente.getId().getIdEtablissement()));
    mail.setIdTypeMail(pTypeMail);
    mail.setIndicatifDocument(pIndicatifDocument);
    // Le statut est en cours de praparation car NewSim doit générer la pièce jointe dans un deuxième temps
    mail.setStatut(EnumStatutMail.EN_COURS_PREPARATION);
    
    // Préparation du destinataire
    IdDestinataireMail idDestinataireMail = IdDestinataireMail.getInstanceAvecCreationIdPourTableMail(mail.getId().getIdEtablissement());
    DestinataireMail destinataireMail = new DestinataireMail(idDestinataireMail);
    destinataireMail.setEmail(pEmail);
    ListeDestinataireMail listeDestinataireMail = new ListeDestinataireMail();
    listeDestinataireMail.add(destinataireMail);
    mail.setListeDestinataire(listeDestinataireMail);
    
    // Préparation du destinataire de la réponse
    pEmailReponse = Constantes.normerTexte(pEmailReponse);
    if (!pEmailReponse.isEmpty()) {
      IdDestinataireMail idDestinataireReponseMail = IdDestinataireMail.getInstanceAvecCreationId(mail.getId().getIdEtablissement());
      DestinataireMail destinataireReponseMail = new DestinataireMail(idDestinataireReponseMail);
      destinataireReponseMail.setEmail(pEmailReponse);
      ListeDestinataireMail listeDestinataireReponseMail = new ListeDestinataireMail();
      listeDestinataireReponseMail.add(destinataireReponseMail);
      mail.setListeDestinataireReponse(listeDestinataireReponseMail);
    }
    
    // Création du mail dans la base de données
    IdMail idMail = ManagerServiceTechnique.creerMail(session.getIdSession(), mail);
    // Rechargement du mail en base avec son identifiant
    mail = ManagerServiceTechnique.chargerMail(session.getIdSession(), idMail);
    
    // Construction et sauvegarde de la liste des variables pour le mail
    ListeVariableMail.initialiserVariableMail(session.getIdSession(), mail);
    
    return mail;
  }
  
  /**
   * Ne conserve que les chiffres du numéro de fax.
   */
  private String getNettoyerNumeroFax(String pNumeroFax) {
    if (pNumeroFax == null) {
      return null;
    }
    return pNumeroFax.trim().replaceAll(" ", "").replaceAll("\\.", "").replaceAll("-", "");
  }
  
  /**
   * Retourne une éventuelle adresse email pour une réponse au mail.
   */
  private String recupererAdresseEmailReponse(IdTypeMail pIdTypeMail) {
    // Recherche du type de mail concerné
    CritereTypeMail critereTypeMail = new CritereTypeMail();
    critereTypeMail.setActif(Boolean.TRUE);
    critereTypeMail.setIdTypeMail(pIdTypeMail);
    ListeTypeMail listeTypeMail = ManagerServiceTechnique.chargerListeTypeMail(session.getIdSession(), critereTypeMail);
    if (listeTypeMail == null || listeTypeMail.isEmpty()) {
      throw new MessageErreurException("Aucun type de mail n'a été trouvé pour générer le mail à envoyer.");
    }
    String emailDestinataireReponse = null;
    if (listeTypeMail.size() > 1) {
      Trace.alerte("Plusieurs types de mail ont été trouvé lors de la recherche du destinataire pour la réponse au mail.");
    }
    TypeMail typeMail = listeTypeMail.get(0);
    
    // Récupération de l'adresse email pour la réponse au mail
    EnumTypeDestinataireReponse typeDestinataireReponse = typeMail.getTypeDestinataireReponse();
    if (typeDestinataireReponse == null || typeDestinataireReponse.equals(EnumTypeDestinataireReponse.AUCUNE_ADRESSE)) {
      return null;
    }
    
    switch (typeDestinataireReponse) {
      // Cas du vendeur
      case EMAIL_VENDEUR:
        Vendeur vendeur = listeVendeur.get(documentVente.getIdVendeur());
        if (vendeur == null) {
          throw new MessageErreurException("Le vendeur est invalide alors qu'il est nécessaire pour la construction du mail.");
        }
        if (vendeur.getAdresseEmail() == null || vendeur.getAdresseEmail().isEmpty()) {
          throw new MessageErreurException(
              "L'adresse email du vendeur est invalide alors qu'elle est nécessaire pour la construction du mail.");
        }
        emailDestinataireReponse = vendeur.getAdresseEmail();
        break;
      
      // Les autres cas sont ignorés
      default:
        // Une trace est écrite dans les logs mais le traitement n'est pas bloqué pour autant
        Trace.alerte("Le type du destinataire du mail n'est pas approprié pour le document en cours.");
    }
    
    return emailDestinataireReponse;
  }
  
  // -- Accesseurs
  
  public void setEmailDestinataire(String pEmail) {
    emailDestinataire = pEmail;
  }
  
  public void setNumeroFaxDestinataire(String pNumeroFax) {
    this.numeroFax = pNumeroFax;
  }
}
