/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;

public class JTableLignesChantierDocumentStringRenderer extends DefaultTableCellRenderer {
  private ListeChantier listeChantier;
  
  public JTableLignesChantierDocumentStringRenderer(ListeChantier pListeChantier) {
    listeChantier = pListeChantier;
  }
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Pour les cellules de type label
    JLabel label = (JLabel) c;
    switch (column) {
      case 1:
        label.setHorizontalAlignment(CENTER);
        break;
      case 2:
        label.setHorizontalAlignment(LEFT);
        break;
      case 3:
        label.setHorizontalAlignment(LEFT);
        break;
      case 4:
        label.setHorizontalAlignment(CENTER);
        break;
      case 5:
        label.setHorizontalAlignment(CENTER);
        break;
    }
    if (listeChantier != null && row < listeChantier.size() && !listeChantier.get(row).isActif()) {
      label.setForeground(Color.LIGHT_GRAY);
    }
    else {
      if (isSelected) {
        label.setForeground(Color.WHITE);
      }
      else {
        label.setForeground(Color.BLACK);
      }
    }
    
    TableColumnModel cm = table.getColumnModel();
    // cm.getColumn(0).setResizable(false);
    // cm.getColumn(0).setMinWidth(50);
    // cm.getColumn(0).setMaxWidth(50);
    cm.getColumn(1).setResizable(false);
    cm.getColumn(1).setPreferredWidth(75);
    cm.getColumn(0).setResizable(true);
    cm.getColumn(2).setPreferredWidth(300);
    cm.getColumn(3).setResizable(false);
    cm.getColumn(3).setPreferredWidth(150);
    cm.getColumn(4).setResizable(false);
    cm.getColumn(4).setPreferredWidth(90);
    cm.getColumn(5).setResizable(false);
    cm.getColumn(5).setPreferredWidth(90);
    
    return c;
  }
}
