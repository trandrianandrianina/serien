/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente.recherchearticledocumentvente;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreDirectUsine;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleRechercheArticleDocument extends AbstractModeleDialogue {
  // Variables
  private UtilisateurGescom utilisateurGescom = null;
  private String messageErreur = "";
  private String texteRecherche = null;
  private IdArticle idArticle = null;
  private List<Article> listeResultatRechercheArticles = null;
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheArticle = -1;
  private boolean isFenetreArticleAffichee = false;
  
  /**
   * Constructeur.
   */
  public ModeleRechercheArticleDocument(SessionBase pSession, String pRecherche) {
    super(pSession);
    texteRecherche = pRecherche;
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Utilisateur de l'application
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getEnvUser().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    setFenetreArticleAffichee(true);
    modifierTexteRecherche(texteRecherche);
  }
  
  /*
  * Modifier le texte de recherche.
  * Cela lance automatiquement une recherche article.
  */
  public void modifierTexteRecherche(String pTexteRecherche) {
    // Même si la valeur est identique à la fois précédente, on lance une recherche.
    texteRecherche = Constantes.normerTexte(pTexteRecherche);
    
    rechercherArticlesStandards();
    
  }
  
  /**
   * Efface le contenu du résultat de la recherche articles.
   * Cela efface le contenu du tableau et les données correspondantes : l'index de la première ligne affichée, l'index de la ligne
   * sélectionnée le message d'erreur correspondant à la dernière recherche. Par contre, cela ne doit pas effacer le texte recherché
   * car celui-ci n'est pas lié à la dernière recherche.
   */
  private void viderListeResultatRechercheArticles() {
    listeResultatRechercheArticles = null;
    indexPremiereLigneAffichee = 0;
    indexRechercheArticle = -1;
    messageErreur = "";
  }
  
  /**
   * Lancer la recherche d'articles standards.
   */
  public void rechercherArticlesStandards() {
    // final String NOM_METHODE = "rechercherArticlesStandards";
    
    // Quitter l'onglet article si rien n'est saisi dans la zone de recherche
    if (texteRecherche.isEmpty()) {
      return;
    }
    
    // Vider le résultat de la recherche précédente
    viderListeResultatRechercheArticles();
    
    // Renseigner les critères de recherche
    CritereArticle critereArticle = new CritereArticle();
    critereArticle.setIdEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    critereArticle.setTexteRechercheGenerique(texteRecherche);
    critereArticle.setDirectUsine(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES);
    
    // Afficher les mots effectivement recherchés dans la zone de recherche
    texteRecherche = critereArticle.getTexteRechercheGenerique();
    
    // Afficher le message issu de l'analyse du texte recherché
    messageErreur = critereArticle.getRechercheGenerique().getMessage();
    
    // Contrôler que l'analyse du texte n'a pas retournée d'erreur
    if (critereArticle.getRechercheGenerique().isEnErreur()) {
      rafraichir();
      return;
    }
    
    // Rechercher les identifiants articles correspondants aux critères de recherche
    ListeArticleBase listeArticlesBases = ListeArticleBase.charger(getIdSession(), critereArticle);
    
    // Enlever un caractère au texte recherché si rien n'a été trouvé
    if (listeArticlesBases == null || listeArticlesBases.isEmpty()) {
      if (texteRecherche.length() > RechercheGenerique.LONGUEUR_MINI_TEXTE_RECHERCHE) {
        texteRecherche = texteRecherche.trim().substring(0, texteRecherche.length() - 1);
        messageErreur = "Aucun article n'a été trouvé, essayez avec : " + texteRecherche;
      }
      else {
        messageErreur = "Aucun article n'a été trouvé.";
      }
      
      return;
    }
    
    // Mémoriser le résultat
    listeResultatRechercheArticles = new ArrayList<Article>();
    for (ArticleBase articleBase : listeArticlesBases) {
      Article article = new Article(articleBase.getId());
      article.setLibelleComplet(articleBase.getLibelleComplet());
      
      listeResultatRechercheArticles.add(article);
    }
    
    if (listeResultatRechercheArticles.size() == 1) {
      idArticle = listeResultatRechercheArticles.get(0).getId();
    }
  }
  
  /**
   * Lance la recherche article puis rafraichi l'affichage
   */
  public void initialiserRecherche() {
    texteRecherche = "";
    messageErreur = "";
    if (listeResultatRechercheArticles != null) {
      listeResultatRechercheArticles.clear();
    }
    rafraichir();
  }
  
  /**
   * Lance la recherche article puis rafraichi l'affichage
   */
  public void lancerRecherche() {
    rechercherArticlesStandards();
    rafraichir();
  }
  
  /**
   * Fermer la boîte de dialogue en validant les modifications.
   * Les données modifiées sont conservées.
   */
  @Override
  public void quitterAvecValidation() {
    modifierTexteRecherche(texteRecherche);
    super.quitterAvecValidation();
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public List<Article> getListeResultatRechercheArticles() {
    return listeResultatRechercheArticles;
  }
  
  public void selectionnerArticle(Article pArticle) {
    idArticle = pArticle.getId();
    texteRecherche = pArticle.getLibelleComplet();
    setFenetreArticleAffichee(false);
  }
  
  public String getTexteRecherche() {
    return texteRecherche;
  }
  
  public void setTexteRecherche(String pTexte) {
    this.texteRecherche = pTexte;
  }
  
  private void setFenetreArticleAffichee(boolean b) {
    this.isFenetreArticleAffichee = b;
  }
  
  public String getMessageErreur() {
    return messageErreur;
  }
  
}
