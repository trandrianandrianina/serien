/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue principale de la gestion des achats.
 * 
 * Cette vue comporte plusieurs vues enfants, une poru chaque onglet :
 * - Vue de l'onglet fournisseur.
 * - Vue de l'onglet livraison.
 * - Vue de l'onglet article.
 * - Vue de l'onglet édition.
 */
public class VueAchat extends AbstractVuePanel<ModeleAchat> {
  // Variables
  protected SNBandeauTitre bandeau = null;
  private VueOngletFournisseur vueOngletFournisseur = null;
  private VueOngletLivraison vueOngletLivraison = null;
  private VueOngletArticle vueOngletArticle = null;
  private VueOngletEdition vueOngletEdition = null;
  
  /**
   * Constructeur.
   */
  public VueAchat(ModeleAchat pModele) {
    super(pModele);
    
    // Ajouter l'onglet fournisseur
    vueOngletFournisseur = new VueOngletFournisseur(getModele());
    ajouterVueEnfant(EnumCleVueAchat.ONGLET_FOURNISSEUR, vueOngletFournisseur);
    
    // Ajouter l'onglet livraison
    vueOngletLivraison = new VueOngletLivraison(getModele());
    ajouterVueEnfant(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT, vueOngletLivraison);
    
    // Ajouter l'onglet article
    vueOngletArticle = new VueOngletArticle(getModele());
    ajouterVueEnfant(EnumCleVueAchat.ONGLET_ARTICLE, vueOngletArticle);
    
    // Ajouter l'onglet édition
    vueOngletEdition = new VueOngletEdition(getModele());
    ajouterVueEnfant(EnumCleVueAchat.ONGLET_EDITION, vueOngletEdition);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    tbpGeneral.insertTab("Fournisseur", null, vueOngletFournisseur, "Fournisseur", EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex());
    tbpGeneral.insertTab("Livraison", null, vueOngletLivraison, "Livraison", EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    tbpGeneral.insertTab("Article", null, vueOngletArticle, "Article", EnumCleVueAchat.ONGLET_ARTICLE.getIndex());
    tbpGeneral.insertTab("Edition", null, vueOngletEdition, "Edition", EnumCleVueAchat.ONGLET_EDITION.getIndex());
    
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    initialiserAspectTitre(pnlBpresentation, "");
    
    // Grise ou cache les objets graphiques non accessibles au départ
    for (int tab = EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex(); tab < tbpGeneral.getTabCount(); tab++) {
      tbpGeneral.setEnabledAt(tab, false);
    }
  }
  
  protected void initialiserAspectTitre(SNBandeauTitre pbandeauTitre, String atitre) {
    bandeau = pbandeauTitre;
    bandeau.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    setTitre(atitre);
  }
  
  /**
   * Mise à jour du titre.
   * 
   * @param ptexte
   */
  public void setTitre(String pTexte) {
    bandeau.setText(Constantes.normerTexte(pTexte));
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraichir le bandeau
    if (getModele() != null && getModele().getEtablissement() != null) {
      pnlBpresentation.setIdEtablissement(getModele().getEtablissement().getId());
    }
    setTitre(getModele().getTitre());
    
    tbpGeneral.setEnabledAt(EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex(), getModele().isOngletFournisseurAccessible());
    tbpGeneral.setEnabledAt(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex(), getModele().isOngletLivraisonAccessible());
    tbpGeneral.setEnabledAt(EnumCleVueAchat.ONGLET_ARTICLE.getIndex(), getModele().isOngletArticleAccessible());
    tbpGeneral.setEnabledAt(EnumCleVueAchat.ONGLET_EDITION.getIndex(), getModele().isOngletEditionAccessible());
    
    // Les informations du magasin et la raison sociale
    lbInfosFournisseur.setText(getModele().getInformationsPermanentes());
    
    // Activer l'onglet actif
    tbpGeneral.setSelectedIndex(getModele().getIndiceOngletCourant());
  }
  
  // -- Méthodes évènementielles
  
  private void tbpGeneralStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().validerOnglet(tbpGeneral.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    pnlBpresentation = new SNBandeauTitre();
    pnlSud = new JPanel();
    scpContenu = new JScrollPane();
    pnlContenu = new JPanel();
    pnlInformationsFournisseur = new JPanel();
    lbInfosFournisseur = new JLabel();
    tbpGeneral = new JTabbedPane();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Achat - ");
      pnlBpresentation.setName("pnlBpresentation");
      pnlNord.add(pnlBpresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setPreferredSize(new Dimension(1024, 650));
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ======== scpContenu ========
      {
        scpContenu.setPreferredSize(new Dimension(1024, 650));
        scpContenu.setBorder(null);
        scpContenu.setName("scpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(1024, 650));
          pnlContenu.setBackground(new Color(239, 239, 222));
          pnlContenu.setForeground(Color.black);
          pnlContenu.setMinimumSize(new Dimension(1200, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new BorderLayout(0, -30));
          
          // ======== pnlInformationsFournisseur ========
          {
            pnlInformationsFournisseur.setMinimumSize(new Dimension(235, 30));
            pnlInformationsFournisseur.setPreferredSize(new Dimension(235, 30));
            pnlInformationsFournisseur.setOpaque(false);
            pnlInformationsFournisseur.setName("pnlInformationsFournisseur");
            pnlInformationsFournisseur.setLayout(new FlowLayout(FlowLayout.RIGHT));
            
            // ---- lbInfosFournisseur ----
            lbInfosFournisseur.setText("Informations fournisseur");
            lbInfosFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbInfosFournisseur.setForeground(new Color(125, 125, 94));
            lbInfosFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
            lbInfosFournisseur.setName("lbInfosFournisseur");
            pnlInformationsFournisseur.add(lbInfosFournisseur);
          }
          pnlContenu.add(pnlInformationsFournisseur, BorderLayout.NORTH);
          
          // ======== tbpGeneral ========
          {
            tbpGeneral.setFont(
                tbpGeneral.getFont().deriveFont(tbpGeneral.getFont().getStyle() & ~Font.BOLD, tbpGeneral.getFont().getSize() + 5f));
            tbpGeneral.setBackground(new Color(212, 133, 54));
            tbpGeneral.setPreferredSize(new Dimension(1245, 670));
            tbpGeneral.setMinimumSize(new Dimension(1245, 670));
            tbpGeneral.setFocusCycleRoot(true);
            tbpGeneral.setFocusTraversalPolicyProvider(true);
            tbpGeneral.setName("tbpGeneral");
            tbpGeneral.addChangeListener(new ChangeListener() {
              @Override
              public void stateChanged(ChangeEvent e) {
                tbpGeneralStateChanged(e);
              }
            });
          }
          pnlContenu.add(tbpGeneral, BorderLayout.CENTER);
        }
        scpContenu.setViewportView(pnlContenu);
      }
      pnlSud.add(scpContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre pnlBpresentation;
  private JPanel pnlSud;
  private JScrollPane scpContenu;
  private JPanel pnlContenu;
  private JPanel pnlInformationsFournisseur;
  private JLabel lbInfosFournisseur;
  private JTabbedPane tbpGeneral;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
