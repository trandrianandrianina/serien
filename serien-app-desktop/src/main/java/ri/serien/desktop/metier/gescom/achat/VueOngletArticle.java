/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet article de la gestion des achats.
 */
public class VueOngletArticle extends AbstractVuePanel<ModeleAchat> {
  // Constantes
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_STANDARDS_HT =
      new String[] { "Qt\u00e9", "UCA", "Article", "Dispo (US)", "UCA/UCS", "UCS", "Prix net HT", "Port HT", "UA", "Observations" };
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_COMMENTAIRES =
      new String[] { "Choisir", "Code", "Texte du commentaire" };
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_PALETTES =
      new String[] { "Qt\u00e9", "Libell\u00e9", "Stock", "Qt\u00e9 chez le client", "Prix consignation", "Prix d\u00e9consignation" };
  // Titres de la liste en mode création
  public static final String[] LISTE_TITRE_LIGNES_ACHAT_DOCUMENT =
      new String[] { "Qt\u00e9", "UCA", "Libellé", "Qt\u00e9 en UA", "UA", "Prix net HT", "Montant HT" };
  // Titres de la liste en mode modification
  public static final String[] LISTE_TITRE_LIGNES_ACHAT_DOCUMENT_EN_MODIF =
      new String[] { "Initiale", "Reliquat", "Trait\u00e9e", "UCA", "Libellé", "Qt\u00e9 en UA", "UA", "Prix net HT", "Montant HT" };
  
  private static final String LIBELLE_RECHERCHE_ARTICLE = "Recherche article";
  private static final String LIBELLE_RECHERCHE_PALETTE = "Recherche palette";
  private static final String LIBELLE_RECHERCHE_COMMENTAIRE = "Recherche commentaire";
  
  // Table contenant le résultat de la recherche articles
  private static final int RESULTAT_RECHERCHE_ARTICLES_COLONNE_QUANTITE = 0;
  private static final int RESULTAT_RECHERCHE_ARTICLES_PREMIERE_LIGNE = 0;
  
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  private static final int NOMBRE_DECIMALE_REMISE = 1;
  private static final int NOMBRE_DECIMALE_QUANTITE = 2;
  
  private static final int AUCUNE_INSERTION = -2;
  
  // Boutons
  private static final String BOUTON_RECHERCHER_PALETTE = "Rechercher une palette";
  private static final String BOUTON_AJOUTER_COMMENTAIRE = "Ajouter un commentaire";
  private static final String BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE = "Rechercher un commentaire";
  private static final String BOUTON_RECHERCHER_ARTICLE_STANDARD = "Rechercher un article standard";
  
  // Variables
  private DefaultTableModel tableModelRechercheArticleStandardHT = null;
  private DefaultTableModel tableModelRechercheArticleCommentaire = null;
  private DefaultTableModel tableModelRechercheArticlePalette = null;
  private boolean celluleQuantiteModifiee = false;
  private JTableCellRendererRechercheArticle rechercheArticleRenderer = new JTableCellRendererRechercheArticle();
  private JTableCellRendererRechercheArticleCommentaire rechercheArticleCommentaireRenderer =
      new JTableCellRendererRechercheArticleCommentaire();
  private JTableCellRendererRechercheArticlePalette rechercheArticlePaletteRenderer = new JTableCellRendererRechercheArticlePalette();
  private int indiceLigneInsertion = AUCUNE_INSERTION;
  
  // Classe interne qui permet de gérer les flèches dans la table tblListeRechercheArticles
  class ActionCellule extends AbstractAction {
    // Constantes
    
    // Variables
    private Action actionOriginale = null;
    private boolean gestionFleches = false;
    
    public ActionCellule(String pAction, Action pActionOriginale, boolean pGestionFleches) {
      super(pAction);
      actionOriginale = pActionOriginale;
      gestionFleches = pGestionFleches;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      keySurListeArticles(e, actionOriginale, gestionFleches);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueOngletArticle(ModeleAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Liste
    scpListeRechercheArticles.getViewport().setBackground(Color.WHITE);
    tblListeRechercheArticles.getTableHeader().setFont(new Font("SansSerif", Font.PLAIN, 14));
    tblListeRechercheArticles.getTableHeader().setBackground(Color.WHITE);
    
    // Une seule ligne peut être sélectionnée dans la tableau résultat
    tblListeRechercheArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeRechercheArticles.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeRechercheArticlesSelectionChanged(e);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpListeRechercheArticles.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeRechercheArticlesStateChanged(e);
      }
    });
    
    tableModelRechercheArticleStandardHT = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_STANDARDS_HT) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    tableModelRechercheArticleCommentaire = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_COMMENTAIRES) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    tableModelRechercheArticlePalette = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_PALETTES) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListeRechercheArticles, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter, false);
    tblListeRechercheArticles.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListeRechercheArticles.getActionMap().put(actionEnter, actionCelluleEnter);
    // Modification de la gestion des évènements sur la touche flèche haut de la jtable
    final Action originalActionHaut = retournerActionComposant(tblListeRechercheArticles, SNCharteGraphique.TOUCHE_HAUT);
    String actionHaut = "actionHaut";
    ActionCellule actionCelluleHaut = new ActionCellule(actionHaut, originalActionHaut, true);
    tblListeRechercheArticles.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_HAUT, actionHaut);
    tblListeRechercheArticles.getActionMap().put(actionHaut, actionCelluleHaut);
    // Modification de la gestion des évènements sur la touche flèche bas de la jtable
    final Action originalActionBas = retournerActionComposant(tblListeRechercheArticles, SNCharteGraphique.TOUCHE_BAS);
    String actionBas = "actionBas";
    ActionCellule actionCelluleBas = new ActionCellule(actionBas, originalActionBas, true);
    tblListeRechercheArticles.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_BAS, actionBas);
    tblListeRechercheArticles.getActionMap().put(actionBas, actionCelluleBas);
    // Modification de la gestion des évènements sur la touche flèche gauche de la jtable
    final Action originalActionGauche = retournerActionComposant(tblListeRechercheArticles, SNCharteGraphique.TOUCHE_GAUCHE);
    String actionGauche = "actionGauche";
    ActionCellule actionCelluleGauche = new ActionCellule(actionGauche, originalActionGauche, true);
    tblListeRechercheArticles.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_GAUCHE, actionGauche);
    tblListeRechercheArticles.getActionMap().put(actionGauche, actionCelluleGauche);
    // Modification de la gestion des évènements sur la touche flèche droite de la jtable
    final Action originalActionDroite = retournerActionComposant(tblListeRechercheArticles, SNCharteGraphique.TOUCHE_DROITE);
    String actionDroite = "actionDroite";
    ActionCellule actionCelluleDroite = new ActionCellule(actionDroite, originalActionDroite, true);
    tblListeRechercheArticles.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_DROITE, actionDroite);
    tblListeRechercheArticles.getActionMap().put(actionDroite, actionCelluleDroite);
    
    // Liste des lignes articles du document
    initialiserListeArticlesDocument();
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(BOUTON_AJOUTER_COMMENTAIRE, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE, 'o', false);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_PALETTE, 'p', true);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD, 'r', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Initialise la structure de la liste des articles du document.
   */
  private void initialiserListeArticlesDocument() {
    // Définition de l'aspect de la table
    scpLignesArticlesDocument.getViewport().setBackground(Color.WHITE);
    
    if (getModele().getDocumentAchatEnCours() == null || getModele().getDocumentAchatEnCours().isEnCoursCreation()) {
      tblLignesArticlesDocument.personnaliserAspect(LISTE_TITRE_LIGNES_ACHAT_DOCUMENT, new int[] { 60, 40, 380, 90, 40, 90, 90 },
          new int[] { 60, 40, -1, 90, 40, 90, 90 }, new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE },
          14);
    }
    else {
      tblLignesArticlesDocument.personnaliserAspect(LISTE_TITRE_LIGNES_ACHAT_DOCUMENT_EN_MODIF,
          new int[] { 60, 70, 60, 40, 380, 90, 40, 90, 90 }, new int[] { 60, 70, 60, 40, -1, 90, 40, 90, 90 },
          new int[] { NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE },
          14);
    }
    
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleActionEnter = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        keyEntreeSurLignesDocument(ae);
      }
      
    };
    Action nouvelleActionSuppr = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        keySupprSurLignesDocument();
      }
    };
    tblLignesArticlesDocument.modifierAction(nouvelleActionEnter, SNCharteGraphique.TOUCHE_ENTREE);
    tblLignesArticlesDocument.modifierAction(nouvelleActionSuppr, SNCharteGraphique.TOUCHE_SUPPR);
    
    lbRechercheArticle.setImportanceMessage(EnumImportanceMessage.MOYEN);
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTexteRecherche();
    rafraichirFiltreTousFournisseur();
    rafraichirFiltreHorsGamme();
    rafraichirTitreListeRecherche();
    rafraichirListeRechercheArticles();
    rafraichirListeLignesDocument();
    
    // Rafraichissement du pied du document
    rafraichirTotaux();
    rafraichirMontantMinimum();
    rafraichirMontantManquantMinimumCommande();
    rafraichirMontantManquantMontantFrancoPort();
    rafraichirTotalHT();
    rafraichirTotalTVA();
    rafraichirTotalTTC();
    rafraichirPortHT();
    rafraichirMontantFrancoPort();
    rafraichirBoutonContinuer();
    // Rafraichissement des boutons
    rafraichirBoutonArticleStandard();
    rafraichirBoutonPalette();
    rafraichirBoutonAjouterCommentaire();
    rafraichirBoutonRechercherArticleCommentaire();
    
    // Positionner le focus
    switch (getModele().getComposantAyantLeFocus()) {
      case ModeleAchat.FOCUS_ARTICLES_RECHERCHE_ARTICLE:
        tfTexteRecherche.requestFocus();
        break;
      
      case ModeleAchat.FOCUS_ARTICLES_RESULTAT_RECHERCHE:
        tblListeRechercheArticles.requestFocusInWindow();
        if (getModele().getIndexRechercheArticle() == -1) {
          tblListeRechercheArticles.changeSelection(0, 0, false, false);
        }
        else {
          tblListeRechercheArticles.changeSelection(getModele().getIndexRechercheArticle(), 0, false, false);
        }
        break;
      
      default:
        tfTexteRecherche.requestFocus();
        break;
    }
  }
  
  /**
   * Mettre à jour les données concernant le texte recherché.
   * Champs concernés : le texte recherché, son libellé et le message d'erreur.
   */
  private void rafraichirTexteRecherche() {
    // Masquer si le document n'est pas modifiable
    if (getModele().getDocumentAchatEnCours() == null || !getModele().getDocumentAchatEnCours().isModifiable()) {
      lbRechercheArticle.setVisible(false);
      tfTexteRecherche.setVisible(false);
      return;
    }
    
    // Afficher les zones
    lbRechercheArticle.setVisible(true);
    tfTexteRecherche.setVisible(true);
    
    // Mettre le libellé adapté mode de recherche
    switch (getModele().getEtape()) {
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD:
      case ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_ARTICLE);
        break;
      
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_PALETTE);
        break;
      
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_COMMENTAIRE);
        break;
      
      default:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_ARTICLE);
        break;
    }
    
    // Mettre à jour le texte rechercher et le message d'erreur
    tfTexteRecherche.setText(getModele().getTexteRecherche());
  }
  
  /**
   * Rafraichir le bouton d'ajout de commentaire
   */
  private void rafraichirBoutonArticleStandard() {
    boolean actif = false;
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD) {
      actif = false;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      actif = true;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD, actif);
  }
  
  /**
   * Rafraichir le bouton des palettes
   */
  private void rafraichirBoutonPalette() {
    boolean actif = false;
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      actif = false;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES) {
      actif = true;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_PALETTE, actif);
  }
  
  /**
   * Rafraichir le bouton d'ajout de commentaire
   */
  private void rafraichirBoutonAjouterCommentaire() {
    boolean actif = false;
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES) {
      actif = true;
    }
    
    snBarreBouton.activerBouton(BOUTON_AJOUTER_COMMENTAIRE, actif);
  }
  
  /**
   * Rafraichir le bouton de recherche de commentaire
   */
  private void rafraichirBoutonRechercherArticleCommentaire() {
    boolean actif = false;
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD) {
      actif = true;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      actif = false;
    }
    if (getModele().getEtape() == ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES) {
      actif = true;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE, actif);
  }
  
  /**
   * Met à jour le filtre de recherche pour voir les articles de tous les fournisseurs
   * Champs concernés : boîte à cocher Tous fournisseurs
   */
  private void rafraichirFiltreTousFournisseur() {
    // Masquer si le document n'est pas modifiable
    if (getModele().getDocumentAchatEnCours() == null || !getModele().getDocumentAchatEnCours().isModifiable()) {
      chkFiltreFournisseur.setVisible(false);
      return;
    }
    
    // Afficher ou masquer les éléments suivant le mode de recherche
    switch (getModele().getEtape()) {
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD:
      case ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES:
        chkFiltreFournisseur.setVisible(true);
        break;
      
      default:
        chkFiltreFournisseur.setVisible(false);
        break;
    }
    
    // Mettre à jour le filtre tous fournisseurs
    chkFiltreFournisseur.setSelected(getModele().isFiltreTousFournisseur());
  }
  
  /**
   * Met à jour le filtre de recherche pour les articles hors gammes.
   * Champs concernés : boîte à cocher hors gamme.
   */
  private void rafraichirFiltreHorsGamme() {
    // Masquer si le document n'est pas modifiable
    if (getModele().getDocumentAchatEnCours() == null || !getModele().getDocumentAchatEnCours().isModifiable()) {
      chkFiltreHorsGamme.setVisible(false);
      return;
    }
    // Masquer si l'utilisateur n'est pas autorisé à saisir les articles hors gamme
    else if (!ManagerSessionClient.getInstance().verifierDroitGescom(getModele().getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_SAISIE_ARTICLES_HORS_GAMME)) {
      chkFiltreHorsGamme.setVisible(false);
      return;
    }
    
    // Afficher ou masquer les éléments suivant le mode de recherche
    switch (getModele().getEtape()) {
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD:
      case ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES:
        chkFiltreHorsGamme.setVisible(true);
        break;
      
      default:
        chkFiltreHorsGamme.setVisible(false);
        break;
    }
    
    // Mettre à jour le filtre hors gamme
    chkFiltreHorsGamme.setSelected(getModele().isFiltreHorsGamme());
  }
  
  /**
   * Rafraîchit le tableau d'articles contenant le résultat d'une recherche.
   */
  private void rafraichirTitreListeRecherche() {
    // Masquer si le document n'est pas modifiable
    if (getModele().getDocumentAchatEnCours() == null || !getModele().getDocumentAchatEnCours().isModifiable()) {
      lbResultatRecherche.setVisible(false);
      return;
    }
    
    // Afficher les champs
    lbResultatRecherche.setVisible(true);
    
    // Rafraîchir le titre du tableau
    Message message = getModele().getMessage();
    if (message != null) {
      lbResultatRecherche.setText(message.getTexte());
      if (message.isImportanceHaute()) {
        lbResultatRecherche.setForeground(Color.RED);
      }
      else {
        lbResultatRecherche.setForeground(Color.BLACK);
      }
    }
    else {
      lbResultatRecherche.setText("");
      lbResultatRecherche.setForeground(Color.BLACK);
    }
  }
  
  /**
   * Rafraîchit le tableau d'articles contenant le résultat d'une recherche.
   */
  private void rafraichirListeRechercheArticles() {
    // Masquer si le document n'est pas modifiable
    if (getModele().getDocumentAchatEnCours() != null && !getModele().getDocumentAchatEnCours().isModifiable()) {
      scpListeRechercheArticles.setVisible(false);
      return;
    }
    
    // Afficher les champs
    scpListeRechercheArticles.setVisible(true);
    
    // Effacer les donnes de tous les modèles
    tableModelRechercheArticleStandardHT.setRowCount(0);
    tableModelRechercheArticleCommentaire.setRowCount(0);
    tableModelRechercheArticlePalette.setRowCount(0);
    
    // Rafraîchir le tableau suivant le mode de recherche en cours
    switch (getModele().getEtape()) {
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_STANDARD:
      case ModeleAchat.ETAPE_SAISIE_ONGLET_ARTICLES:
        rafraichirListeRechercheArticlesStandards();
        break;
      
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
        rafraichirListeRechercheArticlesCommentaires();
        break;
      
      case ModeleAchat.ETAPE_RECHERCHE_ARTICLE_PALETTE:
        rafraichirListeRechercheArticlesStandards();
        break;
      
      default:
        rafraichirListeRechercheArticlesStandards();
        break;
    }
    
    // Recalculer la longueur de la liste afin d'avoir l'ascenceur uniquement quand c'est nécessaire
    tblListeRechercheArticles.setPreferredSize(new Dimension(tblListeRechercheArticles.getPreferredSize().width,
        tblListeRechercheArticles.getRowCount() * tblListeRechercheArticles.getRowHeight()));
    
    // Remettre la ligne sélectionné avant le rafraichissement
    int index = getModele().getIndexRechercheArticle();
    if (index > -1 && index < tblListeRechercheArticles.getRowCount()) {
      tblListeRechercheArticles.getSelectionModel().addSelectionInterval(index, index);
    }
  }
  
  /**
   * Affichage spécial de la recherche d'articles palettes
   */
  private void rafraichirListeRechercheArticlesPalettes() {
    ListeArticle listeArticles = null;
    if (getModele().getListeResultatRechercheArticles() != null) {
      listeArticles = getModele().getListeResultatRechercheArticles();
    }
    
    String[][] donnees = null;
    if (listeArticles != null) {
      donnees = new String[listeArticles.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_PALETTES.length];
      for (int i = 0; i < listeArticles.size(); i++) {
        Article article = listeArticles.get(i);
        String[] ligne = donnees[i];
        
        if (i == 0 && getModele().getNombreDePalettesLies().compareTo(BigDecimal.ZERO) > 0
            && getModele().getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
          ligne[0] = Constantes.formater(getModele().getNombreDePalettesLies(), false);
        }
        else {
          ligne[0] = null;
        }
        ligne[1] = article.getLibelleComplet();
        ligne[2] = Constantes.formater(article.getQuantitePhysique(), false);
        ligne[3] = Constantes.formater(article.getQuantiteStockClient(), false);
        ligne[4] = Constantes.formater(article.getPrixConsignation(), true);
        ligne[5] = Constantes.formater(article.getPrixDeconsignation(), true);
      }
    }
    
    // Mettre à jour les données
    tableModelRechercheArticlePalette.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelRechercheArticlePalette.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblListeRechercheArticles.getModel().equals(tableModelRechercheArticlePalette)) {
      tblListeRechercheArticles.setModel(tableModelRechercheArticlePalette);
      tblListeRechercheArticles.setGridColor(new Color(204, 204, 204));
      tblListeRechercheArticles.setDefaultRenderer(Object.class, rechercheArticlePaletteRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeRechercheArticles.getRowCount() == 0) {
      rechercheArticlePaletteRenderer.redimensionnerColonnes(tblListeRechercheArticles);
    }
  }
  
  /**
   * Met à jour le résultat de la recherche articles
   */
  private void rafraichirListeRechercheArticlesStandards() {
    // Convertir les données à afficher dans le format attendu par le tableau
    ListeArticle listeArticles = getModele().getListeResultatRechercheArticles();
    String[][] donnees = null;
    if (listeArticles != null) {
      donnees = new String[listeArticles.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_STANDARDS_HT.length];
      for (int i = 0; i < listeArticles.size(); i++) {
        Article article = listeArticles.get(i);
        String[] ligne = donnees[i];
        
        // La première colonne est réservée pour la saisie
        ligne[0] = null;
        
        // Unité de commande d'achat
        if (article.getPrixAchat() != null && article.getPrixAchat().getIdUCA() != null) {
          ligne[1] = article.getPrixAchat().getIdUCA().getCode();
        }
        else {
          ligne[1] = null;
        }
        
        // Libellé
        if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
          ligne[2] = article.getId().getCodeArticle() + "| " + article.getLibelleComplet();
        }
        else {
          ligne[2] = article.getLibelleComplet();
        }
        
        // Quantité disponible en stock (US)
        if (article.getPrixAchat() != null) {
          ligne[3] = Constantes.formater(article.getQuantiteDisponibleAchat(), false);
        }
        else {
          ligne[3] = null;
        }
        
        // Nombre d'unité de commande d'achat par unité de conditionnement de stock
        if (article.getPrixAchat() != null) {
          ligne[4] = Constantes.formater(article.getPrixAchat().getNombreUCAParUCS(), false);
        }
        else {
          ligne[4] = null;
        }
        
        // Unité de commande de conditionnement de stock
        if (article.getPrixAchat() != null && article.getIdUCS() != null) {
          ligne[5] = article.getIdUCS().getCode();
        }
        else {
          ligne[5] = null;
        }
        
        // Prix d'achat net HT (sans port)
        if (article.getPrixAchat() != null) {
          ligne[6] = Constantes.formater(article.getPrixAchat().getPrixAchatNetHT(), true);
        }
        else {
          ligne[6] = null;
        }
        
        // Montant du port fournisseur HT
        if (article.getPrixAchat() != null) {
          ligne[7] = Constantes.formater(article.getPrixAchat().getMontantPortHT(), true);
        }
        else {
          ligne[7] = null;
        }
        
        // Unité d'achat
        if (article.getPrixAchat() != null && article.getPrixAchat().getIdUA() != null) {
          ligne[8] = article.getPrixAchat().getIdUA().getCode();
        }
        else {
          ligne[8] = null;
        }
        
        // Prix flash ou Observation courte de l'article si pas de prix flash
        if (article.getPrixFlash() != null) {
          ligne[9] = String.format("<html>%s<span style='color:#8A084B'>(Prix flash)</span></html>", article.getObservationCourte());
        }
        else {
          ligne[9] = article.getObservationCourte();
        }
      }
    }
    
    // Sélectionner le modèle de la table en fonction du HT/TTC
    DefaultTableModel tableModel = tableModelRechercheArticleStandardHT;
    if (tableModel != null) {
      // Mettre à jour les données
      tableModel.setRowCount(0);
      if (donnees != null) {
        for (String[] ligne : donnees) {
          tableModel.addRow(ligne);
        }
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (tblListeRechercheArticles.getModel() != tableModel) {
      tblListeRechercheArticles.setModel(tableModel);
      tblListeRechercheArticles.setGridColor(new Color(204, 204, 204));
      tblListeRechercheArticles.setDefaultRenderer(Object.class, rechercheArticleRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeRechercheArticles.getRowCount() == 0) {
      rechercheArticleRenderer.redimensionnerColonnes(tblListeRechercheArticles);
    }
  }
  
  /**
   * Affichage spécial de la recherche d'articles commentaires
   */
  private void rafraichirListeRechercheArticlesCommentaires() {
    // Convertir les données à afficher dans le format attendu par le tableau
    ListeArticle listeArticles = getModele().getListeResultatRechercheArticles();
    String[][] donnees = null;
    if (listeArticles != null) {
      donnees = new String[listeArticles.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLES_COMMENTAIRES.length];
      for (int i = 0; i < listeArticles.size(); i++) {
        Article article = listeArticles.get(i);
        String[] ligne = donnees[i];
        
        ligne[0] = null;
        ligne[1] = article.getId().getCodeArticle();
        ligne[2] = article.getLibelleComplet();
      }
    }
    
    // Mettre à jour les données
    tableModelRechercheArticleCommentaire.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelRechercheArticleCommentaire.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (tblListeRechercheArticles.getModel() != tableModelRechercheArticleCommentaire) {
      tblListeRechercheArticles.setModel(tableModelRechercheArticleCommentaire);
      tblListeRechercheArticles.setGridColor(new Color(204, 204, 204));
      tblListeRechercheArticles.setDefaultRenderer(Object.class, rechercheArticleCommentaireRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblListeRechercheArticles.getRowCount() == 0) {
      rechercheArticleCommentaireRenderer.redimensionnerColonnes(tblListeRechercheArticles);
    }
  }
  
  /**
   * Met à jour les lignes du bon
   */
  private void rafraichirListeLignesDocument() {
    // Rafraichissement des titres des colonnes an fonction du mode en cours
    if (getModele().getDocumentAchatEnCours() != null) {
      // Document en cours de création mais le nombre de colonnes n'est pas le bon
      if (getModele().getDocumentAchatEnCours().isEnCoursCreation()
          && tblLignesArticlesDocument.getColumnCount() != LISTE_TITRE_LIGNES_ACHAT_DOCUMENT.length) {
        initialiserListeArticlesDocument();
      }
      // Document en cours de modification mais le nombre de colonnes n'est pas le bon
      else if (!getModele().getDocumentAchatEnCours().isEnCoursCreation()
          && tblLignesArticlesDocument.getColumnCount() != LISTE_TITRE_LIGNES_ACHAT_DOCUMENT_EN_MODIF.length) {
        initialiserListeArticlesDocument();
      }
    }
    
    // Récupèration des données de la liste
    tblLignesArticlesDocument.mettreAJourDonnees(getModele().formaterListeArticlesDocument(tblLignesArticlesDocument.getColumnCount()));
    
    // Sélection des lignes articles
    if (getModele().getListeLigneAchatASelectionner().isEmpty()) {
      tblLignesArticlesDocument.clearSelection();
    }
    else {
      ArrayList<IdLigneAchat> liste = getModele().getListeLigneAchatASelectionner();
      int debut = getModele().getDocumentAchatEnCours().retournerIndexLigneAchatAvecId(liste.get(0));
      int fin = getModele().getDocumentAchatEnCours().retournerIndexLigneAchatAvecId(liste.get(liste.size() - 1));
      if (debut > -1 && fin > -1) {
        tblLignesArticlesDocument.setRowSelectionInterval(debut, fin);
      }
      getModele().getListeLigneAchatASelectionner().clear();
    }
  }
  
  /**
   * Rafaichir le poids total et le volume total.
   */
  private void rafraichirTotaux() {
    // Poour l'instant le poids est caché si la commande a été réceptionnée car le poids a été perdu (il faudra à terme faire quelque
    // chose)
    
    DocumentAchat documentAchat = getModele().getDocumentAchatEnCours();
    if (documentAchat != null && documentAchat.getPoids() != null && !documentAchat.isCommandeReceptionnee()) {
      pnlTotaux.setVisible(true);
      zsPoidsTotal.setText(Constantes.formater(getModele().getDocumentAchatEnCours().getPoids(), false));
      zsVolume.setText(Constantes.formater(getModele().getDocumentAchatEnCours().getVolume(), false));
    }
    else {
      pnlTotaux.setVisible(false);
      zsPoidsTotal.setText("");
      zsVolume.setText("");
    }
  }
  
  private void rafraichirTotalHT() {
    if (getModele().getDocumentAchatEnCours() != null) {
      zsTotalHT.setText(Constantes.formater(getModele().getDocumentAchatEnCours().getTotalHTLignesSansPort(), true));
    }
    else {
      zsTotalHT.setText("");
    }
  }
  
  private void rafraichirTotalTVA() {
    if (getModele().getDocumentAchatEnCours() != null && getModele().getDocumentAchatEnCours().getListeTva() != null) {
      zsTotalTVA.setText(Constantes.formater(getModele().getDocumentAchatEnCours().getListeTva().getTotalTVA(), true));
    }
    else {
      zsTotalTVA.setText("");
    }
  }
  
  private void rafraichirTotalTTC() {
    if (getModele().getDocumentAchatEnCours() != null) {
      zsTotalTTC.setText(Constantes.formater(getModele().getDocumentAchatEnCours().getTotalTTC(), true));
    }
    else {
      zsTotalTTC.setText("");
    }
  }
  
  private void rafraichirBoutonContinuer() {
    boolean actif = getModele().isValidationOngletArticlesPossible();
    snBarreBouton.activerBouton(EnumBouton.CONTINUER, actif);
  }
  
  private void rafraichirPortHT() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    if (documentAchatEnCours == null || fournisseur == null) {
      zsPortHT.setText("");
      pnlPort.setVisible(false);
      return;
    }
    pnlPort.setVisible(true);
    pnlApplicationPort.setVisible(false);
    pnlIndicationPort.setVisible(false);
    zsPortHT.setText("0,00");
    chkFrancoPort.setSelected(false);
    
    // En mode enlèvement
    if (documentAchatEnCours.isModeEnlevement()) {
      // Si le port théorique vaut 0 alors on n'affiche pas le texte sur le port indicatif
      if (getModele().isFraisPortTheoriqueValide()) {
        pnlIndicationPort.setVisible(true);
        lbMontantPortIndicatif.setText(Constantes.formater(documentAchatEnCours.getMontantPortTheorique(), true));
      }
    }
    // En mode livraison
    else {
      // Si le fournisseur facture les frais de port
      if (fournisseur.isFactureFraisPort()) {
        // Si le port facturé n'est pas valide alors on n'affiche rien
        if (!getModele().isFraisPortFactureValide()) {
          return;
        }
        pnlApplicationPort.setVisible(true);
        // Si l'utilisateur décide de facturer les frais de port (la case franco est décochée)
        if (documentAchatEnCours.isFraisPortFournisseurFacture()) {
          zsPortHT.setText(Constantes.formater(documentAchatEnCours.getMontantPortFacture(), true));
          chkFrancoPort.setText("Franco de port");
        }
        // Si l'utilisateur décide de ne pas facturer les frais de port
        else {
          chkFrancoPort.setSelected(true);
          chkFrancoPort.setText("Franco de port (" + Constantes.formater(documentAchatEnCours.getMontantPortTheorique(), true) + ")");
        }
      }
      // Si le fournisseur ne facture pas les frais de port
      else {
        // Si le port théorique vaut 0 alors on n'affiche pas le texte sur le port indicatif
        if (getModele().isFraisPortTheoriqueValide()) {
          pnlIndicationPort.setVisible(true);
          lbMontantPortIndicatif.setText(Constantes.formater(documentAchatEnCours.getMontantPortTheorique(), true));
        }
      }
    }
  }
  
  private void rafraichirMontantMinimum() {
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    if (fournisseur != null && fournisseur.getMinimumCommande() != null) {
      zsMontantMinimum.setText(Constantes.formater(fournisseur.getMinimumCommande(), true));
    }
    else {
      zsMontantMinimum.setText("");
    }
  }
  
  private void rafraichirMontantFrancoPort() {
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    if (fournisseur != null && fournisseur.getMontantFrancoPort() != null) {
      zsMontantFranco.setText(Constantes.formater(fournisseur.getMontantFrancoPort(), true));
    }
    else {
      zsMontantFranco.setText("");
    }
  }
  
  private void rafraichirMontantManquantMinimumCommande() {
    BigDecimal montantManquant = getModele().getMontantManquantPourAtteindreMontantMinimumCommande();
    if (montantManquant == null) {
      montantManquant = BigDecimal.ZERO;
    }
    zsManqueCmdMini.setText(Constantes.formater(montantManquant, true));
  }
  
  private void rafraichirMontantManquantMontantFrancoPort() {
    BigDecimal montantManquant = getModele().getMontantManquantPourAtteindreFrancoPort();
    if (montantManquant == null) {
      montantManquant = BigDecimal.ZERO;
    }
    zsManqueFrancoTotal.setText(Constantes.formater(montantManquant, true));
  }
  
  /**
   * Récupère la ligne article sélectionné.
   */
  private void validerListeLigneArticleSelection() {
    if (tblLignesArticlesDocument.getModel().getRowCount() <= 0) {
      return;
    }
    int indexvisuel = tblLignesArticlesDocument.getSelectedRow();
    
    if ((indexvisuel < 0) || (indexvisuel >= getModele().getDocumentAchatEnCours().getListeLigneAchat().size())) {
      return;
    }
    
    // Il s'agit d'une ligne commentaire d'un regroupement
    if (getModele().getDocumentAchatEnCours().getListeLigneAchat().get(indexvisuel).isLigneCommentaire()) {
      getModele().modifierLigneCommentaire(indexvisuel);
    }
    else if (getModele().getDocumentAchatEnCours().getListeLigneAchat().get(indexvisuel).isLigneRegroupee()) {
      return;
    }
    // S'il s'agit d'un article "normal"
    else {
      getModele().modifierLigneArticle(indexvisuel);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  // -- Méthodes privées
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  /**
   * Limite la saisie de quantité à un seul article dans la liste de résultats article
   */
  private void limiterNombreQuantiteSelectionnee() {
    if ((tblListeRechercheArticles != null) && (tblListeRechercheArticles.getSelectedRow() > -1)
        && (tblListeRechercheArticles.getSelectedRow() < tblListeRechercheArticles.getRowCount())) {
      int indiceLigneEnCours = tblListeRechercheArticles.getSelectedRow();
      String quantiteSaisie = (String) tblListeRechercheArticles.getModel().getValueAt(indiceLigneEnCours, 0);
      if (quantiteSaisie != null) {
        for (int i = 0; i < tblListeRechercheArticles.getModel().getRowCount(); i++) {
          if (i != indiceLigneEnCours) {
            tblListeRechercheArticles.getModel().setValueAt("", i, 0);
          }
        }
      }
    }
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        getModele().validerOngletArticles();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().annulerDocument();
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD)) {
        getModele().activerModeRechercheArticleStandard();
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_PALETTE)) {
        getModele().activerModeRechercheArticlePalette();
      }
      else if (pSNBouton.isBouton(BOUTON_AJOUTER_COMMENTAIRE)) {
        int indexvisuel = tblLignesArticlesDocument.getSelectedRow();
        getModele().creerLigneCommentaire(indexvisuel);
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE)) {
        getModele().activerModeRechercheArticleCommentaire();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeRechercheArticlesSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      int index = tblListeRechercheArticles.getSelectedRow();
      getModele().setIndexRechercheArticle(index);
      
      // Mettre le focus sur la première colonne pour permettre une saisie directement sur cette colonne
      if (index != -1) {
        tblListeRechercheArticles.setColumnSelectionInterval(0, 0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void keyEntreeSurLignesDocument(ActionEvent ae) {
    try {
      // Si le document n'est pas modifiable on sort
      if (!getModele().getDocumentAchatEnCours().isModifiable()) {
        return;
      }
      validerListeLigneArticleSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void keySupprSurLignesDocument() {
    try {
      // Si le document n'est pas modifiable on sort
      if (!getModele().getDocumentAchatEnCours().isModifiable()) {
        return;
      }
      getModele().supprimerLignesArticlesAvecIndice(tblLignesArticlesDocument.getSelectedRows());
      rafraichir();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'appui sur les touches flêches et la touche entrée dans la tableau affichant le résultat de la recherche articles.
   */
  private void keySurListeArticles(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSeletionnee = tblListeRechercheArticles.getSelectedRow();
      if (indexLigneSeletionnee == -1) {
        return;
      }
      if (tblListeRechercheArticles.editCellAt(indexLigneSeletionnee, 0)) {
        // Lire la valeur sur la ligne sélectionnée avant de déclencher le traitement de l'action par le tableau car celui-ci efface
        // la valeur saisie dans certains cas (déplacement en fin de tableau).
        String valeur = (String) tblListeRechercheArticles.getValueAt(indexLigneSeletionnee, 0);
        
        // Déclencher le traitement de l'action par le tableau
        actionOriginale.actionPerformed(e);
        
        // Ne rien faire si la cellule est vide et qu'on se déplace avec les flêches
        if (gestionFleches) {
          if (valeur == null || valeur.trim().isEmpty() || valeur.trim().equals("0")) {
            return;
          }
        }
        
        // Informer le modèle qu'une valeur a été saisie pour un article
        if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
          getModele().ajouterArticleCommentaire(indexLigneSeletionnee, tblLignesArticlesDocument.getSelectedRow());
        }
        else {
          getModele().modifierQuantiteLigneRechercheArticles(indexLigneSeletionnee, valeur, tblLignesArticlesDocument.getSelectedRow());
        }
      }
      else {
        if (!gestionFleches) {
          getModele().effacerResultatRechercheArticles();
        }
        else {
          actionOriginale.actionPerformed(e);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRechercheArticleActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierTexteRecherche(tfTexteRecherche.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpListeRechercheArticlesStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeRechercheArticles == null) {
        return;
      }
      Trace.debug(VueOngletArticle.class, "scpListeRechercheArticlesStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeRechercheArticles.getViewport().getViewRect();
      int premiereLigne = tblListeRechercheArticles.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeRechercheArticles.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeRechercheArticles.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageArticlesAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLignesArticlesBonMouseClicked(MouseEvent e) {
    try {
      // Traitement du double clic
      if (e.getClickCount() == 2) {
        // Informer le modèle
        int index = tblLignesArticlesDocument.getSelectedRow();
        getModele().setIndexRechercheArticle(index);
        validerListeLigneArticleSelection();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeRechercheArticlesMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        // Informer le modèle
        int index = tblListeRechercheArticles.getSelectedRow();
        getModele().setIndexRechercheArticle(index);
        if (getModele().getEtape() == ModeleAchat.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
          getModele().ajouterArticleCommentaire(index, tblLignesArticlesDocument.getSelectedRow());
        }
        else {
          getModele().modifierQuantiteLigneRechercheArticles(tblListeRechercheArticles.getSelectedRow(), "d",
              tblLignesArticlesDocument.getSelectedRow());
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkFiltreHorsGammeActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFiltreHorsGamme(chkFiltreHorsGamme.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLignesArticlesDocumentComponentResized(ComponentEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (tblLignesArticlesDocument.getRowCount() == 0 || indiceLigneInsertion == AUCUNE_INSERTION) {
        return;
      }
      
      // On positionne la scrollebar de la table de façon à toujours voir la dernière ligne insérée
      if (indiceLigneInsertion == -1) {
        indiceLigneInsertion = tblLignesArticlesDocument.getRowCount() - 1;
      }
      tblLignesArticlesDocument.scrollRectToVisible(tblLignesArticlesDocument.getCellRect(indiceLigneInsertion, 0, true));
      indiceLigneInsertion = AUCUNE_INSERTION;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btRechercherArticleActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().activerModeRechercheArticleStandard();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeRechercheArticlesPropertyChange(PropertyChangeEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      limiterNombreQuantiteSelectionnee();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkFrancoPortItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        // Si c'est coché alors on ne facture pas les frais de port
        getModele().modifierFraisPortFacture(!chkFrancoPort.isSelected());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkFiltreFournisseurActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFiltreFournisseur(chkFiltreFournisseur.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlRechercheArticle = new SNPanel();
    lbRechercheArticle = new SNLabelChamp();
    tfTexteRecherche = new JTextField();
    pnlRechercheArticleDroite = new JPanel();
    chkFiltreFournisseur = new JCheckBox();
    chkFiltreHorsGamme = new JCheckBox();
    lbResultatRecherche = new SNLabelTitre();
    scpListeRechercheArticles = new JScrollPane();
    tblListeRechercheArticles = new JTable();
    lbArticlesDuDocument = new SNLabelTitre();
    scpLignesArticlesDocument = new JScrollPane();
    tblLignesArticlesDocument = new NRiTable();
    pnlPiedDocument = new JPanel();
    pnlTotaux = new JPanel();
    lbPoidsTotal = new JLabel();
    zsPoidsTotal = new RiZoneSortie();
    lbUnitePoids = new JLabel();
    lbVolume = new JLabel();
    zsVolume = new RiZoneSortie();
    lbUniteVolume = new JLabel();
    pnlSeuil = new JPanel();
    lbMontantMinimum = new JLabel();
    zsMontantMinimum = new RiZoneSortie();
    lbManqueCmdMini = new JLabel();
    zsManqueCmdMini = new RiZoneSortie();
    lbMontantFranco = new JLabel();
    zsMontantFranco = new RiZoneSortie();
    lbManqueFrancoTotal = new JLabel();
    zsManqueFrancoTotal = new RiZoneSortie();
    pnlTotal = new JPanel();
    lbTotalHT = new SNLabelChamp();
    zsTotalHT = new RiZoneSortie();
    lbPortHT = new SNLabelChamp();
    zsPortHT = new RiZoneSortie();
    lbTotalTVA = new SNLabelChamp();
    zsTotalTVA = new RiZoneSortie();
    lbTotalTTC = new SNLabelChamp();
    zsTotalTTC = new RiZoneSortie();
    pnlPort = new JPanel();
    pnlApplicationPort = new JPanel();
    chkFrancoPort = new JCheckBox();
    pnlIndicationPort = new JPanel();
    lbIndicationPort = new JLabel();
    lbMontantPortIndicatif = new JLabel();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setMinimumSize(new Dimension(1240, 650));
      pnlContenu.setPreferredSize(new Dimension(1240, 650));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 35, 0, 110, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.5, 0.0, 0.5, 0.0, 1.0E-4 };
      
      // ======== pnlRechercheArticle ========
      {
        pnlRechercheArticle.setName("pnlRechercheArticle");
        pnlRechercheArticle.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRechercheArticle.getLayout()).columnWidths = new int[] { 205, 0, 0, 0 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbRechercheArticle ----
        lbRechercheArticle.setText("Recherche article");
        lbRechercheArticle.setFont(new Font("sansserif", Font.BOLD, 14));
        lbRechercheArticle.setMaximumSize(new Dimension(300, 30));
        lbRechercheArticle.setMinimumSize(new Dimension(300, 30));
        lbRechercheArticle.setPreferredSize(new Dimension(300, 30));
        lbRechercheArticle.setHorizontalAlignment(SwingConstants.LEADING);
        lbRechercheArticle.setName("lbRechercheArticle");
        pnlRechercheArticle.add(lbRechercheArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfTexteRecherche ----
        tfTexteRecherche.setMinimumSize(new Dimension(500, 30));
        tfTexteRecherche.setPreferredSize(new Dimension(500, 30));
        tfTexteRecherche.setFont(new Font("sansserif", Font.BOLD, 14));
        tfTexteRecherche.setName("tfTexteRecherche");
        tfTexteRecherche.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            tfRechercheArticleActionPerformed(e);
          }
        });
        pnlRechercheArticle.add(tfTexteRecherche, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlRechercheArticleDroite ========
        {
          pnlRechercheArticleDroite.setOpaque(false);
          pnlRechercheArticleDroite.setName("pnlRechercheArticleDroite");
          pnlRechercheArticleDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRechercheArticleDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlRechercheArticleDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlRechercheArticleDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlRechercheArticleDroite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- chkFiltreFournisseur ----
          chkFiltreFournisseur.setText("Articles tous fournisseurs");
          chkFiltreFournisseur.setMaximumSize(new Dimension(180, 30));
          chkFiltreFournisseur.setMinimumSize(new Dimension(150, 30));
          chkFiltreFournisseur.setPreferredSize(new Dimension(180, 30));
          chkFiltreFournisseur.setFont(chkFiltreFournisseur.getFont().deriveFont(chkFiltreFournisseur.getFont().getSize() + 2f));
          chkFiltreFournisseur.setName("chkFiltreFournisseur");
          chkFiltreFournisseur.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkFiltreFournisseurActionPerformed(e);
            }
          });
          pnlRechercheArticleDroite.add(chkFiltreFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- chkFiltreHorsGamme ----
          chkFiltreHorsGamme.setText("Articles hors gamme");
          chkFiltreHorsGamme.setFont(chkFiltreHorsGamme.getFont().deriveFont(chkFiltreHorsGamme.getFont().getSize() + 2f));
          chkFiltreHorsGamme.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          chkFiltreHorsGamme.setPreferredSize(new Dimension(150, 30));
          chkFiltreHorsGamme.setMaximumSize(new Dimension(150, 30));
          chkFiltreHorsGamme.setMinimumSize(new Dimension(150, 30));
          chkFiltreHorsGamme.setName("chkFiltreHorsGamme");
          chkFiltreHorsGamme.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              chkFiltreHorsGammeActionPerformed(e);
            }
          });
          pnlRechercheArticleDroite.add(chkFiltreHorsGamme, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlRechercheArticle.add(pnlRechercheArticleDroite, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlRechercheArticle,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbResultatRecherche ----
      lbResultatRecherche.setText("R\u00e9sultat de la recherche");
      lbResultatRecherche.setMinimumSize(new Dimension(150, 30));
      lbResultatRecherche.setMaximumSize(new Dimension(150, 30));
      lbResultatRecherche.setPreferredSize(new Dimension(150, 30));
      lbResultatRecherche.setName("lbResultatRecherche");
      pnlContenu.add(lbResultatRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeRechercheArticles ========
      {
        scpListeRechercheArticles.setPreferredSize(new Dimension(600, 100));
        scpListeRechercheArticles.setMinimumSize(new Dimension(600, 100));
        scpListeRechercheArticles.setBackground(Color.white);
        scpListeRechercheArticles.setOpaque(true);
        scpListeRechercheArticles.setName("scpListeRechercheArticles");
        
        // ---- tblListeRechercheArticles ----
        tblListeRechercheArticles.setShowVerticalLines(true);
        tblListeRechercheArticles.setShowHorizontalLines(true);
        tblListeRechercheArticles.setBackground(Color.white);
        tblListeRechercheArticles.setOpaque(false);
        tblListeRechercheArticles.setRowHeight(20);
        tblListeRechercheArticles.setGridColor(new Color(204, 204, 204));
        tblListeRechercheArticles.setMinimumSize(new Dimension(600, 100));
        tblListeRechercheArticles.setPreferredSize(new Dimension(600, 100));
        tblListeRechercheArticles.setName("tblListeRechercheArticles");
        tblListeRechercheArticles.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblListeRechercheArticlesMouseClicked(e);
          }
        });
        tblListeRechercheArticles.addPropertyChangeListener(new PropertyChangeListener() {
          @Override
          public void propertyChange(PropertyChangeEvent e) {
            tblListeRechercheArticlesPropertyChange(e);
          }
        });
        scpListeRechercheArticles.setViewportView(tblListeRechercheArticles);
      }
      pnlContenu.add(scpListeRechercheArticles,
          new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbArticlesDuDocument ----
      lbArticlesDuDocument.setText("Lignes du document");
      lbArticlesDuDocument.setPreferredSize(new Dimension(150, 30));
      lbArticlesDuDocument.setMinimumSize(new Dimension(150, 30));
      lbArticlesDuDocument.setMaximumSize(new Dimension(150, 30));
      lbArticlesDuDocument.setName("lbArticlesDuDocument");
      pnlContenu.add(lbArticlesDuDocument, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpLignesArticlesDocument ========
      {
        scpLignesArticlesDocument.setMinimumSize(new Dimension(600, 100));
        scpLignesArticlesDocument.setPreferredSize(new Dimension(600, 100));
        scpLignesArticlesDocument.setBackground(Color.white);
        scpLignesArticlesDocument.setOpaque(true);
        scpLignesArticlesDocument.setName("scrpLignesArticlesDocument");
        
        // ---- tblLignesArticlesDocument ----
        tblLignesArticlesDocument.setShowVerticalLines(true);
        tblLignesArticlesDocument.setShowHorizontalLines(true);
        tblLignesArticlesDocument.setBackground(Color.white);
        tblLignesArticlesDocument.setRowHeight(20);
        tblLignesArticlesDocument.setGridColor(new Color(204, 204, 204));
        tblLignesArticlesDocument.setOpaque(false);
        tblLignesArticlesDocument.setMinimumSize(new Dimension(600, 100));
        tblLignesArticlesDocument.setPreferredSize(new Dimension(600, 100));
        tblLignesArticlesDocument.setName("tblLignesArticlesDocument");
        tblLignesArticlesDocument.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblLignesArticlesBonMouseClicked(e);
          }
        });
        tblLignesArticlesDocument.addComponentListener(new ComponentAdapter() {
          @Override
          public void componentResized(ComponentEvent e) {
            tblLignesArticlesDocumentComponentResized(e);
          }
        });
        scpLignesArticlesDocument.setViewportView(tblLignesArticlesDocument);
      }
      pnlContenu.add(scpLignesArticlesDocument,
          new GridBagConstraints(0, 4, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlPiedDocument ========
      {
        pnlPiedDocument.setOpaque(false);
        pnlPiedDocument.setBorder(null);
        pnlPiedDocument.setName("pnlPiedDocument");
        pnlPiedDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPiedDocument.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlPiedDocument.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlPiedDocument.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlPiedDocument.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setBorder(new TitledBorder(null, "Totaux", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          pnlTotaux.setOpaque(false);
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPoidsTotal ----
          lbPoidsTotal.setText("Poids");
          lbPoidsTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPoidsTotal.setPreferredSize(new Dimension(50, 19));
          lbPoidsTotal.setMinimumSize(new Dimension(50, 19));
          lbPoidsTotal.setMaximumSize(new Dimension(50, 19));
          lbPoidsTotal.setName("lbPoidsTotal");
          pnlTotaux.add(lbPoidsTotal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsPoidsTotal ----
          zsPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          zsPoidsTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsPoidsTotal.setMinimumSize(new Dimension(80, 30));
          zsPoidsTotal.setMaximumSize(new Dimension(80, 30));
          zsPoidsTotal.setPreferredSize(new Dimension(80, 30));
          zsPoidsTotal.setName("zsPoidsTotal");
          pnlTotaux.add(zsPoidsTotal, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbUnitePoids ----
          lbUnitePoids.setText("Kg");
          lbUnitePoids.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbUnitePoids.setHorizontalAlignment(SwingConstants.LEFT);
          lbUnitePoids.setMinimumSize(new Dimension(20, 30));
          lbUnitePoids.setPreferredSize(new Dimension(20, 30));
          lbUnitePoids.setHorizontalTextPosition(SwingConstants.LEFT);
          lbUnitePoids.setMaximumSize(new Dimension(20, 30));
          lbUnitePoids.setName("lbUnitePoids");
          pnlTotaux.add(lbUnitePoids, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbVolume ----
          lbVolume.setText("Volume");
          lbVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbVolume.setHorizontalAlignment(SwingConstants.RIGHT);
          lbVolume.setPreferredSize(new Dimension(50, 19));
          lbVolume.setMinimumSize(new Dimension(50, 19));
          lbVolume.setMaximumSize(new Dimension(50, 19));
          lbVolume.setName("lbVolume");
          pnlTotaux.add(lbVolume, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsVolume ----
          zsVolume.setHorizontalAlignment(SwingConstants.RIGHT);
          zsVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsVolume.setPreferredSize(new Dimension(80, 30));
          zsVolume.setMinimumSize(new Dimension(80, 30));
          zsVolume.setMaximumSize(new Dimension(80, 30));
          zsVolume.setName("zsVolume");
          pnlTotaux.add(zsVolume, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteVolume ----
          lbUniteVolume.setText("m\u00b3");
          lbUniteVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbUniteVolume.setHorizontalAlignment(SwingConstants.LEFT);
          lbUniteVolume.setMaximumSize(new Dimension(20, 30));
          lbUniteVolume.setMinimumSize(new Dimension(20, 30));
          lbUniteVolume.setPreferredSize(new Dimension(20, 30));
          lbUniteVolume.setName("lbUniteVolume");
          pnlTotaux.add(lbUniteVolume, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPiedDocument.add(pnlTotaux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlSeuil ========
        {
          pnlSeuil.setOpaque(false);
          pnlSeuil.setBorder(
              new TitledBorder(null, "Seuil", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlSeuil.setName("pnlSeuil");
          pnlSeuil.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlSeuil.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlSeuil.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlSeuil.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlSeuil.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbMontantMinimum ----
          lbMontantMinimum.setText("Montant mini");
          lbMontantMinimum.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbMontantMinimum.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMontantMinimum.setName("lbMontantMinimum");
          pnlSeuil.add(lbMontantMinimum, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsMontantMinimum ----
          zsMontantMinimum.setHorizontalAlignment(SwingConstants.RIGHT);
          zsMontantMinimum.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsMontantMinimum.setMinimumSize(new Dimension(80, 30));
          zsMontantMinimum.setMaximumSize(new Dimension(80, 30));
          zsMontantMinimum.setPreferredSize(new Dimension(80, 30));
          zsMontantMinimum.setName("zsMontantMinimum");
          pnlSeuil.add(zsMontantMinimum, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbManqueCmdMini ----
          lbManqueCmdMini.setText("Manque");
          lbManqueCmdMini.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbManqueCmdMini.setHorizontalAlignment(SwingConstants.RIGHT);
          lbManqueCmdMini.setPreferredSize(new Dimension(51, 19));
          lbManqueCmdMini.setMinimumSize(new Dimension(51, 19));
          lbManqueCmdMini.setName("lbManqueCmdMini");
          pnlSeuil.add(lbManqueCmdMini, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsManqueCmdMini ----
          zsManqueCmdMini.setHorizontalAlignment(SwingConstants.RIGHT);
          zsManqueCmdMini.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsManqueCmdMini.setMinimumSize(new Dimension(80, 30));
          zsManqueCmdMini.setMaximumSize(new Dimension(80, 30));
          zsManqueCmdMini.setPreferredSize(new Dimension(80, 30));
          zsManqueCmdMini.setName("zsManqueCmdMini");
          pnlSeuil.add(zsManqueCmdMini, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMontantFranco ----
          lbMontantFranco.setText("Montant franco");
          lbMontantFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbMontantFranco.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMontantFranco.setName("lbMontantFranco");
          pnlSeuil.add(lbMontantFranco, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsMontantFranco ----
          zsMontantFranco.setHorizontalAlignment(SwingConstants.RIGHT);
          zsMontantFranco.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsMontantFranco.setMinimumSize(new Dimension(80, 30));
          zsMontantFranco.setMaximumSize(new Dimension(80, 30));
          zsMontantFranco.setPreferredSize(new Dimension(80, 30));
          zsMontantFranco.setName("zsMontantFranco");
          pnlSeuil.add(zsMontantFranco, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbManqueFrancoTotal ----
          lbManqueFrancoTotal.setText("Manque");
          lbManqueFrancoTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbManqueFrancoTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbManqueFrancoTotal.setMaximumSize(new Dimension(60, 19));
          lbManqueFrancoTotal.setMinimumSize(new Dimension(60, 19));
          lbManqueFrancoTotal.setPreferredSize(new Dimension(60, 19));
          lbManqueFrancoTotal.setName("lbManqueFrancoTotal");
          pnlSeuil.add(lbManqueFrancoTotal, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsManqueFrancoTotal ----
          zsManqueFrancoTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          zsManqueFrancoTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsManqueFrancoTotal.setMinimumSize(new Dimension(80, 30));
          zsManqueFrancoTotal.setMaximumSize(new Dimension(80, 30));
          zsManqueFrancoTotal.setPreferredSize(new Dimension(80, 30));
          zsManqueFrancoTotal.setName("zsManqueFrancoTotal");
          pnlSeuil.add(zsManqueFrancoTotal, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPiedDocument.add(pnlSeuil, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlTotal ========
        {
          pnlTotal.setBorder(
              new TitledBorder(null, "Total", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
          pnlTotal.setOpaque(false);
          pnlTotal.setName("pnlTotal");
          pnlTotal.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotal.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlTotal.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTotal.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbTotalHT ----
          lbTotalHT.setText("Total HT");
          lbTotalHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalHT.setMaximumSize(new Dimension(60, 30));
          lbTotalHT.setMinimumSize(new Dimension(60, 30));
          lbTotalHT.setPreferredSize(new Dimension(60, 30));
          lbTotalHT.setName("lbTotalHT");
          pnlTotal.add(lbTotalHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsTotalHT ----
          zsTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsTotalHT.setMinimumSize(new Dimension(100, 30));
          zsTotalHT.setPreferredSize(new Dimension(100, 30));
          zsTotalHT.setMaximumSize(new Dimension(100, 30));
          zsTotalHT.setName("zsTotalHT");
          pnlTotal.add(zsTotalHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPortHT ----
          lbPortHT.setText("Port HT");
          lbPortHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPortHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPortHT.setMaximumSize(new Dimension(60, 30));
          lbPortHT.setMinimumSize(new Dimension(60, 30));
          lbPortHT.setPreferredSize(new Dimension(60, 30));
          lbPortHT.setName("lbPortHT");
          pnlTotal.add(lbPortHT, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsPortHT ----
          zsPortHT.setHorizontalAlignment(SwingConstants.RIGHT);
          zsPortHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsPortHT.setMinimumSize(new Dimension(80, 30));
          zsPortHT.setMaximumSize(new Dimension(80, 30));
          zsPortHT.setPreferredSize(new Dimension(80, 30));
          zsPortHT.setName("zsPortHT");
          pnlTotal.add(zsPortHT, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalTVA ----
          lbTotalTVA.setText("TVA");
          lbTotalTVA.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTVA.setPreferredSize(new Dimension(60, 30));
          lbTotalTVA.setMinimumSize(new Dimension(60, 30));
          lbTotalTVA.setMaximumSize(new Dimension(60, 30));
          lbTotalTVA.setName("lbTotalTVA");
          pnlTotal.add(lbTotalTVA, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsTotalTVA ----
          zsTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalTVA.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsTotalTVA.setMinimumSize(new Dimension(80, 30));
          zsTotalTVA.setPreferredSize(new Dimension(80, 30));
          zsTotalTVA.setMaximumSize(new Dimension(80, 30));
          zsTotalTVA.setName("zsTotalTVA");
          pnlTotal.add(zsTotalTVA, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTotalTTC ----
          lbTotalTTC.setText("Total TTC");
          lbTotalTTC.setFont(new Font("sansserif", Font.BOLD, 14));
          lbTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTTC.setMaximumSize(new Dimension(70, 30));
          lbTotalTTC.setMinimumSize(new Dimension(70, 30));
          lbTotalTTC.setPreferredSize(new Dimension(70, 30));
          lbTotalTTC.setName("lbTotalTTC");
          pnlTotal.add(lbTotalTTC, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- zsTotalTTC ----
          zsTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalTTC.setFont(new Font("sansserif", Font.BOLD, 14));
          zsTotalTTC.setMinimumSize(new Dimension(100, 30));
          zsTotalTTC.setPreferredSize(new Dimension(100, 30));
          zsTotalTTC.setMaximumSize(new Dimension(100, 30));
          zsTotalTTC.setName("zsTotalTTC");
          pnlTotal.add(zsTotalTTC, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlPort ========
          {
            pnlPort.setOpaque(false);
            pnlPort.setName("pnlPort");
            pnlPort.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPort.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlApplicationPort ========
            {
              pnlApplicationPort.setOpaque(false);
              pnlApplicationPort.setName("pnlApplicationPort");
              pnlApplicationPort.setLayout(new FlowLayout(FlowLayout.LEFT));
              
              // ---- chkFrancoPort ----
              chkFrancoPort.setText("Franco de port");
              chkFrancoPort.setFont(chkFrancoPort.getFont().deriveFont(chkFrancoPort.getFont().getSize() + 2f));
              chkFrancoPort.setName("ckAppliquerPort");
              chkFrancoPort.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              chkFrancoPort.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  chkFrancoPortItemStateChanged(e);
                }
              });
              pnlApplicationPort.add(chkFrancoPort);
            }
            pnlPort.add(pnlApplicationPort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlIndicationPort ========
            {
              pnlIndicationPort.setOpaque(false);
              pnlIndicationPort.setName("pnlIndicationPort");
              pnlIndicationPort.setLayout(new FlowLayout(FlowLayout.LEFT));
              
              // ---- lbIndicationPort ----
              lbIndicationPort.setText("Port indicatif : ");
              lbIndicationPort.setFont(lbIndicationPort.getFont().deriveFont(lbIndicationPort.getFont().getSize() + 2f));
              lbIndicationPort.setName("lbMontantPortHT");
              pnlIndicationPort.add(lbIndicationPort);
              
              // ---- lbMontantPortIndicatif ----
              lbMontantPortIndicatif.setText("XXXX");
              lbMontantPortIndicatif
                  .setFont(lbMontantPortIndicatif.getFont().deriveFont(lbMontantPortIndicatif.getFont().getSize() + 2f));
              lbMontantPortIndicatif.setName("lbParentheseFermantePort");
              pnlIndicationPort.add(lbMontantPortIndicatif);
            }
            pnlPort.add(pnlIndicationPort, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlTotal.add(pnlPort, new GridBagConstraints(0, 1, 8, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlPiedDocument.add(pnlTotal, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlPiedDocument,
          new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private SNPanel pnlRechercheArticle;
  private SNLabelChamp lbRechercheArticle;
  private JTextField tfTexteRecherche;
  private JPanel pnlRechercheArticleDroite;
  private JCheckBox chkFiltreFournisseur;
  private JCheckBox chkFiltreHorsGamme;
  private SNLabelTitre lbResultatRecherche;
  private JScrollPane scpListeRechercheArticles;
  private JTable tblListeRechercheArticles;
  private SNLabelTitre lbArticlesDuDocument;
  private JScrollPane scpLignesArticlesDocument;
  private NRiTable tblLignesArticlesDocument;
  private JPanel pnlPiedDocument;
  private JPanel pnlTotaux;
  private JLabel lbPoidsTotal;
  private RiZoneSortie zsPoidsTotal;
  private JLabel lbUnitePoids;
  private JLabel lbVolume;
  private RiZoneSortie zsVolume;
  private JLabel lbUniteVolume;
  private JPanel pnlSeuil;
  private JLabel lbMontantMinimum;
  private RiZoneSortie zsMontantMinimum;
  private JLabel lbManqueCmdMini;
  private RiZoneSortie zsManqueCmdMini;
  private JLabel lbMontantFranco;
  private RiZoneSortie zsMontantFranco;
  private JLabel lbManqueFrancoTotal;
  private RiZoneSortie zsManqueFrancoTotal;
  private JPanel pnlTotal;
  private SNLabelChamp lbTotalHT;
  private RiZoneSortie zsTotalHT;
  private SNLabelChamp lbPortHT;
  private RiZoneSortie zsPortHT;
  private SNLabelChamp lbTotalTVA;
  private RiZoneSortie zsTotalTVA;
  private SNLabelChamp lbTotalTTC;
  private RiZoneSortie zsTotalTTC;
  private JPanel pnlPort;
  private JPanel pnlApplicationPort;
  private JCheckBox chkFrancoPort;
  private JPanel pnlIndicationPort;
  private JLabel lbIndicationPort;
  private JLabel lbMontantPortIndicatif;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
