/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisonnementcommandeclient;

import java.util.ArrayList;
import java.util.List;

/**
 * Cette classe contient une liste de lignes à traiter.
 */
public class ListeLigneATraiter extends ArrayList<LigneATraiter> {
  
  /**
   * Génère une liste de ligen à afficher.
   * 
   * @return
   */
  public List<LigneATraiter> getListeLigneAAfficher() {
    List<LigneATraiter> liste = new ArrayList<LigneATraiter>();
    for (LigneATraiter ligne : this) {
      if (ligne.isLigneCommentaire()) {
        continue;
      }
      liste.add(ligne);
    }
    return liste;
  }
  
  /**
   * Retourne le nombre de lignes à afficher.
   * Ce sont les lignes non commentaires.
   * 
   * @return
   */
  public int getNombreLigneAAfficher() {
    int nombre = 0;
    for (LigneATraiter ligne : this) {
      if (!ligne.isLigneCommentaire()) {
        nombre++;
      }
    }
    return nombre;
  }
  
}
