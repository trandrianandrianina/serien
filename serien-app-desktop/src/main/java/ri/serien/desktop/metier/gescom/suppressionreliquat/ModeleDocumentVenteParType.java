/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.suppressionreliquat;

import java.util.List;

import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * L'ensemble des documents de ventes qui ont subi une extraction partielle.
 */
public class ModeleDocumentVenteParType {
  // Variables
  private IdSession idSession = null;
  private ListeDocumentVenteBase listeDevis = null;
  private ListeDocumentVenteBase listeCommande = null;
  private int indexDevisSelectionne = -1;
  private int indexCommandeSelectionnee = -1;
  private ListeClient listeClient = null;
  
  /**
   * Constructeur.
   */
  public ModeleDocumentVenteParType(IdSession pIdSession) {
    idSession = pIdSession;
  }
  
  // -- Méthodes publiques
  
  /**
   * Effacer les listes.
   */
  public void initialiser() {
    // Initialiser la liste des devis
    listeDevis = null;
    
    // Initialiser la liste des Commandes
    listeCommande = null;
  }
  
  /**
   * Charge l'ensemble des id documents du client avec les critères de base.
   */
  public void charger(IdSession pIdSession, IdClient pIdClient, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdVendeur pIdVendeur, int pNumeroDocument) {
    // Effacer le résultat de la recherche précédente
    initialiser();
    
    // Vérifier les pré-requis
    if (pIdEtablissement == null) {
      throw new MessageErreurException("Impossible de charger la liste des documents de ventes car l'établissement est invalide.");
    }
    if (pIdMagasin == null) {
      throw new MessageErreurException("Impossible de charger la liste des documents de ventes car le magasin est invalide.");
    }
    
    // Renseigner les critères de recherche
    CritereDocumentVente critereDocumentVente = new CritereDocumentVente();
    critereDocumentVente.setIdClient(pIdClient);
    critereDocumentVente.setIdEtablissement(pIdEtablissement);
    critereDocumentVente.setIdMagasin(pIdMagasin);
    critereDocumentVente.setSansHistorique(true);
    critereDocumentVente.setNumeroDocument(pNumeroDocument);
    critereDocumentVente.setIdVendeur(pIdVendeur);
    critereDocumentVente.setExtractionPartielle(true);
    
    // Charger les devis
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.DEVIS);
    List<IdDocumentVente> listeIdDevis = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeDevis = ListeDocumentVenteBase.creerListeNonChargee(listeIdDevis);
    ListeDocumentVenteBase listeDocumentVenteBaseCharge = listeDevis.chargerPremierePage(idSession, 0);
    chargerComplement(pIdSession, listeDocumentVenteBaseCharge);
    
    // Charger les commandes
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.COMMANDE);
    List<IdDocumentVente> listeIdCommande = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeCommande = ListeDocumentVenteBase.creerListeNonChargee(listeIdCommande);
    listeDocumentVenteBaseCharge = listeCommande.chargerPremierePage(idSession, 0);
    chargerComplement(pIdSession, listeDocumentVenteBaseCharge);
  }
  
  /**
   * Modifier la plage de devis affichés.
   */
  public void modifierPlageDevis(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeDevis != null) {
      listeDevis.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier la plage de commandes affichéss.
   */
  public void modifierPlageCommande(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeCommande != null) {
      listeCommande.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Charger les informations complémentaires d'une liste de document de ventes.
   */
  private void chargerComplement(IdSession pIdSession, ListeDocumentVenteBase listeDocumentVenteBaseCharge) {
    if (listeDocumentVenteBaseCharge == null) {
      return;
    }
    
    // Lister les identifiants des clients à charger
    ListeIdClient listeIdClient = new ListeIdClient();
    for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
      IdClient idClient = documentVenteBase.getIdClientFacture();
      if (idClient != null && !listeIdClient.contains(idClient)) {
        listeIdClient.add(documentVenteBase.getIdClientFacture());
      }
    }
    
    // Charger les clients en complément
    if (listeIdClient.size() > 0) {
      ListeClientBase listeClientBase = ManagerServiceClient.chargerListeClientBase(pIdSession, listeIdClient);
      
      // Mettre le client dans le complément du document de ventes
      for (DocumentVenteBase documentVenteBase : listeDocumentVenteBaseCharge) {
        ClientBase clientBase = listeClientBase.getClientBaseParId(documentVenteBase.getIdClientFacture());
        documentVenteBase.setComplement(clientBase);
      }
    }
  }
  
  // -- Accesseurs
  
  /**
   * Liste de devis.
   */
  public ListeDocumentVenteBase getListeDevis() {
    return listeDevis;
  }
  
  /**
   * Liste de commandes.
   */
  public ListeDocumentVenteBase getListeCommande() {
    return listeCommande;
  }
  
  /**
   * Retourne un devis à partir de son index dans la liste.
   */
  public DocumentVenteBase getDevis(int pIndex) {
    if (listeDevis == null || pIndex < 0 || pIndex > listeDevis.size()) {
      return null;
    }
    return listeDevis.get(pIndex);
  }
  
  /**
   * Retourne une commande à partir de son index dans la liste.
   */
  public DocumentVenteBase getCommande(int pIndex) {
    if (listeCommande == null || pIndex < 0 || pIndex > listeCommande.size()) {
      return null;
    }
    return listeCommande.get(pIndex);
  }
  
  /**
   * Index du devis sélectionné dans la liste des documents de ventes du client.
   */
  public int getIndexDevisSelectionne() {
    return indexDevisSelectionne;
  }
  
  /**
   * Modifier le devis sélectionné dans la liste des documents clients.
   */
  public void setIndexDevisSelectionne(int pLigneSelectionnee) {
    indexDevisSelectionne = pLigneSelectionnee;
    indexCommandeSelectionnee = -1;
  }
  
  /**
   * Index de la commande sélectionnée dans la liste des documents de ventes du client.
   */
  public int getIndexCommandeSelectionnee() {
    return indexCommandeSelectionnee;
  }
  
  /**
   * Modifier la commande sélectionnée dans la liste des documents clients.
   */
  public void setIndexCommandeSelectionnee(int pLigneSelectionnee) {
    indexDevisSelectionne = -1;
    indexCommandeSelectionnee = pLigneSelectionnee;
  }
  
  /**
   * Indique si un document de ventes est sélectionné dans la liste des documents de ventes (null si aucun).
   */
  public boolean isDocumentVenteSelectionne() {
    return indexDevisSelectionne != -1 || indexCommandeSelectionnee != -1;
  }
  
  /**
   * Identifiant du document de ventes sélectionné dans la liste des documents de ventes (null si aucun).
   */
  public IdDocumentVente getIdDocumentVenteSelectionne() {
    if (listeDevis != null && indexDevisSelectionne > -1 && indexDevisSelectionne < listeDevis.size()) {
      return listeDevis.get(indexDevisSelectionne).getId();
    }
    else if (listeCommande != null && indexCommandeSelectionnee > -1 && indexCommandeSelectionnee < listeCommande.size()) {
      return listeCommande.get(indexCommandeSelectionnee).getId();
    }
    return null;
  }
  
  /**
   * Document de ventes sélectionné dans la liste des documents de ventes (null si aucun).
   */
  public DocumentVenteBase getDocumentVenteBaseSelectionne() {
    if (listeDevis != null && indexDevisSelectionne > -1 && indexDevisSelectionne < listeDevis.size()) {
      return listeDevis.get(indexDevisSelectionne);
    }
    else if (listeCommande != null && indexCommandeSelectionnee > -1 && indexCommandeSelectionnee < listeCommande.size()) {
      return listeCommande.get(indexCommandeSelectionnee);
    }
    return null;
  }
}
