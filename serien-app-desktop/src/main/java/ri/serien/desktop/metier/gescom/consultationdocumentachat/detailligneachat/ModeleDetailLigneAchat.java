/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat.detailligneachat;

import ri.serien.desktop.metier.gescom.consultationdocumentachat.ModeleConsultationDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleDetailLigneAchat extends AbstractModeleDialogue {
  // Constantes
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  
  // Variables
  private ModeleConsultationDocumentAchat modeleParent = null;
  private LigneAchatArticle ligneAchat = null;
  private Article article = null;
  private ListeUnite listeUnite = null;
  private Fournisseur fournisseur = null;
  private ListeFamille listeFamilles = null;
  // Liens documents
  private LienLigne lienOrigine = null;
  private DocumentAchat documentAchat = null;
  
  // Variables de travail
  private String nomFamille = "";
  private String nomFournisseur = "";
  
  /**
   * Constructeur.
   */
  public ModeleDetailLigneAchat(SessionBase pSession, ModeleConsultationDocumentAchat modeleConsultationDocumentsDetail,
      LigneAchatArticle pLigne) {
    super(pSession);
    modeleParent = modeleConsultationDocumentsDetail;
    ligneAchat = pLigne;
    documentAchat = modeleParent.getDocumentAchatDetail();
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    chargerVariables();
  }
  
  /**
   * Retourne la précision d'une unité donnée
   */
  public int retournerPrecisionUnite(IdUnite pId) {
    if (listeUnite != null && pId != null) {
      return listeUnite.getPrecisionUnite(pId);
    }
    return 0;
  }
  
  // -- Méthodes privées
  /**
   * Initialise les variables du modèle.
   */
  private void chargerVariables() {
    // Récupération de la liste des familles
    listeFamilles = new ListeFamille();
    listeFamilles = listeFamilles.charger(getIdSession(), modeleParent.getDocumentAchatDetail().getId().getIdEtablissement());
    
    // liste des unités
    listeUnite = modeleParent.getListeUniteDetail();
    
    // Fournisseur
    if (fournisseur == null) {
      fournisseur = modeleParent.getFournisseurDetail();
    }
    
    // Article
    article = lireDonneesArticle(article);
    if (article.getIdFamille() != null) {
      nomFamille = listeFamilles.retournerLibelle(article.getIdFamille());
    }
    
    if (!documentAchat.isCharge()) {
      documentAchat = ManagerServiceDocumentAchat.chargerDocumentAchat(getIdSession(), documentAchat.getId(), documentAchat, true);
    }
    if (documentAchat.isFacture()) {
      lienOrigine = new LienLigne(IdLienLigne.getInstanceAchat(documentAchat.getId().getIdEtablissement(),
          EnumTypeLienLigne.LIEN_LIGNE_ACHAT, EnumCodeEnteteDocumentAchat.FACTURE.getCode(),
          ligneAchat.getId().getIdDocumentAchat().getNumero(), 0, ligneAchat.getId().getIdDocumentAchat().getNumero(),
          ligneAchat.getId().getNumeroLigne(), ligneAchat.getId().getIdDocumentAchat(), ligneAchat.getId().getNumeroLigne()));
    }
    else {
      lienOrigine = new LienLigne(IdLienLigne.getInstanceAchat(documentAchat.getId().getIdEtablissement(),
          EnumTypeLienLigne.LIEN_LIGNE_ACHAT, documentAchat.getId().getCodeEntete().getCode(),
          ligneAchat.getId().getIdDocumentAchat().getNumero(), ligneAchat.getId().getIdDocumentAchat().getSuffixe(), 0,
          ligneAchat.getId().getNumeroLigne(), ligneAchat.getId().getIdDocumentAchat(), ligneAchat.getId().getNumeroLigne()));
    }
    
    lienOrigine.setDocumentAchat(documentAchat);
    lienOrigine.setLigne(ligneAchat);
  }
  
  /**
   * Retourne les données "complètes" d'un article.
   * Equivalent de la recherche article + lecture de l'article.
   */
  private Article lireDonneesArticle(Article pArticle) {
    if (pArticle == null) {
      article = new Article(ligneAchat.getIdArticle());
    }
    else {
      article = pArticle;
    }
    
    return ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
  }
  
  /**
   * Initialise les variables du modèle.
   */
  private void effacerVariables() {
    article = null;
    nomFamille = "";
    ligneAchat = null;
    fournisseur = null;
  }
  
  public boolean isUniteConditionnementIdentique() {
    return (Constantes.equals(ligneAchat.getPrixAchat().getIdUCA(), ligneAchat.getPrixAchat().getIdUA())
        || ligneAchat.getPrixAchat().getIdUA() == null);
  }
  
  // -- Accesseurs
  
  public LigneAchatArticle getLigneAchatArticle() {
    return ligneAchat;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public Fournisseur getFournisseur() {
    return fournisseur;
  }
  
  public String getNomFamille() {
    return nomFamille;
  }
  
  public LienLigne getLienOrigine() {
    return lienOrigine;
  }
  
  public DocumentAchat getDocumentAchat() {
    return documentAchat;
  }
}
