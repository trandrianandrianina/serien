/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.prodevis;

import java.util.List;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.prodevis.CriteresRechercheProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleImportationDevisProDevis extends AbstractModeleDialogue {
  // Constantes
  
  // Variables
  private ModeleComptoir modeleParent = null;
  private Client client = null;
  private CriteresRechercheProDevis criteres = null;
  private List<DescriptionProDevis> listeDescriptions = null;
  private IdDocumentVente idCommandeProDevis = null;
  private DescriptionProDevis devisImporte = null;
  
  /**
   * Constructeur.
   */
  public ModeleImportationDevisProDevis(SessionBase pSession, ModeleComptoir pModeleParent, Client pClient) {
    super(pSession);
    modeleParent = pModeleParent;
    client = pClient;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    criteres = new CriteresRechercheProDevis();
    criteres.setIdEtablissement(modeleParent.getEtablissement().getId());
    try {
      listeDescriptions = ManagerServiceDocumentVente.chargerListeDescriptionProDevis(getIdSession(), criteres);
    }
    catch (MessageErreurException e) {
      quitterAvecAnnulation();
      throw (e);
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    controleValiditeSaisie();
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Importation du prodevis sélectionné.
   */
  public void importerDevisSelectionne(int pIndex) {
    if ((pIndex < 0) || (pIndex > listeDescriptions.size())) {
      return;
    }
    
    devisImporte = listeDescriptions.get(pIndex);
    if (devisImporte == null) {
      return;
    }
    boolean reussite = true;
    try {
      idCommandeProDevis = ManagerServiceDocumentVente.importerDevisProDevis(getIdSession(), devisImporte, client.getId());
    }
    catch (MessageErreurException e) {
      reussite = false;
      throw e;
    }
    
    if (idCommandeProDevis == null) {
      reussite = false;
      throw new MessageErreurException("Une erreur est survenue lors de l'importation du devis Pro-Devis dans Série N.");
    }
    
    // Affichage d'un message récapitulatif si tout c'est bien passé
    if (reussite) {
      devisImporte.setIdDocumentImporte(idCommandeProDevis);
      DialogueInformation.afficher("Les données contenues dans le fichier " + devisImporte.getNomFichier() + " du devis Pro-Devis n° "
          + devisImporte.getNumero() + " pour le client " + devisImporte.getNomClient() + " ont été importées avec succés");
      quitterAvecValidation();
    }
  }
  
  /**
   * supprime le fichier sélectionné dans l'IFS
   */
  public void supprimerFichier(int pIndiceDocument) {
    if (listeDescriptions == null || pIndiceDocument < 0) {
      return;
    }
    
    DescriptionProDevis proDevisSelectionne = listeDescriptions.get(pIndiceDocument);
    
    if (proDevisSelectionne == null) {
      return;
    }
    
    if (isConfirmerDemande("Demande de suppression d'un fichier d'import",
        "<html><center>Vous vous apprêtez à supprimer le fichier d'import pro-devis " + proDevisSelectionne.getNomFichier()
            + " de votre disque.\nVeuillez confirmer votre demande.</center></html>")) {
      ManagerServiceDocumentVente.modifierStatutProDevis(getIdSession(), proDevisSelectionne, EnumStatutProDevis.SUPPRIME);
      // On recharge la liste pour visualiser la suppression
      listeDescriptions = ManagerServiceDocumentVente.chargerListeDescriptionProDevis(getIdSession(), criteres);
    }
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Affiche une boite de dialogue pour une demande de confirmation.
   */
  private boolean isConfirmerDemande(String pTitre, String pMessage) {
    pTitre = Constantes.normerTexte(pTitre);
    pMessage = Constantes.normerTexte(pMessage);
    ModeleDialogueConfirmation modele = new ModeleDialogueConfirmation(getSession(), pTitre, pMessage);
    DialogueConfirmation vue = new DialogueConfirmation(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }
  
  /**
   * Controle la validité des informations saisies.
   */
  private void controleValiditeSaisie() {
    // S'il y a un message à afficher
    // if (message.length() > 0) {
    // message.insert(0, "Afin de pouvoir valider votre saisie, merci de renseigner :");
    // throw new MessageUtilisateurException(message.toString());
    // }
  }
  
  // -- Accesseurs
  
  public List<DescriptionProDevis> getListeDescriptions() {
    return listeDescriptions;
  }
  
  public IdDocumentVente getIdCommandeProDevis() {
    return idCommandeProDevis;
  }
  
  public DescriptionProDevis getDevisImporte() {
    return devisImporte;
  }
  
}
