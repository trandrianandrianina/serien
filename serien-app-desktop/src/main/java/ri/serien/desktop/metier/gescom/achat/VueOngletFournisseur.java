/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;

import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet fournisseur de la gestion des achats.
 */
public class VueOngletFournisseur extends AbstractVuePanel<ModeleAchat> {
  // Constantes
  private static final String BOUTON_CREER_COMMANDE = "Créer commande";
  private static final String BOUTON_CREER_COMMANDE_DIRECT_USINE = "Créer commande\ndirect usine";
  private static final String BOUTON_APPROVISIONNEMENT_STOCK = "Approvisionner stock";
  private static final String BOUTON_APPROVISIONNEMENT_HORS_STOCK_CLIENTS = "Approvisionner hors-stock clients";
  private static final String BOUTON_APPROVISIONNEMENT_COMMANDES_CLIENTS = "Approvisionner clients";
  private static final String BOUTON_MODIFIER_DOCUMENT = "Modifier document";
  private static final String[] TITRE_LISTE_BON = new String[] { "Num\u00e9ro", "Date", "Date traitement", "Montant HT",
      "R\u00e9f\u00e9rence interne", "Adresse fournisseur", "Acheteur", "Etat" };
  
  // Onglets des documents fournisseur
  private static final int ONGLET_COMMANDES = 0;
  
  /**
   * Constructeur.
   */
  public VueOngletFournisseur(ModeleAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Initialise l'aspect de la table des devis
    scpCommandeEnCours.getViewport().setBackground(Color.WHITE);
    tblCommandeEnCours.personnaliserAspect(TITRE_LISTE_BON, new int[] { 70, 90, 100, 100, 200, 200, 70, 80 },
        new int[] { 70, 90, 100, 100, -1, 150, 70, 80 }, new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE,
            NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE },
        14);
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_DOCUMENT, 'm', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_COMMANDE, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_COMMANDE_DIRECT_USINE, 'u', false);
    snBarreBouton.ajouterBouton(BOUTON_APPROVISIONNEMENT_STOCK, 's', false);
    snBarreBouton.ajouterBouton(BOUTON_APPROVISIONNEMENT_HORS_STOCK_CLIENTS, 'h', false);
    snBarreBouton.ajouterBouton(BOUTON_APPROVISIONNEMENT_COMMANDES_CLIENTS, 'o', false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snAdresseFournisseur.setContexteSaisieDocumentAchat(true);
    snAdresseFournisseur.initialiserRecherche();
  }
  
  @Override
  public void rafraichir() {
    try {
      rafraichirMessageErreur();
      rafraichirRechercheFournisseur();
      rafraichirListeEtablissement();
      rafraichirListeMagasin();
      
      rafraichirAcheteur();
      rafraichirFournisseur();
      rafraichirObservationsFournisseur();
      rafraichirListeCommandesEnCours();
      rafraichirMessageInfo();
      rafraichirBoutonCreerCommande();
      rafraichirBoutonModifierCommande();
      rafraichirBoutonDirectUsine();
      rafraichirBoutonStock();
      rafraichirBoutonHorsStock();
      rafraichirApproCommande();
      rafraichirBoutonAnnuler();
      rafraichirBoutonQuitter();
      
      // Activation du focus
      snAdresseFournisseur.requestFocus();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir le message d'erreur.
   */
  private void rafraichirMessageErreur() {
    lbMessageErreur.setText(getModele().getMessageErreur());
  }
  
  /**
   * Rafraichir la recherche fournisseur.
   */
  private void rafraichirRechercheFournisseur() {
    snAdresseFournisseur.setEnabled(getModele().isDonneesChargees());
    snAdresseFournisseur.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snAdresseFournisseur.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snAdresseFournisseur.setIdEtablissement(null);
    }
    if (getModele().getIdAdresseFournisseurCourant() == null) {
      snAdresseFournisseur.initialiserRecherche();
    }
    snAdresseFournisseur.charger(false, false);
  }
  
  /**
   * Rafraichir la liste des établissements.
   */
  private void rafraichirListeEtablissement() {
    snEtablissement.setEnabled(getModele().isDonneesChargees());
    snEtablissement.setSession(getModele().getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(getModele().getIdEtablissement());
  }
  
  /**
   * Rafraichir la liste des magasins.
   */
  private void rafraichirListeMagasin() {
    snMagasin.setEnabled(getModele().isDonneesChargees());
    snMagasin.setSession(getModele().getSession());
    IdEtablissement idEtablissement = snEtablissement.getIdSelection();
    snMagasin.setIdEtablissement(idEtablissement);
    if (idEtablissement != null) {
      snMagasin.charger(true);
    }
    snMagasin.setSelection(getModele().getMagasin());
  }
  
  /**
   * Charge la liste des acheteurs.
   */
  private void rafraichirAcheteur() {
    // Rafraichir la liste
    cbAcheteur.removeAllItems();
    if (getModele().getListeAcheteur() != null) {
      for (Acheteur acheteur : getModele().getListeAcheteur()) {
        cbAcheteur.addItem(acheteur);
      }
      
      // Sélectionner l'acheteur courant
      UtilisateurGescom utilisateurGescom = getModele().getUtilisateurGescom();
      if (utilisateurGescom != null && utilisateurGescom.getIdAcheteur() != null) {
        cbAcheteur.setSelectedItem(getModele().getListeAcheteur().retournerAcheteurParId(utilisateurGescom.getIdAcheteur()));
      }
      else {
        cbAcheteur.setSelectedItem(null);
      }
    }
    
    // Griser le composant lors du chargement des données
    cbAcheteur.setEnabled(getModele().isDonneesChargees());
  }
  
  /**
   * Rafraichir le texte explicatif au dessus du panel de controle.
   */
  private void rafraichirMessageInfo() {
    if (getModele().getFournisseurCourant() == null || !getModele().getFournisseurCourant().isExistant()) {
      lbMessageInfo.setText("Vous devez choisir un fournisseur pour saisir un document.");
    }
    else {
      lbMessageInfo.setText("");
    }
  }
  
  /**
   * Rafraichir le bouton de création de commande
   */
  private void rafraichirBoutonCreerCommande() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    
    if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    
    if (getModele().getIndexDocumentAchatSelectionne() >= 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CREER_COMMANDE, actif);
  }
  
  /**
   * Rafraichir le bouton de modification de commandes
   */
  private void rafraichirBoutonModifierCommande() {
    boolean actif = true;
    // N'afficher aucun bouton si le chargement des données est en cours
    if (!getModele().isDonneesChargees()) {
      actif = false;
    }
    // Ne pas afficher les boutons de création si un document est en cours de saisie
    if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    
    if (getModele().getIndexDocumentAchatSelectionne() < 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER_DOCUMENT, actif);
  }
  
  /**
   * Rafraichir le bouton de création de commande direct usine
   */
  private void rafraichirBoutonDirectUsine() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    if (!getModele().isDonneesChargees()) {
      actif = false;
    }
    // Ne pas afficher les boutons de création si un document est en cours de saisie
    if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    if (getModele().getIndexDocumentAchatSelectionne() >= 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CREER_COMMANDE_DIRECT_USINE, actif);
  }
  
  /**
   * Rafraichir le bouton d'approvisionnement du stock
   */
  private void rafraichirBoutonStock() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    if (getModele().isDocumentEnCoursDeSaisie() && !getModele().isAutoriserApprovisionnementStock()) {
      actif = false;
    }
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    if (getModele().getIndexDocumentAchatSelectionne() >= 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_APPROVISIONNEMENT_STOCK, actif);
  }
  
  /**
   * Rafraichir le bouton hors stock clients
   */
  private void rafraichirBoutonHorsStock() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    if (getModele().isDocumentEnCoursDeSaisie() && !getModele().isAutoriserApprovisionnementHorsStockClient()) {
      actif = false;
    }
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    if (getModele().getIndexDocumentAchatSelectionne() >= 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_APPROVISIONNEMENT_HORS_STOCK_CLIENTS, actif);
  }
  
  /**
   * Rafraichir les boutons de réappro des commandes clients
   */
  private void rafraichirApproCommande() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    if (getModele().isDocumentEnCoursDeSaisie() && !getModele().isAutoriserApprovisionnementCommandeClientUrgente()) {
      actif = false;
    }
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    if (getModele().getIndexDocumentAchatSelectionne() >= 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_APPROVISIONNEMENT_COMMANDES_CLIENTS, actif);
  }
  
  /**
   * Rafraichir les boutons du bouton annuler
   */
  private void rafraichirBoutonAnnuler() {
    boolean actif = true;
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    
    // N'afficher aucun bouton si le chargement des données est en cours
    if (!getModele().isDonneesChargees()) {
      actif = false;
    }
    // Tester si un fournisseur est sélectionné
    if (fournisseur == null || !fournisseur.isExistant()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(EnumBouton.ANNULER, actif);
  }
  
  /**
   * Rafraichir le bouton Quitter
   */
  private void rafraichirBoutonQuitter() {
    boolean actif = true;
    
    // N'afficher aucun bouton si le chargement des données est en cours
    if (!getModele().isDonneesChargees()) {
      snBarreBouton.activerBouton(EnumBouton.QUITTER, false);
    }
    
    snBarreBouton.activerBouton(EnumBouton.QUITTER, actif);
  }
  
  /**
   * Met à jour les données du fournisseur à l'écran.
   */
  private void rafraichirFournisseur() {
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    AdresseFournisseur coordonnees = null;
    if (fournisseur == null) {
      tfTelephone.setText("");
      tfFax.setText("");
      snContactPrincipal.setSession(getModele().getSession());
      snContactPrincipal.setIdEtablissement(getModele().getIdEtablissement());
      snContactPrincipal.charger(false, false);
      snContactPrincipal.setSelectionParId(null);
      snContactPrincipal.setModeComposantAffichage(true);
      tfEmail.setText("");
      
      tfNom.setText("");
      tfComplementNom.setText("");
      tfLocalisation.setText("");
      tfRue.setText("");
      tfCodePostal.setText("");
      tfVille.setText("");
    }
    else {
      snContactPrincipal.setSession(getModele().getSession());
      snContactPrincipal.setIdEtablissement(getModele().getIdEtablissement());
      snContactPrincipal.setTypeContact(EnumTypeContact.FOURNISSEUR);
      snContactPrincipal.setIdFournisseur(fournisseur.getId());
      snContactPrincipal.charger(false, false);
      snContactPrincipal.setSelectionContactPrincipal();
      snContactPrincipal.setModeComposantAffichage(true);
      coordonnees = fournisseur.getAdresseFournisseur(getModele().getIdAdresseFournisseurCourant());
      if (coordonnees == null || coordonnees.getAdresse() == null) {
        tfNom.setText("");
        tfComplementNom.setText("");
        tfLocalisation.setText("");
        tfRue.setText("");
        tfCodePostal.setText("");
        tfVille.setText("");
      }
      else {
        tfNom.setText(coordonnees.getAdresse().getNom());
        tfComplementNom.setText(coordonnees.getAdresse().getComplementNom());
        tfLocalisation.setText(coordonnees.getAdresse().getLocalisation());
        tfRue.setText(coordonnees.getAdresse().getRue());
        tfCodePostal.setText(coordonnees.getAdresse().getCodePostalFormate());
        tfVille.setText(coordonnees.getAdresse().getVille());
      }
      
      if (fournisseur.getAdresseSiege() == null || fournisseur.getAdresseSiege().getNumeroTelephone() == null) {
        tfTelephone.setText("");
      }
      else {
        tfTelephone.setText(fournisseur.getAdresseSiege().getNumeroTelephone());
      }
      if (fournisseur.getAdresseSiege() == null || fournisseur.getAdresseSiege().getNumeroFax() == null) {
        tfFax.setText("");
      }
      else {
        tfFax.setText(fournisseur.getAdresseSiege().getNumeroFax());
      }
      
      if (snContactPrincipal.getSelection() == null) {
        tfEmail.setText("");
      }
      else {
        tfEmail.setText(snContactPrincipal.getSelection().getEmail());
      }
    }
    tfNom.setEnabled(false);
    tfComplementNom.setEnabled(false);
    tfLocalisation.setEnabled(false);
    tfRue.setEnabled(false);
    tfCodePostal.setEnabled(false);
    tfVille.setEnabled(false);
    tfTelephone.setEnabled(false);
    tfFax.setEnabled(false);
    tfEmail.setEnabled(false);
  }
  
  /**
   * Met à jour le blocnote du fournisseur.
   */
  private void rafraichirObservationsFournisseur() {
    Fournisseur fournisseur = getModele().getFournisseurCourant();
    if (fournisseur == null || fournisseur.getBlocNote() == null) {
      taBlocnotesFournisseur.setText("");
    }
    else {
      taBlocnotesFournisseur.setText(fournisseur.getBlocNote().getTexte());
    }
    taBlocnotesFournisseur.setCaretPosition(0);
    taBlocnotesFournisseur.setEditable(false);
  }
  
  /**
   * Met à jour les documents de type Commande.
   */
  private void rafraichirListeCommandesEnCours() {
    String[][] donnees = null;
    
    // La liste ne contient aucun document
    List<DocumentAchat> listeDocumentAchat = getModele().getListeDocumentAchat();
    if (listeDocumentAchat != null && !listeDocumentAchat.isEmpty()) {
      // On a des commandes à afficher
      donnees = new String[listeDocumentAchat.size()][TITRE_LISTE_BON.length];
      for (int ligne = 0; ligne < listeDocumentAchat.size(); ligne++) {
        DocumentAchat documentAchat = listeDocumentAchat.get(ligne);
        // Numéro document
        donnees[ligne][0] = documentAchat.getId().toString();
        // date document
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentAchat.getDateCreation());
        // dernier traitement document
        if (documentAchat.getDateDernierTraitement() != null) {
          donnees[ligne][2] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentAchat.getDateDernierTraitement());
        }
        else {
          donnees[ligne][2] = "";
        }
        // Montant document
        if (documentAchat.getTotalHTLignes() != null) {
          donnees[ligne][3] = Constantes.formater(documentAchat.getTotalHTLignes(), true);
        }
        else {
          donnees[ligne][3] = "";
        }
        // Référence interne
        donnees[ligne][4] = documentAchat.getReferenceInterne();
        // Adresse
        if (documentAchat.getAdresseFournisseur() != null) {
          donnees[ligne][5] =
              documentAchat.getAdresseFournisseur().getCodePostalFormate() + " " + documentAchat.getAdresseFournisseur().getVille();
        }
        // Acheteur
        if (documentAchat.getIdAcheteur() != null) {
          donnees[ligne][6] = documentAchat.getIdAcheteur().getCode();
        }
        else {
          donnees[ligne][6] = "";
        }
        // Etat document
        if (documentAchat.getCodeEtat() != null) {
          donnees[ligne][7] = documentAchat.getCodeEtat().getLibelle();
        }
        else {
          donnees[ligne][7] = "";
        }
      }
    }
    tblCommandeEnCours.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = getModele().getIndexDocumentAchatSelectionne();
    if (index > -1) {
      tblCommandeEnCours.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblCommandeEnCours.clearSelection();
    }
  }
  
  /**
   * Active ou pas le bouton de modification du document du fournisseur sélectionné.
   */
  private void selectionnerUneLigneCommande() {
    int indexLigne;
    int indexOngletDocuments = tbpDocumentsEnCours.getSelectedIndex();
    
    switch (indexOngletDocuments) {
      case ONGLET_COMMANDES:
        indexLigne = traiterLigneSelectionnee(tblCommandeEnCours, getModele().getIndexDocumentAchatSelectionne());
        getModele().setIndexDocumentAchatSelectionne(indexLigne);
        break;
    }
    
    rafraichirBoutonModifierCommande();
    rafraichirBoutonCreerCommande();
    rafraichirBoutonDirectUsine();
    rafraichirBoutonStock();
    rafraichirBoutonHorsStock();
    rafraichirApproCommande();
  }
  
  /**
   * Traitement lors de la sélection d'une ligne dans une JTable.
   */
  private int traiterLigneSelectionnee(NRiTable pTable, int pIndexLigne) {
    int nouvelIndex = pTable.getSelectedRow();
    if (nouvelIndex > -1 && pTable.getRowSorter() != null) {
      nouvelIndex = pTable.getRowSorter().convertRowIndexToModel(nouvelIndex);
    }
    if (nouvelIndex >= 0) {
      if (pIndexLigne == nouvelIndex) {
        // Si on a cliqué sur une ligne déjà sélectionnée alors on la désélectionne
        pTable.clearSelection();
        pIndexLigne = -1;
      }
      else {
        // Si on a cliqué sur une nouvelle ligne
        pIndexLigne = nouvelIndex;
      }
    }
    return pIndexLigne;
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_CREER_COMMANDE)) {
        getModele().creerCommande();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_COMMANDE_DIRECT_USINE)) {
        getModele().creerCommandeDirectUsine();
      }
      else if (pSNBouton.isBouton(BOUTON_APPROVISIONNEMENT_STOCK)) {
        getModele().approvisionnerStock();
      }
      else if (pSNBouton.isBouton(BOUTON_APPROVISIONNEMENT_HORS_STOCK_CLIENTS)) {
        getModele().approvisionnerHorsStockClient();
      }
      else if (pSNBouton.isBouton(BOUTON_APPROVISIONNEMENT_COMMANDES_CLIENTS)) {
        getModele().approvisionnerCommandeClientUrgente();
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_DOCUMENT)) {
        getModele().modifierDocumentAchat(getModele().getIdDocumentAchatSelectionne(), true);
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        snAdresseFournisseur.initialiserRecherche();
        getModele().annulerDocument();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        snAdresseFournisseur.initialiserRecherche();
        getModele().fermerEcran();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpCommandeEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblCommandeEnCours == null) {
        return;
      }
      Trace.debug(VueOngletFournisseur.class, "scpCommandeEnCoursStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpCommandeEnCours.getViewport().getViewRect();
      int premiereLigne = tblCommandeEnCours.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblCommandeEnCours.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblCommandeEnCours.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().setIndexDocumentAchatSelectionne(tblCommandeEnCours.getIndiceSelection());
      getModele().modifierPlageDocumentAchat(premiereLigne, derniereLigne);
      rafraichirListeCommandesEnCours();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      getModele().rafraichir();
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierEtablissement(snEtablissement.getSelection());
        rafraichirListeMagasin();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      getModele().rafraichir();
    }
  }
  
  private void snMagasinValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMagasin(snMagasin.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      getModele().rafraichir();
    }
  }
  
  private void cbAcheteurItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      
      if (cbAcheteur.getSelectedItem() instanceof Acheteur) {
        getModele().modifierAcheteur((Acheteur) cbAcheteur.getSelectedItem());
      }
      else {
        getModele().modifierAcheteur(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblCommandeEnCoursMouseClicked(MouseEvent e) {
    try {
      // Action sur double clic
      if (e.getClickCount() == 2) {
        // Méthode plus fiable pour détecter la ligne sur laquelle on a double-cliqué
        Point p = e.getPoint();
        int row = tblCommandeEnCours.rowAtPoint(p);
        getModele().setIndexDocumentAchatSelectionne(row);
        getModele().modifierDocumentAchat();
        return;
      }
      // Action sur simple clic
      selectionnerUneLigneCommande();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snAdresseFournisseurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierAdresseFournisseur(snAdresseFournisseur.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btListeContactActionPerformed(ActionEvent e) {
    try {
      getModele().retournerListeContacts();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    lbRechercheFournisseur = new JLabel();
    snAdresseFournisseur = new SNAdresseFournisseur();
    panel1 = new JPanel();
    lbEtablissement = new JLabel();
    snEtablissement = new SNEtablissement();
    lbMagasin = new JLabel();
    snMagasin = new SNMagasin();
    lbAcheteur = new JLabel();
    cbAcheteur = new XRiComboBox();
    lbMessageErreur = new SNMessage();
    pnlCoordonneesFournisseur = new JPanel();
    lbNom = new JLabel();
    tfNom = new SNTexte();
    lbComplementNom = new JLabel();
    tfComplementNom = new SNTexte();
    lbLocalisation = new JLabel();
    tfRue = new SNTexte();
    lbRue = new JLabel();
    tfLocalisation = new SNTexte();
    lbVille = new JLabel();
    tfVille = new SNTexte();
    lbCodePostal = new JLabel();
    tfCodePostal = new SNTexte();
    lbContact = new JLabel();
    snContactPrincipal = new SNContact();
    lbTelephone = new JLabel();
    tfTelephone = new SNTexte();
    lbEmail = new JLabel();
    tfEmail = new SNTexte();
    lbFax = new JLabel();
    tfFax = new SNTexte();
    scpBlocNoteFournisseur = new JScrollPane();
    taBlocnotesFournisseur = new JTextArea();
    sptDocumentEnCours = new SNLabelTitre();
    tbpDocumentsEnCours = new JTabbedPane();
    pnlCommandeEnCours = new JPanel();
    scpCommandeEnCours = new JScrollPane();
    tblCommandeEnCours = new NRiTable();
    lbMessageInfo = new SNMessage();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(1240, 650));
      pnlContenu.setMinimumSize(new Dimension(1240, 650));
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setAutoscrolls(true);
      pnlContenu.setOpaque(true);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 158, 0, 565, 150, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 23, 202, 15, 22, 119, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ---- lbRechercheFournisseur ----
      lbRechercheFournisseur.setText("Recherche fournisseur");
      lbRechercheFournisseur.setFont(new Font("sansserif", Font.BOLD, 14));
      lbRechercheFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
      lbRechercheFournisseur.setPreferredSize(new Dimension(163, 30));
      lbRechercheFournisseur.setName("lbRechercheFournisseur");
      pnlContenu.add(lbRechercheFournisseur,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ---- snAdresseFournisseur ----
      snAdresseFournisseur.setName("snAdresseFournisseur");
      snAdresseFournisseur.addSNComposantListener(new InterfaceSNComposantListener() {
        @Override
        public void valueChanged(SNComposantEvent e) {
          snAdresseFournisseurValueChanged(e);
        }
      });
      pnlContenu.add(snAdresseFournisseur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setMinimumSize(new Dimension(460, 135));
        panel1.setPreferredSize(new Dimension(460, 135));
        panel1.setName("panel1");
        panel1.setLayout(new GridBagLayout());
        ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 141, 294, 0 };
        ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement");
        lbEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEtablissement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEtablissement.setName("lbEtablissement");
        panel1.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setMinimumSize(new Dimension(315, 30));
        snEtablissement.setPreferredSize(new Dimension(315, 30));
        snEtablissement.setName("snEtablissement");
        snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snEtablissementValueChanged(e);
          }
        });
        panel1.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMagasin ----
        lbMagasin.setText("Magasin");
        lbMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbMagasin.setHorizontalAlignment(SwingConstants.RIGHT);
        lbMagasin.setName("lbMagasin");
        panel1.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snMagasin ----
        snMagasin.setPreferredSize(new Dimension(315, 30));
        snMagasin.setMinimumSize(new Dimension(315, 30));
        snMagasin.setName("snMagasin");
        snMagasin.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snMagasinValueChanged(e);
          }
        });
        panel1.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbAcheteur ----
        lbAcheteur.setText("Acheteur");
        lbAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbAcheteur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbAcheteur.setName("lbAcheteur");
        panel1.add(lbAcheteur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cbAcheteur ----
        cbAcheteur.setBackground(new Color(171, 148, 79));
        cbAcheteur.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbAcheteur.setMinimumSize(new Dimension(315, 30));
        cbAcheteur.setPreferredSize(new Dimension(315, 30));
        cbAcheteur.setName("cbAcheteur");
        panel1.add(cbAcheteur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(panel1, new GridBagConstraints(3, 0, 2, 4, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbMessageErreur ----
      lbMessageErreur.setText("Message erreur");
      lbMessageErreur.setForeground(Color.red);
      lbMessageErreur.setFont(new Font("sansserif", Font.BOLD, 14));
      lbMessageErreur.setName("lbMessageErreur");
      pnlContenu.add(lbMessageErreur, new GridBagConstraints(1, 1, 2, 1, 1.0, 0.0, GridBagConstraints.ABOVE_BASELINE,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlCoordonneesFournisseur ========
      {
        pnlCoordonneesFournisseur.setBorder(new TitledBorder(null, "Coordonn\u00e9es fournisseur", TitledBorder.LEADING,
            TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
        pnlCoordonneesFournisseur.setOpaque(false);
        pnlCoordonneesFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlCoordonneesFournisseur.setMinimumSize(new Dimension(710, 250));
        pnlCoordonneesFournisseur.setPreferredSize(new Dimension(710, 290));
        pnlCoordonneesFournisseur.setMaximumSize(new Dimension(710, 330));
        pnlCoordonneesFournisseur.setName("pnlCoordonneesFournisseur");
        pnlCoordonneesFournisseur.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesFournisseur.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 0, 0, 130, 0 };
        ((GridBagLayout) pnlCoordonneesFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesFournisseur.getLayout()).columnWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesFournisseur.getLayout()).rowWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNom ----
        lbNom.setText("Raison sociale");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setName("lbNom");
        pnlCoordonneesFournisseur.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNom ----
        tfNom.setBackground(Color.white);
        tfNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNom.setEditable(false);
        tfNom.setMinimumSize(new Dimension(485, 30));
        tfNom.setPreferredSize(new Dimension(485, 30));
        tfNom.setName("tfNom");
        pnlCoordonneesFournisseur.add(tfNom, new GridBagConstraints(1, 1, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
        
        // ---- lbComplementNom ----
        lbComplementNom.setText("Compl\u00e9ment");
        lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNom.setName("lbComplementNom");
        pnlCoordonneesFournisseur.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfComplementNom ----
        tfComplementNom.setBackground(Color.white);
        tfComplementNom.setMinimumSize(new Dimension(485, 30));
        tfComplementNom.setPreferredSize(new Dimension(485, 30));
        tfComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfComplementNom.setName("tfComplementNom");
        pnlCoordonneesFournisseur.add(tfComplementNom, new GridBagConstraints(1, 2, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonneesFournisseur.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfRue ----
        tfRue.setBackground(Color.white);
        tfRue.setMinimumSize(new Dimension(485, 30));
        tfRue.setPreferredSize(new Dimension(485, 30));
        tfRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfRue.setName("tfRue");
        pnlCoordonneesFournisseur.add(tfRue, new GridBagConstraints(1, 3, 6, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setName("lbRue");
        pnlCoordonneesFournisseur.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLocalisation ----
        tfLocalisation.setBackground(Color.white);
        tfLocalisation.setMinimumSize(new Dimension(485, 30));
        tfLocalisation.setPreferredSize(new Dimension(485, 30));
        tfLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfLocalisation.setName("tfLocalisation");
        pnlCoordonneesFournisseur.add(tfLocalisation, new GridBagConstraints(1, 4, 6, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbVille ----
        lbVille.setText("Ville");
        lbVille.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbVille.setHorizontalAlignment(SwingConstants.RIGHT);
        lbVille.setName("lbVille");
        pnlCoordonneesFournisseur.add(lbVille, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfVille ----
        tfVille.setBackground(Color.white);
        tfVille.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfVille.setMinimumSize(new Dimension(365, 30));
        tfVille.setPreferredSize(new Dimension(365, 30));
        tfVille.setEditable(true);
        tfVille.setName("tfVille");
        pnlCoordonneesFournisseur.add(tfVille, new GridBagConstraints(3, 5, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbCodePostal ----
        lbCodePostal.setText("Code postal");
        lbCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCodePostal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCodePostal.setName("lbCodePostal");
        pnlCoordonneesFournisseur.add(lbCodePostal, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfCodePostal ----
        tfCodePostal.setBackground(Color.white);
        tfCodePostal.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfCodePostal.setMinimumSize(new Dimension(55, 30));
        tfCodePostal.setPreferredSize(new Dimension(55, 30));
        tfCodePostal.setName("tfCodePostal");
        pnlCoordonneesFournisseur.add(tfCodePostal, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact principal");
        lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setName("lbContact");
        pnlCoordonneesFournisseur.add(lbContact, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snContactPrincipal ----
        snContactPrincipal.setMinimumSize(new Dimension(300, 30));
        snContactPrincipal.setPreferredSize(new Dimension(350, 30));
        snContactPrincipal.setName("snContactPrincipal");
        pnlCoordonneesFournisseur.add(snContactPrincipal, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setPreferredSize(new Dimension(90, 30));
        lbTelephone.setMinimumSize(new Dimension(90, 30));
        lbTelephone.setMaximumSize(new Dimension(100, 30));
        lbTelephone.setName("lbTelephone");
        pnlCoordonneesFournisseur.add(lbTelephone, new GridBagConstraints(4, 6, 2, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfTelephone.setMinimumSize(new Dimension(140, 30));
        tfTelephone.setPreferredSize(new Dimension(140, 30));
        tfTelephone.setName("tfTelephone");
        pnlCoordonneesFournisseur.add(tfTelephone, new GridBagConstraints(6, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbEmail ----
        lbEmail.setText("Email contact");
        lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEmail.setName("lbEmail");
        pnlCoordonneesFournisseur.add(lbEmail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEmail.setMinimumSize(new Dimension(215, 30));
        tfEmail.setPreferredSize(new Dimension(215, 30));
        tfEmail.setName("tfEmail");
        pnlCoordonneesFournisseur.add(tfEmail, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlCoordonneesFournisseur.add(lbFax, new GridBagConstraints(4, 7, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfFax.setMinimumSize(new Dimension(140, 30));
        tfFax.setPreferredSize(new Dimension(140, 30));
        tfFax.setName("tfFax");
        pnlCoordonneesFournisseur.add(tfFax, new GridBagConstraints(6, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
      }
      pnlContenu.add(pnlCoordonneesFournisseur, new GridBagConstraints(0, 2, 3, 4, 1.0, 0.0, GridBagConstraints.CENTER,
          GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 45));
      
      // ======== scpBlocNoteFournisseur ========
      {
        scpBlocNoteFournisseur.setPreferredSize(new Dimension(495, 40));
        scpBlocNoteFournisseur.setMinimumSize(new Dimension(495, 40));
        scpBlocNoteFournisseur.setName("scpBlocNoteFournisseur");
        
        // ---- taBlocnotesFournisseur ----
        taBlocnotesFournisseur.setMinimumSize(new Dimension(350, 40));
        taBlocnotesFournisseur.setPreferredSize(new Dimension(350, 40));
        taBlocnotesFournisseur.setFont(new Font("sansserif", Font.PLAIN, 14));
        taBlocnotesFournisseur.setLineWrap(true);
        taBlocnotesFournisseur.setForeground(Color.red);
        taBlocnotesFournisseur.setName("taBlocnotesFournisseur");
        scpBlocNoteFournisseur.setViewportView(taBlocnotesFournisseur);
      }
      pnlContenu.add(scpBlocNoteFournisseur, new GridBagConstraints(3, 4, 2, 2, 0.0, 0.0, GridBagConstraints.EAST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 10, 5), 0, 0));
      
      // ---- sptDocumentEnCours ----
      sptDocumentEnCours.setText("Documents en cours");
      sptDocumentEnCours.setFont(new Font("sansserif", Font.BOLD, 14));
      sptDocumentEnCours.setMinimumSize(new Dimension(170, 20));
      sptDocumentEnCours.setPreferredSize(new Dimension(170, 20));
      sptDocumentEnCours.setMaximumSize(new Dimension(170, 30));
      sptDocumentEnCours.setName("sptDocumentEnCours");
      pnlContenu.add(sptDocumentEnCours, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.ABOVE_BASELINE,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== tbpDocumentsEnCours ========
      {
        tbpDocumentsEnCours.setFont(tbpDocumentsEnCours.getFont().deriveFont(tbpDocumentsEnCours.getFont().getSize() + 4f));
        tbpDocumentsEnCours.setMinimumSize(new Dimension(1200, 170));
        tbpDocumentsEnCours.setPreferredSize(new Dimension(1200, 170));
        tbpDocumentsEnCours.setBackground(new Color(239, 239, 222));
        tbpDocumentsEnCours.setOpaque(true);
        tbpDocumentsEnCours.setName("tbpDocumentsEnCours");
        
        // ======== pnlCommandeEnCours ========
        {
          pnlCommandeEnCours.setOpaque(false);
          pnlCommandeEnCours.setName("pnlCommandeEnCours");
          pnlCommandeEnCours.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCommandeEnCours.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommandeEnCours.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlCommandeEnCours.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCommandeEnCours.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== scpCommandeEnCours ========
          {
            scpCommandeEnCours.setBackground(Color.white);
            scpCommandeEnCours.setOpaque(true);
            scpCommandeEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            scpCommandeEnCours.setPreferredSize(new Dimension(0, 0));
            scpCommandeEnCours.setName("scpCommandeEnCours");
            
            // ---- tblCommandeEnCours ----
            tblCommandeEnCours.setShowVerticalLines(true);
            tblCommandeEnCours.setShowHorizontalLines(true);
            tblCommandeEnCours.setBackground(Color.white);
            tblCommandeEnCours.setRowHeight(20);
            tblCommandeEnCours.setGridColor(new Color(204, 204, 204));
            tblCommandeEnCours.setMinimumSize(new Dimension(1170, 650));
            tblCommandeEnCours.setPreferredSize(new Dimension(1170, 30));
            tblCommandeEnCours.setMaximumSize(new Dimension(2147483647, 2147483647));
            tblCommandeEnCours.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblCommandeEnCours.setFont(new Font("sansserif", Font.PLAIN, 14));
            tblCommandeEnCours.setPreferredScrollableViewportSize(new Dimension(2147483647, 2147483647));
            tblCommandeEnCours.setOpaque(false);
            tblCommandeEnCours.setName("tblCommandeEnCours");
            tblCommandeEnCours.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblCommandeEnCoursMouseClicked(e);
              }
            });
            scpCommandeEnCours.setViewportView(tblCommandeEnCours);
          }
          pnlCommandeEnCours.add(scpCommandeEnCours, new GridBagConstraints(0, 0, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(5, 5, 5, 5), 0, 0));
        }
        tbpDocumentsEnCours.addTab("Commandes", pnlCommandeEnCours);
      }
      pnlContenu.add(tbpDocumentsEnCours,
          new GridBagConstraints(0, 7, 5, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbMessageInfo ----
      lbMessageInfo.setText("text");
      lbMessageInfo.setFont(new Font("sansserif", Font.BOLD | Font.ITALIC, 14));
      lbMessageInfo.setPreferredSize(new Dimension(150, 20));
      lbMessageInfo.setMinimumSize(new Dimension(150, 20));
      lbMessageInfo.setName("lbMessageInfo");
      pnlContenu.add(lbMessageInfo, new GridBagConstraints(0, 8, 5, 1, 0.0, 0.0, GridBagConstraints.BASELINE,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private JLabel lbRechercheFournisseur;
  private SNAdresseFournisseur snAdresseFournisseur;
  private JPanel panel1;
  private JLabel lbEtablissement;
  private SNEtablissement snEtablissement;
  private JLabel lbMagasin;
  private SNMagasin snMagasin;
  private JLabel lbAcheteur;
  private XRiComboBox cbAcheteur;
  private SNMessage lbMessageErreur;
  private JPanel pnlCoordonneesFournisseur;
  private JLabel lbNom;
  private SNTexte tfNom;
  private JLabel lbComplementNom;
  private SNTexte tfComplementNom;
  private JLabel lbLocalisation;
  private SNTexte tfRue;
  private JLabel lbRue;
  private SNTexte tfLocalisation;
  private JLabel lbVille;
  private SNTexte tfVille;
  private JLabel lbCodePostal;
  private SNTexte tfCodePostal;
  private JLabel lbContact;
  private SNContact snContactPrincipal;
  private JLabel lbTelephone;
  private SNTexte tfTelephone;
  private JLabel lbEmail;
  private SNTexte tfEmail;
  private JLabel lbFax;
  private SNTexte tfFax;
  private JScrollPane scpBlocNoteFournisseur;
  private JTextArea taBlocnotesFournisseur;
  private SNLabelTitre sptDocumentEnCours;
  private JTabbedPane tbpDocumentsEnCours;
  private JPanel pnlCommandeEnCours;
  private JScrollPane scpCommandeEnCours;
  private NRiTable tblCommandeEnCours;
  private SNMessage lbMessageInfo;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
