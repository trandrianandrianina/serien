/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletStockLigneAchat extends JPanel implements InterfaceVueOnglet {
  // Constantes
  public static final int SOUS_ONGLET_ETAT_STOCKS = 0;
  public static final int SOUS_ONGLET_MOUVEMENTS = 1;
  public static final int SOUS_ONGLET_ATTENDUS = 2;
  private static final int NOMBRE_DECIMALE_MONTANT = 3;
  
  // Variables
  private ModeleDetailLigneAchat modele = null;
  private boolean ecranInitialise = false;
  private int indexLigneListeMouvements = -1;
  private int indexLigneListeAttenduCommande = -1;
  private int indexLigneListeEtatStocks = -1;
  private boolean tousDepotEnPlace = false;
  private boolean mouvementStockEnPlace = false;
  private boolean attenducommandeEnPlace = false;
  
  private boolean executerEvenements = false;
  
  /**
   * Constructeur.
   */
  public VueOngletStockLigneAchat(ModeleDetailLigneAchat acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    executerEvenements = false;
    
    ecranInitialise = true;
    rafraichirSousOnglets();
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  
  private void rafraichirSousOnglets() {
    switch (modele.getSousOngletActif()) {
      case SOUS_ONGLET_ETAT_STOCKS:
        rafraichirTousDepots();
        break;
      
      case SOUS_ONGLET_MOUVEMENTS:
        rafraichirMouvements();
        break;
      
      case SOUS_ONGLET_ATTENDUS:
        rafraichirAttenduCommande(modele.getListeLignesAttCmd());
        break;
      
      default:
        break;
    }
  }
  
  /**
   * Affichage des données du sous-onglet mouvements
   */
  private void rafraichirMouvements() {
    if (modele.getVueMouvements() == null) {
      return;
    }
    if (!mouvementStockEnPlace) {
      mouvementStockEnPlace = true;
      pnlMouvementsDeStocks.add(modele.getVueMouvements(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  /**
   * Affichage des données du sous-onglet tous les dépots
   */
  private void rafraichirTousDepots() {
    if (modele.getVueStock() == null) {
      return;
    }
    if (!tousDepotEnPlace) {
      tousDepotEnPlace = true;
      pnlStocksTousDepots.add(modele.getVueStock(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  /**
   * Affichage des données du sous-onglet attendu commandé
   */
  private void rafraichirAttenduCommande(List<LigneAttenduCommande> attcmd) {
    if (modele.getVueAttendus() == null) {
      return;
    }
    if (!attenducommandeEnPlace) {
      attenducommandeEnPlace = true;
      pnlStocksAttenduCommande.add(modele.getVueAttendus(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpStocksPropertyChange(PropertyChangeEvent e) {
    
  }
  
  private void tbpStocksMouseClicked(MouseEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierSousOngletStock(tbpStocks.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    tbpStocks = new JTabbedPane();
    pnlStocksTousDepots = new JPanel();
    pnlMouvementsDeStocks = new JPanel();
    pnlStocksAttenduCommande = new JPanel();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== tbpStocks ========
    {
      tbpStocks.setTabPlacement(SwingConstants.LEFT);
      tbpStocks.setName("tbpStocks");
      tbpStocks.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent e) {
          tbpStocksMouseClicked(e);
        }
      });
      
      // ======== pnlStocksTousDepots ========
      {
        pnlStocksTousDepots.setOpaque(false);
        pnlStocksTousDepots.setName("pnlStocksTousDepots");
        pnlStocksTousDepots.setLayout(new BorderLayout());
      }
      tbpStocks.addTab("<html><center>Etat des<br />Stocks</center></html>", pnlStocksTousDepots);
      
      // ======== pnlMouvementsDeStocks ========
      {
        pnlMouvementsDeStocks.setOpaque(false);
        pnlMouvementsDeStocks.setName("pnlMouvementsDeStocks");
        pnlMouvementsDeStocks.setLayout(new BorderLayout());
      }
      tbpStocks.addTab("<html><center>Mouvements<br />de stocks</center></html>", pnlMouvementsDeStocks);
      
      // ======== pnlStocksAttenduCommande ========
      {
        pnlStocksAttenduCommande.setOpaque(false);
        pnlStocksAttenduCommande.setName("pnlStocksAttenduCommande");
        pnlStocksAttenduCommande.setLayout(new BorderLayout());
      }
      tbpStocks.addTab("<html><center>Attendu<br />command\u00e9</center></html>", pnlStocksAttenduCommande);
    }
    add(tbpStocks, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JTabbedPane tbpStocks;
  private JPanel pnlStocksTousDepots;
  private JPanel pnlMouvementsDeStocks;
  private JPanel pnlStocksAttenduCommande;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
