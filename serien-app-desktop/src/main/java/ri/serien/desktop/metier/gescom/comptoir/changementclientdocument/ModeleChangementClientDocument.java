/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.changementclientdocument;

import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialoge qui propose la quantité correspondant au conditionnement supérieur.
 */
public class ModeleChangementClientDocument extends AbstractModeleDialogue {
  // Variables
  private VueChangementClientDocument vue = null;
  private DocumentVente documentOrigine = null;
  private boolean isRecalculerPrix = true;
  
  /**
   * Constructeur.
   */
  public ModeleChangementClientDocument(SessionBase pSession, DocumentVente pDocumentOrigine) {
    super(pSession);
    documentOrigine = pDocumentOrigine;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    
  }
  
  // Méthodes publiques
  public void modifierClient(ClientBase pClient) {
    if (pClient == null) {
      return;
    }
    if (pClient.getId().equals(documentOrigine.getIdClientFacture())) {
      return;
    }
    documentOrigine.setIdClientFacture(pClient.getId());
    documentOrigine.setIdClientLivre(pClient.getId());
  }
  
  // Méthodes privées
  
  // -- Accesseurs
  /**
   * Vue de la boite de dialogue
   */
  public VueChangementClientDocument getVue() {
    return vue;
  }
  
  /**
   * Message de duplication
   */
  public String getLibelleChangement() {
    if (documentOrigine == null) {
      return "Vous souhaitez changer le client du document en cours de création";
    }
    else {
      return "Vous souhaitez changer le client du document en cours de création numéro " + documentOrigine.getId();
    }
  }
  
  /**
   * Renvoyer le mode de mise à jour des prix
   */
  public boolean isRecalculerPrix() {
    return isRecalculerPrix;
  }
  
  /**
   * Modifier le mode de mise à jour des prix
   */
  public void setRecalculerPrix(boolean pIsRecalculerPrix) {
    isRecalculerPrix = pIsRecalculerPrix;
  }
  
  /**
   * Document d'origine
   */
  public DocumentVente getDocument() {
    return documentOrigine;
  }
  
}
