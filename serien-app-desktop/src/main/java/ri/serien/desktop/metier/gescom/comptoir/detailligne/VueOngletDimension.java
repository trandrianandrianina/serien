/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet général de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletDimension extends SNPanel implements InterfaceVueOnglet {
  // Variables
  private ModeleDetailLigneArticle modele = null;
  
  public VueOngletDimension(ModeleDetailLigneArticle acomptoir) {
    modele = acomptoir;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Rafraichir le nombre
    rafraichirNombre();
    rafraichirUniteVente();
    
    // Rafraichir les dimensions d'origine
    rafraichirLongueurOrigine();
    rafraichirUniteLongueurOrigine();
    rafraichirLargeurOrigine();
    rafraichirUniteLargeurOrigine();
    rafraichirHauteurOrigine();
    rafraichirUniteHauteurOrigine();
    
    // Rafraichir les dimensions
    rafraichirLongueur();
    rafraichirUniteLongueur();
    rafraichirLargeur();
    rafraichirUniteLargeur();
    rafraichirHauteur();
    rafraichirUniteHauteur();
    
    // Rafraichir les totaux
    rafraichirPrixNet();
    rafraichirDimension();
    rafraichirMontant();
    
    // Gestion du focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleDetailLigneArticle.FOCUS_DIMENSION_NOMBRE:
        tfNombre.requestFocus();
        break;
    }
  }
  
  private void rafraichirNombre() {
    tfNombre.setText(Constantes.convertirIntegerEnTexte(modele.getNombreDecoupe(), 0));
  }
  
  private void rafraichirUniteVente() {
    LigneVente ligneVente = modele.getLigneVente();
    
    if (ligneVente != null && ligneVente.getIdUniteVente() != null) {
      lbUniteUV.setText(ligneVente.getIdUniteVente().getCode());
    }
    else {
      lbUniteUV.setText("");
    }
  }
  
  private void rafraichirDimension() {
    BigDecimal dimension = modele.getDimension();
    if (dimension != null && modele.getLibelleTypeDimension() != null) {
      tfDimension.setText(Constantes.formater(dimension, true));
      lbDimension.setText(modele.getLibelleTypeDimension());
    }
    else {
      tfDimension.setText("0");
      lbDimension.setText("Dimension");
    }
  }
  
  private void rafraichirPrixNet() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && modele.isAffichageTTC() && ligneVente.getPrixNetTTC() != null) {
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetTTC(), true));
      lbPrixNet.setText("Prix net TTC");
    }
    else if (ligneVente != null && !modele.isAffichageTTC() && ligneVente.getPrixNetHT() != null) {
      tfPrixNet.setText(Constantes.formater(ligneVente.getPrixNetHT(), true));
      lbPrixNet.setText("Prix net HT");
    }
    else {
      tfPrixNet.setText("0");
      lbPrixNet.setText("Prix net");
    }
  }
  
  private void rafraichirMontant() {
    LigneVente ligneVente = modele.getLigneVente();
    if (ligneVente != null && modele.isAffichageTTC() && ligneVente.getMontantTTC() != null) {
      tfMontantTotal.setText(Constantes.formater(ligneVente.getMontantTTC(), true));
      lbMontant.setText("Montant TTC");
    }
    else if (ligneVente != null && !modele.isAffichageTTC() && ligneVente.getMontantHT() != null) {
      tfMontantTotal.setText(Constantes.formater(ligneVente.getMontantHT(), true));
      lbMontant.setText("Montant HT");
    }
    else {
      tfMontantTotal.setText("0");
      lbMontant.setText("Montant");
    }
  }
  
  private void rafraichirLongueurOrigine() {
    Article article = modele.getArticle();
    
    if (article != null) {
      tfLongueurOrigine.setText(Constantes.formater(article.getLongueur(), true));
    }
    else {
      tfLongueurOrigine.setText("");
    }
    tfLongueurOrigine.setVisible(modele.isLongueurVisible());
    tfLongueurOrigine.setVisible(modele.isLongueurVisible());
  }
  
  private void rafraichirUniteLongueurOrigine() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLongueur() != null) {
      lbUniteLongueurOrigine.setText(article.getCodeUniteLongueur());
    }
    else {
      lbUniteLongueurOrigine.setText("");
    }
    lbUniteLongueurOrigine.setVisible(modele.isLongueurVisible());
  }
  
  private void rafraichirLongueur() {
    Article article = modele.getArticle();
    BigDecimal longueur = modele.getLongueur();
    if (longueur != null && article != null) {
      tfLongueur.setText(Constantes.formater(modele.getLongueur(), true));
    }
    else {
      tfLongueur.setText("");
    }
    lbLongueur.setVisible(modele.isLongueurVisible());
    tfLongueur.setVisible(modele.isLongueurVisible());
  }
  
  private void rafraichirUniteLongueur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLongueur() != null) {
      lbUniteLongueur.setText(article.getCodeUniteLongueur());
    }
    else {
      lbUniteLongueur.setText("");
    }
    lbUniteLongueur.setVisible(modele.isLongueurVisible());
  }
  
  private void rafraichirLargeurOrigine() {
    Article article = modele.getArticle();
    if (article != null) {
      tfLargeurOrigine.setText(Constantes.formater(article.getLargeur(), true));
    }
    else {
      tfLargeurOrigine.setText("");
    }
    lbLargeurOrigine.setVisible(modele.isLargeurVisible());
    tfLargeurOrigine.setVisible(modele.isLargeurVisible());
    lbSymboleMultiplier1.setVisible(modele.isLargeurVisible());
    lbSymboleMultiplier3.setVisible(modele.isLargeurVisible());
  }
  
  private void rafraichirUniteLargeurOrigine() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLargeur() != null) {
      lbUniteLargeurOrigine.setText(article.getCodeUniteLargeur());
    }
    else {
      lbUniteLargeurOrigine.setText("");
    }
    lbUniteLargeurOrigine.setVisible(modele.isLargeurVisible());
  }
  
  private void rafraichirLargeur() {
    Article article = modele.getArticle();
    BigDecimal largeur = modele.getLargeur();
    if (largeur != null && article != null) {
      tfLargeur.setText(Constantes.formater(modele.getLargeur(), true));
    }
    else {
      tfLargeur.setText("");
    }
    lbLargeur.setVisible(modele.isLargeurVisible());
    tfLargeur.setVisible(modele.isLargeurVisible());
  }
  
  private void rafraichirUniteLargeur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteLargeur() != null) {
      lbUniteLargeur.setText(article.getCodeUniteLargeur());
    }
    else {
      lbUniteLargeur.setText("");
    }
    lbUniteLargeur.setVisible(modele.isLargeurVisible());
  }
  
  private void rafraichirHauteurOrigine() {
    Article article = modele.getArticle();
    if (article != null) {
      tfHauteurOrigine.setText(Constantes.formater(article.getHauteur(), true));
    }
    else {
      tfHauteurOrigine.setText("");
    }
    lbHauteurOrigine.setVisible(modele.isHauteurVisible());
    tfHauteurOrigine.setVisible(modele.isHauteurVisible());
    lbSymboleMultiplier2.setVisible(modele.isHauteurVisible());
    lbSymboleMultiplier4.setVisible(modele.isHauteurVisible());
  }
  
  private void rafraichirUniteHauteurOrigine() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteHauteur() != null) {
      lbUniteHauteurOrigine.setText(article.getCodeUniteHauteur());
    }
    else {
      lbUniteHauteurOrigine.setText("");
    }
    lbUniteHauteurOrigine.setVisible(modele.isHauteurVisible());
  }
  
  private void rafraichirHauteur() {
    Article article = modele.getArticle();
    BigDecimal hauteur = modele.getHauteur();
    if (hauteur != null && article != null) {
      tfHauteur.setText(Constantes.formater(modele.getHauteur(), true));
    }
    else {
      tfHauteur.setText("");
    }
    lbHauteur.setVisible(modele.isHauteurVisible());
    tfHauteur.setVisible(modele.isHauteurVisible());
  }
  
  private void rafraichirUniteHauteur() {
    Article article = modele.getArticle();
    if (article != null && article.getCodeUniteHauteur() != null) {
      lbUniteHauteur.setText(article.getCodeUniteHauteur());
    }
    else {
      lbUniteHauteur.setText("");
    }
    lbUniteHauteur.setVisible(modele.isHauteurVisible());
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNombreFocusLost(FocusEvent e) {
    try {
      modele.modifierNombre(tfNombre.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLongueurFocusLost(FocusEvent e) {
    try {
      modele.modifierLongueur(tfLongueur.getText());
    }
    catch (Exception exception) {
      modele.empecherValidation();
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLargeurFocusLost(FocusEvent e) {
    try {
      modele.modifierLargeur(tfLargeur.getText());
    }
    catch (Exception exception) {
      modele.empecherValidation();
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfHauteurFocusLost(FocusEvent e) {
    try {
      modele.modifierHauteur(tfHauteur.getText());
    }
    catch (Exception exception) {
      modele.empecherValidation();
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLongueurKeyPressed(KeyEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlQuantite = new SNPanel();
    lbNombre = new SNLabelChamp();
    tfNombre = new SNTexte();
    pnlDimension = new SNPanelTitre();
    lbLongueurOrigine = new SNLabelChamp();
    tfLongueurOrigine = new SNTexte();
    lbUniteLongueurOrigine = new SNLabelUnite();
    lbSymboleMultiplier3 = new SNLabelChamp();
    lbLargeurOrigine = new SNLabelChamp();
    tfLargeurOrigine = new SNTexte();
    lbUniteLargeurOrigine = new SNLabelUnite();
    lbSymboleMultiplier4 = new SNLabelChamp();
    lbHauteurOrigine = new SNLabelChamp();
    tfHauteurOrigine = new SNTexte();
    lbUniteHauteurOrigine = new SNLabelUnite();
    pnlDecoupe = new SNPanelTitre();
    lbLongueur = new SNLabelChamp();
    tfLongueur = new SNTexte();
    lbUniteLongueur = new SNLabelUnite();
    lbSymboleMultiplier1 = new SNLabelChamp();
    lbLargeur = new SNLabelChamp();
    tfLargeur = new SNTexte();
    lbUniteLargeur = new SNLabelUnite();
    lbSymboleMultiplier2 = new SNLabelChamp();
    lbHauteur = new SNLabelChamp();
    tfHauteur = new SNTexte();
    lbUniteHauteur = new SNLabelUnite();
    pnlResultat = new SNPanelTitre();
    lbPrixNet = new SNLabelChamp();
    tfPrixNet = new SNTexte();
    lbUniteLongueur2 = new SNLabelUnite();
    lbSymboleMultiplier = new SNLabelChamp();
    lbDimension = new SNLabelChamp();
    tfDimension = new SNTexte();
    lbUniteUV = new SNLabelUnite();
    lbSymboleEgal = new SNLabelChamp();
    lbMontant = new SNLabelChamp();
    tfMontantTotal = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlQuantite ========
      {
        pnlQuantite.setName("pnlQuantite");
        pnlQuantite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbNombre ----
        lbNombre.setText("Quantit\u00e9");
        lbNombre.setPreferredSize(new Dimension(100, 30));
        lbNombre.setName("lbNombre");
        pnlQuantite.add(lbNombre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfNombre ----
        tfNombre.setPreferredSize(new Dimension(90, 30));
        tfNombre.setMinimumSize(new Dimension(90, 30));
        tfNombre.setMaximumSize(new Dimension(90, 30));
        tfNombre.setHorizontalAlignment(SwingConstants.TRAILING);
        tfNombre.setName("tfNombre");
        tfNombre.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNombreFocusLost(e);
          }
        });
        pnlQuantite.add(tfNombre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 15, 5, 0), 0, 0));
      
      // ======== pnlDimension ========
      {
        pnlDimension.setOpaque(false);
        pnlDimension.setMinimumSize(new Dimension(600, 90));
        pnlDimension.setMaximumSize(new Dimension(600, 90));
        pnlDimension.setPreferredSize(new Dimension(600, 90));
        pnlDimension.setTitre("Dimensions de l'article");
        pnlDimension.setName("pnlDimension");
        pnlDimension.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDimension.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDimension.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlDimension.getLayout()).columnWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlDimension.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLongueurOrigine ----
        lbLongueurOrigine.setText("Longueur");
        lbLongueurOrigine.setPreferredSize(new Dimension(100, 30));
        lbLongueurOrigine.setMinimumSize(new Dimension(100, 30));
        lbLongueurOrigine.setMaximumSize(new Dimension(100, 30));
        lbLongueurOrigine.setName("lbLongueurOrigine");
        pnlDimension.add(lbLongueurOrigine, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfLongueurOrigine ----
        tfLongueurOrigine.setMaximumSize(new Dimension(90, 30));
        tfLongueurOrigine.setMinimumSize(new Dimension(90, 30));
        tfLongueurOrigine.setPreferredSize(new Dimension(90, 30));
        tfLongueurOrigine.setEnabled(false);
        tfLongueurOrigine.setHorizontalAlignment(SwingConstants.TRAILING);
        tfLongueurOrigine.setName("tfLongueurOrigine");
        tfLongueurOrigine.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLongueurFocusLost(e);
          }
        });
        pnlDimension.add(tfLongueurOrigine, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteLongueurOrigine ----
        lbUniteLongueurOrigine.setText("UL");
        lbUniteLongueurOrigine.setName("lbUniteLongueurOrigine");
        pnlDimension.add(lbUniteLongueurOrigine, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleMultiplier3 ----
        lbSymboleMultiplier3.setText("X");
        lbSymboleMultiplier3.setHorizontalAlignment(SwingConstants.CENTER);
        lbSymboleMultiplier3.setMinimumSize(new Dimension(9, 30));
        lbSymboleMultiplier3.setMaximumSize(new Dimension(9, 30));
        lbSymboleMultiplier3.setPreferredSize(new Dimension(9, 30));
        lbSymboleMultiplier3.setName("lbSymboleMultiplier3");
        pnlDimension.add(lbSymboleMultiplier3, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLargeurOrigine ----
        lbLargeurOrigine.setText("Largeur");
        lbLargeurOrigine.setMinimumSize(new Dimension(100, 30));
        lbLargeurOrigine.setPreferredSize(new Dimension(100, 30));
        lbLargeurOrigine.setMaximumSize(new Dimension(100, 30));
        lbLargeurOrigine.setName("lbLargeurOrigine");
        pnlDimension.add(lbLargeurOrigine, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfLargeurOrigine ----
        tfLargeurOrigine.setMaximumSize(new Dimension(90, 30));
        tfLargeurOrigine.setMinimumSize(new Dimension(90, 30));
        tfLargeurOrigine.setPreferredSize(new Dimension(90, 30));
        tfLargeurOrigine.setEnabled(false);
        tfLargeurOrigine.setHorizontalAlignment(SwingConstants.TRAILING);
        tfLargeurOrigine.setName("tfLargeurOrigine");
        tfLargeurOrigine.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLargeurFocusLost(e);
          }
        });
        pnlDimension.add(tfLargeurOrigine, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteLargeurOrigine ----
        lbUniteLargeurOrigine.setText("Ul");
        lbUniteLargeurOrigine.setName("lbUniteLargeurOrigine");
        pnlDimension.add(lbUniteLargeurOrigine, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleMultiplier4 ----
        lbSymboleMultiplier4.setText("X");
        lbSymboleMultiplier4.setHorizontalAlignment(SwingConstants.CENTER);
        lbSymboleMultiplier4.setMinimumSize(new Dimension(9, 30));
        lbSymboleMultiplier4.setMaximumSize(new Dimension(9, 30));
        lbSymboleMultiplier4.setPreferredSize(new Dimension(9, 30));
        lbSymboleMultiplier4.setName("lbSymboleMultiplier4");
        pnlDimension.add(lbSymboleMultiplier4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbHauteurOrigine ----
        lbHauteurOrigine.setText(" Hauteur");
        lbHauteurOrigine.setMinimumSize(new Dimension(100, 30));
        lbHauteurOrigine.setPreferredSize(new Dimension(100, 30));
        lbHauteurOrigine.setMaximumSize(new Dimension(100, 30));
        lbHauteurOrigine.setName("lbHauteurOrigine");
        pnlDimension.add(lbHauteurOrigine, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfHauteurOrigine ----
        tfHauteurOrigine.setMaximumSize(new Dimension(90, 30));
        tfHauteurOrigine.setMinimumSize(new Dimension(90, 30));
        tfHauteurOrigine.setPreferredSize(new Dimension(90, 30));
        tfHauteurOrigine.setEnabled(false);
        tfHauteurOrigine.setHorizontalAlignment(SwingConstants.TRAILING);
        tfHauteurOrigine.setName("tfHauteurOrigine");
        tfHauteurOrigine.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfHauteurFocusLost(e);
          }
        });
        pnlDimension.add(tfHauteurOrigine, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteHauteurOrigine ----
        lbUniteHauteurOrigine.setText("UH");
        lbUniteHauteurOrigine.setName("lbUniteHauteurOrigine");
        pnlDimension.add(lbUniteHauteurOrigine, new GridBagConstraints(10, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDimension,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlDecoupe ========
      {
        pnlDecoupe.setOpaque(false);
        pnlDecoupe.setPreferredSize(new Dimension(600, 90));
        pnlDecoupe.setMinimumSize(new Dimension(600, 90));
        pnlDecoupe.setMaximumSize(new Dimension(600, 90));
        pnlDecoupe.setTitre("D\u00e9coupe");
        pnlDecoupe.setName("pnlDecoupe");
        pnlDecoupe.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDecoupe.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDecoupe.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlDecoupe.getLayout()).columnWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlDecoupe.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbLongueur ----
        lbLongueur.setText("Longueur");
        lbLongueur.setPreferredSize(new Dimension(100, 30));
        lbLongueur.setMinimumSize(new Dimension(100, 30));
        lbLongueur.setMaximumSize(new Dimension(100, 30));
        lbLongueur.setName("lbLongueur");
        pnlDecoupe.add(lbLongueur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfLongueur ----
        tfLongueur.setMaximumSize(new Dimension(90, 30));
        tfLongueur.setMinimumSize(new Dimension(90, 30));
        tfLongueur.setPreferredSize(new Dimension(90, 30));
        tfLongueur.setHorizontalAlignment(SwingConstants.TRAILING);
        tfLongueur.setName("tfLongueur");
        tfLongueur.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLongueurFocusLost(e);
          }
        });
        tfLongueur.addKeyListener(new KeyAdapter() {
          @Override
          public void keyPressed(KeyEvent e) {
            tfLongueurKeyPressed(e);
          }
        });
        pnlDecoupe.add(tfLongueur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteLongueur ----
        lbUniteLongueur.setText("UL");
        lbUniteLongueur.setName("lbUniteLongueur");
        pnlDecoupe.add(lbUniteLongueur, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleMultiplier1 ----
        lbSymboleMultiplier1.setText("X");
        lbSymboleMultiplier1.setHorizontalAlignment(SwingConstants.CENTER);
        lbSymboleMultiplier1.setMinimumSize(new Dimension(9, 30));
        lbSymboleMultiplier1.setMaximumSize(new Dimension(9, 30));
        lbSymboleMultiplier1.setPreferredSize(new Dimension(9, 30));
        lbSymboleMultiplier1.setName("lbSymboleMultiplier1");
        pnlDecoupe.add(lbSymboleMultiplier1, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLargeur ----
        lbLargeur.setText("Largeur");
        lbLargeur.setMinimumSize(new Dimension(100, 30));
        lbLargeur.setPreferredSize(new Dimension(100, 30));
        lbLargeur.setMaximumSize(new Dimension(100, 30));
        lbLargeur.setName("lbLargeur");
        pnlDecoupe.add(lbLargeur, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfLargeur ----
        tfLargeur.setMaximumSize(new Dimension(90, 30));
        tfLargeur.setMinimumSize(new Dimension(90, 30));
        tfLargeur.setPreferredSize(new Dimension(90, 30));
        tfLargeur.setHorizontalAlignment(SwingConstants.TRAILING);
        tfLargeur.setName("tfLargeur");
        tfLargeur.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLargeurFocusLost(e);
          }
        });
        pnlDecoupe.add(tfLargeur, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteLargeur ----
        lbUniteLargeur.setText("Ul");
        lbUniteLargeur.setName("lbUniteLargeur");
        pnlDecoupe.add(lbUniteLargeur, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleMultiplier2 ----
        lbSymboleMultiplier2.setText("X");
        lbSymboleMultiplier2.setHorizontalAlignment(SwingConstants.CENTER);
        lbSymboleMultiplier2.setMinimumSize(new Dimension(9, 30));
        lbSymboleMultiplier2.setMaximumSize(new Dimension(9, 30));
        lbSymboleMultiplier2.setPreferredSize(new Dimension(9, 30));
        lbSymboleMultiplier2.setName("lbSymboleMultiplier2");
        pnlDecoupe.add(lbSymboleMultiplier2, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbHauteur ----
        lbHauteur.setText(" Hauteur");
        lbHauteur.setPreferredSize(new Dimension(100, 30));
        lbHauteur.setMinimumSize(new Dimension(100, 30));
        lbHauteur.setMaximumSize(new Dimension(100, 30));
        lbHauteur.setName("lbHauteur");
        pnlDecoupe.add(lbHauteur, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfHauteur ----
        tfHauteur.setMaximumSize(new Dimension(90, 30));
        tfHauteur.setMinimumSize(new Dimension(90, 30));
        tfHauteur.setPreferredSize(new Dimension(90, 30));
        tfHauteur.setHorizontalAlignment(SwingConstants.TRAILING);
        tfHauteur.setName("tfHauteur");
        tfHauteur.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfHauteurFocusLost(e);
          }
        });
        pnlDecoupe.add(tfHauteur, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteHauteur ----
        lbUniteHauteur.setText("UH");
        lbUniteHauteur.setName("lbUniteHauteur");
        pnlDecoupe.add(lbUniteHauteur, new GridBagConstraints(10, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlDecoupe,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlResultat ========
      {
        pnlResultat.setTitre("R\u00e9sultat de la d\u00e9coupe");
        pnlResultat.setPreferredSize(new Dimension(600, 90));
        pnlResultat.setMinimumSize(new Dimension(600, 90));
        pnlResultat.setMaximumSize(new Dimension(600, 90));
        pnlResultat.setName("pnlResultat");
        pnlResultat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlResultat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlResultat.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlResultat.getLayout()).columnWeights =
            new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlResultat.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbPrixNet ----
        lbPrixNet.setText("Prix net");
        lbPrixNet.setHorizontalAlignment(SwingConstants.RIGHT);
        lbPrixNet.setMinimumSize(new Dimension(100, 30));
        lbPrixNet.setPreferredSize(new Dimension(100, 30));
        lbPrixNet.setMaximumSize(new Dimension(100, 30));
        lbPrixNet.setName("lbPrixNet");
        pnlResultat.add(lbPrixNet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfPrixNet ----
        tfPrixNet.setMaximumSize(new Dimension(90, 30));
        tfPrixNet.setMinimumSize(new Dimension(90, 30));
        tfPrixNet.setPreferredSize(new Dimension(90, 30));
        tfPrixNet.setEnabled(false);
        tfPrixNet.setHorizontalAlignment(SwingConstants.TRAILING);
        tfPrixNet.setName("tfPrixNet");
        pnlResultat.add(tfPrixNet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteLongueur2 ----
        lbUniteLongueur2.setName("lbUniteLongueur2");
        pnlResultat.add(lbUniteLongueur2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleMultiplier ----
        lbSymboleMultiplier.setText("X");
        lbSymboleMultiplier.setHorizontalAlignment(SwingConstants.CENTER);
        lbSymboleMultiplier.setMinimumSize(new Dimension(9, 30));
        lbSymboleMultiplier.setMaximumSize(new Dimension(9, 30));
        lbSymboleMultiplier.setPreferredSize(new Dimension(9, 30));
        lbSymboleMultiplier.setName("lbSymboleMultiplier");
        pnlResultat.add(lbSymboleMultiplier, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDimension ----
        lbDimension.setText("Volume");
        lbDimension.setMinimumSize(new Dimension(100, 30));
        lbDimension.setPreferredSize(new Dimension(100, 30));
        lbDimension.setMaximumSize(new Dimension(100, 30));
        lbDimension.setName("lbDimension");
        pnlResultat.add(lbDimension, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfDimension ----
        tfDimension.setMaximumSize(new Dimension(90, 30));
        tfDimension.setMinimumSize(new Dimension(90, 30));
        tfDimension.setPreferredSize(new Dimension(90, 30));
        tfDimension.setEnabled(false);
        tfDimension.setHorizontalAlignment(SwingConstants.TRAILING);
        tfDimension.setName("tfDimension");
        pnlResultat.add(tfDimension, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbUniteUV ----
        lbUniteUV.setText("UV");
        lbUniteUV.setName("lbUniteUV");
        pnlResultat.add(lbUniteUV, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbSymboleEgal ----
        lbSymboleEgal.setText("=");
        lbSymboleEgal.setFont(lbSymboleEgal.getFont().deriveFont(lbSymboleEgal.getFont().getSize() + 2f));
        lbSymboleEgal.setHorizontalAlignment(SwingConstants.RIGHT);
        lbSymboleEgal.setMinimumSize(new Dimension(9, 30));
        lbSymboleEgal.setMaximumSize(new Dimension(9, 30));
        lbSymboleEgal.setPreferredSize(new Dimension(9, 30));
        lbSymboleEgal.setName("lbSymboleEgal");
        pnlResultat.add(lbSymboleEgal, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbMontant ----
        lbMontant.setText("Montant HT");
        lbMontant.setMinimumSize(new Dimension(100, 30));
        lbMontant.setPreferredSize(new Dimension(100, 30));
        lbMontant.setMaximumSize(new Dimension(100, 30));
        lbMontant.setName("lbMontant");
        pnlResultat.add(lbMontant, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfMontantTotal ----
        tfMontantTotal.setMinimumSize(new Dimension(90, 30));
        tfMontantTotal.setPreferredSize(new Dimension(90, 30));
        tfMontantTotal.setMaximumSize(new Dimension(90, 30));
        tfMontantTotal.setEnabled(false);
        tfMontantTotal.setHorizontalAlignment(SwingConstants.TRAILING);
        tfMontantTotal.setName("tfMontantTotal");
        pnlResultat.add(tfMontantTotal, new GridBagConstraints(9, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlResultat,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlQuantite;
  private SNLabelChamp lbNombre;
  private SNTexte tfNombre;
  private SNPanelTitre pnlDimension;
  private SNLabelChamp lbLongueurOrigine;
  private SNTexte tfLongueurOrigine;
  private SNLabelUnite lbUniteLongueurOrigine;
  private SNLabelChamp lbSymboleMultiplier3;
  private SNLabelChamp lbLargeurOrigine;
  private SNTexte tfLargeurOrigine;
  private SNLabelUnite lbUniteLargeurOrigine;
  private SNLabelChamp lbSymboleMultiplier4;
  private SNLabelChamp lbHauteurOrigine;
  private SNTexte tfHauteurOrigine;
  private SNLabelUnite lbUniteHauteurOrigine;
  private SNPanelTitre pnlDecoupe;
  private SNLabelChamp lbLongueur;
  private SNTexte tfLongueur;
  private SNLabelUnite lbUniteLongueur;
  private SNLabelChamp lbSymboleMultiplier1;
  private SNLabelChamp lbLargeur;
  private SNTexte tfLargeur;
  private SNLabelUnite lbUniteLargeur;
  private SNLabelChamp lbSymboleMultiplier2;
  private SNLabelChamp lbHauteur;
  private SNTexte tfHauteur;
  private SNLabelUnite lbUniteHauteur;
  private SNPanelTitre pnlResultat;
  private SNLabelChamp lbPrixNet;
  private SNTexte tfPrixNet;
  private SNLabelUnite lbUniteLongueur2;
  private SNLabelChamp lbSymboleMultiplier;
  private SNLabelChamp lbDimension;
  private SNTexte tfDimension;
  private SNLabelUnite lbUniteUV;
  private SNLabelChamp lbSymboleEgal;
  private SNLabelChamp lbMontant;
  private SNTexte tfMontantTotal;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
