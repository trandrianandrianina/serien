/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.propositionrendumonnaieacompte;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.gescom.vente.reglement.Acompte;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleProposerRenduMonnaieAcompte extends AbstractModeleDialogue {
  // Variables
  private boolean rendreMonnaie = false;
  private Acompte acompte = null;
  private ModeleComptoir modeleParent = null;
  
  /**
   * Constructeur.
   */
  public ModeleProposerRenduMonnaieAcompte(SessionBase pSession, Acompte pAcompte) {
    super(pSession);
    acompte = pAcompte;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    rendreMonnaie = false;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne si l'acompte est obligatoire.
   */
  public boolean isAcompteObligatoire() {
    if (acompte != null && acompte.isObligatoire()) {
      return true;
    }
    return false;
  }
  
  /**
   * Modifie l'option de rendu monnaie.
   */
  public void modifierRendreMonnaie(boolean pRendreMonnaie) {
    if (pRendreMonnaie == rendreMonnaie) {
      return;
    }
    rendreMonnaie = pRendreMonnaie;
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les variables du modèle.
   */
  private void effacerVariables() {
    rendreMonnaie = false;
  }
  
  // -- Accesseurs
  
  public ModeleComptoir getModeleParent() {
    return modeleParent;
  }
  
  public boolean isRendreMonnaie() {
    return rendreMonnaie;
  }
  
}
