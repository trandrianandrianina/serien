/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.stocksarticle;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.math.BigDecimal;
import java.math.RoundingMode;

import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.StockComptoir;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueAffichageStockArticle extends AbstractVuePanel<ModeleAffichageStockArticle> {
  // Constantes
  private static final String[] TITRE_LISTE_TOUSDEPOTS_VENTE =
      new String[] { "Etablissement", "Magasin", "Physique", "Command\u00e9", "<html>Dispo. vente</html>", "Attendu", "Mini", "Maxi" };
  private static final String[] TITRE_LISTE_TOUSDEPOTS_ACHAT =
      new String[] { "Etablissement", "Magasin", "Physique", "Command\u00e9", "Attendu", "<html>Dispo. achat</html>", "Mini", "Maxi" };
  
  private DefaultTableModel tableModelStock = null;
  private JTableCellRendererStockArticle rendererStock = new JTableCellRendererStockArticle();
  
  /**
   * Constructeur.
   */
  public VueAffichageStockArticle(ModeleAffichageStockArticle pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    Boolean isModeAchat = getModele().getEnumTypeStockDisponible().equals(EnumTypeStockDisponible.TYPE_ACHAT);
    if (isModeAchat) {
      tblTousDepots.personnaliserAspect(TITRE_LISTE_TOUSDEPOTS_ACHAT, new int[] { 150, 150, 100, 100, 100, 100, 100, 100 },
          new int[] { -1, -1, 100, 100, 100, 100, 100, 100 }, new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE },
          14);
      tableModelStock = new DefaultTableModel(null, TITRE_LISTE_TOUSDEPOTS_ACHAT) {
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
          return (columnIndex < 0);
        }
      };
    }
    else {
      tblTousDepots.personnaliserAspect(TITRE_LISTE_TOUSDEPOTS_VENTE, new int[] { 150, 150, 100, 100, 100, 100, 100, 100 },
          new int[] { -1, -1, 100, 100, 100, 100, 100, 100 }, new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE },
          14);
      tableModelStock = new DefaultTableModel(null, TITRE_LISTE_TOUSDEPOTS_VENTE) {
        @Override
        public boolean isCellEditable(int rowIndex, int columnIndex) {
          return (columnIndex < 0);
        }
      };
    }
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpTousDepots.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        // scpAttenduCommandeStateChanged(e);
      }
    });
    scpTousDepots.setBackground(Color.WHITE);
    scpTousDepots.getViewport().setBackground(Color.WHITE);
    
    tfTousDepotsStockPhysique.setText("0");
    tfTousDepotsCommande.setText("0");
    tfTousDepotsAttendu.setText("0");
    tfTousDepotsStockTotal.setText("0");
  }
  
  @Override
  public void rafraichir() {
    rafraichirMessage();
    rafraichirLibelleUnite();
    rafraichirListe();
    rafraichirStockPhysique();
    rafraichirStockAttendu();
    rafraichirStockCommande();
    rafraichirStockDisponible();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessage() {
    Message message = getModele().getMessage();
    if (message == null) {
      lbMessage.setVisible(false);
      return;
    }
    if (message.isImportanceHaute()) {
      lbMessage.setForeground(Color.RED);
    }
    else {
      lbMessage.setForeground(Color.BLACK);
    }
    lbMessage.setText(message.getTexte());
    lbMessage.setVisible(true);
  }
  
  private void rafraichirLibelleUnite() {
    ListeUnite listeUnite = getModele().getListeUnite();
    Article article = getModele().getArticle();
    if (listeUnite != null && article != null && article.getIdUS() != null) {
      snUniteStock.setText(listeUnite.getLibelleUnite(article.getIdUS()));
    }
    else {
      snUniteStock.setText("");
    }
  }
  
  private void rafraichirStockPhysique() {
    ListeStockComptoir listeStock = getModele().getListeStock();
    if (listeStock != null) {
      BigDecimal stockPhysique =
          listeStock.getQuantitePhysiqueTotale().setScale(getModele().getPrecisionUniteStock(), RoundingMode.HALF_UP);
      tfTousDepotsStockPhysique.setText(Constantes.formater(stockPhysique, false));
      
      // Afficher en rouge si le total est inférieur ou égal à 0
      if (stockPhysique.compareTo(BigDecimal.ZERO) <= 0) {
        tfTousDepotsStockTotal.setForeground(Color.RED);
      }
      else {
        tfTousDepotsStockTotal.setForeground(Color.BLACK);
      }
    }
    else {
      tfTousDepotsStockPhysique.setText("");
      tfTousDepotsStockTotal.setForeground(Color.BLACK);
    }
  }
  
  private void rafraichirStockCommande() {
    ListeStockComptoir listeStock = getModele().getListeStock();
    if (listeStock != null) {
      tfTousDepotsCommande.setText(Constantes.formater(listeStock.getQuantiteCommandeeTotale(), false));
    }
    else {
      tfTousDepotsCommande.setText("");
    }
  }
  
  private void rafraichirStockAttendu() {
    ListeStockComptoir listeStock = getModele().getListeStock();
    if (listeStock != null) {
      tfTousDepotsAttendu.setText(Constantes.formater(listeStock.getQuantiteAttendueTotale(), false));
    }
    else {
      tfTousDepotsAttendu.setText("");
    }
  }
  
  /**
   * Rafraichir la colonne de stock disponible
   */
  private void rafraichirStockDisponible() {
    ListeStockComptoir listeStock = getModele().getListeStock();
    if (listeStock != null) {
      BigDecimal stockDisponible = null;
      
      if (getModele().getEnumTypeStockDisponible().equals(EnumTypeStockDisponible.TYPE_ACHAT)) {
        stockDisponible = listeStock.getQuantiteDisponibleAchatTotale();
        lbStockDisponible.setText("Disponible achat");
      }
      else {
        stockDisponible = listeStock.getQuantiteDisponibleVenteTotale();
        lbStockDisponible.setText("Disponible vente");
      }
      
      tfTousDepotsStockTotal.setText(Constantes.formater(stockDisponible, false));
      
      // Couleur normale du disponible
      tfTousDepotsStockTotal.setForeground(Color.BLACK);
      
      // si le total disponible est inférieur ou égal à 0 on le met en rouge
      if (stockDisponible.compareTo(BigDecimal.ZERO) <= 0) {
        tfTousDepotsStockTotal.setForeground(Color.RED);
      }
      
    }
    else {
      tfTousDepotsStockTotal.setText("");
      tfTousDepotsStockTotal.setForeground(Color.BLACK);
    }
  }
  
  /**
   * Affichage des données du sous-onglet tous les dépots
   */
  private void rafraichirListe() {
    ListeStockComptoir listeStock = getModele().getListeStock();
    String[][] donnees = null;
    Boolean isModeAchat = getModele().getEnumTypeStockDisponible().equals(EnumTypeStockDisponible.TYPE_ACHAT);
    
    if (listeStock != null) {
      donnees = new String[listeStock.size()][tblTousDepots.getColumnCount()];
      for (int ligne = 0; ligne < listeStock.size(); ligne++) {
        StockComptoir stock = listeStock.get(ligne);
        if (stock != null) {
          // On formate avec le bon nombre de décimale les quantités
          donnees[ligne][0] = getModele().getListeEtablissement().getRaisonSocialeEtablissement(stock.getId().getIdEtablissement());
          if (getModele().getListeMagasin() != null) {
            donnees[ligne][1] = getModele().getListeMagasin().getNomMagasin(stock.getId().getIdMagasin());
          }
          donnees[ligne][2] = getModele().getFormaterStockPhysique(stock);
          donnees[ligne][3] = Constantes.formater(stock.getQuantiteCommandee(), false);
          if (isModeAchat) {
            donnees[ligne][4] = Constantes.formater(stock.getQuantiteAttendue(), false);
            donnees[ligne][5] = Constantes.formater(stock.getQuantiteDisponibleAchat(), false);
          }
          else {
            donnees[ligne][4] = Constantes.formater(stock.getQuantiteDisponibleVente(), false);
            donnees[ligne][5] = Constantes.formater(stock.getQuantiteAttendue(), false);
          }
          donnees[ligne][6] = Constantes.formater(stock.getQuantiteMinimum(), false);
          donnees[ligne][7] = Constantes.formater(stock.getQuantiteMax(), false);
        }
      }
    }
    
    // Mettre à jour les données
    tableModelStock.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelStock.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblTousDepots.getModel().equals(tableModelStock)) {
      tblTousDepots.setModel(tableModelStock);
      tblTousDepots.setGridColor(new Color(204, 204, 204));
      tblTousDepots.setDefaultRenderer(Object.class, rendererStock);
      tblTousDepots.trierColonnes();
    }
    
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPanelPrincipal = new SNPanelContenu();
    lbMessage = new SNLabelChamp();
    pnlUniteStock = new SNPanel();
    lbUniteStock = new SNLabelChamp();
    snUniteStock = new SNTexte();
    scpTousDepots = new JScrollPane();
    tblTousDepots = new NRiTable();
    pnlTotauxStocks = new SNPanelTitre();
    lbStockPhysique = new SNLabelChamp();
    tfTousDepotsStockPhysique = new SNTexte();
    lbStockCommande = new SNLabelChamp();
    tfTousDepotsCommande = new SNTexte();
    lbStockAttendu = new SNLabelChamp();
    tfTousDepotsAttendu = new SNTexte();
    lbStockDisponible = new SNLabelChamp();
    tfTousDepotsStockTotal = new SNTexte();
    
    // ======== this ========
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPanelPrincipal ========
    {
      pnlPanelPrincipal.setMinimumSize(new Dimension(917, 515));
      pnlPanelPrincipal.setMaximumSize(new Dimension(917, 515));
      pnlPanelPrincipal.setName("pnlPanelPrincipal");
      pnlPanelPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWidths = new int[] { 880, 0 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 227, 69, 0 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
      
      // ---- lbMessage ----
      lbMessage.setHorizontalAlignment(SwingConstants.LEFT);
      lbMessage.setName("lbMessage");
      pnlPanelPrincipal.add(lbMessage,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlUniteStock ========
      {
        pnlUniteStock.setName("pnlUniteStock");
        pnlUniteStock.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlUniteStock.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlUniteStock.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlUniteStock.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlUniteStock.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbUniteStock ----
        lbUniteStock.setText("Unite de stock");
        lbUniteStock.setName("lbUniteStock");
        pnlUniteStock.add(lbUniteStock, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snUniteStock ----
        snUniteStock.setEditable(false);
        snUniteStock.setEnabled(false);
        snUniteStock.setName("snUniteStock");
        pnlUniteStock.add(snUniteStock, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPanelPrincipal.add(pnlUniteStock,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpTousDepots ========
      {
        scpTousDepots.setName("scpTousDepots");
        
        // ---- tblTousDepots ----
        tblTousDepots.setShowVerticalLines(true);
        tblTousDepots.setShowHorizontalLines(true);
        tblTousDepots.setBackground(Color.white);
        tblTousDepots.setOpaque(false);
        tblTousDepots.setRowHeight(20);
        tblTousDepots.setGridColor(new Color(204, 204, 204));
        tblTousDepots.setName("tblTousDepots");
        scpTousDepots.setViewportView(tblTousDepots);
      }
      pnlPanelPrincipal.add(scpTousDepots,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTotauxStocks ========
      {
        pnlTotauxStocks.setTitre("Totaux");
        pnlTotauxStocks.setBackground(new Color(239, 239, 222));
        pnlTotauxStocks.setName("pnlTotauxStocks");
        pnlTotauxStocks.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTotauxStocks.getLayout()).columnWidths = new int[] { 76, 100, 75, 100, 65, 100, 84, 106, 0 };
        ((GridBagLayout) pnlTotauxStocks.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlTotauxStocks.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTotauxStocks.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbStockPhysique ----
        lbStockPhysique.setText("Physique");
        lbStockPhysique.setHorizontalAlignment(SwingConstants.RIGHT);
        lbStockPhysique.setMaximumSize(new Dimension(80, 30));
        lbStockPhysique.setMinimumSize(new Dimension(80, 30));
        lbStockPhysique.setPreferredSize(new Dimension(80, 30));
        lbStockPhysique.setName("lbStockPhysique");
        pnlTotauxStocks.add(lbStockPhysique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfTousDepotsStockPhysique ----
        tfTousDepotsStockPhysique.setEditable(false);
        tfTousDepotsStockPhysique.setEnabled(false);
        tfTousDepotsStockPhysique.setHorizontalAlignment(SwingConstants.TRAILING);
        tfTousDepotsStockPhysique.setName("tfTousDepotsStockPhysique");
        pnlTotauxStocks.add(tfTousDepotsStockPhysique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbStockCommande ----
        lbStockCommande.setText("Command\u00e9");
        lbStockCommande.setHorizontalAlignment(SwingConstants.RIGHT);
        lbStockCommande.setMaximumSize(new Dimension(80, 30));
        lbStockCommande.setMinimumSize(new Dimension(80, 30));
        lbStockCommande.setPreferredSize(new Dimension(80, 30));
        lbStockCommande.setName("lbStockCommande");
        pnlTotauxStocks.add(lbStockCommande, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfTousDepotsCommande ----
        tfTousDepotsCommande.setEditable(false);
        tfTousDepotsCommande.setEnabled(false);
        tfTousDepotsCommande.setHorizontalAlignment(SwingConstants.TRAILING);
        tfTousDepotsCommande.setName("tfTousDepotsCommande");
        pnlTotauxStocks.add(tfTousDepotsCommande, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbStockAttendu ----
        lbStockAttendu.setText("Attendu");
        lbStockAttendu.setHorizontalAlignment(SwingConstants.RIGHT);
        lbStockAttendu.setMaximumSize(new Dimension(80, 30));
        lbStockAttendu.setMinimumSize(new Dimension(80, 30));
        lbStockAttendu.setPreferredSize(new Dimension(80, 30));
        lbStockAttendu.setName("lbStockAttendu");
        pnlTotauxStocks.add(lbStockAttendu, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfTousDepotsAttendu ----
        tfTousDepotsAttendu.setEditable(false);
        tfTousDepotsAttendu.setEnabled(false);
        tfTousDepotsAttendu.setHorizontalAlignment(SwingConstants.TRAILING);
        tfTousDepotsAttendu.setName("tfTousDepotsAttendu");
        pnlTotauxStocks.add(tfTousDepotsAttendu, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbStockDisponible ----
        lbStockDisponible.setText("Disponible vente");
        lbStockDisponible.setHorizontalAlignment(SwingConstants.RIGHT);
        lbStockDisponible.setMaximumSize(new Dimension(130, 30));
        lbStockDisponible.setMinimumSize(new Dimension(80, 30));
        lbStockDisponible.setPreferredSize(new Dimension(130, 30));
        lbStockDisponible.setName("lbStockDisponible");
        pnlTotauxStocks.add(lbStockDisponible, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfTousDepotsStockTotal ----
        tfTousDepotsStockTotal.setEditable(false);
        tfTousDepotsStockTotal.setEnabled(false);
        tfTousDepotsStockTotal.setHorizontalAlignment(SwingConstants.TRAILING);
        tfTousDepotsStockTotal.setName("tfTousDepotsStockTotal");
        pnlTotauxStocks.add(tfTousDepotsStockTotal, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPanelPrincipal.add(pnlTotauxStocks,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPanelPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlPanelPrincipal;
  private SNLabelChamp lbMessage;
  private SNPanel pnlUniteStock;
  private SNLabelChamp lbUniteStock;
  private SNTexte snUniteStock;
  private JScrollPane scpTousDepots;
  private NRiTable tblTousDepots;
  private SNPanelTitre pnlTotauxStocks;
  private SNLabelChamp lbStockPhysique;
  private SNTexte tfTousDepotsStockPhysique;
  private SNLabelChamp lbStockCommande;
  private SNTexte tfTousDepotsCommande;
  private SNLabelChamp lbStockAttendu;
  private SNTexte tfTousDepotsAttendu;
  private SNLabelChamp lbStockDisponible;
  private SNTexte tfTousDepotsStockTotal;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
