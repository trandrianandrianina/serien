/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletStock extends SNPanelEcranRPG implements InterfaceVueOnglet {
  // Constantes
  public static final int SOUS_ONGLET_ETAT_STOCKS = 0;
  public static final int SOUS_ONGLET_MOUVEMENTS = 1;
  public static final int SOUS_ONGLET_ATTENDUS = 2;
  
  // Variables
  private ModeleDetailLigneArticle modele = null;
  private ListeStockComptoir listeStockParMagasin = null;
  private ListeStockComptoir listeStockEtablissement = null;
  private boolean tousDepotEnPlace = false;
  private boolean mouvementStockEnPlace = false;
  private boolean attenducommandeEnPlace = false;
  private boolean executerEvenements = false;
  
  /**
   * Constructeur.
   */
  public VueOngletStock(ModeleDetailLigneArticle acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  @Override
  public void rafraichir() {
    executerEvenements = false;
    
    switch (modele.getSousOngletActif()) {
      case SOUS_ONGLET_ETAT_STOCKS:
        rafraichirTousDepots();
        break;
      
      case SOUS_ONGLET_MOUVEMENTS:
        rafraichirMouvements();
        break;
      
      case SOUS_ONGLET_ATTENDUS:
        rafraichirAttenduCommande(modele.getListeLigneAttenduCommande());
        break;
      
      default:
        rafraichirTousDepots();
        break;
    }
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  
  /**
   * Affichage des données du sous-onglet mouvements
   */
  private void rafraichirMouvements() {
    if (modele.getVueMouvements() == null) {
      return;
    }
    if (!mouvementStockEnPlace) {
      mouvementStockEnPlace = true;
      pnlMouvementsDeStocks.add(modele.getVueMouvements(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  /**
   * Affichage des données du sous-onglet tous les dépots
   */
  private void rafraichirTousDepots() {
    if (modele.getVueStock() == null) {
      return;
    }
    if (!tousDepotEnPlace) {
      tousDepotEnPlace = true;
      pnlStocksTousDepots.add(modele.getVueStock(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  /**
   * Affichage des données du sous-onglet attendu commandé
   */
  private void rafraichirAttenduCommande(List<LigneAttenduCommande> attcmd) {
    if (modele.getVueAttendus() == null) {
      return;
    }
    if (!attenducommandeEnPlace) {
      attenducommandeEnPlace = true;
      pnlStocksAttenduCommande.add(modele.getVueAttendus(), BorderLayout.CENTER);
      pnlStocksTousDepots.getComponent(0).requestFocus();
    }
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpStocksMouseClicked(MouseEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierSousOngletStock(tbpContenu.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    tbpContenu = new JTabbedPane();
    pnlStocksTousDepots = new SNPanel();
    pnlMouvementsDeStocks = new SNPanel();
    pnlStocksAttenduCommande = new SNPanel();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
      
      // ======== tbpContenu ========
      {
        tbpContenu.setTabPlacement(SwingConstants.LEFT);
        tbpContenu.setMinimumSize(new Dimension(600, 900));
        tbpContenu.setName("tbpContenu");
        tbpContenu.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tbpStocksMouseClicked(e);
          }
        });
        
        // ======== pnlStocksTousDepots ========
        {
          pnlStocksTousDepots.setOpaque(false);
          pnlStocksTousDepots.setName("pnlStocksTousDepots");
          pnlStocksTousDepots.setLayout(new BorderLayout());
        }
        tbpContenu.addTab("<html><center>Etat des<br />Stocks</center></html>", pnlStocksTousDepots);
        
        // ======== pnlMouvementsDeStocks ========
        {
          pnlMouvementsDeStocks.setOpaque(false);
          pnlMouvementsDeStocks.setName("pnlMouvementsDeStocks");
          pnlMouvementsDeStocks.setLayout(new BorderLayout());
        }
        tbpContenu.addTab("<html><center>Mouvements<br />de stocks</center></html>", pnlMouvementsDeStocks);
        
        // ======== pnlStocksAttenduCommande ========
        {
          pnlStocksAttenduCommande.setOpaque(false);
          pnlStocksAttenduCommande.setName("pnlStocksAttenduCommande");
          pnlStocksAttenduCommande.setLayout(new BorderLayout());
        }
        tbpContenu.addTab("<html><center>Attendu<br />command\u00e9</center></html>", pnlStocksAttenduCommande);
      }
      pnlContenu.add(tbpContenu,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private JTabbedPane tbpContenu;
  private SNPanel pnlStocksTousDepots;
  private SNPanel pnlMouvementsDeStocks;
  private SNPanel pnlStocksAttenduCommande;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
