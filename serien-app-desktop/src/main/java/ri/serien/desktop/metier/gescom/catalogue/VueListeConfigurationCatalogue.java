/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue liste des configurations de catalogues fournisseurs.
 */
public class VueListeConfigurationCatalogue extends AbstractVuePanel<ModeleConfigurationCatalogue> {
  // Variables
  private static final String BOUTON_CREER = "Créer configuration";
  private static final String[] TITRE_LISTE_CONFIGURATION =
      new String[] { "Id", "Code Fournisseur", "Raison sociale fournisseur", "Libellé configuration", "Nom du fichier" };
  
  private DefaultTableModel tableModelListeConfiguration = null;
  private JTableCellRendererConfig rendererListeConfiguration = new JTableCellRendererConfig();
  
  /**
   * Constructeur avec modele
   */
  public VueListeConfigurationCatalogue(ModeleConfigurationCatalogue pModele) {
    super(pModele);
  }
  
  /**
   * Construire l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    scpListeConfigurationCatalogue.getViewport().setBackground(Color.WHITE);
    scpListeConfigurationCatalogue.setBackground(Color.white);
    
    tableModelListeConfiguration = new DefaultTableModel(null, TITRE_LISTE_CONFIGURATION) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    if (!tblListeConfigurationCatalogue.getModel().equals(tableModelListeConfiguration)) {
      tblListeConfigurationCatalogue.setModel(tableModelListeConfiguration);
      tblListeConfigurationCatalogue.setGridColor(new Color(204, 204, 204));
      tblListeConfigurationCatalogue.setDefaultRenderer(Object.class, rendererListeConfiguration);
    }
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREER, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirEtablissement();
    rafraichirFournisseur();
    rafraichirListeConfigurationCatalogue();
    rafraichirBoutonConsulter();
  }
  
  /**
   * Rafraichit la combo box des établissement et l'établissement actif
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getModele().getSession());
    snEtablissement.charger(false);
    snEtablissement.setIdSelection(getModele().getIdEtablissement());
    snEtablissement.setEnabled(isDonneesChargees());
  }
  
  /**
   * Rafraichit la combo box des fournisseurs et le fournisseur actif
   */
  private void rafraichirFournisseur() {
    snFournisseur.setSession(getModele().getSession());
    snFournisseur.setIdEtablissement(getModele().getIdEtablissement());
    snFournisseur.charger(false);
    snFournisseur.setSelection(getModele().getFournisseurBase());
    snFournisseur.setEnabled(isDonneesChargees());
  }
  
  /**
   * Rafraichit la table de configurations de catalogue filtrée par les critères d'établissement et de fournisseur
   */
  private void rafraichirListeConfigurationCatalogue() {
    if (getModele().getListeConfigurationCatalogue() == null) {
      return;
    }
    
    String[][] donnees = null;
    
    donnees = new String[getModele().getListeConfigurationCatalogue().size()][TITRE_LISTE_CONFIGURATION.length];
    
    int indexConfigEnCours = -1;
    
    for (int i = 0; i < getModele().getListeConfigurationCatalogue().size(); i++) {
      ConfigurationCatalogue config = getModele().getListeConfigurationCatalogue().get(i);
      
      if (getModele().getConfigurationCatalogue() != null) {
        ConfigurationCatalogue configurationCatalogue = getModele().getConfigurationCatalogue();
        if (Constantes.equals(config.getId(), configurationCatalogue.getId())) {
          indexConfigEnCours = i;
        }
      }
      
      // id de la config
      donnees[i][0] = config.getId().toString();
      // Code du fournisseur
      if (config.getIdFournisseur() != null) {
        donnees[i][1] = config.getIdFournisseur().toString();
      }
      else {
        donnees[i][1] = "";
      }
      // raison sociale du fournisseur
      donnees[i][2] = config.getRaisonSocialeFournisseur();
      // libellé de la configuration
      donnees[i][3] = config.getLibelle();
      // nom du fichier
      if (config.getFichierCSV() != null) {
        donnees[i][4] = config.getFichierCSV();
      }
      else {
        donnees[i][4] = "";
      }
    }
    
    tblListeConfigurationCatalogue.mettreAJourDonnees(donnees);
    
    if (indexConfigEnCours > -1) {
      tblListeConfigurationCatalogue.setRowSelectionInterval(indexConfigEnCours, indexConfigEnCours);
    }
  }
  
  /**
   * Rafraichit l'affichage du bouton de validation de la sélection d'une configuration de catalogue
   */
  private void rafraichirBoutonConsulter() {
    // Si une ligne est sélectionnée on dégrise le bouton valider
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, tblListeConfigurationCatalogue.getSelectedRow() > -1);
  }
  
  // Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        getModele().consulter();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER)) {
        getModele().creer();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * On modifie l'établissement du modèle à partir de la sélection d'un établissement dans la combo Box
   */
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      
      getModele().modifierFiltreEtablissement(snEtablissement.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * On modifie le fournisseur du modèle à partir de la sélection d'un fournisseur dans la combo Box
   */
  private void snFournisseurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFiltreFournisseur(snFournisseur.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeConfigurationCatalogueMouseClicked(MouseEvent e) {
    try {
      if (tblListeConfigurationCatalogue.getRowCount() > 0) {
        int ligneSelectionnee = tblListeConfigurationCatalogue.rowAtPoint(e.getPoint());
        getModele().selectionnerConfigurationCatalogue(ligneSelectionnee);
      }
      else {
        getModele().selectionnerConfigurationCatalogue(-1);
      }
      
      rafraichirBoutonConsulter();
      
      if (e.getClickCount() == 2) {
        getModele().consulter();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    panel1 = new JPanel();
    pnlFiltre1 = new JPanel();
    lbFournisseur = new SNLabelChamp();
    snFournisseur = new SNFournisseur();
    pnlFiltre2 = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    snBarreRecherche = new SNBarreRecherche();
    scpListeConfigurationCatalogue = new JScrollPane();
    tblListeConfigurationCatalogue = new NRiTable();
    lbTitre = new JLabel();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 600));
    setPreferredSize(new Dimension(1200, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setMinimumSize(new Dimension(1280, 600));
      pnlPrincipal.setPreferredSize(new Dimension(1280, 600));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 11, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltre1 ========
          {
            pnlFiltre1.setOpaque(false);
            pnlFiltre1.setName("pnlFiltre1");
            pnlFiltre1.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- lbFournisseur ----
            lbFournisseur.setText("Fournisseur");
            lbFournisseur.setName("lbFournisseur");
            pnlFiltre1.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snFournisseur ----
            snFournisseur.setName("snFournisseur");
            snFournisseur.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snFournisseurValueChanged(e);
              }
            });
            pnlFiltre1.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          panel1.add(pnlFiltre1);
          
          // ======== pnlFiltre2 ========
          {
            pnlFiltre2.setOpaque(false);
            pnlFiltre2.setName("pnlFiltre2");
            pnlFiltre2.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement");
            lbEtablissement.setName("lbEtablissement");
            pnlFiltre2.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlFiltre2.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlFiltre2.add(snBarreRecherche, new GridBagConstraints(0, 1, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          panel1.add(pnlFiltre2);
        }
        pnlContenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeConfigurationCatalogue ========
        {
          scpListeConfigurationCatalogue.setPreferredSize(new Dimension(1050, 500));
          scpListeConfigurationCatalogue.setName("scpListeConfigurationCatalogue");
          
          // ---- tblListeConfigurationCatalogue ----
          tblListeConfigurationCatalogue.setName("tblListeConfigurationCatalogue");
          tblListeConfigurationCatalogue.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeConfigurationCatalogueMouseClicked(e);
            }
          });
          scpListeConfigurationCatalogue.setViewportView(tblListeConfigurationCatalogue);
        }
        pnlContenu.add(scpListeConfigurationCatalogue, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbTitre ----
        lbTitre.setText("Configurations correspondant \u00e0 votre recherche");
        lbTitre.setFont(new Font("sansserif", Font.BOLD, 14));
        lbTitre.setName("lbTitre");
        pnlContenu.add(lbTitre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JPanel panel1;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbFournisseur;
  private SNFournisseur snFournisseur;
  private JPanel pnlFiltre2;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNBarreRecherche snBarreRecherche;
  private JScrollPane scpListeConfigurationCatalogue;
  private NRiTable tblListeConfigurationCatalogue;
  private JLabel lbTitre;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
