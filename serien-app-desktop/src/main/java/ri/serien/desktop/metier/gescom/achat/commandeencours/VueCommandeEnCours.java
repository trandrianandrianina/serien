/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.commandeencours;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueCommandeEnCours extends AbstractVueDialogue<ModeleCommandeEnCours> {
  // Constantes
  private static final String[] TITRE_LISTE_DOCUMENTS = new String[] { "Num\u00e9ro", "Date commande", "Date livraison",
      "Quantit\u00e9 UCA", "Prix achat net HT", "Prix de revient HT", "Acheteur" };
  
  // Variables
  
  // Classe interne qui permet de gérer les flèches dans la table tblListe
  class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;
    
    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      validerSelectionLigne(e, actionOriginale);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueCommandeEnCours(ModeleCommandeEnCours pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(700, 400);
    setResizable(false);
    
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    tblListeDocuments.personnaliserAspect(TITRE_LISTE_DOCUMENTS, new int[] { 60, 75, 75, 60, 60, 60, 200 }, new int[] { NRiTable.CENTRE,
        NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE }, 14);
    
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListeDocuments, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListeDocuments.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListeDocuments.getActionMap().put(actionEnter, actionCelluleEnter);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListeDocuments.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeDocumentsStateChanged(e);
      }
    });
    
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessageAlerte();
    rafraichirListe();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessageAlerte() {
    if (getModele().getMessage() != null) {
      lbMessageAlerte.setText("<html>" + getModele().getMessage().getTexte() + "</html>");
    }
    else {
      lbMessageAlerte.setText("");
    }
  }
  
  private void rafraichirListe() {
    ArrayList<LigneAAfficher> listeDocument = getModele().getListeLigneAAfficher();
    String[][] donnees = null;
    
    if (listeDocument != null) {
      donnees = new String[listeDocument.size()][TITRE_LISTE_DOCUMENTS.length];
      for (int ligne = 0; ligne < listeDocument.size(); ligne++) {
        donnees[ligne][0] = listeDocument.get(ligne).getNumeroDocument();
        donnees[ligne][1] = listeDocument.get(ligne).getDateCommande();
        donnees[ligne][2] = listeDocument.get(ligne).getDateLivraison();
        donnees[ligne][3] = listeDocument.get(ligne).getQuantiteUCA();
        donnees[ligne][4] = listeDocument.get(ligne).getPrixAchatNet();
        donnees[ligne][5] = listeDocument.get(ligne).getPrixRevientHT();
        donnees[ligne][6] = listeDocument.get(ligne).getNomAcheteur();
      }
    }
    tblListeDocuments.mettreAJourDonnees(donnees);
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale) {
    try {
      // Informer le modèle qu'une valeur a été saisie pour un article
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpListeDocumentsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDocuments == null) {
        return;
      }
      Trace.debug(VueCommandeEnCours.class, "scpListeDocumentsStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDocuments.getViewport().getViewRect();
      int premiereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDocuments.getRowCount() - 1;
      }
      
      // Charges les lignes d'achat concernées si besoin
      getModele().modifierPlageLignesAchatsAffichees(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    lbMessageAlerte = new SNLabelChamp();
    scpListeDocuments = new JScrollPane();
    tblListeDocuments = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1155, 400));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Liste des commandes en cours");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 1139, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 35, 297, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ---- lbMessageAlerte ----
        lbMessageAlerte.setText("L'article est d\u00e9j\u00e0 en cours de commande, confirmez vous votre saisie ?");
        lbMessageAlerte.setMaximumSize(new Dimension(457, 60));
        lbMessageAlerte.setMinimumSize(new Dimension(457, 30));
        lbMessageAlerte.setPreferredSize(new Dimension(457, 30));
        lbMessageAlerte.setHorizontalAlignment(SwingConstants.LEFT);
        lbMessageAlerte.setName("lbMessageAlerte");
        pnlContenu.add(lbMessageAlerte, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocuments ========
        {
          scpListeDocuments.setPreferredSize(new Dimension(800, 424));
          scpListeDocuments.setName("scpListeDocuments");
          
          // ---- tblListeDocuments ----
          tblListeDocuments.setModel(new DefaultTableModel(
              new Object[][] { { "", "", "", "", "", null }, { "", null, "", "", "", null }, { "", null, "", "", "", null },
                  { "", null, "", "", "", null }, { "", null, "", "", "", null }, { "", null, "", "", "", null },
                  { "", null, "", "", "", null }, { null, null, null, null, null, null }, { null, null, null, null, null, null },
                  { null, null, null, null, null, null }, { null, null, null, null, null, null },
                  { null, null, null, null, null, null }, },
              new String[] { "Num\u00e9ro", "Date commande", "Date livraison", "Quantit\u00e9 UCA", "Prix achat net HT", "Acheteur" }) {
            boolean[] columnEditable = new boolean[] { false, true, false, false, false, true };
            
            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
              return columnEditable[columnIndex];
            }
          });
          {
            TableColumnModel cm = tblListeDocuments.getColumnModel();
            cm.getColumn(0).setResizable(false);
            cm.getColumn(0).setPreferredWidth(80);
            cm.getColumn(1).setPreferredWidth(100);
            cm.getColumn(2).setResizable(false);
            cm.getColumn(2).setPreferredWidth(100);
            cm.getColumn(3).setResizable(false);
            cm.getColumn(3).setPreferredWidth(80);
            cm.getColumn(4).setResizable(false);
            cm.getColumn(4).setPreferredWidth(150);
          }
          tblListeDocuments.setShowVerticalLines(true);
          tblListeDocuments.setShowHorizontalLines(true);
          tblListeDocuments.setBackground(Color.white);
          tblListeDocuments.setRowHeight(20);
          tblListeDocuments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocuments.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocuments.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblListeDocuments.setName("tblListeDocuments");
          scpListeDocuments.setViewportView(tblListeDocuments);
        }
        pnlContenu.add(scpListeDocuments, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private SNLabelChamp lbMessageAlerte;
  private JScrollPane scpListeDocuments;
  private NRiTable tblListeDocuments;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
