/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.genererconditionventes;

import java.util.ArrayList;
import java.util.Date;

import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialoge qui propose la quantité correspondant au conditionnement supérieur.
 */
public class ModeleGenererConditionVentes extends AbstractModeleDialogue {
  
  // Variables
  private VueGenererConditionVentes vue = null;
  private DocumentVente documentOrigine = null;
  private ListeLigneVente listeLigneVenteOrigine = null;
  private ListeLigneVente listeLigneVenteSelectionnees = null;
  private Date dateValiditeDebutCNV = null;
  
  private Date dateValiditeFinCNV = null;
  private String referenceCNV = null;
  
  /**
   * Constructeur.
   */
  public ModeleGenererConditionVentes(SessionBase pSession, DocumentVente pdocumentOrigine) {
    super(pSession);
    documentOrigine = pdocumentOrigine;
  }
  
  // Méthodes standards du modèle ********************************************************************************************************
  
  @Override
  public void initialiserDonnees() {
    chargerLignesDocumentOrigine();
    // Date de validité par défaut de la CNV est la date du jour
    if (dateValiditeDebutCNV == null) {
      dateValiditeDebutCNV = new Date();
    }
  }
  
  @Override
  public void chargerDonnees() {
    
  }
  
  // Méthodes publiques ******************************************************************************************************************
  /**
   * Modifier la date de validité de début de la CNV
   */
  public void modifierDateValiditeDebut(Date pDate) {
    if (Constantes.equals(pDate, dateValiditeDebutCNV)) {
      return;
    }
    dateValiditeDebutCNV = pDate;
  }
  
  /**
   * Modifier la date de validité de fin de la CNV
   */
  public void modifierDateValiditeFin(Date pDate) {
    if (Constantes.equals(pDate, dateValiditeFinCNV)) {
      return;
    }
    dateValiditeFinCNV = pDate;
  }
  
  /**
   * Modifier la référence de la CNV
   */
  public void modifierReference(String pReference) {
    if (Constantes.equals(pReference, referenceCNV)) {
      return;
    }
    referenceCNV = pReference;
  }
  
  /**
   * Modifier la liste des lignes sélectionnées
   */
  public void modifierListeLigneSelection(ArrayList<Integer> listeIndiceSelection) {
    if (listeIndiceSelection == null || listeIndiceSelection.size() == 0) {
      return;
    }
    
    listeLigneVenteSelectionnees = new ListeLigneVente();
    for (int indice : listeIndiceSelection) {
      listeLigneVenteSelectionnees.add(listeLigneVenteOrigine.get(indice));
    }
  }
  
  /**
   * Vérifier la validité des données et lancer la création de la CNV
   */
  public void validerDonnees() {
    if (dateValiditeDebutCNV == null) {
      throw new MessageErreurException("Vous devez entrer une date de validité pour créer une nouvelle condition de vente.");
    }
    if (dateValiditeDebutCNV.before(Constantes.getDateDelaiJour(new Date(), -1))) {
      throw new MessageErreurException(
          "Vous devez entrer une date de validité égale ou ultérieure à la date du jour pour créer une nouvelle condition de vente.");
    }
    if (dateValiditeFinCNV != null && dateValiditeFinCNV.before(Constantes.getDateDelaiJour(dateValiditeDebutCNV, -1))) {
      throw new MessageErreurException("Vous devez entrer une date de fin de validité\n égale ou ultérieure à la date de début.\n\n "
          + "Ne saisissez pas de date de fin si la condition de vente\n n'a pas de limite dans le temps.");
    }
    if (referenceCNV == null || referenceCNV.trim().isEmpty()) {
      throw new MessageErreurException("Vous devez entrer une référence pour créer une nouvelle condition de vente.");
    }
    
    // Si tout est bon on crée la CNV
    creerCNV();
    
    // Et on quitte
    quitterAvecValidation();
  }
  
  // Méthodes privées ********************************************************************************************************************
  /**
   * Charger les lignes du devis d'origine
   */
  private void chargerLignesDocumentOrigine() {
    if (documentOrigine == null) {
      return;
    }
    if (documentOrigine.getListeLigneVente() == null) {
      return;
    }
    
    listeLigneVenteOrigine = new ListeLigneVente();
    listeLigneVenteOrigine = documentOrigine.getListeLigneVente();
    
    ListeLigneVente listeLigneVenteNegocie = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVenteOrigine) {
      if (ligneVente != null && ligneVente.getTexteOriginePrixVente() != null
          && ligneVente.getTexteOriginePrixVente().equalsIgnoreCase(LigneVente.TEXTE_ORIGINE_PRIX_NEGOCIATION)) {
        listeLigneVenteNegocie.add(ligneVente);
      }
    }
    
    listeLigneVenteOrigine = listeLigneVenteNegocie;
    
    // Pas de ligne négociée
    if (listeLigneVenteOrigine.size() == 0) {
      throw new MessageErreurException("Aucune ligne de ce devis ne permet la génération d'une condition de vente."
          + "\nUne condition de vente peut être générée à partir d'une ligne négociée.");
    }
  }
  
  /**
   * Créer une CNV à partir d'une ligne de devis et des informations de dates et de référence à l'écran
   */
  public void creerCNV() {
    if (listeLigneVenteSelectionnees == null || listeLigneVenteSelectionnees.size() == 0) {
      throw new MessageErreurException("Aucune ligne n'a été sélectionnée.");
    }
    // On lance le service pour chaque ligne sélectionnée
    for (LigneVenteBase ligne : listeLigneVenteSelectionnees) {
      ManagerServiceDocumentVente.genererCNVDepuisDocument(getIdSession(), ligne.getId(), dateValiditeDebutCNV, dateValiditeFinCNV,
          referenceCNV);
    }
  }
  
  // -- Accesseurs ***********************************************************************************************************************
  /**
   * Vue de la boite de dialogue
   */
  public VueGenererConditionVentes getVue() {
    return vue;
  }
  
  /**
   * Liste des lignes du document d'origine
   */
  public ListeLigneVente getListeLigneVenteOrigine() {
    return listeLigneVenteOrigine;
  }
  
  /**
   * Date de début de la période de validité de la CNV à créer
   */
  public Date getDateValiditeDebutCNV() {
    return dateValiditeDebutCNV;
  }
  
  /**
   * Date de fin de la période de validité de la CNV à créer
   */
  public Date getDateValiditeFinCNV() {
    return dateValiditeFinCNV;
  }
  
  /**
   * Référence de la CNV à créer
   */
  public String getReferenceCNV() {
    return referenceCNV;
  }
  
  /**
   * Identifiant du document d'origine
   */
  public IdDocumentVente getIdDocumentOrigine() {
    return documentOrigine.getId();
  }
  
}
