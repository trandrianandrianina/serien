/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.historiquereceptionsachat;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererHistoriqueReceptions extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color blanc = new Color(255, 255, 255);
  private static Color vert = new Color(152, 206, 168);
  private static Color orange = new Color(211, 187, 114);
  private static Color rouge = new Color(218, 166, 161);
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Taille des colonnes
    redimensionnerColonnes(table);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setCellRenderer(leftRenderer);
    cm.getColumn(1).setCellRenderer(centerRenderer);
    cm.getColumn(2).setCellRenderer(leftRenderer);
    cm.getColumn(3).setCellRenderer(leftRenderer);
    cm.getColumn(4).setCellRenderer(rightRenderer);
    cm.getColumn(5).setCellRenderer(rightRenderer);
    cm.getColumn(6).setCellRenderer(leftRenderer);
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(135);
    cm.getColumn(0).setMaxWidth(135);
    cm.getColumn(1).setMinWidth(120);
    cm.getColumn(1).setMaxWidth(120);
    cm.getColumn(2).setMinWidth(90);
    cm.getColumn(3).setMinWidth(145);
    cm.getColumn(3).setMaxWidth(145);
    cm.getColumn(4).setMinWidth(110);
    cm.getColumn(4).setMaxWidth(110);
    cm.getColumn(5).setMinWidth(120);
    cm.getColumn(5).setMaxWidth(120);
    cm.getColumn(6).setMinWidth(65);
    cm.getColumn(6).setMaxWidth(65);
    
  }
}
