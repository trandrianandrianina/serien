/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.commun.contact.EnumTypeContact;
import ri.serien.libcommun.gescom.commun.document.EnumModeEdition;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.contact.sncontact.SNContact;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet édition de la gestion des achats.
 */
public class VueOngletEdition extends AbstractVuePanel<ModeleAchat> {
  /**
   * Constructeur.
   */
  public VueOngletEdition(ModeleAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.EDITER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirLibelleDocument();
    rafraichirChoixChiffrage();
    
    // Le bloc des choix d'édition
    rafraichirModeEditionVisualiser();
    rafraichirModeEditionImprimer();
    rafraichirModeEditionEnvoyerParMail();
    rafraichirModeEditionEnvoyerParFax();
    
    // Recherche contact
    rafraichirRechercheContactMail();
    rafraichirRechercheContactFax();
    rafraichirBoutonEditer();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafrachit le libellé du document.
   */
  private void rafraichirLibelleDocument() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours == null || documentAchatEnCours.getLibelleTypeDocument() == null) {
      zsTitreDocumentEdition.setText("");
    }
    else {
      zsTitreDocumentEdition.setText(documentAchatEnCours.getLibelleTypeDocument());
    }
  }
  
  /**
   * Rafraichit le bloc des options de chiffrage.
   */
  private void rafraichirChoixChiffrage() {
    if (getModele().getOptionChiffrageEdition() != null) {
      switch (getModele().getOptionChiffrageEdition()) {
        case NON_CHIFFRE:
          rbNonChiffre.setSelected(true);
          break;
        case CHIFFRE:
          rbChiffre.setSelected(true);
          break;
        case CHIFFRE_AVEC_TOTAL:
          rbChiffreAvecTotal.setSelected(true);
          break;
        default:
          rbNonChiffre.setSelected(false);
          rbChiffre.setSelected(false);
          rbChiffreAvecTotal.setSelected(false);
          btgChiffrage.clearSelection();
      }
    }
    else {
      rbNonChiffre.setSelected(false);
      rbChiffre.setSelected(false);
      rbChiffreAvecTotal.setSelected(false);
      btgChiffrage.clearSelection();
    }
    
    // Raccourcis clavier
    rbNonChiffre.setMnemonic(KeyEvent.VK_N);
    rbChiffre.setMnemonic(KeyEvent.VK_C);
    rbChiffreAvecTotal.setMnemonic(KeyEvent.VK_T);
    
    // Positionner le focus
    rbNonChiffre.requestFocus();
    
  }
  
  /**
   * Rafraichit le mode d'édition Visualiser.
   */
  private void rafraichirModeEditionVisualiser() {
    ListeModeEdition listeModeEditionVente = getModele().getListeModeEditionAchat();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.VISUALISER);
    if (modeEdition == null || !modeEdition.isActif()) {
      ckVisualiser.setVisible(false);
    }
    else {
      ckVisualiser.setText(modeEdition.getTexte());
      ckVisualiser.setSelected(modeEdition.isSelectionne());
      ckVisualiser.setVisible(true);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Imprimer.
   */
  private void rafraichirModeEditionImprimer() {
    ListeModeEdition listeModeEditionVente = getModele().getListeModeEditionAchat();
    ModeEdition modeEdition = listeModeEditionVente.getModeEditionAPartirEnum(EnumModeEdition.IMPRIMER);
    if (modeEdition == null || !modeEdition.isActif()) {
      ckImprimer.setVisible(false);
    }
    else {
      ckImprimer.setText(modeEdition.getTexte());
      ckImprimer.setSelected(modeEdition.isSelectionne());
      ckImprimer.setVisible(true);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Envoyer par mail.
   */
  private void rafraichirModeEditionEnvoyerParMail() {
    ListeModeEdition listeModeEditionAchat = getModele().getListeModeEditionAchat();
    ModeEdition modeEdition = listeModeEditionAchat.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_MAIL);
    lbMail.setForeground(Color.BLACK);
    tfEmailDestinataire.setText(getModele().getEmailDestinataireEdition());
    if (modeEdition == null || !modeEdition.isActif() || !modeEdition.isSelectionne()) {
      ckEnvoyerParMail.setSelected(false);
      snContactMail.setEnabled(false);
      tfEmailDestinataire.setEnabled(false);
    }
    else {
      ckEnvoyerParMail.setSelected(true);
      ckEnvoyerParMail.setText(modeEdition.getTexte() + " à");
      if (getModele().getEmailDestinataireEdition().isEmpty()) {
        lbMail.setForeground(Color.RED);
      }
      snContactMail.setEnabled(true);
      tfEmailDestinataire.setEnabled(true);
    }
  }
  
  /**
   * Rafraichit le mode d'édition Envoyer par fax.
   */
  private void rafraichirModeEditionEnvoyerParFax() {
    ListeModeEdition listeModeEditionAchat = getModele().getListeModeEditionAchat();
    ModeEdition modeEdition = listeModeEditionAchat.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_FAX);
    lbFax.setForeground(Color.BLACK);
    tfFaxDestinataire.setText(getModele().getFaxDestinataireEdition());
    if (modeEdition == null || !modeEdition.isActif() || !modeEdition.isSelectionne()) {
      ckEnvoyerParFax.setSelected(false);
      snContactFax.setEnabled(false);
      tfFaxDestinataire.setEnabled(false);
    }
    else {
      ckEnvoyerParFax.setSelected(true);
      ckEnvoyerParFax.setText(modeEdition.getTexte() + " au ");
      if (getModele().getFaxDestinataireEdition().isEmpty()) {
        lbFax.setForeground(Color.RED);
      }
      snContactFax.setEnabled(true);
      tfFaxDestinataire.setEnabled(true);
    }
  }
  
  /**
   * Rafraichir la recherche contact pour l'envoi par mail.
   */
  private void rafraichirRechercheContactMail() {
    snContactMail.setSession(getModele().getSession());
    
    if (getModele().getEtablissement() != null && getModele().getFournisseurCourant() != null) {
      snContactMail.setIdEtablissement(getModele().getEtablissement().getId());
      snContactMail.setTypeContact(EnumTypeContact.FOURNISSEUR);
      snContactMail.setIdFournisseur(getModele().getFournisseurCourant().getId());
      snContactMail.setSelection(getModele().getContactMail());
      snContactMail.charger(false, false);
    }
    else {
      snContactMail.setIdEtablissement(null);
    }
  }
  
  /**
   * Rafraichir la recherche contact pour l'envoi par fax.
   */
  private void rafraichirRechercheContactFax() {
    snContactFax.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null && getModele().getFournisseurCourant() != null) {
      snContactFax.setIdEtablissement(getModele().getEtablissement().getId());
      snContactFax.setTypeContact(EnumTypeContact.FOURNISSEUR);
      snContactFax.setIdFournisseur(getModele().getFournisseurCourant().getId());
      snContactFax.setSelection(getModele().getContactFax());
      snContactFax.charger(false, false);
    }
    else {
      snContactFax.setIdEtablissement(null);
    }
  }
  
  /**
   * Rafraichir le bouton Editer.
   */
  private void rafraichirBoutonEditer() {
    boolean actif = true;
    // L'envoi par mail est sélectionné
    if (ckEnvoyerParMail.isSelected()) {
      // Controle que le champ du mail ne soit pas vide
      if (getModele().getEmailDestinataireEdition().isEmpty()) {
        actif = false;
      }
    }
    else if (ckEnvoyerParFax.isSelected()) {
      // Controle que le champ du fax ne soit pas vide
      if (getModele().getFaxDestinataireEdition().isEmpty()) {
        actif = false;
      }
    }
    else {
      actif = true;
    }
    snBarreBouton.activerBouton(EnumBouton.EDITER, actif);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.EDITER)) {
        getModele().validerEdition();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().annulerDocument();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbNonChiffreItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierOptionChiffrageEdition(EnumOptionEdition.NON_CHIFFRE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbChiffreItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierOptionChiffrageEdition(EnumOptionEdition.CHIFFRE);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbChiffreAvecTotalItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierOptionChiffrageEdition(EnumOptionEdition.CHIFFRE_AVEC_TOTAL);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckImprimerActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierModeEdition(EnumModeEdition.IMPRIMER, ckImprimer.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckEnvoyerParMailActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierModeEdition(EnumModeEdition.ENVOYER_PAR_MAIL, ckEnvoyerParMail.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckVisualiserActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierModeEdition(EnumModeEdition.VISUALISER, ckVisualiser.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckEnvoyerParFaxActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierModeEdition(EnumModeEdition.ENVOYER_PAR_FAX, ckEnvoyerParFax.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfEmailDestinataireFocusLost(FocusEvent e) {
    try {
      getModele().modifierEmailDestinataire(tfEmailDestinataire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snContactMailValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEmailDestinataire(snContactMail.getSelection(), true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFaxDestinataireFocusLost(FocusEvent e) {
    try {
      getModele().modifierFaxDestinataire(tfFaxDestinataire.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snContactFaxValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierFaxDestinataire(snContactFax.getSelection(), true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlTitre = new SNPanel();
    lbTitreDocumentEdition = new SNLabelTitre();
    zsTitreDocumentEdition = new RiZoneSortie();
    pnlChiffrage = new SNPanelTitre();
    rbNonChiffre = new JRadioButton();
    rbChiffre = new JRadioButton();
    rbChiffreAvecTotal = new JRadioButton();
    pnlChoixEdition = new SNPanelTitre();
    ckVisualiser = new JCheckBox();
    ckImprimer = new JCheckBox();
    pnlEnvoyerParMail = new SNPanel();
    ckEnvoyerParMail = new JCheckBox();
    pnlMail = new JPanel();
    lbContactMail = new SNLabelChamp();
    snContactMail = new SNContact();
    lbMail = new SNLabelChamp();
    tfEmailDestinataire = new SNTexte();
    pnlEnvoyerParFax = new SNPanel();
    ckEnvoyerParFax = new JCheckBox();
    pnlFax = new JPanel();
    lbContactFax = new SNLabelChamp();
    snContactFax = new SNContact();
    lbFax = new SNLabelChamp();
    tfFaxDestinataire = new SNTexte();
    snBarreBouton = new SNBarreBouton();
    btgChiffrage = new ButtonGroup();
    
    // ======== this ========
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlTitre ========
      {
        pnlTitre.setName("pnlTitre");
        pnlTitre.setOpaque(false);
        pnlTitre.setBorder(BorderFactory.createEmptyBorder());
        pnlTitre.setPreferredSize(new Dimension(685, 40));
        pnlTitre.setMinimumSize(new Dimension(685, 40));
        pnlTitre.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTitre.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlTitre.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlTitre.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlTitre.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbTitreDocumentEdition ----
        lbTitreDocumentEdition.setText("Titre du document");
        lbTitreDocumentEdition.setVerticalAlignment(SwingConstants.CENTER);
        lbTitreDocumentEdition.setName("lbTitreDocumentEdition");
        pnlTitre.add(lbTitreDocumentEdition, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- zsTitreDocumentEdition ----
        zsTitreDocumentEdition.setFont(zsTitreDocumentEdition.getFont().deriveFont(zsTitreDocumentEdition.getFont().getSize() + 2f));
        zsTitreDocumentEdition.setName("zsTitreDocumentEdition");
        pnlTitre.add(zsTitreDocumentEdition, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlTitre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlChiffrage ========
      {
        pnlChiffrage.setTitre("(option valable uniquement lorsque le document est imprim\u00e9)");
        pnlChiffrage.setName("pnlChiffrage");
        pnlChiffrage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChiffrage.getLayout()).columnWidths = new int[] { 343, 0 };
        ((GridBagLayout) pnlChiffrage.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlChiffrage.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlChiffrage.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- rbNonChiffre ----
        rbNonChiffre.setText("<html><u>N</u>on chiffr\u00e9</html>");
        rbNonChiffre.setFont(rbNonChiffre.getFont().deriveFont(rbNonChiffre.getFont().getSize() + 2f));
        rbNonChiffre.setPreferredSize(new Dimension(90, 30));
        rbNonChiffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbNonChiffre.setName("rbNonChiffre");
        rbNonChiffre.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbNonChiffreItemStateChanged(e);
          }
        });
        pnlChiffrage.add(rbNonChiffre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbChiffre ----
        rbChiffre.setText("<html><u>C</u>hiffr\u00e9</html>");
        rbChiffre.setFont(rbChiffre.getFont().deriveFont(rbChiffre.getFont().getSize() + 2f));
        rbChiffre.setPreferredSize(new Dimension(64, 30));
        rbChiffre.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbChiffre.setName("rbChiffre");
        rbChiffre.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbChiffreItemStateChanged(e);
          }
        });
        pnlChiffrage.add(rbChiffre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbChiffreAvecTotal ----
        rbChiffreAvecTotal.setText("<html>Chiffr\u00e9 avec <u>t</u>otal</html>");
        rbChiffreAvecTotal.setFont(rbChiffreAvecTotal.getFont().deriveFont(rbChiffreAvecTotal.getFont().getSize() + 2f));
        rbChiffreAvecTotal.setPreferredSize(new Dimension(129, 30));
        rbChiffreAvecTotal.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbChiffreAvecTotal.setName("rbChiffreAvecTotal");
        rbChiffreAvecTotal.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbChiffreAvecTotalItemStateChanged(e);
          }
        });
        pnlChiffrage.add(rbChiffreAvecTotal, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlChiffrage,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlChoixEdition ========
      {
        pnlChoixEdition.setTitre("Mode d'\u00e9dition");
        pnlChoixEdition.setName("pnlChoixEdition");
        pnlChoixEdition.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChoixEdition.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlChoixEdition.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlChoixEdition.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlChoixEdition.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- ckVisualiser ----
        ckVisualiser.setText("Visualiser");
        ckVisualiser.setPreferredSize(new Dimension(200, 30));
        ckVisualiser.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckVisualiser.setFont(ckVisualiser.getFont().deriveFont(ckVisualiser.getFont().getSize() + 2f));
        ckVisualiser.setName("ckVisualiser");
        ckVisualiser.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ckVisualiserActionPerformed(e);
          }
        });
        pnlChoixEdition.add(ckVisualiser, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- ckImprimer ----
        ckImprimer.setText("Imprimer");
        ckImprimer.setPreferredSize(new Dimension(200, 30));
        ckImprimer.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckImprimer.setFont(ckImprimer.getFont().deriveFont(ckImprimer.getFont().getSize() + 2f));
        ckImprimer.setName("ckImprimer");
        ckImprimer.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ckImprimerActionPerformed(e);
          }
        });
        pnlChoixEdition.add(ckImprimer, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlEnvoyerParMail ========
        {
          pnlEnvoyerParMail.setName("pnlEnvoyerParMail");
          pnlEnvoyerParMail.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEnvoyerParMail.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEnvoyerParMail.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- ckEnvoyerParMail ----
          ckEnvoyerParMail.setText("Envoyer par mail \u00e0 ");
          ckEnvoyerParMail.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckEnvoyerParMail.setName("label21");
          ckEnvoyerParMail.setMinimumSize(new Dimension(150, 30));
          ckEnvoyerParMail.setPreferredSize(new Dimension(150, 30));
          ckEnvoyerParMail.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckEnvoyerParMail.setMaximumSize(new Dimension(150, 30));
          ckEnvoyerParMail.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckEnvoyerParMailActionPerformed(e);
            }
          });
          pnlEnvoyerParMail.add(ckEnvoyerParMail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlMail ========
          {
            pnlMail.setOpaque(false);
            pnlMail.setName("pnlMail");
            pnlMail.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMail.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlMail.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlMail.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMail.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbContactMail ----
            lbContactMail.setText("Contact");
            lbContactMail.setMaximumSize(new Dimension(75, 30));
            lbContactMail.setMinimumSize(new Dimension(75, 30));
            lbContactMail.setPreferredSize(new Dimension(75, 30));
            lbContactMail.setName("lbContactMail");
            pnlMail.add(lbContactMail, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snContactMail ----
            snContactMail.setName("snContactMail");
            snContactMail.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snContactMailValueChanged(e);
              }
            });
            pnlMail.add(snContactMail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMail ----
            lbMail.setText("Mail");
            lbMail.setMaximumSize(new Dimension(75, 30));
            lbMail.setMinimumSize(new Dimension(75, 30));
            lbMail.setPreferredSize(new Dimension(75, 30));
            lbMail.setName("lbMail");
            pnlMail.add(lbMail, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfEmailDestinataire ----
            tfEmailDestinataire.setMinimumSize(new Dimension(400, 30));
            tfEmailDestinataire.setPreferredSize(new Dimension(400, 30));
            tfEmailDestinataire.setName("tfEmailDestinataire");
            tfEmailDestinataire.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfEmailDestinataireFocusLost(e);
              }
            });
            pnlMail.add(tfEmailDestinataire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnvoyerParMail.add(pnlMail, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlChoixEdition.add(pnlEnvoyerParMail, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlEnvoyerParFax ========
        {
          pnlEnvoyerParFax.setName("pnlEnvoyerParFax");
          pnlEnvoyerParFax.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlEnvoyerParFax.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlEnvoyerParFax.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- ckEnvoyerParFax ----
          ckEnvoyerParFax.setText("Envoyer par fax au");
          ckEnvoyerParFax.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckEnvoyerParFax.setName("label21");
          ckEnvoyerParFax.setMinimumSize(new Dimension(150, 30));
          ckEnvoyerParFax.setPreferredSize(new Dimension(150, 30));
          ckEnvoyerParFax.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckEnvoyerParFax.setMaximumSize(new Dimension(150, 30));
          ckEnvoyerParFax.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ckEnvoyerParFaxActionPerformed(e);
            }
          });
          pnlEnvoyerParFax.add(ckEnvoyerParFax, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlFax ========
          {
            pnlFax.setOpaque(false);
            pnlFax.setName("pnlFax");
            pnlFax.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFax.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFax.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFax.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFax.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbContactFax ----
            lbContactFax.setText("Contact");
            lbContactFax.setMaximumSize(new Dimension(75, 30));
            lbContactFax.setMinimumSize(new Dimension(75, 30));
            lbContactFax.setPreferredSize(new Dimension(75, 30));
            lbContactFax.setName("lbContactFax");
            pnlFax.add(lbContactFax, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snContactFax ----
            snContactFax.setName("snContactFax");
            snContactFax.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snContactFaxValueChanged(e);
              }
            });
            pnlFax.add(snContactFax, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFax ----
            lbFax.setText("Fax");
            lbFax.setMaximumSize(new Dimension(75, 30));
            lbFax.setMinimumSize(new Dimension(75, 30));
            lbFax.setPreferredSize(new Dimension(75, 30));
            lbFax.setName("lbFax");
            pnlFax.add(lbFax, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfFaxDestinataire ----
            tfFaxDestinataire.setMinimumSize(new Dimension(400, 30));
            tfFaxDestinataire.setPreferredSize(new Dimension(400, 30));
            tfFaxDestinataire.setName("tfFaxDestinataire");
            tfFaxDestinataire.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFaxDestinataireFocusLost(e);
              }
            });
            pnlFax.add(tfFaxDestinataire, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEnvoyerParFax.add(pnlFax, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlChoixEdition.add(pnlEnvoyerParFax, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlChoixEdition,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgChiffrage ----
    btgChiffrage.add(rbNonChiffre);
    btgChiffrage.add(rbChiffre);
    btgChiffrage.add(rbChiffreAvecTotal);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlTitre;
  private SNLabelTitre lbTitreDocumentEdition;
  private RiZoneSortie zsTitreDocumentEdition;
  private SNPanelTitre pnlChiffrage;
  private JRadioButton rbNonChiffre;
  private JRadioButton rbChiffre;
  private JRadioButton rbChiffreAvecTotal;
  private SNPanelTitre pnlChoixEdition;
  private JCheckBox ckVisualiser;
  private JCheckBox ckImprimer;
  private SNPanel pnlEnvoyerParMail;
  private JCheckBox ckEnvoyerParMail;
  private JPanel pnlMail;
  private SNLabelChamp lbContactMail;
  private SNContact snContactMail;
  private SNLabelChamp lbMail;
  private SNTexte tfEmailDestinataire;
  private SNPanel pnlEnvoyerParFax;
  private JCheckBox ckEnvoyerParFax;
  private JPanel pnlFax;
  private SNLabelChamp lbContactFax;
  private SNContact snContactFax;
  private SNLabelChamp lbFax;
  private SNTexte tfFaxDestinataire;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgChiffrage;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
