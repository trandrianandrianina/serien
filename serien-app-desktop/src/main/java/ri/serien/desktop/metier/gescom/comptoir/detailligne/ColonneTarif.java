/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;

public class ColonneTarif {
  // Variables
  private int nombreDecimales = Constantes.DEUX_DECIMALES;
  private int numeroColonne = TarifArticle.COLONNE_TARIF_PUBLIC;
  private BigDecimal prixBaseHT = BigDecimal.ZERO;
  private BigDecimal prixBaseTTC = BigDecimal.ZERO;
  private boolean affichageTTC = false;
  
  /**
   * Constructeur.
   */
  public ColonneTarif(int pNumeroColonne) {
    numeroColonne = pNumeroColonne;
    prixBaseHT = BigDecimal.ZERO;
    prixBaseTTC = BigDecimal.ZERO;
    nombreDecimales = 0;
  }
  
  /**
   * Constructeur.
   */
  public ColonneTarif(int pNumeroColonne, BigDecimal pPrixBaseHT, BigDecimal pPrixBaseTTC, int pNombreDecimales, boolean pAffichageTTC) {
    numeroColonne = pNumeroColonne;
    prixBaseHT = pPrixBaseHT;
    prixBaseTTC = pPrixBaseTTC;
    nombreDecimales = pNombreDecimales;
    affichageTTC = pAffichageTTC;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne la chaine représentative.
   */
  @Override
  public String toString() {
    // Dans le cas où l'ion veut juste afficher le numéro de la colonne
    if (prixBaseHT.compareTo(BigDecimal.ZERO) == 0 && prixBaseTTC.compareTo(BigDecimal.ZERO) == 0) {
      // La colonne 0 n'existe pas donc on affichera un libellé vide
      if (numeroColonne == 0) {
        return "";
      }
      // Sinon on affiche le libellé avec le numéro de la colonne
      else {
        return String.format("%s %d", LigneVente.TEXTE_ORIGINE_PRIX_COLONNE, numeroColonne);
      }
    }
    // Sinon
    BigDecimal prix = prixBaseHT.setScale(nombreDecimales, RoundingMode.HALF_UP);
    if (affichageTTC) {
      prix = prixBaseTTC;
    }
    // La colonne 0 n'existe pas donc on affichera un libellé vide
    if (numeroColonne == 0) {
      return "";
    }
    // Sinon on affiche le libellé avec le numéro de la colonne et le prix associé
    else {
      return String.format("%s %d=%s", LigneVente.TEXTE_ORIGINE_PRIX_COLONNE, numeroColonne, Constantes.formater(prix, true));
    }
  }
  
  @Override
  public boolean equals(Object pColonneAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (pColonneAComparer == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pColonneAComparer instanceof ColonneTarif)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    ColonneTarif colonne = (ColonneTarif) pColonneAComparer;
    return numeroColonne == colonne.numeroColonne && prixBaseHT == colonne.prixBaseHT && prixBaseTTC == colonne.prixBaseTTC;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + numeroColonne;
    cle = 37 * cle + prixBaseHT.hashCode();
    cle = 37 * cle + prixBaseTTC.hashCode();
    return cle;
  }
  
  // -- Accesseurs
  
  /**
   * Retourner le numéro de colonne tarif.
   * @return Numéro de colonne tarif.
   */
  public int getNumeroColonne() {
    return numeroColonne;
  }
  
  /**
   * Modifier le mode affichage HT/TTC.
   * @param pAffichageTTC true=affichage TTC, false=affichage HT.
   */
  public void setAffichageTTC(boolean pAffichageTTC) {
    affichageTTC = pAffichageTTC;
  }
  
  /**
   * Retourner le prix de base HT.
   * @return Prix de base HT.
   */
  public BigDecimal getPrixBaseHT() {
    return prixBaseHT;
  }
  
  /**
   * Retourner le prix de base TTC.
   * @return Prix de base TTC.
   */
  public BigDecimal getPrixBaseTTC() {
    return prixBaseTTC;
  }
}
