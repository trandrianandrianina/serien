/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.controleconditionnementfournisseur;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue de contrôle du respect du conditionnement fournisseur
 */
public class VueControleConditionnementFournisseur extends AbstractVueDialogue<ModeleControleConditionnementFournisseur> {
  /**
   * Constructeur.
   */
  public VueControleConditionnementFournisseur(ModeleControleConditionnementFournisseur pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Les raccourcis claviers
    rbQuantiteSaisie.setMnemonic(KeyEvent.VK_C);
    rbQuantiteInferieure.setMnemonic(KeyEvent.VK_I);
    rbQuantiteSuperieure.setMnemonic(KeyEvent.VK_S);
    rbQuantiteSaisie.setSelected(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraicirMessage();
    rafraichirBoutons();
    rafraichirLibelleQuantiteSaisie();
    rafraichirLibelleQuantiteInferieure();
    rafraichirLibelleQuantiteSuperieure();
  }
  
  // -- Méthodes privées
  private void rafraicirMessage() {
    snMessage.setMessage(getModele().getMessage());
  }
  
  private void rafraichirBoutons() {
    switch (getModele().getChoixQuantite()) {
      case ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_SAISIE:
        rbQuantiteSaisie.setSelected(true);
        break;
      
      case ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_INFERIEURE:
        rbQuantiteInferieure.setSelected(true);
        break;
      
      case ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_SUPERIEURE:
        rbQuantiteSuperieure.setSelected(true);
        break;
      
      default:
        rbQuantiteSaisie.setSelected(true);
        break;
    }
    rbQuantiteInferieure.setVisible(!getModele().isInferieurConditionnement());
  }
  
  private void rafraichirLibelleQuantiteSaisie() {
    String libelle = getModele().getLibelleRbQuantiteSaisie();
    if (libelle != null) {
      rbQuantiteSaisie.setText(libelle);
    }
    else {
      rbQuantiteSaisie.setText("");
    }
  }
  
  private void rafraichirLibelleQuantiteInferieure() {
    String libelle = getModele().getLibelleRbQuantiteInferieure();
    if (libelle != null) {
      rbQuantiteInferieure.setText(libelle);
    }
    else {
      rbQuantiteInferieure.setText("");
    }
  }
  
  private void rafraichirLibelleQuantiteSuperieure() {
    String libelle = getModele().getLibelleRbQuantiteSuperieure();
    if (libelle != null) {
      rbQuantiteSuperieure.setText(libelle);
    }
    else {
      rbQuantiteSuperieure.setText("");
    }
  }
  
  // -- Méthodes interractives
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbQuantiteSaisieItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierChoixQuantite(ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_SAISIE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbQuantiteInferieureItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierChoixQuantite(ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_INFERIEURE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbQuantiteSuperieureItemStateChanged(ItemEvent e) {
    try {
      getModele().modifierChoixQuantite(ModeleControleConditionnementFournisseur.CHOIX_QUANTITE_SUPERIEURE);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    sNPanelFond1 = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlDonneesControle = new SNPanelContenu();
    snMessage = new SNMessage();
    rbQuantiteSaisie = new SNRadioButton();
    rbQuantiteInferieure = new SNRadioButton();
    rbQuantiteSuperieure = new SNRadioButton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setTitle("Contr\u00f4le du conditionnement fournisseur");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(800, 300));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== sNPanelFond1 ========
    {
      sNPanelFond1.setName("sNPanelFond1");
      sNPanelFond1.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      sNPanelFond1.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlDonneesControle ========
      {
        pnlDonneesControle.setOpaque(false);
        pnlDonneesControle.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlDonneesControle.setName("pnlDonneesControle");
        pnlDonneesControle.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDonneesControle.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDonneesControle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlDonneesControle.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDonneesControle.getLayout()).rowWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== snMessage ========
        {
          snMessage.setName("snMessage");
        }
        pnlDonneesControle.add(snMessage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbQuantiteSaisie ----
        rbQuantiteSaisie.setText("Conserver la quantit\u00e9 saisie");
        rbQuantiteSaisie.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbQuantiteSaisie.setName("rbQuantiteSaisie");
        rbQuantiteSaisie.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbQuantiteSaisieItemStateChanged(e);
          }
        });
        pnlDonneesControle.add(rbQuantiteSaisie, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbQuantiteInferieure ----
        rbQuantiteInferieure.setText("Choisir le conditionnement inf\u00e9rieur");
        rbQuantiteInferieure.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbQuantiteInferieure.setName("rbQuantiteInferieure");
        rbQuantiteInferieure.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbQuantiteInferieureItemStateChanged(e);
          }
        });
        pnlDonneesControle.add(rbQuantiteInferieure, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbQuantiteSuperieure ----
        rbQuantiteSuperieure.setText("Choisir le conditionnement sup\u00e9rieur");
        rbQuantiteSuperieure.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbQuantiteSuperieure.setName("rbQuantiteSuperieure");
        rbQuantiteSuperieure.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbQuantiteSuperieureItemStateChanged(e);
          }
        });
        pnlDonneesControle.add(rbQuantiteSuperieure, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      sNPanelFond1.add(pnlDonneesControle, BorderLayout.CENTER);
    }
    contentPane.add(sNPanelFond1, BorderLayout.CENTER);
    
    // ---- btgMode ----
    btgMode.add(rbQuantiteSaisie);
    btgMode.add(rbQuantiteInferieure);
    btgMode.add(rbQuantiteSuperieure);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond sNPanelFond1;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlDonneesControle;
  private SNMessage snMessage;
  private SNRadioButton rbQuantiteSaisie;
  private SNRadioButton rbQuantiteInferieure;
  private SNRadioButton rbQuantiteSuperieure;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
