/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisionnementstock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueDetailCalcul extends AbstractVueDialogue<ModeleDetailCalcul> {
  // Constantes
  private static final String[] TITRE_LISTE =
      new String[] { "<html>Document<br>de vente</html>", "Raison sociale", "<html>Quantit\u00e9<br>command\u00e9e<html>",
          "<html>Livraison<br>souhait\u00e9e</html>", "<html>Livraison<br>pr\u00e9vue</html>" };
  
  /**
   * Constructeur.
   */
  public VueDetailCalcul(ModeleDetailCalcul pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);
    
    tblListe.personnaliserAspect(TITRE_LISTE, new int[] { 80, 220, 100, 90, 90 }, new int[] { 80, -1, 100, 90, 90 },
        new int[] { NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.CENTRE, NRiTable.CENTRE }, 14);
    // On double la taille de la hauteur de l'entête à cause du titre "Livraison prévue"
    int largeur = tblListe.getTableHeader().getPreferredSize().width;
    int hauteur = tblListe.getTableHeader().getPreferredSize().height;
    tblListe.getTableHeader().setPreferredSize(new Dimension(largeur, hauteur));
    // On autorise aucun sélection puisqu'il s'agit d'une simple consultation
    tblListe.setRowSelectionAllowed(false);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListe.getViewport();
    viewportLigneAchat.setBackground(Color.WHITE);
    
    // Raccourcis clavier
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirQuantiteProposee();
    rafraichirFormuleStockDisponible();
    rafraichirFormuleQuantiteProposee();
    rafraichirNote();
    rafraichirMessageErreur();
    rafraichirListe();
  }
  
  // -- Méthodes privées
  
  private void rafraichirCodeArticle() {
    LigneTableau ligneTableau = getModele().getLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null || ligneTableau.getArticle().getId() == null) {
      tfCodeArticle.setText("");
    }
    else {
      tfCodeArticle.setText(ligneTableau.getArticle().getId().getCodeArticle());
    }
    tfCodeArticle.setEnabled(false);
  }
  
  private void rafraichirLibelleArticle() {
    LigneTableau ligneTableau = getModele().getLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      tfLibelleArticle.setText("");
    }
    else {
      tfLibelleArticle.setText(ligneTableau.getLibelle());
    }
  }
  
  private void rafraichirQuantiteProposee() {
    LigneTableau ligneTableau = getModele().getLigneTableau();
    if (ligneTableau == null) {
      tfQuantiteCommandeUCachat.setText("");
      lbUniteCA.setText("");
    }
    else {
      tfQuantiteCommandeUCachat.setText(ligneTableau.getQuantiteCommandeeUCA());
      lbUniteCA.setText(ligneTableau.getUniteCommandeUCA());
    }
    tfQuantiteCommandeUCachat.setEnabled(false);
  }
  
  private void rafraichirFormuleStockDisponible() {
    LigneTableau ligneTableau = getModele().getLigneTableau();
    if (ligneTableau == null) {
      lbValeurStockDisponible.setText("");
      lbValeurStockPhysique.setText("");
      lbValeurStockCommande.setText("");
      lbValeurStockAttendu.setText("");
      lbValeurStockMinimum.setText("");
    }
    else {
      lbValeurStockDisponible.setText(ligneTableau.getStockDisponible());
      lbValeurStockPhysique.setText(ligneTableau.getStockPhysique());
      lbValeurStockCommande.setText(ligneTableau.getStockCommande());
      lbValeurStockAttendu.setText(ligneTableau.getStockAttendu());
      lbValeurStockMinimum.setText(ligneTableau.getStockMinimum());
      if (ligneTableau.isStockDisponibleInferieurAuMinimum()) {
        lbTitreStockDisponible.setForeground(Color.red);
        lbValeurStockDisponible.setForeground(Color.red);
      }
      else {
        lbTitreStockDisponible.setForeground(Color.black);
        lbValeurStockDisponible.setForeground(Color.black);
      }
    }
  }
  
  private void rafraichirFormuleQuantiteProposee() {
    LigneTableau ligneTableau = getModele().getLigneTableau();
    if (ligneTableau == null) {
      lbValeurQuantiteProposee.setText("");
      lbValeurStockIdeal.setText("");
      lbValeurQteProposeeStockDisponible.setText("");
    }
    else {
      lbValeurQuantiteProposee.setText(ligneTableau.getQuantiteCommandeeUCA());
      lbValeurStockIdeal.setText(ligneTableau.getStockIdeal());
      lbValeurQteProposeeStockDisponible.setText(ligneTableau.getStockDisponible());
    }
  }
  
  private void rafraichirNote() {
    BigDecimal quantiteMinimum = getModele().getQuantiteMinimumAchat();
    if (quantiteMinimum == null) {
      lbNote.setText("<html>Si la quantité proposée est inférieure à la quantité minimum d'achat alors la quantité proposée est égale"
          + " à cette quantité minimum.</html>");
    }
    else {
      lbNote.setText("<html>Si la quantité proposée est inférieure à " + Constantes.formater(quantiteMinimum, false)
          + " qui correspond la quantité minimum d'achat alors la quantité proposée est égale à cette quantité minimum.</html>");
    }
  }
  
  private void rafraichirMessageErreur() {
    Message message = getModele().getMessage();
    if (message == null) {
      lbMessageErreur.setVisible(false);
    }
    else {
      lbMessageErreur.setMessage(message);
      lbMessageErreur.setVisible(true);
    }
  }
  
  private void rafraichirListe() {
    List<LigneAttenduCommande> liste = getModele().getListeCommandeClient();
    if (liste == null || liste.isEmpty()) {
      tblListe.mettreAJourDonnees(null);
      tbpOnglet.setEnabledAt(tbpOnglet.getTabCount() - 1, false);
      return;
    }
    tbpOnglet.setEnabledAt(tbpOnglet.getTabCount() - 1, true);
    IdMagasin idMagasin = getModele().getIdMagasin();
    
    Object[][] donnees = new Object[liste.size()][TITRE_LISTE.length];
    int indice = 0;
    for (int ligne = 0; ligne < liste.size(); ligne++) {
      if (liste.get(ligne) == null || !liste.get(ligne).getIdMagasin().equals(idMagasin)) {
        continue;
      }
      if (liste.get(ligne).getIdLigneVente() == null || liste.get(ligne).getIdDocumentVente() == null) {
        donnees[indice][0] = "";
      }
      else {
        donnees[indice][0] = liste.get(ligne).getIdDocumentVente().toString();
      }
      donnees[indice][1] = liste.get(ligne).getNomClientFacture();
      donnees[indice][2] = Constantes.formater(liste.get(ligne).getQuantiteCommandeeUS(), false); // Qte commandée
      donnees[indice][3] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, liste.get(ligne).getDateLivraisonSouhaitee());
      donnees[indice][4] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, liste.get(ligne).getDateLivraisonPrevue());
      indice++;
    }
    tblListe.mettreAJourDonnees(donnees);
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale) {
    try {
      // Informer le modèle qu'une valeur a été saisie pour un article
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlPrincipal = new SNPanelContenu();
    tbpOnglet = new JTabbedPane();
    pnlOngletGeneral = new JPanel();
    pnlPanelArticle = new SNPanelTitre();
    lbCodeArticle = new SNLabelChamp();
    tfCodeArticle = new SNTexte();
    lbLibelleArticle = new SNLabelChamp();
    tfLibelleArticle = new RiTextArea();
    pnlQuantiteUCA = new JPanel();
    lbQuantiteUCA = new SNLabelChamp();
    tfQuantiteCommandeUCachat = new SNTexte();
    lbUniteCA = new SNLabelUnite();
    pnlCalculSeuilDeclenchement = new SNPanelTitre();
    pnlStockDisponible = new SNPanel();
    lbTitreStockPhysique = new SNLabelChamp();
    lbValeurStockPhysique = new SNLabelChamp();
    lbSymboleMoins = new SNLabelChamp();
    lbTitreStockCommande = new SNLabelChamp();
    lbValeurStockCommande = new SNLabelChamp();
    lbSymbolePlus = new SNLabelChamp();
    lbTitreStockAttendu = new SNLabelChamp();
    lbValeurStockAttendu = new SNLabelChamp();
    lbSymboleEgal = new SNLabelChamp();
    lbTitreStockDisponible = new SNLabelChamp();
    lbValeurStockDisponible = new SNLabelChamp();
    lbSymboleInferieur = new SNLabelChamp();
    lbTitreStockMinimum = new SNLabelChamp();
    lbValeurStockMinimum = new SNLabelChamp();
    pnlCalculQuantiteProposee = new SNPanelTitre();
    pnlQuantiteProposee = new SNPanel();
    lbTitreStockIdeal = new SNLabelChamp();
    lbValeurStockIdeal = new SNLabelChamp();
    lbSymboleMoins2 = new SNLabelChamp();
    lbTitreQteProposeeStockDisponible = new SNLabelChamp();
    lbValeurQteProposeeStockDisponible = new SNLabelChamp();
    lbSymboleEgal2 = new SNLabelChamp();
    lbTitreQuantiteProposee = new SNLabelChamp();
    lbValeurQuantiteProposee = new SNLabelChamp();
    lbNote = new SNLabelChamp();
    pnlOngletListeCommande = new SNPanel();
    scpListe = new JScrollPane();
    tblListe = new NRiTable();
    lbMessageErreur = new SNMessage();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(900, 580));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setBackground(new Color(238, 238, 210));
    setTitle("D\u00e9tail du calcul de la quantit\u00e9 command\u00e9e");
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        
        // ======== tbpOnglet ========
        {
          tbpOnglet.setFont(tbpOnglet.getFont().deriveFont(tbpOnglet.getFont().getSize() + 2f));
          tbpOnglet.setName("tbpOnglet");
          
          // ======== pnlOngletGeneral ========
          {
            pnlOngletGeneral.setOpaque(false);
            pnlOngletGeneral.setName("pnlOngletGeneral");
            pnlOngletGeneral.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOngletGeneral.getLayout()).columnWidths = new int[] { 1139, 0 };
            ((GridBagLayout) pnlOngletGeneral.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOngletGeneral.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOngletGeneral.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0, 1.0E-4 };
            
            // ======== pnlPanelArticle ========
            {
              pnlPanelArticle.setTitre("Article");
              pnlPanelArticle.setName("pnlPanelArticle");
              pnlPanelArticle.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPanelArticle.getLayout()).columnWidths = new int[] { 184, 0, 0 };
              ((GridBagLayout) pnlPanelArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlPanelArticle.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlPanelArticle.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCodeArticle ----
              lbCodeArticle.setText("Code article");
              lbCodeArticle.setName("lbCodeArticle");
              pnlPanelArticle.add(lbCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfCodeArticle ----
              tfCodeArticle.setPreferredSize(new Dimension(160, 30));
              tfCodeArticle.setName("tfCodeArticle");
              pnlPanelArticle.add(tfCodeArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbLibelleArticle ----
              lbLibelleArticle.setText("Libell\u00e9 article");
              lbLibelleArticle.setName("lbLibelleArticle");
              pnlPanelArticle.add(lbLibelleArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfLibelleArticle ----
              tfLibelleArticle.setFont(tfLibelleArticle.getFont().deriveFont(tfLibelleArticle.getFont().getSize() + 1f));
              tfLibelleArticle.setBackground(Color.white);
              tfLibelleArticle.setLineWrap(true);
              tfLibelleArticle.setWrapStyleWord(true);
              tfLibelleArticle.setEditable(false);
              tfLibelleArticle.setFocusable(false);
              tfLibelleArticle.setPreferredSize(new Dimension(610, 30));
              tfLibelleArticle.setMinimumSize(new Dimension(610, 30));
              tfLibelleArticle.setName("tfLibelleArticle");
              pnlPanelArticle.add(tfLibelleArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlQuantiteUCA ========
              {
                pnlQuantiteUCA.setOpaque(false);
                pnlQuantiteUCA.setName("pnlQuantiteUCA");
                pnlQuantiteUCA.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlQuantiteUCA.getLayout()).columnWidths = new int[] { 184, 0, 0, 0 };
                ((GridBagLayout) pnlQuantiteUCA.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnlQuantiteUCA.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlQuantiteUCA.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- lbQuantiteUCA ----
                lbQuantiteUCA.setText("Quantit\u00e9 en UCA");
                lbQuantiteUCA.setName("lbQuantiteUCA");
                pnlQuantiteUCA.add(lbQuantiteUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- tfQuantiteCommandeUCachat ----
                tfQuantiteCommandeUCachat.setPreferredSize(new Dimension(105, 30));
                tfQuantiteCommandeUCachat.setName("tfQuantiteCommandeUCachat");
                pnlQuantiteUCA.add(tfQuantiteCommandeUCachat, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                    GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbUniteCA ----
                lbUniteCA.setName("lbUniteCA");
                pnlQuantiteUCA.add(lbUniteCA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlPanelArticle.add(pnlQuantiteUCA, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletGeneral.add(pnlPanelArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlCalculSeuilDeclenchement ========
            {
              pnlCalculSeuilDeclenchement.setTitre("Calcul du seuil de d\u00e9clenchement");
              pnlCalculSeuilDeclenchement.setMinimumSize(new Dimension(798, 65));
              pnlCalculSeuilDeclenchement.setPreferredSize(new Dimension(798, 65));
              pnlCalculSeuilDeclenchement.setName("pnlCalculSeuilDeclenchement");
              pnlCalculSeuilDeclenchement.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCalculSeuilDeclenchement.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlCalculSeuilDeclenchement.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCalculSeuilDeclenchement.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCalculSeuilDeclenchement.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
              
              // ======== pnlStockDisponible ========
              {
                pnlStockDisponible.setName("pnlStockDisponible");
                pnlStockDisponible.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlStockDisponible.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlStockDisponible.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlStockDisponible.getLayout()).columnWeights =
                    new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlStockDisponible.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbTitreStockPhysique ----
                lbTitreStockPhysique.setText("Physique");
                lbTitreStockPhysique.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockPhysique
                    .setFont(lbTitreStockPhysique.getFont().deriveFont(lbTitreStockPhysique.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockPhysique.setMaximumSize(new Dimension(130, 30));
                lbTitreStockPhysique.setMinimumSize(new Dimension(130, 30));
                lbTitreStockPhysique.setPreferredSize(new Dimension(130, 25));
                lbTitreStockPhysique.setName("lbTitreStockPhysique");
                pnlStockDisponible.add(lbTitreStockPhysique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurStockPhysique ----
                lbValeurStockPhysique.setText("0");
                lbValeurStockPhysique.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockPhysique
                    .setFont(lbValeurStockPhysique.getFont().deriveFont(lbValeurStockPhysique.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockPhysique.setMaximumSize(new Dimension(130, 30));
                lbValeurStockPhysique.setPreferredSize(new Dimension(130, 25));
                lbValeurStockPhysique.setMinimumSize(new Dimension(130, 30));
                lbValeurStockPhysique.setName("lbValeurStockPhysique");
                pnlStockDisponible.add(lbValeurStockPhysique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymboleMoins ----
                lbSymboleMoins.setText("-");
                lbSymboleMoins.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymboleMoins.setPreferredSize(new Dimension(20, 25));
                lbSymboleMoins.setFont(lbSymboleMoins.getFont().deriveFont(lbSymboleMoins.getFont().getSize() + 1f));
                lbSymboleMoins.setName("lbSymbolePlus");
                lbSymboleMoins.setMinimumSize(new Dimension(20, 30));
                lbSymboleMoins.setMaximumSize(new Dimension(20, 30));
                pnlStockDisponible.add(lbSymboleMoins, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbTitreStockCommande ----
                lbTitreStockCommande.setText("Command\u00e9");
                lbTitreStockCommande.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockCommande
                    .setFont(lbTitreStockCommande.getFont().deriveFont(lbTitreStockCommande.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockCommande.setMinimumSize(new Dimension(130, 30));
                lbTitreStockCommande.setMaximumSize(new Dimension(130, 30));
                lbTitreStockCommande.setPreferredSize(new Dimension(130, 25));
                lbTitreStockCommande.setName("lbTitreStockCommande");
                pnlStockDisponible.add(lbTitreStockCommande, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurStockCommande ----
                lbValeurStockCommande.setText("0");
                lbValeurStockCommande.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockCommande
                    .setFont(lbValeurStockCommande.getFont().deriveFont(lbValeurStockCommande.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockCommande.setPreferredSize(new Dimension(130, 25));
                lbValeurStockCommande.setMinimumSize(new Dimension(130, 30));
                lbValeurStockCommande.setMaximumSize(new Dimension(130, 30));
                lbValeurStockCommande.setName("lbValeurStockCommande");
                pnlStockDisponible.add(lbValeurStockCommande, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymbolePlus ----
                lbSymbolePlus.setText("+");
                lbSymbolePlus.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymbolePlus.setPreferredSize(new Dimension(20, 25));
                lbSymbolePlus.setFont(lbSymbolePlus.getFont().deriveFont(lbSymbolePlus.getFont().getSize() + 1f));
                lbSymbolePlus.setMinimumSize(new Dimension(20, 30));
                lbSymbolePlus.setMaximumSize(new Dimension(20, 30));
                lbSymbolePlus.setName("lbSymbolePlus");
                pnlStockDisponible.add(lbSymbolePlus, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbTitreStockAttendu ----
                lbTitreStockAttendu.setText("Attendu");
                lbTitreStockAttendu.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockAttendu
                    .setFont(lbTitreStockAttendu.getFont().deriveFont(lbTitreStockAttendu.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockAttendu.setMinimumSize(new Dimension(130, 30));
                lbTitreStockAttendu.setMaximumSize(new Dimension(130, 30));
                lbTitreStockAttendu.setPreferredSize(new Dimension(130, 25));
                lbTitreStockAttendu.setName("lbTitreStockAttendu");
                pnlStockDisponible.add(lbTitreStockAttendu, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurStockAttendu ----
                lbValeurStockAttendu.setText("0");
                lbValeurStockAttendu.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockAttendu
                    .setFont(lbValeurStockAttendu.getFont().deriveFont(lbValeurStockAttendu.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockAttendu.setPreferredSize(new Dimension(130, 25));
                lbValeurStockAttendu.setMinimumSize(new Dimension(130, 30));
                lbValeurStockAttendu.setMaximumSize(new Dimension(130, 30));
                lbValeurStockAttendu.setName("lbValeurStockAttendu");
                pnlStockDisponible.add(lbValeurStockAttendu, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymboleEgal ----
                lbSymboleEgal.setText("=");
                lbSymboleEgal.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymboleEgal.setPreferredSize(new Dimension(20, 25));
                lbSymboleEgal.setFont(lbSymboleEgal.getFont().deriveFont(lbSymboleEgal.getFont().getStyle() & ~Font.BOLD));
                lbSymboleEgal.setName("lbSymbolePlus");
                lbSymboleEgal.setMinimumSize(new Dimension(20, 30));
                lbSymboleEgal.setMaximumSize(new Dimension(20, 30));
                pnlStockDisponible.add(lbSymboleEgal, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbTitreStockDisponible ----
                lbTitreStockDisponible.setText("Disponible achat");
                lbTitreStockDisponible
                    .setFont(lbTitreStockDisponible.getFont().deriveFont(lbTitreStockDisponible.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockDisponible.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockDisponible.setMinimumSize(new Dimension(130, 30));
                lbTitreStockDisponible.setPreferredSize(new Dimension(130, 25));
                lbTitreStockDisponible.setMaximumSize(new Dimension(130, 30));
                lbTitreStockDisponible.setName("lbTitreStockDisponible");
                pnlStockDisponible.add(lbTitreStockDisponible, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurStockDisponible ----
                lbValeurStockDisponible.setText("0");
                lbValeurStockDisponible.setPreferredSize(new Dimension(130, 25));
                lbValeurStockDisponible
                    .setFont(lbValeurStockDisponible.getFont().deriveFont(lbValeurStockDisponible.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockDisponible.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockDisponible.setMinimumSize(new Dimension(130, 30));
                lbValeurStockDisponible.setMaximumSize(new Dimension(130, 30));
                lbValeurStockDisponible.setName("lbValeurStockDisponible");
                pnlStockDisponible.add(lbValeurStockDisponible, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbSymboleInferieur ----
                lbSymboleInferieur.setText("<");
                lbSymboleInferieur.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymboleInferieur.setPreferredSize(new Dimension(20, 25));
                lbSymboleInferieur.setFont(lbSymboleInferieur.getFont().deriveFont(lbSymboleInferieur.getFont().getStyle() & ~Font.BOLD));
                lbSymboleInferieur.setName("lbSymbolePlus");
                lbSymboleInferieur.setMinimumSize(new Dimension(20, 30));
                lbSymboleInferieur.setMaximumSize(new Dimension(20, 30));
                pnlStockDisponible.add(lbSymboleInferieur, new GridBagConstraints(7, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- lbTitreStockMinimum ----
                lbTitreStockMinimum.setText("Stock minimum");
                lbTitreStockMinimum
                    .setFont(lbTitreStockMinimum.getFont().deriveFont(lbTitreStockMinimum.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockMinimum.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockMinimum.setMinimumSize(new Dimension(130, 30));
                lbTitreStockMinimum.setMaximumSize(new Dimension(130, 30));
                lbTitreStockMinimum.setPreferredSize(new Dimension(130, 25));
                lbTitreStockMinimum.setName("lbTitreStockMinimum");
                pnlStockDisponible.add(lbTitreStockMinimum, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- lbValeurStockMinimum ----
                lbValeurStockMinimum.setText("0");
                lbValeurStockMinimum.setPreferredSize(new Dimension(130, 25));
                lbValeurStockMinimum
                    .setFont(lbValeurStockMinimum.getFont().deriveFont(lbValeurStockMinimum.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockMinimum.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockMinimum.setMinimumSize(new Dimension(130, 30));
                lbValeurStockMinimum.setMaximumSize(new Dimension(130, 30));
                lbValeurStockMinimum.setName("lbValeurStockMinimum");
                pnlStockDisponible.add(lbValeurStockMinimum, new GridBagConstraints(8, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlCalculSeuilDeclenchement.add(pnlStockDisponible, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletGeneral.add(pnlCalculSeuilDeclenchement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlCalculQuantiteProposee ========
            {
              pnlCalculQuantiteProposee.setTitre("Calcul de la quantit\u00e9 propos\u00e9e");
              pnlCalculQuantiteProposee.setName("pnlCalculQuantiteProposee");
              pnlCalculQuantiteProposee.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCalculQuantiteProposee.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlCalculQuantiteProposee.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCalculQuantiteProposee.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
              ((GridBagLayout) pnlCalculQuantiteProposee.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ======== pnlQuantiteProposee ========
              {
                pnlQuantiteProposee.setMinimumSize(new Dimension(740, 65));
                pnlQuantiteProposee.setName("pnlQuantiteProposee");
                pnlQuantiteProposee.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlQuantiteProposee.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantiteProposee.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnlQuantiteProposee.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
                ((GridBagLayout) pnlQuantiteProposee.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
                
                // ---- lbTitreStockIdeal ----
                lbTitreStockIdeal.setText("Id\u00e9al");
                lbTitreStockIdeal.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreStockIdeal.setFont(lbTitreStockIdeal.getFont().deriveFont(lbTitreStockIdeal.getFont().getStyle() & ~Font.BOLD));
                lbTitreStockIdeal.setPreferredSize(new Dimension(130, 25));
                lbTitreStockIdeal.setMinimumSize(new Dimension(130, 30));
                lbTitreStockIdeal.setMaximumSize(new Dimension(130, 30));
                lbTitreStockIdeal.setName("lbTitreStockIdeal");
                pnlQuantiteProposee.add(lbTitreStockIdeal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurStockIdeal ----
                lbValeurStockIdeal.setText("0");
                lbValeurStockIdeal.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurStockIdeal.setFont(lbValeurStockIdeal.getFont().deriveFont(lbValeurStockIdeal.getFont().getStyle() & ~Font.BOLD));
                lbValeurStockIdeal.setPreferredSize(new Dimension(130, 25));
                lbValeurStockIdeal.setMinimumSize(new Dimension(130, 30));
                lbValeurStockIdeal.setMaximumSize(new Dimension(130, 30));
                lbValeurStockIdeal.setName("lbValeurStockIdeal");
                pnlQuantiteProposee.add(lbValeurStockIdeal, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbSymboleMoins2 ----
                lbSymboleMoins2.setText("-");
                lbSymboleMoins2.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymboleMoins2.setPreferredSize(new Dimension(20, 25));
                lbSymboleMoins2.setFont(lbSymboleMoins2.getFont().deriveFont(lbSymboleMoins2.getFont().getStyle() & ~Font.BOLD));
                lbSymboleMoins2.setName("lbSymboleMoins2");
                pnlQuantiteProposee.add(lbSymboleMoins2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbTitreQteProposeeStockDisponible ----
                lbTitreQteProposeeStockDisponible.setText("Disponible achat");
                lbTitreQteProposeeStockDisponible.setHorizontalAlignment(SwingConstants.CENTER);
                lbTitreQteProposeeStockDisponible.setFont(lbTitreQteProposeeStockDisponible.getFont()
                    .deriveFont(lbTitreQteProposeeStockDisponible.getFont().getStyle() & ~Font.BOLD));
                lbTitreQteProposeeStockDisponible.setPreferredSize(new Dimension(130, 25));
                lbTitreQteProposeeStockDisponible.setMinimumSize(new Dimension(130, 30));
                lbTitreQteProposeeStockDisponible.setMaximumSize(new Dimension(130, 30));
                lbTitreQteProposeeStockDisponible.setName("lbTitreQteProposeeStockDisponible");
                pnlQuantiteProposee.add(lbTitreQteProposeeStockDisponible, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurQteProposeeStockDisponible ----
                lbValeurQteProposeeStockDisponible.setText("0");
                lbValeurQteProposeeStockDisponible.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurQteProposeeStockDisponible.setFont(lbValeurQteProposeeStockDisponible.getFont()
                    .deriveFont(lbValeurQteProposeeStockDisponible.getFont().getStyle() & ~Font.BOLD));
                lbValeurQteProposeeStockDisponible.setPreferredSize(new Dimension(130, 25));
                lbValeurQteProposeeStockDisponible.setMinimumSize(new Dimension(130, 30));
                lbValeurQteProposeeStockDisponible.setMaximumSize(new Dimension(130, 30));
                lbValeurQteProposeeStockDisponible.setName("lbValeurQteProposeeStockDisponible");
                pnlQuantiteProposee.add(lbValeurQteProposeeStockDisponible, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbSymboleEgal2 ----
                lbSymboleEgal2.setText("=");
                lbSymboleEgal2.setHorizontalAlignment(SwingConstants.CENTER);
                lbSymboleEgal2.setPreferredSize(new Dimension(20, 25));
                lbSymboleEgal2.setFont(lbSymboleEgal2.getFont().deriveFont(lbSymboleEgal2.getFont().getStyle() & ~Font.BOLD));
                lbSymboleEgal2.setName("lbSymboleEgal2");
                pnlQuantiteProposee.add(lbSymboleEgal2, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbTitreQuantiteProposee ----
                lbTitreQuantiteProposee.setText("Quantit\u00e9 propos\u00e9e");
                lbTitreQuantiteProposee
                    .setFont(lbTitreQuantiteProposee.getFont().deriveFont(lbTitreQuantiteProposee.getFont().getStyle() & ~Font.BOLD));
                lbTitreQuantiteProposee.setMinimumSize(new Dimension(130, 30));
                lbTitreQuantiteProposee.setPreferredSize(new Dimension(130, 25));
                lbTitreQuantiteProposee.setMaximumSize(new Dimension(130, 30));
                lbTitreQuantiteProposee.setName("lbTitreQuantiteProposee");
                pnlQuantiteProposee.add(lbTitreQuantiteProposee, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbValeurQuantiteProposee ----
                lbValeurQuantiteProposee.setText("0");
                lbValeurQuantiteProposee.setPreferredSize(new Dimension(130, 25));
                lbValeurQuantiteProposee
                    .setFont(lbValeurQuantiteProposee.getFont().deriveFont(lbValeurQuantiteProposee.getFont().getStyle() & ~Font.BOLD));
                lbValeurQuantiteProposee.setMinimumSize(new Dimension(130, 30));
                lbValeurQuantiteProposee.setMaximumSize(new Dimension(130, 30));
                lbValeurQuantiteProposee.setHorizontalAlignment(SwingConstants.CENTER);
                lbValeurQuantiteProposee.setName("lbValeurQuantiteProposee");
                pnlQuantiteProposee.add(lbValeurQuantiteProposee, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- lbNote ----
                lbNote.setText(
                    "<html>Si la quantit\u00e9 propos\u00e9e est inf\u00e9rieure \u00e0 XXX qui correspond la quantit\u00e9 minimum d'achat alors la quantit\u00e9 propos\u00e9e est \u00e9gale \u00e0 cette quantit\u00e9 minimum.</html>");
                lbNote.setHorizontalAlignment(SwingConstants.LEFT);
                lbNote.setPreferredSize(new Dimension(150, 50));
                lbNote.setName("lbNote");
                pnlQuantiteProposee.add(lbNote, new GridBagConstraints(0, 2, 6, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlCalculQuantiteProposee.add(pnlQuantiteProposee, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletGeneral.add(pnlCalculQuantiteProposee, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          tbpOnglet.addTab("G\u00e9n\u00e9ral", pnlOngletGeneral);
          
          // ======== pnlOngletListeCommande ========
          {
            pnlOngletListeCommande.setEnabled(false);
            pnlOngletListeCommande.setName("pnlOngletListeCommande");
            pnlOngletListeCommande.setLayout(new BorderLayout());
            
            // ======== scpListe ========
            {
              scpListe.setPreferredSize(new Dimension(800, 400));
              scpListe.setName("scpListe");
              
              // ---- tblListe ----
              tblListe.setShowVerticalLines(true);
              tblListe.setShowHorizontalLines(true);
              tblListe.setBackground(Color.white);
              tblListe.setRowHeight(20);
              tblListe.setSelectionBackground(new Color(57, 105, 138));
              tblListe.setFont(new Font("sansserif", Font.PLAIN, 14));
              tblListe.setGridColor(new Color(204, 204, 204));
              tblListe.setOpaque(false);
              tblListe.setName("tblListe");
              scpListe.setViewportView(tblListe);
            }
            pnlOngletListeCommande.add(scpListe, BorderLayout.CENTER);
          }
          tbpOnglet.addTab("Liste des commandes de vente", pnlOngletListeCommande);
        }
        pnlPrincipal.add(tbpOnglet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbMessageErreur ----
        lbMessageErreur.setName("lbMessageErreur");
        pnlPrincipal.add(lbMessageErreur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlPrincipal, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setEnabled(false);
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    
    setSize(900, 640);
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlPrincipal;
  private JTabbedPane tbpOnglet;
  private JPanel pnlOngletGeneral;
  private SNPanelTitre pnlPanelArticle;
  private SNLabelChamp lbCodeArticle;
  private SNTexte tfCodeArticle;
  private SNLabelChamp lbLibelleArticle;
  private RiTextArea tfLibelleArticle;
  private JPanel pnlQuantiteUCA;
  private SNLabelChamp lbQuantiteUCA;
  private SNTexte tfQuantiteCommandeUCachat;
  private SNLabelUnite lbUniteCA;
  private SNPanelTitre pnlCalculSeuilDeclenchement;
  private SNPanel pnlStockDisponible;
  private SNLabelChamp lbTitreStockPhysique;
  private SNLabelChamp lbValeurStockPhysique;
  private SNLabelChamp lbSymboleMoins;
  private SNLabelChamp lbTitreStockCommande;
  private SNLabelChamp lbValeurStockCommande;
  private SNLabelChamp lbSymbolePlus;
  private SNLabelChamp lbTitreStockAttendu;
  private SNLabelChamp lbValeurStockAttendu;
  private SNLabelChamp lbSymboleEgal;
  private SNLabelChamp lbTitreStockDisponible;
  private SNLabelChamp lbValeurStockDisponible;
  private SNLabelChamp lbSymboleInferieur;
  private SNLabelChamp lbTitreStockMinimum;
  private SNLabelChamp lbValeurStockMinimum;
  private SNPanelTitre pnlCalculQuantiteProposee;
  private SNPanel pnlQuantiteProposee;
  private SNLabelChamp lbTitreStockIdeal;
  private SNLabelChamp lbValeurStockIdeal;
  private SNLabelChamp lbSymboleMoins2;
  private SNLabelChamp lbTitreQteProposeeStockDisponible;
  private SNLabelChamp lbValeurQteProposeeStockDisponible;
  private SNLabelChamp lbSymboleEgal2;
  private SNLabelChamp lbTitreQuantiteProposee;
  private SNLabelChamp lbValeurQuantiteProposee;
  private SNLabelChamp lbNote;
  private SNPanel pnlOngletListeCommande;
  private JScrollPane scpListe;
  private NRiTable tblListe;
  private SNMessage lbMessageErreur;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
