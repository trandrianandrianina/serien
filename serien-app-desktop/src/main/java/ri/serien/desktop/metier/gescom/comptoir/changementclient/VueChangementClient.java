/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.changementclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncivilite.SNCivilite;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueChangementClient extends AbstractVueDialogue<ModeleChangementClient> {
  /**
   * Constructeur.
   */
  public VueChangementClient(ModeleChangementClient pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Formatage des zones de saisie
    tfNom.init(Client.TAILLE_ZONE_NOM, true, true);
    tfComplementNom.init(Client.TAILLE_ZONE_NOM, true, true);
    tfRue.init(Client.TAILLE_ZONE_NOM, false, true);
    tfLocalisation.init(Client.TAILLE_ZONE_NOM, false, true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirCivilite();
    rafraichirNom();
    rafraichirComplementNom();
    rafraichirAdresse1();
    rafraichirAdresse2();
    rafraichirCodePostalVilleClient();
    rafraichirNomContact();
    rafraichirEmail();
    rafraichirTelephone();
    rafraichirFax();
  }
  
  // -- Méthodes privées
  
  private void rafraichirCivilite() {
    Client client = getModele().getClient();
    snCivilite.setSession(getModele().getSession());
    if (client != null && client.isParticulier()) {
      snCivilite.setIdEtablissement(client.getId().getIdEtablissement());
      snCivilite.setAucunAutorise(true);
      snCivilite.charger(false);
      IdCivilite idCivilite = null;
      Adresse adresse = client.getAdresse();
      Contact contact = client.getContactPrincipal();
      if (adresse != null && adresse.getIdCivilite() != null) {
        idCivilite = adresse.getIdCivilite();
      }
      else if (contact != null && contact.getIdCivilite() != null) {
        idCivilite = contact.getIdCivilite();
      }
      
      snCivilite.setIdSelection(idCivilite);
      lbCivilite.setVisible(true);
      snCivilite.setVisible(true);
    }
    else {
      snCivilite.setSelection(null);
      lbCivilite.setVisible(false);
      snCivilite.setVisible(false);
    }
  }
  
  private void rafraichirNom() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNom.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      Adresse adresse = client.getAdresse();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        tfNom.setText(contact.getNom());
      }
      else if (adresse != null && adresse.getNom() != null) {
        tfNom.setText(client.getAdresse().getNom());
      }
      else {
        tfNom.setText("");
      }
    }
  }
  
  private void rafraichirComplementNom() {
    Client client = getModele().getClient();
    if (client == null) {
      tfComplementNom.setText("");
    }
    else {
      Contact contact = getModele().getClient().getContactPrincipal();
      if (client.isParticulier() && contact != null && contact.getNom() != null) {
        tfComplementNom.setText(contact.getPrenom());
      }
      else if (client.getAdresse() != null && client.getAdresse().getComplementNom() != null) {
        tfComplementNom.setText(client.getAdresse().getComplementNom());
      }
      else {
        tfComplementNom.setText("");
      }
    }
  }
  
  private void rafraichirAdresse1() {
    Client client = getModele().getClient();
    if (client != null && client.getAdresse() != null && client.getAdresse().getRue() != null) {
      tfRue.setText(getModele().getClient().getAdresse().getRue());
    }
    else {
      tfRue.setText("");
    }
  }
  
  private void rafraichirAdresse2() {
    Client client = getModele().getClient();
    if (client != null && client.getAdresse() != null && client.getAdresse().getLocalisation() != null) {
      tfLocalisation.setText(getModele().getClient().getAdresse().getLocalisation());
    }
    else {
      tfLocalisation.setText("");
    }
  }
  
  private void rafraichirCodePostalVilleClient() {
    snCodePostalCommune.setSession(getModele().getSession());
    Client client = getModele().getClient();
    if (client == null) {
      snCodePostalCommune.setSelection(null);
      return;
    }
    snCodePostalCommune.setIdEtablissement(getModele().getEtablissement().getId());
    
    Adresse adresse = client.getAdresse();
    if (adresse != null) {
      CodePostalCommune codePostalCommuneClient = CodePostalCommune.getInstance(adresse.getCodePostalFormate(), adresse.getVille());
      snCodePostalCommune.charger(false);
      snCodePostalCommune.setSelection(codePostalCommuneClient);
    }
    else {
      snCodePostalCommune.setSelection(null);
    }
  }
  
  private void rafraichirNomContact() {
    Client client = getModele().getClient();
    if (client == null) {
      tfNomComplet.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNomComplet() != null) {
        tfNomComplet.setText(contact.getNomComplet());
      }
      else {
        tfNomComplet.setText("");
      }
    }
  }
  
  private void rafraichirEmail() {
    Client client = getModele().getClient();
    if (client == null) {
      tfEmail.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getEmail() != null) {
        tfEmail.setText(contact.getEmail());
      }
      else {
        tfEmail.setText("");
      }
    }
  }
  
  private void rafraichirTelephone() {
    Client client = getModele().getClient();
    if (client == null) {
      tfTelephone.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroTelephone1() != null) {
        tfTelephone.setText(contact.getNumeroTelephone1());
      }
      else if (client.getNumeroTelephone() != null) {
        tfTelephone.setText(client.getNumeroTelephone());
      }
      else {
        tfTelephone.setText("");
      }
    }
  }
  
  private void rafraichirFax() {
    Client client = getModele().getClient();
    if (client == null) {
      tfFax.setText("");
    }
    else {
      Contact contact = client.getContactPrincipal();
      if (contact != null && contact.getNumeroFax() != null) {
        tfFax.setText(contact.getNumeroFax());
      }
      else if (client.getNumeroFax() != null) {
        tfFax.setText(client.getNumeroFax());
      }
      else {
        tfFax.setText("");
      }
    }
  }
  
  // -- Méthodes interractives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNomFocusLost(FocusEvent e) {
    try {
      getModele().modifierNomClient(tfNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfComplementNomFocusLost(FocusEvent e) {
    try {
      getModele().modifierComplementNomClient(tfComplementNom.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfRueFocusLost(FocusEvent e) {
    try {
      getModele().modifierRueClient(tfRue.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLocalisationFocusLost(FocusEvent e) {
    try {
      getModele().modifierLocalisationClient(tfLocalisation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTelephoneFocusLost(FocusEvent e) {
    try {
      getModele().modifierTelephoneClient(tfTelephone.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfEmailFocusLost(FocusEvent e) {
    try {
      getModele().modifierMailClient(tfEmail.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFaxFocusLost(FocusEvent e) {
    try {
      getModele().modifierFaxClient(tfFax.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCiviliteValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierCiviliteClient(snCivilite.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCodePostalCommuneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (!getModele().isDonneesChargees()) {
        return;
      }
      getModele().modifierCodePostalCommune(snCodePostalCommune.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlCoordonneesClient = new JPanel();
    lbCivilite = new JLabel();
    snCivilite = new SNCivilite();
    lbNom = new JLabel();
    tfNom = new XRiTextField();
    lbComplementNom = new JLabel();
    tfComplementNom = new XRiTextField();
    lbLocalisation = new JLabel();
    tfRue = new XRiTextField();
    lbRue = new JLabel();
    tfLocalisation = new XRiTextField();
    lbCommune = new JLabel();
    snCodePostalCommune = new SNCodePostalCommune();
    lbContact = new JLabel();
    tfNomComplet = new XRiTextField();
    lbTelephone = new JLabel();
    tfTelephone = new XRiTextField();
    lbEmail = new JLabel();
    tfEmail = new XRiTextField();
    lbFax = new JLabel();
    tfFax = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("Changement client");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(710, 420));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCoordonneesClient ========
      {
        pnlCoordonneesClient.setBorder(new TitledBorder(null, "Coordonn\u00e9es client", TitledBorder.LEADING,
            TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
        pnlCoordonneesClient.setFont(new Font("sansserif", Font.PLAIN, 14));
        pnlCoordonneesClient.setMinimumSize(new Dimension(710, 330));
        pnlCoordonneesClient.setPreferredSize(new Dimension(710, 320));
        pnlCoordonneesClient.setOpaque(false);
        pnlCoordonneesClient.setName("pnlCoordonneesClient");
        pnlCoordonneesClient.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWidths = new int[] { 135, 0, 0, 0, 130, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCoordonneesClient.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCivilite ----
        lbCivilite.setText("Civilit\u00e9");
        lbCivilite.setPreferredSize(new Dimension(36, 19));
        lbCivilite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCivilite.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCivilite.setName("lbCivilite");
        pnlCoordonneesClient.add(lbCivilite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCivilite ----
        snCivilite.setName("snCivilite");
        snCivilite.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCiviliteValueChanged(e);
          }
        });
        pnlCoordonneesClient.add(snCivilite, new GridBagConstraints(1, 0, 4, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNom ----
        lbNom.setText("Nom");
        lbNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNom.setName("lbNom");
        pnlCoordonneesClient.add(lbNom, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNom ----
        tfNom.setBackground(Color.white);
        tfNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNom.setMinimumSize(new Dimension(485, 30));
        tfNom.setPreferredSize(new Dimension(485, 30));
        tfNom.setName("tfNom");
        tfNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfNom, new GridBagConstraints(1, 1, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 1, 1));
        
        // ---- lbComplementNom ----
        lbComplementNom.setText("Pr\u00e9nom");
        lbComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNom.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNom.setName("lbComplementNom");
        pnlCoordonneesClient.add(lbComplementNom, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfComplementNom ----
        tfComplementNom.setBackground(Color.white);
        tfComplementNom.setMinimumSize(new Dimension(485, 30));
        tfComplementNom.setPreferredSize(new Dimension(485, 30));
        tfComplementNom.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfComplementNom.setName("tfComplementNom");
        tfComplementNom.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfComplementNomFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfComplementNom, new GridBagConstraints(1, 2, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbLocalisation ----
        lbLocalisation.setText("Adresse 1");
        lbLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisation.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisation.setName("lbLocalisation");
        pnlCoordonneesClient.add(lbLocalisation, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfRue ----
        tfRue.setBackground(Color.white);
        tfRue.setMinimumSize(new Dimension(485, 30));
        tfRue.setPreferredSize(new Dimension(485, 30));
        tfRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfRue.setName("tfRue");
        tfRue.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfRueFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfRue, new GridBagConstraints(1, 3, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbRue ----
        lbRue.setText("Adresse 2");
        lbRue.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRue.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRue.setName("lbRue");
        pnlCoordonneesClient.add(lbRue, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLocalisation ----
        tfLocalisation.setBackground(Color.white);
        tfLocalisation.setMinimumSize(new Dimension(485, 30));
        tfLocalisation.setPreferredSize(new Dimension(485, 30));
        tfLocalisation.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfLocalisation.setName("tfLocalisation");
        tfLocalisation.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLocalisationFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfLocalisation, new GridBagConstraints(1, 4, 4, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbCommune ----
        lbCommune.setText("Commune");
        lbCommune.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCommune.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCommune.setName("lbCommune");
        pnlCoordonneesClient.add(lbCommune, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snCodePostalCommune ----
        snCodePostalCommune.setName("snCodePostalCommune");
        snCodePostalCommune.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snCodePostalCommuneValueChanged(e);
          }
        });
        pnlCoordonneesClient.add(snCodePostalCommune, new GridBagConstraints(1, 5, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbContact ----
        lbContact.setText("Contact");
        lbContact.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbContact.setHorizontalAlignment(SwingConstants.RIGHT);
        lbContact.setDisplayedMnemonicIndex(2);
        lbContact.setName("lbContact");
        pnlCoordonneesClient.add(lbContact, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNomComplet ----
        tfNomComplet.setBackground(Color.white);
        tfNomComplet.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNomComplet.setMinimumSize(new Dimension(215, 30));
        tfNomComplet.setPreferredSize(new Dimension(215, 30));
        tfNomComplet.setName("tfNomComplet");
        pnlCoordonneesClient.add(tfNomComplet, new GridBagConstraints(1, 6, 2, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbTelephone ----
        lbTelephone.setText("T\u00e9l\u00e9phone");
        lbTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbTelephone.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTelephone.setName("lbTelephone");
        pnlCoordonneesClient.add(lbTelephone, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfTelephone ----
        tfTelephone.setBackground(Color.white);
        tfTelephone.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfTelephone.setMinimumSize(new Dimension(140, 30));
        tfTelephone.setPreferredSize(new Dimension(140, 30));
        tfTelephone.setName("tfTelephone");
        tfTelephone.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfTelephoneFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfTelephone, new GridBagConstraints(4, 6, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 10), 0, 0));
        
        // ---- lbEmail ----
        lbEmail.setText("Email");
        lbEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbEmail.setHorizontalAlignment(SwingConstants.RIGHT);
        lbEmail.setName("lbEmail");
        pnlCoordonneesClient.add(lbEmail, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfEmail ----
        tfEmail.setBackground(Color.white);
        tfEmail.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfEmail.setMinimumSize(new Dimension(250, 30));
        tfEmail.setPreferredSize(new Dimension(250, 30));
        tfEmail.setName("tfEmail");
        tfEmail.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfEmailFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfEmail, new GridBagConstraints(1, 7, 2, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbFax ----
        lbFax.setText("Fax");
        lbFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbFax.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFax.setName("lbFax");
        pnlCoordonneesClient.add(lbFax, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfFax ----
        tfFax.setBackground(Color.white);
        tfFax.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfFax.setPreferredSize(new Dimension(140, 30));
        tfFax.setMinimumSize(new Dimension(140, 30));
        tfFax.setName("tfFax");
        tfFax.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfFaxFocusLost(e);
          }
        });
        pnlCoordonneesClient.add(tfFax, new GridBagConstraints(4, 7, 1, 1, 0.0, 0.0, GridBagConstraints.BELOW_BASELINE,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 10), 0, 0));
      }
      pnlPrincipal.add(pnlCoordonneesClient, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlCoordonneesClient;
  private JLabel lbCivilite;
  private SNCivilite snCivilite;
  private JLabel lbNom;
  private XRiTextField tfNom;
  private JLabel lbComplementNom;
  private XRiTextField tfComplementNom;
  private JLabel lbLocalisation;
  private XRiTextField tfRue;
  private JLabel lbRue;
  private XRiTextField tfLocalisation;
  private JLabel lbCommune;
  private SNCodePostalCommune snCodePostalCommune;
  private JLabel lbContact;
  private XRiTextField tfNomComplet;
  private JLabel lbTelephone;
  private XRiTextField tfTelephone;
  private JLabel lbEmail;
  private XRiTextField tfEmail;
  private JLabel lbFax;
  private XRiTextField tfFax;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
