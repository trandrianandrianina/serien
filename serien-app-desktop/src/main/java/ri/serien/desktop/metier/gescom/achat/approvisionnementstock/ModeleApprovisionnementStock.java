/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisionnementstock;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleApprovisionnementStock extends AbstractModeleDialogue {
  // Variables
  private ModeleAchat modeleParent = null;
  private IdEtablissement idEtablissement = null;
  private Fournisseur fournisseur = null;
  private IdMagasin idMagasin = null;
  private Message message = null;
  private int indiceTableau = -1;
  
  // Variables pour le chargement progressif de la liste
  private ArrayList<LigneTableau> listeLigneTableau = new ArrayList<LigneTableau>();
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheArticle = -1;
  
  /**
   * Constructeur.
   */
  public ModeleApprovisionnementStock(SessionBase pSession, ModeleAchat pModeleParent, Fournisseur pFournisseur, IdMagasin pIdMagasin) {
    super(pSession);
    modeleParent = pModeleParent;
    fournisseur = pFournisseur;
    idMagasin = pIdMagasin;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Contrôle des variables obligatoires
    if (fournisseur == null || fournisseur.getId() == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    if (idMagasin == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    idEtablissement = fournisseur.getId().getIdEtablissement();
    
    // Lancement de la recherche dès l'affichage de la boite de dialogue
    rechercherArticlesEnRupture();
  }
  
  @Override
  public void quitterAvecValidation() {
    // Finir de charger les lignes qui n'ont pas été visionnées avant leur transformation en ligne d'achat afin d'avoir toutes les
    // données nécessaires
    chargerPlageLigneTableau(0, listeLigneTableau.size());
    
    for (LigneTableau ligneTableau : listeLigneTableau) {
      // Contrôle que les lignes qui ont une quantité renseignée et supérieure à zéro soient bien transformées en ligne d'achat
      convertirLigneTableauEnLigneAchat(ligneTableau);
    }
    
    // Chargement du document
    modeleParent.setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    
    // Traitement normal
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    // Suppression éventuelle des lignes d'achat qui auraient pu être crée (lors de l'affichage du détail)
    List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
    for (LigneTableau ligneTableau : listeLigneTableau) {
      if (ligneTableau.getLigneAchatAssociee() != null) {
        listeLigneASupprimer.add(ligneTableau.getLigneAchatAssociee());
      }
    }
    // Appel du service qui va supprimer ces lignes
    if (!listeLigneASupprimer.isEmpty()) {
      ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
      // Chargement du document
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    }
    
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Afficher la plage de lignes d'achat comprise entre deux lignes.
   * Les informations des lignes d'achats sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticlesAffichees(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeLigneTableau == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeLigneTableau.size()) {
      indexPremiereLigneAffichee = listeLigneTableau.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeLigneTableau == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeLigneTableau.size()) {
      nombreLigneAffichee = listeLigneTableau.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacements de l'ascenseur avec la souris
    if (indexRechercheArticle < indexPremiereLigneAffichee) {
      indexRechercheArticle = indexPremiereLigneAffichee;
    }
    else if (indexRechercheArticle > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheArticle = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleApprovisionnementStock.class, "modifierPlageLignesAffichees", "indexPremiereLigneVisible",
        indexPremiereLigneAffichee, "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierIndiceTableau(int pIndiceTableau) {
    indiceTableau = pIndiceTableau;
  }
  
  /**
   * Modifie la quantité d'une ligne du tableau.
   */
  public void modifierQuantiteUCA(int pIndiceTableau, String pSaisie) {
    if (!isQuantiteModifiee(pIndiceTableau, pSaisie)) {
      return;
    }
    message = null;
    indiceTableau = pIndiceTableau;
    LigneTableau ligneTableau = retournerLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      return;
    }
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    // Traitement de la quantité saisie si celle-ci vaut 0
    if (saisie.compareTo(BigDecimal.ZERO) == 0) {
      // On supprime la ligne d'achat si celle-ci existe
      if (ligneTableau.getLigneAchatAssociee() != null) {
        List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
        listeLigneASupprimer.add(ligneTableau.getLigneAchatAssociee());
        ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
        ligneTableau.setLigneAchatAssociee(null);
      }
      ligneTableau.modifierQuantite(saisie);
      return;
    }
    
    // On contrôle la quantité saisie et on met à jour la quantité UCA de la ligne du tableau que l'on affiche
    try {
      ligneTableau.controlerQuantite(pSaisie);
      ligneTableau.modifierQuantite(saisie);
    }
    catch (Exception e) {
      message = Message.getMessageImportant(e.getMessage());
      rafraichir();
      return;
    }
    
    // Créer une ligne article dans le cas d'un ajout de ligne ou on met à jour la ligne d'achat si elle existe déjà
    convertirLigneTableauEnLigneAchat(ligneTableau);
    
    rafraichir();
  }
  
  /**
   * Vérifie qu'au moins un article a été commandé.
   */
  public boolean isVerifierArticleCommande() {
    if (listeLigneTableau == null || modeleParent.getDocumentAchatEnCours() == null) {
      return false;
    }
    for (LigneTableau ligneTableau : listeLigneTableau) {
      BigDecimal valeur = ligneTableau.getQuantiteCommandeeUCABigDecimal();
      if (valeur.compareTo(BigDecimal.ZERO) > 0) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Affiche le détail d'un article.
   */
  public void afficherDetailArticle() {
    LigneTableau ligneTableau = retournerLigneTableau();
    if (ligneTableau == null) {
      rafraichir();
      return;
    }
    
    BigDecimal quantiteCommandeeOrigine = ligneTableau.getQuantiteCommandeeUCABigDecimal();
    // On contrôle que la quantité du tableau soit supérieure à 0
    if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) <= 0) {
      ligneTableau.modifierQuantite(BigDecimal.ONE);
    }
    
    if (ligneTableau.getLigneAchatAssociee() == null) {
      convertirLigneTableauEnLigneAchat(ligneTableau);
    }
    LigneAchatArticle ligneAchatArticle = ligneTableau.getLigneAchatAssociee();
    boolean validation = modeleParent.selectionnerLigneArticleStandard(ligneAchatArticle, ModeleDetailLigneArticle.ONGLET_GENERAL,
        ModeleAchat.TABLE_LIGNES_DOCUMENT, null, false, false, false);
    
    // On met à jour la quantité UCA de la ligne du tableau que l'on affiche dans le cas on l'on a validé la quanitté dans le détail
    if (validation) {
      ligneTableau.modifierQuantite(ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA());
    }
    else {
      ligneTableau.modifierQuantite(quantiteCommandeeOrigine);
      // Si la quantité d'origine était à zéro à l'origine alors il faut supprimer le ligne de commande que l'on a créé pour pouvoir
      // afficher le détail
      if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) == 0) {
        List<IdLigneAchat> listeIdLigneAchat = new ArrayList<IdLigneAchat>();
        listeIdLigneAchat.add(ligneTableau.getLigneAchatAssociee().getId());
        modeleParent.setDocumentAchatEnCours(DocumentAchat.supprimerListeLigneAchat(getIdSession(),
            modeleParent.getDocumentAchatEnCours(), listeIdLigneAchat, ManagerSessionClient.getInstance().getProfil()));
        ligneTableau.setLigneAchatAssociee(null);
      }
    }
    rafraichir();
  }
  
  /**
   * Affiche le détail d'un calcul.
   */
  public void afficherDetailCalcul() {
    LigneTableau ligneTableau = retournerLigneTableau();
    if (ligneTableau == null) {
      rafraichir();
      return;
    }
    
    // On affiche la boite de dialogue qui détaille le calcul de la quantité proposée
    ModeleDetailCalcul modele = new ModeleDetailCalcul(getSession(), modeleParent, ligneTableau);
    VueDetailCalcul vue = new VueDetailCalcul(modele);
    vue.afficher();
    
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les variables liées au chargement des données dans le tableau.
   */
  private void effacerVariablesChargement() {
    indexPremiereLigneAffichee = 0;
    indexRechercheArticle = -1;
    listeLigneTableau.clear();
    
    indiceTableau = -1;
  }
  
  /**
   * Recherche les articles en rupture de stock.
   */
  private void rechercherArticlesEnRupture() {
    message = null;
    
    // Lancement de la recherche des articles normaux en rupture de stock
    CritereArticle critereArticle = new CritereArticle();
    critereArticle.setIdEtablissement(idEtablissement);
    critereArticle.setIdFournisseur(fournisseur.getId());
    critereArticle.setIdMagasin(idMagasin);
    // Comme on ne précise pas spécial ou hors gamme alors la recherche s'effectue sur les articles normaux
    List<ArticleBase> listeArticlesBases = ManagerServiceArticle.chargerListeIdArticlesEnRupture(getIdSession(), critereArticle);
    
    // Création de la liste des lignes pour le tableau
    if (listeArticlesBases == null || listeArticlesBases.isEmpty()) {
      message = Message.getMessageImportant("Aucun article n'a été trouvé.");
      return;
    }
    listeLigneTableau.clear();
    for (ArticleBase articleBase : listeArticlesBases) {
      listeLigneTableau.add(new LigneTableau(idMagasin, articleBase.getId()));
    }
    
    // Charger les lignes du tableau affichés
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
  }
  
  /**
   * Charger les données des lignes du tableau comprises entre deux lignes.
   * Les informations des lignes du tableau sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageLigneTableau(int pPremiereLigne, int pNombreLigne) {
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeLigneTableau.size());
    
    // Lister les id des lignes du tableau dont il faut charger les informations
    List<IdArticle> listeIdArticle = new ArrayList<IdArticle>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneTableau ligneTableau = listeLigneTableau.get(i);
      if (!ligneTableau.isCharge()) {
        listeIdArticle.add(ligneTableau.getIdArticle());
      }
    }
    
    // Quitter immédiatement si aucun article n'est à charger
    if (listeIdArticle.isEmpty()) {
      return;
    }
    
    // Charger les informations des lignes du tableau
    ParametresLireArticle parametres = new ParametresLireArticle();
    parametres.setIdEtablissement(idEtablissement);
    parametres.setIdFournisseur(fournisseur.getId());
    parametres.setIdMagasin(idMagasin);
    ListeArticle listeArticlesCharges = ManagerServiceArticle.chargerListeArticlesEnRupture(getIdSession(), listeIdArticle, parametres);
    
    // Mettre les lignes du tableau chargées au bon endroit dans la liste
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneTableau ligneTableau = listeLigneTableau.get(i);
      if (!ligneTableau.isCharge()) {
        for (Article articleCharge : listeArticlesCharges) {
          if (ligneTableau.getIdArticle().equals(articleCharge.getId())) {
            listeLigneTableau.get(i).initialiserDonnees(articleCharge);
            Trace.debug(ModeleApprovisionnementStock.class, "chargerPlageLigneTableau", "Charger ligne tableau", "ligne", i, "idArticle",
                articleCharge.getId());
            break;
          }
        }
      }
    }
    
    // Chargement du document
    /*
    modeleParent.setDocumentEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentEnCours()));
    // On vérifie que si des lignes achats existent déjà alors on les lie avec la listeLigneTableau
    for (LigneTableau ligneTableau : listeLigneTableau) {
      LigneAchatArticle ligneAchat = retournerLigneAchatArticle(ligneTableau);
      ligneTableau.setLigneAchatAssociee(ligneAchat);
    }
    */
  }
  
  /**
   * Retourne la ligne sélectionnée du tableau.
   */
  private LigneTableau retournerLigneTableau() {
    if (indiceTableau < 0 || indiceTableau >= listeLigneTableau.size()) {
      return null;
    }
    return listeLigneTableau.get(indiceTableau);
  }
  
  /**
   * Retourne la ligne d'achat article correspondant à l'id article.
   */
  private LigneAchatArticle retournerLigneAchatArticle(IdLigneAchat pIdLigneAchat) {
    if (pIdLigneAchat == null) {
      return null;
    }
    ListeLigneAchat listeLignesDocument = modeleParent.getDocumentAchatEnCours().getListeLigneAchat();
    // Récupération de la ligne achat article
    LigneAchat ligneAchat = listeLignesDocument.get(pIdLigneAchat);
    // Il est fortement possible que l'on ait annuler lors de la demande de confirmation (affichage des commandes en cours)
    if (ligneAchat == null) {
      return null;
    }
    LigneAchatArticle ligneAchatArticle = (LigneAchatArticle) ligneAchat;
    return ligneAchatArticle;
  }
  
  /**
   * Converti une ligne du tableau en ligne d'achat ou met à jour une ligne d'achat déjà existante.
   */
  private void convertirLigneTableauEnLigneAchat(LigneTableau pLigneTableau) {
    // On contrôle que la quantité du tableau soit valide
    if (pLigneTableau == null) {
      return;
    }
    BigDecimal valeur = pLigneTableau.getQuantiteCommandeeUCABigDecimal();
    // On contrôle que la quantité du tableau soit supérieure à 0
    if (valeur.compareTo(BigDecimal.ZERO) <= 0) {
      return;
    }
    
    // Créer une ligne article dans le cas d'un ajout de ligne
    if (pLigneTableau.getLigneAchatAssociee() == null) {
      IdLigneAchat idLigneAchat =
          modeleParent.modifierQuantiteLigneTableau(pLigneTableau.getArticle(), pLigneTableau.getQuantiteCommandeeUCA(), -1, false);
      // Chargement du document
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
      // Récupération de la ligne achat article à partir de son id
      pLigneTableau.setLigneAchatAssociee(retournerLigneAchatArticle(idLigneAchat));
    }
    // Dans le cas d'une mise à jour d'une ligne achat déjà existante
    else {
      LigneAchatArticle ligneAchat = pLigneTableau.getLigneAchatAssociee();
      ligneAchat.getPrixAchat().setQuantiteReliquatUCA(valeur, true);
      // Si on est dans une commande direct usine le type de la ligne doit être ligne spéciale non stockée (code ERL == 'S')
      if (modeleParent.getDocumentAchatEnCours().isDirectUsine() && !ligneAchat.isLigneCommentaire()) {
        ligneAchat.setTypeLigne(EnumTypeLigneAchat.SPECIALE_NON_STOCKEE);
      }
      ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), ligneAchat, modeleParent.getDateTraitementDocument());
    }
  }
  
  /**
   * Contrôle si la quantitée saisie est différente de la valeur d'origine.
   */
  private boolean isQuantiteModifiee(int pIndiceTableau, String pSaisie) {
    indiceTableau = pIndiceTableau;
    LigneTableau ligneTableau = retournerLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      return false;
    }
    pSaisie = Constantes.normerTexte(pSaisie);
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    return saisie.compareTo(ligneTableau.getQuantiteCommandeeUCABigDecimal()) != 0;
  }
  
  // -- Accesseurs
  
  public ArrayList<LigneTableau> getListeChargement() {
    return listeLigneTableau;
  }
  
  public Message getMessage() {
    return message;
  }
  
  public int getIndiceTableau() {
    return indiceTableau;
  }
  
}
