/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.selectioncommandeclient;

import java.io.Serializable;

import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

/**
 * Description d'une ligne à afficher dans le tableau des commandes de vente contenant des articles du fournisseur en cours.
 */
public class LigneAAfficher implements Serializable {
  // Variables de travail
  private IdDocumentVente idDocumentVente = null;
  private DocumentVente documentVente = null;
  private boolean charge = false;
  
  /**
   * Constructeur.
   */
  public LigneAAfficher(IdDocumentVente pIdDocumentVente) {
    idDocumentVente = pIdDocumentVente;
  }
  
  // -- Méthodes public
  
  /**
   * Initialise les données.
   */
  public void initialiserDonnees(DocumentVente pDocumentVente) {
    if (pDocumentVente == null) {
      return;
    }
    documentVente = pDocumentVente;
    idDocumentVente = pDocumentVente.getId();
    
    // Si on est arrivé ici alors la ligne est complètement chargée
    charge = true;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public boolean isCharge() {
    return charge;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
}
