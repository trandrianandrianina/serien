/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentvente;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererLigne extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color blanc = new Color(255, 255, 255);
  private static Color vert = new Color(152, 206, 168);
  private static Color orange = new Color(211, 187, 114);
  private static Color rouge = new Color(218, 166, 161);
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Taille des colonnes
    redimensionnerColonnes(table);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setCellRenderer(leftRenderer);
    cm.getColumn(1).setCellRenderer(rightRenderer);
    cm.getColumn(2).setCellRenderer(centerRenderer);
    cm.getColumn(3).setCellRenderer(leftRenderer);
    cm.getColumn(4).setCellRenderer(rightRenderer);
    cm.getColumn(5).setCellRenderer(rightRenderer);
    cm.getColumn(6).setCellRenderer(centerRenderer);
    cm.getColumn(7).setCellRenderer(leftRenderer);
    cm.getColumn(8).setCellRenderer(rightRenderer);
    cm.getColumn(9).setCellRenderer(rightRenderer);
    cm.getColumn(10).setCellRenderer(rightRenderer);
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    // int tailleNumero = 70;
    int tailleQuantite = 60;
    int tailleDate = 80;
    int tailleUnite = 30;
    int tailleLibelle = 140;
    int tailleMontant = 100;
    
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(100);
    cm.getColumn(0).setMaxWidth(100);
    cm.getColumn(1).setMaxWidth(tailleQuantite);
    cm.getColumn(1).setMinWidth(tailleQuantite);
    cm.getColumn(2).setMinWidth(tailleUnite);
    cm.getColumn(2).setMaxWidth(tailleUnite);
    cm.getColumn(3).setMinWidth(tailleLibelle);
    cm.getColumn(4).setMinWidth(tailleMontant);
    cm.getColumn(4).setMaxWidth(tailleMontant);
    cm.getColumn(5).setMinWidth(tailleQuantite);
    cm.getColumn(5).setMaxWidth(tailleQuantite);
    cm.getColumn(6).setMinWidth(tailleUnite);
    cm.getColumn(6).setMaxWidth(tailleUnite);
    cm.getColumn(7).setMinWidth(tailleDate);
    cm.getColumn(7).setMaxWidth(tailleDate);
    cm.getColumn(8).setMinWidth(tailleMontant);
    cm.getColumn(8).setMaxWidth(tailleMontant);
    cm.getColumn(9).setMinWidth(tailleMontant);
    cm.getColumn(9).setMaxWidth(tailleMontant);
    cm.getColumn(10).setMinWidth(tailleMontant);
    cm.getColumn(10).setMaxWidth(tailleMontant);
    
  }
}
