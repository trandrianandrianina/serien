/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.retourarticle;

import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;

public class JTableRetourArticleStringRenderer extends DefaultTableCellRenderer {
  // Variable
  private ListeLigneVente listeLignes;
  
  /**
   * Constructeur.
   */
  public JTableRetourArticleStringRenderer(ListeLigneVente pListeLignes) {
    listeLignes = pListeLignes;
  }
  
  // -- Méthodes publiques
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Pour les cellules de type label
    JLabel label = (JLabel) c;
    switch (column) {
      case 1:
        label.setHorizontalAlignment(LEFT);
        break;
      case 2:
        label.setHorizontalAlignment(CENTER);
        break;
      case 3:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 4:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 5:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 6:
        label.setHorizontalAlignment(RIGHT);
        break;
    }
    
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setResizable(false);
    cm.getColumn(0).setMinWidth(50);
    cm.getColumn(0).setMaxWidth(50);
    cm.getColumn(1).setResizable(false);
    cm.getColumn(1).setPreferredWidth(90);
    cm.getColumn(2).setPreferredWidth(80);
    cm.getColumn(3).setResizable(false);
    cm.getColumn(3).setPreferredWidth(100);
    cm.getColumn(4).setResizable(false);
    cm.getColumn(4).setPreferredWidth(100);
    cm.getColumn(5).setResizable(false);
    cm.getColumn(5).setPreferredWidth(100);
    cm.getColumn(6).setResizable(false);
    cm.getColumn(6).setPreferredWidth(100);
    
    return c;
  }
  
  // -- Accesseurs
  
  public void setListeLignes(ListeLigneVente listeLignes) {
    this.listeLignes = listeLignes;
  }
}
