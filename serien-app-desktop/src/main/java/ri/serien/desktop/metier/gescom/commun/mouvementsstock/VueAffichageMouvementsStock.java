/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.mouvementsstock;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;

import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue pour l'affichage de la liste des mouvements de stock.
 */
public class VueAffichageMouvementsStock extends AbstractVuePanel<ModeleAffichageMouvementsStock> {
  // Constantes
  private static final String[] TITRE_LISTE_MOUVEMENTS_VENTE =
      new String[] { "Date", "Op\u00e9ration", "Tiers ou magasinier", "Num\u00e9ro bon", "Disponible vente", "Mouvement", "Physique" };
  private static final String[] TITRE_LISTE_MOUVEMENTS_ACHAT =
      new String[] { "Date", "Op\u00e9ration", "Tiers ou magasinier", "Num\u00e9ro bon", "Disponible achat", "Mouvement", "Physique" };
  private static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");
  
  /**
   * Constructeur.
   */
  public VueAffichageMouvementsStock(ModeleAffichageMouvementsStock pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    Boolean isModeAchat = getModele().getTypeStockDisponible().equals(EnumTypeStockDisponible.TYPE_ACHAT);
    if (isModeAchat) {
      tblMouvements.personnaliserAspect(TITRE_LISTE_MOUVEMENTS_ACHAT, new int[] { 90, 100, 140, 100, 120, 120, 120 },
          new int[] { 90, 100, -1, 100, 120, 120, 120 }, new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE },
          14);
    }
    else {
      tblMouvements.personnaliserAspect(TITRE_LISTE_MOUVEMENTS_VENTE, new int[] { 90, 100, 140, 100, 120, 120, 120 },
          new int[] { 90, 100, -1, 100, 120, 120, 120 }, new int[] { NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.DROITE,
              NRiTable.DROITE, NRiTable.DROITE, NRiTable.DROITE },
          14);
    }
    
    // Mettre le fond de la liste en blanc
    scpMouvements.setBackground(Color.WHITE);
    scpMouvements.getViewport().setBackground(Color.WHITE);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpMouvements.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpMouvementsStateChanged(e);
      }
    });
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblMouvements.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeMouvementStockSelectionChanged(e);
      }
    });
    
  }
  
  @Override
  public void rafraichir() {
    rafraichirMagasin();
    rafraichirListe();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMagasin() {
    cbMagasinsMouvements.setSession(getModele().getSession());
    cbMagasinsMouvements.setIdEtablissement(getModele().getArticle().getId().getIdEtablissement());
    cbMagasinsMouvements.charger(false);
    cbMagasinsMouvements.setSelection(getModele().getMagasin());
  }
  
  /**
   * Affichage des données du sous-onglet tous les dépots
   */
  private void rafraichirListe() {
    ListeLigneMouvement lignesMouvements = getModele().getListeMouvementStock();
    BigDecimal quantitePrecedente = BigDecimal.ZERO;
    Object[][] donnees = null;
    
    if (lignesMouvements != null) {
      donnees = new Object[lignesMouvements.size()][tblMouvements.getColumnCount()];
      for (int ligne = 0; ligne < lignesMouvements.size(); ligne++) {
        if (lignesMouvements.get(ligne) == null
            && !lignesMouvements.get(ligne).getId().getIdMagasin().equals(getModele().getMagasin().getId())) {
          continue;
        }
        // Date mouvement
        donnees[ligne][0] = DATE_FORMATTER.format(lignesMouvements.get(ligne).getId().getDateMouvement());
        // Opérations
        if (lignesMouvements.get(ligne).getCodeOperation() != null) {
          donnees[ligne][1] = lignesMouvements.get(ligne).getCodeOperation().getLibelle();
        }
        // Tiers ou magasinier
        if (lignesMouvements.get(ligne).getIdClient() != null) {
          donnees[ligne][2] = "Client : " + getModele().getNomClient(lignesMouvements.get(ligne).getIdClient());
        }
        else if (lignesMouvements.get(ligne).getIdFournisseur() != null) {
          donnees[ligne][2] = "Fournisseur : " + getModele().getNomFournisseur(lignesMouvements.get(ligne).getIdFournisseur());
        }
        else if (lignesMouvements.get(ligne).getIdMagasinier() != null) {
          donnees[ligne][2] = "Magasinier : " + getModele().getNomMagasinier(lignesMouvements.get(ligne).getIdMagasinier());
        }
        // Numéro
        if (lignesMouvements.get(ligne).getNumeroBonOrigine() != null) {
          donnees[ligne][3] = Constantes.convertirIntegerEnTexte(lignesMouvements.get(ligne).getNumeroBonOrigine(), 6);
        }
        // Quantité avant mouvement
        if (lignesMouvements.get(ligne).getQuantiteMouvement() != null || lignesMouvements.get(ligne).getQuantiteStock() != null) {
          if (ligne == 0) {
            quantitePrecedente =
                lignesMouvements.get(ligne).getQuantiteStock().subtract(lignesMouvements.get(ligne).getQuantiteMouvement());
          }
          else {
            quantitePrecedente = lignesMouvements.get(ligne - 1).getQuantiteStock();
          }
          
          donnees[ligne][4] = Constantes.formater(quantitePrecedente, false);
          // Quantité du mouvement
          donnees[ligne][5] = Constantes.formater(lignesMouvements.get(ligne).getQuantiteMouvement(), false);
          // Solde en stock
          donnees[ligne][6] = Constantes.formater(lignesMouvements.get(ligne).getQuantiteStock(), false);
        }
      }
    }
    
    // Mettre à jour le tableau
    tblMouvements.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne
    if (lignesMouvements != null) {
      int index = lignesMouvements.getIndexSelection();
      if (index > -1 && index < tblMouvements.getRowCount()) {
        tblMouvements.getSelectionModel().setSelectionInterval(index, index);
      }
      else {
        tblMouvements.getSelectionModel().clearSelection();
      }
    }
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void scpMouvementsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblMouvements == null) {
        return;
      }
      Trace.debug(VueAffichageMouvementsStock.class, "scpMouvementsStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpMouvements.getViewport().getViewRect();
      int premiereLigne = tblMouvements.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblMouvements.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblMouvements.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDocumentsAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeMouvementStockSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ces changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierSelection(tblMouvements.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinsMouvementsValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMagasin(cbMagasinsMouvements.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPanelPrincipal = new SNPanelContenu();
    pnlChoixMagasin = new SNPanel();
    lbMagasinMouvements = new SNLabelChamp();
    cbMagasinsMouvements = new SNMagasin();
    scpMouvements = new JScrollPane();
    tblMouvements = new NRiTable();
    
    // ======== this ========
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPanelPrincipal ========
    {
      pnlPanelPrincipal.setMinimumSize(new Dimension(917, 515));
      pnlPanelPrincipal.setMaximumSize(new Dimension(917, 515));
      pnlPanelPrincipal.setName("pnlPanelPrincipal");
      pnlPanelPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWidths = new int[] { 135, 0 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowHeights = new int[] { 0, 266, 0 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlPanelPrincipal.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlChoixMagasin ========
      {
        pnlChoixMagasin.setName("pnlChoixMagasin");
        pnlChoixMagasin.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlChoixMagasin.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlChoixMagasin.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlChoixMagasin.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlChoixMagasin.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbMagasinMouvements ----
        lbMagasinMouvements.setText("Magasin");
        lbMagasinMouvements.setName("lbMagasinMouvements");
        pnlChoixMagasin.add(lbMagasinMouvements, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- cbMagasinsMouvements ----
        cbMagasinsMouvements.setBackground(new Color(171, 148, 79));
        cbMagasinsMouvements.setMinimumSize(new Dimension(400, 30));
        cbMagasinsMouvements.setPreferredSize(new Dimension(400, 30));
        cbMagasinsMouvements.setMaximumSize(new Dimension(32767, 30));
        cbMagasinsMouvements.setName("cbMagasinsMouvements");
        cbMagasinsMouvements.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            cbMagasinsMouvementsValueChanged(e);
          }
        });
        pnlChoixMagasin.add(cbMagasinsMouvements, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPanelPrincipal.add(pnlChoixMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpMouvements ========
      {
        scpMouvements.setName("scpMouvements");
        
        // ---- tblMouvements ----
        tblMouvements.setShowVerticalLines(true);
        tblMouvements.setShowHorizontalLines(true);
        tblMouvements.setBackground(Color.white);
        tblMouvements.setOpaque(false);
        tblMouvements.setRowHeight(20);
        tblMouvements.setGridColor(new Color(204, 204, 204));
        tblMouvements.setName("tblMouvements");
        scpMouvements.setViewportView(tblMouvements);
      }
      pnlPanelPrincipal.add(scpMouvements,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPanelPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlPanelPrincipal;
  private SNPanel pnlChoixMagasin;
  private SNLabelChamp lbMagasinMouvements;
  private SNMagasin cbMagasinsMouvements;
  private JScrollPane scpMouvements;
  private NRiTable tblMouvements;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
