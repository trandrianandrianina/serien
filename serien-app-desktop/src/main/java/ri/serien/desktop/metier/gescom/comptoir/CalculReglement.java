/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.desktop.metier.gescom.comptoir.propositionrendumonnaieacompte.ModeleProposerRenduMonnaieAcompte;
import ri.serien.desktop.metier.gescom.comptoir.propositionrendumonnaieacompte.VueProposerRenduMonnaieAcompte;
import ri.serien.libcommun.gescom.commun.document.EnumEtatReglement;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.reglement.Acompte;
import ri.serien.libcommun.gescom.vente.reglement.IdReglement;
import ri.serien.libcommun.gescom.vente.reglement.IdReglementAcompte;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.reglement.Reglement;
import ri.serien.libcommun.gescom.vente.reglement.ReglementAcompte;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.moteur.interpreteur.SessionBase;

/**
 * Cette classe concentre tous les calculs et les contrôles nécessaires au règlement d'un document de vente:
 * - acompte lors d'une commande
 * - montant total du document
 * Ces calculs sont basés sur la liste des règlements d'un document.
 */
public class CalculReglement implements Serializable {
  // Variables
  private SessionBase session = null;
  private IdEtablissement idEtablissement = null;
  private Etablissement etablissement = null;
  private Integer numeroDocument = null;
  private EnumTypeDocumentVente typeDocumentVente = null;
  private boolean documentModifiable = true;
  private String devise = "\u20ac";
  private Acompte acompte = null;
  private boolean remboursement = false;
  // Il s'agit du total du document à régler
  private BigDecimal montantTotalARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  // Il s'agit du montant actuel à régler (par exemple lors d'un acompte)
  private BigDecimal montantActuelARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  private BigDecimal montantDejaRegle = null;
  private BigDecimal montantDejaRegleEnEspeces = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  private BigDecimal resteARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  private BigDecimal montantARendre = null;
  private boolean rendreMonnaieSurAcompte = false;
  private ListeReglement listeReglement = null;
  private boolean differerAvoir = false;
  private boolean differerPaiement = false;
  // Contient texte détaillant le règlement
  private StringBuffer note = new StringBuffer();
  
  /**
   * Constructeur.
   */
  public CalculReglement(SessionBase pSession, EnumTypeDocumentVente pTypeDocumentVente, BigDecimal pTotalTTC,
      ListeReglement pListeReglement, IdEtablissement pIdEtablissement, Integer pNumeroDocument, Acompte pAcompte,
      boolean pDocumentModifiable) {
    session = pSession;
    idEtablissement = pIdEtablissement;
    numeroDocument = pNumeroDocument;
    typeDocumentVente = pTypeDocumentVente;
    listeReglement = pListeReglement;
    acompte = pAcompte;
    documentModifiable = pDocumentModifiable;
    
    effacerVariables();
    modifierMontantTotalARegler(pTotalTTC);
    mettreAJourDonnees();
    etablissement = Etablissement.charger(session.getIdSession(), idEtablissement);
  }
  
  // -- Nouvelles méthodes
  
  /**
   * Ajoute un nouveau règlement.
   */
  public void ajouterReglement(Reglement pReglement, EnumTypeDocumentVente pTypeDocumentVente) {
    if (pReglement == null) {
      throw new MessageErreurException("Le règlement à ajouter est invalide.");
    }
    if (listeReglement.size() >= ListeReglement.NOMBRE_REGLEMENTS_MAX) {
      throw new MessageErreurException(
          "Vous avez dépassé le nombre de règlements possibles qui est de " + ListeReglement.NOMBRE_REGLEMENTS_MAX
              + ".\nVeuillez cliquer sur le bouton \"Effacer ce règlement\" afin de recommencer le règlement.");
    }
    typeDocumentVente = pTypeDocumentVente;
    
    if (!remboursement) {
      // Le traitement du règlement en espèces est différent à cause du rendu monnaie
      if (pReglement.isEnEspeces()) {
        // Mise à jour du montant total en espèces pour vérification du non dépassement du maximum autorisé
        mettreAJourMontantRegleEspeces();
        montantDejaRegleEnEspeces = montantDejaRegleEnEspeces.add(pReglement.getMontant());
        // Le montant saisi ne peut pas exéder le montant maximal payable en espèces fixé dans la personnalisation établissement (DG)
        if (montantDejaRegleEnEspeces.compareTo(etablissement.getMontantMaximumEspeces()) == 1) {
          throw new MessageErreurException(
              "Il n'est pas   possible d'effectuer un  paiement en  espèces supérieur\nà " + etablissement.getMontantMaximumEspeces()
                  + " euros sur un document de ventes.\nVeuillez utiliser un  autre moyen de  paiement");
        }
        // Vérification si la boite de dialogue du controle de rendu monnaie sur l'acompte doit être affichée
        if (isAfficherPopositionRenduMonnaieAcompte(pReglement)) {
          // Affichage de la boite de dialogue
          ModeleProposerRenduMonnaieAcompte modele = new ModeleProposerRenduMonnaieAcompte(session, acompte);
          VueProposerRenduMonnaieAcompte vue = new VueProposerRenduMonnaieAcompte(modele);
          vue.afficher();
          // Si pas de rendu monnaie alors le montant actuel n'est plus le montant de l'acompte mais le montant total du document
          // et l'acompte est considéré comme réglé
          rendreMonnaieSurAcompte = modele.isRendreMonnaie();
          if (!rendreMonnaieSurAcompte) {
            montantActuelARegler = montantTotalARegler;
            resteARegler = montantActuelARegler.subtract(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          }
        }
      }
      // Calcul avec ce nouveau règlement
      payer(pReglement);
    }
    else {
      // Contrôle du signe du montant car il s'agit d'un remboursement : il doit être négatif
      if (pReglement.getMontant().compareTo(BigDecimal.ZERO) > 0) {
        pReglement.setMontant(pReglement.getMontant().negate());
      }
      // Calculs avec ce nouveau règlement
      rembourser(pReglement);
    }
    
    // Ajout du règlement à la liste
    listeReglement.add(pReglement);
    
    // Génération de la note affichant le détail des règlements
    genererNote();
  }
  
  /**
   * Suppression d'un règlement.
   * Cas par exemple d'un avoir qui serai désélectionné de la liste.
   */
  public void supprimerReglement(IdReglement pIdReglement) {
    if (pIdReglement == null) {
      throw new MessageErreurException("Impossible de supprimer le règlement car l'id de ce dernier est invalide.");
    }
    Reglement reglementASupprimer = listeReglement.retournerReglementParId(pIdReglement);
    if (reglementASupprimer == null) {
      throw new MessageErreurException("Impossible de supprimer le règlement car ce dernier est invalide.");
    }
    
    // Suppression du règlement de la liste des règlements
    listeReglement.remove(reglementASupprimer);
    
    // Recalcul
    BigDecimal montantReglement = reglementASupprimer.getMontant();
    montantDejaRegle = montantDejaRegle.subtract(montantReglement);
    resteARegler = resteARegler.add(montantReglement);
    if (reglementASupprimer.isEnEspeces()) {
      montantDejaRegleEnEspeces = montantDejaRegleEnEspeces.subtract(montantReglement);
    }
    
    // Génération de la note affichant le détail des règlements
    genererNote();
  }
  
  /**
   * Met à jour les valeurs après modification du montant du document.
   */
  public void modifierMontantTotalARegler(BigDecimal pMontantTotal) {
    if (listeReglement == null) {
      return;
    }
    listeReglement.modifierMontantTotalARegler(pMontantTotal);
    
    // Paiement ou remboursement
    // Il s'agit d'un remboursement si le montant total est négatif
    if (pMontantTotal.compareTo(BigDecimal.ZERO) > 0) {
      remboursement = false;
    }
    else {
      remboursement = true;
    }
    
    // Cas du paiement
    if (!remboursement) {
      // Le document a été totalement règlé et le montant total du document correspond au montant réglé
      if (listeReglement.getEtatReglement() != null && listeReglement.getEtatReglement().equals(EnumEtatReglement.REGLE)) {
        // Cas de la consultation d'un document réglé entièrement
        if (listeReglement.getMontantTotalCalcule().compareTo(pMontantTotal) == 0) {
          montantTotalARegler = listeReglement.getMontantTotalARegler();
          montantActuelARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          montantDejaRegle = montantTotalARegler;
        }
        // Cas de l'extraction partielle avec réglement intégral lors de la commande d'origine
        else {
          montantTotalARegler = pMontantTotal.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          montantActuelARegler = montantTotalARegler;
          montantDejaRegle = listeReglement.getMontantTotalCalcule();
        }
        resteARegler = montantTotalARegler.subtract(listeReglement.getMontantTotalCalcule()).setScale(Constantes.DEUX_DECIMALES,
            RoundingMode.HALF_UP);
      }
      // Le document a été partiellement payé
      else {
        montantTotalARegler = pMontantTotal.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        montantActuelARegler = montantTotalARegler;
        montantDejaRegle = listeReglement.getMontantTotalCalcule();
        resteARegler = montantActuelARegler.subtract(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        
        // S'il existe un acompte son montant doit être mis à jour par rapport au nouveau montant du document
        if (acompte != null) {
          acompte.modifierMontantAcompte(montantTotalARegler);
        }
        
        // Contrôle l'existence d'un acompte à régler si c'est le cas le montant actuel à régler devient le montant de l'acompte
        if (isReglementAcompteEnCours()) {
          // Vérification que le montant de l'acompte soit supérieur au montant déjà réglé
          if (acompte != null && listeReglement.getMontantTotalCalcule().compareTo(acompte.getMontant()) >= 0) {
            rendreMonnaieSurAcompte = false;
          }
          // Sinon on continue de régler l'acompte
          else {
            montantActuelARegler = acompte.getMontant();
            resteARegler = montantActuelARegler.subtract(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          }
        }
      }
    }
    // Cas du remboursement
    else {
      // Le document a été totalement remboursé et le montant total du document correspond au montant remboursé
      if (listeReglement.getEtatReglement() != null && listeReglement.getEtatReglement().equals(EnumEtatReglement.REGLE)) {
        // Cas de la consultation d'un document régle entièrement
        if (listeReglement.getMontantTotalCalcule().compareTo(pMontantTotal) == 0) {
          montantTotalARegler = listeReglement.getMontantTotalARegler();
          montantActuelARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          montantDejaRegle = montantTotalARegler;
        }
      }
      // Cas normal du remboursement
      else {
        montantTotalARegler = pMontantTotal.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        montantActuelARegler = montantTotalARegler;
        if (montantDejaRegle.compareTo(BigDecimal.ZERO) == 0 && listeReglement.getMontantTotalCalcule().compareTo(BigDecimal.ZERO) != 0) {
          montantDejaRegle = listeReglement.getMontantTotalCalcule();
        }
        resteARegler = montantActuelARegler.subtract(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
    }
    
    // Création de la note
    genererNote();
  }
  
  /**
   * Remet à zéro le montant à rendre.
   */
  public void mettreAZeroMontantARendre() {
    montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Controle qu'il y ait au moins un règlement de saisi.
   */
  public boolean isExisteAuMoinsUnReglement() {
    if (listeReglement == null) {
      return false;
    }
    return listeReglement.size() > 0;
  }
  
  /**
   * Contrôle l'affichage de la boite de dialogue qui propose la gestion du rendu monnaie sur acompte.
   */
  public boolean isAfficherPopositionRenduMonnaieAcompte(Reglement pReglement) {
    // Pas d'affichage de la boite de dialogue:
    // - si le reglèment n'est pas en espèces
    // - si l'on n'est pas en train de payer l'acompte
    // - si l'acompte est réglé
    // - si le pourcentage de cet acompte est égal à 100%
    // - si le montant du règlement est inférieur ou égal au reste à régler
    if (!pReglement.isEnEspeces()) {
      return false;
    }
    BigDecimal montantEnEspeces = pReglement.getMontant();
    if (!isReglementAcompteEnCours() || isAcompteRegle() || Acompte.isTotal(acompte) || montantEnEspeces.compareTo(resteARegler) <= 0) {
      return false;
    }
    
    // Le rendu monnaie est effectué uniquement si le montant à rendre est inférieur ou égal au montant total en espèces réglés
    BigDecimal montantARendre = montantEnEspeces.subtract(resteARegler);
    if (montantEnEspeces.compareTo(montantARendre) >= 0) {
      return true;
    }
    return false;
  }
  
  /**
   * Controle si l'avoir peut être différé.
   * Ce doit être un remboursement et il ne doit pas être entierement réglé.
   */
  public boolean avoirPeutEtreDiffere() {
    return remboursement && !isEntierementRegle();
  }
  
  /**
   * Retourne si le règlement de l'acompt est réglé.
   */
  public boolean isAcompteRegle() {
    return listeReglement.isAcompteRegle(acompte);
  }
  
  /**
   * Détermine si le règlement est totalement supprimable.
   */
  public boolean isReglementSupprimable() {
    // Non supprimable si le règlement est vide
    if (listeReglement == null || listeReglement.isEmpty()) {
      return false;
    }
    // Non supprimable si le règlement n'a jamais été sauvé en base
    else if (listeReglement.getListeReglementOriginale() == null) {
      return false;
    }
    // Non supprimable si les règlements ne date pas du jour
    else if (!listeReglement.isRegleAujourdhui()) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne si le règlement qui est en cours correspond à celui de l'acompte.
   */
  private boolean isReglementAcompteEnCours() {
    return acompte != null && typeDocumentVente.equals(EnumTypeDocumentVente.COMMANDE);
  }
  
  /**
   * Met à jour les données à partir de la liste des règlements du document.
   */
  private void mettreAJourDonnees() {
    if (listeReglement == null || listeReglement.isEmpty()) {
      return;
    }
    // Met à jour le montant total déjà réglé sur ce document en espèces
    mettreAJourMontantRegleEspeces();
    // Il n'y a pas de montant à rendre puisqu'il s'agit d'un récapitulatif de ce qui a déjà été réglé
    mettreAZeroMontantARendre();
    // Génére le détail de la note
    genererNote();
  }
  
  /**
   * Met à jour le montant total déjà réglé sur ce document en espèces
   */
  private void mettreAJourMontantRegleEspeces() {
    montantDejaRegleEnEspeces = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    if (listeReglement == null || listeReglement.isEmpty()) {
      return;
    }
    for (Reglement reglement : listeReglement) {
      if (reglement.isEnEspeces()) {
        montantDejaRegleEnEspeces = montantDejaRegleEnEspeces.add(reglement.getMontant());
      }
    }
  }
  
  /**
   * Effectue les calculs pour un règlement de type paiement.
   */
  private void payer(Reglement pReglement) {
    // Contrôle du montant saisi
    BigDecimal montantReglement = pReglement.getMontant();
    if (montantReglement == null || montantReglement.compareTo(BigDecimal.ZERO) <= 0) {
      throw new MessageErreurException("Le montant du règlement est invalide.");
    }
    
    // Cas du règlement du reste de la commande juste après avoir réglé l'acompte (il peut y avoir eu un rendu monnaie qui risque de
    // fausser les calculs donc on le sauvegarde pour l'afficher dans la note)
    if (isReglementAcompteEnCours() && isAcompteRegle()) {
      montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    }
    
    // Calcul du reste à régler par rapport au montant total du document
    BigDecimal resteActuelARegler = montantActuelARegler.subtract(montantDejaRegle).subtract(montantReglement);
    // Si le reste à régler est négatif c'est que l'on a trop perçu et en fonction du mode de règlement le traitement est différent
    if (resteActuelARegler.compareTo(BigDecimal.ZERO) < 0) {
      // Si le règlement est en espèces il faut calculer le rendu monnaie
      if (pReglement.isEnEspeces()) {
        montantARendre = resteActuelARegler.abs();
        pReglement.setMontant(montantReglement.subtract(montantARendre));
      }
      // Si le règlement est avec un avoir, il y aura peut être un reste si l'avoir est supérieur au montant à régler
      else if (pReglement.isAvecAvoir()) {
        // Un message d'avertissement est affiché pour indiquer que le montant du règlement est supérieur au total à régler
        resteActuelARegler = montantTotalARegler.subtract(montantDejaRegle).subtract(montantReglement);
        montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
      // Avec les autres modes de règlement le montant réglé doit être identique au montant à régler
      else {
        // Un message d'avertissement est affiché pour indiquer que le montant du règlement est supérieur au total à régler
        resteActuelARegler = montantTotalARegler.subtract(montantDejaRegle).subtract(montantReglement);
        if (!isReglementAcompteEnCours() || resteActuelARegler.compareTo(BigDecimal.ZERO) < 0) {
          montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
          throw new MessageErreurException(
              String.format("Vous avez saisi un montant supérieur au montant à payer qui est de %.2f %s.", resteARegler, devise));
        }
      }
    }
    
    // Calcul commun à tous les modes de règlement
    montantDejaRegle = montantDejaRegle.add(montantReglement).subtract(montantARendre);
    resteARegler = resteARegler.subtract(montantReglement).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // Traitement particulier dans le cas d'un acompte
    if (isReglementAcompteEnCours()) {
      // Création du règlement spécifique à l'acompte
      if (pReglement.getReglementAcompte() == null && pReglement.isModifiable()) {
        IdReglementAcompte idReglementAcompte = IdReglementAcompte.getInstanceAvecCreationId(idEtablissement);
        ReglementAcompte reglementAcompte = new ReglementAcompte(idReglementAcompte);
        reglementAcompte.setPris(true);
        pReglement.setReglementAcompte(reglementAcompte);
      }
      if (!isAcompteRegle() && resteARegler.compareTo(BigDecimal.ZERO) <= 0) {
        // Lorsque l'acompte est entièrement régler le montant actuel à régler devient celui du montant total du document
        rendreMonnaieSurAcompte = false;
        montantActuelARegler = montantTotalARegler;
        resteARegler = montantActuelARegler.subtract(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
    }
  }
  
  /**
   * Effectue les calculs pour un règlement de type remboursement.
   */
  private void rembourser(Reglement pReglement) {
    // Contrôle du montant saisi
    BigDecimal montantReglement = pReglement.getMontant();
    if (montantReglement == null || (montantReglement.compareTo(BigDecimal.ZERO) >= 0)) {
      throw new MessageErreurException("Le montant du règlement est invalide.");
    }
    
    // Calcul du reste à régler par rapport au montant total du document
    BigDecimal resteActuelARegler = montantActuelARegler.add(montantDejaRegle).subtract(montantReglement);
    
    // Si le reste à régler est négatif c'est que l'on a trop perçu et en fonction du mode de règlement le traitement est différent
    if (resteActuelARegler.compareTo(BigDecimal.ZERO) > 0) {
      // Un message d'avertissement est affiché pour indiquer que le montant du règlement est supérieur au total à rembourser
      resteActuelARegler = montantTotalARegler.subtract(montantDejaRegle).subtract(montantReglement);
      if (!isReglementAcompteEnCours() || resteActuelARegler.compareTo(BigDecimal.ZERO) < 0) {
        montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
        throw new MessageErreurException(String.format("Vous avez saisi un montant supérieur au montant à rembourser qui est de %.2f %s.",
            resteARegler.abs(), devise));
      }
    }
    
    // Calcul commun à tous les modes de règlement
    montantDejaRegle = montantDejaRegle.add(montantReglement);
    resteARegler = resteARegler.subtract(montantReglement).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    
    // Traitement particulier dans le cas d'un acompte
    if (isReglementAcompteEnCours()) {
      if (!isAcompteRegle() && resteARegler.compareTo(BigDecimal.ZERO) <= 0) {
        // Lorsque l'acompte est entièrement régler le montant actuel à régler devient celui du montant total du document
        rendreMonnaieSurAcompte = false;
        montantActuelARegler = montantTotalARegler;
        resteARegler = montantActuelARegler.add(montantDejaRegle).setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
    }
  }
  
  /**
   * Initialise les variables.
   */
  private void effacerVariables() {
    montantActuelARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    montantDejaRegle = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    montantDejaRegleEnEspeces = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    resteARegler = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    montantARendre = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    rendreMonnaieSurAcompte = false;
    note.setLength(0);
  }
  
  /**
   * Retourne le règlement en espèces s'il y en a déjà eu un.
   */
  private Reglement rechercherReglementEnEspeces() {
    for (Reglement reglement : listeReglement) {
      if (reglement.isEnEspeces() && reglement.isModifiable()) {
        return reglement;
      }
    }
    return null;
  }
  
  /**
   * Met à jour la note en fonction des montants saisie.
   */
  private void genererNote() {
    // S'il n'y a aucun paiement d'effectué, on ne génère pas de note
    if (montantDejaRegle.compareTo(BigDecimal.ZERO) == 0 && !differerAvoir && !differerPaiement) {
      note.setLength(0);
      return;
    }
    
    // Initialisation du libellé du sens du règlement si les montants ont le même sens (tous des paiements ou remboursements)
    String libelleSensReglement = null;
    if (listeReglement.isEmpty()) {
      libelleSensReglement = Reglement.PAIEMENT;
      if (remboursement) {
        libelleSensReglement = Reglement.REMBOURSEMENT;
      }
    }
    else {
      libelleSensReglement = listeReglement.get(0).retournerLibelleSensReglement();
    }
    
    // Détail de la note
    note.setLength(0);
    boolean premierePhrase = true;
    for (Reglement reglement : listeReglement) {
      // Création du libellé
      String libelleReglement = reglement.getTypeReglement().getLibelleReglementEnMinuscules();
      // Formatage de la date du règlement
      String date = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, reglement.getDateCreation());
      if (date.isEmpty()) {
        date = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA);
      }
      libelleReglement += " le " + date;
      if (reglement.getReglementAcompte() != null) {
        ReglementAcompte reglementAcompte = reglement.getReglementAcompte();
        if (reglementAcompte.isPris()) {
          libelleReglement += " (Acompte)";
        }
        else if (reglementAcompte.isConsomme()) {
          libelleReglement += " (Déduit sur acompte)";
        }
      }
      
      // Ecriture de la première phrase de la note
      if (premierePhrase) {
        if (listeReglement.size() == 1) {
          note.append(String.format("%s de %.2f %s %s.", libelleSensReglement, reglement.getMontant().abs(), devise, libelleReglement));
        }
        else {
          note.append(String.format("%s de :\n", libelleSensReglement));
          note.append(String.format("   %11.2f %s %s", reglement.getMontant().abs(), devise, libelleReglement));
        }
        premierePhrase = false;
      }
      // Ecriture des lignes suivantes
      else {
        note.append(String.format(" + %11.2f %s %s", reglement.getMontant().abs(), devise, libelleReglement));
      }
      
      note.append("\n");
    }
    
    // Pied de la note
    
    // Si c'est un remboursement, il n'y a rien à écrire
    if (remboursement) {
      if (differerAvoir) {
        note.append(String.format("L'avoir d'un montant de %.2f %s a été différé.", resteARegler.abs(), devise));
      }
      return;
    }
    
    // Pour le règlement d'un acompte uniquement si le pourcentage est inférieur à 100% car dans le cas contraire on considère que la
    // commande est entierement payée
    if (isReglementAcompteEnCours() && isAcompteRegle() && !Acompte.isTotal(acompte)
        && montantDejaRegle.compareTo(montantTotalARegler) < 0) {
      
      // Indiquer le montant à rendre
      if (montantARendre.compareTo(BigDecimal.ZERO) > 0) {
        note.append(String.format("%s : %.2f %s\n", "Montant à rendre", montantARendre, devise));
      }
      
      // Indiquer si l'acompte est entièrement réglé
      if (typeDocumentVente != null) {
        // Si un acompte a été réglé mais que le pourcentage vaut 0 c'est
        if (acompte.getPourcentage().intValue() == 0) {
          note.append("\nUn acompte a été réglé pour cette ").append(typeDocumentVente.getLibelleEnMinuscules()).append(".");
        }
        else {
          note.append(String.format("\nL'acompte d'un montant de %.2f %s est entièrement réglé.", acompte.getMontant(), devise));
        }
      }
      else {
        note.append("\nL'acompte est entièrement réglé.");
      }
    }
    // Pour un règlement normal
    else {
      if (montantARendre.compareTo(BigDecimal.ZERO) > 0) {
        note.append(String.format("%s : %.2f %s\n", "Montant à rendre", montantARendre, devise));
      }
      if (isEntierementRegle()) {
        String finDePhrase = " entièrement réglée.";
        // Dans le cas d'un bon le texte de fin de phrase est modifié à cause de l'accord (car l'orthographe c'est important)
        if (typeDocumentVente.equals(EnumTypeDocumentVente.BON)) {
          finDePhrase = " entièrement réglé.";
        }
        note.append('\n').append(typeDocumentVente.getLibelle()).append(finDePhrase);
      }
      else if (differerPaiement) {
        note.append(String.format("Le paiement d'un montant de %.2f %s a été différé.", resteARegler.abs(), devise));
      }
      else {
        note.append(String.format("\n%s %.2f %s.", "Il reste à régler", resteARegler, devise));
      }
    }
  }
  
  // -- Accesseurs
  
  public ListeReglement getListeReglement() {
    return listeReglement;
  }
  
  /**
   * Retourner la note.
   */
  public String getNote() {
    return note.toString();
  }
  
  /**
   * Retourner le montant total à régler.
   */
  public BigDecimal getMontantTotal() {
    return montantTotalARegler;
  }
  
  /**
   * Retourne le montant déjà réglé.
   */
  public BigDecimal getMontantRegle() {
    return montantDejaRegle;
  }
  
  /**
   * Retourne le montant à régler s'il y a un acompte ou non.
   */
  public BigDecimal getMontantARegler() {
    if (!remboursement) {
      return resteARegler;
    }
    else {
      return resteARegler.abs();
    }
  }
  
  /**
   * Reste à régler.
   * Le reste à régler est le montant restant à payer pour parvenir à l'acompte s'il y a un acompte ou pour parvenir au total s'il n'y a
   * pas d'acompte.
   */
  public BigDecimal getResteARegler() {
    if (!remboursement && acompte != null && acompte.getMontant().compareTo(BigDecimal.ZERO) > 0) {
      BigDecimal reste = acompte.getMontant().subtract(montantDejaRegle);
      if (reste.compareTo(BigDecimal.ZERO) <= 0) {
        return BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
      else {
        return reste;
      }
    }
    else {
      return getResteAReglerSurTotal();
    }
  }
  
  /**
   * Reste à régler par rapport au montant total.
   * Le reste à régler est le montant restant à payer pour parvenir au réglement du total.
   */
  public BigDecimal getResteAReglerSurTotal() {
    BigDecimal reste = montantTotalARegler.subtract(montantDejaRegle);
    if (!remboursement && reste.compareTo(BigDecimal.ZERO) < 0) {
      // Si le dernier règlement est un avoir alors le reste n'est pas remit à zéro
      if (listeReglement.isDernierReglementEffectueEstAvoir()) {
        return reste;
      }
      else {
        reste = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
      }
    }
    return reste;
  }
  
  /**
   * Retourne si le document est entièrement réglé.
   * Attention une commande non modifiable sera consideré comme réglé mais si elle est modifiable elle sera considérée comme entièrement
   * réglée uniquement si le total est réglé.
   */
  public boolean isEntierementRegle() {
    return listeReglement != null && listeReglement.isRegle();
  }
  
  public BigDecimal getMontantARendre() {
    return montantARendre;
  }
  
  public BigDecimal getPourcentageAcompte() {
    if (acompte != null) {
      return acompte.getPourcentage();
    }
    return null;
  }
  
  public Acompte getAcompte() {
    return acompte;
  }
  
  /**
   * Retourne s'il s'agit d'un remboursement ou d'un paiement.
   */
  public boolean isRemboursement() {
    return remboursement;
  }
  
  public void setDiffererAvoir(boolean differerAvoir) {
    this.differerAvoir = differerAvoir;
    genererNote();
  }
  
  public boolean isDiffererAvoir() {
    return differerAvoir;
  }
  
  public boolean isDiffererPaiement() {
    return differerPaiement;
  }
  
  public void setDiffererPaiement(boolean differerPaiement) {
    this.differerPaiement = differerPaiement;
  }
  
  public boolean isReglementDiffere() {
    if (differerPaiement || differerAvoir) {
      return true;
    }
    return false;
  }
  
}
