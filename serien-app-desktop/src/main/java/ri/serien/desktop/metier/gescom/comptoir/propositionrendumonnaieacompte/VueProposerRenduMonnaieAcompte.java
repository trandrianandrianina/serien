/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.propositionrendumonnaieacompte;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueProposerRenduMonnaieAcompte extends AbstractVueDialogue<ModeleProposerRenduMonnaieAcompte> {
  /**
   * Constructeur.
   */
  public VueProposerRenduMonnaieAcompte(ModeleProposerRenduMonnaieAcompte pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Les raccourcis claviers
    rbPayerPlus.setMnemonic(KeyEvent.VK_E);
    rbRendreMonnaie.setMnemonic(KeyEvent.VK_R);
    rbPayerPlus.setSelected(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirTexte();
    rafraichirPayerPlus();
    rafraichirRendreMonnaie();
  }
  
  // -- Méthodes privées
  
  private void rafraichirTexte() {
    if (getModele().isAcompteObligatoire()) {
      lbMessageAcompteDepasse.setText("Vous avez enregistré un paiement en espèces supérieur à l'acompte exigé :");
    }
    else {
      lbMessageAcompteDepasse.setText("Vous avez enregistré un paiement en espèces supérieur à l'acompte proposé :");
    }
  }
  
  private void rafraichirPayerPlus() {
    if (getModele().isAcompteObligatoire()) {
      rbPayerPlus.setText("<html><u>E</u>nregistrer ce règlement et payer plus que l'acompte obligatoire.</html>");
    }
    else {
      rbPayerPlus.setText("<html><u>E</u>nregistrer ce règlement et payer plus que l'acompte proposé.</html>");
    }
    rbPayerPlus.setSelected(!getModele().isRendreMonnaie());
  }
  
  private void rafraichirRendreMonnaie() {
    if (getModele().isAcompteObligatoire()) {
      rbRendreMonnaie.setText("<html><u>R</u>endre la monnaie pour payer seulement le montant de l'acompte obligatoire.</html>");
    }
    else {
      rbRendreMonnaie.setText("<html><u>R</u>endre la monnaie pour payer seulement le montant de l'acompte proposé.</html>");
    }
    rbRendreMonnaie.setSelected(getModele().isRendreMonnaie());
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbPayerPlusActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierRendreMonnaie(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRendreMonnaieActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierRendreMonnaie(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlCommentaire = new JPanel();
    lbMessageAcompteDepasse = new JLabel();
    rbPayerPlus = new JRadioButton();
    rbRendreMonnaie = new JRadioButton();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("R\u00e8glement sup\u00e9rieur \u00e0 l'acompte obligatoire");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(600, 300));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCommentaire ========
      {
        pnlCommentaire.setOpaque(false);
        pnlCommentaire.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlCommentaire.setName("pnlCommentaire");
        pnlCommentaire.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMessageAcompteDepasse ----
        lbMessageAcompteDepasse.setText("Vous avez enregistr\u00e9 un paiement en esp\u00e8ces sup\u00e9rieur \u00e0 l'acompte XXX :");
        lbMessageAcompteDepasse.setHorizontalAlignment(SwingConstants.CENTER);
        lbMessageAcompteDepasse.setFont(new Font("sansserif", Font.BOLD, 14));
        lbMessageAcompteDepasse.setName("lbMessageAcompteDepasse");
        pnlCommentaire.add(lbMessageAcompteDepasse, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.5, GridBagConstraints.SOUTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbPayerPlus ----
        rbPayerPlus.setText("<html><u>E</u>nregistrer ce r\u00e8glement et payer plus que l'acompte XXX.</html>");
        rbPayerPlus.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbPayerPlus.setName("rbPayerPlus");
        rbPayerPlus.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            rbPayerPlusActionPerformed(e);
          }
        });
        pnlCommentaire.add(rbPayerPlus,
            new GridBagConstraints(0, 1, 1, 1, 1.0, 0.2, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbRendreMonnaie ----
        rbRendreMonnaie.setText("<html><u>R</u>endre la monnaie pour payer seulement le montant de l'acompte XXX.</html>");
        rbRendreMonnaie.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbRendreMonnaie.setName("rbRendreMonnaie");
        rbRendreMonnaie.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            rbRendreMonnaieActionPerformed(e);
          }
        });
        pnlCommentaire.add(rbRendreMonnaie, new GridBagConstraints(0, 2, 1, 1, 1.0, 0.5, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlCommentaire, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlCommentaire;
  private JLabel lbMessageAcompteDepasse;
  private JRadioButton rbPayerPlus;
  private JRadioButton rbRendreMonnaie;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
