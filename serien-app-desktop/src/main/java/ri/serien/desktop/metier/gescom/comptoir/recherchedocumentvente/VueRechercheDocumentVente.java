/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.recherchedocumentvente;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumEtatExtractionDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.ModeleChantier;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Vue de la boîte de dialogue de recherche des documents de ventes.
 * 
 * Cette boîte de dialogue est accessible à partir du comptoir, à partir de l'icône loupe de l'onglet "Client". Elle permet d'effectuer
 * une recherche parmis les documents de ventes du client.
 */
public class VueRechercheDocumentVente extends AbstractVueDialogue<ModeleRechercheDocumentVente> {
  // Constantes
  private static final String[] TITRE_LISTE = new String[] { "Num\u00e9ro", "Date", "Type", "Montant HT", "Montant TTC", "Magasin",
      "R\u00e9f\u00e9rence", "D\u00e9livrance", "Direct usine", "Extraction" };
  // Boutons
  private static final String BOUTON_DUPLIQUER_DEVIS = "Dupliquer devis";
  private static final String BOUTON_DUPLIQUER_COMMANDE = "Dupliquer commande";
  private static final String BOUTON_DUPLIQUER_BON = "Dupliquer bon";
  private static final String BOUTON_DUPLIQUER_FACTURE = "Dupliquer facture";
  private static final String BOUTON_EXTRAIRE_COMMANDE = "Extraire en commande";
  private static final String BOUTON_EXTRAIRE_BON = "Extraire en bon";
  private static final String BOUTON_EXTRAIRE_FACTURE = "Extraire en facture";
  private static final String BOUTON_CREER_AVOIR = "Créer avoir";
  
  /**
   * Constructeur.
   */
  public VueRechercheDocumentVente(ModeleRechercheDocumentVente pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1235, 560);
    setResizable(false);
    
    // Initialiser la saisie du numéro de document
    tfNumeroDocument.init(IdDocumentVente.LONGUEUR_FACTURE, SaisieDefinition.NOMBRE_ENTIER);
    
    // Initialiser chantier
    if (getModele().getClient() != null) {
      snChantier.setSession(getModele().getSession());
      snChantier.setIdEtablissement(getModele().getClient().getId().getIdEtablissement());
      snChantier.setIdClient(getModele().getClient().getId());
      snChantier.setModeComposant(ModeleChantier.MODE_FILTRE_RECHERCHE);
      snChantier.charger(false);
    }
    
    // Initialiser la saisie de l'article
    tfArticle.init(IdArticle.LONGUEUR_CODE_ARTICLE, true, true);
    
    // Initialiser le type de document de ventes
    cbTypeDocument.addItem("Tous");
    cbTypeDocument.addItem(EnumTypeDocumentVente.DEVIS);
    cbTypeDocument.addItem(EnumTypeDocumentVente.COMMANDE);
    cbTypeDocument.addItem(EnumTypeDocumentVente.BON);
    cbTypeDocument.addItem(EnumTypeDocumentVente.FACTURE);
    
    // Initialiser le tableau de résultats
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    tblListeDocuments.personnaliserAspect(TITRE_LISTE, new int[] { 80, 80, 80, 80, 80, 120, 220, 90, 150, 80 },
        new int[] { NRiTable.CENTRE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE,
            NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.GAUCHE },
        13);
    
    // Redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        tblListeDocumentEnterKey(ae);
      }
    };
    tblListeDocuments.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    
    // Ajouter un listener si la zone d'affichage du tableau est modifiée
    scpListeDocuments.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent e) {
        scpListeDocumentAdjustementChanged(e);
      }
    });
    
    // Mettre le fond du tableau en blanc
    scpListeDocuments.getViewport().setBackground(Color.WHITE);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListeDocuments.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeDocumentSelectionChanged(e);
      }
    });
    
    // Configurer la barre de boutons
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_DEVIS, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_COMMANDE, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_BON, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLIQUER_FACTURE, 'd', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_COMMANDE, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_BON, 'b', false);
    snBarreBouton.ajouterBouton(BOUTON_EXTRAIRE_FACTURE, 'f', false);
    snBarreBouton.ajouterBouton(BOUTON_CREER_AVOIR, 'a', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir les critères de recherche
    rafraichirNumeroDocument();
    rafraichirChantier();
    rafraichirArticle();
    rafraichirPlageDateCreation();
    rafraichirAvecDocumentsHistorises();
    rafraichirChoixTypeDocument();
    
    // Rafraîchir la liste
    rafraichirTitre();
    rafraichirListe();
    
    // Rafraîchir les boutons
    rafraichirBoutonDupliquerDevis();
    rafraichirBoutonDupliquerCommande();
    rafraichirBoutonDupliquerBon();
    rafraichirBoutonDupliquerFacture();
    rafraichirBoutonExtraireCommande();
    rafraichirBoutonExtraireBon();
    rafraichirBoutonExtraireFacture();
    rafraichirBoutonCreerAvoir();
    rafraichirBoutonValider();
    
    if ((getModele().getListeDocumentVenteBase() != null) && (getModele().getListeDocumentVenteBase().size() > 0)) {
      tblListeDocuments.requestFocusInWindow();
    }
  }
  
  /**
   * Rafraîchir le titre.
   */
  private void rafraichirTitre() {
    Message message = getModele().getMessage();
    if (message != null) {
      lbTitre.setMessage(message);
    }
    else {
      lbTitre.setText("");
    }
  }
  
  /**
   * Rafraîchir le numéro de docuement.
   */
  private void rafraichirNumeroDocument() {
    CritereDocumentVente critere = getModele().getCritereDocumentVente();
    if (critere != null && critere.getNumeroDocument() > 0) {
      tfNumeroDocument.setText(Constantes.convertirIntegerEnTexte(critere.getNumeroDocument(), 0));
    }
    else {
      tfNumeroDocument.setText("");
    }
  }
  
  /**
   * Rafraîchir le chantier.
   */
  private void rafraichirChantier() {
    CritereDocumentVente critere = getModele().getCritereDocumentVente();
    if (critere != null) {
      snChantier.setSelection(critere.getChantier());
    }
    else {
      snChantier.setSelection(null);
    }
  }
  
  /**
   * Rafraîchir l'article.
   */
  private void rafraichirArticle() {
    CritereDocumentVente critere = getModele().getCritereDocumentVente();
    if (critere != null && critere.getCodeArticle() != null) {
      tfArticle.setText(critere.getCodeArticle());
    }
    else {
      tfArticle.setText("");
    }
  }
  
  /**
   * Rafraîchir la plage de date de création.
   */
  private void rafraichirPlageDateCreation() {
    CritereDocumentVente critere = getModele().getCritereDocumentVente();
    if (critere != null && critere.getDateCreationDebut() != null) {
      snPlageDateCreation.setDateDebut(critere.getDateCreationDebut());
    }
    else {
      snPlageDateCreation.setDateDebut(null);
    }
    if (critere != null && critere.getDateCreationFin() != null) {
      snPlageDateCreation.setDateFin(critere.getDateCreationFin());
    }
    else {
      snPlageDateCreation.setDateFin(null);
    }
  }
  
  /**
   * Rafraîchir le choix de type de document.
   */
  private void rafraichirChoixTypeDocument() {
    if (getModele().getClient().isClientProspect()) {
      lbTypeDocument.setVisible(false);
      cbTypeDocument.setVisible(false);
    }
    else {
      lbTypeDocument.setVisible(true);
      cbTypeDocument.setVisible(true);
      CritereDocumentVente critere = getModele().getCritereDocumentVente();
      if (critere != null && critere.getTypeDocumentVente() != null
          && !critere.getTypeDocumentVente().equals(EnumTypeDocumentVente.NON_DEFINI)) {
        cbTypeDocument.setSelectedItem(critere.getTypeDocumentVente());
      }
      else {
        cbTypeDocument.setSelectedIndex(0);
      }
    }
  }
  
  /**
   * Rafraîchir la sléection des documents historisés.
   */
  private void rafraichirAvecDocumentsHistorises() {
    CritereDocumentVente critere = getModele().getCritereDocumentVente();
    if (critere != null) {
      ckDocumentsHistorises.setSelected(!critere.isSansHistorique());
      ckDocumentsHistorises.setEnabled(true);
    }
    else {
      ckDocumentsHistorises.setSelected(false);
      ckDocumentsHistorises.setEnabled(false);
    }
  }
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListe() {
    String[][] donnees = null;
    
    // Construire les données
    ListeDocumentVenteBase listeDocumentVenteBase = getModele().getListeDocumentVenteBase();
    if (listeDocumentVenteBase != null && !listeDocumentVenteBase.isEmpty()) {
      donnees = new String[listeDocumentVenteBase.size()][TITRE_LISTE.length];
      
      for (int ligne = 0; ligne < listeDocumentVenteBase.size(); ligne++) {
        DocumentVenteBase documentVenteBase = listeDocumentVenteBase.get(ligne);
        
        // Identifiant
        donnees[ligne][0] = documentVenteBase.getId().toString();
        
        // Date de création
        if (documentVenteBase.isVerrouille()) {
          donnees[ligne][1] = "Verrouillé";
        }
        else if (documentVenteBase.getDateCreation() != null) {
          donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
        }
        
        // Type du document
        if (documentVenteBase.getTypeDocumentVente() != null) {
          donnees[ligne][2] = documentVenteBase.getTypeDocumentVente().getLibelle();
        }
        
        // Total HT
        if (documentVenteBase.getTotalHT() != null) {
          donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        }
        
        // Total TTC
        if (documentVenteBase.getTotalTTC() != null) {
          donnees[ligne][4] = Constantes.formater(documentVenteBase.getTotalTTC(), true);
        }
        
        // Magasin
        if (getModele().getListeMagasin() != null && documentVenteBase.getIdMagasin() != null) {
          Magasin magasin = getModele().getListeMagasin().getMagasinParId(documentVenteBase.getIdMagasin());
          if (magasin != null) {
            donnees[ligne][5] = magasin.getNom();
          }
        }
        
        // Référence longue
        if (documentVenteBase.getReferenceLongue() != null) {
          donnees[ligne][6] = documentVenteBase.getReferenceLongue();
        }
        
        // Mode d'expédition
        if (getModele().getListeModeExpedition() != null && documentVenteBase.geIdModeExpedition() != null) {
          ModeExpedition modeExpedition =
              getModele().getListeModeExpedition().getModeExpeditionParId(documentVenteBase.geIdModeExpedition());
          if (modeExpedition != null) {
            donnees[ligne][7] = modeExpedition.getLibelle();
          }
        }
        
        // Direct usine
        if (documentVenteBase.isDirectUsine()) {
          donnees[ligne][8] = "Direct usine";
          if (documentVenteBase.isCommandeAchatGeneree()) {
            donnees[ligne][8] += " achat généré";
          }
        }
        
        // Etat de l'extraction
        if (documentVenteBase.getEtatExtraction() != null) {
          donnees[ligne][9] = documentVenteBase.getEtatExtraction().getLibelle();
        }
      }
    }
    else {
      // Se positionner en haut du tableau si la liste est vide
      scpListeDocuments.getVerticalScrollBar().setValue(0);
    }
    
    // Mettre à jour le tableau
    tblListeDocuments.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne
    int index = getModele().getLigneSelectionnee();
    if (index > -1 && index < tblListeDocuments.getRowCount()) {
      tblListeDocuments.getSelectionModel().setSelectionInterval(index, index);
    }
    else {
      tblListeDocuments.getSelectionModel().clearSelection();
    }
  }
  
  /**
   * Rafraîchir le bouton valider.
   */
  private void rafraichirBoutonValider() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Rafraîchir le bouton dupliquer devis.
   */
  private void rafraichirBoutonDupliquerDevis() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactivé si le document n'est pas un devis
    else if (!documentVenteBase.isDevis()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_DEVIS, actif);
  }
  
  /**
   * Rafraîchir le bouton dupliquer commande.
   */
  private void rafraichirBoutonDupliquerCommande() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactivé si le document n'est pas une commande
    else if (!documentVenteBase.isCommande()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_COMMANDE, actif);
  }
  
  /**
   * Rafraîchir le bouton dupliquer bon.
   */
  private void rafraichirBoutonDupliquerBon() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactivé si le document n'est pas un bon
    else if (!documentVenteBase.isBon()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_BON, actif);
  }
  
  /**
   * Rafraîchir le bouton dupliquer facture.
   */
  private void rafraichirBoutonDupliquerFacture() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactivé si le document n'est pas une facture
    else if (!documentVenteBase.isFacture()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_DUPLIQUER_FACTURE, actif);
  }
  
  /**
   * Rafraîchir le bouton extraire commande.
   */
  private void rafraichirBoutonExtraireCommande() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis n'est sélectionné
    else if (!documentVenteBase.isDevis()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (getModele().getClient().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si le document est entièrement extrait
    else if (documentVenteBase.getEtatExtraction().equals(EnumEtatExtractionDocumentVente.COMPLETE)) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_COMMANDE, actif);
  }
  
  /**
   * Rafraîchir le bouton extraire bon.
   */
  private void rafraichirBoutonExtraireBon() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis et aucune commande n'est sélectionnée
    else if (!documentVenteBase.isDevis() && !documentVenteBase.isCommande()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (getModele().getClient().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si le client est comptant
    else if (getModele().getClient().isClientComptant()) {
      actif = false;
    }
    // Désactiver le bouton si le document est entièrement extrait
    else if (documentVenteBase.getEtatExtraction().equals(EnumEtatExtractionDocumentVente.COMPLETE)) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_BON, actif);
  }
  
  /**
   * Rafraîchir le bouton extraire facture.
   */
  private void rafraichirBoutonExtraireFacture() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactiver le bouton si aucun devis et aucune commande n'est sélectionnée
    else if (!documentVenteBase.isDevis() && !documentVenteBase.isCommande()) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    // Désactiver le bouton si le client est interdit
    else if (getModele().getClient().isClientInterdit()) {
      actif = false;
    }
    // Désactiver le bouton si le client est en compte
    else if (getModele().getClient().isClientEnCompte()) {
      actif = false;
    }
    // Désactiver le bouton si le document est entièrement extrait
    else if (documentVenteBase.getEtatExtraction().equals(EnumEtatExtractionDocumentVente.COMPLETE)) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_EXTRAIRE_FACTURE, actif);
  }
  
  /**
   * Rafraîchir le bouton créer avoir.
   */
  private void rafraichirBoutonCreerAvoir() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver si aucun document de sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    // Désactivé si le document n'est pas un bon ou une facture
    else if (!documentVenteBase.isBon() && !documentVenteBase.isFacture()) {
      actif = false;
    }
    // Désactivé si le document est déjà un avoir
    else if (documentVenteBase.getTotalHT().compareTo(BigDecimal.ZERO) < 1) {
      actif = false;
    }
    // Désactiver le bouton si un document est en cours de saisie
    else if (getModele().isDocumentEnCoursDeSaisie()) {
      actif = false;
    }
    // Désactiver le bouton si aucun client n'est sélectionné
    else if (getModele().getClient() == null) {
      actif = false;
    }
    // Désactiver le bouton si le client est en cours de création
    else if (!getModele().getClient().isExistant()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CREER_AVOIR, actif);
  }
  
  // -- Méthodes événementielles
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().valider();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_DEVIS)) {
        getModele().dupliquerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_COMMANDE)) {
        getModele().dupliquerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_BON)) {
        getModele().dupliquerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLIQUER_FACTURE)) {
        getModele().dupliquerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_COMMANDE)) {
        getModele().extraireEnCommande();
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_BON)) {
        getModele().extraireEnBon();
      }
      else if (pSNBouton.isBouton(BOUTON_EXTRAIRE_FACTURE)) {
        getModele().extraireEnFacture();
      }
      else if (pSNBouton.isBouton(BOUTON_CREER_AVOIR)) {
        getModele().creerAvoir();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserCritere(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action sur le scrollpane de la liste des documents.
   * 
   * @param e
   */
  private void scpListeDocumentAdjustementChanged(AdjustmentEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDocuments == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDocuments.getViewport().getViewRect();
      int premiereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDocuments.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDocuments.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDocumentsAffiches(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblListeDocumentSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      // Informer le modèle
      getModele().modifierDocumentSelectionne(tblListeDocuments.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter le clic sur une ligne dans la liste.
   * 
   * @param e
   */
  private void tblListeDocumentsMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        getModele().valider();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'appui sur la touche entrée.
   * 
   * @param e
   */
  private void tblListeDocumentEnterKey(ActionEvent e) {
    try {
      if (getModele().getDocumentVenteBaseSelectionne() != null) {
        getModele().valider();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection de la checkbox de l'option "Afficher les document historisés".
   * 
   * @param e
   */
  private void ckDocumentsHistorisesActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierAffichageHistorique(ckDocumentsHistorises.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du type de document sélectionné.
   * 
   * @param e
   */
  private void cbTypeDocumentItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || e.getStateChange() != ItemEvent.SELECTED) {
        return;
      }
      if (cbTypeDocument.getSelectedItem() instanceof EnumTypeDocumentVente) {
        getModele().modifierTypeDocumentVente((EnumTypeDocumentVente) cbTypeDocument.getSelectedItem());
      }
      else {
        getModele().modifierTypeDocumentVente(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du numéro de document.
   * 
   * @param e
   */
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action sur le champ de saisie du numéro de document.
   * 
   * @param e
   */
  private void tfNumeroDocumentActionPerformed(ActionEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du code article.
   * 
   * @param e
   */
  private void tfArticleFocusLost(FocusEvent e) {
    try {
      getModele().modifierArticle(tfArticle.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'action sur le champ de saisie du code article.
   * 
   * @param e
   */
  private void tfArticleActionPerformed(ActionEvent e) {
    try {
      getModele().modifierArticle(tfArticle.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la plage de date sélectionnée.
   * 
   * @param e
   */
  private void snPlageDateCreationValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPlageDate(snPlageDateCreation.getDateDebut(), snPlageDateCreation.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du chantier sélectionné.
   * 
   * @param e
   */
  private void snChantierValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierChantier(snChantier.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new JPanel();
    pnlRecherche = new JPanel();
    lbNumeroDocument = new JLabel();
    tfNumeroDocument = new XRiTextField();
    sNLabelChamp1 = new SNLabelChamp();
    snPlageDateCreation = new SNPlageDate();
    lbArticle = new JLabel();
    tfArticle = new XRiTextField();
    lbTypeDocument = new JLabel();
    cbTypeDocument = new XRiComboBox();
    lbChantier = new SNLabelChamp();
    snChantier = new SNChantier();
    snBarreRecherche = new SNBarreRecherche();
    ckDocumentsHistorises = new JCheckBox();
    scpListeDocuments = new JScrollPane();
    tblListeDocuments = new NRiTable();
    lbTitre = new SNLabelTitre();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1000, 600));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("Recherche de documents");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 10, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 15, 0, 30, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRecherche ========
        {
          pnlRecherche.setBorder(new TitledBorder(""));
          pnlRecherche.setOpaque(false);
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlRecherche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlRecherche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNumeroDocument ----
          lbNumeroDocument.setText("Num\u00e9ro de document");
          lbNumeroDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbNumeroDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNumeroDocument.setPreferredSize(new Dimension(150, 30));
          lbNumeroDocument.setName("lbNumeroDocument");
          pnlRecherche.add(lbNumeroDocument, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.NONE, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroDocument ----
          tfNumeroDocument.setFont(tfNumeroDocument.getFont().deriveFont(tfNumeroDocument.getFont().getSize() + 2f));
          tfNumeroDocument.setHorizontalAlignment(SwingConstants.LEFT);
          tfNumeroDocument.setPreferredSize(new Dimension(150, 30));
          tfNumeroDocument.setMinimumSize(new Dimension(150, 30));
          tfNumeroDocument.setName("tfNumeroDocument");
          tfNumeroDocument.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroDocumentFocusLost(e);
            }
          });
          tfNumeroDocument.addActionListener(e -> tfNumeroDocumentActionPerformed(e));
          pnlRecherche.add(tfNumeroDocument, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- sNLabelChamp1 ----
          sNLabelChamp1.setText("Date de cr\u00e9ation");
          sNLabelChamp1.setName("sNLabelChamp1");
          pnlRecherche.add(sNLabelChamp1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snPlageDateCreation ----
          snPlageDateCreation.setName("snPlageDateCreation");
          snPlageDateCreation.addSNComposantListener(e -> snPlageDateCreationValueChanged(e));
          pnlRecherche.add(snPlageDateCreation, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbArticle.setPreferredSize(new Dimension(150, 30));
          lbArticle.setName("lbArticle");
          pnlRecherche.add(lbArticle, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfArticle ----
          tfArticle.setFont(tfArticle.getFont().deriveFont(tfArticle.getFont().getSize() + 2f));
          tfArticle.setPreferredSize(new Dimension(400, 30));
          tfArticle.setName("tfArticle");
          tfArticle.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfArticleFocusLost(e);
            }
          });
          tfArticle.addActionListener(e -> tfArticleActionPerformed(e));
          pnlRecherche.add(tfArticle, new GridBagConstraints(1, 3, 1, 1, 0.5, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbTypeDocument ----
          lbTypeDocument.setText("Type de document");
          lbTypeDocument.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTypeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTypeDocument.setPreferredSize(new Dimension(150, 30));
          lbTypeDocument.setName("lbTypeDocument");
          pnlRecherche.add(lbTypeDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbTypeDocument ----
          cbTypeDocument.setBackground(new Color(171, 148, 79));
          cbTypeDocument.setFont(cbTypeDocument.getFont().deriveFont(cbTypeDocument.getFont().getSize() + 2f));
          cbTypeDocument.setPreferredSize(new Dimension(150, 30));
          cbTypeDocument.setMinimumSize(new Dimension(150, 30));
          cbTypeDocument.setName("cbTypeDocument");
          cbTypeDocument.addItemListener(e -> cbTypeDocumentItemStateChanged(e));
          pnlRecherche.add(cbTypeDocument, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbChantier ----
          lbChantier.setText("Chantier");
          lbChantier.setName("lbChantier");
          pnlRecherche.add(lbChantier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snChantier ----
          snChantier.setPreferredSize(new Dimension(500, 30));
          snChantier.setName("snChantier");
          snChantier.addSNComposantListener(e -> snChantierValueChanged(e));
          pnlRecherche.add(snChantier, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          pnlRecherche.add(snBarreRecherche, new GridBagConstraints(2, 3, 2, 2, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- ckDocumentsHistorises ----
          ckDocumentsHistorises.setText("Afficher les documents historis\u00e9s");
          ckDocumentsHistorises.setHorizontalTextPosition(SwingConstants.RIGHT);
          ckDocumentsHistorises.setFont(new Font("sansserif", Font.PLAIN, 14));
          ckDocumentsHistorises.setHorizontalAlignment(SwingConstants.LEFT);
          ckDocumentsHistorises.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ckDocumentsHistorises.setPreferredSize(new Dimension(200, 30));
          ckDocumentsHistorises.setName("ckDocumentsHistorises");
          ckDocumentsHistorises.addActionListener(e -> ckDocumentsHistorisesActionPerformed(e));
          pnlRecherche.add(ckDocumentsHistorises, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeDocuments ========
        {
          scpListeDocuments.setPreferredSize(new Dimension(1050, 424));
          scpListeDocuments.setName("scpListeDocuments");
          
          // ---- tblListeDocuments ----
          tblListeDocuments.setShowVerticalLines(true);
          tblListeDocuments.setShowHorizontalLines(true);
          tblListeDocuments.setBackground(Color.white);
          tblListeDocuments.setRowHeight(20);
          tblListeDocuments.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeDocuments.setSelectionBackground(new Color(57, 105, 138));
          tblListeDocuments.setGridColor(new Color(204, 204, 204));
          tblListeDocuments.setName("tblListeDocuments");
          tblListeDocuments.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeDocumentsMouseClicked(e);
            }
          });
          scpListeDocuments.setViewportView(tblListeDocuments);
        }
        pnlContenu.add(scpListeDocuments, new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbTitre ----
        lbTitre.setText("Documents correspondant \u00e0 votre recherche");
        lbTitre.setFont(new Font("sansserif", Font.BOLD, 14));
        lbTitre.setName("lbTitre");
        pnlContenu.add(lbTitre, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlContenu;
  private JPanel pnlRecherche;
  private JLabel lbNumeroDocument;
  private XRiTextField tfNumeroDocument;
  private SNLabelChamp sNLabelChamp1;
  private SNPlageDate snPlageDateCreation;
  private JLabel lbArticle;
  private XRiTextField tfArticle;
  private JLabel lbTypeDocument;
  private XRiComboBox cbTypeDocument;
  private SNLabelChamp lbChantier;
  private SNChantier snChantier;
  private SNBarreRecherche snBarreRecherche;
  private JCheckBox ckDocumentsHistorises;
  private JScrollPane scpListeDocuments;
  private NRiTable tblListeDocuments;
  private SNLabelTitre lbTitre;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
