/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.affichagelienligne.SNLienLigne;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet mémo de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletLiens extends SNPanel implements InterfaceVueOnglet {
  // Variables
  private ModeleDetailLigneArticle modele = null;
  
  /**
   * Constructeur.
   */
  public VueOngletLiens(ModeleDetailLigneArticle pModele) {
    modele = pModele;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Affichage obligatoire
    rafraichirLiens();
    
    // Effacer tous les boutons
    snBarreBouton.supprimerTout();
    
    // Ajouter les boutons permanents
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    
  }
  
  private void rafraichirLiens() {
    LienLigne lienOrigine = modele.getLienOrigine();
    if (lienOrigine != null) {
      snLignesLiees.setSession(modele.getSession());
      snLignesLiees.setLienOrigine(lienOrigine);
      snLignesLiees.charger(true);
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    snLignesLiees = new SNLienLigne();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setMinimumSize(new Dimension(970, 420));
      pnlContenu.setPreferredSize(new Dimension(970, 420));
      pnlContenu.setBorder(null);
      pnlContenu.setBackground(new Color(238, 238, 210));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout(5, 5));
      
      // ---- snLignesLiees ----
      snLignesLiees.setBackground(new Color(239, 239, 222));
      snLignesLiees.setName("snLignesLiees");
      pnlContenu.add(snLignesLiees, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private SNLienLigne snLignesLiees;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
