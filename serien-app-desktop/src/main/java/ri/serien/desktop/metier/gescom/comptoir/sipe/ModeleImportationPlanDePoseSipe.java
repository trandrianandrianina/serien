/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.sipe;

import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumStatutSipe;
import ri.serien.libcommun.gescom.vente.sipe.IdPlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Cette boite de dialogue permet de gérer les plans de pose Sipe.
 */
public class ModeleImportationPlanDePoseSipe extends AbstractModeleDialogue {
  // Variables
  private IdDocumentVente idDocumentVente = null;
  private CritereSipe critereSipe = null;
  private ListePlanPoseSipe listePlanPose = null;
  private Message titreResultatRecherche = null;
  private PlanPoseSipe planPoseSelectionne = null;
  private int indexPlanPoseSelectionne = -1;
  private boolean miseAJourDepuisIfs = true;
  
  /**
   * Constructeur.
   */
  public ModeleImportationPlanDePoseSipe(SessionBase pSession, IdDocumentVente pIdDocumentVente) {
    super(pSession);
    idDocumentVente = pIdDocumentVente;
  }
  
  // -- Méthodes standards du modèle
  
  /**
   * Initialisation des données.
   */
  @Override
  public void initialiserDonnees() {
  }
  
  /**
   * Chargement des données.
   */
  @Override
  public void chargerDonnees() {
    initialiserRecherche(false);
    rechercher();
    // Construction du titre associé aux résultats de recherche.
    construireTitreRecherche();
  }
  
  /**
   * Quitte avec validation.
   */
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialiser les filtres de la recherche.
   */
  public void initialiserRecherche(boolean pRafraichir) {
    critereSipe = new CritereSipe();
    critereSipe.ajouterStatut(EnumStatutSipe.ATTENTE);
    critereSipe.ajouterStatut(EnumStatutSipe.IMPORTE);
    
    listePlanPose = null;
    planPoseSelectionne = null;
    
    // Construction du titre associé aux résultats de recherche.
    construireTitreRecherche();
    
    // Rafraîchit la vue.
    if (pRafraichir) {
      rafraichir();
    }
  }
  
  /**
   * Lancer la recherche et rafraîchir la vue.
   */
  public void lancerRecherche() {
    // Lance la recherche
    rechercher();
    // Construction du titre associé aux résultats de recherche.
    construireTitreRecherche();
    
    rafraichir();
  }
  
  /**
   * Modifie le plan de pose sélectionné.
   */
  public void modifierPlanPoseSelectionne(int pIndex) {
    if (indexPlanPoseSelectionne == pIndex) {
      return;
    }
    indexPlanPoseSelectionne = pIndex;
    
    // Déterminer le plan de pose sélectionné
    if (listePlanPose != null && 0 <= indexPlanPoseSelectionne && indexPlanPoseSelectionne < listePlanPose.size()) {
      planPoseSelectionne = listePlanPose.get(indexPlanPoseSelectionne);
    }
    else {
      planPoseSelectionne = null;
    }
    rafraichir();
  }
  
  /**
   * Afficher la plage de plans de pose comprise entre deux lignes.
   * Les informations des plans de pose sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlagePlanPose(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listePlanPose != null) {
      listePlanPose.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie le filtre début plage de date.
   */
  public void modifierDebutPlage(Date pDateDebut) {
    if (pDateDebut == null) {
      return;
    }
    // Contrôle sur les null et sur une valeur saisie identique à la valeur existante.
    if (Constantes.equals(pDateDebut, critereSipe.getDateDebutPlan())) {
      return;
    }
    
    critereSipe.setDateDebutPlan(pDateDebut);
    
    // Rafraîchissement de la vue.
    rafraichir();
  }
  
  /**
   * Modifie le filtre fin plage de date.
   */
  public void modifierFinPlage(Date pDateFin) {
    if (pDateFin == null) {
      return;
    }
    // Contrôle sur les null et sur une valeur saisie identique à la valeur existante.
    if (Constantes.equals(pDateFin, critereSipe.getDateFinPlan())) {
      return;
    }
    
    critereSipe.setDateFinPlan(pDateFin);
    
    // Rafraîchissement de la vue.
    rafraichir();
  }
  
  /**
   * Modifie le filtre sur le statut.
   * 
   * @param pStatut
   */
  public void modifierFiltreStatut(EnumStatutSipe pStatut) {
    // Contrôle sur les null et sur une valeur saisie identique à la valeur existante.
    if (pStatut == null && (critereSipe.getListeStatut() == null || critereSipe.getListeStatut().isEmpty())) {
      return;
    }
    else if (pStatut != null && critereSipe.getListeStatut() != null && critereSipe.getListeStatut().size() == 1
        && critereSipe.getListeStatut().contains(pStatut)) {
      return;
    }
    
    critereSipe.effacerListeStatut();
    // Tous les critères (les supprimés exclus)
    if (pStatut == null) {
      critereSipe.ajouterStatut(EnumStatutSipe.ATTENTE);
      critereSipe.ajouterStatut(EnumStatutSipe.IMPORTE);
    }
    else {
      critereSipe.ajouterStatut(pStatut);
    }
    
    // Rafraîchissement de la vue.
    rafraichir();
  }
  
  /**
   * Importe le plan de pose sélectionné.
   */
  public void importerPlanPose() {
    if (planPoseSelectionne == null) {
      return;
    }
    if (idDocumentVente == null) {
      throw new MessageErreurException("L'identifiant du document de vente dans lequel les articles vont être importés est invalide.");
    }
    // Appel du service RPG pour l'importation
    ManagerServiceDocumentVente.importerPlanPoseSipe(getIdSession(), planPoseSelectionne, idDocumentVente);
    rechercher();
    
    // Affiche un message avertissant du succès de l'importation
    DialogueInformation.afficher("Les données du plan " + planPoseSelectionne.getNumeroPlan() + " ont été importées avec succés.");
    
    // Ferme la boite de dialogue
    quitterAvecValidation();
  }
  
  /**
   * Sauvegarde le plan de pose sélectionné.
   */
  public void sauvegarderPlanPose() {
    if (planPoseSelectionne == null) {
      return;
    }
    
    String nomFichier = Constantes.normerTexte(planPoseSelectionne.getNomFichier());
    if (nomFichier.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier Sipe est invalide.");
    }
    // Récupération du dossier Sipe
    String dossier = ManagerServiceDocumentVente.retournerDossierSipe(getIdSession(), planPoseSelectionne.getTypePlanPoseSipe(), false);
    dossier = Constantes.normerTexte(dossier);
    if (dossier.isEmpty()) {
      throw new MessageErreurException("Le dossier pour les Sipe est invalide.");
    }
    // Récupération du dossier de sauvegarde Sipe
    String dossierSauvegarde =
        ManagerServiceDocumentVente.retournerDossierSipe(getIdSession(), planPoseSelectionne.getTypePlanPoseSipe(), true);
    dossierSauvegarde = Constantes.normerTexte(dossierSauvegarde);
    if (dossierSauvegarde.isEmpty()) {
      throw new MessageErreurException("Le dossier de sauvegarde pour les fichiers Sipe est invalide.");
    }
    
    // Copie du fichier de l'IFS
    String fichierASauvegarder = dossier + nomFichier;
    String fichierDestination = dossierSauvegarde + nomFichier;
    boolean retour = ManagerServiceTechnique.copierFichier(getIdSession(), fichierASauvegarder, fichierDestination, false);
    if (!retour) {
      throw new MessageErreurException(
          "Il y a eu un problème lors de la copie du fichier " + fichierASauvegarder + " vers " + fichierDestination);
    }
    
    // Affiche un message avertissant du succès de l'importation
    DialogueInformation.afficher("Les données du plan " + planPoseSelectionne.getNumeroPlan()
        + " ont été sauvegardées avec succés dans le dossier " + fichierDestination + '.');
    
    rafraichir();
  }
  
  /**
   * Supprime le plan de pose sélectionné.
   */
  public void supprimerPlanPose() {
    if (planPoseSelectionne == null) {
      return;
    }
    if (planPoseSelectionne.getStatut() == EnumStatutSipe.SUPPRIME) {
      return;
    }
    
    ManagerServiceDocumentVente.supprimerFichierSipe(getIdSession(), planPoseSelectionne);
    lancerRecherche();
  }
  
  // -- Méthodes privées
  
  /**
   * Rechercher des plans de pose en fonction des critères de recherche.
   */
  private void rechercher() {
    // Mémorisation de la taille de la page.
    int taillePage = 0;
    if (listePlanPose != null) {
      taillePage = listePlanPose.getTaillePage();
    }
    
    // Initialisation d'une nouvelle liste de plans de pose.
    listePlanPose = new ListePlanPoseSipe();
    List<IdPlanPoseSipe> listeIdPlanPose = null;
    
    // Chargement des plans de pose suivant les informations contenues dans les critères.
    listeIdPlanPose = listePlanPose.chargerListeIdPlanPoseSIpe(getIdSession(), critereSipe, miseAJourDepuisIfs);
    miseAJourDepuisIfs = false;
    
    // Si la liste d'id revient vide, on quitte le traitement.
    if (listeIdPlanPose == null) {
      return;
    }
    
    // Initisation de la liste des plans de pose à partir de la liste d'identifiants.
    listePlanPose = ListePlanPoseSipe.creerListeNonChargee(listeIdPlanPose);
    
    // Chargement de la première page et les informations complémentaires.
    listePlanPose.chargerPremierePage(getIdSession(), taillePage);
    chargerComplement(listePlanPose);
    
    // Contrôle du paramètre avancé sélectionné
    if (planPoseSelectionne == null) {
      return;
    }
    
    // Si le paramètre avancé sélectionné est dans la liste après la recherche.
    if (listePlanPose.contains(planPoseSelectionne.getId())) {
      // On met à jour le paramètre avancé sélectionné dans la liste.
      listePlanPose.selectionner(planPoseSelectionne.getId());
    }
    
    // Sinon déselectionne tout et met à null le paramètre avancé sélectionné.
    else {
      planPoseSelectionne = null;
      listePlanPose.deselectionnerTout();
    }
  }
  
  /**
   * Charger les informations complémentaires d'une liste de plans de pose.
   */
  private void chargerComplement(ListePlanPoseSipe plistePlanPose) {
    if (plistePlanPose == null) {
      return;
    }
  }
  
  /**
   * Affiche une boite de dialogue pour une demande de confirmation.
   */
  private boolean isConfirmerDemande(String pTitre, String pMessage) {
    pTitre = Constantes.normerTexte(pTitre);
    pMessage = Constantes.normerTexte(pMessage);
    ModeleDialogueConfirmation modele = new ModeleDialogueConfirmation(getSession(), pTitre, pMessage);
    DialogueConfirmation vue = new DialogueConfirmation(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }
  
  /**
   * Construction du titre de la liste suivant le résultat de recherche.
   */
  private void construireTitreRecherche() {
    // Si la liste est à null.
    if (listePlanPose == null) {
      titreResultatRecherche = Message.getMessageNormal("Plan de pose correspondant à votre recherche");
    }
    
    // Si un résultat.
    else if (listePlanPose.size() == 1) {
      titreResultatRecherche = Message.getMessageNormal("Plan de pose correspondant à votre recherche (1)");
    }
    
    // Si plus d'un résultat.
    else if (listePlanPose.size() > 1) {
      titreResultatRecherche = Message.getMessageNormal("Plan de pose correspondant à votre recherche (" + listePlanPose.size() + ")");
    }
    
    // Si aucun résultat.
    else {
      titreResultatRecherche = Message.getMessageImportant("Aucun Plan de pose ne correspond à votre recherche");
    }
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des plans de pose.
   * 
   * @return
   */
  public ListePlanPoseSipe getListePlanPose() {
    return listePlanPose;
  }
  
  /**
   * Retourne les critères de recherches des plans de pose Sipe.
   * 
   * @return
   */
  public CritereSipe getCritereSipe() {
    return critereSipe;
  }
  
  /**
   * Retourner le titre variable de la liste des résultats de recherche, en fonction du nombre de résultats à afficher.
   * 
   * @return Titre de la liste des résultats de recherche pour affichage.
   */
  public Message getTitreResultatRecherche() {
    return titreResultatRecherche;
  }
  
  /**
   * Retourne le plan de pose sélectionné.
   * 
   * @return
   */
  public PlanPoseSipe getPlanPoseSelectionne() {
    return planPoseSelectionne;
  }
  
  /**
   * Retourne l'index du plan de pose sélectionné.
   * 
   * @return
   */
  public int getIndexPlanPoseSelectionne() {
    return indexPlanPoseSelectionne;
  }
  
}
