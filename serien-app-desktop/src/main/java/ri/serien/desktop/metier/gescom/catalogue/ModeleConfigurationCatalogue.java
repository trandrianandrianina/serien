/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue;

import ri.serien.desktop.metier.gescom.catalogue.expert.ModeleExpertConfigurationCatalogue;
import ri.serien.desktop.metier.gescom.catalogue.expert.VueExpertConfigurationCatalogue;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.IdConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeFichierCSV;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.FournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modele principal de la gestion des configurations de catalogue.
 */
public class ModeleConfigurationCatalogue extends AbstractModelePanel {
  // Constantes
  private static final String TITRE_LISTE = "Liste des configurations de catalogues";
  private static final String TITRE_DETAIL = "Détail de la configuration de catalogue";
  private static final String TITRE_CREATION = "Création d'une configuration de catalogue";
  
  // Variables
  private UtilisateurGescom utilisateurGescom = null;
  private IdEtablissement idEtablissement = null;
  private FournisseurBase fournisseurBase = null;
  
  private ListeConfigurationCatalogue listeConfigurationCatalogue = null;
  private ConfigurationCatalogue configurationCatalogue = null;
  
  private ConfigurationCatalogue configurationInitiale = null;
  private ListeFichierCSV listeFichierCSV = null;
  private ListeChampERP listeChampERP = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et factories
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur standard.
   * @param pSession Session courante.
   */
  public ModeleConfigurationCatalogue(SessionBase pSession) {
    super(pSession);
    
    // Renseigner le titre de l'onglet
    getSession().getPanel().setName(TITRE_LISTE);
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes abstract à implémenter
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserDonnees() {
    // Définir la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
  }
  
  @Override
  public void chargerDonnees() {
    // Charger l'utilisateur
    if (utilisateurGescom == null) {
      utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
          ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), null);
      ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    }
    
    // Renseigner l'établissement par défaut
    if (idEtablissement == null) {
      idEtablissement = utilisateurGescom.getIdEtablissementPrincipal();
    }
    
    // Charger la liste des configurations catalogues
    chargerListeConfigurationCatalogue();
    
    // Charger la liste des champs du fichier CSV
    if (listeFichierCSV == null) {
      listeFichierCSV = ListeFichierCSV.chargerTout(getIdSession());
    }
    
    // Charger la liste des champs du fichier ERP
    if (listeChampERP == null) {
      listeChampERP = ListeChampERP.chargerTout(getIdSession());
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques - Vue liste
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Remettre les valeurs par défaut des filtres.
   */
  public void initialiserRecherche() {
    // Remettre les valeurs par défaut
    idEtablissement = utilisateurGescom.getIdEtablissementPrincipal();
    fournisseurBase = null;
    
    // Lancer uen recherche (le rafraîchissement est effectué après la recherche)
    rechercher();
  }
  
  /**
   * Charger la liste des configurations de catalogues fournisseurs suivant les critères de filtrage.
   */
  public void rechercher() {
    // Effacer le résultat précédent (en cas d'erreur)
    listeConfigurationCatalogue = null;
    configurationCatalogue = null;
    
    // Charger la liste des configurations
    chargerListeConfigurationCatalogue();
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Consulter le détail de la configuration Sélectionnée.
   */
  public void consulter() {
    // Charger la configuration catalogue sélectionnée
    chargerConfigurationCatalogue();
    
    // Changer la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Créer une nouvelle configuration de catalogue.
   */
  public void creer() {
    // Créer un nouvel identifiant de configuration catalogue
    IdConfigurationCatalogue idConfigurationCatalogue = IdConfigurationCatalogue.getInstanceAvecCreationId(idEtablissement);
    
    // Créer une nouvelle configuration catalogue
    configurationCatalogue = new ConfigurationCatalogue(idConfigurationCatalogue);
    configurationCatalogue.setIdEtablissement(idEtablissement);
    
    // Changer la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  /**
   * Modifier le filtre établissement.
   * @param pIdEtablissement Identifiant du nouvel établissement.
   */
  public void modifierFiltreEtablissement(IdEtablissement pIdEtablissement) {
    // Tester si la valeur a changé
    if (Constantes.equals(idEtablissement, pIdEtablissement)) {
      return;
    }
    
    // Modifier l'établissement
    idEtablissement = pIdEtablissement;
    
    // Effacer les autres filtres
    fournisseurBase = null;
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le filtre fournisseur.
   * @param pFournisseurBase Nouveau filtre fournisseur.
   */
  public void modifierFiltreFournisseur(FournisseurBase pFournisseurBase) {
    // Tester si la valeur a changé
    if (Constantes.equals(fournisseurBase, pFournisseurBase)) {
      return;
    }
    
    // Modifier le fournisseur
    fournisseurBase = pFournisseurBase;
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Sélectionner une configuration catalogue dans la liste.
   * @param pLigneSelectionnee Index de la ligne sélectionnée.
   */
  public void selectionnerConfigurationCatalogue(int pLigneSelectionnee) {
    // Récupérer la configuration catalogue sélectionnée
    ConfigurationCatalogue configurationCatalogueTemp = null;
    if (listeConfigurationCatalogue != null && pLigneSelectionnee != -1 && pLigneSelectionnee < listeConfigurationCatalogue.size()) {
      configurationCatalogueTemp = listeConfigurationCatalogue.get(pLigneSelectionnee);
    }
    
    // Tester si la valeur a changé
    if (Constantes.equals(configurationCatalogue, configurationCatalogueTemp)) {
      return;
    }
    
    // Modifier la configuration catalogue sélectionnée
    configurationCatalogue = configurationCatalogueTemp;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques - Vue détail
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Annuler la modification en cours.
   * Quitte le détail d'une configuration pour revenir à la liste des configurations.
   */
  public void annuler() {
    // Changer la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Valider la création ou la modification d'une configuration de catalogue.
   */
  public void valider() {
    // Contrôler les données de la configuration catalogue
    controlerConfigurationCatalogue();
    
    // Sauver la configuration catalogue
    configurationCatalogue.sauver(getIdSession());
    
    // Changer la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Supprimer la configuration de catalogue.
   */
  public void supprimer() {
    // Demander confirmation
    boolean accord = DialogueConfirmation.afficher(getSession(), "Suppression d'une configuration catalogue",
        "Vous avez demandé la suppression de cette configuration catalogue.\nConfirmez-vous cette suppression ?");
    if (!accord) {
      return;
    }
    
    // Supprimer la configuration catalogue
    configurationCatalogue.supprimer(getIdSession());
    configurationCatalogue = null;
    
    // Changer la vue active
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le fournisseur de la configuration catalogue.
   * @param pIdFournisseur Nouveau fournisseur.
   */
  public void modifierFournisseur(IdFournisseur pIdFournisseur) {
    // Tester si la valeur a changé
    if (Constantes.equals(pIdFournisseur, configurationCatalogue.getIdFournisseur())) {
      return;
    }
    
    // Modifier la configuration catalogue
    configurationCatalogue.setIdFournisseur(pIdFournisseur);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le libellé de la configuration catalogue.
   * @param pLibelle Nouveau libellé.
   */
  public void modifierLibelle(String pLibelle) {
    // Tester si la valeur a changé
    if (Constantes.equals(configurationCatalogue.getLibelle(), pLibelle)) {
      return;
    }
    
    // Modifier la configuration catalogue
    configurationCatalogue.setLibelle(pLibelle);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier le fichier CSV.
   * On récupère les champs d'en tête du fichier CSV.
   * @param pFichierCSV Nouveau nom du fichier CSV.
   */
  public void modifierFichierCSV(String pFichierCSV) {
    // Tester si la valeur a changé
    if (Constantes.equals(configurationCatalogue.getFichierCSV(), pFichierCSV)) {
      return;
    }
    
    // Modifier la configuration catalogue
    configurationCatalogue.renseignerInfosFichierCSV(getIdSession(), pFichierCSV);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Ajouter une association d'un champ catalogue à un champ ERP dans le cadre de la configuration catalogue.
   * @param pChampERP Champ ERP à associer.
   * @param pChampCatalogue Champ catalogue à associer.
   */
  public void ajouterAssociationChamp(ChampERP pChampERP, ChampCatalogue pChampCatalogue) {
    // Configurer le champ catalogue
    pChampCatalogue.setDebutDecoupe(1);
    pChampCatalogue.setFinDecoupe(pChampERP.getLongueur());
    
    // Associer le champ catalogue au champ ERP
    ListeChampCatalogue liste = new ListeChampCatalogue();
    liste.add(pChampCatalogue);
    pChampERP.setListeChampCatalogue(liste);
    
    // Ajouter l'association au catalogue
    configurationCatalogue.ajouterUneAssociationChamp(pChampERP);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Retirer toute association de champ catalogue à un champ ERP dans le cadre de la configuration catalogue
   * @param pChampERP Champ ERP dont il faut enlever les associations.
   */
  public void retirerAssociationChamp(ChampERP pChampERP) {
    // Supprimer les associations du champ ERP
    pChampERP.setListeChampCatalogue(null);
    configurationCatalogue.retirerUneAssociationChamp(pChampERP);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Afficher la configuration catalogue précédente dans la liste.
   */
  public void afficherConfigurationPrecedente() {
    // Vérifier que la liste est renseignéé
    if (listeConfigurationCatalogue == null) {
      return;
    }
    
    // Récupérer la configuration précédente
    configurationCatalogue = listeConfigurationCatalogue.retournerConfigurationPrecedente(configurationCatalogue);
    chargerConfigurationCatalogue();
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Afficher la configuration catalogue suivante dans la liste.
   */
  public void afficherConfigurationSuivante() {
    // Vérifier que la liste est renseignéé
    if (listeConfigurationCatalogue == null) {
      return;
    }
    
    // Récupérer la configuration suivante
    configurationCatalogue = listeConfigurationCatalogue.retournerConfigurationSuivante(configurationCatalogue);
    chargerConfigurationCatalogue();
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Afficher la boîte de dialogue expert pour la configuration d'un champ.
   * @param Champ qu'il faut modifier en mode expert.
   */
  public void afficherModeExpert(ChampERP pChampERP) {
    if (configurationCatalogue == null) {
      throw new MessageErreurException("Aucune configuration de catalogue n'est chargée");
    }
    if (pChampERP == null) {
      throw new MessageErreurException("Le champ ERP n'est pas valide");
    }
    ListeChampCatalogue listeChampCatalogue = null;
    ChampERP champERP = null;
    if (configurationCatalogue != null && configurationCatalogue.getListeChampERP() != null) {
      champERP = configurationCatalogue.getListeChampERP().recupererUnChampParId(pChampERP.getId());
      listeChampCatalogue = configurationCatalogue.getListeChampCatalogue();
      if (champERP != null) {
        champERP.setLibelleCourt(pChampERP.getLibelleCourt());
        champERP.setLibelleLong(pChampERP.getLibelleLong());
        champERP.setType(pChampERP.getType());
        champERP.setLongueur(pChampERP.getLongueur());
      }
    }
    if (champERP == null) {
      champERP = pChampERP;
    }
    ModeleExpertConfigurationCatalogue modeleExpert = new ModeleExpertConfigurationCatalogue(getSession(), champERP);
    VueExpertConfigurationCatalogue vueExpert = new VueExpertConfigurationCatalogue(modeleExpert);
    modeleExpert.setListeChampCatalogue(listeChampCatalogue);
    
    vueExpert.afficher();
    
    ChampERP champModifie = modeleExpert.getChampERP();
    if (champModifie != null && champModifie.getListeChampCatalogue() != null) {
      configurationCatalogue.ajouterUneAssociationChamp(champModifie);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Rechercher les configurations de catalogue d'un établissement et d'un fournisseur donné
   * Si aucun Id fournisseur n'est transmis alors ce sont toutes les configurations de l'établissement qui sont sélectionnées.
   */
  private void chargerListeConfigurationCatalogue() {
    // Récupérer l'identifiant du filtre fournisseur
    IdFournisseur idFournisseur = null;
    if (fournisseurBase != null) {
      idFournisseur = fournisseurBase.getId();
    }
    
    // Charger la liste des configurations de catalogue
    listeConfigurationCatalogue = ListeConfigurationCatalogue.charger(getIdSession(), idEtablissement, idFournisseur);
  }
  
  /**
   * Charger les données complètes de la configuration catalogue sélectionnée.
   */
  private void chargerConfigurationCatalogue() {
    // Charger la configuration catalogue
    if (configurationCatalogue != null && configurationCatalogue.getId().isExistant()) {
      configurationCatalogue = ConfigurationCatalogue.chargerParId(getIdSession(), configurationCatalogue.getId());
      configurationInitiale = configurationCatalogue.clone();
    }
    
    // Modifierle filtre sur l'établissement avec celui de la configuration catalogue
    if (configurationCatalogue != null) {
      idEtablissement = configurationCatalogue.getIdEtablissement();
    }
    else {
      idEtablissement = null;
    }
  }
  
  /**
   * Contrôler la validité des données de la configuration de catalogue.
   */
  private void controlerConfigurationCatalogue() {
    if (configurationCatalogue == null) {
      throw new MessageErreurException("Aucune configuration de catalogue n'est chargée");
    }
    else if (configurationCatalogue.getIdFournisseur() == null) {
      throw new MessageErreurException("Aucun fournisseur n'est associé à la configuration");
    }
    else if (configurationCatalogue.getLibelle() == null || configurationCatalogue.getLibelle().trim().length() < 3) {
      throw new MessageErreurException("Aucun libellé compatible n'est associé à la configuration");
    }
    else if (configurationCatalogue.getFichierCSV() == null) {
      throw new MessageErreurException("Aucun fichier CSV n'est associé à la configuration");
    }
    else if (!controlerAssociationChamps(configurationCatalogue)) {
      throw new MessageErreurException("Il n'existe aucune association avec les champs du catalogue");
    }
  }
  
  /**
   * Controler si au moins un champ de l'ERP est associé à un champ du catalogue fournisseur.
   */
  private boolean controlerAssociationChamps(ConfigurationCatalogue pConfiguration) {
    if (pConfiguration == null || pConfiguration.getListeChampERP() == null) {
      return false;
    }
    
    boolean retour = false;
    for (ChampERP champERP : pConfiguration.getListeChampERP()) {
      if (champERP.getListeChampCatalogue() != null) {
        retour = true;
      }
    }
    
    return retour;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner le titre courant de l'écran.
   * @return Titre de l'écran.
   */
  public String getTitre() {
    // Construire un titre pour la vue détail
    if (getCleVueEnfantActive() != null && getCleVueEnfantActive().equals(EnumCleVueListeDetail.DETAIL)) {
      if (configurationCatalogue != null && configurationCatalogue.getId().isExistant()) {
        return TITRE_DETAIL + " " + configurationCatalogue.getId();
      }
      else {
        return TITRE_CREATION;
      }
    }
    
    // Retourner le titre de l'écran liste par défaut
    return TITRE_LISTE;
  }
  
  /**
   * Retourner l'identifiant de l'établissement sélectionné dans les filtres.
   * @return Identifiant de l'établissement.
   */
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  /**
   * Retourner le fournisseur sélectionné dans les filtres de recherche.
   * @return Fournisseur.
   */
  public FournisseurBase getFournisseurBase() {
    return fournisseurBase;
  }
  
  /**
   * Configuration de catalogue sélectionnée.
   * @return Configuration catalogue courante.
   */
  public ConfigurationCatalogue getConfigurationCatalogue() {
    return configurationCatalogue;
  }
  
  /**
   * Retourner la liste des configurations de catalogue sélectionnés
   * @return Liste des configurations catalogues issues de la recherche.
   */
  public ListeConfigurationCatalogue getListeConfigurationCatalogue() {
    return listeConfigurationCatalogue;
  }
  
  /**
   * Retourner la liste des fichiers CSV susceptibles d'être associés à la configuration de catalogue active
   * @return Liste des fichiers CSV.
   */
  public ListeFichierCSV getListeFichierCSV() {
    return listeFichierCSV;
  }
  
  /**
   * Retourner la liste complètes des champs de l'ERP disponibles pour des associations de champs catalogue fournisseurs
   * @return Liste des champs ERP.
   */
  public ListeChampERP getListeChampERP() {
    return listeChampERP;
  }
  
  /**
   * Indiquer si la configuration courante a été modifiée par rapport à la configuration initiale.
   * @return true=modifications présentes, false=aucune modification.
   */
  public boolean isConfigurationModifiee() {
    if (!isDonneesChargees()) {
      return false;
    }
    return !ConfigurationCatalogue.equalsComplet(configurationInitiale, configurationCatalogue);
  }
}
