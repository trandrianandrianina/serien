/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ri.serien.desktop.metier.gescom.achat.approvisionnementstock.ModeleApprovisionnementStock;
import ri.serien.desktop.metier.gescom.achat.approvisionnementstock.VueApprovisionnementStock;
import ri.serien.desktop.metier.gescom.achat.approvisonnementcommandeclient.ModeleApprovisionnementCommandeClient;
import ri.serien.desktop.metier.gescom.achat.approvisonnementcommandeclient.VueApprovisionnementCommandeClient;
import ri.serien.desktop.metier.gescom.achat.approvisonnementhorsstockclient.ModeleApprovisionnementHorsStockClient;
import ri.serien.desktop.metier.gescom.achat.approvisonnementhorsstockclient.VueApprovisionnementHorsStockClient;
import ri.serien.desktop.metier.gescom.achat.commandeencours.ModeleCommandeEnCours;
import ri.serien.desktop.metier.gescom.achat.commandeencours.VueCommandeEnCours;
import ri.serien.desktop.metier.gescom.achat.contacts.ModeleListeContactFournisseur;
import ri.serien.desktop.metier.gescom.achat.contacts.VueListeContactFournisseur;
import ri.serien.desktop.metier.gescom.achat.controleconditionnementfournisseur.ModeleControleConditionnementFournisseur;
import ri.serien.desktop.metier.gescom.achat.controleconditionnementfournisseur.VueControleConditionnementFournisseur;
import ri.serien.desktop.metier.gescom.achat.detailligne.ModeleDetailLigneAchat;
import ri.serien.desktop.metier.gescom.achat.detailligne.VueDetailLigneAchat;
import ri.serien.desktop.metier.gescom.achat.editiondocument.EditionDocument;
import ri.serien.desktop.metier.gescom.achat.selectioncommandeclient.ModeleSelectionCommandeClient;
import ri.serien.desktop.metier.gescom.achat.selectioncommandeclient.VueSelectionCommandeClient;
import ri.serien.desktop.metier.gescom.commun.commentaires.ModeleDetailCommentaire;
import ri.serien.desktop.metier.gescom.commun.commentaires.VueDetailCommentaire;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.desktop.sessions.SessionJava;
import ri.serien.libcommun.commun.EnumContexteMetier;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.commun.recherche.RechercheGenerique;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEnteteDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeEtatDocument;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.EnumTypeApprovisionnement;
import ri.serien.libcommun.gescom.achat.document.EnumTypeCommande;
import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ParametreMenuAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreDirectUsine;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.document.EnumModeEdition;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.IdAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.attente.SwingWorkerAttente;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.composant.dialoguestandard.confirmation.ModeleDialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle de la gestion des achats.
 */
public class ModeleAchat extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Achats";
  public static final String TYPEDOC_COMMANDE_ENLEVEMENT = "Commande d'enlèvement";
  public static final String TYPEDOC_COMMANDE_LIVRAISON = "Commande livraison";
  public static final String TYPEDOC_COMMANDE_DIRECT_USINE = "Commande direct usine";
  
  public static final int ETAPE_RECHERCHE_FOURNISSEUR = 1;
  public static final int ETAPE_SAISIE_ONGLET_LIVRAISON = 3;
  public static final int ETAPE_CREATION_BLOC_ADRESSE_LIVRAISON = 4;
  public static final int ETAPE_RECHERCHE_ARTICLE_STANDARD = 5;
  public static final int ETAPE_SAISIE_ONGLET_ARTICLES = 6;
  public static final int ETAPE_SAISIE_ONGLET_REGLEMENT = 7;
  public static final int ETAPE_SAISIE_ONGLET_EDITION = 8;
  public static final int ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE = 9;
  public static final int ETAPE_RECHERCHE_ARTICLE_PALETTE = 10;
  
  // --- Onglet article
  private static final String LIBELLE_RESULTAT_RECHERCHE_ARTICLES = "R\u00e9sultat de la recherche (%d)";
  
  public static final int FOCUS_ARTICLES_RECHERCHE_ARTICLE = 21;
  public static final int FOCUS_ARTICLES_RESULTAT_RECHERCHE = 22;
  public static final int FOCUS_ARTICLES_BOUTON_RECHERCHE_ARTICLE_TRANSPORT = 23;
  
  public static final int TABLE_RESULTAT_RECHERCHE_ARTICLES = 1;
  public static final int TABLE_LIGNES_DOCUMENT = 2;
  
  // Variables
  private int indiceOngletCourant = EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex();
  private int indiceOngletAccessible = indiceOngletCourant;
  private int etape = ETAPE_RECHERCHE_FOURNISSEUR;
  
  private Etablissement etablissement = null;
  private Magasin magasin = null;
  private UtilisateurGescom utilisateurGescom = null;
  private IdAdresseFournisseur idAdresseFournisseurCourant = null;
  private Fournisseur fournisseurCourant = null;
  private DocumentAchat documentAchatEnCours = null;
  private Date maintenant = null;
  private Date dateTraitement = null;
  private ListeAcheteur listeAcheteur = null;
  private ListeMagasin listeMagasin = null;
  private ListeUnite listeUnite = null;
  private boolean fermetureAutomatique = false;
  
  private ParametreMenuAchat parametres = null;
  private String messageErreur = "";
  
  // -- Onglet fournisseur
  private ListeEtablissement listeEtablissement = null;
  private ListeCivilite listeCivilite = null;
  private ListeDocumentAchat listeDocumentAchat = new ListeDocumentAchat();
  private boolean autoriserApprovisionnementStock = true;
  private boolean autoriserApprovisionnementHorsStockClient = true;
  private boolean autoriserApprovisionnementCommandeClientUrgente = true;
  
  // -- Onglet article
  // private int etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
  private int composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
  private Message message = null;
  private int indexRechercheArticle = -1;
  private String texteRecherche = "";
  private boolean filtreHorsGamme = false;
  private boolean filtreTousFournisseur = false;
  private ListeArticle listeResultatRechercheArticles = null;
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private Pattern patternQuantiteRechercheArticles = Pattern.compile("(-?[0-9.,]{0,})([dDgGsShHnNiIpPmMvVcC-]{0,})");
  // Liste éphémère des id de lignes articles à sélectionner si besoin
  private ArrayList<IdLigneAchat> listeLigneAchatASelectionner = new ArrayList<IdLigneAchat>();
  private BigDecimal nombreDePalettesLies = BigDecimal.ZERO;
  
  // -- Onglet édition
  private EnumOptionEdition optionChiffrageEdition = null;
  private ListeModeEdition listeModeEditionAchat = ListeModeEdition.getInstanceModeEditionAchat();
  private String emailDestinataireEdition = null;
  private String faxDestinataireEdition = null;
  private Contact contactMail = null;
  private Contact contactFax = null;
  
  /**
   * Constructeur.
   */
  public ModeleAchat(SessionBase pSession, Object pParametreMenuAchat) {
    super(pSession);
    pSession.getPanel().setName(TITRE);
    setCleVueEnfantActive(EnumCleVueAchat.ONGLET_FOURNISSEUR);
    
    if (pParametreMenuAchat != null && pParametreMenuAchat instanceof ParametreMenuAchat) {
      parametres = (ParametreMenuAchat) pParametreMenuAchat;
    }
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
    viderListeResultatRechercheArticles();
    texteRecherche = "";
    message = null;
    
    if (listeModeEditionAchat != null) {
      listeModeEditionAchat.setModeDebug(getSession().isModeDebug());
    }
  }
  
  @Override
  public void chargerDonnees() {
    // Permet d'effacer la liste des documents du fournisseur en cours
    effacerVariables();
    
    // -- Onglet fournisseur
    // Récupération des infos de l'utilisateur en gescom
    setUtilisateurGescom(chargerInformationsGescom(null));
    if (getUtilisateurGescom() == null) {
      throw new MessageErreurException("Les données de l'utilisateur n'ont pas pu être chargées.");
    }
    
    // Charger la liste des établissements
    listeEtablissement = ListeEtablissement.charger(getIdSession());
    
    // Récupération de la description générale
    setEtablissement(listeEtablissement.getEtablissementParId(getUtilisateurGescom().getIdEtablissementPrincipal()));
    
    // Chargement de toutes les données liées à l'établissement
    chargerDonneesLieesEtablissement(getUtilisateurGescom().getIdEtablissementPrincipal());
    
    // -- Onglet article
    // Par défaut le filtre sur les articles hors gammes ne doit pas être actif
    filtreHorsGamme = false;
    filtreTousFournisseur = false;
  }
  
  /**
   * Initialise les variables du modèle.
   */
  public void effacerVariables() {
    effacerVariablesNavigation();
    effacerVariablesFournisseur();
    effacerVariablesDocument();
  }
  
  /**
   * Construit le titre en fonction de l'avancement dans la saisie.
   */
  public String getTitre() {
    String titre = TITRE;
    String typedoc = "";
    
    if (documentAchatEnCours == null) {
      return titre;
    }
    
    switch (documentAchatEnCours.getId().getCodeEntete()) {
      case COMMANDE_OU_RECEPTION:
        if (documentAchatEnCours.isModeEnlevement()) {
          typedoc = TYPEDOC_COMMANDE_ENLEVEMENT;
        }
        else {
          typedoc = TYPEDOC_COMMANDE_LIVRAISON;
        }
        if (documentAchatEnCours.isDirectUsine()) {
          typedoc = TYPEDOC_COMMANDE_DIRECT_USINE;
        }
        break;
      
      default:
        throw new MessageErreurException(
            "Impossible de construire le titre d'un document d'achats de type " + documentAchatEnCours.getId().getCodeEntete());
    }
    documentAchatEnCours.setLibelleTypeDocument(typedoc);
    
    // On controle si le document a déjà un numéro
    if (!documentAchatEnCours.isExistant()) {
      if (typedoc.length() > 0) {
        titre = String.format("%s - %s", TITRE, typedoc);
      }
    }
    else {
      titre = String.format("%s - %s %s", TITRE, typedoc, documentAchatEnCours.getId().toString());
    }
    return titre;
  }
  
  /**
   * Fermeture de la getSession() panel.
   */
  public void fermerEcran() {
    getSession().fermerEcran();
  }
  
  /**
   * Retourne une chaine contenant le nom du fournisseur et le nom du magasin.
   * Format : NomFournisseur - NomMagasin
   */
  public String getInformationsPermanentes() {
    String informations = "";
    
    // Ajouter le nom du fournisseur
    if (fournisseurCourant != null) {
      informations += fournisseurCourant.getNomFournisseur();
    }
    
    // Ajouter le nom du magasin
    if (magasin != null) {
      // Ajouter un tiret si un fournisseur est présent
      if (!informations.isEmpty()) {
        informations += " - ";
      }
      
      // Ajouter le nom du magasin
      informations += magasin.getNom();
    }
    
    return informations;
  }
  
  /**
   * Contrôle si un document est en cours de création ou modification.
   */
  public boolean isDocumentEnCoursDeSaisie() {
    return indiceOngletAccessible > EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex();
  }
  
  /**
   * Annuler la saisie d'un document.
   */
  public void annulerDocument(boolean pAvecConfirmation, boolean pConserverFournisseur) {
    // Si le document n'a pas encore été créé.
    if (documentAchatEnCours == null) {
      if (etape != ETAPE_RECHERCHE_FOURNISSEUR && pAvecConfirmation && !isConfirmerDemande("Annulation de la création du document",
          "Vous avez demandé l'annulation du document en cours de création.\n Confirmez-vous cette annulation ?")) {
        return;
      }
      // Fermer l'écran suite à annulation ou mise en attente du document si la fermeture automatique est activée
      if (isFermetureAutomatique()) {
        fermerEcran();
      }
      // Sinon rafraichissement pour nouvelle saisie
      else {
        effacerVariables();
        documentAchatEnCours = null;
        // Permet d'effacer la liste des documents du fournisseur en cours
        effacerVariableOngletFournisseur();
        
        etape = ETAPE_RECHERCHE_FOURNISSEUR;
        modifierIndiceOnglet(EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex());
        rafraichir();
        return;
      }
    }
    if (documentAchatEnCours != null && documentAchatEnCours.isExistant()) {
      // Le document est annulé s'il est en cours de création
      if (documentAchatEnCours.isEnCoursCreation()) {
        if (pAvecConfirmation && !isConfirmerDemande("Annulation de la création du document",
            "Vous avez demandé l'annulation du document en cours de création.\n Confirmez-vous cette annulation ?")) {
          return;
        }
        ManagerServiceDocumentAchat.annulerDocument(getSession().getIdSession(), documentAchatEnCours.getId(), dateTraitement);
      }
      // Par contre, s'il existait auparavant alors il est mis en attente
      else {
        if (documentAchatEnCours.isEnCoursModification()) {
          if (pAvecConfirmation && !isConfirmerDemande("Mise en attente du document",
              "Vous avez demandé l'annulation des modifications du document en cours.\n Préférez-vous mettre en attente ce document ? "
                  + "\nRépondez Non pour revenir en modification du document.")) {
            
            rafraichir();
            return;
          }
        }
        ManagerServiceDocumentAchat.mettreEnAttenteDocument(getSession().getIdSession(), documentAchatEnCours.getId(), dateTraitement);
      }
    }
    
    // Conservation des information du fourniseur
    if (pConserverFournisseur) {
      effacerVariablesNavigation();
      effacerVariablesDocument();
    }
    else {
      effacerVariables();
      // Permet d'effacer la liste des documents du fournisseur en cours
      effacerVariableOngletFournisseur();
    }
    
    // Fermer l'écran suite à annulation ou mise en attente du document si la fermeture automatique est activée
    if (isFermetureAutomatique()) {
      fermerEcran();
    }
    // Sinon rafraichissement pour nouvelle saisie
    else {
      documentAchatEnCours = null;
      etape = ETAPE_RECHERCHE_FOURNISSEUR;
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex());
      rafraichir();
    }
  }
  
  /**
   * Créer une commande.
   */
  public void creerCommande() {
    if (indiceOngletAccessible > EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car un document est déjà en cours de saisie.");
    }
    
    // Création du document
    creerDocument(EnumTypeCommande.NORMALE, false);
    
    // On active l'onglet suivant
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    rafraichir();
  }
  
  /**
   * Modifier une commande.
   */
  public void modifierDocumentAchat(IdDocumentAchat pIdDocumentAchat, boolean pRafraichirVue) {
    if (pIdDocumentAchat == null) {
      throw new MessageErreurException("L'id du document d'achat est incorrect.");
    }
    if (indiceOngletAccessible > EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
      throw new MessageErreurException("Impossible de modifier le document choisi car un document est déjà en cours de saisie.");
    }
    // Modification du document
    modifierDocument(pIdDocumentAchat);
    
    // On rend tous les onglets accessibles puisque nous sommes en modification
    indiceOngletAccessible = EnumCleVueAchat.ONGLET_EDITION.getIndex();
    // Activation de l'onglet Enlèvement/Livraison
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    
    // Afin d'éviter les réentrance lors d'un appel depuis un autre programme comme le comptoir par exemple
    if (pRafraichirVue) {
      rafraichir();
    }
  }
  
  /**
   * Réapprovisionner le stock.
   */
  public void approvisionnerStock() {
    if (!autoriserApprovisionnementStock) {
      throw new MessageErreurException("Impossible d'approvisionner le stock une nouvelle fois le stock.");
    }
    if (magasin == null) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car le magasin n'est pas défini.");
    }
    
    // Création du document avec sauvegarde dans la base si elle n'existe pas déjà
    if (documentAchatEnCours == null || !documentAchatEnCours.isExistant()) {
      creerDocument(EnumTypeCommande.NORMALE, true);
    }
    
    // Affichage de la boite de dialogue qui liste les articles avec un stock négatif
    ModeleApprovisionnementStock modele = new ModeleApprovisionnementStock(getSession(), this, fournisseurCourant, magasin.getId());
    VueApprovisionnementStock vue = new VueApprovisionnementStock(modele);
    vue.afficher();
    
    // On sort de la boite de dialogue sans valider donc on annule le document que l'on a créé
    if (!modele.isSortieAvecValidation()) {
      // On annule le document uniquement si seul l'onglet fournisseur est actif
      if (indiceOngletAccessible == EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
        annulerDocument(false, true);
      }
      return;
    }
    // On l'on a déjà approvionné le stock
    autoriserApprovisionnementStock = false;
    
    // On passe à l'onglet suivant
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Proposer une commande client pour des articles hors stock.
   */
  public void approvisionnerHorsStockClient() {
    if (!autoriserApprovisionnementHorsStockClient) {
      throw new MessageErreurException("Impossible d'approvisionner le stock une nouvelle fois les commandes hors stock.");
    }
    if (magasin == null) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car le magasin n'est pas défini.");
    }
    
    // Création du document avec sauvegarde dans la base
    if (documentAchatEnCours == null || !documentAchatEnCours.isExistant()) {
      creerDocument(EnumTypeCommande.NORMALE, true);
    }
    
    // Affichage de la boite de dialogue qui liste les articles avec un stock négatif
    ModeleApprovisionnementHorsStockClient modele =
        new ModeleApprovisionnementHorsStockClient(getSession(), this, fournisseurCourant, magasin.getId());
    VueApprovisionnementHorsStockClient vue = new VueApprovisionnementHorsStockClient(modele);
    vue.afficher();
    
    // On sort de la boite de dialogue sans valider donc on annule le document que l'on a créé
    if (modele.isSortieAvecAnnulation()) {
      // On annule le document uniquement si seul l'onglet fournisseur est actif
      if (indiceOngletAccessible == EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
        annulerDocument(false, true);
      }
      return;
    }
    // On l'on a déjà approvionné le hors stock
    autoriserApprovisionnementHorsStockClient = false;
    
    // On passe à l'onglet suivant
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Proposer une commande client urgente.
   */
  public void approvisionnerCommandeClientUrgente() {
    if (!autoriserApprovisionnementCommandeClientUrgente) {
      throw new MessageErreurException("Impossible d'approvisionner le stock une nouvelle fois les commandes clients.");
    }
    if (magasin == null) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car le magasin n'est pas défini.");
    }
    
    // Affichage de la boite de dialogue qui liste les commandes clients
    ModeleSelectionCommandeClient modeleSelection =
        new ModeleSelectionCommandeClient(getSession(), this, EnumTypeApprovisionnement.CLIENT, fournisseurCourant, magasin.getId());
    VueSelectionCommandeClient vueSelection = new VueSelectionCommandeClient(modeleSelection);
    vueSelection.afficher();
    
    // Si aucune commande séléctionnée ou si le bouton Annuler a été actionné alors on sort directement
    if (modeleSelection.isSortieAvecAnnulation()) {
      return;
    }
    // Récupération du document de vente du client qui a été choisi
    ArrayList<DocumentVente> listeDocumentVenteChoisi = modeleSelection.getListeDocumentVenteChoisi();
    
    // Création du document avec sauvegarde dans la base
    if (documentAchatEnCours == null || !documentAchatEnCours.isExistant()) {
      creerDocument(EnumTypeCommande.NORMALE, true);
    }
    
    // Affichage de la boite de dialogue qui liste les lignes des commandes client avec les articles du fournisseur à commander
    ModeleApprovisionnementCommandeClient modele = new ModeleApprovisionnementCommandeClient(getSession(), this,
        EnumTypeApprovisionnement.CLIENT, fournisseurCourant, magasin.getId(), listeDocumentVenteChoisi);
    VueApprovisionnementCommandeClient vue = new VueApprovisionnementCommandeClient(modele);
    vue.afficher();
    
    // On sort de la boite de dialogue sans valider donc on annule le document que l'on a créé
    if (!modele.isSortieAvecValidation()) {
      // On annule le document uniquement si seul l'onglet fournisseur est actif
      if (indiceOngletAccessible == EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
        annulerDocument(false, true);
      }
      return;
    }
    // On l'on a déjà approvionné les commandes clients
    autoriserApprovisionnementCommandeClientUrgente = false;
    
    // On passe à l'onglet suivant
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    rafraichir();
  }
  
  /**
   * Créer une commande à partir d'une commande de vente direct usine.
   */
  public void creerCommandeDirectUsine() {
    if (indiceOngletAccessible > EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex()) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car un document est déjà en cours de saisie.");
    }
    if (magasin == null) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car le magasin n'est pas défini.");
    }
    
    // Affichage de la boite de dialogue qui liste les commandes clients
    ModeleSelectionCommandeClient modeleSelection = new ModeleSelectionCommandeClient(getSession(), this,
        EnumTypeApprovisionnement.DIRECT_USINE, fournisseurCourant, magasin.getId());
    modeleSelection.setFiltreIdClientFactureActif(true);
    VueSelectionCommandeClient vueSelection = new VueSelectionCommandeClient(modeleSelection);
    vueSelection.afficher();
    
    // Si aucune commande séléctionnée ou si le bouton Annuler a été actionné alors on sort directement
    if (modeleSelection.isSortieAvecAnnulation()) {
      return;
    }
    // Récupération de la liste des documents de vente qui ont été choisi
    ArrayList<DocumentVente> listeDocumentVenteChoisi = modeleSelection.getListeDocumentVenteChoisi();
    if (listeDocumentVenteChoisi == null || listeDocumentVenteChoisi.isEmpty()) {
      throw new MessageErreurException("Aucun document de vente n'a été choisi, la commande ne peut donc pas être créée.");
    }
    // Création du document avec sauvegarde dans la base
    creerDocument(EnumTypeCommande.DIRECT_USINE, true);
    
    // Permet d'afficher dans le titre le numéro du document
    rafraichir();
    
    // Affichage de la boite de dialogue qui liste les lignes des commandes client directs usines contanant les articles du fournisseur à
    // commander
    ModeleApprovisionnementCommandeClient modele = new ModeleApprovisionnementCommandeClient(getSession(), this,
        EnumTypeApprovisionnement.DIRECT_USINE, fournisseurCourant, magasin.getId(), listeDocumentVenteChoisi);
    VueApprovisionnementCommandeClient vue = new VueApprovisionnementCommandeClient(modele);
    vue.afficher();
    
    // On sort de la boite de dialogue sans valider donc on annule le document que l'on a créé
    if (!modele.isSortieAvecValidation()) {
      // On annule le document car on l'a obligatoirement créé lorsque l'on a lancé la boite de dialogue "Réapprovisionner stock"
      annulerDocument(false, true);
      return;
    }
    
    // Analyse de la liste des documents choisi afin de déterminer le mode Livraison/Enlèvment du document d'achat
    // Par défaut on initialise le document d'achat en mode Enlèvement
    documentAchatEnCours.setModeEnlevement(true);
    DocumentVente documentVenteReference = listeDocumentVenteChoisi.get(0);
    for (DocumentVente documentVente : listeDocumentVenteChoisi) {
      // Si un seul document de vente est en mode livraison alors le document d'achat est en mode livraison
      if (documentVente.isLivraison()) {
        documentAchatEnCours.setModeEnlevement(false);
        // Renseignement de la date de livraison prévue
        if (documentVenteReference.getTransport() != null) {
          documentAchatEnCours.setDateLivraisonPrevue(documentVenteReference.getTransport().getDateLivraisonPrevue());
        }
        // Initialisation de l'adresse de livraison avec celle du document de vente
        if (documentAchatEnCours.getAdresseLivraison() == null) {
          // Vérification qu'il y ait bien une adresse de livraison pour ce document de vente (sinon on prendra celle du document qui
          // suit)
          if (documentVente.getTransport() != null && documentVente.getTransport().getAdresseLivraison() != null) {
            Adresse adresseLivraison = documentVente.getTransport().getAdresseLivraison().clone();
            documentAchatEnCours.setAdresseLivraison(adresseLivraison);
            documentVenteReference = documentVente;
            break;
          }
        }
      }
    }
    if (documentVenteReference != null) {
      // Contrôle que l'on ait bien une adresse de livraison si ce n'est pas le cas on récupère l'adresse du client
      if (documentAchatEnCours.getAdresseLivraison() == null) {
        IdClient idClientLivre = documentVenteReference.getIdClientLivre();
        // Si l'id du client livré n'est pas nulle on charge le client afin de récupérer son adresse
        if (idClientLivre != null) {
          Client client = ManagerServiceClient.chargerClient(getSession().getIdSession(), idClientLivre);
          documentAchatEnCours.setAdresseLivraison(client.getAdresse().clone());
        }
      }
      // Alimentation de la référence interne à partir du document référence
      // Récupération du nom du client du document de vente
      String nomClient = "";
      if (documentVenteReference.getAdresseFacturation() != null && documentVenteReference.getAdresseFacturation().getNom() != null) {
        nomClient = Constantes.normerTexte(documentVenteReference.getAdresseFacturation().getNom());
      }
      // Récupération de la référence longue du document de vente
      String referenceInterne = Constantes.normerTexte(documentVenteReference.getReferenceLongue());
      // On construit la référence interne
      if (nomClient.isEmpty() || referenceInterne.isEmpty()) {
        referenceInterne = nomClient + referenceInterne;
      }
      else {
        referenceInterne = nomClient + " - " + referenceInterne;
      }
      // On contrôle la longueur du texte
      if (referenceInterne.length() > DocumentAchat.LONGUEUR_REFERENCE_INTERNE) {
        documentAchatEnCours.setReferenceInterne(referenceInterne.substring(0, DocumentAchat.LONGUEUR_REFERENCE_INTERNE));
      }
      else {
        documentAchatEnCours.setReferenceInterne(referenceInterne);
      }
    }
    
    // On passe à l'onglet suivant
    modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
    setEtape(ETAPE_SAISIE_ONGLET_LIVRAISON);
    
    rafraichir();
  }
  
  /**
   * Affiche une boite de dialogue pour une demande de confirmation.
   */
  public boolean isConfirmerDemande(String pTitre, String pMessage) {
    pTitre = Constantes.normerTexte(pTitre);
    pMessage = Constantes.normerTexte(pMessage);
    ModeleDialogueConfirmation modele = new ModeleDialogueConfirmation(getSession(), pTitre, pMessage);
    DialogueConfirmation vue = new DialogueConfirmation(modele);
    vue.afficher();
    return modele.isSortieAvecValidation();
  }
  
  /**
   * Indique si l'onglet Fournisseur est accessible.
   */
  public boolean isOngletFournisseurAccessible() {
    return true;
  }
  
  /**
   * Indique si l'onglet Livraison est accessible.
   */
  public boolean isOngletLivraisonAccessible() {
    return indiceOngletAccessible >= EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex();
  }
  
  /**
   * Indique si l'onglet Article est accessible.
   */
  public boolean isOngletArticleAccessible() {
    return indiceOngletAccessible >= EnumCleVueAchat.ONGLET_ARTICLE.getIndex();
  }
  
  /**
   * Indique si l'onglet Edition est accessible.
   */
  public boolean isOngletEditionAccessible() {
    return indiceOngletAccessible == EnumCleVueAchat.ONGLET_EDITION.getIndex();
  }
  
  /**
   * Met à jour l'indice de l'onglet courant ainsi que la valeur de l'indice accessible.
   */
  public void modifierIndiceOnglet(int pIndiceOnglet) {
    if (indiceOngletCourant != pIndiceOnglet) {
      messageErreur = "";
    }
    indiceOngletCourant = pIndiceOnglet;
    if (indiceOngletAccessible < indiceOngletCourant) {
      indiceOngletAccessible = indiceOngletCourant;
    }
    EnumCleVueAchat enumCleVueAchat = EnumCleVueAchat.valueOfByCode(indiceOngletCourant);
    setCleVueEnfantActive(enumCleVueAchat);
    
    // Sauver et recharger le document de ventes si on arrive sur l'onglet article
    // Pour rafraîchir les informations mises à jour par des programmes RPG
    if (indiceOngletCourant == EnumCleVueAchat.ONGLET_ARTICLE.getIndex()) {
      sauverDocumentAchat(documentAchatEnCours);
    }
  }
  
  /**
   * Initialise les variables du fournisseur.
   */
  public void effacerVariablesFournisseur() {
    idAdresseFournisseurCourant = null;
    fournisseurCourant = null;
    if (etablissement != null && etablissement.getDateTraitement() != null) {
      dateTraitement = etablissement.getDateTraitement();
    }
  }
  
  /**
   * Permet de d'appeler les méthodes "controler" des onglets que l'on quitte via le clic sur l'onglet.
   */
  public void validerOnglet(int pIndiceNouvelOnglet) {
    if (getCleVueEnfantActive() == null) {
      return;
    }
    int indiceOngletEnCours = getCleVueEnfantActive().getIndex();
    try {
      switch (indiceOngletEnCours) {
        case 0:
          // Rien à valider
          break;
        case 1:
          controlerOngletLivraisonEnlevement();
          break;
        case 2:
          controlerOngletArticle();
          break;
        case 3:
          controlerOngletEdition();
          break;
      }
      // On active le nouvel onglet
      modifierIndiceOnglet(pIndiceNouvelOnglet);
    }
    catch (Exception e) {
      modifierIndiceOnglet(indiceOngletEnCours);
      throw new MessageErreurException(e, "Impossible de quitter l'onglet en cours.");
    }
    rafraichir();
  }
  
  /**
   * Sauver un document d'achat en base.
   */
  public void sauverDocumentAchat(DocumentAchat pDocumentAchat) {
    // Vérifier que le document ne soit pas invalide
    if (pDocumentAchat == null) {
      throw new MessageErreurException("Impossible de sauver le document d'achats car il est invalide.");
    }
    
    // Sauver l'entête document
    documentAchatEnCours =
        ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(), ManagerSessionClient.getInstance().getProfil(), pDocumentAchat);
    
    // On charge l'entête et les lignes du document
    documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
    rafraichir();
  }
  
  /**
   * Initialise les variables à partir des paramètres d'entrées.
   */
  public void initialiserAvecParametres() {
    if (parametres == null) {
      return;
    }
    
    // Chargement du document à partir de son id
    IdDocumentAchat idDocumentAchat = parametres.getIdDocumentAchat();
    if (idDocumentAchat != null) {
      modifierDocumentAchat(idDocumentAchat, false);
    }
    
    // Controle des null pour les appels à suivre.
    if (documentAchatEnCours == null) {
      return;
    }
    
    // Rafraichissement de l'onglet Fournisseur après le chargement du document car il a fallu récupérer le fournisseur et son adresse
    fournisseurCourant = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), documentAchatEnCours.getIdFournisseur());
    List<AdresseFournisseur> listeAdresse =
        ManagerServiceFournisseur.chargerListeAdresseFournisseur(getIdSession(), documentAchatEnCours.getIdFournisseur(), true);
    if (listeAdresse == null || listeAdresse.isEmpty()) {
      throw new MessageErreurException("Aucune adresse n'a été trouvée pour ce fournisseur.");
    }
    
    // Adresse du fournisseur (Adresse du siège)
    idAdresseFournisseurCourant = listeAdresse.get(0).getId();
    
    fournisseurCourant.chargerListeContact(getIdSession());
    rafraichir();
    
    // Indication de l'onglet à ouvrir
    Integer numeroOnglet = parametres.getNumeroOnglet();
    if (numeroOnglet != null) {
      EnumCleVueAchat enumCleVueAchat = EnumCleVueAchat.valueOfByCode(numeroOnglet.intValue());
      setCleVueEnfantActive(enumCleVueAchat);
      modifierIndiceOnglet(numeroOnglet);
    }
    
    // Indication si la fermeture du programme des achats est automatique
    Boolean fermeture = parametres.getFermetureAutomatique();
    if (fermeture != null) {
      fermetureAutomatique = fermeture.booleanValue();
    }
    
    // Etape
    if (parametres.getEtapeDocument() != null) {
      documentAchatEnCours.setEtape(parametres.getEtapeDocument());
    }
  }
  
  /**
   * Initialisation pour l'onglet édition
   */
  public void initialiserOngletEdition() {
    // Pour l'onglet édition
    if (fournisseurCourant == null) {
      return;
    }
    if (optionChiffrageEdition == null) {
      optionChiffrageEdition = fournisseurCourant.getTypeOptionChiffrageEdition();
    }
    // Initialisation de l'email
    if (contactMail == null) {
      modifierEmailDestinataire(fournisseurCourant.getContactPrincipal(), false);
    }
    // Initialisation du numéro de fax
    if (contactFax == null) {
      modifierFaxDestinataire(fournisseurCourant.getContactPrincipal(), false);
    }
    
  }
  
  /**
   * Modifie le document d'achat qui a été sélectionné.
   */
  public void modifierDocumentAchat() {
    modifierDocumentAchat(getIdDocumentAchatSelectionne(), true);
  }
  
  /**
   * Annuler la saisie d'un document.
   */
  public void annulerDocument() {
    effacerVariables();
    annulerDocument(true, false);
  }
  
  // --- Onglet fournisseur
  
  /**
   * Efface les variables de l'onglet fournisseur.
   */
  public void effacerVariableOngletFournisseur() {
    listeDocumentAchat = null;
    messageErreur = "";
    effacerVariablesFournisseur();
    autoriserApprovisionnementStock = true;
    autoriserApprovisionnementHorsStockClient = true;
    autoriserApprovisionnementCommandeClientUrgente = true;
  }
  
  /**
   * Modifier l'établissement.
   */
  public void modifierEtablissement(Etablissement pEtablissement) {
    if (etablissement != null && (pEtablissement == null || etablissement.equals(pEtablissement))) {
      return;
    }
    
    try {
      if (pEtablissement != null) {
        // Si le chargement se passe bien alors on met à jour l'établissement
        if (utilisateurGescom != null) {
          utilisateurGescom = chargerInformationsGescom(pEtablissement.getId());
        }
        chargerDonneesLieesEtablissement(pEtablissement.getId());
      }
      etablissement = pEtablissement;
    }
    catch (MessageErreurException e) {
      // Dans le cas contraire on revient en arrière
      utilisateurGescom = chargerInformationsGescom(etablissement.getId());
      chargerDonneesLieesEtablissement(etablissement.getId());
      throw e;
    }
    rafraichir();
  }
  
  /**
   * Modifier le magasin.
   */
  public void modifierMagasin(Magasin pMagasin) {
    if (Constantes.equals(pMagasin, magasin)) {
      return;
    }
    
    magasin = pMagasin;
    rafraichir();
  }
  
  /**
   * Modifier l'acheteur.
   */
  public void modifierAcheteur(Acheteur acheteur) {
    if (utilisateurGescom == null) {
      return;
    }
    
    // Récupérer l'identifiant de l'acheteur (null si aucun acheteur de sélectionné)
    IdAcheteur idAcheteur = null;
    if (acheteur != null) {
      idAcheteur = acheteur.getId();
    }
    
    // Tester si l'identifiant à changé
    if (Constantes.equals(idAcheteur, utilisateurGescom.getIdAcheteur())) {
      return;
    }
    
    // Modifier l'identifiant
    utilisateurGescom.setIdAcheteur(idAcheteur);
    rafraichir();
  }
  
  /**
   * Affiche la boite de dialogue qui liste les fournisseurs trouvés avec les critères de recherche.
   */
  public void modifierAdresseFournisseur(AdresseFournisseur pAdresseFournisseur) {
    if (pAdresseFournisseur == null || Constantes.equals(pAdresseFournisseur.getId(), idAdresseFournisseurCourant)) {
      return;
    }
    
    idAdresseFournisseurCourant = pAdresseFournisseur.getId();
    
    // Récupération des infos pour le fournisseur sélectionné
    chargerFournisseur();
    
    rafraichir();
  }
  
  /**
   * Retourne la liste des coordonnées des fournisseurs possibles à partir des critères de recherche.
   */
  public ListeAdresseFournisseur retournerResultatRechercheCoordonneesFournisseur(String pExpression) {
    return ListeAdresseFournisseur.charger(getIdSession(), etablissement.getId(), pExpression);
  }
  
  /**
   * Modifier la plage d'affichage des documents d'achats.
   */
  public void modifierPlageDocumentAchat(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeDocumentAchat != null) {
      ListeDocumentAchat listeDocumentAchatCharge =
          listeDocumentAchat.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
      // Compléter le chargement des documents d'achats (pour l'adresse fournisseur)
      // TODO Pas terrible du tout, à supprimer en compléter la requête SQL de chargement à partir d'une liste d'id
      if (listeDocumentAchatCharge != null) {
        for (DocumentAchat documentAchat : listeDocumentAchatCharge) {
          DocumentAchat documentAchatComplete =
              ManagerServiceDocumentAchat.chargerDocumentAchat(getIdSession(), documentAchat.getId(), null, false);
          listeDocumentAchat.set(documentAchatComplete);
        }
      }
    }
  }
  
  /**
   * Tester si le fournisseur courant existe déjà dans la base de données.
   */
  public boolean isFournisseurCourantExistant() {
    return fournisseurCourant != null && fournisseurCourant.isExistant();
  }
  
  /**
   * Retourne l'id du document d'achat sélectionné dans la liste à partir de son index réel (indifféremment du tri).
   */
  public IdDocumentAchat getIdDocumentAchatSelectionne() {
    if (listeDocumentAchat == null) {
      return null;
    }
    return listeDocumentAchat.getIdSelection();
  }
  
  /**
   * Permet de retourner la liste des contacts du fournisseur affichée dans une boite de dialogue dédiée.
   */
  public void retournerListeContacts() {
    ModeleListeContactFournisseur modeleListeContactFournisseur = new ModeleListeContactFournisseur(getSession(), fournisseurCourant);
    VueListeContactFournisseur vueListeContactFournisseur = new VueListeContactFournisseur(modeleListeContactFournisseur);
    vueListeContactFournisseur.afficher();
  }
  
  // --- Onglet livraison
  
  /**
   * Effectue les controles et traitement lors de la sortie de l'onglet Livraison/Enlèvement.
   */
  public void controlerOngletLivraisonEnlevement() {
    // Contrôle des null
    if (documentAchatEnCours == null) {
      return;
    }
    
    // On enregistre le document dans la base
    // Dans le cas d'un document en cours de modification uniquement car en création à ce stade il n'a pas été enregistré dans la base
    if (documentAchatEnCours.isEnCoursModification()
        || (documentAchatEnCours.isEnCoursCreation() && documentAchatEnCours.getId().isExistant())) {
      documentAchatEnCours = ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(),
          ManagerSessionClient.getInstance().getProfil(), documentAchatEnCours);
    }
  }
  
  /**
   * Modifier le mode de transport de la marchandise (livraison ou enlèvement).
   */
  public void modifierModeEnlevement(boolean pEnlevement) {
    if (documentAchatEnCours == null) {
      return;
    }
    if (pEnlevement && documentAchatEnCours.isModeEnlevement()) {
      return;
    }
    // On modifie la valeur du mode Enlèvement/Livraison
    documentAchatEnCours.setModeEnlevement(pEnlevement);
    // On modifie la valeur qui mémorise si l'on doit appliquer les frais de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
    rafraichir();
  }
  
  /**
   * Modifier la référence commande du document.
   */
  public void modifierReferenceCommande(String pTexte) {
    pTexte = Constantes.normerTexte(pTexte);
    if (documentAchatEnCours == null || Constantes.equals(pTexte, documentAchatEnCours.getReferenceCommande())) {
      return;
    }
    
    documentAchatEnCours.setReferenceCommande(pTexte);
    rafraichir();
  }
  
  /**
   * Modifier la référence interne du document.
   */
  public void modifierReferenceInterne(String pTexte) {
    pTexte = Constantes.normerTexte(pTexte);
    if (documentAchatEnCours == null || Constantes.equals(pTexte, documentAchatEnCours.getReferenceInterne())) {
      return;
    }
    
    documentAchatEnCours.setReferenceInterne(pTexte);
    rafraichir();
  }
  
  /**
   * Modifier la date de traitement du document.
   */
  public void modifierDateTraitementDocument(Date pDateDocument) {
    if (documentAchatEnCours == null) {
      return;
    }
    pDateDocument = Constantes.normerDate(pDateDocument);
    if (pDateDocument.equals(documentAchatEnCours.getDateDernierTraitement())) {
      return;
    }
    
    documentAchatEnCours.setDateDernierTraitement(pDateDocument);
    rafraichir();
  }
  
  /**
   * Modifier la date de livraison (il n'y a pas de prévue ou souhaitée).
   */
  public void modifierDateLivraison(Date pDateLivraison) {
    if ((pDateLivraison == null) || (documentAchatEnCours == null)
        || pDateLivraison.equals(documentAchatEnCours.getDateLivraisonPrevue())) {
      return;
    }
    if (documentAchatEnCours.getDateHomologation() != null && pDateLivraison.before(documentAchatEnCours.getDateHomologation())) {
      throw new MessageErreurException(
          String.format("La date de livraison est antérieure à la date de validation du document %1$td/%1$tm/%1$tY",
              documentAchatEnCours.getDateHomologation()));
    }
    documentAchatEnCours.setDateLivraisonPrevue(pDateLivraison);
    
    rafraichir();
  }
  
  /**
   * Valider l'onglet Livraison.
   */
  public void validerOngletLivraison() {
    try {
      controlerOngletLivraisonEnlevement();
      
      // On active l'onglet suivant
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_ARTICLE.getIndex());
      
      etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
      composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
    }
    catch (Exception e) {
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_LIVRAISONENLEVEMENT.getIndex());
      throw new MessageErreurException(e, "Impossible de quitter l'onglet livraison.");
    }
    rafraichir();
  }
  
  /**
   * Modifier le nom pour la livraison.
   */
  public void modifierNomLivraison(String nomLivraison) {
    if (documentAchatEnCours == null || (documentAchatEnCours.isModeEnlevement() && !documentAchatEnCours.isDirectUsine())) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    nomLivraison = Constantes.normerTexte(nomLivraison);
    if (nomLivraison == null || (documentAchatEnCours.getAdresseLivraison() == null)
        || nomLivraison.equals(documentAchatEnCours.getAdresseLivraison().getNom())) {
      return;
    }
    documentAchatEnCours.getAdresseLivraison().setNom(nomLivraison);
    rafraichir();
  }
  
  /**
   * Modifier le complément de nom pour la livraison.
   */
  public void modifierComplementNomLivraison(String complementNomLivraison) {
    if (documentAchatEnCours == null || (documentAchatEnCours.isModeEnlevement() && !documentAchatEnCours.isDirectUsine())) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    complementNomLivraison = Constantes.normerTexte(complementNomLivraison);
    if (complementNomLivraison == null || documentAchatEnCours.getAdresseLivraison() == null
        || complementNomLivraison.equals(documentAchatEnCours.getAdresseLivraison().getComplementNom())) {
      return;
    }
    documentAchatEnCours.getAdresseLivraison().setComplementNom(complementNomLivraison);
    rafraichir();
  }
  
  /**
   * Modifier la rue pour la livraison.
   */
  public void modifierRueLivraison(String rueLivraison) {
    if (documentAchatEnCours == null || (documentAchatEnCours.isModeEnlevement() && !documentAchatEnCours.isDirectUsine())) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    rueLivraison = Constantes.normerTexte(rueLivraison);
    if (rueLivraison == null || documentAchatEnCours.getAdresseLivraison() == null
        || rueLivraison.equals(documentAchatEnCours.getAdresseLivraison().getRue())) {
      return;
    }
    documentAchatEnCours.getAdresseLivraison().setRue(rueLivraison);
    rafraichir();
  }
  
  /**
   * Modifier la localisation pour la livraison.
   */
  public void modifierLocalisationLivraison(String localisationLivraison) {
    if (documentAchatEnCours == null || (documentAchatEnCours.isModeEnlevement() && !documentAchatEnCours.isDirectUsine())) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    localisationLivraison = Constantes.normerTexte(localisationLivraison);
    if (localisationLivraison == null || documentAchatEnCours.getAdresseLivraison() == null
        || localisationLivraison.equals(documentAchatEnCours.getAdresseLivraison().getLocalisation())) {
      return;
    }
    documentAchatEnCours.getAdresseLivraison().setLocalisation(localisationLivraison);
    rafraichir();
  }
  
  /**
   * Modifier le code postal et la commune de l'adresse de livraison.
   */
  public void modifierCodePostalCommune(CodePostalCommune pCodePostalCommune) {
    if (documentAchatEnCours == null) {
      return;
    }
    if (documentAchatEnCours.isModeEnlevement() && !documentAchatEnCours.isDirectUsine()) {
      throw new MessageErreurException("Impossible de saisir les coordonnées de livraison du client pour un document enlèvement.");
    }
    
    if (pCodePostalCommune == null) {
      return;
    }
    
    if (!documentAchatEnCours.getAdresseLivraison().getCodePostalFormate().equalsIgnoreCase(pCodePostalCommune.getCodePostal())) {
      documentAchatEnCours.getAdresseLivraison().setCodePostal(pCodePostalCommune.getCodePostalFormate());
    }
    if (!documentAchatEnCours.getAdresseLivraison().getVille().equalsIgnoreCase(pCodePostalCommune.getVille())) {
      documentAchatEnCours.getAdresseLivraison().setVille(pCodePostalCommune.getVille());
    }
    
    rafraichir();
  }
  
  // -- Onglet article
  
  /**
   * Effectue les controles et traitement lors de la sortie de l'onglet Article.
   */
  public void controlerOngletArticle() {
    // On contrôle que le montant de la commande est atteint le montant mini de commande du fournisseur
    controlerMinimumCommandeFournisseurAtteint();
  }
  
  /**
   * Retourne si les conditions sont remplis pour valider l'onglet articles.
   */
  public boolean isValidationOngletArticlesPossible() {
    if (documentAchatEnCours == null) {
      return false;
    }
    // On contrôle que la commande ne soit pas vide
    if (documentAchatEnCours.getListeLigneAchat().isEmpty()) {
      return false;
    }
    
    // On contrôle le montant minimum de commande fournisseur
    if (!isMinimumCommandeFournisseurAtteint()) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Valider l'onglet Articles.
   */
  public void validerOngletArticles() {
    // Si le document ne contient aucune ligne d'achat on reste sur l'onglet Article
    if (!isValidationOngletArticlesPossible()) {
      return;
    }
    
    try {
      controlerOngletArticle();
      etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_EDITION.getIndex());
      initialiserOngletEdition();
    }
    catch (Exception e) {
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_ARTICLE.getIndex());
      throw new MessageErreurException(e, "Impossible de quitter l'onglet article.");
    }
    rafraichir();
  }
  
  /**
   * Effacer le résultat de la recherche articles.
   */
  public void effacerResultatRechercheArticles() {
    viderListeResultatRechercheArticles();
    texteRecherche = "";
    filtreHorsGamme = false;
    filtreTousFournisseur = false;
    etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
    message = null;
    
    rafraichir();
  }
  
  /**
   * Modifier le texte de recherche.
   * Cela lance automatiquement une recherche article.
   */
  public void modifierTexteRecherche(String pTexteRecherche) {
    // Même si la valeur est identique à la fois précédente, on lance une recherche.
    texteRecherche = Constantes.normerTexte(pTexteRecherche);
    
    // Lancer une recherche différente suivant le mode
    switch (etape) {
      case ETAPE_RECHERCHE_ARTICLE_STANDARD:
        rechercherArticlesStandards();
        return;
      case ETAPE_RECHERCHE_ARTICLE_PALETTE:
        rechercherArticlesPalettes(null);
        return;
      
      case ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
        rechercherArticleCommentaire();
        return;
      
      default:
        rechercherArticlesStandards();
        return;
    }
  }
  
  /**
   * Modifier l'option de recherche article gamme/hors gamme.
   */
  public void modifierFiltreHorsGamme(boolean pFiltreHorsGamme) {
    if (filtreHorsGamme == pFiltreHorsGamme) {
      return;
    }
    
    filtreHorsGamme = pFiltreHorsGamme;
    
    // Relancer une recherche si une recherche est en cours
    if (texteRecherche != null && !texteRecherche.isEmpty()) {
      rechercherArticlesStandards();
      return;
    }
    
    etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
    rafraichir();
  }
  
  /**
   * Modifier l'option de recherche article tous fournisseurs
   */
  public void modifierFiltreFournisseur(boolean pFiltreToutFournisseur) {
    if (filtreTousFournisseur == pFiltreToutFournisseur) {
      return;
    }
    
    filtreTousFournisseur = pFiltreToutFournisseur;
    // Relancer une recherche si une recherche est en cours
    if (texteRecherche != null && !texteRecherche.isEmpty()) {
      rechercherArticlesStandards();
      return;
    }
    etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
    rafraichir();
  }
  
  /**
   * Lancer la recherche d'articles standards.
   */
  public void rechercherArticlesStandards() {
    // Quitter l'onglet article si rien n'est saisi dans la zone de recherche
    if (texteRecherche.isEmpty()) {
      validerOngletArticles();
    }
    
    // Vider le résultat de la recherche précédente
    viderListeResultatRechercheArticles();
    
    // Renseigner les critères de recherche
    CritereArticle critereArticle = new CritereArticle();
    critereArticle.setIdEtablissement(etablissement.getId());
    critereArticle.setTexteRechercheGenerique(texteRecherche);
    if (filtreHorsGamme) {
      critereArticle.setHorsGamme(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT);
    }
    else {
      critereArticle.setHorsGamme(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME);
    }
    if (!filtreTousFournisseur) {
      critereArticle.setIdFournisseur(fournisseurCourant.getId());
    }
    else {
      critereArticle.setIdFournisseur(null);
    }
    critereArticle.setDirectUsine(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE);
    
    // Afficher les mots effectivement recherchés dans la zone de recherche
    texteRecherche = critereArticle.getTexteRechercheGenerique();
    
    // Afficher le message issu de l'analyse du texte recherché
    message = Message.getMessageNormal(critereArticle.getRechercheGenerique().getMessage());
    
    // Contrôler que l'analyse du texte n'a pas retournée d'erreur
    if (critereArticle.getRechercheGenerique().isEnErreur()) {
      rafraichir();
      return;
    }
    
    // Rechercher les identifiants articles correspondants aux critères de recherche
    ListeArticleBase listeArticlesBases = ListeArticleBase.charger(getIdSession(), critereArticle);
    
    // Enlever un caractère au texte recherché si rien n'a été trouvé
    if (listeArticlesBases == null || listeArticlesBases.isEmpty()) {
      if (texteRecherche.length() > RechercheGenerique.LONGUEUR_MINI_TEXTE_RECHERCHE) {
        texteRecherche = texteRecherche.trim().substring(0, texteRecherche.length() - 1);
        message = Message.getMessageImportant("Aucun article n'a été trouvé, essayez avec : " + texteRecherche);
      }
      else {
        message = Message.getMessageImportant("Aucun article n'a été trouvé.");
      }
      etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
      rafraichir();
      return;
    }
    
    // Construire le message
    message = Message.getMessageNormal(String.format(LIBELLE_RESULTAT_RECHERCHE_ARTICLES, listeArticlesBases.size()));
    
    // Mémoriser le résultat
    listeResultatRechercheArticles = new ListeArticle();
    for (ArticleBase articleBase : listeArticlesBases) {
      Article article = new Article(articleBase.getId());
      article.setLibelleComplet(articleBase.getLibelleComplet());
      
      listeResultatRechercheArticles.add(article);
    }
    
    // Charger les articles affichés
    chargerPlageArticles(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraîchir l'écran
    etape = ETAPE_SAISIE_ONGLET_ARTICLES;
    composantAyantLeFocus = FOCUS_ARTICLES_RESULTAT_RECHERCHE;
    
    rafraichir();
  }
  
  /**
   * Lancer la recherche d'articles commentaires avec les critères de recherche.
   */
  public void rechercherArticleCommentaire() {
    viderListeResultatRechercheArticles();
    
    // Renseigner les critères de recherche
    CritereArticleCommentaire critere = new CritereArticleCommentaire();
    critere.setIdEtablissement(etablissement.getId());
    
    // Renseigner le texte de recherche s'il n'est pas vide
    if (!texteRecherche.isEmpty()) {
      critere.setTexteRechercheGenerique(texteRecherche);
      texteRecherche = critere.getRechercheGenerique().getTexteFinal();
      message = Message.getMessageNormal(critere.getRechercheGenerique().getMessage());
      if (critere.getRechercheGenerique().isEnErreur()) {
        composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
        rafraichir();
        return;
      }
    }
    
    // Rechercher les articles commentaires
    listeResultatRechercheArticles = new ListeArticle();
    listeResultatRechercheArticles = ManagerServiceArticle.chargerListeArticleCommentaire(getSession().getIdSession(), critere);
    
    // Afficher un message si aucun commentaire n'a été trouvé
    if (listeResultatRechercheArticles == null || listeResultatRechercheArticles.size() == 0) {
      message = Message.getMessageImportant("Aucun commentaire n'a été trouvé pour vos critères.");
      composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
      rafraichir();
      return;
    }
    
    // Charger les articles affichés
    chargerPlageArticles(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir
    etape = ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE;
    composantAyantLeFocus = FOCUS_ARTICLES_RESULTAT_RECHERCHE;
    rafraichir();
  }
  
  /**
   * Traitement dans le cas d'une saisie sur une ligne article.
   * Il va falloir faire le ménage la dedans...
   */
  public boolean selectionnerLigneArticleStandard(LigneAchat pLigneAchat, int pIndiceOngletDetail, int pTableAppelante, Article pArticle,
      boolean pEnAjout, boolean pControlerCommandeEnCours, boolean pRafraichirEcran) {
    ModeleDetailLigneAchat modele = null;
    boolean validation = false;
    
    if (documentAchatEnCours == null) {
      return false;
    }
    
    // S'il s'agit d'une ligne commentaire on n'affiche pas la boite de dialogue détail de la ligne
    if (pLigneAchat.isLigneCommentaire()) {
      return false;
    }
    
    // Vérification que les informations d'achats soient chargées
    if (pArticle == null || pArticle.getAlerteConditionnementFournisseur() == null) {
      ParametresLireArticle parametres = new ParametresLireArticle();
      parametres.setIdEtablissement(etablissement.getId());
      parametres.setIdMagasin(magasin.getId());
      parametres.setIdFournisseur(fournisseurCourant.getId());
      List<IdArticle> listeId = new ArrayList<IdArticle>();
      listeId.add(pLigneAchat.getIdArticle());
      ListeArticle listeArticles = ManagerServiceArticle.chargerListeInformationAchat(getIdSession(), listeId, parametres);
      if (listeArticles != null && !listeArticles.isEmpty()) {
        pArticle = listeArticles.get(0);
      }
    }
    // Chargement complet des données principales de l'article
    pArticle = ManagerServiceArticle.completerArticleStandard(getIdSession(), pArticle);
    
    if (pArticle.isArticleTransport()) {
      pIndiceOngletDetail = ModeleDetailLigneAchat.NE_PAS_AFFICHER;
    }
    
    // La boite de dialogue n'est pas affichée car il s'agit juste d'une saisie de quantité dans la liste
    if (pIndiceOngletDetail == ModeleDetailLigneAchat.NE_PAS_AFFICHER) {
      validation = true;
    }
    // La boite de dialogue du détail de la ligne est affichée
    else {
      // Sauvegarde de la ligne article avant l'ouverture de la boite de dialogue
      LigneAchatArticle ligneAchatArticle = (LigneAchatArticle) pLigneAchat;
      boolean appelDepuisRechercheArticle = false;
      if (pTableAppelante == TABLE_RESULTAT_RECHERCHE_ARTICLES) {
        appelDepuisRechercheArticle = true;
      }
      modele = new ModeleDetailLigneAchat(getSession(), pIndiceOngletDetail, ligneAchatArticle, pArticle, documentAchatEnCours,
          appelDepuisRechercheArticle);
      VueDetailLigneAchat vue = new VueDetailLigneAchat(modele);
      vue.afficher();
      
      // On sort de la boite de dialogue en validant
      if (modele.isSortieAvecValidation() && modele.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) != 0) {
        validation = true;
        // Vérification du respect du conditionnement fournisseur
        if (pArticle.isAlerteConditionnementFournisseur()) {
          ligneAchatArticle.getPrixAchat().setQuantiteReliquatUCA(
              controlerRespectConditionnementFournisseur(pArticle, ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA()), true);
          pLigneAchat = ligneAchatArticle;
        }
        
        // Si on est dans une commande direct usine le type de la ligne doit être ligne spéciale non stockée (code ERL == 'S')
        if (documentAchatEnCours.isDirectUsine() && !pLigneAchat.isLigneCommentaire()) {
          pLigneAchat.setTypeLigne(EnumTypeLigneAchat.SPECIALE_NON_STOCKEE);
        }
        
        pLigneAchat = ManagerServiceDocumentAchat.controlerLigneDocument(getIdSession(), pLigneAchat, dateTraitement);
        ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), pLigneAchat, dateTraitement);
      }
    }
    
    // Traitement dans le cas de l'ajout d'un article provenant de la recherche article
    if (pTableAppelante == TABLE_RESULTAT_RECHERCHE_ARTICLES) {
      // Dans le cas d'une validation de saisie, on effectue un contrôle de l'article sur les commandes en cours
      if (validation && pControlerCommandeEnCours) {
        // Contrôle si l'article n'est pas déjà en cours de commande
        validation = controlerArticleEnCoursCommande(pArticle);
      }
      // Au final on ne confirme pas la ligne article donc on la supprime
      if (!validation) {
        supprimerLignesArticlesAvecId(pLigneAchat.getId());
      }
      // Si on confirme on recherche si cet article a des articles liés
      else {
        rechercherArticlesLies(pArticle, pLigneAchat.getId());
      }
    }
    // Dans le cas où l'on modifie la quantité d'une ligne du document déjà existante
    // on contrôle si la quantité vaut zéro si c'est le cas on supprime la ligne du doucment
    else if (pLigneAchat instanceof LigneAchatArticle
        && ((LigneAchatArticle) pLigneAchat).getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) == 0) {
      supprimerLignesArticlesAvecId(pLigneAchat.getId());
    }
    
    // Recharge les lignes et l'entête du document
    documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
    
    // Contrôle le franco de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
    
    if (pRafraichirEcran) {
      rafraichir();
    }
    return validation;
  }
  
  /**
   * Traiter l'ajout d'un article standard avec une quantité suivie de la lettre P.
   */
  private IdLigneAchat ajouterArticleStandardAvecP(Article pArticle, BigDecimal quantite, int pIndiceLigneInsertion) {
    if (documentAchatEnCours == null) {
      return null;
    }
    
    // Tester si une unité de conditionnement de stock est définie
    if (!pArticle.isDefiniNombreUSParUCS()) {
      throw new MessageErreurException("Il n'y a pas d'unité de conditionnement de stock pour cet article.");
    }
    
    // Forcer les quantités de palettes à saisir
    forcerQuantiteSaisiePalette(pArticle);
    
    // Calculer la quantité en UCA par rapport au nombre d'UCA par unité de conditionnement de stock (palette) défini pour l'article
    quantite = (quantite.multiply(pArticle.getPrixAchat().getNombreUCAParUCS(), MathContext.DECIMAL32));
    
    // Insérer l'article dans le document
    IdLigneAchat idLigneAchat = insererArticleDansDocument(pArticle, pIndiceLigneInsertion, quantite, null);
    if (idLigneAchat == null) {
      throw new MessageErreurException("Erreur lors de l'ajout de l'article au document.");
    }
    
    // Traiter l'insertion
    for (LigneAchat ligneAchat : documentAchatEnCours.getListeLigneAchat()) {
      if (ligneAchat.getId().equals(idLigneAchat)) {
        selectionnerLigneArticleStandard(ligneAchat, ModeleDetailLigneArticle.NE_PAS_AFFICHER, TABLE_RESULTAT_RECHERCHE_ARTICLES,
            pArticle, true, true, true);
        idLigneAchat = ligneAchat.getId();
        break;
      }
    }
    
    // Rafraichir
    rafraichir();
    return idLigneAchat;
  }
  
  /**
   * Traiter l'ajout d'un article standard avec une quantité suivie de la lettre M.
   */
  private IdLigneAchat ajouterArticleStandardAvecM(Article pArticle, BigDecimal quantite, int pIndiceLigneInsertion) {
    if (documentAchatEnCours == null) {
      return null;
    }
    
    // Forcer les quantités de palettes à saisir
    forcerQuantiteSaisiePalette(pArticle);
    
    // Calculer la quantité en UCA par rapport au nombre d'UCA par unité d'achat (UA) défini pour l'article
    quantite =
        (quantite.divide(pArticle.getPrixAchat().getNombreUAParUCA(), pArticle.getPrixAchat().getNombreDecimaleUCA(), RoundingMode.UP));
    
    // Insérer l'article dans le document
    IdLigneAchat idLigneAchat = insererArticleDansDocument(pArticle, pIndiceLigneInsertion, quantite, null);
    if (idLigneAchat == null) {
      throw new MessageErreurException("Erreur lors de l'ajout de l'article au document.");
    }
    
    // Traiter l'insertion
    for (LigneAchat ligneAchat : documentAchatEnCours.getListeLigneAchat()) {
      if (ligneAchat.getId().equals(idLigneAchat)) {
        selectionnerLigneArticleStandard(ligneAchat, ModeleDetailLigneArticle.NE_PAS_AFFICHER, TABLE_RESULTAT_RECHERCHE_ARTICLES,
            pArticle, true, true, true);
        idLigneAchat = ligneAchat.getId();
        break;
      }
    }
    
    // Rafraichir
    rafraichir();
    return idLigneAchat;
  }
  
  /**
   * Ajouter un article commentaire au document.
   */
  public void ajouterArticleCommentaire(int pLigneSelectionnee, int pIndexInsertion) {
    if (pLigneSelectionnee == -1) {
      return;
    }
    if (documentAchatEnCours == null) {
      return;
    }
    
    // Récupérer l'article sélectionné
    Article article = listeResultatRechercheArticles.get(pLigneSelectionnee);
    
    // Lancement de la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modeleCommentaire =
        ModeleDetailCommentaire.getInstanceAjouterArticleCommentaire(getSession(), article, EnumContexteMetier.CONTEXTE_ACHAT);
    VueDetailCommentaire vue = new VueDetailCommentaire(modeleCommentaire);
    vue.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    if (modeleCommentaire.isSortieAvecAnnulation()) {
      return;
    }
    else if (modeleCommentaire.isSortieAvecSuppression()) {
      // Ne rien faire. On reste dans le mode recherche commentaire. La liste sera raffraichie sans le commentaire supprimé.
    }
    else if (modeleCommentaire.isSortieAvecValidation()) {
      if (modeleCommentaire.getTexteCommentaire() != null && !modeleCommentaire.getTexteCommentaire().trim().isEmpty()) {
        int numeroLigne = -1;
        numeroLigne = documentAchatEnCours.calculerNumeroLigneArticle(pIndexInsertion);
        
        String[] texteEnTable = modeleCommentaire.getTexteCommentaire().trim().split("(?<=\\G.{120})|\\n");
        
        for (int i = 0; i < texteEnTable.length; i++) {
          insererUneLigneCommentaire(texteEnTable[i], numeroLigne + i);
        }
      }
      
      // Recharger le document
      if (documentAchatEnCours != null && documentAchatEnCours.getId().getNumero() > 0) {
        // Rechargement les lignes et l'entête du document
        documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
      }
    }
    
    // Recharger les commentaires (au cas où si le commentaire à été modifé)
    rechercherArticleCommentaire();
    
    // Rafraichir
    etape = ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE;
    rafraichir();
  }
  
  /**
   * Recherche des articles de type palettes.
   */
  private void rechercherArticlesPalettes(Article pArticle) {
    viderListeResultatRechercheArticles();
    
    if (pArticle != null) {
      String libelleUniteStock = "";
      String libelleUniteConditionnement = "";
      if (pArticle.getIdUS() != null) {
        Unite uniteStock = ManagerServiceParametre.chargerUnite(getSession().getIdSession(), pArticle.getIdUS());
        libelleUniteStock = uniteStock.getLibelle();
      }
      if (pArticle.getIdUCS() != null) {
        Unite uniteCondition = ManagerServiceParametre.chargerUnite(getSession().getIdSession(), pArticle.getIdUCS());
        libelleUniteConditionnement = uniteCondition.getLibelle();
      }
      messageErreur = pArticle.getPrixAchat().getNombreUCAParUCS() + " " + libelleUniteStock + " par " + libelleUniteConditionnement;
    }
    
    // Affichage palettes uniquement si type d'unité de stock est la palette
    // if (pArticle == null || (pArticle.getIdUCS() != null && pArticle.getIdUCS().isPalette())) {
    // Renseigner les critères de recherche
    CritereArticle critereArticle = new CritereArticle();
    critereArticle.setIdEtablissement(etablissement.getId());
    critereArticle.setIdMagasin(magasin.getId());
    // critereArticle.setHorsGamme(filtreHorsGamme);
    critereArticle.setIdFournisseur(fournisseurCourant.getId());
    critereArticle.setDirectUsine(EnumFiltreDirectUsine.OPTION_TOUS_LES_ARTICLES_SAUF_DIRECT_USINE);
    critereArticle.setTexteRechercheGenerique(texteRecherche);
    
    // Récupération d'une liste d'articles avec gestion de la pagination
    ListeArticle listeResultat = ManagerServiceArticle.chargerListeArticlePaletteFournisseur(getSession().getIdSession(), critereArticle);
    
    // Enlever un caractère au texte recherché si rien n'a été trouvé
    if (listeResultat == null || listeResultat.isEmpty()) {
      if (texteRecherche.length() > RechercheGenerique.LONGUEUR_MINI_TEXTE_RECHERCHE) {
        texteRecherche = texteRecherche.trim().substring(0, texteRecherche.length() - 1);
        message = Message.getMessageImportant("Aucun article n'a été trouvé, essayez avec : " + texteRecherche);
      }
      else {
        message = Message.getMessageImportant("Aucun article n'a été trouvé.");
      }
      etape = ETAPE_RECHERCHE_ARTICLE_PALETTE;
      rafraichir();
      return;
    }
    
    // Construire le message
    message = Message.getMessageNormal(String.format(LIBELLE_RESULTAT_RECHERCHE_ARTICLES, listeResultat.size()));
    
    // Mémoriser le résultat
    listeResultatRechercheArticles = new ListeArticle();
    for (Article palette : listeResultat) {
      Article articlePalette = new Article(palette.getId());
      articlePalette.setLibelleComplet(palette.getLibelleComplet());
      
      listeResultatRechercheArticles.add(articlePalette);
    }
    
    // Charger les articles affichés
    chargerPlageArticles(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraîchir l'écran
    etape = ETAPE_SAISIE_ONGLET_ARTICLES;
    composantAyantLeFocus = FOCUS_ARTICLES_RESULTAT_RECHERCHE;
    
    rafraichir();
  }
  
  private void forcerQuantiteSaisiePalette(Article pArticle) {
    if (pArticle == null) {
      return;
    }
    if (documentAchatEnCours == null) {
      return;
    }
    
    for (int i = 0; i < documentAchatEnCours.getListeLigneAchat().size(); i++) {
      LigneAchatArticle ligneAchat = (LigneAchatArticle) documentAchatEnCours.getListeLigneAchat().get(i);
      if (Constantes.equals(ligneAchat.getIdArticle(), pArticle.getId()) && !ligneAchat.isLigneCommentaire()
          && pArticle.isDefiniNombreUSParUCS()) {
        nombreDePalettesLies =
            ligneAchat.getPrixAchat().getQuantiteReliquatUCA().divide(pArticle.getNombreUSParUCS(true), MathContext.DECIMAL32);
      }
    }
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticlesAffiches(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeResultatRechercheArticles == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeResultatRechercheArticles.size()) {
      indexPremiereLigneAffichee = listeResultatRechercheArticles.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeResultatRechercheArticles == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeResultatRechercheArticles.size()) {
      nombreLigneAffichee = listeResultatRechercheArticles.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacement de l'ascenseur avec la souris
    if (indexRechercheArticle < indexPremiereLigneAffichee) {
      indexRechercheArticle = indexPremiereLigneAffichee;
    }
    else if (indexRechercheArticle > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheArticle = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleAchat.class, "modifierPlageArticlesAffiches", "indexPremiereLigneVisible", indexPremiereLigneAffichee,
        "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageArticles(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Passer en mode recherche des articles standards.
   */
  public void activerModeRechercheArticleStandard() {
    viderListeResultatRechercheArticles();
    texteRecherche = "";
    etape = ETAPE_RECHERCHE_ARTICLE_STANDARD;
    composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
    rafraichir();
  }
  
  /**
   * Retourne la précision d'une unité donnée
   */
  public int retournerPrecisionUnite(IdUnite pId) {
    return listeUnite.getPrecisionUnite(pId);
  }
  
  /**
   * Formate la liste des lignes articles du document pour l'affichage.
   */
  public String[][] formaterListeArticlesDocument(int pNombreColonnes) {
    // Vérifier que le document d'achats ainsi que les lignes sont présents
    if (documentAchatEnCours == null || documentAchatEnCours.getListeLigneAchat() == null) {
      return new String[0][pNombreColonnes];
    }
    
    // Création du tableau qui va recevoir les données à afficher dans la liste
    int tailleListeArticles = documentAchatEnCours.getListeLigneAchat().size();
    String[][] donnees = new String[tailleListeArticles][pNombreColonnes];
    
    // En fonction du type de la ligne Article, il y a un traitement spécifique
    for (int ligne = 0; ligne < tailleListeArticles; ligne++) {
      LigneAchat ligneAchat = documentAchatEnCours.getListeLigneAchat().get(ligne);
      // Ligne article normale
      if (ligneAchat instanceof LigneAchatArticle) {
        donnees[ligne] = formaterLigneArticle((LigneAchatArticle) ligneAchat, pNombreColonnes, documentAchatEnCours.isEnCoursCreation());
      }
      else if (ligneAchat instanceof LigneAchatCommentaire) {
        donnees[ligne] =
            formaterLigneCommentaire((LigneAchatCommentaire) ligneAchat, pNombreColonnes, documentAchatEnCours.isEnCoursCreation());
      }
    }
    return donnees;
  }
  
  /**
   * Modifier la quantité d'une ligne de la liste de la recherche article.
   */
  public void modifierQuantiteLigneRechercheArticles(int pLigneModifiee, String pValeur, int pIndiceLigneInsertion) {
    pValeur = Constantes.normerTexte(pValeur);
    
    // Si rien n'est saisit dans la cellule, effacer la recherche en cours
    if (pValeur.isEmpty()) {
      // Vider le résultat de la recherche si on change de mode
      // La condition a été commentée car elle empêche la liste de se vider lorsque l'on a rien saisi dans la quantité
      // A voir si cela pose problème à moyen terme
      viderListeResultatRechercheArticles();
      // setEtape(ETAPE_RECHERCHE_ARTICLE_STANDARD);
      composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
      texteRecherche = "";
      rafraichir();
      return;
    }
    
    // Vérifier que le résultat de la liste n'est pas vide
    if (listeResultatRechercheArticles == null) {
      throw new MessageErreurException("Erreur lors la saisie d'une quantité alors que le résultat de la recherche est vide.");
    }
    
    Article article = listeResultatRechercheArticles.get(pLigneModifiee);
    if (article.getIdFournisseur() == null) {
      throw new MessageErreurException(
          "Cet article ne peut pas être commandé chez ce fournisseur car il n'a pas de fournisseur principal. Le fournisseur principal est nécessaire pour déterminer le prix d'achat.");
    }
    
    modifierQuantiteLigneTableau(article, pValeur, pIndiceLigneInsertion, true);
    rafraichir();
  }
  
  /**
   * Créer une ligne d'achat.
   */
  public IdLigneAchat creerLigneAchat(Article pArticle, BigDecimal pQuantite, IdLigneVente pIdLigneVenteRapprochee) {
    return insererArticleDansDocument(pArticle, -1, pQuantite, pIdLigneVenteRapprochee);
  }
  
  /**
   * Créer une ligne d'achat.
   */
  public IdLigneAchat creerLigneAchat(Article pArticle, int pIndexInsertion, BigDecimal pQuantite, IdLigneVente pIdLigneVenteRapprochee) {
    return insererArticleDansDocument(pArticle, pIndexInsertion, pQuantite, pIdLigneVenteRapprochee);
  }
  
  /**
   * Créer une ligne d'achat commentaire.
   */
  public IdLigneAchat creerLigneAchatCommentaire(String pCommentaire, int pNumeroLigne) {
    return insererUneLigneCommentaire(pCommentaire, pNumeroLigne);
  }
  
  /**
   * Modifie la quantité à partir d'un tableau, insère un article dans un document et met à jour sa quantité.
   */
  public IdLigneAchat modifierQuantiteLigneTableau(Article pArticle, String pValeur, int pIndiceLigneInsertion,
      boolean pControlerCommandeEnCours) {
    if (documentAchatEnCours == null) {
      return null;
    }
    
    pValeur = Constantes.normerTexte(pValeur);
    
    // Charger les données complètes de l'article modifié
    pArticle = ManagerServiceArticle.completerArticleStandard(getIdSession(), pArticle);
    
    // Tester si l'article est un commentaire
    BigDecimal quantite = BigDecimal.ZERO;
    String nombreTrouve = "";
    String lettreTrouve = "";
    
    // On controle que le contenu de pValeur corresponde à une saisie valide (chiffre et/ou lettre)
    Matcher m = patternQuantiteRechercheArticles.matcher(pValeur);
    // Analyse des données saisies dans la cellule quantité
    if (m.find()) {
      nombreTrouve = Constantes.normerTexte(m.group(1));
      lettreTrouve = Constantes.normerTexte(m.group(2));
    }
    
    lettreTrouve = lettreTrouve.toLowerCase();
    if (!lettreTrouve.isEmpty()) {
      quantite = BigDecimal.ONE;
    }
    if (!nombreTrouve.isEmpty()) {
      quantite = Constantes.convertirTexteEnBigDecimal(nombreTrouve);
    }
    
    // On stocke dans un tableau les articles pour lesquels on a saisie une quantité
    if (quantite.compareTo(BigDecimal.ZERO) == 0) {
      rafraichir();
      return null;
    }
    
    // Vérification du respect du conditionnement fournisseur (si on passe par la fenêtre détail, ce contrôle est fait après la sortie de
    // la fenêtre car les quantités peuvent avoit changé).
    if (pArticle.isAlerteConditionnementFournisseur() && lettreTrouve.isEmpty()) {
      quantite = controlerRespectConditionnementFournisseur(pArticle, quantite);
    }
    
    // On force l'affichage du détail de la ligne sur l'onglet stock directement :
    // - si l'utilisateur n'a pas déjà demandé à visionner l'onglet stocks
    // - si l'alerte sur stock est active (PS319)
    // - si l'article a réellement du surstock sur l'ensemble des établissements et magasins
    boolean ps319 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS319);
    if (!lettreTrouve.equals("s") && ps319 && isVerifierArticleEnSurStock(pArticle.getId())) {
      lettreTrouve = "s";
    }
    
    // Saisir une quantité par palette
    if (lettreTrouve.equals("p")) {
      return ajouterArticleStandardAvecP(pArticle, quantite, pIndiceLigneInsertion);
    }
    
    // Saisir en autre conditionnement
    if (lettreTrouve.equals("m") || lettreTrouve.equals("v")) {
      return ajouterArticleStandardAvecM(pArticle, quantite, pIndiceLigneInsertion);
    }
    
    IdLigneAchat idLigneAchat = insererArticleDansDocument(pArticle, pIndiceLigneInsertion, quantite, null);
    
    // Ligne où l'on a saisie que les quantités (on n'affiche pas de boite de dialogue mais on supprime les lignes de la table
    // résultat)
    int ongletAActiver = ModeleDetailLigneAchat.NE_PAS_AFFICHER;
    // S'il s'agit d'un article transport
    if (pArticle.isArticleTransport()) {
      // Pour l'instant on ne fait rien
    }
    // S'il s'agit d'un article normal
    else {
      if (lettreTrouve.equals("d") || lettreTrouve.equals("g")) {
        ongletAActiver = ModeleDetailLigneAchat.ONGLET_GENERAL;
      }
      else if (lettreTrouve.equals("s")) {
        ongletAActiver = ModeleDetailLigneAchat.ONGLET_STOCK;
      }
      else if (lettreTrouve.equals("h")) {
        ongletAActiver = ModeleDetailLigneAchat.ONGLET_HISTORIQUE;
      }
      else if (lettreTrouve.equals("n")) {
        ongletAActiver = ModeleDetailLigneAchat.ONGLET_NEGOCIATION;
      }
      else if (lettreTrouve.equals("i")) {
        ongletAActiver = ModeleDetailLigneAchat.ONGLET_INFORMATIONSTECHNIQUES;
      }
    }
    
    // Affiche la boite de dialogue du détail de la ligne
    if (idLigneAchat != null && documentAchatEnCours.getListeLigneAchat().size() > 0) {
      for (LigneAchat ligneAchat : documentAchatEnCours.getListeLigneAchat()) {
        if (ligneAchat.getId().equals(idLigneAchat)) {
          selectionnerLigneArticleStandard(ligneAchat, ongletAActiver, TABLE_RESULTAT_RECHERCHE_ARTICLES, pArticle, true,
              pControlerCommandeEnCours, true);
          return ligneAchat.getId();
        }
      }
    }
    return idLigneAchat;
  }
  
  /**
   * Modifie une ligne achat du document.
   */
  public void modifierLigneArticle(int pIndex) {
    if (pIndex < -1) {
      return;
    }
    if (documentAchatEnCours == null) {
      return;
    }
    
    LigneAchat ligneAchat = documentAchatEnCours.getListeLigneAchat().get(pIndex);
    selectionnerLigneArticleStandard(ligneAchat, ModeleDetailLigneAchat.ONGLET_GENERAL, TABLE_LIGNES_DOCUMENT, null, false, false, true);
  }
  
  /**
   * Supprime une ligne article à partir de son id.
   */
  public void supprimerLignesArticlesAvecId(IdLigneAchat pIdLigneAchat) {
    // Contrôle des null
    if (pIdLigneAchat == null || documentAchatEnCours == null) {
      return;
    }
    List<IdLigneAchat> listeId = new ArrayList<IdLigneAchat>();
    listeId.add(pIdLigneAchat);
    documentAchatEnCours = DocumentAchat.supprimerListeLigneAchat(getIdSession(), documentAchatEnCours, listeId,
        ManagerSessionClient.getInstance().getProfil());
    
    // Contrôle des frais de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
  }
  
  /**
   * Supprime une liste de lignes articles à partir de leur indice.
   */
  public void supprimerLignesArticlesAvecIndice(int[] pListeIndicesLignes) {
    if (documentAchatEnCours == null) {
      return;
    }
    List<IdLigneAchat> listeId = documentAchatEnCours.convertirListeIndiceEnIdLigneArticle(pListeIndicesLignes);
    if (listeId == null) {
      return;
    }
    
    documentAchatEnCours = DocumentAchat.supprimerListeLigneAchat(getIdSession(), documentAchatEnCours, listeId,
        ManagerSessionClient.getInstance().getProfil());
    
    // Contrôle des frais de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
  }
  
  /**
   * Retourne le montant qu'il manque pour atteindre le montant franco de port.
   */
  public BigDecimal getMontantManquantPourAtteindreFrancoPort() {
    if (documentAchatEnCours == null) {
      return null;
    }
    return documentAchatEnCours.getMontantManquantPourAtteindreFrancoPort(fournisseurCourant.getMontantFrancoPort());
  }
  
  /**
   * Retourne le montant qu'il manque pour atteindre le montant minimum de commande.
   */
  public BigDecimal getMontantManquantPourAtteindreMontantMinimumCommande() {
    if (documentAchatEnCours == null) {
      return null;
    }
    return documentAchatEnCours.getMontantManquantPourAtteindreMontantMinimumCommande(fournisseurCourant.getMinimumCommande());
  }
  
  /**
   * Indique si on facture ou non les frais de port.
   */
  public void modifierFraisPortFacture(boolean pFacturerFraisPort) {
    if (documentAchatEnCours != null && documentAchatEnCours.isFraisPortFournisseurFacture() == pFacturerFraisPort) {
      return;
    }
    documentAchatEnCours.setFraisPortFournisseurFacture(pFacturerFraisPort);
    documentAchatEnCours =
        DocumentAchat.chiffrerDocument(getIdSession(), documentAchatEnCours, ManagerSessionClient.getInstance().getProfil());
    rafraichir();
  }
  
  /**
   * Détermine l'affichage du composant qui permet de facturer ou non les frais de port.
   */
  public boolean isAfficherChoixFrancoPort() {
    if (documentAchatEnCours == null || fournisseurCourant == null) {
      return false;
    }
    
    if (documentAchatEnCours.isModeEnlevement() || !fournisseurCourant.isFactureFraisPort()
        || documentAchatEnCours.getMontantPortFacture() == null
        || documentAchatEnCours.getMontantPortFacture().compareTo(BigDecimal.ZERO) <= 0) {
      return false;
    }
    
    return true;
  }
  
  /**
   * Détermine si les frais de port facturé du document sont valides.
   */
  public boolean isFraisPortFactureValide() {
    if (documentAchatEnCours == null) {
      return false;
    }
    
    return documentAchatEnCours.getMontantPortFacture() != null
        && documentAchatEnCours.getMontantPortFacture().compareTo(BigDecimal.ZERO) >= 0;
  }
  
  /**
   * Déterminse si les frais de port théorique du document sont valides.
   */
  public boolean isFraisPortTheoriqueValide() {
    if (documentAchatEnCours == null) {
      return false;
    }
    
    return documentAchatEnCours.getMontantPortTheorique() != null
        && documentAchatEnCours.getMontantPortTheorique().compareTo(BigDecimal.ZERO) > 0;
  }
  
  /**
   * Contrôle que le montant de la commande est atteint le montant mini de commande du fournisseur.
   */
  public void controlerMinimumCommandeFournisseurAtteint() {
    if (!isMinimumCommandeFournisseurAtteint()) {
      throw new MessageErreurException(
          "Le montant minimum de commande du fournisseur n'a pas été atteint. Vous devez augmenter le montant votre commande afin de "
              + "pouvoir valider votre commande.");
    }
  }
  
  /**
   * Contrôle que le montant de la commande est atteint le montant mini de commande du fournisseur.
   */
  public boolean isMinimumCommandeFournisseurAtteint() {
    // Contrôle des null
    if (documentAchatEnCours == null) {
      throw new MessageErreurException("Le document d'achat est invalide.");
    }
    
    Fournisseur fournisseur = fournisseurCourant;
    BigDecimal montantMiniCommande = fournisseur.getMinimumCommande();
    // Si le montant mini est null ou égal à zéro alors c'est qu'il n'a pas été précisé dans la fiche fourniseur
    if (montantMiniCommande == null || montantMiniCommande.compareTo(BigDecimal.ZERO) == 0) {
      return true;
    }
    BigDecimal montantManquant = documentAchatEnCours.getMontantManquantPourAtteindreMontantMinimumCommande(montantMiniCommande);
    if (montantManquant.compareTo(BigDecimal.ZERO) != 0) {
      return false;
    }
    return true;
  }
  
  /**
   * Passer en mode recherche des articles palettes.
   */
  public void activerModeRechercheArticlePalette() {
    viderListeResultatRechercheArticles();
    texteRecherche = "";
    etape = ETAPE_RECHERCHE_ARTICLE_PALETTE;
    composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
    rafraichir();
  }
  
  public BigDecimal getNombreDePalettesLies() {
    return nombreDePalettesLies;
  }
  
  /**
   * Passer en mode recherche des articles commentaires.
   */
  public void activerModeRechercheArticleCommentaire() {
    viderListeResultatRechercheArticles();
    texteRecherche = "";
    etape = ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE;
    composantAyantLeFocus = FOCUS_ARTICLES_RECHERCHE_ARTICLE;
    rafraichir();
  }
  
  /**
   * Saisir un nouveau commentaire qui sera ajouté au document.
   * Ce commentaire pourra éventuellement être sauvegardé sous forme d'article commentaire pour une utilisation ultérieure.
   */
  public void creerLigneCommentaire(int pLigneInsertion) {
    // Récupérer le code établissement
    IdEtablissement idEtablissement = etablissement.getId();
    
    // Lancer la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modeleCommentaire =
        ModeleDetailCommentaire.getInstanceCreationCommentaireLibre(getSession(), idEtablissement, EnumContexteMetier.CONTEXTE_ACHAT);
    VueDetailCommentaire vue = new VueDetailCommentaire(modeleCommentaire);
    vue.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    if (modeleCommentaire.isSortieAvecAnnulation()) {
      return;
    }
    else if (modeleCommentaire.isSortieAvecValidation()) {
      if (modeleCommentaire.getTexteCommentaire() != null && !modeleCommentaire.getTexteCommentaire().trim().isEmpty()) {
        // Le commentaire ne peut pas faire plus de 120 caractères. On fait une table pour découper le texte s'il est plus long.
        String[] texteEnTable = modeleCommentaire.getTexteCommentaire().trim().split("(?<=\\G.{120})|\\n");
        // On insère une ligne de commentaire par groupe de 120 caractères.
        for (int i = 0; i < texteEnTable.length; i++) {
          insererUneLigneCommentaire(texteEnTable[i], pLigneInsertion + i);
        }
      }
      
      // Recharger le document
      if (documentAchatEnCours != null && documentAchatEnCours.getId().getNumero() > 0) {
        // Rechargement les lignes et l'entête du document
        documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
      }
    }
    
    // Cas particulier, si on ajoute un commentaire et que c'est la recherche commentaire qui est en cours, on relance la recherche
    // afin d'afficher l'éventuel article commentaire ajouté.
    if (etape == ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      rechercherArticleCommentaire();
    }
    
    // Ne pas modifier l'étape pour laisser le comptoir dans l'état ou il était avant ajout de l'article commentaire
    rafraichir();
  }
  
  // --- Onglet édition
  /**
   * Effectue les controles et traitement lors de la sortie de l'onglet Edition.
   */
  public void controlerOngletEdition() {
    // Controle des données de l'onglet
    for (ModeEdition modeEdition : listeModeEditionAchat) {
      // Dans le cas où l'envoi par mail a été sélectionné
      if (EnumModeEdition.ENVOYER_PAR_MAIL.equals(modeEdition.getEnumModeEdition()) && modeEdition.isSelectionne()) {
        // Controle de la validité de l'adresse email du destinataire
        if (!Constantes.controlerEmail(emailDestinataireEdition)) {
          throw new MessageErreurException("L'adresse email du destinataire n'est pas valide.");
        }
        break;
      }
      // Dans le cas où l'envoi par fax a été sélectionné
      if (EnumModeEdition.ENVOYER_PAR_FAX.equals(modeEdition.getEnumModeEdition()) && modeEdition.isSelectionne()) {
        // Controle de la validité du numéro de fax du destinataire
        if (faxDestinataireEdition == null || faxDestinataireEdition.trim().isEmpty()) {
          throw new MessageErreurException("Le numéro de fax du destinataire n'est pas valide.");
        }
        break;
      }
    }
  }
  
  /**
   * Modifie le type d'affichage du chiffrage pour l'édition.
   */
  public void modifierOptionChiffrageEdition(EnumOptionEdition pOptionChiffrageEdition) {
    if (Constantes.equals(optionChiffrageEdition, pOptionChiffrageEdition)) {
      return;
    }
    optionChiffrageEdition = pOptionChiffrageEdition;
    rafraichir();
  }
  
  /**
   * Modifie l'email du destinataire pour l'édition.
   */
  public void modifierEmailDestinataire(Contact pContact, boolean pRafraichirPanel) {
    if (pContact == null && Constantes.equals(contactMail, pContact)) {
      return;
    }
    contactMail = pContact;
    String email = "";
    // Contrôle du premier email
    if (contactMail.getEmail() != null) {
      email = Constantes.normerTexte(contactMail.getEmail());
    }
    // Si le premier email est vide, contrôle du deuxième
    if (email.isEmpty()) {
      if (contactMail.getEmail2() != null) {
        email = Constantes.normerTexte(contactMail.getEmail2());
      }
    }
    if (email.equals(emailDestinataireEdition)) {
      return;
    }
    emailDestinataireEdition = email;
    
    if (pRafraichirPanel) {
      // Activation de la selection ou non du mode d'édition par mail
      ModeEdition modeEdition = listeModeEditionAchat.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_MAIL);
      if (emailDestinataireEdition.isEmpty()) {
        modeEdition.setSelectionne(false);
      }
      else {
        modeEdition.setSelectionne(true);
      }
      rafraichir();
    }
  }
  
  /**
   * Modifie l'email du destinataire pour l'édition.
   */
  public void modifierEmailDestinataire(String pEmail) {
    pEmail = Constantes.normerTexte(pEmail);
    if (pEmail.equals(emailDestinataireEdition)) {
      return;
    }
    emailDestinataireEdition = pEmail;
    contactMail = null;
    rafraichir();
  }
  
  /**
   * Modifie le fax du destinataire pour l'édition.
   */
  public void modifierFaxDestinataire(Contact pContact, boolean pRafraichirPanel) {
    if (pContact == null && Constantes.equals(contactFax, pContact)) {
      return;
    }
    contactFax = pContact;
    String numeroFax = "";
    if (contactFax.getNumeroFax() != null) {
      numeroFax = Constantes.normerTexte(contactFax.getNumeroFax());
    }
    // En désespoir de cause, initialisation avec le fax du siège
    if (numeroFax == null || numeroFax.isEmpty()) {
      if (fournisseurCourant != null && fournisseurCourant.getAdresseSiege() != null
          && fournisseurCourant.getAdresseSiege().getNumeroFax() != null) {
        numeroFax = fournisseurCourant.getAdresseSiege().getNumeroFax();
      }
    }
    if (Constantes.equals(numeroFax, faxDestinataireEdition)) {
      return;
    }
    
    faxDestinataireEdition = numeroFax;
    
    if (pRafraichirPanel) {
      // Activation de la selection ou non du mode d'édition par fax
      ModeEdition modeEdition = listeModeEditionAchat.getModeEditionAPartirEnum(EnumModeEdition.ENVOYER_PAR_FAX);
      if (faxDestinataireEdition.isEmpty()) {
        modeEdition.setSelectionne(false);
      }
      else {
        modeEdition.setSelectionne(true);
      }
      rafraichir();
    }
  }
  
  /**
   * Modifie le fax du destinataire pour l'édition.
   */
  public void modifierFaxDestinataire(String pNumeroFax) {
    pNumeroFax = Constantes.normerTexte(pNumeroFax);
    if (pNumeroFax.equals(faxDestinataireEdition)) {
      return;
    }
    faxDestinataireEdition = pNumeroFax;
    contactFax = null;
    rafraichir();
  }
  
  /**
   * Valide l'édition du document.
   */
  public void validerEdition() {
    // Conrôle des null
    if (documentAchatEnCours == null) {
      return;
    }
    controlerOngletEdition();
    
    // On enregistre le document dans la base
    documentAchatEnCours = ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        documentAchatEnCours);
    
    // Validation de la commande
    ManagerServiceDocumentAchat.validerDocument(getSession().getIdSession(), documentAchatEnCours.getId(), dateTraitement,
        optionChiffrageEdition);
    
    // Lancement de l'édition en tâche de fond
    EditionDocument editiondocument = new EditionDocument(getSession(), documentAchatEnCours, dateTraitement, listeModeEditionAchat,
        optionChiffrageEdition, listeAcheteur);
    editiondocument.setEmailDestinataire(getEmailDestinataireEdition());
    editiondocument.setNumeroFaxDestinataire(getFaxDestinataireEdition());
    SwingWorkerAttente.executer(((SessionJava) getSession()).getFenetre(), editiondocument, "Edition en cours ...");
    
    // Remise à zéro des variables
    effacerVariables();
    documentAchatEnCours = null;
    effacerVariableOngletFournisseur();
    
    // Controle si la session doit se terminer après l'édition
    if (isFermetureAutomatique()) {
      fermerEcran();
    }
    // Ou si elle est réinitialisée pour une nouvelle saisie
    else {
      // On active l'onglet suivant
      etape = ETAPE_RECHERCHE_FOURNISSEUR;
      modifierIndiceOnglet(EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex());
      rafraichir();
    }
  }
  
  /**
   * Modifie la sélection d'un mode d'édition.
   */
  public void modifierModeEdition(EnumModeEdition pEnumModeEdition, boolean pSelectionne) {
    ModeEdition modeEdition = listeModeEditionAchat.getModeEditionAPartirEnum(pEnumModeEdition);
    if (modeEdition == null || modeEdition.isSelectionne() == pSelectionne) {
      return;
    }
    modeEdition.setSelectionne(pSelectionne);
    rafraichir();
  }
  
  /**
   * Email du destinataire du mail avec le document en pièce jointe.
   */
  public String getEmailDestinataireEdition() {
    if (emailDestinataireEdition == null) {
      return "";
    }
    return emailDestinataireEdition;
  }
  
  /**
   * Numéro de fax du destinataire.
   */
  public String getFaxDestinataireEdition() {
    if (faxDestinataireEdition == null) {
      return "";
    }
    return faxDestinataireEdition;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les variables de navigation.
   */
  private void effacerVariablesNavigation() {
    // setCleVueEnfantActive(ONGLET_FOURNISSEUR);
    indiceOngletCourant = EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex();
    indiceOngletAccessible = EnumCleVueAchat.ONGLET_FOURNISSEUR.getIndex();
    
    messageErreur = "";
  }
  
  /**
   * Initialise les variables du document.
   */
  private void effacerVariablesDocument() {
    maintenant = new Date();
    viderListeResultatRechercheArticles();
    texteRecherche = null;
    message = null;
    
    // Permet d'effacer les variables de l'onglet édition pour chaque document
    listeModeEditionAchat.reinitialiserValeurParDefaut();
    contactMail = null;
    contactFax = null;
    emailDestinataireEdition = null;
    faxDestinataireEdition = null;
    optionChiffrageEdition = null;
  }
  
  /**
   * Créer le document.
   */
  private void creerDocument(EnumTypeCommande pEnumTypeCommande, boolean pSauvegarde) {
    if (magasin == null) {
      throw new MessageErreurException("Impossible de créer une nouvelle commande car le magasin n'est pas défini.");
    }
    
    // Effacement des variables globales
    effacerVariablesNavigation();
    effacerVariablesDocument();
    
    // Initialisation du nouveau document
    IdDocumentAchat idDocument =
        IdDocumentAchat.getInstanceAvecCreationId(etablissement.getId(), EnumCodeEnteteDocumentAchat.COMMANDE_OU_RECEPTION);
    documentAchatEnCours = new DocumentAchat(idDocument);
    documentAchatEnCours.setModeEnlevement(fournisseurCourant.isModeEnlevement());
    documentAchatEnCours.setIdMagasinSortie(magasin.getId());
    documentAchatEnCours.setIdAcheteur(utilisateurGescom.getIdAcheteur());
    documentAchatEnCours.setIdFournisseur(fournisseurCourant.getId());
    documentAchatEnCours.setDateCreation(maintenant);
    if (dateTraitement == null) {
      dateTraitement = etablissement.getDateTraitement();
    }
    if (dateTraitement != null) {
      documentAchatEnCours.setDateDernierTraitement(dateTraitement);
    }
    
    documentAchatEnCours.setAdresseFournisseur(fournisseurCourant.getAdresseFournisseur(idAdresseFournisseurCourant).getAdresse());
    // On initialise la variable d'application du port à vrai à la création si le fournisseur facture les frais de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
    // On initialise le type de la commnande (Normal, Directe usine, Interne)
    if (pEnumTypeCommande != null) {
      documentAchatEnCours.setCodeTypeCommande(pEnumTypeCommande);
    }
    
    if (!pSauvegarde) {
      return;
    }
    
    // Sauvegarde du document dans la base
    documentAchatEnCours = ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        documentAchatEnCours);
  }
  
  /**
   * Modifie un document dont on a l'id.
   */
  private void modifierDocument(IdDocumentAchat pIdDocumentAchat) {
    // Effacement des variables globales
    effacerVariablesDocument();
    
    // Lecture de toutes les informations sur le document
    documentAchatEnCours = new DocumentAchat(pIdDocumentAchat);
    documentAchatEnCours.setEtape(DocumentAchat.ETAPE_MODIFICATION_DOCUMENT);
    documentAchatEnCours.setDateDernierTraitement(dateTraitement);
    
    // On charge l'entête et les lignes du document
    documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
  }
  
  // --- Onglet fournisseur
  
  /**
   * Retourne les informations gescom de l'utilisateur.
   */
  private UtilisateurGescom chargerInformationsGescom(IdEtablissement pIdEtablissement) {
    UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
        ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    return utilisateurGescom;
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   * Cette méthode ne doit pas appeler rafraichir() car elle est appelée lors du chargement initial des données.
   */
  private void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return;
    }
    if (utilisateurGescom == null) {
      return;
    }
    
    // Charger la liste des acheteurs
    listeAcheteur = new ListeAcheteur();
    listeAcheteur = listeAcheteur.charger(getIdSession(), pIdEtablissement);
    if (listeAcheteur == null || listeAcheteur.isEmpty()) {
      effacerVariables();
      throw new MessageErreurException("Il n'est pas possible d'utiliser les achats car aucun acheteur n'est configuré. "
          + "Aller dans les personnalisation de type AH pour configurer la liste des acheteurs.");
    }
    
    // Vérifier l'acheteur de l'utilisateur
    IdAcheteur idAcheteur = utilisateurGescom.getIdAcheteur();
    if (idAcheteur == null) {
      effacerVariables();
      throw new MessageErreurException(
          "Il n'est pas possible d'utiliser les achats car l'utilisateur n'est pas associé à un code acheteur. "
              + "Aller dans le menu gestion des sécurités pour configurer l'utilisateur.");
    }
    else if (!listeAcheteur.isPresent(idAcheteur)) {
      effacerVariables();
      throw new MessageErreurException(
          String.format("Il n'est pas possible d'utiliser les achats car le code acheteur associé à l'utilisateur n'existe pas. "
              + "Aller dans la gestion des sécurités pour configurer correctement l'utilisateur ou dans les paramètres de Série N "
              + "afin de créer le code acheteur %s.", idAcheteur.getCode()));
    }
    
    // Charger la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), pIdEtablissement);
    if (listeMagasin == null || listeMagasin.isEmpty()) {
      throw new MessageErreurException("Il n'est pas possible d'utiliser les achats car aucun aucun magasin n'est disponible. "
          + "Vérifier vos droits sur cet établissement.");
    }
    
    // Vérifier le magasin de l'utilisateur
    IdMagasin idMagasin = utilisateurGescom.getIdMagasin();
    if (idMagasin == null) {
      throw new MessageErreurException("Il n'est pas possible d'utiliser les achats car l'utilisateur n'est pas associé à un magasin. "
          + "Aller dans la gestion des sécurités pour configurer l'utilisateur.");
    }
    
    magasin = listeMagasin.getMagasinParId(idMagasin);
    if (magasin == null) {
      throw new MessageErreurException(
          String.format("Il n'est pas possible d'utiliser les achats car le magasin associé à l'utilisateur n'existe pas. "
              + "Aller dans le menu gestion des sécurités pour configurer correctement l'utilisateur ou dans les paramètres de Série N "
              + "afin de créer le code magasin %s.", idMagasin.getCode()));
    }
    
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
  }
  
  /**
   * Chargement des données du fournisseur choisi.
   */
  private void chargerFournisseur() {
    // Effacer les données liées au fournisseur
    listeDocumentAchat = null;
    
    // Vérifier les pré-requis
    if (idAdresseFournisseurCourant == null) {
      throw new MessageErreurException("Les coordonnées du fournisseur sélectionné ne sont pas valides.");
    }
    
    // Charger toutes les données du fournisseur
    IdFournisseur idFournisseur = idAdresseFournisseurCourant.getIdFournisseur();
    fournisseurCourant = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), idFournisseur);
    if (fournisseurCourant == null) {
      throw new MessageErreurException("Impossible de charger le fournisseur.");
    }
    fournisseurCourant.chargerListeContact(getIdSession());
    
    // Charger les documents d'achats du fournisseur
    CritereDocumentAchat critereDocumentAchat = new CritereDocumentAchat();
    critereDocumentAchat.setNumeroPage(1);
    critereDocumentAchat.initialiserNombrePagesMax();
    critereDocumentAchat.setIdEtablissement(etablissement.getId());
    critereDocumentAchat.setIdFournisseur(fournisseurCourant.getId());
    if (magasin != null) {
      critereDocumentAchat.setIdMagasin(magasin.getId());
    }
    critereDocumentAchat.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
    critereDocumentAchat.ajouterCodeEtat(EnumCodeEtatDocument.ATTENTE);
    critereDocumentAchat.ajouterCodeEtat(EnumCodeEtatDocument.VALIDE);
    List<IdDocumentAchat> listeIdDocumentAchat =
        ManagerServiceDocumentAchat.chargerListeIdDocumentAchat(getIdSession(), critereDocumentAchat);
    
    // Créer la liste de document d'achats (sans données)
    listeDocumentAchat = ListeDocumentAchat.creerListeNonChargee(listeIdDocumentAchat);
    
    // Charger la première page
    ListeDocumentAchat listeDocumentAchatCharge = listeDocumentAchat.chargerPremierePage(getIdSession(), 0);
    
    // Compléter le chargement des documents d'achats (pour l'adresse fournisseur)
    if (listeDocumentAchatCharge != null) {
      listeDocumentAchat = ManagerServiceDocumentAchat.chargerListeDocumentAchat(getIdSession(), listeIdDocumentAchat);
    }
  }
  
  // -- Onglet article
  
  // -- Méthodes privées
  
  /**
   * Contrôle que l'on respecte le conditionnement du fournisseur et corrige éventuellement la quantité commandée
   */
  private BigDecimal controlerRespectConditionnementFournisseur(Article pArticle, BigDecimal pQuantite) {
    // Si on n'a pas de quantité on renvoit 0... pas con
    if (pQuantite == null || pQuantite.compareTo(BigDecimal.ZERO) == 0) {
      return BigDecimal.ZERO;
    }
    // Si on n'a pas de conditionnement fournisseur on renvoit la quantité saisie
    if (pArticle.getQuantiteConditionnementFournisseurUS() == null
        || pArticle.getQuantiteConditionnementFournisseurUS().compareTo(BigDecimal.ZERO) == 0) {
      return pQuantite;
    }
    // On calcule le conditionnement fournisseur en UCA
    BigDecimal conditionnementFournisseurUCA =
        pArticle.getQuantiteConditionnementFournisseurUS().multiply(pArticle.getPrixAchat().getNombreUSParUCA());
    // On calcule si la quantité achetée est parfaitement divisible par le conditionnement fournisseur (reste == 0)
    BigDecimal[] resultat = pQuantite.divideAndRemainder(conditionnementFournisseurUCA);
    if (resultat[1].compareTo(BigDecimal.ZERO) == 0) {
      return pQuantite;
    }
    // Si la quantité ne correspond pas à un multiple du conditionnement on demande à l'utilisateur de confirmer
    else {
      ModeleControleConditionnementFournisseur modeleControle =
          new ModeleControleConditionnementFournisseur(getSession(), pArticle, pQuantite);
      VueControleConditionnementFournisseur vueControle = new VueControleConditionnementFournisseur(modeleControle);
      vueControle.afficher();
      return modeleControle.getQuantiteChoisie();
    }
  }
  
  /**
   * On vérifie si l'article a du surstock.
   */
  private boolean isVerifierArticleEnSurStock(IdArticle pIdArticle) {
    if (pIdArticle == null) {
      throw new MessageErreurException("L'id de l'article est incorrect.");
    }
    // On récupère toutes les stocks de l'article dans tous les établissements et tous les magasins
    ListeStockComptoir listeStock = ListeStockComptoir.chargerPourArticle(getIdSession(), null, pIdArticle.getCodeArticle());
    if (listeStock != null && listeStock.isArticleEnSurStock()) {
      return true;
    }
    return false;
  }
  
  /**
   * Insèrer un article saisis dans la liste des articles du document.
   */
  private IdLigneAchat insererArticleDansDocument(Article pArticle, int pIndiceTableau, BigDecimal pQuantite,
      IdLigneVente pIdLigneVenteRapprochee) {
    if (documentAchatEnCours == null) {
      return null;
    }
    
    message = null;
    int numeroLigne = documentAchatEnCours.calculerNumeroLigneArticle(pIndiceTableau);
    
    // Insertion dans le document
    IdLigneAchat idLigneAchat =
        insererUneLigne(ManagerSessionClient.getInstance().getProfil(), pArticle, numeroLigne, pQuantite, pIdLigneVenteRapprochee);
    if (idLigneAchat == null) {
      return null;
    }
    
    // Chargement des lignes et de l'entête du document
    documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
    
    // Contrôle la facturation des frais de port
    documentAchatEnCours = DocumentAchat.controlerFacturationFraisPort(getIdSession(), documentAchatEnCours, fournisseurCourant,
        ManagerSessionClient.getInstance().getProfil());
    rafraichir();
    return idLigneAchat;
  }
  
  /**
   * Insère un article dans un document.
   */
  private IdLigneAchat insererUneLigne(String pUtilisateur, Article pArticle, int pNumeroLigne, BigDecimal pQuantite,
      IdLigneVente pIdLigneVenteRapprochee) {
    if (documentAchatEnCours == null) {
      throw new MessageErreurException("Le document d'achat est invalide");
    }
    if (pArticle.isArticleCommentaire()) {
      throw new MessageErreurException("Impossible d'insérer un article commentaire.");
    }
    // Controle l'existence du document
    if (!documentAchatEnCours.isExistant()) {
      documentAchatEnCours = ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(),
          ManagerSessionClient.getInstance().getProfil(), documentAchatEnCours);
      if (documentAchatEnCours == null) {
        return null;
      }
    }
    
    // Initialisation de la ligne article et on vérifie si on l'ajoute à un regroupement
    LigneAchat ligneAchat = documentAchatEnCours.convertirArticleEnLigne(pArticle, pQuantite, pNumeroLigne);
    
    // Initialisation de la ligne
    if (ligneAchat instanceof LigneAchatArticle) {
      // Information nécessaire lors de l'initialisation de la ligne
      ((LigneAchatArticle) ligneAchat).setIdLigneVente(pIdLigneVenteRapprochee);
    }
    ligneAchat = ManagerServiceDocumentAchat.initialiserLigneDocument(getIdSession(), ligneAchat, dateTraitement);
    // LA quantité est renseignée après pour ne pas être écrasée lors de l'initialisation
    if (ligneAchat instanceof LigneAchatArticle) {
      // Le recalcul va être définit si le prix calculé doit être forcé ou non (cas des prix dérogés par exemple)
      boolean forcerRecalcul = true;
      if (((LigneAchatArticle) ligneAchat).getPrixAchat().isForcerPrixCalcule()) {
        forcerRecalcul = false;
      }
      ((LigneAchatArticle) ligneAchat).getPrixAchat().setQuantiteReliquatUCA(pQuantite, forcerRecalcul);
    }
    
    // Si on est dans une commande direct usine le type de la ligne doit être ligne spéciale non stockée (code ERL == 'S')
    if (documentAchatEnCours.isDirectUsine() && !ligneAchat.isLigneCommentaire()) {
      ligneAchat.setTypeLigne(EnumTypeLigneAchat.SPECIALE_NON_STOCKEE);
    }
    
    // Controle des données saisies
    ligneAchat = ManagerServiceDocumentAchat.controlerLigneDocument(getIdSession(), ligneAchat, dateTraitement);
    
    // Création de la ligne
    ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), ligneAchat, dateTraitement);
    
    return ligneAchat.getId();
  }
  
  /**
   * Insère un article dans un document.
   */
  private IdLigneAchat insererUneLigneCommentaire(String pCommentaire, int pIndiceTableau) {
    if (documentAchatEnCours == null) {
      return null;
    }
    
    if (pCommentaire == null) {
      pCommentaire = "";
    }
    // Calcule le numéro de ligne si besoin
    int numeroLigne = documentAchatEnCours.calculerNumeroLigneArticle(pIndiceTableau);
    
    // Controle l'existence du document
    if (!documentAchatEnCours.isExistant()) {
      documentAchatEnCours = ManagerServiceDocumentAchat.sauverDocumentAchat(getIdSession(),
          ManagerSessionClient.getInstance().getProfil(), documentAchatEnCours);
      if (documentAchatEnCours == null) {
        return null;
      }
    }
    
    IdLigneAchat idLigneAchat = IdLigneAchat.getInstance(documentAchatEnCours.getId(), numeroLigne);
    // Initialisation de la ligne article et on vérifie si on l'ajoute à un regroupement
    LigneAchatCommentaire ligneAchatCommentaire = new LigneAchatCommentaire(idLigneAchat);
    ligneAchatCommentaire.setTexte(pCommentaire);
    
    // Création de la ligne
    ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), ligneAchatCommentaire, dateTraitement);
    
    return ligneAchatCommentaire.getId();
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne article normale.
   */
  private String[] formaterLigneArticle(LigneAchatArticle pLigneAchat, int pNombreColonnes, boolean pModeCreation) {
    PrixAchat prixAchat = pLigneAchat.getPrixAchat();
    if (prixAchat == null) {
      throw new MessageErreurException(
          "Le prix d'achat n'est pas défini pour cette ligne article " + pLigneAchat.getId().getNumeroLigne());
    }
    
    String ligne[] = new String[pNombreColonnes];
    int indice = 0;
    ligne[indice++] = Constantes.formater(prixAchat.getQuantiteInitialeUCA(), false);
    if (!pModeCreation) {
      ligne[indice++] = Constantes.formater(prixAchat.getQuantiteReliquatUCA(), false);
      ligne[indice++] = Constantes.formater(prixAchat.getQuantiteTraiteeUCA(), false);
    }
    if (prixAchat.getIdUCA() != null) {
      ligne[indice++] = prixAchat.getIdUCA().getCode();
    }
    else {
      ligne[indice++] = "";
    }
    ligne[indice++] = pLigneAchat.getLibelleArticle();
    ligne[indice++] = Constantes.formater(prixAchat.getQuantiteInitialeUA(), false);
    
    if (prixAchat.getIdUCA() != null) {
      ligne[indice++] = prixAchat.getIdUA().getCode();
    }
    else {
      ligne[indice++] = "";
    }
    ligne[indice++] = Constantes.formater(prixAchat.getPrixAchatNetHT(), true);
    ligne[indice++] = Constantes.formater(prixAchat.getMontantInitialHT(), true);
    
    return ligne;
  }
  
  /**
   * Formate pour l'affichage dans la liste un tableau correspondant à une ligne commentaire.
   */
  private String[] formaterLigneCommentaire(LigneAchatCommentaire pLigneAchat, int pNombreColonnes, boolean pModeCreation) {
    if (!pLigneAchat.isLigneCommentaire()) {
      throw new MessageErreurException(
          "La ligne d'achat n'est pas une ligne commentaire, ligne numéro:" + pLigneAchat.getId().getNumeroLigne());
    }
    
    String ligne[] = new String[pNombreColonnes];
    // Initialialisation de toutes les colonnes afin qu'elles soient non nulles
    for (int i = 0; i < pNombreColonnes; i++) {
      ligne[i] = "";
    }
    // Renseigne le libellé dans la bonne colonne
    if (pModeCreation) {
      ligne[2] = pLigneAchat.getTexte();
    }
    else {
      ligne[4] = pLigneAchat.getTexte();
    }
    
    return ligne;
  }
  
  /**
   * Efface le contenu du résultat de la recherche articles.
   * Cela efface le contenu du tableau et les données correspondantes : l'index de la première ligne affichée, l'index de la ligne
   * sélectionnée le message d'erreur correspondant à la dernière recherche. Par contre, cela ne doit pas effacer le texte recherché
   * car celui-ci n'est pas lié à la dernière recherche.
   */
  private void viderListeResultatRechercheArticles() {
    listeResultatRechercheArticles = null;
    indexPremiereLigneAffichee = 0;
    indexRechercheArticle = -1;
    message = null;
  }
  
  /**
   * CHarger les données des articles comprise entre deux lignes.
   * Les informations des articles sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageArticles(int pPremiereLigne, int pNombreLigne) {
    final String NOM_METHODE = "chargerPlageArticles";
    
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeResultatRechercheArticles.size());
    
    // Lister les id des articles dont il faut charger les informations
    List<IdArticle> listeIdArticles = new ArrayList<IdArticle>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      Article article = listeResultatRechercheArticles.get(i);
      if (!article.isArticleCharge()) {
        listeIdArticles.add(article.getId());
      }
    }
    
    // Quitter immédiatement si aucun article n'est à charger
    if (listeIdArticles.isEmpty()) {
      return;
    }
    
    // Renseigner les paramètres nécessaires pour charger les informations des articles
    ParametresLireArticle parametres = new ParametresLireArticle();
    parametres.setIdEtablissement(etablissement.getId());
    parametres.setIdMagasin(magasin.getId());
    parametres.setIdFournisseur(fournisseurCourant.getId());
    
    // Charger les informations des articles
    ListeArticle listeArticles = ManagerServiceArticle.chargerListeInformationAchat(getIdSession(), listeIdArticles, parametres);
    
    // Mettre les articles chargés au bon endroit dans la liste de résultat
    for (int i = pPremiereLigne; i < iMax; i++) {
      Article article = listeResultatRechercheArticles.get(i);
      if (!article.isArticleCharge()) {
        for (Article articleCharge : listeArticles) {
          if (article.getId().equals(articleCharge.getId())) {
            listeResultatRechercheArticles.set(i, articleCharge);
            Trace.debug(ModeleAchat.class, NOM_METHODE, "Charger article", "ligne", i, "idarticle", articleCharge.getId());
            break;
          }
        }
      }
    }
    
  }
  
  /**
   * Recherche les articles liés à un autre article.
   */
  private void rechercherArticlesLies(Article pArticleParent, IdLigneAchat pIdLigneAchat) {
    IdFournisseur idFournisseur = fournisseurCourant.getId();
    // Articles liés proposés
    CritereArticleLie criteres = new CritereArticleLie(pArticleParent.getId(), idFournisseur);
    criteres.setFiltreTousFournisseur(filtreTousFournisseur);
    
    boolean possedeArticlesLiesProposes = ManagerServiceArticle.verifierExistenceArticleLiePropose(getIdSession(), criteres);
    if (!possedeArticlesLiesProposes) {
      return;
    }
    
    // Retourner la liste des articles liés proposés (uniquement) à un article donné
    ListeArticle listeArticlesLies = ManagerServiceArticle.chargerListeArticleLieProposes(getIdSession(), criteres);
    if (listeArticlesLies == null || listeArticlesLies.size() == 0) {
      return;
    }
    
    // Lister les id des articles dont il faut charger les informations
    List<IdArticle> listeIdArticles = new ArrayList<IdArticle>();
    for (Article article : listeArticlesLies) {
      listeIdArticles.add(article.getId());
    }
    
    // Renseigner les paramètres nécessaires pour charger les informations des articles
    ParametresLireArticle parametres = new ParametresLireArticle();
    parametres.setIdEtablissement(etablissement.getId());
    parametres.setIdMagasin(magasin.getId());
    parametres.setIdFournisseur(idFournisseur);
    
    // Charger les informations des articles
    ListeArticle listeArticles = ManagerServiceArticle.chargerListeInformationAchat(getIdSession(), listeIdArticles, parametres);
    // Mémoriser le résultat
    listeResultatRechercheArticles = new ListeArticle();
    if (listeArticles != null) {
      for (ArticleBase articleBase : listeArticles) {
        Article article = (Article) articleBase;
        article.setLibelleComplet(articleBase.getLibelleComplet());
        listeResultatRechercheArticles.add(article);
      }
    }
    
    message = Message.getMessageNormal("Articles liés proposés pour " + pArticleParent.getLibelleComplet());
    composantAyantLeFocus = ETAPE_RECHERCHE_ARTICLE_STANDARD;
  }
  
  /**
   * Affiche si nécessaire la boite de dialogue d'alerte contenant la liste des commandes en cours avec l'article en cours de saisie.
   */
  private boolean controlerArticleEnCoursCommande(Article pArticle) {
    if (documentAchatEnCours == null) {
      return false;
    }
    
    if (pArticle == null) {
      throw new MessageErreurException("L'article pour le contrôle des commandes en cours est invalide.");
    }
    
    // Vérifie si l'on doit déclencher une alerte lors de l'insertion d'un article dans le bon de commande, si l'article est déjà en cours
    // de commande.
    boolean ps318 = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS318);
    if (!ps318) {
      return true;
    }
    
    // Lance la recherche des commandes en cours contenant l'article
    List<IdLigneAchat> liste =
        Article.getListeIdLigneAchatContenantArticleEnCoursCommande(getIdSession(), pArticle.getId(), fournisseurCourant.getId());
    if (liste == null || liste.isEmpty()) {
      return true;
    }
    
    // Elimination des lignes correspondant au document en cours
    for (int i = 0; i < liste.size(); i++) {
      IdLigneAchat ligne = liste.get(i);
      if (ligne.getIdDocumentAchat().equals(documentAchatEnCours.getId())) {
        liste.remove(ligne);
      }
    }
    
    // Affichage la boite de dialogue
    if (liste.size() > 0) {
      ModeleCommandeEnCours modele = new ModeleCommandeEnCours(getSession(), this, pArticle, liste, fournisseurCourant);
      VueCommandeEnCours vue = new VueCommandeEnCours(modele);
      vue.afficher();
      
      return modele.isSortieAvecValidation();
    }
    
    return true;
  }
  
  /**
   * Modifier une ligne commentaire
   * 
   * @param Int index de la ligne commentaire à modifier
   */
  public void modifierLigneCommentaire(int pIndexListe) {
    LigneAchatCommentaire ligneCommentaire = null;
    
    // Si pas de document en cours ou pas encore d'article dans le document, on sort.
    if (documentAchatEnCours == null || documentAchatEnCours.getListeLigneAchat() == null) {
      return;
    }
    
    ListeLigneAchat listeLigneAchat = documentAchatEnCours.getListeLigneAchat();
    
    // Si pas de liste, pas de modification !
    if (listeLigneAchat == null || listeLigneAchat.size() == 0) {
      return;
    }
    
    // Vérification de la validité de l'indice
    if (((pIndexListe < 0) || (pIndexListe > listeLigneAchat.size()))) {
      return;
    }
    
    // Si la ligne n'existe pas on sort
    if (listeLigneAchat.get(pIndexListe) == null) {
      return;
    }
    
    // Si la ligne n'est pas une ligne commentaire, on sort.
    if (!listeLigneAchat.get(pIndexListe).isLigneCommentaire()) {
      return;
    }
    
    ligneCommentaire = (LigneAchatCommentaire) listeLigneAchat.get(pIndexListe);
    
    // Lancer la boite de dialogue qui gère les commentaires
    ModeleDetailCommentaire modeleCommentaire = ModeleDetailCommentaire.getInstanceModifierLigneDocument(getSession(), ligneCommentaire);
    VueDetailCommentaire vue = new VueDetailCommentaire(modeleCommentaire);
    vue.afficher();
    
    // Traiter le résultat de la boîte de dialogue
    // Annulation : on sort
    if (modeleCommentaire.isSortieAvecAnnulation()) {
      return;
    }
    // Validation : on sauve la ligne
    else if (modeleCommentaire.isSortieAvecValidation()) {
      // Si le texte du commentaire est valide
      if (modeleCommentaire.getTexteCommentaire() != null && !modeleCommentaire.getTexteCommentaire().trim().isEmpty()) {
        // Mise à jour du texte du commentaire
        ligneCommentaire.setTexte(modeleCommentaire.getTexteCommentaire());
        // Modification de la ligne
        ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), ligneCommentaire, dateTraitement);
      }
    }
    
    // Rechargement des lignes et de l'entête du document
    documentAchatEnCours = DocumentAchat.lireLignesDocument(getIdSession(), documentAchatEnCours, false);
    
    rafraichir();
  }
  
  // -- Accesseurs
  
  public int getIndiceOngletAccessible() {
    return indiceOngletAccessible;
  }
  
  public int getEtape() {
    return etape;
  }
  
  public void setEtape(int etape) {
    this.etape = etape;
  }
  
  public void setMessageErreur(String messageErreur) {
    this.messageErreur = messageErreur;
  }
  
  public String getMessageErreur() {
    return messageErreur;
  }
  
  public Fournisseur getFournisseurCourant() {
    return fournisseurCourant;
  }
  
  public void setFournisseurCourant(Fournisseur pFournisseurCourant) {
    fournisseurCourant = pFournisseurCourant;
  }
  
  public IdAdresseFournisseur getIdAdresseFournisseurCourant() {
    return idAdresseFournisseurCourant;
  }
  
  public void setIdAdresseFournisseurCourant(IdAdresseFournisseur pIdAdresseFournisseurCourant) {
    idAdresseFournisseurCourant = pIdAdresseFournisseurCourant;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  public void setEtablissement(Etablissement pEtablissement) {
    this.etablissement = pEtablissement;
  }
  
  /**
   * Retourne l'idenifiant de l'établissement.
   *
   * @return
   */
  public IdEtablissement getIdEtablissement() {
    if (etablissement == null) {
      if (utilisateurGescom == null || utilisateurGescom.getIdEtablissementPrincipal() == null) {
        return null;
      }
      return utilisateurGescom.getIdEtablissementPrincipal();
    }
    return etablissement.getId();
  }
  
  /**
   * Magasin courant.
   */
  public Magasin getMagasin() {
    return magasin;
  }
  
  /**
   * Modifier le magasin courant.
   */
  public void setMagasin(Magasin pMagasin) {
    magasin = pMagasin;
  }
  
  public UtilisateurGescom getUtilisateurGescom() {
    return utilisateurGescom;
  }
  
  public void setUtilisateurGescom(UtilisateurGescom utilisateurGescom) {
    this.utilisateurGescom = utilisateurGescom;
  }
  
  public DocumentAchat getDocumentAchatEnCours() {
    return documentAchatEnCours;
  }
  
  public void setDocumentAchatEnCours(DocumentAchat pDocumentAchatEnCours) {
    documentAchatEnCours = pDocumentAchatEnCours;
  }
  
  public Date getDateTraitementDocument() {
    return dateTraitement;
  }
  
  public ListeAcheteur getListeAcheteur() {
    return listeAcheteur;
  }
  
  public void setListeAcheteur(ListeAcheteur listeAcheteur) {
    this.listeAcheteur = listeAcheteur;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public void setListeMagasin(ListeMagasin listeMagasin) {
    this.listeMagasin = listeMagasin;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  public void setListeUnite(ListeUnite listeUnite) {
    this.listeUnite = listeUnite;
  }
  
  public boolean isFermetureAutomatique() {
    return fermetureAutomatique;
  }
  
  /**
   * Retourne l'indice de l'onglet courant.
   * 
   * @return
   */
  public int getIndiceOngletCourant() {
    return indiceOngletCourant;
  }
  
  // --- Onglet fournisseur
  
  public int getIndexDocumentAchatSelectionne() {
    if (listeDocumentAchat == null) {
      return -1;
    }
    return listeDocumentAchat.getIndexSelection();
  }
  
  public void setIndexDocumentAchatSelectionne(int indexCommandeFournisseur) {
    if (listeDocumentAchat == null) {
      return;
    }
    listeDocumentAchat.selectionner(indexCommandeFournisseur);
  }
  
  public boolean isAutoriserApprovisionnementStock() {
    return autoriserApprovisionnementStock;
  }
  
  public boolean isAutoriserApprovisionnementHorsStockClient() {
    return autoriserApprovisionnementHorsStockClient;
  }
  
  public boolean isAutoriserApprovisionnementCommandeClientUrgente() {
    return autoriserApprovisionnementCommandeClientUrgente;
  }
  
  public List<DocumentAchat> getListeDocumentAchat() {
    return listeDocumentAchat;
  }
  
  // --- Onglet article
  
  /**
   * Retourner le composant qui détient le focus à l'écran.
   * 
   * @return int
   */
  public int getComposantAyantLeFocus() {
    return composantAyantLeFocus;
  }
  
  /**
   * Retourner l'index de la ligne dans le tableau de recherche des articles.
   * 
   * @return int
   */
  public int getIndexRechercheArticle() {
    return indexRechercheArticle;
  }
  
  /**
   * Retourner le texte recherché.
   * 
   * @return String
   */
  public String getTexteRecherche() {
    return texteRecherche;
  }
  
  /**
   * Retourner la valeur du filtre sur l'appartenance des articles à la gamme des marchandises en catalogue.
   * 
   * @return boolean
   */
  public boolean isFiltreHorsGamme() {
    return filtreHorsGamme;
  }
  
  /**
   * Retourner la valeur du filtre sur la recherche des articles sur l'ensemble des fournisseurs ou sur le fournisseur en cours.
   * 
   * @return boolean
   */
  public boolean isFiltreTousFournisseur() {
    return filtreTousFournisseur;
  }
  
  /**
   * Modifier l'index de la ligne dans le tableau de recherche des articles.
   * 
   * @param int
   */
  public void setIndexRechercheArticle(int indexRechercheArticle) {
    this.indexRechercheArticle = indexRechercheArticle;
  }
  
  /**
   * Retourne le message à afficher au dessus du tableau présentant le résultat de recherche articles.
   */
  public Message getMessage() {
    return message;
  }
  
  /**
   * Retourner la liste des articles trouvés suite à la recherche.
   * 
   * @return ListeArticle
   */
  public ListeArticle getListeResultatRechercheArticles() {
    return listeResultatRechercheArticles;
  }
  
  /**
   * Retourner une liste d'identifiants articles à sélectionner.
   * 
   * @return ArrayList<IdLigneAchat>
   */
  public ArrayList<IdLigneAchat> getListeLigneAchatASelectionner() {
    return listeLigneAchatASelectionner;
  }
  
  // --- Onglet édition
  
  public EnumOptionEdition getOptionChiffrageEdition() {
    return optionChiffrageEdition;
  }
  
  public ListeModeEdition getListeModeEditionAchat() {
    return listeModeEditionAchat;
  }
  
  public Contact getContactMail() {
    return contactMail;
  }
  
  public Contact getContactFax() {
    return contactFax;
  }
  
}
