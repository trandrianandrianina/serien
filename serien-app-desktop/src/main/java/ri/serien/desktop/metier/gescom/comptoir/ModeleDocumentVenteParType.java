/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.util.Date;
import java.util.List;

import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;

/**
 * L'ensemble des documents de ventes d'un client.
 */
public class ModeleDocumentVenteParType {
  private IdSession idSession = null;
  private ListeDocumentVenteBase listeDevis = null;
  private ListeDocumentVenteBase listeCommande = null;
  private ListeDocumentVenteBase listeBon = null;
  private ListeDocumentVenteBase listeFacture = null;
  private int indexDevisSelectionne = -1;
  private int indexCommandeSelectionnee = -1;
  private int indexBonSelectionne = -1;
  private int indexFactureSelectionnee = -1;
  
  /**
   * Constructeur.
   */
  public ModeleDocumentVenteParType(IdSession pIdSession) {
    idSession = pIdSession;
  }
  
  // -- Méthodes publiques
  
  /**
   * Effacer les listes.
   */
  public void initialiser() {
    // Initialiser la liste des devis
    listeDevis = null;
    
    // Initialiser la liste des Commandes
    listeCommande = null;
    
    // Initialiser la liste des bons
    listeBon = null;
    
    // Initialiser la liste des factures
    listeFacture = null;
  }
  
  /**
   * Charge l'ensemble des id documents du client avec les critères de base.
   */
  public void charger(IdSession pIdSession, IdClient pIdClient) {
    // Effacer le résultat de la recherche précédente
    initialiser();
    
    // Vérifier les pré-requis
    if (pIdClient == null) {
      throw new MessageErreurException("Impossible de charger la liste des documents de ventes car le client est invalide.");
    }
    
    // Renseigner les critères de recherche
    CritereDocumentVente critereDocumentVente = new CritereDocumentVente();
    critereDocumentVente.setIdClient(pIdClient);
    critereDocumentVente.setSansHistorique(true);
    critereDocumentVente.setDateCreationFin(new Date());
    critereDocumentVente.setDateCreationDebut(Constantes.getDateDebutMoisPrecedent(new Date()));
    
    // Charger les devis
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.DEVIS);
    List<IdDocumentVente> listeIdDevis = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeDevis = ListeDocumentVenteBase.creerListeNonChargee(listeIdDevis);
    listeDevis.chargerPremierePage(idSession, 0);
    
    // Charger les commandes
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.COMMANDE);
    List<IdDocumentVente> listeIdCommande = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeCommande = ListeDocumentVenteBase.creerListeNonChargee(listeIdCommande);
    listeCommande.chargerPremierePage(idSession, 0);
    
    // Charger les bons
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.BON);
    List<IdDocumentVente> listeIdBon = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeBon = ListeDocumentVenteBase.creerListeNonChargee(listeIdBon);
    listeBon.chargerPremierePage(idSession, 0);
    
    // Charger les factures
    critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
    // Pas de plage de date pour ne pas "oublier" des factures non réglés ou des avoirs
    critereDocumentVente.setDateCreationFin(null);
    critereDocumentVente.setDateCreationDebut(null);
    List<IdDocumentVente> listeIdFacture = ManagerServiceDocumentVente.chargerListeIdDocumentVente(pIdSession, critereDocumentVente);
    listeFacture = ListeDocumentVenteBase.creerListeNonChargee(listeIdFacture);
    listeFacture.chargerPremierePage(idSession, 0);
  }
  
  /**
   * Modifier la plage de devis affichés.
   */
  public void modifierPlageDevis(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeDevis != null) {
      listeDevis.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier la plage de commandes affichéss.
   */
  public void modifierPlageCommande(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeCommande != null) {
      listeCommande.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier la plage de bons affichés.
   */
  public void modifierPlageBon(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeBon != null) {
      listeBon.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier la plage de factures affichés.
   */
  public void modifierPlageFacture(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (listeFacture != null) {
      listeFacture.chargerPage(idSession, pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  // -- Accesseurs
  
  /**
   * Liste de devis.
   */
  public ListeDocumentVenteBase getListeDevis() {
    return listeDevis;
  }
  
  /**
   * Liste de commandes.
   */
  public ListeDocumentVenteBase getListeCommande() {
    return listeCommande;
  }
  
  /**
   * Liste de bons.
   */
  public ListeDocumentVenteBase getListeBon() {
    return listeBon;
  }
  
  /**
   * Liste de factures.
   */
  public ListeDocumentVenteBase getListeFacture() {
    return listeFacture;
  }
  
  /**
   * Retourne un devis à partir de son index dans la liste.
   */
  public DocumentVenteBase getDevis(int pIndex) {
    if (listeDevis == null || pIndex < 0 || pIndex > listeDevis.size()) {
      return null;
    }
    return listeDevis.get(pIndex);
  }
  
  /**
   * Retourne une commande à partir de son index dans la liste.
   */
  public DocumentVenteBase getCommande(int pIndex) {
    if (listeCommande == null || pIndex < 0 || pIndex > listeCommande.size()) {
      return null;
    }
    return listeCommande.get(pIndex);
  }
  
  /**
   * Retourne une commande à partir de son index dans la liste.
   */
  public DocumentVenteBase getBon(int pIndex) {
    if (listeBon == null || pIndex < 0 || pIndex > listeBon.size()) {
      return null;
    }
    return listeBon.get(pIndex);
  }
  
  /**
   * Retourne une facture à partir de son index dans la liste.
   */
  public DocumentVenteBase getFacture(int pIndex) {
    if (listeFacture == null || pIndex < 0 || pIndex > listeBon.size()) {
      return null;
    }
    return listeFacture.get(pIndex);
  }
  
  /**
   * Index du devis sélectionné dans la liste des documents de ventes du client.
   */
  public int getIndexDevisSelectionne() {
    return indexDevisSelectionne;
  }
  
  /**
   * Modifier le devis sélectionné dans la liste des documents clients.
   */
  public void setIndexDevisSelectionne(int pLigneSelectionnee) {
    indexDevisSelectionne = pLigneSelectionnee;
    indexCommandeSelectionnee = -1;
    indexBonSelectionne = -1;
    indexFactureSelectionnee = -1;
  }
  
  /**
   * Index de la commande sélectionnée dans la liste des documents de ventes du client.
   */
  public int getIndexCommandeSelectionnee() {
    return indexCommandeSelectionnee;
  }
  
  /**
   * Modifier la commande sélectionnée dans la liste des documents clients.
   */
  public void setIndexCommandeSelectionnee(int pLigneSelectionnee) {
    indexDevisSelectionne = -1;
    indexCommandeSelectionnee = pLigneSelectionnee;
    indexBonSelectionne = -1;
    indexFactureSelectionnee = -1;
  }
  
  /**
   * Index du bon sélectionné dans la liste des documents de ventes du client.
   */
  public int getIndexBonSelectionne() {
    return indexBonSelectionne;
  }
  
  /**
   * Modifier le bon sélectionné dans la liste des documents clients.
   */
  public void setIndexBonSelectionne(int pLigneSelectionnee) {
    indexDevisSelectionne = -1;
    indexCommandeSelectionnee = -1;
    indexBonSelectionne = pLigneSelectionnee;
    indexFactureSelectionnee = -1;
  }
  
  /**
   * Index de la facture sélectionnée dans la liste des documents de ventes du client.
   */
  public int getIndexFactureSelectionnee() {
    return indexFactureSelectionnee;
  }
  
  /**
   * Modifier le bon sélectionné dans la liste des documents clients.
   */
  public void setIndexFactureSelectionnee(int pLigneSelectionnee) {
    indexDevisSelectionne = -1;
    indexCommandeSelectionnee = -1;
    indexBonSelectionne = -1;
    indexFactureSelectionnee = pLigneSelectionnee;
  }
  
  /**
   * Indique si un document de ventes est sélectionné dans la liste des documents de ventes du client (null si aucun).
   */
  public boolean isDocumentVenteSelectionne() {
    return indexDevisSelectionne != -1 || indexCommandeSelectionnee != -1 || indexBonSelectionne != -1 || indexFactureSelectionnee != -1;
  }
  
  /**
   * Identifiant du document de ventes sélectionné dans la liste des documents de ventes (null si aucun).
   */
  public IdDocumentVente getIdDocumentVenteSelectionne() {
    if (listeDevis != null && indexDevisSelectionne > -1 && indexDevisSelectionne < listeDevis.size()) {
      return listeDevis.get(indexDevisSelectionne).getId();
    }
    else if (listeCommande != null && indexCommandeSelectionnee > -1 && indexCommandeSelectionnee < listeCommande.size()) {
      return listeCommande.get(indexCommandeSelectionnee).getId();
    }
    else if (listeBon != null && indexBonSelectionne > -1 && indexBonSelectionne < listeBon.size()) {
      return listeBon.get(indexBonSelectionne).getId();
    }
    else if (listeFacture != null && indexFactureSelectionnee > -1 && indexFactureSelectionnee < listeFacture.size()) {
      return listeFacture.get(indexFactureSelectionnee).getId();
    }
    return null;
  }
  
  /**
   * Document de ventes sélectionné dans la liste des documents de ventes (null si aucun).
   */
  public DocumentVenteBase getDocumentVenteBaseSelectionne() {
    if (listeDevis != null && indexDevisSelectionne > -1 && indexDevisSelectionne < listeDevis.size()) {
      return listeDevis.get(indexDevisSelectionne);
    }
    else if (listeCommande != null && indexCommandeSelectionnee > -1 && indexCommandeSelectionnee < listeCommande.size()) {
      return listeCommande.get(indexCommandeSelectionnee);
    }
    else if (listeBon != null && indexBonSelectionne > -1 && indexBonSelectionne < listeBon.size()) {
      return listeBon.get(indexBonSelectionne);
    }
    else if (listeFacture != null && indexFactureSelectionnee > -1 && indexFactureSelectionnee < listeFacture.size()) {
      return listeFacture.get(indexFactureSelectionnee);
    }
    return null;
  }
}
