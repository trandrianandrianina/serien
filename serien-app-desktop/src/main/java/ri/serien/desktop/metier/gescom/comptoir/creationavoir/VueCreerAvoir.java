/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.creationavoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.WindowConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.sntypeavoir.SNTypeAvoir;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueCreerAvoir extends AbstractVueDialogue<ModeleCreerAvoir> {
  /**
   * Constructeur.
   */
  public VueCreerAvoir(ModeleCreerAvoir pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  /**
   * Rafraichissement de l'écran
   */
  public void rafraichir() {
    rafraichirMessage();
    rafraichirTypeAvoir();
    rafraichirBouton();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir le message utilisateur
   */
  private void rafraichirMessage() {
    IdDocumentVente idDocument = getModele().getIdDocumentOrigine();
    if (idDocument != null) {
      if (idDocument.isIdFacture()) {
        lbMessage.setMessage(Message.getMessageMoyen("Vous souhaitez créer un avoir à partir de la facture numéro " + idDocument));
      }
      else {
        lbMessage.setMessage(Message.getMessageMoyen("Vous souhaitez créer un avoir à partir du bon de vente numéro " + idDocument));
      }
    }
  }
  
  /**
   * Rafraichir le composant de sélection d'un type d'avoir
   */
  private void rafraichirTypeAvoir() {
    snTypeAvoir.setSession(getModele().getSession());
    snTypeAvoir.setIdEtablissement(getModele().getIdDocumentOrigine().getIdEtablissement());
    snTypeAvoir.setAucunAutorise(true);
    snTypeAvoir.charger(false);
  }
  
  /**
   * Rafraichir le comportement des boutons
   */
  private void rafraichirBouton() {
    boolean actif = true;
    if (getModele().isTypeAvoirObligatoire() && snTypeAvoir.getSelection() == null) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  // -- Méthodes interractives
  /**
   * Actions des boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Action sur sélection d'un type d'avoir
   */
  private void snTypeAvoirValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierTypeAvoir(snTypeAvoir.getSelection());
      rafraichirBouton();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbMessage = new SNLabelTitre();
    lbTypeAvoir = new SNLabelChamp();
    snTypeAvoir = new SNTypeAvoir();
    
    // ======== this ========
    setTitle("Cr\u00e9ation d'un avoir");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(650, 180));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMessage ----
        lbMessage.setText("Vous souhaitez cr\u00e9er un avoir \u00e0 partir");
        lbMessage.setName("lbMessage");
        pnlContenu.add(lbMessage, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTypeAvoir ----
        lbTypeAvoir.setText("Type d'avoir");
        lbTypeAvoir.setPreferredSize(new Dimension(100, 30));
        lbTypeAvoir.setMinimumSize(new Dimension(100, 30));
        lbTypeAvoir.setMaximumSize(new Dimension(100, 30));
        lbTypeAvoir.setName("lbTypeAvoir");
        pnlContenu.add(lbTypeAvoir, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snTypeAvoir ----
        snTypeAvoir.setName("snTypeAvoir");
        snTypeAvoir.addSNComposantListener(new InterfaceSNComposantListener() {
          @Override
          public void valueChanged(SNComposantEvent e) {
            snTypeAvoirValueChanged(e);
          }
        });
        pnlContenu.add(snTypeAvoir, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbMessage;
  private SNLabelChamp lbTypeAvoir;
  private SNTypeAvoir snTypeAvoir;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
