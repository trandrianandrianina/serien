/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

/**
 * Classe appelée par le point de menu.
 */
public class SpAchat extends SessionJava {
  /**
   * Constructeur.
   */
  public SpAchat(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleAchat modele = new ModeleAchat(this, pParametreMenu);
    VueAchat vue = new VueAchat(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
    modele.initialiserAvecParametres();
  }
}
