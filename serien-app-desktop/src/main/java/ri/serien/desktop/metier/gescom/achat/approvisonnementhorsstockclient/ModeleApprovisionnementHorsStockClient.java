/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisonnementhorsstockclient;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.EnumTypeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleApprovisionnementHorsStockClient extends AbstractModeleDialogue {
  // Variables
  private ModeleAchat modeleParent = null;
  private IdEtablissement idEtablissement = null;
  private Fournisseur fournisseur = null;
  private IdMagasin idMagasin = null;
  private Message message = null;
  private int indiceTableau = -1;
  
  // Variables pour le chargement progressif de la liste
  private ArrayList<LigneAAfficher> listeLigneAAfficher = new ArrayList<LigneAAfficher>();
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheArticle = -1;
  
  /**
   * Constructeur.
   */
  public ModeleApprovisionnementHorsStockClient(SessionBase pSession, ModeleAchat pModeleParent, Fournisseur pFournisseur,
      IdMagasin pIdMagasin) {
    super(pSession);
    modeleParent = pModeleParent;
    fournisseur = pFournisseur;
    idMagasin = pIdMagasin;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Contrôle des variables obligatoires
    if (fournisseur == null || fournisseur.getId() == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    if (idMagasin == null) {
      throw new MessageErreurException("Le fournisseur est invalide.");
    }
    idEtablissement = fournisseur.getId().getIdEtablissement();
    
    // Lancement de la recherche dès l'affichage de la boite de dialogue
    rechercherArticlesAApprovisionner();
  }
  
  @Override
  public void quitterAvecValidation() {
    // Finir de charger les lignes qui n'ont pas été visionnées avant leur transformation en ligne d'achat afin d'avoir toutes les
    // données nécessaires
    chargerPlageLigneTableau(0, listeLigneAAfficher.size());
    
    for (LigneAAfficher ligneTableau : listeLigneAAfficher) {
      // Contrôle que les lignes qui ont une quantité renseignée et supérieure à zéro soient bien transformées en ligne d'achat
      convertirLigneAAfficherEnLigneAchat(ligneTableau);
    }
    
    // Chargement du document
    modeleParent.setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    
    // Traitement normal
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    // Suppression éventuelle des lignes d'achat qui auraient pu être crée (lors de l'affichage du détail)
    List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
    for (LigneAAfficher ligneAAfficher : listeLigneAAfficher) {
      if (ligneAAfficher.getLigneAchatAssociee() != null) {
        listeLigneASupprimer.add(ligneAAfficher.getLigneAchatAssociee());
      }
    }
    // Appel du service qui va supprimer ces lignes
    if (!listeLigneASupprimer.isEmpty()) {
      ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
      // Chargement du document
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
    }
    
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Afficher la plage de lignes d'achat comprise entre deux lignes.
   * Les informations des lignes d'achats sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageArticlesAffichees(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeLigneAAfficher == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeLigneAAfficher.size()) {
      indexPremiereLigneAffichee = listeLigneAAfficher.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeLigneAAfficher == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeLigneAAfficher.size()) {
      nombreLigneAffichee = listeLigneAAfficher.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacements de l'ascenseur avec la souris
    if (indexRechercheArticle < indexPremiereLigneAffichee) {
      indexRechercheArticle = indexPremiereLigneAffichee;
    }
    else if (indexRechercheArticle > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheArticle = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleApprovisionnementHorsStockClient.class, "modifierPlageLignesAffichees", "indexPremiereLigneVisible",
        indexPremiereLigneAffichee, "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierIndiceTableau(int pIndiceTableau) {
    if (indiceTableau == pIndiceTableau) {
      return;
    }
    indiceTableau = pIndiceTableau;
  }
  
  /**
   * Modifie la quantité d'une ligne du tableau.
   */
  public void modifierQuantiteUCA(int pIndiceTableau, String pSaisie) {
    if (!isQuantiteModifiee(pIndiceTableau, pSaisie)) {
      return;
    }
    message = null;
    indiceTableau = pIndiceTableau;
    LigneAAfficher ligneTableau = retournerLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      return;
    }
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    // Traitement de la quantité saisie si celle-ci vaut 0
    if (saisie.compareTo(BigDecimal.ZERO) == 0) {
      // On supprime la ligne d'achat si celle-ci existe
      if (ligneTableau.getLigneAchatAssociee() != null) {
        List<LigneAchat> listeLigneASupprimer = new ArrayList<LigneAchat>();
        listeLigneASupprimer.add(ligneTableau.getLigneAchatAssociee());
        ManagerServiceDocumentAchat.supprimerListeLigneDocument(getIdSession(), listeLigneASupprimer);
        ligneTableau.setLigneAchatAssociee(null);
      }
      ligneTableau.modifierQuantite(saisie);
      rafraichir();
      return;
    }
    
    // On contrôle la quantité saisie et on met à jour la quantité UCA de la ligne du tableau que l'on affiche
    try {
      ligneTableau.controlerQuantite(pSaisie);
      ligneTableau.modifierQuantite(saisie);
    }
    catch (Exception e) {
      message = Message.getMessageImportant(e.getMessage());
      rafraichir();
      return;
    }
    
    // Créer une ligne article dans le cas d'un ajout de ligne ou on met à jour la ligne d'achat si elle existe déjà
    convertirLigneAAfficherEnLigneAchat(ligneTableau);
    
    rafraichir();
  }
  
  /**
   * Vérifie qu'au moins un article a été commandé.
   */
  public boolean isVerifierArticleCommande() {
    if (listeLigneAAfficher == null || modeleParent.getDocumentAchatEnCours() == null) {
      return false;
    }
    for (LigneAAfficher ligneTableau : listeLigneAAfficher) {
      BigDecimal valeur = ligneTableau.getQuantiteCommandeeUCABigDecimal();
      if (valeur.compareTo(BigDecimal.ZERO) > 0) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Affiche le détail d'un article.
   */
  public void afficherDetail() {
    LigneAAfficher ligneTableau = retournerLigneTableau();
    if (ligneTableau == null) {
      rafraichir();
      return;
    }
    
    BigDecimal quantiteCommandeeOrigine = ligneTableau.getQuantiteCommandeeUCABigDecimal();
    // On contrôle que la quantité du tableau soit supérieure à 0
    if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) <= 0) {
      ligneTableau.modifierQuantite(BigDecimal.ONE);
    }
    
    if (ligneTableau.getLigneAchatAssociee() == null) {
      convertirLigneAAfficherEnLigneAchat(ligneTableau);
    }
    LigneAchatArticle ligneAchatArticle = ligneTableau.getLigneAchatAssociee();
    boolean validation = modeleParent.selectionnerLigneArticleStandard(ligneAchatArticle, ModeleDetailLigneArticle.ONGLET_GENERAL,
        ModeleAchat.TABLE_LIGNES_DOCUMENT, null, false, false, false);
    
    // On met à jour la quantité UCA de la ligne du tableau que l'on affiche dans le cas on l'on a validé la quanitté dans le détail
    if (validation) {
      ligneTableau.modifierQuantite(ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA());
    }
    else {
      ligneTableau.modifierQuantite(quantiteCommandeeOrigine);
      // Si la quantité d'origine était à zéro à l'origine alors il faut supprimer le ligne de commande que l'on a créé pour pouvoir
      // afficher le détail
      if (quantiteCommandeeOrigine.compareTo(BigDecimal.ZERO) == 0) {
        List<IdLigneAchat> listeIdLigneAchat = new ArrayList<IdLigneAchat>();
        listeIdLigneAchat.add(ligneTableau.getLigneAchatAssociee().getId());
        modeleParent.setDocumentAchatEnCours(DocumentAchat.supprimerListeLigneAchat(getIdSession(),
            modeleParent.getDocumentAchatEnCours(), listeIdLigneAchat, ManagerSessionClient.getInstance().getProfil()));
        ligneTableau.setLigneAchatAssociee(null);
      }
    }
    rafraichir();
  }
  
  /**
   * Détermine si l'on doit afficher le bouton "Afficher détail".
   */
  public boolean isAfficherBoutonDetail() {
    // Si aucune ligne n'est sélectionnée on n'affiche pas le bouton
    if (indiceTableau == -1) {
      return false;
    }
    // On contrôle si la ligne sélectionnée correspond à un article spécial
    LigneAAfficher ligneTableau = retournerLigneTableau();
    if (ligneTableau == null) {
      return false;
    }
    Article article = ligneTableau.getArticle();
    return article != null && !article.isArticleSpecial();
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les variables liées au chargement des données dans le tableau.
   */
  private void effacerVariablesChargement() {
    indexPremiereLigneAffichee = 0;
    indexRechercheArticle = -1;
    listeLigneAAfficher.clear();
    
    indiceTableau = -1;
  }
  
  /**
   * Recherche les articles hors stock à approvisionner.
   */
  private void rechercherArticlesAApprovisionner() {
    message = null;
    
    // Initialisation des critères de recherche des documents de vente
    CritereLigneVente criteres = new CritereLigneVente();
    criteres.setIdEtablissement(modeleParent.getEtablissement().getId());
    criteres.setIdFournisseur(fournisseur.getId());
    criteres.setIdMagasin(modeleParent.getMagasin().getId());
    criteres.setCommandeDirectUsine(false);
    criteres.setContenantArticleSpecial(true);
    criteres.setContenantArticleHorsGamme(true);
    criteres.setContenantArticleNonStockes(true);
    
    // Lancement de la recherche des articles hors stock à approvisionner
    // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
    // Idéalement il faudrait une liste de ligne de vente base qui contiendra l'id d u document de vente
    List<IdLigneVente> listeIdLigneVente = ManagerServiceDocumentVente.chargerListeIdLigneVenteAApprovisionner(getIdSession(), criteres);
    
    // Création de la liste des lignes de vente pour le tableau
    if (listeIdLigneVente == null || listeIdLigneVente.isEmpty()) {
      message = Message.getMessageImportant("Aucune ligne de vente n'a été trouvé.");
      return;
    }
    listeLigneAAfficher.clear();
    for (IdLigneVente idLigneVente : listeIdLigneVente) {
      listeLigneAAfficher.add(new LigneAAfficher(idLigneVente));
    }
    
    // Charger les lignes du tableau affichés
    chargerPlageLigneTableau(indexPremiereLigneAffichee, nombreLigneAffichee);
  }
  
  /**
   * Charger les données des lignes du tableau comprises entre deux lignes.
   * Les informations des lignes du tableau sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageLigneTableau(int pPremiereLigne, int pNombreLigne) {
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeLigneAAfficher.size());
    
    // Lister les id des lignes du tableau dont il faut charger les informations
    List<IdLigneVente> listeIdLigneVente = new ArrayList<IdLigneVente>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneTableau = listeLigneAAfficher.get(i);
      if (!ligneTableau.isCharge()) {
        listeIdLigneVente.add(ligneTableau.getIdLigneVente());
      }
    }
    
    // Quitter immédiatement si aucune ligne vente n'est à charger
    if (listeIdLigneVente.isEmpty()) {
      return;
    }
    
    // Charger les informations des lignes du tableau
    ParametresLireArticle parametres = new ParametresLireArticle();
    parametres.setIdEtablissement(idEtablissement);
    parametres.setIdFournisseur(fournisseur.getId());
    parametres.setIdMagasin(idMagasin);
    List<ReapprovisionnementClient> listeReapproClientCharges =
        ManagerServiceDocumentVente.chargerListeArticlesAApprovisionner(getIdSession(), listeIdLigneVente, parametres);
    
    // Mettre les lignes du tableau chargées au bon endroit dans la liste
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneTableau = listeLigneAAfficher.get(i);
      if (!ligneTableau.isCharge()) {
        for (ReapprovisionnementClient reapproClientCharge : listeReapproClientCharges) {
          if (ligneTableau.getIdLigneVente().equals(reapproClientCharge.getIdLigneVente())) {
            listeLigneAAfficher.get(i).initialiserDonnees(reapproClientCharge);
            Trace.debug(ModeleApprovisionnementHorsStockClient.class, "chargerPlageLigneTableau", "Charger ligne tableau", "ligne", i,
                "idLigneVente", reapproClientCharge.getIdLigneVente());
            break;
          }
        }
      }
    }
    
    // Chargement du document
    /*
    modeleParent.setDocumentEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentEnCours()));
    // On vérifie que si des lignes achats existent déjà alors on les lie avec la listeLigneTableau
    for (LigneAAfficher ligneTableau : listeLigneTableau) {
      LigneAchatArticle ligneAchat = retournerLigneAchatArticle(ligneTableau);
      ligneTableau.setLigneAchatAssociee(ligneAchat);
    }
    */
  }
  
  /**
   * Retourne la ligne sélectionnée du tableau.
   */
  private LigneAAfficher retournerLigneTableau() {
    if (indiceTableau < 0 || indiceTableau >= listeLigneAAfficher.size()) {
      return null;
    }
    return listeLigneAAfficher.get(indiceTableau);
  }
  
  /**
   * Retourne la ligne d'achat article correspondant à l'id article.
   */
  private LigneAchatArticle retournerLigneAchatArticle(IdLigneAchat pIdLigneAchat) {
    if (pIdLigneAchat == null) {
      return null;
    }
    ListeLigneAchat listeLignesDocument = modeleParent.getDocumentAchatEnCours().getListeLigneAchat();
    // Récupération de la ligne achat article
    LigneAchat ligneAchat = listeLignesDocument.get(pIdLigneAchat);
    // Il est fortement possible que l'on ait annuler lors de la demande de confirmation (affichage des commandes en cours)
    if (ligneAchat == null) {
      return null;
    }
    LigneAchatArticle ligneAchatArticle = (LigneAchatArticle) ligneAchat;
    return ligneAchatArticle;
  }
  
  /**
   * Convertit une ligne à afficher en ligne d'achat ou mise à jour une ligne d'achat déjà existante.
   */
  private void convertirLigneAAfficherEnLigneAchat(LigneAAfficher pLigneAAfficher) {
    // On contrôle que la quantité du tableau soit valide
    if (pLigneAAfficher == null) {
      return;
    }
    BigDecimal valeur = pLigneAAfficher.getQuantiteCommandeeUCABigDecimal();
    // On contrôle que la quantité du tableau soit supérieure à 0
    if (valeur.compareTo(BigDecimal.ZERO) <= 0) {
      return;
    }
    
    // Créer une ligne article dans le cas d'un ajout de ligne
    if (pLigneAAfficher.getLigneAchatAssociee() == null) {
      IdLigneAchat idLigneAchat = modeleParent.creerLigneAchat(pLigneAAfficher.getArticle(),
          pLigneAAfficher.getQuantiteCommandeeUCABigDecimal(), pLigneAAfficher.getIdLigneVente());
      // Chargement du document
      modeleParent
          .setDocumentAchatEnCours(DocumentAchat.lireLignesDocument(getIdSession(), modeleParent.getDocumentAchatEnCours(), false));
      // Récupération de la ligne achat article à partir de son id
      pLigneAAfficher.setLigneAchatAssociee(retournerLigneAchatArticle(idLigneAchat));
    }
    // Dans le cas d'une mise à jour d'une ligne achat déjà existante
    else {
      LigneAchatArticle ligneAchat = pLigneAAfficher.getLigneAchatAssociee();
      ligneAchat.getPrixAchat().setQuantiteReliquatUCA(valeur, true);
      // Si on est dans une commande direct usine le type de la ligne doit être ligne spéciale non stockée (code ERL == 'S')
      if (modeleParent.getDocumentAchatEnCours().isDirectUsine() && !ligneAchat.isLigneCommentaire()) {
        ligneAchat.setTypeLigne(EnumTypeLigneAchat.SPECIALE_NON_STOCKEE);
      }
      ManagerServiceDocumentAchat.sauverLigneAchat(getIdSession(), ligneAchat, modeleParent.getDateTraitementDocument());
    }
  }
  
  /**
   * Contrôle si la quantitée saisie est différente de la valeur d'origine.
   */
  private boolean isQuantiteModifiee(int pIndiceTableau, String pSaisie) {
    indiceTableau = pIndiceTableau;
    LigneAAfficher ligneTableau = retournerLigneTableau();
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      return false;
    }
    pSaisie = Constantes.normerTexte(pSaisie);
    BigDecimal saisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    return saisie.compareTo(ligneTableau.getQuantiteCommandeeUCABigDecimal()) != 0;
  }
  
  // -- Accesseurs
  
  public ArrayList<LigneAAfficher> getListeChargement() {
    return listeLigneAAfficher;
  }
  
  public Message getMessage() {
    return message;
  }
  
  public int getIndiceTableau() {
    return indiceTableau;
  }
  
}
