/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.genererconditionventes;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Date;

import javax.swing.JScrollPane;
import javax.swing.WindowConstants;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueGenererConditionVentes extends AbstractVueDialogue<ModeleGenererConditionVentes> {
  // Constantes
  private final static String[] TITRE_LISTE_LIGNE =
      new String[] { "Code article", "Libell\u00e9", "Prix HT", "UV", "Origine", "Remise", "Prix net HT" };
  
  // Variables
  private JTableCellRendererLignesDevis lignesDocumentRenderer = new JTableCellRendererLignesDevis();
  private DefaultTableModel tableModelLignesDocument = null;
  
  /**
   * Constructeur.
   */
  public VueGenererConditionVentes(ModeleGenererConditionVentes pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construire l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Mettre en forme la liste
    scpLignesDevis.getViewport().setBackground(Color.WHITE);
    tblLigneDevis.getTableHeader().setFont(new Font("SansSerif", Font.PLAIN, 14));
    tblLigneDevis.setFont(new Font("SansSerif", Font.PLAIN, 14));
    tblLigneDevis.getTableHeader().setBackground(Color.WHITE);
    tableModelLignesDocument = new DefaultTableModel(null, TITRE_LISTE_LIGNE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
      }
    };
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    tfReference.requestFocus();
  }
  
  /**
   * Rafraîchir l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirPlageDateValidite();
    rafraichirReference();
    rafraichirTableauLignes();
    rafraichirVisibiliteBouton();
  }
  
  // -- Méthodes privées ****************************************************************************************************************
  /**
   * Charger le titre de la boite de dialogue
   */
  private void rafraichirTitre() {
    IdDocumentVente idDocumentOrigine = getModele().getIdDocumentOrigine();
    
    if (idDocumentOrigine != null) {
      setTitle("Génération de conditions de ventes à partir des lignes du devis n°" + idDocumentOrigine);
    }
    else {
      setTitle("Génération de conditions de ventes à partir des lignes du devis");
    }
  }
  
  /**
   * Charger la date de début et de fin de validité dans le composant de saisie de plage de date.
   */
  private void rafraichirPlageDateValidite() {
    Date dateDebut = getModele().getDateValiditeDebutCNV();
    snPlageDateValidite.setDateDebut(dateDebut);
    Date dateFin = getModele().getDateValiditeFinCNV();
    snPlageDateValidite.setDateFin(dateFin);
  }
  
  /**
   * Charger la référence de la CNV
   */
  private void rafraichirReference() {
    String reference = getModele().getReferenceCNV();
    tfReference.setText(reference);
  }
  
  /**
   * Rafraichir la visibilite des boutons
   */
  private void rafraichirVisibiliteBouton() {
    boolean actif = true;
    if (tblLigneDevis.getSelectionModel().isSelectionEmpty()) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Charger les données dans la liste.
   */
  private void rafraichirTableauLignes() {
    // Charger les lignes du document d'origine
    final ListeLigneVente listeLigneVenteBase = getModele().getListeLigneVenteOrigine();
    if (listeLigneVenteBase == null) {
      return;
    }
    
    // Préparer les données pour le tableau
    String[][] donnees = null;
    donnees = new String[listeLigneVenteBase.size()][TITRE_LISTE_LIGNE.length];
    
    if (donnees.length > 0) {
      for (int ligne = 0; ligne < listeLigneVenteBase.size(); ligne++) {
        LigneVenteBase ligneVenteBase = listeLigneVenteBase.get(ligne);
        // Les données
        if (ligneVenteBase.isLigneCommentaire()) {
          continue;
        }
        else {
          donnees[ligne][0] = ligneVenteBase.getIdArticle().getCodeArticle();
          donnees[ligne][1] = ligneVenteBase.getLibelle();
          donnees[ligne][2] = Constantes.formater(ligneVenteBase.getPrixBaseHT(), true);
          
          if (ligneVenteBase.getIdUniteConditionnementVente() != null) {
            donnees[ligne][3] = ligneVenteBase.getIdUniteConditionnementVente().getCode();
          }
          donnees[ligne][4] = "";
          donnees[ligne][5] = Constantes.formater(ligneVenteBase.getTauxRemise1(), true) + " %";
          donnees[ligne][6] = Constantes.formater(ligneVenteBase.getPrixNetHT(), true);
        }
      }
    }
    
    // Effacer les donnes de tous les modèles
    tableModelLignesDocument.setRowCount(0);
    
    // Remplir le tableau avec les données mises en forme
    for (String[] ligne : donnees) {
      tableModelLignesDocument.addRow(ligne);
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblLigneDevis.getModel().equals(tableModelLignesDocument)) {
      tblLigneDevis.setModel(tableModelLignesDocument);
      tblLigneDevis.setGridColor(new Color(204, 204, 204));
      tblLigneDevis.setDefaultRenderer(Object.class, lignesDocumentRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblLigneDevis.getRowCount() == 0) {
      lignesDocumentRenderer.redimensionnerColonnes(tblLigneDevis);
    }
  }
  
  /**
   * Valider la boite de dialogue
   */
  private void valider() {
    getModele().modifierListeLigneSelection(tblLigneDevis.getListeIndiceSelection());
    getModele().validerDonnees();
  }
  
  // -- Méthodes interractives ***********************************************************************************************************
  /**
   * Boutons
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        valider();
      }
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Modification de la plage de date de validité.
   * 
   * @param e
   */
  private void snPlageDateValiditeValueChanged(SNComposantEvent e) {
    try {
      if (!isDonneesChargees()) {
        return;
      }
      getModele().modifierDateValiditeDebut(snPlageDateValidite.getDateDebut());
      getModele().modifierDateValiditeFin(snPlageDateValidite.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Modification de la référence
   */
  private void tfReferenceFocusLost(FocusEvent e) {
    try {
      if (!isDonneesChargees()) {
        return;
      }
      getModele().modifierReference(tfReference.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Click sur la liste
   */
  private void tblLigneDevisMouseClicked(MouseEvent e) {
    try {
      rafraichirVisibiliteBouton();
      if (e.getClickCount() == 2) {
        valider();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlCnv = new SNPanelTitre();
    lbDateValidite = new SNLabelChamp();
    snPlageDateValidite = new SNPlageDate();
    lbReference = new SNLabelChamp();
    tfReference = new SNTexte();
    sNLabelTitre1 = new SNLabelTitre();
    sNPanel1 = new SNPanel();
    scpLignesDevis = new JScrollPane();
    tblLigneDevis = new NRiTable();
    
    // ======== this ========
    setTitle("G\u00e9n\u00e9ration de conditions de ventes \u00e0 partir du devis");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(1060, 510));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlCnv ========
        {
          pnlCnv.setTitre("Condition de vente \u00e0 cr\u00e9er");
          pnlCnv.setName("pnlCnv");
          pnlCnv.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCnv.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCnv.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCnv.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlCnv.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbDateValidite ----
          lbDateValidite.setText("P\u00e9riode de validit\u00e9");
          lbDateValidite.setName("lbDateValidite");
          pnlCnv.add(lbDateValidite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snPlageDateValidite ----
          snPlageDateValidite.setName("snPlageDateValidite");
          snPlageDateValidite.addSNComposantListener(e -> snPlageDateValiditeValueChanged(e));
          pnlCnv.add(snPlageDateValidite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbReference ----
          lbReference.setText("R\u00e9f\u00e9rence de la CNV");
          lbReference.setName("lbReference");
          pnlCnv.add(lbReference, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfReference ----
          tfReference.setName("tfReference");
          tfReference.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfReferenceFocusLost(e);
            }
          });
          pnlCnv.add(tfReference, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlCnv, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelTitre1 ----
        sNLabelTitre1.setText("A partir des lignes du devis");
        sNLabelTitre1.setName("sNLabelTitre1");
        pnlContenu.add(sNLabelTitre1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== sNPanel1 ========
        {
          sNPanel1.setName("sNPanel1");
          sNPanel1.setLayout(new GridBagLayout());
          ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scpLignesDevis ========
          {
            scpLignesDevis.setName("scpLignesDevis");
            
            // ---- tblLigneDevis ----
            tblLigneDevis.setShowVerticalLines(true);
            tblLigneDevis.setShowHorizontalLines(true);
            tblLigneDevis.setBackground(Color.white);
            tblLigneDevis.setRowHeight(20);
            tblLigneDevis.setSelectionBackground(new Color(57, 105, 138));
            tblLigneDevis.setGridColor(new Color(204, 204, 204));
            tblLigneDevis.setName("tblLigneDevis");
            tblLigneDevis.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblLigneDevisMouseClicked(e);
              }
            });
            scpLignesDevis.setViewportView(tblLigneDevis);
          }
          sNPanel1.add(scpLignesDevis, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(sNPanel1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCnv;
  private SNLabelChamp lbDateValidite;
  private SNPlageDate snPlageDateValidite;
  private SNLabelChamp lbReference;
  private SNTexte tfReference;
  private SNLabelTitre sNLabelTitre1;
  private SNPanel sNPanel1;
  private JScrollPane scpLignesDevis;
  private NRiTable tblLigneDevis;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
