/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.contacts;

import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererListeContact extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private ImageIcon iconeGVCmodif = new ImageIcon(getClass().getClassLoader().getResource("images/GVC_modif.png"));
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Taille des colonnes
    redimensionnerColonnes(table);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setCellRenderer(leftRenderer);
    cm.getColumn(1).setCellRenderer(leftRenderer);
    cm.getColumn(2).setCellRenderer(leftRenderer);
    cm.getColumn(3).setCellRenderer(leftRenderer);
    cm.getColumn(4).setCellRenderer(leftRenderer);
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(35);
    cm.getColumn(0).setMaxWidth(35);
    cm.getColumn(1).setMinWidth(220);
    cm.getColumn(2).setMinWidth(120);
    cm.getColumn(2).setMaxWidth(120);
    cm.getColumn(3).setMinWidth(120);
    cm.getColumn(3).setMaxWidth(185);
    cm.getColumn(4).setMinWidth(240);
  }
}
