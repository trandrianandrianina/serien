/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.editiondocument;

import java.util.Date;

import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.DestinataireMail;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeDestinataireReponse;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.IdDestinataireMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.OptionsEditionAchat;
import ri.serien.libcommun.gescom.commun.document.ModeEdition;
import ri.serien.libcommun.gescom.personnalisation.acheteur.Acheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeModeEdition;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceTechnique;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.InterfaceSoumission;

public class EditionDocument implements InterfaceSoumission {
  // Variables
  private DocumentAchat documentAchat = null;
  private SessionBase session = null;
  private Date dateTraitement = null;
  private ListeModeEdition listeModeEdition = null;
  private EnumOptionEdition optionChiffrageEdition = null;
  private String emailDestinataire = null;
  private String numeroFax = null;
  private ListeAcheteur listeAcheteur = null;
  
  /**
   * Constructeur.
   */
  public EditionDocument(SessionBase pSession, DocumentAchat pDocumentAchat, Date pDateTraitement, ListeModeEdition pListeModeEdition,
      EnumOptionEdition pOptionEdition, ListeAcheteur pListeAcheteur) {
    session = pSession;
    documentAchat = pDocumentAchat;
    dateTraitement = pDateTraitement;
    listeModeEdition = pListeModeEdition;
    optionChiffrageEdition = pOptionEdition;
    listeAcheteur = pListeAcheteur;
  }
  
  // -- Méthodes publiques
  
  /**
   * Edition du document.
   */
  public void editer() {
    if (documentAchat == null || documentAchat.getId() == null) {
      throw new MessageErreurException("L'identifiant du document d'achat est invalide.");
    }
    if (optionChiffrageEdition == null) {
      throw new MessageErreurException("Les options de validation sont invalides.");
    }
    
    // On parcourt la liste des modes d'édition qui ont été sélectionné
    for (ModeEdition modeEdition : listeModeEdition) {
      // Si le mode d'édition n'est pas actif ou qu'il n'a pas été sélectionné on passe au suivant
      if (!modeEdition.isActif() || !modeEdition.isSelectionne()) {
        continue;
      }
      // Si le mode d'édition a déjà été traité on passe au suivant
      if (modeEdition.isTraite()) {
        continue;
      }
      
      try {
        switch (modeEdition.getEnumModeEdition()) {
          case IMPRIMER:
            imprimer(modeEdition);
            break;
          case ENVOYER_PAR_FAX:
            faxer(modeEdition);
            break;
          case ENVOYER_PAR_MAIL:
            mailer(modeEdition);
            break;
          case VISUALISER:
            visualiser(modeEdition);
            break;
          default:
            break;
        }
      }
      catch (MessageErreurException exception) {
        DialogueErreur.afficher(exception);
      }
    }
  }
  
  /**
   * Demande l'exécution en tâche de fond.
   */
  @Override
  public void executer() {
    editer();
  }
  
  // -- Méthodes privées
  
  /**
   * Imprime un document à partir de son spool sur une imprimante reconnue par l'AS400 (ne concerne pas les éditions NewSim V2).
   * Il faut une affectation d'impression avec un code état sans le @ pour que cela fonctionne, par exemple CDA.
   */
  private void imprimer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    
    // Appel du service qui va générer lancer l'édition
    OptionsEditionAchat optionsEdition = new OptionsEditionAchat();
    optionsEdition.setModeEdition(pModeEdition);
    optionsEdition.setOptionEdition(optionChiffrageEdition);
    ManagerServiceDocumentAchat.editerDocument(session.getIdSession(), documentAchat.getId(), dateTraitement, optionsEdition,
        session.getLdaSerien());
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Visualise un document.
   * Un fichier PDF est généré, téléchargé sur le poste de travail et enfin affiché avec l'application adéquat.
   * Il faut une affectation d'impression avec un code état avec le @ pour que cela fonctionne, par exemple CDA@.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void visualiser(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    
    // Appel du service qui va générer lancer l'édition
    // L'option d'édition est de type mail car elle permet de sélectionner l'affectation d'impression qui va générer un fichier PDF
    // C'est une limitation de Série N sur laquelle il va falloir travailler
    OptionsEditionAchat optionsEdition = new OptionsEditionAchat();
    optionsEdition.setModeEdition(pModeEdition);
    optionsEdition.setOptionEdition(optionChiffrageEdition);
    String cheminPDF = ManagerServiceDocumentAchat.editerDocument(session.getIdSession(), documentAchat.getId(), dateTraitement,
        optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL")) {
      return;
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException(
          "Une erreur s'est produite pendant la génération de l'édition.\nVeuillez vérifier que l'affectation d'impression"
              + " des commandes d'achats a été correctement paramétrée.");
    }
    
    // Transférer le PDF sur le poste client
    String pdf = session.getLexique().RecuperationFichier(cheminPDF);
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
    
    // Ouvrir le PDF avec l'application associée
    session.getLexique().AfficheDocument(pdf);
  }
  
  /**
   * Envoi un document par mail.
   * Un fichier PDF est généré, téléchargé sur le poste de travail et enfin affiché avec l'application adéquat.
   * Il faut une affectation d'impression avec un code état avec le @ pour que cela fonctionne, par exemple CDA@.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void mailer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    if (emailDestinataire == null) {
      throw new MessageErreurException("L'email du destinataire est incorrect.");
    }
    
    // Création du mail
    Mail mail = null;
    
    String indicatifDocumentPourMail = null;
    if (documentAchat.getId() != null) {
      indicatifDocumentPourMail = documentAchat.getId().getIndicatif(IdDocumentVente.INDICATIF_NUM);
    }
    
    // Recherche s'il y a un destinataire pour la réponse au mail en fonction du type de mail
    IdTypeMail idTypeMail =
        IdTypeMail.getInstance(documentAchat.getId().getIdEtablissement(), EnumTypeMail.MAIL_COMMANDE_FOURNISSEUR_PIECE_JOINTE);
    String emailDestinataireReponse = recupererAdresseEmailReponse(idTypeMail);
    try {
      // Préparation du mail
      mail = preparerMail(emailDestinataire, idTypeMail, indicatifDocumentPourMail, emailDestinataireReponse);
    }
    catch (MessageErreurException e) {
      Trace.erreur("Le mail n'a pas pu être créé.");
    }
    
    if (mail == null) {
      throw new MessageErreurException("Erreur lors de la préparation du mail.");
    }
    
    // Appel du service qui va générer lancer l'édition
    OptionsEditionAchat optionsEdition = new OptionsEditionAchat();
    optionsEdition.setModeEdition(pModeEdition);
    optionsEdition.setOptionEdition(optionChiffrageEdition);
    optionsEdition.setIdMail(mail.getId());
    
    String cheminPDF = ManagerServiceDocumentAchat.editerDocument(session.getIdSession(), documentAchat.getId(), dateTraitement,
        optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(".pdf")) {
      throw new MessageErreurException(
          "Vérifier vos affectations d'impression car ce n'est pas un fichier PDF qui a été généré pour le mode d'édition \""
              + pModeEdition.getTexte() + "\".");
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException(
          "Une erreur s'est produite pendant la génération de l'édition pour le mode d'édition \"" + pModeEdition.getTexte() + "\".");
    }
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Envoi un document par fax.
   * Un fichier PDF est généré, téléchargé sur le poste de travail et enfin affiché avec l'application adéquat.
   * Il faut une affectation d'impression avec un code état avec le 'X' pour que cela fonctionne, par exemple CDAX.
   * Attention ne concerne pas les éditions NewSim V2 car pour l'instant nous ne sommes pas capable de récupérer le spool .
   */
  private void faxer(ModeEdition pModeEdition) {
    if (pModeEdition == null) {
      throw new MessageErreurException("Le mode d'édition est invalide.");
    }
    if (Constantes.normerTexte(numeroFax).isEmpty()) {
      throw new MessageErreurException("Le numéro de fax est incorrect.");
    }
    
    // Création du mail
    Mail mail = null;
    try {
      String indicatifDocument = null;
      if (documentAchat.getId() != null) {
        indicatifDocument = documentAchat.getId().getIndicatif(IdDocumentVente.INDICATIF_NUM);
      }
      
      // Préparation du mail pour le fax
      IdTypeMail idTypeMail = IdTypeMail.getInstance(documentAchat.getId().getIdEtablissement(), EnumTypeMail.MAIL_ENVOI_FAX);
      mail = preparerMail(getNettoyerNumeroFax(numeroFax), idTypeMail, indicatifDocument, null);
    }
    catch (MessageErreurException e) {
      Trace.erreur("Le mail n'a pas pu être créé.");
    }
    
    if (mail == null) {
      throw new MessageErreurException("Erreur lors de la préparation du mail.");
    }
    
    // Appel du service qui va générer lancer l'édition
    OptionsEditionAchat optionsEdition = new OptionsEditionAchat();
    optionsEdition.setModeEdition(pModeEdition);
    optionsEdition.setOptionEdition(optionChiffrageEdition);
    optionsEdition.setIdFaxMail(mail.getId());
    
    String cheminPDF = ManagerServiceDocumentAchat.editerDocument(session.getIdSession(), documentAchat.getId(), dateTraitement,
        optionsEdition, session.getLdaSerien());
    cheminPDF = Constantes.normerTexte(cheminPDF);
    // Si le chemin retourné contient le mot clef *SPOOL, c'est que l'on a généré un fichier spool et donc il n'y a rien à afficher
    if (cheminPDF.equals("*SPOOL") || !cheminPDF.toLowerCase().endsWith(".pdf")) {
      throw new MessageErreurException(
          "Vérifier vos affectations d'impression car ce n'est pas un fichier PDF qui a été généré pour le mode d'édition \""
              + pModeEdition.getTexte() + "\".");
    }
    // Si le chemin retourné est vide c'est qu'il y a eu une erreur
    else if (cheminPDF.isEmpty()) {
      throw new MessageErreurException(
          "Une erreur s'est produite pendant la génération de l'édition pour le mode d'édition \"" + pModeEdition.getTexte() + "\".");
    }
    
    // Si tout c'est bien passé on marque le mode d'édition comme traité
    pModeEdition.marquerCommeTraite();
  }
  
  /**
   * Prépare un mail avec les données du document d'achat.
   */
  private Mail preparerMail(String pEmail, IdTypeMail pTypeMail, String pIndicatifDocument, String pEmailReponse) {
    pEmail = Constantes.normerTexte(pEmail);
    if (pEmail.isEmpty()) {
      throw new MessageErreurException("L'email du destinataire est incorrect.");
    }
    
    Mail mail = new Mail(IdMail.getInstanceAvecCreationId(documentAchat.getId().getIdEtablissement()));
    mail.setIdTypeMail(pTypeMail);
    mail.setIndicatifDocument(pIndicatifDocument);
    // Le statut est en cours de praparation car NewSim doit générer la pièce jointe dans un deuxième temps
    mail.setStatut(EnumStatutMail.EN_COURS_PREPARATION);
    
    // Préparation du destinataire
    IdDestinataireMail idDestinataireMail = IdDestinataireMail.getInstanceAvecCreationIdPourTableMail(mail.getId().getIdEtablissement());
    DestinataireMail destinataireMail = new DestinataireMail(idDestinataireMail);
    destinataireMail.setEmail(pEmail);
    ListeDestinataireMail listeDestinataireMail = new ListeDestinataireMail();
    listeDestinataireMail.add(destinataireMail);
    mail.setListeDestinataire(listeDestinataireMail);
    
    // Préparation du destinataire de la réponse
    pEmailReponse = Constantes.normerTexte(pEmailReponse);
    if (!pEmailReponse.isEmpty()) {
      IdDestinataireMail idDestinataireReponseMail = IdDestinataireMail.getInstanceAvecCreationId(mail.getId().getIdEtablissement());
      DestinataireMail destinataireReponseMail = new DestinataireMail(idDestinataireReponseMail);
      destinataireReponseMail.setEmail(pEmailReponse);
      ListeDestinataireMail listeDestinataireReponseMail = new ListeDestinataireMail();
      listeDestinataireReponseMail.add(destinataireReponseMail);
      mail.setListeDestinataireReponse(listeDestinataireReponseMail);
    }
    
    // Création du mail dans la base de données
    IdMail idMail = ManagerServiceTechnique.creerMail(session.getIdSession(), mail);
    // Rechargement du mail en base avec son id
    mail = ManagerServiceTechnique.chargerMail(session.getIdSession(), idMail);
    
    // Construction et sauvegarde de la liste des variables pour le mail
    ListeVariableMail.initialiserVariableMail(session.getIdSession(), mail);
    
    return mail;
    
  }
  
  /**
   * Ne conserve que les chiffres du numéro de fax.
   */
  private String getNettoyerNumeroFax(String pNumeroFax) {
    if (pNumeroFax == null) {
      return null;
    }
    return pNumeroFax.trim().replaceAll(" ", "").replaceAll("\\.", "").replaceAll("-", "");
  }
  
  /**
   * Retourne une éventuelle adresse email pour une réponse au mail.
   */
  private String recupererAdresseEmailReponse(IdTypeMail pIdTypeMail) {
    // Recherche du type de mail concerné
    CritereTypeMail critereTypeMail = new CritereTypeMail();
    critereTypeMail.setActif(Boolean.TRUE);
    critereTypeMail.setIdTypeMail(pIdTypeMail);
    ListeTypeMail listeTypeMail = ManagerServiceTechnique.chargerListeTypeMail(session.getIdSession(), critereTypeMail);
    if (listeTypeMail == null || listeTypeMail.isEmpty()) {
      throw new MessageErreurException("Aucun type de mail n'a été trouvé pour générer le mail à envoyer.");
    }
    String emailDestinataireReponse = null;
    if (listeTypeMail.size() > 1) {
      Trace.alerte("Plusieurs types de mail ont été trouvé lors de la recherche du destinataire pour la réponse au mail.");
    }
    TypeMail typeMail = listeTypeMail.get(0);
    
    // Récupération de l'adresse email pour la réponse au mail
    EnumTypeDestinataireReponse typeDestinataireReponse = typeMail.getTypeDestinataireReponse();
    if (typeDestinataireReponse == null || typeDestinataireReponse.equals(EnumTypeDestinataireReponse.AUCUNE_ADRESSE)) {
      return null;
    }
    
    switch (typeDestinataireReponse) {
      // Cas du vendeur
      case EMAIL_ACHETEUR:
        Acheteur acheteur = listeAcheteur.get(documentAchat.getIdAcheteur());
        if (acheteur == null) {
          throw new MessageErreurException("L'acheteur est invalide alors qu'il est nécessaire pour la construction du mail.");
        }
        if (acheteur.getAdresseEmail() == null || acheteur.getAdresseEmail().trim().isEmpty()) {
          throw new MessageErreurException(
              "L'adresse email de l'acheteur est invalide alors qu'elle est nécessaire pour la construction du mail.");
        }
        emailDestinataireReponse = acheteur.getAdresseEmail();
        break;
      
      // Les autres cas sont ignorés
      default:
        // Une trace est écrite dans les logs mais le traitement n'est pas bloqué pour autant
        Trace.alerte("Le type du destinataire du mail n'est pas approprié pour le document en cours.");
    }
    
    return emailDestinataireReponse;
  }
  
  // -- Accesseurs
  
  public void setEmailDestinataire(String pEmail) {
    emailDestinataire = pEmail;
  }
  
  public void setNumeroFaxDestinataire(String pNumeroFax) {
    this.numeroFax = pNumeroFax;
  }
  
}
