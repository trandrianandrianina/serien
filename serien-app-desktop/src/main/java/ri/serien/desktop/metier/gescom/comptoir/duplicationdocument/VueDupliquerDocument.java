/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.duplicationdocument;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.WindowConstants;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueDupliquerDocument extends AbstractVueDialogue<ModeleDupliquerDocument> {
  /**
   * Constructeur.
   */
  public VueDupliquerDocument(ModeleDupliquerDocument pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Les raccourcis claviers
    rbConserverPrix.setMnemonic(KeyEvent.VK_C);
    rbRecalculerPrix.setMnemonic(KeyEvent.VK_R);
    
    // Sélection par défaut
    rbRecalculerPrix.setSelected(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirMessageDupplication();
    rafraichirMessageAction();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessageDupplication() {
    lbMessageDuplication.setMessage(getModele().getMessageDupplication());
  }
  
  private void rafraichirMessageAction() {
    lbMessageAction.setMessage(getModele().getMessageAction());
  }
  
  // -- Méthodes interractives
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbConserverPrixActionPerformed(ActionEvent e) {
    try {
      getModele().setRecalculerPrix(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRecalculerPrixActionPerformed(ActionEvent e) {
    try {
      getModele().setRecalculerPrix(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    lbMessageDuplication = new SNLabelTitre();
    lbMessageAction = new SNLabelTitre();
    rbConserverPrix = new SNRadioButton();
    rbRecalculerPrix = new SNRadioButton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setTitle("Duplication d'un document de ventes");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(470, 270));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setOpaque(false);
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbMessageDuplication ----
        lbMessageDuplication.setText("Vous souhaitez duppliquer le devis num\u00e9ro ");
        lbMessageDuplication.setName("lbMessageDuplication");
        pnlContenu.add(lbMessageDuplication, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbMessageAction ----
        lbMessageAction.setText("Souhaitez vous :");
        lbMessageAction.setName("lbMessageAction");
        pnlContenu.add(lbMessageAction, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbConserverPrix ----
        rbConserverPrix.setText("Conserver les prix du document d\u2019origine");
        rbConserverPrix.setPreferredSize(new Dimension(250, 30));
        rbConserverPrix.setMinimumSize(new Dimension(250, 30));
        rbConserverPrix.setMaximumSize(new Dimension(250, 30));
        rbConserverPrix.setName("rbConserverPrix");
        rbConserverPrix.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            rbConserverPrixActionPerformed(e);
          }
        });
        pnlContenu.add(rbConserverPrix, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbRecalculerPrix ----
        rbRecalculerPrix.setText("Recalculer les prix sur le nouveau document ");
        rbRecalculerPrix.setMinimumSize(new Dimension(250, 30));
        rbRecalculerPrix.setMaximumSize(new Dimension(250, 30));
        rbRecalculerPrix.setPreferredSize(new Dimension(250, 30));
        rbRecalculerPrix.setName("rbRecalculerPrix");
        rbRecalculerPrix.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            rbRecalculerPrixActionPerformed(e);
          }
        });
        pnlContenu.add(rbRecalculerPrix, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    // ---- btgMode ----
    btgMode.add(rbConserverPrix);
    btgMode.add(rbRecalculerPrix);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNLabelTitre lbMessageDuplication;
  private SNLabelTitre lbMessageAction;
  private SNRadioButton rbConserverPrix;
  private SNRadioButton rbRecalculerPrix;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
