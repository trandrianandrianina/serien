/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

/**
 * Session maitre de la configuration des catalogues
 */
public class SpSession extends SessionJava {
  /**
   * Constructeur.
   */
  public SpSession(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleConfigurationCatalogue modele = new ModeleConfigurationCatalogue(this);
    VueConfigurationCatalogue vue = new VueConfigurationCatalogue(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
  }
}
