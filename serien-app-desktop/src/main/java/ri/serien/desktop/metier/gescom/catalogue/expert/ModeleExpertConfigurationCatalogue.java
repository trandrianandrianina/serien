/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue.expert;

import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampCatalogue;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modele de la vue Expert d'une zone Série N d'une configuration de catalogue
 */
public class ModeleExpertConfigurationCatalogue extends AbstractModeleDialogue {
  public static final String TITRE = "Détail de la configuration de catalogue";
  
  // Variables
  private ChampERP champERP = null;
  private ChampERP champERPinitial = null;
  private ListeChampCatalogue listeChampCatalogue = null;
  
  /**
   * Constructeur.
   */
  public ModeleExpertConfigurationCatalogue(SessionBase pSession, ChampERP pChampERP) {
    super(pSession);
    champERP = pChampERP;
    champERPinitial = champERP.clone();
  }
  
  // -- Méthodes publiques
  @Override
  public void initialiserDonnees() {
    
  }
  
  /**
   * Récupérer les informations nécessaires.
   */
  @Override
  public void chargerDonnees() {
  }
  
  /**
   * Contrôler si la configuration active a été modifiée par rapport à la configuration initiale
   */
  public boolean isZoneERPmodifiee() {
    
    return !ChampERP.equalsComplet(champERP, champERPinitial);
  }
  
  /**
   * On modifie le premier champ Catalogue de la liste des champs catalogue associés au champ ERP
   */
  public void modifierPremiereZoneCatalogue(ChampCatalogue pChampCatalogue) {
    int nbChampsCatalogue = -1;
    if (champERP.getListeChampCatalogue() != null) {
      nbChampsCatalogue = champERP.getListeChampCatalogue().size();
    }
    
    if (pChampCatalogue != null) {
      pChampCatalogue.setDebutDecoupe(1);
      pChampCatalogue.setFinDecoupe(champERP.getLongueur());
    }
    
    if (nbChampsCatalogue == -1) {
      ListeChampCatalogue listeChampsCatalogue = new ListeChampCatalogue();
      champERP.setListeChampCatalogue(listeChampsCatalogue);
      nbChampsCatalogue = 0;
    }
    
    if (nbChampsCatalogue == 0 && pChampCatalogue != null) {
      champERP.getListeChampCatalogue().add(pChampCatalogue.clone());
    }
    else if (nbChampsCatalogue == 1) {
      ListeChampCatalogue listeChampsCatalogue = new ListeChampCatalogue();
      if (pChampCatalogue != null) {
        listeChampsCatalogue.add(pChampCatalogue.clone());
      }
      champERP.setListeChampCatalogue(listeChampsCatalogue);
    }
    else if (nbChampsCatalogue == 2) {
      ChampCatalogue champ2 = champERP.getListeChampCatalogue().get(1);
      ListeChampCatalogue listeChampsCatalogue = new ListeChampCatalogue();
      if (pChampCatalogue != null) {
        listeChampsCatalogue.add(pChampCatalogue.clone());
      }
      listeChampsCatalogue.add(champ2.clone());
      champERP.setListeChampCatalogue(listeChampsCatalogue);
    }
    rafraichir();
  }
  
  /**
   * On modifie le deuxième champ catalogue de la liste des champs catalogue associés au champ ERP
   */
  public void modifierDeuxiemeZoneCatalogue(ChampCatalogue pChampCatalogue) {
    int nbChampsCatalogue = -1;
    if (champERP.getListeChampCatalogue() != null) {
      nbChampsCatalogue = champERP.getListeChampCatalogue().size();
    }
    
    if (pChampCatalogue != null) {
      pChampCatalogue.setDebutDecoupe(1);
      pChampCatalogue.setFinDecoupe(champERP.getLongueur());
    }
    
    if (nbChampsCatalogue == -1) {
      ListeChampCatalogue listeChampsCatalogue = new ListeChampCatalogue();
      champERP.setListeChampCatalogue(listeChampsCatalogue);
      nbChampsCatalogue = 0;
    }
    if (pChampCatalogue != null && (nbChampsCatalogue == 0 || nbChampsCatalogue == 1)) {
      champERP.getListeChampCatalogue().add(pChampCatalogue.clone());
    }
    else if (nbChampsCatalogue == 1) {
      if (pChampCatalogue != null) {
        champERP.getListeChampCatalogue().add(pChampCatalogue.clone());
      }
    }
    else if (nbChampsCatalogue == 2) {
      ChampCatalogue champ1 = champERP.getListeChampCatalogue().get(0);
      ListeChampCatalogue listeChampsCatalogue = new ListeChampCatalogue();
      listeChampsCatalogue.add(champ1.clone());
      if (pChampCatalogue != null) {
        listeChampsCatalogue.add(pChampCatalogue.clone());
      }
      champERP.setListeChampCatalogue(listeChampsCatalogue);
    }
    rafraichir();
  }
  
  /**
   * Sauver en base de données la zone ERP et ses associations à des champs catalogue dans la configuration catalogue
   */
  @Override
  public void quitterAvecValidation() {
    if (champERP == null) {
      throw new MessageErreurException("Aucune zone ERP ne peut pas être sauvegardée");
    }
    
    int longueur = -1;
    int longueurZone1 = 0;
    int longueurZone2 = 0;
    
    if (champERP.getLongueur() != null) {
      longueur = champERP.getLongueur();
    }
    
    if (champERP.getListeChampCatalogue() != null) {
      ListeChampCatalogue listeChampsCatalogue = champERP.getListeChampCatalogue();
      if (listeChampsCatalogue.size() > 0) {
        ChampCatalogue champCatalogue = listeChampsCatalogue.get(0);
        int debutDecoupe = -1;
        int finDecoupe = -1;
        if (champCatalogue.getDebutDecoupe() == null) {
          throw new MessageErreurException("Il manque une valeur pour le début de découpe de la zone 1");
        }
        debutDecoupe = champCatalogue.getDebutDecoupe();
        if (champCatalogue.getFinDecoupe() == null) {
          throw new MessageErreurException("Il manque une valeur pour la fin de découpe de la zone 1");
        }
        finDecoupe = champCatalogue.getFinDecoupe();
        
        if (finDecoupe < debutDecoupe || finDecoupe < 1) {
          throw new MessageErreurException("La valeur de fin pour la découpe de la zone 1 est incorrecte");
        }
        
        longueurZone1 = (finDecoupe - debutDecoupe) + 1;
        
        if (longueurZone1 > longueur) {
          throw new MessageErreurException(
              "La longueur de la découpe est supérieure à la longueur maximale de la zone " + champERP.getId());
        }
      }
      
      if (listeChampsCatalogue.size() == 2) {
        ChampCatalogue champCatalogue = listeChampsCatalogue.get(1);
        int debutDecoupe = -1;
        int finDecoupe = -1;
        if (champCatalogue.getDebutDecoupe() == null) {
          throw new MessageErreurException("Il manque une valeur pour le début de découpe de la zone 2");
        }
        debutDecoupe = champCatalogue.getDebutDecoupe();
        if (champCatalogue.getFinDecoupe() == null) {
          throw new MessageErreurException("Il manque une valeur pour la fin de découpe de la zone 2");
        }
        finDecoupe = champCatalogue.getFinDecoupe();
        
        if (finDecoupe < debutDecoupe || finDecoupe < 1) {
          throw new MessageErreurException("La valeur de fin pour la découpe de la zone 2 est incorrecte");
        }
        
        longueurZone2 = (finDecoupe - debutDecoupe) + 1;
        
        if (longueurZone1 + longueurZone2 > longueur) {
          throw new MessageErreurException(
              "La longueur de la découpe des deux zones est supérieure à la longueur maximale de la zone " + champERP.getId());
        }
      }
    }
    else {
      throw new MessageErreurException("Il n'y a pas d'association valide de champ catalogue à ce champ ERP");
    }
    
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    champERP = champERPinitial.clone();
    super.quitterAvecAnnulation();
  }
  
  /**
   * Modifier la valeur de début de découpage de la première zone catalogue
   */
  public void modifierDecoupageDebutZone1(String pDebut) {
    Integer debut = 1;
    if (pDebut == null || pDebut.trim().isEmpty()) {
      return;
    }
    
    debut = Integer.parseInt(pDebut);
    
    if (champERP.getListeChampCatalogue() == null || champERP.getListeChampCatalogue().size() == 0) {
      return;
    }
    
    champERP.getListeChampCatalogue().get(0).setDebutDecoupe(debut);
    rafraichir();
  }
  
  /**
   * Modifier la valeur de début de découpage de la deuxième zone catalogue
   */
  public void modifierDecoupageDebutZone2(String pDebut2) {
    Integer debut = 1;
    if (pDebut2 == null || pDebut2.trim().isEmpty()) {
      return;
    }
    
    debut = Integer.parseInt(pDebut2);
    
    if (champERP.getListeChampCatalogue() == null || champERP.getListeChampCatalogue().size() < 2) {
      return;
    }
    
    champERP.getListeChampCatalogue().get(1).setDebutDecoupe(debut);
    rafraichir();
  }
  
  /**
   * Modifier la valeur de fin de découpage de la première zone catalogue
   */
  public void modifierDecoupageFinZone1(String pFin) {
    Integer fin = champERP.getLongueur();
    if (pFin == null || pFin.trim().isEmpty()) {
      return;
    }
    
    fin = Integer.parseInt(pFin);
    Trace.info("fin : " + fin);
    
    if (champERP.getListeChampCatalogue() == null || champERP.getListeChampCatalogue().size() == 0) {
      return;
    }
    
    champERP.getListeChampCatalogue().get(0).setFinDecoupe(fin);
    rafraichir();
  }
  
  /**
   * Modifier la valeur de fin de découpage de la deuxième zone catalogue
   */
  public void modifierDecoupageFinZone2(String pFin2) {
    Integer fin = champERP.getLongueur();
    if (pFin2 == null || pFin2.trim().isEmpty()) {
      return;
    }
    
    fin = Integer.parseInt(pFin2);
    
    if (champERP.getListeChampCatalogue() == null || champERP.getListeChampCatalogue().size() < 2) {
      return;
    }
    
    champERP.getListeChampCatalogue().get(1).setFinDecoupe(fin);
    rafraichir();
  }
  
  /**
   * Modifier le champ ERP sur lequel on va appliquer la formule d'association avec les champs catalogue
   */
  public void setChampERP(ChampERP champERP) {
    this.champERP = champERP;
  }
  
  /**
   * Récupérer le champ ERP sur lequel on va appliquer la formule d'association avec les champs catalogue
   */
  public ChampERP getChampERP() {
    return champERP;
  }
  
  /**
   * Modifier la liste des champs catalogue du fichier CSV disponibles pour une association avec le champ ERP
   */
  public void setListeChampCatalogue(ListeChampCatalogue listeChampCatalogue) {
    this.listeChampCatalogue = listeChampCatalogue;
  }
  
  /**
   * Retourner la liste des champs catalogue du fichier CSV disponibles pour une association avec le champ ERP
   */
  public ListeChampCatalogue getListeChampCatalogue() {
    return listeChampCatalogue;
  }
  
}
