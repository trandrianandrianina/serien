/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.extractiondocument;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map.Entry;

import ri.serien.desktop.metier.gescom.commun.commentaires.ModeleDetailCommentaire;
import ri.serien.desktop.metier.gescom.commun.commentaires.VueDetailCommentaire;
import ri.serien.desktop.metier.gescom.commun.consultationlignevente.ModeleConsultationLigneVente;
import ri.serien.desktop.metier.gescom.commun.consultationlignevente.VueConsultationLigneVente;
import ri.serien.desktop.metier.gescom.comptoir.InformationsExtraction;
import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.StockComptoir;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumOrigineDocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Expérimentation d'un modèle plus propre de modèle/vue - 10/10/2017
 * Ce dernière amène une structure un peu plus "rigide" et sutout le modèle n'implémente plus de classe Swing,
 * il est totalement indépendant de la vue.
 */
public class ModeleExtractionDocument extends AbstractModeleDialogue {
  // Constantes
  public static final int AUCUNE_VALEUR = 0;
  public static final int EXTRACTION_AVEC_RELIQUAT = 1;
  public static final int EXTRACTION_SANS_RELIQUAT = 2;
  public static final int DUPLICATION = 3;
  
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_GESTION_AVEC_RELIQUAT = 1;
  public static final int FOCUS_GESTION_SANS_RELIQUAT = 2;
  public static final int FOCUS_LISTE_ARTICLES = 3;
  
  // Variables
  private NumberFormat nombreDecimal = NumberFormat.getInstance(Locale.FRANCE);
  private int composantAyantLeFocus = FOCUS_NON_INDIQUE;
  private ModeleComptoir modeleParent = null;
  private Date dateTraitement = null;
  private DocumentVente documentVenteOrigine = null;
  private DocumentVente documentVenteSelectionne = null;
  private DocumentVente documentVenteNouveau = null;
  private InformationsExtraction informationsExtraction = null;
  private ListeLigneVente listeLigneVenteOrigine = null;
  private ListeLigneVente listeLigneVenteSelectionnee = null;
  private ListeLigneVente listeLigneVenteNouveau = null;
  private ListeLigneVente listeLigneVenteNouveauSauvegarde = null;
  private int gestionReliquats = EXTRACTION_AVEC_RELIQUAT;
  private String questionMiseAJourPrix = "";
  private String referenceLongue = "";
  private ArrayList<BigDecimal> quantitesRestantesAAfficher = new ArrayList<BigDecimal>();
  private boolean avecRecalculPrix = false;
  private BigDecimal montantTTCLignesExtraites = BigDecimal.ZERO;
  private IdLigneVente idLigneVenteAncienne = null;
  
  /**
   * Constructeur.
   */
  public ModeleExtractionDocument(SessionBase pSession, ModeleComptoir pModeleParent, Date pDateTraitement,
      InformationsExtraction pInformationsExtraction) {
    super(pSession);
    modeleParent = pModeleParent;
    dateTraitement = pDateTraitement;
    informationsExtraction = pInformationsExtraction;
    documentVenteSelectionne = informationsExtraction.getDocumentVenteSource();
    // Le document d'origine correspond à la version originale du document sélectionné
    if (documentVenteOrigine == null) {
      documentVenteOrigine = informationsExtraction.getDocumentVenteSource();
    }
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    if (documentVenteSelectionne == null) {
      quitterAvecAnnulation();
      return;
    }
    
    // Création du nouveau document à partir du document sélectionné
    documentVenteNouveau = documentVenteSelectionne.clone();
    switch (informationsExtraction.getTypeExtraction()) {
      case EXTRACTION_DEVIS_EN_COMMANDE:
        documentVenteNouveau.setTypeDocumentVente(EnumTypeDocumentVente.COMMANDE);
        setTitreEcran(String.format("Extraction du devis %d en commande", documentVenteOrigine.getId().getNumero()));
        referenceLongue = "Devis origine " + documentVenteOrigine.getId().getNumero();
        questionMiseAJourPrix =
            "La date de validité du devis est dépassée.\n" + "Souhaitez-vous conserver les prix du devis pour la nouvelle commande ?";
        break;
      
      case EXTRACTION_DEVIS_EN_BON:
        documentVenteNouveau.setTypeDocumentVente(EnumTypeDocumentVente.BON);
        setTitreEcran(String.format("Extraction du devis %d en bon", documentVenteOrigine.getId().getNumero()));
        referenceLongue = "Devis origine " + documentVenteOrigine.getId().getNumero();
        questionMiseAJourPrix =
            "La date de validité du devis est dépassée.\n" + "Souhaitez-vous conserver les prix du devis pour le nouveau bon ?";
        break;
      
      case EXTRACTION_DEVIS_EN_FACTURE:
        documentVenteNouveau.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
        setTitreEcran(String.format("Extraction du devis %d en facture", documentVenteOrigine.getId().getNumero()));
        referenceLongue = "Devis origine " + documentVenteOrigine.getId().getNumero();
        questionMiseAJourPrix =
            "La date de validité du devis est dépassée.\n" + "Souhaitez-vous conserver les prix devis pour la nouvelle facture ?";
        break;
      
      case EXTRACTION_COMMANDE_EN_BON:
        documentVenteNouveau.setTypeDocumentVente(EnumTypeDocumentVente.BON);
        setTitreEcran(String.format("Extraction de la commande %d en bon", documentVenteOrigine.getId().getNumero()));
        referenceLongue = "Commande origine " + documentVenteOrigine.getId().getNumero();
        questionMiseAJourPrix = "La date de validité de la commande est dépassée.\n"
            + "Souhaitez-vous conserver les prix de la commande pour le nouveau bon ?";
        break;
      
      case EXTRACTION_COMMANDE_EN_FACTURE:
        documentVenteNouveau.setTypeDocumentVente(EnumTypeDocumentVente.FACTURE);
        setTitreEcran(String.format("Extraction de la commande %d en facture", documentVenteOrigine.getId().getNumero()));
        referenceLongue = "Commande origine " + documentVenteOrigine.getId().getNumero();
        questionMiseAJourPrix = "La date de validité de la commande est dépassée.\n"
            + "Souhaitez-vous conserver les prix de la commande pour la nouvelle facture ?";
        break;
      
      default:
        throw new MessageErreurException("Le type d'extraction suivant n'est pas géré : " + informationsExtraction.getTypeExtraction());
    }
    documentVenteNouveau.setIdOrigine(documentVenteNouveau.getId());
    documentVenteNouveau.setId(IdDocumentVente.getInstanceAvecCreationId(documentVenteSelectionne.getId().getIdEtablissement(),
        EnumCodeEnteteDocumentVente.COMMANDE_OU_BON));
    
    // Suppression de l'article remise de pied s'il existe
    listeLigneVenteOrigine = supprimerArticlesIndesirables(documentVenteOrigine);
    listeLigneVenteSelectionnee = supprimerArticlesIndesirables(documentVenteSelectionne);
    listeLigneVenteNouveau = supprimerArticlesIndesirables(documentVenteNouveau);
    
    // Chargement des regroupements (TODO à déplacer dans la classe DocumentVente)
    documentVenteSelectionne.actualiserRegroupement(getIdSession());
    
    // Insertion de toutes les lignes d'origine afin d'avoir le rendu du document d'origine
    insererLignesArticlesQuantitesAZero();
    // On initialise la liste des quantités restantes à afficher (par défaut ce sont les quantités restantes réelles)
    initialiserQuantitesRestantes(false);
    
    composantAyantLeFocus = FOCUS_GESTION_AVEC_RELIQUAT;
  }
  
  @Override
  public void quitterAvecValidation() {
    if (!controlerValiditeSaisie(true)) {
      listeLigneVenteNouveau = listeLigneVenteNouveauSauvegarde;
      return;
    }
    if (!extractionDesLignes()) {
      listeLigneVenteNouveau = listeLigneVenteNouveauSauvegarde;
      return;
    }
    
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Contrôle s'il est possible de valider la boite de dialogue.
   */
  public boolean isValidationPossible() {
    return gestionReliquats != AUCUNE_VALEUR;
  }
  
  /**
   * Modifier la quantité à extraire.
   */
  public void modifierQuantiteAExtraire(BigDecimal[] pQuantitesSaisies, BigDecimal[] pQuantitesOrigines) {
    if (pQuantitesSaisies.length > listeLigneVenteNouveau.size()) {
      return;
    }
    // Sauvegarde de la liste avant le traitement afin de pouvoir recommencer le tratiement si néessaire
    listeLigneVenteNouveauSauvegarde = listeLigneVenteNouveau.clone();
    
    // Cas particulier des regroupements, si une ligne d'un regroupement a été sélectionnée il faut mettre 1 dans la quantité de l'entête
    // et de pied du regroupement afin de les extraire aussi
    ArrayList<IdLigneVente> listeIdEntetePied = new ArrayList<IdLigneVente>();
    HashMap<Character, Regroupement> liste = (HashMap<Character, Regroupement>) documentVenteNouveau.getListeRegroupements();
    // On recherche les lignes appartenants à un regroupement
    for (int ligne = 0; ligne < pQuantitesSaisies.length; ligne++) {
      if (pQuantitesSaisies[ligne].compareTo(BigDecimal.ZERO) > 0) {
        LigneVente ligneVente = listeLigneVenteNouveau.get(ligne);
        if (ligneVente.isLigneRegroupee() && (!ligneVente.isDebutRegroupement() || !ligneVente.isFinRegroupement())) {
          Regroupement regroupement = liste.get(ligneVente.getCodeRegroupement());
          IdLigneVente idDebut = regroupement.getIdLigneVenteDebut();
          IdLigneVente idFin = regroupement.getIdLigneVenteFin();
          if (!listeIdEntetePied.contains(idDebut)) {
            listeIdEntetePied.add(idDebut);
            listeIdEntetePied.add(idFin);
          }
        }
      }
    }
    // On parcourt la liste des ids des entêtes et de pied afin de mettre 1 dans la quantité pour qu'elles soient extraites
    for (int ligne = 0; ligne < pQuantitesSaisies.length; ligne++) {
      LigneVente ligneVente = listeLigneVenteNouveau.get(ligne);
      // On passe les lignes qui ont une quantité supérieure à zero, si leur id vaut null ou s'il s'agit d'une ligne commentaire
      if (pQuantitesSaisies[ligne].compareTo(BigDecimal.ZERO) > 0 || ligneVente.getId() == null) {
        continue;
      }
      // On modifie la quantité de la ligne si elle fait partie de la liste des id
      if (listeIdEntetePied.contains(ligneVente.getId())) {
        pQuantitesSaisies[ligne] = BigDecimal.ONE;
      }
    }
    
    // Traitement final des lignes
    for (int ligne = 0; ligne < pQuantitesSaisies.length; ligne++) {
      // Calcule la quantité en unité de vente pour avoir le bon montant par ligne
      LigneVente ligneVente = listeLigneVenteNouveau.get(ligne);
      // Si la quantité est égale à zéro alors on marque cette ligne afin qu'elle soit supprimée lors de la validation des lignes
      if (pQuantitesSaisies[ligne].compareTo(BigDecimal.ZERO) == 0) {
        listeLigneVenteNouveau.set(ligne, null);
        continue;
      }
      
      // Dans le cas où une quantité a été saisi
      // On contrôle que la valeur saisie ne soit pas supérieure à la quantité restant à prendre
      BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(getQuantiteUCV(listeLigneVenteNouveau, ligne));
      if (pQuantitesSaisies[ligne].compareTo(quantite) > 0) {
        pQuantitesSaisies[ligne] = quantite;
      }
      else if (pQuantitesSaisies[ligne].compareTo(BigDecimal.ZERO) < 0) {
        pQuantitesSaisies[ligne] = BigDecimal.ZERO;
      }
      
      // On traite la quantité de manière s'il s'agit d'un article découpable ou d'un article normal
      if (ligneVente.isArticleDecoupable()) {
        Article article = ManagerServiceArticle.lireArticle(getSession().getIdSession(), ligneVente.getIdArticle());
        ligneVente.setQuantiteArticlesDecoupe(pQuantitesSaisies[ligne], article);
      }
      else {
        ligneVente.setQuantiteUCV(pQuantitesSaisies[ligne]);
      }
      
      // Stockage de la quantité d'origine pour l'onglet Article
      informationsExtraction.ajouterLigne(ligneVente.getId(), pQuantitesOrigines[ligne]);
      
      // Calcul du montant des lignes que l'on est en train d'extraire
      if (ligneVente.getMontantTTC() != null) {
        montantTTCLignesExtraites = montantTTCLignesExtraites.add(ligneVente.getMontantTTC());
      }
    }
  }
  
  /**
   * Prise en compte des reliquats.
   */
  public void modifierGestionAvecReliquats() {
    if (gestionReliquats == EXTRACTION_AVEC_RELIQUAT) {
      // Rafraîchir pour que le bouton radio ne s'efface pas si on utilise le reaccourci clavier
      rafraichir();
      return;
    }
    gestionReliquats = EXTRACTION_AVEC_RELIQUAT;
    composantAyantLeFocus = FOCUS_GESTION_AVEC_RELIQUAT;
    rafraichir();
  }
  
  /**
   * Sans prise en compte des reliquats.
   */
  public void modifierGestionSansReliquats() {
    if (gestionReliquats == EXTRACTION_SANS_RELIQUAT) {
      // Rafraîchir pour que le bouton radio ne s'efface pas si on utilise le raccourci clavier
      rafraichir();
      return;
    }
    gestionReliquats = EXTRACTION_SANS_RELIQUAT;
    composantAyantLeFocus = FOCUS_GESTION_SANS_RELIQUAT;
    rafraichir();
  }
  
  /**
   * Effacer les quantites restantes à afficher: il s'agit d'une mise à zéro forcée.
   */
  public void effacerQuantites() {
    initialiserQuantitesRestantes(true);
    rafraichir();
  }
  
  /**
   * Restaurer les quantites restantes à afficher: il s'agit des valeurs réelles.
   */
  public void restaurerQuantites() {
    initialiserQuantitesRestantes(false);
    rafraichir();
  }
  
  /**
   * Formate et retourne la quantité d'origine d'une ligne de vente.
   */
  public String getQuantiteOrigine(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return "";
    }
    return Constantes.formater(pLigneVente.getQuantiteDocumentOrigineUC(), false);
  }
  
  /**
   * Formate et retourne la quantité déjà extraite d'une ligne de vente.
   */
  public String getQuantiteDejaExtraite(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return "";
    }
    return Constantes.formater(pLigneVente.getQuantiteDejaExtraiteUC(), false);
  }
  
  /**
   * Converti les quantités en unité de conditionnnement de vente.
   */
  public String getQuantiteUCV(ListeLigneVente pListeLigneVente, int pIndiceLigne) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= pListeLigneVente.size()) {
      return "0";
    }
    
    LigneVente lignevente = pListeLigneVente.get(pIndiceLigne);
    if (lignevente == null) {
      return "0";
    }
    // La ligne de vente est une ligne commentaire
    if (lignevente.isLigneCommentaire()) {
      if ((documentVenteNouveau.isFacture() && lignevente.getTopPersonnalisation5().isEmpty())) {
        return "0";
      }
      return "1";
    }
    // Dans les autres cas
    return Constantes.formater(lignevente.getQuantiteUCV(), false);
  }
  
  /**
   * Retourne la quantité restant à prendre pour une ligne d'un indice donné.
   */
  public BigDecimal getQuantiteRestante(int pIndiceLigne) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return BigDecimal.ZERO;
    }
    
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    if (ligneVente == null || ligneVente.getId() == null) {
      return BigDecimal.ZERO;
    }
    // La ligne de vente est une ligne commentaire
    if (ligneVente.isLigneCommentaire()) {
      if ((documentVenteNouveau.isFacture() && ligneVente.isEditionSurFacture())) {
        return BigDecimal.ZERO;
      }
      return BigDecimal.ONE;
    }
    // La ligne de vente correspond à un article découpable
    if (ligneVente.isArticleDecoupable()) {
      return BigDecimal.valueOf(ligneVente.getNombreDecoupe());
    }
    // Dans les autres cas
    return ligneVente.getQuantiteDocumentOrigineUC().subtract(ligneVente.getQuantiteDejaExtraiteUC());
  }
  
  /**
   * Retourne la quantité restant à prendre.
   */
  public String getQuantiteRestanteAAfficher(int pIndiceLigne) {
    if (quantitesRestantesAAfficher.isEmpty() || pIndiceLigne >= quantitesRestantesAAfficher.size()) {
      return "";
    }
    return Constantes.formater(quantitesRestantesAAfficher.get(pIndiceLigne), false);
  }
  
  /**
   * Retourne la quantité déjà commandée.
   */
  public String getQuantiteDejaCommandee(int pIndiceLigne) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return "0";
    }
    
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    return Constantes.formater(ligneVente.getQuantiteDejaExtraiteUC(), false);
  }
  
  /**
   * Retourne la quantité d'origine.
   */
  public String getQuantiteOrigine(int pIndiceLigne) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return "0";
    }
    
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    return Constantes.formater(ligneVente.getQuantiteDocumentOrigineUC(), false);
  }
  
  /**
   * Retourne la quantité disponible à la vente.
   */
  public String getQuantiteDisponible(int pIndiceLigne) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return "0";
    }
    
    // Appel au service stock afin de récupérer le stock détaillé pour cet article
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    StockDetaille stockDetaille = ManagerServiceStock.chargerStockDetaille(getIdSession(), ligneVente.getId().getIdEtablissement(),
        getModeleParent().getDocumentVenteEnCours().getIdMagasin(), ligneVente.getIdArticle());
    if (stockDetaille != null && stockDetaille.getStockDisponible() != null) {
      return Constantes.formater(stockDetaille.getStockDisponible(), false);
    }
    
    return "0";
  }
  
  /**
   * Afficher la fenetre présentant les lignes liées à la ligne sélectionnée.
   */
  public void afficherDetail(int pIndiceLigne) {
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    // Si c'est une ligne commentaire
    if (ligneVente.isLigneCommentaire()) {
      ModeleDetailCommentaire modeleCommentaire = ModeleDetailCommentaire.getInstanceConsulterLigneDocument(getSession(), ligneVente);
      VueDetailCommentaire vue = new VueDetailCommentaire(modeleCommentaire);
      vue.afficher();
    }
    // Autres lignes
    else {
      ModeleConsultationLigneVente modele = new ModeleConsultationLigneVente(getSession(), documentVenteOrigine, ligneVente);
      VueConsultationLigneVente vue = new VueConsultationLigneVente(modele);
      vue.afficher();
    }
  }
  
  /**
   * Le choix "Conserver les prix du document d'’origine" est coché si la date de validité du devis n’'est pas dépassée. Le choix
   * "Recalculer les prix sur le nouveau document" est coché si la date de validité du devis est dépassée. L’utilisateur peut changer ce
   * choix s'’il le souhaite.
   */
  public boolean isRecalculParDefaut() {
    Date dateValidite = null;
    if (documentVenteSelectionne.isDevis()) {
      dateValidite = documentVenteSelectionne.getDateValiditeDevis();
    }
    return (dateValidite != null && dateValidite.before(new Date()));
  }
  
  /**
   * Retourne le montant en TTC de l'acompte qu'il reste à consommé à la suite des différentes extractions.
   */
  public BigDecimal getMontantAcompteTTCRestantAConsommer() {
    if (documentVenteOrigine == null) {
      return null;
    }
    ListeReglement listeReglement = documentVenteOrigine.getListeReglement();
    if (!documentVenteOrigine.isCommande() || listeReglement == null || listeReglement.getMontantAcompteRegle() == null) {
      return null;
    }
    BigDecimal montantAcompte = listeReglement.getMontantAcompteRegle();
    BigDecimal montantTotalExtrait = BigDecimal.ZERO;
    
    // Récupération de tous les documents extraits de cette commande à partir des lignes
    List<IdDocumentVente> listeIdDocumentVenteExtraits = new ArrayList<IdDocumentVente>();
    for (LigneVente ligneVente : documentVenteOrigine.getListeLigneVente()) {
      // Création du lien d'origine (la commande d'origine)
      IdLienLigne idLienLigne =
          IdLienLigne.getInstanceVente(documentVenteOrigine.getId().getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_VENTE,
              documentVenteOrigine.getId().getEntete(), documentVenteOrigine.getId().getNumero(), ligneVente.getId().getSuffixe(), null,
              ligneVente.getId().getNumeroLigne(), documentVenteOrigine.getId(), ligneVente.getId().getNumeroLigne(), false);
      
      LienLigne lienOrigine = new LienLigne(idLienLigne);
      lienOrigine.setDocumentVente(documentVenteOrigine);
      lienOrigine.setLigne(ligneVente);
      
      // Chargement des liens (des documents) pour la ligne en cours
      ListeLienLigne listeLienLigne =
          ListeLienLigne.chargerTout(getSession().getIdSession(), lienOrigine.getId(), modeleParent.getIdMagasin());
      if (listeLienLigne == null || listeLienLigne.isEmpty()) {
        continue;
      }
      // Parcourt de la liste des liens pour faire la somme du total de chaque document déjà extrait
      for (LienLigne lienLigne : listeLienLigne) {
        if (lienLigne == null || lienLigne.getDocumentVente() == null
            || lienLigne.getDocumentVente().getId().equals(documentVenteOrigine.getId())) {
          continue;
        }
        IdDocumentVente id = lienLigne.getDocumentVente().getId();
        // Petit contrôle pour s'assurer que le document extrait n'est pas déjà dans la liste
        if (!listeIdDocumentVenteExtraits.contains(id)) {
          DocumentVente documentVente = lienLigne.getDocumentVente();
          // Calcul du montant total des documents extraits liés à la commande
          if (documentVente != null) {
            montantTotalExtrait = montantTotalExtrait.add(documentVente.getTotalTTC());
          }
          listeIdDocumentVenteExtraits.add(id);
        }
      }
    }
    
    // Calcul du montant de l'acompte restant
    return montantAcompte.subtract(montantTotalExtrait);
  }
  
  /**
   * Retourne la quantité disponible par magasin.
   */
  public BigDecimal getQuantiteDisponibleMagasin(IdArticle pIdArticle) {
    // Chargement du stock pour l'article
    ListeStockComptoir listeStock = ListeStockComptoir.chargerPourArticle(getIdSession(), null, pIdArticle.getCodeArticle());
    if (listeStock == null) {
      return null;
    }
    
    // Recherche du stock pour le magasin en cours
    for (StockComptoir stock : listeStock) {
      if (Constantes.equals(stock.getId().getIdMagasin(), documentVenteSelectionne.getIdMagasin())) {
        return stock.getQuantiteDisponibleVente();
      }
    }
    return null;
  }
  
  // -- Méthodes privées
  
  /**
   * On créer une nouvelle liste de ligne article avec les lignes désirées.
   */
  private ListeLigneVente supprimerArticlesIndesirables(DocumentVente pDocumentVente) {
    ListeLigneVente listeLigneVenteSource = pDocumentVente.getListeLigneVente();
    ListeLigneVente listeLigneVenteResultat = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVenteSource) {
      if (!ligneVente.isLigneRemiseSpeciale()) {
        listeLigneVenteResultat.add(ligneVente);
      }
    }
    return listeLigneVenteResultat;
  }
  
  /**
   * On élimine de la liste les lignes articles dont l'id est à null.
   */
  private void supprimerLignesArticlesIndesirables(ListeLigneVente pListeLigneVente) {
    if (pListeLigneVente == null || pListeLigneVente.isEmpty()) {
      return;
    }
    
    int i = 0;
    while (i < pListeLigneVente.size()) {
      if (pListeLigneVente.get(i) == null) {
        pListeLigneVente.remove(i);
      }
      else {
        i++;
      }
    }
  }
  
  /**
   * Insère les lignes articles du document d'origine qui ont déjà été commandées (quantité à 0).
   */
  private void insererLignesArticlesQuantitesAZero() {
    // Duplication de la liste des lignes articles du document d'origine
    ListeLigneVente listeLigneVenteTemp = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVenteOrigine) {
      listeLigneVenteTemp.add(ligneVente.clone());
    }
    
    // Comparaison avec la liste du nouveau document
    for (int i = 0; i < listeLigneVenteTemp.size(); i++) {
      LigneVente ligneVente = trouverLigneArticle(listeLigneVenteTemp.get(i), listeLigneVenteNouveau);
      // On n'a pas trouvé la ligne article dans le nouveau document donc les quantités sont à zéro
      if (ligneVente == null) {
        listeLigneVenteTemp.get(i).setQuantiteDejaExtraite(listeLigneVenteTemp.get(i).getQuantiteUV());
        listeLigneVenteTemp.get(i).setQuantiteUCV(BigDecimal.ZERO);
      }
      // On a trouvé la ligne article dans le nouveau document donc les quantités sont égales à celles du nouveau document
      else {
        listeLigneVenteTemp.set(i, ligneVente);
      }
    }
    
    // On traite le cas des lignes commentaires appartenants à un regroupement, si toutes les lignes non commentaires ont des quantités
    // à zéro
    ArrayList<Character> listeRegroupementASupprimer = new ArrayList<Character>();
    for (Entry<Character, Regroupement> entry : documentVenteNouveau.getListeRegroupements().entrySet()) {
      boolean mettreAZeroRegroupement = true;
      // Première passe sur le regroupement en cours: on vérifie l'état du regroupement
      for (LigneVente ligne : entry.getValue().getListeLigneVente()) {
        if (!ligne.isLigneCommentaire() && ligne.getQuantiteUV().compareTo(BigDecimal.ZERO) > 0) {
          mettreAZeroRegroupement = false;
          break;
        }
      }
      if (mettreAZeroRegroupement) {
        listeRegroupementASupprimer.add(entry.getKey());
        // Deuxième passe sur le regroupement en cours: on met les id des lignes commentaires à null afin qu'il soit marqué comme déjà
        // pris
        for (LigneVente ligne : entry.getValue().getListeLigneVente()) {
          if (ligne.isLigneCommentaire()) {
            LigneVente ligneVente = trouverLigneArticle(ligne, listeLigneVenteTemp);
            if (ligneVente != null) {
              ligneVente.setQuantiteDejaExtraite(BigDecimal.ONE);
              ligneVente.setQuantiteUCV(BigDecimal.ZERO);
            }
          }
        }
      }
    }
    
    // Nettoyage
    listeLigneVenteNouveau.clear();
    listeLigneVenteNouveau = listeLigneVenteTemp;
    // On supprime les regroupements vides
    for (Character cleRegroupement : listeRegroupementASupprimer) {
      documentVenteNouveau.getListeRegroupements().remove(cleRegroupement);
    }
  }
  
  /**
   * Met un id donné à null et quantité à 0 d'une ligne de vente.
   */
  private void mettreIdANull(LigneVente pLigne) {
    if (pLigne == null) {
      return;
    }
    
    pLigne.setId(null);
    if (!pLigne.isLigneCommentaire()) {
      pLigne.setQuantiteDejaExtraite(pLigne.getQuantiteUV());
    }
    else {
      pLigne.setQuantiteDejaExtraite(BigDecimal.ONE);
    }
    pLigne.setQuantiteUCV(BigDecimal.ZERO);
  }
  
  /**
   * Initialise les quantités restantes des lignes articles affichées.
   */
  private void initialiserQuantitesRestantes(boolean pMiseAZero) {
    quantitesRestantesAAfficher.clear();
    if (pMiseAZero) {
      for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
        quantitesRestantesAAfficher.add(BigDecimal.ZERO);
      }
    }
    else {
      for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
        quantitesRestantesAAfficher.add(getQuantiteRestante(ligne));
      }
    }
  }
  
  /**
   * Recherche une ligne article donnée dans une liste donnée.
   */
  private LigneVente trouverLigneArticle(LigneVente pLigneVente, ListeLigneVente pListeLigneVente) {
    if (pListeLigneVente == null || pLigneVente == null || pLigneVente.getId() == null) {
      return null;
    }
    
    for (LigneVente ligneVente : pListeLigneVente) {
      if (ligneVente.getId() != null && ligneVente.getId().getNumeroLigne().compareTo(pLigneVente.getId().getNumeroLigne()) == 0) {
        return ligneVente;
      }
    }
    return null;
  }
  
  /**
   * Recherche une ligne article donnée àaprtir de son numéro de ligne dans une liste donnée.
   */
  private LigneVente trouverLigneArticle(IdLigneVente pId, ArrayList<LigneVente> pListeLigneVente) {
    if (pListeLigneVente == null || pId == null) {
      return null;
    }
    
    for (LigneVente ligneVente : pListeLigneVente) {
      if (ligneVente.getId().getNumeroLigne().compareTo(pId.getNumeroLigne()) == 0) {
        return ligneVente;
      }
    }
    return null;
  }
  
  /**
   * Controle la validité des informations saisies.
   */
  private boolean controlerValiditeSaisie(boolean pValidationFinale) {
    StringBuilder message = new StringBuilder();
    boolean retour = true;
    
    if (gestionReliquats == AUCUNE_VALEUR) {
      message.append("\n\t- le type d'extraction.");
    }
    
    // S'il y a un message d'erreur à afficher
    if (message.length() > 0) {
      message.insert(0, "Afin de pouvoir valider votre saisie, merci de renseigner :");
      throw new MessageErreurException(message.toString());
    }
    
    // Contrôle de la pertinence de l'activation de l'option "Sans reliquats" dans le cas d'une commande
    if (documentVenteOrigine.isCommande() && gestionReliquats == EXTRACTION_SANS_RELIQUAT) {
      BigDecimal montantAcompteRestant = getMontantAcompteTTCRestantAConsommer();
      if (montantAcompteRestant != null) {
        montantAcompteRestant = montantAcompteRestant.subtract(montantTTCLignesExtraites);
        // Si l'acompte n'a pas été totalement consommé un message d'avertissement est affiché afin que l'utilisateur puisse modifié sa
        // saisie s'il le souhaite
        if (montantAcompteRestant.signum() == 1) {
          boolean reponse = DialogueConfirmation.afficher(getSession(), "Confirmation de l'extraction en cours",
              "Vous avez sélectionné l'option \"Sans reliquat\", hors l'acompte n'a pas été complètement consommé (il reste "
                  + Constantes.formater(montantAcompteRestant, true) + " euros)."
                  + "\n\nSouhaitez-vous modifier votre saisie pour consommer entièrement l'acompte ?"
                  + "\n\nDans le cas contraire c'est l'option \"Avec reliquat\" qui sera activée pour cette extraction.");
          // L'utilisateur à choisi de modifier sa saisie
          if (reponse) {
            retour = false;
          }
          else {
            gestionReliquats = EXTRACTION_AVEC_RELIQUAT;
          }
        }
      }
    }
    
    return retour;
  }
  
  /**
   * Extraction des lignes pour le nouveau document.
   */
  private boolean extractionDesLignes() {
    // On fait un dernier nettoyage dans la liste des lignes article du nouveau document
    // Ce sont les lignes que l'on a pas sélectionnées car la quantité saisie valait zéro
    supprimerLignesArticlesIndesirables(listeLigneVenteNouveau);
    
    // Création du nouveau document
    documentVenteNouveau.setProfilCreation(ManagerSessionClient.getInstance().getEnvUser().getProfil());
    documentVenteNouveau = modeleParent.creerDocumentVente(documentVenteNouveau);
    if (documentVenteNouveau == null) {
      return false;
    }
    
    // Insertion des lignes articles dans le nouveau document
    for (LigneVente ligneVenteNouveau : listeLigneVenteNouveau) {
      // Mise à jour de l'Id des lignes vente du nouveau document
      IdLigneVente idLigneVenteNouvelle = IdLigneVente.getInstance(documentVenteNouveau.getId().getIdEtablissement(),
          documentVenteNouveau.getId().getCodeEntete(), documentVenteNouveau.getId().getNumero(),
          documentVenteNouveau.getId().getSuffixe(), ligneVenteNouveau.getId().getNumeroLigne());
      ligneVenteNouveau.setId(idLigneVenteNouvelle);
      
      idLigneVenteAncienne = getIdLigneVenteSelectionne(ligneVenteNouveau.getId().getNumeroLigne());
      
      // Dans le cas d'une ligne commentaire la quantité UV vaut 0 car il n'y a pas de prix, donc on force 1 pour être sur que le
      // commentaire soit enregistré dans le nouveau document
      BigDecimal quantite = BigDecimal.ONE;
      if (!ligneVenteNouveau.isLigneCommentaire()) {
        quantite = ligneVenteNouveau.getQuantiteUV();
      }
      
      ManagerServiceDocumentVente.sauverLigneVenteDocumentAvecExtraction(getIdSession(), idLigneVenteAncienne, idLigneVenteNouvelle,
          gestionReliquats, quantite, avecRecalculPrix);
    }
    
    // On met à jour la liste des lignes dans le nouveau document
    documentVenteNouveau.setListeLignesArticles(listeLigneVenteNouveau);
    
    return true;
  }
  
  /**
   * Retourne l'id de la ligne du document sélectionné correspondant à partir de son numéro de ligne.
   */
  private IdLigneVente getIdLigneVenteSelectionne(int pNumeroLigne) {
    for (LigneVente ligneVente : listeLigneVenteSelectionnee) {
      if (ligneVente.getId().getNumeroLigne().compareTo(pNumeroLigne) == 0) {
        return ligneVente.getId();
      }
    }
    return null;
  }
  
  // -- Accesseurs
  /**
   * 
   * Retourner le modèle appelant.
   * 
   * @return ModeleComptoir
   */
  public ModeleComptoir getModeleParent() {
    return modeleParent;
  }
  
  /**
   * 
   * Retourner l'état de gestion des reliquats.
   * 
   * @return int
   */
  public int getGestionReliquats() {
    return gestionReliquats;
  }
  
  /**
   * 
   * Modifier l'état de gestion des reliquats.
   * 
   * @param gestionReliquats
   */
  public void setGestionReliquats(int gestionReliquats) {
    this.gestionReliquats = gestionReliquats;
  }
  
  /**
   * 
   * Retourner la liste des lignes du document d'origine.
   * 
   * @return ListeLigneVente
   */
  public ListeLigneVente getListeLigneVenteOrigine() {
    return listeLigneVenteOrigine;
  }
  
  /**
   * 
   * Retourner la liste des lignes sélectionnées pour extraction.
   * 
   * @return ListeLigneVente
   */
  public ListeLigneVente getListeLigneVenteSelectionnee() {
    return listeLigneVenteSelectionnee;
  }
  
  /**
   * 
   * Retourner la liste des lignes à extraire dans le nouveau document.
   * 
   * @return ListeLigneVente
   */
  public ListeLigneVente getListeLigneVenteNouveau() {
    return listeLigneVenteNouveau;
  }
  
  /**
   * 
   * Retourner le document d'extraction.
   * 
   * @return DocumentVente
   */
  public DocumentVente getDocumentVenteNouveau() {
    return documentVenteNouveau;
  }
  
  /**
   * 
   * Retourner le message sur la mise à jour des prix sur le document extrait.
   * 
   * @return String
   */
  public String getQuestionMiseAJourPrix() {
    return questionMiseAJourPrix;
  }
  
  /**
   * 
   * Retourner la référence longue du document.
   * 
   * @return
   */
  public String getReferenceLongue() {
    return referenceLongue;
  }
  
  /**
   * 
   * Retourner le type d'extraction du document de vente.
   * 
   * @return
   */
  public EnumOrigineDocumentVente getTypeExtraction() {
    return informationsExtraction.getTypeExtraction();
  }
  
  /**
   * Composant ayant le focus.
   */
  public int getComposantAyantLeFocus() {
    return composantAyantLeFocus;
  }
  
  /**
   * Indique si les prix du document de ventes initial doivent être conservés ou non pour le document extrait.
   */
  public boolean isRecalculerPrix() {
    return avecRecalculPrix;
  }
  
  /**
   * Modifier si les prix du document de ventes initial doivent être conservés ou non pour le document extrait.
   */
  public void setRecalculerPrix(boolean isRecalculerPrix) {
    this.avecRecalculPrix = isRecalculerPrix;
  }
  
  /**
   * 
   * Retourner l'identifiant de la ligne d'origine.
   * 
   * @return IdLigneVente
   */
  public IdLigneVente getIdLigneVenteAncienne() {
    return idLigneVenteAncienne;
  }
  
}
