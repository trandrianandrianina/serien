/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat.detailligneachat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EtchedBorder;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.vente.documentvente.affichagelienligne.SNLienLigne;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne commentaire.
 */
public class VueDetailLigneAchat extends AbstractVueDialogue<ModeleDetailLigneAchat> {
  // Constantes
  private static final Font POLICE_TITRE = new Font("sansserif", Font.BOLD, 13);
  
  /**
   * Constructeur.
   */
  public VueDetailLigneAchat(ModeleDetailLigneAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
  }
  
  @Override
  public void rafraichir() {
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirQuantiteInitialeUCA();
    rafraichirQuantiteReliquatUCA();
    rafraichirQuantiteTraiteeUCA();
    rafraichirQuantiteInitialeUA();
    rafraichirQuantiteReliquatUA();
    rafraichirQuantiteTraiteeUA();
    rafraichirMontantInitialHT();
    rafraichirMontantReliquatHT();
    rafraichirMontantTraiteHT();
    rafraichirPanelDonneesInitiales();
    rafraichirPanelDonneesReliquats();
    rafraichirPanelDonneesTraitees();
    rafraichirPrixBrutHT();
    rafraichirPrixNetHT();
    rafraichirPort();
    rafraichirPrixRevientFournisseur();
    rafraichirMontant();
    // Liens
    rafraichirLiens();
  }
  
  // Méthodes privées
  
  private void rafraichirCodeArticle() {
    Article article = getModele().getArticle();
    if (article != null && article.getId().getCodeArticle() != null) {
      tfCodeArticle.setText(article.getId().getCodeArticle());
    }
    else {
      tfCodeArticle.setText("");
    }
  }
  
  private void rafraichirLibelleArticle() {
    Article article = getModele().getArticle();
    if (article != null && article.getLibelleComplet() != null) {
      tfLibelleArticle.setText(article.getLibelleComplet());
    }
    else {
      tfLibelleArticle.setText("");
    }
  }
  
  private void rafraichirQuantiteInitialeUCA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteInitialeUCA() != null) {
      tfQuantiteInitialeUCA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteInitialeUCA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUCA().getCode());
      tfQuantiteInitialeUCA.setVisible(true);
    }
    else {
      tfQuantiteInitialeUCA.setVisible(false);
    }
  }
  
  private void rafraichirQuantiteReliquatUCA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteReliquatUCA() != null) {
      tfQuantiteReliquatUCA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteReliquatUCA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUCA().getCode());
      tfQuantiteReliquatUCA.setVisible(true);
    }
    else {
      tfQuantiteReliquatUCA.setVisible(false);
    }
  }
  
  private void rafraichirQuantiteTraiteeUCA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteTraiteeUCA() != null) {
      tfQuantiteTraiteeUCA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteTraiteeUCA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUCA().getCode());
      tfQuantiteTraiteeUCA.setVisible(true);
    }
    else {
      tfQuantiteTraiteeUCA.setVisible(false);
    }
  }
  
  private void rafraichirQuantiteInitialeUA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteInitialeUA() != null) {
      tfQuantiteInitialeUA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteInitialeUA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUA().getCode());
      tfQuantiteInitialeUA.setVisible(true);
    }
    else {
      tfQuantiteInitialeUA.setVisible(false);
    }
  }
  
  private void rafraichirQuantiteReliquatUA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteReliquatUA() != null) {
      tfQuantiteReliquatUA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteReliquatUA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUA().getCode());
      tfQuantiteReliquatUA.setVisible(true);
    }
    else {
      tfQuantiteReliquatUA.setVisible(false);
    }
  }
  
  private void rafraichirQuantiteTraiteeUA() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getQuantiteTraiteeUA() != null) {
      tfQuantiteTraiteeUA.setText(Constantes.formater(ligneArticle.getPrixAchat().getQuantiteTraiteeUA(), false) + " "
          + ligneArticle.getPrixAchat().getIdUA().getCode());
      tfQuantiteTraiteeUA.setVisible(true);
    }
    else {
      tfQuantiteTraiteeUA.setVisible(false);
    }
  }
  
  private void rafraichirMontantInitialHT() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getMontantInitialHT() != null) {
      tfMontantInitialHT.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantInitialHT(), true));
      tfMontantInitialHT.setVisible(true);
    }
    else {
      tfMontantInitialHT.setVisible(false);
    }
  }
  
  private void rafraichirMontantReliquatHT() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getMontantReliquatHT() != null) {
      tfMontantReliquatHT.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantReliquatHT(), true));
      tfMontantReliquatHT.setVisible(true);
    }
    else {
      tfMontantReliquatHT.setVisible(false);
    }
  }
  
  private void rafraichirMontantTraiteHT() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getMontantTraiteHT() != null) {
      tfMontantTraiteHT.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantTraiteHT(), true));
      tfMontantTraiteHT.setVisible(true);
    }
    else {
      tfMontantTraiteHT.setVisible(false);
    }
  }
  
  private void rafraichirPanelDonneesInitiales() {
    // Les Libellés à adapter en fonction du type de document
    DocumentAchat documentAchat = getModele().getDocumentAchat();
    if (documentAchat != null) {
      if (documentAchat.isCommande()) {
        lbQuantiteInitialeUCA.setText("Quantité initiale (UCA) :");
        lbQuantiteInitialeUA.setText("Quantité initiale (UA) :");
        lbMontantInitialHT.setText("Montant initial HT :");
      }
      else if (documentAchat.isReception()) {
        lbQuantiteInitialeUCA.setText("Quantité réceptionnée (UCA) :");
        lbQuantiteInitialeUA.setText("Quantité réceptionnée (UA) :");
        lbMontantInitialHT.setText("Montant réceptionné HT :");
      }
      else if (documentAchat.isFacture()) {
        lbQuantiteInitialeUCA.setText("Quantité facturée (UCA) :");
        lbQuantiteInitialeUA.setText("Quantité facturée (UA) :");
        lbMontantInitialHT.setText("Montant facturé HT :");
      }
      
      // Adapter la police car le composant ne respecte pas la charte (il faut absoluement du gras ...)
      lbQuantiteInitialeUCA.setFont(POLICE_TITRE);
      lbQuantiteInitialeUA.setFont(POLICE_TITRE);
      lbMontantInitialHT.setFont(POLICE_TITRE);
    }
    
    // Visibilité du panel
    if (documentAchat != null) {
      pnlDonneesInitiales.setVisible(true);
    }
    else {
      pnlDonneesInitiales.setVisible(false);
    }
  }
  
  private void rafraichirPanelDonneesReliquats() {
    DocumentAchat documentAchat = getModele().getDocumentAchat();
    if (documentAchat != null && documentAchat.isCommande()) {
      pnlDonneesReliquats.setVisible(true);
    }
    else {
      pnlDonneesReliquats.setVisible(false);
    }
  }
  
  private void rafraichirPanelDonneesTraitees() {
    DocumentAchat documentAchat = getModele().getDocumentAchat();
    if (documentAchat != null && documentAchat.isCommande()) {
      pnlDonneesTraitees.setVisible(true);
    }
    else {
      pnlDonneesTraitees.setVisible(false);
    }
  }
  
  private void rafraichirPrixBrutHT() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getPrixAchatBrutHT() != null) {
      tfPrixAchatBrut.setText(Constantes.formater(ligneArticle.getPrixAchat().getPrixAchatBrutHT(), true));
    }
    else {
      tfPrixAchatBrut.setText("");
    }
  }
  
  private void rafraichirPrixNetHT() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getPrixAchatNetHT() != null) {
      tfPrixAchatNet.setText(Constantes.formater(ligneArticle.getPrixAchat().getPrixAchatNetHT(), true));
    }
    else {
      tfPrixAchatNet.setText("");
    }
  }
  
  private void rafraichirPort() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null && ligneArticle.getPrixAchat().getMontantPortHT() != null) {
      tfPrixPort.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantPortHT(), true));
    }
    else {
      tfPrixPort.setText("");
    }
  }
  
  private void rafraichirPrixRevientFournisseur() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null
        && ligneArticle.getPrixAchat().getPrixRevientFournisseurHT() != null) {
      tfPrixDeRevientFournisseur.setText(Constantes.formater(ligneArticle.getPrixAchat().getPrixRevientFournisseurHT(), true));
    }
    else {
      tfPrixDeRevientFournisseur.setText("");
    }
  }
  
  private void rafraichirMontant() {
    LigneAchatArticle ligneArticle = getModele().getLigneAchatArticle();
    if (ligneArticle != null && ligneArticle.getPrixAchat() != null) {
      // S'il s'agit d'une commande
      if (getModele().getDocumentAchat().isCommande() && ligneArticle.getPrixAchat().getMontantInitialHT() != null) {
        tfMontantTotal.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantInitialHT(), true));
        return;
      }
      // Sinon
      if (ligneArticle.getPrixAchat().getMontantReliquatHT() != null) {
        tfMontantTotal.setText(Constantes.formater(ligneArticle.getPrixAchat().getMontantReliquatHT(), true));
      }
    }
    else {
      tfMontantTotal.setText("");
    }
  }
  
  private void rafraichirLiens() {
    LienLigne lienOrigine = getModele().getLienOrigine();
    if (lienOrigine != null) {
      snLignesLiees.setVisible(true);
      snLignesLiees.setSession(getModele().getSession());
      snLignesLiees.setLienOrigine(lienOrigine);
      snLignesLiees.charger(true);
    }
    // Si aucun lien on cache le panel
    else {
      snLignesLiees.setVisible(false);
    }
  }
  
  // -- Méthodes interractives
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPrixAchatBrutHTFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemise1FocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemise2FocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemise3FocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemise4FocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfTaxeOuMajorationFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfMontantConditionnementFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbMontantAuPoidsPortHTActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfMontantAuPoidsPortHTFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPoidsPortFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbPourcentagePortActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentagePortFocusLost(FocusEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelContenu();
    pnlCommentaire = new JPanel();
    pnlEntete = new JPanel();
    lbCodeArticle = new SNLabelChamp();
    tfCodeArticle = new JLabel();
    lbLibelleArticle = new SNLabelChamp();
    tfLibelleArticle = new JLabel();
    pnlQuantitesEnUCA = new SNPanel();
    pnlDonneesInitiales = new SNPanel();
    lbQuantiteInitialeUCA = new SNLabelChamp();
    tfQuantiteInitialeUCA = new JLabel();
    lbQuantiteInitialeUA = new SNLabelChamp();
    tfQuantiteInitialeUA = new JLabel();
    lbMontantInitialHT = new SNLabelChamp();
    tfMontantInitialHT = new JLabel();
    pnlDonneesReliquats = new SNPanel();
    lbQuantiteReliquatUCA = new SNLabelChamp();
    tfQuantiteReliquatUCA = new JLabel();
    lbQuantiteReliquatUA = new SNLabelChamp();
    tfQuantiteReliquatUA = new JLabel();
    lbMontantReliquatHT = new SNLabelChamp();
    tfMontantReliquatHT = new JLabel();
    pnlDonneesTraitees = new SNPanel();
    lbQuantiteTraiteeUCA = new SNLabelChamp();
    tfQuantiteTraiteeUCA = new JLabel();
    lbQuantiteTraiteeUA = new SNLabelChamp();
    tfQuantiteTraiteeUA = new JLabel();
    lbMontantTraiteHT = new SNLabelChamp();
    tfMontantTraiteHT = new JLabel();
    snTitreInformationsTarifaires = new SNLabelTitre();
    pnlTarif = new SNPanel();
    pnlInfosTarifaires = new JPanel();
    lbPrixAchatNet = new JLabel();
    tfPrixAchatNet = new XRiTextField();
    lbPrixPort = new JLabel();
    tfPrixPort = new XRiTextField();
    lbPrixDeRevientStandard = new JLabel();
    tfPrixDeRevientFournisseur = new XRiTextField();
    lbPrixAchatNet2 = new JLabel();
    tfPrixAchatBrut = new XRiTextField();
    pnlMontant = new JPanel();
    tfMontantTotal = new XRiTextField();
    lbPrixDeRevientStandard2 = new JLabel();
    snTitreLien = new SNLabelTitre();
    snLignesLiees = new SNLienLigne();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setTitle("D\u00e9tail d'une ligne d'achat");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(980, 640));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(Color.white);
      pnlPrincipal.setOpaque(true);
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCommentaire ========
      {
        pnlCommentaire.setOpaque(false);
        pnlCommentaire.setName("pnlCommentaire");
        pnlCommentaire.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlEntete ========
        {
          pnlEntete.setOpaque(false);
          pnlEntete.setName("pnlEntete");
          pnlEntete.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEntete.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEntete.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlEntete.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEntete.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbCodeArticle ----
          lbCodeArticle.setText("Code article :");
          lbCodeArticle.setFont(new Font("sansserif", Font.BOLD, 13));
          lbCodeArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbCodeArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCodeArticle.setPreferredSize(new Dimension(150, 20));
          lbCodeArticle.setMinimumSize(new Dimension(150, 20));
          lbCodeArticle.setMaximumSize(new Dimension(150, 20));
          lbCodeArticle.setName("lbCodeArticle");
          pnlEntete.add(lbCodeArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfCodeArticle ----
          tfCodeArticle.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfCodeArticle.setBorder(null);
          tfCodeArticle.setHorizontalTextPosition(SwingConstants.LEADING);
          tfCodeArticle.setMaximumSize(new Dimension(400, 20));
          tfCodeArticle.setMinimumSize(new Dimension(400, 20));
          tfCodeArticle.setPreferredSize(new Dimension(400, 20));
          tfCodeArticle.setName("tfCodeArticle");
          pnlEntete.add(tfCodeArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbLibelleArticle ----
          lbLibelleArticle.setText("Libell\u00e9 article :");
          lbLibelleArticle.setFont(new Font("sansserif", Font.BOLD, 13));
          lbLibelleArticle.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbLibelleArticle.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLibelleArticle.setPreferredSize(new Dimension(150, 20));
          lbLibelleArticle.setMinimumSize(new Dimension(150, 20));
          lbLibelleArticle.setMaximumSize(new Dimension(150, 20));
          lbLibelleArticle.setName("lbLibelleArticle");
          pnlEntete.add(lbLibelleArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfLibelleArticle ----
          tfLibelleArticle.setFont(new Font("sansserif", Font.PLAIN, 13));
          tfLibelleArticle.setBackground(Color.white);
          tfLibelleArticle.setBorder(null);
          tfLibelleArticle.setHorizontalTextPosition(SwingConstants.LEADING);
          tfLibelleArticle.setPreferredSize(new Dimension(800, 20));
          tfLibelleArticle.setMaximumSize(new Dimension(840, 20));
          tfLibelleArticle.setMinimumSize(new Dimension(80, 20));
          tfLibelleArticle.setName("tfLibelleArticle");
          pnlEntete.add(tfLibelleArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ======== pnlQuantitesEnUCA ========
          {
            pnlQuantitesEnUCA.setName("pnlQuantitesEnUCA");
            pnlQuantitesEnUCA.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlQuantitesEnUCA.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlQuantitesEnUCA.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlQuantitesEnUCA.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlQuantitesEnUCA.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ======== pnlDonneesInitiales ========
            {
              pnlDonneesInitiales.setName("pnlDonneesInitiales");
              pnlDonneesInitiales.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDonneesInitiales.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlDonneesInitiales.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDonneesInitiales.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDonneesInitiales.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbQuantiteInitialeUCA ----
              lbQuantiteInitialeUCA.setText("Quantit\u00e9 initiale (UCA) :");
              lbQuantiteInitialeUCA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteInitialeUCA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteInitialeUCA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteInitialeUCA.setMinimumSize(new Dimension(200, 20));
              lbQuantiteInitialeUCA.setPreferredSize(new Dimension(200, 20));
              lbQuantiteInitialeUCA.setMaximumSize(new Dimension(200, 20));
              lbQuantiteInitialeUCA.setName("lbQuantiteInitialeUCA");
              pnlDonneesInitiales.add(lbQuantiteInitialeUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
              
              // ---- tfQuantiteInitialeUCA ----
              tfQuantiteInitialeUCA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteInitialeUCA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteInitialeUCA.setBorder(null);
              tfQuantiteInitialeUCA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteInitialeUCA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteInitialeUCA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteInitialeUCA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteInitialeUCA.setName("tfQuantiteInitialeUCA");
              pnlDonneesInitiales.add(tfQuantiteInitialeUCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbQuantiteInitialeUA ----
              lbQuantiteInitialeUA.setText("Quantit\u00e9 initiale (UA) :");
              lbQuantiteInitialeUA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteInitialeUA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteInitialeUA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteInitialeUA.setMinimumSize(new Dimension(200, 20));
              lbQuantiteInitialeUA.setPreferredSize(new Dimension(200, 20));
              lbQuantiteInitialeUA.setMaximumSize(new Dimension(200, 20));
              lbQuantiteInitialeUA.setName("lbQuantiteInitialeUA");
              pnlDonneesInitiales.add(lbQuantiteInitialeUA, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 10), 0, 0));
              
              // ---- tfQuantiteInitialeUA ----
              tfQuantiteInitialeUA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteInitialeUA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteInitialeUA.setBorder(null);
              tfQuantiteInitialeUA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteInitialeUA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteInitialeUA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteInitialeUA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteInitialeUA.setInheritsPopupMenu(false);
              tfQuantiteInitialeUA.setName("tfQuantiteInitialeUA");
              pnlDonneesInitiales.add(tfQuantiteInitialeUA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMontantInitialHT ----
              lbMontantInitialHT.setText("Montant initial HT :");
              lbMontantInitialHT.setFont(new Font("sansserif", Font.BOLD, 13));
              lbMontantInitialHT.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbMontantInitialHT.setHorizontalAlignment(SwingConstants.RIGHT);
              lbMontantInitialHT.setMinimumSize(new Dimension(200, 20));
              lbMontantInitialHT.setPreferredSize(new Dimension(200, 20));
              lbMontantInitialHT.setMaximumSize(new Dimension(200, 20));
              lbMontantInitialHT.setName("lbMontantInitialHT");
              pnlDonneesInitiales.add(lbMontantInitialHT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 10), 0, 0));
              
              // ---- tfMontantInitialHT ----
              tfMontantInitialHT.setHorizontalAlignment(SwingConstants.LEFT);
              tfMontantInitialHT.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfMontantInitialHT.setBorder(null);
              tfMontantInitialHT.setHorizontalTextPosition(SwingConstants.LEADING);
              tfMontantInitialHT.setMaximumSize(new Dimension(150, 20));
              tfMontantInitialHT.setMinimumSize(new Dimension(150, 20));
              tfMontantInitialHT.setPreferredSize(new Dimension(150, 20));
              tfMontantInitialHT.setInheritsPopupMenu(false);
              tfMontantInitialHT.setName("tfMontantInitialHT");
              pnlDonneesInitiales.add(tfMontantInitialHT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantitesEnUCA.add(pnlDonneesInitiales, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDonneesReliquats ========
            {
              pnlDonneesReliquats.setName("pnlDonneesReliquats");
              pnlDonneesReliquats.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDonneesReliquats.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlDonneesReliquats.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDonneesReliquats.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDonneesReliquats.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbQuantiteReliquatUCA ----
              lbQuantiteReliquatUCA.setText("Quantit\u00e9 reliquat (UCA) :");
              lbQuantiteReliquatUCA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteReliquatUCA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteReliquatUCA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteReliquatUCA.setMinimumSize(new Dimension(150, 20));
              lbQuantiteReliquatUCA.setPreferredSize(new Dimension(160, 20));
              lbQuantiteReliquatUCA.setMaximumSize(new Dimension(150, 20));
              lbQuantiteReliquatUCA.setName("lbQuantiteReliquatUCA");
              pnlDonneesReliquats.add(lbQuantiteReliquatUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfQuantiteReliquatUCA ----
              tfQuantiteReliquatUCA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteReliquatUCA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteReliquatUCA.setBorder(null);
              tfQuantiteReliquatUCA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteReliquatUCA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteReliquatUCA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteReliquatUCA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteReliquatUCA.setName("tfQuantiteReliquatUCA");
              pnlDonneesReliquats.add(tfQuantiteReliquatUCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbQuantiteReliquatUA ----
              lbQuantiteReliquatUA.setText("Quantit\u00e9 reliquat (UA) :");
              lbQuantiteReliquatUA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteReliquatUA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteReliquatUA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteReliquatUA.setMinimumSize(new Dimension(150, 20));
              lbQuantiteReliquatUA.setPreferredSize(new Dimension(160, 20));
              lbQuantiteReliquatUA.setMaximumSize(new Dimension(150, 20));
              lbQuantiteReliquatUA.setName("lbQuantiteReliquatUA");
              pnlDonneesReliquats.add(lbQuantiteReliquatUA, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfQuantiteReliquatUA ----
              tfQuantiteReliquatUA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteReliquatUA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteReliquatUA.setBorder(null);
              tfQuantiteReliquatUA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteReliquatUA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteReliquatUA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteReliquatUA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteReliquatUA.setInheritsPopupMenu(false);
              tfQuantiteReliquatUA.setName("tfQuantiteReliquatUA");
              pnlDonneesReliquats.add(tfQuantiteReliquatUA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMontantReliquatHT ----
              lbMontantReliquatHT.setText("Montant reliquat HT :");
              lbMontantReliquatHT.setFont(new Font("sansserif", Font.BOLD, 13));
              lbMontantReliquatHT.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbMontantReliquatHT.setHorizontalAlignment(SwingConstants.RIGHT);
              lbMontantReliquatHT.setMinimumSize(new Dimension(150, 20));
              lbMontantReliquatHT.setPreferredSize(new Dimension(160, 20));
              lbMontantReliquatHT.setMaximumSize(new Dimension(150, 20));
              lbMontantReliquatHT.setName("lbMontantReliquatHT");
              pnlDonneesReliquats.add(lbMontantReliquatHT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfMontantReliquatHT ----
              tfMontantReliquatHT.setHorizontalAlignment(SwingConstants.LEFT);
              tfMontantReliquatHT.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfMontantReliquatHT.setBorder(null);
              tfMontantReliquatHT.setHorizontalTextPosition(SwingConstants.LEADING);
              tfMontantReliquatHT.setMaximumSize(new Dimension(150, 20));
              tfMontantReliquatHT.setMinimumSize(new Dimension(150, 20));
              tfMontantReliquatHT.setPreferredSize(new Dimension(150, 20));
              tfMontantReliquatHT.setInheritsPopupMenu(false);
              tfMontantReliquatHT.setName("tfMontantReliquatHT");
              pnlDonneesReliquats.add(tfMontantReliquatHT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantitesEnUCA.add(pnlDonneesReliquats, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDonneesTraitees ========
            {
              pnlDonneesTraitees.setName("pnlDonneesTraitees");
              pnlDonneesTraitees.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDonneesTraitees.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlDonneesTraitees.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDonneesTraitees.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDonneesTraitees.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbQuantiteTraiteeUCA ----
              lbQuantiteTraiteeUCA.setText("Quantit\u00e9 trait\u00e9e (UCA) :");
              lbQuantiteTraiteeUCA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteTraiteeUCA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteTraiteeUCA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteTraiteeUCA.setMinimumSize(new Dimension(150, 20));
              lbQuantiteTraiteeUCA.setPreferredSize(new Dimension(150, 20));
              lbQuantiteTraiteeUCA.setMaximumSize(new Dimension(150, 20));
              lbQuantiteTraiteeUCA.setName("lbQuantiteTraiteeUCA");
              pnlDonneesTraitees.add(lbQuantiteTraiteeUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfQuantiteTraiteeUCA ----
              tfQuantiteTraiteeUCA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteTraiteeUCA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteTraiteeUCA.setBorder(null);
              tfQuantiteTraiteeUCA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteTraiteeUCA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteTraiteeUCA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteTraiteeUCA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteTraiteeUCA.setName("tfQuantiteTraiteeUCA");
              pnlDonneesTraitees.add(tfQuantiteTraiteeUCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbQuantiteTraiteeUA ----
              lbQuantiteTraiteeUA.setText("Quantit\u00e9 trait\u00e9e (UA) :");
              lbQuantiteTraiteeUA.setFont(new Font("sansserif", Font.BOLD, 13));
              lbQuantiteTraiteeUA.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbQuantiteTraiteeUA.setHorizontalAlignment(SwingConstants.RIGHT);
              lbQuantiteTraiteeUA.setMinimumSize(new Dimension(150, 20));
              lbQuantiteTraiteeUA.setPreferredSize(new Dimension(150, 20));
              lbQuantiteTraiteeUA.setMaximumSize(new Dimension(150, 20));
              lbQuantiteTraiteeUA.setName("lbQuantiteTraiteeUA");
              pnlDonneesTraitees.add(lbQuantiteTraiteeUA, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfQuantiteTraiteeUA ----
              tfQuantiteTraiteeUA.setHorizontalAlignment(SwingConstants.LEFT);
              tfQuantiteTraiteeUA.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfQuantiteTraiteeUA.setBorder(null);
              tfQuantiteTraiteeUA.setHorizontalTextPosition(SwingConstants.LEADING);
              tfQuantiteTraiteeUA.setMaximumSize(new Dimension(150, 20));
              tfQuantiteTraiteeUA.setMinimumSize(new Dimension(150, 20));
              tfQuantiteTraiteeUA.setPreferredSize(new Dimension(150, 20));
              tfQuantiteTraiteeUA.setInheritsPopupMenu(false);
              tfQuantiteTraiteeUA.setName("tfQuantiteTraiteeUA");
              pnlDonneesTraitees.add(tfQuantiteTraiteeUA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMontantTraiteHT ----
              lbMontantTraiteHT.setText("Montant trait\u00e9e HT :");
              lbMontantTraiteHT.setFont(new Font("sansserif", Font.BOLD, 13));
              lbMontantTraiteHT.setHorizontalTextPosition(SwingConstants.RIGHT);
              lbMontantTraiteHT.setHorizontalAlignment(SwingConstants.RIGHT);
              lbMontantTraiteHT.setMinimumSize(new Dimension(150, 20));
              lbMontantTraiteHT.setPreferredSize(new Dimension(150, 20));
              lbMontantTraiteHT.setMaximumSize(new Dimension(150, 20));
              lbMontantTraiteHT.setName("lbMontantTraiteHT");
              pnlDonneesTraitees.add(lbMontantTraiteHT, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfMontantTraiteHT ----
              tfMontantTraiteHT.setHorizontalAlignment(SwingConstants.LEFT);
              tfMontantTraiteHT.setFont(new Font("sansserif", Font.PLAIN, 13));
              tfMontantTraiteHT.setBorder(null);
              tfMontantTraiteHT.setHorizontalTextPosition(SwingConstants.LEADING);
              tfMontantTraiteHT.setMaximumSize(new Dimension(150, 20));
              tfMontantTraiteHT.setMinimumSize(new Dimension(150, 20));
              tfMontantTraiteHT.setPreferredSize(new Dimension(150, 20));
              tfMontantTraiteHT.setInheritsPopupMenu(false);
              tfMontantTraiteHT.setName("tfMontantTraiteHT");
              pnlDonneesTraitees.add(tfMontantTraiteHT, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlQuantitesEnUCA.add(pnlDonneesTraitees, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlEntete.add(pnlQuantitesEnUCA, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCommentaire.add(pnlEntete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snTitreInformationsTarifaires ----
        snTitreInformationsTarifaires.setText("Informations tarifaires");
        snTitreInformationsTarifaires.setName("snTitreInformationsTarifaires");
        pnlCommentaire.add(snTitreInformationsTarifaires, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlTarif ========
        {
          pnlTarif.setPreferredSize(new Dimension(900, 80));
          pnlTarif.setMinimumSize(new Dimension(900, 80));
          pnlTarif.setMaximumSize(new Dimension(900, 80));
          pnlTarif.setName("pnlTarif");
          pnlTarif.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTarif.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTarif.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTarif.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTarif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlInfosTarifaires ========
          {
            pnlInfosTarifaires.setBorder(new EtchedBorder());
            pnlInfosTarifaires.setOpaque(false);
            pnlInfosTarifaires.setPreferredSize(new Dimension(650, 75));
            pnlInfosTarifaires.setMinimumSize(new Dimension(650, 75));
            pnlInfosTarifaires.setMaximumSize(new Dimension(650, 75));
            pnlInfosTarifaires.setName("pnlInfosTarifaires");
            pnlInfosTarifaires.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlInfosTarifaires.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlInfosTarifaires.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPrixAchatNet ----
            lbPrixAchatNet.setText("Prix d'achat net HT");
            lbPrixAchatNet.setFont(new Font("sansserif", Font.BOLD, 13));
            lbPrixAchatNet.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixAchatNet.setPreferredSize(new Dimension(150, 30));
            lbPrixAchatNet.setMinimumSize(new Dimension(150, 30));
            lbPrixAchatNet.setMaximumSize(new Dimension(150, 30));
            lbPrixAchatNet.setName("lbPrixAchatNet");
            pnlInfosTarifaires.add(lbPrixAchatNet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfPrixAchatNet ----
            tfPrixAchatNet.setHorizontalAlignment(SwingConstants.CENTER);
            tfPrixAchatNet.setFont(new Font("sansserif", Font.PLAIN, 13));
            tfPrixAchatNet.setPreferredSize(new Dimension(100, 28));
            tfPrixAchatNet.setMinimumSize(new Dimension(100, 28));
            tfPrixAchatNet.setEnabled(false);
            tfPrixAchatNet.setBorder(null);
            tfPrixAchatNet.setBackground(Color.white);
            tfPrixAchatNet.setMaximumSize(new Dimension(100, 28));
            tfPrixAchatNet.setName("tfPrixAchatNet");
            pnlInfosTarifaires.add(tfPrixAchatNet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPrixPort ----
            lbPrixPort.setText("Port HT");
            lbPrixPort.setFont(new Font("sansserif", Font.BOLD, 13));
            lbPrixPort.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixPort.setPreferredSize(new Dimension(150, 30));
            lbPrixPort.setMinimumSize(new Dimension(150, 30));
            lbPrixPort.setMaximumSize(new Dimension(150, 30));
            lbPrixPort.setName("lbPrixPort");
            pnlInfosTarifaires.add(lbPrixPort, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfPrixPort ----
            tfPrixPort.setFont(new Font("sansserif", Font.PLAIN, 13));
            tfPrixPort.setHorizontalAlignment(SwingConstants.CENTER);
            tfPrixPort.setPreferredSize(new Dimension(100, 28));
            tfPrixPort.setMinimumSize(new Dimension(100, 28));
            tfPrixPort.setEnabled(false);
            tfPrixPort.setBorder(null);
            tfPrixPort.setBackground(Color.white);
            tfPrixPort.setMaximumSize(new Dimension(100, 28));
            tfPrixPort.setName("tfPrixPort");
            pnlInfosTarifaires.add(tfPrixPort, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPrixDeRevientStandard ----
            lbPrixDeRevientStandard.setText("<html>Prix de revient fournisseur</html>");
            lbPrixDeRevientStandard.setFont(new Font("sansserif", Font.BOLD, 13));
            lbPrixDeRevientStandard.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixDeRevientStandard.setPreferredSize(new Dimension(180, 30));
            lbPrixDeRevientStandard.setMinimumSize(new Dimension(180, 30));
            lbPrixDeRevientStandard.setMaximumSize(new Dimension(180, 30));
            lbPrixDeRevientStandard.setName("lbPrixDeRevientStandard");
            pnlInfosTarifaires.add(lbPrixDeRevientStandard, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- tfPrixDeRevientFournisseur ----
            tfPrixDeRevientFournisseur.setHorizontalAlignment(SwingConstants.CENTER);
            tfPrixDeRevientFournisseur.setFont(new Font("sansserif", Font.PLAIN, 13));
            tfPrixDeRevientFournisseur.setPreferredSize(new Dimension(100, 28));
            tfPrixDeRevientFournisseur.setMinimumSize(new Dimension(100, 28));
            tfPrixDeRevientFournisseur.setEnabled(false);
            tfPrixDeRevientFournisseur.setBorder(null);
            tfPrixDeRevientFournisseur.setBackground(Color.white);
            tfPrixDeRevientFournisseur.setMaximumSize(new Dimension(100, 28));
            tfPrixDeRevientFournisseur.setName("tfPrixDeRevientFournisseur");
            pnlInfosTarifaires.add(tfPrixDeRevientFournisseur, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixAchatNet2 ----
            lbPrixAchatNet2.setText("Prix d'achat brut HT");
            lbPrixAchatNet2.setFont(new Font("sansserif", Font.BOLD, 13));
            lbPrixAchatNet2.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixAchatNet2.setPreferredSize(new Dimension(150, 30));
            lbPrixAchatNet2.setMinimumSize(new Dimension(150, 30));
            lbPrixAchatNet2.setMaximumSize(new Dimension(150, 30));
            lbPrixAchatNet2.setName("lbPrixAchatNet2");
            pnlInfosTarifaires.add(lbPrixAchatNet2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfPrixAchatBrut ----
            tfPrixAchatBrut.setHorizontalAlignment(SwingConstants.CENTER);
            tfPrixAchatBrut.setFont(new Font("sansserif", Font.PLAIN, 13));
            tfPrixAchatBrut.setPreferredSize(new Dimension(100, 28));
            tfPrixAchatBrut.setMinimumSize(new Dimension(100, 28));
            tfPrixAchatBrut.setEnabled(false);
            tfPrixAchatBrut.setBorder(null);
            tfPrixAchatBrut.setBackground(Color.white);
            tfPrixAchatBrut.setMaximumSize(new Dimension(100, 28));
            tfPrixAchatBrut.setName("tfPrixAchatBrut");
            pnlInfosTarifaires.add(tfPrixAchatBrut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlTarif.add(pnlInfosTarifaires, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlMontant ========
          {
            pnlMontant.setBorder(new EtchedBorder());
            pnlMontant.setOpaque(false);
            pnlMontant.setMinimumSize(new Dimension(200, 75));
            pnlMontant.setPreferredSize(new Dimension(200, 75));
            pnlMontant.setMaximumSize(new Dimension(200, 75));
            pnlMontant.setName("pnlMontant");
            pnlMontant.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMontant.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlMontant.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlMontant.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlMontant.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfMontantTotal ----
            tfMontantTotal.setHorizontalAlignment(SwingConstants.CENTER);
            tfMontantTotal.setFont(new Font("sansserif", Font.BOLD, 13));
            tfMontantTotal.setMinimumSize(new Dimension(150, 28));
            tfMontantTotal.setPreferredSize(new Dimension(150, 28));
            tfMontantTotal.setEnabled(false);
            tfMontantTotal.setBorder(null);
            tfMontantTotal.setBackground(Color.white);
            tfMontantTotal.setMaximumSize(new Dimension(150, 28));
            tfMontantTotal.setName("tfMontantTotal");
            pnlMontant.add(tfMontantTotal, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.NONE,
                new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- lbPrixDeRevientStandard2 ----
            lbPrixDeRevientStandard2.setText("Montant HT de la ligne");
            lbPrixDeRevientStandard2.setFont(new Font("sansserif", Font.BOLD, 13));
            lbPrixDeRevientStandard2.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixDeRevientStandard2.setPreferredSize(new Dimension(150, 30));
            lbPrixDeRevientStandard2.setMinimumSize(new Dimension(150, 30));
            lbPrixDeRevientStandard2.setMaximumSize(new Dimension(150, 30));
            lbPrixDeRevientStandard2.setName("lbPrixDeRevientStandard2");
            pnlMontant.add(lbPrixDeRevientStandard2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlTarif.add(pnlMontant, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCommentaire.add(pnlTarif, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.NONE,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snTitreLien ----
        snTitreLien.setText("Liens");
        snTitreLien.setName("snTitreLien");
        pnlCommentaire.add(snTitreLien, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snLignesLiees ----
        snLignesLiees.setBackground(Color.white);
        snLignesLiees.setOpaque(true);
        snLignesLiees.setPreferredSize(new Dimension(1060, 280));
        snLignesLiees.setName("snLignesLiees");
        pnlCommentaire.add(snLignesLiees, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlPrincipal.add(pnlCommentaire, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlPrincipal;
  private JPanel pnlCommentaire;
  private JPanel pnlEntete;
  private SNLabelChamp lbCodeArticle;
  private JLabel tfCodeArticle;
  private SNLabelChamp lbLibelleArticle;
  private JLabel tfLibelleArticle;
  private SNPanel pnlQuantitesEnUCA;
  private SNPanel pnlDonneesInitiales;
  private SNLabelChamp lbQuantiteInitialeUCA;
  private JLabel tfQuantiteInitialeUCA;
  private SNLabelChamp lbQuantiteInitialeUA;
  private JLabel tfQuantiteInitialeUA;
  private SNLabelChamp lbMontantInitialHT;
  private JLabel tfMontantInitialHT;
  private SNPanel pnlDonneesReliquats;
  private SNLabelChamp lbQuantiteReliquatUCA;
  private JLabel tfQuantiteReliquatUCA;
  private SNLabelChamp lbQuantiteReliquatUA;
  private JLabel tfQuantiteReliquatUA;
  private SNLabelChamp lbMontantReliquatHT;
  private JLabel tfMontantReliquatHT;
  private SNPanel pnlDonneesTraitees;
  private SNLabelChamp lbQuantiteTraiteeUCA;
  private JLabel tfQuantiteTraiteeUCA;
  private SNLabelChamp lbQuantiteTraiteeUA;
  private JLabel tfQuantiteTraiteeUA;
  private SNLabelChamp lbMontantTraiteHT;
  private JLabel tfMontantTraiteHT;
  private SNLabelTitre snTitreInformationsTarifaires;
  private SNPanel pnlTarif;
  private JPanel pnlInfosTarifaires;
  private JLabel lbPrixAchatNet;
  private XRiTextField tfPrixAchatNet;
  private JLabel lbPrixPort;
  private XRiTextField tfPrixPort;
  private JLabel lbPrixDeRevientStandard;
  private XRiTextField tfPrixDeRevientFournisseur;
  private JLabel lbPrixAchatNet2;
  private XRiTextField tfPrixAchatBrut;
  private JPanel pnlMontant;
  private XRiTextField tfMontantTotal;
  private JLabel lbPrixDeRevientStandard2;
  private SNLabelTitre snTitreLien;
  private SNLienLigne snLignesLiees;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
