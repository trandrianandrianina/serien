/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.catalogue.expert;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;
import ri.serien.libswing.outil.documentfilter.SaisieDefinition;

/**
 * Vue de la boîte de dialogue pour le mode Expert d'une zone Série N d'une configuration de catalogue
 */
public class VueExpertConfigurationCatalogue extends AbstractVueDialogue<ModeleExpertConfigurationCatalogue> {
  /**
   * Constructeur.
   */
  public VueExpertConfigurationCatalogue(ModeleExpertConfigurationCatalogue pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // TODO marche pas bien ce truc va falloir le changer
    tfDebut.init(4, SaisieDefinition.NOMBRE_ENTIER);
    tfDebut2.init(4, SaisieDefinition.NOMBRE_ENTIER);
    tfFin.init(4, SaisieDefinition.NOMBRE_ENTIER);
    tfFin2.init(4, SaisieDefinition.NOMBRE_ENTIER);
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirZoneERP();
    rafraichirComboZone1Catalogue();
    rafraichirComboZone2Catalogue();
    rafraichirDebutZone1();
    rafraichirFinZone1();
    rafraichirDebutZone2();
    rafraichirFinZone2();
    rafraichirBoutonValider();
  }
  
  /**
   * Rendre utilisable ou inutilisable le bouton valider en fonction de l'état de la configuration de catalogue active :
   * Non modifiée le bouton est inutilisable, modifiée le bouton "valider" est inutilisable
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isZoneERPmodifiee();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Rafraichir les zones de consultation de la zone ERP sélectionnée
   */
  private void rafraichirZoneERP() {
    if (getModele().getChampERP() == null) {
      return;
    }
    
    if (getModele().getChampERP().getLibelleLong() != null) {
      tflibelleLong.setText(getModele().getChampERP().getLibelleLong());
    }
    else {
      tflibelleLong.setText("");
    }
    if (getModele().getChampERP().getType() != null) {
      tfType.setText(getModele().getChampERP().getType().getLibelle());
    }
    else {
      tfType.setText("");
    }
    if (getModele().getChampERP().getLongueur() != null) {
      tfLongueur.setText(getModele().getChampERP().getLongueur().toString());
    }
    else {
      tfLongueur.setText("");
    }
    if (getModele().getChampERP().getId() != null) {
      tfCode.setText(getModele().getChampERP().getId().getCode());
    }
    else {
      tfCode.setText("");
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Rafraichir les combos d'affichage des zones catalogue
   */
  private void rafraichirComboZone1Catalogue() {
    ChampCatalogue champCatalogue1 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() > 0) {
      champCatalogue1 = getModele().getChampERP().getListeChampCatalogue().get(0);
    }
    
    cbZone1.removeAllItems();
    cbZone1.addItem(" ");
    if (getModele().getListeChampCatalogue() != null) {
      for (ChampCatalogue ChampCatalogue : getModele().getListeChampCatalogue()) {
        cbZone1.addItem(ChampCatalogue);
      }
    }
    
    if (champCatalogue1 != null) {
      cbZone1.setSelectedItem(champCatalogue1);
    }
  }
  
  /**
   * Rafraichir la zone de saisie de début de découpe de la première zone catalogue
   */
  private void rafraichirDebutZone1() {
    ChampCatalogue champCatalogue1 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() > 0) {
      champCatalogue1 = getModele().getChampERP().getListeChampCatalogue().get(0);
    }
    
    tfDebut.setEnabled(champCatalogue1 != null);
    if (champCatalogue1 == null) {
      tfDebut.setText("");
      return;
    }
    
    if (champCatalogue1.getDebutDecoupe() != null) {
      tfDebut.setText(champCatalogue1.getDebutDecoupe().toString());
    }
    else {
      tfDebut.setText("");
    }
  }
  
  /**
   * Rafraichir la zone de saisie de fin de découpe de la première zone catalogue
   */
  private void rafraichirFinZone1() {
    ChampCatalogue champCatalogue1 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() > 0) {
      champCatalogue1 = getModele().getChampERP().getListeChampCatalogue().get(0);
    }
    
    tfFin.setEnabled(champCatalogue1 != null);
    if (champCatalogue1 == null) {
      tfFin.setText("");
      return;
    }
    
    if (champCatalogue1.getFinDecoupe() != null) {
      tfFin.setText(champCatalogue1.getFinDecoupe().toString());
    }
    else {
      tfFin.setText("");
    }
  }
  
  /**
   * Rafraichir la zone de saisie de début de découpe de la deuxième zone catalogue
   */
  private void rafraichirDebutZone2() {
    ChampCatalogue champCatalogue2 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() == 2) {
      champCatalogue2 = getModele().getChampERP().getListeChampCatalogue().get(1);
    }
    
    tfDebut2.setEnabled(champCatalogue2 != null);
    if (champCatalogue2 == null) {
      tfDebut2.setText("");
      return;
    }
    
    if (champCatalogue2.getDebutDecoupe() != null) {
      tfDebut2.setText(champCatalogue2.getDebutDecoupe().toString());
    }
    else {
      tfDebut2.setText("");
    }
  }
  
  /**
   * Rafraichir la zone de saisie de fin de découpe de la deuxième zone catalogue
   */
  private void rafraichirFinZone2() {
    ChampCatalogue champCatalogue2 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() == 2) {
      champCatalogue2 = getModele().getChampERP().getListeChampCatalogue().get(1);
    }
    
    tfFin2.setEnabled(champCatalogue2 != null);
    if (champCatalogue2 == null) {
      tfFin2.setText("");
      return;
    }
    
    if (champCatalogue2.getFinDecoupe() != null) {
      tfFin2.setText(champCatalogue2.getFinDecoupe().toString());
    }
    else {
      tfFin2.setText("");
    }
  }
  
  /**
   * Rafraichir les combos d'affichage des zones catalogue
   */
  private void rafraichirComboZone2Catalogue() {
    ChampCatalogue champCatalogue1 = null;
    ChampCatalogue champCatalogue2 = null;
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() > 0) {
      champCatalogue1 = getModele().getChampERP().getListeChampCatalogue().get(0);
    }
    if (getModele().getChampERP() != null && getModele().getChampERP().getListeChampCatalogue() != null
        && getModele().getChampERP().getListeChampCatalogue().size() == 2) {
      champCatalogue2 = getModele().getChampERP().getListeChampCatalogue().get(1);
    }
    
    cbZone2.removeAllItems();
    cbZone2.addItem(" ");
    if (getModele().getListeChampCatalogue() != null) {
      for (ChampCatalogue ChampCatalogue : getModele().getListeChampCatalogue()) {
        cbZone2.addItem(ChampCatalogue);
      }
    }
    
    if (champCatalogue2 != null) {
      cbZone2.setSelectedItem(champCatalogue2);
    }
    
    cbZone2.setEnabled(champCatalogue1 != null);
  }
  
  private void cbZone1ItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || (e.getStateChange() != ItemEvent.SELECTED)) {
        return;
      }
      if (cbZone1.getSelectedItem() instanceof ChampCatalogue) {
        getModele().modifierPremiereZoneCatalogue((ChampCatalogue) cbZone1.getSelectedItem());
      }
      else {
        getModele().modifierPremiereZoneCatalogue(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbZone2ItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs() || (e.getStateChange() != ItemEvent.SELECTED)) {
        return;
      }
      if (cbZone2.getSelectedItem() instanceof ChampCatalogue) {
        getModele().modifierDeuxiemeZoneCatalogue((ChampCatalogue) cbZone2.getSelectedItem());
      }
      else {
        getModele().modifierDeuxiemeZoneCatalogue(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDebutKeyReleased(KeyEvent e) {
    try {
      getModele().modifierDecoupageDebutZone1(tfDebut.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfDebut2KeyReleased(KeyEvent e) {
    try {
      getModele().modifierDecoupageDebutZone2(tfDebut2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
    
  }
  
  private void tfFinKeyReleased(KeyEvent e) {
    try {
      Trace.info("tfFin text : " + tfFin.getText());
      getModele().modifierDecoupageFinZone1(tfFin.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfFin2KeyReleased(KeyEvent e) {
    try {
      getModele().modifierDecoupageFinZone2(tfFin2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    snBarreBouton = new SNBarreBouton();
    panel1 = new JPanel();
    panel2 = new JPanel();
    tflibelleLong = new RiZoneSortie();
    lbLibelleLong = new JLabel();
    tfType = new RiZoneSortie();
    lbType = new JLabel();
    tfLongueur = new RiZoneSortie();
    tfCode = new RiZoneSortie();
    lbLongueur = new JLabel();
    lbCode = new JLabel();
    panel3 = new JPanel();
    cbZone1 = new XRiComboBox();
    lbDebut = new JLabel();
    lbFIn = new JLabel();
    tfDebut = new XRiTextField();
    tfFin = new XRiTextField();
    cbZone2 = new XRiComboBox();
    tfDebut2 = new XRiTextField();
    tfFin2 = new XRiTextField();
    lbDebut2 = new JLabel();
    lbFIn2 = new JLabel();
    
    // ======== this ========
    setTitle("Mode expert");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(1015, 355));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== panel1 ========
      {
        panel1.setBackground(new Color(238, 238, 210));
        panel1.setName("panel1");
        panel1.setLayout(null);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Champ ERP"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);
          
          // ---- tflibelleLong ----
          tflibelleLong.setText("text");
          tflibelleLong.setFont(tflibelleLong.getFont().deriveFont(tflibelleLong.getFont().getSize() + 2f));
          tflibelleLong.setName("tflibelleLong");
          panel2.add(tflibelleLong);
          tflibelleLong.setBounds(110, 80, 300, 30);
          
          // ---- lbLibelleLong ----
          lbLibelleLong.setText("Libell\u00e9");
          lbLibelleLong.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLibelleLong.setFont(lbLibelleLong.getFont().deriveFont(lbLibelleLong.getFont().getSize() + 2f));
          lbLibelleLong.setName("lbLibelleLong");
          panel2.add(lbLibelleLong);
          lbLibelleLong.setBounds(15, 83, 80, 25);
          
          // ---- tfType ----
          tfType.setText("text");
          tfType.setFont(tfType.getFont().deriveFont(tfType.getFont().getSize() + 2f));
          tfType.setName("tfType");
          panel2.add(tfType);
          tfType.setBounds(110, 125, 300, 30);
          
          // ---- lbType ----
          lbType.setText("Type");
          lbType.setHorizontalAlignment(SwingConstants.RIGHT);
          lbType.setFont(lbType.getFont().deriveFont(lbType.getFont().getSize() + 2f));
          lbType.setName("lbType");
          panel2.add(lbType);
          lbType.setBounds(15, 128, 80, 25);
          
          // ---- tfLongueur ----
          tfLongueur.setText("text");
          tfLongueur.setFont(tfLongueur.getFont().deriveFont(tfLongueur.getFont().getSize() + 2f));
          tfLongueur.setName("tfLongueur");
          panel2.add(tfLongueur);
          tfLongueur.setBounds(110, 170, 70, 30);
          
          // ---- tfCode ----
          tfCode.setText("text");
          tfCode.setFont(tfCode.getFont().deriveFont(tfCode.getFont().getSize() + 2f));
          tfCode.setName("tfCode");
          panel2.add(tfCode);
          tfCode.setBounds(110, 35, 70, 30);
          
          // ---- lbLongueur ----
          lbLongueur.setText("Longueur");
          lbLongueur.setHorizontalAlignment(SwingConstants.RIGHT);
          lbLongueur.setFont(lbLongueur.getFont().deriveFont(lbLongueur.getFont().getSize() + 2f));
          lbLongueur.setName("lbLongueur");
          panel2.add(lbLongueur);
          lbLongueur.setBounds(15, 173, 80, 25);
          
          // ---- lbCode ----
          lbCode.setText("Nom");
          lbCode.setHorizontalAlignment(SwingConstants.RIGHT);
          lbCode.setFont(lbCode.getFont().deriveFont(lbCode.getFont().getSize() + 2f));
          lbCode.setName("lbCode");
          panel2.add(lbCode);
          lbCode.setBounds(15, 38, 80, 25);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        panel1.add(panel2);
        panel2.setBounds(15, 10, 460, 230);
        
        // ======== panel3 ========
        {
          panel3.setBorder(new TitledBorder("Association au catalogue"));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- cbZone1 ----
          cbZone1.setFont(cbZone1.getFont().deriveFont(cbZone1.getFont().getSize() + 2f));
          cbZone1.setBackground(new Color(171, 148, 79));
          cbZone1.setMinimumSize(new Dimension(68, 30));
          cbZone1.setPreferredSize(new Dimension(68, 30));
          cbZone1.setName("cbZone1");
          cbZone1.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbZone1ItemStateChanged(e);
            }
          });
          panel3.add(cbZone1);
          cbZone1.setBounds(25, 35, 275, 30);
          
          // ---- lbDebut ----
          lbDebut.setText("D\u00e9but");
          lbDebut.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDebut.setName("lbDebut");
          panel3.add(lbDebut);
          lbDebut.setBounds(310, 35, 40, 30);
          
          // ---- lbFIn ----
          lbFIn.setText("Fin");
          lbFIn.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFIn.setName("lbFIn");
          panel3.add(lbFIn);
          lbFIn.setBounds(400, 40, 30, 20);
          
          // ---- tfDebut ----
          tfDebut.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfDebut.setMinimumSize(new Dimension(12, 30));
          tfDebut.setPreferredSize(new Dimension(12, 30));
          tfDebut.setName("tfDebut");
          tfDebut.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfDebutKeyReleased(e);
            }
          });
          panel3.add(tfDebut);
          tfDebut.setBounds(360, 35, 40, tfDebut.getPreferredSize().height);
          
          // ---- tfFin ----
          tfFin.setPreferredSize(new Dimension(12, 30));
          tfFin.setMinimumSize(new Dimension(12, 30));
          tfFin.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfFin.setName("tfFin");
          tfFin.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfFinKeyReleased(e);
            }
          });
          panel3.add(tfFin);
          tfFin.setBounds(440, 36, 40, 28);
          
          // ---- cbZone2 ----
          cbZone2.setFont(cbZone2.getFont().deriveFont(cbZone2.getFont().getSize() + 2f));
          cbZone2.setBackground(new Color(171, 148, 79));
          cbZone2.setMinimumSize(new Dimension(68, 30));
          cbZone2.setName("cbZone2");
          cbZone2.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbZone2ItemStateChanged(e);
            }
          });
          panel3.add(cbZone2);
          cbZone2.setBounds(25, 80, 275, 30);
          
          // ---- tfDebut2 ----
          tfDebut2.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfDebut2.setPreferredSize(new Dimension(12, 30));
          tfDebut2.setName("tfDebut2");
          tfDebut2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfDebut2KeyReleased(e);
            }
          });
          panel3.add(tfDebut2);
          tfDebut2.setBounds(360, 81, 40, 28);
          
          // ---- tfFin2 ----
          tfFin2.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfFin2.setMinimumSize(new Dimension(12, 30));
          tfFin2.setName("tfFin2");
          tfFin2.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfFin2KeyReleased(e);
            }
          });
          panel3.add(tfFin2);
          tfFin2.setBounds(440, 81, 40, 28);
          
          // ---- lbDebut2 ----
          lbDebut2.setText("D\u00e9but");
          lbDebut2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDebut2.setName("lbDebut2");
          panel3.add(lbDebut2);
          lbDebut2.setBounds(310, 80, 40, 30);
          
          // ---- lbFIn2 ----
          lbFIn2.setText("Fin");
          lbFIn2.setHorizontalAlignment(SwingConstants.RIGHT);
          lbFIn2.setName("lbFIn2");
          panel3.add(lbFIn2);
          lbFIn2.setBounds(400, 85, 30, 20);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        panel1.add(panel3);
        panel3.setBounds(485, 10, 505, 230);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      pnlPrincipal.add(panel1, BorderLayout.CENTER);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNBarreBouton snBarreBouton;
  private JPanel panel1;
  private JPanel panel2;
  private RiZoneSortie tflibelleLong;
  private JLabel lbLibelleLong;
  private RiZoneSortie tfType;
  private JLabel lbType;
  private RiZoneSortie tfLongueur;
  private RiZoneSortie tfCode;
  private JLabel lbLongueur;
  private JLabel lbCode;
  private JPanel panel3;
  private XRiComboBox cbZone1;
  private JLabel lbDebut;
  private JLabel lbFIn;
  private XRiTextField tfDebut;
  private XRiTextField tfFin;
  private XRiComboBox cbZone2;
  private XRiTextField tfDebut2;
  private XRiTextField tfFin2;
  private JLabel lbDebut2;
  private JLabel lbFIn2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
