/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.propositionsurconditionnement;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de la boîte de dialogue pour le détail d'une ligne
 */
public class VueProposerSurconditionnement extends AbstractVueDialogue<ModeleProposerSurconditionnement> {
  /**
   * Constructeur.
   */
  public VueProposerSurconditionnement(ModeleProposerSurconditionnement pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Les raccourcis claviers
    rbQuantiteSaisie.setMnemonic(KeyEvent.VK_C);
    rbQuantiteSuperieure.setMnemonic(KeyEvent.VK_S);
    rbQuantiteSaisie.setSelected(true);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    rafraichirLibelleQuantiteSaisie();
    rafraichirLibelleQuantiteSuperieure();
  }
  
  // -- Méthodes privées
  
  private void rafraichirLibelleQuantiteSaisie() {
    StringBuffer buffer = getModele().getLibelleQuantiteSaisie();
    if (buffer != null) {
      rbQuantiteSaisie.setText(buffer.toString());
    }
    else {
      rbQuantiteSaisie.setText("");
    }
    rbQuantiteSaisie.setSelected(getModele().isSaisieSurconditionnement());
  }
  
  private void rafraichirLibelleQuantiteSuperieure() {
    StringBuffer buffer = getModele().getLibelleQuantiteSuperieure();
    if (buffer != null) {
      rbQuantiteSuperieure.setText(buffer.toString());
    }
    else {
      rbQuantiteSuperieure.setText("");
    }
  }
  
  // -- Méthodes interractives
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbQuantiteSaisieItemStateChanged(ItemEvent e) {
    getModele().setSaisieSurconditionnement(false);
  }
  
  private void rbQuantiteSuperieureItemStateChanged(ItemEvent e) {
    getModele().setSaisieSurconditionnement(true);
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlCommentaire = new JPanel();
    rbQuantiteSaisie = new JRadioButton();
    rbQuantiteSuperieure = new JRadioButton();
    lbMessageSurconditionnement = new JLabel();
    snBarreBouton = new SNBarreBouton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setTitle("Commander au surconditionnement sup\u00e9rieur");
    setBackground(new Color(238, 238, 210));
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(800, 300));
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlCommentaire ========
      {
        pnlCommentaire.setOpaque(false);
        pnlCommentaire.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlCommentaire.setName("pnlCommentaire");
        pnlCommentaire.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlCommentaire.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCommentaire.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- rbQuantiteSaisie ----
        rbQuantiteSaisie.setText("Conserver la quantit\u00e9 saisie");
        rbQuantiteSaisie.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbQuantiteSaisie.setName("rbQuantiteSaisie");
        rbQuantiteSaisie.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbQuantiteSaisieItemStateChanged(e);
          }
        });
        pnlCommentaire.add(rbQuantiteSaisie,
            new GridBagConstraints(0, 1, 1, 1, 0.0, 0.2, GridBagConstraints.WEST, GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- rbQuantiteSuperieure ----
        rbQuantiteSuperieure.setText("Choisir le surconditionnement sup\u00e9rieur");
        rbQuantiteSuperieure.setFont(new Font("sansserif", Font.PLAIN, 14));
        rbQuantiteSuperieure.setName("rbQuantiteSuperieure");
        rbQuantiteSuperieure.addItemListener(new ItemListener() {
          @Override
          public void itemStateChanged(ItemEvent e) {
            rbQuantiteSuperieureItemStateChanged(e);
          }
        });
        pnlCommentaire.add(rbQuantiteSuperieure, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.5, GridBagConstraints.NORTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbMessageSurconditionnement ----
        lbMessageSurconditionnement.setText("La quantit\u00e9 command\u00e9e est proche du surconditionnement sup\u00e9rieur :");
        lbMessageSurconditionnement.setHorizontalAlignment(SwingConstants.CENTER);
        lbMessageSurconditionnement.setFont(new Font("sansserif", Font.BOLD, 14));
        lbMessageSurconditionnement.setName("lbMessageSurconditionnement");
        pnlCommentaire.add(lbMessageSurconditionnement, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.5, GridBagConstraints.SOUTHWEST,
            GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlPrincipal.add(pnlCommentaire, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    
    // ---- btgMode ----
    btgMode.add(rbQuantiteSaisie);
    btgMode.add(rbQuantiteSuperieure);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private JPanel pnlCommentaire;
  private JRadioButton rbQuantiteSaisie;
  private JRadioButton rbQuantiteSuperieure;
  private JLabel lbMessageSurconditionnement;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
