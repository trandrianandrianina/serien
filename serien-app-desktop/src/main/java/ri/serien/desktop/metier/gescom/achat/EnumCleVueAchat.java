/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import ri.serien.libswing.moteur.mvc.InterfaceCleVue;

/**
 * Clé des vues de la gestion des achats.
 */
public enum EnumCleVueAchat implements InterfaceCleVue {
  ONGLET_FOURNISSEUR(0, "Fournisseur"),
  ONGLET_LIVRAISONENLEVEMENT(1, "Livraison enlèvement"),
  ONGLET_ARTICLE(2, "Article"),
  ONGLET_EDITION(3, "Edition");
  
  private final Integer index;
  private final String libelle;
  
  /**
   * Constructeur.
   *
   * @param pIndex Indice de la vue.
   * @param pLibelle Libellé de la vue.
   */
  EnumCleVueAchat(Integer pIndex, String pLibelle) {
    index = pIndex;
    libelle = pLibelle;
  }
  
  /**
   * Indice de la vue.
   */
  @Override
  public Integer getIndex() {
    return index;
  }
  
  /**
   * Libellé de la vue.
   */
  @Override
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Clé de la vue résumé dans une chaîne de caractères.
   */
  @Override
  public String toString() {
    return String.valueOf(libelle + "(" + index + ")");
  }
  
  /**
   * Retourner l'objet énum par son indice.
   */
  static public EnumCleVueAchat valueOfByCode(Integer pIndex) {
    for (EnumCleVueAchat value : values()) {
      if (pIndex == value.getIndex()) {
        return value;
      }
    }
    return null;
  }
  
}
