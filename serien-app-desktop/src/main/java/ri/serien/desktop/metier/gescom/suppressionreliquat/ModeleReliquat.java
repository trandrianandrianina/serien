/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.suppressionreliquat;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle de l'écran principal de la consultation des carnets de bon de cour.
 */
public class ModeleReliquat extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Suppression des reliquats";
  public static final String TITRE_CONSULTATION = "Suppression des reliquats (consultation des lignes de ventes)";
  public static final int MODE_RECHERCHE = 0;
  public static final int MODE_CONSULTATION = 1;
  
  // Variables communes
  private ListeEtablissement listeEtablissement = null;
  private ListeMagasin listeMagasin = null;
  private ListeVendeur listeVendeur = null;
  private UtilisateurGescom utilisateurGescom = null;
  private BigDecimal montantRestantAconsommer = null;
  private BigDecimal montantAcompte = null;
  
  // Variables pour la gestion de la vue recherche
  private Message titreResultat = null;
  private IdClient idClient = null;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private Vendeur vendeur = null;
  private Integer numeroDocument = null;
  private boolean rechercheLancee = false;
  private ModeleDocumentVenteParType modeleDocumentVenteParType;
  private boolean activationOngletListeDocument = true;
  
  // Variables pour la gestion de la vue consultation
  private ListeUnite listeUnite = null;
  private Client clientEnCours = null;
  private int modeUtilisation = MODE_CONSULTATION;
  private DocumentVente documentVenteNouveau = null;
  private ListeLigneVente listeLigneVenteNouveau = null;
  private ArrayList<BigDecimal> quantitesRestantesAAfficher = new ArrayList<BigDecimal>();
  
  /**
   * Constructeur.
   */
  public ModeleReliquat(SessionBase pSession) {
    super(pSession);
    // Mise à jour du titre de l'onglet de la session
    getSession().getPanel().setName(TITRE);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
    // Se mettre en mode liste par défaut
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    
    // Effacer le message
    titreResultat = null;
  }
  
  @Override
  public void chargerDonnees() {
    // Charger l'utilisateur
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    // Charger la liste des établissements
    listeEtablissement = ListeEtablissement.charger(getIdSession());
    
    if (utilisateurGescom != null) {
      // Initialisation de l'id de l'établissement
      idEtablissement = utilisateurGescom.getIdEtablissementPrincipal();
      
      // Charger tous les données liées à l'établissement
      chargerDonneesLieesEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    }
    
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  public void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    
    // Charger la liste des magasins
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
    }
    else {
      listeMagasin.clear();
    }
    listeMagasin = listeMagasin.charger(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    // Initialisation du magasin par défaut
    idMagasin = utilisateurGescom.getIdMagasin();
    if (idMagasin == null && listeMagasin != null && !listeMagasin.isEmpty()) {
      idMagasin = listeMagasin.get(0).getId();
    }
    
    // Charger la liste des vendeurs
    if (listeVendeur == null) {
      listeVendeur = new ListeVendeur();
    }
    else {
      listeVendeur.clear();
    }
    listeVendeur = listeVendeur.charger(getSession().getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    // Initialisation du vendeur par défaut
    IdVendeur idVendeur = utilisateurGescom.getIdVendeur();
    if (idVendeur == null && listeVendeur != null && !listeVendeur.isEmpty()) {
      vendeur = new Vendeur(listeVendeur.get(0).getId());
    }
    else {
      vendeur = new Vendeur(idVendeur);
    }
  }
  
  /**
   * Solde le reliquat du document sélectionné.
   */
  public void solderReliquat() {
    // Récupérer le document de ventes sélectionné
    DocumentVenteBase documentVenteBase = getDocumentVenteBaseSelectionne();
    
    // Vérifier les pré-requis
    if (documentVenteBase == null) {
      throw new MessageErreurException("Aucun document de ventes n'est sélectionné.");
    }
    if (documentVenteBase.isVerrouille()) {
      throw new MessageErreurException("Impossible d'ouvrir le document de ventes car il est verrouillé.");
    }
    
    // Construction de la demande de confirmation en fonction du type du document
    String message = null;
    if (documentVenteBase.isDevis()) {
      message = "Le devis que vous avez sélectionné va être soldé.\nConfirmez-vous cette opération ?";
    }
    else if (documentVenteBase.isCommande()) {
      message = "La commande que vous avez sélectionné va être soldée.\nConfirmez-vous cette opération ?";
      if (isAcomptePartiellementConsomme(documentVenteBase)) {
        message = "Vous souhaitez solder le reliquat de la commande " + documentVenteBase.getTexte() + ".\nIl reste un montant de "
            + Constantes.formater(montantRestantAconsommer, true) + " euros à consommer sur" + " l'acompte total versé de "
            + Constantes.formater(montantAcompte, true) + " euros. Si vous confirmez l'opération, la commande sera soldée. Le"
            + " remboursement éventuel de l'acompte restant devra être géré en comptabilité.";
      }
    }
    else {
      throw new MessageErreurException("Le type du document n'est pas valide.");
    }
    // Affichage d'une demande de confirmation
    if (!DialogueConfirmation.afficher(getSession(), "Confirmation du solde du reliquat", message)) {
      return;
    }
    
    // Appel du service qui permet de solder le reliquat
    ManagerServiceDocumentVente.solderReliquat(getIdSession(), documentVenteBase.getId());
    
    // Rafraichit la recherche
    rechercher(false);
    afficherRechercheDocument();
  }
  
  /**
   * Fermer la boîte de dialogue en validant les modifications.
   * Les données modifiées sont conservées.
   */
  public void quitterAvecValidation() {
    getSession().fermerEcran();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  // -----------------------------------------------------------------------------------
  // -- Gestion de la vue recherche (méthodes publiques)
  // ------------------------------------------------------------------------------------
  
  /**
   * Initialiser le résultat de la recherche (remet les valeurs par défaut aux filtres).
   */
  public void initialiserRecherche() {
    // Initialiser les filtres
    idClient = null;
    numeroDocument = null;
    chargerDonneesLieesEtablissement(idEtablissement);
    
    // Effacer les données
    modeleDocumentVenteParType = null;
    rechercheLancee = false;
    
    rafraichir();
  }
  
  /**
   * Recherche les carnets.
   */
  public void rechercher(boolean pRafraichirVue) {
    rechercheLancee = true;
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    IdVendeur idVendeur = null;
    if (vendeur != null) {
      idVendeur = vendeur.getId();
    }
    int numeroDocumentVente = 0;
    if (numeroDocument != null) {
      numeroDocumentVente = numeroDocument.intValue();
    }
    
    // Charger les documents de ventes du client
    modeleDocumentVenteParType = new ModeleDocumentVenteParType(getSession().getIdSession());
    modeleDocumentVenteParType.charger(getSession().getIdSession(), idClient, idEtablissement, idMagasin, idVendeur, numeroDocumentVente);
    
    // Permet de se positionner sur le bon onglet
    activationOngletListeDocument = true;
    
    // Rafraîchir
    if (pRafraichirVue) {
      rafraichir();
    }
  }
  
  /**
   * Modifier la plage de devis affichés.
   */
  public void modifierPlageDevis(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageDevis(pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier la plage de commandes affichées.
   */
  public void modifierPlageCommande(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    if (modeleDocumentVenteParType != null) {
      modeleDocumentVenteParType.modifierPlageCommande(pIndexPremiereLigne, pIndexDerniereLigne);
    }
  }
  
  /**
   * Modifier le devis sélectionné dans la liste.
   */
  public void selectionnerDevis(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexDevisSelectionne()) {
      return;
    }
    modeleDocumentVenteParType.setIndexDevisSelectionne(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Modifier la commande sélectionnée dans la liste.
   */
  public void selectionnerCommande(int pLigneSelectionnee) {
    if (pLigneSelectionnee == modeleDocumentVenteParType.getIndexCommandeSelectionnee()) {
      return;
    }
    modeleDocumentVenteParType.setIndexCommandeSelectionnee(pLigneSelectionnee);
    rafraichir();
  }
  
  /**
   * Document de ventes sélectionné dans le tableau de la liste des document de ventes du client.
   */
  public DocumentVenteBase getDocumentVenteBaseSelectionne() {
    if (modeleDocumentVenteParType != null) {
      return modeleDocumentVenteParType.getDocumentVenteBaseSelectionne();
    }
    return null;
  }
  
  /**
   * Modifier le client sélectionné.
   */
  public void modifierClient(IdClient pIdClient) {
    if (Constantes.equals(idClient, pIdClient)) {
      return;
    }
    
    idClient = pIdClient;
    rafraichir();
  }
  
  /**
   * Modifie le numéro du document à rechercher.
   */
  public void modifierNumeroDocument(String pNumero) {
    // Conversion du numéro saisi en entier
    Integer numero = Constantes.convertirTexteEnInteger(pNumero);
    
    // Si le numéro saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (numeroDocument != null && numeroDocument.compareTo(numero) == 0) {
      return;
    }
    
    // Si le numéro vaut 0 le numéro du carnet mémorisé est mis à null
    if (numero.intValue() <= 0) {
      numeroDocument = null;
    }
    else {
      numeroDocument = numero;
    }
    rafraichir();
  }
  
  /**
   * Modifie l'établissement.
   */
  public void modifierEtablissement(IdEtablissement pIdEtablissement) {
    if (Constantes.equals(idEtablissement, pIdEtablissement)) {
      return;
    }
    
    // Si le chargement se passe bien alors mise à jour des données liées à l'établissement
    try {
      chargerDonneesLieesEtablissement(pIdEtablissement);
      idEtablissement = pIdEtablissement;
    }
    catch (MessageErreurException e) {
      // Dans le cas contraire rechargement avec l'ancien établissement
      chargerDonneesLieesEtablissement(pIdEtablissement);
      throw e;
    }
    // La méthode rafraichir() est appelée par initialiserRecherche
    initialiserRecherche();
  }
  
  /**
   * Modifie le magasin.
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    if (Constantes.equals(idMagasin, pIdMagasin)) {
      return;
    }
    idMagasin = pIdMagasin;
    rafraichir();
  }
  
  /**
   * Modifie le vendeur.
   */
  public void modifierVendeur(Vendeur pVendeur) {
    if (pVendeur != null && pVendeur.equals(vendeur)) {
      return;
    }
    
    vendeur = pVendeur;
    rafraichir();
  }
  
  /**
   * Retourne le nom d'un client à partir de son identifiant.
   */
  public String retournerNomClient(DocumentVenteBase pDocumentVenteBase) {
    if (pDocumentVenteBase == null || pDocumentVenteBase.getComplement() == null) {
      return "";
    }
    if (pDocumentVenteBase.getComplement() instanceof ClientBase) {
      ClientBase client = (ClientBase) pDocumentVenteBase.getComplement();
      return client.getTexte();
    }
    
    return "";
  }
  
  /**
   * Retourne le nom d'un vendeur à partir de son identifiant.
   */
  public String retournerNomVendeur(IdVendeur pIdVendeur) {
    if (listeVendeur == null) {
      return null;
    }
    Vendeur vendeur = listeVendeur.get(pIdVendeur);
    if (vendeur == null) {
      return "";
    }
    return vendeur.getTexte();
  }
  
  /**
   * Affiche l'écran de consultation des lignes de ventes.
   */
  public void afficherConsultationLigneVente() {
    // Récupérer le document de ventes sélectionné
    DocumentVenteBase documentVenteBase = getDocumentVenteBaseSelectionne();
    
    // Chargement des données
    chargerDonneesDocument(documentVenteBase);
    
    // Mise à jour de l'écran
    setTitreEcran(TITRE_CONSULTATION);
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    modeUtilisation = MODE_CONSULTATION;
    rafraichir();
  }
  
  // -----------------------------------------------------------------------------------
  // -- Gestion de la vue consultation (méthodes publiques)
  // ------------------------------------------------------------------------------------
  
  /**
   * Affiche l'écran de recherche des documents.
   */
  public void afficherRechercheDocument() {
    // Nettoyage des variables de l'écran
    effacerVariableDocument();
    
    // Mettre à jour l'écran
    setTitreEcran(TITRE);
    modeUtilisation = MODE_RECHERCHE;
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    rafraichir();
  }
  
  /**
   * Retourne la quantité restant à prendre.
   */
  public String retournerQuantiteRestanteAAfficher(int pIndiceLigne) {
    if (quantitesRestantesAAfficher.isEmpty() || pIndiceLigne >= quantitesRestantesAAfficher.size()) {
      return "";
    }
    return Constantes.formater(quantitesRestantesAAfficher.get(pIndiceLigne), false);
  }
  
  /**
   * Retourne la quantité restant à prendre pour une ligne d'un indice donné.
   */
  public BigDecimal retournerQuantiteRestante(int pIndiceLigne, DocumentVenteBase pDocumentVenteBase) {
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return BigDecimal.ZERO;
    }
    
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    if (ligneVente == null || ligneVente.getId() == null) {
      return BigDecimal.ZERO;
    }
    // La ligne de vente est une ligne commentaire
    if (ligneVente.isLigneCommentaire()) {
      if ((pDocumentVenteBase.isFacture() && ligneVente.isEditionSurFacture())) {
        return BigDecimal.ZERO;
      }
      return BigDecimal.ONE;
    }
    // La ligne de vente correspond à un article découpable
    if (ligneVente.isArticleDecoupable()) {
      return BigDecimal.valueOf(ligneVente.getNombreDecoupe());
    }
    // Dans les autres cas
    return ligneVente.getQuantiteDocumentOrigineUC().subtract(ligneVente.getQuantiteDejaExtraiteUC())
        .setScale(retournerPrecisionUnite(ligneVente.getIdUniteConditionnementVente()), RoundingMode.HALF_UP);
  }
  
  /**
   * Retourne la quantité disponible à la vente.
   */
  public String retournerQuantiteDisponible(int pIndiceLigne) {
    if (documentVenteNouveau == null) {
      return "0";
    }
    // Contrôle que l'indice ne soit pas hors du tableau (il faudrait peut être déclencher une exception)
    if (pIndiceLigne < 0 || pIndiceLigne >= listeLigneVenteNouveau.size()) {
      return "0";
    }
    
    // Appel au service stock afin de récupérer le stock détaillé pour cet article
    LigneVente ligneVente = listeLigneVenteNouveau.get(pIndiceLigne);
    StockDetaille stockDetaille = ManagerServiceStock.chargerStockDetaille(getIdSession(), ligneVente.getId().getIdEtablissement(),
        documentVenteNouveau.getIdMagasin(), ligneVente.getIdArticle());
    if (stockDetaille != null && stockDetaille.getStockDisponible() != null) {
      return Constantes.formater(stockDetaille.getStockDisponible(), false);
    }
    
    return "0";
  }
  
  /**
   * Formate et retourne la quantité d'origine d'une ligne de vente.
   */
  public String retournerQuantiteOrigine(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return "";
    }
    BigDecimal quantiteOrigine = pLigneVente.getQuantiteDocumentOrigineUC();
    return Constantes.formater(quantiteOrigine, false);
  }
  
  /**
   * Formate et retourne la quantité déjà extraite d'une ligne de vente.
   */
  public String retournerQuantiteDejaExtraite(LigneVente pLigneVente) {
    if (pLigneVente == null) {
      return "";
    }
    BigDecimal quantiteDejaExtraite = pLigneVente.getQuantiteDejaExtraiteUC();
    return Constantes.formater(quantiteDejaExtraite, false);
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les données du document consulté.
   */
  private void effacerVariableDocument() {
    listeLigneVenteNouveau = null;
    clientEnCours = null;
    documentVenteNouveau = null;
  }
  
  /**
   * Charge les données nécessaires au chargement d'un document de ventes.
   */
  private void chargerDonneesDocument(DocumentVenteBase pDocumentVenteBase) {
    // Vérifier les pré-requis
    if (pDocumentVenteBase == null) {
      throw new MessageErreurException("Aucun document de ventes n'est sélectionné.");
    }
    if (pDocumentVenteBase.isVerrouille()) {
      throw new MessageErreurException("Impossible d'ouvrir le document de ventes car il est verrouillé.");
    }
    
    // Chargement du client
    if (clientEnCours == null || !clientEnCours.getId().equals(pDocumentVenteBase.getIdClientFacture())) {
      clientEnCours = ManagerServiceClient.chargerClient(getSession().getIdSession(), pDocumentVenteBase.getIdClientFacture());
    }
    // Chargement des lignes du document sélectionné
    if (documentVenteNouveau == null || !documentVenteNouveau.getId().equals(pDocumentVenteBase.getId())) {
      documentVenteNouveau = chargerDocumentVente(pDocumentVenteBase.getId());
    }
  }
  
  /**
   * Charger toutes les informations d'un document de ventes.
   */
  private DocumentVente chargerDocumentVente(IdDocumentVente pIdDocumentVente) {
    // L'établissement
    Etablissement etablissement = null;
    if (listeEtablissement != null) {
      etablissement = listeEtablissement.getEtablissementParId(idEtablissement);
    }
    if (etablissement == null) {
      etablissement = new Etablissement(idEtablissement);
    }
    
    // Charger l'entête du document de ventes
    DocumentVente documentVenteNouveau =
        ManagerServiceDocumentVente.chargerEnteteDocumentVente(getSession().getIdSession(), pIdDocumentVente);
    
    // Charger le règlement sauf dans le cas d'un devis
    if (!documentVenteNouveau.isDevis()) {
      ListeReglement listeReglement = ManagerServiceDocumentVente.chargerListeReglement(getSession().getIdSession(),
          documentVenteNouveau.getId(), documentVenteNouveau.getDateDocument());
      documentVenteNouveau.setListeReglement(listeReglement);
      if (listeReglement != null) {
        listeReglement.initialiserInfoDocument(documentVenteNouveau);
      }
    }
    
    // Charger les lignes de ventes
    CritereLigneVente critereLigneVente = new CritereLigneVente();
    ListeLigneVente listeLigneVente = ManagerServiceDocumentVente.chargerListeLigneVente(getSession().getIdSession(), pIdDocumentVente,
        critereLigneVente, documentVenteNouveau.isTTC());
    
    // Ajouter les lignes au document de ventes
    for (LigneVente ligneVente : listeLigneVente) {
      // Initialiser le taux de TVA de la ligne
      ligneVente.initialiserTauxTVA(documentVenteNouveau, etablissement);
      
      // Stocker la ligne dans la liste qui va bien (en fonction de son état)
      if (ligneVente.isAnnulee()) {
        documentVenteNouveau.getListeLigneVenteSupprimee().add(ligneVente);
      }
      else {
        documentVenteNouveau.getListeLigneVente().add(ligneVente);
        
        // Initialiser le tarif dans le prix de vente de chaque ligne
        // Dans le cas d'un retour palette on n'initialise pas le tarif article car il peut y avoir des tarifs différents car on
        // applique le tarif de déconsignation qui avait cours au moment de l'achat de la palette.
        if (!ligneVente.isLigneCommentaire() && !ligneVente.isRetourPalette()) {
          if (clientEnCours != null) {
            ligneVente.setClientTTC(clientEnCours.isFactureEnTTC());
          }
        }
      }
    }
    
    // Traitement pour l'affichage
    listeLigneVenteNouveau = supprimerArticlesIndesirables(documentVenteNouveau);
    
    // Chargement des regroupements
    documentVenteNouveau.actualiserRegroupement(getIdSession());
    
    // Initialisation de la liste des quantités restantes à afficher (par défaut ce sont les quantités restantes réelles)
    initialiserQuantitesRestantes(false, documentVenteNouveau);
    
    return documentVenteNouveau;
  }
  
  /**
   * On créer une nouvelle liste de ligne article avec les lignes désirées.
   */
  private ListeLigneVente supprimerArticlesIndesirables(DocumentVente pDocumentVente) {
    ListeLigneVente listeLigneVenteSource = pDocumentVente.getListeLigneVente();
    ListeLigneVente listeLigneVenteResultat = new ListeLigneVente();
    for (LigneVente ligneVente : listeLigneVenteSource) {
      if (!ligneVente.isLigneRemiseSpeciale()) {
        listeLigneVenteResultat.add(ligneVente);
      }
    }
    return listeLigneVenteResultat;
  }
  
  /**
   * Initialise les quantités restantes des lignes articles affichées.
   */
  private void initialiserQuantitesRestantes(boolean pMiseAZero, DocumentVenteBase pDocumentVenteBase) {
    quantitesRestantesAAfficher.clear();
    if (pMiseAZero) {
      for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
        quantitesRestantesAAfficher.add(BigDecimal.ZERO);
      }
    }
    else {
      for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
        quantitesRestantesAAfficher.add(retournerQuantiteRestante(ligne, pDocumentVenteBase));
      }
    }
  }
  
  /**
   * Retourne si l'acompte d'une commande a été partiellement consommé.
   */
  private boolean isAcomptePartiellementConsomme(DocumentVenteBase pDocumentVenteBase) {
    // S'il ne s'agit pas d'une commande
    if (pDocumentVenteBase == null || !pDocumentVenteBase.isCommande()) {
      return false;
    }
    
    // Chargement des données pour le chargement de la commande
    chargerDonneesDocument(pDocumentVenteBase);
    // Si le montant de l'acompte restant à consommer est inférieur ou égal à zéro
    montantRestantAconsommer = getMontantAcompteTTCRestantAConsommer();
    if (montantRestantAconsommer != null && montantRestantAconsommer.compareTo(BigDecimal.ZERO) <= 0) {
      return false;
    }
    
    // Initialisation du momtant de l'acompte versé
    montantAcompte = documentVenteNouveau.getListeReglement().getMontantAcompteRegle();
    
    return true;
  }
  
  /**
   * Retourne le montant en TTC de l'acompte qu'il reste à consommé à la suite des différentes extractions.
   */
  private BigDecimal getMontantAcompteTTCRestantAConsommer() {
    if (documentVenteNouveau == null) {
      throw new MessageErreurException("La commande n'est pas valide.");
    }
    ListeReglement listeReglement = documentVenteNouveau.getListeReglement();
    if (!documentVenteNouveau.isCommande() || listeReglement == null || listeReglement.getMontantAcompteRegle() == null) {
      return null;
    }
    BigDecimal montantAcompte = listeReglement.getMontantAcompteRegle();
    BigDecimal montantTotalExtrait = BigDecimal.ZERO;
    
    // Récupération de tous les documents extraits de cette commande à partir des lignes
    List<IdDocumentVente> listeIdDocumentVenteExtraits = new ArrayList<IdDocumentVente>();
    for (LigneVente ligneVente : documentVenteNouveau.getListeLigneVente()) {
      // Création du lien d'origine (la commande d'origine)
      IdLienLigne idLienLigne =
          IdLienLigne.getInstanceVente(documentVenteNouveau.getId().getIdEtablissement(), EnumTypeLienLigne.LIEN_LIGNE_VENTE,
              documentVenteNouveau.getId().getEntete(), documentVenteNouveau.getId().getNumero(), ligneVente.getId().getSuffixe(), null,
              ligneVente.getId().getNumeroLigne(), documentVenteNouveau.getId(), ligneVente.getId().getNumeroLigne(), false);
      
      LienLigne lienOrigine = new LienLigne(idLienLigne);
      lienOrigine.setDocumentVente(documentVenteNouveau);
      lienOrigine.setLigne(ligneVente);
      
      // Chargement des liens (des documents) pour la ligne en cours
      ListeLienLigne listeLienLigne = ListeLienLigne.chargerTout(getSession().getIdSession(), lienOrigine.getId(), idMagasin);
      if (listeLienLigne == null || listeLienLigne.isEmpty()) {
        continue;
      }
      // Parcourt de la liste des liens pour faire la somme du total de chaque document déjà extrait
      for (LienLigne lienLigne : listeLienLigne) {
        if (lienLigne == null || lienLigne.getDocumentVente() == null
            || lienLigne.getDocumentVente().getId().equals(documentVenteNouveau.getId())) {
          continue;
        }
        IdDocumentVente id = lienLigne.getDocumentVente().getId();
        // Petit contrôle pour s'assurer que le document extrait n'est pas déjà dans la liste
        if (!listeIdDocumentVenteExtraits.contains(id)) {
          DocumentVente documentVente = lienLigne.getDocumentVente();
          // Calcul du montant total des documents extraits liés à la commande
          if (documentVente != null) {
            montantTotalExtrait = montantTotalExtrait.add(documentVente.getTotalTTC());
          }
          listeIdDocumentVenteExtraits.add(id);
        }
      }
    }
    
    // Calcul du montant de l'acompte restant
    return montantAcompte.subtract(montantTotalExtrait);
  }
  
  /**
   * Retourne la précision d'une unité donnée.
   */
  private int retournerPrecisionUnite(IdUnite pIdUnite) {
    return listeUnite.getPrecisionUnite(pIdUnite);
  }
  
  /**
   * Retourne le nombre de décimales de l'UC.
   */
  private int retournerNombreDecimalesUC(LigneVente pLignevente) {
    return retournerPrecisionUnite(pLignevente.getIdUniteConditionnementVente());
  }
  
  // -- Accesseurs
  
  public UtilisateurGescom getUtilisateurGescom() {
    return utilisateurGescom;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeVendeur getListeVendeur() {
    return listeVendeur;
  }
  
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public Vendeur getVendeur() {
    return vendeur;
  }
  
  public Integer getNumeroDocumentARecherher() {
    return numeroDocument;
  }
  
  public boolean isModeRecherche() {
    return modeUtilisation == MODE_RECHERCHE;
  }
  
  public boolean isModeConsultation() {
    return modeUtilisation == MODE_CONSULTATION;
  }
  
  public ModeleDocumentVenteParType getModeleDocumentVenteParType() {
    return modeleDocumentVenteParType;
  }
  
  public boolean isActivationOngletListeDocument() {
    return activationOngletListeDocument;
  }
  
  public void setActivationOngletListeDocument(boolean activationOngletListeDocument) {
    this.activationOngletListeDocument = activationOngletListeDocument;
  }
  
  public ListeLigneVente getListeLigneVenteNouveau() {
    return listeLigneVenteNouveau;
  }
  
}
