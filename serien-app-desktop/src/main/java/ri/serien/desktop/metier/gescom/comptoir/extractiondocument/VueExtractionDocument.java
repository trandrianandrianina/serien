/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.extractiondocument;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.ButtonGroup;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.gescom.vente.document.EnumOrigineDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composantrpg.autonome.liste.ModeleTableEditable;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueExtractionDocument extends AbstractVueDialogue<ModeleExtractionDocument> {
  // Constantes
  private static final String BOUTON_EFFACER_QUANTITE = "Effacer les quantités";
  private static final String BOUTON_RESTAURER_QUANTITE = "Restaurer les quantités";
  private static final String BOUTON_AFFICHER_DETAIL = "Afficher détail";
  private final static String[] TITRE_LISTE_COMMANDE = new String[] { "Quantit\u00e9", "UC", "Disponible vente", "Code article",
      "Libell\u00e9", "Quantit\u00e9 initiale", "Quantit\u00e9 déjà command\u00e9e" };
  private final static String[] TITRE_LISTE_BON = new String[] { "Quantit\u00e9", "UC", "Disponible vente", "Code article",
      "Libell\u00e9", "Quantit\u00e9 initiale", "Quantit\u00e9 déjà livr\u00e9e" };
  private final static String[] TITRE_LISTE_FACTURE = new String[] { "Quantit\u00e9", "UC", "Disponible vente", "Code article",
      "Libell\u00e9", "Quantit\u00e9 initiale", "Quantit\u00e9 déjà factur\u00e9e" };
  
  // Variables
  private int ligneListeArticles = 0;
  private JTableLignesArticleRenderer ligneArticleCelluleRenderer = new JTableLignesArticleRenderer();
  
  /**
   * Constructeur.
   */
  public VueExtractionDocument(ModeleExtractionDocument pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise les composants graphiques.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // La table contenant la liste des articles
    scpListeArticles.getViewport().setBackground(Color.WHITE);
    final boolean[] colonnesEditables = new boolean[] { true, false, false, false, false, false, false };
    final int[] dimension = new int[] { 70, 40, 120, 110, 360, 120, 190 };
    final int[] justification = new int[] { NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE,
        NRiTable.DROITE, NRiTable.DROITE };
    String[] titresColonnes = TITRE_LISTE_FACTURE;
    switch (getModele().getTypeExtraction()) {
      case EXTRACTION_DEVIS_EN_COMMANDE:
        titresColonnes = TITRE_LISTE_COMMANDE;
        break;
      case EXTRACTION_DEVIS_EN_BON:
      case EXTRACTION_COMMANDE_EN_BON:
        titresColonnes = TITRE_LISTE_BON;
        break;
      case EXTRACTION_DEVIS_EN_FACTURE:
      case EXTRACTION_COMMANDE_EN_FACTURE:
        titresColonnes = TITRE_LISTE_FACTURE;
        break;
      
      default:
        throw new MessageErreurException("Le type d'extraction suivant n'est pas géré : " + getModele().getTypeExtraction());
    }
    ModeleTableEditable modeleTable = new ModeleTableEditable(titresColonnes, colonnesEditables);
    tblListeArticles.personnaliserAspectCellules(ligneArticleCelluleRenderer, modeleTable, dimension, justification, 14);
    tblListeArticles.setSelectAllForEdit(true);
    
    // Permet de forcer la saisie d'une cellule si l'on clique sur un autre composant (le bouton valider par exemple)
    tblListeArticles.forcerSaisieCellule(true);
    
    // Les raccoucis clavier
    rbAvecReliquat.setMnemonic(KeyEvent.VK_A);
    rbSansReliquat.setMnemonic(KeyEvent.VK_S);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_EFFACER_QUANTITE, 'e', true);
    snBarreBouton.ajouterBouton(BOUTON_RESTAURER_QUANTITE, 'r', true);
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Etre notifier des changements de sélection
    tblListeArticles.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeArticlesValueChanged(e);
      }
    });
  }
  
  /**
   * Rafraichi l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirModeExtraction();
    rafraichirRecalcul();
    rafraichirListe();
    rafraichirBoutonDetail();
    rafraichirBoutonValider();
    
    // Positionnement du focus
    switch (getModele().getComposantAyantLeFocus()) {
      case ModeleExtractionDocument.FOCUS_GESTION_AVEC_RELIQUAT:
        rbAvecReliquat.requestFocusInWindow();
        break;
      case ModeleExtractionDocument.FOCUS_GESTION_SANS_RELIQUAT:
        rbSansReliquat.requestFocusInWindow();
        break;
      case ModeleExtractionDocument.FOCUS_LISTE_ARTICLES:
        tblListeArticles.requestFocusInWindow();
        break;
    }
  }
  
  // -- Méthodes privées
  
  private void rafraichirBoutonDetail() {
    boolean actif = tblListeArticles.getSelectedRow() >= 0;
    snBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL, actif);
  }
  
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isValidationPossible();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  private void rafraichirTitre() {
    if (getModele().getTitreEcran() != null) {
      setTitle(getModele().getTitreEcran());
    }
    else {
      setTitle("");
    }
  }
  
  private void rafraichirModeExtraction() {
    switch (getModele().getGestionReliquats()) {
      case ModeleExtractionDocument.EXTRACTION_AVEC_RELIQUAT:
        rbAvecReliquat.setSelected(true);
        rbSansReliquat.setSelected(false);
        break;
      case ModeleExtractionDocument.EXTRACTION_SANS_RELIQUAT:
        rbAvecReliquat.setSelected(false);
        rbSansReliquat.setSelected(true);
        break;
      default:
        rbAvecReliquat.setSelected(false);
        rbSansReliquat.setSelected(false);
    }
  }
  
  private void rafraichirRecalcul() {
    if (getModele().getTypeExtraction() != null
        && (getModele().getTypeExtraction().equals(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_BON)
            || getModele().getTypeExtraction().equals(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_COMMANDE)
            || getModele().getTypeExtraction().equals(EnumOrigineDocumentVente.EXTRACTION_DEVIS_EN_FACTURE))) {
      pnlRecalculPrix.setVisible(true);
      rbRecalculerPrix.setSelected(getModele().isRecalculParDefaut());
      rbConserverPrix.setSelected(!getModele().isRecalculParDefaut());
    }
    else {
      pnlRecalculPrix.setVisible(false);
    }
  }
  
  /**
   * Opérations à effectuer lorsque l'utilisateur appuie sur bouton valider.
   */
  private void quitterAvecValidation() {
    int nbrLignes = tblListeArticles.getRowCount();
    if (nbrLignes > 0) {
      // On récupère les quantités saisies dans les cellules de la liste
      BigDecimal[] quantites = new BigDecimal[nbrLignes];
      BigDecimal[] quantitesOrigines = new BigDecimal[nbrLignes];
      for (int ligne = 0; ligne < nbrLignes; ligne++) {
        String valeur = (String) tblListeArticles.getValueAt(ligne, 0);
        // Pour lignes debut et fin d'un regroupement où la quantité n'est pas affichée entre autres
        if (valeur == null || valeur.trim().isEmpty()) {
          valeur = "0";
        }
        String valeurOrigine = (String) tblListeArticles.getValueAt(ligne, 5);
        // Pour lignes debut et fin d'un regroupement où la quantité n'est pas affichée entre autres
        if (valeurOrigine == null || valeurOrigine.trim().isEmpty()) {
          valeurOrigine = "0";
        }
        // Conversion des quantités en valeurs numériques
        try {
          quantites[ligne] = Constantes.convertirTexteEnBigDecimal(valeur);
          quantitesOrigines[ligne] = Constantes.convertirTexteEnBigDecimal(valeurOrigine);
        }
        catch (Exception e) {
          // On sélectionne la ligne qui pose problème
          tblListeArticles.setRowSelectionInterval(ligne, ligne);
          return;
        }
      }
      getModele().modifierQuantiteAExtraire(quantites, quantitesOrigines);
    }
    getModele().quitterAvecValidation();
  }
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListe() {
    final List<LigneVente> listeLigneVenteNouveau = getModele().getListeLigneVenteNouveau();
    if (listeLigneVenteNouveau == null) {
      return;
    }
    
    // Préparer les données pour le tableau
    final String[][] donnees = new String[listeLigneVenteNouveau.size()][tblListeArticles.getColumnCount()];
    final boolean[][] cellulesEditables = new boolean[listeLigneVenteNouveau.size()][tblListeArticles.getColumnCount()];
    for (int ligne = 0; ligne < listeLigneVenteNouveau.size(); ligne++) {
      LigneVente ligneVente = listeLigneVenteNouveau.get(ligne);
      // Les données
      donnees[ligne][0] = getModele().getQuantiteRestanteAAfficher(ligne);
      if (ligneVente.isLigneCommentaire()) {
        if (ligneVente.isDebutRegroupement() || ligneVente.isFinRegroupement()) {
          donnees[ligne][0] = "";
          donnees[ligne][1] = "";
          donnees[ligne][2] = "";
          donnees[ligne][3] = "";
        }
        else {
          donnees[ligne][1] = "*";
          donnees[ligne][2] = "";
          donnees[ligne][3] = "Commentaire";
        }
      }
      else {
        if (ligneVente.getIdUniteConditionnementVente() != null) {
          donnees[ligne][1] = ligneVente.getIdUniteConditionnementVente().getCode();
        }
        BigDecimal quantiteDisponible = getModele().getQuantiteDisponibleMagasin(ligneVente.getIdArticle());
        if (quantiteDisponible == null) {
          donnees[ligne][2] = "";
        }
        else {
          donnees[ligne][2] = Constantes.formater(quantiteDisponible, false);
        }
        donnees[ligne][3] = ligneVente.getIdArticle().getCodeArticle();
      }
      
      // S'il s'agit du début ou de la fin d'un regroupement
      if (ligneVente.isDebutRegroupement() || ligneVente.isFinRegroupement()) {
        if (ligneVente.getLibelle() != null) {
          donnees[ligne][4] = "<html><i>" + ligneVente.getLibelle() + "</i></html>";
        }
        donnees[ligne][5] = "";
        donnees[ligne][6] = "";
      }
      // Les autres types de lignes
      else {
        donnees[ligne][4] = "<html>" + ligneVente.getLibelle() + "</html>";
        donnees[ligne][5] = getModele().getQuantiteOrigine(ligneVente);
        donnees[ligne][6] = getModele().getQuantiteDejaExtraite(ligneVente);
      }
      
      // La cellule est-elle éditable ?
      // Si l'id de la ligne est null alors il s'agit d'une ligne article où la quantité est à 0 donc non éditable
      // S'il s'agit d'une ligne début ou fin d'un regroupement elle n'est pas éditable
      // S'il s'agit d'un commentaire, la ligne est éditable pour pouvoir la passer à 0 et donc ne pas extraire le commentaire
      if (ligne < listeLigneVenteNouveau.size() && ligneVente.getId() != null && !ligneVente.isDebutRegroupement()
          && !ligneVente.isFinRegroupement()
          && (ligneVente.getQuantiteDejaExtraite().compareTo(ligneVente.getQuantiteDocumentOrigine()) < 0
              || ligneVente.isLigneCommentaire())) {
        cellulesEditables[ligne][0] = true;
      }
      else {
        cellulesEditables[ligne][0] = false;
      }
      cellulesEditables[ligne][1] = false;
      cellulesEditables[ligne][2] = false;
      cellulesEditables[ligne][3] = false;
      cellulesEditables[ligne][4] = false;
      cellulesEditables[ligne][5] = false;
      cellulesEditables[ligne][6] = false;
    }
    
    ((ModeleTableEditable) tblListeArticles.getModel()).setCellulesEditables(cellulesEditables);
    ligneArticleCelluleRenderer.setListeLigneVente(listeLigneVenteNouveau);
    tblListeArticles.mettreAJourDonnees(donnees);
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_EFFACER_QUANTITE)) {
        getModele().effacerQuantites();
      }
      else if (pSNBouton.isBouton(BOUTON_RESTAURER_QUANTITE)) {
        getModele().restaurerQuantites();
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL)) {
        getModele().afficherDetail(tblListeArticles.getSelectedRow());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbAvecReliquatActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierGestionAvecReliquats();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbSansReliquatActionPerformed(ActionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierGestionSansReliquats();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeArticlesValueChanged(ListSelectionEvent e) {
    try {
      rafraichirBoutonDetail();
      rafraichirBoutonValider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeArticleMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        getModele().afficherDetail(tblListeArticles.getSelectedRow());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbConserverPrixActionPerformed(ActionEvent e) {
    try {
      getModele().setRecalculerPrix(false);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rbRecalculerPrixActionPerformed(ActionEvent e) {
    try {
      getModele().setRecalculerPrix(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlFond = new SNPanelFond();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlOptions = new SNPanel();
    pnlTypeExtraction = new SNPanelTitre();
    rbAvecReliquat = new SNRadioButton();
    rbSansReliquat = new SNRadioButton();
    pnlRecalculPrix = new SNPanelTitre();
    rbConserverPrix = new SNRadioButton();
    rbRecalculerPrix = new SNRadioButton();
    lbListeArticles = new SNLabelTitre();
    scpListeArticles = new JScrollPane();
    tblListeArticles = new NRiTable();
    buttonGroup2 = new ButtonGroup();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 400));
    setForeground(Color.black);
    setTitle("Extraction d'un document");
    setBackground(new Color(238, 238, 210));
    setResizable(false);
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlFond ========
    {
      pnlFond.setName("pnlFond");
      pnlFond.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlFond.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlOptions ========
        {
          pnlOptions.setName("pnlOptions");
          pnlOptions.setLayout(new GridLayout(1, 2));
          
          // ======== pnlTypeExtraction ========
          {
            pnlTypeExtraction.setOpaque(false);
            pnlTypeExtraction.setTitre("Type d'extraction");
            pnlTypeExtraction.setName("pnlTypeExtraction");
            pnlTypeExtraction.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTypeExtraction.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTypeExtraction.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTypeExtraction.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTypeExtraction.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- rbAvecReliquat ----
            rbAvecReliquat.setText("<html><u>A</u>vec reliquat</html>");
            rbAvecReliquat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbAvecReliquat.setName("rbAvecReliquat");
            rbAvecReliquat.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbAvecReliquatActionPerformed(e);
              }
            });
            pnlTypeExtraction.add(rbAvecReliquat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbSansReliquat ----
            rbSansReliquat.setText("<html><u>S</u>ans reliquat</html>");
            rbSansReliquat.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbSansReliquat.setName("rbSansReliquat");
            rbSansReliquat.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbSansReliquatActionPerformed(e);
              }
            });
            pnlTypeExtraction.add(rbSansReliquat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlOptions.add(pnlTypeExtraction);
          
          // ======== pnlRecalculPrix ========
          {
            pnlRecalculPrix.setTitre("Prix");
            pnlRecalculPrix.setName("pnlRecalculPrix");
            pnlRecalculPrix.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRecalculPrix.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlRecalculPrix.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlRecalculPrix.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRecalculPrix.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- rbConserverPrix ----
            rbConserverPrix.setText("Conserver les prix du document d\u2019origine");
            rbConserverPrix.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbConserverPrix.setPreferredSize(new Dimension(350, 30));
            rbConserverPrix.setMinimumSize(new Dimension(350, 30));
            rbConserverPrix.setMaximumSize(new Dimension(350, 30));
            rbConserverPrix.setName("rbConserverPrix");
            rbConserverPrix.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbConserverPrixActionPerformed(e);
              }
            });
            pnlRecalculPrix.add(rbConserverPrix, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbRecalculerPrix ----
            rbRecalculerPrix.setText("Recalculer les prix sur le nouveau document");
            rbRecalculerPrix.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            rbRecalculerPrix.setMinimumSize(new Dimension(350, 30));
            rbRecalculerPrix.setMaximumSize(new Dimension(350, 30));
            rbRecalculerPrix.setPreferredSize(new Dimension(350, 30));
            rbRecalculerPrix.setName("rbRecalculerPrix");
            rbRecalculerPrix.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                rbRecalculerPrixActionPerformed(e);
              }
            });
            pnlRecalculPrix.add(rbRecalculerPrix, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlOptions.add(pnlRecalculPrix);
        }
        pnlContenu.add(pnlOptions, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbListeArticles ----
        lbListeArticles.setText("Liste des articles");
        lbListeArticles.setFont(new Font("sansserif", Font.BOLD, 14));
        lbListeArticles.setName("lbListeArticles");
        pnlContenu.add(lbListeArticles, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListeArticles ========
        {
          scpListeArticles.setName("scpListeArticles");
          
          // ---- tblListeArticles ----
          tblListeArticles.setShowVerticalLines(true);
          tblListeArticles.setShowHorizontalLines(true);
          tblListeArticles.setBackground(Color.white);
          tblListeArticles.setRowHeight(20);
          tblListeArticles.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListeArticles.setSelectionBackground(new Color(57, 105, 138));
          tblListeArticles.setGridColor(new Color(204, 204, 204));
          tblListeArticles.setName("tblListeArticles");
          tblListeArticles.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeArticleMouseClicked(e);
            }
          });
          scpListeArticles.setViewportView(tblListeArticles);
        }
        pnlContenu.add(scpListeArticles, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlFond.add(pnlContenu, BorderLayout.CENTER);
    }
    contentPane.add(pnlFond, BorderLayout.CENTER);
    
    // ---- buttonGroup2 ----
    buttonGroup2.add(rbAvecReliquat);
    buttonGroup2.add(rbSansReliquat);
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbConserverPrix);
    buttonGroup1.add(rbRecalculerPrix);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlFond;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlOptions;
  private SNPanelTitre pnlTypeExtraction;
  private SNRadioButton rbAvecReliquat;
  private SNRadioButton rbSansReliquat;
  private SNPanelTitre pnlRecalculPrix;
  private SNRadioButton rbConserverPrix;
  private SNRadioButton rbRecalculerPrix;
  private SNLabelTitre lbListeArticles;
  private JScrollPane scpListeArticles;
  private NRiTable tblListeArticles;
  private ButtonGroup buttonGroup2;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
