/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailcheque;

import ri.serien.libcommun.gescom.vente.reglement.BanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.IdBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.Reglement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleDetailCheque extends AbstractModeleDialogue {
  // Variables
  private Reglement reglement = null;
  private String numeroCheque = null;
  private IdBanqueClient idBanqueClient = null;
  
  /**
   * Constructeur.
   */
  public ModeleDetailCheque(SessionBase pSession, Reglement pReglement) {
    super(pSession);
    reglement = pReglement;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    if (reglement != null) {
      numeroCheque = reglement.getReferenceTiree();
      if (reglement.getBanqueClient() != null) {
        idBanqueClient = reglement.getBanqueClient().getId();
      }
    }
  }
  
  @Override
  public void chargerDonnees() {
    
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier le numéro du chèque.
   */
  public void modifierNumeroCheque(String pValeurSaisie) {
    pValeurSaisie = Constantes.normerTexte(pValeurSaisie);
    if (Constantes.equals(pValeurSaisie, numeroCheque)) {
      return;
    }
    
    numeroCheque = pValeurSaisie;
    rafraichir();
  }
  
  /**
   * Modifier la banque du client.
   */
  public void modifierBanqueClient(IdBanqueClient pIdBanqueClient) {
    if (Constantes.equals(idBanqueClient, pIdBanqueClient)) {
      return;
    }
    
    idBanqueClient = pIdBanqueClient;
    rafraichir();
  }
  
  /**
   * Validation de la boite de dialogue.
   */
  @Override
  public void quitterAvecValidation() {
    // Le numéro du chèque
    if (numeroCheque != null && !numeroCheque.isEmpty() && numeroCheque.length() != Reglement.LONGUEUR_NUMERO_CHEQUE) {
      throw new MessageErreurException("Le numéro du chèque doit comporter " + Reglement.LONGUEUR_NUMERO_CHEQUE + " chiffres.");
    }
    reglement.setReferenceTiree(numeroCheque);
    
    // Le nom de la banque
    if (idBanqueClient != null) {
      reglement.setBanqueClient(new BanqueClient(idBanqueClient));
    }
    
    // Contrôle de cohérence : si un des deux champs est saisi alors les deux doivent être renseignés
    if ((Constantes.normerTexte(reglement.getReferenceTiree()).isEmpty() && reglement.getBanqueClient() != null)) {
      throw new MessageErreurException("Le numéro du chèque doit être renseigné puisque le nom de la banque est renseigné.");
    }
    else if (!Constantes.normerTexte(reglement.getReferenceTiree()).isEmpty() && reglement.getBanqueClient() == null) {
      throw new MessageErreurException("Le nom de la banque doit être renseigné puisque le numéro de chèque est renseigné.");
    }
    
    super.quitterAvecValidation();
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public Reglement getReglement() {
    return reglement;
  }
  
  public String getNumeroCheque() {
    return numeroCheque;
  }
  
  public IdBanqueClient getIdBanqueClient() {
    return idBanqueClient;
  }
  
}
