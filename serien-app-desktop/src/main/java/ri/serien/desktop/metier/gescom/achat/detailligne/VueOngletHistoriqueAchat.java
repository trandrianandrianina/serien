/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.beans.PropertyChangeEvent;

import javax.swing.JLabel;
import javax.swing.JPanel;

import ri.serien.desktop.metier.gescom.achat.historiquereceptionsachat.ModeleHistoriqueReceptions;
import ri.serien.desktop.metier.gescom.achat.historiquereceptionsachat.VueHistoriqueReceptions;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletHistoriqueAchat extends JPanel implements InterfaceVueOnglet {
  // Constantes
  public static final int SOUS_ONGLET_ETAT_STOCKS = 0;
  public static final int SOUS_ONGLET_MOUVEMENTS = 1;
  public static final int SOUS_ONGLET_ATTENDUS = 2;
  private static final int NOMBRE_DECIMALE_MONTANT = 3;
  
  // Variables
  private ModeleDetailLigneAchat modele = null;
  private ListeStockComptoir listeStockParMagasin = null;
  private ListeStockComptoir listeStockEtablissement = null;
  private boolean ecranInitialise = false;
  private int indexLigneListeMouvements = -1;
  private int indexLigneListeAttenduCommande = -1;
  private int indexLigneListeEtatStocks = -1;
  private boolean executerEvenements = false;
  private ModeleHistoriqueReceptions modeleHistorique = null;
  private VueHistoriqueReceptions panelHistorique = null;
  
  /**
   * Constructeur.
   */
  public VueOngletHistoriqueAchat(ModeleDetailLigneAchat pModele) {
    modele = pModele;
    modeleHistorique = new ModeleHistoriqueReceptions(pModele.getSessionPanel(), pModele.getArticle());
    panelHistorique = new VueHistoriqueReceptions(modeleHistorique);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    executerEvenements = false;
    
    ecranInitialise = true;
    
    rafraichirPanelHistorique();
    
    this.getComponent(0).requestFocus();
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  private void rafraichirPanelHistorique() {
    if (panelHistorique != null && modeleHistorique.getListeReceptions() != null && !modeleHistorique.getListeReceptions().isEmpty()) {
      this.add(panelHistorique, BorderLayout.CENTER);
    }
    else {
      this.add(new JLabel("<html><center>Pas de réception d'achats pour cet article</html></center>", JLabel.CENTER),
          BorderLayout.CENTER);
    }
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tbpStocksPropertyChange(PropertyChangeEvent e) {
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
