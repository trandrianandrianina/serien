/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.consultationdocumentachat;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererLigneAchat extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color blanc = new Color(255, 255, 255);
  private static Color vert = new Color(152, 206, 168);
  private static Color orange = new Color(211, 187, 114);
  private static Color rouge = new Color(218, 166, 161);
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Taille des colonnes
    redimensionnerColonnes(table);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setCellRenderer(leftRenderer);
    cm.getColumn(1).setCellRenderer(rightRenderer);
    cm.getColumn(2).setCellRenderer(centerRenderer);
    cm.getColumn(3).setCellRenderer(leftRenderer);
    cm.getColumn(4).setCellRenderer(rightRenderer);
    cm.getColumn(5).setCellRenderer(centerRenderer);
    cm.getColumn(6).setCellRenderer(rightRenderer);
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    int tailleNumero = 100;
    int tailleQuantite = 120;
    int tailleLibelle = 200;
    int tailleUnite = 50;
    int tailleMontant = 120;
    
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(tailleNumero);
    cm.getColumn(0).setMaxWidth(tailleNumero);
    cm.getColumn(1).setMinWidth(tailleQuantite);
    cm.getColumn(1).setMaxWidth(tailleQuantite);
    cm.getColumn(2).setMaxWidth(tailleUnite);
    cm.getColumn(2).setMinWidth(tailleUnite);
    cm.getColumn(3).setMinWidth(tailleLibelle);
    cm.getColumn(4).setMinWidth(tailleQuantite);
    cm.getColumn(4).setMaxWidth(tailleQuantite);
    cm.getColumn(5).setMinWidth(tailleUnite);
    cm.getColumn(5).setMaxWidth(tailleUnite);
    cm.getColumn(6).setMinWidth(tailleMontant);
    cm.getColumn(6).setMaxWidth(tailleMontant);
  }
}
