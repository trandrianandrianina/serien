/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.commentaires;

import ri.serien.libcommun.commun.EnumContexteMetier;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Boîte de dialogue permettant de saisir des commentaires.
 * 
 * Le panneau principal est utilisé comme premier panneau des écrans et boîtes de dialogue. Il contient l'ensemble des composants
 * à l'exception de la barre de boutons (SNBarreBouton). En tant que composant parent, il est opaque et colorie la zone avec la couleur
 * de fond standard. Il comporte également une marge pour séparer les composants des bords de l'écran ou de laboîte de dialogue.
 */
public final class ModeleDetailCommentaire extends AbstractModeleDialogue {
  public static int MODE_CONSULTER_LIGNE_COMMENTAIRE = 0;
  public static int MODE_CREER_COMMENTAIRE_LIBRE = 1;
  public static int MODE_AJOUTER_ARTICLE_COMMENTAIRE = 2;
  public static int MODE_MODIFIER_LIGNE_COMMENTAIRE = 3;
  
  // Variables
  private int mode = 0;
  private IdEtablissement idEtablissement = null;
  
  // Variables de travail
  private String texteCommentaire = "";
  private String codeCommentaire = "";
  private boolean isConserverCommentaire = false;
  private boolean isCopierSurDocumentAchats = true;
  private boolean isArticleExistant = false;
  private boolean isEditionTousDocument = true;
  private boolean isChargerOptionEdition = false;
  private boolean isDirectUsine = false;
  
  private EnumContexteMetier contexte = EnumContexteMetier.CONTEXTE_VENTE;
  
  /**
   * Constructeur privé pour obliger l'utilisation des méthodes factory.
   */
  private ModeleDetailCommentaire(SessionBase pSession) {
    super(pSession);
  }
  
  /**
   * Saisir un nouveau commentaire pour ajout à un document.
   * Ce commentaire pourra éventuellement être sauvegardé sous forme d'article commentaire pour une utilisation ultérieure. Dans le
   * cas contraire, il s'agit d'un commentaire libre spécifique au document.
   */
  static public ModeleDetailCommentaire getInstanceCreationCommentaireLibre(SessionBase pSession, IdEtablissement pIdEtablissement,
      EnumContexteMetier pContexte) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_CREER_COMMENTAIRE_LIBRE;
    instance.idEtablissement = pIdEtablissement;
    instance.contexte = pContexte;
    instance.isChargerOptionEdition = true;
    return instance;
  }
  
  /**
   * Modifier un article commentaire pour ajout à un document.
   * Le commentaire peut être est modifié avant l'ajout au document. Il est possible de conserver les modifications dans l'article
   * commentaire et de supprimer l'article commentaire.
   */
  static public ModeleDetailCommentaire getInstanceAjouterArticleCommentaire(SessionBase pSession, Article pArticle,
      EnumContexteMetier pContexte) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_AJOUTER_ARTICLE_COMMENTAIRE;
    instance.idEtablissement = pArticle.getId().getIdEtablissement();
    instance.texteCommentaire = pArticle.getLibelleComplet();
    instance.isConserverCommentaire = false;
    instance.isEditionTousDocument = false;
    instance.codeCommentaire = pArticle.getId().getCodeArticle().toUpperCase();
    instance.contexte = pContexte;
    return instance;
  }
  
  /**
   * Modifier la ligne de commentaire d'un document.
   */
  static public ModeleDetailCommentaire getInstanceModifierLigneDocument(SessionBase pSession, LigneVente pLigneVente) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_MODIFIER_LIGNE_COMMENTAIRE;
    instance.idEtablissement = pLigneVente.getId().getIdEtablissement();
    instance.texteCommentaire = pLigneVente.getLibelle();
    instance.isConserverCommentaire = false;
    instance.isCopierSurDocumentAchats = pLigneVente.getGenerationBonAchat() > 0;
    if (pLigneVente.getIdArticle() != null) {
      instance.codeCommentaire = pLigneVente.getIdArticle().getCodeArticle();
    }
    instance.contexte = EnumContexteMetier.CONTEXTE_VENTE;
    return instance;
  }
  
  /**
   * Modifier la ligne de commentaire d'un document.
   */
  static public ModeleDetailCommentaire getInstanceModifierLigneDocument(SessionBase pSession, LigneAchatCommentaire pLigneAchat) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_MODIFIER_LIGNE_COMMENTAIRE;
    instance.idEtablissement = pLigneAchat.getId().getIdEtablissement();
    instance.texteCommentaire = pLigneAchat.getTexte();
    instance.isConserverCommentaire = false;
    instance.codeCommentaire = null;
    instance.contexte = EnumContexteMetier.CONTEXTE_ACHAT;
    
    return instance;
  }
  
  /**
   * Consulter la ligne de commentaire d'un document.
   */
  static public ModeleDetailCommentaire getInstanceConsulterLigneDocument(SessionBase pSession, LigneVente pLigneVente) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_CONSULTER_LIGNE_COMMENTAIRE;
    instance.idEtablissement = pLigneVente.getId().getIdEtablissement();
    instance.texteCommentaire = pLigneVente.getLibelle();
    instance.isConserverCommentaire = false;
    instance.isCopierSurDocumentAchats = false;
    if (pLigneVente.getIdArticle() != null) {
      instance.codeCommentaire = pLigneVente.getIdArticle().getCodeArticle();
    }
    instance.contexte = EnumContexteMetier.CONTEXTE_VENTE;
    return instance;
  }
  
  /**
   * Consulter la ligne de commentaire d'un document.
   */
  static public ModeleDetailCommentaire getInstanceConsulterLigneDocument(SessionBase pSession, LigneAchatCommentaire pLigneAchat) {
    ModeleDetailCommentaire instance = new ModeleDetailCommentaire(pSession);
    instance.mode = MODE_CONSULTER_LIGNE_COMMENTAIRE;
    instance.idEtablissement = pLigneAchat.getId().getIdEtablissement();
    instance.texteCommentaire = pLigneAchat.getTexte();
    instance.isConserverCommentaire = false;
    instance.codeCommentaire = null;
    instance.contexte = EnumContexteMetier.CONTEXTE_ACHAT;
    
    return instance;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    if (idEtablissement != null && codeCommentaire != null && !codeCommentaire.trim().isEmpty()) {
      chargerOptionEdition(new Article(IdArticle.getInstance(idEtablissement, codeCommentaire)));
    }
  }
  
  @Override
  public void chargerDonnees() {
    // Rien à charger depuis la base de données
  }
  
  @Override
  public void quitterAvecValidation() {
    if (texteCommentaire.isEmpty()) {
      throw new MessageErreurException("Impossible d'ajouter un commentaire vide.");
    }
    
    // Sauver le commentaire si c'est demandé
    if (isConserverCommentaire) {
      Article articleCommentaire = creerArticleCommentaire();
      enregistrerCommentaire(articleCommentaire);
    }
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecSuppression() {
    // Créer un article commentaire
    isConserverCommentaire = true;
    Article articleCommentaire = creerArticleCommentaire();
    
    // Contrôler l'identifiant
    if (articleCommentaire.getId() == null) {
      throw new MessageErreurException("Impossible de supprimer un commentaire sans code valide.");
    }
    
    // Supprimer l'article commentaire
    ManagerServiceArticle.supprimerArticleCommentaire(getIdSession(), articleCommentaire);
    super.quitterAvecSuppression();
  }
  
  // Méthodes privées
  
  /**
   * Sauver le commentaire en base de données.
   * Si le commentaire n'a pas de code, une erreur est générée.
   */
  private void enregistrerCommentaire(Article pArticle) {
    if (pArticle.getId() == null) {
      throw new MessageErreurException("Impossible de sauvegarder un commentaire sans code.");
    }
    if (pArticle.getLibelleComplet().isEmpty()) {
      throw new MessageErreurException("Impossible de sauvegarder un commentaire vide.");
    }
    
    // Sauver le commentaire
    ManagerServiceArticle.sauverArticleCommentaire(getIdSession(), pArticle);
  }
  
  /**
   * Modifier le texte du commentaire.
   */
  public void modifierTexteCommentaire(String saisie) {
    if (texteCommentaire.equals(saisie)) {
      return;
    }
    texteCommentaire = saisie;
    rafraichir();
  }
  
  /**
   * Modifier l'état de sauvegarde du commentaire.
   */
  public void modifierConserverCommentaire(boolean saisie) {
    if (isConserverCommentaire == saisie) {
      return;
    }
    isConserverCommentaire = saisie;
    rafraichir();
  }
  
  /**
   * Modifier l'indication de report de ce commentaire sur les documents d'achats issus de ce document de ventes
   */
  public void modifierCopierSurDocumentAchat(boolean pSaisie) {
    if (isCopierSurDocumentAchats == pSaisie) {
      return;
    }
    isCopierSurDocumentAchats = pSaisie;
    rafraichir();
  }
  
  /**
   * Modifier le code du commentaire
   */
  public void modifierCodeCommentaire(String saisie) {
    if (codeCommentaire.equals(saisie)) {
      return;
    }
    
    codeCommentaire = IdArticle.controlerCodeArticle(saisie.toUpperCase());
    rafraichir();
  }
  
  /**
   * Modifier l'indicateur d'édition sur tous les documents.
   */
  public void modifierEditionTousDocuments(boolean pIsEditionPartout) {
    isEditionTousDocument = pIsEditionPartout;
    rafraichir();
  }
  
  /**
   * Créer un article commentaire à partir des informations saisie dans la boîte de dialogue.
   */
  public Article creerArticleCommentaire() {
    Article articleCommentaire = null;
    if (!isConserverCommentaire || codeCommentaire.isEmpty()) {
      articleCommentaire = new Article(IdArticle.getInstanceAvecCreationId(idEtablissement));
    }
    else {
      articleCommentaire = new Article(IdArticle.getInstance(idEtablissement, codeCommentaire));
    }
    articleCommentaire.setLibelleComplet(getTexteCommentaire());
    articleCommentaire.setTopEditionBonPreparation(isEditionTousDocument());
    articleCommentaire.setTopAccuseReceptionCommande(isEditionTousDocument());
    articleCommentaire.setTopBonExpedition(isEditionTousDocument());
    articleCommentaire.setTopEditionBonTransporteur(isEditionTousDocument());
    articleCommentaire.setTopDevis(isEditionTousDocument());
    articleCommentaire.setTopFacture(isEditionTousDocument());
    
    return articleCommentaire;
  }
  
  private void chargerOptionEdition(Article pArticle) {
    Article articleCommentaire = ManagerServiceArticle.chargerArticleCommentaire(getIdSession(), pArticle);
    isEditionTousDocument = articleCommentaire.isTopFacture();
    isChargerOptionEdition = true;
  }
  
  // -- Accesseurs
  
  /**
   * Mode d'exécution de la boîte de dialogue.
   */
  public int getMode() {
    return mode;
  }
  
  /**
   * Texte du commentaire.
   */
  public String getTexteCommentaire() {
    return texteCommentaire;
  }
  
  /**
   * Indique si le commentaire doit être conservé sous forme d'article commentaire pour une utilisation future.
   */
  public boolean isConserverCommentaire() {
    return isConserverCommentaire;
  }
  
  /**
   * Indique si le commentaire doit être édité sur les documents d'achats issus de ce document de ventes.
   */
  public boolean isCopierSurDocumentAchats() {
    return isCopierSurDocumentAchats;
  }
  
  /**
   * Code du commentaire.
   */
  public String getCodeCommentaire() {
    return codeCommentaire;
  }
  
  /**
   * Indique si le commentaire doit être édité sur tous les documents.
   */
  public boolean isEditionTousDocument() {
    return isEditionTousDocument;
  }
  
  /**
   * Indiquer si le commentaire est sauvegardable.
   */
  public boolean isCommentaireSauvegardable() {
    if (mode == MODE_MODIFIER_LIGNE_COMMENTAIRE) {
      return false;
    }
    return true;
  }
  
  /**
   * Indiquer si le commentaire est supprimable.
   * C'est le cas uniquement si on est en train de modifier un commentaire issu d'un article commentaire.
   */
  public boolean isCommentaireSupprimable() {
    if (mode != MODE_AJOUTER_ARTICLE_COMMENTAIRE || codeCommentaire == null || codeCommentaire.isEmpty()) {
      return false;
    }
    return true;
  }
  
  public boolean isOptionsEditionVisible() {
    return contexte.equals(EnumContexteMetier.CONTEXTE_VENTE) && isChargerOptionEdition;
  }
  
  /**
   * Indiquer le contexte dans lequel on utilise les commentaires (ventes ou achats)
   */
  public EnumContexteMetier getContexte() {
    return contexte;
  }
  
  /**
   * Le document en cours est un direct usine
   */
  public boolean isDirectUsine() {
    return isDirectUsine;
  }
  
  /**
   * Le document en cours est un direct usine
   */
  public void setDirectUsine(boolean isDirectUsine) {
    this.isDirectUsine = isDirectUsine;
  }
  
}
