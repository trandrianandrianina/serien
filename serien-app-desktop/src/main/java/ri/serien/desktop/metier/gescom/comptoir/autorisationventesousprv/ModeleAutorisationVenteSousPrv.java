/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.autorisationventesousprv;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.exploitation.personnalisation.civilite.Civilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeImageClient;
import ri.serien.libcommun.gescom.vente.document.DemandeDeblocage;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleAutorisationVenteSousPrv extends AbstractModeleDialogue {
  // Variables
  private DocumentVente documentVente = null;
  private LigneVente ligneVente = null;
  private Client client = null;
  private ModeleComptoir modeleParent = null;
  private ListeCivilite listeCivilite = null;
  private UtilisateurGescom utilisateurGescom = null;
  private DemandeDeblocage demandeDeblocage = null;
  
  /**
   * Constructeur
   */
  public ModeleAutorisationVenteSousPrv(SessionBase pSession, ModeleComptoir pParentmodel, LigneVente pLigneVente,
      UtilisateurGescom pUtilisateur) {
    super(pSession);
    modeleParent = pParentmodel;
    documentVente = modeleParent.getDocumentVenteEnCours();
    ligneVente = pLigneVente;
    client = modeleParent.getClientCourant();
    utilisateurGescom = pUtilisateur;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    effacerVariables();
    
  }
  
  @Override
  public void quitterAvecValidation() {
    if (!isAutorisationAcceptee()) {
      return;
    }
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    if (demandeDeblocage != null) {
      ManagerServiceDocumentVente.supprimerDemandeDeblocageVenteSousPrv(getIdSession(), documentVente.getId());
    }
    super.quitterAvecAnnulation();
  }
  
  // Méthodes publiques
  
  /**
   * Modifier la civilité du client.
   */
  public void modifierCiviliteClient(Civilite pCivilite) {
    IdCivilite idCivilite = null;
    if (pCivilite != null) {
      idCivilite = pCivilite.getId();
    }
    if (Constantes.equals(idCivilite, client.getContactPrincipal().getIdCivilite())) {
      return;
    }
    
    client.getAdresse().setIdCivilite(idCivilite);
    client.getContactPrincipal().setIdCivilite(idCivilite);
    rafraichir();
  }
  
  /**
   * Modifier le nom du client.
   */
  public void modifierNomClient(String nomClient) {
    nomClient = Constantes.normerTexte(nomClient);
    if (nomClient.equals(client.getAdresse().getNom())) {
      return;
    }
    
    client.getContactPrincipal().setNom(nomClient);
    client.setTypeCompteClient(EnumTypeCompteClient.COMPTANT);
    client.setTypeImageClient(EnumTypeImageClient.PARTICULIER);
    client.getAdresse().setNom(nomClient);
    rafraichir();
  }
  
  /**
   * Modifier le complément du nom du client.
   */
  public void modifierComplementNomClient(String complementNomClient) {
    complementNomClient = Constantes.normerTexte(complementNomClient);
    if (complementNomClient.equals(client.getAdresse().getComplementNom())) {
      return;
    }
    client.getContactPrincipal().setPrenom(complementNomClient);
    client.getAdresse().setComplementNom(complementNomClient);
    
    rafraichir();
  }
  
  /**
   * Modifier la rue du client.
   */
  public void modifierRueClient(String rueClient) {
    rueClient = Constantes.normerTexte(rueClient);
    if (rueClient.equals(client.getAdresse().getRue())) {
      return;
    }
    client.getAdresse().setRue(rueClient);
    
    rafraichir();
  }
  
  /**
   * Modifier la localisation du client.
   */
  public void modifierLocalisationClient(String localisationClient) {
    localisationClient = Constantes.normerTexte(localisationClient);
    if (localisationClient.equals(client.getAdresse().getLocalisation())) {
      return;
    }
    client.getAdresse().setLocalisation(localisationClient);
    
    rafraichir();
  }
  
  /**
   * Modifier le code postal du client.
   */
  public void modifierCodePostalClient(String codePostalClient) {
    int codePostal = Constantes.convertirTexteEnInteger(codePostalClient);
    if (codePostal == client.getAdresse().getCodePostal()) {
      return;
    }
    client.getAdresse().setCodePostal(codePostal);
    
    rafraichir();
  }
  
  /**
   * Modifier la ville du client.
   */
  public void modifierVilleClient(String villeClient) {
    villeClient = Constantes.normerTexte(villeClient);
    if (villeClient.equals(client.getAdresse().getVille())) {
      return;
    }
    client.getAdresse().setVille(villeClient);
    
    rafraichir();
  }
  
  /**
   * Modifier le mail du client.
   */
  public void modifierMailClient(String mailClient) {
    mailClient = Constantes.normerTexte(mailClient);
    if (mailClient.equals(client.getNumeroTelephone())) {
      return;
    }
    client.getContactPrincipal().setEmail(mailClient);
    
    rafraichir();
  }
  
  /**
   * Modifier le numéro de téléphone du client.
   */
  public void modifierTelephoneClient(String telephoneClient) {
    telephoneClient = Constantes.normerTexte(telephoneClient);
    if (telephoneClient.equals(client.getNumeroTelephone())) {
      return;
    }
    client.getContactPrincipal().setNumeroTelephone1(telephoneClient);
    client.setNumeroTelephone(telephoneClient);
    
    rafraichir();
  }
  
  /**
   * Modifier le numéro de fax du client.
   */
  public void modifierFaxClient(String faxClient) {
    faxClient = Constantes.normerTexte(faxClient);
    if (faxClient.equals(client.getNumeroFax())) {
      return;
    }
    client.getContactPrincipal().setNumeroFax(faxClient);
    client.setNumeroFax(faxClient);
    
    rafraichir();
  }
  
  /**
   * Demande l'autorisation du règlement différé.
   */
  public void demanderAutorisation() {
    if (client == null) {
      throw new MessageErreurException("Impossible de demander l'autorisation pour un règlement de différé car le client est invalide.");
    }
    if (!client.isExistant()) {
      throw new MessageErreurException(
          "Impossible de demander l'autorisation pour un règlement de différé car le client n'existe pas dans la base.");
    }
    
    // Vérifier les informations obligatoires du client
    client.controlerAdresse(getIdSession(), modeleParent.getListeErreurAdresse(), true);
    if (!modeleParent.traiterErreurAdresse(false, false)) {
      quitterAvecAnnulation();
      throw new MessageErreurException(
          "Impossible de demander une autorisation de réglement différé car l'adresse du client n'est pas correcte.");
    }
    
    // Sauver le client
    ManagerServiceClient.sauverClient(getIdSession(), client);
    
    // On écrit ou on met à jour une demande de déblocage (s'il elle existe déjà l'autorisation sera effacée)
    ManagerServiceDocumentVente.sauverDemandeDeblocageVenteSousPrv(getIdSession(), documentVente.getId(), utilisateurGescom.getProfil());
    demandeDeblocage = ManagerServiceDocumentVente.chargerDemandeDeblocageVenteSousPrv(getIdSession(), documentVente.getId());
    
    rafraichir();
  }
  
  /**
   * Controle si la demande de déblocage a été acceptée.
   */
  public void controlerDeblocage() {
    demandeDeblocage = ManagerServiceDocumentVente.chargerDemandeDeblocageVenteSousPrv(getIdSession(), documentVente.getId());
    rafraichir();
  }
  
  /**
   * Retourne s'il existe une demande de déblocage.
   */
  public boolean isDemandeDeblocageExiste() {
    if (demandeDeblocage == null) {
      return false;
    }
    return true;
  }
  
  /**
   * Retourne si la demande de déblocage a été acceptée.
   */
  public boolean isAutorisationAcceptee() {
    if (demandeDeblocage == null) {
      return false;
    }
    return demandeDeblocage.isDemandeAcceptee();
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les variables du modèle.
   */
  private void effacerVariables() {
    demandeDeblocage = null;
    modeleParent.setVenteSousPRV(false);
  }
  
  // -- Accesseurs
  
  public ModeleComptoir getParentModel() {
    return modeleParent;
  }
  
  public ListeCivilite getListeCivilites() {
    return listeCivilite;
  }
  
  public Client getClient() {
    return client;
  }
  
  public LigneVente getLigneVente() {
    return ligneVente;
  }
  
  public DocumentVente getDocumentVente() {
    return documentVente;
  }
  
}
