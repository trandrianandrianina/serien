/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;

public class JTableCellRendererBonCour extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  private static Color blanc = new Color(255, 255, 255);
  private static Color rouge = new Color(218, 166, 161);
  // Label des cellules
  private JLabel lbNumeroBon = null;
  private JLabel lbNumeroCarnet = null;
  private JLabel lbMagasinier = null;
  private JLabel lbDocument = null;
  private JLabel lbDate = null;
  private JLabel lbClient = null;
  private JLabel lbEtat = null;
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    try {
      // Taille et justification des colonnes
      redimensionnerColonnes(table);
      
      lbNumeroBon = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 0);
      lbNumeroCarnet = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 1);
      lbMagasinier = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 2);
      lbDocument = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 3);
      lbDate = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 4);
      lbClient = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 5);
      lbEtat = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, 6);
      
      // Colonne numéro bon de cour
      if (column == 0) {
        lbNumeroBon.setHorizontalAlignment(JLabel.RIGHT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbNumeroBon.setForeground(Color.RED);
        }
        else {
          lbNumeroBon.setForeground(Color.BLACK);
        }
      }
      
      // colonne carnet
      if (column == 1) {
        lbNumeroCarnet.setHorizontalAlignment(JLabel.RIGHT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbNumeroCarnet.setForeground(Color.RED);
        }
        else {
          lbNumeroCarnet.setForeground(Color.BLACK);
        }
      }
      
      // colonne magasinier
      if (column == 2) {
        lbMagasinier.setHorizontalAlignment(JLabel.LEFT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbMagasinier.setForeground(Color.RED);
        }
        else {
          lbMagasinier.setForeground(Color.BLACK);
        }
      }
      
      // colonne document
      if (column == 3) {
        lbDocument.setHorizontalAlignment(JLabel.RIGHT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbDocument.setForeground(Color.RED);
        }
        else {
          lbDocument.setForeground(Color.BLACK);
        }
      }
      
      // colonne date document
      if (column == 4) {
        lbDate.setHorizontalAlignment(JLabel.CENTER);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbDate.setForeground(Color.RED);
        }
        else {
          lbDate.setForeground(Color.BLACK);
        }
      }
      
      // colonne client
      if (column == 5) {
        lbClient.setHorizontalAlignment(JLabel.LEFT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbClient.setForeground(Color.RED);
        }
        else {
          lbClient.setForeground(Color.BLACK);
        }
      }
      
      // colonne état
      if (column == 6) {
        lbEtat.setHorizontalAlignment(JLabel.LEFT);
        if (table.getValueAt(row, 6) != null && table.getValueAt(row, 6).equals(EnumEtatBonCour.MANQUANT.getLibelle())) {
          lbEtat.setForeground(Color.RED);
        }
        else {
          lbEtat.setForeground(Color.BLACK);
        }
      }
    }
    catch (Exception e) {
      DialogueErreur.afficher(e);
    }
    return component;
  }
  
  /**
   * Redimensionne et justifie les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(100);
    cm.getColumn(0).setMaxWidth(100);
    cm.getColumn(1).setMaxWidth(100);
    cm.getColumn(1).setMinWidth(100);
    cm.getColumn(2).setMinWidth(250);
    cm.getColumn(2).setMaxWidth(250);
    cm.getColumn(3).setMinWidth(100);
    cm.getColumn(3).setMaxWidth(100);
    cm.getColumn(4).setMinWidth(90);
    cm.getColumn(4).setMaxWidth(90);
    cm.getColumn(5).setMinWidth(180);
    cm.getColumn(6).setMinWidth(100);
    cm.getColumn(6).setMaxWidth(100);
  }
}
