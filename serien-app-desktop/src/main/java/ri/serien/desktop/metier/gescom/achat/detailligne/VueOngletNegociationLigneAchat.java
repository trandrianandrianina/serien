/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.commun.message.EnumImportanceMessage;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumModeSaisiePort;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet négociation de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletNegociationLigneAchat extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final int NOMBRE_DECIMALE_REMISE = 2;
  private static final int NOMBRE_DECIMALE_COEFFICIENT = 4;
  
  // Variables
  private ModeleDetailLigneAchat modele = null;
  private boolean executerEvenements = false;
  
  /**
   * Constructeur.
   */
  public VueOngletNegociationLigneAchat(ModeleDetailLigneAchat acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  /**
   * Construire l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    pnlTarifAchat.setTitre("Détail de négociation d'achat");
    // Configurer la barre de boutons principale
    
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Positionnement du focus
    tfQuantiteUCA.requestFocusInWindow();
  }
  
  /**
   * Rafraichir l'écran;
   */
  @Override
  public void rafraichir() {
    executerEvenements = false;
    
    rafraichirQuantiteUCA();
    rafraichirQuantiteUA();
    
    rafraichirDateApplicationAchat();
    rafraichirPrixAchatBrutHT();
    rafraichirPourcentageRemise1();
    rafraichirPourcentageRemise2();
    rafraichirPourcentageRemise3();
    rafraichirPourcentageRemise4();
    rafraichirPourcentageTaxeOuMajoration();
    rafraichirMontantConditionnement();
    rafraichirPrixAchatNetHT();
    rafraichirModeSaisiePort();
    rafraichirMontantAuPoidsPortHT();
    rafraichirPoidsPort();
    rafraichirPourcentagePort();
    rafraichirPrixRevientFournisseurHT();
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichir la date d'application du tarif.
   */
  private void rafraichirDateApplicationAchat() {
    snDateApplicationTarifAchat.setDate(modele.getDateApplicationAchat());
  }
  
  /**
   * Rafraichir la quantité UCA.
   */
  private void rafraichirQuantiteUCA() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      if (prixAchat.getQuantiteReliquatUCA() != null) {
        tfQuantiteUCA.setText(Constantes.formater(prixAchat.getQuantiteReliquatUCA(), false));
      }
      else {
        tfQuantiteUCA.setText("");
      }
      
      if (prixAchat.getIdUCA() != null) {
        unQuantiteUCA.setText(prixAchat.getIdUCA().getCode());
      }
      else {
        unQuantiteUCA.setText("");
      }
    }
    else {
      tfQuantiteUCA.setText("");
      unQuantiteUCA.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfQuantiteUCA.setEnabled(true);
    }
    else {
      tfQuantiteUCA.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir la quantité UA.
   */
  private void rafraichirQuantiteUA() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      String libelle = prixAchat.getLibelleNombreUAParUCA();
      if (!libelle.isEmpty()) {
        lbQuantiteUA.setText(libelle + " soit en UA");
        tfQuantiteUA.setText(Constantes.formater(prixAchat.getQuantiteReliquatUA(), false));
        unQuantiteUA.setText(prixAchat.getIdUA().getCode());
        tfQuantiteUA.setVisible(true);
        unQuantiteUA.setVisible(true);
        
        if (modele.isDocumentModifiable()) {
          tfQuantiteUA.setEnabled(true);
        }
        else {
          tfQuantiteUA.setEnabled(false);
        }
      }
      else {
        lbQuantiteUA.setText("");
        tfQuantiteUA.setText("");
        unQuantiteUA.setText("");
        lbQuantiteUA.setVisible(false);
        tfQuantiteUA.setVisible(false);
        unQuantiteUA.setVisible(false);
      }
    }
    else {
      lbQuantiteUA.setText("");
      tfQuantiteUA.setText("");
      unQuantiteUA.setText("");
      lbQuantiteUA.setVisible(false);
      tfQuantiteUA.setVisible(false);
      unQuantiteUA.setVisible(false);
    }
  }
  
  /**
   * Rafraichir le prix d'achat brut HT.
   */
  private void rafraichirPrixAchatBrutHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPrixAchatBrutHT.setText(Constantes.formater(prixAchat.getPrixAchatBrutHT(), true));
      
      if (prixAchat.getIdUA() != null) {
        unPrixAchatBrutHT.setText("par " + prixAchat.getIdUA().getCode());
      }
      else {
        unPrixAchatBrutHT.setText("");
      }
    }
    else {
      tfPrixAchatBrutHT.setText("");
      unPrixAchatBrutHT.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfPrixAchatBrutHT.setEnabled(true);
    }
    else {
      tfPrixAchatBrutHT.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le pourcentage de remise 1.
   */
  private void rafraichirPourcentageRemise1() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPourcentageRemise1.setText(Constantes.formater(prixAchat.getPourcentageRemise1(), true));
    }
    else {
      tfPourcentageRemise1.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfPourcentageRemise1.setEnabled(true);
    }
    else {
      tfPourcentageRemise1.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le poucentage de remise 2.
   */
  private void rafraichirPourcentageRemise2() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPourcentageRemise2.setText(Constantes.formater(prixAchat.getPourcentageRemise2(), true));
    }
    else {
      tfPourcentageRemise2.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfPourcentageRemise2.setEnabled(true);
    }
    else {
      tfPourcentageRemise2.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le pourcentage de remise 3.
   */
  private void rafraichirPourcentageRemise3() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPourcentageRemise3.setText(Constantes.formater(prixAchat.getPourcentageRemise3(), true));
    }
    else {
      tfPourcentageRemise3.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfPourcentageRemise3.setEnabled(true);
    }
    else {
      tfPourcentageRemise3.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le pourcentage de remise 4.
   */
  private void rafraichirPourcentageRemise4() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPourcentageRemise4.setText(Constantes.formater(prixAchat.getPourcentageRemise4(), true));
    }
    else {
      tfPourcentageRemise4.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfPourcentageRemise4.setEnabled(true);
    }
    else {
      tfPourcentageRemise4.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le pourcentage de taxe ou de majoration.
   */
  private void rafraichirPourcentageTaxeOuMajoration() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfTaxeOuMajoration.setText(Constantes.formater(prixAchat.getPourcentageTaxeOuMajoration(), true));
    }
    else {
      tfTaxeOuMajoration.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfTaxeOuMajoration.setEnabled(true);
    }
    else {
      tfTaxeOuMajoration.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le montant de conditionnement.
   */
  private void rafraichirMontantConditionnement() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfMontantConditionnement.setText(Constantes.formater(prixAchat.getMontantConditionnement(), true));
    }
    else {
      tfMontantConditionnement.setText("");
    }
    
    if (modele.isDocumentModifiable()) {
      tfMontantConditionnement.setEnabled(true);
    }
    else {
      tfMontantConditionnement.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le prix d'achat net HT.
   */
  private void rafraichirPrixAchatNetHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPrixAchatNetHT.setText(Constantes.formater(prixAchat.getPrixAchatNetHT(), true));
      
      if (prixAchat.getIdUA() != null) {
        unPrixAchatNetHT.setText("par " + prixAchat.getIdUA().getCode());
      }
      else {
        unPrixAchatNetHT.setText("");
      }
    }
    else {
      tfPrixAchatNetHT.setText("");
      unPrixAchatNetHT.setText("");
    }
  }
  
  /**
   * Rafraichir le mode de saisie du port.
   */
  private void rafraichirModeSaisiePort() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      switch (prixAchat.getModeSaisiePort()) {
        case MONTANT_FOIS_POIDS:
          rbMontantAuPoidsPortHT.setSelected(true);
          tfMontantAuPoidsPortHT.setEnabled(true);
          tfPoidsPort.setEnabled(true);
          tfPourcentagePort.setEnabled(false);
          break;
        
        case POURCENTAGE_PRIX_NET:
          rbPourcentagePort.setSelected(true);
          tfMontantAuPoidsPortHT.setEnabled(false);
          tfPoidsPort.setEnabled(false);
          tfPourcentagePort.setEnabled(true);
          break;
      }
    }
    else {
      rbMontantAuPoidsPortHT.setSelected(false);
      rbPourcentagePort.setSelected(false);
      tfMontantAuPoidsPortHT.setEnabled(false);
      tfPoidsPort.setEnabled(false);
      tfPourcentagePort.setEnabled(false);
    }
    
    if (modele.isDocumentModifiable()) {
      rbMontantAuPoidsPortHT.setEnabled(true);
      rbPourcentagePort.setEnabled(true);
    }
    else {
      rbMontantAuPoidsPortHT.setEnabled(false);
      rbPourcentagePort.setEnabled(false);
      tfMontantAuPoidsPortHT.setEnabled(false);
      tfPoidsPort.setEnabled(false);
      tfPourcentagePort.setEnabled(false);
    }
  }
  
  /**
   * Rafraichir le montant au poids du port HT.
   */
  private void rafraichirMontantAuPoidsPortHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfMontantAuPoidsPortHT.setText(Constantes.formater(prixAchat.getMontantAuPoidsPortHT(), true));
    }
    else {
      tfMontantAuPoidsPortHT.setText("");
    }
  }
  
  /**
   * Rafraichir le poids du port.
   */
  private void rafraichirPoidsPort() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPoidsPort.setText(Constantes.formater(prixAchat.getPoidsPort(), false));
    }
    else {
      tfPoidsPort.setText("");
    }
  }
  
  /**
   * Rafraichir le poucentage de port.
   */
  private void rafraichirPourcentagePort() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null && prixAchat.getPourcentagePort() != null) {
      tfPourcentagePort.setText(Constantes.formater(prixAchat.getPourcentagePort(), true));
    }
    else {
      tfPourcentagePort.setText("");
    }
  }
  
  /**
   * Rafraichir le prix de revient fournisseur HT.
   */
  private void rafraichirPrixRevientFournisseurHT() {
    PrixAchat prixAchat = modele.getPrixAchat();
    if (prixAchat != null) {
      tfPrixRevientFournisseurHT.setText(Constantes.formater(prixAchat.getPrixRevientFournisseurHT(), true));
      
      if (prixAchat.getIdUA() != null) {
        unPrixRevientFournisseurHT.setText("par " + prixAchat.getIdUA().getCode());
      }
      else {
        unPrixRevientFournisseurHT.setText("");
      }
    }
    else {
      tfPrixRevientFournisseurHT.setText("");
      unPrixRevientFournisseurHT.setText("");
    }
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de quantité UCA.
   * 
   * @param e
   */
  private void tfQuantiteUCAFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUCA(tfQuantiteUCA.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de quantité UA.
   * 
   * @param e
   */
  private void tfQuantiteUAFocusLost(FocusEvent e) {
    try {
      modele.modifierQuantiteCommandeeUA(tfQuantiteUA.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de pourcentage de remise 1.
   * 
   * @param e
   */
  private void tfPourcentageRemise1FocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentageRemise1(tfPourcentageRemise1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de pourcentage de remise 2.
   * 
   * @param e
   */
  private void tfPourcentageRemise2FocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentageRemise2(tfPourcentageRemise2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de pourcentage de remise 3.
   * 
   * @param e
   */
  private void tfPourcentageRemise3FocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentageRemise3(tfPourcentageRemise3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de pourcentage de remise 4.
   * 
   * @param e
   */
  private void tfPourcentageRemise4FocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentageRemise4(tfPourcentageRemise4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du prix d'achat brut HT .
   * 
   * @param e
   */
  private void tfPrixAchatBrutHTFocusLost(FocusEvent e) {
    try {
      modele.modifierPrixAchatBrutHT(tfPrixAchatBrutHT.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du montant de conditionnement.
   * 
   * @param e
   */
  private void tfMontantConditionnementFocusLost(FocusEvent e) {
    try {
      modele.modifierMontantConditionnementAchat(tfMontantConditionnement.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du montant au poids du port.
   * 
   * @param e
   */
  private void tfMontantAuPoidsPortHTFocusLost(FocusEvent e) {
    try {
      modele.modifierMontantAuPoidsPortHT(tfMontantAuPoidsPortHT.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du poids du port.
   * 
   * @param e
   */
  private void tfPoidsPortFocusLost(FocusEvent e) {
    try {
      modele.modifierPoidsPort(tfPoidsPort.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du pourcentage de port.
   * 
   * @param e
   */
  private void tfPourcentagePortFocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentagePort(tfPourcentagePort.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du pourcentage de taxe ou de majoration.
   * 
   * @param e
   */
  private void tfTaxeOuMajorationFocusLost(FocusEvent e) {
    try {
      modele.modifierPourcentageTaxeOuMajoration(tfTaxeOuMajoration.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Montant fois poids" pour le mode de saisie du port.
   * 
   * @param e
   */
  private void rbMontantAuPoidsPortHTActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeSaisiePort(EnumModeSaisiePort.MONTANT_FOIS_POIDS);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "pourcentage prix net" pour le mode de saisie du port.
   * 
   * @param e
   */
  private void rbPourcentagePortActionPerformed(ActionEvent e) {
    try {
      modele.modifierModeSaisiePort(EnumModeSaisiePort.POURCENTAGE_PRIX_NET);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    pnlNegociationAchat = new SNPanel();
    pnlEnteteAchat = new SNPanel();
    lbQuantiteUCA = new SNLabelChamp();
    tfQuantiteUCA = new SNTexte();
    unQuantiteUCA = new SNLabelUnite();
    lbQuantiteUA = new SNLabelChamp();
    tfQuantiteUA = new SNTexte();
    unQuantiteUA = new SNLabelUnite();
    pnlTarifAchat = new SNPanelTitre();
    lbDateApplicationTarifAchat = new SNLabelChamp();
    snDateApplicationTarifAchat = new SNDate();
    lbPrixAchatBrutHT = new SNLabelChamp();
    pnlPrixAchatBrutHT = new SNPanel();
    tfPrixAchatBrutHT = new SNTexte();
    unPrixAchatBrutHT = new SNLabelUnite();
    lbPourcentageRemise = new SNLabelChamp();
    pnlPourcentageRemise = new SNPanel();
    tfPourcentageRemise1 = new SNTexte();
    lbPourcentage1 = new SNLabelUnite();
    tfPourcentageRemise2 = new SNTexte();
    lbPourcentage2 = new SNLabelUnite();
    tfPourcentageRemise3 = new SNTexte();
    lbPourcentage3 = new SNLabelUnite();
    tfPourcentageRemise4 = new SNTexte();
    lbPourcentage4 = new SNLabelUnite();
    lbTaxeOuMajoration = new SNLabelChamp();
    pnlTaxeOuMajoration = new SNPanel();
    tfTaxeOuMajoration = new SNTexte();
    unTaxeOuMajoration = new SNLabelUnite();
    lbMontantConditionnement = new SNLabelChamp();
    tfMontantConditionnement = new SNTexte();
    lbPrixAchatNetHT = new SNLabelChamp();
    pnlPrixAchatNetHT = new SNPanel();
    tfPrixAchatNetHT = new SNTexte();
    unPrixAchatNetHT = new SNLabelUnite();
    rbMontantAuPoidsPortHT = new JRadioButton();
    pnlMontantAuPoidsPortHT = new SNPanel();
    tfMontantAuPoidsPortHT = new SNTexte();
    lbSymboleMultiplication = new JLabel();
    tfPoidsPort = new SNTexte();
    rbPourcentagePort = new JRadioButton();
    pnlPourcentagePort = new SNPanel();
    tfPourcentagePort = new SNTexte();
    unPourcentagePort = new SNLabelUnite();
    lbPrixRevientFournisseurHT = new SNLabelChamp();
    pnlPrixRevientFournisseurHT = new SNPanel();
    tfPrixRevientFournisseurHT = new SNTexte();
    unPrixRevientFournisseurHT = new SNLabelUnite();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 520));
    setPreferredSize(new Dimension(945, 1000));
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlNegociationAchat ========
    {
      pnlNegociationAchat.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlNegociationAchat.setName("pnlNegociationAchat");
      pnlNegociationAchat.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWidths = new int[] { 970, 0 };
      ((GridBagLayout) pnlNegociationAchat.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlNegociationAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlNegociationAchat.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlEnteteAchat ========
      {
        pnlEnteteAchat.setName("pnlEnteteAchat");
        pnlEnteteAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEnteteAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlEnteteAchat.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlEnteteAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlEnteteAchat.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbQuantiteUCA ----
        lbQuantiteUCA.setText("Quantit\u00e9 en UCA");
        lbQuantiteUCA.setName("lbQuantiteUCA");
        pnlEnteteAchat.add(lbQuantiteUCA, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfQuantiteUCA ----
        tfQuantiteUCA.setHorizontalAlignment(SwingConstants.TRAILING);
        tfQuantiteUCA.setName("tfQuantiteUCA");
        tfQuantiteUCA.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfQuantiteUCAFocusLost(e);
          }
        });
        pnlEnteteAchat.add(tfQuantiteUCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- unQuantiteUCA ----
        unQuantiteUCA.setText("UCA");
        unQuantiteUCA.setName("unQuantiteUCA");
        pnlEnteteAchat.add(unQuantiteUCA, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbQuantiteUA ----
        lbQuantiteUA.setText("Quantit\u00e9 en UA");
        lbQuantiteUA.setMaximumSize(new Dimension(350, 30));
        lbQuantiteUA.setMinimumSize(new Dimension(350, 30));
        lbQuantiteUA.setPreferredSize(new Dimension(350, 30));
        lbQuantiteUA.setName("lbQuantiteUA");
        pnlEnteteAchat.add(lbQuantiteUA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- tfQuantiteUA ----
        tfQuantiteUA.setHorizontalAlignment(SwingConstants.TRAILING);
        tfQuantiteUA.setName("tfQuantiteUA");
        tfQuantiteUA.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfQuantiteUAFocusLost(e);
          }
        });
        pnlEnteteAchat.add(tfQuantiteUA, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- unQuantiteUA ----
        unQuantiteUA.setText("UA");
        unQuantiteUA.setName("unQuantiteUA");
        pnlEnteteAchat.add(unQuantiteUA, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlNegociationAchat.add(pnlEnteteAchat,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlTarifAchat ========
      {
        pnlTarifAchat.setTitre("N\u00e9gociation achat");
        pnlTarifAchat.setName("pnlTarifAchat");
        pnlTarifAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlTarifAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarifAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlTarifAchat.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlTarifAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDateApplicationTarifAchat ----
        lbDateApplicationTarifAchat.setText("Date d'application du tarif d'achat");
        lbDateApplicationTarifAchat.setMaximumSize(new Dimension(250, 30));
        lbDateApplicationTarifAchat.setMinimumSize(new Dimension(250, 30));
        lbDateApplicationTarifAchat.setPreferredSize(new Dimension(250, 30));
        lbDateApplicationTarifAchat.setName("lbDateApplicationTarifAchat");
        pnlTarifAchat.add(lbDateApplicationTarifAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDateApplicationTarifAchat ----
        snDateApplicationTarifAchat.setEnabled(false);
        snDateApplicationTarifAchat.setName("snDateApplicationTarifAchat");
        pnlTarifAchat.add(snDateApplicationTarifAchat, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPrixAchatBrutHT ----
        lbPrixAchatBrutHT.setText("Prix d'achat brut HT");
        lbPrixAchatBrutHT.setMaximumSize(new Dimension(250, 30));
        lbPrixAchatBrutHT.setMinimumSize(new Dimension(250, 30));
        lbPrixAchatBrutHT.setPreferredSize(new Dimension(250, 30));
        lbPrixAchatBrutHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbPrixAchatBrutHT.setName("lbPrixAchatBrutHT");
        pnlTarifAchat.add(lbPrixAchatBrutHT, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlPrixAchatBrutHT ========
        {
          pnlPrixAchatBrutHT.setName("pnlPrixAchatBrutHT");
          pnlPrixAchatBrutHT.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfPrixAchatBrutHT ----
          tfPrixAchatBrutHT.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPrixAchatBrutHT.setName("tfPrixAchatBrutHT");
          tfPrixAchatBrutHT.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPrixAchatBrutHTFocusLost(e);
            }
          });
          pnlPrixAchatBrutHT.add(tfPrixAchatBrutHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- unPrixAchatBrutHT ----
          unPrixAchatBrutHT.setText("UA");
          unPrixAchatBrutHT.setName("unPrixAchatBrutHT");
          pnlPrixAchatBrutHT.add(unPrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlPrixAchatBrutHT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPourcentageRemise ----
        lbPourcentageRemise.setText("Remise");
        lbPourcentageRemise.setName("lbPourcentageRemise");
        pnlTarifAchat.add(lbPourcentageRemise, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlPourcentageRemise ========
        {
          pnlPourcentageRemise.setName("pnlPourcentageRemise");
          pnlPourcentageRemise.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPourcentageRemise.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlPourcentageRemise.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPourcentageRemise.getLayout()).columnWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPourcentageRemise.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfPourcentageRemise1 ----
          tfPourcentageRemise1.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPourcentageRemise1.setName("tfPourcentageRemise1");
          tfPourcentageRemise1.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPourcentageRemise1FocusLost(e);
            }
          });
          pnlPourcentageRemise.add(tfPourcentageRemise1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage1 ----
          lbPourcentage1.setText("% ");
          lbPourcentage1.setMaximumSize(new Dimension(30, 30));
          lbPourcentage1.setMinimumSize(new Dimension(30, 30));
          lbPourcentage1.setPreferredSize(new Dimension(30, 30));
          lbPourcentage1.setName("lbPourcentage1");
          pnlPourcentageRemise.add(lbPourcentage1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPourcentageRemise2 ----
          tfPourcentageRemise2.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPourcentageRemise2.setName("tfPourcentageRemise2");
          tfPourcentageRemise2.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPourcentageRemise2FocusLost(e);
            }
          });
          pnlPourcentageRemise.add(tfPourcentageRemise2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage2 ----
          lbPourcentage2.setText("% ");
          lbPourcentage2.setMaximumSize(new Dimension(30, 30));
          lbPourcentage2.setMinimumSize(new Dimension(30, 30));
          lbPourcentage2.setPreferredSize(new Dimension(30, 30));
          lbPourcentage2.setName("lbPourcentage2");
          pnlPourcentageRemise.add(lbPourcentage2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPourcentageRemise3 ----
          tfPourcentageRemise3.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPourcentageRemise3.setName("tfPourcentageRemise3");
          tfPourcentageRemise3.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPourcentageRemise3FocusLost(e);
            }
          });
          pnlPourcentageRemise.add(tfPourcentageRemise3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage3 ----
          lbPourcentage3.setText("% ");
          lbPourcentage3.setMaximumSize(new Dimension(30, 30));
          lbPourcentage3.setMinimumSize(new Dimension(30, 30));
          lbPourcentage3.setPreferredSize(new Dimension(30, 30));
          lbPourcentage3.setName("lbPourcentage3");
          pnlPourcentageRemise.add(lbPourcentage3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPourcentageRemise4 ----
          tfPourcentageRemise4.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPourcentageRemise4.setName("tfPourcentageRemise4");
          tfPourcentageRemise4.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPourcentageRemise4FocusLost(e);
            }
          });
          pnlPourcentageRemise.add(tfPourcentageRemise4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbPourcentage4 ----
          lbPourcentage4.setText("% ");
          lbPourcentage4.setMaximumSize(new Dimension(30, 30));
          lbPourcentage4.setMinimumSize(new Dimension(30, 30));
          lbPourcentage4.setPreferredSize(new Dimension(30, 30));
          lbPourcentage4.setName("lbPourcentage4");
          pnlPourcentageRemise.add(lbPourcentage4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlPourcentageRemise, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTaxeOuMajoration ----
        lbTaxeOuMajoration.setText("Taxe ou majoration");
        lbTaxeOuMajoration.setName("lbTaxeOuMajoration");
        pnlTarifAchat.add(lbTaxeOuMajoration, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlTaxeOuMajoration ========
        {
          pnlTaxeOuMajoration.setName("pnlTaxeOuMajoration");
          pnlTaxeOuMajoration.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTaxeOuMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfTaxeOuMajoration ----
          tfTaxeOuMajoration.setHorizontalAlignment(SwingConstants.TRAILING);
          tfTaxeOuMajoration.setName("tfTaxeOuMajoration");
          tfTaxeOuMajoration.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfTaxeOuMajorationFocusLost(e);
            }
          });
          pnlTaxeOuMajoration.add(tfTaxeOuMajoration, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- unTaxeOuMajoration ----
          unTaxeOuMajoration.setText("% ");
          unTaxeOuMajoration.setName("unTaxeOuMajoration");
          pnlTaxeOuMajoration.add(unTaxeOuMajoration, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlTaxeOuMajoration, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbMontantConditionnement ----
        lbMontantConditionnement.setText("Montant conditionnement");
        lbMontantConditionnement.setName("lbMontantConditionnement");
        pnlTarifAchat.add(lbMontantConditionnement, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfMontantConditionnement ----
        tfMontantConditionnement.setHorizontalAlignment(SwingConstants.TRAILING);
        tfMontantConditionnement.setName("tfMontantConditionnement");
        tfMontantConditionnement.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfMontantConditionnementFocusLost(e);
          }
        });
        pnlTarifAchat.add(tfMontantConditionnement, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixAchatNetHT ----
        lbPrixAchatNetHT.setText("Prix d'achat net HT");
        lbPrixAchatNetHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbPrixAchatNetHT.setName("lbPrixAchatNetHT");
        pnlTarifAchat.add(lbPrixAchatNetHT, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlPrixAchatNetHT ========
        {
          pnlPrixAchatNetHT.setName("pnlPrixAchatNetHT");
          pnlPrixAchatNetHT.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfPrixAchatNetHT ----
          tfPrixAchatNetHT.setEnabled(false);
          tfPrixAchatNetHT.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPrixAchatNetHT.setName("tfPrixAchatNetHT");
          pnlPrixAchatNetHT.add(tfPrixAchatNetHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- unPrixAchatNetHT ----
          unPrixAchatNetHT.setText("UA");
          unPrixAchatNetHT.setName("unPrixAchatNetHT");
          pnlPrixAchatNetHT.add(unPrixAchatNetHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlPrixAchatNetHT, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- rbMontantAuPoidsPortHT ----
        rbMontantAuPoidsPortHT.setText("Montant port HT");
        rbMontantAuPoidsPortHT.setFont(rbMontantAuPoidsPortHT.getFont().deriveFont(rbMontantAuPoidsPortHT.getFont().getSize() + 2f));
        rbMontantAuPoidsPortHT.setMaximumSize(new Dimension(140, 30));
        rbMontantAuPoidsPortHT.setMinimumSize(new Dimension(140, 30));
        rbMontantAuPoidsPortHT.setPreferredSize(new Dimension(140, 30));
        rbMontantAuPoidsPortHT.setName("rbMontantAuPoidsPortHT");
        rbMontantAuPoidsPortHT.addActionListener(e -> rbMontantAuPoidsPortHTActionPerformed(e));
        pnlTarifAchat.add(rbMontantAuPoidsPortHT, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlMontantAuPoidsPortHT ========
        {
          pnlMontantAuPoidsPortHT.setName("pnlMontantAuPoidsPortHT");
          pnlMontantAuPoidsPortHT.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlMontantAuPoidsPortHT.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlMontantAuPoidsPortHT.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlMontantAuPoidsPortHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlMontantAuPoidsPortHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfMontantAuPoidsPortHT ----
          tfMontantAuPoidsPortHT.setHorizontalAlignment(SwingConstants.TRAILING);
          tfMontantAuPoidsPortHT.setName("tfMontantAuPoidsPortHT");
          tfMontantAuPoidsPortHT.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfMontantAuPoidsPortHTFocusLost(e);
            }
          });
          pnlMontantAuPoidsPortHT.add(tfMontantAuPoidsPortHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbSymboleMultiplication ----
          lbSymboleMultiplication.setText("x poids");
          lbSymboleMultiplication.setFont(lbSymboleMultiplication.getFont()
              .deriveFont(lbSymboleMultiplication.getFont().getStyle() & ~Font.BOLD, lbSymboleMultiplication.getFont().getSize() + 2f));
          lbSymboleMultiplication.setHorizontalAlignment(SwingConstants.CENTER);
          lbSymboleMultiplication.setName("lbSymboleMultiplication");
          pnlMontantAuPoidsPortHT.add(lbSymboleMultiplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPoidsPort ----
          tfPoidsPort.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPoidsPort.setName("tfPoidsPort");
          tfPoidsPort.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPoidsPortFocusLost(e);
            }
          });
          pnlMontantAuPoidsPortHT.add(tfPoidsPort, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlMontantAuPoidsPortHT, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- rbPourcentagePort ----
        rbPourcentagePort.setText("Pourcentage port");
        rbPourcentagePort.setFont(rbPourcentagePort.getFont().deriveFont(rbPourcentagePort.getFont().getSize() + 2f));
        rbPourcentagePort.setMaximumSize(new Dimension(140, 30));
        rbPourcentagePort.setMinimumSize(new Dimension(140, 30));
        rbPourcentagePort.setPreferredSize(new Dimension(140, 30));
        rbPourcentagePort.setName("rbPourcentagePort");
        rbPourcentagePort.addActionListener(e -> rbPourcentagePortActionPerformed(e));
        pnlTarifAchat.add(rbPourcentagePort, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlPourcentagePort ========
        {
          pnlPourcentagePort.setName("pnlPourcentagePort");
          pnlPourcentagePort.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfPourcentagePort ----
          tfPourcentagePort.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPourcentagePort.setName("tfPourcentagePort");
          tfPourcentagePort.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPourcentagePortFocusLost(e);
            }
          });
          pnlPourcentagePort.add(tfPourcentagePort, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- unPourcentagePort ----
          unPourcentagePort.setText("% ");
          unPourcentagePort.setName("unPourcentagePort");
          pnlPourcentagePort.add(unPourcentagePort, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlPourcentagePort, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbPrixRevientFournisseurHT ----
        lbPrixRevientFournisseurHT.setText("Prix de revient fournisseur HT");
        lbPrixRevientFournisseurHT.setImportanceMessage(EnumImportanceMessage.MOYEN);
        lbPrixRevientFournisseurHT.setName("lbPrixRevientFournisseurHT");
        pnlTarifAchat.add(lbPrixRevientFournisseurHT, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlPrixRevientFournisseurHT ========
        {
          pnlPrixRevientFournisseurHT.setName("pnlPrixRevientFournisseurHT");
          pnlPrixRevientFournisseurHT.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfPrixRevientFournisseurHT ----
          tfPrixRevientFournisseurHT.setEnabled(false);
          tfPrixRevientFournisseurHT.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPrixRevientFournisseurHT.setName("tfPrixRevientFournisseurHT");
          pnlPrixRevientFournisseurHT.add(tfPrixRevientFournisseurHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- unPrixRevientFournisseurHT ----
          unPrixRevientFournisseurHT.setText("UA");
          unPrixRevientFournisseurHT.setName("unPrixRevientFournisseurHT");
          pnlPrixRevientFournisseurHT.add(unPrixRevientFournisseurHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlTarifAchat.add(pnlPrixRevientFournisseurHT, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlNegociationAchat.add(pnlTarifAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
          GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlNegociationAchat, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbMontantAuPoidsPortHT);
    buttonGroup1.add(rbPourcentagePort);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanel pnlNegociationAchat;
  private SNPanel pnlEnteteAchat;
  private SNLabelChamp lbQuantiteUCA;
  private SNTexte tfQuantiteUCA;
  private SNLabelUnite unQuantiteUCA;
  private SNLabelChamp lbQuantiteUA;
  private SNTexte tfQuantiteUA;
  private SNLabelUnite unQuantiteUA;
  private SNPanelTitre pnlTarifAchat;
  private SNLabelChamp lbDateApplicationTarifAchat;
  private SNDate snDateApplicationTarifAchat;
  private SNLabelChamp lbPrixAchatBrutHT;
  private SNPanel pnlPrixAchatBrutHT;
  private SNTexte tfPrixAchatBrutHT;
  private SNLabelUnite unPrixAchatBrutHT;
  private SNLabelChamp lbPourcentageRemise;
  private SNPanel pnlPourcentageRemise;
  private SNTexte tfPourcentageRemise1;
  private SNLabelUnite lbPourcentage1;
  private SNTexte tfPourcentageRemise2;
  private SNLabelUnite lbPourcentage2;
  private SNTexte tfPourcentageRemise3;
  private SNLabelUnite lbPourcentage3;
  private SNTexte tfPourcentageRemise4;
  private SNLabelUnite lbPourcentage4;
  private SNLabelChamp lbTaxeOuMajoration;
  private SNPanel pnlTaxeOuMajoration;
  private SNTexte tfTaxeOuMajoration;
  private SNLabelUnite unTaxeOuMajoration;
  private SNLabelChamp lbMontantConditionnement;
  private SNTexte tfMontantConditionnement;
  private SNLabelChamp lbPrixAchatNetHT;
  private SNPanel pnlPrixAchatNetHT;
  private SNTexte tfPrixAchatNetHT;
  private SNLabelUnite unPrixAchatNetHT;
  private JRadioButton rbMontantAuPoidsPortHT;
  private SNPanel pnlMontantAuPoidsPortHT;
  private SNTexte tfMontantAuPoidsPortHT;
  private JLabel lbSymboleMultiplication;
  private SNTexte tfPoidsPort;
  private JRadioButton rbPourcentagePort;
  private SNPanel pnlPourcentagePort;
  private SNTexte tfPourcentagePort;
  private SNLabelUnite unPourcentagePort;
  private SNLabelChamp lbPrixRevientFournisseurHT;
  private SNPanel pnlPrixRevientFournisseurHT;
  private SNTexte tfPrixRevientFournisseurHT;
  private SNLabelUnite unPrixRevientFournisseurHT;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
