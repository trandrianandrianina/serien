/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisonnementcommandeclient;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Description d'une ligne à afficher dans le tableau des commandes d'achat en cours pour un article.
 */
public class LigneATraiter {
  // Variables de travail
  private LigneVente ligneVente = null;
  private IdDocumentVente idDocumentVente = null;
  private boolean modeLivraison = false;
  private Article article = null;
  private boolean charge = false;
  private LigneAchatArticle ligneAchatAssociee = null;
  private int indiceLigneDansListeATraiter = -1;
  
  // Variables à afficher
  private String quantiteCommandeeUCA = null;
  private String uniteCommandeUCA = null;
  private String raisonSociale = null;
  private String documentVente = null;
  private String dateLivraisonPrevue = null;
  private String libelle = null;
  private String libelleTypeArticle = null;
  private String stockReel = null;
  private String stockReserve = null;
  private String stockAttendu = null;
  private String stockATerme = null;
  
  /**
   * Constructeur.
   */
  public LigneATraiter(LigneVente pLigneVente, int pIndiceLigne, boolean pModeLivraison) {
    ligneVente = pLigneVente;
    indiceLigneDansListeATraiter = pIndiceLigne;
    modeLivraison = pModeLivraison;
    
    // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
    if (ligneVente != null) {
      IdLigneVente idLigneVente = ligneVente.getId();
      idDocumentVente = IdDocumentVente.getInstanceHorsFacture(idLigneVente.getIdEtablissement(), idLigneVente.getCodeEntete(),
          idLigneVente.getNumero(), idLigneVente.getSuffixe());
    }
  }
  
  // -- Méthodes public
  
  /**
   * Initialise les données.
   */
  public void initialiserDonnees(ReapprovisionnementClient pReapprovisionnement) {
    // Initialisation des informations provenant de l'article
    if (pReapprovisionnement == null) {
      return;
    }
    article = pReapprovisionnement.getArticle();
    if (article == null) {
      throw new MessageErreurException("L'article à approvisionner est invalide.");
    }
    Client client = pReapprovisionnement.getClient();
    
    // Les données du client
    if (client != null) {
      raisonSociale = client.getAdresse().getNom();
    }
    
    // Les données de la ligne de vente
    if (idDocumentVente != null) {
      documentVente = idDocumentVente.toString();
    }
    
    // La date de livraison uniquement si le document de vente est en mode livraison
    dateLivraisonPrevue = "";
    if (modeLivraison) {
      dateLivraisonPrevue = Constantes.convertirDateEnTexte(pReapprovisionnement.getDateLivraisonPrevue());
    }
    
    // Alimentation des variables qui seront affichées dans le tableau
    quantiteCommandeeUCA = "0";
    uniteCommandeUCA = "";
    
    // Les données de l'article
    if (article.getPrixAchat() != null) {
      if (article.getPrixAchat().getQuantiteReliquatUA() != null) {
        quantiteCommandeeUCA = Constantes.formater(article.getPrixAchat().getQuantiteReliquatUCA(), false);
      }
      if (article.getPrixAchat().getIdUCA() != null) {
        uniteCommandeUCA = article.getPrixAchat().getIdUCA().getCode();
      }
    }
    
    libelle = Constantes.normerTexte(article.getLibelle1());
    
    // Renseigner le type de l'article
    if (article.isHorsGamme()) {
      libelleTypeArticle = Article.LIBELLE_HORS_GAMME;
    }
    else if (article.getTypeArticle() != null) {
      // On change le libellé du type de l'article uniquement s'il est Standard
      if (article.getTypeArticle().equals(EnumTypeArticle.STANDARD)) {
        libelleTypeArticle = "Géré en stock";
      }
      else {
        libelleTypeArticle = article.getTypeArticle().getLibelle();
      }
    }
    else {
      libelleTypeArticle = "";
    }
    
    stockReel = Constantes.formater(article.getQuantitePhysique(), false);
    stockReserve = Constantes.formater(article.getQuantiteReservee(), false);
    stockAttendu = Constantes.formater(article.getQuantiteAttendueUCA(), false);
    // Calcul du stock à terme
    BigDecimal stock = article.calculerStockDisponible();
    if (stock == null) {
      stockATerme = "";
    }
    else {
      stockATerme = Constantes.formater(stock, false);
    }
    
    // Si on est arrivé ici alors la ligne est complètement chargée
    charge = true;
  }
  
  /**
   * Contrôle la quantité commandée saisie.
   */
  public void controlerQuantite(String pQuantiteSaisie) {
    if (pQuantiteSaisie == null) {
      return;
    }
    
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pQuantiteSaisie);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("La quantité saisie est invalide, elle doit être supérieure à 0.");
    }
    // On contrôle que la quantité commandée saisie n'entraîne pas un stock à terme supérieur au stock maximum
    BigDecimal stockaterme = article.calculerStockDisponible();
    if (stockaterme == null) {
      throw new MessageErreurException("Le stock à terme de cet article ne peut être calculer.");
    }
    if (article.getQuantiteStockMaximumUCA() == null) {
      throw new MessageErreurException("Le stock maximum est invalide.");
    }
    
    // On ajoute un contrôle si le stock maximum est supérieur à 0
    // Le stock à terme doit être inférieur ou égal au stock maximum
    if (article.getQuantiteStockMaximumUCA().compareTo(BigDecimal.ZERO) > 0) {
      stockaterme = stockaterme.add(quantite);
      if (stockaterme.compareTo(article.getQuantiteStockMaximumUCA()) > 0) {
        throw new MessageErreurException("La quantité saisie (" + Constantes.formater(quantite, false)
            + ") est trop grande ce qui entraîne un stock à terme (" + Constantes.formater(stockaterme, false)
            + ") supérieur au stock maximum (" + Constantes.formater(article.getQuantiteStockMaximumUCA(), false) + ").");
      }
    }
  }
  
  /**
   * Modifie la quantité commandée.
   */
  public void modifierQuantite(BigDecimal pQuantiteSaisie) {
    quantiteCommandeeUCA = "0";
    if (pQuantiteSaisie == null) {
      return;
    }
    
    // On met à jour la quantité saisie dans la classe PrixAchat
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getIdUCA() != null) {
      article.getPrixAchat().setQuantiteReliquatUCA(pQuantiteSaisie, true);
    }
    quantiteCommandeeUCA = Constantes.formater(pQuantiteSaisie, false);
  }
  
  /**
   * Vérifie si l'article à une quantité surpérieure à 0.
   */
  public boolean isArticleACommander() {
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getQuantiteReliquatUCA() != null) {
      return article.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) > 0;
    }
    return false;
  }
  
  /**
   * Indique si la ligne de vente est une ligne commentaire.
   * 
   * @return
   */
  public boolean isLigneCommentaire() {
    return ligneVente.isLigneCommentaire();
  }
  
  // -- Accesseurs
  
  public boolean isCharge() {
    return charge;
  }
  
  public IdLigneVente getIdLigneVente() {
    if (ligneVente == null) {
      return null;
    }
    return ligneVente.getId();
  }
  
  public LigneVente getLigneVente() {
    return ligneVente;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public String getRaisonSociale() {
    return raisonSociale;
  }
  
  public String getDocumentVente() {
    return documentVente;
  }
  
  public String getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public BigDecimal getQuantiteCommandeeUCABigDecimal() {
    return Constantes.convertirTexteEnBigDecimal(Constantes.normerTexte(quantiteCommandeeUCA));
  }
  
  public String getQuantiteCommandeeUCA() {
    return quantiteCommandeeUCA;
  }
  
  public String getUniteCommandeUCA() {
    return uniteCommandeUCA;
  }
  
  public String getLibelleTypeArticle() {
    return libelleTypeArticle;
  }
  
  public String getStockReel() {
    return stockReel;
  }
  
  public String getStockReserve() {
    return stockReserve;
  }
  
  public String getStockAttendu() {
    return stockAttendu;
  }
  
  public String getStockATerme() {
    return stockATerme;
  }
  
  public LigneAchatArticle getLigneAchatAssociee() {
    return ligneAchatAssociee;
  }
  
  public void setLigneAchatAssociee(LigneAchatArticle ligneAchatAssociee) {
    this.ligneAchatAssociee = ligneAchatAssociee;
  }
  
  /**
   * Retourne l'indice de la ligne dans la liste des lignes à traiter.
   * Utilisé ensuite afin de générer l'identifiant de la ligne d'achat afin de conserver l'ordre des lignes.
   * 
   * @return
   */
  public int getIndiceLigneDansListeATraiter() {
    return indiceLigneDansListeATraiter;
  }
}
