/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.suppressionreliquat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueRechercheReliquat extends AbstractVuePanel<ModeleReliquat> {
  // Constantes
  private static final String BOUTON_AFFICHER_LIGNE_VENTE = "Afficher lignes de ventes";
  private static final String BOUTON_SOLDER_RELIQUAT = "Solder le reliquat";
  private static final String[] TITRE_LISTE_DOCUMENT =
      new String[] { "Num\u00e9ro", "Date cr\u00e9ation", "R\u00e9f\u00e9rence", "Montant HT", "Client", "Vendeur" };
  // Onglets des liste de documents
  private static final int ONGLET_DEVIS = 0;
  private static final int ONGLET_COMMANDES = 1;
  
  // Variables
  private DefaultTableModel tableModelDocument = null;
  
  /**
   * Constructeur.
   */
  public VueRechercheReliquat(ModeleReliquat pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Champs de saisie
    tfNumeroDocument.setLongueur(IdDocumentVente.LONGUEUR_NUMERO);
    
    // Initialise l'aspect de la table des devis
    scpListeDevis.getViewport().setBackground(Color.WHITE);
    tblListeDevis.personnaliserAspect(TITRE_LISTE_DOCUMENT, new int[] { 80, 100, 400, 100, 200, 200 },
        new int[] { 100, 120, 1000, 200, 300, 300 },
        new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    
    // Ajouter un listener sur les changement de sélection
    tblListeDevis.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblDevisEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportDevis = scpListeDevis.getViewport();
    viewportDevis.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpDevisEnCoursStateChanged(e);
      }
    });
    scpListeDevis.setViewport(viewportDevis);
    
    // Initialise l'aspect de la table des commandes
    scpListeCommande.getViewport().setBackground(Color.WHITE);
    tblListeCommande.personnaliserAspect(TITRE_LISTE_DOCUMENT, new int[] { 80, 100, 400, 100, 200, 200 },
        new int[] { 100, 120, 1000, 200, 300, 300 },
        new int[] { NRiTable.DROITE, NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.GAUCHE }, 14);
    
    // Ajouter un listener sur les changement de sélection
    tblListeCommande.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblCommandeEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener sur les changement de sélection
    tblListeCommande.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblCommandeEnCoursValueChanged(e);
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportCommandes = scpListeCommande.getViewport();
    viewportCommandes.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpCommandesEnCoursStateChanged(e);
      }
    });
    scpListeCommande.setViewport(viewportCommandes);
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_LIGNE_VENTE, 'a', true);
    snBarreBouton.ajouterBouton(BOUTON_SOLDER_RELIQUAT, 's', true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snClient.requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Rafraîchir filtre colonne de gauche
    rafraichirSelectionClient();
    rafraichirNumeroDocument();
    
    // Rafraîchir filtre colonne de droite
    rafraichirEtablissement();
    rafraichirMagasin();
    rafraichirVendeur();
    
    // Rafraîchir tableau
    rafraichirTitreListeDocumentVente();
    rafraichirListeDocumentVente();
    
    // Rafraîchir boutons
    rafraichirBoutonSolder();
    rafraichirBoutonAfficherLigneVente();
  }
  
  private void rafraichirSelectionClient() {
    // Configurer le composant
    snClient.setSession(getModele().getSession());
    if (getModele().getIdEtablissement() != null) {
      snClient.setIdEtablissement(getModele().getIdEtablissement());
    }
    else {
      snClient.setIdEtablissement(null);
    }
    
    // Rafraîchir sa valeur
    if (getModele().getIdClient() != null && getModele().getIdClient().isExistant()) {
      snClient.setSelectionParId(getModele().getIdClient());
    }
    else {
      snClient.setSelection(null);
    }
  }
  
  private void rafraichirNumeroDocument() {
    String numero = "";
    if (getModele().getNumeroDocumentARecherher() != null && getModele().getNumeroDocumentARecherher() > 0) {
      numero = Constantes.convertirIntegerEnTexte(getModele().getNumeroDocumentARecherher(), 0);
    }
    tfNumeroDocument.setText(numero);
    tfNumeroDocument.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirEtablissement() {
    // Charger la liste des établissements le premier coup
    ListeEtablissement listeEtablissement = getModele().getListeEtablissement();
    if (snEtablissement.isEmpty() && listeEtablissement != null) {
      snEtablissement.setListe(listeEtablissement);
    }
    if (getModele().getIdEtablissement() != null) {
      snEtablissement.setIdSelection(getModele().getIdEtablissement());
    }
    snEtablissement.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirMagasin() {
    snMagasin.setSession(getModele().getSession());
    snMagasin.setIdEtablissement(getModele().getIdEtablissement());
    snMagasin.setListe(getModele().getListeMagasin());
    snMagasin.setIdSelection(getModele().getIdMagasin());
    snMagasin.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirVendeur() {
    Vendeur vendeur = getModele().getVendeur();
    snVendeur.setSession(getModele().getSession());
    snVendeur.setIdEtablissement(getModele().getIdEtablissement());
    snVendeur.setTousAutorise(true);
    snVendeur.setListe(getModele().getListeVendeur());
    if (vendeur != null) {
      snVendeur.setIdSelection(vendeur.getId());
    }
    snVendeur.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirTitreListeDocumentVente() {
    if (getModele().getTitreResultat() != null) {
      lblTitreResultat.setMessage(getModele().getTitreResultat());
    }
    else {
      lblTitreResultat.setText("Documents correspondants à votre recherche");
    }
  }
  
  private void rafraichirListeDocumentVente() {
    tbpListeDocument.setEnabled(true);
    
    // Rafraîhir les différents onglets
    rafraichirListeDevis();
    rafraichirListeCommande();
    
    // Activer le premier onglet non grisé
    if (getModele().isActivationOngletListeDocument()) {
      getModele().setActivationOngletListeDocument(false);
      boolean actif = false;
      for (int tab = 0; tab < tbpListeDocument.getTabCount(); tab++) {
        if (tbpListeDocument.isEnabledAt(tab)) {
          tbpListeDocument.setSelectedIndex(tab);
          actif = true;
          break;
        }
      }
      
      // S'il n'y a aucun document alors on active le premier onglet
      if (!actif) {
        tbpListeDocument.setSelectedIndex(ONGLET_DEVIS);
      }
    }
  }
  
  private void rafraichirListeDevis() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = getModele().getModeleDocumentVenteParType();
    
    // Désactiver l'onglet devis s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeDevis() == null
        || modeleDocumentVenteParType.getListeDevis().isEmpty()) {
      tblListeDevis.mettreAJourDonnees(null);
      tbpListeDocument.setEnabledAt(ONGLET_DEVIS, false);
      tbpListeDocument.setTitleAt(ONGLET_DEVIS, "Devis");
      scpListeDevis.getVerticalScrollBar().setValue(0);
      scpListeDevis.getViewport().reshape(0, 0, scpListeDevis.getViewport().getWidth(), 0);
      return;
    }
    tbpListeDocument.setEnabledAt(ONGLET_DEVIS, true);
    tbpListeDocument.setTitleAt(ONGLET_DEVIS, "Devis (" + modeleDocumentVenteParType.getListeDevis().size() + ")");
    
    // Les données
    ListeDocumentVenteBase listeDevis = modeleDocumentVenteParType.getListeDevis();
    String[][] donnees = null;
    
    if (listeDevis != null) {
      donnees = new String[listeDevis.size()][TITRE_LISTE_DOCUMENT.length];
      for (int ligne = 0; ligne < listeDevis.size(); ligne++) {
        DocumentVenteBase documentVenteBase = listeDevis.get(ligne);
        
        // Identifiant
        donnees[ligne][0] = documentVenteBase.getId().toString();
        
        // Date de création
        if (documentVenteBase.isVerrouille()) {
          donnees[ligne][1] = "Verrouillé";
        }
        else if (documentVenteBase.getDateCreation() != null) {
          donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
        }
        
        // Référence longue
        if (documentVenteBase.getReferenceLongue() != null) {
          donnees[ligne][2] = documentVenteBase.getReferenceLongue();
        }
        
        // Total HT
        if (documentVenteBase.getTotalHT() != null) {
          donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        }
        
        // Le client
        donnees[ligne][4] = getModele().retournerNomClient(documentVenteBase);
        
        // Le vendeur
        IdVendeur idVendeur = documentVenteBase.getIdVendeur();
        if (idVendeur != null) {
          donnees[ligne][5] = getModele().retournerNomVendeur(idVendeur);
        }
      }
    }
    tblListeDevis.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexDevisSelectionne();
    if (index > -1) {
      tblListeDevis.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblListeDevis.clearSelection();
    }
  }
  
  private void rafraichirListeCommande() {
    ModeleDocumentVenteParType modeleDocumentVenteParType = getModele().getModeleDocumentVenteParType();
    
    // Désactiver l'onglet commande s'il n'y a en pas
    if (modeleDocumentVenteParType == null || modeleDocumentVenteParType.getListeCommande() == null
        || modeleDocumentVenteParType.getListeCommande().isEmpty()) {
      tblListeCommande.mettreAJourDonnees(null);
      tbpListeDocument.setEnabledAt(ONGLET_COMMANDES, false);
      tbpListeDocument.setTitleAt(ONGLET_COMMANDES, "Commandes");
      scpListeCommande.getVerticalScrollBar().setValue(0);
      scpListeCommande.getViewport().reshape(0, 0, scpListeCommande.getViewport().getWidth(), 0);
      return;
    }
    tbpListeDocument.setEnabledAt(ONGLET_COMMANDES, true);
    tbpListeDocument.setTitleAt(ONGLET_COMMANDES, "Commandes (" + modeleDocumentVenteParType.getListeCommande().size() + ")");
    
    // Les données
    ListeDocumentVenteBase listeCommande = modeleDocumentVenteParType.getListeCommande();
    String[][] donnees = null;
    
    if (listeCommande != null) {
      donnees = new String[listeCommande.size()][TITRE_LISTE_DOCUMENT.length];
      for (int ligne = 0; ligne < listeCommande.size(); ligne++) {
        DocumentVenteBase documentVenteBase = listeCommande.get(ligne);
        
        // Identifiant
        donnees[ligne][0] = documentVenteBase.getId().toString();
        
        // Date de création
        if (documentVenteBase.isVerrouille()) {
          donnees[ligne][1] = "Verrouillé";
        }
        else if (documentVenteBase.getDateCreation() != null) {
          donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, documentVenteBase.getDateCreation());
        }
        
        // Référence longue
        if (documentVenteBase.getReferenceLongue() != null) {
          donnees[ligne][2] = documentVenteBase.getReferenceLongue();
        }
        
        // Total HT
        if (documentVenteBase.getTotalHT() != null) {
          donnees[ligne][3] = Constantes.formater(documentVenteBase.getTotalHT(), true);
        }
        
        // Le client
        donnees[ligne][4] = getModele().retournerNomClient(documentVenteBase);
        
        // Le vendeur
        IdVendeur idVendeur = documentVenteBase.getIdVendeur();
        if (idVendeur != null) {
          donnees[ligne][5] = getModele().retournerNomVendeur(idVendeur);
        }
      }
    }
    tblListeCommande.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours de sélection
    int index = modeleDocumentVenteParType.getIndexCommandeSelectionnee();
    if (index > -1) {
      tblListeCommande.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblListeCommande.clearSelection();
    }
  }
  
  /**
   * Afficher le bouton "Solder le reliquat".
   */
  private void rafraichirBoutonSolder() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun document de ventes n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_SOLDER_RELIQUAT, actif);
  }
  
  /**
   * Afficher le bouton "Afficher lignes de ventes".
   */
  private void rafraichirBoutonAfficherLigneVente() {
    boolean actif = true;
    DocumentVenteBase documentVenteBase = getModele().getDocumentVenteBaseSelectionne();
    
    // Désactiver le bouton si aucun document de ventes n'est sélectionné
    if (documentVenteBase == null) {
      actif = false;
    }
    // Désactiver le bouton si le document de ventes est verrouillé
    else if (documentVenteBase.isVerrouille()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AFFICHER_LIGNE_VENTE, actif);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_AFFICHER_LIGNE_VENTE)) {
        getModele().afficherConsultationLigneVente();
      }
      if (pSNBouton.isBouton(BOUTON_SOLDER_RELIQUAT)) {
        getModele().solderReliquat();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher(true);
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snClientValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (snClient.getSelection() != null) {
        getModele().modifierClient(snClient.getSelection().getId());
      }
      else {
        getModele().modifierClient(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      snClient.initialiserRecherche();
    }
  }
  
  private void tfNumeroDocumentFocusLost(FocusEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroDocumentActionPerformed(ActionEvent e) {
    try {
      getModele().modifierNumeroDocument(tfNumeroDocument.getText());
      getModele().rechercher(true);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissement.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snMagasinValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasin(snMagasin.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snVendeurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierVendeur(snVendeur.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblDevisEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().selectionnerDevis(tblListeDevis.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeDevisMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        getModele().afficherConsultationLigneVente();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpDevisEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeDevis == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeDevis.getViewport().getViewRect();
      int premiereLigne = tblListeDevis.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeDevis.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeDevis.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageDevis(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblCommandeEnCoursValueChanged(ListSelectionEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().selectionnerCommande(tblListeCommande.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeCommandeMouseClicked(MouseEvent e) {
    try {
      if (e.getClickCount() == 2) {
        getModele().afficherConsultationLigneVente();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void scpCommandesEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeCommande == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeCommande.getViewport().getViewRect();
      int premiereLigne = tblListeCommande.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeCommande.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeCommande.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageCommande(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlFiltre = new SNPanel();
    pnlFiltreGauche = new SNPanel();
    lbRechercheClient = new SNLabelChamp();
    snClient = new SNClient();
    lbNumeroDocument = new SNLabelChamp();
    tfNumeroDocument = new SNIdentifiant();
    pnlFiltreDroite = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    tbpListeDocument = new JTabbedPane();
    pnlListeDevis = new SNPanel();
    scpListeDevis = new JScrollPane();
    tblListeDevis = new NRiTable();
    pnlListeCommande = new SNPanel();
    scpListeCommande = new JScrollPane();
    tblListeCommande = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setMinimumSize(new Dimension(1200, 562));
      pnlContenu.setPreferredSize(new Dimension(1200, 691));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 11, 279, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlFiltre ========
      {
        pnlFiltre.setOpaque(false);
        pnlFiltre.setBorder(new TitledBorder(""));
        pnlFiltre.setMinimumSize(new Dimension(1200, 190));
        pnlFiltre.setPreferredSize(new Dimension(1200, 174));
        pnlFiltre.setName("pnlFiltre");
        pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
        
        // ======== pnlFiltreGauche ========
        {
          pnlFiltreGauche.setOpaque(false);
          pnlFiltreGauche.setName("pnlFiltreGauche");
          pnlFiltreGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltreGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbRechercheClient ----
          lbRechercheClient.setText("Recherche client");
          lbRechercheClient.setName("lbRechercheClient");
          pnlFiltreGauche.add(lbRechercheClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snClient ----
          snClient.setName("snClient");
          snClient.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snClientValueChanged(e);
            }
          });
          pnlFiltreGauche.add(snClient, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNumeroDocument ----
          lbNumeroDocument.setText("Num\u00e9ro de document");
          lbNumeroDocument.setName("lbNumeroDocument");
          pnlFiltreGauche.add(lbNumeroDocument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroDocument ----
          tfNumeroDocument.setName("tfNumeroDocument");
          tfNumeroDocument.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tfNumeroDocumentActionPerformed(e);
            }
          });
          tfNumeroDocument.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroDocumentFocusLost(e);
            }
          });
          pnlFiltreGauche.add(tfNumeroDocument, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltreGauche);
        
        // ======== pnlFiltreDroite ========
        {
          pnlFiltreDroite.setOpaque(false);
          pnlFiltreDroite.setName("pnlFiltreDroite");
          pnlFiltreDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltreDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltreDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlFiltreDroite.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlFiltreDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlFiltreDroite.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          snMagasin.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snMagasinValueChanged(e);
            }
          });
          pnlFiltreDroite.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbVendeur ----
          lbVendeur.setText("Vendeur");
          lbVendeur.setName("lbVendeur");
          pnlFiltreDroite.add(lbVendeur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snVendeur ----
          snVendeur.setName("snVendeur");
          snVendeur.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snVendeurValueChanged(e);
            }
          });
          pnlFiltreDroite.add(snVendeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          pnlFiltreDroite.add(snBarreRecherche, new GridBagConstraints(0, 3, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltreDroite);
      }
      pnlContenu.add(pnlFiltre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lblTitreResultat ----
      lblTitreResultat.setText("Document en cours");
      lblTitreResultat.setName("lblTitreResultat");
      pnlContenu.add(lblTitreResultat,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== tbpListeDocument ========
      {
        tbpListeDocument.setFont(new Font("sansserif", Font.PLAIN, 14));
        tbpListeDocument.setName("tbpListeDocument");
        
        // ======== pnlListeDevis ========
        {
          pnlListeDevis.setName("pnlListeDevis");
          pnlListeDevis.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlListeDevis.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlListeDevis.getLayout()).rowHeights = new int[] { 279, 0 };
          ((GridBagLayout) pnlListeDevis.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlListeDevis.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scpListeDevis ========
          {
            scpListeDevis.setPreferredSize(new Dimension(1050, 424));
            scpListeDevis.setFont(new Font("sansserif", Font.PLAIN, 14));
            scpListeDevis.setBackground(Color.white);
            scpListeDevis.setOpaque(true);
            scpListeDevis.setName("scpListeDevis");
            
            // ---- tblListeDevis ----
            tblListeDevis.setShowVerticalLines(true);
            tblListeDevis.setShowHorizontalLines(true);
            tblListeDevis.setBackground(Color.white);
            tblListeDevis.setRowHeight(20);
            tblListeDevis.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblListeDevis.setAutoCreateRowSorter(true);
            tblListeDevis.setSelectionBackground(new Color(57, 105, 138));
            tblListeDevis.setGridColor(new Color(204, 204, 204));
            tblListeDevis.setName("tblListeDevis");
            tblListeDevis.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblListeDevisMouseClicked(e);
              }
            });
            scpListeDevis.setViewportView(tblListeDevis);
          }
          pnlListeDevis.add(scpListeDevis, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tbpListeDocument.addTab("Devis", pnlListeDevis);
        
        // ======== pnlListeCommande ========
        {
          pnlListeCommande.setName("pnlListeCommande");
          pnlListeCommande.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlListeCommande.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlListeCommande.getLayout()).rowHeights = new int[] { 279, 0 };
          ((GridBagLayout) pnlListeCommande.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
          ((GridBagLayout) pnlListeCommande.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== scpListeCommande ========
          {
            scpListeCommande.setPreferredSize(new Dimension(1050, 424));
            scpListeCommande.setFont(new Font("sansserif", Font.PLAIN, 14));
            scpListeCommande.setBackground(Color.white);
            scpListeCommande.setOpaque(true);
            scpListeCommande.setName("scpListeCommande");
            
            // ---- tblListeCommande ----
            tblListeCommande.setShowVerticalLines(true);
            tblListeCommande.setShowHorizontalLines(true);
            tblListeCommande.setBackground(Color.white);
            tblListeCommande.setRowHeight(20);
            tblListeCommande.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            tblListeCommande.setAutoCreateRowSorter(true);
            tblListeCommande.setSelectionBackground(new Color(57, 105, 138));
            tblListeCommande.setGridColor(new Color(204, 204, 204));
            tblListeCommande.setName("tblListeCommande");
            tblListeCommande.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                tblListeCommandeMouseClicked(e);
              }
            });
            scpListeCommande.setViewportView(tblListeCommande);
          }
          pnlListeCommande.add(scpListeCommande, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        tbpListeDocument.addTab("Commandes", pnlListeCommande);
      }
      pnlContenu.add(tbpListeDocument,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanel pnlFiltre;
  private SNPanel pnlFiltreGauche;
  private SNLabelChamp lbRechercheClient;
  private SNClient snClient;
  private SNLabelChamp lbNumeroDocument;
  private SNIdentifiant tfNumeroDocument;
  private SNPanel pnlFiltreDroite;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JTabbedPane tbpListeDocument;
  private SNPanel pnlListeDevis;
  private JScrollPane scpListeDevis;
  private NRiTable tblListeDevis;
  private SNPanel pnlListeCommande;
  private JScrollPane scpListeCommande;
  private NRiTable tblListeCommande;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
