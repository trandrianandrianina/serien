/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;

import javax.swing.ButtonGroup;
import javax.swing.JCheckBox;
import javax.swing.SwingConstants;

import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.commun.sncodepostalcommune.SNCodePostalCommune;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet livraison de la gestion des achats.
 */
public class VueOngletLivraison extends AbstractVuePanel<ModeleAchat> {
  /**
   * Constructeur.
   */
  public VueOngletLivraison(ModeleAchat pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setBackground(SNCharteGraphique.COULEUR_FOND);
    
    // Formatage des zones de saisie (TODO à faire celles qui restent + mettre des constantes) des problèmes de focus
    tfReferenceInterne.setLongueur(DocumentAchat.LONGUEUR_REFERENCE_INTERNE);
    tfReferenceCommande.setLongueur(DocumentAchat.LONGUEUR_REFERENCE_COMMANDE);
    
    tfNomLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfComplementNomLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfRueLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    tfLocalisationLivraison.setLongueur(Client.TAILLE_ZONE_NOM);
    
    // Configuration de la barre de bouton
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichissement de l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirModeLivraison();
    rafraichirReferenceCommande();
    rafraichirReferenceInterne();
    rafraichirDateTraitementDocument();
    rafraichirDateLivraisonPrevueDocument();
    rafraichirAdresseLivraison();
    
    // Raccourcis clavier
    rbModeEnlevement.setMnemonic(KeyEvent.VK_E);
    rbModeLivraison.setMnemonic(KeyEvent.VK_L);
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichissement du mode de livraison.
   */
  private void rafraichirModeLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null) {
      rbModeLivraison.setSelected(documentAchatEnCours.isModeLivraison());
      rbModeEnlevement.setSelected(documentAchatEnCours.isModeEnlevement());
      if (documentAchatEnCours.isDirectUsine()) {
        ckDirectUsine.setSelected(true);
        ckDirectUsine.setEnabled(false);
        ckDirectUsine.setVisible(true);
      }
      else {
        ckDirectUsine.setSelected(false);
        ckDirectUsine.setVisible(false);
      }
    }
    else {
      ckDirectUsine.setSelected(false);
      ckDirectUsine.setVisible(false);
    }
  }
  
  /**
   * Rafraichissement de la référence de commande.
   */
  private void rafraichirReferenceCommande() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getReferenceCommande() != null) {
      tfReferenceCommande.setText(documentAchatEnCours.getReferenceCommande());
    }
    else {
      tfReferenceCommande.setText("");
    }
  }
  
  /**
   * Rafraichissement de la référence interne.
   */
  private void rafraichirReferenceInterne() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getReferenceInterne() != null) {
      tfReferenceInterne.setText(documentAchatEnCours.getReferenceInterne());
    }
    else {
      tfReferenceInterne.setText("");
    }
  }
  
  /**
   * Rafraichissement de la date de traitement.
   */
  private void rafraichirDateTraitementDocument() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null) {
      snDateTraitementDocument.setDate(documentAchatEnCours.getDateDernierTraitement());
    }
    else {
      snDateTraitementDocument.setDate(null);
    }
  }
  
  /**
   * Rafraichissement de la date de livraison prévue.
   */
  private void rafraichirDateLivraisonPrevueDocument() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null) {
      snDateLivraison.setDate(documentAchatEnCours.getDateLivraisonPrevue());
    }
    else {
      snDateLivraison.setDate(null);
    }
  }
  
  /**
   * Rafraichissement de l'adresse de livraison.
   */
  private void rafraichirAdresseLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    // S'il s'agit d'une commande direct usine on affiche toujours les coordonnées du client
    if (documentAchatEnCours != null && documentAchatEnCours.isDirectUsine()) {
      pnAdresseLivraison.setVisible(true);
      
      // En mode enlèvement on met en titre du bloc adresse: "Enlevèe par"
      if (documentAchatEnCours.isModeEnlevement()) {
        pnAdresseLivraison.setTitre("Enlevée par");
      }
      // En mode livraison on met en titre du bloc adresse: "Livrée à"
      else {
        pnAdresseLivraison.setTitre("Livrée à");
      }
      
      // Rafraichir les éléments qui constituent le bloc adresse
      rafraichirNomLivraison();
      rafraichirComplementNomLivraison();
      rafraichirRueLivraison();
      rafraichirLocalisationLivraison();
      rafraichirCodePostalVilleLivraison();
    }
    // S'il s'agit d'une commande normale on n'affiche pas les coordonnées du client
    else {
      pnAdresseLivraison.setVisible(false);
    }
  }
  
  /**
   * Rafraichissement du nom.
   */
  private void rafraichirNomLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getAdresseLivraison() != null) {
      tfNomLivraison.setText(documentAchatEnCours.getAdresseLivraison().getNom());
    }
    else {
      tfNomLivraison.setText("");
    }
  }
  
  /**
   * Rafraichissement du complément de nom.
   */
  private void rafraichirComplementNomLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getAdresseLivraison() != null) {
      tfComplementNomLivraison.setText(documentAchatEnCours.getAdresseLivraison().getComplementNom());
    }
    else {
      tfComplementNomLivraison.setText("");
    }
  }
  
  /**
   * Rafraichissement de la rue.
   */
  private void rafraichirRueLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getAdresseLivraison() != null) {
      tfRueLivraison.setText(documentAchatEnCours.getAdresseLivraison().getRue());
    }
    else {
      tfRueLivraison.setText("");
    }
  }
  
  /**
   * Rafraichissement de la localisation.
   */
  private void rafraichirLocalisationLivraison() {
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours != null && documentAchatEnCours.getAdresseLivraison() != null) {
      tfLocalisationLivraison.setText(documentAchatEnCours.getAdresseLivraison().getLocalisation());
    }
    else {
      tfLocalisationLivraison.setText("");
    }
  }
  
  /**
   * Rafraichissement du code postal et de la ville.
   */
  private void rafraichirCodePostalVilleLivraison() {
    snCodePostalCommune.setEnabled(getModele().isDonneesChargees());
    snCodePostalCommune.setSession(getModele().getSession());
    DocumentAchat documentAchatEnCours = getModele().getDocumentAchatEnCours();
    if (documentAchatEnCours == null) {
      snCodePostalCommune.setSelection(null);
      return;
    }
    
    snCodePostalCommune.setIdEtablissement(documentAchatEnCours.getId().getIdEtablissement());
    Adresse adresse = documentAchatEnCours.getAdresseLivraison();
    if (adresse != null) {
      CodePostalCommune codePostalCommune = CodePostalCommune.getInstance(adresse.getCodePostalFormate(), adresse.getVille());
      snCodePostalCommune.charger(false);
      snCodePostalCommune.setSelection(codePostalCommune);
    }
    else {
      snCodePostalCommune.setSelection(null);
    }
  }
  
  // -- Méthodes évènementielles
  
  /**
   * 
   * Traiter le clic sur un bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        getModele().validerOngletLivraison();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().annulerDocument();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton d'option "Mode enlèvement."
   * 
   * @param e
   */
  private void rbModeEnlevementItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierModeEnlevement(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton d'option "Mode livraison."
   * 
   * @param e
   */
  private void rbModeLivraisonItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        getModele().modifierModeEnlevement(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du composant de sélection de référence commande.
   * 
   * @param e
   */
  private void snReferenceCommandeFocusLost(FocusEvent e) {
    try {
      getModele().modifierReferenceCommande(tfReferenceCommande.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de référence interne.
   * 
   * @param e
   */
  private void tfReferenceInterneFocusLost(FocusEvent e) {
    try {
      getModele().modifierReferenceInterne(tfReferenceInterne.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la date de traitement.
   * 
   * @param e
   */
  private void snDateTraitementDocumentValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateTraitementDocument(snDateTraitementDocument.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la date de livraison.
   * 
   * @param e
   */
  private void snDateLivraisonValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateLivraison(snDateLivraison.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du nom pour la livraison.
   * 
   * @param e
   */
  private void tfNomLivraisonFocusLost(FocusEvent e) {
    try {
      getModele().modifierNomLivraison(tfNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du complément de nom pour la livraison.
   * 
   * @param e
   */
  private void tfComplementNomLivraisonFocusLost(FocusEvent e) {
    try {
      getModele().modifierComplementNomLivraison(tfComplementNomLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de la rue pour la livraison.
   * 
   * @param e
   */
  private void tfRueLivraisonFocusLost(FocusEvent e) {
    try {
      getModele().modifierRueLivraison(tfRueLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de la localisation pour la livraison.
   * 
   * @param e
   */
  private void tfLocalisationLivraisonFocusLost(FocusEvent e) {
    try {
      getModele().modifierLocalisationLivraison(tfLocalisationLivraison.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification du code postal/commune sélectionné pour la livraison.
   * 
   * @param e
   */
  private void snCodePostalCommuneValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      // Il y a un problème de rafraichissement dans certains cas qui n'est pas intercepté par la méthode isEvenementsActifs()
      if (!pnAdresseLivraison.isVisible()) {
        return;
      }
      getModele().modifierCodePostalCommune(snCodePostalCommune.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlModeLivraison = new SNPanelTitre();
    rbModeEnlevement = new SNRadioButton();
    rbModeLivraison = new SNRadioButton();
    ckDirectUsine = new JCheckBox();
    pnlInformationsDocument = new SNPanelTitre();
    lbReferenceInterne = new SNLabelChamp();
    tfReferenceInterne = new SNTexte();
    lbReferenceCommande = new SNLabelChamp();
    tfReferenceCommande = new SNTexte();
    lbDateTraitementDocument = new SNLabelChamp();
    snDateTraitementDocument = new SNDate();
    lbDateDateLivraisonDocument = new SNLabelChamp();
    snDateLivraison = new SNDate();
    pnAdresseLivraison = new SNPanelTitre();
    lbNomLivraison = new SNLabelChamp();
    tfNomLivraison = new SNTexte();
    lbComplementNomLivraison = new SNLabelChamp();
    tfComplementNomLivraison = new SNTexte();
    lbLocalisationLivraison = new SNLabelChamp();
    tfRueLivraison = new SNTexte();
    lbRueLivraison = new SNLabelChamp();
    tfLocalisationLivraison = new SNTexte();
    lbCommune = new SNLabelChamp();
    snCodePostalCommune = new SNCodePostalCommune();
    snBarreBouton = new SNBarreBouton();
    btgMode = new ButtonGroup();
    
    // ======== this ========
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 138, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlModeLivraison ========
      {
        pnlModeLivraison.setTitre("Mode de livraison");
        pnlModeLivraison.setName("pnlModeLivraison");
        pnlModeLivraison.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlModeLivraison.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlModeLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlModeLivraison.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlModeLivraison.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- rbModeEnlevement ----
        rbModeEnlevement.setText("Enl\u00e8vement");
        rbModeEnlevement.setPreferredSize(new Dimension(150, 30));
        rbModeEnlevement.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbModeEnlevement.setName("rbModeEnlevement");
        rbModeEnlevement.addItemListener(e -> rbModeEnlevementItemStateChanged(e));
        pnlModeLivraison.add(rbModeEnlevement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(5, 20, 5, 0), 0, 0));
        
        // ---- rbModeLivraison ----
        rbModeLivraison.setText("Livraison");
        rbModeLivraison.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        rbModeLivraison.setName("rbModeLivraison");
        rbModeLivraison.addItemListener(e -> rbModeLivraisonItemStateChanged(e));
        pnlModeLivraison.add(rbModeLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 20, 5, 0), 0, 0));
        
        // ---- ckDirectUsine ----
        ckDirectUsine.setText("<html>Direct <u>u</u>sine</html>");
        ckDirectUsine.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckDirectUsine.setFont(ckDirectUsine.getFont().deriveFont(ckDirectUsine.getFont().getSize() + 2f));
        ckDirectUsine.setPreferredSize(new Dimension(97, 30));
        ckDirectUsine.setName("ckDirectUsine");
        pnlModeLivraison.add(ckDirectUsine, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 20, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlModeLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlInformationsDocument ========
      {
        pnlInformationsDocument.setTitre("Informations sur le document");
        pnlInformationsDocument.setName("pnlInformationsDocument");
        pnlInformationsDocument.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlInformationsDocument.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlInformationsDocument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlInformationsDocument.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlInformationsDocument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbReferenceInterne ----
        lbReferenceInterne.setText("R\u00e9f\u00e9rence interne");
        lbReferenceInterne.setName("lbReferenceInterne");
        pnlInformationsDocument.add(lbReferenceInterne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfReferenceInterne ----
        tfReferenceInterne.setName("tfReferenceInterne");
        tfReferenceInterne.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfReferenceInterneFocusLost(e);
          }
        });
        pnlInformationsDocument.add(tfReferenceInterne, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbReferenceCommande ----
        lbReferenceCommande.setText("R\u00e9f\u00e9rence commande du fournissseur");
        lbReferenceCommande.setMaximumSize(new Dimension(250, 30));
        lbReferenceCommande.setMinimumSize(new Dimension(250, 30));
        lbReferenceCommande.setPreferredSize(new Dimension(250, 30));
        lbReferenceCommande.setName("lbReferenceCommande");
        pnlInformationsDocument.add(lbReferenceCommande, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfReferenceCommande ----
        tfReferenceCommande.setPreferredSize(new Dimension(114, 31));
        tfReferenceCommande.setMinimumSize(new Dimension(114, 31));
        tfReferenceCommande.setName("tfReferenceCommande");
        tfReferenceCommande.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            snReferenceCommandeFocusLost(e);
          }
        });
        pnlInformationsDocument.add(tfReferenceCommande, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateTraitementDocument ----
        lbDateTraitementDocument.setText("Date du document");
        lbDateTraitementDocument.setName("lbDateTraitementDocument");
        pnlInformationsDocument.add(lbDateTraitementDocument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snDateTraitementDocument ----
        snDateTraitementDocument.setName("snDateTraitementDocument");
        snDateTraitementDocument.addSNComposantListener(e -> snDateTraitementDocumentValueChanged(e));
        pnlInformationsDocument.add(snDateTraitementDocument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbDateDateLivraisonDocument ----
        lbDateDateLivraisonDocument.setText("Date de livraison");
        lbDateDateLivraisonDocument.setName("lbDateDateLivraisonDocument");
        pnlInformationsDocument.add(lbDateDateLivraisonDocument, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snDateLivraison ----
        snDateLivraison.setName("snDateLivraison");
        snDateLivraison.addSNComposantListener(e -> snDateLivraisonValueChanged(e));
        pnlInformationsDocument.add(snDateLivraison, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlInformationsDocument,
          new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnAdresseLivraison ========
      {
        pnAdresseLivraison.setTitre("Adresse de livraison");
        pnAdresseLivraison.setName("pnAdresseLivraison");
        pnAdresseLivraison.setLayout(new GridBagLayout());
        ((GridBagLayout) pnAdresseLivraison.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnAdresseLivraison.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnAdresseLivraison.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnAdresseLivraison.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbNomLivraison ----
        lbNomLivraison.setText("Raison sociale");
        lbNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbNomLivraison.setName("lbNomLivraison");
        pnAdresseLivraison.add(lbNomLivraison, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfNomLivraison ----
        tfNomLivraison.setBackground(Color.white);
        tfNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfNomLivraison.setMinimumSize(new Dimension(485, 30));
        tfNomLivraison.setPreferredSize(new Dimension(485, 30));
        tfNomLivraison.setName("tfNomLivraison");
        tfNomLivraison.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfNomLivraisonFocusLost(e);
          }
        });
        pnAdresseLivraison.add(tfNomLivraison, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbComplementNomLivraison ----
        lbComplementNomLivraison.setText("Compl\u00e9ment");
        lbComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbComplementNomLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbComplementNomLivraison.setName("lbComplementNomLivraison");
        pnAdresseLivraison.add(lbComplementNomLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfComplementNomLivraison ----
        tfComplementNomLivraison.setBackground(Color.white);
        tfComplementNomLivraison.setMinimumSize(new Dimension(485, 30));
        tfComplementNomLivraison.setPreferredSize(new Dimension(485, 30));
        tfComplementNomLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfComplementNomLivraison.setName("tfComplementNomLivraison");
        tfComplementNomLivraison.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfComplementNomLivraisonFocusLost(e);
          }
        });
        pnAdresseLivraison.add(tfComplementNomLivraison, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbLocalisationLivraison ----
        lbLocalisationLivraison.setText("Adresse 1");
        lbLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbLocalisationLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLocalisationLivraison.setName("lbLocalisationLivraison");
        pnAdresseLivraison.add(lbLocalisationLivraison, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfRueLivraison ----
        tfRueLivraison.setBackground(Color.white);
        tfRueLivraison.setMinimumSize(new Dimension(485, 30));
        tfRueLivraison.setPreferredSize(new Dimension(485, 30));
        tfRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfRueLivraison.setName("tfRueLivraison");
        tfRueLivraison.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfRueLivraisonFocusLost(e);
          }
        });
        pnAdresseLivraison.add(tfRueLivraison, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbRueLivraison ----
        lbRueLivraison.setText("Adresse 2");
        lbRueLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbRueLivraison.setHorizontalAlignment(SwingConstants.RIGHT);
        lbRueLivraison.setName("lbRueLivraison");
        pnAdresseLivraison.add(lbRueLivraison, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLocalisationLivraison ----
        tfLocalisationLivraison.setBackground(Color.white);
        tfLocalisationLivraison.setMinimumSize(new Dimension(485, 30));
        tfLocalisationLivraison.setPreferredSize(new Dimension(485, 30));
        tfLocalisationLivraison.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfLocalisationLivraison.setName("tfLocalisationLivraison");
        tfLocalisationLivraison.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLocalisationLivraisonFocusLost(e);
          }
        });
        pnAdresseLivraison.add(tfLocalisationLivraison, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbCommune ----
        lbCommune.setText("Commune");
        lbCommune.setFont(new Font("sansserif", Font.PLAIN, 14));
        lbCommune.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCommune.setName("lbCommune");
        pnAdresseLivraison.add(lbCommune, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snCodePostalCommune ----
        snCodePostalCommune.setName("snCodePostalCommune");
        snCodePostalCommune.addSNComposantListener(e -> snCodePostalCommuneValueChanged(e));
        pnAdresseLivraison.add(snCodePostalCommune, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnAdresseLivraison,
          new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- btgMode ----
    btgMode.add(rbModeEnlevement);
    btgMode.add(rbModeLivraison);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlModeLivraison;
  private SNRadioButton rbModeEnlevement;
  private SNRadioButton rbModeLivraison;
  private JCheckBox ckDirectUsine;
  private SNPanelTitre pnlInformationsDocument;
  private SNLabelChamp lbReferenceInterne;
  private SNTexte tfReferenceInterne;
  private SNLabelChamp lbReferenceCommande;
  private SNTexte tfReferenceCommande;
  private SNLabelChamp lbDateTraitementDocument;
  private SNDate snDateTraitementDocument;
  private SNLabelChamp lbDateDateLivraisonDocument;
  private SNDate snDateLivraison;
  private SNPanelTitre pnAdresseLivraison;
  private SNLabelChamp lbNomLivraison;
  private SNTexte tfNomLivraison;
  private SNLabelChamp lbComplementNomLivraison;
  private SNTexte tfComplementNomLivraison;
  private SNLabelChamp lbLocalisationLivraison;
  private SNTexte tfRueLivraison;
  private SNLabelChamp lbRueLivraison;
  private SNTexte tfLocalisationLivraison;
  private SNLabelChamp lbCommune;
  private SNCodePostalCommune snCodePostalCommune;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup btgMode;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
