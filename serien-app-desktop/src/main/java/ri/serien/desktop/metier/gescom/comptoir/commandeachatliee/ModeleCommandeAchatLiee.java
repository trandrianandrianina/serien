/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.commandeachatliee;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Expérimentation d'un modèle plus propre de modèle/vue - 10/10/2017
 * Ce dernière amène une structure un peu plus "rigide" et sutout le modèle n'implémente plus de classe Swing,
 * il est totalement indépendant de la vue.
 */
public class ModeleCommandeAchatLiee extends AbstractModeleDialogue {
  
  private IdDocumentVente idDocumentVente = null;
  private ListeDocumentAchat listeDocumentAchat = null;
  
  /**
   * Constructeur.
   */
  public ModeleCommandeAchatLiee(SessionBase pSession, IdDocumentVente pIdDocumentVente) {
    super(pSession);
    idDocumentVente = pIdDocumentVente;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    
  }
  
  @Override
  public void chargerDonnees() {
    listeDocumentAchat = recupererListeCommandeAchatLiee(idDocumentVente);
    if (listeDocumentAchat == null) {
      quitterAvecAnnulation();
    }
  }
  
  /**
   * Permet de sélectionner des documents d'achats dans la liste de documents d'achats
   */
  public void selectionnerCommandeAchat(ArrayList<Integer> pListeIndicesSelectionnes) {
    // Si aucun document d'achat généré on sort
    if (listeDocumentAchat == null || listeDocumentAchat.size() == 0) {
      return;
    }
    
    // Si aucun document sélectionné on déssélectionne tous les documents d'achats et on sort
    if (pListeIndicesSelectionnes == null || pListeIndicesSelectionnes.size() == 0) {
      listeDocumentAchat.deselectionnerTout();
    }
    // On contrôle si le nombre de documents sélectionnés ne dépasse pas le nombre maximal de sessions que l'on peut ouvrir dans le client
    else if (!ManagerSessionClient.getInstance().verifierNombreSessionDisponible(pListeIndicesSelectionnes.size())) {
      listeDocumentAchat.deselectionnerTout();
      rafraichir();
      throw new MessageErreurException("Vous ne pouvez pas ouvrir plus de " + ManagerSessionClient.NOMBRE_MAX_SESSION
          + " points de menu.\nMerci de sélectionner moins de documents d'achats."
          + "\nVous pouvez retrouver l'ensemble de vos documents non traités par le point de menu des achats.");
    }
    // On rafraichi la liste des éléments sélectionnés dans la liste des documents d'achats
    else {
      listeDocumentAchat.selectionner(pListeIndicesSelectionnes);
    }
    
    rafraichir();
  }
  
  /**
   * Sélectionner les indices de tous les documents affichés
   **/
  public void selectionnerTout() {
    listeDocumentAchat.selectionnerTout();
    rafraichir();
  }
  
  /**
   * Quitter avec annulation
   */
  @Override
  public void quitterAvecAnnulation() {
    selectionnerCommandeAchat(null);
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes privées
  
  /**
   * Retourner la liste des documents d'achats liés
   */
  private ListeDocumentAchat recupererListeCommandeAchatLiee(IdDocumentVente idDocumentVente) {
    if (idDocumentVente == null) {
      return null;
    }
    return ManagerServiceDocumentAchat.recupererCommandeAchatliee(getSession().getIdSession(), idDocumentVente);
  }
  
  // -- Accesseurs
  /**
   * Retourner la liste des documents d'achats générés à partir du document de ventes
   */
  public ListeDocumentAchat getListeDocumentAchat() {
    return listeDocumentAchat;
  }
  
  /**
   * Retourner la liste des incides de documents d'achats sélectionnés dans la liste de documents d'achats générés
   * null signifie qu'aucun document n'est sélectionné
   */
  public List<Integer> getListeIndicesDocumentSelectionne() {
    if (listeDocumentAchat == null) {
      return null;
    }
    return listeDocumentAchat.getListeIndexSelection();
  }
  
  /**
   * Retourner la liste des documents d'achats sélectionnés
   */
  public ListeDocumentAchat getListeDocumentAchatSelectionnes() {
    if (listeDocumentAchat == null) {
      return null;
    }
    // On transfere la liste sélectionnée sous forme de List<DocumentAchat> en ListeDocumentAchat
    ListeDocumentAchat listeSelectionne = new ListeDocumentAchat();
    listeSelectionne.addAll(listeDocumentAchat.getListeSelection());
    
    return listeSelectionne;
  }
}
