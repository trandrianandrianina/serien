/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.carnetboncour;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JScrollPane;

import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran principal des carnets de bon de cour.
 */
public class VueCarnetBonCour extends AbstractVuePanel<ModeleCarnetBonCour> {
  private VueListeCarnetBonCour vueListe;
  private VueDetailCarnetBonCour vueDetail;
  
  /**
   * Constructeur.
   */
  public VueCarnetBonCour(ModeleCarnetBonCour pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de titre
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    pnlBpresentation.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    
    // Ajouter la vue liste
    vueListe = new VueListeCarnetBonCour(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.LISTE, vueListe);
    pnlContenu.add(vueListe, EnumCleVueListeDetail.LISTE.getLibelle());
    
    // Ajouter la vue détail
    vueDetail = new VueDetailCarnetBonCour(getModele());
    ajouterVueEnfant(EnumCleVueListeDetail.DETAIL, vueDetail);
    pnlContenu.add(vueDetail, EnumCleVueListeDetail.DETAIL.getLibelle());
  }
  
  @Override
  public void rafraichir() {
    rafraichirTitre();
    rafraichirVue();
  }
  
  private void rafraichirTitre() {
    if (getModele().getTitreEcran() != null) {
      pnlBpresentation.setText(getModele().getTitreEcran());
    }
    else {
      pnlBpresentation.setText("");
    }
  }
  
  private void rafraichirVue() {
    InterfaceCleVue interfaceCleVue = getModele().getCleVueEnfantActive();
    if (interfaceCleVue != null) {
      CardLayout cardLayout = (CardLayout) (pnlContenu.getLayout());
      cardLayout.show(pnlContenu, interfaceCleVue.getLibelle());
    }
  }
  
  // -- Méthodes évènementielles
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlBpresentation = new SNBandeauTitre();
    scpContenu = new JScrollPane();
    pnlContenu = new SNPanelFond();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 780));
    setPreferredSize(new Dimension(1024, 780));
    setOpaque(false);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Carnet des bons de cour");
      pnlBpresentation.setName("pnlBpresentation");
      pnlPrincipal.add(pnlBpresentation, BorderLayout.NORTH);
      
      // ======== scpContenu ========
      {
        scpContenu.setPreferredSize(new Dimension(1024, 650));
        scpContenu.setBorder(null);
        scpContenu.setName("scpContenu");
        
        // ======== pnlContenu ========
        {
          pnlContenu.setPreferredSize(new Dimension(1024, 650));
          pnlContenu.setForeground(Color.black);
          pnlContenu.setMinimumSize(new Dimension(1200, 700));
          pnlContenu.setName("pnlContenu");
          pnlContenu.setLayout(new CardLayout());
        }
        scpContenu.setViewportView(pnlContenu);
      }
      pnlPrincipal.add(scpContenu, BorderLayout.CENTER);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNBandeauTitre pnlBpresentation;
  private JScrollPane scpContenu;
  private SNPanelFond pnlContenu;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
