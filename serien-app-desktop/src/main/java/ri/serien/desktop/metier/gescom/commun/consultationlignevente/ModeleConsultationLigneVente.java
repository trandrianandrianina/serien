/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.commun.consultationlignevente;

import java.math.BigDecimal;
import java.util.Date;

import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue qui permet de consulter le détail d'une ligne de vente.
 */
public class ModeleConsultationLigneVente extends AbstractModeleDialogue {
  // Variables
  private DocumentVente documentOrigine = null;
  private LigneVente ligneVente = null;
  private ListeUnite listeUnite = null;
  
  // Variables de travail
  private boolean modeNegoce = true;
  private BigDecimal quantiteUCV = BigDecimal.ZERO;
  private IdUnite idUniteC = null;
  private BigDecimal quantiteUV = BigDecimal.ZERO;
  private IdUnite idUniteV = null;
  private String libelle = "";
  private IdArticle idArticle = null;
  private Fournisseur fournisseur = null;
  private ConditionAchat conditionAchatLigne = null;
  private BigDecimal indiceDeMarge = null;
  private BigDecimal tauxDeMarque = null;
  
  // Liens documents
  private LienLigne lienOrigine = null;
  
  private boolean liensAffichables = false;
  private boolean affichageTTC = false;
  
  /**
   * Constructeur.
   */
  public ModeleConsultationLigneVente(SessionBase pSession, DocumentVente pDocumentOrigine, LigneVente pLigneVente) {
    super(pSession);
    documentOrigine = pDocumentOrigine;
    ligneVente = pLigneVente;
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    effacerVariables();
    chargerVariables();
    
    // Calculer l'indice de marge
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(Constantes.DEUX_DECIMALES);
    // Calculer le taux de marque
    tauxDeMarque = ligneVente.calculerTauxDeMarque(Constantes.DEUX_DECIMALES);
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise les variables du modèle.
   */
  private void chargerVariables() {
    // Déterminer le mode de calcul de prix du comptoir (négoce ou classique)
    modeNegoce = ManagerSessionClient.getInstance().isParametreSystemeActif(getSession().getIdSession(), EnumParametreSysteme.PS287);
    
    // liste des unités
    if (listeUnite == null) {
      listeUnite = new ListeUnite();
      listeUnite = listeUnite.charger(getIdSession());
    }
    
    // Définir si le client est TTC ou HT
    if (documentOrigine != null) {
      affichageTTC = false;
      ligneVente.setClientTTC(documentOrigine.isTTC());
    }
    
    setQuantiteUCV(ligneVente.getQuantiteUCV());
    idUniteC = ligneVente.getIdUniteConditionnementVente();
    setQuantiteUV(ligneVente.getQuantiteUV());
    idUniteV = ligneVente.getIdUniteVente();
    libelle = ligneVente.getLibelle();
    idArticle = ligneVente.getIdArticle();
    
    liensAffichables = (!documentOrigine.isDevis());
    if (liensAffichables) {
      if (!documentOrigine.isCharge()) {
        documentOrigine = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), documentOrigine.getId());
      }
      lienOrigine = new LienLigne(IdLienLigne.getInstanceVente(documentOrigine.getId().getIdEtablissement(),
          EnumTypeLienLigne.LIEN_LIGNE_VENTE, documentOrigine.getId().getEntete(), ligneVente.getId().getNumero(),
          ligneVente.getId().getSuffixe(), documentOrigine.getId().getNumeroFacture(), ligneVente.getId().getNumeroLigne(),
          documentOrigine.getId(), ligneVente.getId().getNumeroLigne(), false));
      lienOrigine.setDocumentVente(documentOrigine);
      lienOrigine.setLigne(ligneVente);
    }
    
    Article article = new Article(ligneVente.getIdArticle());
    article = ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
    if (fournisseur == null && article.getIdFournisseur() != null) {
      fournisseur = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), article.getIdFournisseur());
    }
    
    // Charger la condition d'achats associées à la ligne
    if (fournisseur != null && conditionAchatLigne == null) {
      conditionAchatLigne = ManagerServiceDocumentVente.chargerConditionAchat(getIdSession(), ligneVente);
      if (conditionAchatLigne == null) {
        // Si on ne trouve pas de condition d'achats à la ligne, on charge la condition d'achat de l'article
        conditionAchatLigne = ManagerServiceArticle.chargerConditionAchat(getIdSession(), ligneVente.getIdArticle(), new Date());
        if (conditionAchatLigne == null) {
          throw new MessageErreurException("Impossible de charger l'article car il ne possède pas de condition d'achats.");
        }
        
        // Associer la condition d'achats pour la ligne du document de ventes
        conditionAchatLigne.setIdEtablissement(documentOrigine.getId().getIdEtablissement());
        conditionAchatLigne.setCodeERL(documentOrigine.getId().getCodeEntete().getCode());
        conditionAchatLigne.setNumeroBon(documentOrigine.getId().getNumero());
        conditionAchatLigne.setSuffixeBon(documentOrigine.getId().getSuffixe());
        conditionAchatLigne.setNumeroLigne(ligneVente.getId().getNumeroLigne());
        conditionAchatLigne.setIdArticle(ligneVente.getIdArticle());
        conditionAchatLigne.setIdFournisseur(fournisseur.getId());
      }
    }
  }
  
  /**
   * Initialise les variables du modèle.
   */
  private void effacerVariables() {
    setQuantiteUCV(BigDecimal.ZERO);
    idUniteC = null;
    setQuantiteUV(BigDecimal.ZERO);
    idUniteV = null;
    libelle = "";
    idArticle = null;
  }
  
  /**
   * Lecture de la sécurité sur le droit à négocier les achats avec les frais d'exploitation.
   */
  public boolean isAutoriseFraisExploitation() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_NEGOCIATION_ACHAT_AVEC_FRAIS_EXPLOITATION);
  }
  
  /**
   * Lecture de la sécurité sur la visualisation des marges
   */
  public boolean isAutoriseVisualiserMarge() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_VISUALISATION_MARGES);
  }
  
  // -- Accesseurs
  
  public boolean isAffichageTTC() {
    return affichageTTC;
  }
  
  /**
   * Ligne de ventes.
   */
  public LigneVente getLigneVente() {
    return ligneVente;
  }
  
  public void setQuantiteUCV(BigDecimal pQuantiteUCV) {
    quantiteUCV = pQuantiteUCV;
  }
  
  public BigDecimal getQuantiteUCV() {
    return quantiteUCV;
  }
  
  public IdUnite getIdUCV() {
    return idUniteC;
  }
  
  public void setQuantiteUV(BigDecimal pQuantiteUV) {
    quantiteUV = pQuantiteUV;
  }
  
  public BigDecimal getQuantiteUV() {
    return quantiteUV;
  }
  
  public IdUnite getIdUniteV() {
    return idUniteV;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public IdArticle getIdArticle() {
    return idArticle;
  }
  
  public boolean isLiensAffichables() {
    return liensAffichables;
  }
  
  public LienLigne getLienOrigine() {
    return lienOrigine;
  }
  
  public ConditionAchat getConditionAchatLigne() {
    return conditionAchatLigne;
  }
  
  /**
   * Retourner l'indice de marge.
   * @return L'indice de marge.
   */
  public BigDecimal getIndiceDeMarge() {
    return indiceDeMarge;
  }
  
  /**
   * Retourner le taux de marque.
   * @return Le taux de marque.
   */
  public BigDecimal getTauxDeMarque() {
    return tauxDeMarque;
  }
  
  /**
   * Retourner le mode de calcul de prix en cours (Négoce ou Classique)
   * @return
   */
  public boolean isModeNegoce() {
    return modeNegoce;
  }
}
