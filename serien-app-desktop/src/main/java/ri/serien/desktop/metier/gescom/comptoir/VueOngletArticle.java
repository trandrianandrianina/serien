/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.math.BigDecimal;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.desktop.metier.gescom.comptoir.detailligne.ModeleDetailLigneArticle;
import ri.serien.libcommun.gescom.commun.article.EnumFiltreHorsGamme;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ZoneGeographique;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet article du comptoir.
 */
public class VueOngletArticle extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final String BOUTON_CONTROLER_ENCOURS = "Contrôler l'encours";
  private static final String BOUTON_REGLER_COMPTANT = "Régler comptant";
  private static final String BOUTON_NEGOCIER_TOTAL = "Négocier le total";
  private static final String BOUTON_GROUPER = "Grouper les lignes";
  private static final String BOUTON_MODIFIER_GROUPE = "Modifier le groupe";
  private static final String BOUTON_AJOUTER_ARTICLE_SPECIAL = "Ajouter un article spécial";
  private static final String BOUTON_AJOUTER_COMMENTAIRE = "Ajouter un commentaire";
  private static final String BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE = "Rechercher un commentaire";
  private static final String BOUTON_RECHERCHER_ARTICLE_PALETTE = "Rechercher une palette";
  private static final String BOUTON_RECHERCHER_ARTICLE_TRANSPORT = "Rechercher un transport";
  private static final String BOUTON_RECHERCHER_ARTICLE_STANDARD = "Rechercher un article";
  private static final String BOUTON_AJOUTER_BON_COUR = "Ajouter un bon de cour";
  private static final String BOUTON_IMPORTER_PLAN_DE_POSE_SIPE = "Importer articles plan de pose Sipe";
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_STANDARD_HT =
      new String[] { "Qt\u00e9", "UCV", "Article", "Disponible", "Prix HT", "UV", "UV/UC", "Origine", "Observations" };
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_STANDARD_TTC =
      new String[] { "Qt\u00e9", "UCV", "Article", "Disponible", "Prix TTC", "UV", "UV/UC", "Origine", "Observations" };
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_COMMENTAIRE =
      new String[] { "Choisir", "Code", "Texte du commentaire" };
  private static final String[] LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_PALETTE =
      new String[] { "Qt\u00e9", "Libell\u00e9", "Stock disponible", "Stock client", "Prix consignation", "Prix d\u00e9consignation" };
  public static final String[] LISTE_TITRE_LIGNE_VENTE_CREATION =
      new String[] { "Qt\u00e9", "UCV", "Article", "Prix HT", "UV", "Origine", "Remise", "Prix net", "Qt\u00e9 en UV", "Montant HT" };
  public static final String[] LISTE_TITRE_LIGNE_VENTE_MODIFICATION = new String[] { "Qt\u00e9", "Qt\u00e9 ori.", "UCV", "Article",
      "Prix HT", "UV", "Origine", "Remise", "Prix net", "Qt\u00e9 en UV", "Montant HT" };
  
  private static final String LIBELLE_RECHERCHE_ARTICLE = "Recherche article";
  private static final String LIBELLE_RECHERCHE_TRANSPORT = "Recherche transport";
  private static final String LIBELLE_RECHERCHE_PALETTE = "Recherche palette";
  private static final String LIBELLE_RECHERCHE_COMMENTAIRE = "Recherche commentaire";
  
  private static final String LIBELLE_RESULTAT_RECHERCHE_ARTICLE = "R\u00e9sultat de la recherche (%d)";
  private static final String LIBELLE_RESULTAT_RECHERCHE_PALETTE = "R\u00e9sultat de la recherche de palettes (%d)";
  private static final String LIBELLE_RESULTAT_RECHERCHE_TRANSPORT = "R\u00e9sultat de la recherche de transports (%d)";
  private static final String LIBELLE_RESULTAT_RECHERCHE_COMMENTAIRE = "R\u00e9sultat de la recherche de commentaires (%d)";
  
  private static final String TITRE_COLONNE_PRIX_TTC = "Prix TTC";
  private static final String TITRE_COLONNE_MONTANT_TTC = "Montant TTC";
  private static final String TITRE_COLONNE_PRIX_HT = "Prix HT";
  private static final String TITRE_COLONNE_MONTANT_HT = "Montant HT";
  
  // Table contenant le résultat de la recherche articles
  private static final int RESULTAT_RECHERCHE_ARTICLES_COLONNE_QUANTITE = 0;
  private static final int RESULTAT_RECHERCHE_ARTICLES_PREMIERE_LIGNE = 0;
  
  private static final int AUCUNE_INSERTION = -2;
  
  // Variables
  private DefaultTableModel tableModelRechercheArticleStandardHT = null;
  private DefaultTableModel tableModelRechercheArticleStandardTTC = null;
  private DefaultTableModel tableModelRechercheArticleCommentaire = null;
  private DefaultTableModel tableModelRechercheArticlePalette = null;
  private ModeleComptoir modele = null;
  private boolean celluleQuantiteModifiee = false;
  protected boolean executerEvenements = false;
  private JTableCellRendererRechercheArticle rechercheArticleRenderer = new JTableCellRendererRechercheArticle();
  private JTableCellRendererRechercheArticleCommentaire rechercheArticleCommentaireRenderer =
      new JTableCellRendererRechercheArticleCommentaire();
  private JTableCellRendererRechercheArticlePalette rechercheArticlePaletteRenderer = new JTableCellRendererRechercheArticlePalette();
  private JTableLignesArticleDocumentRenderer ligneArticleCelluleRenderer = new JTableLignesArticleDocumentRenderer();
  private int indiceLigneInsertion = AUCUNE_INSERTION;
  private boolean colonneQuantiteOrigine = false;
  
  // Classe interne qui permet de gérer les flèches dans la table tblListeRechercheArticles
  class ActionCellule extends AbstractAction {
    // Constantes
    
    // Variables
    private Action actionOriginale = null;
    private boolean gestionFleches = false;
    
    public ActionCellule(String pAction, Action pActionOriginale, boolean pGestionFleches) {
      super(pAction);
      actionOriginale = pActionOriginale;
      gestionFleches = pGestionFleches;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      tblResultatRechercheKeyPressed(e, actionOriginale, gestionFleches);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueOngletArticle(ModeleComptoir acomptoir) {
    modele = acomptoir;
  }
  
  // -- Méthodes publiques
  
  /**
   * Affiche l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Onglet Articles
    scpResultatRecherche.getViewport().setBackground(Color.WHITE);
    tblResultatRecherche.getTableHeader().setFont(new Font("SansSerif", Font.PLAIN, 14));
    tblResultatRecherche.getTableHeader().setBackground(Color.WHITE);
    
    // Une seule ligne peut être sélectionnée dans la tableau résultat
    tblResultatRecherche.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblResultatRecherche.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblResultatRechercheSelectionChanged(e);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpResultatRecherche.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpResultatRechercheStateChanged(e);
      }
    });
    
    tableModelRechercheArticleStandardHT = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_STANDARD_HT) {
      
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    tableModelRechercheArticleStandardTTC = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_STANDARD_TTC) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    tableModelRechercheArticleCommentaire = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_COMMENTAIRE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    tableModelRechercheArticlePalette = new DefaultTableModel(null, LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_PALETTE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex == 0);
      }
    };
    
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblResultatRecherche, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter, false);
    tblResultatRecherche.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblResultatRecherche.getActionMap().put(actionEnter, actionCelluleEnter);
    // Modification de la gestion des évènements sur la touche flèche haut de la jtable
    final Action originalActionHaut = retournerActionComposant(tblResultatRecherche, SNCharteGraphique.TOUCHE_HAUT);
    String actionHaut = "actionHaut";
    ActionCellule actionCelluleHaut = new ActionCellule(actionHaut, originalActionHaut, true);
    tblResultatRecherche.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_HAUT, actionHaut);
    tblResultatRecherche.getActionMap().put(actionHaut, actionCelluleHaut);
    // Modification de la gestion des évènements sur la touche flèche bas de la jtable
    final Action originalActionBas = retournerActionComposant(tblResultatRecherche, SNCharteGraphique.TOUCHE_BAS);
    String actionBas = "actionBas";
    ActionCellule actionCelluleBas = new ActionCellule(actionBas, originalActionBas, true);
    tblResultatRecherche.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_BAS, actionBas);
    tblResultatRecherche.getActionMap().put(actionBas, actionCelluleBas);
    // Modification de la gestion des évènements sur la touche flèche gauche de la jtable
    final Action originalActionGauche = retournerActionComposant(tblResultatRecherche, SNCharteGraphique.TOUCHE_GAUCHE);
    String actionGauche = "actionGauche";
    ActionCellule actionCelluleGauche = new ActionCellule(actionGauche, originalActionGauche, true);
    tblResultatRecherche.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_GAUCHE, actionGauche);
    tblResultatRecherche.getActionMap().put(actionGauche, actionCelluleGauche);
    // Modification de la gestion des évènements sur la touche flèche droite de la jtable
    final Action originalActionDroite = retournerActionComposant(tblResultatRecherche, SNCharteGraphique.TOUCHE_DROITE);
    String actionDroite = "actionDroite";
    ActionCellule actionCelluleDroite = new ActionCellule(actionDroite, originalActionDroite, true);
    tblResultatRecherche.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_DROITE, actionDroite);
    tblResultatRecherche.getActionMap().put(actionDroite, actionCelluleDroite);
    
    // Liste des lignes articles du document
    initialiserListeLigneVente();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CONTROLER_ENCOURS, 'e', false);
    snBarreBouton.ajouterBouton(BOUTON_REGLER_COMPTANT, 'r', false);
    snBarreBouton.ajouterBouton(BOUTON_NEGOCIER_TOTAL, 'n', false);
    snBarreBouton.ajouterBouton(BOUTON_GROUPER, 'g', false);
    snBarreBouton.ajouterBouton(BOUTON_MODIFIER_GROUPE, 'g', false);
    snBarreBouton.ajouterBouton(BOUTON_AJOUTER_ARTICLE_SPECIAL, 's', false);
    snBarreBouton.ajouterBouton(BOUTON_AJOUTER_COMMENTAIRE, 'c', false);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE, 'o', false);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_PALETTE, 'p', false);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_TRANSPORT, 't', false);
    snBarreBouton.ajouterBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_AJOUTER_BON_COUR, 'j', false);
    snBarreBouton.ajouterBouton(BOUTON_IMPORTER_PLAN_DE_POSE_SIPE, 'i', false);
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Initialiser la structure de la liste des lignes de ventes.
   */
  private void initialiserListeLigneVente() {
    if (modele.getDocumentVenteEnCours() == null) {
      return;
    }
    
    boolean toutInitialiser = false;
    
    // On contrôle quel type d'aspect de la table on doit initialiser
    // 0 pas de modification
    // 1 aspect avec colonne origine
    // 2 aspect sans colonne origine
    int aspect = 0;
    if (modele.isAfficherColonneOrigine() && !colonneQuantiteOrigine) {
      aspect = 1;
    }
    else if (!modele.isAfficherColonneOrigine() && colonneQuantiteOrigine) {
      aspect = 2;
    }
    
    // Dans le cas d'une première initialisation
    if (tblLigneVente.getColumnCount() == 0) {
      toutInitialiser = true;
      if (aspect == 0) {
        if (colonneQuantiteOrigine) {
          aspect = 1;
        }
        else {
          aspect = 2;
        }
      }
    }
    // Si ce n'est pas la première utilisation on contrôle le changement du client HT / TTC
    else {
      if (aspect == 0) {
        // Dans le cas où la liste actuelle affiche la colonne quantité d'origine
        if (colonneQuantiteOrigine) {
          // Le client est TTC et les colonnes sont en HT alors on force le rafraichissement des titres de colonnes
          // ou le client est HT est les colonnes sont en TTC
          if ((modele.isClientTTC() && !LISTE_TITRE_LIGNE_VENTE_MODIFICATION[4].equals(TITRE_COLONNE_PRIX_TTC))
              || (!modele.isClientTTC() && !LISTE_TITRE_LIGNE_VENTE_MODIFICATION[4].equals(TITRE_COLONNE_PRIX_HT))) {
            aspect = 1;
          }
        }
        // Sinon la liste n'affiche pas la colonne quantité d'origine
        else {
          // Le client est TTC et les colonnes sont en HT alors on force le rafraichissement des titres de colonnes
          // ou le client est HT est les colonnes sont en TTC
          if ((modele.isClientTTC() && !LISTE_TITRE_LIGNE_VENTE_CREATION[3].equals(TITRE_COLONNE_PRIX_TTC))
              || (!modele.isClientTTC() && !LISTE_TITRE_LIGNE_VENTE_CREATION[3].equals(TITRE_COLONNE_PRIX_HT))) {
            aspect = 2;
          }
        }
      }
    }
    
    // Modification de l'aspect de la table
    scpLigneVente.getViewport().setBackground(Color.WHITE);
    switch (aspect) {
      // Avec la colonne des quantités d'origine
      case 1:
        if (modele.isClientTTC()) {
          LISTE_TITRE_LIGNE_VENTE_MODIFICATION[4] = TITRE_COLONNE_PRIX_TTC;
          LISTE_TITRE_LIGNE_VENTE_MODIFICATION[10] = TITRE_COLONNE_MONTANT_TTC;
        }
        else {
          LISTE_TITRE_LIGNE_VENTE_MODIFICATION[4] = TITRE_COLONNE_PRIX_HT;
          LISTE_TITRE_LIGNE_VENTE_MODIFICATION[10] = TITRE_COLONNE_MONTANT_HT;
        }
        tblLigneVente.personnaliserAspect(LISTE_TITRE_LIGNE_VENTE_MODIFICATION,
            new int[] { 60, 60, 40, 380, 80, 40, 80, 70, 80, 80, 100 }, new int[] { 65, 60, 40, -1, 80, 40, 80, 70, 80, 80, 100 }, null,
            14);
        tblLigneVente.personnaliserAspectCellules(ligneArticleCelluleRenderer);
        colonneQuantiteOrigine = true;
        break;
      // Sans la colonne des quantités d'origine
      case 2:
        if (modele.isClientTTC()) {
          LISTE_TITRE_LIGNE_VENTE_CREATION[3] = TITRE_COLONNE_PRIX_TTC;
          LISTE_TITRE_LIGNE_VENTE_CREATION[9] = TITRE_COLONNE_MONTANT_TTC;
        }
        else {
          LISTE_TITRE_LIGNE_VENTE_CREATION[3] = TITRE_COLONNE_PRIX_HT;
          LISTE_TITRE_LIGNE_VENTE_CREATION[9] = TITRE_COLONNE_MONTANT_HT;
        }
        tblLigneVente.personnaliserAspect(LISTE_TITRE_LIGNE_VENTE_CREATION, new int[] { 60, 40, 380, 80, 40, 80, 70, 80, 80, 100 },
            new int[] { 65, 40, -1, 80, 40, 80, 70, 80, 80, 100 }, null, 14);
        tblLigneVente.personnaliserAspectCellules(ligneArticleCelluleRenderer);
        colonneQuantiteOrigine = false;
        break;
    }
    
    // Dans le cas de la création, il faut tout initialiser
    if (toutInitialiser) {
      // Permet de redéfinir la touche Enter sur la liste
      Action nouvelleActionEnter = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent ae) {
          tblLigneVenteKeyEntree(ae);
        }
      };
      Action nouvelleActionSuppr = new AbstractAction() {
        @Override
        public void actionPerformed(ActionEvent ae) {
          tblLigneVenteKeySuppr();
        }
      };
      tblLigneVente.modifierAction(nouvelleActionEnter, SNCharteGraphique.TOUCHE_ENTREE);
      tblLigneVente.modifierAction(nouvelleActionSuppr, SNCharteGraphique.TOUCHE_SUPPR);
    }
    
    // Ajouter un listener sur les changement de sélection dans le tableau des lignes du document
    tblLigneVente.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblLigneVenteSelectionChanged(e);
      }
      
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    // Variable à ne pas toucher ou déplacer car elle permet d'éviter que des composants comme les JComboBox perturbent les traitements
    // lors de leurs rafraichissements par le déclenchment d'évènement (ItemChanged, ActionPerformed, ...)
    // Ne pas oublier son pendant à la fin de la méthode ainsi qu'au test dans toutes les méthodes évènements concernées.
    executerEvenements = false;
    
    // Rafraîchir les panneaux supérieurs
    rafraichirPanneauRecherche();
    rafraichirPanneauLivraison();
    rafraichirPanneauSaisieForcee();
    
    // Rafraîchir le résultat de la recherche
    rafraichirResultatRecherche();
    
    // Rafraîchir la liste des lignes de ventes
    rafraichirTitreDocument();
    rafraichirListeLigneDocument();
    
    // Rafraîchir totaux
    rafraichirPoidsTotal();
    rafraichirVolumeTotal();
    rafraichirTotalHT();
    rafraichirTotalTVA();
    rafraichirTotalTTC();
    
    // Rafraîchir les boutons
    rafraichirBoutonControlerEncours();
    rafraichirBoutonReglerComptant();
    rafraichirBoutonNegocierTotal();
    rafraichirBoutonGrouper();
    rafraichirBoutonModifierGroupe();
    rafraichirBoutonAjouterArticleSpecial();
    rafraichirBoutonAjouterCommentaire();
    rafraichirBoutonRechercherArticleCommentaire();
    rafraichirBoutonRechercherArticlePalette();
    rafraichirBoutonRechercherArticleTransport();
    rafraichirBoutonRechercherArticleStandard();
    rafraichirBoutonAjouterBonCour();
    rafraichirBoutonImporterPlanPoseSipe();
    rafraichirBoutonContinuer();
    
    // Positionner le focus
    switch (modele.getComposantAyantLeFocus()) {
      case ModeleComptoir.FOCUS_ARTICLES_RECHERCHE_ARTICLE:
        tfTexteRecherche.requestFocus();
        break;
      
      case ModeleComptoir.FOCUS_ARTICLES_RESULTAT_RECHERCHE:
        tblResultatRecherche.requestFocusInWindow();
        break;
    }
    
    executerEvenements = true;
    
    // Au rafraichissement on affiche la fin de la liste des lignes sélectionnées
    scpLigneVente.getVerticalScrollBar().setValue(scpLigneVente.getVerticalScrollBar().getMaximum());
    scpLigneVente.validate();
  }
  
  private void rafraichirBoutonControlerEncours() {
    boolean actif = true;
    // Désactiver le bouton si le document n'est pas modifiable
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    // Désactiver le bouton si l'encours n'est pas dépassé
    else if (!modele.isEncoursEstDepasse() && !modele.isModePaiementComptant()) {
      actif = false;
    }
    // Désactiver le bouton si la saisie d'article n'est pas possible (encours dépassé + PS300 à 2 ou à 1)
    else if (!modele.isPaiementComptantPourEncoursDepasse()) {
      actif = false;
    }
    // Désactiver le bouton si on est en paiment comptant
    else if (modele.isModePaiementComptant()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_CONTROLER_ENCOURS, actif);
  }
  
  private void rafraichirBoutonReglerComptant() {
    boolean actif = true;
    // Désactiver le bouton si le document n'est pas modifiable
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    // Désactiver le bouton si l'encours n'est pas dépassé
    else if (!modele.isEncoursEstDepasse() && !modele.isModePaiementComptant()) {
      actif = false;
    }
    // Désactiver le bouton si la saisie d'article n'est pas possible (encours dépassé + PS300 à 1 ou 2)
    else if (!modele.isPaiementComptantPourEncoursDepasse()) {
      actif = false;
    }
    // Désactiver le bouton si on est en paiment comptant
    else if (modele.isModePaiementComptant()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_REGLER_COMPTANT, actif);
  }
  
  private void rafraichirBoutonNegocierTotal() {
    boolean actif = true;
    // Désactiver le bouton si le document n'est pas modifiable
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si le document ne comporte pas d'articles (sauf commentaires)
    else if (!modele.getDocumentVenteEnCours().isArticleHorsCommentairePresent()) {
      actif = false;
    }
    // Désactiver le bouton si le montant total du document vaut 0
    else if (modele.getDocumentVenteEnCours().getTotalHT() != null
        && modele.getDocumentVenteEnCours().getTotalHT().compareTo(BigDecimal.ZERO) == 0) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_NEGOCIER_TOTAL, actif);
  }
  
  private void rafraichirBoutonGrouper() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si aucune ligne n'est sélectionnée
    else if (tblLigneVente.getSelectedRowCount() <= 0) {
      actif = false;
    }
    // Désactiver le bouton si les lignes sélectionnées ne sont pas regroupables
    else if (!modele.getDocumentVenteEnCours().isLignesVenteSontRegroupables(tblLigneVente.getSelectedRows())) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_GROUPER, actif);
  }
  
  private void rafraichirBoutonModifierGroupe() {
    boolean actif = true;
    int ligneSelectionnee = tblLigneVente.getSelectedRow();
    // Désactiver le bouton si le document est vide
    if (modele.getDocumentVenteEnCours() == null || modele.getDocumentVenteEnCours().getListeLigneVente() == null) {
      actif = false;
    }
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    else if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si aucune ligne n'est sélectionnée (ou hors plage)
    else if (ligneSelectionnee < 0 || ligneSelectionnee >= modele.getDocumentVenteEnCours().getListeLigneVente().size()) {
      actif = false;
    }
    // Désactiver le bouton si les lignes sélectionnées n'appartiennent pas au même groupe ou ne sont pas regroupées
    else if (!modele.getDocumentVenteEnCours().isLignesVenteAppartiennentMemeGroupe(tblLigneVente.getSelectedRows())) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_MODIFIER_GROUPE, actif);
  }
  
  private void rafraichirBoutonAjouterArticleSpecial() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si ce n'est pas un devis ou une commande
    else if (!modele.getDocumentVenteEnCours().isDevis() & !modele.getDocumentVenteEnCours().isCommande()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AJOUTER_ARTICLE_SPECIAL, actif);
  }
  
  private void rafraichirBoutonAjouterCommentaire() {
    boolean actif;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    else {
      actif = true;
    }
    snBarreBouton.activerBouton(BOUTON_AJOUTER_COMMENTAIRE, actif);
  }
  
  private void rafraichirBoutonRechercherArticleCommentaire() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si on est déjà en recherche de commentaires
    else if (modele.getEtape() == ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE, actif);
  }
  
  private void rafraichirBoutonRechercherArticlePalette() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si on est déjà en recherche de palettes
    else if (modele.getEtape() == ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_PALETTE, actif);
  }
  
  private void rafraichirBoutonRechercherArticleTransport() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si ce n'est pas une livraison
    else if (!modele.getDocumentVenteEnCours().isLivraison()) {
      actif = false;
    }
    // Désactiver le bouton si c'est un direct usine
    else if (modele.getDocumentVenteEnCours().isDirectUsine()) {
      actif = false;
    }
    // Désactiver le bouton si on est déjà en recherche de transport
    else if (modele.getEtape() == ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_TRANSPORT) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_TRANSPORT, actif);
  }
  
  private void rafraichirBoutonRechercherArticleStandard() {
    boolean actif = true;
    // Désactiver le bouton si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      actif = false;
    }
    // Désactiver le bouton si on est déjà en recherche d'articles standards
    else if (modele.getEtape() == ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_STANDARD) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD, actif);
  }
  
  private void rafraichirBoutonAjouterBonCour() {
    boolean actif = true;
    // Désactiver le bouton si le document n'est pas modifiable
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    // Désactiver le bouton si on le document est une livraison
    else if (modele.getDocumentVenteEnCours().isLivraison()) {
      actif = false;
    }
    // Désactiver le bouton si on le document n'est pas un bon ou une facture
    else if (!modele.getDocumentVenteEnCours().isBon() && !modele.getDocumentVenteEnCours().isFacture()) {
      actif = false;
    }
    // Désactiver le bouton si le document est un avoir
    else if (modele.getDocumentVenteEnCours().isAvoir()) {
      actif = false;
    }
    // Désactiver le bouton si la gestion des bons de cour n'est pas activée (PS330)
    else if (!modele.isGestionBonCourActive()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_AJOUTER_BON_COUR, actif);
  }
  
  private void rafraichirBoutonImporterPlanPoseSipe() {
    // L'option est elle activée ?
    boolean actif = modele.isImportationPlanPoseSipeActive();
    // Désactiver le bouton si le document n'est pas modifiable
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()) {
      actif = false;
    }
    // Désactiver le bouton si on le document n'est pas un devis
    else if (!modele.getDocumentVenteEnCours().isDevis()) {
      actif = false;
    }
    // Désactiver le bouton si le document est un avoir
    else if (modele.getDocumentVenteEnCours().isAvoir()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(BOUTON_IMPORTER_PLAN_DE_POSE_SIPE, actif);
  }
  
  private void rafraichirBoutonContinuer() {
    boolean actif = true;
    if (!modele.getDocumentVenteEnCours().isArticleHorsCommentairePresent()) {
      actif = false;
    }
    // Désactiver le bouton si l'encours est dépassé (sans autorisation) et que le paiement n'est pas comptant
    else if (modele.isEncoursEstDepasse() && !modele.isModePaiementComptant()) {
      actif = false;
    }
    // Désactiver le bouton si la saisie d'article n'est pas possible (encours dépassé + PS300 à 2)
    else if (!modele.isSaisieArticlePossible() && !modele.isModePaiementComptant()) {
      actif = false;
    }
    // Désactiver le bouton si il y a une erreur blocante sur le document
    else if (modele.isErreurBlocanteDocument()) {
      actif = false;
    }
    // Désactiver le bouton si le document ne comporte pas d'articles (sauf commentaires)
    else {
      actif = true;
    }
    snBarreBouton.activerBouton(EnumBouton.CONTINUER, actif);
  }
  
  private void rafraichirPanneauRecherche() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      pnlRechercheArticle.setVisible(false);
    }
    else {
      pnlRechercheArticle.setVisible(true);
      rafraichirTexteRecherche();
      rafraichirMessage();
      rafraichirFiltreHorsGamme();
    }
  }
  
  private void rafraichirTexteRecherche() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      lbRechercheArticle.setVisible(false);
      tfTexteRecherche.setVisible(false);
      return;
    }
    lbRechercheArticle.setVisible(true);
    tfTexteRecherche.setVisible(true);
    
    // Mettre le libellé adapté au mode de recherche
    switch (modele.getEtape()) {
      case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_STANDARD:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_ARTICLE);
        break;
      
      case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_TRANSPORT:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_TRANSPORT);
        break;
      
      case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_PALETTE);
        break;
      
      case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_COMMENTAIRE);
        break;
      
      default:
        lbRechercheArticle.setText(LIBELLE_RECHERCHE_ARTICLE);
        break;
    }
    
    // Mettre à jour le texte rechercher et le message d'erreur
    tfTexteRecherche.setText(modele.getTexteRecherche());
    tfTexteRecherche.setEnabled(modele.isSaisieArticlePossible() || modele.isForcerReglementComptantACauseEncours());
  }
  
  /**
   * Mettre à jour les données concernant le texte recherché.
   * Champs concernés : le texte recherché, son libellé et le message d'erreur.
   */
  private void rafraichirMessage() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      lbErreurArticle.setVisible(false);
      return;
    }
    
    // Mettre à jour le texte rechercher et le message d'erreur
    lbErreurArticle.setText(modele.getMessageArticle());
    
    // Afficher les zones
    lbErreurArticle.setVisible(true);
  }
  
  /**
   * Met à jour le filtre de recherche pour les articles hors gammes.
   * Champs concernés : boîte à cocher hors gamme.
   */
  private void rafraichirFiltreHorsGamme() {
    // Masquer si les quantités articles du document ne sont pas modifiables ou si les articles hors gamme ne sont pas autorisés
    if (!modele.isModifierQuantitePossible() || !modele.isArticleHorsGammeAutorise()) {
      chkFiltreHorsGamme.setVisible(false);
      return;
    }
    
    // Afficher ou masquer les éléments suivant le mode de recherche
    switch (modele.getEtape()) {
      case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_STANDARD:
        chkFiltreHorsGamme.setVisible(true);
        break;
      
      default:
        chkFiltreHorsGamme.setVisible(false);
        break;
    }
    
    // Mettre à jour le filtre hors gamme
    chkFiltreHorsGamme.setSelected(modele.getFiltreHorsGamme().equals(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT));
  }
  
  private void rafraichirPanneauLivraison() {
    // Masquer si le document n'est pas modifiable
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()) {
      pnlLivraison.setVisible(false);
    }
    else {
      pnlLivraison.setVisible(true);
      rafraichirZoneGeographique();
      rafraichirVilleLivraison();
    }
  }
  
  private void rafraichirZoneGeographique() {
    // Masquer si le document n'est pas modifiable, si ce n'est pas une livraison ou si c'est un direct usine
    if (modele.getDocumentVenteEnCours() == null || !modele.getDocumentVenteEnCours().isModifiable()
        || !modele.getDocumentVenteEnCours().isLivraison() || modele.getDocumentVenteEnCours().isDirectUsine()) {
      pnlLivraison.setVisible(false);
      return;
    }
    
    // Mettre à jour les données
    pnlLivraison.setVisible(true);
    cbZoneGeographique.removeAllItems();
    cbZoneGeographique.addItem("Tous");
    ListeZoneGeographique listeZoneGeographique = modele.getListeZoneGeographique();
    if (listeZoneGeographique != null) {
      // Chargement de la boite à choix
      for (ZoneGeographique ZoneGeographique : listeZoneGeographique) {
        cbZoneGeographique.addItem(ZoneGeographique);
      }
      // Sélection d'un élément de la liste à partir d'un id
      if (modele.retournerIdZoneGeographique() != null) {
        cbZoneGeographique.setSelectedItem(listeZoneGeographique.get(modele.retournerIdZoneGeographique()));
      }
    }
    else {
      cbZoneGeographique.setEnabled(false);
    }
  }
  
  private void rafraichirVilleLivraison() {
    // Masquer si le document n'est pas modifiable, si ce n'est pas une livraison ou si c'est un direct usine
    if (modele.getDocumentVenteEnCours() != null && modele.getDocumentVenteEnCours().isModifiable()
        && modele.getDocumentVenteEnCours().getTransport() != null
        && modele.getDocumentVenteEnCours().getTransport().getAdresseLivraison() != null) {
      lbVilleLivraison.setText(modele.getDocumentVenteEnCours().getTransport().getAdresseLivraison().getVille());
      lbVilleLivraison.setVisible(true);
    }
    else {
      lbVilleLivraison.setText("");
      lbVilleLivraison.setVisible(false);
    }
  }
  
  private void rafraichirPanneauSaisieForcee() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      pnlSaisieForcee.setVisible(false);
    }
    else {
      pnlSaisieForcee.setVisible(true);
      rafraichirPourcentageRemiseForcee();
      rafraichirColonneTarifaireForcee();
    }
  }
  
  private void rafraichirPourcentageRemiseForcee() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      tfPourcentageRemiseForcee.setText(Constantes.formater(modele.getTauxRemiseForcee(), true));
      tfPourcentageRemiseForcee.setVisible(true);
    }
    else {
      tfPourcentageRemiseForcee.setText("");
      tfPourcentageRemiseForcee.setVisible(false);
    }
  }
  
  private void rafraichirColonneTarifaireForcee() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (modele.isModifierQuantitePossible()) {
      // Déterminer la première colonne
      int premiereColonne = 1;
      if (modele.getClientCourant() != null && modele.getClientCourant().getNumeroColonneTarif() != null) {
        premiereColonne = modele.getClientCourant().getNumeroColonneTarif();
      }
      
      // Remplir la liste déroulante
      cbColonneTarifaireForcee.removeAllItems();
      cbColonneTarifaireForcee.addItem("");
      for (int i = premiereColonne; i <= TarifArticle.getNombreColonneTarifMax(modele.isModeNegoce()); i++) {
        cbColonneTarifaireForcee.addItem(Integer.valueOf(i));
      }
      
      //
      if (modele.getNumeroColonneTarifForcee() != null) {
        cbColonneTarifaireForcee.setSelectedItem(modele.getNumeroColonneTarifForcee());
      }
      else {
        cbColonneTarifaireForcee.setSelectedItem(null);
      }
      
      cbColonneTarifaireForcee.setVisible(true);
    }
    else {
      cbColonneTarifaireForcee.setSelectedIndex(-1);
      cbColonneTarifaireForcee.setVisible(false);
    }
  }
  
  private void rafraichirResultatRecherche() {
    // Masquer si les quantités articles du document ne sont pas modifiables
    if (!modele.isModifierQuantitePossible()) {
      lbResultatRecherche.setVisible(false);
      scpResultatRecherche.setVisible(false);
      return;
    }
    lbResultatRecherche.setVisible(true);
    scpResultatRecherche.setVisible(true);
    
    // Effacer les données de tous les modèles
    tableModelRechercheArticleStandardHT.setRowCount(0);
    tableModelRechercheArticleStandardTTC.setRowCount(0);
    tableModelRechercheArticleCommentaire.setRowCount(0);
    tableModelRechercheArticlePalette.setRowCount(0);
    
    // Saisie bloquée pour encours dépassé
    tblResultatRecherche.setEnabled(modele.isSaisieArticlePossible() || modele.isForcerReglementComptantACauseEncours());
    if (!modele.isSaisieArticlePossible() && !modele.isForcerReglementComptantACauseEncours()) {
      lbErreurArticle.setText("Saisie bloquée : l'encours client est dépassé");
    }
    else {
      // Rafraîchir le tableau suivant le mode de recherche en cours
      switch (modele.getEtape()) {
        case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_STANDARD:
        case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_TRANSPORT:
          rafraichirResultatRechercheArticleStandard();
          break;
        
        case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE:
          rafraichirResultatRechercheArticleCommentaire();
          break;
        
        case ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE:
          rafraichirResultatRechercheArticlePalette();
          break;
        
        default:
          rafraichirResultatRechercheArticleStandard();
          break;
      }
      
      // Remettre la ligne sélectionné avant le rafraichissement
      int index = modele.getIndexArticleSelectionne();
      if (index > -1 && index < tblResultatRecherche.getRowCount()) {
        tblResultatRecherche.getSelectionModel().setSelectionInterval(index, index);
        tblResultatRecherche.setColumnSelectionInterval(0, 0);
      }
      else {
        tblResultatRecherche.getSelectionModel().clearSelection();
      }
    }
  }
  
  /**
   * Rafraîchir le résultat de la recherche d'articles standards.
   */
  private void rafraichirResultatRechercheArticleStandard() {
    // Vérifier le mode d'affichage
    if (modele.getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_STANDARD
        && modele.getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_TRANSPORT) {
      return;
    }
    
    // Rafraîchir le titre du tableau
    ListeResultatRechercheArticle listeResultatRechercheArticle = modele.getListeResultatRechercheArticle();
    if (listeResultatRechercheArticle != null) {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_ARTICLE, listeResultatRechercheArticle.size()));
    }
    else {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_ARTICLE, 0));
    }
    
    String[][] donnees = null;
    if (listeResultatRechercheArticle != null) {
      donnees = new String[listeResultatRechercheArticle.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_STANDARD_HT.length];
      for (int i = 0; i < listeResultatRechercheArticle.size(); i++) {
        ResultatRechercheArticle resultatRechercheArticle = listeResultatRechercheArticle.get(i);
        String[] ligne = donnees[i];
        
        // La première colonne est réservée pour la saisie
        ligne[0] = null;
        
        // Unité de conditionnement de vente
        if (resultatRechercheArticle.getIdUCV() != null) {
          ligne[1] = resultatRechercheArticle.getIdUCV().getCode();
        }
        
        // Libellé
        if (ManagerSessionClient.getInstance().getEnvUser().isDebugStatus()) {
          ligne[2] = resultatRechercheArticle.getId().getIdArticle().getCodeArticle() + " | " + resultatRechercheArticle.getLibelle();
        }
        else {
          ligne[2] = resultatRechercheArticle.getLibelle();
        }
        
        // Quantité en stock
        BigDecimal stock = resultatRechercheArticle.getStock();
        if (stock != null) {
          if (stock.compareTo(BigDecimal.ZERO) <= 0) {
            ligne[3] = "<html><span style='color:#cc0000'>" + Constantes.formater(stock, false) + "</span></html>";
          }
          else {
            ligne[3] = Constantes.formater(stock, false);
          }
        }
        
        // Prix de vente
        if (resultatRechercheArticle.getPrixNet(modele.isClientTTC()) != null) {
          ligne[4] = Constantes.formater(resultatRechercheArticle.getPrixNet(modele.isClientTTC()), true);
        }
        
        // Unité de vente
        if (resultatRechercheArticle.getIdUV() != null) {
          ligne[5] = resultatRechercheArticle.getIdUV().getCode();
        }
        
        // Construction texte conditionnement
        if (resultatRechercheArticle.getTexteUVParUCV() != null) {
          ligne[6] = resultatRechercheArticle.getTexteUVParUCV();
        }
        
        // Origine prix
        if (resultatRechercheArticle.getOrigine() != null) {
          ligne[7] = resultatRechercheArticle.getOrigine();
        }
        
        // Observation
        if (resultatRechercheArticle.getObservation() != null) {
          ligne[8] = resultatRechercheArticle.getObservation();
        }
      }
    }
    
    // Sélectionner le modèle de la table en fonction du HT/TTC
    DefaultTableModel tableModel = null;
    if (modele.isClientTTC()) {
      tableModel = tableModelRechercheArticleStandardTTC;
    }
    else {
      tableModel = tableModelRechercheArticleStandardHT;
    }
    
    // Mettre à jour les données
    tableModel.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModel.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblResultatRecherche.getModel().equals(tableModel)) {
      tblResultatRecherche.setModel(tableModel);
      tblResultatRecherche.setGridColor(new Color(204, 204, 204));
      tblResultatRecherche.setDefaultRenderer(Object.class, rechercheArticleRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblResultatRecherche.getRowCount() == 0) {
      rechercheArticleRenderer.redimensionnerColonnes(tblResultatRecherche);
    }
  }
  
  /**
   * Rafraîchir le résultat de la recherche d'articles commentaires.
   */
  private void rafraichirResultatRechercheArticleCommentaire() {
    // Vérifier le mode d'affichage
    if (modele.getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_COMMENTAIRE) {
      return;
    }
    
    // Rafraîchir le titre du tableau
    ListeResultatRechercheArticle listeResultatRechercheArticle = modele.getListeResultatRechercheArticle();
    if (listeResultatRechercheArticle != null) {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_COMMENTAIRE, listeResultatRechercheArticle.size()));
    }
    else {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_COMMENTAIRE, 0));
    }
    
    String[][] donnees = null;
    if (listeResultatRechercheArticle != null) {
      donnees = new String[listeResultatRechercheArticle.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_COMMENTAIRE.length];
      for (int i = 0; i < listeResultatRechercheArticle.size(); i++) {
        ResultatRechercheArticle resultatRechercheArticle = listeResultatRechercheArticle.get(i);
        String[] ligne = donnees[i];
        
        ligne[0] = null;
        ligne[1] = resultatRechercheArticle.getId().getIdArticle().getCodeArticle();
        ligne[2] = resultatRechercheArticle.getLibelle().replace('\n', ' ');
      }
    }
    
    // Mettre à jour les données
    tableModelRechercheArticleCommentaire.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelRechercheArticleCommentaire.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblResultatRecherche.getModel().equals(tableModelRechercheArticleCommentaire)) {
      tblResultatRecherche.setModel(tableModelRechercheArticleCommentaire);
      tblResultatRecherche.setGridColor(new Color(204, 204, 204));
      tblResultatRecherche.setDefaultRenderer(Object.class, rechercheArticleCommentaireRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblResultatRecherche.getRowCount() == 0) {
      rechercheArticleCommentaireRenderer.redimensionnerColonnes(tblResultatRecherche);
    }
  }
  
  /**
   * Rafraîchir le résultat de la recherche d'articles palettes.
   */
  private void rafraichirResultatRechercheArticlePalette() {
    // Vérifier le mode d'affichage
    if (modele.getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
      return;
    }
    
    // Rafraîchir le titre du tableau
    ListeResultatRechercheArticlePalette listeResultatRechercheArticlePalette = modele.getListeResultatRechercheArticlePalette();
    if (listeResultatRechercheArticlePalette != null) {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_PALETTE, listeResultatRechercheArticlePalette.size()));
    }
    else {
      lbResultatRecherche.setText(String.format(LIBELLE_RESULTAT_RECHERCHE_PALETTE, 0));
    }
    
    // Remplir les données du tableau
    String[][] donnees = null;
    if (listeResultatRechercheArticlePalette != null) {
      donnees = new String[listeResultatRechercheArticlePalette.size()][LISTE_TITRE_RESULTAT_RECHERCHE_ARTICLE_PALETTE.length];
      for (int i = 0; i < listeResultatRechercheArticlePalette.size(); i++) {
        ResultatRechercheArticlePalette resultatRechercheArticlePalette = listeResultatRechercheArticlePalette.get(i);
        String[] ligne = donnees[i];
        
        // Quantité saisie
        if (i == 0 && modele.getNombreDePalettesLies().compareTo(BigDecimal.ZERO) > 0
            && modele.getEtape() != ModeleComptoir.ETAPE_RECHERCHE_ARTICLE_PALETTE) {
          ligne[0] = Constantes.formater(modele.getNombreDePalettesLies(), false);
        }
        else {
          ligne[0] = null;
        }
        
        // Libellé
        ligne[1] = resultatRechercheArticlePalette.getLibelle();
        
        // Stock physique
        BigDecimal stock = resultatRechercheArticlePalette.getStockPhysique();
        if (stock != null) {
          if (stock.compareTo(BigDecimal.ZERO) <= 0) {
            ligne[2] = "<html><span style='color:#cc0000'>" + Constantes.formater(stock, false) + "</span></html>";
          }
          else {
            ligne[2] = Constantes.formater(stock, false);
          }
        }
        
        // Stock client
        ligne[3] = Constantes.formater(resultatRechercheArticlePalette.getQuantiteStockClient(), false);
        
        // Prix consignation
        ligne[4] = Constantes.formater(resultatRechercheArticlePalette.getPrixConsignation(), true);
        
        // Prix déconsignation
        ligne[5] = Constantes.formater(resultatRechercheArticlePalette.getPrixDeconsignation(), true);
      }
    }
    // Mettre à jour les données
    tableModelRechercheArticlePalette.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelRechercheArticlePalette.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!tblResultatRecherche.getModel().equals(tableModelRechercheArticlePalette)) {
      tblResultatRecherche.setModel(tableModelRechercheArticlePalette);
      tblResultatRecherche.setGridColor(new Color(204, 204, 204));
      tblResultatRecherche.setDefaultRenderer(Object.class, rechercheArticlePaletteRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblResultatRecherche.getRowCount() == 0) {
      rechercheArticlePaletteRenderer.redimensionnerColonnes(tblResultatRecherche);
    }
  }
  
  private void rafraichirTitreDocument() {
    if (modele.getDocumentVenteEnCours() != null || modele.getDocumentVenteEnCours().getListeLigneVente() != null) {
      ListeLigneVente listeLigneVente = modele.getDocumentVenteEnCours().getListeLigneVente();
      lbTitreDocument.setText("Lignes du document (" + listeLigneVente.size() + ")");
    }
    else {
      lbTitreDocument.setText("Lignes du document (0)");
    }
  }
  
  private void rafraichirListeLigneDocument() {
    // Si besoin on change la structure de la table
    initialiserListeLigneVente();
    
    // Récupère les données de la liste
    ligneArticleCelluleRenderer.setListeLigneVente(modele.getDocumentVenteEnCours().getListeLigneVente());
    tblLigneVente.mettreAJourDonnees(modele.formaterListeArticlesDocument(tblLigneVente.getColumnCount()));
    
    // Sélection des lignes articles
    if (modele.getListeLigneVenteASelectionner().isEmpty()) {
      tblLigneVente.clearSelection();
    }
    else {
      ArrayList<IdLigneVente> listeIdLigneVente = modele.getListeLigneVenteASelectionner();
      int debut = modele.getDocumentVenteEnCours().getIndexLigneVente(listeIdLigneVente.get(0));
      int fin = modele.getDocumentVenteEnCours().getIndexLigneVente(listeIdLigneVente.get(listeIdLigneVente.size() - 1));
      if (debut > -1 && fin > -1) {
        tblLigneVente.setRowSelectionInterval(debut, fin);
      }
      modele.getListeLigneVenteASelectionner().clear();
    }
    
    // Remettre la ligne sélectionné avant le rafraichissement
    int index = modele.getIndexLigneVenteSelectionnee();
    if (index > -1 && index < tblLigneVente.getRowCount()) {
      tblLigneVente.getSelectionModel().setSelectionInterval(index, index);
      tblLigneVente.setColumnSelectionInterval(0, 0);
    }
    else {
      tblLigneVente.getSelectionModel().clearSelection();
    }
  }
  
  private void rafraichirPoidsTotal() {
    if (modele.getDocumentVenteEnCours() != null) {
      zsPoidsTotal.setText(Constantes.formater(modele.getDocumentVenteEnCours().getPoidsTotalKg(), false));
    }
    else {
      zsPoidsTotal.setText("");
    }
  }
  
  private void rafraichirVolumeTotal() {
    if (modele.getDocumentVenteEnCours() != null) {
      zsVolume.setText(Constantes.formater(modele.getDocumentVenteEnCours().getVolume(), false));
    }
    else {
      zsVolume.setText("");
    }
  }
  
  private void rafraichirTotalHT() {
    if (modele.getDocumentVenteEnCours() != null) {
      zsTotalHT.setText(Constantes.formater(modele.getDocumentVenteEnCours().getTotalHT(), true));
    }
    else {
      zsTotalHT.setText("");
    }
  }
  
  private void rafraichirTotalTVA() {
    if (modele.getDocumentVenteEnCours() != null) {
      zsTotalTVA.setText(Constantes.formater(modele.getDocumentVenteEnCours().getTotalTVA(), true));
    }
    else {
      zsTotalTVA.setText("");
    }
  }
  
  private void rafraichirTotalTTC() {
    if (modele.getDocumentVenteEnCours() != null) {
      zsTotalTTC.setText(Constantes.formater(modele.getDocumentVenteEnCours().getTotalTTC(), true));
    }
    else {
      zsTotalTTC.setText("");
    }
  }
  
  /**
   * Récupère la ligne article sélectionné.
   */
  private void validerListeLigneArticleSelection() {
    // Récupérer le document de ventes en cours
    DocumentVente documentVente = modele.getDocumentVenteEnCours();
    if (documentVente == null) {
      return;
    }
    
    // Récupérer les lignes de ventes
    ListeLigneVente listeLigneVente = modele.getDocumentVenteEnCours().getListeLigneVente();
    if (listeLigneVente == null) {
      return;
    }
    
    // Vérifier si la ligne sélectionnée est dans la plage
    int indexvisuel = tblLigneVente.getSelectedRow();
    if (indexvisuel < 0 || indexvisuel >= listeLigneVente.size()) {
      return;
    }
    
    // Récupérer la ligne sélectionné
    LigneVente ligneVente = listeLigneVente.get(indexvisuel);
    if (ligneVente == null) {
      return;
    }
    
    // Tester d'il s'agit d'une ligne commentaire d'un regroupement
    if (ligneVente.isLigneCommentaire() && ligneVente.isLigneRegroupee()) {
      return;
    }
    // Tester s'il s'agit d'un article commentaire
    else if (ligneVente.isLigneCommentaire()) {
      modele.modifierLigneCommentaire(ligneVente);
    }
    else if (ligneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) != 0) {
      // S'il s'agit d'un article spécial
      if (modele.etreArticleSpecial(ligneVente)) {
        modele.selectionnerLigneVenteArticleSpecial(ligneVente, indexvisuel);
      }
      // S'il s'agit d'un article transport
      else if (modele.isArticleTransport(ligneVente)) {
        // modele.selectionnerLigneVente(ligneVente, ModeleDetailLigneArticle.NE_PAS_AFFICHER, ModeleComptoir.TABLE_LIGNES_DOCUMENT, null,
        // false);
        modele.selectionnerLigneVente(ligneVente, ModeleDetailLigneArticle.ONGLET_GENERAL, ModeleComptoir.TABLE_LIGNES_DOCUMENT, null,
            false);
      }
      // S'il s'agit d'un article remise ou assimilé (à améliorer car le * est trop générique)
      else if ((ligneVente.getQuantiteUV().compareTo(BigDecimal.ZERO) < 0)
          && (ligneVente.getIdArticle().getCodeArticle().charAt(0) == '*')) {
        modele.negocierGlobalement();
      }
      // S'il s'agit d'un article "normal"
      else {
        modele.selectionnerLigneVente(ligneVente, ModeleDetailLigneArticle.ONGLET_GENERAL, ModeleComptoir.TABLE_LIGNES_DOCUMENT, null,
            false);
      }
    }
    // Suppression systématique de la sélection dans la liste de la ligne de vente lors de la modification d'une ligne
    modele.modifierLigneVenteSelectionnee(-1);
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  // -- Méthodes privées
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  /**
   * Limite la saisie de quantité à un seul article dans la liste de résultats article
   */
  private void limiterNombreQuantiteSelectionnee() {
    if ((tblResultatRecherche != null) && (tblResultatRecherche.getSelectedRow() > -1)
        && (tblResultatRecherche.getSelectedRow() < tblResultatRecherche.getRowCount())) {
      int indiceLigneEnCours = tblResultatRecherche.getSelectedRow();
      String quantiteSaisie = (String) tblResultatRecherche.getModel().getValueAt(indiceLigneEnCours, 0);
      if (quantiteSaisie != null) {
        for (int i = 0; i < tblResultatRecherche.getModel().getRowCount(); i++) {
          if (i != indiceLigneEnCours) {
            tblResultatRecherche.getModel().setValueAt("", i, 0);
          }
        }
      }
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        modele.validerOngletArticle();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.annulerDocumentVente();
      }
      else if (pSNBouton.isBouton(BOUTON_REGLER_COMPTANT)) {
        modele.setForcerReglementComptantACauseEncours(true);
        modele.activerPaiementComptant();
        rafraichir();
      }
      else if (pSNBouton.isBouton(BOUTON_GROUPER)) {
        modele.creerRegroupement(tblLigneVente.getSelectedRows());
      }
      else if (pSNBouton.isBouton(BOUTON_MODIFIER_GROUPE)) {
        int indexreel = tblLigneVente.getSelectedRow();
        if (indexreel >= 0) {
          modele.modifierRegroupement(indexreel);
        }
      }
      else if (pSNBouton.isBouton(BOUTON_CONTROLER_ENCOURS)) {
        modele.controlerEncoursClient();
        rafraichir();
      }
      else if (pSNBouton.isBouton(BOUTON_NEGOCIER_TOTAL)) {
        modele.negocierGlobalement();
      }
      else if (pSNBouton.isBouton(BOUTON_AJOUTER_ARTICLE_SPECIAL)) {
        indiceLigneInsertion = tblLigneVente.getSelectedRow();
        modele.selectionnerLigneVenteArticleSpecial(null, indiceLigneInsertion);
      }
      else if (pSNBouton.isBouton(BOUTON_AJOUTER_COMMENTAIRE)) {
        indiceLigneInsertion = tblLigneVente.getSelectedRow();
        int numeroLigne = modele.getDocumentVenteEnCours().calculerNumeroLigneArticle(indiceLigneInsertion);
        modele.creerLigneCommentaire(numeroLigne);
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_COMMENTAIRE)) {
        modele.activerModeRechercheArticleCommentaire();
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_PALETTE)) {
        modele.activerModeRechercheArticlePalette();
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_TRANSPORT)) {
        modele.activerModeRechercheArticleTransport();
      }
      else if (pSNBouton.isBouton(BOUTON_RECHERCHER_ARTICLE_STANDARD)) {
        modele.activerModeRechercheArticleStandard();
      }
      else if (pSNBouton.isBouton(BOUTON_AJOUTER_BON_COUR)) {
        indiceLigneInsertion = tblLigneVente.getSelectedRow();
        int numeroLigne = modele.getDocumentVenteEnCours().calculerNumeroLigneArticle(indiceLigneInsertion);
        modele.ajouterBonCour(numeroLigne);
      }
      else if (pSNBouton.isBouton(BOUTON_IMPORTER_PLAN_DE_POSE_SIPE)) {
        modele.importerPlanDePoseSipe();
      }
      
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles - Critères de recherche
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void tfRechercheArticleActionPerformed(ActionEvent e) {
    try {
      modele.modifierTexteRecherche(tfTexteRecherche.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void chkFiltreHorsGammeActionPerformed(ActionEvent e) {
    try {
      if (chkFiltreHorsGamme.isSelected()) {
        modele.modifierFiltreHorsGamme(EnumFiltreHorsGamme.OPTION_LES_HORS_GAMME_UNIQUEMENT);
      }
      else {
        modele.modifierFiltreHorsGamme(EnumFiltreHorsGamme.OPTION_TOUS_LES_ARTICLES_SAUF_HORS_GAMME);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemiseForceeActionPerformed(ActionEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierRemiseForcee(tfPourcentageRemiseForcee.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPourcentageRemiseForceeFocusLost(FocusEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      modele.modifierRemiseForcee(tfPourcentageRemiseForcee.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfColonneTarifaireForceeItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      
      if (cbColonneTarifaireForcee.getSelectedItem() instanceof Integer) {
        modele.modifierNumeroColonneTarifForcee((Integer) cbColonneTarifaireForcee.getSelectedItem());
      }
      else {
        modele.modifierNumeroColonneTarifForcee(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbZoneGeographiqueItemStateChanged(ItemEvent e) {
    try {
      if (!executerEvenements) {
        return;
      }
      
      if (cbZoneGeographique.getSelectedItem() instanceof ZoneGeographique) {
        modele.modifierZoneGeographique((ZoneGeographique) cbZoneGeographique.getSelectedItem());
      }
      else {
        modele.modifierZoneGeographique(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles - Tableau résultat de la recherche
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * La ligne sélectionnée a changée dans le tableau résultat.
   */
  private void tblResultatRechercheSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!executerEvenements) {
        return;
      }
      
      // Informer le modèle
      int index = tblResultatRecherche.getSelectedRow();
      modele.modifierArticleSelectionne(index);
      
      // Mettre le focus sur la première colonne pour permettre une saisie directement sur cette colonne
      if (index != -1) {
        tblResultatRecherche.setColumnSelectionInterval(0, 0);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'appui sur les touches flêches et la touche entrée dans la tableau affichant le résultat de la recherche articles.
   */
  private void tblResultatRechercheKeyPressed(ActionEvent e, Action actionOriginale, boolean gestionFleches) {
    try {
      int indexLigneSelectionnee = tblResultatRecherche.getSelectedRow();
      if (indexLigneSelectionnee == -1) {
        return;
      }
      if (tblResultatRecherche.editCellAt(indexLigneSelectionnee, 0)) {
        // Lire la valeur sur la ligne sélectionnée avant de déclencher le traitement de l'action par le tableau car celui-ci efface
        // la valeur saisie dans certains cas (déplacement en fin de tableau).
        String valeur = (String) tblResultatRecherche.getValueAt(indexLigneSelectionnee, 0);
        
        // Déclencher le traitement de l'action par le tableau
        actionOriginale.actionPerformed(e);
        
        // Ne rien faire si la cellule est vide et qu'on se déplace avec les flêches
        if (gestionFleches) {
          if ((valeur == null) || valeur.trim().isEmpty() || valeur.trim().equals("0")) {
            return;
          }
        }
        
        // Informer le modèle qu'une valeur a été saisie pour un article
        modele.modifierQuantiteArticle(valeur, indexLigneSelectionnee, tblLigneVente.getSelectedRow());
      }
      else {
        actionOriginale.actionPerformed(e);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblResultatRechercheMouseClicked(MouseEvent e) {
    try {
      // Informer le modèle
      int index = tblResultatRecherche.getSelectedRow();
      modele.modifierArticleSelectionne(index);
      
      // Mettre le focus sur la première colonne pour permettre une saisie directement sur cette colonne
      if (index != -1) {
        tblResultatRecherche.setColumnSelectionInterval(0, 0);
      }
      
      if (e.getClickCount() == 2) {
        modele.modifierQuantiteArticle("d", tblResultatRecherche.getSelectedRow(), tblLigneVente.getSelectedRow());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpResultatRechercheStateChanged(ChangeEvent e) {
    try {
      if (tblResultatRecherche == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpResultatRecherche.getViewport().getViewRect();
      int premiereLigne = tblResultatRecherche.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblResultatRecherche.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblResultatRecherche.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.modifierPlageArticleStandardAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblResultatRecherchePropertyChange(PropertyChangeEvent e) {
    try {
      limiterNombreQuantiteSelectionnee();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes évènementielles - Liste des lignes de ventes
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void tblLigneVenteSelectionChanged(ListSelectionEvent e) {
    try {
      // Ne rien fair ene cours de rafraîchissement
      if (!executerEvenements) {
        return;
      }
      // Notifier le modèle
      modele.modifierLigneVenteSelectionnee(tblLigneVente.getSelectedRow());
      
      // Remettre le focus sur le tableau après le rafraichissement
      tblLigneVente.requestFocus();
      
      // Rafraichir les boutons concernant les regroupements si nécessaire
      rafraichirBoutonGrouper();
      rafraichirBoutonModifierGroupe();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLigneVenteMouseClicked(MouseEvent e) {
    try {
      // Traitement du double clic
      if (e.getClickCount() == 2) {
        validerListeLigneArticleSelection();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLigneVenteKeyEntree(ActionEvent ae) {
    try {
      // Si les quantités articles du document ne sont pas modifiables alors on sort
      if (!modele.isModifierQuantitePossible()) {
        return;
      }
      validerListeLigneArticleSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLigneVenteKeySuppr() {
    try {
      // Si les quantités articles du document ne sont pas modifiables alors on sort
      if (!modele.isModifierQuantitePossible()) {
        return;
      }
      modele.supprimerListeLigneVente(tblLigneVente.getSelectedRows());
      modele.conserverTotalTTC();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblLigneVenteComponentResized(ComponentEvent e) {
    try {
      if ((tblLigneVente.getRowCount() == 0) || (indiceLigneInsertion == AUCUNE_INSERTION)) {
        return;
      }
      
      // On positionne la scrollebar de la table de façon à toujours voir la dernière ligne insérée
      if (indiceLigneInsertion == -1) {
        indiceLigneInsertion = tblLigneVente.getRowCount() - 1;
      }
      tblLigneVente.scrollRectToVisible(tblLigneVente.getCellRect(indiceLigneInsertion, 0, true));
      indiceLigneInsertion = AUCUNE_INSERTION;
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // JFormDesigner
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new JPanel();
    pnlRechercheArticle = new JPanel();
    lbRechercheArticle = new JLabel();
    tfTexteRecherche = new JTextField();
    pnlFiltresRechercheArticle = new JPanel();
    chkFiltreHorsGamme = new JCheckBox();
    lbErreurArticle = new JLabel();
    pnlSaisieParDefaut = new SNPanel();
    pnlLivraison = new JPanel();
    lbTitreLivraison = new JLabel();
    pnlZoneGeographique = new JPanel();
    lbZoneGeographique = new SNLabelChamp();
    cbZoneGeographique = new JComboBox();
    lbVilleLivraison = new JLabel();
    pnlSaisieForcee = new JPanel();
    lbTitreSaisieForcee = new JLabel();
    pnlSaisieForceeInformations = new JPanel();
    lbPourcentageRemise = new JLabel();
    tfPourcentageRemiseForcee = new JTextField();
    lbColonneTarifaire = new JLabel();
    cbColonneTarifaireForcee = new XRiComboBox();
    lbResultatRecherche = new JLabel();
    scpResultatRecherche = new JScrollPane();
    tblResultatRecherche = new JTable();
    lbTitreDocument = new JLabel();
    scpLigneVente = new JScrollPane();
    tblLigneVente = new NRiTable();
    pnlRecapitulatif = new JPanel();
    pnlInfosDiverses = new JPanel();
    lbPoidsTotal = new JLabel();
    zsPoidsTotal = new RiZoneSortie();
    lbUnitePoids = new JLabel();
    lbVolume = new JLabel();
    zsVolume = new RiZoneSortie();
    lbUniteVolume = new JLabel();
    pnlTotaux = new JPanel();
    lbTotalHT = new JLabel();
    zsTotalHT = new RiZoneSortie();
    lbTotalTVA = new JLabel();
    zsTotalTVA = new RiZoneSortie();
    lbTotalTTC = new JLabel();
    zsTotalTTC = new RiZoneSortie();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1240, 650));
    setPreferredSize(new Dimension(1240, 650));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlRechercheArticle ========
      {
        pnlRechercheArticle.setOpaque(false);
        pnlRechercheArticle.setPreferredSize(new Dimension(675, 105));
        pnlRechercheArticle.setMinimumSize(new Dimension(675, 105));
        pnlRechercheArticle.setName("pnlRechercheArticle");
        pnlRechercheArticle.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRechercheArticle.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlRechercheArticle.getLayout()).rowWeights = new double[] { 0.1, 0.1, 0.1, 1.0E-4 };
        
        // ---- lbRechercheArticle ----
        lbRechercheArticle.setText("Recherche article");
        lbRechercheArticle.setFont(lbRechercheArticle.getFont().deriveFont(lbRechercheArticle.getFont().getStyle() | Font.BOLD,
            lbRechercheArticle.getFont().getSize() + 2f));
        lbRechercheArticle.setPreferredSize(new Dimension(180, 20));
        lbRechercheArticle.setMinimumSize(new Dimension(180, 20));
        lbRechercheArticle.setMaximumSize(new Dimension(180, 20));
        lbRechercheArticle.setHorizontalAlignment(SwingConstants.LEFT);
        lbRechercheArticle.setName("lbRechercheArticle");
        pnlRechercheArticle.add(lbRechercheArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfTexteRecherche ----
        tfTexteRecherche.setMinimumSize(new Dimension(470, 30));
        tfTexteRecherche.setPreferredSize(new Dimension(470, 30));
        tfTexteRecherche.setFont(new Font("sansserif", Font.BOLD, 14));
        tfTexteRecherche.setName("tfTexteRecherche");
        tfTexteRecherche.addActionListener(e -> tfRechercheArticleActionPerformed(e));
        pnlRechercheArticle.add(tfTexteRecherche, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlFiltresRechercheArticle ========
        {
          pnlFiltresRechercheArticle.setOpaque(false);
          pnlFiltresRechercheArticle.setMinimumSize(new Dimension(490, 30));
          pnlFiltresRechercheArticle.setPreferredSize(new Dimension(490, 30));
          pnlFiltresRechercheArticle.setMaximumSize(new Dimension(2147483647, 2147483647));
          pnlFiltresRechercheArticle.setName("pnlFiltresRechercheArticle");
          pnlFiltresRechercheArticle.setLayout(new FlowLayout(FlowLayout.LEFT));
          
          // ---- chkFiltreHorsGamme ----
          chkFiltreHorsGamme.setText("Articles hors gamme");
          chkFiltreHorsGamme.setFont(chkFiltreHorsGamme.getFont().deriveFont(chkFiltreHorsGamme.getFont().getSize() + 2f));
          chkFiltreHorsGamme.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          chkFiltreHorsGamme.setPreferredSize(new Dimension(180, 30));
          chkFiltreHorsGamme.setMaximumSize(new Dimension(180, 30));
          chkFiltreHorsGamme.setMinimumSize(new Dimension(180, 30));
          chkFiltreHorsGamme.setName("chkFiltreHorsGamme");
          chkFiltreHorsGamme.addActionListener(e -> chkFiltreHorsGammeActionPerformed(e));
          pnlFiltresRechercheArticle.add(chkFiltreHorsGamme);
        }
        pnlRechercheArticle.add(pnlFiltresRechercheArticle, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbErreurArticle ----
        lbErreurArticle.setText("text");
        lbErreurArticle.setFont(new Font("sansserif", Font.BOLD, 14));
        lbErreurArticle.setForeground(Color.red);
        lbErreurArticle.setMaximumSize(new Dimension(2147483647, 2147483647));
        lbErreurArticle.setPreferredSize(new Dimension(490, 30));
        lbErreurArticle.setMinimumSize(new Dimension(490, 30));
        lbErreurArticle.setName("lbErreurArticle");
        pnlRechercheArticle.add(lbErreurArticle, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlRechercheArticle,
          new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== pnlSaisieParDefaut ========
      {
        pnlSaisieParDefaut.setOpaque(false);
        pnlSaisieParDefaut.setMinimumSize(new Dimension(530, 105));
        pnlSaisieParDefaut.setPreferredSize(new Dimension(540, 105));
        pnlSaisieParDefaut.setName("pnlSaisieParDefaut");
        pnlSaisieParDefaut.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSaisieParDefaut.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlSaisieParDefaut.getLayout()).rowHeights = new int[] { 5, 0, 0 };
        ((GridBagLayout) pnlSaisieParDefaut.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlSaisieParDefaut.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlLivraison ========
        {
          pnlLivraison.setOpaque(false);
          pnlLivraison.setMinimumSize(new Dimension(280, 90));
          pnlLivraison.setPreferredSize(new Dimension(280, 90));
          pnlLivraison.setName("pnlLivraison");
          pnlLivraison.setLayout(new BorderLayout());
          
          // ---- lbTitreLivraison ----
          lbTitreLivraison.setText("Livraison");
          lbTitreLivraison.setFont(new Font("sansserif", Font.BOLD, 14));
          lbTitreLivraison.setVerticalAlignment(SwingConstants.BOTTOM);
          lbTitreLivraison.setPreferredSize(new Dimension(280, 20));
          lbTitreLivraison.setMinimumSize(new Dimension(280, 20));
          lbTitreLivraison.setMaximumSize(new Dimension(2147483647, 2147483647));
          lbTitreLivraison.setName("lbTitreLivraison");
          pnlLivraison.add(lbTitreLivraison, BorderLayout.NORTH);
          
          // ======== pnlZoneGeographique ========
          {
            pnlZoneGeographique.setOpaque(false);
            pnlZoneGeographique.setMinimumSize(new Dimension(280, 80));
            pnlZoneGeographique.setPreferredSize(new Dimension(280, 80));
            pnlZoneGeographique.setBorder(new TitledBorder(""));
            pnlZoneGeographique.setName("pnlZoneGeographique");
            pnlZoneGeographique.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlZoneGeographique.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlZoneGeographique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlZoneGeographique.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlZoneGeographique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbZoneGeographique ----
            lbZoneGeographique.setText("Zone g\u00e9ographique");
            lbZoneGeographique.setHorizontalAlignment(SwingConstants.RIGHT);
            lbZoneGeographique.setPreferredSize(new Dimension(125, 30));
            lbZoneGeographique.setMinimumSize(new Dimension(125, 30));
            lbZoneGeographique.setMaximumSize(new Dimension(125, 30));
            lbZoneGeographique.setName("lbZoneGeographique");
            pnlZoneGeographique.add(lbZoneGeographique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- cbZoneGeographique ----
            cbZoneGeographique.setPreferredSize(new Dimension(80, 30));
            cbZoneGeographique.setMinimumSize(new Dimension(80, 30));
            cbZoneGeographique.setFont(cbZoneGeographique.getFont().deriveFont(cbZoneGeographique.getFont().getSize() + 2f));
            cbZoneGeographique.setName("cbZoneGeographique");
            cbZoneGeographique.addItemListener(e -> cbZoneGeographiqueItemStateChanged(e));
            pnlZoneGeographique.add(cbZoneGeographique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVilleLivraison ----
            lbVilleLivraison.setText("Ville");
            lbVilleLivraison.setFont(lbVilleLivraison.getFont().deriveFont(lbVilleLivraison.getFont().getSize() + 2f));
            lbVilleLivraison.setMinimumSize(new Dimension(270, 30));
            lbVilleLivraison.setMaximumSize(new Dimension(270, 30));
            lbVilleLivraison.setPreferredSize(new Dimension(270, 30));
            lbVilleLivraison.setName("lbVilleLivraison");
            pnlZoneGeographique.add(lbVilleLivraison, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlLivraison.add(pnlZoneGeographique, BorderLayout.CENTER);
        }
        pnlSaisieParDefaut.add(pnlLivraison, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        
        // ======== pnlSaisieForcee ========
        {
          pnlSaisieForcee.setOpaque(false);
          pnlSaisieForcee.setMinimumSize(new Dimension(255, 90));
          pnlSaisieForcee.setPreferredSize(new Dimension(255, 90));
          pnlSaisieForcee.setName("pnlSaisieForcee");
          pnlSaisieForcee.setLayout(new BorderLayout());
          
          // ---- lbTitreSaisieForcee ----
          lbTitreSaisieForcee.setText("Saisie forc\u00e9e");
          lbTitreSaisieForcee.setFont(new Font("sansserif", Font.BOLD, 14));
          lbTitreSaisieForcee.setVerticalAlignment(SwingConstants.BOTTOM);
          lbTitreSaisieForcee.setMaximumSize(new Dimension(2147483647, 2147483647));
          lbTitreSaisieForcee.setMinimumSize(new Dimension(255, 20));
          lbTitreSaisieForcee.setPreferredSize(new Dimension(255, 20));
          lbTitreSaisieForcee.setName("lbTitreSaisieForcee");
          pnlSaisieForcee.add(lbTitreSaisieForcee, BorderLayout.NORTH);
          
          // ======== pnlSaisieForceeInformations ========
          {
            pnlSaisieForceeInformations.setBorder(
                new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
            pnlSaisieForceeInformations.setOpaque(false);
            pnlSaisieForceeInformations.setPreferredSize(new Dimension(255, 80));
            pnlSaisieForceeInformations.setMinimumSize(new Dimension(255, 80));
            pnlSaisieForceeInformations.setName("pnlSaisieForceeInformations");
            pnlSaisieForceeInformations.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlSaisieForceeInformations.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSaisieForceeInformations.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlSaisieForceeInformations.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlSaisieForceeInformations.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPourcentageRemise ----
            lbPourcentageRemise.setText("Pourcentage de remise");
            lbPourcentageRemise.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentageRemise.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbPourcentageRemise.setName("lbPourcentageRemise");
            pnlSaisieForceeInformations.add(lbPourcentageRemise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfPourcentageRemiseForcee ----
            tfPourcentageRemiseForcee.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPourcentageRemiseForcee.setMinimumSize(new Dimension(70, 30));
            tfPourcentageRemiseForcee.setPreferredSize(new Dimension(70, 30));
            tfPourcentageRemiseForcee.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfPourcentageRemiseForcee.setName("tfPourcentageRemiseForcee");
            tfPourcentageRemiseForcee.addActionListener(e -> tfPourcentageRemiseForceeActionPerformed(e));
            tfPourcentageRemiseForcee.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPourcentageRemiseForceeFocusLost(e);
              }
            });
            pnlSaisieForceeInformations.add(tfPourcentageRemiseForcee, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbColonneTarifaire ----
            lbColonneTarifaire.setText("Colonne tarifaire");
            lbColonneTarifaire.setHorizontalAlignment(SwingConstants.RIGHT);
            lbColonneTarifaire.setFont(new Font("sansserif", Font.PLAIN, 14));
            lbColonneTarifaire.setName("lbColonneTarifaire");
            pnlSaisieForceeInformations.add(lbColonneTarifaire, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbColonneTarifaireForcee ----
            cbColonneTarifaireForcee.setPreferredSize(new Dimension(30, 30));
            cbColonneTarifaireForcee.setMinimumSize(new Dimension(30, 30));
            cbColonneTarifaireForcee.setFont(new Font("sansserif", Font.PLAIN, 14));
            cbColonneTarifaireForcee
                .setModel(new DefaultComboBoxModel<>(new String[] { " ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
            cbColonneTarifaireForcee.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            cbColonneTarifaireForcee.setName("cbColonneTarifaireForcee");
            cbColonneTarifaireForcee.addItemListener(e -> tfColonneTarifaireForceeItemStateChanged(e));
            pnlSaisieForceeInformations.add(cbColonneTarifaireForcee, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlSaisieForcee.add(pnlSaisieForceeInformations, BorderLayout.CENTER);
        }
        pnlSaisieParDefaut.add(pnlSaisieForcee, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlSaisieParDefaut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
          GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbResultatRecherche ----
      lbResultatRecherche.setText("R\u00e9sultat de la recherche");
      lbResultatRecherche.setFont(lbResultatRecherche.getFont().deriveFont(lbResultatRecherche.getFont().getStyle() | Font.BOLD,
          lbResultatRecherche.getFont().getSize() + 2f));
      lbResultatRecherche.setMaximumSize(new Dimension(250, 20));
      lbResultatRecherche.setMinimumSize(new Dimension(250, 20));
      lbResultatRecherche.setPreferredSize(new Dimension(250, 20));
      lbResultatRecherche.setName("lbResultatRecherche");
      pnlContenu.add(lbResultatRecherche,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
      
      // ======== scpResultatRecherche ========
      {
        scpResultatRecherche.setBackground(Color.white);
        scpResultatRecherche.setOpaque(true);
        scpResultatRecherche.setMinimumSize(new Dimension(800, 200));
        scpResultatRecherche.setPreferredSize(new Dimension(800, 200));
        scpResultatRecherche.setName("scpResultatRecherche");
        
        // ---- tblResultatRecherche ----
        tblResultatRecherche.setShowVerticalLines(true);
        tblResultatRecherche.setShowHorizontalLines(true);
        tblResultatRecherche.setBackground(Color.white);
        tblResultatRecherche.setOpaque(false);
        tblResultatRecherche.setRowHeight(20);
        tblResultatRecherche.setGridColor(new Color(204, 204, 204));
        tblResultatRecherche.setMaximumSize(new Dimension(2147483647, 1000));
        tblResultatRecherche.setName("tblResultatRecherche");
        tblResultatRecherche.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblResultatRechercheMouseClicked(e);
          }
        });
        tblResultatRecherche.addPropertyChangeListener(e -> tblResultatRecherchePropertyChange(e));
        scpResultatRecherche.setViewportView(tblResultatRecherche);
      }
      pnlContenu.add(scpResultatRecherche,
          new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbTitreDocument ----
      lbTitreDocument.setText("Lignes du document");
      lbTitreDocument.setFont(lbTitreDocument.getFont().deriveFont(lbTitreDocument.getFont().getStyle() | Font.BOLD,
          lbTitreDocument.getFont().getSize() + 2f));
      lbTitreDocument.setMaximumSize(new Dimension(200, 20));
      lbTitreDocument.setMinimumSize(new Dimension(150, 20));
      lbTitreDocument.setPreferredSize(new Dimension(150, 20));
      lbTitreDocument.setName("lbTitreDocument");
      pnlContenu.add(lbTitreDocument,
          new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpLigneVente ========
      {
        scpLigneVente.setPreferredSize(new Dimension(800, 200));
        scpLigneVente.setBackground(Color.white);
        scpLigneVente.setOpaque(true);
        scpLigneVente.setName("scrpLignesArticlesDocument");
        scpLigneVente.setAutoscrolls(true);
        scpLigneVente.setMinimumSize(new Dimension(800, 200));
        
        // ---- tblLigneVente ----
        tblLigneVente.setShowVerticalLines(true);
        tblLigneVente.setShowHorizontalLines(true);
        tblLigneVente.setBackground(Color.white);
        tblLigneVente.setRowHeight(20);
        tblLigneVente.setGridColor(new Color(204, 204, 204));
        tblLigneVente.setOpaque(false);
        tblLigneVente.setMaximumSize(new Dimension(32767, 32767));
        tblLigneVente.setName("tblLigneVente");
        tblLigneVente.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblLigneVenteMouseClicked(e);
          }
        });
        tblLigneVente.addComponentListener(new ComponentAdapter() {
          @Override
          public void componentResized(ComponentEvent e) {
            tblLigneVenteComponentResized(e);
          }
        });
        scpLigneVente.setViewportView(tblLigneVente);
      }
      pnlContenu.add(scpLigneVente,
          new GridBagConstraints(0, 4, 2, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlRecapitulatif ========
      {
        pnlRecapitulatif.setMinimumSize(new Dimension(1195, 50));
        pnlRecapitulatif.setPreferredSize(new Dimension(1195, 50));
        pnlRecapitulatif.setOpaque(false);
        pnlRecapitulatif.setName("pnlRecapitulatif");
        pnlRecapitulatif.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRecapitulatif.getLayout()).columnWidths = new int[] { 458, 0, 696, 5, 0 };
        ((GridBagLayout) pnlRecapitulatif.getLayout()).rowHeights = new int[] { 70, 0 };
        ((GridBagLayout) pnlRecapitulatif.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlRecapitulatif.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlInfosDiverses ========
        {
          pnlInfosDiverses.setMinimumSize(new Dimension(250, 40));
          pnlInfosDiverses.setPreferredSize(new Dimension(250, 40));
          pnlInfosDiverses.setMaximumSize(new Dimension(32767, 40));
          pnlInfosDiverses.setOpaque(false);
          pnlInfosDiverses.setName("pnlInfosDiverses");
          pnlInfosDiverses.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInfosDiverses.getLayout()).columnWidths = new int[] { 25, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInfosDiverses.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbPoidsTotal ----
          lbPoidsTotal.setText("Poids total");
          lbPoidsTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPoidsTotal.setName("lbPoidsTotal");
          pnlInfosDiverses.add(lbPoidsTotal, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsPoidsTotal ----
          zsPoidsTotal.setHorizontalAlignment(SwingConstants.RIGHT);
          zsPoidsTotal.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsPoidsTotal.setMinimumSize(new Dimension(80, 30));
          zsPoidsTotal.setMaximumSize(new Dimension(100, 40));
          zsPoidsTotal.setPreferredSize(new Dimension(80, 30));
          zsPoidsTotal.setName("zsPoidsTotal");
          pnlInfosDiverses.add(zsPoidsTotal, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUnitePoids ----
          lbUnitePoids.setText("Kg");
          lbUnitePoids.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbUnitePoids.setHorizontalAlignment(SwingConstants.LEFT);
          lbUnitePoids.setMinimumSize(new Dimension(25, 19));
          lbUnitePoids.setPreferredSize(new Dimension(25, 19));
          lbUnitePoids.setHorizontalTextPosition(SwingConstants.LEFT);
          lbUnitePoids.setName("lbUnitePoids");
          pnlInfosDiverses.add(lbUnitePoids, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbVolume ----
          lbVolume.setText("Volume total");
          lbVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbVolume.setHorizontalAlignment(SwingConstants.RIGHT);
          lbVolume.setName("lbVolume");
          pnlInfosDiverses.add(lbVolume, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- zsVolume ----
          zsVolume.setHorizontalAlignment(SwingConstants.RIGHT);
          zsVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsVolume.setPreferredSize(new Dimension(65, 30));
          zsVolume.setMinimumSize(new Dimension(65, 30));
          zsVolume.setMaximumSize(new Dimension(100, 40));
          zsVolume.setName("zsVolume");
          pnlInfosDiverses.add(zsVolume, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUniteVolume ----
          lbUniteVolume.setText("m\u00b3");
          lbUniteVolume.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbUniteVolume.setHorizontalAlignment(SwingConstants.LEFT);
          lbUniteVolume.setMaximumSize(new Dimension(25, 19));
          lbUniteVolume.setMinimumSize(new Dimension(25, 19));
          lbUniteVolume.setPreferredSize(new Dimension(25, 19));
          lbUniteVolume.setName("lbUniteVolume");
          pnlInfosDiverses.add(lbUniteVolume, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlRecapitulatif.add(pnlInfosDiverses, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setBackground(new Color(222, 222, 206));
          pnlTotaux.setMaximumSize(new Dimension(1080, 40));
          pnlTotaux.setMinimumSize(new Dimension(680, 40));
          pnlTotaux.setPreferredSize(new Dimension(680, 40));
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 52, 120, 0, 0, 80, 107, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 40, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbTotalHT ----
          lbTotalHT.setText("Total HT");
          lbTotalHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalHT.setName("lbTotalHT");
          pnlTotaux.add(lbTotalHT, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_TRAILING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
          
          // ---- zsTotalHT ----
          zsTotalHT.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalHT.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsTotalHT.setMinimumSize(new Dimension(125, 30));
          zsTotalHT.setPreferredSize(new Dimension(125, 30));
          zsTotalHT.setMaximumSize(new Dimension(1000, 30));
          zsTotalHT.setName("zsTotalHT");
          pnlTotaux.add(zsTotalHT, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_LEADING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
          
          // ---- lbTotalTVA ----
          lbTotalTVA.setText("Total TVA");
          lbTotalTVA.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTVA.setName("lbTotalTVA");
          pnlTotaux.add(lbTotalTVA, new GridBagConstraints(2, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_TRAILING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
          
          // ---- zsTotalTVA ----
          zsTotalTVA.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalTVA.setFont(new Font("sansserif", Font.PLAIN, 14));
          zsTotalTVA.setMinimumSize(new Dimension(125, 30));
          zsTotalTVA.setPreferredSize(new Dimension(125, 30));
          zsTotalTVA.setMaximumSize(new Dimension(1000, 30));
          zsTotalTVA.setName("zsTotalTVA");
          pnlTotaux.add(zsTotalTVA, new GridBagConstraints(3, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_LEADING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
          
          // ---- lbTotalTTC ----
          lbTotalTTC.setText("Total TTC");
          lbTotalTTC.setFont(new Font("sansserif", Font.BOLD, 14));
          lbTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTotalTTC.setName("lbTotalTTC");
          pnlTotaux.add(lbTotalTTC, new GridBagConstraints(4, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_TRAILING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 5), 0, 0));
          
          // ---- zsTotalTTC ----
          zsTotalTTC.setHorizontalAlignment(SwingConstants.RIGHT);
          zsTotalTTC.setFont(new Font("sansserif", Font.BOLD, 14));
          zsTotalTTC.setMinimumSize(new Dimension(125, 30));
          zsTotalTTC.setPreferredSize(new Dimension(125, 30));
          zsTotalTTC.setMaximumSize(new Dimension(1000, 30));
          zsTotalTTC.setName("zsTotalTTC");
          pnlTotaux.add(zsTotalTTC, new GridBagConstraints(5, 0, 1, 1, 1.0, 0.0, GridBagConstraints.BELOW_BASELINE_LEADING,
              GridBagConstraints.NONE, new Insets(0, 10, 0, 0), 0, 0));
        }
        pnlRecapitulatif.add(pnlTotaux, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlContenu.add(pnlRecapitulatif, new GridBagConstraints(0, 6, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 10, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlContenu;
  private JPanel pnlRechercheArticle;
  private JLabel lbRechercheArticle;
  private JTextField tfTexteRecherche;
  private JPanel pnlFiltresRechercheArticle;
  private JCheckBox chkFiltreHorsGamme;
  private JLabel lbErreurArticle;
  private SNPanel pnlSaisieParDefaut;
  private JPanel pnlLivraison;
  private JLabel lbTitreLivraison;
  private JPanel pnlZoneGeographique;
  private SNLabelChamp lbZoneGeographique;
  private JComboBox cbZoneGeographique;
  private JLabel lbVilleLivraison;
  private JPanel pnlSaisieForcee;
  private JLabel lbTitreSaisieForcee;
  private JPanel pnlSaisieForceeInformations;
  private JLabel lbPourcentageRemise;
  private JTextField tfPourcentageRemiseForcee;
  private JLabel lbColonneTarifaire;
  private XRiComboBox cbColonneTarifaireForcee;
  private JLabel lbResultatRecherche;
  private JScrollPane scpResultatRecherche;
  private JTable tblResultatRecherche;
  private JLabel lbTitreDocument;
  private JScrollPane scpLigneVente;
  private NRiTable tblLigneVente;
  private JPanel pnlRecapitulatif;
  private JPanel pnlInfosDiverses;
  private JLabel lbPoidsTotal;
  private RiZoneSortie zsPoidsTotal;
  private JLabel lbUnitePoids;
  private JLabel lbVolume;
  private RiZoneSortie zsVolume;
  private JLabel lbUniteVolume;
  private JPanel pnlTotaux;
  private JLabel lbTotalHT;
  private RiZoneSortie zsTotalHT;
  private JLabel lbTotalTVA;
  private RiZoneSortie zsTotalTVA;
  private JLabel lbTotalTTC;
  private RiZoneSortie zsTotalTTC;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
