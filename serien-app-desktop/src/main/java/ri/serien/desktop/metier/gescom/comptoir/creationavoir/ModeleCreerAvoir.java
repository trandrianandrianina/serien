/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.creationavoir;

import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.TypeAvoir;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialoge qui propose la quantité correspondant au conditionnement supérieur.
 */
public class ModeleCreerAvoir extends AbstractModeleDialogue {
  // Variables
  private VueCreerAvoir vue = null;
  private ListeParametreSysteme listeParametreSysteme = null;
  private IdDocumentVente idDocumentOrigine = null;
  private TypeAvoir typeAvoir = null;
  
  /**
   * Constructeur.
   */
  public ModeleCreerAvoir(SessionBase pSession, IdDocumentVente pIdDocumentOrigine) {
    super(pSession);
    idDocumentOrigine = pIdDocumentOrigine;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Lecture des PS
    listeParametreSysteme = new ListeParametreSysteme().charger(getSession().getIdSession(), idDocumentOrigine.getIdEtablissement());
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier le type d'avoir
   */
  public void modifierTypeAvoir(TypeAvoir pTypeAvoir) {
    if (Constantes.equals(typeAvoir, pTypeAvoir)) {
      return;
    }
    typeAvoir = pTypeAvoir;
  }
  
  /**
   * Tester si la saisie d'un type d'avoir est obligatoire (PS145)
   */
  public boolean isTypeAvoirObligatoire() {
    if (listeParametreSysteme == null) {
      return false;
    }
    
    Character ps145 = ManagerSessionClient.getInstance().getValeurParametreSysteme(getSession().getIdSession(), EnumParametreSysteme.PS145);
    if (ps145.equals('1') || ps145.equals('3')) {
      return true;
    }
    return false;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Vue de la boite de dialogue
   */
  public VueCreerAvoir getVue() {
    return vue;
  }
  
  /**
   * Identifiant du document d'origine
   */
  public IdDocumentVente getIdDocumentOrigine() {
    return idDocumentOrigine;
  }
  
  /**
   * Type d'avoir sélectionné
   */
  public TypeAvoir getTypeAvoir() {
    return typeAvoir;
  }
  
}
