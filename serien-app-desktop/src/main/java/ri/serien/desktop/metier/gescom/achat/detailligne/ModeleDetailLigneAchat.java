/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ri.serien.desktop.metier.gescom.commun.attenduscommandes.ModeleAffichageAttendusCommandes;
import ri.serien.desktop.metier.gescom.commun.attenduscommandes.VueAffichageAttendusCommandes;
import ri.serien.desktop.metier.gescom.commun.mouvementsstock.ModeleAffichageMouvementsStock;
import ri.serien.desktop.metier.gescom.commun.mouvementsstock.VueAffichageMouvementsStock;
import ri.serien.desktop.metier.gescom.commun.stocksarticle.ModeleAffichageStockArticle;
import ri.serien.desktop.metier.gescom.commun.stocksarticle.VueAffichageStockArticle;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.prix.PrixAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumModeSaisiePort;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.lienligne.EnumTypeLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.LienLigne;
import ri.serien.libcommun.gescom.commun.stock.EnumTypeStockDisponible;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.pourcentage.BigPercentage;
import ri.serien.libcommun.outils.pourcentage.EnumFormatPourcentage;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleDetailLigneAchat extends AbstractModeleDialogue {
  // Constantes
  public static final int NE_PAS_AFFICHER = -1;
  private static final int NOMBRE_DECIMALE_MONTANT = 2;
  private static final int NOMBRE_DECIMALE_COEFFICIENT = 4;
  
  public static final int ONGLET_GENERAL = 0;
  public static final int ONGLET_NEGOCIATION = 1;
  public static final int ONGLET_INFORMATIONSTECHNIQUES = 2;
  public static final int ONGLET_STOCK = 3;
  public static final int ONGLET_HISTORIQUE = 4;
  public static final int ONGLET_DIMENSIONS = 5;
  public static final int ONGLET_LIENS = 6;
  
  public static final int SOUS_ONGLET_ETAT_STOCKS = 0;
  public static final int SOUS_ONGLET_MOUVEMENTS = 1;
  public static final int SOUS_ONGLET_ATTENDUS = 2;
  
  public static final int TAILLE_LIGNE_ITC = 120;
  public static final int NOMBRE_LIGNES_ITC = 10;
  public static final boolean HT = false;
  public static final boolean TTC = true;
  public static final boolean MODE_NEGOCIATION_VENTE = false;
  public static final boolean MODE_NEGOCIATION_ACHAT = true;
  
  public static final int DIMENSION_LONGUEUR = 0;
  public static final int DIMENSION_SURFACE = 1;
  public static final int DIMENSION_VOLUME = 2;
  
  public static final int NOMBREMAXURL = 5;
  
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_LIBELLE_ARTICLE = 1;
  public static final int FOCUS_QUANTITE = 2;
  public static final int FOCUS_DIMENSION_NOMBRE = 3;
  
  private static final String PREFIXE_LIBELLE_ARTICLE_DECOUPE = " (Découpé  : ";
  
  private static final Pattern PATTERN_NOM_DOMAINE = Pattern
      .compile("(http|ftp|https):\\/\\/[\\w\\-_]+(\\.[\\w\\-_.]+)?([\\w\\-\\.,@?^=%&amp;:/~\\+#]*" + "[\\w\\-\\@?^=%&amp;/~\\+#])?");
  private static final Pattern PATTERN_SAISIE_QUANTITE_UC = Pattern.compile("(-?[0-9.,]{0,})([pPmMvVcC]{0,})");
  
  // Variables
  private int ongletActif = ONGLET_GENERAL;
  private int sousOngletActif = 0;
  private int composantAyantLeFocus = FOCUS_QUANTITE;
  
  private ModeleAffichageStockArticle modeleStock = null;
  private VueAffichageStockArticle vueStock = null;
  private ModeleAffichageMouvementsStock modeleMouvements = null;
  private VueAffichageMouvementsStock vueMouvements = null;
  private ModeleAffichageAttendusCommandes modeleAttendus = null;
  private VueAffichageAttendusCommandes vueAttendus = null;
  private boolean ongletGeneralCharge = false;
  private boolean ongletHistoriqueCharge = false;
  private boolean ongletNegociationAchatCharge = false;
  private boolean ongletInformationsTechniquesCharge = false;
  private boolean ongletLiensCharge = false;
  private boolean sousOngletEtatStocks = false;
  private boolean sousOngletMouvements = false;
  private boolean sousOngletAttendus = false;
  
  private ListeUnite listeUnite = null;
  private Fournisseur fournisseur = null;
  private Article article = null;
  private ConditionAchat conditionAchat = null;
  private LigneAchatArticle ligneAchatArticle = null;
  private ListeStockComptoir listeStock = null;
  private List<LigneAttenduCommande> listeLignesAttCmd = null;
  private boolean sauverBlocNoteCNA = false;
  private BlocNote blocNoteCNA = null;
  private String[] listeURL = new String[5];
  private boolean isSaisieEnUS = false;
  private String messageUCS = "";
  private LienLigne lienOrigine = null;
  private DocumentAchat documentAchat = null;
  private boolean appelDepuisRechercheArticle = true;
  
  /**
   * Constructeur.
   */
  public ModeleDetailLigneAchat(SessionBase pSession, int pOnglet, LigneAchatArticle pLigneAchatArticle, Article pArticle,
      DocumentAchat pDocumentAchat, boolean pAppelDepuisRecherche) {
    super(pSession);
    
    if (pLigneAchatArticle == null) {
      throw new MessageErreurException("Impossible d'afficher le détail car la ligne d'achats est invalide.");
    }
    if (pArticle == null) {
      throw new MessageErreurException("Impossible d'afficher le détail car l'article est invalide.");
    }
    
    ongletActif = pOnglet;
    ligneAchatArticle = pLigneAchatArticle;
    article = pArticle;
    documentAchat = pDocumentAchat;
    appelDepuisRechercheArticle = pAppelDepuisRecherche;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    sauverBlocNoteCNA = false;
    
    // L'article est chargé pour tous les onglets
    article = lireArticle(ligneAchatArticle.getIdArticle());
    
    // Charger le fournisseur si ce n'est pas fait
    if (fournisseur == null) {
      if (article.getIdFournisseur() != null) {
        fournisseur = ManagerServiceFournisseur.chargerFournisseur(getIdSession(), article.getIdFournisseur());
      }
    }
    
    chargerListeUnites();
    
    switch (ongletActif) {
      case ModeleDetailLigneAchat.ONGLET_GENERAL:
        chargerOngletGeneral();
        break;
      
      case ModeleDetailLigneAchat.ONGLET_STOCK:
        chargerOngletStock();
        break;
      
      case ModeleDetailLigneAchat.ONGLET_HISTORIQUE:
        chargerOngletHistorique();
        break;
      
      case ModeleDetailLigneAchat.ONGLET_NEGOCIATION:
        chargerOngletNegociationAchat();
        break;
      
      case ModeleDetailLigneAchat.ONGLET_INFORMATIONSTECHNIQUES:
        chargerOngletInformationsTechniques();
        break;
      
      case ModeleDetailLigneAchat.ONGLET_LIENS:
        chargerOngletLiens();
        break;
      
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    // La validation n'a lieu que si le document est modifiable
    if (isDocumentModifiable()) {
      super.quitterAvecValidation();
    }
    else {
      quitterAvecAnnulation();
    }
  }
  
  @Override
  public void quitterAvecAnnulation() {
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne si le document est en cours de création on non.
   */
  public boolean isDocumentEnCoursCreation() {
    if (documentAchat != null && documentAchat.isEnCoursCreation()) {
      return true;
    }
    return false;
  }
  
  /**
   * Retourne si le document est modifiable.
   */
  public boolean isDocumentModifiable() {
    if (documentAchat != null && documentAchat.isModifiable()) {
      return true;
    }
    return false;
  }
  
  /**
   * Ouvre l'url du bouton à l'indice passé
   */
  public void ouvrirURL(int indiceBouton) {
    getSession().getLexique().ouvrirURL(getListeURL()[indiceBouton]);
  }
  
  /**
   * Changer l'onglet actif.
   */
  public void modifierOngletActif(int saisie) {
    if (saisie == ongletActif) {
      return;
    }
    ongletActif = saisie;
    chargerDonnees();
    rafraichir();
  }
  
  /**
   * Modifier la quantité commandée en UCA.
   */
  public void modifierQuantiteCommandeeUCA(String saisie) {
    if (ligneAchatArticle.getPrixAchat() == null) {
      return;
    }
    
    BigDecimal quantite = BigDecimal.ZERO;
    String nombreTrouve = "";
    String lettreTrouve = "";
    
    // On controle que le contenu de pValeur corresponde à une saisie valide (chiffre et/ou lettre)
    Matcher m = PATTERN_SAISIE_QUANTITE_UC.matcher(saisie);
    // Analyse des données saisies dans la cellule quantité
    if (m.find()) {
      nombreTrouve = Constantes.normerTexte(m.group(1));
      lettreTrouve = Constantes.normerTexte(m.group(2));
    }
    if (!lettreTrouve.isEmpty()) {
      lettreTrouve = lettreTrouve.toLowerCase();
    }
    if (!nombreTrouve.isEmpty()) {
      quantite = Constantes.convertirTexteEnBigDecimal(nombreTrouve);
    }
    if (quantite.compareTo(BigDecimal.ZERO) == 0) {
      quantite = BigDecimal.ONE;
    }
    
    // On controle que la quantité ne passe pas de positive à négative et inversement car les traitements sont trop complexe au niveau des
    // tarifs donc on interdit, l'utilisateur doit supprimer sa ligne et saisir la bonne quantité
    if (quantite.compareTo(BigDecimal.ZERO) < 0
        && ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) > 0) {
      throw new MessageErreurException(
          "Vous ne pouvez pas convertir une vente en retour d'article, vous devez supprimer la ligne en cours" + " de saisie.");
    }
    else if (quantite.compareTo(BigDecimal.ZERO) > 0
        && ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException(
          "Vous ne pouvez pas convertir un retour d'article en vente, vous devez supprimer la ligne" + " en cours de saisie.");
    }
    
    isSaisieEnUS = lettreTrouve.equals("p") || lettreTrouve.equals("v") || lettreTrouve.equals("c");
    
    if (quantite.compareTo(BigDecimal.ZERO) < 0 && article.isArticlePalette()) {
      ligneAchatArticle.getPrixAchat().setQuantiteReliquatUS(quantite, true);
    }
    else if (quantite.compareTo(BigDecimal.ZERO) >= 0 /*&& !article.isArticleDecoupable()*/) {
      if (isSaisieEnUS()) {
        BigDecimal quantiteCommandeUca = quantite.multiply(article.getNombreUSParUCS(true));
        ligneAchatArticle.getPrixAchat().setQuantiteReliquatUCA(quantiteCommandeUca, true);
      }
      else {
        if (lettreTrouve.equals("m")) {
          modifierQuantiteCommandeeUA(nombreTrouve);
        }
        else {
          ligneAchatArticle.getPrixAchat().setQuantiteReliquatUCA(quantite, true);
        }
      }
    }
    
    construireMessageUCS();
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la quantité commandée en UA.
   */
  public void modifierQuantiteCommandeeUA(String pSaisie) {
    BigDecimal quantiteUASaisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    if (quantiteUASaisie.compareTo(BigDecimal.ZERO) <= 0) {
      rafraichir();
      return;
    }
    
    ligneAchatArticle.getPrixAchat().setQuantiteReliquatUA(quantiteUASaisie, true);
    construireMessageUCS();
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la quantité en unité de stock
   */
  public void modifierQuantiteUCS(String pSaisie) {
    BigDecimal quantiteUCSSaisie = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (quantiteUCSSaisie.compareTo(BigDecimal.ZERO) < 0 || article.getNombreUSParUCS(true).compareTo(BigDecimal.ZERO) <= 0) {
      rafraichir();
      return;
    }
    
    ligneAchatArticle.getPrixAchat().setQuantiteInitialeUCA(quantiteUCSSaisie.multiply(article.getNombreUSParUCS(true)), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le mémo du document.
   */
  public void modifierITC(String pTexteSaisi) {
    if ((pTexteSaisi == null) || (blocNoteCNA == null) || (pTexteSaisi.equals(blocNoteCNA.getTexte()))) {
      return;
    }
    
    sauverBlocNoteCNA = true;
    blocNoteCNA.setTexte(pTexteSaisi);
    
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Retourne le nom de domaine à partir d'une URL.
   */
  public String getNomDomaine(String URL) {
    if ((URL == null) || (URL.trim().equals(""))) {
      return "";
    }
    String nomDomaine = "";
    Matcher matcher = PATTERN_NOM_DOMAINE.matcher(URL);
    try {
      if (matcher.find()) {
        nomDomaine = matcher.group(2).substring(1);
      }
    }
    catch (NullPointerException e) {
      nomDomaine = "";
    }
    return "<html><center>Fiche technique<br/>" + nomDomaine + "</center></html>";
  }
  
  // -- Méthodes privées
  
  /**
   * Charger les données de l'onglet général.
   */
  private void chargerOngletGeneral() {
    if (ongletGeneralCharge) {
      return;
    }
    ongletGeneralCharge = true;
    
    // Vérifier les pré-requis
    if (ligneAchatArticle == null) {
      throw new MessageErreurException("Impossible de récupérer la ligne d'achat.");
    }
    if (ligneAchatArticle.getIdArticle() == null) {
      throw new MessageErreurException("Impossible de récupérer l'identifiant de l'article de la ligne d'achat.");
    }
    
    chargerOngletNegociationAchat();
    
    // Charger le stock uniquement si le magasin est renseigné (les articles non gérés en stock n'ont pas de magasin)
    if (ligneAchatArticle.getIdMagasin() != null) {
      CritereStockComptoir critereStock = new CritereStockComptoir();
      critereStock.setIdMagasin(ligneAchatArticle.getIdMagasin());
      critereStock.setCodeArticle(ligneAchatArticle.getIdArticle().getCodeArticle());
      listeStock = ManagerServiceStock.chargerListeStockComptoir(getIdSession(), critereStock);
    }
    
    construireMessageUCS();
    
    rafraichir();
  }
  
  /**
   * Charger les données de l'onglet stock.
   */
  private void chargerOngletStock() {
    if (sousOngletActif == SOUS_ONGLET_MOUVEMENTS) {
      // Les mouvements de stock
      chargerSousOngletMouvements();
    }
    else if (sousOngletActif == SOUS_ONGLET_ATTENDUS) {
      // Les attendus commandé
      chargerSousOngletAttendus();
    }
    else {
      chargerSousOngletEtatStock();
    }
    rafraichir();
  }
  
  /**
   * Charger les données du sous onglet etat stock.
   */
  private void chargerSousOngletEtatStock() {
    if (sousOngletEtatStocks) {
      return;
    }
    sousOngletEtatStocks = true;
    // Les états des stocks
    modeleStock = new ModeleAffichageStockArticle(getSession(), article, EnumTypeStockDisponible.TYPE_ACHAT);
    vueStock = new VueAffichageStockArticle(modeleStock);
    vueStock.afficher();
  }
  
  /**
   * Charger les données du sous onglet mouvements stock.
   */
  private void chargerSousOngletMouvements() {
    if (sousOngletMouvements) {
      return;
    }
    sousOngletMouvements = true;
    // Les mouvements de stock
    modeleMouvements =
        new ModeleAffichageMouvementsStock(getSession(), article, EnumTypeStockDisponible.TYPE_ACHAT, ligneAchatArticle.getIdMagasin());
    vueMouvements = new VueAffichageMouvementsStock(modeleMouvements);
    vueMouvements.afficher();
  }
  
  /**
   * Charger les données du sous onglet attendus/commandés.
   */
  private void chargerSousOngletAttendus() {
    if (sousOngletAttendus) {
      return;
    }
    sousOngletAttendus = true;
    // Les mouvements de stock
    modeleAttendus = new ModeleAffichageAttendusCommandes(getSession(), article, ligneAchatArticle.getIdMagasin());
    vueAttendus = new VueAffichageAttendusCommandes(modeleAttendus);
    vueAttendus.afficher();
  }
  
  /**
   * Defini le sous onglet stock actif
   */
  public void modifierSousOngletStock(int selectedIndex) {
    sousOngletActif = selectedIndex;
    chargerOngletStock();
  }
  
  private void chargerListeUnites() {
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
  }
  
  /**
   * Charger les données de l'onglet historique.
   */
  private void chargerOngletHistorique() {
    if (ongletHistoriqueCharge) {
      return;
    }
    ongletHistoriqueCharge = true;
  }
  
  /**
   * Charger les données de l'onglet négociation vente.
   */
  private void chargerOngletNegociationAchat() {
    if (ongletNegociationAchatCharge) {
      return;
    }
    
    ongletNegociationAchatCharge = true;
    
    if (conditionAchat == null) {
      conditionAchat = ManagerServiceArticle.chargerConditionAchat(getIdSession(), ligneAchatArticle.getIdArticle(), new Date());
    }
  }
  
  /**
   * Charger les données de l'onglet mémo.
   */
  private void chargerOngletInformationsTechniques() {
    if (ongletInformationsTechniquesCharge) {
      return;
    }
    ongletInformationsTechniquesCharge = true;
    listeURL = chargerURL(article);
    
    lireBlocNoteCNA();
  }
  
  /**
   * Charger les données de l'onglet liens.
   */
  private void chargerOngletLiens() {
    if (ongletLiensCharge) {
      return;
    }
    
    lienOrigine = new LienLigne(IdLienLigne.getInstanceAchat(documentAchat.getId().getIdEtablissement(),
        EnumTypeLienLigne.LIEN_LIGNE_ACHAT, documentAchat.getId().getCodeEntete().getCode(), documentAchat.getId().getNumero(),
        documentAchat.getId().getSuffixe(), documentAchat.getNumeroFacture(), ligneAchatArticle.getId().getNumeroLigne(),
        documentAchat.getId(), ligneAchatArticle.getId().getNumeroLigne()));
    lienOrigine.setDocumentAchat(documentAchat);
    lienOrigine.setLigne(ligneAchatArticle);
    
    ongletLiensCharge = true;
    
  }
  
  /**
   * Enregistrer mémo ITC comme de nouvelles lignes commentaire
   */
  public void copierITCversCommentaire() {
    if (blocNoteCNA == null) {
      return;
    }
  }
  
  public int getNombreDecimalesUCA() {
    if (ligneAchatArticle.getPrixAchat() != null && ligneAchatArticle.getPrixAchat().getIdUCA() != null) {
      return retournerPrecisionUnite(ligneAchatArticle.getPrixAchat().getIdUCA());
    }
    return retournerPrecisionUnite(article.getIdUCV());
  }
  
  public int getNombreDecimalesUA() {
    if ((ligneAchatArticle) != null && ligneAchatArticle.getPrixAchat().getIdUA() != null) {
      return retournerPrecisionUnite(ligneAchatArticle.getPrixAchat().getIdUA());
    }
    return retournerPrecisionUnite(article.getIdUV());
  }
  
  public int getNombreDecimalesUS() {
    return retournerPrecisionUniteStock();
  }
  
  public int getNombreDecimalesUCS() {
    return retournerPrecisionUCS();
  }
  
  public String getLibelleUnite(IdUnite pIdUnite) {
    if (listeUnite != null) {
      return listeUnite.getLibelleUnite(pIdUnite);
    }
    return "";
  }
  
  /**
   * Change l'unité de la quantité en UC pour saisie en palette ou en UC
   */
  public void changerUnite() {
    
    if (Constantes.equals(article.getIdUCV(), article.getIdUCS())) {
      return;
    }
    
    // setSaisieEnUS(!isSaisieEnUS);
    rafraichir();
  }
  
  /**
   * Construit un message informatif sur l'équivalence entre quantités commandées et quantités en unité de commande de stock
   */
  private void construireMessageUCS() {
    messageUCS = "";
    // On doit pouvoir lire les informations de la ligne et de l'article
    if (ligneAchatArticle != null && article != null && ligneAchatArticle.getPrixAchat() != null && article.getIdUCS() != null
        && ligneAchatArticle.getPrixAchat().getNombreUCAParUCS().compareTo(BigDecimal.ZERO) > 0) {
      
      // Nombre d'UCA contenu par une UCS
      BigDecimal nbrUCAparUCS = ligneAchatArticle.getPrixAchat().getNombreUCAParUCS();
      
      // Si ce ratio n'a pas été chargé dans la ligne achat on le calcule à partir de l'article
      if (nbrUCAparUCS.compareTo(BigDecimal.ONE) == 0) {
        nbrUCAparUCS = ligneAchatArticle.getPrixAchat().getNombreUSParUCA().multiply(article.getNombreUSParUCS(true));
      }
      // On divise la quantité saisie en UCA par ce rapport
      BigDecimal[] division = ligneAchatArticle.getPrixAchat().getQuantiteReliquatUCA().divideAndRemainder(nbrUCAparUCS);
      
      // Si on a une quantité en UCA supérieure au nombre d'UCA contenu par une UCS on affiche le détail
      if (division[0].compareTo(BigDecimal.ZERO) > 0) {
        // Partie entière (nombre d'UCS)
        messageUCS = "Soit " + Constantes.formater(division[0], false) + " " + getLibelleUnite(article.getIdUCS());
        // reste en UCA
        if (division[1].compareTo(BigDecimal.ZERO) > 0) {
          messageUCS +=
              " et " + Constantes.formater(division[1], false) + " " + getLibelleUnite(ligneAchatArticle.getPrixAchat().getIdUCA());
        }
      }
      // On affiche le rapport de l'UCA au conditionnement de stock
      messageUCS += String.format(" (Conditionnement %s %s/%s)", Constantes.formater(nbrUCAparUCS, false),
          ligneAchatArticle.getPrixAchat().getIdUCA().getCode(), article.getIdUCS());
      
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne la précision d'une unité donnée
   */
  private int retournerPrecisionUnite(IdUnite pIdUnite) {
    if (listeUnite == null) {
      chargerListeUnites();
    }
    return listeUnite.getPrecisionUnite(pIdUnite);
  }
  
  /**
   * Lit le premier mémo de l'article.
   */
  private void lireBlocNoteCNA() {
    if (article == null || fournisseur == null) {
      return;
    }
    blocNoteCNA = ManagerServiceArticle.chargerBlocNoteConditionAchat(getIdSession(), article.getId(), fournisseur.getId());
  }
  
  /**
   * Retourne les données d'un article
   */
  private Article lireArticle(IdArticle pIdArticle) {
    // Lecture des informations complètes de l'article
    Article article = new Article(pIdArticle);
    article = ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
    return article;
  }
  
  /**
   * Charge les URL associées à un article
   * si une URL n'est pas valide elle est ignorée
   */
  private String[] chargerURL(Article aArticle) {
    String[] listeURLlues = ManagerServiceArticle.chargerListeURLArticle(getIdSession(), article);
    
    for (int i = 0; i < NOMBREMAXURL; i++) {
      if (i >= listeURLlues.length) {
        listeURL[i] = "";
      }
      else {
        if (getSession().getLexique().isValidURL(listeURLlues[i])) {
          listeURL[i] = listeURLlues[i];
        }
        else {
          listeURL[i] = "";
        }
      }
    }
    return listeURL;
  }
  
  /**
   * Calcule et renvoit le délai d'approvisionnement
   */
  public int getDelaiApprovisionnement() {
    if (conditionAchat == null) {
      return 0;
    }
    if (Constantes.equals(conditionAchat.getDelaiJours(), 'J')) {
      return (conditionAchat.getDelaiLivraison() + conditionAchat.getDelaiSupplementaire());
    }
    else {
      return ((conditionAchat.getDelaiLivraison() * 7) + (conditionAchat.getDelaiSupplementaire() * 7));
    }
  }
  
  /**
   * Retourne le texte du mémo.
   */
  public String retournerTexteBlocNoteCNA() {
    if (blocNoteCNA != null) {
      return blocNoteCNA.getTexte();
    }
    return "";
  }
  
  /**
   * Modifier prix achat brut.
   */
  public void modifierPrixAchatBrutHT(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPrixAchatBrutHT(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 1 fournisseur.
   */
  public void modifierPourcentageRemise1(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPourcentageRemise1(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 2 fournisseur.
   */
  public void modifierPourcentageRemise2(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPourcentageRemise2(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 3 fournisseur.
   */
  public void modifierPourcentageRemise3(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPourcentageRemise3(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 4 fournisseur.
   */
  public void modifierPourcentageRemise4(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPourcentageRemise4(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage de majoration.
   */
  public void modifierPourcentageTaxeOuMajoration(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPourcentageTaxeOuMajoration(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur de conditionnement.
   */
  public void modifierMontantConditionnementAchat(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setMontantConditionnement(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur des frais de port.
   */
  public void modifierMontantAuPoidsPortHT(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setMontantAuPoidsPortHT(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le mode de saisie du port.
   */
  public void modifierModeSaisiePort(EnumModeSaisiePort pModeSaisiePort) {
    if (Constantes.equals(pModeSaisiePort, ligneAchatArticle.getPrixAchat().getModeSaisiePort())) {
      return;
    }
    ligneAchatArticle.getPrixAchat().setModeSaisiePort(pModeSaisiePort, true);
    rafraichir();
  }
  
  /**
   * Modifier le poids des frais de port.
   */
  public void modifierPoidsPort(String pSaisie) {
    ligneAchatArticle.getPrixAchat().setPoidsPort(Constantes.convertirTexteEnBigDecimal(pSaisie), true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage des frais de port.
   */
  public void modifierPourcentagePort(String pSaisie) {
    BigPercentage bigPercentage = new BigPercentage(pSaisie, EnumFormatPourcentage.POURCENTAGE);
    ligneAchatArticle.getPrixAchat().setPourcentagePort(bigPercentage, true);
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Retourne la précision d'une unité de stock
   */
  private int retournerPrecisionUniteStock() {
    if (listeUnite != null && article != null && article.getIdUS() != null) {
      return listeUnite.getPrecisionUnite(article.getIdUS());
    }
    return 0;
  }
  
  /**
   * Retourne la précision d'une unité de conditionnement de stock
   */
  private int retournerPrecisionUCS() {
    if (listeUnite != null && article != null && article.getIdUS() != null) {
      return listeUnite.getPrecisionUnite(article.getIdUCS());
    }
    return 0;
  }
  
  // -- Accesseurs
  
  public LigneAchat getLigneAchat() {
    return ligneAchatArticle;
  }
  
  public LigneAchatArticle getLigneAchatArticle() {
    return ligneAchatArticle;
  }
  
  public ListeStockComptoir getListeStock() {
    return listeStock;
  }
  
  public List<LigneAttenduCommande> getListeLignesAttCmd() {
    return listeLignesAttCmd;
  }
  
  public int getOngleActif() {
    return ongletActif;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public void setArticle(Article articleEnCours) {
    this.article = articleEnCours;
  }
  
  public Fournisseur getFournisseur() {
    return fournisseur;
  }
  
  public void setFournisseur(Fournisseur fournisseur) {
    this.fournisseur = fournisseur;
  }
  
  public String[] getListeURL() {
    return listeURL;
  }
  
  public Date getDateApplicationAchat() {
    if (conditionAchat == null) {
      return null;
    }
    return conditionAchat.getDateApplication();
  }
  
  public int getComposantAyantLeFocus() {
    int valeur = composantAyantLeFocus;
    composantAyantLeFocus = -1;
    return valeur;
  }
  
  public boolean isSaisieEnUS() {
    return isSaisieEnUS;
  }
  
  public PrixAchat getPrixAchat() {
    if (ligneAchatArticle == null) {
      return null;
    }
    return ligneAchatArticle.getPrixAchat();
  }
  
  public VueAffichageStockArticle getVueStock() {
    return vueStock;
  }
  
  public VueAffichageMouvementsStock getVueMouvements() {
    return vueMouvements;
  }
  
  public VueAffichageAttendusCommandes getVueAttendus() {
    return vueAttendus;
  }
  
  public int getSousOngletActif() {
    return sousOngletActif;
  }
  
  public SessionBase getSessionPanel() {
    return getSession();
  }
  
  public String getMessageUCS() {
    return messageUCS;
  }
  
  public LienLigne getLienOrigine() {
    return lienOrigine;
  }
  
  public boolean isAppelDepuisRechercheArticle() {
    return appelDepuisRechercheArticle;
  }
}
