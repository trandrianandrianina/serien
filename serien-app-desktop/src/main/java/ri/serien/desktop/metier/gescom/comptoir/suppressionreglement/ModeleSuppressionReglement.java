/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.suppressionreglement;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boite de dialogue qui permet de choisir le mode d'effacement d'un règlement. Il peut y avoir 2 choix possibles:
 * - Effacer la saisie qui vient d'être effectuée afin de revenir aux règlements qui est stockés en base.
 * - Effacer la totalité des règlements si ces derniers ont été effectué dans la journée et ne sont pas remis en banque.
 */
public class ModeleSuppressionReglement extends AbstractModeleDialogue {
  // Constantes
  public static final int EFFACER_RIEN = 0;
  public static final int EFFACER_SAISIE = 1;
  public static final int EFFACER_TOUT = 2;
  
  // Variables
  private ModeleComptoir modeleParent = null;
  private int choix = EFFACER_RIEN;
  private boolean afficherChoixEffacerSaisie = false;
  private boolean afficherChoixEffacerTout = false;
  
  /**
   * Constructeur.
   */
  public ModeleSuppressionReglement(SessionBase pSession, ModeleComptoir pModeleParent) {
    super(pSession);
    modeleParent = pModeleParent;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    afficherChoixEffacerSaisie = modeleParent.isPossibleEffacerSaisieReglement();
    afficherChoixEffacerTout = modeleParent.isPossibleEffacerToutReglement();
  }
  
  @Override
  public void chargerDonnees() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifie le choix du type d'effacement des règlements.
   */
  public void modifierChoixTypeEffacement(int pChoix) {
    if (pChoix == choix) {
      return;
    }
    choix = pChoix;
    rafraichir();
  }
  
  // -- Accesseurs
  
  public ModeleComptoir getModeleParent() {
    return modeleParent;
  }
  
  public boolean isAfficherChoixEffacerSaisie() {
    return afficherChoixEffacerSaisie;
  }
  
  public boolean isAfficherChoixToutEffacer() {
    return afficherChoixEffacerTout;
  }
  
  public boolean isEffacerSaisie() {
    if (choix == EFFACER_RIEN && !modeleParent.isPossibleEffacerToutReglement()) {
      choix = EFFACER_SAISIE;
    }
    if (choix == EFFACER_SAISIE) {
      return true;
    }
    return false;
  }
  
  public boolean isEffacerTout() {
    if (choix == EFFACER_RIEN && !modeleParent.isPossibleEffacerSaisieReglement()) {
      choix = EFFACER_TOUT;
    }
    if (choix == EFFACER_TOUT) {
      return true;
    }
    return false;
  }
}
