/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.sipe;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JViewport;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumStatutSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.OutilGraphique;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueImportationPlanDePoseSipe extends AbstractVueDialogue<ModeleImportationPlanDePoseSipe> {
  // Constantes
  private static final String[] TITRE_LISTE_PLAN =
      new String[] { "Date", "Affaire", "Plan", "Chantier", "Entreprise", "Bâtiment", "Type", "Statut" };
  private static final String BOUTON_IMPORTER = "Importer";
  private static final String BOUTON_SUPPRIMER = "Supprimer";
  private static final String BOUTON_SAUVEGARDER = "Sauvegarder";
  
  /**
   * Constructeur.
   */
  public VueImportationPlanDePoseSipe(ModeleImportationPlanDePoseSipe pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setSize(1200, 600);
    setResizable(false);
    
    // Renseigner les filtres des statuts
    cbFiltreStatut.removeAllItems();
    cbFiltreStatut.addItem("Tous");
    for (EnumStatutSipe statut : EnumStatutSipe.values()) {
      cbFiltreStatut.addItem(statut);
    }
    cbFiltreStatut.removeItem(EnumStatutSipe.SUPPRIME);
    
    // La liste
    scpListePlanPose.getViewport().setBackground(Color.WHITE);
    tblListePlanPose.personnaliserAspect(TITRE_LISTE_PLAN, new int[] { 100, 150, 120, 200, 150, 150, 80, 80 },
        new int[] { 100, 150, 150, -1, 150, 150, 80, 80 }, new int[] { NRiTable.CENTRE, NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE,
            NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE },
        13);
    // Permet de redéfinir la touche Enter sur la liste
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER ferme la fenêtre
        if (tblListePlanPose.getSelectedRowCount() > 0) {
          validerListeSelection();
        }
      }
    };
    tblListePlanPose.modifierAction(nouvelleAction, SNCharteGraphique.TOUCHE_ENTREE);
    tblListePlanPose.getTableHeader().addMouseListener(new MouseAdapter() {
      @Override
      public void mouseClicked(MouseEvent e) {
        getModele().modifierPlagePlanPose(0, tblListePlanPose.getModel().getRowCount());
      }
    });
    
    // Ajouter un listener si la zone d'affichage est modifiée
    JViewport viewportPlanPose = scpListePlanPose.getViewport();
    viewportPlanPose.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpPlanPoseEnCoursStateChanged(e);
      }
    });
    scpListePlanPose.setViewport(viewportPlanPose);
    
    // Permet de redéfinir la touche Bas et Haut sur la liste
    final Action originalActionHaut = OutilGraphique.retournerActionComposant(tblListePlanPose, SNCharteGraphique.TOUCHE_HAUT);
    final Action originalActionBas = OutilGraphique.retournerActionComposant(tblListePlanPose, SNCharteGraphique.TOUCHE_BAS);
    Action nouvelleActionFlecheHaut = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionHaut.actionPerformed(ae);
        // Active les boutons si besoin
        rafraichirBouton();
      }
    };
    Action nouvelleActionFlecheBas = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Exécute l'action d'origine de la jTable
        originalActionBas.actionPerformed(ae);
        // Active les boutons si besoin
        rafraichirBouton();
      }
    };
    tblListePlanPose.modifierAction(nouvelleActionFlecheHaut, SNCharteGraphique.TOUCHE_HAUT);
    tblListePlanPose.modifierAction(nouvelleActionFlecheBas, SNCharteGraphique.TOUCHE_BAS);
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer le menu contextuel de la barre de boutons
    snBarreBouton.setMenuContextuel(pmListeOption);
    
    // Configurer la barre de boutons
    // snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(BOUTON_IMPORTER, 'i', false);
    snBarreBouton.ajouterBouton(BOUTON_SAUVEGARDER, 's', false);
    snBarreBouton.ajouterBouton(BOUTON_SUPPRIMER, 'e', false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirFiltrePlageDate();
    rafraichirFiltreStatut();
    rafraichirTitreResultat();
    rafraichirListe();
    rafraichirBouton();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit le filtre sur la plage de date.
   */
  private void rafraichirFiltrePlageDate() {
    CritereSipe critere = getModele().getCritereSipe();
    if (critere == null) {
      snFiltrePlageDate.setDateDebut(null);
      snFiltrePlageDate.setDateFin(null);
      return;
    }
    
    snFiltrePlageDate.setDateDebut(critere.getDateDebutPlan());
    snFiltrePlageDate.setDateFin(critere.getDateFinPlan());
  }
  
  /**
   * Rafraichit le filtre sur le statut.
   */
  private void rafraichirFiltreStatut() {
    CritereSipe critere = getModele().getCritereSipe();
    if (critere == null) {
      cbFiltreStatut.setSelectedIndex(0);
      return;
    }
    
    List<EnumStatutSipe> listeStatut = critere.getListeStatut();
    // Sélection du statut uniquement s'il n'y en a qu'un seul
    if (listeStatut != null && listeStatut.size() == 1) {
      cbFiltreStatut.setSelectedItem(listeStatut.get(0));
    }
    // Sinon c'est le statut "Tous" qui est sélectionné (notamment s'il y a plusieurs statuts car c'est équivalent à "Tous")
    else {
      cbFiltreStatut.setSelectedIndex(0);
    }
  }
  
  /**
   * Rafraichit le titre du résultat de la recherche.
   */
  private void rafraichirTitreResultat() {
    Message message = getModele().getTitreResultatRecherche();
    lblTitreResultat.setMessage(message);
  }
  
  /**
   * Rafraichit la liste des plans de pose.
   */
  private void rafraichirListe() {
    ListePlanPoseSipe liste = getModele().getListePlanPose();
    String[][] donnees = null;
    
    if (liste != null) {
      donnees = new String[liste.size()][TITRE_LISTE_PLAN.length];
      for (int ligne = 0; ligne < liste.size(); ligne++) {
        PlanPoseSipe planPose = liste.get(ligne);
        if (planPose != null) {
          donnees[ligne][0] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, planPose.getDatePlan());
          donnees[ligne][1] = planPose.getNumeroAffaire();
          donnees[ligne][2] = planPose.getNumeroPlan();
          donnees[ligne][3] = planPose.getNomChantier();
          donnees[ligne][4] = planPose.getNomEntreprise();
          donnees[ligne][5] = planPose.getNomBatiment();
          if (planPose.getTypePlanPoseSipe() != null) {
            donnees[ligne][6] = planPose.getTypePlanPoseSipe().getLibelle();
          }
          else {
            donnees[ligne][6] = "";
          }
          if (planPose.getStatut() != null) {
            donnees[ligne][7] = planPose.getStatut().getLibelle();
          }
          else {
            donnees[ligne][7] = "";
          }
        }
      }
    }
    tblListePlanPose.mettreAJourDonnees(donnees);
    
    // Afficher le début de la liste (attention à faire avant la sélection de la ligne)
    if (liste != null && liste.isAfficherDebutListe()) {
      tblListePlanPose.getSelectionModel().setSelectionInterval(0, 0);
      tblListePlanPose.scrollRectToVisible(new Rectangle(tblListePlanPose.getCellRect(0, 0, true)));
    }
    // Sélectionner la ligne en cours de sélection
    int index = getModele().getIndexPlanPoseSelectionne();
    if (index > -1 && index < tblListePlanPose.getRowCount()) {
      if (tblListePlanPose.getRowSorter() != null) {
        index = tblListePlanPose.getRowSorter().convertRowIndexToView(index);
      }
      tblListePlanPose.getSelectionModel().addSelectionInterval(index, index);
    }
    else {
      tblListePlanPose.clearSelection();
    }
  }
  
  /**
   * Afficher les boutons.
   */
  private void rafraichirBouton() {
    rafraichirBoutonImporter();
    rafraichirBoutonSauvegarder();
    rafraichirBoutonSupprimer();
  }
  
  /**
   * Afficher le bouton "Importer".
   */
  private void rafraichirBoutonImporter() {
    boolean actif = true;
    PlanPoseSipe planPoseSipe = getModele().getPlanPoseSelectionne();
    if (planPoseSipe == null) {
      actif = false;
    }
    else if (planPoseSipe.getStatut() == EnumStatutSipe.SUPPRIME) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_IMPORTER, actif);
  }
  
  /**
   * Afficher le bouton "Sauvegarder".
   */
  private void rafraichirBoutonSauvegarder() {
    boolean actif = true;
    PlanPoseSipe planPoseSipe = getModele().getPlanPoseSelectionne();
    if (planPoseSipe == null) {
      actif = false;
    }
    else if (planPoseSipe.getStatut() == EnumStatutSipe.SUPPRIME) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_SAUVEGARDER, actif);
  }
  
  /**
   * Afficher le bouton "Supprimer".
   */
  private void rafraichirBoutonSupprimer() {
    boolean actif = true;
    PlanPoseSipe planPoseSipe = getModele().getPlanPoseSelectionne();
    if (planPoseSipe == null) {
      actif = false;
    }
    else if (planPoseSipe.getStatut() == EnumStatutSipe.SUPPRIME) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_SUPPRIMER, actif);
  }
  
  /**
   * Récupère le plan de pose sélectionné.
   */
  private void validerListeSelection() {
    int indexreel = -1;
    int indexvisuel = tblListePlanPose.getSelectedRow();
    if (indexvisuel >= 0) {
      indexreel = indexvisuel;
      if (tblListePlanPose.getRowSorter() != null) {
        indexreel = tblListePlanPose.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
    }
    getModele().modifierPlanPoseSelectionne(indexreel);
  }
  
  // -- Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().lancerRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche(true);
      }
      else if (pSNBouton.isBouton(BOUTON_IMPORTER)) {
        getModele().importerPlanPose();
      }
      else if (pSNBouton.isBouton(BOUTON_SAUVEGARDER)) {
        getModele().sauvegarderPlanPose();
      }
      else if (pSNBouton.isBouton(BOUTON_SUPPRIMER)) {
        getModele().supprimerPlanPose();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListePlanPoseMouseClicked(MouseEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      
      validerListeSelection();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbFiltreStatutItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (cbFiltreStatut.getSelectedItem() instanceof EnumStatutSipe) {
        getModele().modifierFiltreStatut((EnumStatutSipe) cbFiltreStatut.getSelectedItem());
      }
      else {
        getModele().modifierFiltreStatut(null);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void snFiltrePlageDateValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierDebutPlage(snFiltrePlageDate.getDateDebut());
      getModele().modifierFinPlage(snFiltrePlageDate.getDateFin());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void scpPlanPoseEnCoursStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListePlanPose == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListePlanPose.getViewport().getViewRect();
      int premiereLigne = tblListePlanPose.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListePlanPose.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListePlanPose.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlagePlanPose(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlPrincipal = new JPanel();
    pnlContenu = new SNPanelContenu();
    pnlFiltre = new SNPanel();
    pnlFiltreGauche = new SNPanel();
    lbPlageDate = new SNLabelChamp();
    snFiltrePlageDate = new SNPlageDate();
    lbStatut = new SNLabelChamp();
    cbFiltreStatut = new SNComboBox();
    pnlFiltreDroit = new SNPanel();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    scpListePlanPose = new JScrollPane();
    tblListePlanPose = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    pmListeOption = new JPopupMenu();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 600));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setTitle("S\u00e9lection d'un plan de pose Sipe");
    setModal(true);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setBackground(new Color(238, 238, 210));
      pnlPrincipal.setPreferredSize(new Dimension(1220, 600));
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlContenu ========
      {
        pnlContenu.setBackground(new Color(238, 238, 210));
        pnlContenu.setPreferredSize(new Dimension(1220, 590));
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 1139, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 297, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setOpaque(false);
          pnlFiltre.setBorder(new TitledBorder(""));
          pnlFiltre.setMinimumSize(new Dimension(1200, 100));
          pnlFiltre.setPreferredSize(new Dimension(1200, 100));
          pnlFiltre.setName("pnlFiltre");
          pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlFiltreGauche ========
          {
            pnlFiltreGauche.setOpaque(false);
            pnlFiltreGauche.setPreferredSize(new Dimension(300, 100));
            pnlFiltreGauche.setMinimumSize(new Dimension(300, 100));
            pnlFiltreGauche.setName("pnlFiltreGauche");
            pnlFiltreGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltreGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFiltreGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlFiltreGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPlageDate ----
            lbPlageDate.setText("Plage de date");
            lbPlageDate.setName("lbPlageDate");
            pnlFiltreGauche.add(lbPlageDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFiltrePlageDate ----
            snFiltrePlageDate.setName("snFiltrePlageDate");
            snFiltrePlageDate.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snFiltrePlageDateValueChanged(e);
              }
            });
            pnlFiltreGauche.add(snFiltrePlageDate, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbStatut ----
            lbStatut.setText("Statut");
            lbStatut.setPreferredSize(new Dimension(200, 30));
            lbStatut.setMinimumSize(new Dimension(200, 30));
            lbStatut.setMaximumSize(new Dimension(200, 30));
            lbStatut.setName("lbStatut");
            pnlFiltreGauche.add(lbStatut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbFiltreStatut ----
            cbFiltreStatut.setPreferredSize(new Dimension(150, 30));
            cbFiltreStatut.setMinimumSize(new Dimension(150, 30));
            cbFiltreStatut.setName("cbFiltreStatut");
            cbFiltreStatut.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbFiltreStatutItemStateChanged(e);
              }
            });
            pnlFiltreGauche.add(cbFiltreStatut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlFiltre.add(pnlFiltreGauche);
          
          // ======== pnlFiltreDroit ========
          {
            pnlFiltreDroit.setName("pnlFiltreDroit");
            pnlFiltreDroit.setLayout(new BorderLayout());
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlFiltreDroit.add(snBarreRecherche, BorderLayout.SOUTH);
          }
          pnlFiltre.add(pnlFiltreDroit);
        }
        pnlContenu.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lblTitreResultat ----
        lblTitreResultat.setText("Liste des plans de pose");
        lblTitreResultat.setName("lblTitreResultat");
        pnlContenu.add(lblTitreResultat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListePlanPose ========
        {
          scpListePlanPose.setPreferredSize(new Dimension(1050, 350));
          scpListePlanPose.setMinimumSize(new Dimension(1050, 350));
          scpListePlanPose.setName("scpListePlanPose");
          
          // ---- tblListePlanPose ----
          tblListePlanPose.setShowVerticalLines(true);
          tblListePlanPose.setShowHorizontalLines(true);
          tblListePlanPose.setBackground(Color.white);
          tblListePlanPose.setRowHeight(20);
          tblListePlanPose.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
          tblListePlanPose.setSelectionBackground(new Color(57, 105, 138));
          tblListePlanPose.setPreferredScrollableViewportSize(new Dimension(450, 424));
          tblListePlanPose.setPreferredSize(new Dimension(870, 350));
          tblListePlanPose.setGridColor(new Color(204, 204, 204));
          tblListePlanPose.setMinimumSize(new Dimension(420, 20));
          tblListePlanPose.setOpaque(false);
          tblListePlanPose.setName("tblListePlanPose");
          tblListePlanPose.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListePlanPoseMouseClicked(e);
            }
          });
          scpListePlanPose.setViewportView(tblListePlanPose);
        }
        pnlContenu.add(scpListePlanPose, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlContenu, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlPrincipal, BorderLayout.CENTER);
    
    // ======== pmListeOption ========
    {
      pmListeOption.setName("pmListeOption");
    }
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlPrincipal;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlFiltre;
  private SNPanel pnlFiltreGauche;
  private SNLabelChamp lbPlageDate;
  private SNPlageDate snFiltrePlageDate;
  private SNLabelChamp lbStatut;
  private SNComboBox cbFiltreStatut;
  private SNPanel pnlFiltreDroit;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JScrollPane scpListePlanPose;
  private NRiTable tblListePlanPose;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu pmListeOption;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
