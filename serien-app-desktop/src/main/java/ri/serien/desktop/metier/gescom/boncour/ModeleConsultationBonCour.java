/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue qui ajoute un bon de cour à un bon ou une facture d'enlèvement
 *
 */
public class ModeleConsultationBonCour extends AbstractModeleDialogue {
  
  // variables
  private BonCour bonCourSelectionne = null;
  private DocumentVente document = null;
  private ListeVendeur listeVendeur = null;
  private Client client = null;
  private Vendeur magasinier = null;
  
  /**
   * Constructeur.
   */
  public ModeleConsultationBonCour(SessionBase pSession, BonCour pBonCour) {
    super(pSession);
    bonCourSelectionne = pBonCour;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    
  }
  
  // Chargement des données d'affichage du bon de cour
  @Override
  public void chargerDonnees() {
    // Liste vendeurs
    listeVendeur = new ListeVendeur();
    listeVendeur = listeVendeur.charger(getIdSession(), getIdEtablissement());
    
    // Document
    IdDocumentVente idDocument = bonCourSelectionne.getIdDocumentGenere();
    if (idDocument != null) {
      document = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), idDocument);
    }
    
    // Client
    if (document != null) {
      client = document.chargerClientFacture(getIdSession());
    }
    
    // Magasinier
    CarnetBonCour carnet =
        ManagerServiceDocumentVente.chargerCarnetBonCour(getIdSession(), bonCourSelectionne.getId().getIdCarnetBonCour());
    magasinier = listeVendeur.get(carnet.getIdMagasinier());
  }
  
  // Accesseurs
  
  /**
   * Identifiant de l'établissement du bon de cour affiché
   */
  public IdEtablissement getIdEtablissement() {
    return bonCourSelectionne.getId().getIdCarnetBonCour().getIdEtablissement();
  }
  
  /**
   * Bon de cour affiché
   */
  public BonCour getBonCourSelectionne() {
    return bonCourSelectionne;
  }
  
  /**
   * Document de vente associé au bon de cour affiché
   */
  public DocumentVente getDocumentVente() {
    return document;
  }
  
  /**
   * Magasinier (Vendeur) de vente associé au bon de cour affiché
   */
  public Vendeur getMagasinier() {
    return magasinier;
  }
  
  /**
   * Client associé au bon de cour affiché
   */
  public Client getClient() {
    return client;
  }
  
}
