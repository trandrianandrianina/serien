/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisonnementhorsstockclient;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Description d'une ligne à afficher dans le tableau des commandes d'achat en cours pour un article.
 */
public class LigneAAfficher {
  // Variables de travail
  private IdLigneVente idLigneVente = null;
  private Article article = null;
  private boolean charge = false;
  private LigneAchatArticle ligneAchatAssociee = null;
  
  // Variables à afficher
  private String quantiteCommandeeUCA = null;
  private String uniteCommandeUCA = null;
  private String raisonSociale = null;
  private String documentVente = null;
  private String dateLivraisonPrevue = null;
  private String libelle = null;
  private String typeArticle = null;
  private String stockReel = null;
  private String stockReserve = null;
  private String stockAttendu = null;
  private String stockATerme = null;
  
  /**
   * Constructeur.
   */
  public LigneAAfficher(IdLigneVente pIdLigneVente) {
    idLigneVente = pIdLigneVente;
  }
  
  // -- Méthodes public
  
  /**
   * Initialise les données.
   */
  public void initialiserDonnees(ReapprovisionnementClient pReapprovisionnement) {
    // Initialisation des informations provenant de l'article
    if (pReapprovisionnement == null) {
      return;
    }
    article = pReapprovisionnement.getArticle();
    if (article == null) {
      throw new MessageErreurException("L'article à approvisionner est invalide.");
    }
    Client client = pReapprovisionnement.getClient();
    IdLigneVente idLigneVente = pReapprovisionnement.getIdLigneVente();
    
    // Les données du client
    if (client != null) {
      raisonSociale = client.getAdresse().getNom();
    }
    
    // Les données de la ligne de vente
    if (idLigneVente != null) {
      // TODO 2019-01-09 A modifier lorsque le service listeLigneVenteBase sera fait
      IdDocumentVente idDocumentLigneVente = IdDocumentVente.getInstanceHorsFacture(idLigneVente.getIdEtablissement(),
          idLigneVente.getCodeEntete(), idLigneVente.getNumero(), idLigneVente.getSuffixe());
      
      documentVente = idDocumentLigneVente.toString();
    }
    
    // La date
    dateLivraisonPrevue = Constantes.convertirDateEnTexte(pReapprovisionnement.getDateLivraisonPrevue());
    
    // Alimentation des variables qui seront affichées dans le tableau
    quantiteCommandeeUCA = "0";
    uniteCommandeUCA = "";
    
    // Les données de l'article
    if (article.getPrixAchat() != null) {
      if (article.getPrixAchat().getQuantiteReliquatUCA() != null) {
        quantiteCommandeeUCA = Constantes.formater(article.getPrixAchat().getQuantiteReliquatUCA(), false);
      }
      if (article.getPrixAchat().getIdUCA() != null) {
        uniteCommandeUCA = article.getPrixAchat().getIdUCA().getCode();
      }
    }
    
    libelle = Constantes.normerTexte(article.getLibelle1());
    
    // Renseigner le type d'articles
    if (article.isHorsGamme()) {
      typeArticle = Article.LIBELLE_HORS_GAMME;
    }
    else if (article.getTypeArticle() != null) {
      typeArticle = article.getTypeArticle().getLibelle();
    }
    else {
      typeArticle = "";
    }
    
    stockReel = Constantes.formater(article.getQuantitePhysique(), false);
    stockReserve = Constantes.formater(article.getQuantiteReservee(), false);
    stockAttendu = Constantes.formater(article.getQuantiteAttendueUCA(), false);
    // Calcul du stock à terme
    BigDecimal stock = article.calculerStockDisponible();
    if (stock == null) {
      stockATerme = "";
    }
    else {
      stockATerme = Constantes.formater(stock, false);
    }
    
    // Si on est arrivé ici alors la ligne est complètement chargée
    charge = true;
  }
  
  /**
   * Contrôle la quantité commandée saisie.
   */
  public void controlerQuantite(String pQuantiteSaisie) {
    if (pQuantiteSaisie == null) {
      return;
    }
    
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pQuantiteSaisie);
    if (quantite == null || quantite.compareTo(BigDecimal.ZERO) < 0) {
      throw new MessageErreurException("La quantité saisie est invalide, elle doit être supérieure à 0.");
    }
    // On contrôle que la quantité commandée saisie n'entraîne pas un stock à terme supérieur au stock maximum
    BigDecimal stockaterme = article.calculerStockDisponible();
    if (stockaterme == null) {
      throw new MessageErreurException("Le stock à terme de cet article ne peut être calculer.");
    }
    if (article.getQuantiteStockMaximumUCA() == null) {
      throw new MessageErreurException("Le stock maximum est invalide.");
    }
    
    // On ajoute un contrôle si le stock maximum est supérieur à 0
    // Le stock à terme doit être inférieur ou égal au stock maximum
    if (article.getQuantiteStockMaximumUCA().compareTo(BigDecimal.ZERO) > 0) {
      stockaterme = stockaterme.add(quantite);
      if (stockaterme.compareTo(article.getQuantiteStockMaximumUCA()) > 0) {
        throw new MessageErreurException("La quantité saisie (" + Constantes.formater(quantite, false)
            + ") est trop grande ce qui entraîne un stock à terme (" + Constantes.formater(stockaterme, false)
            + ") supérieur au stock maximum (" + Constantes.formater(article.getQuantiteStockMaximumUCA(), false) + ").");
      }
    }
  }
  
  /**
   * Modifie la quantité commandée.
   */
  public void modifierQuantite(BigDecimal pQuantiteSaisie) {
    quantiteCommandeeUCA = "0";
    if (pQuantiteSaisie == null) {
      return;
    }
    
    // On met à jour la quantité saisie dans la classe PrixAchat
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getIdUCA() != null) {
      article.getPrixAchat().setQuantiteReliquatUCA(pQuantiteSaisie, true);
    }
    quantiteCommandeeUCA = Constantes.formater(pQuantiteSaisie, false);
  }
  
  /**
   * Vérifie si l'article à une quantité surpérieure à 0.
   */
  public boolean isArticleACommander() {
    if (article != null && article.getPrixAchat() != null && article.getPrixAchat().getQuantiteReliquatUCA() != null) {
      return article.getPrixAchat().getQuantiteReliquatUCA().compareTo(BigDecimal.ZERO) > 0;
    }
    return false;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public boolean isCharge() {
    return charge;
  }
  
  public IdLigneVente getIdLigneVente() {
    return idLigneVente;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public String getRaisonSociale() {
    return raisonSociale;
  }
  
  public String getDocumentVente() {
    return documentVente;
  }
  
  public String getDateLivraisonPrevue() {
    return dateLivraisonPrevue;
  }
  
  public String getLibelle() {
    return libelle;
  }
  
  public BigDecimal getQuantiteCommandeeUCABigDecimal() {
    return Constantes.convertirTexteEnBigDecimal(Constantes.normerTexte(quantiteCommandeeUCA));
  }
  
  public String getQuantiteCommandeeUCA() {
    return quantiteCommandeeUCA;
  }
  
  public String getUniteCommandeUCA() {
    return uniteCommandeUCA;
  }
  
  public String getTypeArticle() {
    return typeArticle;
  }
  
  public String getStockReel() {
    return stockReel;
  }
  
  public String getStockReserve() {
    return stockReserve;
  }
  
  public String getStockAttendu() {
    return stockAttendu;
  }
  
  public String getStockATerme() {
    return stockATerme;
  }
  
  public LigneAchatArticle getLigneAchatAssociee() {
    return ligneAchatAssociee;
  }
  
  public void setLigneAchatAssociee(LigneAchatArticle ligneAchatAssociee) {
    this.ligneAchatAssociee = ligneAchatAssociee;
  }
  
}
