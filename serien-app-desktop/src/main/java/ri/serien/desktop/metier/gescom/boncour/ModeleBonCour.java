/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.client.ClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modele de l'écran de recherche et d'affichage des bons de cour
 */
public class ModeleBonCour extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Bons de cour";
  
  // Informations générales
  private ListeEtablissement listeEtablissement = null;
  private UtilisateurGescom utilisateurGescom = null;
  private Etablissement etablissement = null;
  private IdMagasin idMagasin = null;
  private Message titreResultat = null;
  private CritereBonCour critereBonCour = null;
  
  // Variables
  private ListeBonCour listeBonCour = null;
  private int numeroCarnet = 0;
  private int numeroBonCour = 0;
  private Vendeur magasinier = null;
  private EnumEtatBonCour etatBonCour = null;
  private ListeVendeur listeVendeur = null;
  private ListeCarnetBonCour listeCarnet = null;
  private ListeDocumentVenteBase listeDocumentVenteBase = null;
  private BonCour bonCourSelectionne = null;
  private int indiceSelection = -1;
  private ListeClientBase listeClientBase = new ListeClientBase();
  
  /**
   * Constructeur
   */
  public ModeleBonCour(SessionBase pSession) {
    super(pSession);
    getSession().getPanel().setName(TITRE);
  }
  
  /**
   * Initialisation
   */
  @Override
  public void initialiserDonnees() {
    titreResultat = null;
    
  }
  
  /**
   * Chargement des données de base
   */
  @Override
  public void chargerDonnees() {
    // Récupération des infos de l'utilisateur en gescom
    utilisateurGescom = chargerInformationsGescom(null);
    if (utilisateurGescom == null) {
      throw new MessageErreurException("Les données de l'utilisateur n'ont pas pu être chargées.");
    }
    
    // Charger la liste des établissements
    listeEtablissement = ListeEtablissement.charger(getSession().getIdSession());
    
    // Récupérer l'établissement courant
    if (utilisateurGescom.getIdEtablissementPrincipal() != null) {
      etablissement = listeEtablissement.getEtablissementParId(utilisateurGescom.getIdEtablissementPrincipal());
    }
    else {
      etablissement = listeEtablissement.get(0);
    }
    
    // Chargement de toutes les données liées à l'établissement
    chargerDonneesLieesEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    
    // Construction du titre de la liste
    construireTitre();
  }
  
  // Méthodes publiques
  
  /**
   * Initialiser le résultat de la recherche (remet les valeurs par défaut aux filtres).
   */
  public void initialiserRecherche() {
    // Initialiser les filtres
    critereBonCour = new CritereBonCour();
    numeroBonCour = 0;
    numeroCarnet = 0;
    etatBonCour = null;
    magasinier = null;
    if (utilisateurGescom.getIdEtablissementPrincipal() != null) {
      etablissement = listeEtablissement.getEtablissementParId(utilisateurGescom.getIdEtablissementPrincipal());
    }
    else {
      etablissement = listeEtablissement.get(0);
    }
    chargerDonneesLieesEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    
    // Effacer les données
    listeBonCour = null;
    listeClientBase = new ListeClientBase();
    
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  /**
   * Recherche des bons de cour
   */
  public void rechercher() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeBonCour != null) {
      taillePage = listeBonCour.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeBonCour = null;
    rafraichir();
    
    // Renseigner les critères de recherche
    critereBonCour = new CritereBonCour();
    critereBonCour.setIdEtablissement(etablissement.getId());
    critereBonCour.setIdMagasin(idMagasin);
    if (numeroBonCour > 0) {
      critereBonCour.setNumeroBonCour(numeroBonCour);
    }
    if (magasinier != null) {
      critereBonCour.setIdMagasinier(magasinier.getId());
    }
    if (numeroCarnet > 0) {
      critereBonCour.setIdCarnetBonCour(IdCarnetBonCour.getInstance(etablissement.getId(), idMagasin, numeroCarnet));
    }
    
    if (etatBonCour != null) {
      if (etatBonCour.equals(EnumEtatBonCour.MANQUANT)) {
        critereBonCour.setEtatBonCour(EnumEtatBonCour.NON_UTILISE);
      }
      else {
        critereBonCour.setEtatBonCour(etatBonCour);
      }
    }
    
    // Charger la liste des identifiants des bons de cour correspondant au résultat de la recherche
    List<IdBonCour> listeIdBonCour = ManagerServiceDocumentVente.chargerListeIdBonCour(getIdSession(), critereBonCour);
    // On constitue parallèlement une liste des carnets correspondants aux critères pour disposer de toutes les informations à l'affichage
    List<IdCarnetBonCour> listeIdCarnet = ManagerServiceDocumentVente.chargerListeIdCarnetBonCour(getIdSession(), critereBonCour);
    
    // Si nécessaire on filtre les manquants ou les non-utilisé. L'état "manquant" n'est pas persisté en base de donnée, il est déduit. En
    // base de donnée le bon de cour manquant sera "non-utilisé"
    listeIdBonCour = filtrerManquant(listeIdBonCour);
    
    // Créer le résulat de la recherche
    listeBonCour = ListeBonCour.creerListeNonChargee(listeIdBonCour);
    
    // Charger liste des carnets
    listeCarnet = new ListeCarnetBonCour();
    if (listeIdCarnet != null && listeIdCarnet.size() > 0) {
      listeCarnet = listeCarnet.charger(getIdSession(), listeIdCarnet);
    }
    
    // Charger la première page
    ListeBonCour listeBonCourCharge = listeBonCour.chargerPremierePage(getIdSession(), taillePage);
    chargerComplement(listeBonCourCharge);
    
    // Construire le titre
    construireTitre();
    
    // Rafraîchir
    rafraichir();
  }
  
  /**
   * Retourner le nom d'un magasinier (vendeur) à partir d'un numéro de bon de cour
   */
  public String retournerNomMagasinier(BonCour pBonCour) {
    if (pBonCour == null) {
      return "";
    }
    
    // Identifiant du carnet
    IdCarnetBonCour idCarnet = pBonCour.getId().getIdCarnetBonCour();
    
    // Chargement du carnet
    CarnetBonCour carnet = listeCarnet.get(idCarnet);
    
    Vendeur magasinierBonCour = listeVendeur.get(carnet.getIdMagasinier());
    if (magasinierBonCour == null) {
      return "";
    }
    
    return magasinierBonCour.getNomAvecCode();
  }
  
  /**
   * Afficher la plage de bons de cour comprise entre deux lignes.
   * Les informations des bons de cour sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageBonCourAffiche(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeBonCour != null) {
      ListeBonCour listeBonCourCharge = listeBonCour.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      
      // Charger les informations complémentaires
      chargerComplement(listeBonCourCharge);
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Annuler un manquant
   */
  public void annulerBonCour(int pIndice) {
    modifierBonCourSelectionne(pIndice);
    bonCourSelectionne.setEtatBonCour(EnumEtatBonCour.ANNULE);
    ManagerServiceDocumentVente.modifierBonCour(getIdSession(), bonCourSelectionne);
    rafraichir();
  }
  
  /**
   * Réactiver un annulé
   */
  public void reactiverBonCour(int pIndice) {
    modifierBonCourSelectionne(pIndice);
    bonCourSelectionne.setEtatBonCour(EnumEtatBonCour.NON_UTILISE);
    ManagerServiceDocumentVente.modifierBonCour(getIdSession(), bonCourSelectionne);
    rechercher();
  }
  
  /**
   * Modifier le filtre établissement
   */
  public void modifierEtablissement(Etablissement pEtablissement) {
    etablissement = pEtablissement;
    chargerDonneesLieesEtablissement(etablissement.getId());
  }
  
  /**
   * Modifier le filtre magasin
   */
  public void modifierMagasin(Magasin pMagasin) {
    if (pMagasin == null || Constantes.equals(idMagasin, pMagasin.getId())) {
      return;
    }
    idMagasin = pMagasin.getId();
  }
  
  /**
   * Modifier le filtre numéro de carnet
   */
  public void modifierNumeroCarnet(String pNumero) {
    if (pNumero.trim().isEmpty()) {
      numeroCarnet = 0;
    }
    else {
      numeroCarnet = Integer.parseInt(pNumero);
    }
  }
  
  /**
   * Modifier le filtre magasinier
   */
  public void modifierMagasinier(Vendeur pMagasinier) {
    magasinier = pMagasinier;
  }
  
  /**
   * Modifier le filtre numéro de bon de cour
   */
  public void modifierNumeroBonCour(String pNumero) {
    if (pNumero.trim().isEmpty()) {
      numeroBonCour = 0;
    }
    else {
      numeroBonCour = Integer.parseInt(pNumero);
    }
  }
  
  /**
   * Modifier le filtre état de bon de cour
   */
  public void modifierEtatBonCour(EnumEtatBonCour pEtat) {
    etatBonCour = pEtat;
  }
  
  /**
   * Modifier le bon de cour sélectionné
   */
  public void modifierBonCourSelectionne(int indexreel) {
    if (listeBonCour == null) {
      return;
    }
    bonCourSelectionne = listeBonCour.get(indexreel);
  }
  
  /**
   * Modifier l'indice du bon de cour sélectionné
   */
  public void modifierIndiceSelection(int pIndiceSelection) {
    indiceSelection = pIndiceSelection;
  }
  
  /**
   * Afficher le détail d'un bon de cour dans une boite de dialogue
   */
  public void afficherBonCour() {
    ModeleConsultationBonCour modeleConsultation = new ModeleConsultationBonCour(getSession(), bonCourSelectionne);
    VueConsultationBonCour vueConsultation = new VueConsultationBonCour(modeleConsultation);
    vueConsultation.afficher();
  }
  
  // Méthodes privées
  /**
   * Construction du titre de la liste suivant le résultat de recherche
   */
  private void construireTitre() {
    if (listeBonCour == null) {
      titreResultat = Message.getMessageNormal("Bons de cour correspondant à votre recherche");
    }
    else if (listeBonCour.size() == 1) {
      titreResultat = Message.getMessageNormal("Bon de cour correspondant à votre recherche (1)");
    }
    else if (listeBonCour.size() > 1) {
      titreResultat = Message.getMessageNormal("Bons de cour correspondant à votre recherche (" + listeBonCour.size() + ")");
    }
    else {
      titreResultat = Message.getMessageImportant("Aucun bon de cour ne correspond à votre recherche");
    }
  }
  
  /**
   * Charger les informations complémentaires d'une liste de document de ventes.
   * - document de vente
   * - chargement de la liste des clients facturés correspondant aux documents chargés
   */
  private void chargerComplement(ListeBonCour pListeBonCour) {
    if (pListeBonCour == null) {
      return;
    }
    // Complément du bon de cour : document lié
    for (BonCour bonCour : pListeBonCour) {
      if (bonCour.getIdDocumentGenere() != null && bonCour.getComplement() == null) {
        IdDocumentVente idDocument = bonCour.getIdDocumentGenere();
        DocumentVente document = null;
        try {
          document = ManagerServiceDocumentVente.chargerEnteteDocumentVente(getIdSession(), idDocument);
        }
        catch (Exception e) {
          // protection contre effacement du document joint
        }
        if (document != null) {
          bonCour.setComplement(document);
        }
      }
    }
    
    // Liste d'identifiants de clients
    ListeIdClient listeIdClient = new ListeIdClient();
    ListeClientBase listeClientCharge = new ListeClientBase();
    // On charge la liste des identifiants clients à partir des documents
    for (BonCour bonCour : pListeBonCour) {
      if (bonCour.getComplement() != null && bonCour.getComplement() instanceof DocumentVente) {
        listeIdClient.add(((DocumentVente) bonCour.getComplement()).getIdClientFacture());
      }
    }
    
    // On charge les clients
    if (listeIdClient.size() > 0) {
      listeClientCharge = ManagerServiceClient.chargerListeClientBase(getIdSession(), listeIdClient);
    }
    
    // On ajoute le client s'il n'est pas déjà dans la liste
    for (ClientBase clientBase : listeClientCharge) {
      if (!listeClientBase.contains(clientBase)) {
        listeClientBase.add(clientBase);
      }
    }
  }
  
  /**
   * Filtrer pour obtenir seulement les manquants ou seulement les non utilises
   * En base l'état "manquant" n'est pas persisté. Il est déduit par cette méthode.
   * On persiste seulement non utilisé, utilisé et annulé.
   */
  private List<IdBonCour> filtrerManquant(List<IdBonCour> pListeIdBonCour) {
    if (etatBonCour != null && etatBonCour.equals(EnumEtatBonCour.MANQUANT)) {
      List<IdBonCour> listeIdBonCourFiltree = new ArrayList<IdBonCour>();
      ListeBonCour listeFiltree = new ListeBonCour();
      listeFiltree = listeFiltree.charger(getIdSession(), pListeIdBonCour);
      for (BonCour bonCour : listeFiltree) {
        if (bonCour.getEtatBonCour().equals(EnumEtatBonCour.MANQUANT)) {
          listeIdBonCourFiltree.add(bonCour.getId());
        }
      }
      return listeIdBonCourFiltree;
    }
    else if (etatBonCour != null && etatBonCour.equals(EnumEtatBonCour.NON_UTILISE)) {
      List<IdBonCour> listeIdBonCourFiltree = new ArrayList<IdBonCour>();
      ListeBonCour listeFiltree = new ListeBonCour();
      listeFiltree = listeFiltree.charger(getIdSession(), pListeIdBonCour);
      for (BonCour bonCour : listeFiltree) {
        if (bonCour.getEtatBonCour().equals(EnumEtatBonCour.NON_UTILISE)) {
          listeIdBonCourFiltree.add(bonCour.getId());
        }
      }
      return listeIdBonCourFiltree;
    }
    return pListeIdBonCour;
  }
  
  /**
   * Retourne les informations gescom de l'utilisateur.
   */
  private UtilisateurGescom chargerInformationsGescom(IdEtablissement pIdEtablissement) {
    UtilisateurGescom utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getSession().getIdSession(),
        ManagerSessionClient.getInstance().getProfil(), ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    return utilisateurGescom;
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  private void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    
    // Récupération de la liste des magasins
    ListeMagasin listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getSession().getIdSession(), pIdEtablissement);
    idMagasin = utilisateurGescom.getIdMagasin();
    
    // Récupération de la liste des vendeurs
    listeVendeur = new ListeVendeur();
    listeVendeur = listeVendeur.charger(getSession().getIdSession(), pIdEtablissement);
  }
  
  // Accesseurs
  /**
   * Valeur du filtre identifiant du magasin
   */
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  /**
   * Valeur du filtre numéro de carnet
   */
  public int getNumeroCarnet() {
    return numeroCarnet;
  }
  
  /**
   * Valeur du filtre numéro de bon de cour
   */
  public int getNumeroBonCour() {
    return numeroBonCour;
  }
  
  /**
   * Valeur du filtre magasinier
   */
  public Vendeur getMagasinier() {
    return magasinier;
  }
  
  /**
   * Valeur du filtre état du bon de cour
   */
  public EnumEtatBonCour getEtatBonCour() {
    return etatBonCour;
  }
  
  /**
   * Liste des bons de cour trouvés
   */
  public ListeBonCour getListeBonCour() {
    return listeBonCour;
  }
  
  /**
   * Valeur du filtre établissement
   */
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  /**
   * Retourner le titre de la liste
   */
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  /**
   * Retourner le bon de cour sélectionné
   */
  public BonCour getBonCourSelectionne() {
    return bonCourSelectionne;
  }
  
  /**
   * Retourner l'indice du bon de cour sélectionné
   */
  public int getIndiceSelection() {
    return indiceSelection;
  }
  
  /**
   * Retourner la liste des clients des bons de cour
   */
  public ListeClientBase getListeClientBase() {
    return listeClientBase;
  }
  
}
