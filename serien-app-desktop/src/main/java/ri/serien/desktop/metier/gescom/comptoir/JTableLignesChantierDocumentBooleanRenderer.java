/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;

public class JTableLignesChantierDocumentBooleanRenderer extends DefaultTableCellRenderer {
  private ListeChantier listeChantier;
  
  public JTableLignesChantierDocumentBooleanRenderer(ListeChantier pListeChantier) {
    listeChantier = pListeChantier;
  }
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setResizable(false);
    cm.getColumn(0).setMinWidth(50);
    cm.getColumn(0).setMaxWidth(50);
    
    // Pour les cellules de type checkbox
    if (listeChantier != null && row < listeChantier.size() && listeChantier.get(row).isActif() && value != null) {
      JCheckBox c = new JCheckBox();
      c.setHorizontalAlignment(JCheckBox.CENTER);
      c.setOpaque(false);
      c.setSelected((Boolean) value);
      return c;
    }
    else {
      JLabel c = new JLabel("");
      return c;
    }
  }
}
