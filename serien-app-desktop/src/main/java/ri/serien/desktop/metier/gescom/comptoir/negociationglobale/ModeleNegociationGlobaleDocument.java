/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.negociationglobale;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.desktop.metier.gescom.comptoir.detailligne.ColonneTarif;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.negociation.EnumTypePrixSaisiInvalide;
import ri.serien.libcommun.gescom.vente.negociation.NegociationVenteGlobale;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleNegociationGlobaleDocument extends AbstractModeleDialogue {
  // Constantes
  public static final int NOMBRE_DECIMALE_MONTANT = 2;
  
  // Variables
  private ModeleComptoir modeleParent = null;
  private DocumentVente documentVente = null;
  private ListeLigneVente listeLigneVenteNegociable = null;
  private IdClient idClient = null;
  private boolean affichageTTC = false;
  private boolean afficherDetails = true;
  private Date maintenant = null;
  private NegociationVenteGlobale negociationVenteGlobaleOrigine = null;
  private NegociationVenteGlobale negociationVenteGlobaleNegocie = null;
  private HashMap<IdArticle, Article> listeArticles = new HashMap<IdArticle, Article>();
  private ColonneTarif[] listeColonnesTarif = null;
  
  /**
   * Constructeur.
   */
  public ModeleNegociationGlobaleDocument(SessionBase pSession, ModeleComptoir pModeleParent, DocumentVente pDocumentVente,
      IdClient pIdClient) {
    super(pSession);
    modeleParent = pModeleParent;
    documentVente = pDocumentVente;
    idClient = pIdClient;
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    maintenant = new Date();
    
    if (documentVente.isExistant()) {
      affichageTTC = documentVente.isTTC();
      
      // Charger les colonnes de tarif
      listeColonnesTarif = new ColonneTarif[TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE + 1];
      for (int col = 0; col <= TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE; col++) {
        listeColonnesTarif[col] = new ColonneTarif(col);
      }
      
      ListeLigneVente listeLigneVenteOrigine = documentVente.getListeLigneVente();
      listeLigneVenteNegociable = new ListeLigneVente();
      
      if (listeLigneVenteOrigine != null && !listeLigneVenteOrigine.isEmpty()) {
        for (LigneVente ligneVente : listeLigneVenteOrigine) {
          if (ligneVente.isLigneCommentaire() || ligneVente.isLigneRemiseSpeciale()) {
            continue;
          }
          listeLigneVenteNegociable.add(ligneVente);
        }
        
        // Contrôle qu'il y ait des lignes de ventes négociables
        if (listeLigneVenteNegociable.isEmpty()) {
          quitterAvecAnnulation();
          throw new MessageErreurException("Le document ne contient pas de ligne négociable");
        }
      }
      else {
        throw new MessageErreurException("La liste des lignes du document est invalide.");
      }
      
      // Initialisation de la classe négociation vente d'origine
      negociationVenteGlobaleOrigine = new NegociationVenteGlobale(getIdSession(), affichageTTC, isModeNegoce());
      negociationVenteGlobaleOrigine.initialiser(listeLigneVenteNegociable);
      
      // Initialisation de la classe négociation vente modifiée
      negociationVenteGlobaleNegocie = new NegociationVenteGlobale(getIdSession(), affichageTTC, isModeNegoce());
      negociationVenteGlobaleNegocie.initialiser(listeLigneVenteNegociable);
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    controleValiditeSaisie();
    
    // Mise à jour des lignes de ventes négociées
    for (LigneVente ligneVente : listeLigneVenteNegociable) {
      LigneVente ligneVenteNegociee = negociationVenteGlobaleNegocie.getLigneVente(ligneVente.getId());
      if (ligneVenteNegociee != null) {
        if (!controlerLigneDocument(ligneVenteNegociee)) {
          return;
        }
        ManagerServiceDocumentVente.modifierLignedocumentVente(getIdSession(), ligneVenteNegociee);
      }
    }
    
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Modification de la colonne de tarif.
   */
  public void modifierColonne(ColonneTarif pColonne) {
    if (listeColonnesTarif == null) {
      return;
    }
    if (pColonne.getNumeroColonne() == negociationVenteGlobaleNegocie.getNumeroColonne() && pColonne.getNumeroColonne() == 0) {
      return;
    }
    negociationVenteGlobaleNegocie.modifierColonneTarif(pColonne.getNumeroColonne());
    rafraichir();
  }
  
  /**
   * Modification de la remise.
   */
  public void modifierRemise(String pSaisie) {
    BigDecimal taux = Constantes.convertirTexteEnBigDecimal(pSaisie);
    // Ne rien faire si la valeur n'a pas changé
    if (taux.compareTo(negociationVenteGlobaleNegocie.getTauxRemise()) == 0) {
      return;
    }
    
    // Vérifie la remise avant d'être appliquée aux articles
    if (negociationVenteGlobaleOrigine.verifierRemiseSaisi(taux)) {
      negociationVenteGlobaleNegocie.modifierTauxRemise(taux);
    }
    // Procéder au changement même si la valeur est en dessous du prix de revient
    if (EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT.equals(negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide())) {
      negociationVenteGlobaleNegocie.modifierTauxRemise(taux);
    }
    rafraichir();
    afficherMessageInformationNegociationInvalide();
  }
  
  /**
   * Modification du prix net HT ou TTC.
   */
  public void modifierPrixNet(String pSaisie) {
    // Continuer la négociation si les conditions sont vérifiées
    boolean procederNegociation = false;
    BigDecimal montant = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    // Ne rien faire si la valeur n'a pas changé
    if (isAffichageTTC()) {
      if (montant.compareTo(negociationVenteGlobaleNegocie.getTotalNetTTC()) == 0) {
        return;
      }
    }
    else {
      if (montant.compareTo(negociationVenteGlobaleNegocie.getTotalNetHT()) == 0) {
        return;
      }
    }
    
    // Vérifie le prix net avant d'être appliqué aux articles
    if (negociationVenteGlobaleOrigine.verifierTotalNetSaisi(montant)) {
      procederNegociation = true;
    }
    // Proceder au changement même si la valeur est en dessous du prix de revient
    if (EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT.equals(negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide())) {
      procederNegociation = true;
    }
    
    if (procederNegociation) {
      // Affichage TTC
      if (isAffichageTTC()) {
        negociationVenteGlobaleNegocie.modifierTotalNetTTC(montant);
      }
      // Affichage HT
      else {
        negociationVenteGlobaleNegocie.modifierTotalNetHT(montant);
      }
    }
    
    rafraichir();
    afficherMessageInformationNegociationInvalide();
  }
  
  /**
   * Modification du coefficient.
   */
  public void modifierIndiceDeMarge(String pSaisie) {
    BigDecimal indice = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    // Ne rien faire si la valeur n'a pas changé
    if (indice.compareTo(negociationVenteGlobaleNegocie.getIndiceMarge()) == 0) {
      return;
    }
    // Vérifie du prix net avant son application aux articles
    if (negociationVenteGlobaleOrigine.verifierIndiceDeMargeSaisi(indice)) {
      negociationVenteGlobaleNegocie.modifierIndiceMarge(indice);
    }
    
    // Proceder au changement même si la valeur est en dessous du prix de revient
    if (EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT.equals(negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide())) {
      negociationVenteGlobaleNegocie.modifierIndiceMarge(indice);
    }
    
    rafraichir();
    afficherMessageInformationNegociationInvalide();
  }
  
  /**
   * Modification de la marge.
   */
  public void modifierMarge(String pSaisie) {
    BigDecimal tauxmarge = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    if (tauxmarge.compareTo(negociationVenteGlobaleNegocie.getTauxMarge()) == 0) {
      return;
    }
    // Vérifie du prix net avant son application aux articles
    if (negociationVenteGlobaleOrigine.verifierMargeSaisi(tauxmarge)) {
      negociationVenteGlobaleNegocie.modifierTauxMarge(tauxmarge);
    }
    
    // Proceder au changement même si la valeur est en dessous du prix de revient
    if (EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT.equals(negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide())) {
      negociationVenteGlobaleNegocie.modifierTauxMarge(tauxmarge);
    }
    
    rafraichir();
    afficherMessageInformationNegociationInvalide();
  }
  
  /**
   * Modification de l'affichage ou non des marges.
   */
  public void modifierAffichageDetails(boolean pAfficherDetails) {
    if (afficherDetails == pAfficherDetails) {
      return;
    }
    afficherDetails = pAfficherDetails;
    rafraichir();
  }
  
  /**
   * Modification de l'affichage en TTC ou HT.
   */
  public void modifierAffichageTTC(boolean pAfficherTTC) {
    if (affichageTTC == pAfficherTTC) {
      return;
    }
    affichageTTC = pAfficherTTC;
    negociationVenteGlobaleOrigine.setEnTTC(affichageTTC);
    negociationVenteGlobaleNegocie.setEnTTC(affichageTTC);
    rafraichir();
  }
  
  /**
   * Controle la validité des informations saisies.
   */
  public void controleValiditeSaisie() {
    StringBuilder message = new StringBuilder();
    
    // S'il y a un message à afficher
    if (message.length() > 0) {
      message.insert(0, "Afin de pouvoir valider votre saisie, merci de renseigner :");
      throw new MessageErreurException(message.toString());
    }
  }
  
  /**
   * Lecture de la sécurité sur la visualisation des marges
   */
  public boolean isAutoriseVisualiserMarge() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(), EnumDroitSecurite.IS_AUTORISE_VISUALISATION_MARGES);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôle les lignes d'un document
   */
  private boolean controlerLigneDocument(LigneVente pLigneVente) {
    AlertesLigneDocument alerte =
        ManagerServiceDocumentVente.controlerLigneDocumentVente(getIdSession(), pLigneVente, modeleParent.getDateTraitement());
    if (!alerte.getMessages().trim().isEmpty()) {
      alerte.setMessages("Erreur");
      return false;
    }
    
    return true;
  }
  
  /**
   * Affiche une information si le prix de négociation est invalide
   */
  private void afficherMessageInformationNegociationInvalide() {
    if (negociationVenteGlobaleOrigine != null && (negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide() != null)) {
      String messageInformation = negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide().getLibelle();
      if (negociationVenteGlobaleOrigine.getTypePrixSaisiInvalide().equals(EnumTypePrixSaisiInvalide.PRIX_NET_INFERIEUR_PRIX_REVIENT)) {
        messageInformation += "\nLa remise maximale sera appliquée.";
      }
      else {
        messageInformation += "\nIl sera remis à sa dernière valeur valide.";
      }
      DialogueInformation.afficher(messageInformation);
    }
  }
  
  // -- Accesseurs
  
  public boolean isAffichageTTC() {
    return affichageTTC;
  }
  
  public boolean isAfficherDetails() {
    return afficherDetails;
  }
  
  public int getIndexColonneTarifaireMax() {
    return TarifArticle.NOMBRE_COLONNES_MODE_NEGOCE;
  }
  
  public NegociationVenteGlobale getNegociationVenteGlobaleOrigine() {
    return negociationVenteGlobaleOrigine;
  }
  
  public NegociationVenteGlobale getNegociationVenteGlobaleNegocie() {
    return negociationVenteGlobaleNegocie;
  }
  
  public ColonneTarif[] getListeColonnesTarif() {
    return listeColonnesTarif;
  }
  
  /**
   * Retourne le mode en cours Négoce ou Classique.
   * @return
   */
  public boolean isModeNegoce() {
    return modeleParent.isModeNegoce();
  }
}
