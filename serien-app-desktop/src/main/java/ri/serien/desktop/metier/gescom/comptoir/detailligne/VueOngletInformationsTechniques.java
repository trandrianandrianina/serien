/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.detailligne;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.JScrollPane;
import javax.swing.border.BevelBorder;

import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.outils.OutilURL;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.EnumCategorieBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet mémo de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueOngletInformationsTechniques extends SNPanel implements InterfaceVueOnglet {
  private static final String BOUTON_COPIER_EN_COMMENTAIRE = "Copier en commentaire";
  private static final String BOUTON_FICHE_TECHNIQUE = "Fiche technique";
  
  private ModeleDetailLigneArticle modele = null;
  
  public VueOngletInformationsTechniques(ModeleDetailLigneArticle acomptoir) {
    modele = acomptoir;
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    setName(getClass().getSimpleName());
    
    // Taille max du mémo (10 lignes de 120 caractères)
    taMemoItc.setLongueurMaximumTexte(Memo.LONGUEUR_TEXTE_MAX);
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    taMemoItc.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    taMemoItc.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    ckAffichageObligatoire.setMnemonic(KeyEvent.VK_O);
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    // Affichage obligatoire
    rafraichirAffichageObligatoire();
    
    // Effacer tous les boutons
    snBarreBouton.supprimerTout();
    
    // Ajouter les boutons permanents
    snBarreBouton.ajouterBouton(BOUTON_COPIER_EN_COMMENTAIRE, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    
    // Ajouter les boutons des fiches techniques
    rafraichirBoutonsURL();
    
    // Le mémo
    rafraichirTexteMemo();
  }
  
  private void rafraichirAffichageObligatoire() {
    Article article = modele.getArticle();
    if (article != null) {
      ckAffichageObligatoire.setSelected(article.isAffichageMemoObligatoire());
      ckAffichageObligatoire.setEnabled(true);
    }
    else {
      ckAffichageObligatoire.setSelected(false);
      ckAffichageObligatoire.setEnabled(false);
    }
    // La zone est rendu non saisissable si le document n'est pas modifiable
    if (modele.getDocumentVente() == null || !modele.getDocumentVente().isModifiable()) {
      ckAffichageObligatoire.setEnabled(false);
    }
  }
  
  private void rafraichirBoutonsURL() {
    if (modele.getListeURL() != null) {
      for (int i = 0; i < modele.getListeURL().length && i < 10; i++) {
        String url = modele.getListeURL()[i];
        if (url != null && !url.trim().isEmpty()) {
          String libelle = "Fiche " + (i + 1) + "\n" + OutilURL.getNomDomaine(url);
          SNBouton snBouton = new SNBouton(EnumCategorieBouton.STANDARD, libelle, Character.forDigit(i + 1, 10), false);
          snBouton.setIndex(i);
          snBarreBouton.ajouterBouton(snBouton, true);
        }
      }
    }
  }
  
  private void rafraichirTexteMemo() {
    Article article = modele.getArticle();
    if (modele.getTexteMemoITC() != null) {
      taMemoItc.setText(modele.getTexteMemoITC());
    }
    else {
      taMemoItc.setText("");
    }
    if (article != null) {
      taMemoItc.setEditable(true);
      if (taMemoItc.isEditable()) {
        taMemoItc.setCaretPosition(0);
      }
    }
    else {
      taMemoItc.setEditable(false);
    }
    // La zone est rendu non saisissable si le document n'est pas modifiable
    if (modele.getDocumentVente() != null && modele.getDocumentVente().isModifiable()) {
      taMemoItc.setEnabled(true);
    }
    else {
      taMemoItc.setEnabled(false);
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        modele.quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        modele.quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(BOUTON_COPIER_EN_COMMENTAIRE)) {
        modele.copierMemoITCversCommentaire();
      }
      else if (pSNBouton.getIndex() != null) {
        modele.ouvrirURL(pSNBouton.getIndex());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void taMemoItcFocusLost(FocusEvent e) {
    try {
      modele.modifierMemoITC(taMemoItc.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void ckAffichageObligatoireItemStateChanged(ItemEvent e) {
    try {
      modele.modifierTopMemo(ckAffichageObligatoire.isSelected());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    snBarreBouton = new SNBarreBouton();
    sNPanelPrincipal1 = new SNPanelContenu();
    scpMemoItc = new JScrollPane();
    taMemoItc = new RiTextArea();
    ckAffichageObligatoire = new XRiCheckBox();
    
    // ======== this ========
    setMinimumSize(new Dimension(945, 420));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== sNPanelPrincipal1 ========
    {
      sNPanelPrincipal1.setName("sNPanelPrincipal1");
      sNPanelPrincipal1.setLayout(new GridBagLayout());
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) sNPanelPrincipal1.getLayout()).rowWeights = new double[] { 1.0, 0.0, 1.0E-4 };
      
      // ======== scpMemoItc ========
      {
        scpMemoItc.setName("scpMemoItc");
        
        // ---- taMemoItc ----
        taMemoItc.setBorder(new BevelBorder(BevelBorder.LOWERED));
        taMemoItc.setWrapStyleWord(true);
        taMemoItc.setLineWrap(true);
        taMemoItc.setFont(taMemoItc.getFont().deriveFont(taMemoItc.getFont().getSize() + 3f));
        taMemoItc.setName("taMemoItc");
        taMemoItc.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            taMemoItcFocusLost(e);
          }
        });
        scpMemoItc.setViewportView(taMemoItc);
      }
      sNPanelPrincipal1.add(scpMemoItc,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- ckAffichageObligatoire ----
      ckAffichageObligatoire.setText("<html>Affichage <u>o</u>bligatoire</html>");
      ckAffichageObligatoire.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      ckAffichageObligatoire.setFont(ckAffichageObligatoire.getFont().deriveFont(ckAffichageObligatoire.getFont().getSize() + 2f));
      ckAffichageObligatoire.setPreferredSize(new Dimension(151, 30));
      ckAffichageObligatoire.setMinimumSize(new Dimension(89, 30));
      ckAffichageObligatoire.setMaximumSize(new Dimension(2147483647, 99));
      ckAffichageObligatoire.setName("ckAffichageObligatoire");
      ckAffichageObligatoire.addItemListener(new ItemListener() {
        @Override
        public void itemStateChanged(ItemEvent e) {
          ckAffichageObligatoireItemStateChanged(e);
        }
      });
      sNPanelPrincipal1.add(ckAffichageObligatoire,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(sNPanelPrincipal1, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu sNPanelPrincipal1;
  private JScrollPane scpMemoItc;
  private RiTextArea taMemoItc;
  private XRiCheckBox ckAffichageObligatoire;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
