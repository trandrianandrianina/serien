/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.historiquereceptionsachat;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ReceptionAchat;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.onglet.InterfaceVueOnglet;

/**
 * Vue de l'onglet stock de la boîte de dialogue pour le détail d'une ligne.
 */
public class VueHistoriqueReceptions extends JPanel implements InterfaceVueOnglet {
  // Constantes
  private static final String[] TITRE_LISTE_HISTORIQUE = new String[] { "Num\u00e9ro de r\u00e9ception", "Date de r\u00e9ception",
      "Fournisseur", "R\u00e9f\u00e9rence commande", "Quantit\u00e9 en UCA", "Prix d'achat net HT", "Acheteur" };
  private static final int NOMBRE_DECIMALE_PRIX = 2;
  
  // Variables
  private ModeleHistoriqueReceptions modele = null;
  private DefaultTableModel tableModelDocument = null;
  private JTableCellRendererHistoriqueReceptions listeHistoRenderer = new JTableCellRendererHistoriqueReceptions();
  private ListeStockComptoir listeStockParMagasin = null;
  private ListeStockComptoir listeStockMagasinsEtablissement = null;
  private boolean ecranInitialise = false;
  private int indexLigneListeEtatStocks = -1;
  private boolean executerEvenements = false;
  private int nombreDecimalesStocks = 0;
  
  /**
   * Constructeur.
   */
  public VueHistoriqueReceptions(ModeleHistoriqueReceptions pModele) {
    modele = pModele;
    initialiserComposants();
    rafraichir();
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    tableModelDocument = new DefaultTableModel(null, TITRE_LISTE_HISTORIQUE) {
      @Override
      public boolean isCellEditable(int rowIndex, int columnIndex) {
        return (columnIndex < 0);
      }
    };
    
    // Ajout d'un évènement afin de gérer la pagination "intelligente" avec le scrollbar des états des stocks
    scpHistorique.getVerticalScrollBar().addAdjustmentListener(new AdjustmentListener() {
      @Override
      public void adjustmentValueChanged(AdjustmentEvent arg0) {
        // Si l'écran n'est pas initialisé, on ignore cet évènement
        if (!ecranInitialise) {
          return;
        }
        // Traitement à effectuer si l'évènement est activé
        int longueurBarre = scpHistorique.getVerticalScrollBar().getModel().getExtent();
        if (((scpHistorique.getVerticalScrollBar().getValue() + longueurBarre) == scpHistorique.getVerticalScrollBar().getMaximum())) {
          indexLigneListeEtatStocks = tblHistorique.getSelectedRow();
        }
      }
    });
    scpHistorique.setBackground(Color.WHITE);
    scpHistorique.getViewport().setBackground(Color.WHITE);
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewport = scpHistorique.getViewport();
    viewport.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpHistoriqueStateChanged(e);
      }
    });
  }
  
  @Override
  public void rafraichir() {
    executerEvenements = false;
    ecranInitialise = true;
    
    rafraichirLibelleUniteAchat();
    rafraichirReceptions();
    
    executerEvenements = true;
  }
  
  // -- Méthodes privées
  
  private void rafraichirLibelleUniteAchat() {
    if (modele.retournerNomUniteAchat() != null) {
      lbUniteAchat.setText("Achats en " + modele.retournerNomUniteAchat());
    }
    else {
      lbUniteAchat.setText("");
    }
  }
  
  /**
   * Affichage des données du sous-onglet tous les dépots.
   */
  private void rafraichirReceptions() {
    ListeReceptionAchat listeReceptionAchat = modele.getListeReceptions();
    String[][] donnees = null;
    
    if (listeReceptionAchat != null && !listeReceptionAchat.isEmpty()) {
      nombreDecimalesStocks = modele.retournerPrecisionUniteAchat(listeReceptionAchat.get(0).getIdUniteAchat());
      
      donnees = new String[listeReceptionAchat.size()][TITRE_LISTE_HISTORIQUE.length];
      for (int ligne = 0; ligne < listeReceptionAchat.size(); ligne++) {
        if (listeReceptionAchat.get(ligne) == null || !listeReceptionAchat.get(ligne).isCharge()) {
          continue;
        }
        ReceptionAchat reception = listeReceptionAchat.get(ligne);
        donnees[ligne][0] = reception.getId().getIdDocumentAchat().getTexte();
        donnees[ligne][1] = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA, reception.getDateReception());
        donnees[ligne][2] = modele.getListeFournisseur().retournerNomFournisseurParId(reception.getIdFournisseur());
        if (reception.getReferences() != null) {
          donnees[ligne][3] = reception.getReferences();
        }
        else {
          donnees[ligne][3] = "";
        }
        donnees[ligne][4] = Constantes.formater(reception.getQuantiteUCA(), false);
        donnees[ligne][5] = Constantes.formater(reception.getPrixAchat(), true);
        if (reception.getAcheteur() != null) {
          donnees[ligne][6] = reception.getAcheteur();
        }
        else {
          donnees[ligne][6] = "";
        }
      }
    }
    
    // Mettre à jour les données
    tableModelDocument.setRowCount(0);
    if (donnees != null) {
      for (String[] ligne : donnees) {
        tableModelDocument.addRow(ligne);
      }
    }
    
    // Remplacer le modèle de la table si besoin
    if (!Constantes.equals(tblHistorique.getModel(), tableModelDocument)) {
      tblHistorique.setModel(tableModelDocument);
      tblHistorique.setGridColor(new Color(204, 204, 204));
      tblHistorique.setDefaultRenderer(Object.class, listeHistoRenderer);
    }
    
    // Forcer le redimensionnement dans le cas où il n'y a pas de données afin de formater les colonnes correctement
    if (tblHistorique.getRowCount() == 0) {
      listeHistoRenderer.redimensionnerColonnes(tblHistorique);
    }
  }
  
  // -- Accesseurs et méthodes évènementielles
  
  private void scpHistoriqueStateChanged(ChangeEvent e) {
    try {
      if (!executerEvenements || tblHistorique == null) {
        return;
      }
      Trace.debug(VueHistoriqueReceptions.class, "scpHistoriqueStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpHistorique.getViewport().getViewRect();
      int premiereLigne = tblHistorique.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblHistorique.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblHistorique.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      modele.setIndexRechercheDocument(tblHistorique.getSelectedRow());
      modele.modifierPlageReceptionsAffiches(premiereLigne, derniereLigne);
      rafraichirReceptions();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btValiderStockActionPerformed(ActionEvent e) {
    try {
      modele.quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btRetourStockActionPerformed(ActionEvent e) {
    try {
      modele.quitterAvecAnnulation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btRetourGeneralActionPerformed(ActionEvent e) {
    try {
      modele.quitterAvecAnnulation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btValiderGeneralActionPerformed(ActionEvent e) {
    try {
      modele.quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    lbUniteAchat = new JLabel();
    scpHistorique = new JScrollPane();
    tblHistorique = new JTable();
    
    // ======== this ========
    setBackground(new Color(238, 238, 210));
    setMinimumSize(new Dimension(917, 515));
    setMaximumSize(new Dimension(917, 515));
    setName("this");
    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[] { 880, 0 };
    ((GridBagLayout) getLayout()).rowHeights = new int[] { 35, 378, 0 };
    ((GridBagLayout) getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
    ((GridBagLayout) getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
    
    // ---- lbUniteAchat ----
    lbUniteAchat.setText("Achats en ");
    lbUniteAchat
        .setFont(lbUniteAchat.getFont().deriveFont(lbUniteAchat.getFont().getStyle() | Font.BOLD, lbUniteAchat.getFont().getSize() + 1f));
    lbUniteAchat.setName("lbUniteAchat");
    add(lbUniteAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
        new Insets(10, 10, 15, 10), 0, 0));
    
    // ======== scpHistorique ========
    {
      scpHistorique.setName("scpHistorique");
      
      // ---- tblHistorique ----
      tblHistorique.setShowVerticalLines(true);
      tblHistorique.setShowHorizontalLines(true);
      tblHistorique.setBackground(Color.white);
      tblHistorique.setOpaque(false);
      tblHistorique.setRowHeight(20);
      tblHistorique.setGridColor(new Color(204, 204, 204));
      tblHistorique.setName("tblHistorique");
      scpHistorique.setViewportView(tblHistorique);
    }
    add(scpHistorique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(10, 10, 10, 10), 0, 0));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel lbUniteAchat;
  private JScrollPane scpHistorique;
  private JTable tblHistorique;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
