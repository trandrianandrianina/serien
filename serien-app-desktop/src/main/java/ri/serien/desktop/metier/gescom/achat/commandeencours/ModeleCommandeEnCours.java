/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.commandeencours;

import java.util.ArrayList;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleCommandeEnCours extends AbstractModeleDialogue {
  // Variables
  private ModeleAchat modeleParent = null;
  private List<IdLigneAchat> listeIdLigneAchat = null;
  private Article article = null;
  private Fournisseur fournisseur = null;
  private Message message = null;
  private ArrayList<LigneAAfficher> listeAAfficher = new ArrayList<LigneAAfficher>();
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheArticle = -1;
  
  /**
   * Constructeur.
   */
  public ModeleCommandeEnCours(SessionBase pSession, ModeleAchat pModeleParent, Article pArticle, List<IdLigneAchat> pListeIdLigneAchat,
      Fournisseur pFournisseur) {
    super(pSession);
    modeleParent = pModeleParent;
    article = pArticle;
    listeIdLigneAchat = pListeIdLigneAchat;
    fournisseur = pFournisseur;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Création de la liste des documents avec les id des lignes qui nous intéressent
    if (listeIdLigneAchat != null) {
      listeAAfficher.clear();
      for (IdLigneAchat id : listeIdLigneAchat) {
        listeAAfficher.add(new LigneAAfficher(id));
      }
    }
    
    // Charger les lignes a afficher
    chargerPlageLigneAAfficher(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Construction du message d'alerte
    contruireMessageAlerte();
  }
  
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Afficher la plage de lignes d'achat comprise comprise entre deux lignes.
   * Les informations des lignes d'achats sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageLignesAchatsAffichees(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeAAfficher == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeAAfficher.size()) {
      indexPremiereLigneAffichee = listeAAfficher.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeAAfficher == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeAAfficher.size()) {
      nombreLigneAffichee = listeAAfficher.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corriger la ligne sélectionnée si elle est hors limite
    // Cela sert lors des déplacement de l'ascenseur avec la souris
    if (indexRechercheArticle < indexPremiereLigneAffichee) {
      indexRechercheArticle = indexPremiereLigneAffichee;
    }
    else if (indexRechercheArticle > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheArticle = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Tracer les lignes affichées en mode debug
    Trace.debug(ModeleCommandeEnCours.class, "modifierPlageLignesAffichees", "indexPremiereLigneVisible", indexPremiereLigneAffichee,
        "nombreLigneVisible", nombreLigneAffichee);
    
    // Charger les données des articles visibles
    chargerPlageLigneAAfficher(indexPremiereLigneAffichee, nombreLigneAffichee);
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Charger les données des lignes à afficher comprises entre deux lignes.
   * Les informations des lignes du tableau sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageLigneAAfficher(int pPremiereLigne, int pNombreLigne) {
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeAAfficher.size());
    
    // Lister les id des lignes à afficher dont il faut charger les informations
    List<IdLigneAchat> listeIdLigneAchat = new ArrayList<IdLigneAchat>();
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneAAfficher = listeAAfficher.get(i);
      if (!ligneAAfficher.isCharge()) {
        listeIdLigneAchat.add(ligneAAfficher.getIdLigneAchat());
      }
    }
    
    // Quitter immédiatement si aucun article n'est à charger
    if (listeIdLigneAchat.isEmpty()) {
      return;
    }
    
    // Charger les informations des lignes à afficher
    ListeDocumentAchat listeDocumentAchat =
        ManagerServiceDocumentAchat.chargerListeDocumentAchatAvecLigne(getIdSession(), listeIdLigneAchat);
    
    // Mettre les lignes à afficher chargées au bon endroit dans la liste
    for (int i = pPremiereLigne; i < iMax; i++) {
      LigneAAfficher ligneTableau = listeAAfficher.get(i);
      if (!ligneTableau.isCharge()) {
        for (DocumentAchat documentAchatCharge : listeDocumentAchat) {
          for (LigneAchat ligneAchatChargee : documentAchatCharge.getListeLigneAchat()) {
            if (ligneAchatChargee != null && ligneTableau.getIdLigneAchat().equals(ligneAchatChargee.getId())) {
              listeAAfficher.get(i).initialiserDonnees(documentAchatCharge, ligneAchatChargee, fournisseur,
                  modeleParent.getListeAcheteur());
              Trace.debug(ModeleCommandeEnCours.class, "chargerPlageLigneTableau", "Charger ligne tableau", "ligne", i, "idLigneAchat",
                  ligneAchatChargee.getId());
              break;
            }
          }
        }
      }
    }
  }
  
  private void contruireMessageAlerte() {
    if (article != null) {
      String texteArticle = "";
      if (article.getLibelle1() != null || !article.getLibelle1().isEmpty()) {
        texteArticle = String.format("%s (%s)", article.getId().getCodeArticle(), article.getLibelle1());
      }
      else {
        texteArticle = article.getId().getCodeArticle();
      }
      message = Message.getMessageNormal(
          "L'article " + texteArticle + " est déjà en cours de commande. Confirmez vous l'achat de cet article pour le bon "
              + modeleParent.getDocumentAchatEnCours().getId() + " ?");
    }
  }
  
  // -- Accesseurs
  
  public ArrayList<LigneAAfficher> getListeLigneAAfficher() {
    return listeAAfficher;
  }
  
  public Message getMessage() {
    return message;
  }
}
