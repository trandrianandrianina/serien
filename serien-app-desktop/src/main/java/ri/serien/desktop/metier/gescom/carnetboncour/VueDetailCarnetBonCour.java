/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.carnetboncour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'écran liste de la consultation de documents de ventes.
 */
public class VueDetailCarnetBonCour extends AbstractVuePanel<ModeleCarnetBonCour> {
  
  /**
   * Constructeur.
   */
  public VueDetailCarnetBonCour(ModeleCarnetBonCour pModele) {
    super(pModele);
  }
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    requestFocus();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirDateRemise();
    rafraichirNumeroCarnet();
    rafraichirMagasinier();
    rafraichirNombrePage();
    rafraichirPremierNumeroBon();
    
    rafraichirEtablissement();
    rafraichirMagasin();
    
    // Rafraîchir boutons
    rafraichirBoutonValider();
    
    if (getModele().isModeCreation()) {
      if (tfNumeroCarnet.getText().trim().isEmpty()) {
        EventQueue.invokeLater(new Runnable() {
          @Override
          public void run() {
            tfNumeroCarnet.requestFocus();
          }
        });
      }
    }
  }
  
  /**
   * Rafraîchir la date de remise.
   */
  private void rafraichirDateRemise() {
    CarnetBonCour carnet = getModele().getCarnetBonCour();
    if (carnet != null) {
      snDateRemise.setDate(carnet.getDateRemise());
    }
    else {
      snDateRemise.setDate(getModele().getMaintenant());
    }
    snDateRemise.setEnabled(isDonneesChargees() && !getModele().isModeConsultation());
  }
  
  /**
   * Rafraîchir le numéro de carnet.
   */
  private void rafraichirNumeroCarnet() {
    String numero = "";
    
    CarnetBonCour carnet = getModele().getCarnetBonCour();
    if (carnet != null && carnet.getId().getNumero() != null && carnet.getId().getNumero() > 0) {
      numero = Constantes.convertirIntegerEnTexte(carnet.getId().getNumero(), 0);
    }
    tfNumeroCarnet.setText(numero);
    tfNumeroCarnet.setEnabled(isDonneesChargees() && getModele().isModeCreation());
  }
  
  /**
   * Rafraîchir le magasinier.
   */
  private void rafraichirMagasinier() {
    IdVendeur idMagasinier = null;
    CarnetBonCour carnet = getModele().getCarnetBonCour();
    if (carnet != null) {
      idMagasinier = carnet.getIdMagasinier();
    }
    cbMagasinier.setSession(getModele().getSession());
    cbMagasinier.setIdEtablissement(getModele().getIdEtablissement());
    cbMagasinier.setAucunAutorise(true);
    cbMagasinier.setListe(getModele().getListeMagasinier());
    cbMagasinier.setIdSelection(idMagasinier);
    cbMagasinier.setEnabled(isDonneesChargees() && !getModele().isModeConsultation());
  }
  
  /**
   * Rafraîchir le nombre de pages.
   */
  private void rafraichirNombrePage() {
    String numero = "";
    
    CarnetBonCour carnet = getModele().getCarnetBonCour();
    if (carnet != null && carnet.getNombrePages() != null && carnet.getNombrePages() > 0) {
      numero = Constantes.convertirIntegerEnTexte(carnet.getNombrePages(), 0);
    }
    tfNombrePage.setText(numero);
    tfNombrePage.setEnabled(isDonneesChargees() && !getModele().isModeConsultation());
  }
  
  /**
   * Rafraîchir le premier numéro de bon.
   */
  private void rafraichirPremierNumeroBon() {
    String numero = "";
    
    CarnetBonCour carnet = getModele().getCarnetBonCour();
    if (carnet != null && carnet.getPremierNumero() != null && carnet.getPremierNumero() > 0) {
      numero = Constantes.convertirIntegerEnTexte(carnet.getPremierNumero(), 0);
    }
    tfPremierNumeroBon.setText(numero);
    tfPremierNumeroBon.setEnabled(isDonneesChargees() && !getModele().isModeConsultation());
  }
  
  /**
   * Rafraîchir l'établissement.
   */
  private void rafraichirEtablissement() {
    // Charger la liste des établissements le premier coup
    ListeEtablissement listeEtablissement = getModele().getListeEtablissement();
    if (snEtablissement.isEmpty() && listeEtablissement != null) {
      snEtablissement.setListe(listeEtablissement);
    }
    if (getModele().getIdEtablissement() != null) {
      snEtablissement.setIdSelection(getModele().getIdEtablissement());
    }
    snEtablissement.setEnabled(isDonneesChargees() && getModele().isModeCreation());
  }
  
  /**
   * Rafraîchir le magasin.
   */
  private void rafraichirMagasin() {
    ListeMagasin listeMagasin = getModele().getListeMagasin();
    cbMagasin.setListe(listeMagasin);
    cbMagasin.setIdSelection(getModele().getIdMagasin());
    
    cbMagasin.setEnabled(isDonneesChargees());
  }
  
  /**
   * Afficher le bouton Valider.
   */
  private void rafraichirBoutonValider() {
    snBarreBouton.activerBouton(EnumBouton.VALIDER, isDonneesChargees() && getModele().isValidationActive());
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().validerEcranDetail();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().annulerEcranDetail();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de la date saisie.
   * 
   * @param e
   */
  private void snDateRemiseValueChanged(SNComposantEvent e) {
    try {
      getModele().modifierDateRemiseDetail(snDateRemise.getDate());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichirDateRemise();
    }
  }
  
  /**
   * Traiter la modification du magasinier sélectionné.
   * 
   * @param e
   */
  private void cbMagasinierValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasinierDetail(cbMagasinier.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de l'établissement sélectionné.
   * 
   * @param e
   */
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierEtablissement(snEtablissement.getSelection().getId());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  /**
   * Traiter la modification du magasin sléectionné.
   * 
   * @param e
   */
  private void cbMagasinValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMagasin(cbMagasin.getIdSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la saisie en cours du numéro de carnet.
   * 
   * @param e
   */
  private void tfNumeroCarnetKeyReleased(KeyEvent e) {
    try {
      getModele().modifierNumeroCarnetDetail(tfNumeroCarnet.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la saisie en cours du nombre de page.
   * 
   * @param e
   */
  private void tfNombrePageKeyReleased(KeyEvent e) {
    try {
      getModele().modifierNombrePageDetail(tfNombrePage.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la saisie en cours du premier numéro de bon.
   * 
   * @param e
   */
  private void tfPremierNumeroBonKeyReleased(KeyEvent e) {
    try {
      if (e.getKeyCode() == KeyEvent.VK_ENTER && !tfPremierNumeroBon.getText().isEmpty()) {
        getModele().modifierNumeroBonDeCourDetail(tfPremierNumeroBon.getText());
        btTraiterClicBouton(snBarreBouton.getBouton(EnumBouton.VALIDER));
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du premier numéro de bon.
   * 
   * @param e
   */
  private void tfPremierNumeroBonFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierNumeroBonDeCourDetail(tfPremierNumeroBon.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlContenu = new SNPanelContenu();
    pnlDetails = new SNPanelTitre();
    pnlGauche = new SNPanel();
    lbDateRemise = new JLabel();
    snDateRemise = new SNDate();
    lbNumeroCarnet = new SNLabelChamp();
    tfNumeroCarnet = new SNTexte();
    lbMagasinier = new SNLabelChamp();
    cbMagasinier = new SNVendeur();
    lbNombrePage = new SNLabelChamp();
    tfNombrePage = new SNTexte();
    lbPremierNumeroBon = new SNLabelChamp();
    tfPremierNumeroBon = new SNTexte();
    pnlDroite = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    cbMagasin = new SNMagasin();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1205, 655));
    setPreferredSize(new Dimension(1205, 655));
    setBackground(new Color(239, 239, 222));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlContenu ========
    {
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setBorder(new EmptyBorder(10, 10, 10, 10));
      pnlContenu.setOpaque(false);
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 279, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlDetails ========
      {
        pnlDetails.setOpaque(false);
        pnlDetails.setTitre("D\u00e9tails");
        pnlDetails.setName("pnlDetails");
        pnlDetails.setLayout(new GridLayout(1, 2, 5, 5));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setOpaque(false);
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbDateRemise ----
          lbDateRemise.setText("Date de remise");
          lbDateRemise.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbDateRemise.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateRemise.setName("lbDateRemise");
          pnlGauche.add(lbDateRemise, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snDateRemise ----
          snDateRemise.setName("snDateRemise");
          snDateRemise.addSNComposantListener(e -> snDateRemiseValueChanged(e));
          pnlGauche.add(snDateRemise, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNumeroCarnet ----
          lbNumeroCarnet.setText("Num\u00e9ro de carnet");
          lbNumeroCarnet.setName("lbNumeroCarnet");
          pnlGauche.add(lbNumeroCarnet, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroCarnet ----
          tfNumeroCarnet.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
          tfNumeroCarnet.setHorizontalAlignment(SwingConstants.TRAILING);
          tfNumeroCarnet.setName("tfNumeroCarnet");
          tfNumeroCarnet.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfNumeroCarnetKeyReleased(e);
            }
          });
          pnlGauche.add(tfNumeroCarnet, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasinier ----
          lbMagasinier.setText("Magasinier");
          lbMagasinier.setName("lbMagasinier");
          pnlGauche.add(lbMagasinier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- cbMagasinier ----
          cbMagasinier.setName("cbMagasinier");
          cbMagasinier.addSNComposantListener(e -> cbMagasinierValueChanged(e));
          pnlGauche.add(cbMagasinier, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNombrePage ----
          lbNombrePage.setText("Nombre de pages");
          lbNombrePage.setPreferredSize(new Dimension(180, 30));
          lbNombrePage.setName("lbNombrePage");
          pnlGauche.add(lbNombrePage, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNombrePage ----
          tfNombrePage.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
          tfNombrePage.setHorizontalAlignment(SwingConstants.TRAILING);
          tfNombrePage.setName("tfNombrePage");
          tfNombrePage.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfNombrePageKeyReleased(e);
            }
          });
          pnlGauche.add(tfNombrePage, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbPremierNumeroBon ----
          lbPremierNumeroBon.setText("Premier num\u00e9ro de bon");
          lbPremierNumeroBon.setPreferredSize(new Dimension(180, 30));
          lbPremierNumeroBon.setName("lbPremierNumeroBon");
          pnlGauche.add(lbPremierNumeroBon, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfPremierNumeroBon ----
          tfPremierNumeroBon.setToolTipText("Num\u00e9ro de document ou num\u00e9ro de facture");
          tfPremierNumeroBon.setHorizontalAlignment(SwingConstants.TRAILING);
          tfPremierNumeroBon.setName("tfPremierNumeroBon");
          tfPremierNumeroBon.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfPremierNumeroBonKeyReleased(e);
            }
          });
          tfPremierNumeroBon.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfPremierNumeroBonFocusLost(e);
            }
          });
          pnlGauche.add(tfPremierNumeroBon, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetails.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setOpaque(false);
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlDroite.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlDroite.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbMagasin ----
          cbMagasin.setName("cbMagasin");
          cbMagasin.addSNComposantListener(e -> cbMagasinValueChanged(e));
          pnlDroite.add(cbMagasin, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDetails.add(pnlDroite);
      }
      pnlContenu.add(pnlDetails,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlDetails;
  private SNPanel pnlGauche;
  private JLabel lbDateRemise;
  private SNDate snDateRemise;
  private SNLabelChamp lbNumeroCarnet;
  private SNTexte tfNumeroCarnet;
  private SNLabelChamp lbMagasinier;
  private SNVendeur cbMagasinier;
  private SNLabelChamp lbNombrePage;
  private SNTexte tfNombrePage;
  private SNLabelChamp lbPremierNumeroBon;
  private SNTexte tfPremierNumeroBon;
  private SNPanel pnlDroite;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin cbMagasin;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
