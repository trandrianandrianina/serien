/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.approvisionnementstock;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.List;

import ri.serien.desktop.metier.gescom.achat.ModeleAchat;
import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.EnumAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceStock;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleDetailCalcul extends AbstractModeleDialogue {
  // Variables
  private ModeleAchat modeleParent = null;
  private Message message = null;
  private List<IdLigneVente> listeIdLigneVenteAApprovisionner = null;
  private LigneTableau ligneTableau = null;
  private List<LigneAttenduCommande> listeCommandeClient = null;
  private ConditionAchat conditionAchat = null;
  private Article article = null;
  
  // Variables pour le chargement progressif de la liste
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  private int indexRechercheDocumentVente = -1;
  
  /**
   * Constructeur.
   */
  public ModeleDetailCalcul(SessionBase pSession, ModeleAchat pModeleParent, LigneTableau pLigneTableau) {
    super(pSession);
    modeleParent = pModeleParent;
    ligneTableau = pLigneTableau;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    if (ligneTableau == null || ligneTableau.getArticle() == null) {
      throw new MessageErreurException("Les données de l'article sont invalides.");
    }
    article = ligneTableau.getArticle();
    
    // Chargement de la condition d'achat afin de récupérer la quantité minimum de commande
    conditionAchat = ManagerServiceArticle.chargerConditionAchat(getIdSession(), article.getId(), new Date());
    
    // Chargement des commandés client
    CritereAttenduCommande critereAttenduCommande = new CritereAttenduCommande();
    critereAttenduCommande.setTypefiltrage(EnumAttenduCommande.COMMANDE_CLIENT);
    critereAttenduCommande.setIdEtablissement(ligneTableau.getArticle().getId().getIdEtablissement());
    critereAttenduCommande.setIdArticle(article.getId());
    critereAttenduCommande.setIdMagasin(ligneTableau.getIdMagasin());
    listeCommandeClient = ManagerServiceStock.chargerListeLigneAttenduCommande(getIdSession(), critereAttenduCommande);
  }
  
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    super.quitterAvecAnnulation();
  }
  
  // -- Méthodes publiques
  
  // -- Méthodes privées
  
  /**
   * Efface les variables liées au chargement des données dans le tableau.
   */
  private void effacerVariablesChargement() {
    nombreLigneAffichee = 0;
    indexPremiereLigneAffichee = 0;
    indexRechercheDocumentVente = -1;
  }
  
  // -- Accesseurs
  public Message getMessage() {
    return message;
  }
  
  public LigneTableau getLigneTableau() {
    return ligneTableau;
  }
  
  public List<LigneAttenduCommande> getListeCommandeClient() {
    return listeCommandeClient;
  }
  
  public IdMagasin getIdMagasin() {
    if (modeleParent.getMagasin() == null || modeleParent.getMagasin().getId() == null) {
      return null;
    }
    return modeleParent.getMagasin().getId();
  }
  
  public BigDecimal getQuantiteMinimumAchat() {
    if (conditionAchat == null || article == null) {
      return null;
    }
    BigDecimal quantiteMinimum = conditionAchat.getQuantiteMinimumCommande();
    // Formate avec le bon nombre de décimale si c'est possible
    if (article.getPrixAchat() != null && article.getPrixAchat().getNombreDecimaleUCA() != null) {
      quantiteMinimum = quantiteMinimum.setScale(article.getPrixAchat().getNombreDecimaleUCA(), RoundingMode.HALF_UP);
    }
    
    return quantiteMinimum;
  }
}
