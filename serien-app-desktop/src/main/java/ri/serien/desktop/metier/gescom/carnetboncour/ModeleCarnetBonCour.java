/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.carnetboncour;

import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.vendeur.IdVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.vendeur.Vendeur;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libcommun.rmi.ManagerServiceParametre;
import ri.serien.libswing.composant.dialoguestandard.confirmation.DialogueConfirmation;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.EnumCleVueListeDetail;
import ri.serien.libswing.moteur.mvc.panel.AbstractModelePanel;

/**
 * Modèle de l'écran principal de la consultation des carnets de bon de cour.
 */
public class ModeleCarnetBonCour extends AbstractModelePanel {
  // Constantes
  public static final String TITRE = "Carnets de bons de cour";
  public static final String TITRE_CONSULTATION = "Consultation d'un carnet de bons de cour";
  public static final String TITRE_CREATION = "Création d'un carnet de bons de cour";
  public static final String TITRE_MODIFICATION = "Modification d'un carnet de bons de cour";
  public static final int NOMBRE_LIGNES_DEVIS = 15;
  public static final int MODE_CONSULTATION = 0;
  public static final int MODE_CREATION = 1;
  public static final int MODE_MODIFICATION = 2;
  
  // Variables communes
  private ListeEtablissement listeEtablissement = null;
  private ListeMagasin listeMagasin = null;
  private ListeVendeur listeMagasinier = null;
  private UtilisateurGescom utilisateurGescom = null;
  
  // Variables pour la gestion de la vue liste
  private Message titreResultat = null;
  private IdEtablissement idEtablissement = null;
  private IdMagasin idMagasin = null;
  private Integer numeroCarnetARecherher = null;
  private IdVendeur idMagasinierARechercher = null;
  private Integer numeroBonDeCourARecherher = null;
  private ListeCarnetBonCour listeCarnet = null;
  private int indexCarnetSelectionne = -1;
  private IdCarnetBonCour idCarnetSelectionne = null;
  private boolean rechercheLancee = false;
  
  // Variables pour la gestion de la vue détail
  private Date maintenant = new Date();
  private int modeUtilisation = MODE_CONSULTATION;
  private CarnetBonCour carnetEnCours = null;
  private CarnetBonCour carnetOriginal = null;
  
  /**
   * Constructeur.
   */
  public ModeleCarnetBonCour(SessionBase pSession) {
    super(pSession);
    // Renseigner le titre de l'onglet
    getSession().getPanel().setName(TITRE);
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
    // Se mettre en mode liste par défaut
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    
    // Effacer le message
    titreResultat = null;
  }
  
  @Override
  public void chargerDonnees() {
    // Charger l'utilisateur
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), null);
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    // Charger la liste des établissements
    listeEtablissement = ListeEtablissement.charger(getIdSession());
    
    if (utilisateurGescom != null) {
      // Initialisation de l'id de l'établissement
      idEtablissement = utilisateurGescom.getIdEtablissementPrincipal();
      
      // Charger le magasin courant
      idMagasin = utilisateurGescom.getIdMagasin();
      
      // Charger tous les données liées à l'établissement
      chargerDonneesLieesEtablissement(utilisateurGescom.getIdEtablissementPrincipal());
    }
  }
  
  /**
   * Charge toutes les données liées à un établissement donné.
   */
  public void chargerDonneesLieesEtablissement(IdEtablissement pIdEtablissement) {
    utilisateurGescom = ManagerServiceParametre.chargerUtilisateurGescom(getIdSession(), ManagerSessionClient.getInstance().getProfil(),
        ManagerSessionClient.getInstance().getCurlib(), pIdEtablissement);
    
    // Charger la liste des magasins
    if (listeMagasin == null) {
      listeMagasin = new ListeMagasin();
    }
    else {
      listeMagasin.clear();
    }
    listeMagasin = listeMagasin.charger(getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
    
    idMagasin = utilisateurGescom.getIdMagasin();
    if (idMagasin == null && listeMagasin != null && !listeMagasin.isEmpty()) {
      idMagasin = listeMagasin.get(0).getId();
    }
    
    // Charger la liste des magasiniers
    listeMagasinier = new ListeVendeur();
    listeMagasinier = listeMagasinier.charger(getSession().getIdSession(), utilisateurGescom.getIdEtablissementPrincipal());
  }
  
  /**
   * Fermer la boîte de dialogue en validant les modifications.
   * Les données modifiées sont conservées.
   */
  public void quitterAvecValidation() {
    getSession().fermerEcran();
  }
  
  /**
   * Fermer la boîte de dialogue en annulant les modifications.
   * Les données modifiées ne sont pas conservées.
   */
  public void quitterAvecAnnulation() {
    getSession().fermerEcran();
  }
  
  // -----------------------------------------------------------------------------------
  // -- Gestion de la vue liste (méthodes publiques)
  // ------------------------------------------------------------------------------------
  /**
   * Initialiser le résultat de la recherche (remet les valeurs par défaut aux filtres).
   */
  public void initialiserRecherche() {
    // Initialiser les filtres
    numeroCarnetARecherher = null;
    numeroBonDeCourARecherher = null;
    idMagasinierARechercher = null;
    
    // Effacer les données
    listeCarnet = null;
    indexCarnetSelectionne = -1;
    rechercheLancee = false;
    construireTitre();
    
    rafraichir();
  }
  
  /**
   * Recherche les carnets.
   */
  public void rechercher(boolean pRafraichirVue) {
    rechercheLancee = true;
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeCarnet != null) {
      taillePage = listeCarnet.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeCarnet = null;
    indexCarnetSelectionne = -1;
    
    // Renseigner les critères de recherche
    CritereCarnetBonCour critereCarnetBonCour = new CritereCarnetBonCour();
    critereCarnetBonCour.setIdEtablissement(idEtablissement);
    critereCarnetBonCour.setIdMagasin(idMagasin);
    if (numeroCarnetARecherher != null) {
      critereCarnetBonCour.setNumeroCarnet(numeroCarnetARecherher);
    }
    if (idMagasinierARechercher != null) {
      critereCarnetBonCour.setIdMagasinier(idMagasinierARechercher);
    }
    if (numeroBonDeCourARecherher != null) {
      critereCarnetBonCour.setNumeroBonDeCour(numeroBonDeCourARecherher);
    }
    
    // Charger la liste des identifiants des documents de vente correspondant au résultat de la recherche
    List<IdCarnetBonCour> listeIdCarnet = ManagerServiceDocumentVente.chargerListeIdCarnetBonCour(getIdSession(), critereCarnetBonCour);
    
    // Créer le résultat de la recherche
    listeCarnet = ListeCarnetBonCour.creerListeNonChargee(listeIdCarnet);
    
    // Charger la première page
    listeCarnet = listeCarnet.chargerPremierePage(getIdSession(), taillePage);
    
    // Construire le titre
    construireTitre();
    
    // Rafraîchir
    if (pRafraichirVue) {
      rafraichir();
    }
  }
  
  /**
   * Afficher la plage d'articles comprise entre deux lignes.
   * Les informations des articles sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageCarnetAffiche(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeCarnet != null) {
      ListeCarnetBonCour listeCarnetCharger = listeCarnet.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
      if (listeCarnetCharger != null) {
        listeCarnet = listeCarnetCharger;
      }
    }
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifie le numéro du carnet des bons de cour à rechercher.
   */
  public void modifierNumeroCarnetARechercher(String pNumero) {
    // Conversion du numéro saisi en entier
    Integer numero = Constantes.convertirTexteEnInteger(pNumero);
    
    // Si le numéro saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (numeroCarnetARecherher != null && numeroCarnetARecherher.compareTo(numero) == 0) {
      return;
    }
    
    // Si le numéro vaut 0 le numéro du carnet mémorisé est mis à null
    if (numero.intValue() <= 0) {
      numeroCarnetARecherher = null;
    }
    else {
      numeroCarnetARecherher = numero;
    }
    rafraichir();
  }
  
  /**
   * Modifie le magasinier à rechercher.
   */
  public void modifierMagasinierARechercher(Vendeur pMagasinier) {
    IdVendeur idMagasinier = null;
    if (pMagasinier != null) {
      idMagasinier = pMagasinier.getId();
    }
    if (Constantes.equals(idMagasinierARechercher, idMagasinier)) {
      return;
    }
    
    idMagasinierARechercher = idMagasinier;
    rafraichir();
  }
  
  /**
   * Modifie le numéro du bon de cour à rechercher.
   */
  public void modifierNumeroBonDeCourARechercher(String pNumero) {
    // Conversion du numéro saisi en entier
    Integer numero = Constantes.convertirTexteEnInteger(pNumero);
    
    // Si le numéro saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (numeroBonDeCourARecherher != null && numeroBonDeCourARecherher.compareTo(numero) == 0) {
      return;
    }
    
    // Si le numéro vaut 0 le numéro du bon de cour mémorisé est mis à null
    if (numero.intValue() <= 0) {
      numeroBonDeCourARecherher = null;
    }
    else {
      numeroBonDeCourARecherher = numero;
    }
    rafraichir();
  }
  
  /**
   * Modifie le magasin des carnets à rechercher.
   */
  public void modifierMagasin(IdMagasin pIdMagasin) {
    if (Constantes.equals(idMagasin, pIdMagasin)) {
      return;
    }
    idMagasin = pIdMagasin;
    rafraichir();
  }
  
  /**
   * Modifie l'établissement des carnets à rechercher.
   */
  public void modifierEtablissement(IdEtablissement pIdEtablissement) {
    if (Constantes.equals(idEtablissement, pIdEtablissement)) {
      return;
    }
    
    // Si le chargement se passe bien alors mise à jour des données liées à l'établissement
    try {
      chargerDonneesLieesEtablissement(pIdEtablissement);
      idEtablissement = pIdEtablissement;
    }
    catch (MessageErreurException e) {
      // Dans le cas contraire rechargement avec l'ancien établissement
      chargerDonneesLieesEtablissement(pIdEtablissement);
      throw e;
    }
    // La méthode rafraichir() est appelée par initialiserRecherche
    initialiserRecherche();
  }
  
  /**
   * Modifie le carnet sélectionné par l'utilisateur.
   */
  public void modifierCarnetSelectionne(int pIndexCarnetSelectionne) {
    // Contrôle de l'index sélectionné
    if (pIndexCarnetSelectionne < 0 || pIndexCarnetSelectionne >= listeCarnet.size()) {
      pIndexCarnetSelectionne = -1;
    }
    if (pIndexCarnetSelectionne == indexCarnetSelectionne) {
      return;
    }
    indexCarnetSelectionne = pIndexCarnetSelectionne;
    
    // Initialisation de l'identifiant du carnet sélectionné
    idCarnetSelectionne = null;
    if (indexCarnetSelectionne != -1) {
      CarnetBonCour carnet = listeCarnet.get(indexCarnetSelectionne);
      if (carnet != null) {
        idCarnetSelectionne = carnet.getId();
      }
    }
    rafraichir();
  }
  
  /**
   * Retourne le bom du magasiniers à partir de son identifiant.
   */
  public String retournerNomMagasinier(IdVendeur pIdMagasinier) {
    if (listeMagasinier == null) {
      return null;
    }
    Vendeur magasinier = listeMagasinier.get(pIdMagasinier);
    if (magasinier == null) {
      return "";
    }
    return magasinier.getTexte();
  }
  
  /**
   * Consultation d'un carnet sélectionné.
   */
  public void afficherDetailCarnetPourConsultation() {
    modeUtilisation = MODE_CONSULTATION;
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    
    // Chargement des données du carnet sélectionné
    chargerCarnet(idCarnetSelectionne);
    // Mettre à jour le titre
    setTitreEcran(TITRE_CONSULTATION);
    rafraichir();
  }
  
  /**
   * Création d'un carnet.
   */
  public void afficherDetailCarnetPourCreation() {
    modeUtilisation = MODE_CREATION;
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    
    // Mettre à jour le titre
    setTitreEcran(TITRE_CREATION);
    rafraichir();
  }
  
  /**
   * Modification un carnet.
   */
  public void afficherDetailCarnetPourModification() {
    setCleVueEnfantActive(EnumCleVueListeDetail.DETAIL);
    modeUtilisation = MODE_MODIFICATION;
    
    // Chargement des données du carnet sélectionné
    chargerCarnet(idCarnetSelectionne);
    // Mettre à jour le titre
    setTitreEcran(TITRE_MODIFICATION);
    rafraichir();
  }
  
  /**
   * Affiche l'écran de recherche des carnets.
   */
  public void afficherListeCarnet() {
    modeUtilisation = MODE_CONSULTATION;
    setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    
    // Mettre à jour le titre
    setTitreEcran(TITRE);
    rafraichir();
  }
  
  // -----------------------------------------------------------------------------------
  // -- Gestion de la vue détail (méthodes publiques)
  // ------------------------------------------------------------------------------------
  
  /**
   * Valide l'écran de détail.
   */
  public void validerEcranDetail() {
    
    if (!isModeConsultation()) {
      controlerCarnet();
      sauverCarnet();
      setCleVueEnfantActive(EnumCleVueListeDetail.LISTE);
    }
    
    // Nettoyage des variables de l'écran détail
    effacerVariableDetail();
    // Retour sur l'écran de recherche
    afficherListeCarnet();
    initialiserRecherche();
    rechercher(true);
  }
  
  /**
   * Annulation de la saisie de l'écran de détail.
   */
  public void annulerEcranDetail() {
    switch (modeUtilisation) {
      case MODE_CONSULTATION:
        break;
      case MODE_CREATION:
        // Un carnet est en cours de création : un message de confirmation est affiché
        if (carnetEnCours != null) {
          if (!DialogueConfirmation.afficher(getSession(), "Annulation de la création du carnet",
              "Vous avez demandé l'annulation du carnet en cours de création.\n Confirmez-vous cette annulation ?")) {
            return;
          }
        }
        break;
      case MODE_MODIFICATION:
        if (carnetEnCours != null && !carnetEnCours.equalsComplet(carnetOriginal)) {
          if (!DialogueConfirmation.afficher(getSession(), "Annulation de la modification du carnet",
              "Vous avez demandé l'annulation de la modification du carnet en cours.\n Confirmez-vous cette annulation ?")) {
            return;
          }
        }
        break;
    }
    // Nettoyage des variables de l'écran détail
    effacerVariableDetail();
    // Retour sur l'écran de recherche
    afficherListeCarnet();
  }
  
  /**
   * Modifie la date de remise du carnet.
   */
  public void modifierDateRemiseDetail(Date pDateSaisie) {
    if (carnetEnCours == null) {
      carnetEnCours = initialiserCarnetVierge();
    }
    
    // Si la date saisie est identique à celle qui est mémorisée aucune action n'est effectuée
    if (Constantes.equals(carnetEnCours.getDateRemise(), pDateSaisie)) {
      return;
    }
    
    carnetEnCours.setDateRemise(pDateSaisie);
    rafraichir();
  }
  
  /**
   * Modifie le numéro de l'identifiant du carnet.
   */
  public void modifierNumeroCarnetDetail(String pSaisie) {
    if (carnetEnCours == null) {
      carnetEnCours = initialiserCarnetVierge();
    }
    
    // Conversion du numéro saisi en entier
    Integer numeroSaisi = Constantes.convertirTexteEnInteger(pSaisie);
    
    // Si le numéro saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (carnetEnCours.getId().getNumero().compareTo(numeroSaisi) == 0) {
      return;
    }
    
    // La création de l'identifiant dépend du mode en cours
    IdCarnetBonCour id;
    if (isModeCreation()) {
      id = IdCarnetBonCour.getInstanceAvecCreationId(idEtablissement, idMagasin, numeroSaisi);
      IdCarnetBonCour.controlerId(id, false);
    }
    else {
      id = IdCarnetBonCour.getInstance(idEtablissement, idMagasin, numeroSaisi);
      IdCarnetBonCour.controlerId(id, true);
    }
    carnetEnCours.setId(id);
  }
  
  /**
   * Modifie le magasinier du carnet.
   */
  public void modifierMagasinierDetail(Vendeur pMagasinierSaisi) {
    if (carnetEnCours == null) {
      carnetEnCours = initialiserCarnetVierge();
    }
    
    IdVendeur idMagasinier = null;
    if (pMagasinierSaisi != null) {
      idMagasinier = pMagasinierSaisi.getId();
    }
    if (Constantes.equals(idMagasinierARechercher, idMagasinier)) {
      return;
    }
    
    carnetEnCours.setIdMagasinier(idMagasinier);
    rafraichir();
  }
  
  /**
   * Modifie le nombre de page du carnet.
   */
  public void modifierNombrePageDetail(String pSaisie) {
    if (carnetEnCours == null) {
      carnetEnCours = initialiserCarnetVierge();
    }
    pSaisie = Constantes.normerTexte(pSaisie);
    if (pSaisie.isEmpty()) {
      carnetEnCours.setNombrePages(null);
      rafraichir();
      return;
    }
    
    // Conversion du nombre saisi en entier
    Integer nombreSaisi = Constantes.convertirTexteEnInteger(pSaisie);
    
    // Contrôle du nombre saisi
    if (nombreSaisi <= 0 && nombreSaisi > Constantes.valeurMaxZoneNumerique(CarnetBonCour.LONGUEUR_NOMBRE_PAGE)) {
      throw new MessageErreurException("Le nombre de pages est un nombre qui être compris entre 1 et "
          + Constantes.valeurMaxZoneNumerique(CarnetBonCour.LONGUEUR_NOMBRE_PAGE) + ".");
    }
    
    // Si le nombre saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (Constantes.equals(carnetEnCours.getNombrePages(), nombreSaisi)) {
      return;
    }
    
    carnetEnCours.setNombrePages(nombreSaisi);
    rafraichir();
  }
  
  /**
   * Modifie le premier numéro du bon du carnet.
   */
  public void modifierNumeroBonDeCourDetail(String pSaisie) {
    if (carnetEnCours == null) {
      carnetEnCours = initialiserCarnetVierge();
    }
    pSaisie = Constantes.normerTexte(pSaisie);
    if (pSaisie.isEmpty()) {
      carnetEnCours.setPremierNumero(null);
      rafraichir();
      return;
    }
    
    // Conversion du numéro saisi en entier
    Integer numeroSaisi = Constantes.convertirTexteEnInteger(pSaisie);
    
    // Si le numéro saisi est identique à celui qui est mémorisé aucune action n'est effectuée
    if (Constantes.equals(carnetEnCours.getPremierNumero(), numeroSaisi)) {
      return;
    }
    
    IdBonCour idBonCour;
    if (isModeCreation()) {
      idBonCour = IdBonCour.getInstanceAvecCreationId(carnetEnCours.getId(), numeroSaisi);
    }
    else {
      idBonCour = IdBonCour.getInstance(carnetEnCours.getId(), numeroSaisi);
    }
    carnetEnCours.setPremierNumero(idBonCour.getNumero());
    rafraichir();
  }
  
  /**
   * Retourne si la validation sur l'écran détail est active.
   */
  public boolean isValidationActive() {
    // En mode consultation la validation n'est pas active
    if (isModeConsultation()) {
      return false;
    }
    // Contrôle les champs obligatoires
    if (carnetEnCours == null || carnetEnCours.getId().getNumero() <= 0 || carnetEnCours.getNombrePages() == null
        || carnetEnCours.getPremierNumero() == null) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes privées
  
  /**
   * Efface les données qui concernent l'écran détail.
   */
  private void effacerVariableDetail() {
    carnetEnCours = null;
    carnetOriginal = null;
    modeUtilisation = MODE_CONSULTATION;
  }
  
  /**
   * Construction du titre de la liste suivant le résultat de recherche.
   */
  private void construireTitre() {
    if (rechercheLancee && listeCarnet == null) {
      titreResultat = Message.getMessageImportant("Aucun carnet ne correspond à votre recherche");
    }
    else if (listeCarnet == null) {
      titreResultat = Message.getMessageNormal("Carnets correspondant à votre recherche");
    }
    else if (listeCarnet.size() == 1) {
      titreResultat = Message.getMessageNormal("Carnet correspondant à votre recherche (1)");
    }
    else if (listeCarnet.size() > 1) {
      titreResultat = Message.getMessageNormal("Carnets correspondant à votre recherche (" + listeCarnet.size() + ")");
    }
  }
  
  /**
   * Initialise un nouveau carnet vide.
   */
  private CarnetBonCour initialiserCarnetVierge() {
    IdCarnetBonCour id = IdCarnetBonCour.getInstanceAvecCreationId(idEtablissement, idMagasin);
    CarnetBonCour carnet = new CarnetBonCour(id);
    carnet.setDateRemise(maintenant);
    return carnet;
  }
  
  /**
   * Contrôle la validité des données du carnet.
   */
  private void controlerCarnet() {
    // Contrôle des données obligatoires (la date de remise et le code du magasinier ne sont pas contrôlés car non obligatoires)
    CarnetBonCour.controler(carnetEnCours);
  }
  
  /**
   * Charger un carnet.
   */
  private void chargerCarnet(IdCarnetBonCour pIdCarnetBonCour) {
    if (idCarnetSelectionne == null) {
      throw new MessageErreurException("Aucun carnet de bons de cour n'est sélectionné.");
    }
    
    carnetEnCours = ManagerServiceDocumentVente.chargerCarnetBonCour(getIdSession(), idCarnetSelectionne);
    // En mode modification, le carnet est cloné afin de contrôler les éventuelles modifications
    if (isModeModification()) {
      carnetOriginal = carnetEnCours.clone();
    }
  }
  
  /**
   * Sauve le carnet.
   */
  private void sauverCarnet() {
    // En consultation le carnet n'est pas sauvé
    if (isModeConsultation()) {
      return;
    }
    // Si le carnet n'a pas été modifié le carnet n'est pas sauvé
    if (isModeModification() && carnetEnCours != null && carnetEnCours.equalsComplet(carnetOriginal)) {
      return;
    }
    // Enregistrement en base
    carnetEnCours = ManagerServiceDocumentVente.sauverCarnetBonCour(getIdSession(), carnetEnCours);
  }
  
  // -- Accesseurs
  
  public UtilisateurGescom getUtilisateurGescom() {
    return utilisateurGescom;
  }
  
  public ListeEtablissement getListeEtablissement() {
    return listeEtablissement;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeVendeur getListeMagasinier() {
    return listeMagasinier;
  }
  
  /**
   * Indice du carnet sélectionné.
   */
  public int getIndexCarnetSelectionne() {
    return indexCarnetSelectionne;
  }
  
  /**
   * Identifiant du document de ventes sélectionné.
   */
  public IdCarnetBonCour getIdDocumentVenteSelectionne() {
    if (listeCarnet != null && indexCarnetSelectionne >= 0 && indexCarnetSelectionne < listeCarnet.size()) {
      idCarnetSelectionne = listeCarnet.get(indexCarnetSelectionne).getId();
    }
    return idCarnetSelectionne;
  }
  
  /**
   * Message d'information.
   */
  public Message getTitreResultat() {
    return titreResultat;
  }
  
  public IdMagasin getIdMagasin() {
    return idMagasin;
  }
  
  public Integer getNumeroCarnetARecherher() {
    return numeroCarnetARecherher;
  }
  
  public Integer getNumeroBonDeCourARecherher() {
    return numeroBonDeCourARecherher;
  }
  
  public IdVendeur getIdMagasinierARechercher() {
    return idMagasinierARechercher;
  }
  
  public ListeCarnetBonCour getListeCarnet() {
    return listeCarnet;
  }
  
  public IdEtablissement getIdEtablissement() {
    return idEtablissement;
  }
  
  public CarnetBonCour getCarnetBonCour() {
    return carnetEnCours;
  }
  
  public boolean isModeCreation() {
    return modeUtilisation == MODE_CREATION;
  }
  
  public boolean isModeConsultation() {
    return modeUtilisation == MODE_CONSULTATION;
  }
  
  public boolean isModeModification() {
    return modeUtilisation == MODE_MODIFICATION;
  }
  
  public Date getMaintenant() {
    return maintenant;
  }
  
}
