/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.changementclient;

import ri.serien.libcommun.exploitation.personnalisation.civilite.Civilite;
import ri.serien.libcommun.exploitation.personnalisation.civilite.IdCivilite;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.client.EnumTypeImageClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.codepostalcommune.CodePostalCommune;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceClient;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleChangementClient extends AbstractModeleDialogue {
  // Variables
  private Etablissement etablissement = null;
  private Client clientOrigine = null;
  private Client client = null;
  private UtilisateurGescom utilisateurGescom = null;
  
  /**
   * Constructeur.
   */
  public ModeleChangementClient(SessionBase pSession, Etablissement pEtablissement, Client pClientOrigine,
      UtilisateurGescom pUtilisateurGescom) {
    super(pSession);
    etablissement = pEtablissement;
    clientOrigine = pClientOrigine;
    utilisateurGescom = pUtilisateurGescom;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // La duplication des coordonnées du client d'origine
    IdClient idClient = IdClient.getInstanceAvecCreationId(clientOrigine.getId().getIdEtablissement());
    client = new Client(idClient);
    client.setTypeCompteClient(EnumTypeCompteClient.COMPTANT);
    client.setTypeImageClient(EnumTypeImageClient.PARTICULIER);
    if (clientOrigine.getAdresse() != null) {
      client.setAdresse(clientOrigine.getAdresse().clone());
    }
    if (clientOrigine.getContactPrincipal() != null) {
      client.setContactPrincipal(clientOrigine.getContactPrincipal().clone());
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    if (!creerClientComptant()) {
      return;
    }
    
    super.quitterAvecValidation();
  }
  
  // -- Méthodes publiques
  
  /**
   * Modifier la civilité du client.
   */
  public void modifierCiviliteClient(Civilite pCivilite) {
    IdCivilite idCivilite = null;
    if (pCivilite != null) {
      idCivilite = pCivilite.getId();
    }
    
    if (Constantes.equals(idCivilite, client.getContactPrincipal().getIdCivilite())) {
      return;
    }
    
    client.getContactPrincipal().setIdCivilite(idCivilite);
    rafraichir();
  }
  
  /**
   * Modifier le nom du client.
   */
  public void modifierNomClient(String nomClient) {
    nomClient = Constantes.normerTexte(nomClient);
    if (nomClient.equals(client.getAdresse().getNom()) && nomClient.equals(client.getContactPrincipal().getNom())) {
      return;
    }
    
    client.setTypeCompteClient(EnumTypeCompteClient.COMPTANT);
    client.setTypeImageClient(EnumTypeImageClient.PARTICULIER);
    client.getAdresse().setNom(nomClient);
    client.getContactPrincipal().setNom(nomClient);
    rafraichir();
  }
  
  /**
   * Modifier le complément du nom du client.
   */
  public void modifierComplementNomClient(String complementNomClient) {
    complementNomClient = Constantes.normerTexte(complementNomClient);
    if (complementNomClient.equals(client.getAdresse().getComplementNom())
        && complementNomClient.equals(client.getContactPrincipal().getPrenom())) {
      return;
    }
    client.getAdresse().setComplementNom(complementNomClient);
    client.getContactPrincipal().setPrenom(complementNomClient);
    rafraichir();
  }
  
  /**
   * Modifier la rue du client.
   */
  public void modifierRueClient(String rueClient) {
    rueClient = Constantes.normerTexte(rueClient);
    if (rueClient.equals(client.getAdresse().getRue())) {
      return;
    }
    client.getAdresse().setRue(rueClient);
    
    rafraichir();
  }
  
  /**
   * Modifier la localisation du client.
   */
  public void modifierLocalisationClient(String localisationClient) {
    localisationClient = Constantes.normerTexte(localisationClient);
    if (localisationClient.equals(client.getAdresse().getLocalisation())) {
      return;
    }
    client.getAdresse().setLocalisation(localisationClient);
    
    rafraichir();
  }
  
  /**
   * Modifier le code postal et la commune du client.
   */
  public void modifierCodePostalCommune(CodePostalCommune pCodePostalCommune) {
    if (client == null) {
      return;
    }
    if (pCodePostalCommune == null) {
      return;
    }
    
    if (!client.getAdresse().getCodePostalFormate().equalsIgnoreCase(pCodePostalCommune.getCodePostal())) {
      client.getAdresse().setCodePostal(pCodePostalCommune.getCodePostalFormate());
    }
    if (!client.getAdresse().getVille().equalsIgnoreCase(pCodePostalCommune.getVille())) {
      client.getAdresse().setVille(pCodePostalCommune.getVille());
    }
    
    rafraichir();
  }
  
  /**
   * Modifier le mail du client.
   */
  public void modifierMailClient(String mailClient) {
    mailClient = Constantes.normerTexte(mailClient);
    if (mailClient.equals(client.getContactPrincipal().getEmail())) {
      return;
    }
    client.getContactPrincipal().setEmail(mailClient);
    rafraichir();
  }
  
  /**
   * Modifier le numéro de téléphone du client.
   */
  public void modifierTelephoneClient(String telephoneClient) {
    telephoneClient = Constantes.normerTexte(telephoneClient);
    if (telephoneClient.equals(client.getNumeroTelephone())
        && telephoneClient.equals(client.getContactPrincipal().getNumeroTelephone1())) {
      return;
    }
    client.setNumeroTelephone(telephoneClient);
    client.getContactPrincipal().setNumeroTelephone1(telephoneClient);
    rafraichir();
  }
  
  /**
   * Modifier le numéro de fax du client.
   */
  public void modifierFaxClient(String faxClient) {
    faxClient = Constantes.normerTexte(faxClient);
    if (faxClient.equals(client.getNumeroFax()) && faxClient.equals(client.getContactPrincipal().getNumeroFax())) {
      return;
    }
    client.setNumeroFax(faxClient);
    client.getContactPrincipal().setNumeroFax(faxClient);
    rafraichir();
  }
  
  // -- Méthodes privées
  
  /**
   * Création du client comptant
   */
  private boolean creerClientComptant() {
    if (client.isExistant()) {
      throw new MessageErreurException("Impossible de créer un client qui existe déjà dans la base.");
    }
    
    client = ManagerServiceClient.sauverClientComptant(getIdSession(), client);
    return true;
  }
  
  // -- Accesseurs
  
  public Client getClient() {
    return client;
  }
  
  public Etablissement getEtablissement() {
    return etablissement;
  }
}
