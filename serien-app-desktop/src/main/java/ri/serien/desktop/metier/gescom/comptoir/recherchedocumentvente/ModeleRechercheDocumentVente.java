/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.recherchedocumentvente;

import java.util.Date;
import java.util.List;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.EnumTypeDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialogue de recherche des documents de ventes.
 */
public class ModeleRechercheDocumentVente extends AbstractModeleDialogue {
  // Constantes
  public static final int RETOUR_MODIFIER_DOCUMENT = 1;
  public static final int RETOUR_DUPLIQUER_DOCUMENT = 3;
  public static final int RETOUR_CREER_AVOIR = 4;
  public static final int RETOUR_EXTRAIRE_EN_COMMANDE = 5;
  public static final int RETOUR_EXTRAIRE_EN_BON = 6;
  public static final int RETOUR_EXTRAIRE_EN_FACTURE = 7;
  
  // Variables
  private ListeMagasin listeMagasin = null;
  private ListeModeExpedition listeModeExpedition = null;
  private boolean documentEnCoursSaisie = false;
  
  // Critères de recherche
  private Client client = null;
  private CritereDocumentVente critereDocumentVente = new CritereDocumentVente();
  
  // Résultat
  private ListeDocumentVenteBase listeDocumentVenteBase = null;
  private int ligneSelectionnee = -1;
  private DocumentVenteBase documentVenteBaseSelectionne = null;
  private int retour = 0;
  
  /**
   * Constructeur.
   */
  public ModeleRechercheDocumentVente(SessionBase pSession, Client pClient, boolean pDocumentEnCoursSaisie) {
    super(pSession);
    client = pClient;
    critereDocumentVente.setIdClient(pClient.getId());
    documentEnCoursSaisie = pDocumentEnCoursSaisie;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Effacer les critères de recherche
    initialiserCritere(false);
    
    // Charger la liste des magasins
    CritereMagasin critereMagasin = new CritereMagasin();
    critereMagasin.setIdEtablissement(client.getId().getIdEtablissement());
    listeMagasin = ListeMagasin.charger(getIdSession(), critereMagasin);
    
    // Charger la liste des modes d'expédition
    listeModeExpedition = ListeModeExpedition.charger(getIdSession());
    
    // Charger automatiquement les documents de vente
    chargerListeDocumentVente();
  }
  
  // -- Méthodes publiques
  
  /**
   * Réinitialise les critères de recherches.
   */
  public void initialiserCritere(boolean pMettreAJour) {
    // Initialsier les critères de recherche
    critereDocumentVente = new CritereDocumentVente();
    critereDocumentVente.setIdClient(client.getId());
    critereDocumentVente.setSansHistorique(true);
    Date dateFin = new Date();
    critereDocumentVente.setDateCreationFin(dateFin);
    critereDocumentVente.setDateCreationDebut(Constantes.getDateDebutMoisPrecedent(dateFin));
    
    // Effacer le résultat précédent
    listeDocumentVenteBase = null;
    
    if (pMettreAJour) {
      rafraichir();
    }
  }
  
  /**
   * Charger la liste des documents de ventes suivant les critères de recherche.
   */
  private void chargerListeDocumentVente() {
    // Mémoriser la taille de la page
    int taillePage = 0;
    if (listeDocumentVenteBase != null) {
      taillePage = listeDocumentVenteBase.getTaillePage();
    }
    
    // Effacer le résultat de la recherche précédente (faire un rafraichir pour remettre le tableau en haut)
    listeDocumentVenteBase = null;
    ligneSelectionnee = -1;
    documentVenteBaseSelectionne = null;
    
    if (client.isClientProspect()) {
      critereDocumentVente.setTypeDocumentVente(EnumTypeDocumentVente.DEVIS);
    }
    
    // Charger les identifiants des documents de vente
    List<IdDocumentVente> listeIdDocumentVente =
        ManagerServiceDocumentVente.chargerListeIdDocumentVente(getIdSession(), critereDocumentVente);
    
    // Initier la liste des documents de ventes à partir de la liste d'identifiants
    listeDocumentVenteBase = ListeDocumentVenteBase.creerListeNonChargee(listeIdDocumentVente);
    
    // Charger la première page
    listeDocumentVenteBase.chargerPremierePage(getIdSession(), taillePage);
  }
  
  /**
   * Afficher la plage de chantiers compris entre deux lignes.
   * Les informations des chantiers sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageDocumentsAffiches(int pIndexPremiereLigne, int pIndexDerniereLigne) {
    // Charger les données visibles
    if (listeDocumentVenteBase != null) {
      listeDocumentVenteBase.chargerPage(getIdSession(), pIndexPremiereLigne, pIndexDerniereLigne);
    }
    
    // Rafraichir l'écran
    rafraichir();
  }
  
  /**
   * Recherche documents
   */
  public void lancerRecherche() {
    chargerListeDocumentVente();
    rafraichir();
  }
  
  /**
   * Modifie l'indice de la ligne séléctionnée dans le tableau.
   */
  public void modifierDocumentSelectionne(int pLigneSelectionnee) {
    if (ligneSelectionnee == pLigneSelectionnee) {
      return;
    }
    ligneSelectionnee = pLigneSelectionnee;
    
    // Déterminer le document de ventes sélectionné
    documentVenteBaseSelectionne = null;
    if (listeDocumentVenteBase != null && 0 <= ligneSelectionnee && ligneSelectionnee < listeDocumentVenteBase.size()) {
      documentVenteBaseSelectionne = listeDocumentVenteBase.get(ligneSelectionnee);
    }
    
    rafraichir();
  }
  
  /**
   * Modifie le type de documents à rechercher selon le choix de la combo.
   */
  public void modifierTypeDocumentVente(EnumTypeDocumentVente pTypeDocumentVente) {
    if (Constantes.equals(pTypeDocumentVente, critereDocumentVente.getTypeDocumentVente())) {
      return;
    }
    critereDocumentVente.setTypeDocumentVente(pTypeDocumentVente);
    rafraichir();
  }
  
  /**
   * Modifie le numéro du document à rechercher.
   */
  public void modifierNumeroDocument(String pNumeroDocument) {
    pNumeroDocument = Constantes.normerTexte(pNumeroDocument);
    int numero = 0;
    try {
      if (!pNumeroDocument.isEmpty()) {
        numero = Integer.parseInt(pNumeroDocument);
      }
    }
    catch (NumberFormatException e) {
      numero = 0;
    }
    
    if (numero == critereDocumentVente.getNumeroDocument()) {
      return;
    }
    critereDocumentVente.setNumeroDocument(numero);
    critereDocumentVente.setNumeroFacture(numero);
    rafraichir();
  }
  
  /**
   * Modifier le filtre chantier afin de rechercher les documents liés au chantier sélectionné
   */
  public void modifierChantier(Chantier pChantier) {
    critereDocumentVente.setChantier(pChantier);
    rafraichir();
  }
  
  /**
   * Modifie le filtre article afin de n'afficher que les documents contenant l'article sélectionné.
   */
  public void modifierArticle(String pArticle) {
    pArticle = Constantes.normerTexte(pArticle);
    if (Constantes.equals(pArticle, critereDocumentVente.getCodeArticle())) {
      return;
    }
    critereDocumentVente.setCodeArticle(pArticle);
    rafraichir();
  }
  
  /**
   * Modifie le filtrage sur les documents historisés.
   */
  public void modifierAffichageHistorique(boolean pAvecHistorique) {
    if (pAvecHistorique != critereDocumentVente.isSansHistorique()) {
      return;
    }
    critereDocumentVente.setSansHistorique(!pAvecHistorique);
    rafraichir();
  }
  
  /**
   * Modifier la plage de date.
   */
  public void modifierPlageDate(Date pDateDebut, Date pDateFin) {
    if (Constantes.equals(critereDocumentVente.getDateCreationDebut(), pDateDebut)
        && Constantes.equals(critereDocumentVente.getDateCreationFin(), pDateFin)) {
      return;
    }
    if (!Constantes.equals(critereDocumentVente.getDateCreationDebut(), pDateDebut)) {
      critereDocumentVente.setDateCreationDebut(pDateDebut);
    }
    if (!Constantes.equals(critereDocumentVente.getDateCreationFin(), pDateFin)) {
      critereDocumentVente.setDateCreationFin(pDateFin);
    }
    rafraichir();
  }
  
  /**
   * Valider la boîte de dialogue.
   */
  public void valider() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible de valider car aucun document de ventes n'est sélectionné.");
    }
    // Générer une erreur si le document de ventes est verrouillé
    if (documentVenteBaseSelectionne.isVerrouille()) {
      throw new MessageErreurException("Impossible d'ouvrir le document de ventes car il est verrouillé.");
    }
    
    // Renseigner le retour et quitter
    retour = RETOUR_MODIFIER_DOCUMENT;
    quitterAvecValidation();
  }
  
  public void dupliquerDocumentVente() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible de dupliquer car aucun document d'origine n'est sélectionné.");
    }
    
    // Renseigner le retour et quitter
    retour = RETOUR_DUPLIQUER_DOCUMENT;
    quitterAvecValidation();
  }
  
  /**
   * Extraction du document de ventes en commande.
   */
  public void extraireEnCommande() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible d'effectuer l'extraction car aucun document d'origine n'est sélectionné.");
    }
    if (!documentVenteBaseSelectionne.isDevis()) {
      throw new MessageErreurException("Impossible d'effectuer l'extraction car le document sélectionné n'est pas un devis.");
    }
    
    // Renseigner le retour et quitter
    retour = RETOUR_EXTRAIRE_EN_COMMANDE;
    quitterAvecValidation();
  }
  
  /**
   * Extraction du document de ventes en bon.
   */
  public void extraireEnBon() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible d'effectuer l'extraction car aucun document d'origine n'est sélectionné.");
    }
    if (!documentVenteBaseSelectionne.isDevis() && !documentVenteBaseSelectionne.isCommande()) {
      throw new MessageErreurException(
          "Impossible d'effectuer l'extraction car le document sélectionné n'est pas un devis ou une commande.");
    }
    
    // Renseigner le retour et quitter
    retour = RETOUR_EXTRAIRE_EN_BON;
    quitterAvecValidation();
  }
  
  /**
   * Extraction du document de ventes en facture.
   */
  public void extraireEnFacture() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible d'effectuer l'extraction car aucun document d'origine n'est sélectionné.");
    }
    if (!documentVenteBaseSelectionne.isDevis() && !documentVenteBaseSelectionne.isCommande()) {
      throw new MessageErreurException(
          "Impossible d'effectuer l'extraction car le document sélectionné n'est pas un devis ou une commande.");
    }
    
    // Renseigner le retour et quitter
    retour = RETOUR_EXTRAIRE_EN_FACTURE;
    quitterAvecValidation();
  }
  
  /**
   * Créer un avoir à partir d'un bon ou d'une facture
   */
  public void creerAvoir() {
    // Vérifier si un document est sélectionné
    if (documentVenteBaseSelectionne == null) {
      throw new MessageErreurException("Impossible de créer un avoir car aucun document d'origine n'est sélectionné.");
    }
    // Vérifier que l'on part d'un bon ou d'une facture
    if (!documentVenteBaseSelectionne.isBon() && !documentVenteBaseSelectionne.isFacture()) {
      throw new MessageErreurException(
          "Impossible de créer un avoir à partir de ce document : " + documentVenteBaseSelectionne.getTypeDocumentVente());
    }
    
    // Renseigner le retour et quitter
    retour = ModeleRechercheDocumentVente.RETOUR_CREER_AVOIR;
    quitterAvecValidation();
  }
  
  // -- Accesseurs
  
  /**
   * Liste des magasins.
   */
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  /**
   * Liste des modes d'expédition.
   */
  public ListeModeExpedition getListeModeExpedition() {
    return listeModeExpedition;
  }
  
  /**
   * Client.
   */
  public Client getClient() {
    return client;
  }
  
  /**
   * Critères de recherche des documents de ventes.
   */
  public CritereDocumentVente getCritereDocumentVente() {
    return critereDocumentVente;
  }
  
  /**
   * Message d'information à afficher dans la boîte de dialogue.
   */
  public Message getMessage() {
    if (listeDocumentVenteBase == null) {
      return Message.getMessageNormal("Document de ventes");
    }
    else if (listeDocumentVenteBase.size() == 0) {
      return Message.getMessageImportant("Aucun document de ventes ne correspond à votre recherche");
    }
    else if (listeDocumentVenteBase.size() == 1) {
      return Message.getMessageNormal("Document de ventes correspondant à votre recherche (1)");
    }
    else if (listeDocumentVenteBase.size() > 1) {
      return Message.getMessageNormal("Documents de ventes correspondants à votre recherche (" + listeDocumentVenteBase.size() + ")");
    }
    else {
      return Message.getMessageNormal("Document de ventes");
    }
  }
  
  /**
   * Résultat de la recherche.
   */
  public ListeDocumentVenteBase getListeDocumentVenteBase() {
    return listeDocumentVenteBase;
  }
  
  /**
   * Index de la ligne sélectionnée.
   */
  public int getLigneSelectionnee() {
    return ligneSelectionnee;
  }
  
  /**
   * Document de ventes sélectionné.
   */
  public DocumentVenteBase getDocumentVenteBaseSelectionne() {
    return documentVenteBaseSelectionne;
  }
  
  /**
   * Type de retour de la boîe de dialogue.
   */
  public int getRetour() {
    return retour;
  }
  
  /**
   * Retourne si un document en déjà en cours de saisie.
   */
  public boolean isDocumentEnCoursDeSaisie() {
    return documentEnCoursSaisie;
  }
  
}
