/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.articlespecial;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.math.BigDecimal;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumModeSaisiePort;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.date.SNDate;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet général de la boîte de dialogue de détail d'un article spécial.
 */
public class VueNegociationAchat extends AbstractVuePanel<ModeleArticleSpecial> {
  private boolean valeurEstSaisie = false;
  
  /**
   * Constructeur.
   */
  public VueNegociationAchat(ModeleArticleSpecial pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construire l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraîchir l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirTitreNegociationAchat();
    rafraichirDateApplicationTarifAchat();
    rafraichirQuantiteUCA();
    rafraichirUniteCommandeAchat();
    rafraichirPrixAchatBrutHT();
    rafraichirRemiseFournisseur1();
    rafraichirRemiseFournisseur2();
    rafraichirRemiseFournisseur3();
    rafraichirRemiseFournisseur4();
    rafraichirPourcentageTaxeMajoration();
    rafraichirValeurConditionnement();
    rafraichirPrixAchatNet();
    rafraichirModeSaisiePort();
    rafraichirValeurPort();
    rafraichirPoids();
    rafraichirCoefficientPort();
    rafraichirPrixRevientFournisseur();
    rafraichirFraisExploitation();
    rafraichirPrixRevientStandard();
  }
  
  // -- Méthodes privées
  
  /**
   * Rafraîchir le titre.
   */
  private void rafraichirTitreNegociationAchat() {
    AdresseFournisseur adresseFournisseur = getModele().getAdresseFournisseur();
    if (adresseFournisseur != null && adresseFournisseur.getId() != null && adresseFournisseur.getAdresse() != null
        && adresseFournisseur.getAdresse().getNom() != null) {
      pZonesAchat.setBorder(new TitledBorder(
          String.format("Négociation d'achat chez %s (%s)", adresseFournisseur.getAdresse().getNom(), adresseFournisseur.getId())));
    }
  }
  
  /**
   * Rafraîchir la date d'application du tarif d'achat.
   */
  private void rafraichirDateApplicationTarifAchat() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    if (conditionAchat != null && conditionAchat.getDateApplication() != null) {
      snDateApplicationTarif.setDate(conditionAchat.getDateApplication());
    }
  }
  
  /**
   * Rafraîchir la quantité UCA.
   */
  private void rafraichirQuantiteUCA() {
    if (getModele().getLigneVente() != null && getModele().getLigneVente().getQuantiteUCV() != null) {
      BigDecimal quantite = getModele().getLigneVente().getQuantiteUCV();
      tfQuantiteUCA.setText(Constantes.formater(quantite, false));
    }
    else {
      tfQuantiteUCA.setText("");
    }
  }
  
  /**
   * Rafraîchir l'UCA.
   */
  private void rafraichirUniteCommandeAchat() {
    IdUnite idUnite = getModele().getArticle().getIdUV();
    if (idUnite != null) {
      tfUniteConditionnementAchat.setText(idUnite.getCode());
      lbUniteAchatBrut.setText(idUnite.getCode());
      lbUnitePrixAchatNetHT.setText(idUnite.getCode());
      lbUnitePrixRevientFournisseurHT.setText(idUnite.getCode());
      lbUnitePrixRevientStandard.setText(idUnite.getCode());
    }
    else {
      tfUniteConditionnementAchat.setText("");
      lbUniteAchatBrut.setText("");
      lbUnitePrixAchatNetHT.setText("");
      lbUnitePrixRevientFournisseurHT.setText("");
      lbUnitePrixRevientStandard.setText("");
    }
  }
  
  /**
   * Rafraîchir le prix d'achat brut HT.
   */
  private void rafraichirPrixAchatBrutHT() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal prix = null;
    if (conditionAchat != null) {
      prix = conditionAchat.getPrixAchatBrutHT();
    }
    if (prix != null) {
      tfPrixAchatBrut.setText(Constantes.formater(prix, true));
    }
    else {
      tfPrixAchatBrut.setText("");
    }
  }
  
  /**
   * Rafraîchir la remise fournisseur 1.
   */
  private void rafraichirRemiseFournisseur1() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal remise = null;
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise1();
    }
    if (remise != null) {
      tfRemiseFrs1.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFrs1.setText("");
    }
  }
  
  /**
   * Rafraîchir la remise fournisseur 2.
   */
  private void rafraichirRemiseFournisseur2() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal remise = null;
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise2();
    }
    if (remise != null) {
      tfRemiseFrs2.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFrs2.setText("");
    }
  }
  
  /**
   * Rafraîchir la remise fournisseur 3.
   */
  private void rafraichirRemiseFournisseur3() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal remise = null;
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise3();
    }
    if (remise != null) {
      tfRemiseFrs3.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFrs3.setText("");
    }
  }
  
  /**
   * Rafraîchir la rmeise fournisseur 4.
   */
  private void rafraichirRemiseFournisseur4() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal remise = null;
    if (conditionAchat != null) {
      remise = conditionAchat.getPourcentageRemise4();
    }
    if (remise != null) {
      tfRemiseFrs4.setText(Constantes.formater(remise, true));
    }
    else {
      tfRemiseFrs4.setText("");
    }
  }
  
  /**
   * Rafraîchir le pourcentage de taxe ou de majoration.
   */
  private void rafraichirPourcentageTaxeMajoration() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getPourcentageTaxeOuMajoration();
    }
    if (valeur != null) {
      tfTaxe.setText(Constantes.formater(valeur, true));
    }
    else {
      tfTaxe.setText("");
    }
  }
  
  /**
   * Rafraîchir le montant de conditionnment.
   */
  private void rafraichirValeurConditionnement() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getMontantConditionnement();
    }
    if (valeur != null) {
      tfValeurConditionnementAchat.setText(Constantes.formater(valeur, true));
    }
    else {
      tfValeurConditionnementAchat.setText("");
    }
  }
  
  /**
   * Rafraîchir le prix d'achat net.
   */
  private void rafraichirPrixAchatNet() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getPrixAchatNetHT();
    }
    if (valeur != null) {
      tfPrixAchatNetHT.setText(Constantes.formater(valeur, true));
    }
    else {
      tfPrixAchatNetHT.setText("");
    }
  }
  
  /**
   * Rafraîchir le mode de saisie du port.
   */
  private void rafraichirModeSaisiePort() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    if (conditionAchat != null) {
      if (conditionAchat.getModeSaisiePort().equals(EnumModeSaisiePort.MONTANT_FOIS_POIDS)) {
        rbMontantPort.setSelected(true);
      }
      else {
        rbCoeffPort.setSelected(true);
      }
      rbMontantPort.setEnabled(true);
      rbCoeffPort.setEnabled(true);
    }
    else {
      rbMontantPort.setEnabled(false);
      rbCoeffPort.setEnabled(false);
    }
    
    tfPortValeur.setEnabled(rbMontantPort.isSelected());
    tfPortPoids.setEnabled(rbMontantPort.isSelected());
    tfPortPourcentage.setEnabled(!rbMontantPort.isSelected());
  }
  
  /**
   * Rafraîchir le montant du port.
   */
  private void rafraichirValeurPort() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getMontantAuPoidsPortHT();
    }
    if (valeur != null) {
      tfPortValeur.setText(Constantes.formater(valeur, true));
    }
    else {
      tfPortValeur.setText("");
    }
  }
  
  /**
   * Rafraîchir le poids.
   */
  private void rafraichirPoids() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal poids = null;
    if (conditionAchat != null) {
      poids = conditionAchat.getPoidsPort();
    }
    if (poids != null) {
      tfPortPoids.setText(Constantes.formater(poids, true));
    }
    else {
      tfPortPoids.setText("");
    }
  }
  
  /**
   * Rafraîchir le coefficient du port.
   */
  private void rafraichirCoefficientPort() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal coefficient = null;
    if (conditionAchat != null) {
      coefficient = conditionAchat.getPourcentagePort();
    }
    if (coefficient != null) {
      tfPortPourcentage.setText(Constantes.formater(coefficient, true));
    }
    else {
      tfPortPourcentage.setText("");
    }
  }
  
  /**
   * Rafraîchir le prix de revient fournisseur.
   */
  private void rafraichirPrixRevientFournisseur() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getPrixRevientFournisseurHT();
    }
    if (valeur != null) {
      tfPrixRevientFournisseurHT.setText(Constantes.formater(valeur, true));
    }
    else {
      tfPrixRevientFournisseurHT.setText("");
    }
  }
  
  /**
   * Rafraîchir les frais d'exploitation.
   */
  private void rafraichirFraisExploitation() {
    if (getModele().isAutoriseFraisExploitation()) {
      ConditionAchat conditionAchat = getModele().getConditionAchat();
      BigDecimal valeur = null;
      if (conditionAchat != null) {
        valeur = conditionAchat.getFraisExploitation();
      }
      if (valeur != null) {
        tfFraisExploitation.setText(Constantes.formater(valeur, true));
      }
      else {
        tfFraisExploitation.setText("");
      }
    }
    else {
      tfFraisExploitation.setVisible(false);
    }
    lbFraisExploitation.setVisible(tfFraisExploitation.isVisible());
    lbPourcentageFraisExploitation.setVisible(tfFraisExploitation.isVisible());
  }
  
  /**
   * Rafraîchir le prix de revient standard.
   */
  private void rafraichirPrixRevientStandard() {
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    BigDecimal valeur = null;
    if (conditionAchat != null) {
      valeur = conditionAchat.getPrixRevientStandardHT();
    }
    if (valeur != null) {
      tfPrixRevientStandard.setText(Constantes.formater(valeur, true));
    }
    else {
      tfPrixRevientStandard.setText("");
    }
  }
  
  /**
   * Teste si la touche du clavier utilisée peut entrainer une modification de valeur.
   */
  private boolean testerModificationValeur(int pKeyCode, boolean pValeurEstModifiee) {
    // Si la valeur a été déjà modifiée alors on retourne vrai
    if (pValeurEstModifiee) {
      return true;
    }
    if ((pKeyCode == KeyEvent.VK_TAB) || (pKeyCode == KeyEvent.VK_LEFT) || (pKeyCode == KeyEvent.VK_RIGHT)
        || (pKeyCode == KeyEvent.VK_UP)) {
      return false;
    }
    return true;
  }
  
  // -- Méthodes évènementielles
  
  /**
   * Traiter le clic bouton.
   * 
   * @param pSNBouton
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de quantite UCA.
   * 
   * @param e
   */
  private void tfQuantiteUCAFocusLost(FocusEvent e) {
    try {
      getModele().modifierQuantiteUCV(tfQuantiteUCA.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du prix d'achat brut.
   * 
   * @param e
   */
  private void tfPrixAchatBrutFocusLost(FocusEvent e) {
    try {
      getModele().modifierPrixAchatBrut(tfPrixAchatBrut.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de remise fournisseur 1.
   * 
   * @param e
   */
  private void tfRemiseFrs1FocusLost(FocusEvent e) {
    try {
      getModele().modifierRemiseFrs1(tfRemiseFrs1.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de remise fournisseur 2.
   * 
   * @param e
   */
  private void tfRemiseFrs2FocusLost(FocusEvent e) {
    try {
      getModele().modifierRemiseFrs2(tfRemiseFrs2.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de remise fournisseur 3.
   * 
   * @param e
   */
  private void tfRemiseFrs3FocusLost(FocusEvent e) {
    try {
      getModele().modifierRemiseFrs3(tfRemiseFrs3.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de remise fournisseur 4.
   * 
   * @param e
   */
  private void tfRemiseFrs4FocusLost(FocusEvent e) {
    try {
      getModele().modifierRemiseFrs4(tfRemiseFrs4.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de taxe ou de majoration.
   * 
   * @param e
   */
  private void tfMajorationsFocusLost(FocusEvent e) {
    try {
      getModele().modifierTaxe(tfTaxe.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie de la valeur de conditionnement.
   * 
   * @param e
   */
  private void tfValeurConditionnementAchatFocusLost(FocusEvent e) {
    try {
      getModele().modifierValeurConditionnementAchat(tfValeurConditionnementAchat.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du montant du port.
   * 
   * @param e
   */
  private void tfPortValeurFocusLost(FocusEvent e) {
    try {
      getModele().modifierPortValeur(tfPortValeur.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du poids.
   * 
   * @param e
   */
  private void tfPortPoidsFocusLost(FocusEvent e) {
    try {
      getModele().modifierPortPoids(tfPortPoids.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la perte de focus du champ de saisie du coefficient de port.
   * 
   * @param e
   */
  private void tfPortPourcentageFocusLost(FocusEvent e) {
    try {
      getModele().modifierPortPourcentage(tfPortPourcentage.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Montant".
   * 
   * @param e
   */
  private void rbMontantPortActionPerformed(ActionEvent e) {
    try {
      getModele().modifierModeSaisiePort(EnumModeSaisiePort.MONTANT_FOIS_POIDS);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la modification de sélection du radio bouton de l'option "Pourcentage".
   * 
   * @param e
   */
  private void rbCoeffPortActionPerformed(ActionEvent e) {
    try {
      getModele().modifierModeSaisiePort(EnumModeSaisiePort.POURCENTAGE_PRIX_NET);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter la touche pressée pour vérifier si la modification du contenu du champ de saisie de prix net peut être effectuée.
   * 
   * @param e
   */
  private void tfPrixNetKeyPressed(KeyEvent e) {
    // Permet de detcter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  /**
   * Traiter la touche pressée pour vérifier si la modification du contenu du champ de saisie de l'indice de marge peut être effectuée.
   * 
   * @param e
   */
  private void tfIndiceDeMargeKeyPressed(KeyEvent e) {
    // Permet de detcter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  /**
   * Traiter la touche pressée pour vérifier si la modification du contenu du champ de saisie de marge peut être effectuée.
   * 
   * @param e
   */
  private void tfMargeKeyPressed(KeyEvent e) {
    // Permet de detcter si la valeur a été saisie ou non
    valeurEstSaisie = testerModificationValeur(e.getKeyCode(), valeurEstSaisie);
  }
  
  /**
   * Traiter la perte de focus du champ de saisie des frais d'exploitation.
   * 
   * @param e
   */
  private void tfFraisExploitationFocusLost(FocusEvent e) {
    try {
      getModele().modifierFraisExploitation(tfFraisExploitation.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlAchat = new SNPanelContenu();
    pnlQuantite = new SNPanel();
    lbQuantiteUCAchat = new JLabel();
    tfQuantiteUCA = new XRiTextField();
    tfUniteConditionnementAchat = new SNTexte();
    pZonesAchat = new SNPanelTitre();
    lbPrixAchatBrut = new SNLabelChamp();
    pnlPrixAchatBrutHT = new SNPanel();
    tfPrixAchatBrut = new SNTexte();
    lbUniteAchatBrut = new SNLabelUnite();
    lbDateApplicationAchat = new SNLabelChamp();
    snDateApplicationTarif = new SNDate();
    lbRemisesFournisseur = new SNLabelChamp();
    pnlRemisesFournisseur = new SNPanel();
    tfRemiseFrs1 = new SNTexte();
    lbPourcentage1 = new SNLabelUnite();
    tfRemiseFrs2 = new SNTexte();
    lbPourcentage2 = new SNLabelUnite();
    tfRemiseFrs3 = new SNTexte();
    lbPourcentage3 = new SNLabelUnite();
    tfRemiseFrs4 = new SNTexte();
    lbPourcentage4 = new SNLabelUnite();
    lbTaxesAchat = new SNLabelChamp();
    pnlTaxeMajoration = new SNPanel();
    tfTaxe = new SNTexte();
    lbPourcentage6 = new SNLabelUnite();
    lbMontantConditionnement = new SNLabelChamp();
    tfValeurConditionnementAchat = new SNTexte();
    lbPrixAchatNetHT = new SNLabelChamp();
    pnlPrixAchatNetHT = new SNPanel();
    tfPrixAchatNetHT = new SNTexte();
    lbUnitePrixAchatNetHT = new SNLabelUnite();
    rbMontantPort = new JRadioButton();
    pnlMontantPort = new SNPanel();
    tfPortValeur = new SNTexte();
    lbSymboleMultiplication = new SNLabelUnite();
    tfPortPoids = new XRiTextField();
    rbCoeffPort = new JRadioButton();
    pnlPourcentagePort = new SNPanel();
    tfPortPourcentage = new SNTexte();
    lbPourcentage5 = new SNLabelUnite();
    lbPrixRevientFournisseurHT = new SNLabelChamp();
    pnlPrixRevientFournisseurHT = new SNPanel();
    tfPrixRevientFournisseurHT = new SNTexte();
    lbUnitePrixRevientFournisseurHT = new SNLabelUnite();
    lbFraisExploitation = new SNLabelChamp();
    pnlFraisExploitation = new SNPanel();
    tfFraisExploitation = new SNTexte();
    lbPourcentageFraisExploitation = new SNLabelUnite();
    lbPRVAchat = new SNLabelChamp();
    pnlPrixRevientStandardHT = new SNPanel();
    tfPrixRevientStandard = new SNTexte();
    lbUnitePrixRevientStandard = new SNLabelUnite();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(965, 550));
    setPreferredSize(new Dimension(945, 420));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlAchat ========
      {
        pnlAchat.setName("pnlAchat");
        pnlAchat.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlAchat.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlAchat.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlAchat.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlAchat.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlQuantite ========
        {
          pnlQuantite.setName("pnlQuantite");
          pnlQuantite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlQuantite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlQuantite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlQuantite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlQuantite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbQuantiteUCAchat ----
          lbQuantiteUCAchat.setText("Quantit\u00e9 en UCA");
          lbQuantiteUCAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbQuantiteUCAchat.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbQuantiteUCAchat.setHorizontalAlignment(SwingConstants.RIGHT);
          lbQuantiteUCAchat.setMinimumSize(new Dimension(150, 30));
          lbQuantiteUCAchat.setPreferredSize(new Dimension(150, 30));
          lbQuantiteUCAchat.setName("lbQuantiteUCAchat");
          pnlQuantite.add(lbQuantiteUCAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfQuantiteUCA ----
          tfQuantiteUCA.setHorizontalAlignment(SwingConstants.RIGHT);
          tfQuantiteUCA.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfQuantiteUCA.setPreferredSize(new Dimension(100, 30));
          tfQuantiteUCA.setMinimumSize(new Dimension(100, 30));
          tfQuantiteUCA.setName("tfQuantiteUCA");
          tfQuantiteUCA.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfQuantiteUCAFocusLost(e);
            }
          });
          pnlQuantite.add(tfQuantiteUCA, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfUniteConditionnementAchat ----
          tfUniteConditionnementAchat.setFont(new Font("sansserif", Font.PLAIN, 14));
          tfUniteConditionnementAchat.setMinimumSize(new Dimension(36, 30));
          tfUniteConditionnementAchat.setPreferredSize(new Dimension(36, 30));
          tfUniteConditionnementAchat.setEnabled(false);
          tfUniteConditionnementAchat.setName("tfUniteConditionnementAchat");
          pnlQuantite.add(tfUniteConditionnementAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlAchat.add(pnlQuantite, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pZonesAchat ========
        {
          pZonesAchat.setOpaque(false);
          pZonesAchat.setMinimumSize(new Dimension(893, 395));
          pZonesAchat.setPreferredSize(new Dimension(893, 395));
          pZonesAchat.setTitre("N\u00e9gociation achats");
          pZonesAchat.setName("pZonesAchat");
          pZonesAchat.setLayout(new GridBagLayout());
          ((GridBagLayout) pZonesAchat.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pZonesAchat.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pZonesAchat.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pZonesAchat.getLayout()).rowWeights =
              new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbPrixAchatBrut ----
          lbPrixAchatBrut.setText("Prix d'achat brut HT");
          lbPrixAchatBrut.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPrixAchatBrut.setComponentPopupMenu(null);
          lbPrixAchatBrut.setPreferredSize(new Dimension(200, 30));
          lbPrixAchatBrut.setMinimumSize(new Dimension(200, 30));
          lbPrixAchatBrut.setMaximumSize(new Dimension(200, 30));
          lbPrixAchatBrut.setFont(lbPrixAchatBrut.getFont().deriveFont(lbPrixAchatBrut.getFont().getStyle() | Font.BOLD));
          lbPrixAchatBrut.setName("lbPrixAchatBrut");
          pZonesAchat.add(lbPrixAchatBrut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixAchatBrutHT ========
          {
            pnlPrixAchatBrutHT.setPreferredSize(new Dimension(155, 30));
            pnlPrixAchatBrutHT.setMinimumSize(new Dimension(155, 30));
            pnlPrixAchatBrutHT.setName("pnlPrixAchatBrutHT");
            pnlPrixAchatBrutHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixAchatBrutHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixAchatBrut ----
            tfPrixAchatBrut.setHorizontalAlignment(SwingConstants.TRAILING);
            tfPrixAchatBrut.setName("tfPrixAchatBrut");
            tfPrixAchatBrut.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPrixAchatBrutFocusLost(e);
              }
            });
            pnlPrixAchatBrutHT.add(tfPrixAchatBrut, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUniteAchatBrut ----
            lbUniteAchatBrut.setText("UA");
            lbUniteAchatBrut.setComponentPopupMenu(null);
            lbUniteAchatBrut.setName("lbUniteAchatBrut");
            pnlPrixAchatBrutHT.add(lbUniteAchatBrut, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlPrixAchatBrutHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbDateApplicationAchat ----
          lbDateApplicationAchat.setText("Date d'application du tarif d'achat");
          lbDateApplicationAchat.setHorizontalAlignment(SwingConstants.RIGHT);
          lbDateApplicationAchat.setMinimumSize(new Dimension(250, 30));
          lbDateApplicationAchat.setPreferredSize(new Dimension(250, 30));
          lbDateApplicationAchat.setMaximumSize(new Dimension(250, 30));
          lbDateApplicationAchat.setName("lbDateApplicationAchat");
          pZonesAchat.add(lbDateApplicationAchat, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snDateApplicationTarif ----
          snDateApplicationTarif.setEnabled(false);
          snDateApplicationTarif.setName("snDateApplicationTarif");
          pZonesAchat.add(snDateApplicationTarif, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRemisesFournisseur ----
          lbRemisesFournisseur.setText("Remises fournisseur");
          lbRemisesFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
          lbRemisesFournisseur.setComponentPopupMenu(null);
          lbRemisesFournisseur.setName("lbRemisesFournisseur");
          pZonesAchat.add(lbRemisesFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlRemisesFournisseur ========
          {
            pnlRemisesFournisseur.setMinimumSize(new Dimension(515, 30));
            pnlRemisesFournisseur.setPreferredSize(new Dimension(515, 30));
            pnlRemisesFournisseur.setName("pnlRemisesFournisseur");
            pnlRemisesFournisseur.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).columnWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlRemisesFournisseur.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfRemiseFrs1 ----
            tfRemiseFrs1.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemiseFrs1.setComponentPopupMenu(null);
            tfRemiseFrs1.setPreferredSize(new Dimension(100, 30));
            tfRemiseFrs1.setMinimumSize(new Dimension(100, 30));
            tfRemiseFrs1.setMaximumSize(new Dimension(100, 30));
            tfRemiseFrs1.setName("tfRemiseFrs1");
            tfRemiseFrs1.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs1FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFrs1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage1 ----
            lbPourcentage1.setText("% ");
            lbPourcentage1.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage1.setComponentPopupMenu(null);
            lbPourcentage1.setPreferredSize(new Dimension(20, 30));
            lbPourcentage1.setMinimumSize(new Dimension(20, 30));
            lbPourcentage1.setMaximumSize(new Dimension(20, 30));
            lbPourcentage1.setName("lbPourcentage1");
            pnlRemisesFournisseur.add(lbPourcentage1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFrs2 ----
            tfRemiseFrs2.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemiseFrs2.setComponentPopupMenu(null);
            tfRemiseFrs2.setPreferredSize(new Dimension(100, 30));
            tfRemiseFrs2.setMinimumSize(new Dimension(100, 30));
            tfRemiseFrs2.setMaximumSize(new Dimension(100, 30));
            tfRemiseFrs2.setName("tfRemiseFrs2");
            tfRemiseFrs2.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs2FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFrs2, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage2 ----
            lbPourcentage2.setText("% ");
            lbPourcentage2.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage2.setComponentPopupMenu(null);
            lbPourcentage2.setPreferredSize(new Dimension(20, 30));
            lbPourcentage2.setMinimumSize(new Dimension(20, 30));
            lbPourcentage2.setMaximumSize(new Dimension(20, 30));
            lbPourcentage2.setName("lbPourcentage2");
            pnlRemisesFournisseur.add(lbPourcentage2, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFrs3 ----
            tfRemiseFrs3.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemiseFrs3.setComponentPopupMenu(null);
            tfRemiseFrs3.setPreferredSize(new Dimension(100, 30));
            tfRemiseFrs3.setMinimumSize(new Dimension(100, 30));
            tfRemiseFrs3.setMaximumSize(new Dimension(100, 30));
            tfRemiseFrs3.setName("tfRemiseFrs3");
            tfRemiseFrs3.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs3FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFrs3, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage3 ----
            lbPourcentage3.setText("% ");
            lbPourcentage3.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage3.setComponentPopupMenu(null);
            lbPourcentage3.setPreferredSize(new Dimension(20, 30));
            lbPourcentage3.setMinimumSize(new Dimension(20, 30));
            lbPourcentage3.setMaximumSize(new Dimension(20, 30));
            lbPourcentage3.setName("lbPourcentage3");
            pnlRemisesFournisseur.add(lbPourcentage3, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfRemiseFrs4 ----
            tfRemiseFrs4.setHorizontalAlignment(SwingConstants.RIGHT);
            tfRemiseFrs4.setComponentPopupMenu(null);
            tfRemiseFrs4.setPreferredSize(new Dimension(100, 30));
            tfRemiseFrs4.setMinimumSize(new Dimension(100, 30));
            tfRemiseFrs4.setMaximumSize(new Dimension(100, 30));
            tfRemiseFrs4.setName("tfRemiseFrs4");
            tfRemiseFrs4.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfRemiseFrs4FocusLost(e);
              }
            });
            pnlRemisesFournisseur.add(tfRemiseFrs4, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage4 ----
            lbPourcentage4.setText("% ");
            lbPourcentage4.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage4.setComponentPopupMenu(null);
            lbPourcentage4.setPreferredSize(new Dimension(20, 30));
            lbPourcentage4.setMinimumSize(new Dimension(20, 30));
            lbPourcentage4.setMaximumSize(new Dimension(20, 30));
            lbPourcentage4.setName("lbPourcentage4");
            pnlRemisesFournisseur.add(lbPourcentage4, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlRemisesFournisseur, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbTaxesAchat ----
          lbTaxesAchat.setText("Taxe ou majoration");
          lbTaxesAchat.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTaxesAchat.setComponentPopupMenu(null);
          lbTaxesAchat.setName("lbTaxesAchat");
          pZonesAchat.add(lbTaxesAchat, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlTaxeMajoration ========
          {
            pnlTaxeMajoration.setName("pnlTaxeMajoration");
            pnlTaxeMajoration.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTaxeMajoration.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfTaxe ----
            tfTaxe.setHorizontalAlignment(SwingConstants.RIGHT);
            tfTaxe.setComponentPopupMenu(null);
            tfTaxe.setMinimumSize(new Dimension(100, 30));
            tfTaxe.setPreferredSize(new Dimension(100, 30));
            tfTaxe.setMaximumSize(new Dimension(100, 30));
            tfTaxe.setName("tfTaxe");
            tfTaxe.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfMajorationsFocusLost(e);
              }
            });
            pnlTaxeMajoration.add(tfTaxe, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage6 ----
            lbPourcentage6.setText("% ");
            lbPourcentage6.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage6.setComponentPopupMenu(null);
            lbPourcentage6.setPreferredSize(new Dimension(20, 30));
            lbPourcentage6.setMinimumSize(new Dimension(20, 30));
            lbPourcentage6.setMaximumSize(new Dimension(20, 30));
            lbPourcentage6.setName("lbPourcentage6");
            pnlTaxeMajoration.add(lbPourcentage6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlTaxeMajoration, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMontantConditionnement ----
          lbMontantConditionnement.setText("Montant conditionnement");
          lbMontantConditionnement.setHorizontalAlignment(SwingConstants.RIGHT);
          lbMontantConditionnement.setComponentPopupMenu(null);
          lbMontantConditionnement.setName("lbMontantConditionnement");
          pZonesAchat.add(lbMontantConditionnement, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfValeurConditionnementAchat ----
          tfValeurConditionnementAchat.setHorizontalAlignment(SwingConstants.RIGHT);
          tfValeurConditionnementAchat.setComponentPopupMenu(null);
          tfValeurConditionnementAchat.setMinimumSize(new Dimension(100, 30));
          tfValeurConditionnementAchat.setPreferredSize(new Dimension(100, 30));
          tfValeurConditionnementAchat.setMaximumSize(new Dimension(100, 30));
          tfValeurConditionnementAchat.setName("tfValeurConditionnementAchat");
          tfValeurConditionnementAchat.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfValeurConditionnementAchatFocusLost(e);
            }
          });
          pZonesAchat.add(tfValeurConditionnementAchat, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixAchatNetHT ----
          lbPrixAchatNetHT.setText("Prix d'achat net HT");
          lbPrixAchatNetHT.setFont(lbPrixAchatNetHT.getFont().deriveFont(lbPrixAchatNetHT.getFont().getStyle() | Font.BOLD));
          lbPrixAchatNetHT.setName("lbPrixAchatNetHT");
          pZonesAchat.add(lbPrixAchatNetHT, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixAchatNetHT ========
          {
            pnlPrixAchatNetHT.setMinimumSize(new Dimension(155, 30));
            pnlPrixAchatNetHT.setMaximumSize(new Dimension(155, 30));
            pnlPrixAchatNetHT.setPreferredSize(new Dimension(155, 30));
            pnlPrixAchatNetHT.setName("pnlPrixAchatNetHT");
            pnlPrixAchatNetHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixAchatNetHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixAchatNetHT ----
            tfPrixAchatNetHT.setEditable(false);
            tfPrixAchatNetHT.setEnabled(false);
            tfPrixAchatNetHT.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixAchatNetHT.setName("tfPrixAchatNetHT");
            pnlPrixAchatNetHT.add(tfPrixAchatNetHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixAchatNetHT ----
            lbUnitePrixAchatNetHT.setText("UA");
            lbUnitePrixAchatNetHT.setName("lbUnitePrixAchatNetHT");
            pnlPrixAchatNetHT.add(lbUnitePrixAchatNetHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlPrixAchatNetHT, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbMontantPort ----
          rbMontantPort.setText("  Montant port HT");
          rbMontantPort.setFont(rbMontantPort.getFont().deriveFont(rbMontantPort.getFont().getSize() + 2f));
          rbMontantPort.setHorizontalAlignment(SwingConstants.RIGHT);
          rbMontantPort.setMaximumSize(new Dimension(134, 19));
          rbMontantPort.setMinimumSize(new Dimension(134, 19));
          rbMontantPort.setPreferredSize(new Dimension(134, 19));
          rbMontantPort.setName("rbMontantPort");
          rbMontantPort.addActionListener(e -> rbMontantPortActionPerformed(e));
          pZonesAchat.add(rbMontantPort, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlMontantPort ========
          {
            pnlMontantPort.setMinimumSize(new Dimension(260, 30));
            pnlMontantPort.setPreferredSize(new Dimension(260, 30));
            pnlMontantPort.setName("pnlMontantPort");
            pnlMontantPort.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlMontantPort.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlMontantPort.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlMontantPort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlMontantPort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPortValeur ----
            tfPortValeur.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPortValeur.setComponentPopupMenu(null);
            tfPortValeur.setMinimumSize(new Dimension(100, 30));
            tfPortValeur.setPreferredSize(new Dimension(100, 30));
            tfPortValeur.setMaximumSize(new Dimension(100, 30));
            tfPortValeur.setName("tfPortValeur");
            tfPortValeur.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortValeurFocusLost(e);
              }
            });
            pnlMontantPort.add(tfPortValeur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbSymboleMultiplication ----
            lbSymboleMultiplication.setText("x poids");
            lbSymboleMultiplication.setHorizontalAlignment(SwingConstants.CENTER);
            lbSymboleMultiplication.setMaximumSize(new Dimension(50, 30));
            lbSymboleMultiplication.setMinimumSize(new Dimension(50, 30));
            lbSymboleMultiplication.setPreferredSize(new Dimension(50, 30));
            lbSymboleMultiplication.setName("lbSymboleMultiplication");
            pnlMontantPort.add(lbSymboleMultiplication, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPortPoids ----
            tfPortPoids.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPortPoids.setComponentPopupMenu(null);
            tfPortPoids.setMinimumSize(new Dimension(100, 30));
            tfPortPoids.setPreferredSize(new Dimension(100, 30));
            tfPortPoids.setMaximumSize(new Dimension(100, 30));
            tfPortPoids.setFont(new Font("sansserif", Font.PLAIN, 14));
            tfPortPoids.setName("tfPortPoids");
            tfPortPoids.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortPoidsFocusLost(e);
              }
            });
            pnlMontantPort.add(tfPortPoids, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlMontantPort, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- rbCoeffPort ----
          rbCoeffPort.setText("Pourcentage port");
          rbCoeffPort.setFont(rbCoeffPort.getFont().deriveFont(rbCoeffPort.getFont().getSize() + 2f));
          rbCoeffPort.setHorizontalAlignment(SwingConstants.RIGHT);
          rbCoeffPort.setMaximumSize(new Dimension(134, 19));
          rbCoeffPort.setMinimumSize(new Dimension(134, 19));
          rbCoeffPort.setPreferredSize(new Dimension(134, 19));
          rbCoeffPort.setName("rbCoeffPort");
          rbCoeffPort.addActionListener(e -> rbCoeffPortActionPerformed(e));
          pZonesAchat.add(rbCoeffPort, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPourcentagePort ========
          {
            pnlPourcentagePort.setMinimumSize(new Dimension(155, 30));
            pnlPourcentagePort.setPreferredSize(new Dimension(155, 30));
            pnlPourcentagePort.setName("pnlPourcentagePort");
            pnlPourcentagePort.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPourcentagePort.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPortPourcentage ----
            tfPortPourcentage.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPortPourcentage.setComponentPopupMenu(null);
            tfPortPourcentage.setMinimumSize(new Dimension(100, 30));
            tfPortPourcentage.setPreferredSize(new Dimension(100, 30));
            tfPortPourcentage.setMaximumSize(new Dimension(100, 30));
            tfPortPourcentage.setName("tfPortPourcentage");
            tfPortPourcentage.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPortPourcentageFocusLost(e);
              }
            });
            pnlPourcentagePort.add(tfPortPourcentage, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentage5 ----
            lbPourcentage5.setText("% ");
            lbPourcentage5.setHorizontalAlignment(SwingConstants.LEFT);
            lbPourcentage5.setComponentPopupMenu(null);
            lbPourcentage5.setPreferredSize(new Dimension(20, 30));
            lbPourcentage5.setMinimumSize(new Dimension(20, 30));
            lbPourcentage5.setMaximumSize(new Dimension(20, 30));
            lbPourcentage5.setName("lbPourcentage5");
            pnlPourcentagePort.add(lbPourcentage5, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlPourcentagePort, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPrixRevientFournisseurHT ----
          lbPrixRevientFournisseurHT.setText("Prix de revient fournisseur HT");
          lbPrixRevientFournisseurHT
              .setFont(lbPrixRevientFournisseurHT.getFont().deriveFont(lbPrixRevientFournisseurHT.getFont().getStyle() | Font.BOLD));
          lbPrixRevientFournisseurHT.setMaximumSize(new Dimension(200, 30));
          lbPrixRevientFournisseurHT.setMinimumSize(new Dimension(200, 30));
          lbPrixRevientFournisseurHT.setPreferredSize(new Dimension(225, 30));
          lbPrixRevientFournisseurHT.setName("lbPrixRevientFournisseurHT");
          pZonesAchat.add(lbPrixRevientFournisseurHT, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlPrixRevientFournisseurHT ========
          {
            pnlPrixRevientFournisseurHT.setPreferredSize(new Dimension(155, 30));
            pnlPrixRevientFournisseurHT.setMinimumSize(new Dimension(155, 30));
            pnlPrixRevientFournisseurHT.setMaximumSize(new Dimension(155, 30));
            pnlPrixRevientFournisseurHT.setName("pnlPrixRevientFournisseurHT");
            pnlPrixRevientFournisseurHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixRevientFournisseurHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixRevientFournisseurHT ----
            tfPrixRevientFournisseurHT.setName("tfPrixRevientFournisseurHT");
            tfPrixRevientFournisseurHT.setEditable(false);
            tfPrixRevientFournisseurHT.setEnabled(false);
            tfPrixRevientFournisseurHT.setHorizontalAlignment(SwingConstants.RIGHT);
            pnlPrixRevientFournisseurHT.add(tfPrixRevientFournisseurHT, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixRevientFournisseurHT ----
            lbUnitePrixRevientFournisseurHT.setText("UA");
            lbUnitePrixRevientFournisseurHT.setName("lbUnitePrixRevientFournisseurHT");
            pnlPrixRevientFournisseurHT.add(lbUnitePrixRevientFournisseurHT, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlPrixRevientFournisseurHT, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbFraisExploitation ----
          lbFraisExploitation.setText("Frais d'exploitation");
          lbFraisExploitation.setName("lbFraisExploitation");
          pZonesAchat.add(lbFraisExploitation, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ======== pnlFraisExploitation ========
          {
            pnlFraisExploitation.setPreferredSize(new Dimension(155, 30));
            pnlFraisExploitation.setMinimumSize(new Dimension(155, 30));
            pnlFraisExploitation.setMaximumSize(new Dimension(155, 30));
            pnlFraisExploitation.setName("pnlFraisExploitation");
            pnlFraisExploitation.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlFraisExploitation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfFraisExploitation ----
            tfFraisExploitation.setMaximumSize(new Dimension(100, 30));
            tfFraisExploitation.setHorizontalAlignment(SwingConstants.RIGHT);
            tfFraisExploitation.setName("tfFraisExploitation");
            tfFraisExploitation.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfFraisExploitationFocusLost(e);
              }
            });
            pnlFraisExploitation.add(tfFraisExploitation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPourcentageFraisExploitation ----
            lbPourcentageFraisExploitation.setText("%");
            lbPourcentageFraisExploitation.setName("lbPourcentageFraisExploitation");
            pnlFraisExploitation.add(lbPourcentageFraisExploitation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlFraisExploitation, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPRVAchat ----
          lbPRVAchat.setText("Prix de revient standard HT");
          lbPRVAchat.setHorizontalAlignment(SwingConstants.RIGHT);
          lbPRVAchat.setComponentPopupMenu(null);
          lbPRVAchat.setPreferredSize(new Dimension(200, 30));
          lbPRVAchat.setMinimumSize(new Dimension(200, 30));
          lbPRVAchat.setMaximumSize(new Dimension(200, 30));
          lbPRVAchat.setFont(lbPRVAchat.getFont().deriveFont(lbPRVAchat.getFont().getStyle() | Font.BOLD));
          lbPRVAchat.setName("lbPRVAchat");
          pZonesAchat.add(lbPRVAchat, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlPrixRevientStandardHT ========
          {
            pnlPrixRevientStandardHT.setName("pnlPrixRevientStandardHT");
            pnlPrixRevientStandardHT.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlPrixRevientStandardHT.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
            
            // ---- tfPrixRevientStandard ----
            tfPrixRevientStandard.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixRevientStandard.setComponentPopupMenu(null);
            tfPrixRevientStandard.setMinimumSize(new Dimension(100, 30));
            tfPrixRevientStandard.setPreferredSize(new Dimension(100, 30));
            tfPrixRevientStandard.setEnabled(false);
            tfPrixRevientStandard.setMaximumSize(new Dimension(100, 30));
            tfPrixRevientStandard.setName("tfPrixRevientStandard");
            pnlPrixRevientStandardHT.add(tfPrixRevientStandard, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbUnitePrixRevientStandard ----
            lbUnitePrixRevientStandard.setText("UA");
            lbUnitePrixRevientStandard.setComponentPopupMenu(null);
            lbUnitePrixRevientStandard.setPreferredSize(new Dimension(50, 30));
            lbUnitePrixRevientStandard.setMinimumSize(new Dimension(50, 30));
            lbUnitePrixRevientStandard.setMaximumSize(new Dimension(50, 30));
            lbUnitePrixRevientStandard.setName("lbUnitePrixRevientStandard");
            pnlPrixRevientStandardHT.add(lbUnitePrixRevientStandard, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pZonesAchat.add(pnlPrixRevientStandardHT, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlAchat.add(pZonesAchat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlAchat, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    
    // ---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(rbMontantPort);
    buttonGroup1.add(rbCoeffPort);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlAchat;
  private SNPanel pnlQuantite;
  private JLabel lbQuantiteUCAchat;
  private XRiTextField tfQuantiteUCA;
  private SNTexte tfUniteConditionnementAchat;
  private SNPanelTitre pZonesAchat;
  private SNLabelChamp lbPrixAchatBrut;
  private SNPanel pnlPrixAchatBrutHT;
  private SNTexte tfPrixAchatBrut;
  private SNLabelUnite lbUniteAchatBrut;
  private SNLabelChamp lbDateApplicationAchat;
  private SNDate snDateApplicationTarif;
  private SNLabelChamp lbRemisesFournisseur;
  private SNPanel pnlRemisesFournisseur;
  private SNTexte tfRemiseFrs1;
  private SNLabelUnite lbPourcentage1;
  private SNTexte tfRemiseFrs2;
  private SNLabelUnite lbPourcentage2;
  private SNTexte tfRemiseFrs3;
  private SNLabelUnite lbPourcentage3;
  private SNTexte tfRemiseFrs4;
  private SNLabelUnite lbPourcentage4;
  private SNLabelChamp lbTaxesAchat;
  private SNPanel pnlTaxeMajoration;
  private SNTexte tfTaxe;
  private SNLabelUnite lbPourcentage6;
  private SNLabelChamp lbMontantConditionnement;
  private SNTexte tfValeurConditionnementAchat;
  private SNLabelChamp lbPrixAchatNetHT;
  private SNPanel pnlPrixAchatNetHT;
  private SNTexte tfPrixAchatNetHT;
  private SNLabelUnite lbUnitePrixAchatNetHT;
  private JRadioButton rbMontantPort;
  private SNPanel pnlMontantPort;
  private SNTexte tfPortValeur;
  private SNLabelUnite lbSymboleMultiplication;
  private XRiTextField tfPortPoids;
  private JRadioButton rbCoeffPort;
  private SNPanel pnlPourcentagePort;
  private SNTexte tfPortPourcentage;
  private SNLabelUnite lbPourcentage5;
  private SNLabelChamp lbPrixRevientFournisseurHT;
  private SNPanel pnlPrixRevientFournisseurHT;
  private SNTexte tfPrixRevientFournisseurHT;
  private SNLabelUnite lbUnitePrixRevientFournisseurHT;
  private SNLabelChamp lbFraisExploitation;
  private SNPanel pnlFraisExploitation;
  private SNTexte tfFraisExploitation;
  private SNLabelUnite lbPourcentageFraisExploitation;
  private SNLabelChamp lbPRVAchat;
  private SNPanel pnlPrixRevientStandardHT;
  private SNTexte tfPrixRevientStandard;
  private SNLabelUnite lbUnitePrixRevientStandard;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
