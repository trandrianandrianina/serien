/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.detailligne;

import java.math.BigDecimal;
import java.math.RoundingMode;

import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;

public class ColonneTarif {
  // Variables
  private int numeroColonne = TarifArticle.COLONNE_TARIF_PUBLIC;
  private BigDecimal prixNetHT = null;
  private BigDecimal prixNetTTC = null;
  private boolean isAffichageTTC = false;
  
  /**
   * Constructeur.
   */
  public ColonneTarif(int pNumeroColonne) {
    numeroColonne = pNumeroColonne;
    prixNetHT = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
    prixNetTTC = BigDecimal.ZERO.setScale(Constantes.DEUX_DECIMALES, RoundingMode.HALF_UP);
  }
  
  /**
   * Constructeur.
   */
  public ColonneTarif(int pNumeroColonne, BigDecimal pPrixNetHT, BigDecimal pPrixNetTTC, boolean pAffichageTTC) {
    numeroColonne = pNumeroColonne;
    prixNetHT = pPrixNetHT;
    prixNetTTC = pPrixNetTTC;
    isAffichageTTC = pAffichageTTC;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne la chaine représentative.
   */
  @Override
  public String toString() {
    // Dans le cas où l'ion veut juste afficher le numéro de la colonne
    if (prixNetHT.compareTo(BigDecimal.ZERO) == 0 && prixNetTTC.compareTo(BigDecimal.ZERO) == 0) {
      // La colonne 0 n'existe pas donc on affichera un libellé vide
      if (numeroColonne == 0) {
        return "";
      }
      // Sinon on affiche le libellé avec le numéro de la colonne
      else {
        return String.format("%s %d", LigneVente.TEXTE_ORIGINE_PRIX_COLONNE, numeroColonne);
      }
    }
    // Sinon
    BigDecimal prix = prixNetHT;
    if (isAffichageTTC) {
      prix = prixNetTTC;
    }
    // La colonne 0 n'existe pas donc on affichera un libellé vide
    if (numeroColonne == 0) {
      return "";
    }
    // Sinon on affiche le libellé avec le numéro de la colonne et le prix associé
    else {
      return String.format("%s %d=%s", LigneVente.TEXTE_ORIGINE_PRIX_COLONNE, numeroColonne, Constantes.formater(prix, true));
    }
  }
  
  @Override
  public boolean equals(Object pColonneAComparer) {
    // Tester si l'objet est nous-même (optimisation)
    if (pColonneAComparer == this) {
      return true;
    }
    
    // Tester si l'objet est du type attendu (teste la nullité également)
    if (!(pColonneAComparer instanceof ColonneTarif)) {
      return false;
    }
    
    // Comparer les valeurs internes en commençant par les plus discriminantes
    ColonneTarif colonneTarif = (ColonneTarif) pColonneAComparer;
    return numeroColonne == colonneTarif.numeroColonne && prixNetHT == colonneTarif.prixNetHT && prixNetTTC == colonneTarif.prixNetTTC;
  }
  
  @Override
  public int hashCode() {
    int cle = 17;
    cle = 37 * cle + numeroColonne;
    cle = 37 * cle + prixNetHT.hashCode();
    cle = 37 * cle + prixNetTTC.hashCode();
    return cle;
  }
  
  // -- Accesseurs
  
  public int getNumeroColonne() {
    return numeroColonne;
  }
  
  public void setAffichageTTC(boolean isAffichageTTC) {
    this.isAffichageTTC = isAffichageTTC;
  }
  
}
