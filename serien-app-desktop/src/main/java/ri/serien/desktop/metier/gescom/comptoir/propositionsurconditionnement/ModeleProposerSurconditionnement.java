/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.propositionsurconditionnement;

import java.math.BigDecimal;

import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.Surconditionnement;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modèle de la boîte de dialoge qui propose la quantité correspondant au conditionnement supérieur.
 */
public class ModeleProposerSurconditionnement extends AbstractModeleDialogue {
  // Variables
  private VueProposerSurconditionnement vue = null;
  private Surconditionnement surconditionnement = null;
  private LigneVente ligneVente = null;
  private Article article = null;
  private StringBuffer libelleQuantiteSaisie = null;
  private StringBuffer libelleQuantiteSuperieure = null;
  private boolean isSaisieSurconditionnement = false;
  private boolean isProposable = false;
  private BigDecimal quantiteCalculee = BigDecimal.ZERO;
  
  /**
   * Constructeur.
   */
  public ModeleProposerSurconditionnement(SessionBase pSession, Article pArticle, LigneVente pLigneVente) {
    super(pSession);
    article = pArticle;
    ligneVente = pLigneVente;
    surconditionnement = new Surconditionnement(article, ligneVente);
    isProposable = surconditionnement.isProposable();
  }
  
  // Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    surconditionnement.initialiser(getIdSession());
    setQuantiteCalculee(surconditionnement.getQuantiteCalculee());
    setLibelleQuantiteSaisie(surconditionnement.getLibelleQuantiteSaisie());
    setLibelleQuantiteSuperieure(surconditionnement.getLibelleQuantiteSuperieure());
  }
  
  // Méthodes privées
  
  /**
   * Initialise les variables du modèle.
   */
  private void effacerVariables() {
  }
  
  // -- Accesseurs
  
  public VueProposerSurconditionnement getVue() {
    return vue;
  }
  
  public boolean isSaisieSurconditionnement() {
    return isSaisieSurconditionnement;
  }
  
  public void setSaisieSurconditionnement(boolean isSaisieSurconditionnement) {
    this.isSaisieSurconditionnement = isSaisieSurconditionnement;
  }
  
  public StringBuffer getLibelleQuantiteSaisie() {
    return libelleQuantiteSaisie;
  }
  
  public void setLibelleQuantiteSaisie(StringBuffer libelleQuantiteSaisie) {
    this.libelleQuantiteSaisie = libelleQuantiteSaisie;
  }
  
  public StringBuffer getLibelleQuantiteSuperieure() {
    return libelleQuantiteSuperieure;
  }
  
  public void setLibelleQuantiteSuperieure(StringBuffer libelleQuantiteSuperieure) {
    this.libelleQuantiteSuperieure = libelleQuantiteSuperieure;
  }
  
  public BigDecimal getQuantiteCalculee() {
    return quantiteCalculee;
  }
  
  public void setQuantiteCalculee(BigDecimal quantiteCalculee) {
    this.quantiteCalculee = quantiteCalculee;
  }
  
  public boolean isProposable() {
    return isProposable;
  }
  
}
