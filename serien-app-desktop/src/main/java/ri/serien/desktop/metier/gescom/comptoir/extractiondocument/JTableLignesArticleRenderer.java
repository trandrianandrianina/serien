/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.extractiondocument;

import java.awt.Color;
import java.awt.Component;
import java.math.BigDecimal;
import java.util.List;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;

public class JTableLignesArticleRenderer extends DefaultTableCellRenderer {
  // Variables
  private List<LigneVente> listeLigneVente;
  
  // -- Méthodes publiques
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component cellule = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    if (!isSelected) {
      if (value != null) {
        if (column == 0 || column == 2 || column == 5 || column == 6) {
          try {
            BigDecimal valeur = Constantes.convertirTexteEnBigDecimal((String) value);
            if (valeur.compareTo(BigDecimal.ZERO) > 0) {
              cellule.setForeground(Color.BLACK);
            }
            else {
              cellule.setForeground(Color.RED);
            }
          }
          catch (MessageErreurException e) {
            cellule.setForeground(Color.BLACK);
          }
        }
        else {
          cellule.setForeground(Color.BLACK);
        }
        if (listeLigneVente != null && listeLigneVente.size() > row && listeLigneVente.get(row) != null
            && listeLigneVente.get(row).isLigneRegroupee()) {
          cellule.setBackground(Constantes.COULEUR_LISTE_FOND_COMMENTAIRE);
        }
        else {
          cellule.setBackground(Color.WHITE);
        }
      }
    }
    
    // Justification des colonnes
    justifierColonnesNormal(cellule, column);
    
    return cellule;
  }
  
  // -- Méthodes privées
  
  /**
   * Justification pour lorsque le document est en création.
   */
  private void justifierColonnesNormal(Component pCellule, int pColonne) {
    JLabel label = (JLabel) pCellule;
    switch (pColonne) {
      case 0:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 1:
        label.setHorizontalAlignment(CENTER);
        break;
      case 2:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 3:
        label.setHorizontalAlignment(LEFT);
        break;
      case 4:
        label.setHorizontalAlignment(LEFT);
        break;
      case 5:
        label.setHorizontalAlignment(RIGHT);
        break;
      case 6:
        label.setHorizontalAlignment(RIGHT);
        break;
    }
  }
  
  // -- Accesseurs
  
  public void setListeLigneVente(List<LigneVente> pListeLigneVente) {
    this.listeLigneVente = pListeLigneVente;
  }
  
}
