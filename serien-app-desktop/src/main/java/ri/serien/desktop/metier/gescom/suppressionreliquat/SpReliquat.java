/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.suppressionreliquat;

import java.awt.BorderLayout;

import ri.serien.desktop.divers.ClientSerieN;
import ri.serien.desktop.sessions.SessionJava;

public class SpReliquat extends SessionJava {
  /**
   * Constructeur.
   */
  public SpReliquat(ClientSerieN pFenetre, Object pParametreMenu) {
    super(pFenetre, pParametreMenu);
    initialiserSession();
    
    ModeleReliquat modele = new ModeleReliquat(this);
    VueReliquat vue = new VueReliquat(modele);
    panelPanel.add(vue, BorderLayout.CENTER);
    vue.afficher();
  }
}
