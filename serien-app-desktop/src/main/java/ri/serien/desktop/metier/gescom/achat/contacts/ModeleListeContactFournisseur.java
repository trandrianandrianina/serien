/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.contacts;

import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.CategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.IdCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Boîte de dialogue permettant de saisir des commentaires.
 */
public final class ModeleListeContactFournisseur extends AbstractModeleDialogue {
  
  private IdEtablissement idEtablissement = null;
  private Fournisseur fournisseur = null;
  private ListeCategorieContact listeCategorieContact = null;
  private Contact contactPrincipal = null;
  
  /**
   * Constructeur privé pour obliger l'utilisation des méthodes factory.
   */
  public ModeleListeContactFournisseur(SessionBase pSession, Fournisseur pFournisseur) {
    super(pSession);
    fournisseur = pFournisseur;
  }
  
  // -- Méthodes standards du modèle
  
  @Override
  public void initialiserDonnees() {
    listeCategorieContact = ListeCategorieContact.charger(getSession().getIdSession());
  }
  
  @Override
  public void chargerDonnees() {
    if (fournisseur == null) {
      throw new MessageErreurException("Impossible de charger la liste de contacts. il n'y a pas de fournisseur associé.");
    }
    fournisseur.chargerListeContact(getIdSession());
    contactPrincipal = fournisseur.getContactPrincipal();
  }
  
  // -- Méthodes publiques
  
  /**
   * Permet de retourner le libellé d'un contact sur la base de son identifiant
   * On sert de la liste complète des contacts pour chercher le contact à partir de son identifiant
   */
  public String retournerLibelleContact(IdCategorieContact pIdCategorieContact) {
    if (pIdCategorieContact == null || listeCategorieContact == null) {
      return "";
    }
    CategorieContact categorieContact = listeCategorieContact.get(pIdCategorieContact);
    if (categorieContact == null) {
      return "";
    }
    
    return categorieContact.getLibelle();
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Liste des contacts du fournisseur en cours
   */
  public ListeContact getListeContact() {
    return fournisseur.getListeContact();
  }
  
  /**
   * Contact principal du fournisseur en cours
   */
  public Contact getContactPrincipal() {
    return contactPrincipal;
  }
  
}
