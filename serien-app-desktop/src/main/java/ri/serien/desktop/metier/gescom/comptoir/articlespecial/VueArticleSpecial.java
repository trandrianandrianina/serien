/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.articlespecial;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dialog;
import java.awt.Dimension;

import javax.swing.JTabbedPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.moteur.mvc.InterfaceCleVue;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

/**
 * Vue de de la boîte de dialogue du détail d'un article spécial.
 * 
 * Cette boîte de dialogue comporte deux onglets gérés dans des vues enfants :
 * - onglet "Informations générales",
 * - onglet ""Négociation achat".
 * 
 */
public class VueArticleSpecial extends AbstractVueDialogue<ModeleArticleSpecial> {
  // Variables
  private VueGeneral vueGeneral;
  private VueNegociationAchat vueNegociationAchat;
  
  /**
   * Constructeur.
   */
  public VueArticleSpecial(ModeleArticleSpecial pModele) {
    super(pModele);
  }
  
  /**
   * Initialiser les composants graphiques de l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    
    // Créer l'onglet général
    vueGeneral = new VueGeneral(getModele());
    ajouterVueEnfant(EnumCleVueArticleSpecial.ONGLET_GENERAL, vueGeneral);
    tbpOnglets.addTab("Général", null, vueGeneral, "Informations générales");
    
    // Créer l'onglet négociation achat
    vueNegociationAchat = new VueNegociationAchat(getModele());
    ajouterVueEnfant(EnumCleVueArticleSpecial.ONGLET_NEGOCIATION_ACHAT, vueNegociationAchat);
    tbpOnglets.addTab("Négociation achat", null, vueNegociationAchat, "Informations de négociation achat");
  }
  
  /**
   * Rafraichir l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirVue();
  }
  
  /**
   * Rafraichir la vue active.
   */
  private void rafraichirVue() {
    // Afficher ou masquer les onglets suivants les données
    tbpOnglets.setEnabledAt(EnumCleVueArticleSpecial.ONGLET_GENERAL.getIndex(), true);
    tbpOnglets.setEnabledAt(EnumCleVueArticleSpecial.ONGLET_NEGOCIATION_ACHAT.getIndex(), getModele().getFamille() != null);
    
    // Sélectioner l'onglet actuellement actif
    InterfaceCleVue interfaceCleVue = getModele().getCleVueEnfantActive();
    if (interfaceCleVue != null) {
      tbpOnglets.setSelectedIndex(interfaceCleVue.getIndex());
    }
  }
  
  /**
   * Traiter les clics que les onglets pour changer la vue active.
   */
  private void pnlOngletsStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().changerOnglet(tbpOnglets.getSelectedIndex());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * JFormDesigner : on touche plus en dessous !
   */
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    tbpOnglets = new JTabbedPane();
    
    // ======== this ========
    setTitle("D\u00e9tail de l'article sp\u00e9cial");
    setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
    setResizable(false);
    setMinimumSize(new Dimension(1000, 650));
    setBackground(new Color(239, 239, 222));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== tbpOnglets ========
    {
      tbpOnglets.setFont(tbpOnglets.getFont().deriveFont(tbpOnglets.getFont().getSize() + 3f));
      tbpOnglets.setPreferredSize(new Dimension(190, 200));
      tbpOnglets.setBackground(new Color(239, 239, 222));
      tbpOnglets.setOpaque(true);
      tbpOnglets.setName("tbpOnglets");
      tbpOnglets.addChangeListener(new ChangeListener() {
        @Override
        public void stateChanged(ChangeEvent e) {
          pnlOngletsStateChanged(e);
        }
      });
    }
    contentPane.add(tbpOnglets, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JTabbedPane tbpOnglets;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
