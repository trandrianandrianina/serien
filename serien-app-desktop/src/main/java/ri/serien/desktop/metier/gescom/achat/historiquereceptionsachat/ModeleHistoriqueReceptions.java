/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.historiquereceptionsachat;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.gescom.achat.document.EnumTypeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.IdReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.rmi.ManagerServiceDocumentAchat;
import ri.serien.libcommun.rmi.ManagerServiceFournisseur;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

public class ModeleHistoriqueReceptions extends AbstractModeleDialogue {
  // Constantes
  private static final int NOMBRE_LIGNES_HISTORIQUE = 15;
  
  // Variables
  private Article article = null;
  private Etablissement etablissement = null;
  private ListeMagasin listeMagasin = null;
  private ListeUnite listeUnite = null;
  private ListeFournisseur listeFournisseur = null;
  private ListeReceptionAchat listeReceptionAchat = null;
  private List<IdLigneAchat> listeIdLigneAchatReception = null;
  private int indexRechercheDocument = -1;
  private int indexPremiereLigneAffichee = 0;
  private int nombreLigneAffichee = 0;
  
  /**
   * Constructeur.
   */
  public ModeleHistoriqueReceptions(SessionBase pSession, Article pArticle) {
    super(pSession);
    article = pArticle;
    chargerDonnees();
  }
  
  // -- Méthodes publiques
  
  @Override
  public void initialiserDonnees() {
  }
  
  @Override
  public void chargerDonnees() {
    // Récupération de la liste des magasins
    listeMagasin = new ListeMagasin();
    listeMagasin = listeMagasin.charger(getIdSession(), article.getId().getIdEtablissement());
    // Récupération de la liste des unités
    listeUnite = new ListeUnite();
    listeUnite = listeUnite.charger(getIdSession());
    // fournisseurs
    listeFournisseur = new ListeFournisseur();
    listeFournisseur =
        ManagerServiceFournisseur.chargerListeFournisseurEtablissement(getIdSession(), article.getId().getIdEtablissement());
    
    // Charge la liste des documents d'achat de type réception
    chargerReceptionsAchat();
  }
  
  /**
   * Retourne la liste complète des identifiants des réceptions.
   */
  private void chargerReceptionsAchat() {
    // Constitution des critères de recherche pour l'historique
    CritereLigneAchat criteresHistorique = new CritereLigneAchat();
    criteresHistorique.setIdEtablissement(article.getId().getIdEtablissement());
    // Le type de document COMMANDE est ajouté pour retrouver les documents avant le changement de l'identifiant des réceptions (SUF = 0)
    criteresHistorique.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.COMMANDE);
    // Le type de document RECEPTION est ajouté pour retrouver les documents après le changement de l'identifiant des réceptions (SUF > 0)
    criteresHistorique.ajouterTypeDocumentAchat(EnumTypeDocumentAchat.RECEPTION);
    criteresHistorique.setIdArticle(article.getId());
    criteresHistorique.setRechercherReception(true);
    
    // Chargement des identifiants des lignes d'achats suivant les critères définis
    List<IdLigneAchat> listeIdLigneAchat = ManagerServiceDocumentAchat.chargerListeIdLignesAchat(getIdSession(), criteresHistorique);
    if (listeIdLigneAchat == null || listeIdLigneAchat.isEmpty()) {
      return;
    }
    
    // Traitement des idetifiants trouvés
    listeReceptionAchat = new ListeReceptionAchat();
    listeIdLigneAchatReception = new ArrayList<IdLigneAchat>();
    for (IdLigneAchat idLigneAchat : listeIdLigneAchat) {
      ReceptionAchat reception =
          new ReceptionAchat(IdReceptionAchat.getInstance(idLigneAchat.getIdDocumentAchat(), idLigneAchat.getNumeroLigne()));
      reception.setIsCharge(false);
      listeReceptionAchat.add(reception);
      listeIdLigneAchatReception.add(idLigneAchat);
    }
    chargerPlageReceptions(0, NOMBRE_LIGNES_HISTORIQUE);
  }
  
  /**
   * Charger les données des réceptions comprises entre deux lignes.
   * Les informations des réceptions sont chargées uniquement si cela n'a pas déjà été fait.
   */
  private void chargerPlageReceptions(int pPremiereLigne, int pNombreLigne) {
    if (listeIdLigneAchatReception == null) {
      return;
    }
    
    // Sortir si aucune ligne n'est visible
    if (pPremiereLigne < 0 || pNombreLigne <= 0 || listeIdLigneAchatReception.size() <= 0) {
      return;
    }
    
    // Calculer l'index maximum à traiter
    int iMax = Math.min(pPremiereLigne + pNombreLigne, listeIdLigneAchatReception.size());
    
    // Lister les id des réceptions dont il faut charger les informations
    List<IdLigneAchat> listeIdReceptionsACharger = new ArrayList<IdLigneAchat>();
    
    for (int i = pPremiereLigne; i < iMax; i++) {
      ReceptionAchat reception = listeReceptionAchat.get(i);
      if (!reception.isCharge()) {
        listeIdReceptionsACharger
            .add(IdLigneAchat.getInstance(reception.getId().getIdDocumentAchat(), reception.getId().getNumeroLigne()));
      }
    }
    
    // Quitter immédiatement si aucune réceptions n'est à charger
    if (listeIdReceptionsACharger.isEmpty()) {
      return;
    }
    
    // Charger les informations minimales des documents pour affichage en liste
    ListeReceptionAchat listeReceptionsCharge =
        ManagerServiceDocumentAchat.chargerListeReceptionAchat(getIdSession(), listeIdReceptionsACharger);
    
    // Mettre les réceptions chargées au bon endroit dans la liste de résultat
    for (int i = pPremiereLigne; i < iMax; i++) {
      ReceptionAchat receptionAchat = listeReceptionAchat.get(i);
      if (receptionAchat.isCharge()) {
        continue;
      }
      for (ReceptionAchat receptionAchatCharge : listeReceptionsCharge) {
        if (receptionAchat.getId().equals(receptionAchatCharge.getId())) {
          receptionAchatCharge.setIsCharge(true);
          listeReceptionAchat.set(i, receptionAchatCharge);
          break;
        }
      }
    }
  }
  
  /**
   * Afficher la plage de réceptions comprise entre deux lignes.
   * Les informations des réceptions sont chargées si cela n'a pas déjà été fait.
   */
  public void modifierPlageReceptionsAffiches(int pPremiereLigne, int pDerniereLigne) {
    // Mettre à jour la première ligne visible
    if (pPremiereLigne < 0 || listeReceptionAchat == null) {
      indexPremiereLigneAffichee = 0;
    }
    else if (pPremiereLigne >= listeReceptionAchat.size()) {
      indexPremiereLigneAffichee = listeReceptionAchat.size() - 1;
    }
    else {
      indexPremiereLigneAffichee = pPremiereLigne;
    }
    
    // Mettre à jour la dernière ligne visible
    if (pDerniereLigne < pPremiereLigne || listeReceptionAchat == null) {
      nombreLigneAffichee = 0;
    }
    else if (pDerniereLigne >= listeReceptionAchat.size()) {
      nombreLigneAffichee = listeReceptionAchat.size() - pPremiereLigne;
    }
    else {
      nombreLigneAffichee = pDerniereLigne - pPremiereLigne + 1;
    }
    
    // Corrige la ligne sélectionnée si elle est hors limite (cela sert lors des déplacements de l'ascenseur avec la souris)
    if (indexRechercheDocument < indexPremiereLigneAffichee) {
      indexRechercheDocument = indexPremiereLigneAffichee;
    }
    else if (indexRechercheDocument > indexPremiereLigneAffichee + nombreLigneAffichee - 1) {
      indexRechercheDocument = indexPremiereLigneAffichee + nombreLigneAffichee - 1;
    }
    
    // Charger les données des réceptions visibles
    chargerPlageReceptions(indexPremiereLigneAffichee, nombreLigneAffichee);
  }
  
  /**
   * Retourne la précision d'une unité donnée.
   */
  public int retournerPrecisionUniteAchat(IdUnite pId) {
    if (listeUnite == null) {
      return 0;
    }
    return listeUnite.getPrecisionUnite(pId);
  }
  
  /**
   * Retourne le nom d'une unité donnée.
   */
  public String retournerNomUniteAchat() {
    if (listeUnite == null || listeReceptionAchat == null || listeReceptionAchat.isEmpty()) {
      return "";
    }
    if (listeReceptionAchat.get(0).getIdUniteAchat() != null) {
      return listeUnite.getLibelleUnite(listeReceptionAchat.get(0).getIdUniteAchat());
    }
    else {
      return "";
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    super.quitterAvecValidation();
  }
  
  @Override
  public void quitterAvecAnnulation() {
    
    super.quitterAvecAnnulation();
  }
  
  // -- Accesseurs
  
  public ListeReceptionAchat getListeReceptions() {
    return listeReceptionAchat;
  }
  
  public Article getArticle() {
    return article;
  }
  
  public ListeMagasin getListeMagasin() {
    return listeMagasin;
  }
  
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  public void setIndexRechercheDocument(int selectedRow) {
    indexRechercheDocument = selectedRow;
  }
  
  public ListeFournisseur getListeFournisseur() {
    return listeFournisseur;
  }
  
}
