/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.articlespecial;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.math.BigDecimal;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.groupe.Groupe;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snadressefournisseur.SNAdresseFournisseur;
import ri.serien.libswing.composant.metier.vente.lot.SNBoutonLot;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelUnite;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelFond;
import ri.serien.libswing.composantrpg.lexical.RiTextArea;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Vue de l'onglet général de la boîte de dialogue de détail d'un article spécial.
 */
public class VueGeneral extends AbstractVuePanel<ModeleArticleSpecial> {
  private static final String AFFICHER_TTC = "Afficher en TTC";
  private static final String AFFICHER_HT = "Afficher en HT";
  private static final String AFFICHER_DETAIL = "Afficher détails";
  private static final String MASQUER_DETAIL = "Masquer détails";
  private static final String LIBELLE_PRIX_NET_HT = "Prix net HT";
  private static final String LIBELLE_PRIX_NET_TTC = "Prix net TTC";
  private static final String LIBELLE_PRIX_REVIENT_STANDARD_HT = "Prix revient HT";
  private static final String LIBELLE_PRIX_REVIENT_STANDARD_TTC = "Prix revient TTC";
  
  /**
   * Constructeur.
   */
  public VueGeneral(ModeleArticleSpecial pModele) {
    super(pModele);
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    
    Action nouvelleAction = new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent ae) {
        // Si aucune ligne n'a été sélectionnée le ENTER lance la recherce
        if (snAdresseFournisseur != null && snAdresseFournisseur.hasFocus()) {
          snAdresseFournisseur.charger(true, false);
        }
        else {
          getModele().quitterAvecValidation();
        }
      }
    };
    this.getInputMap(WHEN_IN_FOCUSED_WINDOW).put(SNCharteGraphique.TOUCHE_ENTREE, "enter");
    this.getActionMap().put("enter", nouvelleAction);
    
    tfLibelleArticle.setLongueurMaximumTexte(120);
    
    // Permet d'intercepter la touche TAB pour envoyé le focus sur un autre composant
    tfLibelleArticle.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    tfLibelleArticle.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    tfPrixNet.getInputMap().put(SNCharteGraphique.TOUCHE_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocus();
      }
    });
    tfPrixNet.getInputMap().put(SNCharteGraphique.TOUCHE_SHIFT_TAB, new AbstractAction() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ((Component) e.getSource()).transferFocusBackward();
      }
    });
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(AFFICHER_TTC, 'a', true);
    snBarreBouton.ajouterBouton(AFFICHER_HT, 'a', false);
    snBarreBouton.ajouterBouton(MASQUER_DETAIL, 'd', true);
    snBarreBouton.ajouterBouton(AFFICHER_DETAIL, 'd', false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    snAdresseFournisseur.setContexteSaisieDocumentAchat(true);
    snAdresseFournisseur.initialiserRecherche();
  }
  
  @Override
  public void rafraichir() {
    rafraichirCodeArticle();
    rafraichirLibelleArticle();
    rafraichirGroupe();
    rafraichirFamille();
    rafraichirSousFamille();
    rafraichirFournisseurs();
    rafraichirUvparUCV();
    rafraichirUV();
    rafraichirUCV();
    rafraichirLibelleUVparUCV();
    rafraichirQuantite();
    rafraichirUniteQuantite();
    
    rafraichirPrixNet();
    rafraichirIndiceMarge();
    rafraichirMarge();
    rafraichirPrixRevientStandard();
    
    rafraichirBoutonAfficherTTC();
    rafraichirBoutonAfficherHT();
    rafraichirBoutonAfficherDetail();
    rafraichirBoutonMasquerDetail();
    rafraichirBoutonLot();
    rafraichirPanelTarifNegocieMarge();
    
    // Positionnement du focus
    switch (getModele().getComposantAyantLeFocus()) {
      case ModeleArticleSpecial.FOCUS_LIBELLE_ARTICLE:
        tfLibelleArticle.requestFocusInWindow();
        break;
    }
  }
  
  private void rafraichirCodeArticle() {
    Article article = getModele().getArticle();
    if (article != null && article.getId() != null) {
      tfCodeArticle.setText(article.getId().getCodeArticle());
    }
    else {
      tfCodeArticle.setText("");
    }
    tfLibelleArticle.setEnabled(false);
  }
  
  private void rafraichirLibelleArticle() {
    Article article = getModele().getArticle();
    String valeur = "";
    if (article != null && article.getLibelleComplet() != null) {
      valeur = article.getLibelleComplet().trim();
    }
    if (!valeur.equals(tfLibelleArticle.getText())) {
      tfLibelleArticle.setText(valeur);
    }
    
    tfLibelleArticle.setEnabled(isDonneesChargees());
  }
  
  private void rafraichirFournisseurs() {
    snAdresseFournisseur.setEnabled(getModele().isDonneesChargees());
    snAdresseFournisseur.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snAdresseFournisseur.setIdEtablissement(getModele().getEtablissement().getId());
    }
    else {
      snAdresseFournisseur.setIdEtablissement(null);
    }
    if (getModele().getAdresseFournisseur() == null) {
      snAdresseFournisseur.initialiserRecherche();
    }
    else {
      snAdresseFournisseur.setSelectionParId(getModele().getAdresseFournisseur().getId());
    }
    snAdresseFournisseur.charger(false, false);
  }
  
  private void rafraichirGroupe() {
    cbGroupe.removeAllItems();
    if (getModele().getListeGroupe() != null) {
      for (Groupe groupe : getModele().getListeGroupe()) {
        cbGroupe.addItem(groupe);
      }
      cbGroupe.setSelectedItem(getModele().getGroupe());
    }
    cbGroupe.setEnabled(isDonneesChargees() && getModele().getAdresseFournisseur() != null);
  }
  
  private void rafraichirFamille() {
    cbFamille.removeAllItems();
    if (getModele().getListeFamille() != null) {
      for (Famille famille : getModele().getListeFamille()) {
        cbFamille.addItem(famille);
      }
      cbFamille.setSelectedItem(getModele().getFamille());
    }
    cbFamille.setEnabled(isDonneesChargees() && getModele().getGroupe() != null);
  }
  
  private void rafraichirSousFamille() {
    cbSousFamille.removeAllItems();
    if (getModele().getListeSousFamille() != null) {
      for (SousFamille sousFamille : getModele().getListeSousFamille()) {
        cbSousFamille.addItem(sousFamille);
      }
      cbSousFamille.setSelectedItem(getModele().getSousFamille());
    }
    cbSousFamille.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirUvparUCV() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null && ligneVente.getNombreUVParUCV() != null) {
      tfQuantiteUVparUCV.setText(Constantes.formater(ligneVente.getNombreUVParUCV(), false));
    }
    else {
      tfQuantiteUVparUCV.setText("1");
    }
    tfQuantiteUVparUCV.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
    if (ligneVente != null && ligneVente.isUniteVenteIdentique()) {
      lbConditionnement.setVisible(false);
      tfQuantiteUVparUCV.setVisible(false);
      lbUVparUCV.setVisible(false);
    }
    else {
      lbConditionnement.setVisible(true);
      tfQuantiteUVparUCV.setVisible(true);
      lbUVparUCV.setVisible(true);
    }
  }
  
  private void rafraichirLibelleUVparUCV() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null) {
      if (ligneVente.getIdUniteVente() != null && ligneVente.getIdUniteConditionnementVente() == null) {
        lbUVparUCV.setText(ligneVente.getIdUniteVente().getTexte());
      }
      else if (ligneVente.getIdUniteVente() != null && ligneVente.getIdUniteConditionnementVente() != null) {
        lbUVparUCV.setText(ligneVente.getIdUniteVente().getTexte() + "/" + ligneVente.getIdUniteConditionnementVente().getTexte());
      }
    }
    else {
      lbUVparUCV.setText("");
    }
  }
  
  private void rafraichirQuantite() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null) {
      if (ligneVente.getQuantiteUCV() != null) {
        BigDecimal quantiteUCV = ligneVente.getQuantiteUCV();
        tfQuantite.setText(Constantes.formater(quantiteUCV, false));
      }
      else {
        tfQuantite.setText("");
      }
    }
    tfQuantite.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirUniteQuantite() {
    LigneVente ligneVente = getModele().getLigneVente();
    if (ligneVente != null) {
      if (ligneVente.getIdUniteVente() != null && ligneVente.getIdUniteConditionnementVente() != null
          && !ligneVente.getIdUniteVente().equals(ligneVente.getIdUniteConditionnementVente())) {
        lbUnite.setText(ligneVente.getIdUniteConditionnementVente().getTexte() + " (soit "
            + Constantes.formater(ligneVente.getQuantiteUV(), false) + " " + ligneVente.getIdUniteVente().getTexte() + ")");
      }
      else if (ligneVente.getIdUniteConditionnementVente() != null) {
        lbUnite.setText(ligneVente.getIdUniteConditionnementVente().getTexte());
      }
      else if (ligneVente.getIdUniteVente() != null) {
        lbUnite.setText(ligneVente.getIdUniteVente().getTexte());
      }
      else {
        lbUnite.setText("");
      }
    }
    else {
      lbUnite.setText("");
    }
  }
  
  private void rafraichirUV() {
    cbUV.removeAllItems();
    if (getModele().getListeUnite() != null) {
      // Chargement de la combo avec la liste des unités chargées
      for (Unite unite : getModele().getListeUnite()) {
        cbUV.addItem(unite);
      }
      
      LigneVente ligne = getModele().getLigneVente();
      // Sélection de l'unité de la combo à l'aide des informations de l'article si ces dernières sont disponibles
      if (ligne != null && ligne.getIdUniteVente() != null) {
        IdUnite idUV = ligne.getIdUniteVente();
        Unite unite = getModele().getListeUnite().get(idUV);
        cbUV.setSelectedItem(unite);
      }
      else {
        cbUV.setSelectedItem(null);
      }
    }
    cbUV.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirUCV() {
    cbUCV.removeAllItems();
    if (getModele().getListeUnite() != null) {
      // Chargement de la combo avec la liste des unités chargées
      for (Unite unite : getModele().getListeUnite()) {
        cbUCV.addItem(unite);
      }
      
      LigneVente ligne = getModele().getLigneVente();
      // Sélection de l'unité de la combo à l'aide des informations de l'article si ces dernières sont disponibles
      if (ligne != null && ligne.getIdUniteConditionnementVente() != null) {
        IdUnite idUCV = ligne.getIdUniteConditionnementVente();
        Unite unite = getModele().getListeUnite().get(idUCV);
        cbUCV.setSelectedItem(unite);
      }
      else {
        cbUCV.setSelectedItem(null);
      }
    }
    cbUCV.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirPrixNet() {
    if (getModele().isAffichageTTC()) {
      lbPrixNet.setText(LIBELLE_PRIX_NET_TTC);
      if (getModele().getPrixNetTTC() != null) {
        tfPrixNet.setText(Constantes.formater(getModele().getPrixNetTTC(), true));
      }
      else {
        tfPrixNet.setText("");
      }
    }
    else {
      lbPrixNet.setText(LIBELLE_PRIX_NET_HT);
      if (getModele().getPrixNetHT() != null) {
        tfPrixNet.setText(Constantes.formater(getModele().getPrixNetHT(), true));
      }
      else {
        tfPrixNet.setText("");
      }
    }
    tfPrixNet.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirIndiceMarge() {
    if (getModele().getIndiceDeMarge() != null) {
      tfIndiceDeMarge.setText(Constantes.formater(getModele().getIndiceDeMarge(), true));
    }
    else {
      tfIndiceDeMarge.setText("");
    }
    tfIndiceDeMarge.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirMarge() {
    if (getModele().getTauxDeMarque() != null) {
      tfMarge.setText(Constantes.formater(getModele().getTauxDeMarque(), true));
    }
    else {
      tfMarge.setText("");
    }
    tfMarge.setEnabled(isDonneesChargees() && getModele().getFamille() != null);
  }
  
  private void rafraichirPrixRevientStandard() {
    // Mettre à jour le libellé
    if (getModele().isAffichageTTC()) {
      lbPrixRevientStandard.setText(LIBELLE_PRIX_REVIENT_STANDARD_TTC);
    }
    else {
      lbPrixRevientStandard.setText(LIBELLE_PRIX_REVIENT_STANDARD_HT);
    }
    
    // Mettre à jour la données en focntion du type d'affichage
    if (getModele().isAffichageTTC()) {
      if (getModele().getPrixDeRevientTTC() != null) {
        tfPrixRevientStandard.setText(Constantes.formater(getModele().getPrixDeRevientTTC(), true));
      }
      else {
        tfPrixRevientStandard.setText("");
        tfPrixRevientStandard.setEnabled(false);
      }
    }
    else {
      if (getModele().getPrixDeRevientHT() != null) {
        tfPrixRevientStandard.setText(Constantes.formater(getModele().getPrixDeRevientHT(), true));
      }
      else {
        tfPrixRevientStandard.setText("");
        tfPrixRevientStandard.setEnabled(false);
      }
    }
    
    // Désactiver le champs si le prix brut HT est différent du prix de revient standard HT (négociation saisie)
    ConditionAchat conditionAchat = getModele().getConditionAchat();
    if (conditionAchat != null && !conditionAchat.isEmpty()) {
      tfPrixRevientStandard.setEnabled(false);
    }
    // Désactiver le champs si on n'est pas en création
    else if (getModele().getMode() != EnumModeUtilisation.CREATION) {
      tfPrixRevientStandard.setEnabled(false);
    }
    // Désactiver le champs tant que la famille n'est pas renseignée
    else if (getModele().getFamille() == null) {
      tfPrixRevientStandard.setEnabled(false);
    }
    else {
      tfPrixRevientStandard.setEnabled(isDonneesChargees());
    }
  }
  
  private void rafraichirBoutonAfficherTTC() {
    boolean actif = true;
    // Le bouton "Afficher en TTC" ne doit pas être affiché si le client est facturé en HT (car le calcul de prix en Java ne calcule pas
    // les prix TTC en mode HT et que cela n'a pas vraiment de sens)
    if (getModele().getClient() == null || !getModele().getClient().isFactureEnTTC()) {
      actif = false;
    }
    else if (getModele().isAffichageTTC()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(AFFICHER_TTC, actif);
  }
  
  private void rafraichirBoutonAfficherHT() {
    boolean actif = true;
    if (!getModele().isAffichageTTC()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(AFFICHER_HT, actif);
  }
  
  private void rafraichirBoutonAfficherDetail() {
    boolean actif = true;
    if (getModele().isMargeVisible()) {
      actif = false;
    }
    
    snBarreBouton.activerBouton(AFFICHER_DETAIL, actif);
  }
  
  private void rafraichirBoutonMasquerDetail() {
    boolean actif = true;
    if (!getModele().isMargeVisible()) {
      actif = false;
    }
    snBarreBouton.activerBouton(MASQUER_DETAIL, actif);
  }
  
  private void rafraichirPanelTarifNegocieMarge() {
    boolean actif = true;
    if (!getModele().isMargeVisible()) {
      actif = false;
    }
    pnlTarifNegocieMarges.setVisible(actif);
  }
  
  /**
   * 
   * Rafraichir le bouton de saisie des lots pour les articles lotis.
   *
   */
  private void rafraichirBoutonLot() {
    if (getModele().isGestionParLot()) {
      snBoutonLot.setVisible(true);
    }
    else {
      snBoutonLot.setVisible(false);
    }
  }
  
  // Méthodes évènementielles
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        if (!snAdresseFournisseur.hasFocus()) {
          getModele().quitterAvecValidation();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(AFFICHER_TTC)) {
        getModele().modifierTypeAffichage(!getModele().isAffichageTTC());
      }
      else if (pSNBouton.isBouton(AFFICHER_HT)) {
        getModele().modifierTypeAffichage(!getModele().isAffichageTTC());
      }
      else if (pSNBouton.isBouton(AFFICHER_DETAIL)) {
        getModele().modifierMargeVisible(true);
      }
      else if (pSNBouton.isBouton(MASQUER_DETAIL)) {
        getModele().modifierMargeVisible(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfLibelleArticleFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierLibelleArticle(tfLibelleArticle.getText().trim());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snAdresseFournisseurValueChanged(SNComposantEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierAdresseFournisseur(snAdresseFournisseur.getSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbGroupeItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        if (cbGroupe.getSelectedItem() instanceof Groupe) {
          getModele().modifierGroupe((Groupe) cbGroupe.getSelectedItem());
        }
        else {
          getModele().modifierGroupe(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbFamilleItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        if (cbFamille.getSelectedItem() instanceof Famille) {
          getModele().modifierFamille((Famille) cbFamille.getSelectedItem());
        }
        else {
          getModele().modifierFamille(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbSousFamilleItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs() && e.getStateChange() == ItemEvent.SELECTED) {
        if (cbSousFamille.getSelectedItem() instanceof SousFamille) {
          getModele().modifierSousFamille((SousFamille) cbSousFamille.getSelectedItem());
        }
        else {
          getModele().modifierSousFamille(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfQuantiteFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierQuantiteUCV(tfQuantite.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfQuantiteUVparUCVFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierQuantiteUVparUCV(tfQuantiteUVparUCV.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbUCVItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED && (e.getItem() instanceof Unite)) {
        getModele().modifierUCV((Unite) cbUCV.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbUVItemStateChanged(ItemEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      if (e.getStateChange() == ItemEvent.SELECTED && (e.getItem() instanceof Unite)) {
        getModele().modifierUV((Unite) cbUV.getSelectedItem());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfPrixNetFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixNet(tfPrixNet.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfIndiceDeMargeFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierIndiceDeMarge(tfIndiceDeMarge.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfMargeFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierMarge(tfMarge.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void tfPrixRevientStandardFocusLost(FocusEvent e) {
    try {
      if (!isEvenementsActifs()) {
        return;
      }
      getModele().modifierPrixRevientStandard(tfPrixRevientStandard.getText());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snBoutonLotActionPerformed(ActionEvent e) {
    try {
      getModele().saisirLot();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
      rafraichir();
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlPrincipal = new SNPanelFond();
    pnlGeneral = new SNPanelContenu();
    lbCode = new JLabel();
    tfCodeArticle = new XRiTextField();
    lbLibelle = new JLabel();
    tfLibelleArticle = new RiTextArea();
    lbFournisseur = new JLabel();
    snAdresseFournisseur = new SNAdresseFournisseur();
    lbGroupe = new JLabel();
    cbGroupe = new SNComboBox();
    lbFamille = new JLabel();
    cbFamille = new SNComboBox();
    lbSousFamille = new JLabel();
    cbSousFamille = new SNComboBox();
    lbUniteVente = new SNLabelChamp();
    cbUV = new SNComboBox();
    lbUniteVente2 = new SNLabelChamp();
    cbUCV = new SNComboBox();
    lbConditionnement = new JLabel();
    pnlConditionnement = new SNPanel();
    tfQuantiteUVparUCV = new XRiTextField();
    lbUVparUCV = new SNLabelUnite();
    lbQuantite = new JLabel();
    pnlQuantiteUCV = new SNPanel();
    snBoutonLot = new SNBoutonLot();
    tfQuantite = new XRiTextField();
    lbUnite = new SNLabelUnite();
    pnlPrixVenteNegocie = new JPanel();
    pnlTarifNegocie = new JPanel();
    lbPrixNet = new JLabel();
    tfPrixNet = new XRiTextField();
    pnlTarifNegocieMarges = new JPanel();
    lbIndiceDeMarge = new JLabel();
    tfIndiceDeMarge = new XRiTextField();
    lbMarge = new JLabel();
    tfMarge = new XRiTextField();
    lbSymbolePourcentageMarge = new JLabel();
    lbPrixRevientStandard = new JLabel();
    tfPrixRevientStandard = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1000, 600));
    setPreferredSize(new Dimension(1000, 600));
    setOpaque(true);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new BorderLayout());
      
      // ======== pnlGeneral ========
      {
        pnlGeneral.setPreferredSize(new Dimension(1000, 600));
        pnlGeneral.setMinimumSize(new Dimension(1000, 600));
        pnlGeneral.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlGeneral.setName("pnlGeneral");
        pnlGeneral.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGeneral.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowHeights = new int[] { 0, 35, 0, 0, 0, 27, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlGeneral.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlGeneral.getLayout()).rowWeights =
            new double[] { 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbCode ----
        lbCode.setText("Code article");
        lbCode.setFont(lbCode.getFont().deriveFont(lbCode.getFont().getStyle() & ~Font.BOLD, lbCode.getFont().getSize() + 2f));
        lbCode.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbCode.setHorizontalAlignment(SwingConstants.RIGHT);
        lbCode.setMinimumSize(new Dimension(150, 30));
        lbCode.setPreferredSize(new Dimension(150, 30));
        lbCode.setMaximumSize(new Dimension(150, 30));
        lbCode.setName("lbCode");
        pnlGeneral.add(lbCode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfCodeArticle ----
        tfCodeArticle.setFont(new Font("sansserif", Font.PLAIN, 14));
        tfCodeArticle.setEditable(false);
        tfCodeArticle.setPreferredSize(new Dimension(200, 30));
        tfCodeArticle.setMinimumSize(new Dimension(200, 30));
        tfCodeArticle.setEnabled(false);
        tfCodeArticle.setRequestFocusEnabled(false);
        tfCodeArticle.setNextFocusableComponent(lbLibelle);
        tfCodeArticle.setName("tfCodeArticle");
        pnlGeneral.add(tfCodeArticle, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbLibelle ----
        lbLibelle.setText("Libell\u00e9 article");
        lbLibelle
            .setFont(lbLibelle.getFont().deriveFont(lbLibelle.getFont().getStyle() & ~Font.BOLD, lbLibelle.getFont().getSize() + 2f));
        lbLibelle.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLibelle.setHorizontalAlignment(SwingConstants.RIGHT);
        lbLibelle.setMaximumSize(new Dimension(150, 30));
        lbLibelle.setMinimumSize(new Dimension(150, 30));
        lbLibelle.setPreferredSize(new Dimension(150, 30));
        lbLibelle.setName("lbLibelle");
        pnlGeneral.add(lbLibelle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLibelleArticle ----
        tfLibelleArticle.setFont(tfLibelleArticle.getFont().deriveFont(tfLibelleArticle.getFont().getSize() + 2f));
        tfLibelleArticle.setBackground(Color.white);
        tfLibelleArticle.setLineWrap(true);
        tfLibelleArticle.setWrapStyleWord(true);
        tfLibelleArticle.setMinimumSize(new Dimension(605, 60));
        tfLibelleArticle.setPreferredSize(new Dimension(605, 60));
        tfLibelleArticle.setName("tfLibelleArticle");
        tfLibelleArticle.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            tfLibelleArticleFocusLost(e);
          }
        });
        pnlGeneral.add(tfLibelleArticle, new GridBagConstraints(1, 1, 4, 1, 1.0, 1.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbFournisseur ----
        lbFournisseur.setText("Fournisseur");
        lbFournisseur.setFont(
            lbFournisseur.getFont().deriveFont(lbFournisseur.getFont().getStyle() & ~Font.BOLD, lbFournisseur.getFont().getSize() + 2f));
        lbFournisseur.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbFournisseur.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFournisseur.setPreferredSize(new Dimension(150, 30));
        lbFournisseur.setMaximumSize(new Dimension(150, 30));
        lbFournisseur.setMinimumSize(new Dimension(150, 30));
        lbFournisseur.setName("lbFournisseur");
        pnlGeneral.add(lbFournisseur, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snAdresseFournisseur ----
        snAdresseFournisseur.setFocusCycleRoot(true);
        snAdresseFournisseur.setFocusTraversalPolicyProvider(true);
        snAdresseFournisseur.setName("snAdresseFournisseur");
        snAdresseFournisseur.addSNComposantListener(e -> snAdresseFournisseurValueChanged(e));
        pnlGeneral.add(snAdresseFournisseur, new GridBagConstraints(1, 2, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbGroupe ----
        lbGroupe.setText("Groupe");
        lbGroupe.setFont(lbGroupe.getFont().deriveFont(lbGroupe.getFont().getStyle() & ~Font.BOLD, lbGroupe.getFont().getSize() + 2f));
        lbGroupe.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbGroupe.setHorizontalAlignment(SwingConstants.RIGHT);
        lbGroupe.setMaximumSize(new Dimension(150, 30));
        lbGroupe.setMinimumSize(new Dimension(150, 30));
        lbGroupe.setPreferredSize(new Dimension(150, 30));
        lbGroupe.setName("lbGroupe");
        pnlGeneral.add(lbGroupe, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbGroupe ----
        cbGroupe.setMinimumSize(new Dimension(400, 30));
        cbGroupe.setPreferredSize(new Dimension(400, 30));
        cbGroupe.setName("cbGroupe");
        cbGroupe.addItemListener(e -> cbGroupeItemStateChanged(e));
        pnlGeneral.add(cbGroupe, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbFamille ----
        lbFamille.setText("Famille");
        lbFamille
            .setFont(lbFamille.getFont().deriveFont(lbFamille.getFont().getStyle() & ~Font.BOLD, lbFamille.getFont().getSize() + 2f));
        lbFamille.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbFamille.setHorizontalAlignment(SwingConstants.RIGHT);
        lbFamille.setMaximumSize(new Dimension(150, 30));
        lbFamille.setMinimumSize(new Dimension(150, 30));
        lbFamille.setPreferredSize(new Dimension(150, 30));
        lbFamille.setName("lbFamille");
        pnlGeneral.add(lbFamille, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbFamille ----
        cbFamille.setMinimumSize(new Dimension(400, 30));
        cbFamille.setPreferredSize(new Dimension(400, 30));
        cbFamille.setName("cbFamille");
        cbFamille.addItemListener(e -> cbFamilleItemStateChanged(e));
        pnlGeneral.add(cbFamille, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbSousFamille ----
        lbSousFamille.setText("Sous-famille");
        lbSousFamille.setFont(
            lbSousFamille.getFont().deriveFont(lbSousFamille.getFont().getStyle() & ~Font.BOLD, lbSousFamille.getFont().getSize() + 2f));
        lbSousFamille.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbSousFamille.setHorizontalAlignment(SwingConstants.RIGHT);
        lbSousFamille.setMaximumSize(new Dimension(150, 30));
        lbSousFamille.setMinimumSize(new Dimension(150, 30));
        lbSousFamille.setPreferredSize(new Dimension(150, 30));
        lbSousFamille.setName("lbSousFamille");
        pnlGeneral.add(lbSousFamille, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbSousFamille ----
        cbSousFamille.setMinimumSize(new Dimension(400, 30));
        cbSousFamille.setPreferredSize(new Dimension(400, 30));
        cbSousFamille.setName("cbSousFamille");
        cbSousFamille.addItemListener(e -> cbSousFamilleItemStateChanged(e));
        pnlGeneral.add(cbSousFamille, new GridBagConstraints(1, 5, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbUniteVente ----
        lbUniteVente.setText("Unit\u00e9 de vente (UV)");
        lbUniteVente.setName("lbUniteVente");
        pnlGeneral.add(lbUniteVente, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbUV ----
        cbUV.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbUV.setBackground(Color.white);
        cbUV.setName("cbUV");
        cbUV.addItemListener(e -> cbUVItemStateChanged(e));
        pnlGeneral.add(cbUV, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbUniteVente2 ----
        lbUniteVente2.setText("Unit\u00e9 de conditionnement (UCV)");
        lbUniteVente2.setMaximumSize(new Dimension(250, 30));
        lbUniteVente2.setMinimumSize(new Dimension(250, 30));
        lbUniteVente2.setPreferredSize(new Dimension(250, 30));
        lbUniteVente2.setName("lbUniteVente2");
        pnlGeneral.add(lbUniteVente2, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbUCV ----
        cbUCV.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbUCV.setBackground(Color.white);
        cbUCV.setName("cbUCV");
        cbUCV.addItemListener(e -> cbUCVItemStateChanged(e));
        pnlGeneral.add(cbUCV, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbConditionnement ----
        lbConditionnement.setText("Conditionnement");
        lbConditionnement.setFont(lbConditionnement.getFont().deriveFont(lbConditionnement.getFont().getStyle() & ~Font.BOLD,
            lbConditionnement.getFont().getSize() + 2f));
        lbConditionnement.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbConditionnement.setHorizontalAlignment(SwingConstants.RIGHT);
        lbConditionnement.setMaximumSize(new Dimension(150, 30));
        lbConditionnement.setMinimumSize(new Dimension(150, 30));
        lbConditionnement.setPreferredSize(new Dimension(150, 30));
        lbConditionnement.setName("lbConditionnement");
        pnlGeneral.add(lbConditionnement, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlConditionnement ========
        {
          pnlConditionnement.setName("pnlConditionnement");
          pnlConditionnement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlConditionnement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlConditionnement.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlConditionnement.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlConditionnement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- tfQuantiteUVparUCV ----
          tfQuantiteUVparUCV.setHorizontalAlignment(SwingConstants.RIGHT);
          tfQuantiteUVparUCV.setFont(tfQuantiteUVparUCV.getFont().deriveFont(tfQuantiteUVparUCV.getFont().getSize() + 3f));
          tfQuantiteUVparUCV.setMinimumSize(new Dimension(100, 30));
          tfQuantiteUVparUCV.setPreferredSize(new Dimension(100, 30));
          tfQuantiteUVparUCV.setName("tfQuantiteUVparUCV");
          tfQuantiteUVparUCV.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfQuantiteUVparUCVFocusLost(e);
            }
          });
          pnlConditionnement.add(tfQuantiteUVparUCV, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbUVparUCV ----
          lbUVparUCV.setText("UV/UCV");
          lbUVparUCV.setPreferredSize(new Dimension(150, 30));
          lbUVparUCV.setMinimumSize(new Dimension(150, 30));
          lbUVparUCV.setMaximumSize(new Dimension(150, 30));
          lbUVparUCV.setName("lbUVparUCV");
          pnlConditionnement.add(lbUVparUCV, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGeneral.add(pnlConditionnement, new GridBagConstraints(1, 8, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbQuantite ----
        lbQuantite.setText("Quantit\u00e9 en UCV");
        lbQuantite
            .setFont(lbQuantite.getFont().deriveFont(lbQuantite.getFont().getStyle() & ~Font.BOLD, lbQuantite.getFont().getSize() + 2f));
        lbQuantite.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbQuantite.setHorizontalAlignment(SwingConstants.RIGHT);
        lbQuantite.setMaximumSize(new Dimension(150, 30));
        lbQuantite.setMinimumSize(new Dimension(150, 30));
        lbQuantite.setPreferredSize(new Dimension(150, 30));
        lbQuantite.setName("lbQuantite");
        pnlGeneral.add(lbQuantite, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlQuantiteUCV ========
        {
          pnlQuantiteUCV.setName("pnlQuantiteUCV");
          pnlQuantiteUCV.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlQuantiteUCV.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlQuantiteUCV.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlQuantiteUCV.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlQuantiteUCV.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- snBoutonLot ----
          snBoutonLot.setMaximumSize(new Dimension(30, 30));
          snBoutonLot.setMinimumSize(new Dimension(30, 30));
          snBoutonLot.setPreferredSize(new Dimension(30, 30));
          snBoutonLot.setName("snBoutonLot");
          snBoutonLot.addActionListener(e -> snBoutonLotActionPerformed(e));
          pnlQuantiteUCV.add(snBoutonLot, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- tfQuantite ----
          tfQuantite.setHorizontalAlignment(SwingConstants.RIGHT);
          tfQuantite.setFont(tfQuantite.getFont().deriveFont(tfQuantite.getFont().getSize() + 3f));
          tfQuantite.setMinimumSize(new Dimension(100, 30));
          tfQuantite.setPreferredSize(new Dimension(100, 30));
          tfQuantite.setName("tfQuantite");
          tfQuantite.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfQuantiteFocusLost(e);
            }
          });
          pnlQuantiteUCV.add(tfQuantite, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGeneral.add(pnlQuantiteUCV, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbUnite ----
        lbUnite.setText("UV");
        lbUnite.setName("lbUnite");
        pnlGeneral.add(lbUnite, new GridBagConstraints(2, 9, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlPrixVenteNegocie ========
        {
          pnlPrixVenteNegocie.setOpaque(false);
          pnlPrixVenteNegocie.setName("pnlPrixVenteNegocie");
          pnlPrixVenteNegocie.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlPrixVenteNegocie.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlPrixVenteNegocie.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlPrixVenteNegocie.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlPrixVenteNegocie.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlTarifNegocie ========
          {
            pnlTarifNegocie.setBackground(new Color(222, 222, 206));
            pnlTarifNegocie.setBorder(new TitledBorder("Prix de vente"));
            pnlTarifNegocie.setOpaque(false);
            pnlTarifNegocie.setName("pnlTarifNegocie");
            pnlTarifNegocie.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifNegocie.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbPrixNet ----
            lbPrixNet.setText("Prix net HT");
            lbPrixNet
                .setFont(lbPrixNet.getFont().deriveFont(lbPrixNet.getFont().getStyle() & ~Font.BOLD, lbPrixNet.getFont().getSize() + 2f));
            lbPrixNet.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixNet.setComponentPopupMenu(null);
            lbPrixNet.setPreferredSize(new Dimension(100, 20));
            lbPrixNet.setMinimumSize(new Dimension(100, 20));
            lbPrixNet.setName("lbPrixNet");
            pnlTarifNegocie.add(lbPrixNet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- tfPrixNet ----
            tfPrixNet.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixNet.setFont(tfPrixNet.getFont().deriveFont(tfPrixNet.getFont().getSize() + 2f));
            tfPrixNet.setComponentPopupMenu(null);
            tfPrixNet.setMinimumSize(new Dimension(150, 30));
            tfPrixNet.setPreferredSize(new Dimension(150, 30));
            tfPrixNet.setName("tfPrixNet");
            tfPrixNet.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPrixNetFocusLost(e);
              }
            });
            pnlTarifNegocie.add(tfPrixNet, new GridBagConstraints(0, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPrixVenteNegocie.add(pnlTarifNegocie, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlTarifNegocieMarges ========
          {
            pnlTarifNegocieMarges.setBackground(new Color(222, 222, 206));
            pnlTarifNegocieMarges.setBorder(new TitledBorder("D\u00e9tail prix de vente"));
            pnlTarifNegocieMarges.setOpaque(false);
            pnlTarifNegocieMarges.setFocusTraversalPolicyProvider(true);
            pnlTarifNegocieMarges.setName("pnlTarifNegocieMarges");
            pnlTarifNegocieMarges.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlTarifNegocieMarges.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocieMarges.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlTarifNegocieMarges.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            ((GridBagLayout) pnlTarifNegocieMarges.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbIndiceDeMarge ----
            lbIndiceDeMarge.setText("Indice");
            lbIndiceDeMarge.setFont(lbIndiceDeMarge.getFont().deriveFont(lbIndiceDeMarge.getFont().getStyle() & ~Font.BOLD,
                lbIndiceDeMarge.getFont().getSize() + 2f));
            lbIndiceDeMarge.setHorizontalAlignment(SwingConstants.CENTER);
            lbIndiceDeMarge.setComponentPopupMenu(null);
            lbIndiceDeMarge.setName("lbIndiceDeMarge");
            pnlTarifNegocieMarges.add(lbIndiceDeMarge, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfIndiceDeMarge ----
            tfIndiceDeMarge.setHorizontalAlignment(SwingConstants.RIGHT);
            tfIndiceDeMarge.setFont(tfIndiceDeMarge.getFont().deriveFont(tfIndiceDeMarge.getFont().getSize() + 2f));
            tfIndiceDeMarge.setComponentPopupMenu(null);
            tfIndiceDeMarge.setPreferredSize(new Dimension(100, 30));
            tfIndiceDeMarge.setMinimumSize(new Dimension(100, 30));
            tfIndiceDeMarge.setName("tfIndiceDeMarge");
            tfIndiceDeMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfIndiceDeMargeFocusLost(e);
              }
            });
            pnlTarifNegocieMarges.add(tfIndiceDeMarge, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbMarge ----
            lbMarge.setText("Marge");
            lbMarge.setFont(lbMarge.getFont().deriveFont(lbMarge.getFont().getStyle() & ~Font.BOLD, lbMarge.getFont().getSize() + 2f));
            lbMarge.setHorizontalAlignment(SwingConstants.CENTER);
            lbMarge.setComponentPopupMenu(null);
            lbMarge.setName("lbMarge");
            pnlTarifNegocieMarges.add(lbMarge, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- tfMarge ----
            tfMarge.setHorizontalAlignment(SwingConstants.RIGHT);
            tfMarge.setFont(tfMarge.getFont().deriveFont(tfMarge.getFont().getSize() + 2f));
            tfMarge.setComponentPopupMenu(null);
            tfMarge.setMinimumSize(new Dimension(100, 30));
            tfMarge.setPreferredSize(new Dimension(100, 30));
            tfMarge.setName("tfMarge");
            tfMarge.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfMargeFocusLost(e);
              }
            });
            pnlTarifNegocieMarges.add(tfMarge, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbSymbolePourcentageMarge ----
            lbSymbolePourcentageMarge.setText("%");
            lbSymbolePourcentageMarge.setHorizontalAlignment(SwingConstants.LEFT);
            lbSymbolePourcentageMarge
                .setFont(lbSymbolePourcentageMarge.getFont().deriveFont(lbSymbolePourcentageMarge.getFont().getSize() + 2f));
            lbSymbolePourcentageMarge.setComponentPopupMenu(null);
            lbSymbolePourcentageMarge.setMinimumSize(new Dimension(20, 30));
            lbSymbolePourcentageMarge.setPreferredSize(new Dimension(20, 30));
            lbSymbolePourcentageMarge.setName("lbSymbolePourcentageMarge");
            pnlTarifNegocieMarges.add(lbSymbolePourcentageMarge, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- lbPrixRevientStandard ----
            lbPrixRevientStandard.setText("Prix de revient HT");
            lbPrixRevientStandard.setFont(lbPrixRevientStandard.getFont()
                .deriveFont(lbPrixRevientStandard.getFont().getStyle() & ~Font.BOLD, lbPrixRevientStandard.getFont().getSize() + 2f));
            lbPrixRevientStandard.setHorizontalAlignment(SwingConstants.CENTER);
            lbPrixRevientStandard.setComponentPopupMenu(null);
            lbPrixRevientStandard.setName("lbPrixRevientStandard");
            pnlTarifNegocieMarges.add(lbPrixRevientStandard, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- tfPrixRevientStandard ----
            tfPrixRevientStandard.setHorizontalAlignment(SwingConstants.RIGHT);
            tfPrixRevientStandard.setFont(tfPrixRevientStandard.getFont().deriveFont(tfPrixRevientStandard.getFont().getSize() + 2f));
            tfPrixRevientStandard.setComponentPopupMenu(null);
            tfPrixRevientStandard.setPreferredSize(new Dimension(150, 30));
            tfPrixRevientStandard.setMinimumSize(new Dimension(150, 30));
            tfPrixRevientStandard.setEnabled(false);
            tfPrixRevientStandard.setName("tfPrixRevientStandard");
            tfPrixRevientStandard.addFocusListener(new FocusAdapter() {
              @Override
              public void focusLost(FocusEvent e) {
                tfPrixRevientStandardFocusLost(e);
              }
            });
            pnlTarifNegocieMarges.add(tfPrixRevientStandard, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlPrixVenteNegocie.add(pnlTarifNegocieMarges, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGeneral.add(pnlPrixVenteNegocie, new GridBagConstraints(1, 10, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlGeneral, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlPrincipal.add(snBarreBouton, BorderLayout.SOUTH);
    }
    add(pnlPrincipal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelFond pnlPrincipal;
  private SNPanelContenu pnlGeneral;
  private JLabel lbCode;
  private XRiTextField tfCodeArticle;
  private JLabel lbLibelle;
  private RiTextArea tfLibelleArticle;
  private JLabel lbFournisseur;
  private SNAdresseFournisseur snAdresseFournisseur;
  private JLabel lbGroupe;
  private SNComboBox cbGroupe;
  private JLabel lbFamille;
  private SNComboBox cbFamille;
  private JLabel lbSousFamille;
  private SNComboBox cbSousFamille;
  private SNLabelChamp lbUniteVente;
  private SNComboBox cbUV;
  private SNLabelChamp lbUniteVente2;
  private SNComboBox cbUCV;
  private JLabel lbConditionnement;
  private SNPanel pnlConditionnement;
  private XRiTextField tfQuantiteUVparUCV;
  private SNLabelUnite lbUVparUCV;
  private JLabel lbQuantite;
  private SNPanel pnlQuantiteUCV;
  private SNBoutonLot snBoutonLot;
  private XRiTextField tfQuantite;
  private SNLabelUnite lbUnite;
  private JPanel pnlPrixVenteNegocie;
  private JPanel pnlTarifNegocie;
  private JLabel lbPrixNet;
  private XRiTextField tfPrixNet;
  private JPanel pnlTarifNegocieMarges;
  private JLabel lbIndiceDeMarge;
  private XRiTextField tfIndiceDeMarge;
  private JLabel lbMarge;
  private XRiTextField tfMarge;
  private JLabel lbSymbolePourcentageMarge;
  private JLabel lbPrixRevientStandard;
  private XRiTextField tfPrixRevientStandard;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
