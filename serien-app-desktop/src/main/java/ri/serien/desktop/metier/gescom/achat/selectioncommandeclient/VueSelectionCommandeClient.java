/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.achat.selectioncommandeclient;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.WindowConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractVueDialogue;

public class VueSelectionCommandeClient extends AbstractVueDialogue<ModeleSelectionCommandeClient> {
  // Constantes
  private static final String[] TITRE_LISTE = new String[] { "<html>Document<br>de vente</html>", "Raison sociale",
      "R\u00e9f\u00e9rence commande vente", "<html>Livraison<br>pr\u00e9vue</html>", "<html>Enlèvement<br>Livraison</html>" };
  private static final String BOUTON_AFFICHER_SANS_FILTRE = "Afficher tous les documents";
  
  // Variables
  
  // Classe interne qui permet de gérer les flèches dans la table tblListe
  class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;
    
    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      validerSelectionLigne(e, actionOriginale);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueSelectionCommandeClient(ModeleSelectionCommandeClient pModele) {
    super(pModele);
  }
  
  // -- Méthodes publiques
  
  /**
   * Construit l'écran.
   */
  @Override
  public void initialiserComposants() {
    initComponents();
    setResizable(false);
    setTitle(getModele().getTitreEcran());
    
    tblListe.personnaliserAspect(TITRE_LISTE, new int[] { 80, 220, 300, 90, 100 }, new int[] { 80, 220, -1, 90, 100 },
        new int[] { NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE, NRiTable.CENTRE, NRiTable.CENTRE }, 14);
    // On double la taille de la hauteur de l'entête à cause du titre "Livraison prévue"
    int largeur = tblListe.getTableHeader().getPreferredSize().width;
    int hauteur = tblListe.getTableHeader().getPreferredSize().height;
    tblListe.getTableHeader().setPreferredSize(new Dimension(largeur, hauteur));
    tblListe.setSelectAllForEdit(true);
    // Permet de forcer la saisie d'une cellule si l'on clique sur un autre composant (le bouton valider par exemple)
    tblListe.forcerSaisieCellule(true);
    // On configure le multi-selection ou simple séléction
    if (getModele().isMultiSelection()) {
      tblListe.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }
    else {
      tblListe.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
    }
    
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListe, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListe.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListe.getActionMap().put(actionEnter, actionCelluleEnter);
    
    // Ajouter un listener sur les changement de sélection dans le tableau résultat
    tblListe.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
      @Override
      public void valueChanged(ListSelectionEvent e) {
        tblListeSelectionChanged(e);
      }
    });
    
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListe.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeStateChanged(e);
      }
    });
    viewportLigneAchat.setBackground(Color.WHITE);
    
    // Raccourcis clavier
    
    // Configurer la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_AFFICHER_SANS_FILTRE, 't', true);
    snBarreBouton.ajouterBouton(EnumBouton.ANNULER, true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Rafraichit l'écran.
   */
  @Override
  public void rafraichir() {
    rafraichirMessageErreur();
    rafraichirListe();
    rafraichirBoutonSansFiltre();
    rafraichirBoutonValider();
  }
  
  // -- Méthodes privées
  
  private void rafraichirMessageErreur() {
    Message message = getModele().getMessage();
    if (message == null) {
      lbMessageErreur.setVisible(false);
    }
    else {
      lbMessageErreur.setMessage(message);
      lbMessageErreur.setVisible(true);
    }
  }
  
  private void rafraichirListe() {
    ArrayList<LigneAAfficher> liste = getModele().getListeAAfficher();
    Object[][] donnees = null;
    
    if (liste != null) {
      donnees = new Object[liste.size()][TITRE_LISTE.length];
      for (int ligne = 0; ligne < liste.size(); ligne++) {
        if (liste.get(ligne).getIdDocumentVente() == null || liste.get(ligne).getDocumentVente() == null) {
          donnees[ligne][0] = "";
          donnees[ligne][1] = "";
          donnees[ligne][2] = "";
          donnees[ligne][3] = "";
          donnees[ligne][4] = "";
        }
        else {
          donnees[ligne][0] = liste.get(ligne).getIdDocumentVente().getTexte();
          if (liste.get(ligne).getDocumentVente().getAdresseFacturation() != null) {
            donnees[ligne][1] = liste.get(ligne).getDocumentVente().getAdresseFacturation().getNom();
          }
          else {
            donnees[ligne][1] = "";
          }
          donnees[ligne][2] = liste.get(ligne).getDocumentVente().getReferenceLongue();
          // La date de livraison prévue uniquement si le document est en mode livraison
          if (liste.get(ligne).getDocumentVente().isLivraison() && liste.get(ligne).getDocumentVente().getTransport() != null) {
            donnees[ligne][3] =
                Constantes.convertirDateEnTexte(liste.get(ligne).getDocumentVente().getTransport().getDateLivraisonPrevue());
          }
          else {
            donnees[ligne][3] = "";
          }
          // Le mode enlèvement / livraison
          if (liste.get(ligne).getDocumentVente().isEnlevement()) {
            donnees[ligne][4] = "Enl\u00e8vement";
          }
          else {
            donnees[ligne][4] = "Livraison";
          }
        }
      }
    }
    tblListe.mettreAJourDonnees(donnees);
    
    // Remettre la ligne sélectionné avant le rafraichissement
    ArrayList<Integer> listeIndex = getModele().getListeIndiceLigneSelectionnee();
    if (listeIndex != null) {
      for (Integer index : listeIndex) {
        if (index > -1 && index < tblListe.getRowCount()) {
          tblListe.getSelectionModel().addSelectionInterval(index, index);
        }
      }
    }
  }
  
  /**
   * Permet de rafraichir le bouton qui recherche sans filtre
   */
  private void rafraichirBoutonSansFiltre() {
    boolean actif = getModele().isFiltrageEnCours();
    snBarreBouton.activerBouton(BOUTON_AFFICHER_SANS_FILTRE, actif);
  }
  
  /**
   * Permet de rafraichir le bouton qui recherche sans filtre
   */
  private void rafraichirBoutonValider() {
    boolean actif = getModele().isVerifierSelectionDocument();
    snBarreBouton.activerBouton(EnumBouton.VALIDER, actif);
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans le tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale) {
    try {
      // Informer le modèle qu'une valeur a été saisie pour un article
      getModele().quitterAvecValidation();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  // -- Méthodes protégées
  
  // -- Méthodes évènementielles
  
  /**
   * La ligne sélectionnée a changée dans le tableau.
   */
  private void tblListeSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      // Informer le modèle de la sélection des lignes
      getModele().modifierListeIndiceTableau(tblListe.getListeIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_AFFICHER_SANS_FILTRE)) {
        getModele().modifierListeIndiceTableau(null);
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        getModele().quitterAvecValidation();
      }
      else if (pSNBouton.isBouton(EnumBouton.ANNULER)) {
        getModele().quitterAvecAnnulation();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Listener appelé lorsque les lignes affichées dans le résultat de la recherche changent.
   */
  private void scpListeStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListe == null) {
        return;
      }
      Trace.debug(VueSelectionCommandeClient.class, "scpListeStateChanged", "");
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListe.getViewport().getViewRect();
      int premiereLigne = tblListe.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListe.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListe.getRowCount() - 1;
      }
      
      // Charges les lignes d'achat concernées si besoin
      getModele().modifierPlageArticlesAffichees(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeMouseClicked(MouseEvent e) {
    try {
      // Envoyer au modèle la liste des indices des lignes sélectionnées
      getModele().modifierListeIndiceTableau(tblListe.getListeIndiceSelection());
      // On affiche le détail de la ligne double cliquée
      if (e.getClickCount() == 2) {
        getModele().quitterAvecValidation();
        return;
      }
      rafraichirBoutonSansFiltre();
      rafraichirBoutonValider();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlCouleurFond = new JPanel();
    pnlPrincipal = new SNPanelContenu();
    lbMessageErreur = new SNLabelTitre();
    scpListe = new JScrollPane();
    tblListe = new NRiTable();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(900, 400));
    setForeground(Color.black);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    setModal(true);
    setBackground(new Color(238, 238, 210));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== pnlCouleurFond ========
    {
      pnlCouleurFond.setBackground(new Color(238, 238, 210));
      pnlCouleurFond.setName("pnlCouleurFond");
      pnlCouleurFond.setLayout(new BorderLayout());
      
      // ======== pnlPrincipal ========
      {
        pnlPrincipal.setName("pnlPrincipal");
        pnlPrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWidths = new int[] { 1139, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowHeights = new int[] { 0, 297, 0 };
        ((GridBagLayout) pnlPrincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlPrincipal.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        
        // ---- lbMessageErreur ----
        lbMessageErreur.setName("lbMessageErreur");
        pnlPrincipal.add(lbMessageErreur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== scpListe ========
        {
          scpListe.setPreferredSize(new Dimension(800, 400));
          scpListe.setName("scpListe");
          
          // ---- tblListe ----
          tblListe.setShowVerticalLines(true);
          tblListe.setShowHorizontalLines(true);
          tblListe.setBackground(Color.white);
          tblListe.setRowHeight(20);
          tblListe.setSelectionBackground(new Color(57, 105, 138));
          tblListe.setFont(new Font("sansserif", Font.PLAIN, 14));
          tblListe.setGridColor(new Color(204, 204, 204));
          tblListe.setOpaque(false);
          tblListe.setName("tblListe");
          tblListe.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              tblListeMouseClicked(e);
            }
          });
          scpListe.setViewportView(tblListe);
        }
        pnlPrincipal.add(scpListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlCouleurFond.add(pnlPrincipal, BorderLayout.CENTER);
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlCouleurFond.add(snBarreBouton, BorderLayout.SOUTH);
    }
    contentPane.add(pnlCouleurFond, BorderLayout.CENTER);
    
    pack();
    setLocationRelativeTo(getOwner());
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlCouleurFond;
  private SNPanelContenu pnlPrincipal;
  private SNLabelTitre lbMessageErreur;
  private JScrollPane scpListe;
  private NRiTable tblListe;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
