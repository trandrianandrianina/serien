/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.boncour;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JViewport;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.table.DefaultTableModel;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.EnumEtatBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.saisie.SNIdentifiant;
import ri.serien.libswing.composantrpg.autonome.liste.NRiTable;
import ri.serien.libswing.moteur.SNCharteGraphique;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.mvc.panel.AbstractVuePanel;

/**
 * Ecran recherche/liste des bons de cour
 */
public class VueBonCour extends AbstractVuePanel<ModeleBonCour> {
  private static final String BOUTON_ANNULER_BON_COUR = "Annuler bon de cour";
  private static final String BOUTON_REACTIVER_BON_COUR = "Réactiver bon de cour";
  private static final String[] TITRE_LISTE_BON_COUR =
      new String[] { "Num\u00e9ro bon", "Carnet", "Magasinier", "Document", "Date doc.", "Client", "Etat" };
  private DefaultTableModel tableModelBonCour = null;
  private JTableCellRendererBonCour listeBonCourRenderer = new JTableCellRendererBonCour();
  
  // Classe interne qui permet de gérer les flèches dans la table tblListe
  private class ActionCellule extends AbstractAction {
    // Variables
    private Action actionOriginale = null;
    
    public ActionCellule(String pAction, Action pActionOriginale) {
      super(pAction);
      actionOriginale = pActionOriginale;
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
      validerSelectionLigne(e, actionOriginale);
    }
  }
  
  /**
   * Constructeur.
   */
  public VueBonCour(ModeleBonCour pModele) {
    super(pModele);
    
  }
  
  @Override
  public void initialiserComposants() {
    initComponents();
    this.setBackground(SNCharteGraphique.COULEUR_FOND);
    this.setOpaque(true);
    
    tfNumeroCarnet.setLongueur(IdCarnetBonCour.LONGUEUR_NUMERO);
    tfNumeroBonCour.setLongueur(IdBonCour.LONGUEUR_NUMERO);
    
    // Configurer la barre de titre
    pnlBpresentation.setCapitaliserPremiereLettre(false);
    pnlBpresentation.setCouleurFoncee(SNCharteGraphique.COULEUR_BARRE_TITRE_GESCOM);
    
    // La liste
    scpListeBonCour.getViewport().setBackground(Color.WHITE);
    tblListeBonCour.personnaliserAspect(TITRE_LISTE_BON_COUR, new int[] { 100, 100, 250, 100, 90, 180, 100 },
        new int[] { 100, 100, 350, 100, 90, 1800, 100 }, new int[] { NRiTable.DROITE, NRiTable.DROITE, NRiTable.GAUCHE, NRiTable.DROITE,
            NRiTable.CENTRE, NRiTable.GAUCHE, NRiTable.GAUCHE },
        14);
    tblListeBonCour.personnaliserAspectCellules(listeBonCourRenderer);
    // Modification de la gestion des évènements sur la touche ENTER de la jtable
    final Action originalActionEnter = retournerActionComposant(tblListeBonCour, SNCharteGraphique.TOUCHE_ENTREE);
    String actionEnter = "actionEnter";
    ActionCellule actionCelluleEnter = new ActionCellule(actionEnter, originalActionEnter);
    tblListeBonCour.getInputMap(JTable.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(SNCharteGraphique.TOUCHE_ENTREE, actionEnter);
    tblListeBonCour.getActionMap().put(actionEnter, actionCelluleEnter);
    // Ajouter un listener sur les changements d'affichage des lignes de la table
    JViewport viewportLigneAchat = scpListeBonCour.getViewport();
    viewportLigneAchat.addChangeListener(new ChangeListener() {
      @Override
      public void stateChanged(ChangeEvent e) {
        scpListeBonCourStateChanged(e);
      }
    });
    
    // Renseigner les types de documents de vente
    cbEtatBonCour.removeAllItems();
    cbEtatBonCour.addItem("Tous");
    cbEtatBonCour.addItem(EnumEtatBonCour.NON_UTILISE);
    cbEtatBonCour.addItem(EnumEtatBonCour.MANQUANT);
    cbEtatBonCour.addItem(EnumEtatBonCour.UTILISE);
    cbEtatBonCour.addItem(EnumEtatBonCour.ANNULE);
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonBonCour(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons principale
    snBarreBouton.ajouterBouton(BOUTON_ANNULER_BON_COUR, 'a', false);
    snBarreBouton.ajouterBouton(BOUTON_REACTIVER_BON_COUR, 'r', false);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBoutonBonCour(pSNBouton);
      }
    });
    
    // requestFocus();
  }
  
  @Override
  public void rafraichir() {
    rafraichirEtablissement();
    rafraichirMagasin();
    rafraichirNumeroCarnet();
    rafraichirNumeroBonCour();
    rafraichirMagasinier();
    rafraichirEtatBonCour();
    rafraichierTitre();
    rafraichirListeBonCour();
    rafraichirBoutonAnnuler();
    rafraichirBoutonReactiver();
    rafraichirBoutonConsulter();
  }
  
  /**
   * Affichage du filtre établissement
   */
  private void rafraichirEtablissement() {
    snEtablissement.setSession(getModele().getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelection(getModele().getEtablissement());
  }
  
  /**
   * Affichage du filtre magasin
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snMagasin.setIdEtablissement(getModele().getEtablissement().getId());
      snMagasin.charger(false);
      snMagasin.setIdSelection(getModele().getIdMagasin());
    }
  }
  
  /**
   * Affichage du filtre numéro de carnet
   */
  private void rafraichirNumeroCarnet() {
    int numero = getModele().getNumeroCarnet();
    
    if (numero > 0) {
      tfNumeroCarnet.setText(Constantes.convertirIntegerEnTexte(numero, 0));
    }
    else {
      tfNumeroCarnet.setText("");
    }
  }
  
  /**
   * Affichage du filtre numéro de bon de cour
   */
  private void rafraichirNumeroBonCour() {
    int numero = getModele().getNumeroBonCour();
    
    if (numero > 0) {
      tfNumeroBonCour.setText(Constantes.convertirIntegerEnTexte(numero, 0));
    }
    else {
      tfNumeroBonCour.setText("");
    }
  }
  
  /**
   * Affichage du filtre magasinier
   */
  private void rafraichirMagasinier() {
    snVendeur.setSession(getModele().getSession());
    if (getModele().getEtablissement() != null) {
      snVendeur.setIdEtablissement(getModele().getEtablissement().getId());
      
      snVendeur.charger(false);
      snVendeur.setTousAutorise(true);
      
      snVendeur.setSelection(getModele().getMagasinier());
    }
  }
  
  /**
   * Affichage du filtre état du bon de cour
   */
  private void rafraichirEtatBonCour() {
    EnumEtatBonCour etat = getModele().getEtatBonCour();
    
    if (etat != null) {
      cbEtatBonCour.setSelectedItem(etat);
    }
    else {
      cbEtatBonCour.setSelectedIndex(0);
    }
  }
  
  /**
   * Affichage du message titre de la liste
   */
  private void rafraichierTitre() {
    Message message = getModele().getTitreResultat();
    lblTitreResultat.setMessage(message);
  }
  
  /**
   * Charge les données dans la liste.
   */
  private void rafraichirListeBonCour() {
    ListeBonCour listeBonCour = getModele().getListeBonCour();
    String[][] donnees = null;
    
    if (listeBonCour != null) {
      donnees = new String[listeBonCour.size()][TITRE_LISTE_BON_COUR.length];
      for (int i = 0; i < listeBonCour.size(); i++) {
        BonCour bonCour = listeBonCour.get(i);
        String[] ligne = donnees[i];
        
        DocumentVente document = null;
        if (bonCour.getComplement() != null) {
          document = (DocumentVente) bonCour.getComplement();
        }
        
        // Numéro du bon de cour
        ligne[0] = Constantes.convertirIntegerEnTexte(bonCour.getId().getNumero(), 0);
        
        // Numéro de carnet
        ligne[1] = Constantes.convertirIntegerEnTexte(bonCour.getId().getIdCarnetBonCour().getNumero(), 0);
        
        // Magasinier
        ligne[2] = getModele().retournerNomMagasinier(bonCour);
        
        // Document
        if (document != null) {
          if (bonCour.getIdDocumentGenere().isIdFacture()) {
            ligne[3] = Constantes.convertirIntegerEnTexte(bonCour.getIdDocumentGenere().getNumeroFacture(), 0);
          }
          else {
            ligne[3] = bonCour.getIdDocumentGenere().getTexte();
          }
        }
        
        // Date document
        if (document != null) {
          ligne[4] = Constantes.convertirDateEnTexte(document.getDateCreation());
        }
        
        // Nom du client
        if (document != null && getModele().getListeClientBase() != null) {
          ligne[5] = getModele().getListeClientBase().getClientBaseParId(document.getIdClientFacture()).toString();
        }
        
        // Etat du bon de cour
        if (bonCour.getEtatBonCour() != null) {
          ligne[6] = bonCour.getEtatBonCour().getLibelle();
        }
      }
    }
    
    tblListeBonCour.mettreAJourDonnees(donnees);
    
    // Sélectionner la ligne en cours
    int index = getModele().getIndiceSelection();
    if (index > -1 && index < tblListeBonCour.getRowCount()) {
      tblListeBonCour.getSelectionModel().setSelectionInterval(index, index);
    }
  }
  
  private void rafraichirBoutonConsulter() {
    boolean actif = true;
    // Désactiver le bouton si aucune ligne n'est sélectionnée
    if (tblListeBonCour.getSelectedRowCount() <= 0) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, actif);
  }
  
  private void rafraichirBoutonAnnuler() {
    boolean actif = true;
    // Désactiver le bouton si aucune ligne n'est sélectionnée
    if (tblListeBonCour.getSelectedRowCount() <= 0) {
      actif = false;
    }
    // Désactiver le bouton si le bon de cour sélectionné n'est pas un manquant
    else if (!getModele().getListeBonCour().get(tblListeBonCour.getSelectedRow()).isAnnulable()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_ANNULER_BON_COUR, actif);
  }
  
  private void rafraichirBoutonReactiver() {
    boolean actif = true;
    // Désactiver le bouton si aucune ligne n'est sélectionnée
    if (tblListeBonCour.getSelectedRowCount() <= 0) {
      actif = false;
    }
    // Désactiver le bouton si le bon de cour sélectionné n'est pas un annulé
    else if (!getModele().getListeBonCour().get(tblListeBonCour.getSelectedRow()).isReactivable()) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_REACTIVER_BON_COUR, actif);
  }
  
  /**
   * Récupérer le bon de cour sélectionné
   */
  private void validerSelectionBonCour() {
    int indexvisuel = tblListeBonCour.getSelectedRow();
    
    if (indexvisuel >= 0) {
      int indexreel = indexvisuel;
      if (tblListeBonCour.getRowSorter() != null) {
        indexreel = tblListeBonCour.getRowSorter().convertRowIndexToModel(indexvisuel);
      }
      getModele().modifierBonCourSelectionne(indexreel);
      getModele().afficherBonCour();
    }
  }
  
  /**
   * Retourne l'action associée à une touche d'un composant.
   */
  private Action retournerActionComposant(JComponent component, KeyStroke keyStroke) {
    Object actionKey = getKeyForActionMap(component, keyStroke);
    if (actionKey == null) {
      return null;
    }
    return component.getActionMap().get(actionKey);
  }
  
  /**
   * Rechercher les 3 InputMaps pour trouver the KeyStroke.
   */
  private Object getKeyForActionMap(JComponent component, KeyStroke keyStroke) {
    for (int i = 0; i < 3; i++) {
      InputMap inputMap = component.getInputMap(i);
      if (inputMap != null) {
        Object key = inputMap.get(keyStroke);
        if (key != null) {
          return key;
        }
      }
    }
    return null;
  }
  
  private void scpListeBonCourStateChanged(ChangeEvent e) {
    try {
      if (!isEvenementsActifs() || tblListeBonCour == null) {
        return;
      }
      
      // Déterminer les index des premières et dernières lignes
      Rectangle rectangleVisible = scpListeBonCour.getViewport().getViewRect();
      int premiereLigne = tblListeBonCour.rowAtPoint(new Point(0, rectangleVisible.y));
      int derniereLigne = tblListeBonCour.rowAtPoint(new Point(0, rectangleVisible.y + rectangleVisible.height - 1));
      if (derniereLigne == -1) {
        // Cas d'un affichage blanc sous la dernière ligne
        derniereLigne = tblListeBonCour.getRowCount() - 1;
      }
      
      // Charges les articles concernés si besoin
      getModele().modifierPlageBonCourAffiche(premiereLigne, derniereLigne);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btTraiterClicBoutonBonCour(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        validerSelectionBonCour();
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        getModele().quitterAvecAnnulation();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        getModele().rechercher();
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        getModele().initialiserRecherche();
        rafraichirListeBonCour();
      }
      else if (pSNBouton.isBouton(BOUTON_ANNULER_BON_COUR)) {
        getModele().annulerBonCour(tblListeBonCour.getSelectedRow());
      }
      else if (pSNBouton.isBouton(BOUTON_REACTIVER_BON_COUR)) {
        getModele().reactiverBonCour(tblListeBonCour.getSelectedRow());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeBonCourSelectionChanged(ListSelectionEvent e) {
    try {
      // La ligne sélectionnée change lorsque l'écran est rafraîchi mais il faut ignorer ses changements
      if (!isEvenementsActifs()) {
        return;
      }
      
      rafraichirBoutonAnnuler();
      rafraichirBoutonConsulter();
      rafraichirBoutonReactiver();
      
      // Informer le modèle
      getModele().modifierIndiceSelection(tblListeBonCour.getIndiceSelection());
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierEtablissement(snEtablissement.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbMagasinValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMagasin(snMagasin.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroCarnetFocusLost(FocusEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierNumeroCarnet(tfNumeroCarnet.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snVendeurValueChanged(SNComposantEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierMagasinier(snVendeur.getSelection());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroBonCourFocusLost(FocusEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierNumeroBonCour(tfNumeroBonCour.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbEtatBonCourItemStateChanged(ItemEvent e) {
    try {
      if (isEvenementsActifs()) {
        if (cbEtatBonCour.getSelectedItem() instanceof EnumEtatBonCour) {
          getModele().modifierEtatBonCour((EnumEtatBonCour) cbEtatBonCour.getSelectedItem());
        }
        else {
          getModele().modifierEtatBonCour(null);
        }
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tblListeBonCourMouseClicked(MouseEvent e) {
    try {
      rafraichirBoutonAnnuler();
      rafraichirBoutonConsulter();
      rafraichirBoutonReactiver();
      
      // Traitement du double clic
      if (e.getClickCount() == 2) {
        validerSelectionBonCour();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroBonCourKeyReleased(KeyEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierNumeroBonCour(tfNumeroBonCour.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfNumeroCarnetKeyReleased(KeyEvent e) {
    try {
      if (isEvenementsActifs()) {
        getModele().modifierNumeroCarnet(tfNumeroCarnet.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Traiter l'appui sur la touche entrée dans la tableau.
   */
  private void validerSelectionLigne(ActionEvent e, Action actionOriginale) {
    try {
      validerSelectionBonCour();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlFiltre = new JPanel();
    pnlFiltre1 = new JPanel();
    lbNumeroCarnet = new SNLabelChamp();
    tfNumeroCarnet = new SNIdentifiant();
    lbMagasinier = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbNumeroBonCour = new SNLabelChamp();
    tfNumeroBonCour = new SNIdentifiant();
    lbEtatBonCour = new SNLabelChamp();
    cbEtatBonCour = new SNComboBox();
    pnlFiltre2 = new JPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    snBarreRecherche = new SNBarreRecherche();
    lblTitreResultat = new SNLabelTitre();
    scpListeBonCour = new JScrollPane();
    tblListeBonCour = new NRiTable();
    
    // ======== this ========
    setMinimumSize(new Dimension(1200, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- pnlBpresentation ----
    pnlBpresentation.setText("Bons de cour");
    pnlBpresentation.setName("pnlBpresentation");
    add(pnlBpresentation, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlFiltre ========
      {
        pnlFiltre.setOpaque(false);
        pnlFiltre.setBorder(new TitledBorder(""));
        pnlFiltre.setName("pnlFiltre");
        pnlFiltre.setLayout(new GridLayout(1, 2, 5, 5));
        
        // ======== pnlFiltre1 ========
        {
          pnlFiltre1.setOpaque(false);
          pnlFiltre1.setName("pnlFiltre1");
          pnlFiltre1.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltre1.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlFiltre1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltre1.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltre1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNumeroCarnet ----
          lbNumeroCarnet.setText("Num\u00e9ro de carnet");
          lbNumeroCarnet.setName("lbNumeroCarnet");
          pnlFiltre1.add(lbNumeroCarnet, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroCarnet ----
          tfNumeroCarnet.setToolTipText("Rechercher par num\u00e9ro de carnet de bons de cour");
          tfNumeroCarnet.setHorizontalAlignment(SwingConstants.TRAILING);
          tfNumeroCarnet.setName("tfNumeroCarnet");
          tfNumeroCarnet.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroCarnetFocusLost(e);
            }
          });
          tfNumeroCarnet.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfNumeroCarnetKeyReleased(e);
            }
          });
          pnlFiltre1.add(tfNumeroCarnet, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbMagasinier ----
          lbMagasinier.setText("Magasinier");
          lbMagasinier.setName("lbMagasinier");
          pnlFiltre1.add(lbMagasinier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snVendeur ----
          snVendeur.setToolTipText("Filtrer par magasinier");
          snVendeur.setName("snVendeur");
          snVendeur.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snVendeurValueChanged(e);
            }
          });
          pnlFiltre1.add(snVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbNumeroBonCour ----
          lbNumeroBonCour.setText("Num\u00e9ro de bon de cour");
          lbNumeroBonCour.setMinimumSize(new Dimension(200, 30));
          lbNumeroBonCour.setPreferredSize(new Dimension(200, 30));
          lbNumeroBonCour.setMaximumSize(new Dimension(200, 30));
          lbNumeroBonCour.setName("lbNumeroBonCour");
          pnlFiltre1.add(lbNumeroBonCour, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- tfNumeroBonCour ----
          tfNumeroBonCour.setHorizontalAlignment(SwingConstants.TRAILING);
          tfNumeroBonCour.setToolTipText("Rechercher par num\u00e9ro de bon de cour");
          tfNumeroBonCour.setName("tfNumeroBonCour");
          tfNumeroBonCour.addFocusListener(new FocusAdapter() {
            @Override
            public void focusLost(FocusEvent e) {
              tfNumeroBonCourFocusLost(e);
            }
          });
          tfNumeroBonCour.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
              tfNumeroBonCourKeyReleased(e);
            }
          });
          pnlFiltre1.add(tfNumeroBonCour, new GridBagConstraints(1, 2, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtatBonCour ----
          lbEtatBonCour.setText("Etat de bon de cour");
          lbEtatBonCour.setName("lbEtatBonCour");
          pnlFiltre1.add(lbEtatBonCour, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- cbEtatBonCour ----
          cbEtatBonCour.setBackground(new Color(171, 148, 79));
          cbEtatBonCour.setFont(cbEtatBonCour.getFont().deriveFont(14f));
          cbEtatBonCour.setPreferredSize(new Dimension(150, 30));
          cbEtatBonCour.setMinimumSize(new Dimension(150, 30));
          cbEtatBonCour.setToolTipText("Filtrer par \u00e9tat de bons de cour");
          cbEtatBonCour.setName("cbEtatBonCour");
          cbEtatBonCour.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
              cbEtatBonCourItemStateChanged(e);
            }
          });
          pnlFiltre1.add(cbEtatBonCour, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlFiltre.add(pnlFiltre1);
        
        // ======== pnlFiltre2 ========
        {
          pnlFiltre2.setOpaque(false);
          pnlFiltre2.setName("pnlFiltre2");
          pnlFiltre2.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlFiltre2.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltre2.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlFiltre2.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlFiltre2.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement");
          lbEtablissement.setName("lbEtablissement");
          pnlFiltre2.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlFiltre2.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbMagasin ----
          lbMagasin.setText("Magasin");
          lbMagasin.setName("lbMagasin");
          pnlFiltre2.add(lbMagasin, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snMagasin ----
          snMagasin.setName("snMagasin");
          snMagasin.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              cbMagasinValueChanged(e);
            }
          });
          pnlFiltre2.add(snMagasin, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          pnlFiltre2.add(snBarreRecherche, new GridBagConstraints(0, 2, 2, 1, 1.0, 1.0, GridBagConstraints.SOUTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlFiltre.add(pnlFiltre2);
      }
      pnlContenu.add(pnlFiltre,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lblTitreResultat ----
      lblTitreResultat.setText("Bons de cour correspondant \u00e0 votre recherche");
      lblTitreResultat.setName("lblTitreResultat");
      pnlContenu.add(lblTitreResultat,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== scpListeBonCour ========
      {
        scpListeBonCour.setPreferredSize(new Dimension(1050, 424));
        scpListeBonCour.setFont(new Font("sansserif", Font.PLAIN, 14));
        scpListeBonCour.setName("scpListeBonCour");
        
        // ---- tblListeBonCour ----
        tblListeBonCour.setShowVerticalLines(true);
        tblListeBonCour.setShowHorizontalLines(true);
        tblListeBonCour.setBackground(Color.white);
        tblListeBonCour.setRowHeight(20);
        tblListeBonCour.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tblListeBonCour.setAutoCreateRowSorter(true);
        tblListeBonCour.setSelectionBackground(new Color(57, 105, 138));
        tblListeBonCour.setGridColor(new Color(204, 204, 204));
        tblListeBonCour.setName("tblListeBonCour");
        tblListeBonCour.addMouseListener(new MouseAdapter() {
          @Override
          public void mouseClicked(MouseEvent e) {
            tblListeBonCourMouseClicked(e);
          }
        });
        scpListeBonCour.setViewportView(tblListeBonCour);
      }
      pnlContenu.add(scpListeBonCour,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre pnlBpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private JPanel pnlFiltre;
  private JPanel pnlFiltre1;
  private SNLabelChamp lbNumeroCarnet;
  private SNIdentifiant tfNumeroCarnet;
  private SNLabelChamp lbMagasinier;
  private SNVendeur snVendeur;
  private SNLabelChamp lbNumeroBonCour;
  private SNIdentifiant tfNumeroBonCour;
  private SNLabelChamp lbEtatBonCour;
  private SNComboBox cbEtatBonCour;
  private JPanel pnlFiltre2;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNBarreRecherche snBarreRecherche;
  private SNLabelTitre lblTitreResultat;
  private JScrollPane scpListeBonCour;
  private NRiTable tblListeBonCour;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
