/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.retourarticle;

import java.awt.Component;

import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableRetourArticleBooleanRenderer extends DefaultTableCellRenderer {
  // Variable
  private String[][] donnees = null;
  
  // -- Méthodes publiques
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setResizable(false);
    cm.getColumn(0).setMinWidth(50);
    cm.getColumn(0).setMaxWidth(50);
    
    // Pour les cellules de type checkbox
    if (donnees != null && row < donnees.length) {
      JCheckBox c = new JCheckBox();
      c.setHorizontalAlignment(JCheckBox.CENTER);
      c.setOpaque(false);
      boolean selection = isSelected || value.equals("TRUE") || donnees[row][column].equals("TRUE");
      c.setSelected(selection);
      if (value.equals("DESABLE")) {
        c.setSelected(false);
        c.setEnabled(false);
      }
      return c;
    }
    else {
      JLabel c = new JLabel("");
      return c;
    }
  }
  
  // -- Accesseurs
  
  public void setDonnees(String[][] donnees) {
    this.donnees = donnees;
  }
  
}
