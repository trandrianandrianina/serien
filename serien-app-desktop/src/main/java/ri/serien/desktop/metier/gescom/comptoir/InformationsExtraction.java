/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir;

import java.math.BigDecimal;
import java.util.HashMap;

import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.EnumOrigineDocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;

public class InformationsExtraction {
  // Variables
  // Il s'agit du document sur lequel on va effectuer l'extraction
  private DocumentVente documentVenteSource = null;
  // Il s'agit de la version d'origine du document source
  private DocumentVente documentVenteSourceOriginal = null;
  private EnumOrigineDocumentVente typeExtraction = EnumOrigineDocumentVente.NOUVEAU;
  private HashMap<Integer, BigDecimal> listeLignesExtraites = new HashMap<Integer, BigDecimal>();
  
  // -- Méthodes publiques
  
  /**
   * Ajoute une ligne article extraite.
   */
  public void effacerVariables() {
    documentVenteSource = null;
    documentVenteSourceOriginal = null;
    typeExtraction = EnumOrigineDocumentVente.NOUVEAU;
    listeLignesExtraites.clear();
  }
  
  /**
   * Ajoute une ligne article extraite.
   */
  public void ajouterLigne(IdLigneVente pIdLigneVente, BigDecimal pQuantiteOrigine) {
    listeLignesExtraites.put(pIdLigneVente.getNumeroLigne(), pQuantiteOrigine);
  }
  
  /**
   * Retourner la quantité d'origine pour une ligne article donné.
   */
  public BigDecimal retournerQuantiteOrigine(int pNumeroLigne) {
    if (!listeLignesExtraites.containsKey(pNumeroLigne)) {
      return BigDecimal.ZERO;
    }
    return listeLignesExtraites.get(pNumeroLigne);
  }
  
  // -- Accesseurs
  
  public DocumentVente getDocumentVenteSource() {
    return documentVenteSource;
  }
  
  public void setDocumentVenteSource(DocumentVente pDocumentVenteSource) {
    this.documentVenteSource = pDocumentVenteSource;
  }
  
  public DocumentVente getDocumentVenteSourceOriginal() {
    return documentVenteSourceOriginal;
  }
  
  public void setDocumentVenteSourceOriginal(DocumentVente documentVenteSourceOriginal) {
    this.documentVenteSourceOriginal = documentVenteSourceOriginal;
  }
  
  public EnumOrigineDocumentVente getTypeExtraction() {
    return typeExtraction;
  }
  
  public void setTypeExtraction(EnumOrigineDocumentVente pTypeExtraction) {
    this.typeExtraction = pTypeExtraction;
  }
  
}
