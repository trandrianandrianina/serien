/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.articlespecial;

import java.math.BigDecimal;
import java.math.MathContext;

import ri.serien.desktop.metier.gescom.comptoir.ModeleComptoir;
import ri.serien.libcommun.commun.EnumModeUtilisation;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.exploitation.securite.EnumDroitSecurite;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.EnumTypeArticle;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.conditionachat.EnumModeSaisiePort;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.personnalisation.famille.Famille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.groupe.Groupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.SousFamille;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.EnumTypeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.prixvente.ArrondiPrix;
import ri.serien.libcommun.gescom.vente.prixvente.OutilCalculPrix;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenanceColonneTarif;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixBase;
import ri.serien.libcommun.gescom.vente.prixvente.parametrelignevente.EnumProvenancePrixNet;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libcommun.rmi.ManagerServiceArticle;
import ri.serien.libcommun.rmi.ManagerServiceDocumentVente;
import ri.serien.libswing.composant.dialoguestandard.information.DialogueInformation;
import ri.serien.libswing.composant.metier.vente.lot.ModeleLot;
import ri.serien.libswing.composant.metier.vente.lot.VueLot;
import ri.serien.libswing.moteur.interpreteur.SessionBase;
import ri.serien.libswing.moteur.mvc.dialogue.AbstractModeleDialogue;

/**
 * Modéle de de la boîte de dialogue du détail d'un article spécial.
 */
public class ModeleArticleSpecial extends AbstractModeleDialogue {
  public static final int FOCUS_NON_INDIQUE = 0;
  public static final int FOCUS_LIBELLE_ARTICLE = 1;
  public static final int FOCUS_PRIX_ACHAT_BRUT = 2;
  
  private ModeleComptoir modeleParent = null;
  private EnumModeUtilisation modeUtilisation = EnumModeUtilisation.CREATION;
  private int composantAyantLeFocus = FOCUS_LIBELLE_ARTICLE;
  
  private UtilisateurGescom utilisateur = null;
  private DocumentVente documentVente = null;
  private Client client = null;
  private LigneVente ligneVente = null;
  
  private Etablissement etablissement = null;
  private Article article = null;
  private ConditionAchat conditionAchat;
  private ListeUnite listeUnite = null;
  private AdresseFournisseur adressefournisseur = null;
  
  private ListeGroupe listeGroupe = null;
  private Groupe groupe = null;
  private ListeFamille listeFamille = null;
  private ListeFamille listeFamillePourGroupe = null;
  private Famille famille = null;
  private ListeSousFamille listeSousFamille = null;
  private ListeSousFamille listeSousFamillePourFamille = null;
  private SousFamille sousFamille = null;
  
  private Boolean affichageTTC = null;
  private Boolean margeVisible = true;
  private int nombreDecimalesUCV = 0;
  
  private int arrondiPrix = Constantes.DEUX_DECIMALES;
  private BigDecimal indiceDeMarge = null;
  private BigDecimal tauxDeMarque = null;
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et factories
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur.
   */
  public ModeleArticleSpecial(SessionBase pSession, ModeleComptoir pModeleParent, UtilisateurGescom pUtilisateur,
      DocumentVente pDocumentVente, LigneVente pLigneVente, Client pClient) {
    super(pSession);
    modeleParent = pModeleParent;
    utilisateur = pUtilisateur;
    documentVente = pDocumentVente;
    client = pClient;
    ligneVente = pLigneVente;
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes abstract à implémenter
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  @Override
  public void initialiserDonnees() {
    // Contrôler la présence des données obligatoires
    if (documentVente == null) {
      throw new MessageErreurException("Le document de ventes est invalide.");
    }
    if (client == null) {
      throw new MessageErreurException("Le client est invalide.");
    }
    
    // Définir le mode dans lequel on se trouve (création ou modification)
    if (ligneVente == null) {
      modeUtilisation = EnumModeUtilisation.CREATION;
    }
    else {
      modeUtilisation = EnumModeUtilisation.MODIFICATION;
    }
    
    affichageTTC = client.isFactureEnTTC();
    
    // Afficher l'onglet général par défaut
    setCleVueEnfantActive(EnumCleVueArticleSpecial.ONGLET_GENERAL);
  }
  
  @Override
  public void chargerDonnees() {
    // Charger l'établissement
    if (etablissement == null) {
      etablissement = Etablissement.charger(getIdSession(), documentVente.getId().getIdEtablissement());
    }
    
    // Charger la liste des unités
    if (listeUnite == null) {
      listeUnite = new ListeUnite();
      listeUnite = listeUnite.charger(getIdSession());
    }
    
    // Charger la liste des groupes
    if (listeGroupe == null) {
      listeGroupe = new ListeGroupe();
      listeGroupe = listeGroupe.charger(getIdSession(), getEtablissement().getId());
    }
    
    // Charger la liste des familles
    if (listeFamille == null) {
      listeFamille = new ListeFamille();
      listeFamille = listeFamille.charger(getIdSession(), getEtablissement().getId());
    }
    
    // Charger la liste des sous-familles
    if (listeSousFamille == null) {
      listeSousFamille = new ListeSousFamille();
      listeSousFamille = listeSousFamille.charger(getIdSession(), getEtablissement().getId());
    }
    
    // Renseigner les données en fonction du mode
    switch (modeUtilisation) {
      case CREATION:
        creerArticleSpecial();
        break;
      
      case MODIFICATION:
        modifierArticleSpecial();
        break;
      
      case ANNULATION:
      case CONSULTATION:
      case DUPLICATION:
      case SUPPRESSION:
      default:
        break;
    }
    
    // Compléter la ligne de vente
    if (ligneVente != null) {
      ligneVente.setAutoriserPrixNetInferieurPrixRevient(ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
          EnumDroitSecurite.IS_SUPERVISEUR_GESTION_DES_BONS_ET_FACTURES));
    }
    
    // Renseigner le groupe
    if (article != null && article.getIdGroupe() != null) {
      groupe = listeGroupe.get(article.getIdGroupe());
      listeFamillePourGroupe = listeFamille.getListeFamillePourGroupe(groupe.getId());
    }
    else {
      groupe = null;
      listeFamillePourGroupe = null;
    }
    
    // Renseigner la famille
    if (article != null && article.getIdFamille() != null) {
      famille = listeFamille.get(article.getIdFamille());
      listeSousFamillePourFamille = listeSousFamille.getListeSousFamillePourFamille(famille.getId());
    }
    else {
      famille = null;
      listeSousFamillePourFamille = null;
    }
    
    // Renseigner la sous-famille
    if (article != null && article.getIdSousFamille() != null) {
      sousFamille = listeSousFamille.get(article.getIdSousFamille());
    }
    else {
      sousFamille = null;
    }
    
    // Renseigner le fournisseur
    if (getLigneVente() != null && ligneVente.getIdAdresseFournisseur() != null) {
      setAdresseFournisseur(new AdresseFournisseur(ligneVente.getIdAdresseFournisseur()));
    }
    else {
      setAdresseFournisseur(null);
    }
    
    // Renseigner l'unité de conditionnement de vente de la ligne de vente
    if (ligneVente.getIdUniteConditionnementVente() == null) {
      if (getListeUnite().retournerIdUniteParDefaut() != null) {
        ligneVente.setIdUniteConditionnementVente(getListeUnite().retournerIdUniteParDefaut());
      }
      else {
        ligneVente.setIdUniteConditionnementVente(getListeUnite().get(0).getId());
      }
    }
    
    // Renseigner l'unité de vente de la ligne de vente
    if (ligneVente.getIdUniteVente() == null) {
      if (getListeUnite().retournerIdUniteParDefaut() != null) {
        ligneVente.setIdUniteVente(getListeUnite().retournerIdUniteParDefaut());
      }
      else {
        ligneVente.setIdUniteVente(getListeUnite().get(0).getId());
      }
    }
  }
  
  @Override
  public void quitterAvecValidation() {
    controlerSaisie();
    sauverArticleSpecial();
    sauverLigneVente();
    super.quitterAvecValidation();
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes privées
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Créer un article spécial.
   */
  private void creerArticleSpecial() {
    // Créer un nouvel article
    article = new Article(IdArticle.getInstanceAvecCreationId(etablissement.getId()));
    article.setTypeArticle(EnumTypeArticle.SPECIAL);
    article.setIdUV(listeUnite.retournerIdUniteParDefaut());
    if (article.getIdUV() == null) {
      throw new MessageErreurException("L'unite de l'article est invalide.");
    }
    
    // Créer la ligne de vente
    IdLigneVente idLigneVente = IdLigneVente.getInstance(documentVente.getId().getIdEtablissement(),
        documentVente.getId().getCodeEntete(), documentVente.getId().getNumero(), documentVente.getId().getSuffixe(), 10);
    ligneVente = new LigneVente(idLigneVente);
    if (client != null) {
      ligneVente.setClientTTC(client.isFactureEnTTC());
    }
    ligneVente.setNombreDecoupe(0);
    ligneVente.setNombreDecimaleUV(listeUnite.getPrecisionUnite(article.getIdUV()));
    ligneVente.setTypeArticle(EnumTypeArticle.SPECIAL);
    ligneVente.setTypeLigne(EnumTypeLigneVente.ARTICLE);
    
    // Le prix de vente
    ligneVente.setQuantiteUV(BigDecimal.ONE);
    ligneVente.setProvenanceColonneTarif(EnumProvenanceColonneTarif.SAISIE_LIGNE_VENTE);
    ligneVente.setNumeroColonneTarif(1);
    ligneVente.setPrixBaseHT(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixBaseTTC(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setProvenancePrixBase(EnumProvenancePrixBase.SAISIE_LIGNE_VENTE);
    ligneVente.setPrixNetSaisiHT(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixNetSaisiTTC(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    ligneVente.setPrixNetCalculeHT(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixNetCalculeTTC(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixDeRevientLigneHT(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixDeRevientLigneTTC(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixDeRevientStandardHT(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setPrixDeRevientStandardTTC(BigDecimal.ZERO.setScale(arrondiPrix));
    ligneVente.setProvenancePrixDeRevient(LigneVente.ORIGINE_SAISI);
    indiceDeMarge = BigDecimal.ZERO.setScale(arrondiPrix);
    tauxDeMarque = BigDecimal.ZERO.setScale(arrondiPrix);
    
    // Créer la condition d'achats
    conditionAchat = new ConditionAchat();
  }
  
  /**
   * Modifier un article spécial.
   */
  private void modifierArticleSpecial() {
    // Renseigner l'article
    article = new Article(ligneVente.getIdArticle());
    article = ManagerServiceArticle.completerArticleStandard(getIdSession(), article);
    article.setIdUV(ligneVente.getIdUniteVente());
    
    if (ligneVente.getIdAdresseFournisseur() != null) {
      adressefournisseur = new AdresseFournisseur(ligneVente.getIdAdresseFournisseur());
    }
    
    // Charger la condition d'achats
    conditionAchat = ManagerServiceDocumentVente.chargerConditionAchat(getIdSession(), getLigneVente());
    if (conditionAchat != null && conditionAchat.getCoefficientRemise().compareTo(BigDecimal.ZERO) == 0) {
      conditionAchat.setCoefficientRemise(BigDecimal.ONE);
    }
    
    // Calculer l'indice de marge et le taux de marque
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
  }
  
  /**
   * Controle la validité des informations saisies.
   */
  private void controlerSaisie() {
    StringBuilder message = new StringBuilder();
    
    // Message d'erreur
    if (article.getLibelleComplet().trim().isEmpty()) {
      message.append("\n\t- un libellé.");
    }
    if (article.getIdFamille() == null) {
      message.append("\n\t- une famille.");
    }
    if (article.getIdSousFamille() == null && isSousFamilleExiste()) {
      message.append("\n\t- une sous-famille.");
    }
    if (article.getIdFournisseur() == null) {
      message.append("\n\t- un fournisseur.");
    }
    if (ligneVente.getQuantiteUCV().compareTo(BigDecimal.ZERO) == 0) {
      message.append("\n\t- une quantité.");
    }
    if (ligneVente.getPrixDeRevientLigneHT().compareTo(ligneVente.getPrixNetHT()) > 0) {
      message.append("\n\t- un nouveau prix de vente (car il vous est interdit de vendre en dessous du prix de revient).");
    }
    // S'il y a un message d'erreur à afficher
    if (message.length() > 0) {
      message.insert(0, "Afin de pouvoir valider votre saisie, merci de renseigner :");
      throw new MessageErreurException(message.toString());
    }
    
    // Message d'alerte
    boolean afficherAlerte = false;
    if (ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) == 0) {
      afficherAlerte = true;
    }
    if (ligneVente.getPrixNetHT().compareTo(BigDecimal.ZERO) == 0) {
      afficherAlerte = true;
    }
    // S'il y a un message d'alerte à afficher
    if (afficherAlerte) {
      message.setLength(0);
      message
          .append("Vous n'avez pas saisie de prix pour cet article.\nVous pourrez les renseigner utltérieurement dans la fiche article");
      DialogueInformation.afficher(message.toString());
    }
  }
  
  /**
   * Sauver l'article spécial.
   */
  private void sauverArticleSpecial() {
    // Contrôler le prix net HT
    BigDecimal prixNetHT = null;
    if (ligneVente.getPrixNetHT() != null) {
      prixNetHT = ligneVente.getPrixNetHT();
    }
    
    // Tester si l'article existe déjà
    if (!article.getId().isExistant()) {
      article = ManagerServiceArticle.sauverArticleSpecial(getIdSession(), article, client, prixNetHT, conditionAchat);
    }
    else {
      ManagerServiceArticle.modifierArticleSpecial(getIdSession(), article, client, prixNetHT, conditionAchat);
    }
  }
  
  /**
   * Sauver la ligne de vente.
   */
  private void sauverLigneVente() {
    if (ligneVente == null) {
      throw new MessageErreurException("Impossible de modifier l'article spcécial car le document de ventes est invalide.");
    }
    
    // Calculer les montants de la ligne
    BigDecimal montant = BigDecimal.ZERO.setScale(arrondiPrix);
    if (affichageTTC) {
      montant = ligneVente.getQuantiteUV().multiply(ligneVente.getPrixNetTTC());
      ligneVente.setMontantTTC(montant);
      ligneVente.setMontantHT(OutilCalculPrix.calculerPrixHTAvecPrixTTC(montant, ligneVente.getPourcentageTVA(), arrondiPrix));
    }
    else {
      montant = ligneVente.getQuantiteUV().multiply(ligneVente.getPrixNetHT());
      ligneVente.setMontantHT(montant);
      ligneVente.setMontantTTC(OutilCalculPrix.calculerPrixTTC(montant, ligneVente.getPourcentageTVA(), arrondiPrix));
    }
    // Mise à jour des informations ayant pu être modifié
    ligneVente.setIdArticle(article.getId());
    ligneVente.setLibelle(article.getLibelleComplet());
    ligneVente.setIdUniteConditionnementVente(article.getIdUCV());
    ligneVente.setIdUniteVente(article.getIdUV());
    ligneVente.setIdAdresseFournisseur(adressefournisseur.getId());
    ligneVente.setNombreDecimaleUV(article.getNombreDecimaleUV());
    ligneVente.setNombreUVParUCV(article.getNombreUVParUCV());
    
    switch (modeUtilisation) {
      case CREATION:
        // L'insertion de la ligne est effectuée dans le ModeleComptoir car ici il manque la ligne d'insertion (entre autres)
        // Il y a peut être une amélioration à faire
        break;
      
      case MODIFICATION:
        if (ligneVente.getId().getNumero() == 0) {
          throw new MessageErreurException("Impossible de modifier l'article spécial car l'identifiantd de la ligne est invalide.");
        }
        AlertesLigneDocument alerteLigne =
            ManagerServiceDocumentVente.controlerLigneDocumentVente(getIdSession(), ligneVente, documentVente.getDateCreation());
        if (!alerteLigne.getMessages().trim().isEmpty()) {
          throw new MessageErreurException(alerteLigne.getMessages());
        }
        ManagerServiceDocumentVente.modifierLignedocumentVente(getIdSession(), ligneVente);
        break;
      
      case ANNULATION:
      case CONSULTATION:
      case DUPLICATION:
      case SUPPRESSION:
      default:
        break;
    }
  }
  
  /**
   * Modification de l'article suivant la famille choisie.
   */
  private void modifierArticleSuivantFamille() {
    if (getFamille() == null) {
      return;
    }
    // Récupération de l'unité de la famille
    IdUnite idUnite = getFamille().getIdUniteVente();
    // Si la famille n'a pas d'unité alors l'unité par défaut est U
    if (idUnite == null && getListeUnite() != null) {
      idUnite = getListeUnite().retournerIdUniteParDefaut();
    }
    article.setIdUV(idUnite);
    article.setCodeTVA(getFamille().getCodeTVA());
    ligneVente.setPourcentageTVA(getEtablissement().getTauxTVA(article.getCodeTVA()));
    
    // Mettre à jour le prix de revient s'il n'est pas renseigné
    calculerPrixRevientStandard();
  }
  
  /**
   * Calculer le prix de revient à partir du prix net et du coefficient de la famille.
   * Cela ne modifie pas le prix de revient s'il est déjà renseigné.
   */
  private void calculerPrixRevientStandard() {
    // Récupérer le coefficient de conversion du prix net vers le PRV
    if (getFamille() == null) {
      return;
    }
    // Initialise le coefficient de prix de revient de la famille avec la valeur 1, car il arrive que ce coefficient ne soit pas
    // initialisé dans la personnalisation des ventes (code FA). On ne fait pas le calcul avec un coefficient valant 0.
    BigDecimal coefficientPRVFamille = BigDecimal.ONE;
    if (getFamille().getCoefficientPRV() != null && getFamille().getCoefficientPRV().compareTo(BigDecimal.ZERO) != 0) {
      coefficientPRVFamille = getFamille().getCoefficientPRV();
    }
    
    if (affichageTTC) {
      // Ne pas modifier le prix de revient s'il est déjà renseigné
      if (ligneVente.getPrixDeRevientLigneTTC() != null && ligneVente.getPrixDeRevientLigneTTC().compareTo(BigDecimal.ZERO) > 0) {
        return;
      }
      // Cpnversion du prix TTC en HT si le prix HT n'est pas initialisé
      if (ligneVente.getPrixNetSaisiTTC() != null && ligneVente.getPrixNetSaisiHT() == null) {
        ligneVente.setPrixNetSaisiHT(
            ArrondiPrix.appliquer(ligneVente.getPrixNetSaisiTTC().divide(coefficientPRVFamille, MathContext.DECIMAL32), arrondiPrix));
      }
    }
    else {
      // Ne pas modifier le prix de revient s'il est déjà renseigné
      if (ligneVente.getPrixDeRevientLigneHT() != null && ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) > 0) {
        return;
      }
    }
    
    // Calculer le prix de revient à partir du prix net et du coefficient de la famille
    // Le prix de revient est calculé avec les prix HT car il est calculé à partir d'un coefficent, lors des conversions TTC vers HT
    // il peut arriver des différences de 1 centime c'est pour cette raison que le PRV est ici toujours calculé en HT
    if (ligneVente.getPrixNetSaisiHT() != null) {
      BigDecimal prixRevientHT =
          ArrondiPrix.appliquer(ligneVente.getPrixNetSaisiHT().divide(coefficientPRVFamille, MathContext.DECIMAL32), arrondiPrix);
      ligneVente.setPrixDeRevientLigneHT(prixRevientHT);
      ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(prixRevientHT, ligneVente.getPourcentageTVA(), arrondiPrix));
      // Initialisation du prix standard s'il ne l'est pas
      if (ligneVente.getPrixDeRevientStandardHT() == null || ligneVente.getPrixDeRevientStandardHT().compareTo(BigDecimal.ZERO) == 0) {
        ligneVente.setPrixDeRevientStandardHT(prixRevientHT);
        ligneVente.setPrixDeRevientStandardTTC(ligneVente.getPrixDeRevientLigneTTC());
      }
    }
    
    // Modifier la condition d'achats
    ConditionAchat conditionAchat = getConditionAchat();
    if (conditionAchat != null && conditionAchat.isEmpty()) {
      getConditionAchat().setPrixAchatBrutHT(ligneVente.getPrixDeRevientLigneHT());
    }
  }
  
  /**
   * Modifier le prix de revient de la ligne de ventes.
   */
  private void modifierPrixRevientLigne() {
    if (ligneVente == null) {
      return;
    }
    BigDecimal prixRevient = getConditionAchat().getPrixRevientStandardHT();
    if (prixRevient == null) {
      return;
    }
    ligneVente.setPrixDeRevientLigneHT(prixRevient);
    ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(prixRevient, ligneVente.getPourcentageTVA(), arrondiPrix));
    // Mise à jour de l'indice de marge et du taux de marque
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
  }
  
  /**
   * Modifier le fournisseur.
   * Méthode privée car elle ne doit être utilisée que dans le modèle. La vue doit utiliser modifierAdresseFournisseur().
   */
  private void setAdresseFournisseur(AdresseFournisseur pAdresseFournisseur) {
    adressefournisseur = pAdresseFournisseur;
    if (adressefournisseur != null) {
      article.setIdFournisseur(adressefournisseur.getId().getIdFournisseur());
      if (ligneVente != null) {
        ligneVente.setIdAdresseFournisseur(pAdresseFournisseur.getId());
      }
    }
    else {
      article.setIdFournisseur(null);
      if (ligneVente != null) {
        ligneVente.setIdAdresseFournisseur(null);
      }
    }
  }
  
  /**
   * Modifier la famille de l'article spécial.
   * Méthode privée car elle ne doit être utilisée que dans le modèle. La vue doit utiliser modifierFamille().
   */
  private void setFamille(Famille pFamille) {
    famille = pFamille;
    if (famille != null) {
      article.setIdFamille(famille.getId());
    }
    else {
      article.setIdFamille(null);
    }
  }
  
  /**
   * Modifier la sous-famille de l'article spécial.
   * Méthode privée car elle ne doit être utilisée que dans le modèle. La vue doit utiliser modifierSousFamille().
   */
  private void setSousFamille(SousFamille pSousFamille) {
    sousFamille = pSousFamille;
    if (sousFamille != null) {
      article.setIdSousFamille(sousFamille.getId());
    }
    else {
      article.setIdSousFamille(null);
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes publiques
  // --------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Changer l'onglet actif.
   */
  public void changerOnglet(int pIndiceOnglet) {
    EnumCleVueArticleSpecial enumCleVueArticleSpecial = EnumCleVueArticleSpecial.valueOfByCode(pIndiceOnglet);
    if (enumCleVueArticleSpecial != null) {
      setCleVueEnfantActive(enumCleVueArticleSpecial);
    }
    rafraichir();
  }
  
  /**
   * Modifier le libellé de l'article.
   * @param pLibelle Nouveau libellé de l'article.
   */
  public void modifierLibelleArticle(String pLibelle) {
    // Tester si la valeur a changé
    String libelle = Constantes.normerTexte(pLibelle);
    if (libelle.equals(article.getLibelleComplet())) {
      return;
    }
    
    // Modifier l'article
    article.setLibelleComplet(libelle);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le fournisseur.
   */
  public void modifierAdresseFournisseur(AdresseFournisseur pAdresseFournisseur) {
    // Tester si la valeur a changé
    if (Constantes.equals(pAdresseFournisseur, adressefournisseur)) {
      return;
    }
    
    // Modifier l'adresse fournisseur
    setAdresseFournisseur(pAdresseFournisseur);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le groupe de l'article.
   */
  public void modifierGroupe(Groupe pGroupe) {
    // Tester si la valeur a changé
    if (Constantes.equals(pGroupe, getGroupe())) {
      return;
    }
    
    // Modifier le groupe
    setGroupe(pGroupe);
    
    // Mettre à jour la liste des familles disponibles
    if (getGroupe() != null) {
      listeFamillePourGroupe = listeFamille.getListeFamillePourGroupe(getGroupe().getId());
    }
    else {
      listeFamillePourGroupe = null;
    }
    
    // Sélectionner la famille s'il n'y en a qu'une
    if (getGroupe() != null && listeFamillePourGroupe != null && listeFamillePourGroupe.size() == 1) {
      setFamille(listeFamillePourGroupe.get(0));
    }
    else {
      setFamille(null);
    }
    
    // Mettre à jour la liste des sous-familles disponibles
    if (getFamille() != null) {
      listeSousFamillePourFamille = listeSousFamille.getListeSousFamillePourFamille(getFamille().getId());
    }
    else {
      listeSousFamillePourFamille = null;
    }
    
    // Sélectionner la sous-famille s'il n'y en a qu'une
    if (getFamille() != null && listeSousFamillePourFamille != null && listeSousFamillePourFamille.size() == 1) {
      setSousFamille(listeSousFamillePourFamille.get(0));
    }
    else {
      setSousFamille(null);
    }
    
    // Actualiser l'article
    modifierArticleSuivantFamille();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la famille de l'article.
   */
  public void modifierFamille(Famille pFamille) {
    // Tester si la valeur a changé
    if (Constantes.equals(pFamille, getFamille())) {
      return;
    }
    
    // Modifier la famille
    setFamille(pFamille);
    
    // Mettre à jour la liste des sous-familles disponibles
    if (getFamille() != null) {
      listeSousFamillePourFamille = listeSousFamille.getListeSousFamillePourFamille(getFamille().getId());
    }
    else {
      listeSousFamillePourFamille = null;
    }
    
    // Sélectionner la sous-famille s'il n'y en a qu'une
    if (getFamille() != null && listeSousFamillePourFamille != null && listeSousFamillePourFamille.size() == 1) {
      setSousFamille(listeSousFamillePourFamille.get(0));
    }
    else {
      setSousFamille(null);
    }
    
    // Actualiser l'article
    modifierArticleSuivantFamille();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la sous-famille de l'article
   */
  public void modifierSousFamille(SousFamille pSousFamille) {
    // Tester si la valeur a changé
    if (Constantes.equals(pSousFamille, getSousFamille())) {
      return;
    }
    setSousFamille(pSousFamille);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la quantité UV par UCV.
   */
  public void modifierQuantiteUVparUCV(String pSaisie) {
    // Tester si la valeur a changé
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (ligneVente.getNombreUVParUCV() != null && ligneVente.getNombreUVParUCV().compareTo(quantite) == 0) {
      return;
    }
    
    // Contrôler la quantité saisie
    if (quantite.compareTo(BigDecimal.ZERO) <= 0) {
      quantite = BigDecimal.ONE;
    }
    
    // Modifier le rapport entre UV et UCV dans la ligne de vente sans modifier la quantité en UCV de la ligne de vente
    // La méthode la plus simple pour ne pas refaire les calculs situés dans LigneVente est de réaffecter la quantité en UCV
    // initiale après le changement de coefficient
    BigDecimal quantiteUCV = ligneVente.getQuantiteUCV();
    ligneVente.setNombreUVParUCV(quantite);
    ligneVente.setQuantiteUCV(quantiteUCV);
    
    // Modifier le rapport entre UV et UCV dans l'article
    article.setNombreUVParUCV(quantite);
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifier la quantité en UCV.
   */
  public void modifierQuantiteUCV(String pSaisie) {
    // Tester si la valeur a changé
    BigDecimal quantite = Constantes.convertirTexteEnBigDecimal(pSaisie);
    if (ligneVente.getQuantiteUCV().compareTo(quantite) == 0) {
      return;
    }
    
    // Contrôler la quantité saisie
    if (quantite.compareTo(BigDecimal.ZERO) <= 0) {
      rafraichir();
      throw new MessageErreurException("La quantité doit être supérieure à 0");
    }
    
    // Modifier la quantité en UCV dans la ligne de vente
    ligneVente.setQuantiteUCV(quantite);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier l'unité de conditionnement de ventes de l'article.
   */
  public void modifierUCV(Unite pUnite) {
    // Récupérer l'identifiant de l'unité
    IdUnite idUnite = null;
    if (pUnite != null) {
      idUnite = pUnite.getId();
    }
    
    // Tester si la valeur a changé
    if (Constantes.equals(article.getIdUCV(), idUnite)) {
      return;
    }
    
    // Mettre à jour l'unité de conditionnement de ventes de l'article
    article.setIdUCV(idUnite);
    ligneVente.setIdUniteConditionnementVente(idUnite);
    
    if (article.getIdUCV() == null || article.getIdUCV().equals(article.getIdUV()) || article.isUniteConditionnementVenteNonScindable()) {
      nombreDecimalesUCV = getListeUnite().getPrecisionUnite(idUnite);
    }
    else {
      nombreDecimalesUCV = Article.NOMBRE_DECIMALE_UCV_SCINDABLE;
    }
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier l'unité de ventes de l'article.
   */
  public void modifierUV(Unite pUnite) {
    // Récupérer l'identifiant de l'unité
    IdUnite idUnite = null;
    if (pUnite != null) {
      idUnite = pUnite.getId();
    }
    
    // Tester si la valeur a changé
    if (Constantes.equals(ligneVente.getIdUniteVente(), idUnite)) {
      return;
    }
    
    // Si l'UV == l'UCV, on modifie aussi l'UCV pour garder cette égalité. Il faut modifier explicitement l'UCV si on veut deux unités
    // différentes.
    if (Constantes.equals(ligneVente.getIdUniteConditionnementVente(), ligneVente.getIdUniteVente())) {
      modifierUCV(pUnite);
    }
    
    // Mettre à jour l'unité de ventes de l'article (UV et UCV sont identiques pour un article spécial)
    article.setIdUV(idUnite);
    ligneVente.setIdUniteVente(idUnite);
    
    // Mettre à jour la précision de la ligne de ventes
    if (pUnite != null) {
      ligneVente.setNombreDecimaleUV(pUnite.getNombreDecimale());
    }
    else {
      ligneVente.setNombreDecimaleUV(0);
    }
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le prix net TTC ou HT.
   */
  public void modifierPrixNet(String pSaisie) {
    BigDecimal prixNetSaisi = Constantes.convertirTexteEnBigDecimal(pSaisie);
    prixNetSaisi = ArrondiPrix.appliquer(prixNetSaisi, arrondiPrix);
    
    if (affichageTTC) {
      // Contrôle de la validité du prix net uniquement en modification
      if (modeUtilisation == EnumModeUtilisation.MODIFICATION) {
        prixNetSaisi = ligneVente.controlerValiditePrixNetSaisiHT(prixNetSaisi, false);
      }
      // Le prix net
      ligneVente.setPrixNetSaisiTTC(prixNetSaisi);
      ligneVente.setPrixNetSaisiHT(OutilCalculPrix.calculerPrixHTAvecPrixTTC(prixNetSaisi, ligneVente.getPourcentageTVA(), arrondiPrix));
    }
    else {
      // Contrôle de la validité du prix net uniquement en modification
      if (modeUtilisation == EnumModeUtilisation.MODIFICATION) {
        prixNetSaisi = ligneVente.controlerValiditePrixNetSaisiHT(prixNetSaisi, false);
      }
      // Le prix net
      ligneVente.setPrixNetSaisiHT(prixNetSaisi);
      ligneVente.setPrixNetSaisiTTC(OutilCalculPrix.calculerPrixTTC(prixNetSaisi, ligneVente.getPourcentageTVA(), arrondiPrix));
    }
    ligneVente.setProvenancePrixNet(EnumProvenancePrixNet.SAISIE_LIGNE_VENTE);
    
    // Le prix de base
    ligneVente.setPrixBaseHT(ligneVente.getPrixNetSaisiHT());
    ligneVente.setPrixBaseTTC(ligneVente.getPrixNetSaisiTTC());
    
    // Mettre à jour le prix de revient s'il n'est pas renseigné
    calculerPrixRevientStandard();
    // Calcul de l'indice de marge
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    // Calcul du taux de marque
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier l'indice de marge.
   * @param pSaisie L'indice de marge
   */
  public void modifierIndiceDeMarge(String pSaisie) {
    // Contrôle de la valeur saisie
    BigDecimal indiceMarge = Constantes.convertirTexteEnBigDecimal(pSaisie);
    indiceMarge = ligneVente.controlerValiditeIndiceDeMarge(indiceMarge, false);
    
    // Détermination du prix de revient à utiliser
    BigDecimal prixDeRevientHT = ligneVente.getPrixDeRevientStandardHT();
    if (ligneVente.getPrixDeRevientLigneHT() != null && ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) != 0) {
      prixDeRevientHT = ligneVente.getPrixDeRevientLigneHT();
    }
    
    // Calcul du prix net à l'aide du nouvel indice
    BigDecimal prixNetHT =
        ArrondiPrix.appliquer(prixDeRevientHT.multiply(indiceMarge).divide(Constantes.VALEUR_CENT, MathContext.DECIMAL32), arrondiPrix);
    ligneVente.setPrixNetSaisiHT(prixNetHT);
    ligneVente.setPrixNetSaisiTTC(OutilCalculPrix.calculerPrixTTC(prixNetHT, ligneVente.getPourcentageTVA(), arrondiPrix));
    // Calcul de l'indice de marge et du taux de marque suite au recalcul du prix net
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la marge (taux de marque).
   */
  public void modifierMarge(String pSaisie) {
    // Contrôle de la valeur saisie
    BigDecimal tauxMarque = Constantes.convertirTexteEnBigDecimal(pSaisie);
    tauxMarque = ligneVente.controlerValiditeTauxDeMarque(tauxMarque, false);
    if (Constantes.VALEUR_CENT.subtract(tauxMarque).compareTo(BigDecimal.ZERO) == 0) {
      rafraichir();
      return;
    }
    
    // Détermination du prix de revient à utiliser
    BigDecimal prixDeRevientHT = ligneVente.getPrixDeRevientStandardHT();
    if (ligneVente.getPrixDeRevientLigneHT() != null && ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) != 0) {
      prixDeRevientHT = ligneVente.getPrixDeRevientLigneHT();
    }
    
    // Calcul du prix net à l'aide du nouveau taux de marque
    BigDecimal prixNetHT = ArrondiPrix.appliquer(
        Constantes.VALEUR_CENT.multiply(prixDeRevientHT).divide(Constantes.VALEUR_CENT.subtract(tauxMarque), MathContext.DECIMAL32),
        arrondiPrix);
    ligneVente.setPrixNetSaisiHT(prixNetHT);
    ligneVente.setPrixNetSaisiTTC(OutilCalculPrix.calculerPrixTTC(prixNetHT, ligneVente.getPourcentageTVA(), arrondiPrix));
    
    // Calcul de l'indice de marge et du taux de marque suite au recalcul du prix net
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le prix de revient standard.
   */
  public void modifierPrixRevientStandard(String pSaisie) {
    BigDecimal prixRevient = Constantes.convertirTexteEnBigDecimal(pSaisie);
    
    if (affichageTTC) {
      ligneVente
          .setPrixDeRevientLigneHT(OutilCalculPrix.calculerPrixHTAvecPrixTTC(prixRevient, ligneVente.getPourcentageTVA(), arrondiPrix));
      ligneVente.setPrixDeRevientLigneTTC(prixRevient);
      ligneVente.setPrixDeRevientStandardHT(
          OutilCalculPrix.calculerPrixHTAvecPrixTTC(prixRevient, ligneVente.getPourcentageTVA(), arrondiPrix));
      ligneVente.setPrixDeRevientStandardTTC(prixRevient);
    }
    else {
      ligneVente.setPrixDeRevientLigneHT(prixRevient);
      ligneVente.setPrixDeRevientLigneTTC(OutilCalculPrix.calculerPrixTTC(prixRevient, ligneVente.getPourcentageTVA(), arrondiPrix));
      ligneVente.setPrixDeRevientStandardHT(prixRevient);
      ligneVente.setPrixDeRevientStandardTTC(OutilCalculPrix.calculerPrixTTC(prixRevient, ligneVente.getPourcentageTVA(), arrondiPrix));
    }
    
    // Calcul de l'indice de marge et du taux de marque suite au recalcul du prix net
    tauxDeMarque = ligneVente.calculerTauxDeMarque(arrondiPrix);
    indiceDeMarge = ligneVente.calculerIndiceDeMarge(arrondiPrix);
    
    // Prix de revient standard de l'article
    article.setPrixDeRevientStandardHT(ligneVente.getPrixDeRevientLigneHT());
    
    // Modifier la condition d'achats
    ConditionAchat conditionAchat = getConditionAchat();
    if (conditionAchat != null && conditionAchat.isEmpty()) {
      getConditionAchat().setPrixAchatBrutHT(ligneVente.getPrixDeRevientLigneHT());
    }
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier si l'unité de conditionnement de ventes est scindable
   */
  public void modifierUCVScindable(boolean pScindable) {
    article.setUniteConditionnementVenteNonScindable(!pScindable);
    
    if (article.getIdUCV() == null || article.getIdUCV().equals(article.getIdUV()) || article.isUniteConditionnementVenteNonScindable()) {
      nombreDecimalesUCV = getListeUnite().getPrecisionUnite(article.getIdUCV());
    }
    else {
      nombreDecimalesUCV = Article.NOMBRE_DECIMALE_UCV_SCINDABLE;
    }
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le type d'affichage: soit le prix TTC soit le prix HT.
   */
  public void modifierTypeAffichage(boolean pAffichageTTC) {
    // Tester si la valeur a changé
    if (affichageTTC == pAffichageTTC) {
      return;
    }
    affichageTTC = pAffichageTTC;
    
    // Rafraîchir l'affichage
    rafraichir();
  }
  
  /**
   * Modifie la visiblité des informations sur les marges.
   */
  public void modifierMargeVisible(boolean pMargeVisible) {
    // Tester si la valeur a changé
    if (margeVisible == pMargeVisible) {
      return;
    }
    margeVisible = pMargeVisible;
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier prix achat brut.
   */
  public void modifierPrixAchatBrut(String pSaisie) {
    getConditionAchat().setPrixAchatBrutHT(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 1 fournisseur.
   */
  public void modifierRemiseFrs1(String pSaisie) {
    getConditionAchat().setPourcentageRemise1(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 2 fournisseur.
   */
  public void modifierRemiseFrs2(String pSaisie) {
    getConditionAchat().setPourcentageRemise2(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 3 fournisseur.
   */
  public void modifierRemiseFrs3(String pSaisie) {
    getConditionAchat().setPourcentageRemise3(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la remise 4 fournisseur.
   */
  public void modifierRemiseFrs4(String pSaisie) {
    getConditionAchat().setPourcentageRemise4(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage de majoration.
   */
  public void modifierTaxe(String pSaisie) {
    getConditionAchat().setPourcentageTaxeOuMajoration(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur de conditionnement.
   */
  public void modifierValeurConditionnementAchat(String pSaisie) {
    getConditionAchat().setMontantConditionnement(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le mode de saisie du port.
   */
  public void modifierModeSaisiePort(EnumModeSaisiePort pEnumModeSaisiePort) {
    // Tester si la valeur a changé
    if (Constantes.equals(getConditionAchat().getModeSaisiePort(), pEnumModeSaisiePort)) {
      return;
    }
    
    getConditionAchat().setModeSaisiePort(pEnumModeSaisiePort);
    
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier la valeur des frais de port.
   */
  public void modifierPortValeur(String pSaisie) {
    getConditionAchat().setMontantAuPoidsPortHT(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le poids des frais de port.
   */
  public void modifierPortPoids(String pSaisie) {
    getConditionAchat().setPoidsPort(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier le pourcentage des frais de port.
   */
  public void modifierPortPourcentage(String pSaisie) {
    getConditionAchat().setPourcentagePort(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * Modifier les frais d'exploitation.
   */
  public void modifierFraisExploitation(String pSaisie) {
    getConditionAchat().setFraisExploitation(Constantes.convertirTexteEnBigDecimal(pSaisie));
    modifierPrixRevientLigne();
    
    // Rafraîchir l'affichage
    composantAyantLeFocus = FOCUS_NON_INDIQUE;
    rafraichir();
  }
  
  /**
   * 
   * Retourner si la gestion par lot est activée et, pour les articles spéciaux, si le document en cours est un avoir.
   * 
   * @return boolean
   */
  public boolean isGestionParLot() {
    return modeleParent.isGestionParLot() && documentVente.isAvoir();
  }
  
  /**
   * 
   * Saisir les numéros de lots (sur un avoir contenant un article spécial).
   *
   */
  public void saisirLot() {
    article.setIdMagasin(utilisateur.getIdMagasin());
    ModeleLot modeleLot = new ModeleLot(getSession(), ligneVente.getId(), article, ligneVente.getQuantiteUCV());
    if (documentVente.isAvoir()) {
      modeleLot.setIdDocumentOrigine(getParentModel().getIdDocumentOrigineAvoir());
    }
    VueLot vueLot = new VueLot(modeleLot);
    vueLot.afficher();
    
    // Sortie avec validation
    if (modeleLot.isSortieAvecValidation()) {
      // mise à jour de la ligne pour indiquer si la saisie des lots est complète
      if (modeleLot.isSaisieLotComplete()) {
        ligneVente.setTopNumSerieOuLot(40);
      }
      else {
        ligneVente.setTopNumSerieOuLot(30);
      }
      rafraichir();
    }
  }
  
  // --------------------------------------------------------------------------------------------------------------------------------------
  // Accesseurs
  // --------------------------------------------------------------------------------------------------------------------------------------
  /**
   * Retourner le modele
   */
  public ModeleComptoir getParentModel() {
    return modeleParent;
  }
  
  /**
   * Mode création ou modification.
   */
  public EnumModeUtilisation getMode() {
    return modeUtilisation;
  }
  
  /**
   * Composant ayant le focus.
   */
  public int getComposantAyantLeFocus() {
    return composantAyantLeFocus;
  }
  
  /**
   * Etablissement courant.
   */
  public Etablissement getEtablissement() {
    return etablissement;
  }
  
  /**
   * Ligne de ventes en cours de saisie.
   */
  public LigneVente getLigneVente() {
    return ligneVente;
  }
  
  /**
   * Article spécial en cours de saisie.
   */
  public Article getArticle() {
    return article;
  }
  
  /**
   * Condition d'achats.
   */
  public ConditionAchat getConditionAchat() {
    return conditionAchat;
  }
  
  /**
   * Liste des unités.
   */
  public ListeUnite getListeUnite() {
    return listeUnite;
  }
  
  /**
   * Renvoyer la liste des groupes
   */
  public ListeGroupe getListeGroupe() {
    return listeGroupe;
  }
  
  /**
   * Renvoyer la liste des familles
   */
  public ListeFamille getListeFamille() {
    return listeFamillePourGroupe;
  }
  
  /**
   * Renvoyer la liste des sous familles par familles
   */
  public ListeSousFamille getListeSousFamille() {
    return listeSousFamillePourFamille;
  }
  
  /**
   * Indiquer s'il existe au moins une sous-famille dans la liste des sous-familles pour la famille sélectionnée.
   * @return true=liste des sous-familles non vide, false=liste des sous-familles vide.
   */
  public boolean isSousFamilleExiste() {
    return listeSousFamillePourFamille != null && listeSousFamillePourFamille.size() > 0;
  }
  
  /**
   * Indiquer si l'affichage doit être en TTC.
   */
  public boolean isAffichageTTC() {
    return affichageTTC;
  }
  
  /**
   * Renvoyer si la marge est visualisable par l'utilisateur
   */
  public boolean isMargeVisible() {
    return margeVisible;
  }
  
  /**
   * Adresse fournisseur.
   */
  public AdresseFournisseur getAdresseFournisseur() {
    return adressefournisseur;
  }
  
  /**
   * Groupe de l'article spécial.
   * Méthode privée car elle ne doit être utilisée que dans le modèle.
   */
  public Groupe getGroupe() {
    return groupe;
  }
  
  /**
   * Modifier le groupe de l'article spécial.
   * Méthode privée car elle ne doit être utilisée que dans le modèle. La vue doit utiliser modifierGroupe().
   */
  private void setGroupe(Groupe pGroupe) {
    groupe = pGroupe;
  }
  
  /**
   * Famille de l'article spécial.
   */
  public Famille getFamille() {
    return famille;
  }
  
  /**
   * Sous-famille de l'article spécial.
   */
  public SousFamille getSousFamille() {
    return sousFamille;
  }
  
  /**
   * Lecture de la sécurité sur le droit à négocier les achats avec les frais d'exploitation.
   */
  public boolean isAutoriseFraisExploitation() {
    return ManagerSessionClient.getInstance().verifierDroitGescom(getIdSession(),
        EnumDroitSecurite.IS_AUTORISE_NEGOCIATION_ACHAT_AVEC_FRAIS_EXPLOITATION);
  }
  
  /**
   * Retourner le client.
   * @return Le client.
   */
  public Client getClient() {
    return client;
  }
  
  /**
   * Retourne le prix net HT.
   * @return Le prix net HT.
   */
  public BigDecimal getPrixNetHT() {
    if (ligneVente == null) {
      return null;
    }
    return ligneVente.getPrixNetHT();
  }
  
  /**
   * Retourne le prix net TTC.
   * @return Le prix net TTC.
   */
  public BigDecimal getPrixNetTTC() {
    if (ligneVente == null) {
      return null;
    }
    return ligneVente.getPrixNetTTC();
  }
  
  /**
   * Retourne le prix de revient HT.
   * @return Le prix de revient HT.
   */
  public BigDecimal getPrixDeRevientHT() {
    if (ligneVente == null) {
      return null;
    }
    if (ligneVente.getPrixDeRevientLigneHT() != null && ligneVente.getPrixDeRevientLigneHT().compareTo(BigDecimal.ZERO) > 0) {
      return ligneVente.getPrixDeRevientLigneHT();
    }
    return ligneVente.getPrixDeRevientStandardHT();
  }
  
  /**
   * Retourne le prix de revient TTC.
   * @return Le prix de revient TTC.
   */
  public BigDecimal getPrixDeRevientTTC() {
    if (ligneVente == null) {
      return null;
    }
    if (ligneVente.getPrixDeRevientLigneTTC() != null && ligneVente.getPrixDeRevientLigneTTC().compareTo(BigDecimal.ZERO) > 0) {
      return ligneVente.getPrixDeRevientLigneTTC();
    }
    return ligneVente.getPrixDeRevientStandardTTC();
  }
  
  /**
   * Retourner l'indice de marge.
   * @return L'indice de marge.
   */
  public BigDecimal getIndiceDeMarge() {
    return indiceDeMarge;
  }
  
  /**
   * Retourner le taux de marque.
   * @return Le taux de marque.
   */
  public BigDecimal getTauxDeMarque() {
    return tauxDeMarque;
  }
  
}
