/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.desktop.metier.gescom.comptoir.genererconditionventes;

import java.awt.Component;

import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableColumnModel;

public class JTableCellRendererLignesDevis extends DefaultTableCellRenderer {
  private static DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
  private static DefaultTableCellRenderer leftRenderer = new DefaultTableCellRenderer();
  
  @Override
  public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
    Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    
    // Taille des colonnes
    redimensionnerColonnes(table);
    
    // Justification
    centerRenderer.setHorizontalAlignment(SwingConstants.CENTER);
    rightRenderer.setHorizontalAlignment(SwingConstants.RIGHT);
    leftRenderer.setHorizontalAlignment(SwingConstants.LEFT);
    TableColumnModel cm = table.getColumnModel();
    cm.getColumn(0).setCellRenderer(leftRenderer);
    cm.getColumn(1).setCellRenderer(leftRenderer);
    cm.getColumn(2).setCellRenderer(rightRenderer);
    cm.getColumn(3).setCellRenderer(centerRenderer);
    cm.getColumn(4).setCellRenderer(leftRenderer);
    cm.getColumn(5).setCellRenderer(rightRenderer);
    cm.getColumn(6).setCellRenderer(rightRenderer);
    
    return component;
  }
  
  /**
   * Redimensionne les colonnes de la table.
   */
  public void redimensionnerColonnes(JTable pTable) {
    TableColumnModel cm = pTable.getColumnModel();
    cm.getColumn(0).setMinWidth(100);
    cm.getColumn(0).setMaxWidth(100);
    cm.getColumn(1).setMinWidth(150);
    cm.getColumn(2).setMinWidth(100);
    cm.getColumn(2).setMaxWidth(100);
    cm.getColumn(3).setMinWidth(40);
    cm.getColumn(3).setMaxWidth(40);
    cm.getColumn(4).setMinWidth(100);
    cm.getColumn(4).setMaxWidth(100);
    cm.getColumn(5).setMinWidth(70);
    cm.getColumn(5).setMaxWidth(70);
    cm.getColumn(6).setMinWidth(100);
    cm.getColumn(6).setMaxWidth(100);
    
  }
  
}
