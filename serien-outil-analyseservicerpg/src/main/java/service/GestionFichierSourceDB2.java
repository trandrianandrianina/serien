/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package service;

import java.io.IOException;
import java.util.ArrayList;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400File;
import com.ibm.as400.access.AS400FileRecordDescription;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.Record;
import com.ibm.as400.access.RecordFormat;
import com.ibm.as400.access.SequentialFile;

/**
 * Outils qui permet de travailler avec les sources.
 */
public class GestionFichierSourceDB2 {
  private AS400 system = null;
  private QSYSObjectPathName filePathName = null;
  private ArrayList<String> listing = null;
  private int lgrecord = 92;
  private String typeMember = "RPG";
  
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   * @param system
   */
  public GestionFichierSourceDB2(AS400 system, String bib, String file, String member) {
    this.system = system;
    filePathName = new QSYSObjectPathName(bib, file, member, "MBR");
  }
  
  /**
   * Initialise le type du membre (RPG, CLP, ...)
   * @param type
   */
  public void setTypeMember(String type) {
    typeMember = type;
  }
  
  /**
   * Vérifie l'existence d'un membre
   * @param fch
   * @return
   * @throws IOException
   */
  private boolean isMemberPresent(String fch) throws IOException {
    IFSFile fichier = new IFSFile(system, fch);
    return fichier.exists();
  }
  
  /**
   * Vérifie l'existence d'un fichier
   * @param fch
   * @return
   * @throws IOException
   */
  private boolean isFilePresent(String fch) throws IOException {
    if (fch.endsWith(".MBR")) {
      fch = fch.substring(0, fch.lastIndexOf('/'));
    }
    IFSFile fichier = new IFSFile(system, fch);
    return fichier.exists();
  }
  
  /**
   * Initialise la longeur des enregistrements pour le fichier
   * @param lg
   */
  public void setLongRecord(int lg) {
    lgrecord = lg;
  }
  
  /**
   * Initialise le contenu du fichier
   * @param liste
   */
  public void setContenuFichier(ArrayList<String> liste) {
    listing = liste;
  }
  
  /**
   * Initialise le contenu du fichier
   * @param liste
   */
  public ArrayList<String> getContenuFichier() {
    return listing;
  }
  
  /**
   * Lit le contenu d'un fichier
   * @param infos
   */
  public boolean readMember(boolean infos) {
    SequentialFile rf = null;
    
    try {
      // On vérifie que le fichier existe
      if (!isMemberPresent(filePathName.getPath())) {
        return false;
      }
      
      listing = new ArrayList<String>();
      
      rf = new SequentialFile(system, filePathName.getPath());
      AS400FileRecordDescription recordDescription = new AS400FileRecordDescription(system, filePathName.getPath());
      
      RecordFormat[] format = recordDescription.retrieveRecordFormat();
      
      // Set the record format for the file
      rf.setRecordFormat(format[0]);
      
      // Open the file for reading. Read 100 records at a time if possible.
      rf.open(AS400File.READ_ONLY, 100, AS400File.COMMIT_LOCK_LEVEL_NONE);
      
      Record record = rf.readNext();
      if (infos) {
        while (record != null) {
          listing.add(record.toString());
          record = rf.readNext();
        }
      }
      else {
        while (record != null) {
          listing.add(record.getField("SRCDTA").toString());
          record = rf.readNext();
        }
      }
      
      // Close the file
      rf.close();
    }
    catch (Exception e) {
      return false;
    }
    
    // Disconnect from the record level access service
    system.disconnectService(AS400.RECORDACCESS);
    return true;
  }
  
  /**
   * Ecrit le contenu d'un fichier
   * @param infos
   */
  public boolean writeMember(String descriptif, String type) {
    if (listing == null) {
      return false;
    }
    
    // Create a file object that represents the file
    SequentialFile newFile = new SequentialFile(system, filePathName.getPath());
    AS400FileRecordDescription recordDescription = new AS400FileRecordDescription(system, filePathName.getPath());
    try {
      // Création du membre si besoin
      createMemberwithCL(descriptif, type);
      RecordFormat[] format = recordDescription.retrieveRecordFormat();
      newFile.setRecordFormat(format[0]);
      
      // Open the file for writing only.
      // Note: The record format for the file has already been set by create()
      newFile.open(AS400File.WRITE_ONLY, 0, AS400File.COMMIT_LOCK_LEVEL_NONE);
      
      // Write a record to the file. Because the record format was set on the create(), getRecordFormat()
      // can be called to get a record properly formatted for this file.
      Record writeRec = newFile.getRecordFormat().getNewRecord();
      for (int i = 0; i < listing.size(); i++) {
        writeRec.setField("SRCDTA", listing.get(i));
        newFile.write(writeRec);
      }
      // Close the file since I am done using it
      newFile.close();
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
      return false;
    }
    
    // Disconnect since I am done using record-level access
    system.disconnectService(AS400.RECORDACCESS);
    return true;
  }
  
  /**
   * Crée le fichier source uniquement
   */
  public boolean createFile(String descriptif) {
    String fichier = filePathName.getPath();
    if (descriptif == null) {
      descriptif = "";
    }
    
    try {
      if (isFilePresent(fichier)) {
        return true;
      }
      executeCmd("CRTSRCPF FILE(" + filePathName.getLibraryName() + "/" + filePathName.getObjectName() + ") RCDLEN(" + lgrecord + ") TEXT("
          + descriptif + ")");
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
    
    return true;
  }
  
  /**
   * Crée le fichier source uniquement
   * TODO c'est pas bo mais je ne sais pas faire autrement pour l'instant
   * 
   * public boolean createFile(String descriptif)
   * {
   * String fichier=filePathName.getPath();
   * if (descriptif == null) descriptif = "";
   * try
   * {
   * if (isFilePresent(fichier)) return true;
   * // Create a file object that represents the file
   * SequentialFile newFile = new SequentialFile(system, fichier);
   * newFile.create(lgrecord, AS400File.TYPE_SOURCE, descriptif);
   * newFile.
   * AS400FileRecordDescription recordDescription = new AS400FileRecordDescription(system, filePathName.getPath());
   * newFile.deleteMember();
   * }
   * catch(Exception e)
   * {
   * msgErreur += "\n" + e;
   * return false;
   * }
   * 
   * // Disconnect since I am done using record-level access
   * system.disconnectService(AS400.RECORDACCESS);
   * return true;
   * }
   */
  
  /**
   * Crée le membre source
   */
  public boolean createMember(String descriptif) {
    if (descriptif == null) {
      descriptif = "";
    }
    try {
      if (isMemberPresent(filePathName.getPath())) {
        return true;
      }
      // Création du fichier si besoin
      createFile("");
      
      // Create a file object that represents the file
      SequentialFile newFile = new SequentialFile(system, filePathName.getPath());
      newFile.addPhysicalFileMember(filePathName.getMemberName(), descriptif);
    }
    catch (Exception e) {
      msgErreur += "\n" + e;
      return false;
    }
    
    // Disconnect since I am done using record-level access
    system.disconnectService(AS400.RECORDACCESS);
    return true;
  }
  
  /**
   * Crée le fichier source uniquement (avec ordre CL)
   */
  public boolean createMemberwithCL(String descriptif, String type) {
    typeMember = type;
    if (descriptif == null) {
      descriptif = "";
    }
    
    try {
      if (isMemberPresent(filePathName.getPath())) {
        return true;
      }
      String chaine = "ADDPFM FILE(" + filePathName.getLibraryName() + "/" + filePathName.getObjectName() + ") MBR("
          + filePathName.getMemberName() + ") TEXT('" + descriptif + "') SRCTYPE(" + typeMember + ")";
      executeCmd(chaine);
    }
    catch (Exception e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
    }
    
    return true;
  }
  
  /**
   * Supprime un source (membre)
   * @return
   */
  public boolean removeMember() {
    IFSFile file = new IFSFile(system, filePathName.getPath());
    try {
      return file.delete();
    }
    catch (IOException e) {
      e.printStackTrace();
      msgErreur += "\n" + e.getMessage();
      return false;
    }
  }
  
  /**
   * Retourne le type du source (RPG, SQLRPG, CLP, ...)
   * NE FONCTIONNE PAS !!!
   * @return
   */
  public String getType() {
    if (filePathName.getObjectName().equals("QCLSRC")) {
      return "CLP";
    }
    else if (filePathName.getObjectName().equals("QRPGSRC")) {
      return "RPG";
    }
    else if (filePathName.getObjectName().equals("QRPGLESRC")) {
      return "RPGLE";
    }
    else {
      return "";
    }
    
    /*
    IFSFile file = new IFSFile(system, filePathName.getPath());
    try
    {
      return file.getSubtype();
    }
    catch (Exception e)
    {
      return "";
    }
    */
  }
  
  /**
   * Lance une commande CL
   */
  private void executeCmd(String command) throws Exception {
    CommandCall cmd = new CommandCall(system);
    cmd.run(command);
    
    AS400Message[] messageList = cmd.getMessageList();
    for (int i = 0; i < messageList.length; i++) {
      msgErreur += "\nCodeRetour : " + messageList[i].getID() + " " + messageList[i].getText();
    }
    
    system.disconnectService(AS400.COMMAND);
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    String chaine;
    
    // La récupération du message est à usage unique
    chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
}
