/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package service;

import java.io.File;
import java.util.ArrayList;

import com.ibm.as400.access.AS400;

import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Analyse un fichier DDS (QDDSSRC) et génère les classes java associée pour les data structures du fichier physique
 * (type PGVMSECM).
 */
public class Qdds3dsToJava {
  private AS400 system = null;
  private String bibliotheque = null;
  private String membre = null;
  private String cheminRacine = null;
  
  private boolean justEntete = false;
  
  /**
   * Constructeur
   */
  public Qdds3dsToJava(AS400 systeme, String chemin) {
    cheminRacine = chemin;
    system = systeme;
  }
  
  /**
   * Analyse du fichier RPG3 et génération du fichier java
   * @param bib
   * @param fichier
   * @param member
   * @return
   */
  public boolean analyseDDS3SRC(String bib, String fichier, String member, String fichierClass, String forceModule, int longueurRecord) {
    ArrayList<String> source = chargementFichier(bib, fichier, member);
    bibliotheque = bib;
    membre = member;
    if (source == null) {
      return false;
    }
    ArrayList<VariableDDS> listeVariables = new ArrayList<VariableDDS>();
    recolteVariables(source, listeVariables);
    
    genereSourceJava(listeVariables, cheminRacine + fichierClass.toLowerCase() + ".java", forceModule, longueurRecord);
    
    destructor();
    return true;
  }
  
  /**
   * Charge le contenu du fichier source dans une arraylist
   */
  private ArrayList<String> chargementFichier(String bib, String fichier, String member) {
    if ((member == null) || (fichier == null) || (bib == null)) {
      return null;
    }
    
    GestionFichierSourceDB2 gfsDB2 = new GestionFichierSourceDB2(system, bib, fichier, member);
    gfsDB2.readMember(false);
    
    return gfsDB2.getContenuFichier();
  }
  
  /**
   * Récupère les variables et leurs attributs
   * @param source
   * @param listeVariables
   */
  private void recolteVariables(ArrayList<String> source, ArrayList<VariableDDS> listeVariables) {
    String dec = null;
    String chaine = null;
    VariableDDS varDDS = null;
    char type = ' ';
    
    // On parcourt le listing complet
    for (int i = 0; i < source.size(); i++) {
      chaine = source.get(i);
      
      // Si ligne vide on zappe
      if (chaine.trim().equals("")) {
        continue;
      }
      // Si ligne contenant nom du record
      if (chaine.charAt(16) == 'R') {
        continue;
      }
      // Si carte différent de A on zappe
      if (chaine.charAt(5) != 'A') {
        continue;
      }
      // Si ligne commentaire on zappe
      if (chaine.charAt(6) == '*') {
        continue;
      }
      
      // Récupération des infos
      varDDS = new VariableDDS();
      varDDS.setNomVar(chaine.substring(18, 28).trim());
      
      // On recherche le type de la variable
      type = chaine.charAt(34);
      if (type == ' ') {
        type = 'A';
      }
      else if (type == 'P') {
        type = 'S';
        varDDS.setPacked(true);
      }
      else if ((type != 'A') && (type != 'S')) {
        break;
      }
      varDDS.setTypeVar(type);
      varDDS.setLongVar(Integer.parseInt(chaine.substring(29, 34).trim()));
      if (varDDS.getTypeVar() == 'S') {
        dec = "" + chaine.substring(35, 37).trim();
        varDDS.setDecVar(dec.equals("") ? 0 : Integer.parseInt(dec));
      }
      // varDDS.setLibelle(chaine.substring(44).trim());
      listeVariables.add(varDDS);
    }
  }
  
  /**
   * Génére le fichier source Java
   */
  public boolean genereSourceJava(final ArrayList<VariableDDS> listeVariables, String ficjva, String forceModule, int longueurRecord) {
    File fficjva = null;
    
    String module = "gvx";
    if (forceModule == null) {
      if (!bibliotheque.substring(1).toLowerCase().equals("gvmx")) {
        module = bibliotheque.substring(1, 4).toLowerCase();
      }
    }
    else {
      module = forceModule;
    }
    
    String nomfic = ficjva;
    int pos = nomfic.lastIndexOf(File.separatorChar);
    if (pos != -1) {
      pos++;
      nomfic = nomfic.substring(0, pos) + nomfic.substring(pos, pos + 1).toUpperCase() + nomfic.substring(pos + 1);
    }
    else {
      nomfic = nomfic.substring(0, 1).toUpperCase() + nomfic.substring(pos + 1);
    }
    
    ArrayList<String> source = new ArrayList<String>();
    ficjva = nomfic.replaceFirst(".java", "_ds.java");
    fficjva = new File(ficjva);
    
    // Ecriture de l'entête du source
    ecritureEntete(source, fficjva.getName().substring(0, fficjva.getName().lastIndexOf('.')), module);
    
    // Ecriture des autres méthodes
    ecritureMethodes(source, listeVariables, longueurRecord);
    
    // Ecriture du pied du source
    ecriturePied(source);
    
    // On génére le source
    if (fficjva.exists()) {
      fficjva.delete();
    }
    
    GestionFichierTexte gfp = new GestionFichierTexte(fficjva);
    gfp.setContenuFichier(source);
    gfp.ecritureFichier();
    return true;
  }
  
  /**
   * Ecriture de l'entête du source
   * @param source
   */
  private void ecritureEntete(final ArrayList<String> source, String nomClass, String module) {
    final String dateJour = DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA);
    
    source.add("//=================================================================================================");
    source.add("//==>                                                                       " + dateJour + " - " + dateJour);
    source.add("//==> Description de l'enregistrement du fichier " + nomClass);
    source.add("//==> Généré avec la classe Qdds3dsToJava du projet AnalyseSourceRPG");
    source.add("//=================================================================================================");
    source.add("package " + module + ".database;\n");
    source.add("import com.ibm.as400.access.AS400Text;");
    source.add("import com.ibm.as400.access.CharacterFieldDescription;");
    source.add("import com.ibm.as400.access.AS400ZonedDecimal;");
    source.add("import com.ibm.as400.access.ZonedDecimalFieldDescription;");
    source.add("import com.ibm.as400.access.AS400PackedDecimal;");
    source.add("import com.ibm.as400.access.PackedDecimalFieldDescription;");
    source.add("import database.record.DataStructureRecord;");
    source.add("\npublic class " + nomClass + " extends DataStructureRecord");
    source.add("{");
  }
  
  /**
   * Ecriture du pied du source
   * @param source
   */
  private void ecriturePied(final ArrayList<String> source) {
    source.add("}");
  }
  
  /**
   * Ecriture des autres méthodes
   * @param source
   * @param listeVariables
   */
  private void ecritureMethodes(final ArrayList<String> source, final ArrayList<VariableDDS> listeVariables, int longueurRecord) {
    // Methodes
    ecritureInit(source, listeVariables, longueurRecord);
  }
  
  /**
   * Ecriture de la methode d'init
   * @param source
   * @param varDDS
   */
  private void ecritureInit(final ArrayList<String> source, final ArrayList<VariableDDS> listeVariables, int longueurRecord) {
    VariableDDS varDDS = null;
    final String controle = " // A contrôler car à l'origine c'est une zone packed";
    
    // Ecriture commentaire
    source.add("\n\t/**\n\t * Création de la data structure pour l'enregistrement du fichier\n\t */");
    // Ecriture du code
    source.add("\tpublic void initRecord()\n\t{");
    for (int i = 0; i < listeVariables.size(); i++) {
      varDDS = listeVariables.get(i);
      if (varDDS.getTypeVar() == 'A') {
        if (varDDS.isPacked()) {
          source.add("\t\trf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(" + varDDS.getLongVar() + ", "
              + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));" + controle);
        }
        else {
          source.add("\t\trf.addFieldDescription(new CharacterFieldDescription(new AS400Text(" + varDDS.getLongVar() + "), \""
              + varDDS.getNomVar() + "\"));");
        }
      }
      else if (varDDS.getTypeVar() == 'S') {
        if (varDDS.isPacked()) {
          source.add("\t\trf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal("
              + ((varDDS.getLongVar() * 2) - 1) + ", " + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));");
          // Cas un peu spécial à contrôler
          if (varDDS.getLongVar() > 15) {
            source.set(source.size() - 1, source.get(source.size() - 1) + controle);
          }
        }
        else {
          source.add("\t\trf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(" + varDDS.getLongVar() + ", "
              + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));");
        }
      }
      
    }
    // Ajout de la taille total du record
    source.add("\n\t\tlength = " + longueurRecord + ";");
    
    source.add("\t}");
  }
  
  /**
   * @param justEntete the justEntete to set
   */
  public void setJustEntete(boolean justEntete) {
    this.justEntete = justEntete;
  }
  
  /**
   * @return the justEntete
   */
  public boolean isJustEntete() {
    return justEntete;
  }
  
  /**
   * Termine les opérations
   */
  private void destructor() {
    if (system == null) {
      return;
    }
    system.disconnectAllServices();
  }
  
  /**
   * Voir plutot la classe generationDSSerieNMetier
   * @param args
   */
  /*
  public static void main(String[] args) {
    AS400 systeme = new AS400("172.31.1.249", "RIDEVSV", "gar1972");
    Qdds3dsToJava test = new Qdds3dsToJava(systeme,
        "C:\\Documents and Settings\\Administrateur\\Bureau\\GenereDS\\");
    test.setJustEntete(true);
    if (test.analyseDDS3SRC("VEXPAS", "QDDSSRC", "PGVMSECDS", "pgvmsecds", "gvx", 800)) {
    }
    else {
    }
  }*/
  
}
