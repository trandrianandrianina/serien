/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package service;

/**
 * Description d'une variable d'un fichier DDS.
 */
public class VariableDDS {
  // Les positions sont exprimées en valeurs depuis SEU, il faut les recalculer pour avoir la vrai colonne depuis le
  // source (-9)
  // les colonnes 25 non blanches, 15 (lignes commentées) sont ignorées
  
  // Variables
  private String nomVar = null; // 27 sur 10
  private char typeVar = ' '; // 43 A: Alphanumérique S:numerique
  private int longVar = 0; // 38 sur 5
  private int decVar = 0; // 44 sur 2
  private String libelle = null; // 53 sur 36
  private boolean isPacked = false;
  
  /**
   * @return the nomVar
   */
  public String getNomVar() {
    return nomVar;
  }
  
  /**
   * @param nomVar the nomVar to set
   */
  public void setNomVar(String nomVar) {
    this.nomVar = nomVar;
  }
  
  /**
   * @return the typeVar
   */
  public char getTypeVar() {
    return typeVar;
  }
  
  /**
   * @param typeVar the typeVar to set
   */
  public void setTypeVar(char typeVar) {
    this.typeVar = typeVar;
  }
  
  /**
   * @return the longVar
   */
  public int getLongVar() {
    return longVar;
  }
  
  /**
   * @param longVar the longVar to set
   */
  public void setLongVar(int longVar) {
    this.longVar = longVar;
  }
  
  /**
   * @return the decVar
   */
  public int getDecVar() {
    return decVar;
  }
  
  /**
   * @param decVar the decVar to set
   */
  public void setDecVar(int decVar) {
    this.decVar = decVar;
  }
  
  /**
   * @return the libelle
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * @param libelle the libelle to set
   */
  public void setLibelle(String libelle) {
    this.libelle = libelle;
  }
  
  /**
   * @param isPacked the isPacked to set
   */
  public void setPacked(boolean isPacked) {
    this.isPacked = isPacked;
  }
  
  /**
   * @return the isPacked
   */
  public boolean isPacked() {
    return isPacked;
  }
  
}
