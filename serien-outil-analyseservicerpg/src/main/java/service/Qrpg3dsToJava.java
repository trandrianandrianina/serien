/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package service;

import java.io.File;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import com.ibm.as400.access.AS400;

import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Analyse un fichier RPG3 (QRPGSRC) et génère les classes java associée pour les data structures du fichier physique
 * (type PGVMPARM).
 */
public class Qrpg3dsToJava {
  private AS400 system = null;
  private String bibliotheque = null;
  private String membre = null;
  private String cheminRacine = null;
  
  private boolean justEntete = false;
  
  /**
   * Constructeur
   */
  public Qrpg3dsToJava(AS400 systeme, String chemin) {
    cheminRacine = chemin;
    system = systeme;
  }
  
  /**
   * Analyse du fichier RPG3 et génération du fichier java
   * @param bib
   * @param fichier
   * @param member
   * @return
   */
  public boolean analyseRPG3SRC(String bib, String fichier, String member, String fichierClass, String forceModule, int longueurRecord) {
    ArrayList<String> source = chargementFichier(bib, fichier, member);
    bibliotheque = bib;
    membre = member;
    if (source == null) {
      return false;
    }
    LinkedHashMap<String, ArrayList<VariableDDS>> listeTypesVariables = new LinkedHashMap<String, ArrayList<VariableDDS>>();
    recolteVariables(source, listeTypesVariables);
    
    genereSourceJava(listeTypesVariables, cheminRacine + fichierClass.toLowerCase() + ".java", forceModule, longueurRecord);
    
    destructor();
    return true;
  }
  
  /**
   * Charge le contenu du fichier source dans une arraylist
   */
  private ArrayList<String> chargementFichier(String bib, String fichier, String member) {
    if ((member == null) || (fichier == null) || (bib == null)) {
      return null;
    }
    
    GestionFichierSourceDB2 gfsDB2 = new GestionFichierSourceDB2(system, bib, fichier, member);
    gfsDB2.readMember(false);
    
    return gfsDB2.getContenuFichier();
  }
  
  /**
   * Récupère les variables et leurs attributs
   * @param source
   * @param listeVariables
   */
  private void recolteVariables(ArrayList<String> source, LinkedHashMap<String, ArrayList<VariableDDS>> listeTypesVariables) {
    String dec = null;
    String chaine = null;
    VariableDDS varDDS = null;
    String type = null;
    ArrayList<VariableDDS> listeVariables = null;
    
    // On parcourt le listing complet
    for (int i = 0; i < source.size(); i++) {
      chaine = source.get(i);
      
      // Si ligne vide on zappe
      if (chaine.trim().equals("")) {
        continue;
      }
      // Si carte différent de I on zappe
      if (chaine.charAt(5) != 'I') {
        continue;
      }
      // Si ligne commentaire on zappe
      if (chaine.charAt(6) == '*') {
        continue;
      }
      // Si Data Structure fichier avec juste NS alors on arrête le traitement
      if ((chaine.substring(14, 16).equals("NS") && (chaine.substring(20).trim().equals(""))) || (chaine.charAt(5) == 'C')) {
        break;
      }
      
      // Si Data Structure fichier NS alors nouveau découpage
      if ((chaine.substring(14, 16).equals("NS")) && (!chaine.substring(20).trim().equals(""))) {
        type = "" + chaine.charAt(26) + chaine.charAt(33);
        listeVariables = new ArrayList<VariableDDS>();
        listeTypesVariables.put(type, listeVariables);
        continue;
      }
      // Cas particulier (VGVM01EX) : "I AND 19NC"
      if (chaine.substring(42).trim().equals("")) {
        continue;
      }
      
      // Récupération des infos
      varDDS = new VariableDDS();
      varDDS.setNomVar(chaine.substring(52, 59).trim());
      
      // On recherche le type de la variable
      if (chaine.charAt(51) != ' ') {
        varDDS.setTypeVar('S');
      }
      else {
        varDDS.setTypeVar('A');
      }
      varDDS.setPacked(chaine.charAt(42) == 'P');
      // if (varDDS.isPacked()) // Dans certains cas, il n'y a pas le zéro dans les décimales et on pourrait croire que
      // c'est une zone alpha
      // varDDS.setTypeVar('S');
      
      varDDS.setLongVar(Integer.parseInt(chaine.substring(47, 51).trim()) - Integer.parseInt(chaine.substring(43, 47).trim()) + 1);
      if (varDDS.getTypeVar() == 'S') {
        dec = "" + chaine.charAt(51);
        varDDS.setDecVar(dec.equals("") ? 0 : Integer.parseInt(dec));
      }
      // varDDS.setLibelle(chaine.substring(44).trim());
      if (listeVariables != null) {
        listeVariables.add(varDDS);
      }
    }
  }
  
  /**
   * Génére le fichier source Java
   */
  public boolean genereSourceJava(final LinkedHashMap<String, ArrayList<VariableDDS>> listeTypesVariables, String ficjva,
      String forceModule, int longueurRecord) {
    File fficjva = null;
    
    String module = "gvx";
    if (forceModule == null) {
      if (!bibliotheque.substring(1).toLowerCase().equals("gvmx")) {
        module = bibliotheque.substring(1, 4).toLowerCase();
      }
    }
    else {
      module = forceModule;
    }
    
    String nomfic = ficjva;
    int pos = nomfic.lastIndexOf(File.separatorChar);
    if (pos != -1) {
      pos++;
      nomfic = nomfic.substring(0, pos) + nomfic.substring(pos, pos + 1).toUpperCase() + nomfic.substring(pos + 1);
    }
    else {
      nomfic = nomfic.substring(0, 1).toUpperCase() + nomfic.substring(pos + 1);
    }
    
    for (Entry<String, ArrayList<VariableDDS>> e : listeTypesVariables.entrySet()) {
      ArrayList<VariableDDS> listeVariables = e.getValue();
      
      ArrayList<String> source = new ArrayList<String>();
      ficjva = nomfic.replaceFirst(".java", "_ds_" + e.getKey() + ".java");
      fficjva = new File(ficjva);
      
      // Ecriture de l'entête du source
      ecritureEntete(source, e.getKey(), fficjva.getName().substring(0, fficjva.getName().lastIndexOf('.')), module);
      
      // Ecriture des autres méthodes
      ecritureMethodes(source, listeVariables, longueurRecord);
      
      // Ecriture du pied du source
      ecriturePied(source);
      
      // On génére le source
      if (fficjva.exists()) {
        fficjva.delete();
      }
      
      GestionFichierTexte gfp = new GestionFichierTexte(fficjva);
      gfp.setContenuFichier(source);
      gfp.ecritureFichier();
    }
    return true;
  }
  
  /**
   * Ecriture de l'entête du source
   * @param source
   */
  private void ecritureEntete(final ArrayList<String> source, String type, String nomClass, String module) {
    DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA);
    
    source.add("/*");
    source.add(" * Copyright (C) Résolution Informatique - Tout droits réservés.");
    source.add(" * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.");
    source.add(" */\n");
    
    source.add("package ri.serien.libas400.dao." + module + ".database.files;\n");
    source.add("import com.ibm.as400.access.AS400PackedDecimal;");
    source.add("import com.ibm.as400.access.AS400Text;");
    source.add("import com.ibm.as400.access.AS400ZonedDecimal;");
    source.add("import com.ibm.as400.access.CharacterFieldDescription;");
    source.add("import com.ibm.as400.access.PackedDecimalFieldDescription;");
    source.add("import com.ibm.as400.access.ZonedDecimalFieldDescription;");
    source.add("\nimport ri.serien.libas400.database.record.DataStructureRecord;\n");
    
    source.add("/**");
    source.add(" * Description de l'enregistrement du fichier " + nomClass + " pour les " + type);
    source.add(" * Généré avec la classe Qrpg3dsToJava du projet AnalyseSourceRPG");
    source.add(" */");
    source.add("\npublic class " + nomClass + " extends DataStructureRecord {");
  }
  
  /**
   * Ecriture du pied du source
   * @param source
   */
  private void ecriturePied(final ArrayList<String> source) {
    source.add("}");
  }
  
  /**
   * Ecriture des autres méthodes
   * @param source
   * @param listeVariables
   */
  private void ecritureMethodes(final ArrayList<String> source, final ArrayList<VariableDDS> listeVariables, int longueurRecord) {
    // Methodes
    ecritureInit(source, listeVariables, longueurRecord);
  }
  
  /**
   * Ecriture de la methode d'init
   * @param source
   * @param varDDS
   */
  private void ecritureInit(final ArrayList<String> source, final ArrayList<VariableDDS> listeVariables, int longueurRecord) {
    VariableDDS varDDS = null;
    final String controle = " // A contrôler car à l'origine c'est une zone packed";
    
    // Ecriture commentaire
    source.add("\n  /**\n   * Création de la data structure pour l'enregistrement du fichier\n   */");
    // Ecriture du code
    source.add("  public void initRecord() {");
    for (int i = 0; i < listeVariables.size(); i++) {
      varDDS = listeVariables.get(i);
      if (varDDS.getTypeVar() == 'A') {
        if (varDDS.isPacked()) {
          source.add("    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(" + varDDS.getLongVar() + ", "
              + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));" + controle);
        }
        else {
          source.add("    rf.addFieldDescription(new CharacterFieldDescription(new AS400Text(" + varDDS.getLongVar() + "), \""
              + varDDS.getNomVar() + "\"));");
        }
      }
      else if (varDDS.getTypeVar() == 'S') {
        if (varDDS.isPacked()) {
          source.add("    rf.addFieldDescription(new PackedDecimalFieldDescription(new AS400PackedDecimal("
              + ((varDDS.getLongVar() * 2) - 1) + ", " + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));");
          // Cas un peu spécial à contrôler
          if (varDDS.getLongVar() > 15) {
            source.set(source.size() - 1, source.get(source.size() - 1) + controle);
          }
        }
        else {
          source.add("    rf.addFieldDescription(new ZonedDecimalFieldDescription(new AS400ZonedDecimal(" + varDDS.getLongVar() + ", "
              + varDDS.getDecVar() + "), \"" + varDDS.getNomVar() + "\"));");
        }
      }
      
    }
    // Ajout de la taille total du record
    source.add("\n    length = " + longueurRecord + ";");
    
    source.add("  }");
  }
  
  /**
   * @param justEntete the justEntete to set
   */
  public void setJustEntete(boolean justEntete) {
    this.justEntete = justEntete;
  }
  
  /**
   * @return the justEntete
   */
  public boolean isJustEntete() {
    return justEntete;
  }
  
  /**
   * Termine les opérations
   */
  private void destructor() {
    if (system == null) {
      return;
    }
    system.disconnectAllServices();
  }
  
  /**
   * Voir plutot la classe generationDSSerieNMetier
   * @param args
   */
  public static void main(String[] args) {
    AS400 systeme = new AS400("srp_dev", "RIDEVSV", "gar1972");
    Qrpg3dsToJava test = new Qrpg3dsToJava(systeme, System.getProperty("user.home") + File.separatorChar + "Desktop" + File.separatorChar
        + "ConversionServiceRPG" + File.separatorChar);
    test.setJustEntete(true);
    test.analyseRPG3SRC("SCGMAS", "QRPGSRC", "VCGM01", "pcgmpacm", "cgm", 300);
    systeme.disconnectAllServices();
  }
  
}
