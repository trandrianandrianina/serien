/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package service;

import java.io.File;
import java.util.ArrayList;

import javax.swing.JOptionPane;

import com.ibm.as400.access.AS400FileRecordDescription;
import com.ibm.as400.access.CharacterFieldDescription;
import com.ibm.as400.access.FieldDescription;
import com.ibm.as400.access.PackedDecimalFieldDescription;
import com.ibm.as400.access.RecordFormat;
import com.ibm.as400.access.ZonedDecimalFieldDescription;

import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Cette classe à pour objectif de générer les classes paramètres associées aux services RPG.
 * <ul>
 * <li>http://stackoverflow.com/questions/30076472/how-to-call-rpgiv-program-from-java-that-returns-more-than-one-record
 * <li>http://stanleyvong.blogspot.fr/
 * <li>https://www.ibm.com/support/knowledgecenter/ssw_i5_54/rzahh/dtxmp.htm
 * <li>http://stackoverflow.com/questions/33834028/call-rpg-program-from-java?rq=1
 * <li>http://stackoverflow.com/questions/30076472/how-to-call-rpgiv-program-from-java-that-returns-more-than-one-record
 * <li>http://stanleyvong.blogspot.fr
 * <li>http://java400-l.midrange.narkive.com/9M93J1MS/using-an-as400structure-as-a-program-parameter
 * <li>http://webcache.googleusercontent.com/search?q=cache:http://permalink.gmane.org/gmane.comp.lang.as400.java/12403
 * </ul>
 */
public class ConvertDDS4Services {
  // Constantes
  private static final String DOSSIER_DESTINATION = "ConversionServiceRPG";
  
  // Variables
  private SystemeManager system = null;
  
  // 'j' : correspond au détail des champs en input
  // 'd' : correspond au détail des champs en output
  // 'e' n'est pas nécessaire car il est générique
  private char[] typeAvecDSj = { 'i', 'o', 'j' };
  private char[] typeAvecDSd = { 'i', 'o', 'd' };
  private char[] typeAvecDSjd = { 'i', 'o', 'd', 'j' };
  // classe ParametreGeneriqueErreur
  private char[] typeSansDS = { 'i', 'o' };
  private String nomMembreGenerique = null;
  private String nomBibliotheque = null;
  private String dossierDestination = null;
  private String cheminSource = null;
  private int tailleZoneRecouperj = -1;
  private int nombreLignesj = -1;
  private int tailleZoneRecouperd = -1;
  private int nombreLignesd = -1;
  private boolean variablesPrivates = true;
  
  /**
   * Constructeur.
   */
  public ConvertDDS4Services(boolean pVariablesPrivate) {
    variablesPrivates = pVariablesPrivate;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialise les données.
   */
  public void init(String pHost, String pRadicalMembre, String pNomBib, int[] pDetailInput, int[] pDetailOutput) {
    nomMembreGenerique = pRadicalMembre.trim().toUpperCase() + "?";
    nomBibliotheque = pNomBib.trim().toUpperCase();
    dossierDestination = System.getProperty("user.home") + File.separatorChar + "Desktop" + File.separatorChar + DOSSIER_DESTINATION;
    // Détail pour le type 'j'
    if (pDetailInput != null) {
      tailleZoneRecouperj = pDetailInput[0];
      nombreLignesj = pDetailInput[1];
    }
    // Détail pour le type 'd'
    if (pDetailOutput != null) {
      tailleZoneRecouperd = pDetailOutput[0];
      nombreLignesd = pDetailOutput[1];
    }
    
    system = new SystemeManager(pHost, "SERIEMAD", "GAR1972", true);
  }
  
  /**
   * Analyse le membre et génére le mappage des paramètres.
   */
  public String analyze(char pType, ArrayList<String> pSource) {
    boolean typedExiste = (tailleZoneRecouperd > 0);
    boolean typejExiste = (tailleZoneRecouperj > 0);
    
    String nomClasse = null;
    String nomMembre = nomMembreGenerique.replace('?', pType);
    cheminSource = "/QSYS.LIB/" + nomBibliotheque + ".LIB/" + nomMembre + ".FILE/" + nomMembre + ".MBR";
    AS400FileRecordDescription frd = new AS400FileRecordDescription(system.getSystem(), cheminSource);
    
    ArrayList<String> sourceTmp = new ArrayList<String>();
    
    try {
      RecordFormat[] rf = frd.retrieveRecordFormat();
      if (rf.length < 1) {
        return null;
      }
      
      // Ajout de l'entête
      pSource.add(formaterBlocCommentaire(0, 0, "Copyright (C) Résolution Informatique - Tout droits réservés.",
          "Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites."));
      
      // Ajout des imports
      pSource.add("import java.beans.PropertyVetoException;");
      pSource.add("import java.math.BigDecimal;");
      pSource.add("import java.util.Date;");
      pSource.add("import com.ibm.as400.access.AS400DataType;");
      if (pType == 'i' || pType == 'o') {
        pSource.add("import com.ibm.as400.access.AS400Structure;");
        pSource.add("import ri.serien.libcommun.outils.Trace;");
      }
      pSource.add("import com.ibm.as400.access.AS400PackedDecimal;");
      pSource.add("import com.ibm.as400.access.AS400Text;");
      pSource.add("import com.ibm.as400.access.AS400ZonedDecimal;");
      pSource.add("import com.ibm.as400.access.ProgramParameter;\n");
      pSource.add("import ri.serien.libcommun.outils.dateheure.ConvertDate;");
      pSource.add("import ri.serien.libcommun.outils.MessageErreurException;\n");
      
      nomClasse = frd.getFileName().substring(0, 1).toUpperCase() + frd.getFileName().substring(1).toLowerCase();
      if (pType == 'd' || pType == 'j') {
        pSource.add("public class " + nomClasse + " {");
      }
      else {
        pSource.add("public class " + nomClasse + " extends ProgramParameter {");
      }
      
      // Ajout des constantes des tailles des variables (pour les datastructures )
      int longueurDS = 0;
      String[] nomVariables = new String[rf[0].getFieldDescriptions().length];
      int[] tailleVariables = new int[rf[0].getFieldDescriptions().length];
      int[] nombreDecimales = new int[rf[0].getFieldDescriptions().length];
      longueurDS = ajouterConstantesTailleVar(pSource, rf, nomVariables, tailleVariables, nombreDecimales);
      pSource.add(formaterLigneInstruction(0, 1, "public static final int SIZE_TOTALE_DS\t= " + longueurDS + ";"));
      // Dans le cas de la classe d ou j on calcule le filler pour avoir la même longueur des zones (sinon il y a un
      // décalage dans les output/input)
      if (pType == 'd') {
        pSource
            .add(formaterLigneInstruction(0, 1, "public static final int SIZE_FILLER\t= " + tailleZoneRecouperd + "-" + longueurDS + ";"));
      }
      else if (pType == 'j') {
        pSource
            .add(formaterLigneInstruction(0, 1, "public static final int SIZE_FILLER\t= " + tailleZoneRecouperj + "-" + longueurDS + ";"));
      }
      
      // Ajout des constantes des indices (pour les datastructures )
      ajouterConstantesIndicesVar(sourceTmp, nomVariables);
      pSource.addAll(sourceTmp);
      
      // Ajout des variables
      String[] typeVariables;
      if (pType == 'o' && typedExiste) {
        String nom = nomClasse.substring(0, nomClasse.length() - 1) + 'd';
        ajouterVariablesDetail(pSource, nom, nombreLignesd);
        typeVariables = ajouterVariables(pSource, sourceTmp, rf, tailleZoneRecouperd, -1, pType);
      }
      else if (pType == 'i' && typejExiste) {
        String nom = nomClasse.substring(0, nomClasse.length() - 1) + 'j';
        ajouterVariablesDetail(pSource, nom, nombreLignesj);
        typeVariables = ajouterVariables(pSource, sourceTmp, rf, tailleZoneRecouperj, -1, pType);
      }
      else if (pType == 'd') {
        typeVariables = ajouterVariables(pSource, sourceTmp, rf, tailleZoneRecouperd, tailleZoneRecouperd - longueurDS, pType);
      }
      else if (pType == 'j') {
        typeVariables = ajouterVariables(pSource, sourceTmp, rf, tailleZoneRecouperj, tailleZoneRecouperj - longueurDS, pType);
      }
      else {
        typeVariables = ajouterVariables(pSource, sourceTmp, rf, -1, -1, pType);
      }
      
      pSource.addAll(sourceTmp);
      sourceTmp.clear();
      
      // Préparation de la liste des variables
      StringBuilder sb = new StringBuilder(10 * nomVariables.length);
      for (String var : nomVariables) {
        sb.append(var.toLowerCase()).append(", ");
      }
      
      // Classe j
      if (pType == 'j') {
        if ((tailleZoneRecouperj - longueurDS) > 0) {
          sb.append("filler").append(", ");
        }
        // pSource.add(formaterLigneInstruction(0, 1, "public Object[] o = {" + sb.toString() + "};"));
      }
      // Classe d
      else if (pType == 'd') {
        if ((tailleZoneRecouperd - longueurDS) > 0) {
          sb.append("filler").append(", ");
        }
        pSource.add(formaterLigneInstruction(0, 1, "public Object[] o = {" + sb.toString() + "};"));
      }
      else {
        // Ajoute la méthode DSInput
        if (pType == 'i' && typejExiste) {
          ajouterMethodeDSInputAvecDetail(pSource, sb.toString(), nomVariables, typeVariables);
        }
        else {
          ajouterMethodeDSInput(pSource, sb.toString());
        }
        
        // Ajoute la méthode DSOutput
        if (pType == 'o' && typedExiste) {
          ajouterMethodeDSOutputAvecDetail(pSource, nomVariables, typeVariables);
          ajouterMethodeListeObject(pSource, nomVariables, tailleVariables, tailleZoneRecouperd);
          ajouterMethodeConvertToObject(pSource);
          ajouterMethodeControlerProblemeConversionElement(pSource);
        }
        else {
          ajouterMethodeDSOutput(pSource, nomVariables, typeVariables);
        }
      }
      
      // Ajoute les accesseurs
      pSource.add(formaterLigneCommentaire(1, 1, "-- Accesseurs"));
      if (pType == 'j') {
        // Ajout de la méthode pour les tableaux en input
        pSource.add(formaterLigneInstruction(1, 1, "public Object[] getStructureObject() {"));
        pSource.add(formaterLigneInstruction(0, 2, "Object[] o = {" + sb.toString() + "};"));
        pSource.add(formaterLigneInstruction(0, 2, "return o;"));
        pSource.add(formaterLigneInstruction(0, 1, "}"));
      }
      for (int i = 0; i < nomVariables.length; i++) {
        // Dans le cas où le type de la varible est Object on ne créé par les accesseurs car il s'agit d'une variable
        // particulière
        if (typeVariables[i].startsWith("Object")) {
          continue;
        }
        ajouterAccesseurSet(pSource, nomVariables[i], typeVariables[i], tailleVariables[i], nombreDecimales[i]);
        ajouterAccesseurGet(pSource, nomVariables[i], typeVariables[i], tailleVariables[i], nombreDecimales[i]);
      }
      
      pSource.add("}");
    }
    catch (Exception e) {
      JOptionPane.showMessageDialog(null, e.getMessage(), "Erreur", JOptionPane.ERROR_MESSAGE);
      return null;
    }
    return nomClasse;
  }
  
  /**
   * Effectue le traitement sur tous les paramètres.
   */
  public void treatment() {
    // Création du dossier sur le bureau
    File dst = new File(dossierDestination);
    dst.mkdirs();
    
    // Analyse des DDS des paramètres et génération des classes java
    ArrayList<String> source = new ArrayList<String>();
    char[] type = null;
    // Détermination de quel type de traitement il faut faire (quels sont les détails à générer ?)
    if (tailleZoneRecouperj > -1 && tailleZoneRecouperd == -1) {
      type = typeAvecDSj;
    }
    else if (tailleZoneRecouperj == -1 && tailleZoneRecouperd > -1) {
      type = typeAvecDSd;
    }
    else if (tailleZoneRecouperd > -1 && tailleZoneRecouperj > -1) {
      type = typeAvecDSjd;
    }
    else {
      type = typeSansDS;
    }
    for (char untype : type) {
      source.clear();
      String nomClasse = analyze(untype, source);
      GestionFichierTexte src = new GestionFichierTexte(dossierDestination + File.separator + nomClasse + ".java");
      src.setContenuFichier(source);
      src.ecritureFichier();
    }
    
    system.deconnecter();
  }
  
  // -- Méthodes privées
  
  /**
   * Permet de mettre la première lettre d'un mmot en majuscule.
   */
  private String capitalize(String pMot) {
    if (pMot == null || pMot.isEmpty()) {
      return pMot;
    }
    return pMot.substring(0, 1).toUpperCase() + pMot.substring(1).toLowerCase();
  }
  
  /**
   * Prépare la chaine qui contient le nombre de saut de ligne précédent la ligne.
   */
  private String preparerSautLigne(StringBuilder pStringBuilder, int pNombreSaut) {
    if (pStringBuilder == null) {
      pStringBuilder = new StringBuilder();
    }
    pStringBuilder.setLength(0);
    // On prépare la chaine d'indentation
    for (int i = 0; i < pNombreSaut; i++) {
      pStringBuilder.append('\n');
    }
    return pStringBuilder.toString();
  }
  
  /**
   * Prépare la chaine qui contient le nombre d'indentation nécessaire pour une ligne.
   */
  private String preparerIndentation(StringBuilder pStringBuilder, int pNombreIndentation) {
    if (pStringBuilder == null) {
      pStringBuilder = new StringBuilder();
    }
    pStringBuilder.setLength(0);
    // On prépare la chaine d'indentation
    for (int i = 0; i < pNombreIndentation; i++) {
      pStringBuilder.append('\t');
    }
    return pStringBuilder.toString();
  }
  
  /**
   * Formate une chaine de caractère contenant un bloc de commentaire.
   */
  private String formaterBlocCommentaire(int pNombreSaut, int pNombreIndentation, String... pCommentaire) {
    StringBuilder bloc = new StringBuilder();
    String sautLigne = preparerSautLigne(bloc, pNombreSaut);
    String indentation = preparerIndentation(bloc, pNombreIndentation);
    
    // Préparation du bloc commentaire
    bloc.setLength(0);
    bloc.append(sautLigne);
    bloc.append(indentation).append("/*").append('\n');
    for (String commentaire : pCommentaire) {
      bloc.append(indentation).append(" * ").append(Constantes.normerTexte(commentaire)).append('\n');
    }
    bloc.append(indentation).append(" */");
    
    return bloc.toString();
  }
  
  /**
   * Formate une chaine de caractère contenant une ligne de commentaire.
   */
  private String formaterLigneCommentaire(int pNombreSaut, int pNombreIndentation, String pCommentaire) {
    StringBuilder ligne = new StringBuilder();
    String sautLigne = preparerSautLigne(ligne, pNombreSaut);
    String indentation = preparerIndentation(ligne, pNombreIndentation);
    
    // Préparation de la ligne commentaire
    ligne.setLength(0);
    ligne.append(sautLigne);
    ligne.append(indentation).append("// ").append(Constantes.normerTexte(pCommentaire));
    return ligne.toString();
  }
  
  /**
   * Formate une chaine de caractère contenant une ligne d'instruction.
   */
  private String formaterLigneInstruction(int pNombreSaut, int pNombreIndentation, String pInstruction) {
    StringBuilder ligne = new StringBuilder();
    String sautLigne = preparerSautLigne(ligne, pNombreSaut);
    String indentation = preparerIndentation(ligne, pNombreIndentation);
    
    // Préparation de la ligne d'instruction
    ligne.setLength(0);
    ligne.append(sautLigne);
    ligne.append(indentation).append(Constantes.normerTexte(pInstruction));
    return ligne.toString();
  }
  
  /**
   * Ajoute les constantes des tailles des variables dans la datastructure.
   */
  private int ajouterConstantesTailleVar(ArrayList<String> src, RecordFormat[] rf, String[] nomVariables, int[] tailleVariables,
      int[] nombreDecimales) {
    src.add(formaterLigneCommentaire(0, 1, "Constantes"));
    FieldDescription[] lfd = rf[0].getFieldDescriptions();
    int index = 0;
    int longueurds = 0;
    for (FieldDescription fd : lfd) {
      String variable = fd.getDDSName();
      tailleVariables[index] = fd.getLength();
      nombreDecimales[index] = -1;
      nomVariables[index] = variable;
      longueurds += fd.getLength();
      src.add(formaterLigneInstruction(0, 1, "public static final int SIZE_" + variable + "\t\t= " + fd.getLength() + ";"));
      if (fd instanceof PackedDecimalFieldDescription) {
        nombreDecimales[index] = ((PackedDecimalFieldDescription) fd).getDecimalPositions();
        src.add(formaterLigneInstruction(0, 1,
            "public static final int DECIMAL_" + variable + "\t= " + ((PackedDecimalFieldDescription) fd).getDecimalPositions() + ";"));
      }
      else if (fd instanceof ZonedDecimalFieldDescription) {
        nombreDecimales[index] = ((ZonedDecimalFieldDescription) fd).getDecimalPositions();
        src.add(formaterLigneInstruction(0, 1,
            "public static final int DECIMAL_" + variable + "\t= " + ((ZonedDecimalFieldDescription) fd).getDecimalPositions() + ";"));
      }
      index++;
    }
    return longueurds;
  }
  
  /**
   * Ajoute les constantes des indices des variables dans la datastructure.
   */
  private void ajouterConstantesIndicesVar(ArrayList<String> src, String[] lstvar) {
    src.clear();
    src.add(formaterLigneCommentaire(1, 1, "Constantes indices Nom DS"));
    for (int i = 0; i < lstvar.length; i++) {
      src.add(formaterLigneInstruction(0, 1, "public static final int\tVAR_" + lstvar[i] + "\t\t= " + i + ";"));
    }
  }
  
  /**
   * Ajoute les variables.
   */
  private String[] ajouterVariables(ArrayList<String> pSource, ArrayList<String> srctmp, RecordFormat[] pRf, int pTailleZone,
      int pTailleFiller, char pType) {
    srctmp.clear();
    FieldDescription[] lfd = pRf[0].getFieldDescriptions();
    String initAS = null;
    String typeJV = null;
    String initJV = null;
    boolean isAlpha = false;
    String[] typeVariables = new String[lfd.length];
    int index = 0;
    int ids = 0;
    pSource.add(formaterLigneCommentaire(1, 1, "Variables AS400"));
    for (FieldDescription fd : lfd) {
      if (fd instanceof ZonedDecimalFieldDescription) {
        initAS = "new AS400ZonedDecimal(SIZE_" + fd.getDDSName() + ", DECIMAL_" + fd.getDDSName() + ")";
        typeJV = "BigDecimal\t";
        initJV = "BigDecimal.ZERO";
        isAlpha = false;
        typeVariables[index] = "BigDecimal";
      }
      else if (fd instanceof CharacterFieldDescription) {
        initAS = "new AS400Text(SIZE_" + fd.getDDSName() + ")";
        typeJV = "String\t\t";
        if (fd.getDDSName().equals("PIARR") || fd.getDDSName().equals("POARR") || fd.getDDSName().equals("PEARR")) {
          initJV = "\"X\"";
          
        }
        else {
          initJV = "\"\"";
        }
        isAlpha = true;
        typeVariables[index] = "String";
      }
      else if (fd instanceof PackedDecimalFieldDescription) {
        initAS = "new AS400PackedDecimal(SIZE_" + fd.getDDSName() + ", DECIMAL_" + fd.getDDSName() + ")";
        typeJV = "BigDecimal\t";
        initJV = "BigDecimal.ZERO";
        isAlpha = false;
        typeVariables[index] = "BigDecimal";
      }
      if (fd.getLength() == pTailleZone) {
        initAS = "new AS400Structure(dsd[" + ids + "].structure)";
        typeJV = "Object[]\t";
        initJV = "dsd[" + ids + "].o";
        isAlpha = false;
        typeVariables[index] = "Object[]";
        ids++;
      }
      index++;
      
      // Ajout des ordres pour la déclaration des variables AS400 avec le type Java (String, BigDecimal, ...)
      if (variablesPrivates) {
        if (pType == 'i' && typeJV != null && typeJV.trim().equals("Object[]")) {
          initJV = "null";
        }
        pSource.add(formaterLigneInstruction(0, 1,
            "private " + typeJV + "" + fd.getDDSName().toLowerCase() + "\t= " + initJV + ";\t\t// " + fd.getTEXT()));
      }
      else {
        pSource.add(formaterLigneInstruction(0, 1,
            "public " + typeJV + "" + fd.getDDSName().toLowerCase() + "\t= " + initJV + ";\t\t// " + fd.getTEXT()));
      }
      // Ajout des ordres pour la datastructure
      srctmp.add(formaterLigneInstruction(0, 2, initAS + (isAlpha ? ",\t\t\t\t\t\t\t\t// " : ",\t\t// ") + fd.getTEXT()));
    }
    // Ajout du filler si besoin dans les variables AS400
    if (pTailleFiller > 0) {
      pSource.add(formaterLigneInstruction(0, 1, "private String\t\tfiller\t\t= \"\";"));
    }
    
    // Création de la déclaration de la datastructure
    srctmp.add(0, formaterLigneInstruction(0, 1, "public AS400DataType[] structure = {"));
    if (pTailleFiller > 0) {
      srctmp.add(formaterLigneInstruction(0, 2, "new AS400Text(SIZE_FILLER),\t\t// Filler"));
    }
    srctmp.add(formaterLigneInstruction(0, 1, "};"));
    if (pType == 'i' || pType == 'o') {
      srctmp.add(formaterLigneInstruction(0, 1, "private AS400Structure ds = new AS400Structure(structure);"));
    }
    srctmp.add(0, formaterLigneCommentaire(1, 1, "Création de la datastructure"));
    
    return typeVariables;
  }
  
  private void ajouterVariablesDetail(ArrayList<String> src, String nomclassed, int nbrlignes) {
    src.add(formaterLigneInstruction(1, 1, "public " + nomclassed + "[] dsd = {"));
    for (int i = 0; i < nbrlignes; i++) {
      src.add(formaterLigneInstruction(0, 2, "new " + nomclassed + "(),"));
    }
    src.add(formaterLigneInstruction(0, 1, "};"));
  }
  
  /**
   * Création de la méthode setDSInput pour les classes i et o.
   */
  private void ajouterMethodeDSInput(ArrayList<String> src, String listevars) {
    // Ajout de la méthode qui initialise les variables avant l'appel du RPG
    src.add(formaterBlocCommentaire(1, 1, "Initialise la datastructure avec les variables métiers."));
    src.add(formaterLigneInstruction(0, 1, "public void setDSInput() {"));
    src.add(formaterLigneInstruction(0, 2, "try {"));
    src.add(formaterLigneInstruction(0, 3, ""));
    src.add(formaterLigneInstruction(0, 3, "Object[] o = {" + listevars + "};"));
    src.add(formaterLigneInstruction(0, 3, "setInputData(ds.toBytes(o));"));
    src.add(formaterLigneInstruction(0, 3, "setOutputDataLength(SIZE_TOTALE_DS);"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, "catch (PropertyVetoException e) {"));
    src.add(formaterLigneInstruction(0, 3,
        "throw new MessageErreurException(e, \"Impossible d'échanger les paramètres avec le programme RPG.\");"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, "catch (IllegalArgumentException e) {"));
    src.add(formaterLigneInstruction(0, 3,
        "throw new MessageErreurException(e, \"Impossible d'échanger les paramètres avec le programme RPG.\");"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de la méthode setDSOutput pour la classe i.
   */
  private void ajouterMethodeDSOutput(ArrayList<String> src, String[] nomVariables, String[] typeVariables) {
    StringBuilder sb = new StringBuilder(10 * nomVariables.length);
    
    // Préparation de la liste des variables pour le retour
    for (int i = 0; i < nomVariables.length; i++) {
      sb.append("\t\t").append(nomVariables[i].toLowerCase()).append("\t\t").append("= (").append(typeVariables[i]).append(") output[")
          .append(i).append("];\n");
    }
    
    // Ajout de la méthode qui les variables après l'appel du RPG
    src.add(formaterBlocCommentaire(1, 1, "Initialise la datastructure avec les variables AS400."));
    src.add(formaterLigneInstruction(0, 1, "public void setDSOutput() {"));
    src.add(formaterLigneInstruction(0, 2, "Object[] output = (Object[]) ds.toObject(getOutputData());"));
    src.add(formaterLigneInstruction(0, 2, sb.toString()));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de la méthode setDSInput pour la classe i lorsque une classe j existe.
   */
  private void ajouterMethodeDSInputAvecDetail(ArrayList<String> src, String listevars, String[] nomVariables, String[] typeVariables) {
    // Ajout de la méthode qui initialise les variables avant l'appel du RPG
    src.add(formaterBlocCommentaire(1, 1, "Initialise la datastructure avec les variables métiers."));
    src.add(formaterLigneInstruction(0, 1, "public void setDSInput() {"));
    src.add(formaterLigneInstruction(0, 2, "try {"));
    int indice = 0;
    for (int i = 0; i < nomVariables.length; i++) {
      if (typeVariables[i].equals("Object[]")) {
        src.add(formaterLigneInstruction(0, 3, nomVariables[i].toLowerCase() + " = dsd[" + indice + "].getStructureObject();"));
        indice++;
      }
    }
    src.add(formaterLigneInstruction(0, 3, "Object[] o = {" + listevars + "};"));
    src.add(formaterLigneInstruction(0, 3, "setInputData(ds.toBytes(o));"));
    src.add(formaterLigneInstruction(0, 3, "setOutputDataLength(SIZE_TOTALE_DS);"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, "catch (PropertyVetoException e) {"));
    src.add(formaterLigneInstruction(0, 3,
        "throw new MessageErreurException(e, \"Impossible d'échanger les paramètres avec le programme RPG.\");"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, "catch (IllegalArgumentException e) {"));
    src.add(formaterLigneInstruction(0, 3,
        "throw new MessageErreurException(e, \"Impossible d'échanger les paramètres avec le programme RPG.\");"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 1, "}"));
    
  }
  
  /**
   * Création de la méthode setDSOutput pour la classe o lorsque une classe d existe.
   */
  private void ajouterMethodeDSOutputAvecDetail(ArrayList<String> src, String[] nomVariables, String[] typeVariables) {
    // Ajout de la méthode qui les variables après l'appel du RPG
    src.add(formaterBlocCommentaire(1, 1, "Initialise la datastructure avec les variables AS400 et leur redécoupage."));
    src.add(formaterLigneInstruction(0, 1, "public void setDSOutput() {"));
    src.add(formaterLigneInstruction(0, 2, "byte[] as400out = getOutputData();"));
    src.add(formaterLigneInstruction(0, 2, "int offset = 0;\n"));
    for (int i = 0; i < nomVariables.length; i++) {
      if (typeVariables[i].equals("Object[]")) {
        src.add(formaterLigneInstruction(0, 2, nomVariables[i].toLowerCase() + " = convertTObject(as400out, offset, " + i + ");"));
      }
      else {
        src.add(formaterLigneInstruction(0, 2,
            nomVariables[i].toLowerCase() + " = (" + typeVariables[i] + ") ds.getMembers(" + i + ").toObject(as400out, offset);"));
      }
      src.add(formaterLigneInstruction(0, 2, "offset += SIZE_" + nomVariables[i] + ";"));
    }
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de la méthode convertTObject pour la classe o.
   */
  private void ajouterMethodeConvertToObject(ArrayList<String> src) {
    src.add(formaterBlocCommentaire(1, 1, "Convertit les valeurs de la datastructure."));
    src.add(formaterLigneInstruction(0, 1, "private Object[] convertTObject(byte[] out, int offset, int indice){"));
    src.add(formaterLigneInstruction(0, 2, "try {"));
    src.add(formaterLigneInstruction(0, 3, "return (Object[]) ds.getMembers(indice).toObject(out, offset);"));
    src.add(formaterLigneInstruction(0, 2, "} catch(NumberFormatException e) {"));
    src.add(formaterLigneInstruction(0, 3,
        "Trace.debug(\"Erreur lors de la conversion du tableau fournit par le programme RPG.\" + e.getMessage());"));
    src.add(formaterLigneInstruction(0, 3, "return null;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de la méthode controlerProblemeConversionElement pour la classe o.
   */
  private void ajouterMethodeControlerProblemeConversionElement(ArrayList<String> src) {
    src.add(formaterBlocCommentaire(1, 1,
        "Permet de contrôler qu'un élément du tableau ne contient pas une erreur de données décimale lors de la conversion RPG -> Java."));
    src.add(formaterLigneInstruction(0, 1, "private void controlerProblemeConversionElement(Object[] pTableau) {"));
    src.add(formaterLigneInstruction(0, 2, "if (pTableau == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, "for (int i = 0; i < pTableau.length; i++) {"));
    src.add(formaterLigneInstruction(0, 3, "int j = i + 1;"));
    src.add(formaterLigneInstruction(0, 3, "if (j < pTableau.length && pTableau[i] == null && pTableau[j] != null) {"));
    src.add(formaterLigneInstruction(0, 4,
        "throw new MessageErreurException(\"Il y a eu un problème lors de la conversion (RPG -> Java) des données à l'indice \" + i +\" du tableau. \");"));
    src.add(formaterLigneInstruction(0, 3, "}"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de la méthode getValeursBrutesLignes pour la classe o.
   */
  private void ajouterMethodeListeObject(ArrayList<String> src, String[] nomVariables, int[] tailleVariables, int taillezoneadecouper) {
    StringBuilder sb = new StringBuilder(10 * nomVariables.length);
    for (int i = 0; i < nomVariables.length; i++) {
      if (tailleVariables[i] == taillezoneadecouper) {
        sb.append(nomVariables[i].toLowerCase()).append(", ");
      }
    }
    
    src.add(formaterBlocCommentaire(1, 1, "Retourne un tableau des valeurs correspondants aux lignes."));
    src.add(formaterLigneInstruction(0, 1, "public Object[] getValeursBrutesLignes() {"));
    src.add(formaterLigneInstruction(0, 2, "Object[] o = { " + sb.toString() + " };"));
    src.add(formaterLigneInstruction(0, 2, "controlerProblemeConversionElement(o);"));
    src.add(formaterLigneInstruction(0, 2, "return o;"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur set pour une variable donnée.
   */
  private void ajouterAccesseurSet(ArrayList<String> src, String nomVariable, String typeVariable, int taille, int decimale) {
    nomVariable = Constantes.normerTexte(nomVariable);
    typeVariable = Constantes.normerTexte(typeVariable);
    
    // Les variables de type String
    if (typeVariable.equals("String")) {
      // Si la variable est une String avec une longueur de 1 alors on créé un accesseur particulier
      if (taille == 1) {
        creerAccesseurSetTypeCharacter(src, nomVariable, typeVariable, taille);
      }
      else {
        creerAccesseurSetTypeString(src, nomVariable, typeVariable, taille);
      }
    }
    else {
      // Les variables de type BigDecimal
      if (typeVariable.equals("BigDecimal")) {
        creerAccesseurSetTypeBigDecimal(src, nomVariable, typeVariable, taille);
        // S'il n'y a pas de décimale alors il s'agit d'un Integer
        if (decimale == 0) {
          creerAccesseurSetTypeInteger(src, nomVariable, typeVariable, taille);
          // Cas particluiers: S'il n'y a pas de décimale et que sa taille vaut 7 alors il s'agit peut être d'une date
          if (taille == 7) {
            creerAccesseurSetTypeDate(src, nomVariable, typeVariable, taille);
          }
        }
        else {
          creerAccesseurSetTypeDouble(src, nomVariable, typeVariable, taille);
        }
      }
      else {
        // Les autres type (s'il y en a) de variables n'ont pas de traitement particluliers
        creerAccesseurSet(src, nomVariable, typeVariable, taille);
      }
    }
  }
  
  /**
   * Création de l'accesseur get pour une variable donnée.
   */
  private void ajouterAccesseurGet(ArrayList<String> src, String nomVariable, String typeVariable, int taille, int decimale) {
    nomVariable = Constantes.normerTexte(nomVariable);
    typeVariable = Constantes.normerTexte(typeVariable);
    
    // Les variables de type String
    if (typeVariable.equals("String")) {
      // Si la variable est une String avec une longueur de 1 alors on créé un accesseur particulier
      if (taille == 1) {
        creerAccesseurGetTypeCharacter(src, nomVariable, typeVariable, taille);
      }
      else {
        creerAccesseurGetTypeString(src, nomVariable, typeVariable, taille);
      }
    }
    else {
      // Les variables de type BigDecimal
      if (typeVariable.equals("BigDecimal")) {
        // S'il n'y a pas de décimale alors il s'agit d'un Integer
        if (decimale == 0) {
          creerAccesseurGetTypeInteger(src, nomVariable, typeVariable, taille);
          // Cas particluiers: S'il n'y a pas de décimale et que sa taille vaut 7 alors il s'agit peut être d'une date
          if (taille == 7) {
            creerAccesseurGetTypeDate(src, nomVariable, typeVariable, taille);
          }
        }
        else {
          creerAccesseurGetTypeBigDecimal(src, nomVariable, typeVariable, taille);
        }
      }
      else {
        // Les autres type (s'il y en a) de variables n'ont pas de traitement particluliers
        creerAccesseurGet(src, nomVariable, typeVariable, taille);
      }
    }
  }
  
  /**
   * Création de l'accesseur de type Character pour une variable String.
   */
  private void creerAccesseurSetTypeCharacter(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "Character p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, nomVariable.toLowerCase() + " = String.valueOf(p" + capitalize(nomVariable) + ");"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type String pour une variable String.
   */
  private void creerAccesseurSetTypeString(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "String p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, nomVariable.toLowerCase() + " = p" + capitalize(nomVariable) + ";"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type BigDecimal pour une variable BigDecimal.
   */
  private void creerAccesseurSetTypeBigDecimal(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "BigDecimal p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2,
        nomVariable.toLowerCase() + " = p" + capitalize(nomVariable) + ".setScale(DECIMAL_" + nomVariable + ", RoundingMode.HALF_UP);"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Integer pour une variable BigDecimal.
   */
  private void creerAccesseurSetTypeInteger(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "Integer p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, nomVariable.toLowerCase() + " = BigDecimal.valueOf(p" + capitalize(nomVariable) + ");"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Date pour une variable BigDecimal.
   */
  private void creerAccesseurSetTypeDate(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "Date p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2,
        nomVariable.toLowerCase() + " = BigDecimal.valueOf(ConvertDate.dateToDb2(p" + capitalize(nomVariable) + "));"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Double pour une variable BigDecimal.
   */
  private void creerAccesseurSetTypeDouble(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = "Double p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, nomVariable.toLowerCase() + " = BigDecimal.valueOf(p" + capitalize(nomVariable)
        + ").setScale(DECIMAL_" + nomVariable + ", RoundingMode.HALF_UP);"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur avec le type de la variable.
   */
  private void creerAccesseurSet(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    String parametre = typeVariable + " p" + capitalize(nomVariable);
    src.add(formaterLigneInstruction(1, 1, "public void set" + capitalize(nomVariable) + "(" + parametre + ") {"));
    src.add(formaterLigneInstruction(0, 2, "if (p" + capitalize(nomVariable) + " == null) {"));
    src.add(formaterLigneInstruction(0, 3, "return;"));
    src.add(formaterLigneInstruction(0, 2, "}"));
    src.add(formaterLigneInstruction(0, 2, nomVariable.toLowerCase() + " = p" + capitalize(nomVariable) + ";"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Character pour une variable String.
   */
  private void creerAccesseurGetTypeCharacter(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public Character get" + capitalize(nomVariable) + "() {"));
    src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ".charAt(0);"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type String pour une variable String.
   */
  private void creerAccesseurGetTypeString(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public String get" + capitalize(nomVariable) + "() {"));
    src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ";"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type BigDecimal pour une variable BigDecimal.
   */
  private void creerAccesseurGetTypeBigDecimal(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public BigDecimal get" + capitalize(nomVariable) + "() {"));
    // src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ";"));
    src.add(formaterLigneInstruction(0, 2,
        "return " + nomVariable.toLowerCase() + ".setScale(DECIMAL_" + nomVariable + ", RoundingMode.HALF_UP);"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Integer pour une variable BigDecimal.
   */
  private void creerAccesseurGetTypeInteger(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public Integer get" + capitalize(nomVariable) + "() {"));
    src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ".intValue();"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Date pour une variable BigDecimal.
   */
  private void creerAccesseurGetTypeDate(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public Date get" + capitalize(nomVariable) + "ConvertiEnDate() {"));
    src.add(formaterLigneInstruction(0, 2, "return ConvertDate.db2ToDate(" + nomVariable.toLowerCase() + ".intValue(), null);"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur de type Double pour une variable BigDecimal.
   */
  private void creerAccesseurGetTypeDouble(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public Double get" + capitalize(nomVariable) + "() {"));
    src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ".doubleValue();"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  /**
   * Création de l'accesseur avec le type de la variable.
   */
  private void creerAccesseurGet(ArrayList<String> src, String nomVariable, String typeVariable, int taille) {
    src.add(formaterLigneInstruction(1, 1, "public " + typeVariable + " get" + capitalize(nomVariable) + "() {"));
    src.add(formaterLigneInstruction(0, 2, "return " + nomVariable.toLowerCase() + ";"));
    src.add(formaterLigneInstruction(0, 1, "}"));
  }
  
  // -- Méthode principale
  
  /**
   * Démarrage du programme.
   */
  public static void main(String[] args) {
    Trace.info("Lancement de la conversion...");
    ConvertDDS4Services dds4service = new ConvertDDS4Services(true);
    
    // Exemple paramètre avec DDS avec redécoupage (classe [i,j], [o, d})
    // 172.31.0.200 -> Adresse IP AS400
    // SRGVM13LE -> Nom du programme RPG
    // sgvmas -> Nom de la bibliothèque source contenant les DDS (QDDSSRC)
    // Tableau pour input ou null
    // ??? -> Longueur des enregistrements de type tableau des valeurs du paramètre de sortie (QDDSRC/???O)
    // ?? -> Nombre de lignes du tableau du paramètre de sortie (QDDSRC/???O)
    // Tableau pour output ou null
    // ??? -> Longueur des enregistrements de type tableau des valeurs du paramètre de sortie (QDDSRC/???O)
    // ?? -> Nombre de lignes du tableau du paramètre de sortie (QDDSRC/???O)
    // dds4service.init("172.31.0.200", "SVGVM0059", "sgvmas", null, new int[] {400, 65});
    // dds4service.init("172.31.0.200", "SVGAM0008", "sgamas", null, new int[] { 400, 65 });
    // dds4service.init("172.31.0.200", "SVGVM0020", "sgvmas", null, new int[] {200, 15);
    // dds4service.init("172.31.0.200", "SVGVX0008", "sgvmx", null, new int[] {200, 15});
    // dds4service.init("172.31.0.200", "SVGVX0017", "sgvmx", null, new int[] { 500, 65 });
    // dds4service.init("172.31.0.200", "SVGVX0022", "sgvmx", null, new int[] {50, 15});
    // dds4service.init("172.31.0.200", "SVGAM0007 ", "sgamas", null, new int[] {700, 15});
    // dds4service.init("172.31.0.200", "SVGVM0014", "sgvmas", null, new int[] { 700, 15 });
    // dds4service.init("srp_dev", "SVGVM0008", "sgvmas", null, new int[] { 100, 18 });
    // dds4service.init("srp_dev", "SVGVM0042", "sgvmas", new int[] { 100, 36 }, null);
    // dds4service.init("srp_dev", "SVGVM0054", "sgvmas", null, new int[] { 700, 15 });
    // dds4service.init("172.31.0.200", "SVGVX0021", "sgvmx", null, new int[] { 250, 65 });
    
    // Exemple paramètre sans DDS sans redécoupage (classe i, o)
    // 172.31.0.200 -> Adresse IP AS400
    // SRGVM13LE -> Nom du programme RPG
    // sgvmas -> Nom de la bibliothèque source contenant les DDS (QDDSSRC)
    // null -> Pour les données en entrée - Pas de gestion de la description et pas de gestion des lignes
    // null -> Pour les données en sortie - Pas de gestion de la description et pas de gestion des lignes
    // dds4service.init("172.31.0.200", "SVGVM0004", "sgvmas", null, null);
    // dds4service.init("172.31.0.200", "SVGAM0006", "sgamas", null, null);
    // dds4service.init("172.31.0.200", "SVGVX0013", "sgvmx", null, null);
    // dds4service.init("srp_dev", "SVGVM0018", "sgvmas", null, null);
    // dds4service.init("srp_dev", "SVGVM0024", "sgvmas", null, null);
    // Service MODIFIER_LIGNE_VENTE
    dds4service.init("srp_dev", "SVGVM0027", "sgvmas", null, null);
    // dds4service.init("srp_dev", "SVGVM0028", "sgvmas", null, null);
    // dds4service.init("172.31.0.200", "SVGVX0020", "sgvmx", null, null);
    // dds4service.init("srp_dev", "SVGVM0044", "sgvmas", null, null);
    // dds4service.init("172.31.0.200", "SVGVX0009", "sgvmx", null, null);
    // dds4service.init("172.31.0.200", "SVGAM0004", "sgamas", null, null);
    // Service INITIALISER_LIGNES_ACHAT
    // dds4service.init("172.31.0.200", "SVGAM0003", "sgamas", null, null);
    // Service SAUVER_LOT
    // dds4service.init("172.31.0.200", "SVGVX0025", "sgvmx", null, null);
    
    // lancement commun du traitement
    dds4service.treatment();
    Trace.info("Conversion terminée.");
  }
  
}
