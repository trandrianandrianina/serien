/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package magento;

/*
*  MagentoServiceStub java implementation
*/

public class MagentoServiceStub extends org.apache.axis2.client.Stub
    implements MagentoService {
  protected org.apache.axis2.description.AxisOperation[] _operations;
  
  // hashmaps to keep the fault mapping
  private java.util.HashMap faultExceptionNameMap = new java.util.HashMap();
  private java.util.HashMap faultExceptionClassNameMap = new java.util.HashMap();
  private java.util.HashMap faultMessageMap = new java.util.HashMap();
  
  private static int counter = 0;
  
  private static synchronized java.lang.String getUniqueSuffix() {
    // reset the counter if it is greater than 99999
    if (counter > 99999) {
      counter = 0;
    }
    counter = counter + 1;
    return java.lang.Long.toString(java.lang.System.currentTimeMillis()) + "_" + counter;
  }
  
  private void populateAxisService() {
    
    // creating the Service with a unique name
    _service = new org.apache.axis2.description.AxisService("MagentoService" + getUniqueSuffix());
    addAnonymousOperations();
    
    // creating the operations
    org.apache.axis2.description.AxisOperation __operation;
    
    _operations = new org.apache.axis2.description.AxisOperation[3];
    
    __operation = new org.apache.axis2.description.OutInAxisOperation();
    
    __operation.setName(new javax.xml.namespace.QName("urn:Magento", "login"));
    _service.addOperation(__operation);
    
    _operations[0] = __operation;
    
    __operation = new org.apache.axis2.description.OutInAxisOperation();
    
    __operation.setName(new javax.xml.namespace.QName("urn:Magento", "endSession"));
    _service.addOperation(__operation);
    
    _operations[1] = __operation;
    
    __operation = new org.apache.axis2.description.OutInAxisOperation();
    
    __operation.setName(new javax.xml.namespace.QName("urn:Magento", "serieNReceive"));
    _service.addOperation(__operation);
    
    _operations[2] = __operation;
    
  }
  
  // populates the faults
  private void populateFaults() {
    
  }
  
  /**
   * Constructor that takes in a configContext
   */
  
  public MagentoServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
      java.lang.String targetEndpoint)
      throws org.apache.axis2.AxisFault {
    this(configurationContext, targetEndpoint, false);
  }
  
  /**
   * Constructor that takes in a configContext and useseperate listner
   */
  public MagentoServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext,
      java.lang.String targetEndpoint, boolean useSeparateListener)
      throws org.apache.axis2.AxisFault {
    // To populate AxisService
    populateAxisService();
    populateFaults();
    
    _serviceClient = new org.apache.axis2.client.ServiceClient(configurationContext, _service);
    
    _serviceClient.getOptions().setTo(new org.apache.axis2.addressing.EndpointReference(
        targetEndpoint));
    _serviceClient.getOptions().setUseSeparateListener(useSeparateListener);
    
  }
  
  /**
   * Default Constructor
   */
  public MagentoServiceStub(org.apache.axis2.context.ConfigurationContext configurationContext) throws org.apache.axis2.AxisFault {
    
    this(configurationContext, "http://admin.123elec.com/index.php/api/v2_soap/index/");
    
  }
  
  /**
   * Default Constructor
   */
  public MagentoServiceStub() throws org.apache.axis2.AxisFault {
    
    this("http://admin.123elec.com/index.php/api/v2_soap/index/");
    
  }
  
  /**
   * Constructor taking the target endpoint
   */
  public MagentoServiceStub(java.lang.String targetEndpoint) throws org.apache.axis2.AxisFault {
    this(null, targetEndpoint);
  }
  
  /**
   * Auto generated method signature
   * Login user and retrive session id
   * @see magento.MagentoService#login
   * @param login6
   * 
   */
  
  @Override
  public magento.LoginResponse login(
      
      magento.Login login6)
      
      throws java.rmi.RemoteException {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try {
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
      _operationClient.getOptions().setAction("urn:Action");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      
      addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
      
      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();
      
      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          login6,
          optimizeContent(new javax.xml.namespace.QName("urn:Magento",
              "login")),
          new javax.xml.namespace.QName("urn:Magento",
              "login"));
      
      // adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);
      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);
      
      // add the message contxt to the operation client
      _operationClient.addMessageContext(_messageContext);
      
      // execute the operation client
      _operationClient.execute(true);
      
      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
      
      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement(),
          magento.LoginResponse.class,
          getEnvelopeNamespaces(_returnEnv));
      
      return (magento.LoginResponse) object;
      
    }
    catch (org.apache.axis2.AxisFault f) {
      
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt != null) {
        if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"))) {
          // make the fault by reflection
          try {
            java.lang.String exceptionClassName =
                (java.lang.String) faultExceptionClassNameMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"));
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
            java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
            // message class
            java.lang.String messageClassName =
                (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"));
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[] { messageClass });
            m.invoke(ex, new java.lang.Object[] { messageObject });
            
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }
          catch (java.lang.ClassCastException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }
        else {
          throw f;
        }
      }
      else {
        throw f;
      }
    }
    finally {
      if (_messageContext != null && _messageContext.getTransportOut() != null) {
        _messageContext.getTransportOut().getSender().cleanup(_messageContext);
      }
    }
  }
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * Login user and retrive session id
   * @see magento.MagentoService#startlogin
   * @param login6
   * 
   */
  @Override
  public void startlogin(
      
      magento.Login login6,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException {
    
    org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[0].getName());
    _operationClient.getOptions().setAction("urn:Action");
    _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
    
    addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
    
    // create SOAP envelope with that payload
    org.apache.axiom.soap.SOAPEnvelope env = null;
    final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
    
    // Style is Doc.
    
    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
        login6,
        optimizeContent(new javax.xml.namespace.QName("urn:Magento",
            "login")),
        new javax.xml.namespace.QName("urn:Magento",
            "login"));
    
    // adding SOAP soap_headers
    _serviceClient.addHeadersToEnvelope(env);
    // create message context with that soap envelope
    _messageContext.setEnvelope(env);
    
    // add the message context to the operation client
    _operationClient.addMessageContext(_messageContext);
    
    _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
      @Override
      public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
        try {
          org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
          
          java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
              magento.LoginResponse.class,
              getEnvelopeNamespaces(resultEnv));
          callback.receiveResultlogin(
              (magento.LoginResponse) object);
          
        }
        catch (org.apache.axis2.AxisFault e) {
          callback.receiveErrorlogin(e);
        }
      }
      
      @Override
      public void onError(java.lang.Exception error) {
        if (error instanceof org.apache.axis2.AxisFault) {
          org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
          org.apache.axiom.om.OMElement faultElt = f.getDetail();
          if (faultElt != null) {
            if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"))) {
              // make the fault by reflection
              try {
                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                    .get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"));
                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
                // message class
                java.lang.String messageClassName =
                    (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "login"));
                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                    new java.lang.Class[] { messageClass });
                m.invoke(ex, new java.lang.Object[] { messageObject });
                
                callback.receiveErrorlogin(new java.rmi.RemoteException(ex.getMessage(), ex));
              }
              catch (java.lang.ClassCastException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (java.lang.ClassNotFoundException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (java.lang.NoSuchMethodException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (java.lang.reflect.InvocationTargetException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (java.lang.IllegalAccessException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (java.lang.InstantiationException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
              catch (org.apache.axis2.AxisFault e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorlogin(f);
              }
            }
            else {
              callback.receiveErrorlogin(f);
            }
          }
          else {
            callback.receiveErrorlogin(f);
          }
        }
        else {
          callback.receiveErrorlogin(error);
        }
      }
      
      @Override
      public void onFault(org.apache.axis2.context.MessageContext faultContext) {
        org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
        onError(fault);
      }
      
      @Override
      public void onComplete() {
        try {
          _messageContext.getTransportOut().getSender().cleanup(_messageContext);
        }
        catch (org.apache.axis2.AxisFault axisFault) {
          callback.receiveErrorlogin(axisFault);
        }
      }
    });
    
    org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
    if (_operations[0].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
      _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
      _operations[0].setMessageReceiver(
          _callbackReceiver);
    }
    
    // execute the operation client
    _operationClient.execute(false);
    
  }
  
  /**
   * Auto generated method signature
   * End web service session
   * @see magento.MagentoService#endSession
   * @param endSession8
   * 
   */
  
  @Override
  public magento.EndSessionResponse endSession(
      
      magento.EndSession endSession8)
      
      throws java.rmi.RemoteException {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try {
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
      _operationClient.getOptions().setAction("urn:Action");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      
      addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
      
      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();
      
      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          endSession8,
          optimizeContent(new javax.xml.namespace.QName("urn:Magento",
              "endSession")),
          new javax.xml.namespace.QName("urn:Magento",
              "endSession"));
      
      // adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);
      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);
      
      // add the message contxt to the operation client
      _operationClient.addMessageContext(_messageContext);
      
      // execute the operation client
      _operationClient.execute(true);
      
      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
      
      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement(),
          magento.EndSessionResponse.class,
          getEnvelopeNamespaces(_returnEnv));
      
      return (magento.EndSessionResponse) object;
      
    }
    catch (org.apache.axis2.AxisFault f) {
      
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt != null) {
        if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"))) {
          // make the fault by reflection
          try {
            java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                .get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"));
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
            java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
            // message class
            java.lang.String messageClassName =
                (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"));
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[] { messageClass });
            m.invoke(ex, new java.lang.Object[] { messageObject });
            
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }
          catch (java.lang.ClassCastException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }
        else {
          throw f;
        }
      }
      else {
        throw f;
      }
    }
    finally {
      if (_messageContext != null && _messageContext.getTransportOut() != null) {
        _messageContext.getTransportOut().getSender().cleanup(_messageContext);
      }
    }
  }
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * End web service session
   * @see magento.MagentoService#startendSession
   * @param endSession8
   * 
   */
  @Override
  public void startendSession(
      
      magento.EndSession endSession8,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException {
    
    org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[1].getName());
    _operationClient.getOptions().setAction("urn:Action");
    _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
    
    addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
    
    // create SOAP envelope with that payload
    org.apache.axiom.soap.SOAPEnvelope env = null;
    final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
    
    // Style is Doc.
    
    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
        endSession8,
        optimizeContent(new javax.xml.namespace.QName("urn:Magento",
            "endSession")),
        new javax.xml.namespace.QName("urn:Magento",
            "endSession"));
    
    // adding SOAP soap_headers
    _serviceClient.addHeadersToEnvelope(env);
    // create message context with that soap envelope
    _messageContext.setEnvelope(env);
    
    // add the message context to the operation client
    _operationClient.addMessageContext(_messageContext);
    
    _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
      @Override
      public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
        try {
          org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
          
          java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
              magento.EndSessionResponse.class,
              getEnvelopeNamespaces(resultEnv));
          callback.receiveResultendSession(
              (magento.EndSessionResponse) object);
          
        }
        catch (org.apache.axis2.AxisFault e) {
          callback.receiveErrorendSession(e);
        }
      }
      
      @Override
      public void onError(java.lang.Exception error) {
        if (error instanceof org.apache.axis2.AxisFault) {
          org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
          org.apache.axiom.om.OMElement faultElt = f.getDetail();
          if (faultElt != null) {
            if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"))) {
              // make the fault by reflection
              try {
                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                    .get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"));
                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
                // message class
                java.lang.String messageClassName =
                    (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "endSession"));
                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                    new java.lang.Class[] { messageClass });
                m.invoke(ex, new java.lang.Object[] { messageObject });
                
                callback.receiveErrorendSession(new java.rmi.RemoteException(ex.getMessage(), ex));
              }
              catch (java.lang.ClassCastException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (java.lang.ClassNotFoundException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (java.lang.NoSuchMethodException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (java.lang.reflect.InvocationTargetException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (java.lang.IllegalAccessException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (java.lang.InstantiationException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
              catch (org.apache.axis2.AxisFault e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorendSession(f);
              }
            }
            else {
              callback.receiveErrorendSession(f);
            }
          }
          else {
            callback.receiveErrorendSession(f);
          }
        }
        else {
          callback.receiveErrorendSession(error);
        }
      }
      
      @Override
      public void onFault(org.apache.axis2.context.MessageContext faultContext) {
        org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
        onError(fault);
      }
      
      @Override
      public void onComplete() {
        try {
          _messageContext.getTransportOut().getSender().cleanup(_messageContext);
        }
        catch (org.apache.axis2.AxisFault axisFault) {
          callback.receiveErrorendSession(axisFault);
        }
      }
    });
    
    org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
    if (_operations[1].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
      _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
      _operations[1].setMessageReceiver(
          _callbackReceiver);
    }
    
    // execute the operation client
    _operationClient.execute(false);
    
  }
  
  /**
   * Auto generated method signature
   * Reception Flux Serie N
   * @see magento.MagentoService#serieNReceive
   * @param serieNReceive10
   * 
   */
  
  @Override
  public magento.SerieNReceiveResponse serieNReceive(
      
      magento.SerieNReceive serieNReceive10)
      
      throws java.rmi.RemoteException {
    org.apache.axis2.context.MessageContext _messageContext = null;
    try {
      org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
      _operationClient.getOptions().setAction("urn:Action");
      _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
      
      addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
      
      // create a message context
      _messageContext = new org.apache.axis2.context.MessageContext();
      
      // create SOAP envelope with that payload
      org.apache.axiom.soap.SOAPEnvelope env = null;
      
      env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
          serieNReceive10,
          optimizeContent(new javax.xml.namespace.QName("urn:Magento",
              "serieNReceive")),
          new javax.xml.namespace.QName("urn:Magento",
              "serieNReceive"));
      
      // adding SOAP soap_headers
      _serviceClient.addHeadersToEnvelope(env);
      // set the message context with that soap envelope
      _messageContext.setEnvelope(env);
      
      // add the message contxt to the operation client
      _operationClient.addMessageContext(_messageContext);
      
      // execute the operation client
      _operationClient.execute(true);
      
      org.apache.axis2.context.MessageContext _returnMessageContext = _operationClient.getMessageContext(
          org.apache.axis2.wsdl.WSDLConstants.MESSAGE_LABEL_IN_VALUE);
      org.apache.axiom.soap.SOAPEnvelope _returnEnv = _returnMessageContext.getEnvelope();
      
      java.lang.Object object = fromOM(
          _returnEnv.getBody().getFirstElement(),
          magento.SerieNReceiveResponse.class,
          getEnvelopeNamespaces(_returnEnv));
      
      return (magento.SerieNReceiveResponse) object;
      
    }
    catch (org.apache.axis2.AxisFault f) {
      
      org.apache.axiom.om.OMElement faultElt = f.getDetail();
      if (faultElt != null) {
        if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"))) {
          // make the fault by reflection
          try {
            java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                .get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"));
            java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
            java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
            java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
            // message class
            java.lang.String messageClassName =
                (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"));
            java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
            java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
            java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                new java.lang.Class[] { messageClass });
            m.invoke(ex, new java.lang.Object[] { messageObject });
            
            throw new java.rmi.RemoteException(ex.getMessage(), ex);
          }
          catch (java.lang.ClassCastException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.ClassNotFoundException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.NoSuchMethodException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.reflect.InvocationTargetException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.IllegalAccessException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
          catch (java.lang.InstantiationException e) {
            // we cannot intantiate the class - throw the original Axis fault
            throw f;
          }
        }
        else {
          throw f;
        }
      }
      else {
        throw f;
      }
    }
    finally {
      if (_messageContext != null && _messageContext.getTransportOut() != null) {
        _messageContext.getTransportOut().getSender().cleanup(_messageContext);
      }
    }
  }
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * Reception Flux Serie N
   * @see magento.MagentoService#startserieNReceive
   * @param serieNReceive10
   * 
   */
  @Override
  public void startserieNReceive(
      
      magento.SerieNReceive serieNReceive10,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException {
    
    org.apache.axis2.client.OperationClient _operationClient = _serviceClient.createClient(_operations[2].getName());
    _operationClient.getOptions().setAction("urn:Action");
    _operationClient.getOptions().setExceptionToBeThrownOnSOAPFault(true);
    
    addPropertyToOperationClient(_operationClient, org.apache.axis2.description.WSDL2Constants.ATTR_WHTTP_QUERY_PARAMETER_SEPARATOR, "&");
    
    // create SOAP envelope with that payload
    org.apache.axiom.soap.SOAPEnvelope env = null;
    final org.apache.axis2.context.MessageContext _messageContext = new org.apache.axis2.context.MessageContext();
    
    // Style is Doc.
    
    env = toEnvelope(getFactory(_operationClient.getOptions().getSoapVersionURI()),
        serieNReceive10,
        optimizeContent(new javax.xml.namespace.QName("urn:Magento",
            "serieNReceive")),
        new javax.xml.namespace.QName("urn:Magento",
            "serieNReceive"));
    
    // adding SOAP soap_headers
    _serviceClient.addHeadersToEnvelope(env);
    // create message context with that soap envelope
    _messageContext.setEnvelope(env);
    
    // add the message context to the operation client
    _operationClient.addMessageContext(_messageContext);
    
    _operationClient.setCallback(new org.apache.axis2.client.async.AxisCallback() {
      @Override
      public void onMessage(org.apache.axis2.context.MessageContext resultContext) {
        try {
          org.apache.axiom.soap.SOAPEnvelope resultEnv = resultContext.getEnvelope();
          
          java.lang.Object object = fromOM(resultEnv.getBody().getFirstElement(),
              magento.SerieNReceiveResponse.class,
              getEnvelopeNamespaces(resultEnv));
          callback.receiveResultserieNReceive(
              (magento.SerieNReceiveResponse) object);
          
        }
        catch (org.apache.axis2.AxisFault e) {
          callback.receiveErrorserieNReceive(e);
        }
      }
      
      @Override
      public void onError(java.lang.Exception error) {
        if (error instanceof org.apache.axis2.AxisFault) {
          org.apache.axis2.AxisFault f = (org.apache.axis2.AxisFault) error;
          org.apache.axiom.om.OMElement faultElt = f.getDetail();
          if (faultElt != null) {
            if (faultExceptionNameMap.containsKey(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"))) {
              // make the fault by reflection
              try {
                java.lang.String exceptionClassName = (java.lang.String) faultExceptionClassNameMap
                    .get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"));
                java.lang.Class exceptionClass = java.lang.Class.forName(exceptionClassName);
                java.lang.reflect.Constructor constructor = exceptionClass.getConstructor(java.lang.String.class);
                java.lang.Exception ex = (java.lang.Exception) constructor.newInstance(f.getMessage());
                // message class
                java.lang.String messageClassName =
                    (java.lang.String) faultMessageMap.get(new org.apache.axis2.client.FaultMapKey(faultElt.getQName(), "serieNReceive"));
                java.lang.Class messageClass = java.lang.Class.forName(messageClassName);
                java.lang.Object messageObject = fromOM(faultElt, messageClass, null);
                java.lang.reflect.Method m = exceptionClass.getMethod("setFaultMessage",
                    new java.lang.Class[] { messageClass });
                m.invoke(ex, new java.lang.Object[] { messageObject });
                
                callback.receiveErrorserieNReceive(new java.rmi.RemoteException(ex.getMessage(), ex));
              }
              catch (java.lang.ClassCastException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (java.lang.ClassNotFoundException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (java.lang.NoSuchMethodException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (java.lang.reflect.InvocationTargetException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (java.lang.IllegalAccessException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (java.lang.InstantiationException e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
              catch (org.apache.axis2.AxisFault e) {
                // we cannot intantiate the class - throw the original Axis fault
                callback.receiveErrorserieNReceive(f);
              }
            }
            else {
              callback.receiveErrorserieNReceive(f);
            }
          }
          else {
            callback.receiveErrorserieNReceive(f);
          }
        }
        else {
          callback.receiveErrorserieNReceive(error);
        }
      }
      
      @Override
      public void onFault(org.apache.axis2.context.MessageContext faultContext) {
        org.apache.axis2.AxisFault fault = org.apache.axis2.util.Utils.getInboundFaultFromMessageContext(faultContext);
        onError(fault);
      }
      
      @Override
      public void onComplete() {
        try {
          _messageContext.getTransportOut().getSender().cleanup(_messageContext);
        }
        catch (org.apache.axis2.AxisFault axisFault) {
          callback.receiveErrorserieNReceive(axisFault);
        }
      }
    });
    
    org.apache.axis2.util.CallbackReceiver _callbackReceiver = null;
    if (_operations[2].getMessageReceiver() == null && _operationClient.getOptions().isUseSeparateListener()) {
      _callbackReceiver = new org.apache.axis2.util.CallbackReceiver();
      _operations[2].setMessageReceiver(
          _callbackReceiver);
    }
    
    // execute the operation client
    _operationClient.execute(false);
    
  }
  
  /**
   * A utility method that copies the namepaces from the SOAPEnvelope
   */
  private java.util.Map getEnvelopeNamespaces(org.apache.axiom.soap.SOAPEnvelope env) {
    java.util.Map returnMap = new java.util.HashMap();
    java.util.Iterator namespaceIterator = env.getAllDeclaredNamespaces();
    while (namespaceIterator.hasNext()) {
      org.apache.axiom.om.OMNamespace ns = (org.apache.axiom.om.OMNamespace) namespaceIterator.next();
      returnMap.put(ns.getPrefix(), ns.getNamespaceURI());
    }
    return returnMap;
  }
  
  private javax.xml.namespace.QName[] opNameArray = null;
  
  private boolean optimizeContent(javax.xml.namespace.QName opName) {
    
    if (opNameArray == null) {
      return false;
    }
    for (int i = 0; i < opNameArray.length; i++) {
      if (opName.equals(opNameArray[i])) {
        return true;
      }
    }
    return false;
  }
  
  // http://admin.123elec.com/index.php/api/v2_soap/index/
  private org.apache.axiom.om.OMElement toOM(magento.Login param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.Login.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.om.OMElement toOM(magento.LoginResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.LoginResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.om.OMElement toOM(magento.EndSession param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.EndSession.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.om.OMElement toOM(magento.EndSessionResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.EndSessionResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.om.OMElement toOM(magento.SerieNReceive param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.SerieNReceive.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.om.OMElement toOM(magento.SerieNReceiveResponse param, boolean optimizeContent)
      throws org.apache.axis2.AxisFault {
    
    try {
      return param.getOMElement(magento.SerieNReceiveResponse.MY_QNAME,
          org.apache.axiom.om.OMAbstractFactory.getOMFactory());
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, magento.Login param,
      boolean optimizeContent, javax.xml.namespace.QName methodQName)
      throws org.apache.axis2.AxisFault {
    
    try {
      
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
      emptyEnvelope.getBody().addChild(param.getOMElement(magento.Login.MY_QNAME, factory));
      return emptyEnvelope;
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  /* methods to provide back word compatibility */
  
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, magento.EndSession param,
      boolean optimizeContent, javax.xml.namespace.QName methodQName)
      throws org.apache.axis2.AxisFault {
    
    try {
      
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
      emptyEnvelope.getBody().addChild(param.getOMElement(magento.EndSession.MY_QNAME, factory));
      return emptyEnvelope;
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  /* methods to provide back word compatibility */
  
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory, magento.SerieNReceive param,
      boolean optimizeContent, javax.xml.namespace.QName methodQName)
      throws org.apache.axis2.AxisFault {
    
    try {
      
      org.apache.axiom.soap.SOAPEnvelope emptyEnvelope = factory.getDefaultEnvelope();
      emptyEnvelope.getBody().addChild(param.getOMElement(magento.SerieNReceive.MY_QNAME, factory));
      return emptyEnvelope;
    }
    catch (org.apache.axis2.databinding.ADBException e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    
  }
  
  /* methods to provide back word compatibility */
  
  /**
   * get the default envelope
   */
  private org.apache.axiom.soap.SOAPEnvelope toEnvelope(org.apache.axiom.soap.SOAPFactory factory) {
    return factory.getDefaultEnvelope();
  }
  
  private java.lang.Object fromOM(
      org.apache.axiom.om.OMElement param,
      java.lang.Class type,
      java.util.Map extraNamespaces) throws org.apache.axis2.AxisFault {
    
    try {
      
      if (magento.EndSession.class.equals(type)) {
        
        return magento.EndSession.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
      if (magento.EndSessionResponse.class.equals(type)) {
        
        return magento.EndSessionResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
      if (magento.Login.class.equals(type)) {
        
        return magento.Login.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
      if (magento.LoginResponse.class.equals(type)) {
        
        return magento.LoginResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
      if (magento.SerieNReceive.class.equals(type)) {
        
        return magento.SerieNReceive.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
      if (magento.SerieNReceiveResponse.class.equals(type)) {
        
        return magento.SerieNReceiveResponse.Factory.parse(param.getXMLStreamReaderWithoutCaching());
        
      }
      
    }
    catch (java.lang.Exception e) {
      throw org.apache.axis2.AxisFault.makeFault(e);
    }
    return null;
  }
  
}
