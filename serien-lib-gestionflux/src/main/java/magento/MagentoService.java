/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package magento;

/*
 *  MagentoService java interface
 */

public interface MagentoService {
  
  /**
   * Auto generated method signature
   * Login user and retrive session id
   * @param login0
   * 
   */
  
  public magento.LoginResponse login(
      
      magento.Login login0)
      throws java.rmi.RemoteException;
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * Login user and retrive session id
   * @param login0
   * 
   */
  public void startlogin(
      
      magento.Login login0,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException;
  
  /**
   * Auto generated method signature
   * End web service session
   * @param endSession2
   * 
   */
  
  public magento.EndSessionResponse endSession(
      
      magento.EndSession endSession2)
      throws java.rmi.RemoteException;
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * End web service session
   * @param endSession2
   * 
   */
  public void startendSession(
      
      magento.EndSession endSession2,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException;
  
  /**
   * Auto generated method signature
   * Reception Flux Serie N
   * @param serieNReceive4
   * 
   */
  
  public magento.SerieNReceiveResponse serieNReceive(
      
      magento.SerieNReceive serieNReceive4)
      throws java.rmi.RemoteException;
  
  /**
   * Auto generated method signature for Asynchronous Invocations
   * Reception Flux Serie N
   * @param serieNReceive4
   * 
   */
  public void startserieNReceive(
      
      magento.SerieNReceive serieNReceive4,
      
      final magento.MagentoServiceCallbackHandler callback)
      
      throws java.rmi.RemoteException;
  
  //
}
