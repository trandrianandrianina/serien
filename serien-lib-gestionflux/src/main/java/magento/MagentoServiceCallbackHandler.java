/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package magento;

/**
 * MagentoServiceCallbackHandler Callback class, Users can extend this class and implement
 * their own receiveResult and receiveError methods.
 */
public abstract class MagentoServiceCallbackHandler {
  
  protected Object clientData;
  
  /**
   * User can pass in any object that needs to be accessed once the NonBlocking
   * Web service call is finished and appropriate method of this CallBack is called.
   * @param clientData Object mechanism by which the user can pass in user data
   *          that will be avilable at the time this callback is called.
   */
  public MagentoServiceCallbackHandler(Object clientData) {
    this.clientData = clientData;
  }
  
  /**
   * Please use this constructor if you don't want to set any clientData
   */
  public MagentoServiceCallbackHandler() {
    this.clientData = null;
  }
  
  /**
   * Get the client data
   */
  
  public Object getClientData() {
    return clientData;
  }
  
  /**
   * auto generated Axis2 call back method for login method
   * override this method for handling normal response from login operation
   */
  public void receiveResultlogin(
      magento.LoginResponse result) {
  }
  
  /**
   * auto generated Axis2 Error handler
   * override this method for handling error response from login operation
   */
  public void receiveErrorlogin(java.lang.Exception e) {
  }
  
  /**
   * auto generated Axis2 call back method for endSession method
   * override this method for handling normal response from endSession operation
   */
  public void receiveResultendSession(
      magento.EndSessionResponse result) {
  }
  
  /**
   * auto generated Axis2 Error handler
   * override this method for handling error response from endSession operation
   */
  public void receiveErrorendSession(java.lang.Exception e) {
  }
  
  /**
   * auto generated Axis2 call back method for serieNReceive method
   * override this method for handling normal response from serieNReceive operation
   */
  public void receiveResultserieNReceive(
      magento.SerieNReceiveResponse result) {
  }
  
  /**
   * auto generated Axis2 Error handler
   * override this method for handling error response from serieNReceive operation
   */
  public void receiveErrorserieNReceive(java.lang.Exception e) {
  }
  
}
