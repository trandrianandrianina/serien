/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package magento;

/**
 * EndSession bean class
 */
@SuppressWarnings({ "unchecked", "unused" })

public class EndSession implements org.apache.axis2.databinding.ADBBean {
  
  public static final javax.xml.namespace.QName MY_QNAME = new javax.xml.namespace.QName("urn:Magento", "endSession", "ns2");
  
  /**
   * field for SessionId
   */
  
  protected String localSessionId;
  
  /**
   * Auto generated getter method
   * @return String
   */
  public String getSessionId() {
    return localSessionId;
  }
  
  /**
   * Auto generated setter method
   * @param param SessionId
   */
  public void setSessionId(String param) {
    
    this.localSessionId = param;
    
  }
  
  /**
   *
   * @param parentQName
   * @param factory
   * @return org.apache.axiom.om.OMElement
   */
  @Override
  public org.apache.axiom.om.OMElement getOMElement(final javax.xml.namespace.QName parentQName,
      final org.apache.axiom.om.OMFactory factory) throws org.apache.axis2.databinding.ADBException {
    
    org.apache.axiom.om.OMDataSource dataSource = new org.apache.axis2.databinding.ADBDataSource(this, MY_QNAME);
    return factory.createOMElement(dataSource, MY_QNAME);
    
  }
  
  @Override
  public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    serialize(parentQName, xmlWriter, false);
  }
  
  @Override
  public void serialize(final javax.xml.namespace.QName parentQName, javax.xml.stream.XMLStreamWriter xmlWriter, boolean serializeType)
      throws javax.xml.stream.XMLStreamException, org.apache.axis2.databinding.ADBException {
    
    String prefix = null;
    String namespace = null;
    
    prefix = parentQName.getPrefix();
    namespace = parentQName.getNamespaceURI();
    writeStartElement(prefix, namespace, parentQName.getLocalPart(), xmlWriter);
    
    if (serializeType) {
      
      String namespacePrefix = registerPrefix(xmlWriter, "urn:Magento");
      if ((namespacePrefix != null) && (namespacePrefix.trim().length() > 0)) {
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", namespacePrefix + ":endSession", xmlWriter);
      }
      else {
        writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "type", "endSession", xmlWriter);
      }
      
    }
    
    namespace = "";
    writeStartElement(null, namespace, "sessionId", xmlWriter);
    
    if (localSessionId == null) {
      // write the nil attribute
      
      writeAttribute("xsi", "http://www.w3.org/2001/XMLSchema-instance", "nil", "1", xmlWriter);
      
    }
    else {
      
      xmlWriter.writeCharacters(localSessionId);
      
    }
    
    xmlWriter.writeEndElement();
    
    xmlWriter.writeEndElement();
    
  }
  
  private static String generatePrefix(String namespace) {
    if (namespace.equals("urn:Magento")) {
      return "ns2";
    }
    return org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
  }
  
  /**
   * Utility method to write an element start tag.
   */
  private void writeStartElement(String prefix, String namespace, String localPart, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    String writerPrefix = xmlWriter.getPrefix(namespace);
    if (writerPrefix != null) {
      xmlWriter.writeStartElement(namespace, localPart);
    }
    else {
      if (namespace.length() == 0) {
        prefix = "";
      }
      else if (prefix == null) {
        prefix = generatePrefix(namespace);
      }
      
      xmlWriter.writeStartElement(prefix, localPart, namespace);
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
  }
  
  /**
   * Util method to write an attribute with the ns prefix
   */
  private void writeAttribute(String prefix, String namespace, String attName, String attValue,
      javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
    if (xmlWriter.getPrefix(namespace) == null) {
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    xmlWriter.writeAttribute(namespace, attName, attValue);
  }
  
  /**
   * Util method to write an attribute without the ns prefix
   */
  private void writeAttribute(String namespace, String attName, String attValue, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attValue);
    }
    else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(namespace, attName, attValue);
    }
  }
  
  /**
   * Util method to write an attribute without the ns prefix
   */
  private void writeQNameAttribute(String namespace, String attName, javax.xml.namespace.QName qname,
      javax.xml.stream.XMLStreamWriter xmlWriter) throws javax.xml.stream.XMLStreamException {
    
    String attributeNamespace = qname.getNamespaceURI();
    String attributePrefix = xmlWriter.getPrefix(attributeNamespace);
    if (attributePrefix == null) {
      attributePrefix = registerPrefix(xmlWriter, attributeNamespace);
    }
    String attributeValue;
    if (attributePrefix.trim().length() > 0) {
      attributeValue = attributePrefix + ":" + qname.getLocalPart();
    }
    else {
      attributeValue = qname.getLocalPart();
    }
    
    if (namespace.equals("")) {
      xmlWriter.writeAttribute(attName, attributeValue);
    }
    else {
      registerPrefix(xmlWriter, namespace);
      xmlWriter.writeAttribute(namespace, attName, attributeValue);
    }
  }
  
  /**
   * method to handle Qnames
   */
  
  private void writeQName(javax.xml.namespace.QName qname, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    String namespaceURI = qname.getNamespaceURI();
    if (namespaceURI != null) {
      String prefix = xmlWriter.getPrefix(namespaceURI);
      if (prefix == null) {
        prefix = generatePrefix(namespaceURI);
        xmlWriter.writeNamespace(prefix, namespaceURI);
        xmlWriter.setPrefix(prefix, namespaceURI);
      }
      
      if (prefix.trim().length() > 0) {
        xmlWriter.writeCharacters(prefix + ":" + org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }
      else {
        // i.e this is the default namespace
        xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
      }
      
    }
    else {
      xmlWriter.writeCharacters(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qname));
    }
  }
  
  private void writeQNames(javax.xml.namespace.QName[] qnames, javax.xml.stream.XMLStreamWriter xmlWriter)
      throws javax.xml.stream.XMLStreamException {
    
    if (qnames != null) {
      // we have to store this data until last moment since it is not possible to write any
      // namespace data after writing the charactor data
      StringBuffer stringToWrite = new StringBuffer();
      String namespaceURI = null;
      String prefix = null;
      
      for (int i = 0; i < qnames.length; i++) {
        if (i > 0) {
          stringToWrite.append(" ");
        }
        namespaceURI = qnames[i].getNamespaceURI();
        if (namespaceURI != null) {
          prefix = xmlWriter.getPrefix(namespaceURI);
          if ((prefix == null) || (prefix.length() == 0)) {
            prefix = generatePrefix(namespaceURI);
            xmlWriter.writeNamespace(prefix, namespaceURI);
            xmlWriter.setPrefix(prefix, namespaceURI);
          }
          
          if (prefix.trim().length() > 0) {
            stringToWrite.append(prefix).append(":").append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
          else {
            stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
          }
        }
        else {
          stringToWrite.append(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(qnames[i]));
        }
      }
      xmlWriter.writeCharacters(stringToWrite.toString());
    }
    
  }
  
  /**
   * Register a namespace prefix
   */
  private String registerPrefix(javax.xml.stream.XMLStreamWriter xmlWriter, String namespace) throws javax.xml.stream.XMLStreamException {
    String prefix = xmlWriter.getPrefix(namespace);
    if (prefix == null) {
      prefix = generatePrefix(namespace);
      javax.xml.namespace.NamespaceContext nsContext = xmlWriter.getNamespaceContext();
      while (true) {
        String uri = nsContext.getNamespaceURI(prefix);
        if (uri == null || uri.length() == 0) {
          break;
        }
        prefix = org.apache.axis2.databinding.utils.BeanUtil.getUniquePrefix();
      }
      xmlWriter.writeNamespace(prefix, namespace);
      xmlWriter.setPrefix(prefix, namespace);
    }
    return prefix;
  }
  
  /**
   * databinding method to get an XML representation of this object
   *
   */
  @Override
  public javax.xml.stream.XMLStreamReader getPullParser(javax.xml.namespace.QName qName)
      throws org.apache.axis2.databinding.ADBException {
    
    java.util.ArrayList elementList = new java.util.ArrayList();
    java.util.ArrayList attribList = new java.util.ArrayList();
    
    elementList.add(new javax.xml.namespace.QName("", "sessionId"));
    
    elementList.add(localSessionId == null ? null : org.apache.axis2.databinding.utils.ConverterUtil.convertToString(localSessionId));
    
    return new org.apache.axis2.databinding.utils.reader.ADBXMLStreamReaderImpl(qName, elementList.toArray(), attribList.toArray());
    
  }
  
  /**
   * Factory class that keeps the parse method
   */
  public static class Factory {
    
    /**
     * static method to create the object
     * Precondition: If this object is an element, the current or next start element starts this object and any intervening reader events
     * are ignorable
     * If this object is not an element, it is a complex type and the reader is at the event just after the outer start element
     * Postcondition: If this object is an element, the reader is positioned at its end element
     * If this object is a complex type, the reader is positioned at the end element of its outer element
     */
    public static EndSession parse(javax.xml.stream.XMLStreamReader reader) throws java.lang.Exception {
      EndSession object = new EndSession();
      String nillableValue = null;
      
      try {
        while (!reader.isStartElement() && !reader.isEndElement()) {
          reader.next();
        }
        
        if (reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type") != null) {
          String fullTypeName = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "type");
          if (fullTypeName != null) {
            String nsPrefix = null;
            if (fullTypeName.indexOf(":") > -1) {
              nsPrefix = fullTypeName.substring(0, fullTypeName.indexOf(":"));
            }
            nsPrefix = nsPrefix == null ? "" : nsPrefix;
            
            String type = fullTypeName.substring(fullTypeName.indexOf(":") + 1);
            
            if (!"endSession".equals(type)) {
              // find namespace for the prefix
              String nsUri = reader.getNamespaceContext().getNamespaceURI(nsPrefix);
              return (EndSession) magento.ExtensionMapper.getTypeObject(nsUri, type, reader);
            }
          }
        }
        
        reader.next();
        while (!reader.isStartElement() && !reader.isEndElement()) {
          reader.next();
        }
        
        if (reader.isStartElement() && new javax.xml.namespace.QName("", "sessionId").equals(reader.getName())) {
          
          nillableValue = reader.getAttributeValue("http://www.w3.org/2001/XMLSchema-instance", "nil");
          if (!"true".equals(nillableValue) && !"1".equals(nillableValue)) {
            
            String content = reader.getElementText();
            
            object.setSessionId(org.apache.axis2.databinding.utils.ConverterUtil.convertToString(content));
            
          }
          else {
            
            reader.getElementText(); // throw away text nodes if any.
          }
          
          reader.next();
          
        } // End of if for expected property start element
        
        else {
          // A start element we are not expecting indicates an invalid parameter was passed
          throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
        }
        
        while (!reader.isStartElement() && !reader.isEndElement()) {
          reader.next();
        }
        
        if (reader.isStartElement()) {
          // A start element we are not expecting indicates a trailing invalid property
          throw new org.apache.axis2.databinding.ADBException("Unexpected subelement " + reader.getName());
        }
        
      }
      catch (javax.xml.stream.XMLStreamException e) {
        throw new java.lang.Exception(e);
      }
      
      return object;
    }
    
  }
  
}
