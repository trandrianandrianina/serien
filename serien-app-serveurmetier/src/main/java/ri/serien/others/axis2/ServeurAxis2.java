/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.others.axis2;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.axis2.AxisFault;
import org.apache.axis2.transport.SimpleAxis2Server;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Gestionnaire du serveur Axis2
 */
public class ServeurAxis2 {
  // Constantes
  private static final String STARTUP_FILE = "axis2server.sh";
  public static final String RADICAL = "axis2";
  public static final int STT_STOPPED = -1;
  public static final int STT_STARTED = 0;
  
  // Variables
  private static SimpleAxis2Server serverAxis2 = null;
  private String dossierRacineAxis2 = null;
  private int state = STT_STOPPED;
  private String msgErreur = ""; // Conserve le dernier message d'erreur émit et non lu
  
  /**
   * Constructeur
   * @param pDossierRacine
   */
  public ServeurAxis2(String pDossierRacine) {
    dossierRacineAxis2 = pDossierRacine;
  }
  
  // -- Méthodes publiques
  
  /**
   * Lance le serveur Axis2 de manière embedded
   */
  private void launchServer() {
    try {
      serverAxis2 = new SimpleAxis2Server(null, null);
      String[] args = { "-repo", dossierRacineAxis2 + File.separatorChar + "repository", "-conf",
          dossierRacineAxis2 + File.separatorChar + "conf/axis2.xml" };
      SimpleAxis2Server.main(args);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors du démarrage du serveur de WebServices");
    }
  }
  
  /**
   * Lance le serveur Axis2 via son script shell
   * @return
   */
  private boolean launchShellServer() {
    // Construction du chemin
    String pathserver = dossierRacineAxis2 + File.separatorChar + "bin" + File.separatorChar + STARTUP_FILE;
    Trace.info("Chemin du serveur Axis2 : " + pathserver);
    
    try {
      Process process = Runtime.getRuntime().exec(pathserver);
      AfficheurFlux fluxSortie = new AfficheurFlux(process.getInputStream());
      AfficheurFlux fluxErreur = new AfficheurFlux(process.getErrorStream());
      new Thread(fluxSortie).start();
      new Thread(fluxErreur).start();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'exécution du programme");
      return false;
    }
    return true;
  }
  
  public synchronized void startServer() {
    launchServer();
    setState(STT_STARTED);
    Trace.info("Démarrage du serveur Axis2 réussit.");
  }
  
  /**
   * Arrêt du serveur Axis2
   */
  public synchronized void stopServer() {
    if (serverAxis2 != null) {
      try {
        serverAxis2.stop();
      }
      catch (AxisFault e) {
        throw new MessageErreurException(e, "Erreur lors de l'arrêt du serveur de WebServices");
      }
    }
  }
  
  /**
   * Retourne le message d'erreur
   * @return
   */
  public String getMsgErreur() {
    // La récupération du message est à usage unique
    final String chaine = msgErreur;
    msgErreur = "";
    
    return chaine;
  }
  
  // -- Accesseurs
  
  /**
   * @return le state
   */
  public int getState() {
    return state;
  }
  
  /**
   * @param state le state à définir
   */
  public void setState(int state) {
    this.state = state;
  }
  
  /**
   * Permet de rediriger les flux vers la console pour le programme lancé
   * @author Administrateur
   * 
   */
  class AfficheurFlux implements Runnable {
    
    private final InputStream inputStream;
    
    AfficheurFlux(InputStream inputStream) {
      this.inputStream = inputStream;
    }
    
    private BufferedReader getBufferedReader(InputStream is) {
      return new BufferedReader(new InputStreamReader(is));
    }
    
    // @Override
    @Override
    public void run() {
    }
  }
  
}
