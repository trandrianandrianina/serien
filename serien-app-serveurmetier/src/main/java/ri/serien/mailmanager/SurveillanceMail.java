/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mailmanager;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.ibm.as400.access.CommandCall;

import ri.serien.libas400.dao.sql.exploitation.mail.SqlMail;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.mail.CritereMail;
import ri.serien.libcommun.exploitation.mail.CritereServeurMail;
import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.DestinataireMail;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.EnumTypeMail;
import ri.serien.libcommun.exploitation.mail.EnvoiMail;
import ri.serien.libcommun.exploitation.mail.IdDestinataireMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeDestinataireMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.PatternMailHTML;
import ri.serien.libcommun.exploitation.mail.ServeurMail;
import ri.serien.libcommun.exploitation.mail.TemplateTypeMail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.vente.document.EnumCodeEnteteDocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe surveille la table PSEMMAILMM pour une base de données.
 * Elle traite en fonction du type de mail associé et de la configuration associée.
 */
public class SurveillanceMail extends Thread {
  // Variables
  private SystemeManager systemeManager = null;
  private QueryManager queryManager = null;
  private IdBibliotheque idBaseDeDonnees = null;
  private SqlMail sqlMail = null;
  private HashMap<IdEtablissement, ServeurMail> listeServeurMailSortant = null;
  private PatternMailHTML patternMailHTML = null;
  
  private boolean modeDebug = false;
  // Le mode simulation permet de lancer le traitement d'envoi de mail ou fax tout en n'exécutant pas l'envoi. Cela permet de vérifier
  // la bonne construction des mail/fax sans que les données soient altérées comme c'est le cas par le mode test. Ces deux modes sont
  // indépendants
  private boolean simulationEnvoi = false;
  // Le mode test permet de changer les destinataires (FAX et EMAIL) par une adresse de test ou un numéro de fax test au moment de l'envoi
  // afin de ne pas envoyer des mail/fax à de vrais clients pendant des tests
  private boolean modeTest = false;
  private String adresseMailTest = null;
  private String adresseMailTestFaxReception = null;
  private Integer delaiLectureMail = null;
  
  /**
   * Constructeur.
   */
  public SurveillanceMail(SystemeManager pSystemeManager, IdBibliotheque pBaseDeDonnees) {
    if (pSystemeManager == null) {
      throw new MessageErreurException("Erreur lors de l'initialisation de la connexion à DB2.");
    }
    systemeManager = pSystemeManager;
    idBaseDeDonnees = pBaseDeDonnees;
    Connection connection = systemeManager.getDatabaseManager().getConnection();
    queryManager = new QueryManager(connection);
    queryManager.setLibrary(pBaseDeDonnees);
    setName(getClass().getName() + " lettre d'environnement :" + ManagerAS400.getLettreEnvironnement() + ", base de données : "
        + queryManager.getLibrary());
  }
  
  // -- Méthodes publiques
  
  /**
   * Charge les données depuis les tables.
   */
  public boolean chargerDonnees() {
    sqlMail = new SqlMail(queryManager);
    
    try {
      // Cette opération est effectuée chaque lundi
      if (isLancerNettoyageTable()) {
        Trace.info("Réogarnisation de la table " + queryManager.getLibrary() + "/" + EnumTableBDD.MAIL_VARIABLE);
        // Lancement du RGZPFM qui permet de supprimer les enregistrements supprimés pour un gain de place
        CommandCall programmeCL = new CommandCall(systemeManager.getSystem());
        String ordre = "RGZPFM FILE(" + queryManager.getLibrary() + "/" + EnumTableBDD.MAIL_VARIABLE + ")";
        try {
          programmeCL.setCommand(ordre);
        }
        catch (PropertyVetoException e) {
          Trace.erreur(e, "Erreur lors du lancement de la commande: " + ordre);
        }
      }
      
      // Charge la liste des serveurs de mail
      List<ServeurMail> listeServeurMail = sqlMail.chargerListeServeurMail(new CritereServeurMail());
      // Test des serveurs de mail afin de conserver les serveurs actifs
      listeServeurMailSortant = testerServeurMail(listeServeurMail);
      if (listeServeurMailSortant == null || listeServeurMailSortant.isEmpty()) {
        Trace.alerte("Aucun serveur de mails n'est opérationnel pour la base " + queryManager.getLibrary() + ".");
        return false;
      }
    }
    catch (Exception e) {
      // Erreur possible si la table a évolué et que le REC n'est pas encore passé car la base de données est "abandonnée"
      Trace.erreur("Erreur lors du chargement de la liste des serveurs d'envoi de mails pour " + queryManager.getLibrary());
      return false;
    }
    
    return true;
  }
  
  /**
   * Surveillance infini du contenu de la table PSEMMAILMM.
   */
  @Override
  public void run() {
    // Construction du nom du thread
    String nomThread = String.format("SurveillanceMail(%03d)-%-10s", Thread.currentThread().getId(), queryManager.getLibrary());
    setName(nomThread);
    
    // Contrôle des données en table
    if (listeServeurMailSortant == null || listeServeurMailSortant.isEmpty()) {
      Trace.info("Il n'y a aucun serveur de mail de configurés dans la " + queryManager.getLibrary() + '.');
      return;
    }
    
    Trace.info("La surveillance de la base de données " + queryManager.getLibrary() + " démarre.");
    
    // Initialise les données
    initialiserDonnees();
    
    // Boucle infinie
    while (true) {
      try {
        // Mise en pause de la thread
        Thread.sleep(delaiLectureMail);
        
        // Contrôle du contenu de la table
        envoyerListeMail();
      }
      catch (InterruptedException ie) {
        Thread.currentThread().interrupt();
        break;
      }
      catch (Exception e) {
      }
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Vérifie si le nettoyage de la table PSEMVARMAM doit être lancé.
   * Cette opération est lancée tous les lundi.
   */
  private boolean isLancerNettoyageTable() {
    Calendar cal = Calendar.getInstance();
    cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
    String maintenant = DateHeure.getJourHeure(4, new Date());
    String lundiSemaineEnCours = DateHeure.getJourHeure(4, cal.getTime());
    
    // Contrôle si la date correspond au lundi de cette semaine
    return maintenant.equals(lundiSemaineEnCours);
  }
  
  /**
   * Initialise les données.
   */
  private void initialiserDonnees() {
    // Création du pattern qui va permettre de générer le mail au format html
    // Pour l'instant il est unique pour l'ensemble des bases
    patternMailHTML = new PatternMailHTML();
    patternMailHTML.setNomServeur(idBaseDeDonnees.getNom());
    // patternMailHTML.setUrlPhotoMail(urlPhotoMail);
    
    // Changement du statut des mails "En attente" vers "A envoyer"
    sqlMail.changerStatutMailDeAttenteVersAEnvoyer();
  }
  
  /**
   * Envoi des mails qui ont le statut "A envoyer".
   */
  private void envoyerListeMail() {
    // Récupèration de la liste des id des mails à envoyer
    CritereMail critereMail = new CritereMail();
    critereMail.setStatutMail(EnumStatutMail.A_ENVOYER);
    List<IdMail> listeIdMail = sqlMail.chargerListeIdMail(critereMail);
    // Si la liste est vide alors on sort
    if (listeIdMail == null || listeIdMail.isEmpty()) {
      return;
    }
    
    // Chargement complet des mails à envoyer à partir de la liste d'id
    List<Mail> listeMail = sqlMail.chargerListeMail(listeIdMail);
    if (listeMail == null || listeMail.isEmpty()) {
      throw new MessageErreurException(
          "Il y a eu un problème lors du chargement de la liste des mails de la base " + queryManager.getLibrary() + ".");
    }
    
    // Gérer la création des types de mail non existant dans la base de données
    gererTypeMailNonExistant(listeMail);
    
    // Chargement des types de mail (chargement au moment car des types de mail peuvent être activés ou désactivés à tout moment)
    CritereTypeMail critereTypeMail = new CritereTypeMail();
    critereTypeMail.setActif(Boolean.TRUE);
    ListeTypeMail listeTypeMail = sqlMail.chargerTypeMail(critereTypeMail);
    if (listeTypeMail == null || listeTypeMail.isEmpty()) {
      Trace.alerte("La liste des types de mail est vide (ils sont peut être inactifs) pour la base " + queryManager.getLibrary() + ".");
      return;
    }
    
    // Envoi des mails de la liste
    // Note :
    // Si le type du mail est désactivé alors le statut du mail est marqué comme en "Erreur de traitement"
    for (Mail mail : listeMail) {
      try {
        // Récupération du type de mail pour le mail en cours
        if (mail.getIdTypeMail() == null) {
          Trace.erreur("Le mail " + mail.getId() + " a été ignoré car son type est invalide.");
          continue;
        }
        TypeMail typeMail = listeTypeMail.get(mail.getIdTypeMail());
        if (typeMail == null) {
          Trace.erreur("Le mail " + mail.getId() + " a été ignoré car son type (" + mail.getIdTypeMail() + ") est invalide ou inactif.");
          continue;
        }
        // Récupère le serveur de mail pour le mail à envoyer
        if (patternMailHTML != null) {
          ServeurMail serveurMail = listeServeurMailSortant.get(mail.getId().getIdEtablissement());
          if (serveurMail != null) {
            patternMailHTML.setUrlPhotoMail(serveurMail.getUrlLogo());
          }
        }
        // Construction du mail à partir du type de mail
        mail.construire(typeMail, patternMailHTML);
        
        // Remplacement des variables dans le cas de certains types de mail
        // TODO A traiter
        TypeMailCommandeWeb.majContenu(mail, queryManager.getLibrary());
        
        // Récupèration du destinataire
        majDestinataire(mail, typeMail);
        
        // Sauvegarde du mail avec les dernières mises à jour (car pour l'instant certaines données du mail sont construites à la volée)
        sqlMail.modifierMail(mail);
        
        // Envoi du mail
        envoyerMail(mail);
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors du traitement du mail n°" + mail.getId());
        mail.setStatut(EnumStatutMail.ERREUR_TRAITEMENT);
        mail.ajouterMessage("Erreur de traitement mail", false);
      }
      
      // Mise à jour des informations du mail après la tentative d'envoi
      try {
        // Mise à jour des informations du mail dans la base
        sqlMail.modifierMail(mail);
      }
      catch (MessageErreurException e) {
        Trace.erreur("Erreur lors de l'enregistrement du mail n°" + mail.getId());
      }
    }
  }
  
  /**
   * Gère la création des types de mail qui n'existent pas encore dans la base données.
   * 
   * @param pListeMail Liste des mails à envoyer
   */
  private void gererTypeMailNonExistant(List<Mail> pListeMail) {
    // Extraction de la liste des types de mail à partir des mails existant
    List<IdTypeMail> listeIdTypeMailViaMailConcerne = pListeMail.stream()
        // Parcourir la liste et retourner les IdTypeMail uniquement
        .map(mail -> mail.getIdTypeMail())
        // Retourner une collection
        .collect(Collectors.toList());
    
    // Préparer les types de mail non existant dans la base de données
    List<IdTypeMail> listeIdTypeMailNonExistant = new ArrayList<IdTypeMail>();
    // Charger uniquement les types de mail dont nous avons besoin
    ListeTypeMail listeTypeMailExistant = sqlMail.chargerListeTypeMail(listeIdTypeMailViaMailConcerne);
    // Si la liste est vide, tous les types de mail chargés n'existent pas encore dans la base de données
    if (listeTypeMailExistant == null || listeTypeMailExistant.isEmpty()) {
      listeIdTypeMailNonExistant.addAll(listeIdTypeMailViaMailConcerne);
    }
    // Sinon, recenser uniquement ceux qui n'existe pas encore
    else {
      for (IdTypeMail idTypeMail : listeIdTypeMailViaMailConcerne) {
        // Ajouter les IdTypeMail qui ne sont pas dans la liste des existants
        if (!listeTypeMailExistant.contains(idTypeMail)) {
          listeIdTypeMailNonExistant.add(idTypeMail);
        }
      }
    }
    
    // Créer les types de mails non existant, s'il y en a
    if (!listeIdTypeMailNonExistant.isEmpty()) {
      creerListeTypeMail(listeIdTypeMailNonExistant);
    }
  }
  
  /**
   * Crée les types de mail qui n'existent pas encore dans la base de données.
   * 
   * @param pIdTypeMail Les Id des types de mail non existant
   * @return
   */
  private void creerListeTypeMail(List<IdTypeMail> pIdTypeMail) {
    if (pIdTypeMail == null || pIdTypeMail.isEmpty()) {
      Trace.erreur("La liste des types mails à créer est vide.");
      return;
    }
    
    List<TypeMail> listeTypeMail = new ArrayList<TypeMail>();
    for (IdTypeMail idTypeMail : pIdTypeMail) {
      TypeMail typeMail = TypeMail.getInstance(idTypeMail);
      typeMail.setLibelle(idTypeMail.getEnumTypeMail().getLibelle());
      switch (idTypeMail.getEnumTypeMail()) {
        case MAIL_CONTACT_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailContact());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailContact());
          break;
        case MAIL_ANNULATION_COMMANDE_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailAnnulationCommande());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailAnnulationCommande());
          break;
        case MAIL_CONFIRMATION_COMMANDE_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailClientConfirmationCommande());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailClientConfirmationCommande());
          break;
        case MAIL_RAPPORT_COMMANDE_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailRapportConfirmationCommande());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailRapportConfirmationCommande());
          break;
        case MAIL_CONFIRMATION_DEVIS_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailClientConfirmationDevis());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailClientConfirmationDevis());
          break;
        case MAIL_RAPPORT_DEVIS_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailRapportConfirmationDevis());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailRapportConfirmationDevis());
          break;
        case MAIL_VALIDATION_INSCRIPTION_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailValidationInscription());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailValidationInscription());
          break;
        case MAIL_NOTIFICATION_INSCRIPTION_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailNotificationInscription());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailNotificationInscription());
          break;
        case MAIL_CHANGEMENT_MOT_DE_PASSE_HTML:
          typeMail.setSujet(TemplateTypeMail.genererSujetMailChangementMotDePasse());
          typeMail.setCorpsMessage(TemplateTypeMail.genererCorpsMailChangementMotDePasse());
          break;
        default:
          break;
      }
      // S'assurer que le nouveau type de mail à persister soit correcte
      if (!Constantes.normerTexte(typeMail.getCorpsMessage()).isEmpty() && !Constantes.normerTexte(typeMail.getSujet()).isEmpty()) {
        Trace.info("Ajout du nouveau type de mail : " + idTypeMail.getTexte() + " dans la liste à persister.");
        listeTypeMail.add(typeMail);
      }
    }
    
    sqlMail.creerListeTypeMail(listeTypeMail);
  }
  
  /**
   * Préparer le ou les destinataires d'un mail.
   * Avec gestion du mode test qui permet de remplacer de vrais adresses mails ou fax par des adresses de test.
   */
  private void majDestinataire(Mail pMail, TypeMail pTypeMail) {
    ListeDestinataireMail listeDestinataire = pMail.getListeDestinataire();
    if (listeDestinataire == null) {
      listeDestinataire = new ListeDestinataireMail();
      pMail.setListeDestinataire(listeDestinataire);
    }
    
    // En mode test, les mails sont envoyés à l'adresse qui est renseignée dans le fichier context.xml de tomcat
    if (modeTest) {
      Trace.info("Le MailManager fonctionne en mode test pour le mail " + pMail.getId());
      // En mode test tous les destinataires sont supprimés pour être remplacé par le destinataire de test
      listeDestinataire.clear();
      IdDestinataireMail idDestinataireMail = IdDestinataireMail.getInstancePourTableMail(pMail.getId());
      DestinataireMail destinataire = new DestinataireMail(idDestinataireMail);
      // S'il s'agit d'un type fax alors on met l'adresse mail de test du fax de réception
      if (pTypeMail.getId().getEnumTypeMail().equals(EnumTypeMail.MAIL_ENVOI_FAX) && adresseMailTestFaxReception != null) {
        destinataire.setNom("Compte fax (Mode test)");
        destinataire.setEmail(adresseMailTestFaxReception);
        listeDestinataire.add(destinataire);
        return;
      }
      // S'il s'agit d'un mail classique alors on met l'adresse mail de test
      if (adresseMailTest != null) {
        destinataire.setNom("Compte mail (Mode test)");
        destinataire.setEmail(adresseMailTest);
        listeDestinataire.add(destinataire);
        return;
      }
      else {
        throw new MessageErreurException(
            "Erreur lors de la préparation du destinataire du mail, l'email du destinataire de test n'est pas renseignée.");
      }
    }
    
    // En fonctionnement normal
    // Vérification de la présence d'un id de document pour les types de mail qui en ont besoin
    if (pTypeMail != null && pTypeMail.isIdDocumentVenteObligatoire()) {
      String indicatifDocument = pMail.getIndicatifDocument();
      if (indicatifDocument == null || indicatifDocument.trim().isEmpty()) {
        throw new MessageErreurException(
            "Erreur lors de la préparation du destinataire du mail, l'id du document de vente n'a pas été trouvé.");
      }
      // Construction d'un id document de vente digne de ce nom
      IdDocumentVente idDocumentVente = IdDocumentVente.getInstanceParIndicatif(
          pTypeMail.getId().getCodeEtablissement() + EnumCodeEnteteDocumentVente.COMMANDE_OU_BON.getCode() + indicatifDocument);
      
      // Lecture du destinataire du mail
      DestinataireMail destinataire = sqlMail.chargerDestinataireMailWebshop(pMail.getId(), idDocumentVente);
      if (destinataire == null) {
        throw new MessageErreurException(
            "Erreur lors de la préparation du destinataire du mail, le contact du document de vente n'a pas été trouvé.");
      }
      listeDestinataire.add(destinataire);
    }
    
    return;
  }
  
  /**
   * Envoyer le mail passé en paramètre avec la configuration attribuée à son établissement.
   */
  private boolean envoyerMail(Mail pMail) {
    if (pMail == null || pMail.getId() == null || pMail.getId().getCodeEtablissement() == null) {
      Trace.erreur("Le mail est invalide.");
      return false;
    }
    // Vérification que le statut du mail est "A envoyer" avant l'envoi
    if (!pMail.getStatut().equals(EnumStatutMail.A_ENVOYER)) {
      return false;
    }
    boolean envoye = false;
    
    // Récupération du serveur de mail pour cet établissement
    ServeurMail serveurMail = listeServeurMailSortant.get(pMail.getId().getIdEtablissement());
    if (serveurMail == null) {
      pMail.ajouterMessage("Aucun serveur de mail n'est associé à l'établissement " + pMail.getId().getCodeEtablissement() + ".", false);
      return false;
    }
    
    // Contrôle que le serveur de mail soit actif
    if (serveurMail.isActif()) {
      envoye = EnvoiMail.envoyer(serveurMail, pMail, modeDebug, simulationEnvoi);
    }
    // Dans le cas contraire le mail est mis en attente
    else {
      pMail.setStatut(EnumStatutMail.EN_ATTENTE);
      pMail.ajouterMessage("Le serveur de mail de l'établissement " + pMail.getId().getCodeEtablissement()
          + " n'est pas actif, le mail est mis en attente.", false);
    }
    
    // Mise à jour du statut et du message du mail en cas de succés
    if (envoye) {
      pMail.setStatut(EnumStatutMail.ENVOYE);
      pMail.ajouterMessage("Mail n° " + pMail.getId() + " envoyé avec succès", true);
      // Si le statut du mail est envoyé on ne peut plus le modifier donc il faut utiliser la méthode mettreStatutMailEnvoye
      // pour changer le statut en base
      sqlMail.mettreStatutMailEnvoye(pMail);
    }
    else {
      // Si le mail n'est pas en attente alors le statut du mail est changé en "Erreur d'envoi"
      if (!Constantes.equals(pMail.getStatut(), EnumStatutMail.EN_ATTENTE)) {
        pMail.setStatut(EnumStatutMail.ERREUR_ENVOI);
        pMail.ajouterMessage("Erreur d'envoi du mail n° " + pMail.getId() + " de la base " + queryManager.getLibrary(), false);
      }
    }
    return envoye;
  }
  
  /**
   * Teste une liste de serveurs de mail, afin de savoir lesquels sont actifs, en envoyant un mail test.
   */
  private HashMap<IdEtablissement, ServeurMail> testerServeurMail(List<ServeurMail> pListeServeurMail) {
    if (listeServeurMailSortant == null) {
      listeServeurMailSortant = new HashMap<IdEtablissement, ServeurMail>();
    }
    listeServeurMailSortant.clear();
    if (pListeServeurMail == null) {
      return null;
    }
    
    // Teste l'ensemble la liste des serveurs
    for (ServeurMail serveurMail : pListeServeurMail) {
      boolean retour = false;
      if (simulationEnvoi) {
        retour = true;
      }
      else {
        // Pour le mail de test le mode débug est forcé afin de facilité la résolution des problèmes en clientèle
        retour = EnvoiMail.testerEnvoi(serveurMail, adresseMailTest, true);
      }
      if (retour) {
        listeServeurMailSortant.put(serveurMail.getId().getIdEtablissement(), serveurMail);
        Trace.info("Le serveur de mail " + serveurMail.getId() + " de la base " + queryManager.getLibrary() + " est actif.");
      }
    }
    return listeServeurMailSortant;
  }
  
  // -- Accesseurs
  
  public void setModeDebug(boolean modeDebug) {
    this.modeDebug = modeDebug;
  }
  
  public void setSimulationEnvoi(boolean simulationEnvoi) {
    this.simulationEnvoi = simulationEnvoi;
  }
  
  public void setModeTest(boolean modeTest) {
    this.modeTest = modeTest;
  }
  
  public void setAdresseMailTest(String adresseMailTest) {
    this.adresseMailTest = adresseMailTest;
  }
  
  public void setAdresseMailTestFaxReception(String adresseMailTestFaxReception) {
    this.adresseMailTestFaxReception = adresseMailTestFaxReception;
  }
  
  public void setDelaiLectureMail(Integer delaiLectureMail) {
    this.delaiLectureMail = delaiLectureMail;
  }
}
