/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mailmanager;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.dao.sql.exploitation.basededonnees.SqlBaseDeDonnees;
import ri.serien.libas400.dao.sql.exploitation.environnement.SqlEnvironnement;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.bdd.CritereBDD;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe regroupe les méthodes pour gérer le mailManager.
 */
public class MailManager implements InterfaceModule {
  // Constantes
  // La fréquence de balayge des bases de données est par défaut de 20 secondes
  private static final int FREQUENCE_BALAYAGE = 20000;
  
  // Variables
  private static EnumModule enumModule = EnumModule.MAILMANAGER;
  private static boolean moduleActif = false;
  private static ListeParametreModule listeParametre = null;
  private boolean modeDebug = false;
  // Le mode simulation permet de lancer le traitement d'envoi de mail ou fax tout en n'exécutant pas l'envoi. Cela permet de vérifier
  // la bonne construction des mail/fax sans que les données soient altérées comme c'est le cas par le mode test. Ces deux modes sont
  // indépendants
  private boolean simulationEnvoi = false;
  // Le mode test permet de changer les destinataires (FAX et EMAIL) par une adresse de test ou un numéro de fax test au moment de l'envoi
  // afin de ne pas envoyer des mail/fax à de vrais clients pendant des tests
  private boolean modeTest = false;
  private String adresseMailTest = null;
  private String adresseMailTestFaxReception = null;
  private Integer delaiLectureMail = null;
  private List<SurveillanceMail> listeSurveillanceMail = null;
  private static QueryManager queryManager = null;
  private static SystemeManager systemeManager = null;
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return listeParametre;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return false;
  }
  
  /**
   * Démarre le module mailManager.
   */
  @Override
  public void demarrer() {
    if (moduleActif) {
      Trace.alerte("Le module " + enumModule + " est déjà actif.");
      return;
    }
    Trace.info("Démarrage du module " + enumModule + '.');
    
    // Initialisation et contrôle des paramètres
    initialiserParametre();
    
    // Connexion à DB2
    initialiserConnexionDB2();
    
    // Recherche les bases de données contenant la table des mails
    List<IdBibliotheque> listeBaseDeDonnees = retournerListeBaseDeDonnees();
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      deconnecterDB2();
      Trace.info("Le module " + enumModule + " n'est pas démarré car il n'y a aucune table de mail à surveiller.");
      return;
    }
    
    // Parcours la liste des bases de données afin de lancer un thread de surveillance pour chacune
    listeSurveillanceMail = new ArrayList<SurveillanceMail>();
    for (IdBibliotheque baseDeDonnees : listeBaseDeDonnees) {
      SurveillanceMail surveillanceMail = new SurveillanceMail(systemeManager, baseDeDonnees);
      surveillanceMail.setDelaiLectureMail(delaiLectureMail);
      surveillanceMail.setModeDebug(modeDebug);
      surveillanceMail.setModeTest(modeTest);
      surveillanceMail.setSimulationEnvoi(simulationEnvoi);
      surveillanceMail.setAdresseMailTest(adresseMailTest);
      surveillanceMail.setAdresseMailTestFaxReception(adresseMailTestFaxReception);
      
      // Chargement et test des serveurs de mail
      if (!surveillanceMail.chargerDonnees()) {
        continue;
      }
      
      // Au moins un serveur de mail est configuré
      surveillanceMail.start();
      listeSurveillanceMail.add(surveillanceMail);
    }
    Trace.info("Nombre de bases de données surveillées : " + listeSurveillanceMail.size());
    
    // Contrôle si des bases de données sont effectivement sous surveillance
    if (listeSurveillanceMail.isEmpty()) {
      deconnecterDB2();
      Trace.erreur("Le module " + enumModule + " n'est pas démarré car aucune configuration n'est active.");
      return;
    }
    
    moduleActif = true;
    Trace.info("Le module " + enumModule + " est démarré.");
  }
  
  /**
   * Arrête le module mailManager.
   */
  @Override
  public void arreter() {
    if (!moduleActif) {
      Trace.alerte("Le module " + enumModule + " n'est pas actif.");
      return;
    }
    
    // Arrêt de la surveillance des bases de données
    for (SurveillanceMail surveillanceMail : listeSurveillanceMail) {
      if (surveillanceMail.isAlive()) {
        surveillanceMail.interrupt();
      }
    }
    listeSurveillanceMail.clear();
    deconnecterDB2();
    
    moduleActif = false;
    Trace.info("Module " + enumModule + " stoppé.");
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return moduleActif;
  }
  
  /**
   * Envoi un ordre au module mailManager.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + enumModule + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case MAILMANAGER_START:
          // Recherche les paramètres pour ce module dans le cas où ils auraient été modifié
          ManagerParametreModule.rechargerParametre(pEnumControleModule.getEnumModule());
          // Démarre le module
          demarrer();
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return null;
        
        case MAILMANAGER_STOP:
          arreter();
          return null;
        
        case MAILMANAGER_STATUS:
          String retour;
          if (moduleActif) {
            retour = "Le module " + enumModule + " est démarré.";
          }
          else {
            retour = "Le module " + enumModule + " est arrêté.";
          }
          Trace.info(retour);
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return retour;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + enumModule + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser les paramètres du module à partir de la liste des paramètres pour ce module.
   */
  private void initialiserParametre() {
    // Extraction des paramètres spécifiques pour ce module
    listeParametre = ManagerParametreModule.extraireParametreModule(enumModule);
    if (listeParametre == null || listeParametre.isEmpty()) {
      throw new MessageErreurException("Aucun paramètre pour le module " + enumModule + " n'a été trouvé.");
    }
    
    // Le mode simulation d'envoi de mail
    Integer valeur = listeParametre.retournerValeurEnInteger(EnumParametreModule.MAILMANAGER_SIMULATION_ENVOI);
    if (valeur == null || valeur <= 0) {
      simulationEnvoi = false;
    }
    else {
      simulationEnvoi = true;
    }
    
    // Le mode test
    valeur = listeParametre.retournerValeurEnInteger(EnumParametreModule.MAILMANAGER_MODE_TEST);
    if (valeur == null || valeur <= 0) {
      modeTest = false;
    }
    else {
      modeTest = true;
    }
    
    // L'adresse mail de test
    adresseMailTest = listeParametre.retournerValeurEnString(EnumParametreModule.MAILMANAGER_ADRESSEMAILTEST);
    
    // L'adresse mail pour le fax de test
    adresseMailTestFaxReception = listeParametre.retournerValeurEnString(EnumParametreModule.MAILMANAGER_ADRESSEMAILFAXTEST);
    
    // La fréquence de balayage
    delaiLectureMail = listeParametre.retournerValeurEnInteger(EnumParametreModule.MAILMANAGER_DELAILECTUREMAIL);
    
    // Le mode débug
    valeur = listeParametre.retournerValeurEnInteger(EnumParametreModule.MAILMANAGER_MODE_DEBUG);
    if (valeur == null || valeur <= 0) {
      modeDebug = false;
    }
    else {
      modeDebug = true;
    }
    
    // Initialisation et contrôle des paramètres du mail manager
    if (delaiLectureMail == null || delaiLectureMail.intValue() <= 0) {
      delaiLectureMail = FREQUENCE_BALAYAGE;
    }
    
    // Contrôle de l'adresse mail si le mode test est actif
    if (modeTest) {
      if (adresseMailTest == null || adresseMailTest.isEmpty()) {
        throw new MessageErreurException("L'adresse mail de test pour " + enumModule + " est invalide.");
      }
      if (adresseMailTestFaxReception == null || adresseMailTestFaxReception.isEmpty()) {
        Trace.alerte("L'adresse mail du fax de test n'est pas renseignée, veuillez mettre à jour ce paramètre dans la table PSEMPMODM."
            + " Dans l'immédiat cette adresse sera initialisée avec l'adresse mail de test " + adresseMailTest);
        // Pour ne pas planter les clients qui utilise déjà le mailManager
        adresseMailTestFaxReception = adresseMailTest;
      }
    }
  }
  
  /**
   * Initialise la connexion à DB2.
   */
  private void initialiserConnexionDB2() {
    systemeManager = new SystemeManager(ManagerAS400.getSystemeAS400(), true);
    if (systemeManager == null) {
      throw new MessageErreurException("Erreur lors de l'initialisation de la connexion à DB2.");
    }
    Connection connection = systemeManager.getDatabaseManager().getConnection();
    queryManager = new QueryManager(connection);
    Trace.info("Connexion à DB2 réussi.");
  }
  
  /**
   * Retourner la liste des bibliothèques clients présentes sur l'AS400.
   */
  private List<IdBibliotheque> retournerListeBaseDeDonnees() {
    CritereBDD critere = new CritereBDD();
    critere.setLettreEnvironnement(ManagerAS400.getLettreEnvironnement());
    SqlBaseDeDonnees sqlBaseDeDonnees = new SqlBaseDeDonnees(queryManager);
    List<IdBibliotheque> listeIdBibliotheque = sqlBaseDeDonnees.chargerListeIdBaseDeDonnees(critere);
    if (listeIdBibliotheque == null || listeIdBibliotheque.isEmpty()) {
      return null;
    }
    
    // Retourne la liste des bases de données qui contienne la table PSEMMAILMM
    SqlEnvironnement sqlEnvironnement = new SqlEnvironnement(queryManager);
    return sqlEnvironnement.retournerListeBaseDeDonneesPresenceTable(EnumTableBDD.MAIL, listeIdBibliotheque);
  }
  
  public static QueryManager getQueryManager() {
    return queryManager;
  }
  
  /**
   * Déconnecte la connexion à DB2.
   */
  private void deconnecterDB2() {
    if (queryManager == null) {
      return;
    }
    queryManager.deconnecter();
  }
}
