/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libcommun.commun.id.AbstractId;
import ri.serien.libcommun.commun.id.EnumEtatObjetMetier;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Cette classe définit l'id d'un module.
 */
public class IdModule extends AbstractId {
  // Constantes
  public static final int LONGUEUR_NOM_MODULE = 255;
  
  // Variables
  private final EnumModule enumModule;
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdModule(EnumEtatObjetMetier pEnumEtatObjetMetier, EnumModule pEnumModule) {
    super(pEnumEtatObjetMetier);
    enumModule = controlerEnumModule(pEnumModule);
  }
  
  /**
   * Constructeur privé pour un identifiant complet.
   */
  private IdModule(EnumEtatObjetMetier pEnumEtatObjetMetier, String pNomModule) {
    super(pEnumEtatObjetMetier);
    enumModule = controlerNom(pNomModule);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdModule getInstance(String pNomModule) {
    return new IdModule(EnumEtatObjetMetier.MODIFIE, pNomModule);
  }
  
  /**
   * Créer un identifiant à partir de ses informations sous forme éclatée.
   */
  public static IdModule getInstance(EnumModule pEnumModule) {
    return new IdModule(EnumEtatObjetMetier.MODIFIE, pEnumModule);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôler le nom du paramètre.
   */
  private static EnumModule controlerEnumModule(EnumModule pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le nom du paramètre n'est pas renseigné.");
    }
    return pValeur;
  }
  
  /**
   * Contrôler le nom du paramètre.
   */
  private static EnumModule controlerNom(String pValeur) {
    if (pValeur == null) {
      throw new MessageErreurException("Le nom du module n'est pas renseigné.");
    }
    if (pValeur.length() > LONGUEUR_NOM_MODULE) {
      throw new MessageErreurException("Le nom du module n'est pas correct car il est supérieur à la longueur attendu.");
    }
    return EnumModule.valueOfByCode(pValeur);
  }
  
  // -- Méthodes abstraites à surcharger dans les classes enfants
  
  @Override
  public int hashCode() {
    int code = 17;
    code = 37 * code + enumModule.hashCode();
    return code;
  }
  
  @Override
  public boolean equals(Object pObject) {
    if (pObject == this) {
      return true;
    }
    
    if (pObject == null) {
      return false;
    }
    
    if (!(pObject instanceof IdModule)) {
      throw new MessageErreurException("Erreur lors de la comparaison de deux identifiants de module.");
    }
    IdModule id = (IdModule) pObject;
    return enumModule.equals(id.getEnumModule());
  }
  
  @Override
  public int compareTo(Object pObject) {
    if (pObject == null) {
      throw new MessageErreurException("Impossible de comparer un " + getClass().getSimpleName() + " avec null");
    }
    if (!(pObject instanceof IdModule)) {
      throw new MessageErreurException(
          "Impossible de comparer un " + getClass().getSimpleName() + " avec " + pObject.getClass().getSimpleName());
    }
    IdModule id = (IdModule) pObject;
    return enumModule.compareTo(id.enumModule);
  }
  
  @Override
  public String getTexte() {
    return enumModule.getLibelle();
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'enum du module.
   */
  public EnumModule getEnumModule() {
    return enumModule;
  }
  
}
