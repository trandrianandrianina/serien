/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import java.util.List;

import ri.serien.libcommun.commun.classemetier.ListeClasseMetier;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Liste d'objets Module.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de modules de l'application Série N.
 */
public class ListeModule extends ListeClasseMetier<IdModule, Module, ListeModule> {
  
  // -- Méthodes publiques
  
  /**
   * Charger les paramètres dont les identifiants sont fournis.
   */
  @Override
  public ListeModule charger(IdSession pIdSession, List<IdModule> pListeId) {
    throw new MessageErreurException("Le chargement de données paginé n'est pas opérationnel.");
  }
  
  /**
   * Charge la liste complète des objets métiers pour l'établissement donné.
   */
  @Override
  public ListeModule charger(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    throw new MessageErreurException("Le chargement des modules de l'application Série N par établissement n'a pas de raison d'être.");
  }
  
}
