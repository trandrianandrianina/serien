/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libas400.dao.sql.exploitation.module.SqlModule;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.module.CritereParametreModule;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.EnumOrigineParametre;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.IdParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.outils.MigrationParametreModuleVersV4;

/**
 * Cette classe regroupe l'ensemble des traitements concernant la gestion des paramètres des modules.
 */
public class ManagerParametreModule {
  // Variables
  private static ListeParametreModule listeParametre = null;
  private static QueryManager queryManager = null;
  
  // -- Méthodes publiques
  
  /**
   * Récupère les paramètres en entrée depuis la ligne de commande.
   * @param pChaineParametres
   */
  public static ListeParametreModule chargerParametreLigneCommande(String pChaineParametres) {
    if (pChaineParametres == null) {
      throw new MessageErreurException(
          "Les paramètres sont vides. Deux paramètres sont obligatoires: " + EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT
              + " et " + EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE + ".");
    }
    
    // Conversion de la chaine de caractères en liste de paramètres
    ListeParametreModule listeParametre = new ListeParametreModule(pChaineParametres, EnumOrigineParametre.LIGNE_DE_COMMANDE);
    
    // Contrôle la présence des paramètres obligatoires
    if (listeParametre.isEmpty()) {
      throw new MessageErreurException("Deux paramètres sont obligatoires: " + EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT
          + " et " + EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE + ".");
    }
    
    // Paramètre bibliothèque environnement
    IdParametreModule idParametreBibliotheque =
        IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT);
    if (!listeParametre.contains(idParametreBibliotheque)) {
      throw new MessageErreurException(
          "Le paramètre " + EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT + " est obligatoire.");
    }
    
    // Paramètre dossier racine
    IdParametreModule idParametreDossier = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE);
    if (!listeParametre.contains(idParametreDossier)) {
      throw new MessageErreurException("Le paramètre " + EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE + " est obligatoire.");
    }
    
    return listeParametre;
  }
  
  /**
   * Contrôle la table contenant les paramètres (éventuellement ajoute les paramètres manquants).
   * @param pQueryManager L'accès à la base de données
   */
  public static void controlerParametreEnTable(QueryManager pQueryManager) {
    Trace.info("Début du contrôle de la table des paramètres des modules.");
    
    // Contrôle du paramètre
    if (pQueryManager == null) {
      throw new MessageErreurException("Le querymanager n'est pas initialisé.");
    }
    queryManager = pQueryManager;
    
    // Contrôle de la bibliothèque d'environnement du queryManager
    String bibliothequeEnvironnement = Constantes.normerTexte(queryManager.getLibrairieEnvironnement());
    if (bibliothequeEnvironnement.isEmpty()) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    
    // Chargement de tous les paramètres stockés en table afin de vérifier que la table soit à jour
    SqlModule sqlModule = new SqlModule(queryManager);
    ListeParametreModule listeParametre = sqlModule.chargerListeParametre(null);
    
    // Aucun paramètre n'a été trouvé, il s'agit peut être d'une migration des paramètres de Série N
    if (listeParametre == null || listeParametre.isEmpty()) {
      MigrationParametreModuleVersV4 migrationParametreModule = new MigrationParametreModuleVersV4(queryManager);
      migrationParametreModule.migrer();
    }
    // Des paramètres ont été trouvé, ils sont systématiquement contrôlés avec ceux contenu dans EnumParametreModule
    else {
      synchroniserTableParametre(queryManager, listeParametre);
    }
    
    Trace.info("Fin du contrôle de la table des paramètres des modules.");
  }
  
  /**
   * Charge tous les paramètres de serveur métier et les paramètres ".demarrage" de tous les modules.
   * @param pQueryManager L'accès à la base de données
   */
  public static ListeParametreModule chargerParametreEnTable(QueryManager pQueryManager, CritereParametreModule pCritere) {
    Trace.info("Début du chargement des paramètres des modules.");
    
    // Chargement des paramètres nécessaires au Serveur Métier
    SqlModule sqlModule = new SqlModule(pQueryManager);
    ListeParametreModule listeParametre = sqlModule.chargerListeParametre(pCritere);
    if (listeParametre == null || listeParametre.isEmpty()) {
      String nomTable = pQueryManager.getLibrairieEnvironnement() + "/" + EnumTableBDD.PARAMETRE_MODULE;
      throw new MessageErreurException("La table " + nomTable + " est vide et cela est anormal.");
    }
    
    Trace.info("Fin du chargement des paramètres des modules.");
    return listeParametre;
  }
  
  /**
   * Synchronise la table des paramètres PSEMPMODM avec les paramètres de EnumParametreModule.
   * @param pQueryManager L'accès à la base de données
   */
  public static void synchroniserTableParametre(QueryManager pQueryManager, ListeParametreModule pListeParametreModule) {
    Trace.info("\tDébut de l'initialisation de la table des paramètres des modules.");
    
    // Création d'une liste à partir de EnumParametreModule
    if (pListeParametreModule == null) {
      pListeParametreModule = ListeParametreModule.creerListeParametreAvecValeurParDefaut();
    }
    else {
      // Mise à jour de la liste des paramètres trouvés avec la liste des paramètres par défaut
      pListeParametreModule.miseAJourAvecParametreParDefaut();
    }
    
    // Mise à jour de la table
    SqlModule sqlModule = new SqlModule(pQueryManager);
    sqlModule.sauverListeParametre(pListeParametreModule);
    
    Trace.info("\tFin de l'initialisation de la table des paramètres des modules.");
  }
  
  /**
   * Recharge les paramètres d'un module donné.
   * Utilisé lors de l'utilisation du fichier de contrôle.
   * @param pEnumModule Le module dont il faut recharger les paramètres
   */
  public static void rechargerParametre(EnumModule pEnumModule) {
    // Contrôle du paramètre
    if (pEnumModule == null) {
      throw new MessageErreurException("Le module n'est pas valide.");
    }
    
    // Contrôle du queryManager
    if (queryManager == null) {
      throw new MessageErreurException("Le querymanager n'est pas initialisé.");
    }
    
    // Chargement des paramètres du module
    CritereParametreModule critere = new CritereParametreModule();
    critere.ajouterNomModuleListe(pEnumModule);
    ListeParametreModule listeParametreLu = ManagerParametreModule.chargerParametreEnTable(queryManager, critere);
    // Mise à jour des paramètres du module dans la liste originale
    listeParametre.miseAJourValeurAvecAutreListe(listeParametreLu);
  }
  
  /**
   * Retourne la valeur de type String d'un paramètre donnée.
   * @param pEnumParametreModule EnumParametreModule à rechercher
   * @return
   */
  public static String retournerValeurEnString(EnumParametreModule pEnumParametreModule) {
    if (listeParametre == null) {
      throw new MessageErreurException("La liste des paramètres n'a pas été initialisée.");
    }
    return listeParametre.retournerValeurEnString(pEnumParametreModule);
  }
  
  /**
   * Retourne la valeur de type Integer d'un paramètre donnée.
   * @param pEnumParametreModule EnumParametreModule à rechercher
   * @return
   */
  public static Integer retournerValeurEnInteger(EnumParametreModule pEnumParametreModule) {
    if (listeParametre == null) {
      throw new MessageErreurException("La liste des paramètres n'a pas été initialisée.");
    }
    return listeParametre.retournerValeurEnInteger(pEnumParametreModule);
  }
  
  /**
   * Retourne les paramètres d'un module donné.
   */
  public static ListeParametreModule extraireParametreModule(EnumModule pEnumModule) {
    if (listeParametre == null) {
      throw new MessageErreurException("La liste des paramètres en cours de l'application est invalide.");
    }
    
    return listeParametre.extraireParametreModule(pEnumModule);
  }
  
  /**
   * Ecrit dans les logs l'ensemble des paramètres contenus dans la liste.
   */
  public static void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  /**
   * Initialise la liste des paramètres en cours de l'application.
   */
  public static void setListeParametre(ListeParametreModule pListeParametre) {
    listeParametre = pListeParametre;
  }
  
}
