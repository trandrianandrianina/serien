/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import java.io.File;
import java.io.FilenameFilter;

import ri.serien.gestionnaireflux.GestionnaireFlux;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.mailmanager.MailManager;
import ri.serien.newsim.serveur.ServeurNewSim;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurmetier.serveur.ServeurSerieN;
import ri.serien.serveurrmi.ServeurRMI;
import ri.serien.tomcat.Tomcat;

/**
 * Cette classe permet la gestion de l'ensemble des modules de l'application.
 * Elle permet de contrôler les modules via des fichiers déposés dans le dossier ctrl.
 */
public class ManagerModule {
  // Constantes
  private static final int DELAI_EN_SEC = 10;
  public static final String EXTENSION = ".ctrl";
  
  // Variables
  private static Thread surveillance = null;
  private static ListeModule listeModule = new ListeModule();
  // Création du filtre associé
  private static FilenameFilter filtreCtrl = new FilenameFilter() {
    @Override
    public boolean accept(File dir, String name) {
      return name.toLowerCase().endsWith(EXTENSION);
    }
  };
  
  // -- Méthodes publiques
  
  /**
   * Arrêt de la surveillance.
   */
  public static void arreterSurveillance() {
    if (surveillance != null && surveillance.isAlive()) {
      surveillance.interrupt();
    }
  }
  
  // -- Méthodes publiques
  
  /**
   * Enregistre les modules et les initialise.
   */
  public static void initialiserModule(ServeurSerieN pServeurMetier) {
    enregistrerModule(pServeurMetier);
    
    Trace.info("Initialisation des modules.");
    for (Module module : listeModule) {
      module.initialiser();
    }
    
    Trace.titre("Initialisation la surveillance de la communication.");
    initialiserSurveillanceControle();
  }
  
  /**
   * Arrête tous les modules en cours de fonctionnement.
   */
  public static void arreterModule(boolean pArretSurveillance) {
    Trace.info("Arrêt des modules.");
    for (Module module : listeModule) {
      InterfaceModule instanceModule = (InterfaceModule) module.getInstanceModule();
      // S'il s'agit de l'application principale, l'arrêt est ignoré
      if (instanceModule.isApplication()) {
        continue;
      }
      // Si le module est démarré alors il est arrêté
      if (instanceModule.isDemarre()) {
        instanceModule.arreter();
      }
    }
    
    // Révocation de tous les modules si besoin
    if (pArretSurveillance) {
      Trace.info("Révocation des modules.");
      listeModule.clear();
      
      // Arrêt de la surveillance du dossier de contrôle
      Trace.info("Arrêt de la surveillance du dossier de contrôle.");
      ManagerModule.arreterSurveillance();
    }
  }
  
  /**
   * Interroge et affiche le status de tous les modules dans les traces.
   */
  public static void interrogerStatus() {
    Trace.info("Status des modules.");
    for (Module module : listeModule) {
      InterfaceModule instanceModule = (InterfaceModule) module.getInstanceModule();
      // S'il s'agit de l'application principale, l'interrogation est ignoré
      if (instanceModule.isApplication()) {
        continue;
      }
      
      EnumControleModule enumControleModule = EnumControleModule.valueOfByEnumEtTexte(module.getId().getEnumModule(), "status");
      ((InterfaceModule) module.getInstanceModule()).envoyerControle(enumControleModule, null);
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Enregistre tous les modules de l'application.
   */
  private static void enregistrerModule(ServeurSerieN pServeurMetier) {
    // L'application Serveur Métier
    try {
      Trace.info("Enregistrement du module " + EnumModule.SERVEURMETIER + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.SERVEURMETIER);
      Module module = new Module(idModule, pServeurMetier);
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.SERVEURMETIER + " a rencontré un problème lors de son enregistrement.");
    }
    
    // Le module RMI
    try {
      Trace.info("Enregistrement du module " + EnumModule.SERVEURRMI + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.SERVEURRMI);
      Module module = new Module(idModule, new ServeurRMI());
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.SERVEURRMI + " a rencontré un problème lors de son enregistrement.");
    }
    
    // Le module Newsim
    try {
      Trace.info("Enregistrement du module " + EnumModule.NEWSIM + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.NEWSIM);
      Module module = new Module(idModule, new ServeurNewSim());
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.NEWSIM + " a rencontré un problème lors de son enregistrement.");
    }
    
    // Le module mailManager
    try {
      Trace.info("Enregistrement du module " + EnumModule.MAILMANAGER + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.MAILMANAGER);
      Module module = new Module(idModule, new MailManager());
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.MAILMANAGER + " a rencontré un problème lors de son enregistrement.");
    }
    
    // Le module gestionnaire de flux
    try {
      Trace.info("Enregistrement du module " + EnumModule.GESTIONNAIREFLUX + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.GESTIONNAIREFLUX);
      Module module = new Module(idModule, new GestionnaireFlux());
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.GESTIONNAIREFLUX + " a rencontré un problème lors de son enregistrement.");
    }
    
    // Le module Tomcat
    try {
      Trace.info("Enregistrement du module " + EnumModule.TOMCAT + '.');
      IdModule idModule = IdModule.getInstance(EnumModule.TOMCAT);
      Module module = new Module(idModule, new Tomcat());
      listeModule.add(module);
    }
    catch (Exception e) {
      Trace.info("Le module " + EnumModule.TOMCAT + " a rencontré un problème lors de son enregistrement.");
    }
  }
  
  /**
   * Initialise la surveillance de la communication via les fichiers ctrl.
   */
  private static void initialiserSurveillanceControle() {
    final File dossierASurveiller = initialiserDossierASurveiller();
    
    // Lancement de la surveillance du dossier de contrôle
    surveillance = new Thread() {
      @Override
      public void run() {
        // Construction du nom du thread
        String nomThread = String.format("ManagerModule(%03d)-surveiller", Thread.currentThread().getId());
        setName(nomThread);
        
        // Boucle infinie
        while (true) {
          try {
            // Contrôle que le dossier existe dans le cas contraire il est créé
            if (!dossierASurveiller.exists()) {
              dossierASurveiller.mkdirs();
            }
            
            // Inventaire du contenu du dossier en filtrant l'extension du fichier
            File[] listeFichiers = dossierASurveiller.listFiles(filtreCtrl);
            if (listeFichiers != null && listeFichiers.length > 0) {
              // Parcours de la liste des fichiers afin de savoir à qui est destiné le message
              for (File fichierControle : listeFichiers) {
                // Extraction des informations du nom du fichier de contrôle
                AnalyseMessageControle analyseMessageControle = new AnalyseMessageControle(new FileNG(fichierControle));
                IdModule idModule = analyseMessageControle.getIdModule();
                Module module = listeModule.get(idModule);
                if (module == null) {
                  continue;
                }
                
                // Envoi du message au module
                ((InterfaceModule) module.getInstanceModule()).envoyerControle(analyseMessageControle.getEnumControleModule(),
                    analyseMessageControle.getParametre());
              }
            }
            else {
              Thread.sleep(DELAI_EN_SEC * 1000);
            }
          }
          catch (InterruptedException ex) {
            // Très important de réinterrompre
            Thread.currentThread().interrupt();
            break;
          }
          catch (Exception e) {
          }
        }
      }
    };
    surveillance.start();
  }
  
  /**
   * Initialise le nom du dossier de contrôle à surveiller.
   */
  private static File initialiserDossierASurveiller() {
    // Construction du chemin du dossier de controle
    String pDossier;
    if (ManagerAS400.getDossierRacineServeur() == null) {
      pDossier = ManagerModule.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    }
    pDossier = ManagerAS400.getDossierRacineServeur() + File.separatorChar + Constantes.DOSSIER_CTRL;
    File dossierDeControle = new File(pDossier);
    
    // Création du dossier de contrôle si besoin
    if (!dossierDeControle.exists()) {
      dossierDeControle.mkdirs();
    }
    Trace.info("Dossier de contrôle : " + dossierDeControle.getAbsolutePath());
    
    // Suppression de tous les fichiers ctrl existants
    File[] listeFichiers = dossierDeControle.listFiles(filtreCtrl);
    if (listeFichiers != null) {
      for (File fichier : listeFichiers) {
        fichier.delete();
      }
    }
    Trace.info("Nettoyage du dossier " + dossierDeControle.getAbsolutePath() + " effectué.");
    
    return dossierDeControle;
  }
  
}
