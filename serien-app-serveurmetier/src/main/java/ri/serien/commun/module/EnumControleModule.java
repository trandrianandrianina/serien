/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.outils.MessageErreurException;

/**
 * Messages contrôles possibles pour l'ensemble des modules.
 */
public enum EnumControleModule {
  SERVEURMETIER_STOP(0, EnumModule.SERVEURMETIER, "stop"),
  SERVEURMETIER_STATUS(1, EnumModule.SERVEURMETIER, "status"),
  NEWSIM_START(2, EnumModule.NEWSIM, "start"),
  NEWSIM_STOP(3, EnumModule.NEWSIM, "stop"),
  NEWSIM_STATUS(4, EnumModule.NEWSIM, "status"),
  SERVEURRMI_START(5, EnumModule.SERVEURRMI, "start"),
  SERVEURRMI_STOP(6, EnumModule.SERVEURRMI, "stop"),
  SERVEURRMI_STATUS(7, EnumModule.SERVEURRMI, "status"),
  MAILMANAGER_START(8, EnumModule.MAILMANAGER, "start"),
  MAILMANAGER_STOP(9, EnumModule.MAILMANAGER, "stop"),
  MAILMANAGER_STATUS(10, EnumModule.MAILMANAGER, "status"),
  GESTIONNAIREFLUX_START(11, EnumModule.GESTIONNAIREFLUX, "start"),
  GESTIONNAIREFLUX_STOP(12, EnumModule.GESTIONNAIREFLUX, "stop"),
  GESTIONNAIREFLUX_STATUS(13, EnumModule.GESTIONNAIREFLUX, "status"),
  TOMCAT_START(14, EnumModule.TOMCAT, "start"),
  TOMCAT_STOP(15, EnumModule.TOMCAT, "stop"),
  TOMCAT_STATUS(16, EnumModule.TOMCAT, "status"),
  GESTIONNAIREFLUX_FORCEFLUX(17, EnumModule.GESTIONNAIREFLUX, "forceflux");
  
  // Variables
  private final int numero;
  private final EnumModule enumModule;
  private final String texte;
  
  /**
   * Constructeur.
   */
  EnumControleModule(int pNumero, EnumModule pEnumModule, String pTexte) {
    numero = pNumero;
    enumModule = pEnumModule;
    texte = pTexte;
  }
  
  /**
   * Le numéro sous lequel le message est persisté.
   */
  public int getNumero() {
    return numero;
  }
  
  /**
   * L'énum module du message.
   */
  public EnumModule getEnumModule() {
    return enumModule;
  }
  
  /**
   * Le texte du message.
   */
  public String getTexte() {
    return texte;
  }
  
  /**
   * Retourner le texte associé dans une chaîne de caractère.
   * C'est cette valeur qui sera visible dans les listes déroulantes.
   */
  @Override
  public String toString() {
    return getTexte() + "(" + enumModule + ')';
  }
  
  /**
   * Retourner l'enum par son texte.
   */
  static public EnumControleModule valueOfByEnumEtTexte(EnumModule pEnumModule, String pTexte) {
    for (EnumControleModule value : values()) {
      if (pEnumModule.equals(value.getEnumModule()) && pTexte.equals(value.getTexte())) {
        return value;
      }
    }
    throw new MessageErreurException("Le message est invalide : " + pTexte);
  }
}
