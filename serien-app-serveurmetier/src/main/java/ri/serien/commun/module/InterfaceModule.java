/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libcommun.exploitation.module.ListeParametreModule;

/**
 * Cette interface décrit les méthodes qui doivent être impémentées dans les modules de Série N.
 */
public interface InterfaceModule {
  /**
   * Retourne les paramètres propre à ce module.
   */
  public ListeParametreModule listerParametre();
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  public boolean isApplication();
  
  /**
   * Démarre le module.
   */
  public void demarrer();
  
  /**
   * Arrête le module
   */
  public void arreter();
  
  /**
   * Retourne si le module est en cours de fonctionnement.
   */
  public boolean isDemarre();
  
  /**
   * Envoi un ordre au module provenant du fichier ctrl.
   */
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre);
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  public void tracerListeParametre();
}
