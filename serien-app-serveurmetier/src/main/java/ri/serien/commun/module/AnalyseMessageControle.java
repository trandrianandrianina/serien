/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Cette classe analyse le nom d'un fichier de contrôle afin d'en extraire le message et le nom du module concerné par ce message.
 * Permet de récupérer l'enumControleModule à partir du message et du module.
 */
public class AnalyseMessageControle {
  // Variables
  private FileNG fichierControle = null;
  private String contenu = null;
  private IdModule idModule = null;
  private EnumControleModule enumControleModule = null;
  
  /**
   * Constructeur.
   */
  public AnalyseMessageControle(FileNG pFichierControle) {
    fichierControle = pFichierControle;
    decouperNomFichier();
  }
  
  // -- Méthodes privées
  
  /**
   * Découpe le nom du fichier afin d'en extraire les informations utiles.
   */
  private void decouperNomFichier() {
    try {
      // Contrôle de la validité du fichier de contrôle
      if (fichierControle == null) {
        throw new MessageErreurException("Le nom du fichier est invalide.");
      }
      
      // Suppression du fichier avant son analyse
      if (fichierControle.exists()) {
        lectureContenu();
        fichierControle.delete();
      }
      
      // Contrôle de la structure du nom du fichier de contrôle
      String chaine = fichierControle.getNamewoExtension(false).trim().toLowerCase();
      if (chaine.isEmpty()) {
        throw new MessageErreurException(
            "Le nom du fichier de contrôle (" + chaine + ") n'est pas construit correctement, il manque le symbole '_'.");
      }
      int posd = chaine.indexOf('_');
      if (posd == -1) {
        throw new MessageErreurException(
            "Le nom du fichier de contrôle (" + chaine + ") n'est pas construit correctement, il manque le symbole '_'.");
      }
      if (posd == 0) {
        throw new MessageErreurException("Le nom du fichier de contrôle (" + chaine
            + ") n'est pas construit correctement, il manque le nom du module avant le symbole '_'.");
      }
      
      // Récupération du nom du module
      String nomModule = chaine.substring(0, posd).trim();
      idModule = IdModule.getInstance(nomModule);
      
      // Récupération du message
      posd++;
      String message = chaine.substring(posd);
      
      // Récupération de l'enumControleModule associé
      enumControleModule = EnumControleModule.valueOfByEnumEtTexte(idModule.getEnumModule(), message);
    }
    catch (Exception e) {
      Trace.erreur("Le message n'a pas pu être extrait du nom du fichier de contrôle.");
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Récupère les éventuels paramètres contenus dans le fichiers.
   * 
   * @return
   */
  private void lectureContenu() {
    if (fichierControle == null || fichierControle.length() == 0) {
      return;
    }
    GestionFichierTexte gft = new GestionFichierTexte(fichierControle);
    contenu = gft.getContenuFichierString(true);
  }
  
  // -- Accesseurs
  
  /**
   * Retourne l'identifiant du module pour le message.
   */
  public IdModule getIdModule() {
    return idModule;
  }
  
  /**
   * Retourne l'enumControleModule pour le message analysé.
   */
  public EnumControleModule getEnumControleModule() {
    return enumControleModule;
  }
  
  /**
   * Retourne les paramètres éventuels.
   * @return
   */
  public String getParametre() {
    return contenu;
  }
}
