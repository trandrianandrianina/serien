/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.module;

import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe associe l'instance d'un objet et un EnumModule représentant un module de l'application.
 */
public class Module extends AbstractClasseMetier<IdModule> {
  // Variables
  private Object instanceModule = null;
  
  /**
   * Constructeur.
   */
  public Module(IdModule pIdModule, Object pInstanceModule) {
    super(pIdModule);
    instanceModule = controlerInstanceObjet(pInstanceModule);
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne le texte du module.
   */
  @Override
  public String getTexte() {
    return getId().getTexte();
  }
  
  /**
   * Retourne l'instance du module.
   */
  public Object getInstanceModule() {
    return instanceModule;
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise le module au démarrage de l'application.
   */
  public void initialiser() {
    // S'il s'agit de l'application principale, il n'y a pas d'initialisation à faire
    if (((InterfaceModule) instanceModule).isApplication()) {
      return;
    }
    
    Trace.titre("MODULE " + getId());
    try {
      // Construction du nom du paramètre correspondant au demarrage pour le module en cours
      String nomParametre = getId().getEnumModule().getCode() + ".demarrage";
      EnumParametreModule enumParametreModule = EnumParametreModule.valueOfByCode(nomParametre);
      
      // Doit-on démarrer le module au démarrage de l'application ?
      Integer demarrerModule = ManagerParametreModule.retournerValeurEnInteger(enumParametreModule);
      if (demarrerModule == null || demarrerModule.intValue() == 0) {
        Trace.info("Le module " + getId().getEnumModule() + " ne sera pas démarré.");
        return;
      }
      
      // Démarre le module s'il n'est pas déjà démarré
      if (!((InterfaceModule) instanceModule).isDemarre()) {
        ((InterfaceModule) instanceModule).demarrer();
      }
    }
    catch (Exception e) {
      Trace.info("Le module " + getId().getEnumModule() + " n'a pas pu être démarré.");
    }
  }
  
  /**
   * Contrôle la validité de l'instance de l'objet.
   */
  private Object controlerInstanceObjet(Object pInstanceModule) {
    if (pInstanceModule == null) {
      throw new MessageErreurException("L'instance de l'objet du module est invalide.");
    }
    if (!(pInstanceModule instanceof InterfaceModule)) {
      throw new MessageErreurException("L'instance de l'objet du module n'implémente pas l'interface InterfaceModule.");
    }
    
    return pInstanceModule;
  }
  
}
