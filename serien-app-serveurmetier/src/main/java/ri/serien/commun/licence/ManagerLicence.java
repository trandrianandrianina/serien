/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.commun.licence;

import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.licence.SqlLicence;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libas400.system.valeursysteme.ValeurSysteme;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.licence.CritereLicence;
import ri.serien.libcommun.exploitation.licence.EnumStatutLicence;
import ri.serien.libcommun.exploitation.licence.IdLicence;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.licence.NombreUtilisateur;
import ri.serien.libcommun.exploitation.licence.SpecifiqueUtilisateur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.outils.MigrationLicenceVersV4_1;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe permet de centraliser la gestion des licences côté serveur.
 */
public class ManagerLicence {
  // Variables
  private static ListeLicence listeLicenceActive = null;
  
  // -- Méthodes publiques
  
  /**
   * Contrôle l'existence de la table des licences et son contenu.
   */
  public static void controlerTableLicence(QueryManager pQueryManager) {
    // Contrôle du paramètre
    if (pQueryManager == null) {
      throw new MessageErreurException("Le querymanager n'est pas initialisé.");
    }
    
    // Requête afin de voir si la table contient des enregistrements
    SqlLicence operation = new SqlLicence(pQueryManager);
    List<IdLicence> listeIdLicence = operation.chargerListeIdLicence(null);
    
    // Des enregistrements ont été trouvés donc la migration n'est pas lancée
    if (listeIdLicence != null && !listeIdLicence.isEmpty()) {
      return;
    }
    
    // Vérifie que la nouvelle gestion des licences soit active
    if (!nouvelleGestionLicenceActive()) {
      return;
    }
    
    // Lancement de la migration des licence à partir de l'ancienne table
    MigrationLicenceVersV4_1 migrationLicence = new MigrationLicenceVersV4_1(pQueryManager);
    migrationLicence.migrer();
  }
  
  /**
   * Charge la liste des licences actives avec la lettre d'environnement et le numéro de série du serveur.
   */
  public static void chargerListeLicenceActive(QueryManager pQueryManager) {
    if (ValeurSysteme.getIdServeurPower() == null) {
      throw new MessageErreurException("Le numéro de série du serveur est invalide.");
    }
    if (ManagerAS400.getLettreEnvironnement() == null) {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    
    // Préparation des critères de recherche des licences
    CritereLicence critere = new CritereLicence();
    critere.setIdServeurPower(ValeurSysteme.getIdServeurPower());
    critere.setLettreEnvironnement(ManagerAS400.getLettreEnvironnement());
    List<EnumStatutLicence> listeStatut = new ArrayList<EnumStatutLicence>();
    listeStatut.add(EnumStatutLicence.ACTIVE);
    critere.setListeStatutLicence(listeStatut);
    
    // Chargement des identifiants
    SqlLicence sqlLicence = new SqlLicence(pQueryManager);
    List<IdLicence> listeIdLicence = sqlLicence.chargerListeIdLicence(critere);
    if (listeIdLicence == null || listeIdLicence.isEmpty()) {
      listeLicenceActive = null;
      return;
    }
    
    // Chargement des licences à partir des identifiants
    ListeLicence listeLicence = new ListeLicence();
    for (IdLicence idLicence : listeIdLicence) {
      Licence licence = sqlLicence.chargerLicence(idLicence);
      if (licence != null) {
        listeLicence.add(licence);
      }
    }
    
    // Contrôle des licences lues
    int indice = 0;
    while (indice < listeLicence.size()) {
      Licence licence = listeLicence.get(indice);
      
      // La licence est elle valide ?
      if (!licence.dechiffrerCleLicence(false)) {
        // Le statut est changé en invalide si un problème de structure est détecté
        sqlLicence.changerStatutLicence(licence.getId(), EnumStatutLicence.INVALIDE);
        listeLicence.remove(licence);
        continue;
      }
      
      // La licence est elle encore active ? Le statut et la date de fin de validité sont contrôlés
      if (!licence.isActive()) {
        // Le statut est changé uniquement si le statut de la licence est active
        if (licence.getStatutLicence() == EnumStatutLicence.ACTIVE) {
          Trace.alerte("Le statut de la licence " + licence.getId().getTypeLicence().getLibelle() + " (" + licence.getId()
              + ") va être changé car la date de fin de validité est dépassé.");
          sqlLicence.changerStatutLicence(licence.getId(), EnumStatutLicence.INACTIVE);
        }
        listeLicence.remove(licence);
        continue;
      }
      indice++;
    }
    if (listeLicence.isEmpty()) {
      listeLicenceActive = null;
      return;
    }
    
    listeLicenceActive = listeLicence;
  }
  
  /**
   * Contrôle la licence utilisateurs.
   */
  public static void controlerLicenceUtilisateur(NombreUtilisateur pNombreUtilisateurActuel) {
    // Vérifie que la nouvelle gestion des licences soit active
    if (!nouvelleGestionLicenceActive()) {
      return;
    }
    
    // Récupération de la licence utilisateurs
    Licence licenceUtilisateur = null;
    if (listeLicenceActive != null) {
      licenceUtilisateur = listeLicenceActive.getLicenceUtilisateurActive();
    }
    
    // Contrôle sans licence active
    if (listeLicenceActive == null || licenceUtilisateur == null) {
      Trace.alerte("Il n'y a pas de licence utilisateur active donc seul 1 utilisateur peut se connecter.");
      // Un seul utilisateur est autorisé sans présence de licence utilisateur
      if (pNombreUtilisateurActuel.getTotal() > 1) {
        throw new MessageErreurException("Il y a " + pNombreUtilisateurActuel
            + " utilisateurs connectés actuellement.\nOr aucune licence active n'est présente donc seul 1 utilisateur peut se connecter.");
      }
      return;
    }
    
    // Contrôle avec licence active
    Integer nombreUtilisateurMax = ((SpecifiqueUtilisateur) licenceUtilisateur.getSpecifiqueLicence()).getNombreUtilisateur();
    if (nombreUtilisateurMax == null || nombreUtilisateurMax.intValue() < pNombreUtilisateurActuel.getTotal()) {
      throw new MessageErreurException("Il y a " + pNombreUtilisateurActuel.getTotal()
          + " utilisateurs connectés actuellement.\nOr votre licence utilisateur vous donne droit à " + nombreUtilisateurMax
          + " utilisateurs connectés simultanément.");
    }
  }
  
  /**
   * Contrôle la licence pour la gestion commerciale.
   */
  public static void controlerLicenceGestionCommerciale() {
    // Vérifie que la nouvelle gestion des licences soit active
    if (!nouvelleGestionLicenceActive()) {
      return;
    }
    
    // Récupération de la licence pour la gestion commerciale
    Licence licenceGestionCommerciale = null;
    if (listeLicenceActive != null) {
      licenceGestionCommerciale = listeLicenceActive.getLicenceGestionCommercialeActive();
    }
    
    // Contrôle de la licence active
    if (listeLicenceActive == null || licenceGestionCommerciale == null) {
      throw new MessageErreurException("Vous n'avez pas de licence active pour la gestion commerciale."
          + "\nVeuillez contacter notre service commercial si vous souhaitez activer la gestion commerciale sur votre système.");
    }
  }
  
  /**
   * Contrôle la licence pour la comptabilité.
   */
  public static void controlerLicenceComptabilite() {
    // Vérifie que la nouvelle gestion des licences soit active
    if (!nouvelleGestionLicenceActive()) {
      return;
    }
    
    // Récupération de la licence pour la comptabilité
    Licence licenceGestionComptabilite = null;
    if (listeLicenceActive != null) {
      licenceGestionComptabilite = listeLicenceActive.getLicenceComptabiliteActive();
    }
    
    // Contrôle de la licence active
    if (listeLicenceActive == null || licenceGestionComptabilite == null) {
      throw new MessageErreurException("Vous n'avez pas de licence active pour la comptabilité."
          + "\nVeuillez contacter notre service commercial si vous souhaitez activer la comptabilité sur votre système.");
    }
  }
  
  /**
   * Retourne une liste des descriptions des licences actives qui sont bientôt en fin de validité.
   */
  public static List<String> retournerListeDescriptionLicenceFinValiditeProche(int pNombreJourRestants) {
    if (listeLicenceActive == null || listeLicenceActive.isEmpty()) {
      return null;
    }
    
    List<String> listeDescription = new ArrayList<String>();
    for (Licence licence : listeLicenceActive) {
      if (licence.isProcheFinValidite(pNombreJourRestants)) {
        listeDescription.add(licence.getDescription());
      }
    }
    if (listeDescription.isEmpty()) {
      return null;
    }
    return listeDescription;
  }
  
  /**
   * Indique si la nouvelle gestion des licences est active.
   * Pour être activer les nouvelles licences :
   * - la datararea PNEWLIC doit exister dans la bibliothèque d'environnement,
   * - la datararea PNEWLIC contient '1'.
   */
  public static boolean nouvelleGestionLicenceActive() {
    String contenu = null;
    try {
      // La dataarea pour la nouvelle gestion des licences
      Dataarea dataarea = new Dataarea();
      dataarea.setNom("PNEWLIC");
      dataarea.setBibliotheque(ManagerAS400.getBibliothequeEnvironnement());
      
      // Lecture du contenu
      GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(ManagerAS400.getSystemeAS400());
      contenu = gestionDataareaAS400.chargerDataarea(dataarea);
    }
    catch (Exception e) {
    }
    
    // Contrôle si les nouvelles licences sont actives
    contenu = Constantes.normerTexte(contenu);
    if (contenu.equals("1")) {
      Trace.alerte("La gestion des nouvelles licences est active.");
      return true;
    }
    Trace.alerte("La gestion des nouvelles licences n'est pas active.");
    return false;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des licences actives.
   */
  public static ListeLicence getListeLicenceActive() {
    return listeLicenceActive;
  }
  
}
