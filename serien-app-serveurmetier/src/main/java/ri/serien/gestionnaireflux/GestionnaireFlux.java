/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.gestionnaireflux;

import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;

import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.Avertissement;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.ManagerFluxMagentoASurveiller;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.exploitation.notification.EnumPrioriteNotification;
import ri.serien.libcommun.exploitation.notification.ParametreCreationNotification;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierINI;
import ri.serien.others.axis2.ServeurAxis2;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurmetier.serveur.ServeurSerieN;

/**
 * Cette classe regroupe les méthodes pour gérer le gestionnaire de flux.
 */
public class GestionnaireFlux implements InterfaceModule {
  // Constantes
  private final static String NAME_FILE_CONF = "msflux.ini";
  private final static String KEY_DELAY = "delai";
  private final static String KEY_STATUS = "status";
  private final static String KEY_LETTREENV = "LETTREENV";
  private final static String KEY_MAILSRVALIAS = "mailsrvalias";
  private final static String KEY_MAILTOALIAS = "mailtoalias";
  private final static String KEY_MAILSUBJECT = "mailsubject";
  private final static String KEY_URL_MAGENTO = "urlMagento";
  private static final String KEY_FORCAGE_BIB_WS = "forcageBibWs";
  private static final String KEY_VERSION_FLUX = "versionFlux";
  
  // Génération des notifications
  private final static String SUJET_DEMARRAGE_FLUX = "Démarrage superviseur flux";
  private final static String MESSAGE_DEMARRAGE_FLUX = "Le superviseur de flux a démarré.";
  private final static String SUJET_ARRET_FLUX = "Arrêt superviseur flux";
  private final static String MESSAGE_ARRET_FLUX = "Le superviseur de flux a été arrêté.";
  
  // Variables
  private ServeurAxis2 serveurAxis2 = null;
  private ArrayList<FluxSuperviseur> listeFluxSuperviseur = new ArrayList<FluxSuperviseur>();
  private SurveillanceJob surveillanceJob = null;
  
  private static EnumModule enumModule = EnumModule.GESTIONNAIREFLUX;
  private static boolean moduleActif = false;
  private static ListeParametreModule listeParametre = null;
  
  // Déclarations pour la gestion des notifications
  private static QueryManager queryManager = null;
  private static SystemeManager systemeManager = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return listeParametre;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return false;
  }
  
  /**
   * Démarre le module Gestionnaire de flux.
   */
  @Override
  public void demarrer() {
    if (moduleActif) {
      Trace.alerte("Le module " + enumModule + " est déjà actif.");
      return;
    }
    Trace.info("Démarrage du module " + enumModule + '.');
    
    // Initialisation et contrôle des paramètres
    initialiserParametre();
    
    // Démarrage du serveur Axis2 si nécessaire
    startAxis2Server();
    
    // Démarrage des superviseurs de flux
    demarrerListeSuperviseurFlux();
    
    moduleActif = true;
    Trace.info("Le module " + enumModule + " est démarré.");
    
    // Connexion à DB2
    initialiserConnexionDB2();
    // Générer la notification à envoyer
    genererNotification(SUJET_DEMARRAGE_FLUX, MESSAGE_DEMARRAGE_FLUX);
    // Déconnexion de la DB2
    deconnecterDB2();
  }
  
  /**
   * Arrête le module Gestionnaire de flux.
   */
  @Override
  public void arreter() {
    if (!moduleActif) {
      Trace.alerte("Le module " + enumModule + " n'est pas actif.");
      return;
    }
    
    // Arrêt des superviseurs de flux
    arreterListeSuperviseurFlux();
    
    // Arrêt du serveur Axis
    stopAxis2Server();
    
    moduleActif = false;
    Trace.info("Module " + enumModule + " stoppé.");
    
    // Connexion à DB2
    initialiserConnexionDB2();
    // Générer la notification à envoyer
    genererNotification(SUJET_ARRET_FLUX, MESSAGE_ARRET_FLUX);
    // Déconnexion de la DB2
    deconnecterDB2();
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return moduleActif;
  }
  
  /**
   * Envoi un ordre au module Gestionnaire de flux.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + enumModule + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case GESTIONNAIREFLUX_START:
          // Recherche les paramètres pour ce module dans le cas où ils auraient été modifié
          ManagerParametreModule.rechargerParametre(pEnumControleModule.getEnumModule());
          // Démarre le module
          demarrer();
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return null;
        
        case GESTIONNAIREFLUX_STOP:
          arreter();
          return null;
        
        case GESTIONNAIREFLUX_STATUS:
          String retour;
          if (moduleActif) {
            retour = "Le module " + enumModule + " est démarré.";
          }
          else {
            retour = "Le module " + enumModule + " est arrêté.";
          }
          Trace.info(retour);
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return retour;
        
        case GESTIONNAIREFLUX_FORCEFLUX:
          forceDeclenchementTypeFlux(pParametre);
          return null;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + enumModule + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise la connexion à DB2.
   */
  private void initialiserConnexionDB2() {
    systemeManager = new SystemeManager(ManagerAS400.getSystemeAS400(), true);
    if (systemeManager == null) {
      throw new MessageErreurException("Erreur lors de l'initialisation de la connexion à DB2.");
    }
    Connection connection = systemeManager.getDatabaseManager().getConnection();
    queryManager = new QueryManager(connection);
    queryManager.setLibrairieEnvironnement(ManagerAS400.getBibliothequeEnvironnement().getNom());
    
    Trace.info("Connexion à DB2 réussie.");
  }
  
  /**
   * Déconnecte la connexion à DB2.
   */
  private void deconnecterDB2() {
    if (queryManager == null) {
      return;
    }
    queryManager.deconnecter();
  }
  
  // Gestion des notifications pour démarrage et arrêt du gestionnaire de flux
  
  /**
   * Générer notification
   * @param pMessage
   * @param pSujet
   */
  public void genererNotification(String pSujet, String pMessage) {
    try {
      // Initialisation des paramètres
      ParametreCreationNotification parametreCreationNotification = new ParametreCreationNotification();
      parametreCreationNotification.setPriorite(EnumPrioriteNotification.NORMALE);
      parametreCreationNotification.setSujet(pSujet);
      parametreCreationNotification.setMessage(pMessage);
      
      // Envoi de la notification
      Avertissement.envoyerNotification(systemeManager, queryManager, parametreCreationNotification);
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Initialiser les paramètres du module à partir de la liste des paramètres pour ce module.
   */
  private void initialiserParametre() {
    // Extraction des paramètres spécifiques pour ce module
    listeParametre = ManagerParametreModule.extraireParametreModule(enumModule);
    if (listeParametre == null || listeParametre.isEmpty()) {
      throw new MessageErreurException("Aucun paramètre pour le module " + enumModule + " n'a été trouvé.");
    }
  }
  
  /**
   * Charge les données depuis le fichier msflux.ini.
   */
  private void chargerDepuisFichierIni() {
    // Chargement du fichier de conf
    GestionFichierINI gfi = new GestionFichierINI(ManagerAS400.getDossierRacineServeur() + File.separatorChar + NAME_FILE_CONF);
    LinkedHashMap<String, LinkedHashMap<String, String>> listeSuperviseurs = gfi.getSections();
    if (listeSuperviseurs.isEmpty()) {
      Trace.info("Le fichier " + NAME_FILE_CONF + " est vide ou n'a pas été trouvé.");
      return;
    }
    Trace.info("Lecture des données du fichier " + gfi.getNomFichier());
    
    // Création de la liste des superviseurs de flux
    for (Entry<String, LinkedHashMap<String, String>> entry : listeSuperviseurs.entrySet()) {
      FluxSuperviseur fluxSuperviseur = new FluxSuperviseur();
      fluxSuperviseur.setCurlib(entry.getKey());
      HashMap<String, String> map = entry.getValue();
      for (Entry<String, String> entryvalue : map.entrySet()) {
        // Récupération du délai
        if (entryvalue.getKey().equalsIgnoreCase(KEY_DELAY)) {
          fluxSuperviseur.setDelaiEnSeconde(Integer.parseInt(entryvalue.getValue().trim()));
        }
        // Récupération du status
        else if (entryvalue.getKey().equalsIgnoreCase(KEY_STATUS)) {
          fluxSuperviseur.setEnabled(entryvalue.getValue().trim());
        }
        // Récupération de la lettre environnement
        else if (entryvalue.getKey().equalsIgnoreCase(KEY_LETTREENV)) {
          fluxSuperviseur.setLettreEnvironnement(entryvalue.getValue().trim());
        }
        // Force la bib de travail pour l'appel à nos web services (écrase la bib passée dans l'appel
        else if (entryvalue.getKey().equalsIgnoreCase(KEY_FORCAGE_BIB_WS)) {
          FluxSuperviseur.bibForcee = entryvalue.getValue().trim();
        }
        // Récupère la version des flux
        else if (entryvalue.getKey().equalsIgnoreCase(KEY_VERSION_FLUX)) {
          try {
            fluxSuperviseur.setVersionFlux(entryvalue.getValue().trim());
            Trace.info("Version du flux : " + fluxSuperviseur.getVersionFlux());
          }
          catch (Exception e) {
            Trace.erreur("La version du flux n'a pas pu être initialisée.");
          }
        }
      }
      listeFluxSuperviseur.add(fluxSuperviseur);
    }
  }
  
  /**
   * Démarrage des superviseurs de flux.
   */
  private void demarrerListeSuperviseurFlux() {
    Trace.info("Démarrer les superviseurs de flux.");
    // Chargement des données à partir du fichier msflux.ini
    chargerDepuisFichierIni();
    if (listeFluxSuperviseur.isEmpty()) {
      Trace.info("Il n'y a aucun superviseur de flux.");
      return;
    }
    
    // Démarrage de la thread dédiée à la surveillance des jobs AS400
    if (surveillanceJob == null) {
      // Initialisation de la surveillance des job et des flux
      surveillanceJob = new SurveillanceJob(ManagerAS400.getBibliothequeEnvironnement());
      surveillanceJob.demarrer();
      FluxSuperviseur.setSurveillanceJob(surveillanceJob);
    }
    
    // Démarrage des superviseurs de flux un par un. Il y a un superviseur par base de données.
    int cpt = 0;
    for (FluxSuperviseur fluxSuperviseur : listeFluxSuperviseur) {
      if (fluxSuperviseur.demarrer()) {
        cpt++;
      }
    }
    
    // Création de la liste des base de données après le démarrage des surperviseurs
    List<ManagerFluxMagentoASurveiller> listeManagerFluxMagentoASurveiller = new ArrayList<ManagerFluxMagentoASurveiller>();
    for (FluxSuperviseur fluxSuperviseur : listeFluxSuperviseur) {
      if (!fluxSuperviseur.isEnabled()) {
        continue;
      }
      ManagerFluxMagentoASurveiller managerFluxMagentoASurveiller =
          new ManagerFluxMagentoASurveiller(fluxSuperviseur.getManagerFluxMagento());
      listeManagerFluxMagentoASurveiller.add(managerFluxMagentoASurveiller);
    }
    if (surveillanceJob != null) {
      surveillanceJob.setListeManagerFluxMagentoASurveiller(listeManagerFluxMagentoASurveiller);
    }
    
    Trace.info(cpt + " superviseurs de flux ont été démarrés.");
  }
  
  /**
   * Arrêt des superviseurs de flux.
   */
  private void arreterListeSuperviseurFlux() {
    Trace.info("Arrêter les superviseurs de flux");
    if (listeFluxSuperviseur.isEmpty()) {
      Trace.info("Il n'y a aucun superviseur de flux.");
      return;
    }
    
    // Arrêt de la thread dédiée à la surveillance des jobs AS400
    if (surveillanceJob != null) {
      surveillanceJob.arreter();
    }
    
    // Arrêt des superviseurs de flux un par un
    for (FluxSuperviseur fluxSuperviseur : listeFluxSuperviseur) {
      fluxSuperviseur.arreter();
    }
    
    // Vide la liste des superviseurs de flux
    viderListeSuperviseurFlux();
  }
  
  /**
   * Supprime la liste des superviseurs de flux.
   */
  private void viderListeSuperviseurFlux() {
    if (listeFluxSuperviseur != null) {
      listeFluxSuperviseur.clear();
    }
  }
  
  /**
   * Force le déclenchement d'un type de flux en V3.
   */
  private void forceDeclenchementTypeFlux(Object pParametre) {
    try {
      if (pParametre == null || !(pParametre instanceof String)) {
        Trace.alerte("Le paramètre pour le déclenchement du type de flux est invalide.");
        return;
      }
      // Découpage du paramètre
      String[] parametre = Constantes.splitString((String) pParametre, '|');
      if (parametre.length != 2) {
        Trace.erreur("Il manque un paramètre pour forcer le déclenchement du type d'un flux.");
        return;
      }
      // Contrôle des paramètres
      // La base de données
      IdBibliotheque idBaseDeDonnees = IdBibliotheque.getInstance(Constantes.normerTexte(parametre[0]));
      
      // Le type de flux
      EnumTypeFlux enumTypeFlux = EnumTypeFlux.valueOfByCode(Constantes.normerTexte(parametre[1]));
      
      Trace.info(
          "Une demande de déclenchement du type flux " + enumTypeFlux.getCode() + " de la base de données " + idBaseDeDonnees.getNom());
      // Recherche du superviseur concerné
      for (FluxSuperviseur fluxSuperviseur : listeFluxSuperviseur) {
        if (!fluxSuperviseur.isEnabled()) {
          continue;
        }
        if (idBaseDeDonnees.equals(fluxSuperviseur.getBaseDeDonnees().getId())) {
          fluxSuperviseur.forcerDeclenchementTypeFlux(enumTypeFlux);
          return;
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
  
  // --- Gestion du serveur Axis (il faudra voir à terme si l'on ne le tranforme pas en module)
  
  /**
   * Démarrage du serveur Axis2.
   */
  private void startAxis2Server() {
    if (serveurAxis2 == null) {
      String dossierRacineTiers = searchPathOthersX(ManagerAS400.getDossierRacineServeur(), ServeurAxis2.RADICAL);
      if (dossierRacineTiers == null) {
        throw new MessageErreurException("Impossible de demarrer le serveur Axis car le dossier du serveur Axis2 n'a pas ete trouve.");
      }
      if (updateClasspath(dossierRacineTiers)) {
        // Ajout de log4j au classpath pour Axis2
        try {
          addPath(dossierRacineTiers + File.separatorChar + "conf");
        }
        catch (Exception e) {
          Trace.erreur(e, "Impossible de demarrer le serveur Axis");
        }
        serveurAxis2 = new ServeurAxis2(dossierRacineTiers);
      }
    }
    if (serveurAxis2.getState() == ServeurAxis2.STT_STARTED) {
      return;
    }
    
    serveurAxis2.startServer();
  }
  
  /**
   * Arrêt du serveur Axis2
   */
  private void stopAxis2Server() {
    if (serveurAxis2 == null) {
      return;
    }
    if (serveurAxis2.getState() == ServeurAxis2.STT_STOPPED) {
      return;
    }
    
    serveurAxis2.stopServer();
  }
  
  /**
   * Retourne le nom complet du dossier contenant une application tiers.
   * @param pRootpath
   * @return
   */
  private String searchPathOthersX(String pRootpath, final String pRadical) {
    if (pRootpath == null) {
      return null;
    }
    
    File fpath = new File(pRootpath + File.separatorChar + Constantes.DOSSIER_OTHERS);
    String[] list = fpath.list(new FilenameFilter() {
      // @Override
      @Override
      public boolean accept(File dir, String name) {
        return dir.isDirectory() && name.startsWith(pRadical);
      }
    });
    
    if (list.length > 0) {
      return fpath.getAbsolutePath() + File.separatorChar + list[0];
    }
    return null;
  }
  
  /**
   * Permet d'ajouter un jar dans le classloader courant.
   * @param s
   * @throws Exception
   */
  private static void addPath(String s) throws Exception {
    File f = new File(s);
    URL u = f.toURI().toURL();
    URLClassLoader urlClassLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    Class<URLClassLoader> urlClass = URLClassLoader.class;
    Method method = urlClass.getDeclaredMethod("addURL", new Class[] { URL.class });
    method.setAccessible(true);
    method.invoke(urlClassLoader, new Object[] { u });
  }
  
  /**
   * Met à jour le classpath du classloader avec l'application tierce.
   * @return
   */
  private boolean updateClasspath(String pDossierRacineTiers) {
    try {
      File dlib = new File(pDossierRacineTiers + File.separatorChar + "lib");
      File[] jarfiles = dlib.listFiles(new FileFilter() {
        // @Override
        @Override
        public boolean accept(File pathname) {
          return pathname.getAbsolutePath().endsWith(ServeurSerieN.EXTENSION_JAR);
        }
      });
      if (jarfiles != null) {
        Trace.info("Ajout des jar de l'application tierce (" + pDossierRacineTiers + ") dans le classpath:");
        for (File file : jarfiles) {
          addPath(file.getAbsolutePath());
          Trace.info(file.getAbsolutePath());
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la mise à jour du classpath.");
      return false;
    }
    
    return true;
  }
  
}
