/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.gestionnaireflux;

import java.sql.Timestamp;
import java.util.Calendar;

import ri.serien.libas400.dao.sql.gescom.flux.ManagerFluxAutomatique;
import ri.serien.libas400.dao.sql.gescom.flux.ParametresFluxBibli;
import ri.serien.libas400.dao.sql.gescom.flux.commun.ManagerFluxMagento;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.job.SurveillanceJob;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.flux.EnumStatutFlux;
import ri.serien.libcommun.exploitation.flux.EnumTypeFlux;
import ri.serien.libcommun.outils.Trace;

/**
 * Cette classe décrit la traitement d'un superviseur associé à une base de données.
 */
public class FluxSuperviseur extends Thread {
  // Constantes
  // Délai en seconde
  private final static int DELAY_DEFAULT = 10;
  private final static int STT_CODE_STOPPED = 0;
  private final static int STT_CODE_STARTED = 1;
  private final static String STT_TEXT_ENABLED = "Actif";
  
  // Variables
  private int delaiLogsEnJour = 10;
  private String dossierRacine = null;
  private Bibliotheque baseDeDonnees = null;
  private int delaiEnSeconde = DELAY_DEFAULT;
  private int statut = STT_CODE_STOPPED;
  private boolean enabled = false;
  private String lettreEnvironnement = null;
  private String versionFlux = "1.0";
  private ManagerFluxMagento daoFluxMagento = null;
  private boolean go = false;
  // Gestionnaire de flux automatiques à intervalles réguliers
  private ManagerFluxAutomatique managerFluxAutomatique = null;
  private int idSuperviseur = 0;
  private ParametresFluxBibli parametresBibli = null;
  private EnumTypeFlux typeFluxForcerADeclencher = null;
  
  public static String bibForcee = null;
  private static int nombreSuperviseur = 0;
  private static SurveillanceJob surveillanceJob = null;
  
  /**
   * Constructeur
   */
  public FluxSuperviseur() {
    nombreSuperviseur++;
    idSuperviseur = nombreSuperviseur;
    
    setName("Superviseur Flux " + idSuperviseur);
  }
  
  // -- Méthodes publiques
  
  /**
   * Démarrage du superviseur.
   */
  public boolean demarrer() {
    if (!enabled) {
      return false;
    }
    
    Trace.info("Démarrer le superviseur");
    
    // Initialisation des parametres propres aux flux
    parametresBibli = ManagerFluxMagento.initParametres(baseDeDonnees);
    parametresBibli.setLettreEnvironnement(lettreEnvironnement);
    parametresBibli.setLibrairie(baseDeDonnees);
    parametresBibli.setVersionFlux(versionFlux);
    
    // Initialisation de la surveillance des jobs
    if (parametresBibli != null && surveillanceJob != null) {
      surveillanceJob.ajouterListeDestinataireNotification(baseDeDonnees, parametresBibli.getListeDestinataireNotification());
      surveillanceJob.ajouterListeDestinataireMail(baseDeDonnees, parametresBibli.getMailDestinataireErreurs());
    }
    
    go = true;
    start();
    statut = STT_CODE_STARTED;
    
    // Suppression des vieux flux de la liste de flux du fichier PSEMHFLXM
    supprimerAncienFlux(baseDeDonnees);
    
    // Lance aussi l'écouteur automatique des flux après une petite pause afin d'éviter la lecture simultanée du paramètre 'FX'
    try {
      Thread.sleep(DELAY_DEFAULT * 1000);
    }
    catch (Exception e) {
    }
    managerFluxAutomatique = new ManagerFluxAutomatique(parametresBibli);
    
    return true;
  }
  
  /**
   * Arrêt du superviseur.
   */
  public void arreter() {
    if (!enabled) {
      return;
    }
    
    go = false;
    Trace.info("Arrêter le superviseur");
    statut = STT_CODE_STOPPED;
  }
  
  /**
   * Traitements
   */
  @Override
  public void run() {
    String nomThread = String.format("FluxSuperviseur(%03d)-surveiller-%s", Thread.currentThread().getId(), baseDeDonnees);
    setName(nomThread);
    
    int count = 0;
    int i = 0;
    
    // Initialisation de la logique applicative
    daoFluxMagento = new ManagerFluxMagento(parametresBibli);
    if (ManagerFluxMagento.getSurveillanceJob() == null) {
      ManagerFluxMagento.setSurveillanceJob(surveillanceJob);
    }
    
    while (go) {
      try {
        // Contrôle s'il y a une demande de déclenchement forcé d'un flux
        if (typeFluxForcerADeclencher != null && managerFluxAutomatique != null) {
          managerFluxAutomatique.forcerDeclenchementFluxAutomatique(typeFluxForcerADeclencher);
        }
        
        // Traite les flux reçus de Magento
        if (go && daoFluxMagento.controlerStatutBaseDeDonnees()) {
          count = daoFluxMagento.traiterFluxRecuDepuisMagento();
          if (count < 0) {
            Trace.erreur(daoFluxMagento.getMsgError());
          }
        }
        
        // Traite les flux à envoyer vers Magento ou SFCC
        if (go && daoFluxMagento.controlerStatutBaseDeDonnees()) {
          i = daoFluxMagento.traiterFluxAenvoyerVersMagento(null, typeFluxForcerADeclencher);
          if (i < 0) {
            Trace.erreur(daoFluxMagento.getMsgError());
          }
          count += i;
        }
        
        // Traite les flux à envoyer vers EDI
        if (go && daoFluxMagento.controlerStatutBaseDeDonnees()) {
          i = daoFluxMagento.traiterFluxEDI();
          if (i < 0) {
            Trace.erreur(daoFluxMagento.getMsgError());
          }
          count += i;
        }
        
        // Remise à null du type de flux à forcer le déclenchement
        typeFluxForcerADeclencher = null;
        
        // Pause durant le délai imparti s'il n'y a pas eu de flux à traiter
        Thread.sleep(getDelaiEnSeconde() * 1000);
        Trace.alerte("Fin de boucle thread flux superviseur.");
      }
      catch (Exception e) {
        Trace.erreur(e, "Debug");
        Thread.currentThread().interrupt();
        // Arrêt de la thread du superviseur de flux
        arreter();
        Trace.alerte("Le thread du superviseur de flux (" + getName() + ") a été arrêté.");
      }
    }
    // Libère les ressources (ne pas mettre dans le dispose)
    daoFluxMagento.dispose();
  }
  
  /**
   * Force le déclenchement d'un type de flux.
   */
  public void forcerDeclenchementTypeFlux(EnumTypeFlux pTypeFluxForcerADeclencher) {
    if (pTypeFluxForcerADeclencher == null) {
      return;
    }
    typeFluxForcerADeclencher = pTypeFluxForcerADeclencher;
  }
  
  // -- Méthodes privées
  
  /**
   * Supprime les anciens flux.
   * Afin de maitriser la taille de la liste de flux dans la table PSEMHFLXM les anciens flux antérieurs à x jours d'archivage sont
   * supprimés.
   * Où x = NBJOUR_FLUX_MAX dans la table PSEMFXPA
   */
  private int supprimerAncienFlux(Bibliotheque pBaseDeDonnees) {
    int resultat = -1;
    try {
      SystemeManager systemeManager = new SystemeManager(true);
      QueryManager queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
      queryManager.setLibrary(pBaseDeDonnees.getNom());
      
      // 1 jour en Millisecond = (1000 * 60 * 60 * 24 * NBJOURS) 10 jours par défaut
      long nbJoursLogsMax = 864000000;
      long valeurParametre = parametresBibli.getNbJoursMaxFlux();
      
      if (valeurParametre > 0) {
        nbJoursLogsMax = 86400000 * valeurParametre;
      }
      
      Timestamp dateLimite = new Timestamp(Calendar.getInstance().getTimeInMillis() - nbJoursLogsMax);
      
      // Suppression des flux inférieurs à la date fixée et en statut traité / à ne pas traiter
      // @formatter:off
      resultat = queryManager
          .requete("DELETE FROM " + queryManager.getLibrary() + "." + EnumTableBDD.FLUX + " WHERE FLTRT < '" + dateLimite + "'"
              + " AND (FLSTT= " + EnumStatutFlux.TRAITE.getNumero() + " OR FLSTT= " + EnumStatutFlux.NE_PAS_TRAITER.getNumero() + ")");
      // @formatter:on
      Trace.info("Supprimer les anciens flux : nbSupression=" + resultat + " dateLimite=" + dateLimite);
      return resultat;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la suppression des anciens flux");
    }
    
    return resultat;
  }
  
  // -- Accesseurs
  
  /**
   * Initialise la base de données.
   * @param pCurlib la curlib à définir
   */
  public void setCurlib(String pCurlib) {
    if (pCurlib != null) {
      IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(pCurlib);
      this.baseDeDonnees = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.BASE_DE_DONNEES);
    }
  }
  
  public int getDelaiEnSeconde() {
    return delaiEnSeconde;
  }
  
  public void setDelaiEnSeconde(int pDelaiEnSeconde) {
    this.delaiEnSeconde = pDelaiEnSeconde;
  }
  
  public boolean isEnabled() {
    return enabled;
  }
  
  public void setEnabled(boolean pEnabled) {
    this.enabled = pEnabled;
  }
  
  public void setEnabled(String pEnabled) {
    if (pEnabled == null) {
      return;
    }
    
    if (pEnabled.equalsIgnoreCase(STT_TEXT_ENABLED)) {
      this.setEnabled(true);
    }
    else {
      this.setEnabled(false);
    }
  }
  
  public void setLettreEnvironnement(String lettreEnvironnement) {
    this.lettreEnvironnement = lettreEnvironnement;
  }
  
  public String getVersionFlux() {
    return versionFlux;
  }
  
  public void setVersionFlux(String versionFlux) {
    this.versionFlux = versionFlux;
  }
  
  public static SurveillanceJob getSurveillanceJob() {
    return surveillanceJob;
  }
  
  public static void setSurveillanceJob(SurveillanceJob surveillanceJob) {
    FluxSuperviseur.surveillanceJob = surveillanceJob;
  }
  
  public ManagerFluxMagento getManagerFluxMagento() {
    return daoFluxMagento;
  }
  
  public Bibliotheque getBaseDeDonnees() {
    return baseDeDonnees;
  }
  
}
