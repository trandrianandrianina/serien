/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.serveur;

import java.io.File;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.MessageQueue;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.system.EnvironnementExecution;
import ri.serien.libas400.system.valeursysteme.ValeurSysteme;
import ri.serien.libcommun.exploitation.IdServeurPower;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.outils.Constantes;

public class ManagerAS400 {
  // Variables
  private static AS400 systeme = new AS400();
  private static MessageQueue messageQueue = null;
  private static String dossierRacineServeur = File.separator + Constantes.DOSSIER_RACINE;
  private static ValeurSysteme valeurSysteme = null;
  private static Bibliotheque bibliothequeEnvironnement = null;
  private static ListeLicence listeLicenceActive = null;
  
  /**
   * Retourne le système.
   */
  public static AS400 getSystemeAS400() {
    return systeme;
  }
  
  // -- Méthodes publiques
  
  /**
   * Charge les valeurs systèmes.
   */
  public static void chargerValeurSysteme(boolean pForcerChargement) {
    if (valeurSysteme == null || pForcerChargement) {
      valeurSysteme = new ValeurSysteme(systeme);
    }
  }
  
  // -- Accesseurs
  /**
   * Retourne la liste des messages.
   */
  public static MessageQueue getMessageQueue() {
    if (messageQueue == null) {
      messageQueue = new MessageQueue(ManagerAS400.getSystemeAS400(), QSYSObjectPathName.toPath("QSYS", "QSYSOPR", "MSGQ"));
    }
    return messageQueue;
  }
  
  /**
   * Retourne le dossier racine.
   */
  public static String getDossierRacineServeur() {
    return dossierRacineServeur;
  }
  
  /**
   * Initialise le dossier racine.
   */
  public static void setDossierRacine(String pDossierRacine) {
    ManagerAS400.dossierRacineServeur = pDossierRacine;
  }
  
  /**
   * Retourne la lettre d'environnement.
   */
  public static Character getLettreEnvironnement() {
    return EnvironnementExecution.getLettreEnvironnement();
  }
  
  /**
   * Initialise la lettre d'environnement.
   */
  public static void setLettreEnvironnement(Character lettreEnvironnement) {
    // Initialise les noms des bibliothèques programmes en même temps
    EnvironnementExecution.initialiserNomBibliotheques(lettreEnvironnement);
  }
  
  /**
   * Retourne la liste des licences actives.
   */
  public static ListeLicence getListeLicenceActive() {
    return listeLicenceActive;
  }
  
  /**
   * Initialise la liste des licences actives.
   */
  public static void setListeLicenceActive(ListeLicence listeLicenceActive) {
    ManagerAS400.listeLicenceActive = listeLicenceActive;
  }
  
  /**
   * Retourne l'identifiant du serveur.
   */
  public static IdServeurPower getIdServeurPower() {
    chargerValeurSysteme(false);
    return ValeurSysteme.getIdServeurPower();
  }
  
  /**
   * Retourne la bibliothèque d'environnement.
   */
  public static Bibliotheque getBibliothequeEnvironnement() {
    if (bibliothequeEnvironnement == null) {
      String nomBibliotheque =
          ManagerParametreModule.retournerValeurEnString(EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT);
      bibliothequeEnvironnement = new Bibliotheque(IdBibliotheque.getInstance(nomBibliotheque), EnumTypeBibliotheque.SYSTEME_SERIEN);
      
    }
    
    return bibliothequeEnvironnement;
  }
  
}
