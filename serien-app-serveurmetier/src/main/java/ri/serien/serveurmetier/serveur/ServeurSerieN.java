/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.serveur;

import java.io.File;
import java.io.FilenameFilter;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;

import ri.serien.commun.licence.ManagerLicence;
import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.dao.sql.exploitation.environnement.SqlEnvironnement;
import ri.serien.libas400.dao.sql.exploitation.notification.SqlNotification;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.outils.NettoyerServeur;
import ri.serien.serveurrmi.ManagerServeurRMI;

/**
 * Classe de démarrage su serveur métier Série N.
 */
public class ServeurSerieN implements InterfaceModule {
  // Constnates
  private final static String NOM_LOGICIEL = "SERIE N SERVEUR METIER";
  public final static String VERSION = "4.1";
  public final static String EXTENSION_JAR = ".jar";
  
  // Variables
  private String parametresLigneCommande = null;
  private boolean modeDebug = false;
  private static ListeSousEnvironnement listeSousEnvironnement = null;
  
  /**
   * Constructeur.
   */
  public ServeurSerieN(String pParametres) {
    parametresLigneCommande = pParametres;
  }
  
  // -- Méthodes publiques
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return null;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return true;
  }
  
  /**
   * Démarre le module.
   */
  @Override
  public void demarrer() {
    // Initialisation du querymanager permettant l'accès à la base de données
    SystemeManager systemManager = new SystemeManager(ManagerAS400.getSystemeAS400(), true);
    QueryManager queryManager = new QueryManager(systemManager.getDatabaseManager().getConnection());
    
    // Analyse et traitement des paramètres en ligne de commande
    initialiserParametre(queryManager, parametresLigneCommande);
    
    // Activer le mode débug des traces
    Trace.setModeDebug(modeDebug);
    
    // Balayage du dossier des RT au démarrage du serveur (doit être fait en premier)
    Trace.titre("INITIALISER SOUS-SYSTEMES");
    initialiserListeSousSysteme(queryManager);
    
    // Opérations de contrôle, de nettoyage et de migration au démarrage du serveur
    Trace.titre("INITIALISER FICHIERS");
    initialiserEnvironnement();
    
    // Chargement des licences actives
    Trace.titre("INITIALISER LICENCES");
    initialiserListeLicence(queryManager);
    
    // Initialisation et activation de l'ensemble des modules
    Trace.titre("INITIALISER LES MODULES");
    ManagerModule.initialiserModule(this);
    
    // Tracer les informations intéressantes du serveur (en fin d'initialisation car les paramètres sont censés être correctement
    // initialisés)
    tracerInformationsServeur();
    
    // Opérations sur les notifications
    Trace.titre("INITIALISER NOTIFICATIONS");
    initialiserNotification(queryManager);
    
    // Initialisation du servur terminée
    Trace.titre("FIN DEMARRAGE SERVEUR METIER");
  }
  
  /**
   * Arrête le module.
   */
  @Override
  public void arreter() {
    // Arrêt des modules
    ManagerModule.arreterModule(true);
    
    // Arrêt des connexions
    ManagerAS400.getSystemeAS400().disconnectAllServices();
    
    Trace.arreterLogiciel();
    System.exit(0);
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return true;
  }
  
  /**
   * Envoi un ordre au module.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + EnumModule.SERVEURMETIER + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case SERVEURMETIER_STOP:
          arreter();
          break;
        
        case SERVEURMETIER_STATUS:
          ManagerModule.interrogerStatus();
          tracerInformationsServeur();
          break;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + EnumModule.SERVEURMETIER + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Retourne la liste des sous-environnements.
   */
  public static ListeSousEnvironnement getListeSousEnvironnement() {
    return listeSousEnvironnement;
  }
  
  // -- Méthodes privées
  
  /**
   * Nettoyer et contrôler les fichiers du serveur.
   * Assurer d'éventuelles migration de fichier lors des changements de version.
   */
  private void initialiserEnvironnement() {
    if (listeSousEnvironnement == null) {
      throw new MessageErreurException("La liste des sous environnements est invalide.");
    }
    NettoyerServeur nettoyerServeur = new NettoyerServeur(ManagerAS400.getDossierRacineServeur());
    nettoyerServeur.controleMAJVersion(ManagerAS400.getSystemeAS400(), listeSousEnvironnement.getListe());
    
    // Lister les bases de données (dans le but d'accélérer les futurs accès car peut être très long la première fois)
    listerBaseDeDonnees();
    
    // Chargement des valeurs systèmes
    ManagerAS400.chargerValeurSysteme(false);
  }
  
  /**
   * Initialiser la liste des sous-environnements du serveur.
   */
  private void initialiserListeSousSysteme(QueryManager pQueryManager) {
    listeSousEnvironnement = new ListeSousEnvironnement();
    listeSousEnvironnement.chargerListeSousEnvironnement(ManagerAS400.getDossierRacineServeur());
    List<SousEnvironnement> listeSousEnvironnementTemp = listeSousEnvironnement.getListe();
    for (SousEnvironnement sousEnvironnement : listeSousEnvironnementTemp) {
      Character lettre = ManagerAS400.getLettreEnvironnement();
      if (lettre == null || lettre.charValue() == ' ') {
        throw new MessageErreurException("La lettre de l'environnement est incorrecte.");
      }
      sousEnvironnement.setLettreExecution(lettre);
      Trace.info("Sous-environnement=" + sousEnvironnement.getId() + " prefixeDevice=" + sousEnvironnement.getPrefixeDevice() + " lettre:"
          + lettre);
    }
  }
  
  /**
   * Charge la liste des nouvelles licences (à partir de 2021).
   */
  private void initialiserListeLicence(QueryManager pQueryManager) {
    // Contrôle le contenu de la table des licences et converti les anciennes licences vers les nouvelles si nécessaire
    try {
      ManagerLicence.controlerTableLicence(pQueryManager);
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    
    // Initialise les licences actives
    try {
      // Recherche les licences actives pour l'environnement
      ManagerLicence.chargerListeLicenceActive(pQueryManager);
      ListeLicence listeLicence = ManagerLicence.getListeLicenceActive();
      if (listeLicence == null || listeLicence.isEmpty()) {
        Trace.alerte("Aucune licence active n'a été trouvée");
      }
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Opérations sur les notifications.
   * - Suppression des notifications lues de plus d'un an.
   * - Tentative d'envoi par mail des notifs qui n'ont pas pu le faire précédemment.
   */
  private void initialiserNotification(QueryManager pQueryManager) {
    // Suppression des notifications lues de plus d'un an
    try {
      SqlNotification sqlNotification = new SqlNotification(ManagerAS400.getSystemeAS400(), pQueryManager);
      sqlNotification.purgerAncienneNotification();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la purge des anciennes notifications.");
    }
  }
  
  /**
   * Récupère la lettre d'exécution pour l'environnement.
   */
  private void recupererLettreEnvironnement(QueryManager pQueryManager, Bibliotheque pBibliothequeEnvironnementClient) {
    SqlEnvironnement sqlEnvironnement = new SqlEnvironnement(pQueryManager);
    Character lettre = sqlEnvironnement.retournerLettreExecution(pBibliothequeEnvironnementClient);
    if (lettre == null || lettre.charValue() == ' ') {
      throw new MessageErreurException("La lettre de l'environnement est incorrecte.");
    }
    
    ManagerAS400.setLettreEnvironnement(lettre);
    Trace.debug("Lettre d'environnement : " + lettre);
  }
  
  /**
   * Liste les bases de données du systèmes.
   */
  private List<BDD> listerBaseDeDonnees() {
    Trace.info("Inventaire des bases de données Série N sur le système.");
    CritereBibliotheque criteres = new CritereBibliotheque();
    criteres.setFiltrePrefixe("FM*");
    criteres.setFiltreBaseDeDonneesUniquement(true);
    return BibliothequeAS400.chargerListeBaseDeDonnees(ManagerAS400.getSystemeAS400(), criteres);
  }
  
  /**
   * Affiche les informations sur le serveur.
   */
  private void tracerInformationsServeur() {
    // Récupérer des informations sur le jar exécuté (localisation, date de dernière modification)
    final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
    String cheminJarPrincipal = "";
    File fichierJarPrincipal = null;
    String dateJarPrincipal = null;
    try {
      cheminJarPrincipal = ServeurSerieN.class.getProtectionDomain().getCodeSource().getLocation().toURI().getPath();
      fichierJarPrincipal = new File(cheminJarPrincipal);
      dateJarPrincipal = dateFormat.format(fichierJarPrincipal.lastModified());
    }
    catch (URISyntaxException e) {
      Trace.erreur(e, "Impossible de récupérer le chemin du JAR courant.");
      return;
    }
    
    // Contrôler le port de débug (il est calculé s'il n'est pas renseigné)
    int portDebug = 0;
    if (portDebug <= 0) {
      portDebug = ManagerServeurRMI.getPortRegistre() + 2;
    }
    
    Trace.titre("LIGNE DE COMMANDE");
    // Tracer la ligne de commande utilisée
    Trace.info("java -jar " + fichierJarPrincipal + " " + parametresLigneCommande);
    
    // Tracer la ligne de commande utilisable en mode debug à distance
    Trace.info("java -Xdebug -Xrunjdwp:server=y,transport=dt_socket,address=" + portDebug + ",suspend=n -jar "
        + fichierJarPrincipal.getName() + " " + parametresLigneCommande);
    
    // Tracer tous les paramètres
    Trace.titre("INFORMATIONS SERVEUR");
    tracerListeParametre();
    
    // Afficher les informations sur la mémoire
    Trace.titre("MEMOIRE");
    Trace.afficherEtatMemoire();
    
    // Afficher l'environnement Java
    Trace.titre("ENVIRONNEMENT JAVA");
    Trace.info("Nom JVM (java.vm.name)          : " + System.getProperty("java.vm.name"));
    Trace.info("Version JVM (java.version)      : " + System.getProperty("java.version"));
    Trace.info("Java Home (java.home)           : " + System.getProperty("java.home"));
    Trace.info("Fichier JAR principal           : " + fichierJarPrincipal);
    Trace.info("Derniere modification du JAR    : " + dateJarPrincipal);
    
    // Afficher le classpath
    Trace.info("Classpath                       : ");
    try {
      ClassLoader cl = ClassLoader.getSystemClassLoader();
      URL[] urls = ((URLClassLoader) cl).getURLs();
      for (URL url : urls) {
        String cheminJar = url.getFile();
        File fichierJar = new File(cheminJarPrincipal);
        dateFormat.format(fichierJar.lastModified());
        Trace.info(dateJarPrincipal + " " + cheminJar);
      }
    }
    catch (Exception e) {
      // En cas d'échec, ce n'est pas grave en soit, on continue
      Trace.erreur(e, "Erreur lors de la récupération du CLASSPATH.");
    }
    
    // Lister les .jar dans le même répertoire
    try {
      File repertoire = fichierJarPrincipal.getParentFile();
      File[] listeFichiersJar = repertoire.listFiles(new FilenameFilter() {
        @Override
        public boolean accept(File pFile, String pNomFichier) {
          return pNomFichier.toLowerCase().endsWith(EXTENSION_JAR);
        }
      });
      
      for (File fichierJar : listeFichiersJar) {
        String dateJar = dateFormat.format(fichierJar.lastModified());
        Trace.info(dateJar + " " + fichierJar);
      }
    }
    catch (Exception e) {
      // En cas d'échec, ce n'est pas grave en soit, on continue
      Trace.erreur(e, "Erreur lors de la récupération des fichiers JAR associés au serveur.");
    }
  }
  
  /**
   * Initialisation des paramètres à partir de la ligne de commande et de ceux stockés en table.
   */
  private void initialiserParametre(QueryManager pQueryManager, String pChaineParametres) {
    Trace.info("Initialiser les paramètres");
    
    // Chargement des paramètres à partir de la ligne de commande
    ListeParametreModule listeParametreLigneCommande = ManagerParametreModule.chargerParametreLigneCommande(pChaineParametres);
    
    // Initialisation des 2 paramètres qui doivent être obligatoirement renseignés en ligne de commande
    // Initialisation de la bibliothèque d'environnement à partir de la ligne de commande
    ParametreModule parametre =
        listeParametreLigneCommande.retournerParametreAvecEnum(EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT);
    if (parametre == null) {
      throw new MessageErreurException(
          "Le paramètre correspondant à " + EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT + " n'a pas été trouvé.");
    }
    String nomBibliothequeEnvironnement = parametre.getValeurEnString();
    Bibliotheque bibliothequeEnvironnement =
        new Bibliotheque(IdBibliotheque.getInstance(nomBibliothequeEnvironnement), EnumTypeBibliotheque.SYSTEME_SERIEN);
    pQueryManager.setLibrairieEnvironnement(nomBibliothequeEnvironnement);
    
    // Récupération de la lettre d'environnement (qui est unique pour l'instance en cours et tous les sous environnements)
    recupererLettreEnvironnement(pQueryManager, bibliothequeEnvironnement);
    
    // Initialisation du dossier racine de l'application à partir de la ligne de commande
    parametre = listeParametreLigneCommande.retournerParametreAvecEnum(EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE);
    if (parametre == null) {
      throw new MessageErreurException(
          "Le paramètre correspondant à " + EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE + " n'a pas été trouvé.");
    }
    ManagerAS400.setDossierRacine(parametre.getValeurEnString());
    
    // Les traces sont effacées avant et activées le plus tôt possible
    Trace.demarrerLogiciel(ManagerAS400.getDossierRacineServeur(), EnumModule.SERVEURMETIER.getCode(), NOM_LOGICIEL);
    
    // Contrôle que la liste des paramètres stockés en table soit à jour (nouveaux paramètres ou paramètres devenus obsolètes)
    ManagerParametreModule.controlerParametreEnTable(pQueryManager);
    
    // Chargement de tous les paramètres stockés en table
    ListeParametreModule listeParametreEnTable = ManagerParametreModule.chargerParametreEnTable(pQueryManager, null);
    
    // Mise à jour des valeurs des paramètres lus en table avec les paramètres de la ligne de commande
    listeParametreEnTable.miseAJourValeurAvecAutreListe(listeParametreLigneCommande);
    
    // Initialisation de la liste du manager avec la liste initialisée
    ManagerParametreModule.setListeParametre(listeParametreEnTable);
    
    // Chargement des paramètres dédiés au module principal ServeurSerieN autres que ".bibliothequeenvironnement" et ".dossierracine"
    initialiserParametreServeurMetier();
  }
  
  /**
   * Initialiser les paramètres dédiés au module Serveur Metier.
   */
  public void initialiserParametreServeurMetier() {
    // Le mode débug
    Integer debugModule = ManagerParametreModule.retournerValeurEnInteger(EnumParametreModule.SERVEURMETIER_MODE_DEBUG);
    if (debugModule == null || debugModule.intValue() == 0) {
      modeDebug = false;
    }
    else {
      modeDebug = true;
    }
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    ManagerParametreModule.tracerListeParametre();
  }
  
  /**
   * Programme principal du serveur Série métier N.
   */
  public static void main(String[] args) {
    // Forum sur Sécurity manager: http://forums.sun.com/thread.jspa?threadID=5360172&tstart=0
    // Permet de régler les problèmes de chargement d'images malgrès le fait que les jar soient signés
    System.setSecurityManager(null);
    
    // Concatènation des arguments saisis en ligne de commande
    final StringBuffer parametres = new StringBuffer();
    for (int i = 0; i < args.length; i++) {
      parametres.append(args[i].trim()).append(' ');
    }
    
    // Démarrer le serveur
    try {
      ServeurSerieN serveur = new ServeurSerieN(parametres.toString());
      serveur.demarrer();
    }
    catch (Exception e) {
      Trace.fatal(e, "Une erreur grave a empêchée le serveur de démarrer.");
      Trace.arreterLogiciel();
      System.exit(-1);
    }
  }
}
