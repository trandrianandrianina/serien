/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.outils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

public class ConversionPreferencePour214 {
  // Constantes
  private static final String MARQUEUR_CLE = "  <void property=\"";
  private static final String CLE_NUMERO = "numero";
  private static final String CLE_FAVORIS = "menuWinPlus";
  private static final String CLE_FAVORIS_NOUVEAU_NOM = "listeNumerosOrdreFavoris";
  private static final String CLE_IMPRIMANTE = "listeSelectionImprimantes";
  private static final String CLE_NUMERO_ORDRE = "MNEORD";
  private static final String CLE_GROUPE_MODULE = "WGrp";
  private static final String CLE_LIBELLE_ALTERNATIF = "libelleAlternatif";
  private static final String[] CLES_A_IGNORER = { "resolution" };
  private static final String[] CLES_SPECIALES = { CLE_FAVORIS, CLE_IMPRIMANTE, CLE_NUMERO };
  private static final String[] TYPES_SIMPLES_SANS_GUILLEMETS = { "boolean", "int", "char" };
  private static final String MARQUEUR_VALEUR_LISTE = "method=\"add\"";
  private static final String MARQUEUR_FIN_LISTE = "     </object>";
  private static final String MARQUEUR_FIN_TABLEAU = "</array>";
  private static final String EXTENSION_PREF = ".pref";
  // private static final String encodageFichier = "UTF-8";
  private static final String[][] equivalences = new String[][] { { "\\\\", "\\\\\\\\" }, { "&apos;", "'" } };
  
  // Variables
  private String prefixe = "";
  
  /**
   * Constructeur.
   */
  public ConversionPreferencePour214(String pPrefixe) {
    prefixe = Constantes.normerTexte(pPrefixe);
  }
  
  /**
   * Lance la conversion de tous les fichiers de préférence d'un dossier donné.
   */
  public void lancerConversion(String pDossierConfig) {
    pDossierConfig = Constantes.normerTexte(pDossierConfig);
    if (pDossierConfig.isEmpty()) {
      Trace.erreur("Le nom du dossier de configuration est vide.");
      return;
    }
    
    // Contrôle l'existence du dossier
    File dossierConfig = new File(pDossierConfig);
    if (!dossierConfig.exists()) {
      Trace.erreur(
          "Le dossier de configuration " + pDossierConfig + " n'existe pas, la conversion des fichiers de préférence a été avortée.");
      return;
    }
    
    // Liste les fichiers concernés
    File[] listeFichiers = dossierConfig.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.startsWith(prefixe) && name.endsWith("xml")) {
          return true;
        }
        return false;
      }
    });
    
    // Conversion
    for (File fichier : listeFichiers) {
      if (convertirXmlVersPref(fichier)) {
        File fichierRenomme = new File(fichier.getAbsolutePath() + ".213");
        if (fichier.renameTo(fichierRenomme)) {
          Trace.info(String.format("Le fichier %s a été renommé en %s.", fichier, fichierRenomme));
        }
      }
    }
    if (listeFichiers.length > 0) {
      Trace.info("Conversion des fichiers de préférence terminée.");
    }
  }
  
  /**
   * Converti le fichier des préférences en Xml en fichier Json.
   */
  public boolean convertirXmlVersPref(File pFichierPreferencesXml) {
    String[] preferencesXml = chargerFichierPreferenceOriginal(pFichierPreferencesXml);
    if (preferencesXml == null) {
      return false;
    }
    StringBuilder preferenceJson = new StringBuilder();
    String cle = null;
    boolean cleTrouvee = false;
    
    preferenceJson.append('{').append('\n');
    for (int i = 0; i < preferencesXml.length; i++) {
      String ligne = preferencesXml[i];
      // On cherche la clé
      if (ligne.startsWith(MARQUEUR_CLE)) {
        cle = retournerCle(ligne);
        if (cle == null) {
          cleTrouvee = false;
        }
        else {
          cleTrouvee = true;
        }
      }
      // Dans le cas où une clé a été trouvée on cherche la ou les valeurs
      else if (cleTrouvee) {
        // On recherche les clés avec des traitements spéciaux
        if (contenir(CLES_SPECIALES, cle)) {
          if (CLE_FAVORIS.equalsIgnoreCase(cle)) {
            traiterValeursFavoris(preferencesXml, i, cle, preferenceJson);
          }
          else if (CLE_IMPRIMANTE.equalsIgnoreCase(cle)) {
            traiterValeursImprimante(preferencesXml, i, cle, preferenceJson);
          }
          else if (CLE_NUMERO.equalsIgnoreCase(cle)) {
            String valeur = retournerValeurSimple(ligne);
            if (valeur != null) {
              try {
                // On incrémente le numéro pour être sur que ce fichier sera téléchargé par le poste client
                int numero = Integer.parseInt(valeur);
                preferenceJson.append(String.format("\"%s\":%d,", cle, ++numero)).append('\n');
              }
              catch (NumberFormatException e) {
                preferenceJson.append(String.format("\"%s\":%s,", cle, valeur)).append('\n');
              }
            }
          }
        }
        // On traite les cas simples
        else {
          String valeur = retournerValeurSimple(ligne);
          if (valeur != null) {
            preferenceJson.append(String.format("\"%s\":%s,", cle, valeur)).append('\n');
          }
        }
        cleTrouvee = false;
      }
    }
    if (preferenceJson.length() > 0) {
      preferenceJson.deleteCharAt(preferenceJson.length() - 2);
    }
    preferenceJson.append('}');
    
    // Construction du nom du fichier converti
    String nomFichierJson = pFichierPreferencesXml.getName();
    int posd = nomFichierJson.indexOf('.');
    if (posd != -1) {
      nomFichierJson = nomFichierJson.substring(0, posd) + EXTENSION_PREF;
    }
    else {
      nomFichierJson = nomFichierJson + EXTENSION_PREF;
    }
    File fichierJson = new File(pFichierPreferencesXml.getParent() + File.separatorChar + nomFichierJson);
    return sauverFichierPreference(fichierJson, preferenceJson.toString());
  }
  
  /**
   * Récupère les valeurs des favoris.
   */
  private void traiterValeursFavoris(String[] pPreferencesXml, int pIndice, String pCle, StringBuilder pPreferenceJson) {
    Trace.info("traiterValeursFavoris");
    String valeurs[] = retournerValeursFavoris(pPreferencesXml, pIndice);
    if (valeurs != null) {
      pPreferenceJson.append(String.format("\"%s\":{", CLE_FAVORIS_NOUVEAU_NOM));
      for (String valeur : valeurs) {
        Trace.info(valeur);
        pPreferenceJson.append(valeur).append(',');
      }
      if (valeurs.length > 0) {
        // On supprime la dernière virgule qui n'a pas de raison d'être
        pPreferenceJson.deleteCharAt(pPreferenceJson.length() - 1);
      }
      pPreferenceJson.append("},").append('\n');
    }
  }
  
  /**
   * Récupère les valeurs des imprimantes.
   */
  private void traiterValeursImprimante(String[] pPreferencesXml, int pIndice, String pCle, StringBuilder pPreferenceJson) {
    String valeurs[] = retournerValeursImprimante(pPreferencesXml, pIndice);
    if (valeurs != null) {
      pPreferenceJson.append(String.format("\"%s\":[", pCle));
      for (String valeur : valeurs) {
        pPreferenceJson.append(valeur).append(',');
      }
      if (valeurs.length > 0) {
        // On supprime la dernière virgule qui n'a pas de raison d'être
        pPreferenceJson.deleteCharAt(pPreferenceJson.length() - 1);
      }
      pPreferenceJson.append("],").append('\n');
    }
  }
  
  /**
   * Retourne la clé.
   */
  private String retournerCle(String pLigne) {
    int posd = pLigne.indexOf('"') + 1;
    int posf = pLigne.lastIndexOf('"');
    if ((posd == -1) || (posf == -1) || (posd == posf)) {
      return null;
    }
    String cle = pLigne.substring(posd, posf);
    if (contenir(CLES_A_IGNORER, cle)) {
      return null;
    }
    return cle;
  }
  
  /**
   * Retourne une valeur simple.
   */
  private String retournerValeurSimple(String pLigne) {
    // Récupération du type
    int posd = pLigne.indexOf('<') + 1;
    int posf = pLigne.indexOf(">");
    String type = pLigne.substring(posd, posf);
    
    // Récupération de la valeur
    posd = pLigne.indexOf('>') + 1;
    posf = pLigne.lastIndexOf("</");
    if ((posd == -1) || (posf == -1) || (posd == posf)) {
      return null;
    }
    String valeur = pLigne.substring(posd, posf);
    
    // Traitement si la valeur contient des caractères spéciaux
    for (int i = 0; i < equivalences.length; i++) {
      valeur = valeur.replaceAll(equivalences[i][0], equivalences[i][1]);
    }
    
    // Formatage de la valeur en fonction du type
    if (contenir(TYPES_SIMPLES_SANS_GUILLEMETS, type)) {
      return valeur;
    }
    else {
      return String.format("\"%s\"", valeur);
    }
  }
  
  /**
   * Retourne les valeurs pour les favoris.
   * Le fait d'utiliser une hashmap évite les numéro d'ordre en double (et donc des problèmes par la suite).
   */
  private String[] retournerValeursFavoris(String[] pTableau, int pIndice) {
    LinkedHashMap<String, String> listeValeurs = new LinkedHashMap<String, String>();
    String numeroOrdre = "";
    String wgrp = "";
    String libelleAlternatif = "\"\"";
    int i = pIndice;
    while (i < pTableau.length) {
      if (pTableau[i].contains(MARQUEUR_FIN_TABLEAU)) {
        break;
      }
      if (pTableau[i].startsWith(MARQUEUR_FIN_LISTE)) {
        // On vérifie que le point de menu n'appartienne pas à un menu spécifique (dans ce cas il sera ignoré à cause de la renumérotation
        // des numéros d'ordre)
        if (!wgrp.trim().equals("6")) {
          listeValeurs.put(numeroOrdre, libelleAlternatif);
        }
        numeroOrdre = "";
        wgrp = "";
        libelleAlternatif = "\"\"";
      }
      else if (pTableau[i].contains(CLE_NUMERO_ORDRE)) {
        i++;
        numeroOrdre = retournerValeurSimple(pTableau[i]);
      }
      else if (pTableau[i].contains(CLE_GROUPE_MODULE)) {
        i++;
        wgrp = retournerValeurSimple(pTableau[i]);
      }
      else if (pTableau[i].contains(CLE_LIBELLE_ALTERNATIF)) {
        i++;
        libelleAlternatif = retournerValeurSimple(pTableau[i]);
      }
      i++;
    }
    
    // Création du tableau définitif sans risque de doublon dans les numéros d'ordre
    String[] liste = new String[listeValeurs.size()];
    i = 0;
    for (Entry<String, String> entry : listeValeurs.entrySet()) {
      liste[i] = String.format("%s:%s", entry.getKey(), entry.getValue());
      i++;
    }
    
    return liste;
  }
  
  /**
   * Retourne les valeurs pour les imprimantes.
   */
  private String[] retournerValeursImprimante(String[] pTableau, int pIndice) {
    ArrayList<String> listeValeurs = new ArrayList<String>();
    int i = pIndice;
    while (i < pTableau.length) {
      if (pTableau[i].contains(MARQUEUR_FIN_LISTE)) {
        break;
      }
      else if (pTableau[i].contains(MARQUEUR_VALEUR_LISTE)) {
        i++;
        listeValeurs.add(retournerValeurSimple(pTableau[i]));
      }
      i++;
    }
    
    return listeValeurs.toArray(new String[0]);
  }
  
  /**
   * Contrôle si une liste contient la valeur.
   */
  private boolean contenir(String[] pListe, String pValeur) {
    pValeur = Constantes.normerTexte(pValeur);
    for (String valeur : pListe) {
      if (pValeur.equalsIgnoreCase(valeur)) {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Chargement d'un fichier de préférences utilisateur au format xml.
   */
  private String[] chargerFichierPreferenceOriginal(File pFichier) {
    GestionFichierTexte gft = new GestionFichierTexte(pFichier);
    gft.initUnicode();
    String[] preferencesXml = gft.getContenuFichierTab();
    gft.dispose();
    return preferencesXml;
  }
  
  /**
   * Sauvegarde d'un fichier de préférences utilisateur converti.
   */
  private boolean sauverFichierPreference(File pFichier, String pContenu) {
    pContenu = Constantes.normerTexte(pContenu);
    if (pContenu.isEmpty()) {
      Trace.erreur("Le contenu à sauvegarder dans le fichier " + pFichier.getAbsolutePath() + " est vide.");
      return false;
    }
    
    boolean retour = true;
    GestionFichierTexte gft = new GestionFichierTexte(pFichier);
    gft.initUnicode();
    gft.setContenuFichier(pContenu);
    if (gft.ecritureFichier() == Constantes.ERREUR) {
      Trace.erreur("Erreur lors de l'écriture dans le fichier " + pFichier.getAbsolutePath() + '.');
      retour = false;
    }
    gft.dispose();
    return retour;
  }
}
