/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.outils;

import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.sql.comptabilite.parametres.SqlComptabiliteParametres;
import ri.serien.libas400.dao.sql.exploitation.basededonnees.SqlBaseDeDonnees;
import ri.serien.libas400.dao.sql.exploitation.licence.SqlLicence;
import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.commun.bdd.CritereBDD;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.licence.CodeTypeLicence;
import ri.serien.libcommun.exploitation.licence.EnumStatutLicence;
import ri.serien.libcommun.exploitation.licence.EnumTypeLicence;
import ri.serien.libcommun.exploitation.licence.IdLicence;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.licence.ancienne.AncienneLicence;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe est utilisée pour assurer la migration des licences anciennes génération de Série N vers la version 4.1.
 * Les licences créées lors de la migration auront pour date de fin de validité povisoire (date du jour + 1 mois) afin que RI génère de
 * nouvelles licences pour que notre base de données des licences soit cohérente avec l'existant.
 * Les licences module (gescom et compta) sont initialisées avec la valeur 999 pour le nombre d'établissements, de magasins et de
 * sociétés.
 */
public class MigrationLicenceVersV4_1 {
  // Variables
  private QueryManager queryManager = null;
  private int nombreMaxEtablissement = 0;
  private int nombreMaxMagasin = 0;
  private int nombreMaxSociete = 0;
  
  /**
   * Constructeur.
   */
  public MigrationLicenceVersV4_1(QueryManager pQueryManager) {
    queryManager = controler(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Migre les licences anciennes génération vers la nouvelle table PSEMLICM.
   */
  public void migrer() {
    String nomTable = queryManager.getLibrairieEnvironnement() + '/' + EnumTableBDD.LICENCE_ANCIENNE;
    
    Trace.info("La table " + queryManager.getLibrairieEnvironnement() + "/" + EnumTableBDD.LICENCE
        + " contenant les licences est vide : elle va être initialisée avec les licences de la table " + nomTable + ".");
    
    // Lecture de l'ancienne table des licences
    Trace.info("Recherche des anciennes licences.");
    List<AncienneLicence> listeAncienneLicence = chargerAncienneLicence();
    if (listeAncienneLicence == null || listeAncienneLicence.isEmpty()) {
      Trace.alerte("Aucun enregistrement n'a été trouvé dans la table " + nomTable + ".");
      return;
    }
    Trace.info(listeAncienneLicence.size() + " anciennes licences ont été trouvé.");
    
    // Conversion des anciennes licences trouvées
    convertirAncienneLicence(listeAncienneLicence);
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôle la validité du queryManager.
   * @param pQueryManager L'accès à la base de données
   * @return
   */
  private QueryManager controler(QueryManager pQueryManager) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le querymanager n'est pas initialisé.");
    }
    // Contrôle de la bibliothèque d'environnement
    String bibliothequeEnvironnement = Constantes.normerTexte(pQueryManager.getLibrairieEnvironnement());
    if (bibliothequeEnvironnement.isEmpty()) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    return pQueryManager;
  }
  
  /**
   * Charge toutes les licences de la table PSEMDRTM.
   */
  private List<AncienneLicence> chargerAncienneLicence() {
    SqlLicence operation = new SqlLicence(queryManager);
    return operation.chargerListeAncienneLicence(ManagerAS400.getLettreEnvironnement());
  }
  
  /**
   * Convertis la liste des anciennes licences fournit.
   */
  private void convertirAncienneLicence(List<AncienneLicence> pListeAncienneLicence) {
    if (pListeAncienneLicence == null || pListeAncienneLicence.isEmpty()) {
      return;
    }
    
    Trace.info("Génération des nouvelles licences...");
    // Récupération des informations nécessaire à la conversion
    controlePourLicenceModule();
    Trace.info("Nombre maximum d'établissements : " + nombreMaxEtablissement);
    Trace.info("Nombre maximum de magasins      : " + nombreMaxMagasin);
    Trace.info("Nombre maximum de sociétés      : " + nombreMaxSociete);
    
    // Conversion des licences
    Trace.info("Début de la conversion des " + pListeAncienneLicence.size() + " anciennes licences.");
    ListeLicence listeNouvelleLicence = new ListeLicence();
    // Création de la licence utilisateur (à partir de la première licence de la liste) cela n'a pas d'importance
    Licence nouvelleLicence =
        pListeAncienneLicence.get(0).convertirVersNouvelleLicence(true, nombreMaxEtablissement, nombreMaxMagasin, nombreMaxSociete);
    if (nouvelleLicence != null) {
      listeNouvelleLicence.add(nouvelleLicence);
    }
    
    // Création des licences classiques
    for (AncienneLicence licence : pListeAncienneLicence) {
      nouvelleLicence = licence.convertirVersNouvelleLicence(false, nombreMaxEtablissement, nombreMaxMagasin, nombreMaxSociete);
      if (nouvelleLicence != null) {
        listeNouvelleLicence.add(nouvelleLicence);
      }
    }
    Trace.info(listeNouvelleLicence.size() + " nouvelles licences.");
    Trace.info("Fin de la conversion des anciennes licences.");
    
    // Création des licences "base de production" uniquement si des anciennes licences ont été convertis car sinon l'idclient est inconnu
    Trace.info("Début de la création des licences de bases de données de production.");
    if (!listeNouvelleLicence.isEmpty()) {
      IdClient idClient = listeNouvelleLicence.get(0).getIdClient();
      creerLicenceBaseDeProduction(listeNouvelleLicence, idClient);
    }
    Trace.info("Fin de la création des licences de bases de données de production.");
    
    // Sauvegarde en base de données
    Trace.info("Début de la sauvegarde en base des licences.");
    SqlLicence operation = new SqlLicence(queryManager);
    for (Licence licence : listeNouvelleLicence) {
      try {
        operation.sauverLicence(licence);
      }
      catch (Exception e) {
        Trace.erreur(e, "");
      }
    }
    Trace.info("Fin de la sauvegarde en base des licences.");
    
    Trace.info("Génération des nouvelles licences terminée.");
  }
  
  /**
   * Création des nouvelles licences pour les bases de données de production.
   * Seules les bases de données référencées dans la table PSEMFMVM seront prise en compte lors de la génération des licences de bases de
   * données.
   */
  private void creerLicenceBaseDeProduction(ListeLicence pListeNouvelleLicence, IdClient pIdClient) {
    if (pIdClient == null) {
      Trace.alerte("L'identifiant du client est incorrect donc aucune licence de production créée.");
      return;
    }
    
    ListeBDD listeBaseDeDonnees = null;
    try {
      // Chargement de la table PSEMFMVM qui fait le lien entre une base de données et sa lettre d'environnement
      CritereBDD critere = new CritereBDD();
      critere.setLettreEnvironnement(ManagerAS400.getLettreEnvironnement());
      SqlBaseDeDonnees sqlBaseDeDonnees = new SqlBaseDeDonnees(queryManager);
      List<IdBibliotheque> listeIdBibliotheque = sqlBaseDeDonnees.chargerListeIdBaseDeDonnees(critere);
      if (listeIdBibliotheque == null || listeIdBibliotheque.isEmpty()) {
        Trace.alerte("Aucune base de données n'a été trouvé dans la table " + EnumTableBDD.LETTRE_ENVIRONNMENT_BDD
            + " donc aucune licence de production créée.");
        return;
      }
      listeBaseDeDonnees = sqlBaseDeDonnees.chargerListeBaseDeDonnees(listeIdBibliotheque);
    }
    catch (Exception e) {
    }
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      return;
    }
    
    // Création des licences
    for (BDD baseDeDonnees : listeBaseDeDonnees) {
      try {
        Licence licence = new Licence(IdLicence.getInstanceAvecCreationId(EnumTypeLicence.BDD_PRODUCTION));
        licence.setDateCreation(new Date());
        licence.setDateFinValidite(Licence.genererDateProvisoire());
        licence.setIdClient(pIdClient);
        licence.setIdServeurPower(ManagerAS400.getIdServeurPower());
        licence.setLettreEnvironnement(ManagerAS400.getLettreEnvironnement());
        licence.setStatutLicence(EnumStatutLicence.ACTIVE);
        // Création du code du type de la licence
        CodeTypeLicence codeTypeLicence = CodeTypeLicence.creerCodeTypeBaseDeDonnees(baseDeDonnees.getNom());
        licence.setCodeTypeLicence(codeTypeLicence);
        // Ajout à la liste des nouvelles licences
        licence.chiffrerCleLicence();
        pListeNouvelleLicence.add(licence);
      }
      catch (Exception e) {
        Trace.erreur(e, "La licence pour la base de données " + baseDeDonnees.getNom() + " n'a pas pu être créée.");
      }
    }
  }
  
  /**
   * Balaye la liste des bases de données afin de cherche les nombres max d'établissements, de magasins et de sociétés.
   */
  private void controlePourLicenceModule() {
    ListeBDD listeBaseDeDonnees = null;
    try {
      // Chargement de la table PSEMFMVM qui fait le lien entre une base de données et sa lettre d'environnement
      CritereBDD critere = new CritereBDD();
      critere.setLettreEnvironnement(ManagerAS400.getLettreEnvironnement());
      SqlBaseDeDonnees sqlBaseDeDonnees = new SqlBaseDeDonnees(queryManager);
      List<IdBibliotheque> listeIdBibliotheque = sqlBaseDeDonnees.chargerListeIdBaseDeDonnees(critere);
      if (listeIdBibliotheque == null || listeIdBibliotheque.isEmpty()) {
        Trace.alerte("Aucune base de données n'a été trouvé dans la table " + EnumTableBDD.LETTRE_ENVIRONNMENT_BDD
            + " donc aucune licence de production créée.");
        return;
      }
      listeBaseDeDonnees = sqlBaseDeDonnees.chargerListeBaseDeDonnees(listeIdBibliotheque);
    }
    catch (Exception e) {
    }
    
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      return;
    }
    
    // Récupération des informations
    for (BDD baseDeDonnees : listeBaseDeDonnees) {
      recherherInformationsBaseDeDonnees(baseDeDonnees);
    }
  }
  
  /**
   * Recherche dans la base de données indiquée le nombre d'établissement, de magasins et de sociétés.
   * Si le nombre est supérieur à celui stocké alors il est mis à jour avec cette valeur.
   */
  private void recherherInformationsBaseDeDonnees(BDD pBaseDeDonnees) {
    if (pBaseDeDonnees == null) {
      return;
    }
    String curlibOrigine = queryManager.getLibrary();
    queryManager.setLibrary(pBaseDeDonnees.getNom());
    
    // Le nombre d'établissement et de magasins
    try {
      SqlGescomParametres sqlGescomParametres = new SqlGescomParametres(queryManager);
      ListeEtablissement listeEtablissement = sqlGescomParametres.chargerListeEtablissement();
      if (listeEtablissement != null && !listeEtablissement.isEmpty()) {
        // Contrôle du nombre d'établissements
        if (nombreMaxEtablissement < listeEtablissement.size()) {
          nombreMaxEtablissement = listeEtablissement.size();
        }
        // Les magasins
        CritereMagasin critere = new CritereMagasin();
        for (Etablissement etablissement : listeEtablissement) {
          critere.setIdEtablissement(etablissement.getId());
          ListeMagasin listeMagasin = sqlGescomParametres.chargerListeMagasin(critere);
          // Contrôle du nombre de magasins
          if (listeMagasin != null && !listeMagasin.isEmpty() && nombreMaxMagasin < listeMagasin.size()) {
            nombreMaxMagasin = listeMagasin.size();
          }
        }
      }
    }
    catch (Exception e) {
      Trace.alerte("Contrôle du nombre d'établissements et de magasins de " + pBaseDeDonnees.getNom() + " en echec.");
    }
    
    // Le nombre de sociétés
    try {
      SqlComptabiliteParametres sqlComptabiliteParametres = new SqlComptabiliteParametres(queryManager);
      int nombreSociete = sqlComptabiliteParametres.getNombreSociete();
      // Contrôle du nombre de sociétés
      if (nombreMaxSociete < nombreSociete) {
        nombreMaxSociete = nombreSociete;
        Trace.info("Mise à jour du nombre de sociétés de " + pBaseDeDonnees.getNom() + '.');
      }
    }
    catch (Exception e) {
      Trace.alerte("Contrôle du nombre de sociétés de " + pBaseDeDonnees.getNom() + " en echec.");
    }
    
    queryManager.setLibrary(curlibOrigine);
  }
}
