/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.outils;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.dao.sql.exploitation.basededonnees.SqlBaseDeDonnees;
import ri.serien.libas400.dao.sql.exploitation.environnement.SqlEnvironnement;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.commun.bdd.CritereBDD;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.module.EnumOrigineParametre;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.IdParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierINI;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.sql.RequeteSql;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe est utilisée pour assurer la migration du moteur de Série N vers la version 4.
 * Elle gère la migration des paramètres stockés dans une dataarea vers une table.
 */
public class MigrationParametreModuleVersV4 {
  // Variables
  private QueryManager queryManager = null;
  private Dataarea dataareaParametre = null;
  private GestionDataareaAS400 gestionDataarea = new GestionDataareaAS400(ManagerAS400.getSystemeAS400());
  private String contenuDataarea = null;
  
  /**
   * Constructeur.
   */
  public MigrationParametreModuleVersV4(QueryManager pQueryManager) {
    queryManager = controler(pQueryManager);
  }
  
  // -- Méthodes publiques
  
  /**
   * Migre les paramètres de l'application stockés dans une dataarea vers une table.
   */
  public void migrer() {
    Trace.info("La table " + queryManager.getLibrairieEnvironnement() + "/" + EnumTableBDD.PARAMETRE_MODULE
        + " contenant les paramètres des modules est vide : elle va être initialisée avec les paramètres par défaut.");
    
    // La liste est vide pour 2 raisons :
    // - il s'agit d'une migration d'une ancienne version de Série N (version <= 3) vers une plus récente (version >= 4)
    // - un malotru aura effacé le contenu de la table PSEMPMODM
    // Récupération des paramètres stockés dans la dataarea pour les anciennes versions (version <= 3)
    ListeParametreModule listeParametre = convertirParametreDepuisDataarea();
    
    // Migration des paramètres pour le mailManager qui sont ajoutés à la liste déjà existante
    listeParametre = convertirParametreMailManager(listeParametre);
    
    // Création de la liste des paramètres initialisée avec les valeurs trouvées dans la dataarea
    ManagerParametreModule.synchroniserTableParametre(queryManager, listeParametre);
    
    // Nettoyer les 510 premiers octets de la datarea
    effacerPartiellementDataarea();
  }
  
  // -- Méthodes privées
  
  /**
   * Conversion des paramètres stockés dans la dataarea en liste de paramètres (version <= 3).
   * Cette méthode ne sera appelée qu'une seule fois afin d'assurer la migration vers la nouvelle gestion des paramètres qui sont stockés
   * dorénavant en table.
   */
  private ListeParametreModule convertirParametreDepuisDataarea() {
    String dossierRacine = Constantes.normerTexte(ManagerAS400.getDossierRacineServeur());
    if (dossierRacine.isEmpty()) {
      throw new MessageErreurException("Le dossier racine du serveur est invalide.");
    }
    
    // Lecture du fichier rt.ini afin de récupérer le paramètre bibliothèque d'environnement client
    String nomFichier = dossierRacine + File.separatorChar + Constantes.FIC_CONFIG;
    GestionFichierINI gfi = new GestionFichierINI(nomFichier);
    HashMap<String, LinkedHashMap<String, String>> sections = gfi.getSections();
    if (sections == null || sections.isEmpty()) {
      throw new MessageErreurException("Le fichier " + nomFichier + " est vide.");
    }
    
    // Récupération de la bibliothèque d'environnement client de la première section trouvée (cela suffit en théorie)
    String[] listeNomSection = gfi.getListeNomSection();
    if (listeNomSection == null || listeNomSection.length == 0) {
      throw new MessageErreurException("Aucune section trouvée dans le " + nomFichier + ".");
    }
    String nomSection = listeNomSection[0];
    LinkedHashMap<String, String> donneesSection = sections.get(nomSection);
    if (donneesSection == null || donneesSection.isEmpty()) {
      throw new MessageErreurException("Aucune données trouvées dans la section " + nomSection + ".");
    }
    String nomBibliothequeEnvironnementClient = donneesSection.get(ListeSousEnvironnement.BIBLIOTHEQUE_ENVIRONNEMENT_CLIENT);
    nomBibliothequeEnvironnementClient = Constantes.normerTexte(nomBibliothequeEnvironnementClient);
    if (nomBibliothequeEnvironnementClient.isEmpty()) {
      Trace.alerte("Le nom de la bibliothèque d'environnement client est invalide.");
      // La bibliothèque d'environnement client est invalide alors tentative avec la bibliothèque d'environnement
      nomBibliothequeEnvironnementClient = donneesSection.get(ListeSousEnvironnement.BIBLIOTHEQUE_ENVIRONNEMENT);
      nomBibliothequeEnvironnementClient = Constantes.normerTexte(nomBibliothequeEnvironnementClient);
      if (nomBibliothequeEnvironnementClient.isEmpty()) {
        throw new MessageErreurException("Le nom de la bibliothèque d'environnement est invalide.");
      }
    }
    
    // Récupération de la lettre d'environnement avec la bibliothèque d'environnement client
    IdBibliotheque idBibliotheque = IdBibliotheque.getInstance(nomBibliothequeEnvironnementClient);
    Bibliotheque bibliothequeEnvironnementClient = new Bibliotheque(idBibliotheque, EnumTypeBibliotheque.SYSTEME_SERIEN);
    SqlEnvironnement sqlEnvironnement = new SqlEnvironnement(queryManager);
    Character lettreEnvironnement = sqlEnvironnement.retournerLettreExecution(bibliothequeEnvironnementClient);
    if (lettreEnvironnement == null || lettreEnvironnement == ' ') {
      throw new MessageErreurException("La lettre d'environnement est invalide.");
    }
    
    // Lecture et conversion des paramètres stockés dans la dataarea de QGPL
    try {
      dataareaParametre = new Dataarea();
      dataareaParametre.setBibliotheque(new Bibliotheque(IdBibliotheque.getInstance("QGPL"), EnumTypeBibliotheque.SYSTEME));
      dataareaParametre.setNom(lettreEnvironnement + "SERIEN");
      Trace.info("\tLecture de la dataarea QGPL/" + dataareaParametre.getNom() + '.');
      contenuDataarea = gestionDataarea.chargerDataarea(dataareaParametre);
      if (contenuDataarea.trim().isEmpty()) {
        Trace.info("\tLa dataarea " + dataareaParametre.getNom() + " n'existe pas ou est vide.");
        return null;
      }
      // Mise à jour de la liste des paramètres avec les paramètres trouvés dans la dataarea
      return convertirContenuDataarea(contenuDataarea);
    }
    catch (Exception e) {
      Trace.erreur("Erreur lors de la lecture de la dataarea contenant les paramètres.");
    }
    
    return null;
  }
  
  /**
   * Efface les 510 premiers octets de la dataarea contenant les anciens paramètres de l'application.
   */
  private void effacerPartiellementDataarea() {
    if (dataareaParametre == null) {
      Trace.erreur("Le nom de la dataarea est invalide.");
    }
    if (contenuDataarea.length() < 1024) {
      throw new MessageErreurException("La dataarea " + dataareaParametre.getNom() + " n'a pas la longueur attendu > 1024.");
    }
    
    // Efface les 510 octets (255 pour Serie N + 255 pour Newsim)
    StringBuffer sb = new StringBuffer(contenuDataarea);
    for (int i = 0; i < 510; i++) {
      sb.setCharAt(i, ' ');
    }
    contenuDataarea = sb.toString();
    
    // Enregistre le contenu de la dataarea modifié
    try {
      GestionDataareaAS400 gestionDataarea = new GestionDataareaAS400(ManagerAS400.getSystemeAS400());
      dataareaParametre.setValeur(contenuDataarea);
      gestionDataarea.ecrireDataarea(dataareaParametre);
    }
    catch (Exception e) {
      Trace.erreur("Erreur lors de l'écriture de la dataarea dans le but d'effacer les paramètres.");
    }
  }
  
  /**
   * Contrôle la validité du queryManager.
   * @param pQueryManager L'accès à la base de données
   * @return
   */
  private QueryManager controler(QueryManager pQueryManager) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le querymanager n'est pas initialisé.");
    }
    // Contrôle de la bibliothèque d'environnement
    String bibliothequeEnvironnement = Constantes.normerTexte(pQueryManager.getLibrairieEnvironnement());
    if (bibliothequeEnvironnement.isEmpty()) {
      throw new MessageErreurException("La bibliothèque d'environnement est invalide.");
    }
    return pQueryManager;
  }
  
  /**
   * Analyse et converti les paramètres contenus dans la dataarea de QGPL.
   * @param pContenu Contenu de la dataarea de QGPL/?SERIEN
   * @return
   */
  private ListeParametreModule convertirContenuDataarea(String pContenu) {
    // Contrôle du contenu de la dataarea
    if (pContenu == null || pContenu.length() < 1024) {
      Trace.info("\tLe contenu dataarea est vide ou inférieur à 1024 octets.");
      return null;
    }
    // Récupération des paramètres de Serie N
    ListeParametreModule listeParametreSerieN = convertirParametreSerieN(pContenu);
    
    // Récupération des paramètres de NewSim
    ListeParametreModule listeParametreNewsim = convertirParametreNewsim(pContenu);
    
    // Fusion des deux listes de paramètres
    listeParametreSerieN.addAll(listeParametreNewsim);
    return listeParametreSerieN;
  }
  
  /**
   * Converti les paramètres Série N stockés dans la dataarea.
   * L'origine des valeurs des paramètres sont défini par défaut car elles doivent être modifiables puisqu'il s'agit d'une conversion.
   * @return
   */
  private ListeParametreModule convertirParametreSerieN(String pContenu) {
    Trace.info("\tDébut conversion des paramètres pour Série N.");
    if (Constantes.normerTexte(pContenu).isEmpty()) {
      return null;
    }
    
    ListeParametreModule listeParametre = new ListeParametreModule();
    String contenu = pContenu.substring(0, 255).trim().replace('\t', ' ');
    
    // Démarrage du module RMI (dans la nouvelle philosophie)
    if (contenu.trim().toUpperCase().equals("*NOSTART")) {
      IdParametreModule idParametreModule = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURRMI_DEMARRAGE);
      ParametreModule parametre = new ParametreModule(idParametreModule, 0, EnumOrigineParametre.PAR_DEFAUT);
      listeParametre.add(parametre);
    }
    // Il n'y a aucun paramètre de fourni donc ajout du paramètre pour le dossier racine
    else if (contenu.trim().toUpperCase().equals("*NONE")) {
      IdParametreModule idParametreModule = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURMETIER_DOSSIER_RACINE);
      ParametreModule parametre =
          new ParametreModule(idParametreModule, ManagerAS400.getDossierRacineServeur(), EnumOrigineParametre.PAR_DEFAUT);
      listeParametre.add(parametre);
    }
    // Anayse de la chaine complète
    else {
      // Découper la chaine et on stocke les différents paramètres dans un tableau
      String[] listeparametres = Constantes.splitString(contenu, ' ');
      
      // Parcourir la liste afin d'en extraire les infos
      IdParametreModule idParametre;
      ParametreModule parametre;
      for (int i = 0; i < listeparametres.length; i++) {
        String chaine = listeparametres[i].trim();
        if (chaine.isEmpty()) {
          continue;
        }
        
        // Recherche le nom du serveur RMI
        if (chaine.toLowerCase().startsWith("-h=")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURRMI_NOM);
          parametre = new ParametreModule(idParametre, chaine.substring(3), EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche le port du registre RMI
        else if (chaine.toLowerCase().startsWith("-p=")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURRMI_PORT_REGISTRE);
          parametre = new ParametreModule(idParametre, chaine.substring(3), EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche le port des services RMI
        else if (chaine.toLowerCase().startsWith("-s=")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURRMI_PORT_SERVICES);
          parametre = new ParametreModule(idParametre, chaine.substring(3), EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche le dossier utilisateur
        else if (chaine.toLowerCase().startsWith("-dir=")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURRMI_PORT_SERVICES);
          parametre = new ParametreModule(idParametre, chaine.substring(5), EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche si le déboggueur doit être activé
        else if (chaine.toLowerCase().equalsIgnoreCase("-d")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.SERVEURMETIER_MODE_DEBUG);
          parametre = new ParametreModule(idParametre, 1, EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche si le gestionnaire de flux doit être démarré
        else if (chaine.toLowerCase().equalsIgnoreCase("-sflux")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.GESTIONNAIREFLUX_DEMARRAGE);
          parametre = new ParametreModule(idParametre, 1, EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
      }
    }
    
    Trace.info("\tFin conversion des paramètres pour Série N.");
    return listeParametre;
  }
  
  /**
   * Converti les paramètres Newsim stockés dans la dataarea.
   * L'origine des valeurs des paramètres sont défini par défaut car elles doivent être modifiables puisqu'il s'agit d'une conversion.
   * @return
   */
  private ListeParametreModule convertirParametreNewsim(String pContenu) {
    Trace.info("\tDébut conversion des paramètres pour Newsim.");
    if (Constantes.normerTexte(pContenu).isEmpty()) {
      return null;
    }
    
    ListeParametreModule listeParametre = new ListeParametreModule();
    String contenu = pContenu.substring(255, 510).trim().replace('\t', ' ');
    
    // Démarrage du module Newsim au lancement de Série N
    if (contenu.trim().toUpperCase().equals("*NOSTART")) {
      IdParametreModule idParametreModule = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.NEWSIM_DEMARRAGE);
      ParametreModule parametre = new ParametreModule(idParametreModule, 0, EnumOrigineParametre.PAR_DEFAUT);
      listeParametre.add(parametre);
    }
    // Anayse de la chaine complète
    else {
      // Découper la chaine et on stocke les différents paramètres dans un tableau
      String[] listeparametres = Constantes.splitString(contenu, ' ');
      
      // Parcourir la liste afin d'en extraire les infos
      IdParametreModule idParametre;
      ParametreModule parametre;
      for (int i = 0; i < listeparametres.length; i++) {
        String chaine = listeparametres[i].trim();
        if (chaine.isEmpty()) {
          continue;
        }
        
        // Recherche le port serveur Newsim
        if (chaine.toLowerCase().startsWith("-p=")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.NEWSIM_PORT);
          parametre = new ParametreModule(idParametre, chaine.substring(3), EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
        // Recherche si le déboggueur doit être activé
        else if (chaine.toLowerCase().equalsIgnoreCase("-d")) {
          idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.NEWSIM_MODE_DEBUG);
          parametre = new ParametreModule(idParametre, 1, EnumOrigineParametre.PAR_DEFAUT);
          if (!listeParametre.contains(idParametre)) {
            listeParametre.add(parametre);
          }
        }
      }
    }
    
    Trace.info("\tFin conversion des paramètres pour Newsim.");
    return listeParametre;
  }
  
  /**
   * Converti les paramètres du MailManager.
   * L'origine des valeurs des paramètres sont défini par défaut car elles doivent être modifiables puisqu'il s'agit d'une conversion.
   * @return
   */
  private ListeParametreModule convertirParametreMailManager(ListeParametreModule pListeParametre) {
    Trace.info("\tDébut conversion des paramètres pour le mailManager.");
    
    // Vérification de la présence d'un mailManager
    File warMailManager = new File(ManagerAS400.getDossierRacineServeur() + "/web/tomcat/webapps/SerieNMailManager.war");
    if (!warMailManager.exists()) {
      Trace.info(
          "\t\tLe mailManager n'est pas installé sur cet environnement, la procédure de migration du mailManager ne sera pas exécutée.");
      return null;
    }
    
    // Lecture du fichier context.xml
    pListeParametre = extraireParametreContexte(pListeParametre);
    
    // Suppression du war du mailManager et de son dossier si présent
    warMailManager.delete();
    FileNG dossierMailManager = new FileNG(ManagerAS400.getDossierRacineServeur() + "/web/tomcat/webapps/SerieNMailManager");
    if (dossierMailManager.exists()) {
      FileNG.remove(dossierMailManager);
    }
    
    Trace.info("\tFin conversion des paramètres pour le mailManager.");
    return pListeParametre;
  }
  
  /**
   * Extraction des paramètres du mailManager du fichier context.xml de Tomcat.
   */
  private ListeParametreModule extraireParametreContexte(ListeParametreModule pListeParametre) {
    // @formatter:off
    String[][] tableauMarqueur = new String[][] { { "\"MAIL_FREQUENCE_SCAN\"", "mailmanager.delailecturemail" },
        { "\"MAIL_DEBUG\"", "mailmanager.debug" }, { "\"MAIL_MODE_TEST\"", "mailmanager.test" },
        { "\"MAIL_SIMULATION_ENVOI\"", "mailmanager.simulationenvoi" }, { "\"MAIL_ADRESSE_TEST\"", "mailmanager.adressemailtest" },
        { "\"MAIL_ADRESSE_TEST_FAX_RECEPTION\"", "mailmanager.adressemailfaxreception" }, { "\"MAIL_URL_PHOTO\"", "STOCKER" },
        { "\"LETTRE_ENV\"", "IGNORER" }, { "\"MAIL_PROFIL_AS\"", "IGNORER" }, { "\"MAIL_MP_AS\"", "IGNORER" } };
    // @formatter:on
    
    FileNG context = new FileNG(ManagerAS400.getDossierRacineServeur() + "/web/tomcat/conf/context.xml");
    if (!context.exists()) {
      Trace.erreur("Fichier " + context.getAbsolutePath() + " non trouvé.");
      return pListeParametre;
    }
    
    // Duplication du fichier
    String extension = DateHeure.getFormateDateHeure(DateHeure.AAAAMMJJ);
    String nouveauNom = context.getParentFile().getPath() + File.separator + context.getNamewoExtension(false) + '.' + extension;
    if (!context.copyTo(nouveauNom)) {
      Trace.info("\t\tLe fichier n'a pas été copié en " + nouveauNom);
    }
    GestionFichierTexte gft = new GestionFichierTexte(context);
    ArrayList<String> contenuOriginal = gft.getContenuFichier();
    
    // Suppression des commentaires
    String contenuChaine = gft.getContenuFichierString(true);
    contenuChaine = eliminerCommentaire(contenuChaine);
    gft.setContenuFichier(contenuChaine);
    
    // Ajout du paramètre pour le demarrage du module
    if (pListeParametre == null) {
      pListeParametre = new ListeParametreModule();
    }
    IdParametreModule idParametre = IdParametreModule.getInstanceAvecCreationId(EnumParametreModule.MAILMANAGER_DEMARRAGE);
    ParametreModule parametre = new ParametreModule(idParametre, 1, EnumOrigineParametre.PAR_DEFAUT);
    pListeParametre.add(parametre);
    
    // Extraction des paramètres
    Trace.info("\t\tExtraction des paramètres.");
    ArrayList<String> contenu = gft.getContenuFichier();
    for (String ligne : contenu) {
      for (int l = 0; l < tableauMarqueur.length; l++) {
        String marqueur = tableauMarqueur[l][0];
        String nomParametre = tableauMarqueur[l][1];
        // Les paramètres marqués comme ignorer
        if (nomParametre.equals("IGNORER")) {
          break;
        }
        if (ligne.contains(marqueur)) {
          String valeur = retournerValeurParametre(marqueur, ligne);
          if (valeur == null) {
            break;
          }
          // La paramètre url mail est stocké dans toutes les tables PSEMSMM
          if (nomParametre.equals("STOCKER")) {
            sauverUrlPhoto(valeur);
            break;
          }
          // Les paramètres de type ligne de commande
          idParametre = IdParametreModule.getInstanceAvecCreationId(nomParametre);
          parametre = new ParametreModule(idParametre, valeur, EnumOrigineParametre.PAR_DEFAUT);
          pListeParametre.add(parametre);
          break;
        }
      }
    }
    
    // Nettoyage des paramètres inutiles désormais du fichier context.xml
    int i = 0;
    boolean trouve = false;
    while (i < contenuOriginal.size()) {
      trouve = false;
      for (int l = 0; l < tableauMarqueur.length; l++) {
        String marqueur = tableauMarqueur[l][0];
        if (contenuOriginal.get(i).contains(marqueur)) {
          contenuOriginal.remove(i);
          trouve = true;
          break;
        }
      }
      if (!trouve) {
        i++;
      }
    }
    
    // Enregistrement du fichier context.xml modifié
    gft.setContenuFichier(contenuOriginal);
    gft.ecritureFichier();
    
    return pListeParametre;
  }
  
  /**
   * Elimine les commentaires du contenu du fichier context.xml.
   * @param pContenu
   * @return
   */
  private String eliminerCommentaire(String pContenu) {
    Pattern p = Pattern.compile("<!--[\\s\\S]*?-->");
    Matcher m = p.matcher(pContenu);
    if (!m.find()) {
      return pContenu;
    }
    return m.replaceAll("").trim();
  }
  
  /**
   * Retourne la valeur d'un paramètre trouvé dans le context.xml.
   * @param pMarqueur
   * @param pLigne
   * @return
   */
  private String retournerValeurParametre(String pMarqueur, String pLigne) {
    pLigne = Constantes.normerTexte(pLigne);
    if (pLigne.isEmpty()) {
      return null;
    }
    Pattern p = Pattern.compile("value\\s*=\\s*\"([^\"]*)", Pattern.CASE_INSENSITIVE);
    Matcher m = p.matcher(pLigne);
    if (m.find()) {
      return m.group(1).trim();
    }
    return null;
  }
  
  /**
   * Enregistrement de l'url du logo des mails en table.
   */
  private void sauverUrlPhoto(String pUrlLogo) {
    pUrlLogo = Constantes.normerTexte(pUrlLogo);
    if (pUrlLogo.isEmpty()) {
      Trace.info("\t\tAucune url pour le logo des mails n'a été récupérée.");
      return;
    }
    
    // Récupération de la liste des bases de données à mettre à jour
    CritereBDD critere = new CritereBDD();
    critere.setLettreEnvironnement((ManagerAS400.getLettreEnvironnement()));
    SqlBaseDeDonnees sqlBaseDeDonnees = new SqlBaseDeDonnees(queryManager);
    List<IdBibliotheque> listeBaseDeDonnees = sqlBaseDeDonnees.chargerListeIdBaseDeDonnees(critere);
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      Trace.info("\t\tAucune base de données n'a été trouvée pour la lettre d'environnement " + ManagerAS400.getLettreEnvironnement());
      return;
    }
    
    // Retourne la liste des bases de données qui contiennent la table PSEMMAILMM
    SqlEnvironnement sqlEnvironnement = new SqlEnvironnement(queryManager);
    listeBaseDeDonnees =
        sqlEnvironnement.retournerListeBaseDeDonneesPresenceTable(EnumTableBDD.MAIL_CONFIGURATION_ENVOI, listeBaseDeDonnees);
    if (listeBaseDeDonnees == null || listeBaseDeDonnees.isEmpty()) {
      Trace.info("\t\tAucune base de données ne contient la table " + EnumTableBDD.MAIL_CONFIGURATION_ENVOI);
      return;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    for (IdBibliotheque idBaseDeDonnees : listeBaseDeDonnees) {
      // Ajout du champ SMLOGO dans la table PSEMMAILMM (Dans certains cas les recs seront passés après la migration)
      ajouterChampSMLOGO(idBaseDeDonnees);
      
      // Mise à jour du champ
      requeteSql.ajouter("update " + idBaseDeDonnees.getNom() + '.' + EnumTableBDD.MAIL_CONFIGURATION_ENVOI + " set ");
      requeteSql.ajouterValeurUpdate("SMLOGO", pUrlLogo);
      try {
        queryManager.requete(requeteSql.getRequete());
        Trace.info("\t\tLa table " + idBaseDeDonnees.getNom() + '.' + EnumTableBDD.MAIL_CONFIGURATION_ENVOI
            + " a été mise à jour avec " + pUrlLogo + '.');
      }
      catch (Exception e) {
        Trace.erreur("L'erreur est certainement dû au fait que les REC n'aient pas été passé sur cette base de données.");
      }
      requeteSql.effacerRequete();
    }
  }
  
  /**
   * Ajoute le champ SMLOGO à la table PSEMSMM car les REC sont passés après le émarrage de Série N en général.
   */
  private void ajouterChampSMLOGO(IdBibliotheque pIdBaseDeDonnees) {
    if (pIdBaseDeDonnees == null) {
      return;
    }
    
    RequeteSql requeteSql = new RequeteSql();
    try {
      // Ajout du champ SMLOGO
      requeteSql.ajouter("alter table " + pIdBaseDeDonnees.getNom() + '.' + EnumTableBDD.MAIL_CONFIGURATION_ENVOI
          + " add SMLOGO character(256) default ' '");
      queryManager.requete(requeteSql.getRequete());
      
      // Ajout du texte descriptif
      requeteSql.effacerRequete();
      requeteSql.ajouter("label on column " + pIdBaseDeDonnees.getNom() + '.' + EnumTableBDD.MAIL_CONFIGURATION_ENVOI
          + " (SMLOGO text is 'Url logo du mail')");
      queryManager.requete(requeteSql.getRequete());
      
      Trace.info("\t\tLe champ SMLOGO a été ajouté à la table " + pIdBaseDeDonnees.getNom() + '.'
          + EnumTableBDD.MAIL_CONFIGURATION_ENVOI + '.');
    }
    catch (Exception e) {
      Trace.erreur("Le champ SMLOGO existe déjà.");
    }
  }
}
