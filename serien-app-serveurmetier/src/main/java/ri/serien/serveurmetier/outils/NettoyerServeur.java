/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurmetier.outils;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.List;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.CharacterDataArea;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.IFSFileFilter;
import com.ibm.as400.access.QSYSObjectPathName;

import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;

/**
 * Préparation de l'environnement lors du démarrage du serveur Serie N.
 */
public class NettoyerServeur {
  // Variables
  private String dossierRacine = null;
  // Liste des fichiers obsolètes à supprimer dans le dossier lib
  private String[] listFileToRemoveInLib =
      new String[] { "SerieNSrv.version", "faxnet.jks", "jt400Native.jar", "communAS400.jar", "SerieNSrv.jar", "commons-codec-1.4.jar" };
  // Liste des fichiers obsolètes à supprimer dans le dossier www
  private String[] listFileToRemoveInWeb = new String[] { "index.html", "installation.html", "SerieN.jar", "SerieN.version",
      "SerieN_09_06.jar", "SerieN_11_05.jar", "SerieN_11_07.jar", "SerieN_30_05.jar", "liste.ini", "jacob-1.17-x64.dll",
      "jacob-1.17-x86.dll", "jacob.jar", "balloontip-1.2.4.1.jar" };
  
  /**
   * Constructeur.
   */
  public NettoyerServeur(String pDossierRacine) {
    dossierRacine = pDossierRacine;
  }
  
  // -- Méthodes publiques
  
  /**
   * Traitement à effectuer lors des mises à jour de version Série N.
   */
  public void controleMAJVersion(AS400 pSysteme, List<SousEnvironnement> pListeSousEnvironnement) {
    // Conversion pour la migration 2.13 vers 2.14
    migration213vers214();
    
    // Traitement à faire sur chaque bibliothèque d'environnement trouvée
    for (SousEnvironnement sousEnvironnement : pListeSousEnvironnement) {
      // Controle des longueurs des DTAQ
      controlerLongueurDataqueue(pSysteme, sousEnvironnement.getBibliothequeEnvironnementClient());
      
      // Mise à jour systématique du programme SERIEN dans la bibliothèque (au cas où il y a eu une mise à jour de ce dernier)
      copierProgrammeRPGEnvironnementClient(pSysteme, sousEnvironnement);
    }
    
    // Suppression des fichiers inutiles dans le dossier www
    nettoyerDossierWeb();
    
    // Suppression des fichiers inutiles dans le dossier lib si besoin
    nettoyerDossierLib();
    
    // Contrôle de l'existence du dossier des Menus dans le dossier Config
    creerDossierConfig();
  }
  
  // -- Méthodes privées
  
  /**
   * Les opérations pour la migration de la version en 2.14.
   */
  private void migration213vers214() {
    boolean enregister = false;
    
    // Modification du chemin pour le programme des menus
    GestionFichierTexte gft = new GestionFichierTexte(dossierRacine + File.separatorChar + Constantes.FIC_CONFIG);
    ArrayList<String> contenu = gft.getContenuFichier();
    for (int i = 0; i < contenu.size(); i++) {
      if (contenu.get(i).trim().startsWith("program=exp.programs.MenuPrincipal.MenuPrincipal.class")) {
        contenu.set(i, "program=ri.serien.libas400.dao.exp.programs.MenuPrincipal.MenuPrincipal.class");
        enregister = true;
      }
    }
    if (enregister) {
      if (gft.ecritureFichier() == Constantes.ERREUR) {
        Trace.erreur("Problème lors de l'enregistrement du fichier " + gft.getNomFichier());
      }
      else {
        Trace.info("Enregistrement des modifications pour la migration en 2.14 dans le fichier " + gft.getNomFichier());
      }
    }
    gft.dispose();
    
    // Conversion des fichiers de préférences utilisateurs
    ConversionPreferencePour214 conversion = new ConversionPreferencePour214("preferences_");
    conversion.lancerConversion(dossierRacine + File.separatorChar + Constantes.DOSSIER_CONFIG);
  }
  
  /**
   * Contrôle la valeur de la longueur de la dataqueue des échanges et corrige si nécessaire.
   */
  private void controlerLongueurDataqueue(AS400 pSysteme, Bibliotheque pBibliothequeEnvironnementClient) {
    String nomBibliotheque = pBibliothequeEnvironnementClient.getNom();
    Trace.info("Contrôle de la bibliothèque d'environnement client " + pBibliothequeEnvironnementClient);
    try {
      // Récupération de la valeur dans la DATAAREA SGMDTAQ
      QSYSObjectPathName path = new QSYSObjectPathName(nomBibliotheque, "SGMDTAQ", "DTAARA");
      CharacterDataArea sgmdtaq = new CharacterDataArea(pSysteme, path.getPath());
      String length = sgmdtaq.read(10, 5);
      if (!length.equals(String.format("%05d", Constantes.DATAQLONG))) {
        // On met à jour la longueur
        sgmdtaq.write(String.format("%05d", Constantes.DATAQLONG), 10, 5);
        Trace.info("Mise à jour de la longueur des DTAQ dans " + pBibliothequeEnvironnementClient + "/SGMDTAQ : " + Constantes.DATAQLONG);
        // On supprime toutes les datq car elles ont l'ancienne taille
        IFSFile lib = new IFSFile(pSysteme, QSYSObjectPathName.toPath("QSYS", nomBibliotheque, "LIB"));
        IFSFile[] contenu = lib.listFiles(new IFSFileFilter() {
          
          // @Override
          @Override
          public boolean accept(IFSFile obj) {
            if (obj.getName().endsWith(".DTAQ")) {
              return true;
            }
            return false;
          }
        });
        for (int i = 0; i < contenu.length; i++) {
          Trace.info("Suppression de " + contenu[i].getAbsolutePath() + " réussi : " + contenu[i].delete());
        }
        Trace.info("Suppression terminée des DTAQ contenues dans " + pBibliothequeEnvironnementClient);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur dans controlerLongueurDataqueue.");
    }
  }
  
  /**
   * Mise à jour du programme SERIEN si nécessaire dans les bibliothèques disponibles.
   * Peut etre inutile car on dirait que c'est celui de EXPAS qui est utilisé (à confirmer).
   */
  private void copierProgrammeRPGEnvironnementClient(AS400 systeme, SousEnvironnement pSousEnvironnement) {
    Bibliotheque bibliothequeEnvironnementClient = pSousEnvironnement.getBibliothequeEnvironnementClient();
    String nombibliothequeEnvironnementClient = null;
    if (bibliothequeEnvironnementClient != null) {
      nombibliothequeEnvironnementClient = bibliothequeEnvironnementClient.getNom();
    }
    Character lettre = pSousEnvironnement.getLettreExecution();
    
    Trace.info("Contrôle du programme SERIEN de " + pSousEnvironnement.getBibliothequeEnvironnementClient());
    try {
      IFSFile serienOri = new IFSFile(systeme, new QSYSObjectPathName(lettre + "EXPAS", "SERIEN", "PGM").getPath());
      IFSFile serienDst = new IFSFile(systeme, new QSYSObjectPathName(nombibliothequeEnvironnementClient, "SERIEN", "PGM").getPath());
      long lastModifiedDst = 0;
      if (serienDst.exists()) {
        lastModifiedDst = serienDst.lastModified();
      }
      
      if (serienOri.lastModified() > lastModifiedDst) {
        Trace.info("Mise à jour du programme SERIEN - Controle de " + nombibliothequeEnvironnementClient);
        // Suppresion du programme
        if (serienDst.exists()) {
          if (serienDst.delete()) {
            Trace.info("Réussite de la suppression du programme SERIEN de " + nombibliothequeEnvironnementClient);
          }
          else {
            Trace.info("Echec de la suppression du programme SERIEN de " + nombibliothequeEnvironnementClient);
          }
        }
        // Copie du programme
        final CommandCall cmd = new CommandCall(systeme);
        String commande =
            "CRTDUPOBJ OBJ(SERIEN) FROMLIB(" + lettre + "EXPAS) OBJTYPE(*PGM) TOLIB(" + nombibliothequeEnvironnementClient + ")";
        if (cmd.run(commande) != true) {
          Trace.erreur("Echec de la copie du programme SERIEN de " + lettre + "EXPAS vers " + nombibliothequeEnvironnementClient);
        }
        else {
          Trace.info("Réussite de la copie du programme SERIEN de " + lettre + "EXPAS vers " + nombibliothequeEnvironnementClient);
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur dans copyPrgInBibEnv ");
    }
  }
  
  /**
   * Supprime tous les fichiers obsolètes du dossier LIB.
   */
  private void nettoyerDossierLib() {
    File dossierLib = new File(dossierRacine + File.separatorChar + Constantes.DOSSIER_LIB);
    File[] listeFichiers = dossierLib.listFiles();
    if (listeFichiers == null) {
      return;
    }
    
    for (File file : listeFichiers) {
      for (String file2remove : listFileToRemoveInLib) {
        if (file.getName().equals(file2remove)) {
          if (file.delete()) {
            Trace.info("Suppression de  " + file.getAbsolutePath() + " effectuée.");
          }
        }
      }
    }
    Trace.info("Nettoyage du dossier " + dossierLib.getAbsolutePath() + " effectué.");
  }
  
  /**
   * Supprime tous les fichiers obsolètes du dossier WWW.
   */
  private void nettoyerDossierWeb() {
    File dossierWeb = new File(dossierRacine + File.separatorChar + Constantes.DOSSIER_WEB);
    List<File> listeFichiers = FileNG.filtreFichier(dossierWeb, null, new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return true;
      }
    });
    if (listeFichiers == null) {
      return;
    }
    
    for (File file : listeFichiers) {
      for (String file2remove : listFileToRemoveInWeb) {
        if (file.getName().equals(file2remove)) {
          if (file.delete()) {
            Trace.info("Suppression de  " + file.getAbsolutePath() + " effectuée.");
          }
        }
      }
    }
    Trace.info("Nettoyage du dossier " + dossierWeb.getAbsolutePath() + " effectué.");
  }
  
  /**
   * Création du dossier menus s'il n'existe pas.
   */
  private void creerDossierConfig() {
    File dossier =
        new File(dossierRacine + File.separatorChar + Constantes.DOSSIER_CONFIG + File.separatorChar + Constantes.DOSSIER_MENUS);
    if (!dossier.exists()) {
      dossier.mkdirs();
    }
  }
  
}
