/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libas400.basedocumentaire.BaseDocumentaire;
import ri.serien.libas400.dao.gvx.programs.UtilisateurETB_MAG;
import ri.serien.libas400.dao.sql.comptabilite.parametres.SqlComptabiliteParametres;
import ri.serien.libas400.dao.sql.exploitation.SqlCommune;
import ri.serien.libas400.dao.sql.exploitation.SqlMemo;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlExploitationParametres;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlParametreAvance;
import ri.serien.libas400.dao.sql.exploitation.securite.SqlSecurite;
import ri.serien.libas400.dao.sql.gescom.SqlBlocNote;
import ri.serien.libas400.dao.sql.gescom.codepostal.SqlCodePostal;
import ri.serien.libas400.dao.sql.gescom.communezonegeographique.SqlCommuneZoneGeographique;
import ri.serien.libas400.dao.sql.gescom.devise.SqlDevise;
import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.memo.FiltreMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.ListeMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.comptabilite.personnalisation.journal.CriteresRechercheJournaux;
import ri.serien.libcommun.comptabilite.personnalisation.journal.ListeJournal;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.documentstocke.CritereDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.DocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.ListeDocumentStocke;
import ri.serien.libcommun.exploitation.parametreavance.CritereParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.IdParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ListeParametreAvance;
import ri.serien.libcommun.exploitation.parametreavance.ParametreAvance;
import ri.serien.libcommun.exploitation.personnalisation.categoriecontact.ListeCategorieContact;
import ri.serien.libcommun.exploitation.personnalisation.civilite.ListeCivilite;
import ri.serien.libcommun.exploitation.personnalisation.flux.CriterePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.flux.ListePersonnalisationFlux;
import ri.serien.libcommun.exploitation.personnalisation.zonepersonnaliseeexploitation.ListeZonePersonnaliseeExploitation;
import ri.serien.libcommun.exploitation.profil.UtilisateurGescom;
import ri.serien.libcommun.exploitation.securiteutilisateurgescom.SecuriteUtilisateurGescom;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.blocnotes.IdBlocNote;
import ri.serien.libcommun.gescom.commun.devise.CriteresRechercheDevise;
import ri.serien.libcommun.gescom.commun.devise.ListeDevise;
import ri.serien.libcommun.gescom.commun.etablissement.Etablissement;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.etablissement.ListeEtablissement;
import ri.serien.libcommun.gescom.personnalisation.acheteur.CriteresRechercheAcheteur;
import ri.serien.libcommun.gescom.personnalisation.acheteur.ListeAcheteur;
import ri.serien.libcommun.gescom.personnalisation.affaire.CriteresRechercheAffaires;
import ri.serien.libcommun.gescom.personnalisation.affaire.ListeAffaire;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.CriteresRechercheCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.canaldevente.ListeCanalDeVente;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.CriteresRechercheCategoriesClients;
import ri.serien.libcommun.gescom.personnalisation.categorieclient.ListeCategorieClient;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.CriteresRechercheCategoriesFournisseur;
import ri.serien.libcommun.gescom.personnalisation.categoriefournisseur.ListeCategorieFournisseur;
import ri.serien.libcommun.gescom.personnalisation.codepostal.CriteresCodePostal;
import ri.serien.libcommun.gescom.personnalisation.codepostal.ListeCodePostal;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.CritereCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.communezonegeographique.ListeCommuneZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.CriteresConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.conditioncommissionnement.ListeConditionCommissionnement;
import ri.serien.libcommun.gescom.personnalisation.famille.CritereFamille;
import ri.serien.libcommun.gescom.personnalisation.famille.ListeFamille;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.CritereFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.formuleprix.ListeFormulePrix;
import ri.serien.libcommun.gescom.personnalisation.groupe.CritereGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupe.ListeGroupe;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.CritereGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.groupeconditionvente.ListeGroupeConditionVente;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.CritereLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.lieutransport.ListeLieuTransport;
import ri.serien.libcommun.gescom.personnalisation.magasin.CritereMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.ListeMagasin;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.CriteresModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ListeModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.modeexpedition.ModeExpedition;
import ri.serien.libcommun.gescom.personnalisation.motifretour.ListeMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.CriteresParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.participationfraisexpedition.ListeParticipationFraisExpedition;
import ri.serien.libcommun.gescom.personnalisation.pays.CriteresPays;
import ri.serien.libcommun.gescom.personnalisation.pays.ListePays;
import ri.serien.libcommun.gescom.personnalisation.reglement.CritereModeReglement;
import ri.serien.libcommun.gescom.personnalisation.reglement.ListeModeReglement;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.CriteresRechercheSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sectionanalytique.ListeSectionAnalytique;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.CritereSousFamilles;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.personnalisation.tarif.CriteresRechercheTarifs;
import ri.serien.libcommun.gescom.personnalisation.tarif.ListeTarif;
import ri.serien.libcommun.gescom.personnalisation.tarif.Tarif;
import ri.serien.libcommun.gescom.personnalisation.transporteur.CriteresRechercheTransporteurs;
import ri.serien.libcommun.gescom.personnalisation.transporteur.ListeTransporteur;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.ListeTypeAvoir;
import ri.serien.libcommun.gescom.personnalisation.typefacturation.ListeTypeFacturation;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.CriteresRechercheTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typegratuit.ListeTypeGratuit;
import ri.serien.libcommun.gescom.personnalisation.typestockagemagasin.ListeTypeStockageMagasin;
import ri.serien.libcommun.gescom.personnalisation.unite.CriteresRechercheUnites;
import ri.serien.libcommun.gescom.personnalisation.unite.IdUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.ListeUnite;
import ri.serien.libcommun.gescom.personnalisation.unite.Unite;
import ri.serien.libcommun.gescom.personnalisation.vendeur.CriteresRechercheVendeurs;
import ri.serien.libcommun.gescom.personnalisation.vendeur.ListeVendeur;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.CritereZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.IdZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonegeographique.ListeZoneGeographique;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.CritereCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.personnalisation.zonepersonnalisee.ListeCategorieZonePersonnalisee;
import ri.serien.libcommun.gescom.vente.commune.CritereCommune;
import ri.serien.libcommun.gescom.vente.commune.ListeCommune;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceParametre;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service paramètre.
 */
public class ServiceParametre implements InterfaceServiceParametre {
  private static InterfaceServiceParametre service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceParametre() {
  }
  
  /**
   * Retourner l'implémentation du service parametre.
   */
  protected static InterfaceServiceParametre getService() {
    if (service == null) {
      InterfaceServiceParametre interfaceService = new ServiceParametre();
      service = (InterfaceServiceParametre) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceParametre.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  // -- Paramètres de la Gescom
  
  /**
   * Retourne les informations d'un utilisateur en gescom.
   */
  @Override
  public UtilisateurGescom chargerUtilisateurGescom(IdSession pIdSession, String pProfil, Bibliotheque pCurLib,
      IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemManager = sessionServeur.getSystemManager();
    UtilisateurETB_MAG user = new UtilisateurETB_MAG(systemManager);
    return user.chargerUtilisateurGescom(pProfil, pCurLib.getNom(), pIdEtablissement);
  }
  
  /**
   * Recherche des groupes suivant certains critères.
   */
  @Override
  public ListeGroupe chargerListeGroupe(IdSession pIdSession, CritereGroupe pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeGroupe(pCritere);
  }
  
  /**
   * Recherche des familles suivant certains critères.
   */
  @Override
  public ListeFamille chargerListeFamille(IdSession pIdSession, CritereFamille pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeFamille(pCriteres);
  }
  
  /**
   * Recherche des sous-familles suivant certains critères.
   */
  @Override
  public ListeSousFamille chargerListeSousFamille(IdSession pIdSession, CritereSousFamilles pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeSousFamille(pCriteres);
  }
  
  /**
   * Recherche des magasins suivant certains critères.
   */
  @Override
  public ListeMagasin chargerListeMagasin(IdSession pIdSession, CritereMagasin pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeMagasin(pCriteres);
  }
  
  /**
   * Lit les types de facturation pour un établissement.
   */
  @Override
  public ListeTypeFacturation chargerListeTypeFacturation(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTypeFacturation(pIdEtablissement);
  }
  
  /**
   * Lit les motifs de retour pour un établissement.
   */
  @Override
  public ListeMotifRetour chargerListeMotifRetour(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeMotifRetour(pIdEtablissement);
  }
  
  /**
   * Lit les types de stockages magasin pour un établissement.
   */
  @Override
  public ListeTypeStockageMagasin chargerListeTypeStockageMagasin(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTypeStockageMagasin(pIdEtablissement);
  }
  
  /**
   * Recherche des unités suivant certains critères.
   */
  @Override
  public ListeUnite chargerListeUnite(IdSession pIdSession, CriteresRechercheUnites pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeUnite(pCriteres);
  }
  
  /**
   * Recherche des catégories clients suivant certains critères.
   */
  @Override
  public ListeCategorieClient chargerListeCategorieClient(IdSession pIdSession, CriteresRechercheCategoriesClients pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeCategorieClient(pCriteres);
  }
  
  /**
   * Retourne la liste de tous les établissements
   */
  @Override
  public ListeEtablissement chargerListeEtablissement(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeEtablissement();
  }
  
  /**
   * Charger un établissement à partir de son id.
   */
  @Override
  public Etablissement chargerEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerEtablissement(pIdEtablissement);
  }
  
  /**
   * Retourne la liste des transporteurs suivant les critères donnés.
   */
  @Override
  public ListeTransporteur chargerListeTransporteur(IdSession pIdSession, CriteresRechercheTransporteurs pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTransporteur(pCriteres);
  }
  
  /**
   * Retourne la liste des lieu transport suivant les critères donnés.
   */
  @Override
  public ListeLieuTransport chargerListeLieuTransport(IdSession pIdSession, CritereLieuTransport pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeLieuTransport(pCriteres);
  }
  
  /**
   * Retourne la liste des règlements suivant les critères donnés.
   */
  @Override
  public ListeModeReglement chargerListeModeReglement(IdSession pIdSession, CritereModeReglement pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeModeReglement(pCriteres);
  }
  
  /**
   * Retourne la liste des vendeurs suivant les critères donnés.
   */
  @Override
  public ListeVendeur chargerListeVendeur(IdSession pIdSession, CriteresRechercheVendeurs pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeVendeur(pCriteres);
  }
  
  /**
   * Retourne la liste des acheteurs suivant les critères donnés.
   */
  @Override
  public ListeAcheteur chargerListeAcheteur(IdSession pIdSession, CriteresRechercheAcheteur pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeAcheteur(pCriteres);
  }
  
  /**
   * Retourne la liste des tarifs suivant les critères donnés.
   */
  @Override
  public ListeTarif chargerListeTarif(IdSession pIdSession, CriteresRechercheTarifs pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTarif(pCriteres);
  }
  
  /**
   * Lit les données d'un tarif à partir de son code.
   */
  @Override
  public Tarif chargerTarif(IdSession pIdSession, IdEtablissement pIdEtablissement, String pCodeTarif) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerTarif(pIdEtablissement, pCodeTarif);
  }
  
  /**
   * Charger la liste des paramètres systèmes d'un établissement.
   */
  @Override
  public ListeParametreSysteme chargerListeParametreSysteme(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeParametreSysteme(pIdEtablissement);
  }
  
  /**
   * Retourne la liste des civilités suivant les critères donnés.
   */
  @Override
  public ListeCivilite chargerListeCivilite(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExploitationParametres parametres = new SqlExploitationParametres(queryManager);
    return parametres.chargerListeCivilite();
  }
  
  /**
   * Retourne la liste des catégories contact suivant les critères donnés.
   */
  @Override
  public ListeCategorieContact chargerListeCategorieContact(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExploitationParametres parametres = new SqlExploitationParametres(queryManager);
    return parametres.chargerListeCategorieContact();
  }
  
  /**
   * Retourne la liste des zones personnalisées du module exploitation suivant les critères donnés.
   */
  @Override
  public ListeZonePersonnaliseeExploitation chargerListeZonePersonnaliseeExploitation(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExploitationParametres parametres = new SqlExploitationParametres(queryManager);
    return parametres.chargerListeZonePersonnaliseeExploitation();
  }
  
  /**
   * Retourne la liste des personnalisations de flux suivant les critères donnés.
   */
  @Override
  public ListePersonnalisationFlux chargerListePersonnalisationFlux(IdSession pIdSession, CriterePersonnalisationFlux pCritere)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExploitationParametres parametres = new SqlExploitationParametres(queryManager);
    return parametres.chargerListePersonnalisationFlux(pCritere);
  }
  
  /**
   * Retourne la liste des modes d'expédition suivant les critères donnés.
   */
  @Override
  public ListeModeExpedition chargerListeModeExpedition(IdSession pIdSession, CriteresModeExpedition pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeModeExpedition(pCriteres);
  }
  
  /**
   * Charge les données d'un mode d'expédition
   */
  @Override
  public ModeExpedition chargerModeExpedition(IdSession pIdSession, ModeExpedition pModeExpedition) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerModeExpedition(pModeExpedition);
  }
  
  /**
   * Retourne la liste des journaux suivant les critères donnés.
   */
  @Override
  public ListeJournal chargerListeJournal(IdSession pIdSession, CriteresRechercheJournaux pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlComptabiliteParametres parametres = new SqlComptabiliteParametres(queryManager);
    return parametres.chargerListeJournal(pCriteres);
  }
  
  // -- Autres
  
  /**
   * Retourne la liste des communes suivant les critères donnés.
   */
  @Override
  public ListeCommune chargerListeCommune(IdSession pIdSession, CritereCommune pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlCommune operation = new SqlCommune(sessionServeur.getQueryManager());
    return operation.chargerListeCommune(pCriteres);
  }
  
  /**
   * Recherche des mémos suivant des critères données.
   */
  @Override
  public ListeMemo chargerListeMemo(IdSession pIdSession, FiltreMemo pFiltreMemo) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMemo operation = new SqlMemo(sessionServeur.getQueryManager());
    return operation.chargerListeMemo(pFiltreMemo);
  }
  
  /**
   * Lit un mémo.
   */
  @Override
  public Memo chargerMemo(IdSession pIdSession, IdMemo pIdMemo) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMemo operation = new SqlMemo(sessionServeur.getQueryManager());
    return operation.chargerMemo(pIdMemo);
  }
  
  /**
   * Ecrit un mémo.
   */
  @Override
  public void sauverMemo(IdSession pIdSession, Memo pMemo) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMemo operation = new SqlMemo(sessionServeur.getQueryManager());
    operation.sauverMemo(pMemo);
  }
  
  /**
   * Supprime un mémo.
   */
  @Override
  public void supprimerMemo(IdSession pIdSession, IdMemo pIdMemo) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMemo operation = new SqlMemo(sessionServeur.getQueryManager());
    operation.supprimerMemo(pIdMemo);
  }
  
  /**
   * Lire un bloc-notes.
   */
  @Override
  public BlocNote chargerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocNote) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBlocNote operation = new SqlBlocNote(sessionServeur.getQueryManager());
    return operation.chargerBlocNote(pIdBlocNote);
  }
  
  /**
   * Ecrit un bloc-notes.
   */
  @Override
  public void sauverBlocNote(IdSession pIdSession, BlocNote pBlocNote) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBlocNote operation = new SqlBlocNote(sessionServeur.getQueryManager());
    operation.sauverBlocNote(pBlocNote);
  }
  
  /**
   * Supprime un bloc-notes.
   */
  @Override
  public void supprimerBlocNote(IdSession pIdSession, IdBlocNote pIdBlocNote) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBlocNote operation = new SqlBlocNote(sessionServeur.getQueryManager());
    operation.supprimerBlocNote(pIdBlocNote);
  }
  
  /**
   * Retourne une liste de pays
   */
  @Override
  public ListePays chargerListePays(IdSession pIdSession, CriteresPays pPays) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExploitationParametres operation = new SqlExploitationParametres(queryManager);
    return operation.chargerListePays(pPays);
  }
  
  /**
   * Retourne une liste de zone géographique
   */
  @Override
  public ListeZoneGeographique chargerListeZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeZoneGeographique(pCriteres);
  }
  
  /**
   * Retourne une liste d'IdZoneGeographique.
   */
  @Override
  public List<IdZoneGeographique> chargerListeIdZoneGeographique(IdSession pIdSession, CritereZoneGeographique pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeIdZoneGeographique(pCriteres);
  }
  
  /**
   * Retourne une commune d'une zone géographique suivant les critères donnés
   */
  @Override
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession, IdZoneGeographique pIdZoneGeographique)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlCommuneZoneGeographique operation = new SqlCommuneZoneGeographique(queryManager);
    return operation.chargerListeCommuneZoneGeographique(pIdZoneGeographique);
  }
  
  /**
   * Retourne une liste des toutes les communes des zone géographique
   */
  @Override
  public ListeCommuneZoneGeographique chargerListeCommuneZoneGeographique(IdSession pIdSession, CritereCommuneZoneGeographique pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlCommuneZoneGeographique operation = new SqlCommuneZoneGeographique(queryManager);
    return operation.chargerListeCommuneZoneGeographique(pCriteres);
  }
  
  /**
   * Retourne la liste des code postaux suivant les critères donnés
   */
  @Override
  public ListeCodePostal chargerListeCodePostal(IdSession pIdSession, CriteresCodePostal pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlCodePostal operation = new SqlCodePostal(queryManager);
    return operation.chargerListeCodePostal(pCriteres);
  }
  
  /**
   * Retourne la liste des devise suviant certain critères donnés
   */
  @Override
  public ListeDevise chargerListeDevise(IdSession pIdSession, CriteresRechercheDevise pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlDevise operation = new SqlDevise(queryManager);
    return operation.chargerListeDevise(pCriteres);
  }
  
  /**
   * Retourne la liste des affaires suivant les critères donnés
   */
  @Override
  public ListeAffaire chargerListeAffaire(IdSession pIdSession, CriteresRechercheAffaires pchargerListeAffaire) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeAffaire(pchargerListeAffaire);
  }
  
  /**
   * Retourne la liste des canal de vente suivant les critères données
   */
  @Override
  public ListeCanalDeVente chargerListeCanalDeVente(IdSession pIdSession, CriteresRechercheCanalDeVente pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeCanalDeVente(pCriteres);
  }
  
  /**
   * Retourne la liste des type gratuit suivant les critères données
   */
  @Override
  public ListeTypeGratuit chargerListeTypeGratuit(IdSession pIdSession, CriteresRechercheTypeGratuit pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTypeGratuit(pCriteres);
  }
  
  /**
   * Retourne la liste des SectionAnalytique suivent les critères données
   */
  @Override
  public ListeSectionAnalytique chargerListeSectionAnalytique(IdSession pIdSession, CriteresRechercheSectionAnalytique pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeSectionAnalytique(pCriteres);
  }
  
  /**
   * Retourne une unité sur la base de son identifiant
   */
  @Override
  public Unite chargerUnite(IdSession pIdSession, IdUnite pIdUnite) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerUnite(pIdUnite);
  }
  
  /**
   * Retourne une liste de catégories de zones personnalisées à l'aide du code établissement.
   */
  @Override
  public ListeCategorieZonePersonnalisee chargerListeCategorieZonePersonnalisee(IdSession pIdSession,
      CritereCategorieZonePersonnalisee pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeCategorieZonePersonnalisee(pCritere);
  }
  
  /**
   * Retourne la lsite des CategorieFournisseur
   */
  @Override
  public ListeCategorieFournisseur chargerListeCategorieFournisseur(IdSession pIdSession,
      CriteresRechercheCategoriesFournisseur pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeCategorieFournisseur(pCriteres);
  }
  
  /**
   * Retourne la liste des ConditionVente
   */
  @Override
  public ListeGroupeConditionVente chargerListeGroupeConditionVente(IdSession pIdSession, CritereGroupeConditionVente pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeGroupeConditionVente(pCriteres);
  }
  
  /**
   * Retourne la liste des ConditionCommissionnement
   */
  @Override
  public ListeConditionCommissionnement chargerListeConditionCommissionnement(IdSession pIdSession,
      CriteresConditionCommissionnement pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeConditionCommissionnement(pCriteres);
  }
  
  /**
   * Charger le sécurité gescom d'un utilisateur pour un établissement.
   */
  @Override
  public SecuriteUtilisateurGescom chargerSecuriteGescom(IdSession pIdSession, IdEtablissement pIdEtablissement, String pProfil) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlSecurite sqlSecuriteGescom = new SqlSecurite(queryManager);
    
    return sqlSecuriteGescom.chargerSecuriteGescom(pIdEtablissement, pProfil);
  }
  
  @Override
  public ListeTypeAvoir chargerListeTypeAvoir(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeTypeAvoir(pIdEtablissement);
  }
  
  /**
   * Charge la liste des document stocké
   */
  @Override
  public ListeDocumentStocke chargerListeDocumentStocke(IdSession pIdSession, CritereDocumentStocke pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    return BaseDocumentaire.listerDocumentStocke(queryManager, pCriteres);
  }
  
  /**
   * Charge le chemin complet d'un document stocké
   */
  @Override
  public String chargerCheminCompletDocumentStocke(IdSession pIdSession, DocumentStocke pDocumentStocke) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    return BaseDocumentaire.getCheminCompletDocumentStocke(queryManager, pDocumentStocke);
  }
  
  /**
   * Vérifie si un paramètre système donné est actif sur au moins un établissement de la base de données.
   */
  @Override
  public boolean isParametreSystemeActifSurUnEtablissement(IdSession pIdSession, EnumParametreSysteme pEnumParametreSysteme)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.isParametreSystemeActifSurUnEtablissement(pEnumParametreSysteme);
  }
  
  // ------
  // PARAMETRES AVANCES
  // ------
  
  /**
   * Charge une liste d'identifiants des paramètres avancés à partir de critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherche des paramètres avancés.
   * @return Liste d'identifiants de paramètres avancés.
   */
  @Override
  public List<IdParametreAvance> chargerListeIdParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(queryManager);
    return operation.chargerListeIdParametreAvance(pCriteres);
  }
  
  /**
   * Charge une liste de paramètres avancés à partir de critères de recherche.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pCriteres Critères de recherche de paramètres avancés
   * @return Liste de paramètres avancés.
   */
  @Override
  public ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, CritereParametreAvance pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(querym);
    return operation.chargerListeParametreAvance(pCriteres);
  }
  
  /**
   * Charge une liste de paramètres avancés à partir d'une liste d'identifiants.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pListeIdParametreAvance Liste d'identifiants de paramètres avancés.
   * @return Liste de paramètres avancés.
   */
  @Override
  public ListeParametreAvance chargerListeParametreAvance(IdSession pIdSession, List<IdParametreAvance> pListeIdParametreAvance)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(querym);
    return operation.chargerListeParametreAvance(pListeIdParametreAvance);
  }
  
  /**
   * Créer ou modifier un paramètre avancé en base.
   * 
   * @param pIdSession Identifiant de la sessions.
   * @param pParametreAvance Paramètre avancé à persister en base.
   */
  @Override
  public void sauverParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(querym);
    operation.sauverParametreAvance(pParametreAvance);
  }
  
  /**
   * Supprimer un paramètre avancé de la base.
   * 
   * @param pIdSession Identifiant de la session.
   * @param pParametreAvance Paramètre avancé à supprimer de la base.
   */
  @Override
  public void supprimerParametreAvance(IdSession pIdSession, ParametreAvance pParametreAvance) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(querym);
    operation.supprimerParametreAvance(pParametreAvance);
  }
  
  /**
   * Retourner l'identifiant maximal existant pour les paramètres avancés.
   * 
   * @param pIdSession
   * @return Identifiant maximal existant.
   */
  @Override
  public int getIdMaxParametreAvance(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlParametreAvance operation = new SqlParametreAvance(querym);
    return operation.getIdMaxParametreAvance();
  }
  
  /**
   * Charger une liste de participations aux frais d'expédition (personnalisation PE).
   */
  @Override
  public ListeParticipationFraisExpedition chargerListeParticipationFraisExpedition(IdSession pIdSession,
      CriteresParticipationFraisExpedition pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeParticipationFraisExpedition(pCriteres);
  }
  
  /**
   * Charger une liste de formules de prix (personnalisation FP).
   * 
   * @param pIdSession
   * @param pCriteres
   * @return La liste des formules de prix.
   */
  @Override
  public ListeFormulePrix chargerListeFormulePrix(IdSession pIdSession, CritereFormulePrix pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlGescomParametres operation = new SqlGescomParametres(queryManager);
    return operation.chargerListeFormulePrix(pCriteres);
  }
}
