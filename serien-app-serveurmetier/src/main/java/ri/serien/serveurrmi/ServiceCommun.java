/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.SqlContact;
import ri.serien.libas400.dao.sql.exploitation.configurationtableau.SqlColonneConfigurationTableau;
import ri.serien.libas400.dao.sql.exploitation.configurationtableau.SqlConfigurationTableau;
import ri.serien.libas400.dao.sql.exploitation.planification.SqlPlanification;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.commun.planification.IdPlanification;
import ri.serien.libcommun.commun.planification.ListePlanification;
import ri.serien.libcommun.commun.planification.Planification;
import ri.serien.libcommun.exploitation.configurationtableau.ConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.CritereConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.IdConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.ListeConfigurationTableau;
import ri.serien.libcommun.exploitation.configurationtableau.colonneconfigurationtableau.CritereConfigurationColonne;
import ri.serien.libcommun.exploitation.configurationtableau.colonneconfigurationtableau.ListeColonneConfigurationTableau;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.contact.Contact;
import ri.serien.libcommun.gescom.commun.contact.CritereContact;
import ri.serien.libcommun.gescom.commun.contact.IdContact;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.IdLienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.LienContact;
import ri.serien.libcommun.gescom.commun.contact.liencontact.ListeLienContact;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceCommun;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service commun qui va regrouper les traitements communs.
 */
public class ServiceCommun implements InterfaceServiceCommun {
  private static InterfaceServiceCommun service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceCommun() {
  }
  
  /**
   * Retourner l'implémentation du service commun.
   */
  protected static InterfaceServiceCommun getService() {
    if (service == null) {
      InterfaceServiceCommun interfaceService = new ServiceCommun();
      service = (InterfaceServiceCommun) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceCommun.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Commun est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Recherche les contacts correspondants aux critères passés.
   */
  @Override
  public List<IdContact> chargerListeIdContact(IdSession pIdSession, CritereContact pCritereContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeIdContact(pCritereContact);
  }
  
  /**
   * Charge une liste de contact à partir d'une liste d'id.
   */
  @Override
  public ListeContact chargerListeContact(IdSession pIdSession, List<IdContact> pListeIdContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeContact(pListeIdContact);
  }
  
  /**
   * Charger une liste de contacts à partir de critères de recherche.
   */
  @Override
  public ListeContact chargerListeContact(IdSession pIdSession, CritereContact pCritereContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeContact(pCritereContact);
  }
  
  @Override
  public Contact chargerContact(IdSession pIdSession, IdContact pIdContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerContact(pIdContact);
  }
  
  @Override
  public ListeLienContact chargerListeLienContact(IdSession pIdSession, List<IdLienContact> pListeIdLienContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeLienContact(pListeIdLienContact);
  }
  
  @Override
  public LienContact chargerLienContact(IdSession pIdSession, IdLienContact pIdLienContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerLienContact(pIdLienContact);
  }
  
  @Override
  public ListeLienContact chargerListeLienContactTiers(IdSession pIdSession, List<IdContact> pListeIdContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeLien(pListeIdContact);
  }
  
  @Override
  public ListeLienContact chargerListeLienContactTiers(IdSession pIdSession, IdContact pIdContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerListeLien(pIdContact);
  }
  
  @Override
  public Contact chargerContactPrincipalClient(IdSession pIdSession, IdClient pIdClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerContactPrincipalClient(pIdClient);
  }
  
  @Override
  public Contact chargerContactPrincipalFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chargerContactPrincipalFournisseur(pIdFournisseur);
  }
  
  @Override
  public IdContact chercherPremiereIdContactLibre(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.chercherPremiereIdContactLibre();
  }
  
  @Override
  public void sauverContact(IdSession pIdSession, Contact pContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    sqlContact.sauverContact(pContact);
  }
  
  @Override
  public void supprimerContact(IdSession pIdSession, IdContact pIdContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    sqlContact.supprimerContact(pIdContact);
  }
  
  @Override
  public void desactiverContact(IdSession pIdSession, Contact pContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    sqlContact.desactiverContact(pContact);
  }
  
  @Override
  public void activerContact(IdSession pIdSession, Contact pContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    sqlContact.activerContact(pContact);
  }
  
  @Override
  public boolean controlerExistenceLienClientParticulier(IdSession pIdSession, IdLienContact pIdLienContact) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlContact sqlContact = new SqlContact(querym);
    return sqlContact.controlerExistenceLienClientParticulier(pIdLienContact);
  }
  
  @Override
  public ListeConfigurationTableau chargerListeConfigurationTableau(IdSession pIdSession,
      CritereConfigurationTableau pCritereConfigurationTableau) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlConfigurationTableau sqlConfigurationTableau = new SqlConfigurationTableau(querym);
    return sqlConfigurationTableau.chargerListeConfigurationTableau(pCritereConfigurationTableau);
  }
  
  @Override
  public ConfigurationTableau chargerConfigurationTableau(IdSession pIdSession, IdConfigurationTableau pIdConfigurationTableau)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlConfigurationTableau sqlConfigurationTableau = new SqlConfigurationTableau(querym);
    return sqlConfigurationTableau.chargerConfigurationTableau(pIdConfigurationTableau);
  }
  
  @Override
  public ListeColonneConfigurationTableau chargerListeConfigurationColonne(IdSession pIdSession,
      CritereConfigurationColonne pCritereConfigurationColonne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlColonneConfigurationTableau sqlColonneConfigurationTableau = new SqlColonneConfigurationTableau(querym);
    return sqlColonneConfigurationTableau.chargerListeColonneConfigurationTableau(pCritereConfigurationColonne);
  }
  
  @Override
  public ConfigurationTableau sauverConfigurationTableau(IdSession pIdSession, ConfigurationTableau pConfiguration)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlConfigurationTableau sqlConfigurationTableau = new SqlConfigurationTableau(querym);
    return sqlConfigurationTableau.sauverConfigurationTableau(pConfiguration);
  }
  
  @Override
  public ListePlanification chargerListePlanification(IdSession pIdSession, List<IdPlanification> pListeIdPlanification)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlPlanification sqlSqlPlanification = new SqlPlanification(querym);
    return sqlSqlPlanification.chargerListePlanification(pListeIdPlanification);
  }
  
  @Override
  public Planification chargerPlanification(IdSession pIdSession, IdPlanification pIdPlanification) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlPlanification sqlSqlPlanification = new SqlPlanification(querym);
    return sqlSqlPlanification.chargerPlanification(pIdPlanification);
  }
  
  @Override
  public Planification sauverPlanification(IdSession pIdSession, Planification pPlanification) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlPlanification sqlSqlPlanification = new SqlPlanification(querym);
    return sqlSqlPlanification.sauverPlanification(pPlanification);
  }
}
