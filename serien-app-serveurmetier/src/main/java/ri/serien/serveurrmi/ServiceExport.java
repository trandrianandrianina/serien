/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libas400.dao.sql.exploitation.export.SqlExport;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.export.CritereExport;
import ri.serien.libcommun.exploitation.export.Export;
import ri.serien.libcommun.exploitation.export.IdExport;
import ri.serien.libcommun.exploitation.export.ListeExport;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceExport;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service export.
 */
public class ServiceExport implements InterfaceServiceExport {
  private static InterfaceServiceExport service;
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Constructeurs et fabriques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceExport() {
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes standards
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Tester que le service Export est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Retourner la liste des identifiants des exports à partir de critères passés en paramètre.
   * @param pIdSession La session.
   * @param pCritere Les critères de filtrage.
   * @return La liste des identifiants des exports.
   */
  @Override
  public List<IdExport> chargerListeIdExport(IdSession pIdSession, CritereExport pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExport sqlExport = new SqlExport(queryManager);
    return sqlExport.chargerListeIdExport(pCritere);
  }
  
  /**
   * Charger une liste d'exports à partir d'une liste d'identifiants.
   * Le chargement n'est pas complet c'est à dire que les colonnes ne sont pas chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   */
  @Override
  public ListeExport chargerListeExport(IdSession pIdSession, List<IdExport> pListeIdExport) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExport sqlExport = new SqlExport(queryManager);
    return sqlExport.chargerListeExport(pListeIdExport);
  }
  
  /**
   * Charger un export à partir d'un identifiant.
   * Le chargement est complet c'est à dire que les colonnes sont également chargées.
   * @param pIdSession La session.
   * @param pListeIdExport La liste d'identifiants d'exports.
   * @return La liste d'exports.
   */
  @Override
  public Export chargerExport(IdSession pIdSession, IdExport pIdExport) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExport sqlExport = new SqlExport(queryManager);
    return sqlExport.chargerExport(pIdExport);
  }
  
  /**
   * Sauver un export.
   * @param pIdSession La session.
   * @param pExport L'export.
   * @return L'export.
   */
  @Override
  public Export sauverExport(IdSession pIdSession, Export pExport) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlExport sqlExport = new SqlExport(queryManager);
    return sqlExport.sauverExport(pExport);
  }
  
  // ---------------------------------------------------------------------------------------------------------------------------------------
  // Méthodes statiques
  // ---------------------------------------------------------------------------------------------------------------------------------------
  
  /**
   * Retourner l'implémentation du service export.
   * @return L'implémentation du service export.
   */
  protected static InterfaceServiceExport getService() {
    if (service == null) {
      InterfaceServiceExport interfaceService = new ServiceExport();
      service = (InterfaceServiceExport) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceExport.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
}
