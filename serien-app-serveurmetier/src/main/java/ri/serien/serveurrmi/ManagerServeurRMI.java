/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.net.InetAddress;
import java.rmi.NoSuchObjectException;
import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.InterfaceServiceArticle;
import ri.serien.libcommun.rmi.InterfaceServiceClient;
import ri.serien.libcommun.rmi.InterfaceServiceCommun;
import ri.serien.libcommun.rmi.InterfaceServiceDocumentAchat;
import ri.serien.libcommun.rmi.InterfaceServiceDocumentVente;
import ri.serien.libcommun.rmi.InterfaceServiceExport;
import ri.serien.libcommun.rmi.InterfaceServiceFournisseur;
import ri.serien.libcommun.rmi.InterfaceServiceGVC;
import ri.serien.libcommun.rmi.InterfaceServiceInstallation;
import ri.serien.libcommun.rmi.InterfaceServiceMenu;
import ri.serien.libcommun.rmi.InterfaceServiceParametre;
import ri.serien.libcommun.rmi.InterfaceServiceRepresentant;
import ri.serien.libcommun.rmi.InterfaceServiceSession;
import ri.serien.libcommun.rmi.InterfaceServiceStock;
import ri.serien.libcommun.rmi.InterfaceServiceTechnique;

/**
 * Gestionnaire RMI côté serveur.
 * Centralise les méthodes utilisées pour la gestion des aspects RMI côté serveur : initialisation, enregistrement auprès du registre RMI,
 * gestion des entrées et sorties de services.
 */
public class ManagerServeurRMI {
  // Constantes
  private static final Map<String, Remote> listeServices = new HashMap<String, Remote>();
  
  // Variables
  private static String hostName = null;
  private static int portRegistre = 0;
  private static int portServices = 0;
  private static Registry registry = null;
  
  // -- Méthodes publiques
  
  /**
   * Initialiser les paramètres du serveur RMI.
   */
  public static void initialiserParametre(String pHostname, Integer pPortRegistre, Integer pPortServices) {
    hostName = pHostname;
    portRegistre = pPortRegistre;
    portServices = pPortServices;
    
    if (hostName == null) {
      // Utiliser la variable système si le paramètre n'a pas été fourni dans les paramètres
      hostName = System.getProperty("java.rmi.server.hostname");
      
      if (hostName == null) {
        // Sinon utilisé l'adresse correspondant au localhost
        try {
          InetAddress inetAddress = InetAddress.getLocalHost();
          hostName = inetAddress.getHostAddress();
        }
        catch (Exception e) {
          Trace.erreur(e, "Impossible de récupérer l'adresse correspondant à localhost.");
        }
      }
    }
    
    // Configurer le nom utilisé pour enregistrer les services RMI
    if (hostName != null) {
      System.setProperty("java.rmi.server.hostname", hostName);
    }
    else {
      // Une alternative serait de lire la dataarea QGPL/VGMDTA afin delire l'adresse IP de l'AS400 (testé en dur et cela fonctionne)
      Trace.info("java.rmi.server.hostname = " + hostName);
      Trace.alerte("Le hostname RMI est à null donc si le fichier host de l'AS400 n'est pas correctement renseigné,");
      Trace.alerte("le poste client tentera de contacter le serveur RMI sur l'adresse 127.0.0.1 ce qui déclenchera une erreur.");
      Trace.alerte("Deux solutions : ");
      Trace.alerte("- soit vous renseignez cette valeur à l'aide du paramètre -h en lançant le serveur");
      Trace.alerte("- soit vous lancez la commande CFGTCP de l'AS400 et vous ajoutez une ligne dans la table des hosts (option 10)");
      Trace.alerte("  contenant l'adresse IP utilisée et vous concaténez les valeurs Host name et Domain name de l'option 12");
    }
    
    // Vérifier que les paramètres obligatoires soient bien renseignés
    if (portRegistre <= 0) {
      throw new MessageErreurException("La valeur du port registre RMI est manquante ou erronée : portRegistre=" + portRegistre);
    }
    // Contrôle de la valeur du port services
    if (portServices <= 0) {
      portServices = portRegistre + 1;
    }
  }
  
  /**
   * Initialiser l'ensemble des services RMI.
   */
  public static void initialiserServices() {
    // Initialiser le registre RMI
    try {
      registry = LocateRegistry.createRegistry(portRegistre);
    }
    catch (RemoteException e) {
      throw new MessageErreurException(e, "Impossible de créer le registre RMI");
    }
    
    // Instancier les services
    listeServices.clear();
    ajouterImplementationService(ServiceArticle.NOMSERVICE, ServiceArticle.getService());
    ajouterImplementationService(ServiceClient.NOMSERVICE, ServiceClient.getService());
    ajouterImplementationService(ServiceDocumentAchat.NOMSERVICE, ServiceDocumentAchat.getService());
    ajouterImplementationService(ServiceDocumentVente.NOMSERVICE, ServiceDocumentVente.getService());
    ajouterImplementationService(ServiceFournisseur.NOMSERVICE, ServiceFournisseur.getService());
    ajouterImplementationService(ServiceGVC.NOMSERVICE, ServiceGVC.getService());
    ajouterImplementationService(ServiceMenu.NOMSERVICE, ServiceMenu.getService());
    ajouterImplementationService(ServiceParametre.NOMSERVICE, ServiceParametre.getService());
    ajouterImplementationService(ServiceRepresentant.NOMSERVICE, ServiceRepresentant.getService());
    ajouterImplementationService(ServiceSession.NOMSERVICE, ServiceSession.getService());
    ajouterImplementationService(ServiceStock.NOMSERVICE, ServiceStock.getService());
    ajouterImplementationService(ServiceTechnique.NOMSERVICE, ServiceTechnique.getService());
    ajouterImplementationService(ServiceCommun.NOMSERVICE, ServiceCommun.getService());
    ajouterImplementationService(ServiceInstallation.NOMSERVICE, ServiceInstallation.getService());
    ajouterImplementationService(ServiceExport.NOMSERVICE, ServiceExport.getService());
    
    // Enregistrer tous les services
    if (!enregistrerTousServices()) {
      throw new MessageErreurException("Impossible d'enregistrer les services RMI");
    }
    
    // Interroger le registre RMI pur vérifier que l'enregistrement des services est correct
    recupererService(InterfaceServiceArticle.NOMSERVICE);
    recupererService(InterfaceServiceClient.NOMSERVICE);
    recupererService(InterfaceServiceDocumentAchat.NOMSERVICE);
    recupererService(InterfaceServiceDocumentVente.NOMSERVICE);
    recupererService(InterfaceServiceFournisseur.NOMSERVICE);
    recupererService(InterfaceServiceGVC.NOMSERVICE);
    recupererService(InterfaceServiceMenu.NOMSERVICE);
    recupererService(InterfaceServiceParametre.NOMSERVICE);
    recupererService(InterfaceServiceRepresentant.NOMSERVICE);
    recupererService(InterfaceServiceSession.NOMSERVICE);
    recupererService(InterfaceServiceStock.NOMSERVICE);
    recupererService(InterfaceServiceTechnique.NOMSERVICE);
    recupererService(InterfaceServiceCommun.NOMSERVICE);
    recupererService(InterfaceServiceInstallation.NOMSERVICE);
    recupererService(InterfaceServiceExport.NOMSERVICE);
  }
  
  /**
   * Arrête tous les services RMI.
   */
  public static void arreterServices() {
    desenregistrerTousServices();
    listeServices.clear();
    
    if (registry != null) {
      try {
        UnicastRemoteObject.unexportObject(registry, true);
      }
      catch (NoSuchObjectException e) {
        Trace.erreur(e, "Erreur lors de libération du registre RMI.");
      }
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Ajouter une implementation pour un service.
   */
  private static boolean ajouterImplementationService(String nomService, Remote remote) {
    if (listeServices.containsKey(nomService)) {
      Trace.erreur("Une implémentation pour le service a déjà été ajoutée : " + nomService);
      return false;
    }
    
    listeServices.put(nomService, remote);
    return true;
  }
  
  /**
   * Enregistrer un service auprès du registre RMI.
   */
  private static boolean enregistrerService(final String nomService) {
    try {
      // Récupérer l'instance du service
      Remote remote = listeServices.get(nomService);
      if (remote == null) {
        Trace.erreur("Impossible d'enregistrer un service car son implémentation n'a pas été trouvée : " + nomService);
        return false;
      }
      
      try {
        UnicastRemoteObject.exportObject(remote, portServices);
      }
      catch (Exception e) {
      }
      registry.rebind(nomService, remote);
      
      Trace.info("Service RMI démarré : " + nomService);
    }
    
    catch (RemoteException e) {
      Trace.erreur(e, "Impossible d'enregistrer le service RMI auprès du registre RMI : " + nomService);
      return false;
    }
    catch (Exception e) {
      Trace.erreur(e, "Impossible de démarrer le service RMI : " + nomService);
      return false;
    }
    
    return true;
  }
  
  /**
   * Enregistrer tous les services.
   */
  private static boolean enregistrerTousServices() {
    try {
      for (Map.Entry<String, Remote> entry : listeServices.entrySet()) {
        if (!enregistrerService(entry.getKey())) {
          return false;
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Impossible de démarrer le service RMI");
      return false;
    }
    
    return true;
  }
  
  /**
   * Désenregistrer le service demandé.
   */
  private static boolean desenregistrerService(String pNomService) {
    try {
      if (!listeServices.containsKey(pNomService)) {
        Trace.erreur("Impossible de désenregistrer le service RMI car il n'a pas ete enregistré : : " + pNomService);
        return false;
      }
      
      Remote remote = listeServices.get(pNomService);
      if (remote != null) {
        try {
          UnicastRemoteObject.exportObject(remote, portServices);
        }
        catch (Exception e) {
        }
      }
      
      registry.unbind(pNomService);
      Trace.info("Service RMI arrêté : " + pNomService);
    }
    catch (NotBoundException e) {
      Trace.erreur(e, "Impossible de désenregistrer le service RMI car il n'est pas enregistré : " + pNomService);
      return false;
    }
    catch (RemoteException e) {
      Trace.erreur(e, "Impossible de désenregistrer le service RMI : " + pNomService);
      return false;
    }
    
    return true;
  }
  
  /**
   * Désenregistrer tous les services.
   */
  private static boolean desenregistrerTousServices() {
    try {
      for (Map.Entry<String, Remote> entry : listeServices.entrySet()) {
        desenregistrerService(entry.getKey());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Impossible d'arrêter le service RMI");
      return false;
    }
    
    return true;
  }
  
  /**
   * Récupérer le stub du service auprès du registre RMI.
   */
  private static Remote recupererService(String pNomService) {
    Trace.info("Récupérer le service RMI " + pNomService);
    Remote service = null;
    try {
      Registry registry = LocateRegistry.getRegistry("127.0.0.1", portRegistre);
      service = registry.lookup(pNomService);
      if (service == null) {
        Trace.erreur("Impossible de récupérer le service RMI " + pNomService);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Impossible de récupérer le service RMI " + pNomService);
    }
    
    if (service != null) {
      Trace.info(service.toString());
    }
    return service;
  }
  
  // -- Accesseurs
  
  /**
   * Nom du serveur à utiliser pour atteindre les services RMI.
   */
  public static String getHostName() {
    return hostName;
  }
  
  /**
   * Port du registre RMI.
   */
  public static int getPortRegistre() {
    return portRegistre;
  }
  
  /**
   * Port du service RMI.
   */
  public static int getPortServices() {
    return portServices;
  }
}
