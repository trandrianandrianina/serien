/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import ri.serien.libas400.dao.sql.exploitation.environnement.BibliothequeSpecifique;
import ri.serien.libas400.dao.sql.exploitation.menu.SqlMenus;
import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.menu.CriteresMenus;
import ri.serien.libcommun.exploitation.menu.EnregistrementMneMnp;
import ri.serien.libcommun.exploitation.menu.EnumModuleMenu;
import ri.serien.libcommun.exploitation.menu.MenuDetail;
import ri.serien.libcommun.exploitation.menu.ProgrammeALancer;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceMenu;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service menu.
 */
public class ServiceMenu implements InterfaceServiceMenu {
  private static InterfaceServiceMenu service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceMenu() {
  }
  
  /**
   * Retourner l'implémentation du service menu.
   */
  protected static InterfaceServiceMenu getService() {
    if (service == null) {
      InterfaceServiceMenu interfaceService = new ServiceMenu();
      service = (InterfaceServiceMenu) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceMenu.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Recherche les enregistrements correspondant aux modules des menus
   */
  @Override
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnp(IdSession pIdSession, CriteresMenus pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    return operation.chargerListeEnregistrementMneMnp(pCriteres);
  }
  
  /**
   * Retourne la liste des modules pour les menus spécifiques.
   */
  @Override
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifique(IdSession pIdSession, CriteresMenus pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    return operation.chargerListeEnregistrementMneMnpSpecifique(pCriteres);
  }
  
  /**
   * Retourne le détail d'un module pour les menus.
   */
  @Override
  public EnregistrementMneMnp[] rechercherDetailModulesMenu(IdSession pIdSession, CriteresMenus pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    return operation.rechercherDetailModulesMenu(pCriteres);
  }
  
  /**
   * Retourne le détail d'un module pour les menus spécifiques.
   */
  @Override
  public EnregistrementMneMnp[] chargerListeEnregistrementMneMnpSpecifiqueDetailSpecifique(IdSession pIdSession, CriteresMenus pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    EnregistrementMneMnp[] listeModules = operation.chargerListeEnregistrementMneMnpSpecifiqueDetailSpecifique(pCriteres);
    return listeModules;
  }
  
  /**
   * Lit les bibliothèques spécifique d'un environnement.
   */
  @Override
  public ArrayList<String> chargerListeBibliothequeSpecifiqueEnvironnement(IdSession pIdSession, Bibliotheque pBibliothequeEnv,
      Bibliotheque pCurlib, char pLettre) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    BibliothequeSpecifique operationSpe = new BibliothequeSpecifique(sessionServeur.getSystemManager());
    return operationSpe.chargerListeBibliothequeSpecifiqueEnvironnement(pBibliothequeEnv, pCurlib, "" + pLettre);
  }
  
  /**
   * Lit l'ensemble des enregistrements du menu.
   */
  @Override
  public LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetail(IdSession pIdSession, EnvUser pEnvUser)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Lecture des menus classiques
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> listeMenus = operation.lireMenu(pEnvUser.getLetter());
    if (listeMenus == null) {
      return null;
    }
    
    // Lecture des menus spécifiques
    LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> menuSpecifiques = operation.lireMenuSpecifique(pEnvUser.getListeBibSpe());
    if (menuSpecifiques != null) {
      listeMenus.putAll(menuSpecifiques);
    }
    
    // Contrôle si l'utilisateur à les droits d'accès sur chaque point de menus.
    for (Entry<MenuDetail, ArrayList<MenuDetail>> entry : listeMenus.entrySet()) {
      for (MenuDetail menuDetail : entry.getValue()) {
        // Contrôle le droit d'accès du point de menu
        controlerDroitAccesMenuDetail(sessionServeur.getQueryManager(), pEnvUser, menuDetail);
        // Modifie les droits d'accès si le point de menu est un noeud
        modifierDroitAccesListeEnfantMenuDetail(listeMenus, menuDetail);
      }
    }
    
    return listeMenus;
  }
  
  /**
   * Lit l'ensemble des enregistrements du menu d'un menu personnalisé.
   */
  @Override
  public LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> chargerListeMenuDetailPersonnalise(IdSession pIdSession, EnvUser pEnvUser,
      String pNomFichierMenuPerso) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Lecture des menus personnalisés
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> listeMenus =
        operation.chargerListeMenuDetailPersonnalise(pNomFichierMenuPerso, pEnvUser.getLetter());
    if (listeMenus == null) {
      return null;
    }
    
    // Contrôle si l'utilisateur à les droits d'accès sur chaque point de menus.
    for (Entry<MenuDetail, ArrayList<MenuDetail>> entry : listeMenus.entrySet()) {
      for (MenuDetail menuDetail : entry.getValue()) {
        // Contrôle le droit d'accès du point de menu
        controlerDroitAccesMenuDetail(sessionServeur.getQueryManager(), pEnvUser, menuDetail);
        // Modifie les droits d'accès si le point de menu est un noeud
        modifierDroitAccesListeEnfantMenuDetail(listeMenus, menuDetail);
      }
    }
    
    return listeMenus;
  }
  
  /**
   * Lit les informations programme d'un point de menu.
   */
  @Override
  public ProgrammeALancer chargerMenuProgramme(IdSession pIdSession, MenuDetail pPointDeMenu, char pLettreEnvironnement)
      throws RemoteException {
    if (pPointDeMenu == null) {
      throw new MessageErreurException("Le point de menu passé en paramètre est à null.");
    }
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    ProgrammeALancer menuProgramme = null;
    
    if (pPointDeMenu.getModule() != EnumModuleMenu.SPECIFIQUE.getCode()) {
      // Récupération des informations sur le programme à executer
      menuProgramme = operation.lireMenuProgramme(pPointDeMenu, pLettreEnvironnement);
    }
    else {
      // Récupération des informations sur le programme spécifique à executer
      menuProgramme = operation.lireMenuProgrammeSpecifique(pPointDeMenu);
    }
    // Récupération des bibliothèques à mettre en ligne pour ce programme
    if (menuProgramme != null) {
      operation.lireBibliothequesProgramme(pPointDeMenu, menuProgramme, pLettreEnvironnement);
    }
    return menuProgramme;
  }
  
  /**
   * Modifie un enregistrement du menu.
   */
  @Override
  public boolean sauverMenuDetail(IdSession pIdSession, MenuDetail pPointDeMenu) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlMenus operation = new SqlMenus(sessionServeur.getQueryManager());
    boolean retour = operation.modifierMenuDetail(pPointDeMenu);
    if (retour) {
      retour = operation.sauverMenuDetail(pPointDeMenu);
    }
    return retour;
  }
  
  // -- Méthodes privées
  
  /**
   * Contrôle les droits d'accès d'un menuDetail pour un utilisateur.
   */
  private void controlerDroitAccesMenuDetail(QueryManager pQueryManager, EnvUser pEnvUser, MenuDetail pMenuDetail) {
    if (pEnvUser == null) {
      throw new MessageErreurException("L'utilisateur est invalide.");
    }
    // Contrôle le droit d'accès uniquement si la variable autorisationAcces n'est pas initialisée
    if (pMenuDetail == null || pMenuDetail.getAutorisationAcces() != null) {
      return;
    }
    
    Boolean autorisationAcces = null;
    
    // Le point de menu n'est pas du tout autorisé quel que soit les droits de l'utilisateur
    if (pMenuDetail.getMNPTYP() == '9') {
      autorisationAcces = Boolean.FALSE;
    }
    // Le point de menu est autorisé suivant un test spécifique
    else if (pMenuDetail.getTypeDroitAccesAControler().equalsIgnoreCase("*TS33")) {
      autorisationAcces = Boolean.valueOf(verifierDroitTS33(pQueryManager));
    }
    // L'utilisateur à accès à tout pour l'instant sauf pour le module EXP
    else if (!pMenuDetail.getMNPP1().trim().equals(MenuDetail.EXPLOITATION)) {
      autorisationAcces = Boolean.TRUE;
    }
    
    // Cas particulier pour les droits sur l'exploitation pour les clients classiques et ASP
    // Si l'utilisateur a tous les droits alors il est autorisé (Option "Possibilités restreintes sur menu" dans la gestion de
    // l'utilisateur)
    else if (pEnvUser.getUSSPOR() == EnvUser.TOUS_LES_DROITS) {
      autorisationAcces = Boolean.TRUE;
    }
    // Cas particulier pour les droits sur l'exploitation pour les clients ASP uniquement
    // si l'utilisateur est autorisé malgré un accès restreint
    else if (pMenuDetail.isAutoriseMalgreAccesRestreint()) {
      autorisationAcces = Boolean.TRUE;
    }
    
    // Sinon pas d'autorisation d'accès
    else {
      autorisationAcces = Boolean.FALSE;
    }
    
    pMenuDetail.setAutorisationAcces(autorisationAcces);
  }
  
  /**
   * Modifie les droit d'accès des points de enfant si le menuDetail est un noeud.
   */
  private void modifierDroitAccesListeEnfantMenuDetail(LinkedHashMap<MenuDetail, ArrayList<MenuDetail>> pListeMenus,
      MenuDetail pMenuDetailParent) {
    if (pMenuDetailParent == null || !pMenuDetailParent.isTitre()) {
      return;
    }
    if (pMenuDetailParent.getTypeDroitAccesAControler().isEmpty()) {
      return;
    }
    
    // Parcourt la liste des menus afin de rechercher les points de menus enfants
    boolean autorisationAcces = pMenuDetailParent.isAutorisationAcces();
    for (Entry<MenuDetail, ArrayList<MenuDetail>> entry : pListeMenus.entrySet()) {
      MenuDetail ssmodule = entry.getKey();
      String code = pMenuDetailParent.concatenerMNPPX();
      for (MenuDetail menuDetail : pListeMenus.get(ssmodule)) {
        if (menuDetail.concatenerMNPPX().startsWith(code)) {
          menuDetail.setAutorisationAcces(Boolean.valueOf(autorisationAcces));
        }
      }
    }
  }
  
  /**
   * Vérifier les droits identifiés par le code *TS33.
   * Le code *TS33 est utilisé dans certains points de menu dans le champ MNPAR5 de la table MNPMSGM.
   * Il vérifie si la PS117 est activée sur au moins un établissement de la base de données en cours.
   */
  private boolean verifierDroitTS33(QueryManager pQueryManager) {
    if (pQueryManager == null) {
      throw new MessageErreurException("Le queryManager est invalide.");
    }
    if (pQueryManager.getLibrary() == null) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    
    SqlGescomParametres operation = new SqlGescomParametres(pQueryManager);
    return operation.isParametreSystemeActifSurUnEtablissement(EnumParametreSysteme.PS117);
  }
  
}
