/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.sql.Connection;
import java.util.Date;

import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.dao.sql.exploitation.traceconnexion.SqlTraceConnexion;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.exploitation.traceconnexion.EnumCodeDeconnexion;
import ri.serien.libcommun.exploitation.traceconnexion.IdTraceConnexion;
import ri.serien.libcommun.exploitation.traceconnexion.TraceConnexion;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.rmi.ManagerClientRMI;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Cette classe regroupe les méthodes pour gérer le serveur RMI.
 */
public class ServeurRMI implements InterfaceModule {
  // Variables
  private static EnumModule enumModule = EnumModule.SERVEURRMI;
  private static boolean moduleActif = false;
  private static ListeParametreModule listeParametre = null;
  private String hostname = null;
  private Integer portRegistre = null;
  private Integer portServices = null;
  private boolean modeDebug = false;
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return listeParametre;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return false;
  }
  
  /**
   * Démarre le module RMI.
   */
  @Override
  public void demarrer() {
    if (moduleActif) {
      Trace.alerte("Le module " + enumModule + " est déjà actif.");
      return;
    }
    Trace.info("Démarrage du module " + enumModule + '.');
    
    // Initialisation et contrôle des paramètres
    initialiserParametre();
    
    // Initialiser le services et le client RMI
    Trace.info("1 - Initialiser services RMI.");
    ManagerServeurRMI.initialiserServices();
    
    // Initialiser la connexion en tant que client RMI
    Trace.info("2 - Initialiser client RMI.");
    ManagerClientRMI.configurerServeur(hostname, portRegistre);
    
    moduleActif = true;
    Trace.info("Le module " + enumModule + " est démarré.");
    
    // Tracer le démarrage du module dans la table des traces de connexion
    tracerDemarrageModule();
  }
  
  /**
   * Arrête le module RMI.
   */
  @Override
  public void arreter() {
    if (!moduleActif) {
      Trace.alerte("Le module " + enumModule + " n'est pas actif.");
      return;
    }
    
    // Arrêt du contrôle des clients desktop
    ManagerClientDesktop.arreterControleClientDesktop();
    
    // Arrêt des services RMI
    ManagerServeurRMI.arreterServices();
    
    moduleActif = false;
    Trace.info("Module " + enumModule + " stoppé.");
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return moduleActif;
  }
  
  /**
   * Envoi un ordre au module RMI.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + enumModule + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case SERVEURRMI_START:
          // Recherche les paramètres pour ce module dans le cas où ils auraient été modifié
          ManagerParametreModule.rechargerParametre(pEnumControleModule.getEnumModule());
          // Démarre le module
          demarrer();
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return null;
        
        case SERVEURRMI_STOP:
          arreter();
          return null;
        
        case SERVEURRMI_STATUS:
          String retour;
          if (moduleActif) {
            retour = "Le module " + enumModule + " est démarré.";
          }
          else {
            retour = "Le module " + enumModule + " est arrêté.";
          }
          Trace.info(retour);
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return retour;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + enumModule + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser les paramètres du module à partir de la liste des paramètres pour ce module.
   */
  private void initialiserParametre() {
    // Extraction des paramètres spécifiques pour ce module
    listeParametre = ManagerParametreModule.extraireParametreModule(enumModule);
    if (listeParametre == null || listeParametre.isEmpty()) {
      throw new MessageErreurException("Aucun paramètre pour le module " + enumModule + " n'a été trouvé.");
    }
    
    // Le hostname
    hostname = listeParametre.retournerValeurEnString(EnumParametreModule.SERVEURRMI_NOM);
    
    // Le port de registre utilisé par le serveur
    portRegistre = listeParametre.retournerValeurEnInteger(EnumParametreModule.SERVEURRMI_PORT_REGISTRE);
    
    // Le port des services utilisé par le serveur
    portServices = listeParametre.retournerValeurEnInteger(EnumParametreModule.SERVEURRMI_PORT_SERVICES);
    
    // Le mode débug
    Integer valeur = listeParametre.retournerValeurEnInteger(EnumParametreModule.SERVEURRMI_MODE_DEBUG);
    if (valeur == null || valeur <= 0) {
      modeDebug = false;
    }
    else {
      modeDebug = true;
    }
    
    // Initialisation et contrôle des paramètres du manager RMI
    ManagerServeurRMI.initialiserParametre(hostname, portRegistre, portServices);
  }
  
  /**
   * Trace le démarrage du module dans la table des traces de connexion afin d'avoir un repère.
   */
  private void tracerDemarrageModule() {
    try {
      IdTraceConnexion idTraceConnexion = IdTraceConnexion.getInstanceAvecCreationId(ManagerAS400.getSystemeAS400().getUserId());
      TraceConnexion traceConnexion = new TraceConnexion(idTraceConnexion);
      traceConnexion.setDateDebut(new Date());
      traceConnexion.setAdresseIP("-- Démarrage du module " + enumModule.getLibelle() + " --");
      traceConnexion.setCodeDeconnexion(EnumCodeDeconnexion.NON_DEFINI);
      traceConnexion.setIdClientDesktop(null);
      traceConnexion.setTypeConnexion(null);
      traceConnexion.setNombreUtilisateur(null);
      
      // Connexion à DB2
      SystemeManager systemeManager = new SystemeManager(ManagerAS400.getSystemeAS400(), true);
      Connection connection = systemeManager.getDatabaseManager().getConnection();
      QueryManager queryManager = new QueryManager(connection);
      ListeParametreModule listeParametre = ManagerParametreModule.extraireParametreModule(EnumModule.SERVEURMETIER);
      ParametreModule parametreModule =
          listeParametre.retournerParametreAvecEnum(EnumParametreModule.SERVEURMETIER_BIBLIOTHEQUEENVIRONNEMENT);
      queryManager.setLibrairieEnvironnement(parametreModule.getValeurEnString());
      
      // Enregistrement en base
      SqlTraceConnexion operation = new SqlTraceConnexion(queryManager);
      idTraceConnexion = operation.sauverTraceConnexion(traceConnexion);
      traceConnexion.setId(idTraceConnexion);
      
      // Déconnexion de DB2
      queryManager.deconnecter();
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
  }
}
