/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.io.File;
import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libcommun.installation.FichierBinaire;
import ri.serien.libcommun.installation.InformationsFichierInstallation;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.rmi.InterfaceServiceInstallation;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Implémentation du service installation.
 */
public class ServiceInstallation implements InterfaceServiceInstallation {
  private static InterfaceServiceInstallation service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceInstallation() {
  }
  
  /**
   * Retourner l'implémentation du service installation.
   */
  protected static InterfaceServiceInstallation getService() {
    if (service == null) {
      InterfaceServiceInstallation interfaceService = new ServiceInstallation();
      service = (InterfaceServiceInstallation) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceInstallation.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Liste le contenu d'un dossier du serveur.
   * Le dossier est exempt de la racine (/xserien par exemple). Exemple, pDossier = "www"
   */
  @Override
  public List<String> listerDossierServeur(String pDossierRelatif) throws RemoteException {
    if (pDossierRelatif == null || pDossierRelatif.isEmpty()) {
      throw new MessageErreurException("Le nom du dossier est incorrect.");
    }
    String dossier = ManagerAS400.getDossierRacineServeur() + File.separator + pDossierRelatif;
    Trace.info("Liste le contenu du dossier " + dossier);
    File fdossier = new File(dossier);
    List<File> listeFichiers = FileNG.listeFichier(fdossier, null);
    if (listeFichiers == null || listeFichiers.isEmpty()) {
      return null;
    }
    
    // Construction d'une liste avec les noms des fichiers mais sans le dossier racine du serveur (/xserien par exemple)
    int longueurRacine = dossier.length();
    if (!dossier.endsWith(File.separator)) {
      longueurRacine++;
    }
    List<String> listeFichiersATelecharger = new ArrayList<String>();
    for (File fichier : listeFichiers) {
      listeFichiersATelecharger.add(fichier.getAbsolutePath().substring(longueurRacine));
    }
    return listeFichiersATelecharger;
  }
  
  /**
   * Retourne les informations de base d'un fichier (longueur, date dernière modification).
   */
  @Override
  public InformationsFichierInstallation retournerInformationsFichier(String pNomFichier) throws RemoteException {
    InformationsFichierInstallation infosFichier = new InformationsFichierInstallation();
    String dossierRacine = ManagerAS400.getDossierRacineServeur() + File.separator + Constantes.DOSSIER_WEB + File.separator;
    File fichier = new File(dossierRacine + pNomFichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    Trace.info("Rechercher le fichier :" + fichier.getAbsolutePath());
    if (fichier.exists()) {
      infosFichier.setNom(fichier.getAbsolutePath());
      infosFichier.setTaille(fichier.length());
      infosFichier.setDerniereModification(fichier.lastModified());
    }
    else {
      Trace.info("Le fichier " + fichier.getAbsolutePath() + " n'a pas été trouvé.");
    }
    return infosFichier;
  }
  
  /**
   * Retourne le fichier demandé.
   */
  @Override
  public FichierBinaire demanderFichier(String pNomFichier) throws RemoteException {
    Trace.info("Fichier à uploader " + pNomFichier);
    FichierBinaire gfb = new FichierBinaire();
    gfb.setNomFichier(pNomFichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    gfb.lectureFichier();
    return gfb;
  }
  
}
