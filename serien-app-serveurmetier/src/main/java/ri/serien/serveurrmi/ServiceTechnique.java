/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import ri.serien.commun.licence.ManagerLicence;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libas400.dao.rpg.exploitation.imprimante.RpgOperationsImprimantes;
import ri.serien.libas400.dao.sql.exploitation.SqlUtilisateur;
import ri.serien.libas400.dao.sql.exploitation.basededonnees.SqlBaseDeDonnees;
import ri.serien.libas400.dao.sql.exploitation.documentlie.SqlDocumentLie;
import ri.serien.libas400.dao.sql.exploitation.edition.SqlDemandeEdition;
import ri.serien.libas400.dao.sql.exploitation.licence.SqlLicence;
import ri.serien.libas400.dao.sql.exploitation.mail.SqlMail;
import ri.serien.libas400.dao.sql.exploitation.module.SqlModule;
import ri.serien.libas400.dao.sql.exploitation.notification.SqlNotification;
import ri.serien.libas400.dao.sql.exploitation.parametres.SqlExploitationParametres;
import ri.serien.libas400.dao.sql.gescom.flux.SqlFlux;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libas400.system.edition.GestionImprimanteAS400;
import ri.serien.libas400.system.edition.GestionSpoolAS400;
import ri.serien.libas400.system.objet.ObjetSavfAS400;
import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.ListeBDD;
import ri.serien.libcommun.commun.tablebdd.EnumTableBDD;
import ri.serien.libcommun.exploitation.IdServeurPower;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.CritereBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.exploitation.documentlie.IdDossierDocumentLie;
import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.libcommun.exploitation.edition.SessionEdition;
import ri.serien.libcommun.exploitation.edition.demandeedition.DemandeEdition;
import ri.serien.libcommun.exploitation.edition.imprimante.CritereImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.IdImprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.Imprimante;
import ri.serien.libcommun.exploitation.edition.imprimante.ListeImprimante;
import ri.serien.libcommun.exploitation.edition.spool.CritereSpool;
import ri.serien.libcommun.exploitation.edition.spool.IdSpool;
import ri.serien.libcommun.exploitation.edition.spool.ListeSpool;
import ri.serien.libcommun.exploitation.edition.spool.Spool;
import ri.serien.libcommun.exploitation.environnement.FichierRuntime;
import ri.serien.libcommun.exploitation.flux.CritereFlux;
import ri.serien.libcommun.exploitation.flux.Flux;
import ri.serien.libcommun.exploitation.flux.IdFlux;
import ri.serien.libcommun.exploitation.flux.ListeFlux;
import ri.serien.libcommun.exploitation.licence.CodeTypeLicence;
import ri.serien.libcommun.exploitation.licence.CritereLicence;
import ri.serien.libcommun.exploitation.licence.EnumStatutLicence;
import ri.serien.libcommun.exploitation.licence.EnumTypeLicence;
import ri.serien.libcommun.exploitation.licence.IdLicence;
import ri.serien.libcommun.exploitation.licence.Licence;
import ri.serien.libcommun.exploitation.licence.ListeLicence;
import ri.serien.libcommun.exploitation.mail.CritereMail;
import ri.serien.libcommun.exploitation.mail.CritereTypeMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeMail;
import ri.serien.libcommun.exploitation.mail.ListeTypeMail;
import ri.serien.libcommun.exploitation.mail.ListeVariableMail;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.TypeMail;
import ri.serien.libcommun.exploitation.module.CritereParametreModule;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.notification.CritereNotification;
import ri.serien.libcommun.exploitation.notification.IdNotification;
import ri.serien.libcommun.exploitation.notification.ListeNotification;
import ri.serien.libcommun.exploitation.notification.Notification;
import ri.serien.libcommun.exploitation.personnalisation.parametrent.ParametreNT;
import ri.serien.libcommun.exploitation.utilisateur.CritereUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.IdUtilisateur;
import ri.serien.libcommun.exploitation.utilisateur.ListeUtilisateur;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierByte;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceTechnique;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service technique.
 */
public class ServiceTechnique implements InterfaceServiceTechnique {
  private static InterfaceServiceTechnique service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceTechnique() {
  }
  
  /**
   * Retourner l'implémentation du service technique.
   */
  protected static InterfaceServiceTechnique getService() {
    if (service == null) {
      InterfaceServiceTechnique interfaceService = new ServiceTechnique();
      service = (InterfaceServiceTechnique) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceTechnique.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Activer ou désactiver les traces en mode debug sur le serveur.
   */
  @Override
  public void activerTraceDebug(boolean pDebug) throws RemoteException {
    Trace.setModeDebug(pDebug);
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Permet de confirmer au Desktop de certifier sa présence.
   */
  @Override
  public void confirmerPresenceClient(IdClientDesktop pIdClientDesktop) throws RemoteException {
    ManagerClientDesktop.confirmerPresenceClient(pIdClientDesktop);
  }
  
  // ==========================================================================
  // == Méthodes concernant les échanges de buffers
  // ==========================================================================
  
  /**
   * Envoi un message au programme RPG avec un délai d'attente en seconde.
   */
  @Override
  public String dialoguerAvecProgramme(IdSession pIdSession, String pMessage, int pDelaiAttente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.dialoguerAvecProgramme(pMessage, pDelaiAttente);
  }
  
  /**
   * Envoi un message au programme RPG.
   */
  @Override
  public boolean envoyerMessageAuProgramme(IdSession pIdSession, String pMessage) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.envoyerMessageAuProgramme(pMessage);
  }
  
  /**
   * Réceptionne un message du programme RPG avec un délai d'attente en seconde.
   */
  @Override
  public String recevoirMessageDuProgramme(IdSession pIdSession, int pDelaiAttente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.recevoirMessageDuProgramme(pDelaiAttente);
  }
  
  /**
   * Réceptionne un message du programme RPG.
   */
  @Override
  public void repondreMessageErreur(IdSession pIdSession, String pReponse) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    sessionServeur.repondreMessageErreur(pIdSession.getNumero(), pReponse);
  }
  
  // ==========================================================================
  // == Méthodes concernant les fichiers
  // ==========================================================================
  
  /**
   * Télécharge un fichier de l'IFS.
   * La variable pFichierSourceIFS est le chemin complet IFS et formaté, exemple: /documents/FMPRO/GVM/test.txt
   */
  @Override
  public GestionFichierByte telechargerFichier(IdSession pIdSession, String pFichierSourceIFS) throws RemoteException {
    pFichierSourceIFS = Constantes.normerTexte(pFichierSourceIFS);
    if (pFichierSourceIFS.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier à télécharger est invalide.");
    }
    GestionFichierByte gestionFichierByte = new GestionFichierByte();
    gestionFichierByte.setNomFichier(pFichierSourceIFS);
    gestionFichierByte.lectureFichier();
    return gestionFichierByte;
  }
  
  /**
   * Envoi vers le serveur (AS400) un fichier donné.
   */
  @Override
  public boolean televerserFichier(IdSession pIdSession, String pFichierDestinationIfs, GestionFichierByte pFichier)
      throws RemoteException {
    final File ufichier = new File(pFichierDestinationIfs.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    
    // On s'assure que le dossier existe dans le cas contraire on le créé
    if (!ufichier.getParentFile().exists()) {
      ufichier.getParentFile().mkdirs();
    }
    
    // Récupération du fichier
    if (pFichier == null || pFichier.getContenuFichier() == null) {
      return false;
    }
    
    // On enregistre le fichier
    pFichier.changeNomFichier(ufichier.getAbsolutePath());
    if (pFichier.ecritureFichier() == Constantes.OK) {
      Trace.info("Le fichier a été uploadé et enregistré : " + ufichier.getName());
    }
    else {
      return false;
    }
    
    return true;
  }
  
  /**
   * Supprime un fichier donné.
   */
  @Override
  public boolean supprimerFichier(IdSession pIdSession, String pNomFichier) throws RemoteException {
    pNomFichier = Constantes.normerTexte(pNomFichier);
    if (pNomFichier.isEmpty()) {
      return false;
    }
    // Nouveau fonctionnement
    if (pNomFichier.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR) == -1) {
      File fichier = new File(pNomFichier);
      if (fichier.exists()) {
        return fichier.delete();
      }
      return false;
    }
    
    // Ancien fonctionnement à chasser dans Série N
    // Récupération du nom du fichier et de sa destination
    String[] clesuppr = Constantes.splitString(pNomFichier, Constantes.SEPARATEUR_CHAINE_CHAR);
    clesuppr[0] = clesuppr[0].replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar);
    if (clesuppr[0].lastIndexOf(File.separatorChar) != clesuppr[0].length()) {
      clesuppr[0] = clesuppr[0] + File.separatorChar;
    }
    for (int i = 1; i < clesuppr.length; i++) {
      File rfichier = new File(clesuppr[0] + clesuppr[i]);
      if (rfichier.isFile()) {
        rfichier.delete();
        Trace.info("Supprimer le fichier : " + rfichier.getAbsolutePath());
      }
    }
    return true;
  }
  
  /**
   * Copie un fichier donné vers un chemin complet.
   * L'option est offerte de supprimer le fichier original.
   * 
   * @param pIdSession
   * @param pCheminFichierSource
   * @param pCheminFichierDestination
   * @param pSuppressionFichierOriginal
   * @return
   * @throws RemoteException
   */
  @Override
  public boolean copierFichier(IdSession pIdSession, String pCheminFichierSource, String pCheminFichierDestination,
      boolean pSuppressionFichierOriginal) throws RemoteException {
    pCheminFichierSource = Constantes.normerTexte(pCheminFichierSource);
    if (pCheminFichierSource.isEmpty()) {
      throw new MessageErreurException("Le chemin complet du fichier à copier est invalide.");
    }
    pCheminFichierDestination = Constantes.normerTexte(pCheminFichierDestination);
    if (pCheminFichierDestination.isEmpty()) {
      throw new MessageErreurException("Le chemin complet du fichier destination est invalide.");
    }
    // Contrôle l'existence du fichier source
    File fichierSource = new File(pCheminFichierSource);
    if (!fichierSource.exists()) {
      Trace.alerte("Le fichier " + pCheminFichierSource + " n'a pas été trouvé.");
      return false;
    }
    // Copie du fichier
    try {
      FileNG.copyFile(fichierSource, new File(pCheminFichierDestination));
    }
    catch (IOException e) {
      throw new MessageErreurException(e, "");
    }
    
    // Suppression du fichier source
    if (pSuppressionFichierOriginal) {
      return fichierSource.delete();
    }
    return true;
  }
  
  /**
   * Retourne la liste des fichiers contenus dans un dossier.
   */
  @Override
  public String demanderContenuDossier(IdSession pIdSession, String pNomDossier) throws RemoteException {
    String retour = null;
    
    // On vérifie l'existence du fichier sur le disque
    File fichier = new File(pNomDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    Trace.info("Récupérer le contenu du dossier : " + fichier.getAbsolutePath());
    if ((!fichier.exists()) && (fichier.isFile())) {
      Trace.info("Dossier non trouve ou vide : " + fichier.getPath());
    }
    else {
      retour = pNomDossier;
      String[] listefichier = fichier.list();
      if (listefichier != null) {
        Trace.info(String.format("%d fichiers trouvés.", listefichier.length));
        for (int i = 0; i < listefichier.length; i++) {
          retour += Constantes.SEPARATEUR_CHAINE + listefichier[i].replace(File.separatorChar, Constantes.SEPARATEUR_DOSSIER_CHAR);
        }
      }
    }
    return retour;
  }
  
  /**
   * Retourne le fichier demandé.
   */
  @Override
  public FichierRuntime demanderFichier(IdSession pIdSession, String pNomFichier) throws RemoteException {
    FichierRuntime gfb = new FichierRuntime();
    gfb.setNomFichier(pNomFichier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    gfb.setNomFichierRuntime(pNomFichier);
    gfb.lectureFichier();
    return gfb;
  }
  
  /**
   * Télécharge un fichier jar des runtimes donné.
   */
  @Override
  public FichierRuntime telechargerFichierRuntime(IdSession pIdSession, String pNomFichierRuntime) throws RemoteException {
    Trace.info("Récupérer le fichier runtime : " + pNomFichierRuntime);
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.chargerFichierRuntime(pIdSession.getNumero(), pNomFichierRuntime);
  }
  
  /**
   * Envoi une requête système.
   */
  @Override
  public String effectuerRequeteSysteme(IdSession pIdSession, String pChaine) throws RemoteException {
    Trace.info("Effectuer une requête systeme : " + pChaine);
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.effectuerRequeteSysteme(pIdSession.getNumero(), pChaine);
  }
  
  /**
   * Copie un fichier SAVF en 5250.
   */
  @Override
  public void copierFichierSavf(IdSession pIdSession, String pFichierSavf, Bibliotheque pBibliotheque) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ObjetSavfAS400.copierStreamSavfVers5250(sessionServeur.getSystemManager().getSystem(), pFichierSavf, pBibliotheque);
  }
  
  /**
   * Restaure un fichier SAVF.
   */
  @Override
  public void restaurerFichierSavf(IdSession pIdSession, Bibliotheque pBibliotheque, String pNomFichierSavf) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ObjetSavfAS400.restaurer(sessionServeur.getSystemManager().getSystem(), pBibliotheque, pNomFichierSavf);
  }
  
  // ==========================================================================
  // == Méthodes concernant la gestion des spools
  // ==========================================================================
  
  /**
   * Retourne les informations du pramètre NT d'une FM donnée.
   */
  @Override
  public ParametreNT retournerInformationsParamNT(IdSession pIdSession, String pCurlib) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlExploitationParametres operation = new SqlExploitationParametres(querym);
    return operation.lireParametreNT();
  }
  
  /**
   * Exporte un spool au format PDF.
   */
  @Override
  public String exporterSpool(IdSession pIdSession, DemandeEdition pDemandeEdition) throws RemoteException {
    // Sauver la demande d'édition en table
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlDemandeEdition sqlDemandeEdition = new SqlDemandeEdition(queryManager);
    sqlDemandeEdition.sauverDemandeEdition(pDemandeEdition, true);
    
    // Construction de la demande sous forme de texte pour la socket à partir de la demande d'édition
    String messageDemande = pDemandeEdition.construireDemandePourSocket();
    
    // Lancement de la requête
    Integer portNewsim = ManagerParametreModule.retournerValeurEnInteger(EnumParametreModule.NEWSIM_PORT);
    SessionEdition sedition = new SessionEdition("127.0.0.1", portNewsim.intValue());
    if (sedition.setSession() == Constantes.ERREUR || !sedition.attendreConnexion()) {
      sedition.closeConnexion();
      throw new MessageErreurException("Il a été impossible de se connecter au serveur SerieNNewSim.");
    }
    if (sedition.EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE, messageDemande) == Constantes.FALSE) {
      sedition.closeConnexion();
      throw new MessageErreurException("Problème lors de l'envoi de la requête vers le serveur d'édition");
    }
    sedition.attendreReponse();
    String chemin = sedition.getMessageReceptionne();
    Trace.info("Chemin de stockage du fichier " + pDemandeEdition.getTypeDocument().getExtension() + " : " + chemin);
    
    // Fermeture de la connexion avec le serveur NewSim
    if (sedition.EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE_DECONNEXION, null) == Constantes.FALSE) {
      sedition.closeConnexion();
    }
    return chemin;
  }
  
  /**
   * Charge une liste de Spool à partir d'une liste d'IdSpool.
   */
  @Override
  public ListeSpool chargerListeSpool(IdSession pIdSession, List<IdSpool> pListeIdSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerListeSpool(pListeIdSpool);
  }
  
  /**
   * Charge une liste d'IdSpool à partir de critères de recherche.
   */
  @Override
  public List<IdSpool> chargerListeIdSpool(IdSession pIdSession, CritereSpool pCritereSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerListeIdSpool(pCritereSpool);
  }
  
  /**
   * Charge une liste d'IdImprimante à partir de critères de recherche.
   */
  @Override
  public List<IdImprimante> chargerListeIdImprimante(IdSession pIdSession, CritereImprimante pCritereImprimante) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionImprimanteAS400 operation = new GestionImprimanteAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerListeIdImprimante(pCritereImprimante);
  }
  
  /**
   * Charge une liste d'imprimantes à partir d'une liste d'IdImprimante.
   */
  @Override
  public ListeImprimante chargerListeImprimante(IdSession pIdSession, List<IdImprimante> pListeIdImprimante) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionImprimanteAS400 operation = new GestionImprimanteAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerListeImprimante(pListeIdImprimante);
  }
  
  /**
   * Suspend un spool donné.
   */
  @Override
  public boolean suspendreSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.suspendreSpool(pIdSpool);
  }
  
  /**
   * Libère un spool donné.
   */
  @Override
  public boolean libererSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.libererSpool(pIdSpool);
  }
  
  @Override
  public boolean imprimerSpool(IdSession pIdSession, IdSpool pIdSpool, String pNomOutq, int pNombreExemplaire) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.imprimerSpool(pIdSpool, pNomOutq, pNombreExemplaire);
  }
  
  /**
   * Supprime un spool donné.
   */
  @Override
  public boolean supprimerSpool(IdSession pIdSession, IdSpool pIdSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.supprimerSpool(pIdSpool);
  }
  
  /**
   * Charge le contenu d'un spool donné.
   */
  @Override
  public Spool chargerContenuSpool(IdSession pIdSession, Spool pSpool) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionSpoolAS400 operation = new GestionSpoolAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerContenuSpool(pSpool);
  }
  
  /**
   * Redémarre une imprimante (outq, editeur, device).
   */
  @Override
  public boolean demarrerImprimante(IdSession pIdSession, Imprimante pImprimante) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgOperationsImprimantes operation = new RpgOperationsImprimantes();
    return operation.demarrerImprimante(sessionServeur.getSystemManager(), pImprimante);
  }
  
  // ==========================================================================
  // == Méthodes concernant la gestion des objets AS400
  // ==========================================================================
  
  /**
   * Retourne une liste de bibliothèques depuis le système de fichier.
   */
  @Override
  public List<Bibliotheque> chargerListeBibliothequeOS400(IdSession pIdSession, CritereBibliotheque pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return BibliothequeAS400.chargerListeBibliotheque(sessionServeur.getSystemManager().getSystem(), pCriteres);
  }
  
  /**
   * Retourne une liste de base de données depuis le système de fichier.
   */
  @Override
  public ListeBDD chargerListeBaseDeDonneesOS400(IdSession pIdSession, CritereBibliotheque pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return BibliothequeAS400.chargerListeBaseDeDonnees(sessionServeur.getSystemManager().getSystem(), pCriteres);
  }
  
  /**
   * Retourne la description d'une bibliothèque.
   */
  @Override
  public String chargerDescriptionBibliotheque(IdSession pIdSession, IdBibliotheque pIdBibliotheque) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return BibliothequeAS400.chargerDescription(sessionServeur.getSystemManager().getSystem(), pIdBibliotheque);
  }
  
  /**
   * Retourne une liste de base de données depuis la table PSEMFMVM.
   */
  @Override
  public ListeBDD chargerListeBaseDeDonnees(IdSession pIdSession, List<IdBibliotheque> pListeIdBibliotheque) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlBaseDeDonnees operation = new SqlBaseDeDonnees(queryManager);
    return operation.chargerListeBaseDeDonnees(pListeIdBibliotheque);
  }
  
  /**
   * Retourne le contenu d'une dataarea.
   */
  @Override
  public String chargerDataarea(IdSession pIdSession, Dataarea pDataarea) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    GestionDataareaAS400 operation = new GestionDataareaAS400(sessionServeur.getSystemManager().getSystem());
    return operation.chargerDataarea(pDataarea);
  }
  
  // ==========================================================================
  // == Méthodes concernant la gestion des flux
  // ==========================================================================
  
  /**
   * Retourner la liste des identifiants des flux selon les critères de recherche passés en paramètre.
   */
  @Override
  public List<IdFlux> chargerListeIdFlux(IdSession pIdSession, CritereFlux pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    return sqlFlux.chargerListeIdFlux(pCritere);
  }
  
  /**
   * Retourner la liste des flux correspondant à la liste d'identifiants de flux passée en paramètre.
   */
  @Override
  public ListeFlux chargerListeFlux(IdSession pIdSession, List<IdFlux> pListeIdFlux) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    return sqlFlux.chargerListeFlux(pListeIdFlux);
  }
  
  /**
   * Retourner la version des flux par défaut pour la base.
   */
  @Override
  public String getVersionDefautFlux(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    return sqlFlux.getVersionDefautFlux();
  }
  
  /**
   * Sauver un flux dans la base.
   */
  @Override
  public void sauverFlux(IdSession pIdSession, Flux pFlux) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    sqlFlux.sauverFlux(pFlux);
  }
  
  /**
   * Rejouer la liste de flux.
   */
  @Override
  public void rejouerFlux(IdSession pIdSession, ListeFlux pListeFlux) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFlux sqlFlux = new SqlFlux(queryManager);
    sqlFlux.rejouerFlux(pListeFlux);
  }
  
  /**
   * Contrôler l'existence de la table des flux.
   */
  @Override
  public void controlerPresenceTableFlux(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    if (!queryManager.isTableExiste(EnumTableBDD.FLUX)) {
      throw new MessageErreurException(
          "La table de l’historique des flux n’est pas présente car le gestionnaire de flux n’est pas paramétré pour cette base de données");
    }
  }
  
  // ==========================================================================
  // == Méthodes concernant la gestion des mails
  // ==========================================================================
  
  /**
   * Créer un mail.
   */
  @Override
  public IdMail creerMail(IdSession pIdSession, Mail pMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.creerMail(pMail);
  }
  
  /**
   * Charger un mail.
   */
  @Override
  public Mail chargerMail(IdSession pIdSession, IdMail pIdMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    List<IdMail> listeIdMail = new ArrayList<IdMail>();
    listeIdMail.add(pIdMail);
    List<Mail> listeMail = operation.chargerListeMail(listeIdMail);
    if (listeMail.isEmpty()) {
      return null;
    }
    return listeMail.get(0);
  }
  
  /**
   * Charge une liste de mail via une liste d'IdMail
   */
  @Override
  public ListeMail chargerListeMail(IdSession pIdSession, List<IdMail> pListeId) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerListeMail(pListeId);
  }
  
  /**
   * Charge une lsite de mail via une liste d'IdMail
   */
  @Override
  public List<IdMail> chargerListeIdMail(IdSession pIdSession, CritereMail pCritereMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerListeIdMail(pCritereMail);
  }
  
  /**
   * Charger une liste de type de mail.
   */
  @Override
  public ListeTypeMail chargerListeTypeMail(IdSession pIdSession, List<IdTypeMail> pListeIdTypeMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerListeTypeMail(pListeIdTypeMail);
  }
  
  /**
   * Charger une liste de type de mail.
   */
  @Override
  public ListeTypeMail chargerListeTypeMail(IdSession pIdSession, CritereTypeMail pCritereTypeMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerTypeMail(pCritereTypeMail);
  }
  
  /**
   * Sauve un type de mail.
   */
  @Override
  public void sauverTypeMail(IdSession pIdSession, TypeMail pTypeMail, boolean pJusteLeCorps) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    operation.modifierTypeMail(pTypeMail, pJusteLeCorps);
  }
  
  /**
   * Crée des types de mail à partir d'une liste.
   */
  @Override
  public void creerListeTypeMail(IdSession pIdSession, List<TypeMail> pListeTypeMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    operation.creerListeTypeMail(pListeTypeMail);
  }
  
  /**
   * Charge la liste des variables pour un mail donné.
   */
  @Override
  public ListeVariableMail chargerListeVariableMail(IdSession pIdSession, IdMail pIdMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerListeVariableMail(pIdMail);
  }
  
  /**
   * Sauver la liste des variables pour un mail donné.
   */
  @Override
  public void creerListeVariableMail(IdSession pIdSession, ListeVariableMail pListeVariableMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    operation.creerListeVariableMail(pListeVariableMail);
  }
  
  /**
   * Charge une liste de mail
   */
  @Override
  public ListeMail chargerListeMail(IdSession pIdSession, CritereMail pCritereMail) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlMail operation = new SqlMail(queryManager);
    return operation.chargerListeMail(pCritereMail);
  }
  
  // ==========================================================================
  // == Méthodes concernant les documents liés
  // ==========================================================================
  
  /**
   * Lire une liste d'identifiants de dossiers de documents liés.
   */
  @Override
  public List<IdDossierDocumentLie> chargerListeIdDossierDocumentLieClient(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlDocumentLie operation = new SqlDocumentLie(queryManager);
    return operation.chargerListeIdDossierDocumentLieClient();
  }
  
  /**
   * Retourne la liste des fichiers contenus dans un dossier.
   */
  @Override
  public String[] chargerListeDocumentLie(IdSession pIdSession, String pDossierDocumentLie) throws RemoteException {
    // On vérifie l'existence du fichier sur le disque
    File fichier = new File(pDossierDocumentLie.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    
    Trace.info("Récupérer le contenu du dossier : " + fichier.getAbsolutePath());
    if ((!fichier.exists()) && (fichier.isFile())) {
      Trace.info("Dossier non trouve ou vide : " + fichier.getPath());
      return null;
    }
    else {
      String[] nomFichiers = fichier.list();
      return nomFichiers;
    }
  }
  
  /**
   * Retourne le nombre de fichiers contenus dans un dossier.
   */
  @Override
  public int compterDocumentLie(IdSession pIdSession, String pDossierDocumentLie) throws RemoteException {
    // On vérifie l'existence du fichier sur le disque
    File fichier = new File(pDossierDocumentLie.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    
    if (!fichier.exists() && fichier.isFile()) {
      return 0;
    }
    else {
      if (fichier.list() == null) {
        return 0;
      }
      return fichier.list().length;
    }
  }
  
  /**
   * Ajouter un document lié sur le serveur (AS400) un fichier donné.
   */
  @Override
  public void ajouterDocumentLie(IdSession pIdSession, String pCheminDossier, GestionFichierByte pCheminDocument) throws RemoteException {
    final File dossier = new File(pCheminDossier.replace(Constantes.SEPARATEUR_DOSSIER_CHAR, File.separatorChar));
    
    // On s'assure que le dossier existe dans le cas contraire on le créé
    if (!dossier.getParentFile().exists()) {
      dossier.getParentFile().mkdirs();
    }
    
    // Récupération du fichier
    if (pCheminDocument == null || pCheminDocument.getContenuFichier() == null) {
      throw new MessageErreurException("Le fichier choisi est corrompu.");
    }
    
    // On enregistre le fichier
    pCheminDocument.changeNomFichier(dossier.getAbsolutePath());
    if (pCheminDocument.ecritureFichier() == Constantes.OK) {
      Trace.info("Le fichier a été uploadé et enregistré : " + dossier.getName());
    }
    else {
      throw new MessageErreurException("Le fichier n'a pas pu être ajouté au dossier en cours");
    }
  }
  
  // ==========================================================================
  // == Méthodes concernant les paramètres des modules
  // ==========================================================================
  
  /**
   * Charge la liste des paramètres des modules suivant les critères.
   */
  @Override
  public ListeParametreModule chargerListeParametre(IdSession pIdSession, CritereParametreModule pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlModule operation = new SqlModule(queryManager);
    return operation.chargerListeParametre(pCritere);
  }
  
  // ==========================================================================
  // == Méthodes concernant les licences
  // ==========================================================================
  
  @Override
  public List<IdLicence> chargerListeIdLicence(IdSession pIdSession, CritereLicence pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlLicence operation = new SqlLicence(queryManager);
    return operation.chargerListeIdLicence(pCritere);
  }
  
  @Override
  public ListeLicence chargerListeLicence(IdSession pIdSession, List<IdLicence> pListeIdLicence) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlLicence operation = new SqlLicence(queryManager);
    return operation.chargerListeLicence(pListeIdLicence);
  }
  
  @Override
  public IdLicence sauverLicence(IdSession pIdSession, Licence pLicence) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlLicence operation = new SqlLicence(queryManager);
    return operation.sauverLicence(pLicence);
  }
  
  @Override
  public Licence chargerLicence(IdSession pIdSession, IdLicence pIdLicence) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlLicence operation = new SqlLicence(queryManager);
    return operation.chargerLicence(pIdLicence);
  }
  
  @Override
  public void changerStatutLicence(IdSession pIdSession, IdLicence pIdLicence, EnumStatutLicence pStatutLicence) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlLicence operation = new SqlLicence(queryManager);
    operation.changerStatutLicence(pIdLicence, pStatutLicence);
  }
  
  @Override
  public void controlerLicence(EnumTypeLicence pTypeLicence, CodeTypeLicence pCodeTypeLicence) throws RemoteException {
    if (pTypeLicence == null) {
      throw new MessageErreurException("Le type de la licence est invalide.");
    }
    if (pCodeTypeLicence == null) {
      throw new MessageErreurException("Le code du type de la licence est invalide.");
    }
    
    // Contrôle le licence en fonction de son type et de son code
    switch (pTypeLicence) {
      case MODULE:
        if (pCodeTypeLicence.getCode().equals(CodeTypeLicence.GESCOM)) {
          ManagerLicence.controlerLicenceGestionCommerciale();
        }
        if (pCodeTypeLicence.getCode().equals(CodeTypeLicence.COMPTA)) {
          ManagerLicence.controlerLicenceComptabilite();
        }
        break;
      
      default:
        break;
    }
  }
  
  @Override
  public List<String> retournerListeDescriptionLicenceFinValiditeProche(int pNombreJourRestants) throws RemoteException {
    return ManagerLicence.retournerListeDescriptionLicenceFinValiditeProche(pNombreJourRestants);
  }
  
  @Override
  public void rafraichirListeLicenceActiveServeur(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    try {
      // Recherche les licences actives pour l'environnement
      ManagerLicence.chargerListeLicenceActive(queryManager);
      ListeLicence listeLicence = ManagerLicence.getListeLicenceActive();
      if (listeLicence == null || listeLicence.isEmpty()) {
        Trace.alerte("Aucune licence active n'a été trouvée");
      }
    }
    catch (Exception e) {
    }
  }
  
  // ==========================================================================
  // == Méthodes concernant les informations systèmes
  // ==========================================================================
  
  @Override
  public IdServeurPower retournerIdServeurPower() throws RemoteException {
    return ManagerAS400.getIdServeurPower();
  }
  
  // ==========================================================================
  // == Méthodes concernant les notifications
  // ==========================================================================
  
  @Override
  public List<IdNotification> chargerListeIdNotification(IdSession pIdSession, CritereNotification pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlNotification operation = new SqlNotification(sessionServeur.getSystemManager().getSystem(), queryManager);
    return operation.chargerListeIdNotification(pCriteres);
  }
  
  @Override
  public ListeNotification chargerListeNotification(IdSession pIdSession, List<IdNotification> pListeIdNotification)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlNotification operation = new SqlNotification(sessionServeur.getSystemManager().getSystem(), queryManager);
    return operation.chargerListeNotification(pListeIdNotification);
  }
  
  @Override
  public IdNotification sauverNotification(IdSession pIdSession, Notification pNotification) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlNotification operation = new SqlNotification(sessionServeur.getSystemManager().getSystem(), queryManager);
    return operation.sauverNotification(pNotification);
  }
  
  @Override
  public Notification chargerNotification(IdSession pIdSession, IdNotification pIdNotification) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlNotification operation = new SqlNotification(sessionServeur.getSystemManager().getSystem(), queryManager);
    return operation.chargerNotification(pIdNotification);
  }
  
  // ==========================================================================
  // == Méthodes concernant les utilisateurs
  // ==========================================================================
  
  /**
   * Retourne la liste des profils SERIE N de l'environnement en cours
   */
  @Override
  public ListeUtilisateur chargerListeUtilisateur(IdSession pIdSession, List<IdUtilisateur> pListeIdUtilisateur) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlUtilisateur operation = new SqlUtilisateur(queryManager);
    return operation.chargerListeUtilisateur(pListeIdUtilisateur);
  }
  
  /**
   * Retourne la liste des IdUtilisateur des profils de l'environnement en cours à partir de critères de recherche.
   */
  @Override
  public List<IdUtilisateur> chargerListeIdUtilisateur(IdSession pIdSession, CritereUtilisateur pCritereUtilisateur)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlUtilisateur operation = new SqlUtilisateur(queryManager);
    return operation.chargerListeIdUtilisateur(pCritereUtilisateur);
  }
}
