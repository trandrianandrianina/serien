/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.rmi.server.RemoteServer;

import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.thread.OutilThread;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Intercepte les appels de méthodes RMI du côté serveur.
 * La methode invoke de cette classe est appelée avant chaque appel à une méthode du service RMI.
 */
public class InvocationHandlerServeurRMI implements InvocationHandler {
  // Constantes
  private static final String PREFIXE_RMI = "[RMI] ";
  
  // Variables
  private Object object;
  private long totalMemoryDebut = 0;
  private long freeMemoryDebut = 0;
  private long usedMemoryDebut = 0;
  
  /**
   * Constructeur.
   */
  public InvocationHandlerServeurRMI(Object pObject) {
    object = pObject;
  }
  
  /**
   * La méthode trace le nom de la méthode RMI appelée et mesure le temps d'exécution côte serveur.
   * Le nom de la thread est changé suivant le format "RMI(<IdThread>)-<AdresseIp>-<Profil>-<IdSession>".
   */
  @Override
  public Object invoke(Object pProxy, Method pMethod, Object[] pArgs) throws Throwable {
    final String classe = pMethod.getDeclaringClass().getSimpleName();
    final String methode = pMethod.getName();
    
    Object objet = null;
    try {
      // Mesurer l'usage mémoire au début
      totalMemoryDebut = Runtime.getRuntime().totalMemory() / 1024;
      freeMemoryDebut = Runtime.getRuntime().freeMemory() / 1024;
      usedMemoryDebut = totalMemoryDebut - freeMemoryDebut;
      
      // Récupérer l'identifiant de la session dans le premier argument
      IdSession idSession = null;
      if (pArgs != null && pArgs.length != 0 && pArgs[0] instanceof IdSession) {
        idSession = (IdSession) pArgs[0];
      }
      
      // Tracer le début de l'appel
      entrer(classe, methode, idSession);
      
      // Vérifier si l'identifiant de session correspond à une session valide.
      // Le côte multithreads du client peut amener à utiliser une session qui a été fermée par ailleurs. On ne peut pas ignorer
      // silencieusement cette situation car cela va planter côté client si on ne renvoit pas les données attendues. Le mieux est de
      // générer un message "Passe partout" qui confirme que la session est bien fermée. Pour éviter ce message, il faut protéger le
      // client afin de ne pas pouvoir fermer la session lorsque des données sont en cours de chargement.
      if (idSession != null && idSession.getNumero() != 0 && ManagerClientDesktop.getSession(idSession) == null) {
        throw new MessageErreurException("La session est fermée.");
      }
      
      // Appeler la méthode RMI prévue
      long debut = System.currentTimeMillis();
      objet = pMethod.invoke(object, pArgs);
      long duree = System.currentTimeMillis() - debut;
      
      // Mesurer l'usage mémoire à la fin
      long totalMemoryFin = Runtime.getRuntime().totalMemory() / 1024;
      long freeMemoryFin = Runtime.getRuntime().freeMemory() / 1024;
      long usedMemoryFin = totalMemoryFin - freeMemoryFin;
      long totalMemoryDifference = totalMemoryFin - totalMemoryDebut;
      long usedMemoryDifference = usedMemoryFin - usedMemoryDebut;
      
      // Tracer les threads
      OutilThread.tracerNombreThread();
      OutilThread.traceThreadDifference();
      
      // Tracer le retour avec succès
      Trace.soustitre("SUCCES " + classe + "." + methode + " (" + usedMemoryFin + "/" + totalMemoryFin + " ko;" + usedMemoryDifference
          + "/" + totalMemoryDifference + " ko;" + duree + " ms)");
    }
    catch (InvocationTargetException e) {
      throw sortirAvecException(classe, methode, e);
    }
    catch (Exception e) {
      throw sortirAvecException(classe, methode, e);
    }
    return objet;
  }
  
  /**
   * Effectuer toutes les tâches génériques liées à l'entrée dans un service RMI.
   */
  private static void entrer(String classe, String methode, IdSession pIdSession) {
    // Récupérer l'identifant de la thread courante
    long idThread = Thread.currentThread().getId();
    
    // Récupérer l'adresse IP du client RMI
    String adresseIP = "";
    try {
      adresseIP = RemoteServer.getClientHost();
    }
    catch (Exception e) {
    }
    
    // Récupérer le profil associé à la session
    String profil = null;
    if (pIdSession != null && pIdSession.getNumero() != 0) {
      // Récupérer les informations de l'utilisateur associé la session
      SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
      if (sessionServeur != null && sessionServeur.getEnvUser() != null) {
        profil = sessionServeur.getEnvUser().getProfil();
      }
    }
    
    // Formater le nom de la thread
    String nomThread = "";
    if (profil != null && pIdSession != null) {
      nomThread = String.format("RMI(%03d)-%-15s-%-10s-%d", idThread, adresseIP, profil, pIdSession.getNumero());
    }
    else if (profil != null) {
      nomThread = String.format("RMI(%03d)-%-15s-%-10s", idThread, adresseIP, profil);
    }
    else if (pIdSession != null && pIdSession.getNumero() != 0) {
      nomThread = String.format("RMI(%03d)-%-15s-%-10s-%d", idThread, adresseIP, "", pIdSession.getNumero());
    }
    else {
      nomThread = String.format("RMI(%03d)-%-15s", idThread, adresseIP);
    }
    
    // Ajouter des informations dans le nom de la thread
    Thread.currentThread().setName(nomThread);
    
    // Tracer l'entrée dans le service
    Trace.soustitre("ENTREE " + classe + "." + methode);
  }
  
  /**
   * Effectuer toutes les tâches génériques liées à la sortie avec exception d'un service RMI.
   * <br>
   * L'idée directrice est que toutes les exceptions remontées par le serveur doivent être des MessageErreurException. Ainsi, côte client,
   * on peut faire la différence entre des erreurs dans notre code et des erreurs liées à la couche RMI. Si on reçoit une exception autre
   * autre que MessageUtilisateurException, c'est une erreur technique qui n'a pas été captée. On génère alors un
   * MessageUtilisateurException avec le message générique "Une erreur technique est survenue. Merci de contacter le service assistance".
   */
  private Exception sortirAvecException(String classe, String methode, Exception e) {
    // Contrôler que l'exception n'est pas nulle
    if (e == null) {
      Trace.soustitre("ECHEC " + classe + "." + methode + " : Sortie fatale du service sans exception.");
      return new MessageErreurException(MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
    }
    
    // Rechercher la présence d'une MessageUtilisateurException dans la pile des exceptions.
    // Dans le cas classique, la MessageUtilisateurException est encapculée dans une InvocationTargetException.
    for (Throwable cause = e; cause != null; cause = cause.getCause()) {
      if (cause instanceof MessageErreurException) {
        Trace.soustitre("ECHEC " + classe + "." + methode);
        return e;
      }
    }
    
    // C'est une exception imprévue, on génère une MessageUtilisateurException avec le message "Erreur technique"
    Trace.soustitre("ECHEC " + classe + "." + methode + " : Exception = " + e.getMessage());
    return new MessageErreurException(e, MessageErreurException.MESSAGE_ERREUR_TECHNIQUE);
  }
}
