/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;

import ri.serien.libas400.dao.gvx.programs.representant.GM_Representant;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.gescom.commun.representant.CriteresRechercheRepresentant;
import ri.serien.libcommun.gescom.commun.representant.ListeRepresentant;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceRepresentant;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service représentant.
 */
public class ServiceRepresentant implements InterfaceServiceRepresentant {
  private static InterfaceServiceRepresentant service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceRepresentant() {
  }
  
  /**
   * Retourner l'implémentation du service representant.
   */
  protected static InterfaceServiceRepresentant getService() {
    if (service == null) {
      InterfaceServiceRepresentant interfaceService = new ServiceRepresentant();
      service = (InterfaceServiceRepresentant) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceRepresentant.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Retourne la liste des représentants suiavnt les critères donnés.
   */
  @Override
  public ListeRepresentant chargerListeRepresentant(IdSession pIdSession, CriteresRechercheRepresentant pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    GM_Representant gmrep = new GM_Representant(queryManager);
    return gmrep.chargerListeRepresentant(pCriteres);
  }
  
}
