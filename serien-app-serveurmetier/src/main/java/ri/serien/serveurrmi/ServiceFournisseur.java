/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import ri.serien.libas400.dao.sql.gescom.fournisseur.SqlFournisseur;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.dataarea.GestionDataareaAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.dataarea.Dataarea;
import ri.serien.libcommun.gescom.achat.catalogue.ChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.CriteresRechercheCfgCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.IdChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.IdConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeChampERP;
import ri.serien.libcommun.gescom.achat.catalogue.ListeConfigurationCatalogue;
import ri.serien.libcommun.gescom.achat.catalogue.ListeFichierCSV;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.AdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.CritereFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.FiltreFournisseurBase;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeAdresseFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseur;
import ri.serien.libcommun.gescom.commun.fournisseur.ListeFournisseurBase;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceFournisseur;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

public class ServiceFournisseur implements InterfaceServiceFournisseur {
  private static InterfaceServiceFournisseur service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceFournisseur() {
  }
  
  /**
   * Retourner l'implémentation du service fournisseur.
   */
  protected static InterfaceServiceFournisseur getService() {
    if (service == null) {
      InterfaceServiceFournisseur interfaceService = new ServiceFournisseur();
      service = (InterfaceServiceFournisseur) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceFournisseur.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Lit les données d'un fournisseur à partir d'une instanciation fournisseur
   */
  @Override
  public Fournisseur chargerFournisseur(IdSession pIdSession, Fournisseur pFournisseur) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.chargerFournisseur(pFournisseur);
  }
  
  @Override
  public ListeFournisseurBase chargerListeFournisseurBase(IdSession pIdSession, List<IdFournisseur> pListeId) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.chargerListeFournisseurBase(pListeId);
  }
  
  /**
   * Lit les données du fournisseur demandé à partir de son numéro
   */
  @Override
  public Fournisseur chargerFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.chargerFournisseur(pIdFournisseur);
  }
  
  /**
   * Lit la liste complète des fournisseurs d'un établissement Série N
   */
  @Override
  public ListeFournisseur chargerListeFournisseurEtablissement(IdSession pIdSession, IdEtablissement pIdEtablissement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur sqlFournisseur = new SqlFournisseur(sessionServeur.getQueryManager());
    return sqlFournisseur.chargerListeFournisseurEtablissement(pIdEtablissement);
  }
  
  /**
   * Rechercher une liste d'IdFournisseur suivant les critères voulus
   */
  @Override
  public List<IdFournisseur> chargerListeIdFournisseur(IdSession pIdSession, CritereFournisseur pCritereFournisseur)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur sql = new SqlFournisseur(sessionServeur.getQueryManager());
    return sql.chargerListeIdFournisseur(pCritereFournisseur);
  }
  
  /**
   * Retourne une liste d'adresses fournisseurs renseignées à partir d'une liste d'id adresse fournisseur.
   */
  @Override
  public ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession,
      List<IdAdresseFournisseur> pListeIdAdresseFournisseur) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur sqlFournisseur = new SqlFournisseur(sessionServeur.getQueryManager());
    return sqlFournisseur.chargerListeAdresseFournisseur(pListeIdAdresseFournisseur);
  }
  
  @Override
  public AdresseFournisseur chargerAdresseFournisseur(IdSession pIdSession, IdAdresseFournisseur pIdAdresseFournisseur)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur sqlFournisseur = new SqlFournisseur(sessionServeur.getQueryManager());
    return sqlFournisseur.chargerAdresseFournisseur(pIdAdresseFournisseur);
  }
  
  /**
   * Retourne une liste d'adresses fournisseurs renseignées à partir d'une liste de fournisseurs.
   */
  @Override
  public ListeAdresseFournisseur chargerListeAdresseFournisseur(IdSession pIdSession, IdEtablissement pIdEtablissement) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlFournisseur sqlFournisseur = new SqlFournisseur(sessionServeur.getQueryManager());
    return sqlFournisseur.chargerListeAdresseFournisseurEtablissement(pIdEtablissement);
  }
  
  /**
   * Rechercher une liste d'adresse fournisseurs suivant les critères voulus
   */
  @Override
  public ArrayList<IdAdresseFournisseur> chargerListeIdAdresseFournisseur(IdSession pIdSession, CritereFournisseur pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.chargerListeIdAdresseFournisseur(pCriteres);
  }
  
  /**
   * Rechercher une liste de fournisseurs suivant les critères voulus
   */
  @Override
  public ListeFournisseur chargerListeFournisseur(IdSession pIdSession, CritereFournisseur pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.chargerListeFournisseur(pCriteres);
  }
  
  /**
   * Charger la liste des adresses d'un fournisseur.
   */
  @Override
  public List<AdresseFournisseur> chargerListeAdresseFournisseur(IdSession pIdSession, IdFournisseur pIdFournisseur,
      boolean pInclureSiege) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(sessionServeur.getQueryManager());
    return operation.lireListeAdresseFournisseur(pIdFournisseur, pInclureSiege);
  }
  
  /**
   * Rechercher une liste de configurations de catalogue suivant les critères voulus
   */
  @Override
  public ListeConfigurationCatalogue chargerListeConfigsCatalogue(IdSession pIdSession, CriteresRechercheCfgCatalogue pCriteres) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    return operation.chargerListeConfigsCatalogue(pCriteres);
  }
  
  /**
   * Rechercher une configuration de catalogue en fonction d'un Id de configuration transmis en paramètre
   */
  @Override
  public ConfigurationCatalogue chargerConfigurationCatalogueParId(IdSession pIdSession, IdConfigurationCatalogue pIdConfiguration) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    
    // Charger la configuration catalogue
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    ConfigurationCatalogue configurationCatalogue = operation.chargerConfigurationCatalogueParId(pIdSession, pIdConfiguration);
    
    // Charger la liste des champs
    ListeChampCatalogue listeChampCatalogue = ListeChampCatalogue.chargerTout(pIdSession, configurationCatalogue.getFichierCSV());
    configurationCatalogue.setListeChampCatalogue(listeChampCatalogue);
    
    return configurationCatalogue;
  }
  
  /**
   * Retourner la liste des fichiers CSV disponibles pour des imports catalogues fournisseurs.
   * Le dossier qui contient ses fichiers se trouve dans la DTAARA DTAFCATA.
   */
  @Override
  public ListeFichierCSV chargerListeFichiersCataloguesCSV(IdSession pIdSession) {
    String cheminRacine = null;
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    String nomBibliotheque = queryManager.getLibrary();
    Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(nomBibliotheque), EnumTypeBibliotheque.NON_DEFINI);
    Dataarea dataarea = new Dataarea();
    dataarea.setBibliotheque(bibliotheque);
    dataarea.setNom("DTAFCATA");
    GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(sessionServeur.getSystemManager().getSystem());
    cheminRacine = gestionDataareaAS400.chargerDataarea(dataarea);
    if (cheminRacine == null) {
      return null;
    }
    
    ListeFichierCSV listeFichierCSV = new ListeFichierCSV();
    File dossier = new File(cheminRacine.trim());
    if (dossier.isDirectory()) {
      File[] fichiers = dossier.listFiles();
      if (fichiers != null) {
        for (File fichier : fichiers) {
          if (fichier.isFile()) {
            if (fichier.getName().endsWith(".csv") || fichier.getName().endsWith(".CSV")) {
              listeFichierCSV.add(fichier.getName());
            }
          }
        }
      }
    }
    else {
      return null;
    }
    
    return listeFichierCSV;
  }
  
  /**
   * Retourner les différents champs d'un catalogue fournisseur CSV.
   * Les noms de champs sont automatiquement déclarés sur la première ligne.
   */
  @Override
  public ListeChampCatalogue chargerListeChampUnCatalogue(IdSession pIdSession, String pNomFichier) {
    
    String cheminRacine = null;
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    String nomBibliotheque = queryManager.getLibrary();
    Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(nomBibliotheque), EnumTypeBibliotheque.NON_DEFINI);
    Dataarea dataarea = new Dataarea();
    dataarea.setBibliotheque(bibliotheque);
    dataarea.setNom("DTAFCATA");
    GestionDataareaAS400 gestionDataareaAS400 = new GestionDataareaAS400(sessionServeur.getSystemManager().getSystem());
    cheminRacine = gestionDataareaAS400.chargerDataarea(dataarea);
    if (cheminRacine == null) {
      return null;
    }
    
    ListeChampCatalogue listeChampCatalogue = new ListeChampCatalogue();
    
    File fichierCSV = new File(cheminRacine.trim() + pNomFichier);
    Trace.info("Valeur fichierCSV : " + fichierCSV);
    if (fichierCSV.isFile()) {
      String[] tableau = retournerChampsFichierCSV(fichierCSV, 1);
      if (tableau == null) {
        return null;
      }
      for (int i = 0; i < tableau.length; i++) {
        ChampCatalogue champCatalogue = new ChampCatalogue();
        champCatalogue.setOrdre(i + 1);
        champCatalogue.setNom(tableau[i]);
        listeChampCatalogue.add(champCatalogue);
      }
      
      String[] tableauExemple = retournerChampsFichierCSV(fichierCSV, 2);
      
      if (listeChampCatalogue.size() == tableauExemple.length) {
        for (int i = 0; i < tableauExemple.length; i++) {
          listeChampCatalogue.get(i).setValeurExemple(tableauExemple[i]);
        }
      }
    }
    else {
      return null;
    }
    
    return listeChampCatalogue;
  }
  
  /**
   * Permet de retourner un tableau contenant les valeurs d'une ligne d'un fichier CSV.
   */
  public static String[] retournerChampsFichierCSV(File pFichier, int pLigne) {
    if (pFichier == null) {
      return null;
    }
    if (!pFichier.isFile()) {
      return null;
    }
    
    if (pLigne == 0) {
      return null;
    }
    
    String[] tableau = null;
    
    try {
      BufferedReader reader = null;
      reader = new BufferedReader(new FileReader(pFichier));
      String contenu = "";
      // Lecture du fichier
      String chaine = null;
      int nombreLignes = 0;
      while ((chaine = reader.readLine()) != null && nombreLignes < pLigne) {
        if (nombreLignes == pLigne - 1) {
          contenu = chaine;
        }
        nombreLignes++;
      }
      reader.close();
      
      if (!contenu.isEmpty()) {
        tableau = contenu.split(";");
      }
      
    }
    catch (Exception e) {
      return null;
    }
    
    return tableau;
  }
  
  /**
   * Permet de charger la liste complète des champs de l'ERP qui peuvent être mappés avec des champs catalogues fournisseurs.
   */
  @Override
  public ListeChampERP chargerListeChampERP(IdSession pIdSession) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    return operation.chargerListeChampERP();
  }
  
  /**
   * Permet de sauver une configuration de catalogue en base de données.
   */
  @Override
  public void sauverUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    operation.sauverUneConfigurationCatalogue(pConfigurationCatalogue);
  }
  
  /**
   * Permet de supprimer une configuration de catalogue en base de données.
   */
  @Override
  public void supprimerUneConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfigurationCatalogue)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    operation.supprimerUneConfigurationCatalogue(pConfigurationCatalogue);
  }
  
  /**
   * Permet de charger une liste de fournisseur de base à partir des filtres transmis en paramètres.
   */
  @Override
  public ListeFournisseurBase chargerListeFournisseurBaseParEtablissement(IdSession pIdSession, FiltreFournisseurBase pFiltres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    return operation.chargerListeFournisseurBaseParEtablissement(pFiltres);
  }
  
  /**
   * Permet de charger un champ ERP à partir de son Identifiant.
   */
  @Override
  public ChampERP chargerUnChampERPparId(IdSession pIdSession, IdChampERP pIdChampERP) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    return operation.chargerUnChampERPparId(pIdChampERP);
  }
  
  /**
   * Permet de sauver une association de champs ERP et catalogue au sein d'une configration de catalogue.
   */
  @Override
  public void sauverAssociationChampsConfigurationCatalogue(IdSession pIdSession, ConfigurationCatalogue pConfiguration,
      IdChampERP pIdChampERP) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlFournisseur operation = new SqlFournisseur(queryManager);
    operation.sauverAssociationChampsConfigurationCatalogue(pConfiguration, pIdChampERP);
  }
  
}
