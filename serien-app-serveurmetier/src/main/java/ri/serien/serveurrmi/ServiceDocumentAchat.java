/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.rpg.gescom.documentachat.controle.RpgControlerLigneDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.creation.RpgCreerDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.creation.RpgSauverLigneDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.edition.RpgEditerDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.initialisation.RpgInitialiserLigneDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.lecture.RpgChargerDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.modification.RpgModifierDocumentAchat;
import ri.serien.libas400.dao.rpg.gescom.documentachat.recherche.RpgChargerLignesDocument;
import ri.serien.libas400.dao.rpg.gescom.documentachat.recherche.RpgLigneAchatBase;
import ri.serien.libas400.dao.rpg.gescom.documentachat.validation.RpgValiderDocumentAchat;
import ri.serien.libas400.dao.sql.gescom.documentachat.SqlDocumentAchat;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.CritereDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.DocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumCodeExtractionDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.EnumOptionEdition;
import ri.serien.libcommun.gescom.achat.document.IdDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ListeReceptionAchat;
import ri.serien.libcommun.gescom.achat.document.OptionsEditionAchat;
import ri.serien.libcommun.gescom.achat.document.ValeurInitialeCommandeAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.CritereLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatArticle;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchatCommentaire;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeDocumentAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchat;
import ri.serien.libcommun.gescom.achat.document.ligneachat.ListeLigneAchatBase;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceDocumentAchat;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

public class ServiceDocumentAchat implements InterfaceServiceDocumentAchat {
  private static InterfaceServiceDocumentAchat service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceDocumentAchat() {
  }
  
  /**
   * Retourner l'implémentation du service.
   */
  protected static InterfaceServiceDocumentAchat getService() {
    if (service == null) {
      InterfaceServiceDocumentAchat interfaceService = new ServiceDocumentAchat();
      service = (InterfaceServiceDocumentAchat) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceDocumentAchat.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Retourne une liste d'id de documents pour un fournisseur correspondants aux critères de recherche.
   */
  @Override
  public List<IdDocumentAchat> chargerListeIdDocumentAchat(IdSession pIdSession, CritereDocumentAchat pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerListeIdDocumentAchat(pCriteres);
  }
  
  /**
   * Charger une liste de documents d'achat à partir d'une liste d'identifiants.
   */
  @Override
  public ListeDocumentAchat chargerListeDocumentAchat(IdSession pIdSession, List<IdDocumentAchat> pListeIdDocumentAchat)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerListeDocumentAchat(pListeIdDocumentAchat);
  }
  
  /**
   * Charger un document d'achats.
   */
  @Override
  public DocumentAchat chargerDocumentAchat(IdSession pIdSession, IdDocumentAchat pIdDocument, DocumentAchat pDocumentAchat,
      boolean avecHistorique) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    RpgChargerDocumentAchat operation = new RpgChargerDocumentAchat(querym);
    pDocumentAchat = operation.chargerDocumentAchat(sessionServeur.getSystemManager(), pIdDocument, pDocumentAchat, avecHistorique);
    if (pDocumentAchat == null) {
      throw new MessageErreurException("La lecture de l'entête du document est impossible");
    }
    return pDocumentAchat;
  }
  
  /**
   * Création d'un document.
   */
  @Override
  public DocumentAchat sauverDocumentAchat(IdSession pIdSession, String pUtilisateur, DocumentAchat pDocumentAchat)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    // On créer le document dans la base
    if (!pDocumentAchat.isExistant()) {
      RpgCreerDocumentAchat operation = new RpgCreerDocumentAchat();
      pDocumentAchat = operation.creerDocumentAchat(sessionServeur.getSystemManager(), pUtilisateur, pDocumentAchat);
      if (pDocumentAchat == null) {
        throw new MessageErreurException("La création du document a échoué");
      }
    }
    // On modifie le document dans la base
    else {
      RpgModifierDocumentAchat operation = new RpgModifierDocumentAchat();
      pDocumentAchat = operation.modifierDocumentAchat(sessionServeur.getSystemManager(), pUtilisateur, pDocumentAchat);
      if (pDocumentAchat == null) {
        throw new MessageErreurException("La création du document a échoué");
      }
    }
    return pDocumentAchat;
  }
  
  /**
   * Initialise les informations d'une ligne document.
   */
  @Override
  public LigneAchat initialiserLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgInitialiserLigneDocumentAchat operation = new RpgInitialiserLigneDocumentAchat();
    // Il s'agit d'une ligne article
    if (pLigneAchat instanceof LigneAchatArticle) {
      LigneAchatArticle ligneAchat = (LigneAchatArticle) pLigneAchat;
      ligneAchat = operation.initialiserLigneArticleDocument(sessionServeur.getSystemManager(), ligneAchat, pDateTraitement);
      pLigneAchat = ligneAchat;
    }
    if (pLigneAchat == null) {
      throw new MessageErreurException("L'initialisation de la ligne document a échoué");
    }
    return pLigneAchat;
  }
  
  /**
   * Contrôle les informations d'une ligne document.
   */
  @Override
  public LigneAchat controlerLigneDocument(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgControlerLigneDocumentAchat operation = new RpgControlerLigneDocumentAchat();
    // Il s'agit d'une ligne article
    if (pLigneAchat instanceof LigneAchatArticle) {
      LigneAchatArticle ligneAchat = (LigneAchatArticle) pLigneAchat;
      return operation.controlerLigneArticleDocument(sessionServeur.getSystemManager(), ligneAchat, pDateTraitement);
    }
    return pLigneAchat;
  }
  
  /**
   * Créer une ligne document.
   */
  @Override
  public void sauverLigneAchat(IdSession pIdSession, LigneAchat pLigneAchat, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverLigneDocumentAchat operation = new RpgSauverLigneDocumentAchat();
    // Il s'agit d'une ligne article
    if (pLigneAchat instanceof LigneAchatArticle) {
      LigneAchatArticle ligneAchat = (LigneAchatArticle) pLigneAchat;
      operation.sauverLigneAchat(sessionServeur.getSystemManager(), ligneAchat, pDateTraitement);
    }
    else if (pLigneAchat instanceof LigneAchatCommentaire) {
      operation.sauverLigneAchatCommentaire(sessionServeur.getSystemManager(), pLigneAchat, pDateTraitement);
    }
  }
  
  /**
   * Recherche toutes lignes articles d'un document à partir des critères donnés.
   */
  @Override
  public ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, CritereLigneAchat pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerLignesDocument operation = new RpgChargerLignesDocument();
    return operation.chargerListeLigneAchat(sessionServeur.getSystemManager(), pCriteres);
  }
  
  /**
   * Recherche les lignes d'un document à partir d'une liste d'identifiants.
   */
  @Override
  public ListeLigneAchat chargerListeLigneAchat(IdSession pIdSession, List<IdLigneAchat> pListeIdLigneAchat) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerListeLignesAchat(pListeIdLigneAchat);
  }
  
  /**
   * Recherche toutes lignes ID de lignes d'achat à partir des critères donnés.
   */
  @Override
  public List<IdLigneAchat> chargerListeIdLignesAchat(IdSession pIdSession, CritereLigneAchat pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerListeIdLignesAchat(pCriteres);
  }
  
  /**
   * Charger une ListeLigneAchatBase à partir d'une liste d'identifiants de lignes d'achat.
   */
  @Override
  public ListeLigneAchatBase chargerListeLigneAchatBase(IdSession pIdSession, List<IdLigneAchat> pListeId) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneAchatBase operation = new RpgLigneAchatBase();
    return operation.chargerListeLigneAchatBase(sessionServeur.getSystemManager(), pListeId);
  }
  
  /**
   * Charge les données des réceptions d'achat à partir d'une liste d'ID de lignes d'achat.
   */
  @Override
  public ListeReceptionAchat chargerListeReceptionAchat(IdSession pIdSession, List<IdLigneAchat> listeIdLignes) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerListeReceptionAchat(listeIdLignes);
  }
  
  /**
   * Supprime une liste de ligne d'un document.
   */
  @Override
  public void supprimerListeLigneDocument(IdSession pIdSession, List<LigneAchat> pListeLigne) throws RemoteException {
    if (pListeLigne == null || pListeLigne.isEmpty()) {
      return;
    }
    Date dateTraitement = new Date();
    for (LigneAchat ligneAchat : pListeLigne) {
      if (ligneAchat == null) {
        continue;
      }
      ligneAchat.setCodeExtraction(EnumCodeExtractionDocumentAchat.ANNULATION);
      sauverLigneAchat(pIdSession, ligneAchat, dateTraitement);
    }
  }
  
  /**
   * Valide un document d'achat.
   */
  @Override
  public void validerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement, EnumOptionEdition pOptionEdition)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgValiderDocumentAchat operation = new RpgValiderDocumentAchat();
    operation.validerDocument(sessionServeur.getSystemManager(), pIdDocument, pDateTraitement, pOptionEdition);
  }
  
  /**
   * Annule un document d'achat.
   */
  @Override
  public void annulerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgValiderDocumentAchat operation = new RpgValiderDocumentAchat();
    operation.annulerDocument(sessionServeur.getSystemManager(), pIdDocument, pDateTraitement);
  }
  
  /**
   * Mettre en attente un document d'achat.
   */
  @Override
  public void mettreEnAttenteDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgValiderDocumentAchat operation = new RpgValiderDocumentAchat();
    operation.mettreEnAttenteDocument(sessionServeur.getSystemManager(), pIdDocument, pDateTraitement);
  }
  
  /**
   * Edite un doucment d'achat.
   */
  @Override
  public String editerDocument(IdSession pIdSession, IdDocumentAchat pIdDocument, Date pDateTraitement,
      OptionsEditionAchat pOptionEdition, LdaSerien pLdaSerien) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgEditerDocumentAchat operation = new RpgEditerDocumentAchat();
    return operation.editerDocument(sessionServeur.getSystemManager(), pIdDocument, pDateTraitement, pOptionEdition, pLdaSerien);
  }
  
  /**
   * Retourne les documents d'achat et les lignes d'achats à partir d'une liste d'Id de ligne d'achat.
   * Les informations du document sont partielles.
   */
  @Override
  public ListeDocumentAchat chargerListeDocumentAchatAvecLigne(IdSession pIdSession, List<IdLigneAchat> pListeId) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    
    // On commence par récupérer les informations partielles d'entête des documents
    List<IdDocumentAchat> listeIdDocument = new ArrayList<IdDocumentAchat>();
    for (IdLigneAchat idLigneAchat : pListeId) {
      IdDocumentAchat.controlerId(idLigneAchat.getIdDocumentAchat(), true);
      if (!listeIdDocument.contains(idLigneAchat.getIdDocumentAchat())) {
        listeIdDocument.add(idLigneAchat.getIdDocumentAchat());
      }
    }
    ListeDocumentAchat listeDocument = sqlDocumentAchat.chargerListeDocumentAchat(listeIdDocument);
    
    // On récupère les informations sur les lignes qui nous intéressent
    RpgChargerLignesDocument operation = new RpgChargerLignesDocument();
    CritereLigneAchat critere = new CritereLigneAchat();
    for (IdLigneAchat idLigne : pListeId) {
      // On initialise les critères de recherche pour la ligne
      critere.setIdDocumentAchat(idLigne.getIdDocumentAchat());
      critere.setIdLigneAchat(idLigne);
      // On appelle le programme RPG qui va nous charger les donnés de la ligne
      ListeLigneAchat listeLigne = operation.chargerListeLigneAchat(sessionServeur.getSystemManager(), critere);
      // On charge la ligne dans la liste des lignes du document concerné
      DocumentAchat documentAchat = listeDocument.get(idLigne.getIdDocumentAchat());
      documentAchat.getListeLigneAchat().add(listeLigne.get(0));
    }
    
    return listeDocument;
  }
  
  /**
   * Retourne les documents d'achat générés automatiquement à partir d'un document de vente.
   */
  @Override
  public ListeDocumentAchat recupererCommandeAchatliee(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    ListeDocumentAchat listeDocumentAchat = null;
    if (pIdDocumentVente == null) {
      return listeDocumentAchat;
    }
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    List<IdDocumentAchat> listeIdDocumentAchat = sqlDocumentAchat.chargerCommandeAchatliee(pIdDocumentVente);
    if (listeIdDocumentAchat == null) {
      return null;
    }
    listeDocumentAchat = new ListeDocumentAchat();
    RpgChargerDocumentAchat operation = new RpgChargerDocumentAchat(querym);
    for (IdDocumentAchat idDocumentAchat : listeIdDocumentAchat) {
      DocumentAchat documentAchat = new DocumentAchat(idDocumentAchat);
      documentAchat = operation.chargerDocumentAchat(sessionServeur.getSystemManager(), idDocumentAchat, documentAchat, false);
      listeDocumentAchat.add(documentAchat);
    }
    
    return listeDocumentAchat;
  }
  
  /**
   * Recaclcule et retourne des informations complémentaires concernant une commande d'achat initiale qui a été réceptionnée.
   */
  @Override
  public ValeurInitialeCommandeAchat chargerDonneesCommandeInitiale(IdSession pIdSession, IdDocumentAchat pIdDocumentAchat)
      throws RemoteException {
    if (pIdDocumentAchat == null) {
      return null;
    }
    
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDocumentAchat sqlDocumentAchat = new SqlDocumentAchat(querym);
    return sqlDocumentAchat.chargerDonneesCommandeInitiale(pIdDocumentAchat);
  }
  
}
