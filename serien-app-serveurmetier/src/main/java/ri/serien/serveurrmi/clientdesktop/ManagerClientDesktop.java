/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi.clientdesktop;

import ri.serien.commun.licence.ManagerLicence;
import ri.serien.libas400.dao.rpg.exploitation.licence.RpgLicence;
import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.exploitation.licence.NombreUtilisateur;
import ri.serien.libcommun.exploitation.traceconnexion.EnumCodeDeconnexion;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.serveurrmi.SessionServeur;

/**
 * Cette classe regroupe la gestion des connexions des clients desktop côté serveur.
 */
public class ManagerClientDesktop {
  // Constantes
  private static final int DELAI_EN_SEC = 60;
  
  // Variables
  private static int compteurIdClientDesktop = 0;
  private static ListeClientDesktop listeClientDesktop = new ListeClientDesktop();
  private static Thread controleClientDesktop = null;
  
  // -- Méthodes publiques
  
  /**
   * Créer un nouvel objet client desktop et l'ajoute à la liste.
   */
  public synchronized static IdClientDesktop creerClientDesktop() {
    // Création de l'objet ClientDesktop et insertion dans la liste
    compteurIdClientDesktop++;
    IdClientDesktop idClientDesktop = IdClientDesktop.getInstance(compteurIdClientDesktop);
    ClientDesktop clientDesktop = new ClientDesktop(idClientDesktop);
    listeClientDesktop.add(clientDesktop);
    
    // Active une thread dédiée au contrôle des clients desktop
    if (controleClientDesktop == null) {
      demarrerControleClientDesktop();
    }
    
    Trace.info("Création du client desktop " + idClientDesktop);
    Trace.info("Nombre de client desktop en cours = " + listeClientDesktop.size());
    return idClientDesktop;
  }
  
  /**
   * Connecte un client desktop au serveur.
   */
  public static boolean connecterClientDesktop(IdClientDesktop pIdClientDesktop, EnvUser pEnvUser) {
    ClientDesktop clientDesktop = listeClientDesktop.retournerClientDesktopParId(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    
    // Connexion au serveur
    if (!clientDesktop.connecter(pEnvUser)) {
      return false;
    }
    // Contrôle de la licence si la connexion est opérationnelle
    controlerNombreUtilisateur(clientDesktop);
    return true;
  }
  
  /**
   * Supprime un client desktop à partir de son id et clôture toutes les connexions associées.
   */
  public static void detruireClientDesktop(IdClientDesktop pIdClientDesktop) {
    Trace.info("Destruction du client desktop " + pIdClientDesktop);
    ClientDesktop clientDesktop = listeClientDesktop.retournerClientDesktopParId(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    clientDesktop.deconnecter();
    listeClientDesktop.remove(clientDesktop);
  }
  
  /**
   * Retourne un objet Client Desktop à partir de son id.
   */
  public static ClientDesktop getClientDesktop(IdClientDesktop pIdClientDesktop) {
    return listeClientDesktop.retournerClientDesktopParId(pIdClientDesktop);
  }
  
  /**
   * Retourne le nombre de clients desktop en cours.
   */
  public static int getNombreClientDesktop() {
    return listeClientDesktop.size();
  }
  
  /**
   * Retourne les données utilisateurs d'un client desktop donné.
   */
  public static EnvUser getEnvUser(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = listeClientDesktop.retournerClientDesktopParId(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    return clientDesktop.getEnvUser();
  }
  
  /**
   * Permet à un Desktop de confirmer sa présence.
   */
  public static void confirmerPresenceClient(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = listeClientDesktop.retournerClientDesktopParId(pIdClientDesktop);
    if (clientDesktop == null) {
      return;
    }
    clientDesktop.confirmerPresence();
  }
  
  /**
   * Contrôle si les Desktop ont signalé leur présence dans le temps convenu.
   * Si ce n'est pas le cas le client Desktop n'est pas détruit mais l'information est marquée dans la table des traces de connexion.
   */
  public static void controlerPresenceClient() {
    for (ClientDesktop clientDesktop : listeClientDesktop) {
      if (!clientDesktop.isPresent()) {
        clientDesktop.tracerDeconnexion(EnumCodeDeconnexion.CLIENT_NON_PRESENT);
      }
    }
  }
  
  /**
   * Arrête le contrôle des clients desktop.
   */
  public static void arreterControleClientDesktop() {
    if (controleClientDesktop != null && controleClientDesktop.isAlive()) {
      controleClientDesktop.interrupt();
    }
    
    // Force l'arrêt des connexions desktop qui seraient encore présents
    forcerDeconnexionSysteme();
  }
  
  // -- Méthodes privées
  
  /**
   * Retourne le nombre total d'utilisateurs 5250.
   */
  private static NombreUtilisateur retournerNombreUtilisateur5250(ClientDesktop pClientDesktop) {
    if (pClientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    
    // Appel du programme RPG qui comptabilise ces utilisateurs
    RpgLicence operation = new RpgLicence();
    return operation.retournerNombreUtilisateur5250(pClientDesktop.getSystemeManager());
  }
  
  /**
   * Contrôle le nombre d'utilisateurs de Série N à l'instant t.
   */
  private static void controlerNombreUtilisateur(ClientDesktop pClientDesktop) {
    // Récupération du nombre d'utilisateurs 5250 et terminaux
    NombreUtilisateur nombreUtilisateur = retournerNombreUtilisateur5250(pClientDesktop);
    if (nombreUtilisateur == null) {
      throw new MessageErreurException("Le nombre d'utilisateurs 5250 et terminaux retourné par le service est invalide.");
    }
    
    // Récupération du nombre d'utilisateurs graphiques (dont les desktop ont confirmés leur présence récemment)
    int nombreUtilisateurGraphique = 0;
    for (ClientDesktop clientDesktop : listeClientDesktop) {
      if (clientDesktop.isPresent()) {
        nombreUtilisateurGraphique++;
      }
    }
    nombreUtilisateur.setGraphique(nombreUtilisateurGraphique);
    
    // Tracer la connexion
    Trace.debug("Nombre total d'utilisateurs 5250       = " + nombreUtilisateur.get5250());
    Trace.debug("Nombre total d'utilisateurs terminaux  =" + nombreUtilisateur.getTerminaux());
    Trace.debug("Nombre total d'utilisateurs graphiques = " + nombreUtilisateur.getGraphique());
    pClientDesktop.tracerConnexion(nombreUtilisateur);
    
    // Contrôle avec la licence utilisateur
    ManagerLicence.controlerLicenceUtilisateur(nombreUtilisateur);
  }
  
  /**
   * Active la thread qui va surveiller les clients desktop.
   */
  private static void demarrerControleClientDesktop() {
    if (controleClientDesktop != null) {
      return;
    }
    
    controleClientDesktop = new Thread() {
      @Override
      public void run() {
        // Construction du nom du thread
        String nomThread = String.format("ManagerClientDesktop(%03d)-controler", Thread.currentThread().getId());
        setName(nomThread);
        Trace.info("Démarrage du contrôle des clients desktop.");
        
        // Boucle infinie
        while (true) {
          try {
            // Contrôle la présence des clients desktop afin de mettre à jour la table des traces de connexion
            controlerPresenceClient();
            
            // Mise en attente
            Thread.sleep(DELAI_EN_SEC * 1000);
          }
          catch (InterruptedException ex) {
            Trace.info("Arrêt du contrôle des clients desktop.");
            // Très important de réinterrompre
            Thread.currentThread().interrupt();
            break;
          }
          catch (Exception e) {
          }
        }
      }
    };
    controleClientDesktop.start();
  }
  
  /**
   * Force la déconnexion des connexions afin d'éviter d'avoir des jobs qui trainent après l'arrêt du serveur Série N.
   * Il peut rester des instances de clients desktop pour plusieurs raisons alors que l'application côté desktop n'existe plus.
   * Cette méthode permet de d'arrêter proprement toutes les connexions des clients desktop. Elle est appelé lors de l'arrêt du serveur
   * RMI.
   */
  private static void forcerDeconnexionSysteme() {
    if (listeClientDesktop == null) {
      return;
    }
    
    // Parcourt de la liste des clients desktop encore connectés
    Trace.info("Force l'arrêt des clients desktop zombies : " + listeClientDesktop.size());
    for (ClientDesktop clientDesktop : listeClientDesktop) {
      if (clientDesktop == null) {
        continue;
      }
      Trace.info("Arrêt du client desktop " + clientDesktop);
      clientDesktop.deconnecter();
    }
    Trace.info("Fin de l'arrêt forcé des clients desktop zombies.");
  }
  
  /****************************************************************************
   * Les sessions
   ****************************************************************************/
  
  /**
   * Créer une nouvelle session pour un objet client desktop donné.
   */
  public static SessionServeur creerSession(IdClientDesktop pIdClientDesktop) {
    if (pIdClientDesktop == null) {
      throw new MessageErreurException("L'id du client desktop est incorrect.");
    }
    ClientDesktop clientDesktop = getClientDesktop(pIdClientDesktop);
    if (clientDesktop != null) {
      return clientDesktop.creerSession();
    }
    throw new MessageErreurException("L'objet client desktop est incorrect.");
  }
  
  /**
   * Supprime une session d'un client desktop donné.
   */
  public static void supprimerSession(IdSession pIdSession) {
    IdSession.controlerId(pIdSession, true);
    ClientDesktop clientDesktop = getClientDesktop(pIdSession.getIdClientDesktop());
    if (clientDesktop != null) {
      clientDesktop.supprimerSession(pIdSession);
    }
    else {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
  }
  
  /**
   * Retourne une session donnée.
   */
  public static SessionServeur getSession(IdSession pIdSession) {
    IdSession.controlerId(pIdSession, true);
    ClientDesktop clientDesktop = getClientDesktop(pIdSession.getIdClientDesktop());
    if (clientDesktop != null) {
      return clientDesktop.getSession(pIdSession);
    }
    throw new MessageErreurException("L'objet client desktop est incorrect.");
  }
  
  /**
   * Retourne le nombre de sessions pour un client desktop donné.
   */
  public static int getNombreSessions(IdSession pIdSession) {
    return getNombreSessions(pIdSession.getIdClientDesktop());
  }
  
  /**
   * Retourne le nombre de sessions pour un client desktop donné.
   */
  public static int getNombreSessions(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = getClientDesktop(pIdClientDesktop);
    if (clientDesktop != null) {
      return clientDesktop.getNombreSessions();
    }
    throw new MessageErreurException("L'objet client desktop est incorrect.");
  }
}
