/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi.clientdesktop;

import java.util.ArrayList;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.outils.Constantes;

/**
 * Liste de client desktop.
 * L'utilisation de cette classe est à privilégier dès qu'on manipule une liste de client desktop.
 */
public class ListeClientDesktop extends ArrayList<ClientDesktop> {
  
  /**
   * Constructeur.
   */
  public ListeClientDesktop() {
  }
  
  /**
   * Retourner un ClientDesktop de la liste à partir de son identifiant.
   */
  public ClientDesktop retournerClientDesktopParId(IdClientDesktop pIdClientDesktop) {
    if (pIdClientDesktop == null) {
      return null;
    }
    for (ClientDesktop clientDesktop : this) {
      if (clientDesktop != null && Constantes.equals(clientDesktop.getId(), pIdClientDesktop)) {
        return clientDesktop;
      }
    }
    
    return null;
  }
  
  /**
   * Tester si un ClientDesktop est présent dans la liste.
   */
  public boolean isPresent(IdClientDesktop pIdClientDesktop) {
    return retournerClientDesktopParId(pIdClientDesktop) != null;
  }
  
}
