/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi.clientdesktop;

import java.rmi.server.RemoteServer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import ri.serien.libas400.dao.sql.exploitation.SqlUtilisateur;
import ri.serien.libas400.dao.sql.exploitation.environnement.BibliothequeSpecifique;
import ri.serien.libas400.dao.sql.exploitation.traceconnexion.SqlTraceConnexion;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.VersionManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libas400.system.valeursysteme.ValeurSysteme;
import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.commun.classemetier.AbstractClasseMetier;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.exploitation.licence.NombreUtilisateur;
import ri.serien.libcommun.exploitation.traceconnexion.EnumCodeDeconnexion;
import ri.serien.libcommun.exploitation.traceconnexion.EnumTypeConnexion;
import ri.serien.libcommun.exploitation.traceconnexion.IdTraceConnexion;
import ri.serien.libcommun.exploitation.traceconnexion.TraceConnexion;
import ri.serien.libcommun.exploitation.version.Version;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurmetier.serveur.ServeurSerieN;
import ri.serien.serveurrmi.SessionServeur;

/**
 * Informations de base de client desktop.
 */
public class ClientDesktop extends AbstractClasseMetier<IdClientDesktop> {
  // Variables
  private SystemeManager systemeManager = null;
  private QueryManager queryManager = null;
  private SousEnvironnement sousEnvironnement = null;
  private EnvUser envUser = null;
  private int compteurIdSession = 0;
  private Map<IdSession, SessionServeur> listSessions = new ConcurrentHashMap<IdSession, SessionServeur>();
  private Date dernierePresenceClient = new Date();
  private TraceConnexion traceConnexion = null;
  
  /**
   * Constructeur.
   */
  public ClientDesktop(IdClientDesktop pIdClientDesktop) {
    super(pIdClientDesktop);
  }
  
  // -- Méthodes publiques
  
  @Override
  public String getTexte() {
    // Retourner le texte de l'identifiant à défaut de mieux car je ne vois pas quel libellé pertinent on a pour l'instant.
    return id.getTexte();
  }
  
  /**
   * Connecte l'objet client desktop au serveur à l'aide des paramètres.
   */
  public boolean connecter(EnvUser pEnvUser) {
    if (pEnvUser == null) {
      throw new MessageErreurException("Les données de connexion au serveur sont invalides.");
    }
    envUser = pEnvUser;
    Trace.debug("-connecterClientDesktop-> " + id + " host:" + envUser.getHost() + " profil:" + envUser.getProfil() + " mdp:"
        + envUser.getMotDePasse());
    
    if (systemeManager != null && systemeManager.isConnecte()
        && Constantes.equals(systemeManager.getProfilUtilisateur(), envUser.getProfil())) {
      Trace.debug("-connecterClientDesktop-> " + id + " La connexion est déjà établi.");
      return true;
    }
    
    // Mise à jour des informations du client desktop
    sousEnvironnement = ServeurSerieN.getListeSousEnvironnement().getSousEnvironnement(envUser.getIdSousEnvironnement());
    // Mise à jour des données de EnvUser à l'aide des informations du sous-environnement
    sousEnvironnement.initialiserEnvUser(envUser);
    envUser.setVersionServeur(ServeurSerieN.VERSION);
    envUser.getDossierServeur().setDossierRacine(ManagerAS400.getDossierRacineServeur());
    envUser.setCodepageServeur(System.getProperty("file.encoding"));
    envUser.setListeBibliothequeUtilisateur(ValeurSysteme.getListeBibliothequeUtilisateur());
    
    // Connection à l'AS400 et serveur JDBC puis mise à jour des données du SystemManager
    try {
      systemeManager = new SystemeManager(envUser.getHost(), envUser.getProfil(), envUser.getMotDePasse(), true);
    }
    catch (Exception e) {
      return false;
    }
    if (!systemeManager.isConnecte()) {
      return false;
    }
    Trace.debug("--SystemeManager (" + id + ") -->" + systemeManager + "/" + sousEnvironnement.getLettreExecution());
    systemeManager.setProfilUtilisateur(envUser.getProfil());
    systemeManager.setLibenv(sousEnvironnement.getBibliothequeEnvironnement());
    if (envUser.getCurlib() != null) {
      systemeManager.setCurlib(envUser.getCurlib().getNom());
    }
    
    // Mise à jour des données du QueryManager
    initialiserQueryManager();
    
    // Lecture des données utilisateurs
    chargerInformationsEnvUser();
    connecterBaseDeDonnees(envUser.getCurlib());
    return true;
  }
  
  /**
   * Déconnecte le client desktop du serveur.
   */
  public void deconnecter() {
    // Tracer la déconnexion
    tracerDeconnexion(EnumCodeDeconnexion.NORMALE);
    
    // Déconnexion de la base de données
    deconnecterBaseDeDonnees();
    
    // Déconnexion du serveur AS400
    if (systemeManager != null) {
      systemeManager.deconnecter();
    }
  }
  
  /**
   * Contrôle la version des programmes Série N et de la base de données courante.
   * 0 = Versions identiques
   * 1 = La version de Série N est supérieure à la version de la base de données
   * -1 = La version de Série N est antérieure à la version de la base de données
   */
  public int controlerVersion() {
    if (envUser.getVersionSerieN() == null || envUser.getCurlib() == null || systemeManager == null) {
      throw new MessageErreurException("Les données sont invalides pour le contrôle de version.");
    }
    // Lire la version de la base de données
    Version versionBd = VersionManager.retournerVersionBaseDeDonnee(systemeManager.getSystem(), envUser.getCurlib());
    envUser.getCurlib().setVersion(versionBd);
    return envUser.getVersionSerieN().compareTo(versionBd);
  }
  
  /**
   * Créer une nouvelle session.
   */
  public SessionServeur creerSession() {
    Trace.info("Création d'une nouvelle session pour le client desktop " + id);
    // On calcule un nouvel id
    compteurIdSession++;
    IdSession idSession = IdSession.getInstance(id, compteurIdSession);
    
    // On instancie la session serveur
    SessionServeur sessionServeur = new SessionServeur(idSession, envUser);
    ajouterSession(idSession, sessionServeur);
    String profil = null;
    if (envUser != null) {
      profil = envUser.getProfil();
    }
    
    Trace.info("Créer une session : idSession=" + idSession + " pour l'idClientDesktop " + id + " du profil " + profil);
    return sessionServeur;
  }
  
  /**
   * Retourner la session à partir de son identifiant.
   * La méthode retourne toujours un objet (elle ne retourne jamais null). Si elle ne trouve pas de session associée à l'identififant,
   * elle génère une exception. Pour vérifier la présence d'une session via son identifiant, utiliser la méthode isPresent() qui ne
   * générera pas d'errreur si la session n'existe pas.
   */
  public SessionServeur getSession(IdSession pIdSession) {
    if (pIdSession == null) {
      throw new MessageErreurException("Impossible de retourner la session car son identifiant est invalide.");
    }
    return listSessions.get(pIdSession);
  }
  
  /**
   * Vérifie l'existence d'une session à partir de son identifiant.
   */
  public boolean isSessionPresente(IdSession pIdSession) {
    return listSessions.containsKey(pIdSession);
  }
  
  /**
   * Ajouter une session.
   */
  public void ajouterSession(IdSession pIdSession, SessionServeur pSessionServeur) {
    if (listSessions.containsKey(pIdSession)) {
      throw new MessageErreurException(
          "Impossible d'ajouter une nouvelle session car il existe déjà une session avec l'identifiant " + pIdSession);
    }
    listSessions.put(pIdSession, pSessionServeur);
  }
  
  /**
   * Supprimer une session.
   */
  public void supprimerSession(IdSession pIdSession) {
    listSessions.remove(pIdSession);
  }
  
  /**
   * Retourne le nombre de SessionServer.
   */
  public int getNombreSessions() {
    return listSessions.size();
  }
  
  /**
   * Connexion à la base de données.
   */
  public void connecterBaseDeDonnees(BDD pBaseDeDonnees) {
    if (pBaseDeDonnees == null) {
      throw new MessageErreurException("La base de données est invalide.");
    }
    // Déconnexion de la base de donnée actuelle
    Trace.debug("Déconnection de la base de données " + envUser.getCurlib() + '.');
    deconnecterBaseDeDonnees();
    
    // baseDeDonnees = pBaseDeDonnees;
    envUser.setCurlib(pBaseDeDonnees);
    
    // Reconnexion à la nouvelle base de données
    Trace.debug("Connexion à la base de données " + envUser.getCurlib() + '.');
    systemeManager.setCurlib(envUser.getCurlib().getNom());
    systemeManager.connecterJDBC(envUser.getProfil(), envUser.getMotDePasse(), true);
    initialiserQueryManager();
    
    // Récupération de la version et de la description de la base de données
    if (systemeManager != null) {
      // Version
      Version versionBd = VersionManager.retournerVersionBaseDeDonnee(systemeManager.getSystem(), envUser.getCurlib());
      envUser.getCurlib().setVersion(versionBd);
      // Description
      String description = BibliothequeAS400.chargerDescription(systemeManager.getSystem(), envUser.getCurlib().getId());
      envUser.getCurlib().setDescription(description);
    }
  }
  
  /**
   * Déconnecte de la base de données.
   */
  public void deconnecterBaseDeDonnees() {
    // Déconnexion du query manager
    if (queryManager != null) {
      queryManager.deconnecter();
    }
    // Déconnexion du JDBC
    if (systemeManager != null) {
      systemeManager.deconnecterJDBC();
    }
  }
  
  /**
   * Met à jour la variable de présence avec la date et heure.
   */
  public void confirmerPresence() {
    dernierePresenceClient = new Date();
    tracerDeconnexion(EnumCodeDeconnexion.CONNEXION_EN_COURS);
    Trace.alerte("Confirmation de présence de " + id + " à " + dernierePresenceClient);
  }
  
  /**
   * Confirme que le desktop était présent récemment.
   */
  public boolean isPresent() {
    if (dernierePresenceClient == null) {
      return false;
    }
    // Calcul de la différence en seconde entre maintenant et la dernière confirmation de présence du desktop
    long deltaEnSeconde = DateHeure.getDeltaEnSecondeEntreDate(new Date(), dernierePresenceClient);
    if (deltaEnSeconde > ManagerSessionClient.DELAI_CONFIRMATION_PRESENCE_EN_SEC) {
      return false;
    }
    return true;
  }
  
  /**
   * Trace la connexion de l'utilisateur.
   */
  public void tracerConnexion(NombreUtilisateur pNombreUtilisateur) {
    if (envUser == null) {
      return;
    }
    try {
      IdTraceConnexion idTraceConnexion = IdTraceConnexion.getInstanceAvecCreationId(envUser.getProfil());
      traceConnexion = new TraceConnexion(idTraceConnexion);
      traceConnexion.setDateDebut(new Date());
      traceConnexion.setAdresseIP(RemoteServer.getClientHost());
      traceConnexion.setCodeDeconnexion(EnumCodeDeconnexion.CONNEXION_EN_COURS);
      traceConnexion.setIdClientDesktop(getId());
      traceConnexion.setTypeConnexion(EnumTypeConnexion.GRAPHIQUE);
      traceConnexion.setNombreUtilisateur(pNombreUtilisateur);
      
      // Enregistrement en base
      SqlTraceConnexion operation = new SqlTraceConnexion(queryManager);
      idTraceConnexion = operation.sauverTraceConnexion(traceConnexion);
      traceConnexion.setId(idTraceConnexion);
    }
    catch (Exception e) {
    }
  }
  
  /**
   * Trace la déconnexion de l'utilisateur.
   */
  public void tracerDeconnexion(EnumCodeDeconnexion pCodeDeconnexion) {
    try {
      traceConnexion.setCodeDeconnexion(pCodeDeconnexion);
      traceConnexion.setDateFin(new Date());
      
      // Mise à jour en base
      SqlTraceConnexion operation = new SqlTraceConnexion(queryManager);
      operation.sauverTraceConnexion(traceConnexion);
    }
    catch (Exception e) {
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialise le query manager.
   */
  private void initialiserQueryManager() {
    // Mise à jour des données du QueryManager
    if (systemeManager.isJDBCconnecte()) {
      Trace.debug("-initialiserQueryManager()-> ");
      queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
      if (envUser.getCurlib() != null) {
        queryManager.setLibrary(envUser.getCurlib().getNom());
      }
      queryManager.setLibrairieEnvironnement(sousEnvironnement.getBibliothequeEnvironnement().getNom());
    }
  }
  
  /**
   * Charge les informations de l'utilisateur contenu dans la table PSEMUSSM et les dataarea.
   */
  private void chargerInformationsEnvUser() {
    SqlUtilisateur sqlUtilisateur = new SqlUtilisateur(queryManager);
    envUser = sqlUtilisateur.lireUtilisateur(envUser);
    
    // Récupération de la liste des bibilothèques spécifiques uniquement si le profil a une curlib de configuré
    // car dans le cas contraire le profil n'est pas utilisable
    if (envUser.isCurlibValide()) {
      BibliothequeSpecifique operationSpe = new BibliothequeSpecifique(systemeManager);
      ArrayList<String> listeSpe = operationSpe.chargerListeBibliothequeSpecifiqueEnvironnement(envUser.getBibliothequeEnvironnement(),
          envUser.getCurlib(), "" + envUser.getLetter());
      envUser.setListeBibSpe(listeSpe);
    }
    
    // La version de Série N
    envUser.setVersionSerieN(VersionManager.retournerVersionSerieN(systemeManager.getSystem(), envUser.getLetter()));
  }
  
  /**
   * Met à jour les informations de l'utilisateur.
   */
  private EnvUser mettreAJourEnvUser(EnvUser pEnvUser) {
    if (pEnvUser != null && sousEnvironnement != null) {
      envUser.setDossierTravail(sousEnvironnement.getId().toString());
    }
    return envUser;
  }
  
  // -- Accesseurs
  
  public EnvUser getEnvUser() {
    return envUser;
  }
  
  public String getHost() {
    return envUser.getHost();
  }
  
  public String getProfil() {
    return envUser.getProfil();
  }
  
  public String getMdp() {
    return envUser.getMotDePasse();
  }
  
  public SystemeManager getSystemeManager() {
    systemeManager.setCurlib(envUser.getCurlib().getNom());
    Trace.debug("-getSystemeManager()-> " + systemeManager.getCurlib());
    return systemeManager;
  }
  
  public QueryManager getQueryManager() {
    queryManager.setLibrary(envUser.getCurlib().getNom());
    Trace.debug("-queryManager()-> " + queryManager.getLibrary());
    return queryManager;
  }
  
  public SousEnvironnement getSousEnvironnement() {
    return sousEnvironnement;
  }
  
  public BDD getBaseDeDonnees() {
    if (envUser == null) {
      return null;
    }
    return envUser.getCurlib();
  }
  
  public BDD getBaseDeDonneesParDefaut() {
    if (envUser == null) {
      return null;
    }
    return envUser.getCurlibParDefault();
  }
  
  public Date getDernierePresenceClient() {
    return dernierePresenceClient;
  }
  
}
