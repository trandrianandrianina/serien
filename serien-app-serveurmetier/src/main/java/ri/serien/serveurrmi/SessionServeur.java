/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.util.Enumeration;
import java.util.concurrent.ConcurrentHashMap;

import com.ibm.as400.access.Job;
import com.ibm.as400.access.JobList;

import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.exploitation.environnement.FichierRuntime;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvProgram;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurmetier.serveur.ServeurSerieN;
import ri.serien.serveurrmi.clientdesktop.ClientDesktop;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Stocke les informations d'une session côté serveur.
 */
public class SessionServeur {
  // Variables
  // Contient la liste des devices différentes (prefixe) et le numéro d'incrément pour chacune
  private static ConcurrentHashMap<String, Integer> listeCompteurDevice = new ConcurrentHashMap<String, Integer>();
  
  private IdSession idSession = null;
  private EnumTypeSession type = EnumTypeSession.SESSION_INCONNUE;
  private SessionAS400 sessionAS400 = null;
  private EnvProgram envProgram = null;
  private String clientIP = null;
  private EnvUser envUser = null;
  private String profil = "";
  
  private ClientDesktop clientDesktop = null;
  
  /**
   * Constructeur.
   */
  public SessionServeur(IdSession pIdSession, EnvUser pEnvUser) {
    idSession = pIdSession;
    envUser = pEnvUser;
    if (pEnvUser != null) {
      profil = pEnvUser.getProfil();
    }
    clientIP = "unknown";
  }
  
  public IdSession getIdSession() {
    return idSession;
  }
  
  public String dialoguerAvecProgramme(String messageaenvoyer, int waitsec) {
    Trace.debug(SessionServeur.class, "dialoguerAvecProgramme", "sessionas400", sessionAS400);
    if (sessionAS400 == null) {
      return null;
    }
    return sessionAS400.dialoguerAvecProgramme(messageaenvoyer, waitsec);
  }
  
  public boolean envoyerMessageAuProgramme(String messageaenvoyer) {
    if (sessionAS400 == null) {
      return false;
    }
    return sessionAS400.envoyerMessageAuProgramme(messageaenvoyer);
  }
  
  public String recevoirMessageDuProgramme(int waitsec) {
    Trace.debug(SessionServeur.class, "recevoirMessageDuProgramme", "sessionas400", sessionAS400);
    if (sessionAS400 == null) {
      return null;
    }
    return sessionAS400.recevoirMessageDuProgramme(waitsec);
  }
  
  // -- Méthodes publiques
  
  public ListeSousEnvironnement listerSousEnvironnement() {
    return ServeurSerieN.getListeSousEnvironnement().listerSousEnvironnementPourIP(clientIP);
  }
  
  /**
   * Récupérer le sous-environnement à partir de son Id.
   */
  public SousEnvironnement recupererSousEnvironnement(IdSousEnvironnement pIdSousEnvironnement) {
    SousEnvironnement sousEnvironnement = listerSousEnvironnement().getSousEnvironnement(pIdSousEnvironnement);
    if (sousEnvironnement == null) {
      throw new MessageErreurException("Aucun sous-système ne correspond au nom " + pIdSousEnvironnement);
    }
    
    // Mettre à jour les runtimes du sous-environnement
    // Pour gérer le cas où les runtimes du serveur ont été changés et que le serveur n'a pas été redémarré
    sousEnvironnement.balayerDossierRuntime();
    return sousEnvironnement;
  }
  
  public FichierRuntime chargerFichierRuntime(long idsession, String pNomFichierRuntime) {
    Trace.info("Récupérer le fichier runtime : " + pNomFichierRuntime);
    IdSousEnvironnement idSousEnvironnement = clientDesktop.getSousEnvironnement().getId();
    return listerSousEnvironnement().chargerFichierRuntime(idSousEnvironnement, pNomFichierRuntime);
  }
  
  /**
   * Ouvre une session.
   */
  public void ouvrirSession(EnumTypeSession pEnumSession, String pMessage) {
    type = pEnumSession;
    Trace.info("Démarrer une session de type " + type.getLibelle() + " : idSession=" + idSession + " profil=" + profil);
    
    clientDesktop = ManagerClientDesktop.getClientDesktop(idSession.getIdClientDesktop());
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    
    switch (type) {
      case SESSION_RPG:
        initialiserSessionRPG(pMessage);
        break;
      case SESSION_JAVA:
        initialiserSessionJava(pMessage);
        break;
      default:
        break;
    }
    
    Trace.info(
        "Nombre de sessions en cours pour ce client desktop= " + ManagerClientDesktop.getNombreSessions(idSession.getIdClientDesktop()));
    Trace.afficherEtatMemoire();
  }
  
  /**
   * Détermine un nouveau nom de device pour l'utilisateur en cours.
   * Le nom de la device est contrôlée avec les nom de job déjà actif afin d'être sur qu'il n'y a pas d'ambiguité possible.
   * @return Le nom de la device.
   */
  private String determinerNouvelleDevice(String pPrefixeDevice) {
    pPrefixeDevice = Constantes.normerTexte(pPrefixeDevice);
    if (pPrefixeDevice.isEmpty()) {
      throw new MessageErreurException("Le préfixe de la device est invalide.");
    }
    
    // Construire le nom de la device en incrémentant le numéro
    int numeroCompteurDevice = 0;
    // Récupère le numéro de device le plus élevé pour le préfixe de la device en cours
    if (listeCompteurDevice.containsKey(pPrefixeDevice)) {
      numeroCompteurDevice = listeCompteurDevice.get(pPrefixeDevice).intValue();
      numeroCompteurDevice++;
    }
    // Insertion immédiate de la device dans la liste afin d'éviter qu'un autre utilisateur récupère le même numéro pendant le contrôle de
    // l'existence du job
    listeCompteurDevice.put(pPrefixeDevice, Integer.valueOf(numeroCompteurDevice));
    
    // Construction du nom de la device finale
    String device = pPrefixeDevice + String.format("%04d", numeroCompteurDevice);
    
    // Contrôle si un job a le même nom que celui de la device
    try {
      JobList listeJob = new JobList(ManagerAS400.getSystemeAS400());
      listeJob.clearJobSelectionCriteria();
      listeJob.addJobSelectionCriteria(JobList.SELECTION_PRIMARY_JOB_STATUS_ACTIVE, Boolean.TRUE);
      listeJob.addJobSelectionCriteria(JobList.SELECTION_PRIMARY_JOB_STATUS_JOBQ, Boolean.FALSE);
      listeJob.addJobSelectionCriteria(JobList.SELECTION_PRIMARY_JOB_STATUS_OUTQ, Boolean.FALSE);
      boolean trouve;
      do {
        trouve = false;
        // Récupération de la liste des jobs actifs sur l'AS00
        Enumeration liste = listeJob.getJobs();
        // Parcourt de la liste des jobs afin de contrôler si la device calculée n'est pas déjà utilisée
        while (liste.hasMoreElements()) {
          Job job = (Job) liste.nextElement();
          // Contrôle le nom du job
          if (job.getName().equalsIgnoreCase(device)) {
            Trace.alerte("Le job " + device + " existe déjà, une nouvelle device va être calculée.");
            // Détermine une nouvelle device
            numeroCompteurDevice = listeCompteurDevice.get(pPrefixeDevice).intValue();
            listeCompteurDevice.put(pPrefixeDevice, Integer.valueOf(++numeroCompteurDevice));
            device = pPrefixeDevice + String.format("%04d", numeroCompteurDevice);
            trouve = true;
            break;
          }
        }
      }
      while (trouve);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du contrôle de la détermination de nouvelle device " + device + '.');
    }
    
    return device;
  }
  
  /**
   * Initialiser une session RPG.
   */
  private void initialiserSessionRPG(String pMessage) {
    Trace.debug("-initialiserSessionRPG->" + pMessage);
    // Récupérer les informations de l'utilisateur et du programme
    int pos = pMessage.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR);
    String idUser = pMessage.substring(0, pos);
    envProgram = new EnvProgram(pMessage.substring(pos + 1));
    
    clientDesktop.getEnvUser().setId(idUser);
    envProgram.setEnvUser(clientDesktop.getEnvUser());
    
    // Détermination de la nouvelle device
    String device = determinerNouvelleDevice(envProgram.getEnvUser().getPrefixDevice());
    envProgram.getEnvUser().setDevice(device);
    
    // Ouvrir la session AS400
    sessionAS400 = new SessionAS400(clientDesktop.getSystemeManager().getSystem(), envProgram, Constantes.DATAQ_TIMEOUT,
        ManagerAS400.getMessageQueue(), idSession);
    sessionAS400.setDebug(false);
    sessionAS400.setListeSousEnvironnement(listerSousEnvironnement());
    sessionAS400.demarre();
  }
  
  /**
   * Initialiser une session Java.
   */
  private void initialiserSessionJava(String pMessage) {
    Trace.debug("-initialiserSessionJava->" + pMessage);
    // Récupérer les informations de l'utilisateur et du programme
    int pos = pMessage.indexOf(Constantes.SEPARATEUR_CHAINE_CHAR);
    String idUser = pMessage.substring(0, pos);
    envProgram = new EnvProgram(pMessage.substring(pos + 1));
    clientDesktop.getEnvUser().setId(idUser);
    envProgram.setEnvUser(clientDesktop.getEnvUser());
    
    // Détermination de la nouvelle device
    String device = determinerNouvelleDevice(envProgram.getEnvUser().getPrefixDevice());
    envProgram.getEnvUser().setDevice(device);
  }
  
  /**
   * Fermer la session.
   */
  public void fermerSession() {
    // Fermer la session AS400 si besoin
    if (sessionAS400 != null) {
      sessionAS400.terminerSession(false);
    }
    
    // Supprimer la session
    ManagerClientDesktop.supprimerSession(idSession);
    
    // Tracer
    Trace.info("Fermer une session : idSession=" + idSession + " profil=" + profil + " type=" + type.getLibelle());
    Trace.info("Nombre de sessions en cours = " + ManagerClientDesktop.getNombreSessions(idSession));
  }
  
  public void repondreMessageErreur(long idsession, String reponse) {
    // On récupère la réponse du client pour le job soumis
    String[] infos = Constantes.splitString(reponse, Constantes.SEPARATEUR_CHAINE_CHAR);
    sessionAS400.replyQueuedMessage(infos[1]);
  }
  
  /**
   * Lance une requete système (TODO A transformer en service assez rapidement)
   */
  public String effectuerRequeteSysteme(long idsession, String chaine) {
    Trace.info(chaine);
    IdSousEnvironnement idSousEnvironnement = clientDesktop.getSousEnvironnement().getId();
    TraitementRequeteSystemeMetier trs = new TraitementRequeteSystemeMetier(clientDesktop.getSystemeManager().getSystem(), chaine,
        listerSousEnvironnement().getSousEnvironnement(idSousEnvironnement), ManagerAS400.getDossierRacineServeur());
    String message = trs.getResponse();
    trs.dispose();
    return message;
  }
  
  /**
   * Libère les ressources
   */
  public void dispose() {
    if (sessionAS400 != null) {
      sessionAS400.terminerSession(true);
      sessionAS400 = null;
    }
    if (envProgram != null) {
      envProgram.dispose();
      envProgram = null;
    }
  }
  
  // -- Accesseurs
  
  public EnvUser getEnvUser() {
    return envUser;
  }
  
  /**
   * Retourne le systeme manager.
   */
  public SystemeManager getSystemManager() {
    return clientDesktop.getSystemeManager();
  }
  
  public QueryManager getQueryManager() {
    return clientDesktop.getQueryManager();
  }
}
