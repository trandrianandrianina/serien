/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;

import ri.serien.libas400.dao.gvx.programs.parametres.GM_Personnalisation;
import ri.serien.libas400.dao.gvx.programs.tiers.pricejet.GestionPriceJet;
import ri.serien.libas400.dao.sql.gescom.gvc.DAOLignesGVC;
import ri.serien.libas400.dao.sql.gescom.gvc.DAOParametreGVC;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.fournisseur.Fournisseur;
import ri.serien.libcommun.gescom.vente.gvc.ConditionAchatGVC;
import ri.serien.libcommun.gescom.vente.gvc.FiltresGVC;
import ri.serien.libcommun.gescom.vente.gvc.LigneGvc;
import ri.serien.libcommun.gescom.vente.gvc.ListeParametresGVC;
import ri.serien.libcommun.gescom.vente.gvc.pricejet.TarifPriceJet;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceGVC;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Cette classe définit l'ensemble des services propres à la gestion de veille concurrentielle
 */
public class ServiceGVC implements InterfaceServiceGVC {
  private static InterfaceServiceGVC service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceGVC() {
  }
  
  /**
   * Retourner l'implémentation du service GVC.
   */
  protected static InterfaceServiceGVC getService() {
    if (service == null) {
      InterfaceServiceGVC interfaceService = new ServiceGVC();
      service = (InterfaceServiceGVC) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceGVC.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Récupérer la liste des paramètres du GVC à partir de la base de données.
   */
  @Override
  public ListeParametresGVC chargerListeParametresGVC(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOParametreGVC daoParametreGVC = new DAOParametreGVC(queryManager);
    return daoParametreGVC.chargerListeParametresGVC();
  }
  
  /**
   * Ecrire la liste des paramètres du GVC dans la base de données.
   */
  @Override
  public void sauverListeParametresGVC(IdSession pIdSession, ListeParametresGVC listeParametresGVC) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOParametreGVC daoParametreGVC = new DAOParametreGVC(queryManager);
    daoParametreGVC.sauverListeParametresGVC(listeParametresGVC);
  }
  
  /**
   * Retourne le taux de TVA défini dans la DG (établisssement pilote)
   */
  @Override
  public BigDecimal chargerTauxTVAetbPilote(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    GM_Personnalisation personnalisation = new GM_Personnalisation(queryManager);
    return personnalisation.getTVA1EtablissementPilote();
  }
  
  /**
   * Retourne l'ensemble des établissements de la bibliothèque de l'utilisateur
   */
  @Override
  public String[] chargerListeEtablissement(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    GM_Personnalisation personnalisation = new GM_Personnalisation(queryManager);
    return personnalisation.getListeEtablissements();
  }
  
  /**
   * Retourne la liste des profils SERIE N de l'environnement en cours
   */
  @Override
  public ArrayList<String> chargerListeProfils(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    ArrayList<GenericRecord> liste = queryManager
        .select(" SELECT USSPRF FROM " + queryManager.getLibrairieEnvironnement() + ".PSEMUSSM WHERE USSDSP = 'N' ORDER BY USSPRF ");
    if (liste == null) {
      throw new MessageErreurException("Erreur lors de la lecture des profils : " + queryManager.getMsgError());
    }
    
    ArrayList<String> listeProfils = new ArrayList<String>();
    for (int i = 0; i < liste.size(); i++) {
      if (liste.get(i).isPresentField("USSPRF")) {
        listeProfils.add(liste.get(i).getField("USSPRF").toString().trim());
      }
    }
    
    return listeProfils;
  }
  
  /**
   * retourne le nombre d'enregistrements de la recherche de lignes GVC
   */
  @Override
  public int chargerNombreLignesGVC(IdSession pIdSession, FiltresGVC pFiltres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerNombreLignesGVC(pFiltres);
  }
  
  /**
   * Retourner les enregistrements de la recherche de lignes GVC
   */
  @Override
  public ArrayList<LigneGvc> chargerListeLigneGvc(IdSession pIdSession, FiltresGVC pFiltres, boolean onLitTout) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerListeLigneGvc(pFiltres, onLitTout);
  }
  
  /**
   * Lire la condition d'achat active du fournisseur principal d'un article Série N
   * TODO conscient que l'objet ConditionAchat existe déjà avec appel RPG. Celui-ci est trop éloigné de l'existant sql pour en modifier
   * la structure (risque d'impact sur les démos HOLLOCO)
   */
  @Override
  public ConditionAchatGVC chargerConditionAchatGVC(IdSession pIdSession, LigneGvc pLigne, Fournisseur pFournisseur)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerConditionAchatGVC(pLigne, pFournisseur);
  }
  
  /**
   * Lire la condition d'achat active du distributeur de référence d'un article Série N si elle existe
   */
  @Override
  public ConditionAchatGVC chargerConditionAchatGVCdistributeurReference(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerConditionAchatGVCdistributeurReference(pLigne);
  }
  
  /**
   * Récupérer les historiques de Prix d'achat d'un article
   * sous la forme d'un tableau de Bigdecimal de longueur 2
   * 0 -> PUMP prix unitaire moyen pondéré, 1 -> H1PRV prix de revient standard
   */
  @Override
  public BigDecimal[] chargerPrixAchatArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerPrixAchatArticle(pIdArticle);
  }
  
  /**
   * Insérer ou mettre à jour les 10 colonnes de tarifs de vente d'un article Série N à partir d'une ligne GVC
   */
  @Override
  public boolean sauverTarifsArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.sauverTarifsArticle(pLigne);
  }
  
  /**
   * Insérer ou mettre à jour le workflow d'une ligne Gvc :
   * état du workflow, expéditeur et destinataire
   */
  @Override
  public boolean sauverWorkFlow(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.sauverWorkFlow(pLigne);
  }
  
  /**
   * Met à jour la valeur de mise en ligne Web de l'article Série N
   */
  @Override
  public boolean sauverMiseEnLigneArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.sauverMiseEnLigneArticle(pLigne);
  }
  
  /**
   * Met à jour les points Watt de l'article dans la base de données Série N
   */
  @Override
  public boolean sauverPointsWattArticle(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.sauverPointsWattArticle(pLigne);
  }
  
  /**
   * Met à jour les informations GSB propres à la veille des GSB (grandes surfaces de bricolage) d'une ligne GVC
   **/
  @Override
  public boolean sauverVeilleGSB(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.sauverVeilleGSB(pLigne);
  }
  
  /**
   * Crée un flux Magento FLX001 sur la base d'une ligne GVC
   */
  @Override
  public void sauverFluxMagento(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    daolignesGVC.sauverFluxMagento(pLigne);
  }
  
  /**
   * Permet de calculer s'il existe un tarif PriceJet moins cher que notre tarif TTC particulier,
   * met à jour ce tarif et le nom du concurrent correspondant
   * Si on déjà scanné cet article sur Pricejet, et qu'il n'a pas de tarif
   * on force malgré tout la création de l'objet à vide pour détecter ce passif et éviter de scanner à nouveau
   */
  @Override
  public TarifPriceJet chargerTarifPriceJet(IdSession pIdSession, LigneGvc pLigne) throws RemoteException {
    TarifPriceJet tarifPriceJet = null;
    GestionPriceJet gestionPJ = new GestionPriceJet();
    if (pLigne.getIdArticle() != null && pLigne.getPrixMoinsCher() != null) {
      tarifPriceJet = gestionPJ.retournerTarifConcurrentMoinsCher(pLigne.getIdArticle());
      
      if (tarifPriceJet == null) {
        tarifPriceJet = new TarifPriceJet(null);
      }
    }
    else {
      tarifPriceJet = new TarifPriceJet(null);
    }
    return tarifPriceJet;
  }
  
  @Override
  public String chargerAdresseMailProfilAs400(IdSession pIdSession, String pProfil) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    DAOLignesGVC daolignesGVC = new DAOLignesGVC(queryManager);
    return daolignesGVC.chargerAdresseMailProfilAs400(pProfil);
  }
  
}
