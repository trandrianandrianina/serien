/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.io.File;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.ibm.as400.access.AS400;

import ri.serien.libas400.dao.gvx.programs.UtilisateurETB_MAG;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.TraitementRequeteSysteme;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.protocolemessage.MessageHead;

/**
 * Regroupe toutes les requêtes pour le système avec les spécificités métiers
 * Permet de charger dynamiquement les jar du dossier "metier"
 */
public class TraitementRequeteSystemeMetier extends TraitementRequeteSysteme {
  private SystemeManager systemeM = null;
  private SousEnvironnement sousEnvironnement = null;
  private String racineServeur = null;
  private URL[] listUrlClassloader = null;
  
  /**
   * Constructeur
   * @param system
   * @param amessage
   */
  public TraitementRequeteSystemeMetier(AS400 system, String amessage, SousEnvironnement pSousEnvironnement, String racine) {
    super(system, amessage);
    systemeM = new SystemeManager(system, true);
    sousEnvironnement = pSousEnvironnement;
    racineServeur = racine;
    listUrlClassloader = getListURL();
  }
  
  /**
   * Analyse le message reçu
   * 
   */
  @Override
  protected void analyseMessage() {
    super.analyseMessage();
    if (reponse != null) {
      return;
    }
    
    // Vérification que les jar (Métier) soient bien dans le classpath
    initJAR();
    
    // C'est un message avec une chaine complexe (ici du JSON)
    if ((mc.getHead().getBodycode() == MessageHead.BODY_COMPLEXSTRING) && (mc.getHead().getBodydesc().contains("IDREQUEST"))) {
      int idrequest = 0;
      JsonObject objet = null;
      
      // Extraction de l'ID pour connaitre le type de requête
      try {
        objet = JsonParser.parseString(mc.getHead().getBodydesc()).getAsJsonObject();
        idrequest = objet.get("IDREQUEST").getAsInt();
      }
      catch (Exception e) {
        Trace.erreur(e, "-TraitementRequeteSystemeMetier (analyseMessage)->");
      }
      
      // Choisit le traitement en fonction de la requête
      switch (idrequest) {
        case Constantes.ID_TEXTTITLEBARINFORMATIONS:
          traitementInfosComplementaireUtilisateur(objet);
          break;
      }
      reponse = mc.getMessageToSend();
    }
  }
  
  /**
   * Permet de récupérer les informations les informations complémentaire d'un utilisateur
   * @param objet
   * 
   */
  private void traitementInfosComplementaireUtilisateur(JsonObject objet) {
    UtilisateurETB_MAG userInfosETB_MAG = new UtilisateurETB_MAG(systemeM);
    // mc.createMessage(mc.getHead().getId(), true, userInfosETB_MAG.getTitleBarInfos(objet.get("PROFIL").getAsString(),
    // objet.get("CURLIB").getAsString()));
    mc.createMessage(true, userInfosETB_MAG.getTitleBarInfos(objet.get("PROFIL").getAsString(), objet.get("CURLIB").getAsString()));
  }
  
  /**
   * Ajoute les jars si nécessaire au classpath
   * @return
   */
  private boolean initJAR() {
    File[] listejar = new File(racineServeur + File.separatorChar + Constantes.DOSSIER_RT + File.separatorChar
        + sousEnvironnement.getFolder() + File.separatorChar + Constantes.DOSSIER_METIER).listFiles();
    if (listejar == null) {
      return false;
    }
    for (File jar : listejar) {
      chargeJar(jar.getAbsolutePath());
    }
    return true;
  }
  
  /**
   * Retourne la liste du classpath du classloader
   * @return
   */
  private URL[] getListURL() {
    // Récupère le ClassPath et cast en UrlClassLoader
    URLClassLoader sysLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    try {
      Method getURLs = URLClassLoader.class.getDeclaredMethod("getURLs");
      return (URL[]) getURLs.invoke(sysLoader);
    }
    catch (Exception e) {
      Trace.erreur(e, "-TraitementRequeteSystemeMetier (getListURL)->");
    }
    return null;
  }
  
  /**
   * Charge un jar dans la classloader si besoin
   * @param cheminjar
   */
  private void chargeJar(String cheminjar) {
    // Récupère le ClassPath et cast en UrlClassLoader
    URLClassLoader sysLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
    try {
      URL url = new File(cheminjar).toURI().toURL();
      
      if (listUrlClassloader != null) {
        for (URL elt : listUrlClassloader) {
          if (elt.sameFile(url)) {
            return;
          }
        }
      }
      
      // Ajoute le jar
      Method addURL = URLClassLoader.class.getDeclaredMethod("addURL", new Class<?>[] { URL.class });
      addURL.setAccessible(true); // Permet de mettre la methode addURL accessible car elle est protected
      addURL.invoke(sysLoader, new Object[] { url });
    }
    catch (Exception e) {
      Trace.erreur(e, "-TraitementRequeteSystemeMetier (chargeJar)->");
    }
  }
  
  /**
   * Libère les ressources
   */
  @Override
  public void dispose() {
    super.dispose();
    
    systemeM.deconnecter();
    systemeM = null;
    sousEnvironnement = null;
    listUrlClassloader = null;
  }
}
