/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.gvx.programs.parametres.GM_Personnalisation;
import ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires.RpgChargerArticleCommentaire;
import ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires.RpgSauverArticleCommentaire;
import ri.serien.libas400.dao.rpg.gescom.article.articlescommentaires.RpgSupprimerArticleCommentaire;
import ri.serien.libas400.dao.rpg.gescom.article.articlesenrupture.RpgChargerListeArticlesEnRupture;
import ri.serien.libas400.dao.rpg.gescom.article.articlespalettes.RpgChargerArticlesPalettes;
import ri.serien.libas400.dao.rpg.gescom.article.articlespecial.RpgModifierArticleSpecial;
import ri.serien.libas400.dao.rpg.gescom.article.articlespecial.RpgSauverArticleSpecial;
import ri.serien.libas400.dao.rpg.gescom.article.conditionachat.RpgChargerConditionAchat;
import ri.serien.libas400.dao.rpg.gescom.article.lot.RpgSauverLot;
import ri.serien.libas400.dao.rpg.gescom.article.standard.RpgChargerListeArticlesAchat;
import ri.serien.libas400.dao.rpg.gescom.article.standard.RpgChargerResultatRechercheArticle;
import ri.serien.libas400.dao.rpg.gescom.article.tarif.RpgChargerTarifArticle;
import ri.serien.libas400.dao.rpg.gescom.negociation.RpgSauverNegociationAchat;
import ri.serien.libas400.dao.sql.gescom.article.SqlArticle;
import ri.serien.libas400.dao.sql.gescom.article.SqlArticlesLies;
import ri.serien.libas400.dao.sql.gescom.article.SqlLot;
import ri.serien.libas400.dao.sql.gescom.article.SqlURLArticle;
import ri.serien.libas400.dao.sql.gescom.prixvente.ChargementPrixVente;
import ri.serien.libas400.dao.sql.gescom.prixvente.Testeur;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.ArticleBase;
import ri.serien.libcommun.gescom.commun.article.CritereArticle;
import ri.serien.libcommun.gescom.commun.article.CritereArticleCommentaire;
import ri.serien.libcommun.gescom.commun.article.CritereArticleLie;
import ri.serien.libcommun.gescom.commun.article.CriteresRechercheArticles;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticle;
import ri.serien.libcommun.gescom.commun.article.ListeArticleBase;
import ri.serien.libcommun.gescom.commun.article.ListeGroupesArticles;
import ri.serien.libcommun.gescom.commun.article.ListeMarques;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.article.TarifArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.CritereResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.IdResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticle.ListeResultatRechercheArticle;
import ri.serien.libcommun.gescom.commun.resultatrecherchearticlepalette.ListeResultatRechercheArticlePalette;
import ri.serien.libcommun.gescom.personnalisation.sousfamille.ListeSousFamille;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.lot.CritereLot;
import ri.serien.libcommun.gescom.vente.lot.IdLot;
import ri.serien.libcommun.gescom.vente.lot.ListeLot;
import ri.serien.libcommun.gescom.vente.prixvente.CalculPrixVente;
import ri.serien.libcommun.gescom.vente.prixvente.ParametreChargement;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceArticle;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service article.
 */
public class ServiceArticle implements InterfaceServiceArticle {
  private static InterfaceServiceArticle service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceArticle() {
  }
  
  /**
   * Retourner l'implémentation du service article.
   */
  protected static InterfaceServiceArticle getService() {
    if (service == null) {
      InterfaceServiceArticle interfaceService = new ServiceArticle();
      service = (InterfaceServiceArticle) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceArticle.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Lecture des données complètes d'un article
   */
  @Override
  public Article lireArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerArticle(pIdArticle);
  }
  
  /**
   * Rechercher la liste des identifiants articles correspondants aux critères de recherche.
   */
  @Override
  public List<IdArticle> chargerListeIdArticle(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerListeIdArticle(pCritereArticle);
  }
  
  /**
   * Charger un articlesBase à partir de son identifiant.
   * Seules les informations de base des articles sont récupérées.
   */
  @Override
  public ArticleBase chargerArticleBase(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerArticleBase(pIdArticle);
  }
  
  /**
   * Rechercher la liste des articles correspondants aux critères de recherche.
   * Seules les informations de base des articles sont récupérées.
   */
  @Override
  public ListeArticleBase chargerListeArticleBase(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerListeArticleBase(pCritereArticle);
  }
  
  /**
   * Rechercher la liste des articles à partir d'une liste d'identifiants.
   * Seules les informations de base des articles sont récupérées.
   */
  @Override
  public ListeArticleBase chargerListeArticleBase(IdSession pIdSession, List<IdArticle> pListeIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerListeArticleBaseParID(pListeIdArticle);
  }
  
  /**
   * Rechercher la liste des id articles en rupture de stock correspondants aux critères de recherche.
   */
  @Override
  public ListeArticleBase chargerListeIdArticlesEnRupture(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle sql = new SqlArticle(sessionServeur.getQueryManager());
    return sql.chargerListeIdArticlesEnRupture(pCritereArticle);
  }
  
  /**
   * Recherche les articles transports correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeArticle chargerListeArticleTransport(IdSession pIdSession, CriteresRechercheArticles pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    return operation.chargerListeArticleTransport(pCriteres);
  }
  
  /**
   * Recherche les articles commentaires correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeArticle chargerListeArticleCommentaire(IdSession pIdSession, CritereArticleCommentaire pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    return operation.chargerListeArticleCommentaire(pCritere);
  }
  
  /**
   * Recherche les articles palettes correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeResultatRechercheArticlePalette chargerListeResultatRechercheArticlePalette(IdSession pIdSession,
      CritereResultatRechercheArticlePalette pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerArticlesPalettes traitement = new RpgChargerArticlesPalettes();
    return traitement.chargerListeResultatRechercheArticlePalette(sessionServeur.getSystemManager(), pCritere);
  }
  
  /**
   * Recherche les articles palettes correspondants aux critères passés en paramètre pour un fournisseur.
   */
  @Override
  public ListeArticle chargerListeArticlePaletteFournisseur(IdSession pIdSession, CritereArticle pCritereArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    return operation.chargerListeArticlePalette(pCritereArticle);
  }
  
  /**
   * Charger les informations affichées à l'utilisateur lors d'une recherche d'articles pour une vente.
   */
  @Override
  public ListeResultatRechercheArticle chargerListeResultatRechercheArticle(IdSession pIdSession,
      List<IdResultatRechercheArticle> pListeIdResultatRechercheArticle, ParametresLireArticle pParametres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerResultatRechercheArticle rpg = new RpgChargerResultatRechercheArticle(pIdSession);
    return rpg.chargerListeResultatRechercheArticle(sessionServeur.getSystemManager(), pListeIdResultatRechercheArticle, pParametres);
  }
  
  /**
   * Charger les informations affichées à l'utilisateur lors d'une recherche d'articles pour une vente.
   */
  @Override
  public ListeArticle chargerListeInformationAchat(IdSession pIdSession, List<IdArticle> pListeIdArticle,
      ParametresLireArticle pParametres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerListeArticlesAchat rpg = new RpgChargerListeArticlesAchat();
    return rpg.chargerListeInformationAchat(sessionServeur.getSystemManager(), pListeIdArticle, pParametres);
  }
  
  /**
   * Rechercher la liste des articles en rupture correspondants aux critères de recherche.
   */
  @Override
  public ListeArticle chargerListeArticlesEnRupture(IdSession pIdSession, List<IdArticle> pListeIdArticle,
      ParametresLireArticle pParametres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerListeArticlesEnRupture rpg = new RpgChargerListeArticlesEnRupture();
    return rpg.chargerListeArticlesEnRupture(sessionServeur.getSystemManager(), pListeIdArticle, pParametres);
  }
  
  /**
   * Lit les données d'un article à partir d'un objet article instancié.
   */
  @Override
  public Article chargerArticle(IdSession pIdSession, Article pArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    return operation.chargerArticle(pArticle);
  }
  
  /**
   * Vérifie si un article existe dans la base
   */
  @Override
  public boolean verifierExistenceArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    return operation.verifierExistenceArticle(pIdArticle);
  }
  
  /**
   * Lit les tarifs de l'article demandé à partir de son code.
   * On peut préciser si on veut :
   * - le tarif pour une devise donnée (sinon euro)
   * - le tarif à une date donnée (sinon tarif en cours)
   * - le tarif TTC (sinon HT)
   */
  @Override
  public TarifArticle chargerTarifArticle(IdSession pIdSession, IdArticle pIdArticle, String pCodeDevise, Date pDateTarif, boolean isTTC)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerTarifArticle traitement = new RpgChargerTarifArticle();
    return traitement.chargerTarifArticle(sessionServeur.getSystemManager(), pIdArticle, pCodeDevise, pDateTarif, isTTC);
  }
  
  /**
   * Lit les URL attachées à un article
   */
  @Override
  public String[] chargerListeURLArticle(IdSession pIdSession, Article pArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlURLArticle operation = new SqlURLArticle(sessionServeur.getQueryManager());
    return operation.chargerListeURLArticle(pArticle);
  }
  
  /**
   * Lit une condition d'achat pour un article à la date précisée.
   * Si la date est à null on cherchera la condition en cours.
   */
  @Override
  public ConditionAchat chargerConditionAchat(IdSession pIdSession, IdArticle pIdArticle, Date pDate) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerConditionAchat traitement = new RpgChargerConditionAchat();
    return traitement.chargerConditionAchat(sessionServeur.getSystemManager(), pIdArticle, pDate);
  }
  
  /**
   * retourner la liste de groupes de familles du catalogue article d'un établissement
   */
  @Override
  public ListeGroupesArticles chargerListeGroupesArticles(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    GM_Personnalisation personnalisation = new GM_Personnalisation(queryManager);
    return personnalisation.chargerListeGroupesArticles(pIdEtablissement);
  }
  
  /**
   * retourner la liste de sous-familles du catalogue article d'un établissement
   */
  @Override
  public ListeSousFamille chargerListeSousFamille(IdSession pIdSession, IdEtablissement pIdEtablissement, String pFamille)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    GM_Personnalisation personnalisation = new GM_Personnalisation(queryManager);
    return personnalisation.chargerListeSousFamille(pIdEtablissement, pFamille);
  }
  
  /**
   * Retourner une liste distincte des marques définies dans le catalogue article de Série N
   */
  @Override
  public ListeMarques chargerListeMarques(IdSession pIdSession, IdEtablissement pIdEtablissement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    return sqlArticle.chargerListeMarques(pIdEtablissement);
  }
  
  /**
   * Retourner le montant DEEE d'un article Série N
   */
  @Override
  public BigDecimal chargerMontantDEEEArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    return sqlArticle.chargerMontantDEEEArticle(pIdArticle);
  }
  
  /**
   * Charge le bloc notes de la condition d'achat
   */
  @Override
  public BlocNote chargerBlocNoteConditionAchat(IdSession pIdSession, IdArticle pIdArticle, IdFournisseur pIdFournisseur)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    return sqlArticle.chargerBlocNoteConditionAchat(pIdArticle, pIdFournisseur);
  }
  
  /**
   * Lire la référence fabricant d'un article Série N
   */
  @Override
  public String chargerTexteReferenceFabricant(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    return sqlArticle.chargerTexteReferenceFabricant(pIdArticle);
  }
  
  /**
   * Lire le prix fabricant d'un article Série N
   */
  @Override
  public BigDecimal chargerPrixFabricantArticle(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager queryManager = sessionServeur.getQueryManager();
    SqlArticle sqlArticle = new SqlArticle(queryManager);
    return sqlArticle.chargerPrixFabricantArticle(pIdArticle);
  }
  
  /**
   * ecrit un article spécial en base et renvoi le code article généré
   */
  @Override
  public Article sauverArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT, ConditionAchat pCNA)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverArticleSpecial traitement = new RpgSauverArticleSpecial();
    return traitement.sauverArticleSpecial(sessionServeur.getSystemManager(), pArticle, pClient, pPrixNetHT, pCNA);
  }
  
  /**
   * modifie un article spécial en base
   * 
   * il faudrait à terme fusionner avec le service du dessus !
   */
  @Override
  public void modifierArticleSpecial(IdSession pIdSession, Article pArticle, Client pClient, BigDecimal pPrixNetHT, ConditionAchat pCNA)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgModifierArticleSpecial traitement = new RpgModifierArticleSpecial();
    traitement.modifierArticleSpecial(sessionServeur.getSystemManager(), pArticle, pClient, pPrixNetHT, pCNA);
  }
  
  /**
   * Recherche les articles correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeArticle chargerListeArticleLie(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticlesLies operation = new SqlArticlesLies(sessionServeur.getQueryManager());
    return operation.chargerListeArticleLie(pCritere);
  }
  
  /**
   * Recherche les articles liés proposés correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeArticle chargerListeArticleLieProposes(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticlesLies operation = new SqlArticlesLies(sessionServeur.getQueryManager());
    return operation.chargerListeArticleLieProposes(pCritere);
  }
  
  /**
   * Recherche les articles liés proposés correspondants aux critères passés en paramètre.
   */
  @Override
  public ListeArticle chargerListeArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticlesLies operation = new SqlArticlesLies(sessionServeur.getQueryManager());
    return operation.chargerListeArticleLieAutomatiquement(pCritere);
  }
  
  /**
   * Vérifie si au moins un article est lié à un article donné
   */
  @Override
  public boolean verifierExistenceArticleLiePropose(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticlesLies operation = new SqlArticlesLies(sessionServeur.getQueryManager());
    return operation.verifierExistenceArticleLiePropose(pCritere);
  }
  
  /**
   * Vérifie si au moins un article est lié à un article donné
   */
  @Override
  public boolean verifierExistenceArticleLieAutomatiquement(IdSession pIdSession, CritereArticleLie pCritere) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticlesLies operation = new SqlArticlesLies(sessionServeur.getQueryManager());
    return operation.verifierExistenceArticleLieAutomatiquement(pCritere);
  }
  
  @Override
  public void sauverTopMemoObligatoire(IdSession pIdSession, Article aArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlArticle operation = new SqlArticle(sessionServeur.getQueryManager());
    operation.sauverTopMemoObligatoire(aArticle);
  }
  
  /**
   * Ecrit ou modifie un article commentaire
   */
  @Override
  public void sauverArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverArticleCommentaire traitement = new RpgSauverArticleCommentaire();
    traitement.sauverArticleCommentaire(sessionServeur.getSystemManager(), pArticle);
  }
  
  /**
   * Lit les données d'un article commentaire à partir d'un objet article instancié.
   */
  @Override
  public Article chargerArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerArticleCommentaire traitement = new RpgChargerArticleCommentaire();
    return traitement.chargerArticleCommentaire(sessionServeur.getSystemManager(), pArticle);
  }
  
  /**
   * Supprimer un article commentaire de la base à partir d'un objet article instancié.
   */
  @Override
  public void supprimerArticleCommentaire(IdSession pIdSession, Article pArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSupprimerArticleCommentaire traitement = new RpgSupprimerArticleCommentaire();
    traitement.supprimerArticleCommentaire(sessionServeur.getSystemManager(), pArticle);
  }
  
  /**
   * Sauvegarder une négociation achat.
   */
  @Override
  public void sauverNegociationAchat(IdSession pIdSession, ConditionAchat pCNA) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverNegociationAchat operation = new RpgSauverNegociationAchat();
    operation.sauverNegociationAchat(sessionServeur.getSystemManager(), pCNA);
  }
  
  /**
   * Calcule un prix de vente.
   */
  @Override
  public CalculPrixVente calculerPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    QueryManager queryManager = sessionServeur.getQueryManager();
    // Chargement des paramètres nécessaires au calcul
    ChargementPrixVente chargementPrixVente = new ChargementPrixVente(systemeManager, queryManager, pParametreChargement);
    ParametreChargement parametreChargement = chargementPrixVente.charger();
    if (parametreChargement == null) {
      return null;
    }
    // Le calcul à partir des données qui viennent d'être chargées en base
    CalculPrixVente calculPrixVente = new CalculPrixVente();
    calculPrixVente.calculer(parametreChargement);
    return calculPrixVente;
  }
  
  /**
   * Charger les paramètres pour le calcul du prix de vente.
   */
  @Override
  public ParametreChargement chargerParametreCalculPrixVente(IdSession pIdSession, ParametreChargement pParametreChargement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    QueryManager queryManager = sessionServeur.getQueryManager();
    // Chargement des paramètres nécessaires au calcul
    ChargementPrixVente chargementPrixVente = new ChargementPrixVente(systemeManager, queryManager, pParametreChargement);
    return chargementPrixVente.charger();
  }
  
  /**
   * Comparer le calcul du prix de vente en Java avec celui en RPG.
   */
  @Override
  public void testerCalculPrixVente(IdSession pIdSession, CritereLigneVente pCritereLigneVente, String pCheminRacine,
      int pNombreCalculMax) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    QueryManager queryManager = sessionServeur.getQueryManager();
    
    if (pCheminRacine == null) {
      pCheminRacine = ManagerAS400.getDossierRacineServeur();
    }
    
    Testeur testeur = new Testeur(systemeManager, queryManager);
    testeur.executer(pCritereLigneVente, pCheminRacine, pNombreCalculMax);
  }
  
  /**
   * Charger la liste des lots à partir d'une liste d'identifiants
   */
  @Override
  public ListeLot chargerListeLot(IdSession pIdSession, List<IdLot> pListeIdLot) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLot sqlLot = new SqlLot(querym);
    return sqlLot.chargerListeLot(pListeIdLot);
  }
  
  /**
   * Charger la liste des lots à partir de critères
   */
  @Override
  public ListeLot chargerListeLot(IdSession pIdSession, CritereLot pCritereLot) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLot sqlLot = new SqlLot(querym);
    return sqlLot.chargerListeLot(pCritereLot);
  }
  
  /**
   * Charger la liste des lots saisis sur un document
   */
  @Override
  public ListeLot chargerListeLotDocument(IdSession pIdSession, CritereLot pCritereLot) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLot sqlLot = new SqlLot(querym);
    return sqlLot.chargerListeLotDocument(pCritereLot);
  }
  
  /**
   * Sauver les quantités sur lot
   */
  @Override
  public IdLigneVente sauverListeLot(IdSession pIdSession, IdLigneVente pIdLigneVente, BigDecimal pQuantiteArticle,
      BigDecimal pQuantiteTotale, ListeLot pListeLot) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverLot traitement = new RpgSauverLot();
    return traitement.sauverListeLot(sessionServeur.getSystemManager(), pIdLigneVente, pQuantiteArticle, pQuantiteTotale, pListeLot);
  }
}
