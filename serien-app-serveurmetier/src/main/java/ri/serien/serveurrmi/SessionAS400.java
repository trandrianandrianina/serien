/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.AS400Message;
import com.ibm.as400.access.CommandCall;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.Job;
import com.ibm.as400.access.MessageQueue;
import com.ibm.as400.access.QSYSObjectPathName;
import com.ibm.as400.access.QueuedMessage;

import ri.serien.libas400.system.BaseProgram;
import ri.serien.libas400.system.DataQ;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.session.EnvProgram;
import ri.serien.libcommun.outils.session.IdSession;

/**
 * Session AS400.
 */
class SessionAS400 extends Thread {
  // Variables
  private boolean modeDebug = false;
  private AS400 systeme = null;
  private EnvProgram infoPrg = null;
  private String infosJob = null;
  private Thread threadSurveillanceJob = null;
  private Job jobSoumis = null;
  private ListeSousEnvironnement listeSousEnvironnement = null;
  private MessageQueue messageQueue = null;
  private QueuedMessage queuedMessageEncours = null;
  private DataQ dataQJavaVersRPG = null;
  private DataQ dataQRPGVersJava = null;
  
  public String nomcomplet;
  public String langue;
  public int actif = Constantes.FALSE; // Le profil peut être utilisé
  public boolean jobActif = false; // Le profil a son GAP lancé (donc est en train d'etre utilisé)
  public int finTraitement = Constantes.FALSE;
  
  private IdSession idSession = null;
  
  /**
   * Constructeur
   */
  public SessionAS400(AS400 sys, EnvProgram infos, int delaitimeout, MessageQueue msgQ, IdSession pIdSession) {
    super("SessionAS400-id=" + pIdSession);
    systeme = sys;
    infoPrg = infos;
    messageQueue = msgQ;
    idSession = pIdSession;
  }
  
  // -- Méthodes privées
  
  /**
   * Construction de la commande à lancer sur l'I5
   * @param soumission
   * @return
   */
  private String contruireCommande(boolean soumission) {
    int i = 0;
    String commande = null;
    final StringBuffer sbmjob = new StringBuffer();
    
    // Contrôle des library qui ne doivent pas être nulles (à supprimer/ou améliorer le jour où tout sera centralisé sur le serveur)
    i = 0;
    while (i < infoPrg.getListeBib().size()) {
      if (infoPrg.getListeBib().get(i).equals("null")) {
        infoPrg.getListeBib().remove(i);
      }
      i++;
    }
    if (infoPrg.getEnvUser().getBibliothequeEnvironnement() == null) {
      infoPrg.getEnvUser().setBibliothequeEnvironnement(infoPrg.getEnvUser().getBibliothequeEnvironnementClient());
    }
    else if (infoPrg.getEnvUser().getBibliothequeEnvironnementClient() == null) {
      infoPrg.getEnvUser().setBibliothequeEnvironnementClient(infoPrg.getEnvUser().getBibliothequeEnvironnement());
    }
    
    // On vérifie si l'appel vient du menu (si c'est le cas pas de programme renseigné)
    if (infoPrg.getProgram().trim().equals("")) {
      commande = "CALL (" + infoPrg.getEnvUser().getBibliothequeEnvironnementClient() + "/" + infoPrg.getEnvUser().getProgram()
          + ") PARM(" + infoPrg.getEnvUser().getBibliothequeEnvironnementClient() + ")";
    }
    else {
      if (infoPrg.getLibrary().trim().equals("")) {
        commande = "CALL (" + infoPrg.getProgram() + ")";
      }
      else {
        commande = "CALL (" + infoPrg.getLibrary() + "/" + infoPrg.getProgram() + ")";
      }
      // S'il y a des paramètres
      if (infoPrg.getListeParam() != null) {
        commande = commande + " PARM(";
        for (i = 0; i < infoPrg.getListeParam().size(); i++) {
          commande = commande + infoPrg.getListeParam().get(i) + " ";
        }
        commande = commande.trim() + ")";
      }
    }
    
    // Modification pour la nouvelle méthode avec lanceur, calcul de la longueur des paramètres du programmes RPG
    int len = commande.length();
    // Préparation des paramètres
    char[] tablignecmd = new char[1536]; // 1536 longueur de la variable LIGNECMD dans le programme SERIEN de SEXPAS
    Arrays.fill(tablignecmd, ' ');
    String lignecmd = new String(tablignecmd);
    commande = "'" + commande.replaceAll("'", "''") + lignecmd.substring(0, 1536 - len) + "'";
    String programme = infoPrg.getProgram().trim() + new String(Constantes.spaces, 0, (10 - infoPrg.getProgram().trim().length()));
    String pointmenu = infoPrg.getPointMenu().trim() + new String(Constantes.spaces, 0, (10 - infoPrg.getPointMenu().trim().length()));
    // Cette dtaara fera 1024 au total
    String ldaserien = String.format("'%-10s%-1014s'", infoPrg.getEnvUser().getDeviceImpression(), "");
    commande = "CALL PGM(SERIEN) PARM(" + commande + " '" + String.format("%04d", len) + "' '" + programme + "' '" + pointmenu + "' "
        + ldaserien + ")";
    
    // Si l'on souhaite soumettre la commande
    if (soumission) {
      // Ajout des paramètres propre au profil
      sbmjob.append("SBMJOB CMD(").append(commande);
      sbmjob.append(") JOB(").append(infoPrg.getEnvUser().getDevice());
      sbmjob.append(") JOBQ(").append(infoPrg.getEnvUser().getBibliothequeEnvironnementClient()).append("/")
          .append(infoPrg.getEnvUser().getNomSousSysteme());
      if (infoPrg.getEnvUser().getCurlib() != null) {
        sbmjob.append(") CURLIB(").append(infoPrg.getEnvUser().getCurlib().getNom());
      }
      // Ajout de la liste des bib à mettre en ligne si besoin
      if (infoPrg.getListeBib() != null) {
        sbmjob.append(") INLLIBL(");
        for (i = 0; i < infoPrg.getListeBib().size(); i++) {
          sbmjob.append(infoPrg.getListeBib().get(i)).append(" ");
        }
      }
      sbmjob.append(")");
      return sbmjob.toString();
    }
    
    return commande;
  }
  
  /**
   * Création des 2 DataQ avec les informations détenues par l'utilisateur.
   */
  private boolean creationDataQueue() {
    if (infoPrg == null || infoPrg.getEnvUser() == null || infoPrg.getEnvUser().getBibliothequeEnvironnementClient() == null) {
      Trace.erreur("Création des dataqueues impossible car les informations sont invalides.");
    }
    
    // Création des dataqueues
    String nomBibliotheque = infoPrg.getEnvUser().getBibliothequeEnvironnementClient().getNom();
    final String DataQJR = QSYSObjectPathName.toPath(nomBibliotheque, infoPrg.getEnvUser().getDevice() + "R", "DTAQ");
    final String DataQRJ = QSYSObjectPathName.toPath(nomBibliotheque, infoPrg.getEnvUser().getDevice() + "S", "DTAQ");
    try {
      dataQJavaVersRPG = new DataQ(systeme, DataQJR, Constantes.DATAQLONG, "DataQueue pour Série N", true);
      Trace.info("Connexion à la dataqueue : " + DataQJR);
      dataQRPGVersJava = new DataQ(systeme, DataQRJ, Constantes.DATAQLONG, "DataQueue pour Série N", true);
      Trace.info("Connexion à la dataqueue : " + DataQRJ);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la connexion à la dataqueue : ");
      return false;
    }
    
    return true;
  }
  
  /**
   * Ferme les dataqueues.
   */
  private void fermerDataQueue() {
    if (dataQJavaVersRPG != null) {
      // Déconnexion et suppression de la dataqueue
      dataQJavaVersRPG.close();
      try {
        Trace.info("Suppression de la dataqueue " + dataQJavaVersRPG.getPath());
        if (dataQJavaVersRPG.exists()) {
          dataQJavaVersRPG.delete();
        }
      }
      catch (Exception e) {
        String message = e.getMessage();
        // Tester si c'est le fait d'interrompre le thread qui lève l'exception
        if (message != null && !message.equals("java.lang.InterruptedException")) {
          Trace.erreur(e, "Erreur lors de la suppression de la dataqueue " + dataQJavaVersRPG.getName());
        }
      }
    }
    
    if (dataQRPGVersJava != null) {
      // Déconnexion et suppression de la dataqueue
      dataQRPGVersJava.close();
      try {
        Trace.info("Suppression de la dataqueue " + dataQRPGVersJava.getPath());
        if (dataQRPGVersJava.exists()) {
          dataQRPGVersJava.delete();
        }
      }
      catch (Exception e) {
        String message = e.getMessage();
        // Tester si c'est le fait d'interrompre le thread qui lève l'exception
        if (message != null && !message.equals("java.lang.InterruptedException")) {
          Trace.erreur(e, "Erreur lors de la suppression de la dataqueue " + dataQRPGVersJava.getName());
        }
      }
    }
    
    dataQJavaVersRPG = null;
    dataQRPGVersJava = null;
    
    // Trace que la session de l'utilisateur est terminée
    if (infoPrg != null && infoPrg.getEnvUser() != null && infoPrg.getEnvUser().getDevice() != null) {
      Trace.info(infoPrg.getEnvUser().getDevice() + " terminée.");
    }
  }
  
  /**
   * Initialise le job.
   * 
   * @param jobSoumis the jobSoumis to set
   */
  private void setJobSoumis(String jobinfos) {
    final String[] infos = Constantes.splitString(infosJob, '/');
    jobSoumis = new Job(systeme, infos[2], infos[1], infos[0]);
    
    // Lancer la thread de surveillance du job
    threadSurveillanceJob = new ThreadSurveillanceJob(infoPrg.getEnvUser().getDevice(), infoPrg.getProgram());
    threadSurveillanceJob.start();
  }
  
  /**
   * Lecture de données sur la dataqueue.
   */
  private String retournerMessageLu(DataQ pDataQueue, int pDelaiAttente) {
    Trace.debug(SessionAS400.class, "retournerMessageLu",
        "Attente d'un message venant de la DTA" + dataQRPGVersJava.getName() + " pendant " + pDelaiAttente + " sec");
    String message = dataQRPGVersJava.readString(pDelaiAttente);
    switch (pDataQueue.getEtatLecture()) {
      case DataQ.ETAT_LECTURE_DONNEES_LUES:
        message = Constantes.normerTexte(message);
        // Traitement message arrivé depuis la DTAQ (on évite d'écrire les menus car ca prend trop de place)
        Trace.info("Device=" + infoPrg.getEnvUser().getDevice() + ", Données lues, Taille=" + message.length() + " <- " + message);
        break;
      case DataQ.ETAT_LECTURE_DELAI_ATTENTE_ATTEINT:
        message = Constantes.RPGMSG_DELAI_ATTENTE_EXPIRE;
        Trace.info(
            "Device=" + infoPrg.getEnvUser().getDevice() + ", Délai d'attente atteint, Taille=" + message.length() + " <- " + message);
        break;
      case DataQ.ETAT_LECTURE_ERREUR:
        Trace.info("Device=" + infoPrg.getEnvUser().getDevice() + ", Erreur lecture, Taille=null <- null");
        break;
      default:
        Trace.info("Device=" + infoPrg.getEnvUser().getDevice() + ", Erreur inconnue (default), Taille=null <- null");
        break;
    }
    return message;
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialisation du nom & connexion aux DataQueues.
   */
  public boolean demarre() {
    // Contrôle des données
    if (infoPrg == null) {
      throw new MessageErreurException("Les informations du programme sont invalides.");
    }
    if (infoPrg.getEnvUser() == null) {
      throw new MessageErreurException("Les informations de l'utilisateur sont invalides.");
    }
    if (infoPrg.getEnvUser().getBibliothequeEnvironnementClient() == null) {
      throw new MessageErreurException("La bibliothèque d'environnement du client est invalide.");
    }
    
    // Vérifie que la bibliothèque de travail existe bien (stockage des DTAQs)
    final IFSFile bibliothequeEnvironnementClient =
        new IFSFile(systeme, infoPrg.getEnvUser().getBibliothequeEnvironnementClient().getCheminIFS());
    try {
      // Dans le cas contraire, la bilbiothèque est créée
      if (!bibliothequeEnvironnementClient.exists()) {
        bibliothequeEnvironnementClient.mkdir();
      }
    }
    catch (IOException ioe) {
      Trace.erreur(ioe, "-SessionAS400 (demarre)-> ");
      return false;
    }
    
    // Lancement du Programme en soumission
    if (creationDataQueue()) {
      switch (infoPrg.getType()) {
        // Programme RPG ou CL
        case EnvProgram.AS400:
          if (lancePrgAS400(contruireCommande(true))) {
            jobActif = true;
            if (!isAlive()) {
              start();
            }
            return true;
          }
          break;
        // Programme java
        case EnvProgram.JAVA:
          if (lancePrgJava(infoPrg.getProgram())) {
            jobActif = true;
            start();
            return true;
          }
          break;
      }
    }
    
    return false;
  }
  
  /**
   * Lance une commande AS400.
   * 
   * @param pCommande
   * @return
   */
  public boolean lancePrgAS400(String pCommande) {
    final CommandCall cmd = new CommandCall(systeme);
    cmd.setThreadSafe(true);
    Trace.info("Lancement RPG :" + pCommande);
    
    try {
      if (cmd.run(pCommande) != true) {
        // Affiche les messages si problème
        final StringBuffer message = new StringBuffer();
        message.append(Constantes.TYPE_ERR_SIMPLE);
        final AS400Message[] messageList = cmd.getMessageList();
        Trace.erreur("Erreur lors du lancement de la commande");
        for (int i = 0; i < messageList.length; ++i) {
          // Montre chaque message
          Trace.erreur(messageList[i].getText());
          // On récupère les infos du message d'erreur et on les envoi au client
          message.append(Constantes.SEPARATEUR_CHAINE_CHAR).append(messageList[i].getText());
        }
        message.append(Constantes.SEPARATEUR_CHAINE_CHAR).append(pCommande);
        return false;
      }
      // Recherche le nom/numéro du job soumis
      final AS400Message[] messageList = cmd.getMessageList();
      for (int i = 0; i < messageList.length; i++) {
        if (messageList[i].getID().equals("CPC1221")) {
          infosJob = messageList[i].getText();
          infosJob = infosJob.substring(infosJob.indexOf(' ')).trim();
          infosJob = infosJob.substring(0, infosJob.indexOf(' '));
          setJobSoumis(infosJob);
          break;
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du lancement de la commande");
      return false;
    }
    
    Trace.info("Fin du lancement RPG");
    return true;
  }
  
  /**
   * Lance un programme java (ici le MenuPrincipal à titre d'exemple).
   * 
   * @param commande
   * @return
   */
  public boolean lancePrgJava(String pFichierjava) {
    Trace.info("Lancement JAR : " + pFichierjava);
    Trace.debugMemoire(SessionAS400.class, "lancePrgJava");
    try {
      // Contrôle des paramètres
      if (infoPrg.getEnvUser().getBibliothequeEnvironnement() == null) {
        infoPrg.getEnvUser().setBibliothequeEnvironnement(infoPrg.getEnvUser().getBibliothequeEnvironnementClient());
      }
      else if (infoPrg.getEnvUser().getBibliothequeEnvironnementClient() == null) {
        infoPrg.getEnvUser().setBibliothequeEnvironnementClient(infoPrg.getEnvUser().getBibliothequeEnvironnement());
      }
      
      // Préparation des paramètres
      HashMap<String, Object> listeParametres = new HashMap<String, Object>();
      listeParametres.put("PROFIL", infoPrg.getEnvUser().getProfil());
      listeParametres.put("DEVICE", infoPrg.getEnvUser().getDevice());
      listeParametres.put("BIBENV", infoPrg.getEnvUser().getBibliothequeEnvironnement());
      listeParametres.put("LETASP", "" + infoPrg.getEnvUser().getLetter());
      listeParametres.put("BIBJOB", infoPrg.getEnvUser().getBibliothequeEnvironnementClient());
      String typemenu = listeSousEnvironnement.getSousEnvironnement(infoPrg.getEnvUser().getIdSousEnvironnement()).getTypeMenu();
      if (typemenu != null) {
        listeParametres.put("TYPEMENU", typemenu);
      }
      
      // Préparation du classloader
      Class<?> classe = Class.forName(pFichierjava, true, this.getClass().getClassLoader());
      // Récupération du constructeur prenant en paramètre une ArrayList (ainsi on est pas limité en nombre de paramètres)
      Constructor<?> constructeur = classe.getConstructor(new Class[] { Class.forName("java.util.HashMap") });
      BaseProgram obj = (ri.serien.libas400.system.BaseProgram) constructeur.newInstance(new Object[] { listeParametres });
      obj.treatment();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du lancement du fichier jar");
      return false;
    }
    
    Trace.info("Fin du lancement JAR : " + pFichierjava);
    Trace.debugMemoire(SessionAS400.class, "lancePrgJava");
    return true;
  }
  
  /**
   * Envoi un message à un programme et attend sa réponse.
   * 
   * @throws
   */
  public String dialoguerAvecProgramme(String messageaenvoyer, int waitsec) {
    String message = null;
    try {
      // Si le message est null on passe direct en attente de réponse du programme
      if (messageaenvoyer != null) {
        Trace.info(
            "Device=" + infoPrg.getEnvUser().getDevice() + ", Taille=" + messageaenvoyer.length() + " -> " + messageaenvoyer.trim());
        if (!dataQJavaVersRPG.writeString(messageaenvoyer, Constantes.DATAQLONG)) {
          Trace.erreur("Problème lors de l'envoi du message dans dataqueue : " + "device=" + infoPrg.getEnvUser().getDevice());
          return null;
        }
      }
      
      // Lecture blocante
      if ((jobActif) && (dataQRPGVersJava.exists())) {
        message = retournerMessageLu(dataQRPGVersJava, waitsec);
      }
      else {
        Trace.info("Pas de programme RPG actif : device=" + infoPrg.getEnvUser().getDevice());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'écoute de la dataqueue : device=" + infoPrg.getEnvUser().getDevice());
      terminerSession(false);
    }
    
    return message;
  }
  
  /**
   * Envoi un message vers le programme RPG via la Dataqueue.
   * 
   * @param pMessage
   * @return
   */
  public boolean envoyerMessageAuProgramme(String pMessage) {
    if (pMessage != null) {
      Trace.info("Device=" + infoPrg.getEnvUser().getDevice() + ", Taille=" + pMessage.length() + " -> " + pMessage.trim());
      
      if (!dataQJavaVersRPG.writeString(pMessage, Constantes.DATAQLONG)) {
        Trace.erreur("Problème lors de l'envoi du message dans dataqueue : device=" + infoPrg.getEnvUser().getDevice());
        return false;
      }
      else {
        return true;
      }
    }
    return false;
  }
  
  /**
   * Reçoit un buffer du programme RPG via la Dataqueue.
   * La lecture est bloquante pendant le temps passé en paramètres.
   * 
   * @param pTempsAttenteEnSec
   * @return
   */
  public String recevoirMessageDuProgramme(int pTempsAttenteEnSec) {
    String message = null;
    try {
      // Lecture blocante
      if ((jobActif) && (dataQRPGVersJava.exists())) {
        message = retournerMessageLu(dataQRPGVersJava, pTempsAttenteEnSec);
      }
      else {
        Trace.info("Pas de programme RPG actif : Device=" + infoPrg.getEnvUser().getDevice());
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'écoute de la dataqueue : device=" + infoPrg.getEnvUser().getDevice());
      terminerSession(false);
    }
    return message;
  }
  
  /**
   * Termine la connexion avec les DTAQs.
   */
  public void terminerSession(boolean pForceFermeturePanel) {
    // On arrête la surveillance du job si nécessaire
    if (threadSurveillanceJob != null && threadSurveillanceJob.isAlive()) {
      threadSurveillanceJob.interrupt();
      threadSurveillanceJob = null;
    }
    // On ferme les dataqueue qui communique avec le programme RPG
    fermerDataQueue();
    // A supprimer à court terme
    dispose();
  }
  
  /**
   * Répond au message en attente.
   */
  public void replyQueuedMessage(String pReponse) {
    if (queuedMessageEncours == null) {
      return;
    }
    QueuedMessage lstmsg = null;
    
    try {
      // On trie la liste des messages par ordre descendant et uniquement sur ceux qui attendent une réponse
      messageQueue.setSelectMessagesNeedReply(true);
      messageQueue.setListDirection(false);
      messageQueue.load();
      // On recherche le message dans la message queue pour pouvoir y répondre
      for (Enumeration<?> e = messageQueue.getMessages(); e.hasMoreElements();) {
        lstmsg = (QueuedMessage) e.nextElement();
        if (lstmsg.getID().equals(queuedMessageEncours.getID()) && lstmsg.getFromJobNumber().equals(jobSoumis.getNumber())
            && lstmsg.getFromJobName().equals(jobSoumis.getName()) && lstmsg.getUser().equals(jobSoumis.getUser())) {
          messageQueue.reply(lstmsg.getKey(), pReponse, false);
          queuedMessageEncours.load();
          queuedMessageEncours = null;
          break;
        }
      }
    }
    catch (Exception e) {
      Trace.erreur("[SessionAS400] Reponse CPF : " + pReponse + e);
    }
  }
  
  /**
   * Libère les ressources.
   * TODO à supprimer
   */
  private void dispose() {
    systeme = null;
    infoPrg = null;
    threadSurveillanceJob = null;
    jobSoumis = null;
    listeSousEnvironnement = null;
    dataQJavaVersRPG = null;
    dataQRPGVersJava = null;
    messageQueue = null;
    queuedMessageEncours = null;
  }
  
  // -- Accesseurs
  
  /**
   * Retourne la liste des sous environnements.
   * 
   * @return
   */
  public ListeSousEnvironnement getListeSousEnvironnement() {
    return listeSousEnvironnement;
  }
  
  /**
   * Initialise la liste des sous environnements.
   * 
   * @param pListeSousEnvironnement
   */
  public void setListeSousEnvironnement(ListeSousEnvironnement pListeSousEnvironnement) {
    listeSousEnvironnement = pListeSousEnvironnement;
  }
  
  /**
   * Active le mode débug.
   * 
   * @param pModeDebug
   */
  public void setDebug(boolean pModeDebug) {
    modeDebug = pModeDebug;
  }
  
  /**
   * Surveillance du job.
   * 
   * Cette thread vérifie à intervalle régulier si le job est toujours actif sur le serveur IBM.
   * Elle vérifie également la présence de messages ou d'un lock pour en informer le client.
   * 
   * Le job surveillé commencé par lancé le programme (RPG ou CL) qui a été appelé par le point de menu. Ce programme peut lui-même
   * appeler d'autres programmes si bien que le job va passer d'un programme à l'autre. On mémorise le premier programme lancé dans la
   * thread de surveillance afin de pourvoir faire le lien entre la thread de surveillance et le point de menu lancé.
   */
  private class ThreadSurveillanceJob extends Thread {
    private final long DELAI_ENTRE_VERIFICATION = 2000;
    private final String device;
    private final String nomProgramme;
    private String statutPrecedent = "";
    
    /**
     * Constructeur.
     * 
     * @param pDevice Device associé à la session.
     * @param pNomProgramme Nom du programme RPG ou CL lancé par le point de menu.
     */
    public ThreadSurveillanceJob(String pDevice, String pNomProgramme) {
      super("SurveillanceJob " + infosJob);
      device = pDevice;
      nomProgramme = pNomProgramme;
    }
    
    @Override
    public void run() {
      // Tracer le début de la surveillance du job
      Trace.info("Démarrage de la thread de surveillance du job");
      
      // Capturer toutes les exceptions générées dans la thread afin de les afficher dans les traces
      try {
        // Vérifier la présence du job tant que c'est souhaité et tant que le job est présent
        while (!isInterrupted() && jobActif) {
          // Attendre le délai souhaité entre deux vérifications
          try {
            Thread.sleep(DELAI_ENTRE_VERIFICATION);
          }
          catch (InterruptedException e) {
            // Tracer la cause d'arrêt de la surveillance du job
            Trace.info("Une demande d'arrêt de thread de surveillance du job a été reçue : PointDeMenu=" + nomProgramme);
            
            // Activer le flag d'interruption de la thread
            Thread.currentThread().interrupt();
          }
          
          // Vérifier le job (sauf si la thread a été interrompue)
          if (!isInterrupted()) {
            verifierJob();
          }
        }
        
        // Notififier le client que le job est arrété
        envoyerMessageVersClient(Constantes.RPGMSG_JOBENDED);
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur dans la thread de surveillance du job");
      }
      finally {
        // Tracer la fin de la surveillance du job
        Trace.info("Arrêt de la thread de surveillance du job");
      }
    }
    
    /**
     * Vérifier si le job est toujours actif.
     */
    private void verifierJob() {
      // Interrompre la thread s'il n'y a pas ou plus de job
      if (jobSoumis == null) {
        // Tracer la cause d'arrêt de la surveillance du job
        Trace.info("L'objet Java correspondant au job à surveiller n'existe plus : PointDeMenu=" + nomProgramme);
        
        // Activer le flag d'interruption de la thread
        Thread.currentThread().interrupt();
        jobActif = false;
        return;
      }
      
      // Récupérer les informations sur le job (statut, présence de messages ou de lock)
      // C'est fait dans un try/catch pour gérer le cas ou le job s'est arrété et rend impossible la récupération de ses informations
      boolean statutMessagePresent = false;
      boolean lockPresent = false;
      Enumeration<?> enumQueuedMessage = null;
      try {
        // Mettre à jour les informations du job
        jobSoumis.loadInformation();
        
        // Récupérer le statut du job
        jobActif = jobSoumis.getStatus().equals(Job.JOB_STATUS_ACTIVE);
        
        // Tester s'il y a un message en attente
        statutMessagePresent = jobSoumis.getValue(Job.ACTIVE_JOB_STATUS).toString().equals("MSGW");
        
        // Lire les messages en attente s'il y en a
        if (statutMessagePresent) {
          // Lire les messages les plus récents d'abord
          jobSoumis.getJobLog().setListDirection(false);
          
          // Récupérer la likste des messages en attente
          enumQueuedMessage = jobSoumis.getJobLog().getMessages();
        }
        
        // Tester s'il y a un lock en attente
        lockPresent = jobSoumis.getValue(Job.ACTIVE_JOB_STATUS).toString().equals("LCKW");
      }
      catch (Exception e) {
        // Tracer la cause d'arrêt de la surveillance du job
        Trace.info("Le job s'est stoppé de lui-même ou a été stoppé : PointDeMenu=" + nomProgramme);
        
        // Interrompre la thread en cas d'erreur car le job n'existe plus
        // On continue tout de même le traitement pour envoyer les éventuels messages reçus
        Thread.currentThread().interrupt();
        jobActif = false;
      }
      
      // Tester s'il y a des messages en attente dans le job
      if (statutMessagePresent && enumQueuedMessage != null && queuedMessageEncours == null) {
        // Mémoriser le statut pour la prochaine fois
        statutPrecedent = "MSGW";
        
        // Tracer l'information de lock
        Trace.info("Le job a un ou des messages en attente");
        
        try {
          // Construire le message à envoyer au client
          StringBuffer message = new StringBuffer();
          
          // Parcourir les messages en attente
          while (enumQueuedMessage.hasMoreElements()) {
            QueuedMessage queuedMessage = (QueuedMessage) enumQueuedMessage.nextElement();
            
            // Ne conserver que les messages qui demande une réponse
            if (queuedMessage.getReplyStatus().equals("W")) {
              // Stocker le message
              queuedMessageEncours = queuedMessage;
              
              // Constuire le message
              message.append(Constantes.RPGMSG_ERR).append(infosJob).append(Constantes.SEPARATEUR_CHAINE_CHAR);
              message.append(queuedMessage.getID()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
              message.append(queuedMessage.getSeverity()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
              message.append(queuedMessage.getText()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
              message.append(queuedMessage.getHelp()).append(Constantes.SEPARATEUR_CHAINE_CHAR);
              message.append(DateHeure.getJourHeure(0, queuedMessage.getDate().getTime()));
              
              // Envoyer le message au client
              envoyerMessageVersClient(message.toString());
            }
          }
        }
        catch (Exception e) {
          Trace.erreur(e, "Erreur lors de la récupération des messages du job");
        }
      }
      // Tester s'il y a un lock en attente dans le job
      else if (lockPresent) {
        // Ne rien faire si un message a déjà été envoyé pour le lock lors de la vérification précédente
        if (!statutPrecedent.equals("LCKW")) {
          // Mémoriser le statut pour la prochaine fois
          statutPrecedent = "LCKW";
          
          // Tracer l'information de lock
          Trace.info("Le job est verrouillé");
          
          // Constuire le message
          String message = Constantes.RPGMSG_ERR + "W" + Constantes.SEPARATEUR_CHAINE_CHAR + "Le travail " + infosJob + " est verrouillé."
              + Constantes.SEPARATEUR_CHAINE_CHAR + "L'enregistrement sélectionné doit être tenu par un autre utilisateur.";
          
          // Envoyer le message au client
          envoyerMessageVersClient(message);
        }
      }
      else {
        // Effacer le statut pour la prochaine fois
        statutPrecedent = "";
      }
    }
    
    /**
     * Envoyer un message au client Java via la Dataqueue.
     * 
     * @param pMessage Message à envoyer au client graphique.
     */
    private void envoyerMessageVersClient(String pMessage) {
      // Vérifier les paramètres
      if (pMessage == null) {
        Trace.erreur("Impossible d'envoyer un message null via la dataqueue");
        return;
      }
      
      // Tracer l'envoi du message
      Trace.info("Device=" + device + ", Taille=" + pMessage.length() + ", Message vers client=" + pMessage.trim());
      
      // Vérifier que la dataqueue est toujours présente
      if (dataQRPGVersJava == null) {
        Trace.erreur("Impossible d'écrire le message dans la dataqueue car celle-ci n'existe plus");
        return;
      }
      
      // Ajouter le message dans la dataqueue du RPG vers le Java
      if (!dataQRPGVersJava.writeString(pMessage, Constantes.DATAQLONG)) {
        Trace.erreur("Impossible d'écrire le message dans la dataqueue");
      }
    }
  }
}
