/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.rpg.gescom.stocks.RpgStock;
import ri.serien.libas400.dao.rpg.gescom.stocks.RpgStocksAttenduCommande;
import ri.serien.libas400.dao.sql.gescom.article.SqlStocks;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlLigneVente;
import ri.serien.libas400.dao.sql.gescom.stock.SqlStock;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.IdLigneMouvement;
import ri.serien.libcommun.gescom.commun.client.ListeLigneMouvement;
import ri.serien.libcommun.gescom.commun.document.CritereAttenduCommande;
import ri.serien.libcommun.gescom.commun.document.LigneAttenduCommande;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.stock.CritereStock;
import ri.serien.libcommun.gescom.commun.stock.ListeStock;
import ri.serien.libcommun.gescom.commun.stock.StockDetaille;
import ri.serien.libcommun.gescom.commun.stockattenducommande.CritereStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockattenducommande.ListeStockAttenduCommande;
import ri.serien.libcommun.gescom.commun.stockcomptoir.CritereStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.IdStockComptoir;
import ri.serien.libcommun.gescom.commun.stockcomptoir.ListeStockComptoir;
import ri.serien.libcommun.gescom.commun.stockmouvement.CritereStockMouvement;
import ri.serien.libcommun.gescom.commun.stockmouvement.ListeStockMouvement;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.magasin.Magasin;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceStock;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service stock.
 */
public class ServiceStock implements InterfaceServiceStock {
  private static InterfaceServiceStock service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceStock() {
  }
  
  /**
   * Retourner l'implémentation du service stock.
   */
  protected static InterfaceServiceStock getService() {
    if (service == null) {
      InterfaceServiceStock interfaceService = new ServiceStock();
      service = (InterfaceServiceStock) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceStock.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Retourne la liste des stocks d'un article pour un établissement donné.
   */
  @Override
  public ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, CritereStockComptoir pCritereStock) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgStock traitementLigne = new RpgStock();
    return traitementLigne.chargerListeStock(sessionServeur.getSystemManager(), pCritereStock);
  }
  
  @Override
  public ListeStockComptoir chargerListeStockComptoir(IdSession pIdSession, List<IdStockComptoir> pListeId) throws RemoteException {
    // XXX Auto-generated method stub
    return null;
  }
  
  /**
   * Retourne la liste des stocks (attendu/commandé) d'un article.
   */
  @Override
  public List<LigneAttenduCommande> chargerListeLigneAttenduCommande(IdSession pIdSession, CritereAttenduCommande pCritereAttenduCommande)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgStocksAttenduCommande traiterUneLigneAttenduCommande = new RpgStocksAttenduCommande();
    List<LigneAttenduCommande> liste =
        traiterUneLigneAttenduCommande.chargerListeLigneAttenduCommande(sessionServeur.getSystemManager(), pCritereAttenduCommande);
    if (liste == null || liste.isEmpty()) {
      return liste;
    }
    
    // Récupération des id des documents de vente pour les id des lignes de vente renseignés
    List<IdLigneVente> listeIdLigneVente = new ArrayList<IdLigneVente>();
    for (LigneAttenduCommande ligne : liste) {
      if (ligne == null || ligne.getIdLigneVente() == null) {
        continue;
      }
      listeIdLigneVente.add(ligne.getIdLigneVente());
    }
    // Lancement du chargement des lignes de vente à partir d'une liste d'id de ligne de vente
    SqlLigneVente sqlLigneVente = new SqlLigneVente(sessionServeur.getQueryManager());
    List<LigneVente> listeLigneVente = sqlLigneVente.chargerListeLigneVente(listeIdLigneVente);
    if (listeLigneVente == null || listeLigneVente.isEmpty()) {
      return liste;
    }
    // Chargement de l'id document dans les lignes d'attendu/commandé
    for (LigneAttenduCommande ligne : liste) {
      if (ligne == null || ligne.getIdLigneVente() == null) {
        continue;
      }
      for (LigneVente ligneVente : listeLigneVente) {
        if (ligneVente.getId().equals(ligne.getIdLigneVente())) {
          ligne.setIdDocumentVente(ligneVente.getIdDocumentVente());
          break;
        }
      }
    }
    
    return liste;
  }
  
  /**
   * Retourne la liste des id de mouvements de stocks d'un article.
   */
  @Override
  public List<IdLigneMouvement> chargerListeIdLigneMouvement(IdSession pIdSession, IdArticle pIdArticle, Magasin pMagasin)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStocks sql = new SqlStocks(sessionServeur.getQueryManager());
    return sql.chargerListeIdLigneMouvement(pIdArticle, pMagasin);
  }
  
  /**
   * Retourne la liste des mouvements de stocks d'un article.
   */
  @Override
  public ListeLigneMouvement chargerListeLigneMouvement(IdSession pIdSession, List<IdLigneMouvement> pListeId,
      Integer pNombreDecimalesStock) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStocks sql = new SqlStocks(sessionServeur.getQueryManager());
    return sql.chargerListeLigneMouvement(pListeId, pNombreDecimalesStock);
  }
  
  /**
   * Charger la liste des stocks suivant les critères de recherche.
   */
  @Override
  public ListeStock chargerListeStock(IdSession pIdSession, CritereStock pCritereStock) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStock operation = new SqlStock(sessionServeur.getQueryManager());
    return operation.chargerListeStock(pCritereStock);
  }
  
  /**
   * Charger le stock détaillé d'un article pour un magasin ou un établissement.
   */
  @Override
  public StockDetaille chargerStockDetaille(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin,
      IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStock operation = new SqlStock(sessionServeur.getQueryManager());
    return operation.chargerStockDetaille(pIdEtablissement, pIdMagasin, pIdArticle);
  }
  
  /**
   * Charger les lignes attendus et commandés suivant les critères de recerche.
   */
  @Override
  public ListeStockAttenduCommande chargerListeStockAttenduCommande(IdSession pIdSession,
      CritereStockAttenduCommande pCritereStockAttenduCommande) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStock operation = new SqlStock(sessionServeur.getQueryManager());
    return operation.chargerListeStockAttenduCommande(pCritereStockAttenduCommande);
  }
  
  /**
   * Charger la liste des mouvements de stock suivant les critères de recherche.
   */
  @Override
  public ListeStockMouvement chargerListeStockMouvement(IdSession pIdSession, CritereStockMouvement pCritereStockMouvement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStock operation = new SqlStock(sessionServeur.getQueryManager());
    return operation.chargerListeStockMouvement(pCritereStockMouvement);
  }
  
  /**
   * Charger la date minimum de réapprovisionnement possible.
   */
  @Override
  public Date chargerDateMiniReappro(IdSession pIdSession, IdArticle pIdArticle) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlStock operation = new SqlStock(sessionServeur.getQueryManager());
    return operation.chargerDateMiniReappro(pIdArticle);
  }
}
