/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;

import ri.serien.libcommun.commun.IdClientDesktop;
import ri.serien.libcommun.commun.bdd.BDD;
import ri.serien.libcommun.exploitation.environnement.IdSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.ListeSousEnvironnement;
import ri.serien.libcommun.exploitation.environnement.SousEnvironnement;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.JarUtils;
import ri.serien.libcommun.outils.session.EnumTypeSession;
import ri.serien.libcommun.outils.session.EnvUser;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceSession;
import ri.serien.serveurmetier.serveur.ManagerAS400;
import ri.serien.serveurrmi.clientdesktop.ClientDesktop;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service session.
 */
public class ServiceSession implements InterfaceServiceSession {
  private static InterfaceServiceSession service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceSession() {
  }
  
  /**
   * Retourner l'implémentation du service session.
   */
  protected static InterfaceServiceSession getService() {
    if (service == null) {
      InterfaceServiceSession interfaceService = new ServiceSession();
      service = (InterfaceServiceSession) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceSession.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Récupérer la version du serveur métier.
   */
  @Override
  public String getVersion() throws RemoteException {
    // Lire la version en utilisant le ClassLoader de ce JAR
    String version = null;
    try {
      version = JarUtils.getVersionJar(getClass().getClassLoader());
    }
    catch (Exception e) {
      throw new MessageErreurException("Impossible de récupérer la version du serveur métier");
    }
    
    // Tracer
    Trace.info("Version serien-app-serveur : " + version);
    return version;
  }
  
  /**
   * Création d'un objet client desktop centralisant toutes les données d'une instance de l'application Desktop.
   */
  @Override
  public IdClientDesktop creerClientDesktop() {
    return ManagerClientDesktop.creerClientDesktop();
  }
  
  /**
   * Destruction d'un objet client desktop.
   */
  @Override
  public void detruireClientDesktop(IdClientDesktop pIdClientDesktop) {
    ManagerClientDesktop.detruireClientDesktop(pIdClientDesktop);
  }
  
  /**
   * Connecte l'objet client desktop au serveur tout en validant le profil et le mot de passe.
   */
  @Override
  public boolean connecterClientDesktop(IdClientDesktop pIdClientDesktop, EnvUser pEnvUser) throws RemoteException {
    return ManagerClientDesktop.connecterClientDesktop(pIdClientDesktop, pEnvUser);
  }
  
  @Override
  public EnvUser getEnvUser(IdClientDesktop pIdClientDesktop) {
    return ManagerClientDesktop.getEnvUser(pIdClientDesktop);
  }
  
  /**
   * Retourne la liste des runtimes pour des environnements.
   */
  @Override
  public ListeSousEnvironnement listerSousEnvironnement(IdSession pIdSession) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.listerSousEnvironnement();
  }
  
  /**
   * Retourner le sous-système à partir de son identifiant.
   */
  @Override
  public SousEnvironnement recupererSousEnvironnement(IdSession pIdSession, IdSousEnvironnement pIdSousEnvironnement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    return sessionServeur.recupererSousEnvironnement(pIdSousEnvironnement);
  }
  
  /**
   * Retourne l'id de la session lors de la création de la session.
   */
  @Override
  public IdSession creerSession(IdClientDesktop pIdClientDesktop) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.creerSession(pIdClientDesktop);
    if (sessionServeur == null) {
      throw new MessageErreurException(
          "Une erreur a été rencontrée lors de la création d'une session pour le client desktop " + pIdClientDesktop);
    }
    return sessionServeur.getIdSession();
  }
  
  /**
   * Demande le démarrage d'une session.
   */
  @Override
  public void ouvrirSession(IdSession pIdSession, EnumTypeSession pEnumSession, String pMessage) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    sessionServeur.ouvrirSession(pEnumSession, pMessage);
  }
  
  /**
   * Demande la fermeture d'une session donnée.
   */
  @Override
  public void fermerSession(IdSession pIdSession) throws RemoteException {
    if (pIdSession == null) {
      throw new MessageErreurException("Impossible de récupérer la session car son identifiant est invalide.");
    }
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    if (sessionServeur == null) {
      throw new MessageErreurException("L'objet sessionServeur est incorrect.");
    }
    sessionServeur.fermerSession();
  }
  
  /**
   * Se connecte à la base de données.
   */
  @Override
  public void connecterBdd(IdClientDesktop pIdClientDesktop, BDD pBaseDeDonnees) {
    ClientDesktop clientDesktop = ManagerClientDesktop.getClientDesktop(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    clientDesktop.connecterBaseDeDonnees(pBaseDeDonnees);
  }
  
  /**
   * Se déconnecte de la base de données.
   */
  @Override
  public void deconnecterBdd(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = ManagerClientDesktop.getClientDesktop(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    clientDesktop.deconnecterBaseDeDonnees();
  }
  
  /**
   * Retourne la base de données courante de l'utilisateur.
   */
  @Override
  public BDD recupererBdd(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = ManagerClientDesktop.getClientDesktop(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    return clientDesktop.getBaseDeDonnees();
  }
  
  /**
   * Retourne la base de données par défaut de l'utilisateur.
   */
  @Override
  public BDD recupererBddParDefaut(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = ManagerClientDesktop.getClientDesktop(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    return clientDesktop.getBaseDeDonneesParDefaut();
  }
  
  /**
   * Contrôler les versions de Série N et de la base de données en cours.
   */
  @Override
  public int controlerVersion(IdClientDesktop pIdClientDesktop) {
    ClientDesktop clientDesktop = ManagerClientDesktop.getClientDesktop(pIdClientDesktop);
    if (clientDesktop == null) {
      throw new MessageErreurException("L'objet client desktop est incorrect.");
    }
    return clientDesktop.controlerVersion();
  }
  
  /**
   * Retourne le chemin du dossier racine du serveur.
   */
  @Override
  public String getNomDossierRacineServeur() {
    return ManagerAS400.getDossierRacineServeur();
  }
  
}
