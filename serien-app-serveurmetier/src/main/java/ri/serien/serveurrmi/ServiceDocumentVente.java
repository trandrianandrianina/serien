/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import ri.serien.libas400.dao.rpg.gescom.article.articlesaapprovisionner.RpgChargerListeArticlesAApprovisionner;
import ri.serien.libas400.dao.rpg.gescom.documentvente.RpgDocumentVente;
import ri.serien.libas400.dao.rpg.gescom.documentvente.RpgGenererCNVDepuisDocument;
import ri.serien.libas400.dao.rpg.gescom.lienligne.RpgChargerLienLigne;
import ri.serien.libas400.dao.rpg.gescom.lignevente.RpgLigneVente;
import ri.serien.libas400.dao.rpg.gescom.lignevente.RpgLigneVenteBase;
import ri.serien.libas400.dao.rpg.gescom.negociation.RpgChargerConditionAchat;
import ri.serien.libas400.dao.rpg.gescom.negociation.RpgChargerNegociation;
import ri.serien.libas400.dao.rpg.gescom.negociation.RpgChargerNegociationAchat;
import ri.serien.libas400.dao.rpg.gescom.negociation.RpgSauverNegociation;
import ri.serien.libas400.dao.rpg.gescom.prixflash.RpgChargerPrixFlash;
import ri.serien.libas400.dao.rpg.gescom.prodevis.OperationsProDevis;
import ri.serien.libas400.dao.rpg.gescom.prodevis.RpgModifierStatutProDevis;
import ri.serien.libas400.dao.rpg.gescom.sipe.ImportationPlanPoseSipe;
import ri.serien.libas400.dao.sql.gescom.adressedocument.SqlAdresseDocument;
import ri.serien.libas400.dao.sql.gescom.boncour.SqlBonCour;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlBlocNoteDocumentVente;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlDemandeDeblocage;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlDemandeDeblocageVenteSousPrv;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlDocumentVente;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlInitialiserPrixClient;
import ri.serien.libas400.dao.sql.gescom.documentvente.SqlLigneVente;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libcommun.commun.memo.EnumTypeMemo;
import ri.serien.libcommun.commun.memo.IdMemo;
import ri.serien.libcommun.commun.memo.Memo;
import ri.serien.libcommun.exploitation.dataarea.LdaSerien;
import ri.serien.libcommun.gescom.achat.document.ligneachat.LigneAchat;
import ri.serien.libcommun.gescom.achat.reapprovisionnement.ReapprovisionnementClient;
import ri.serien.libcommun.gescom.commun.adresse.Adresse;
import ri.serien.libcommun.gescom.commun.adressedocument.AdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.CritereAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.IdAdresseDocument;
import ri.serien.libcommun.gescom.commun.adressedocument.ListeAdresseDocument;
import ri.serien.libcommun.gescom.commun.article.Article;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.article.ParametresLireArticle;
import ri.serien.libcommun.gescom.commun.blocnotes.BlocNote;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.Negociation;
import ri.serien.libcommun.gescom.commun.conditionachat.ConditionAchat;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.lienligne.IdLienLigne;
import ri.serien.libcommun.gescom.commun.lienligne.ListeLienLigne;
import ri.serien.libcommun.gescom.personnalisation.magasin.IdMagasin;
import ri.serien.libcommun.gescom.personnalisation.motifretour.IdMotifRetour;
import ri.serien.libcommun.gescom.personnalisation.typeavoir.IdTypeAvoir;
import ri.serien.libcommun.gescom.vente.alertedocument.AlertesLigneDocument;
import ri.serien.libcommun.gescom.vente.avoir.ListeAvoir;
import ri.serien.libcommun.gescom.vente.boncour.BonCour;
import ri.serien.libcommun.gescom.vente.boncour.CarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereBonCour;
import ri.serien.libcommun.gescom.vente.boncour.CritereCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdBonCour;
import ri.serien.libcommun.gescom.vente.boncour.IdCarnetBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeBonCour;
import ri.serien.libcommun.gescom.vente.boncour.ListeCarnetBonCour;
import ri.serien.libcommun.gescom.vente.document.CritereDocumentVente;
import ri.serien.libcommun.gescom.vente.document.DemandeDeblocage;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.gescom.vente.document.ListeDocumentVenteBase;
import ri.serien.libcommun.gescom.vente.document.OptionsEditionVente;
import ri.serien.libcommun.gescom.vente.document.OptionsValidation;
import ri.serien.libcommun.gescom.vente.document.PrixFlash;
import ri.serien.libcommun.gescom.vente.ligne.CritereLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.LigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeIdLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneRetourArticle;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVente;
import ri.serien.libcommun.gescom.vente.ligne.ListeLigneVenteBase;
import ri.serien.libcommun.gescom.vente.ligne.Regroupement;
import ri.serien.libcommun.gescom.vente.prodevis.CriteresRechercheProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.DescriptionProDevis;
import ri.serien.libcommun.gescom.vente.prodevis.EnumStatutProDevis;
import ri.serien.libcommun.gescom.vente.reglement.ListeReglement;
import ri.serien.libcommun.gescom.vente.sipe.CritereSipe;
import ri.serien.libcommun.gescom.vente.sipe.EnumTypePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.IdPlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.ListePlanPoseSipe;
import ri.serien.libcommun.gescom.vente.sipe.PlanPoseSipe;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceDocumentVente;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service document.
 */
public class ServiceDocumentVente implements InterfaceServiceDocumentVente {
  private static InterfaceServiceDocumentVente service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceDocumentVente() {
  }
  
  /**
   * Retourner l'implémentation du service.
   */
  protected static InterfaceServiceDocumentVente getService() {
    if (service == null) {
      InterfaceServiceDocumentVente interfaceService = new ServiceDocumentVente();
      service = (InterfaceServiceDocumentVente) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceDocumentVente.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Charger les identifiants des documents de ventes correspondants aux critères de recherche.
   */
  @Override
  public List<IdDocumentVente> chargerListeIdDocumentVente(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentVente sql = new SqlDocumentVente(sessionServeur.getQueryManager());
    return sql.chargerListeIdDocumentVente(pCritereDocumentVente);
  }
  
  /**
   * Charge les informations minimales pour affichage en liste des documents dont on a passé l'ID
   */
  @Override
  public ListeDocumentVenteBase chargerListeDocumentVenteBase(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Charger les documents de vente
    SqlDocumentVente sql = new SqlDocumentVente(sessionServeur.getQueryManager());
    ListeDocumentVenteBase listeDocumentVenteBase = sql.chargerListeDocumentVenteBase(pListeIdDocumentVente);
    
    // Compléter les documents de ventes avec le libellé des chantiers
    listeDocumentVenteBase.chargerLibelleChantier(pIdSession);
    return listeDocumentVenteBase;
  }
  
  /**
   * Charge les IDs des documents de ventes de type avoir pour un document de vente donné.
   */
  @Override
  public List<IdDocumentVente> chargerListeIdDocumentAvoir(IdSession pIdSession, IdDocumentVente pIdDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentVente sql = new SqlDocumentVente(sessionServeur.getQueryManager());
    return sql.chargerListeIdDocumentAvoir(pIdDocument);
  }
  
  @Override
  public ListeDocumentVenteBase chargerListeAvoir(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentAvoir) {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentVente sql = new SqlDocumentVente(sessionServeur.getQueryManager());
    return sql.chargerListeAvoir(pListeIdDocumentAvoir);
  }
  
  @Override
  public List<IdDocumentVente> chargerListeIdFactureNonReglee(IdSession pIdSession, CritereDocumentVente pCritereDocumentVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentVente sql = new SqlDocumentVente(sessionServeur.getQueryManager());
    return sql.chargerListeIdFactureNonReglee(pCritereDocumentVente);
  }
  
  /**
   * Charger l'entête d'un document de ventes.
   */
  @Override
  public DocumentVente chargerEnteteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    // QueryManager queryManager = sessionServeur.getQueryManager();
    
    // Charger l'entête du document de ventes
    RpgDocumentVente operation = new RpgDocumentVente();
    DocumentVente documentVente = operation.chargerEnteteDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente);
    
    // Charger les adresses de facturation et de livraison
    List<Adresse> listeAdresse = operation.chargerListeAdresseDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente);
    if (listeAdresse.size() >= 1) {
      documentVente.setAdresseFacturation(listeAdresse.get(0));
    }
    if (listeAdresse.size() >= 2) {
      documentVente.getTransport().setAdresseLivraison(listeAdresse.get(1));
    }
    
    // Charger le mémo pour les informations de livraison/enlèvement
    Memo memo = Memo.charger(pIdSession, pIdDocumentVente, EnumTypeMemo.LIVRAISON);
    documentVente.setMemoLivraisonEnlevement(memo);
    
    return documentVente;
  }
  
  /**
   * Charger les entêtes d'une liste de documents de ventes.
   */
  @Override
  public List<DocumentVente> chargerEnteteListeDocumentVente(IdSession pIdSession, List<IdDocumentVente> pListeIdDocumentVente)
      throws RemoteException {
    List<DocumentVente> listeDocument = new ArrayList<DocumentVente>();
    if (pListeIdDocumentVente != null) {
      for (IdDocumentVente idDocumentVente : pListeIdDocumentVente) {
        DocumentVente document = chargerEnteteDocumentVente(pIdSession, idDocumentVente);
        listeDocument.add(document);
      }
    }
    return listeDocument;
  }
  
  /**
   * Créer un document de ventes.
   * La méthode retourne l'identifiant du document de ventes créés.
   */
  @Override
  public IdDocumentVente creerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Créer le document de ventes
    RpgDocumentVente operation = new RpgDocumentVente();
    IdDocumentVente idDocumentVente = operation.creerDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pDateTraitement);
    
    // Sauver le mémo s'il y en a un
    if (pDocumentVente.getMemoLivraisonEnlevement() != null) {
      IdMemo idMemo = IdMemo.getInstancePourDocumentVente(idDocumentVente, 1);
      pDocumentVente.getMemoLivraisonEnlevement().setId(idMemo);
      pDocumentVente.getMemoLivraisonEnlevement().sauver(pIdSession);
    }
    
    return idDocumentVente;
  }
  
  /**
   * Sauver un document de ventes existants.
   */
  @Override
  public void sauverEnteteDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.sauverEnteteDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pDateTraitement);
  }
  
  /**
   * Contrôle de l'entête d'un document avant sa création/modification.
   */
  @Override
  public void controlerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.controlerDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pDateTraitement);
  }
  
  /**
   * Contrôle de fin d'un document.
   */
  @Override
  public OptionsValidation controlerFinDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente,
      OptionsValidation pOptionsValidation, Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    OptionsValidation options =
        operation.controlerFinDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pOptionsValidation, pDateTraitement);
    if (options == null) {
      throw new MessageErreurException("Le contrôle de fin du document a révélé une erreur");
    }
    return options;
  }
  
  /**
   * Chiffre un document (par exemple: après l'insertion d'une ou plusieurs lignes articles).
   */
  @Override
  public void chiffrerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pNePasReactualiserPrix)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.modifierChiffrageDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente, pNePasReactualiserPrix);
    if (pIdDocumentVente == null) {
      throw new MessageErreurException("Le chiffrage du document a échoué");
    }
  }
  
  /**
   * Valide un document.
   */
  @Override
  public DocumentVente validerDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, OptionsValidation pOptions,
      Date pDateTraitement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Sauver le mémo s'il y en a un
    if (pDocumentVente.getMemoLivraisonEnlevement() != null) {
      IdMemo idMemo = IdMemo.getInstancePourDocumentVente(pDocumentVente.getId(), 1);
      pDocumentVente.getMemoLivraisonEnlevement().setId(idMemo);
      pDocumentVente.getMemoLivraisonEnlevement().sauver(pIdSession);
    }
    
    // Valider le document de ventes
    RpgDocumentVente operation = new RpgDocumentVente();
    pDocumentVente =
        operation.modifierValidationDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pOptions, pDateTraitement);
    if (pDocumentVente == null) {
      throw new MessageErreurException("La validation du document a échoué");
    }
    return pDocumentVente;
  }
  
  /**
   * Annule un document.
   */
  @Override
  public void annulerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.annulerDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente);
  }
  
  /**
   * Edite un document.
   */
  @Override
  public String editerDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pDocumentModifiable,
      Date pDateTraitement, OptionsEditionVente pOptions, LdaSerien pLdaSerien) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    return operation.editerDocument(sessionServeur.getSystemManager(), pIdDocumentVente, pDocumentModifiable, pDateTraitement, pOptions,
        pLdaSerien);
  }
  
  /**
   * Lit une ligne d'article donnée d'un document.
   */
  public LigneVente lireLigneArticle(IdSession pIdSession, IdDocumentVente pIdDocumentVente, int pNumeroLigne) {
    return null;
  }
  
  /**
   * Initialise les informations d'une ligne de ventes.
   */
  @Override
  public LigneVente initialiserLigneVente(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle, Date pDateTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    LigneVente ligneVente = operation.initialiserLigneVente(systemeManager, pIdLigneVente, pIdArticle, pDateTraitement);
    if (ligneVente == null) {
      throw new MessageErreurException("L'initialisation de la ligne article a échoué");
    }
    return ligneVente;
  }
  
  /**
   * Controle la validité d'une ligne article.
   */
  @Override
  public AlertesLigneDocument controlerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.controlerLigneVente(sessionServeur.getSystemManager(), pLigneVente, pDateTraitement);
  }
  
  /**
   * Création d'une ligne dans un document, si le document n'existe pas il est créé.
   */
  @Override
  public void sauverLigneVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    QueryManager queryManager = sessionServeur.getQueryManager();
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    operation.sauverLigneVente(systemeManager, queryManager, pLigneVente);
  }
  
  /**
   * Modification d'une ligne dans un document.
   */
  @Override
  public boolean modifierLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SystemeManager systemeManager = sessionServeur.getSystemManager();
    QueryManager queryManager = sessionServeur.getQueryManager();
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    boolean reussite = operation.modifierLigneVente(systemeManager, queryManager, pLigneVente);
    if (reussite == false) {
      throw new MessageErreurException("La ligne du document n'a pas pu être modifiée");
    }
    return reussite;
  }
  
  /**
   * Supprime une ligne article du document.
   */
  @Override
  public boolean supprimerLigneDocumentVente(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLigneVente sqlLigneArticle = new SqlLigneVente(querym);
    boolean reussite = sqlLigneArticle.supprimerLigne(pLigneVente);
    if (!reussite) {
      throw new MessageErreurException("La ligne du document n'a pas pu être supprimée");
    }
    return reussite;
  }
  
  /**
   * recherche toutes les id des lignes correspondant aux critères
   */
  @Override
  public ListeIdLigneVente chargerListeIdLigneVente(IdSession pIdSession, CritereLigneVente pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlLigneVente sqlLigneArticle = new SqlLigneVente(querym);
    return sqlLigneArticle.chargerListeIdLigneVente(pCriteres);
  }
  
  /**
   * recherche toutes les id des lignes contenant un code article donné, dans un document
   */
  @Override
  public List<IdLigneVente> chargerIdLigneVenteParCodeArticle(IdSession pIdSession, CritereLigneVente pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlLigneVente sqlLigneArticle = new SqlLigneVente(querym);
    return sqlLigneArticle.chargerIdLigneVenteParCodeArticle(pCriteres);
  }
  
  /**
   * recherche toutes les id des lignes contenant un code article donné, dans un document
   */
  @Override
  public ListeIdLigneVente chargerToutesIdLigneVenteParCodeArticle(IdSession pIdSession, CritereLigneVente pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    SqlLigneVente sqlLigneArticle = new SqlLigneVente(querym);
    return sqlLigneArticle.chargerToutesIdLigneVenteParCodeArticle(pCriteres);
  }
  
  /**
   * Lit le petit bloc-notes d'un document.
   */
  @Override
  public BlocNote chargerPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlBlocNoteDocumentVente traitementBlocNoteDocument = new SqlBlocNoteDocumentVente(querym);
    return traitementBlocNoteDocument.chargerPetitBlocNotesDocumentVente(pIdDocumentVente);
  }
  
  /**
   * Ecrit le petit bloc-note d'un document.
   */
  @Override
  public int sauverPetitBlocNoteDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, BlocNote pBlocNote)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlBlocNoteDocumentVente traitementBlocNotesDocument = new SqlBlocNoteDocumentVente(querym);
    return traitementBlocNotesDocument.sauverPetitBlocNoteDocumentVente(pIdDocumentVente, pBlocNote);
  }
  
  /**
   * Recherche toutes lignes articles d'un document à partir des critères donnés.
   */
  @Override
  public ListeLigneVente chargerListeLigneVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, CritereLigneVente pCriteres,
      boolean isTTC) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.chargerListeLigneVente(sessionServeur.getSystemManager(), pIdDocumentVente, pCriteres, isTTC);
  }
  
  /**
   * Charge toutes les lignes base correspondant à la liste d'identifiants de lignes passée en paramètre
   */
  @Override
  public ListeLigneVenteBase chargerListeLigneVenteBase(IdSession pIdSession, List<IdLigneVente> pListeIdLigneVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVenteBase operation = new RpgLigneVenteBase();
    return operation.chargerListeLigneVenteBase(sessionServeur.getSystemManager(), pListeIdLigneVente);
  }
  
  /**
   * Recherche toutes lignes articles des documents contenant un article retourné par un client
   */
  @Override
  public ListeLigneRetourArticle chargerListeLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantite, Article pArticle,
      Client pClient, ListeLigneRetourArticle pListeOrigine) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.chargerListeLigneVenteRetour(sessionServeur.getSystemManager(), pQuantite, pArticle, pClient, pListeOrigine);
  }
  
  /**
   * Recherche toutes lignes articles des documents contenant un article retourné par un client
   */
  @Override
  public void sauverLigneRetourArticle(IdSession pIdSession, BigDecimal pQuantiteRetour, BigDecimal pPrixNetRetour,
      IdMotifRetour pMotifRetour, LigneVente pLigneVente, DocumentVente pDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    operation.sauverLigneVenteRetour(sessionServeur.getSystemManager(), pQuantiteRetour, pPrixNetRetour, pMotifRetour, pLigneVente,
        pDocument);
  }
  
  /**
   * Lit un regroupement.
   */
  @Override
  public Regroupement chargerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente, Regroupement pRegroupement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.chargerLigneVenteGroupe(sessionServeur.getSystemManager(), pIdLigneVente, pRegroupement);
  }
  
  /**
   * Créé ou modifie un regroupement de lignes articles pour un document.
   */
  @Override
  public void sauverRegroupement(IdSession pIdSession, Regroupement pRegroupement) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    operation.sauverLigneVenteGroupe(sessionServeur.getSystemManager(), pRegroupement);
  }
  
  /**
   * Supprime un regroupement de lignes articles d'un document.
   */
  @Override
  public void supprimerRegroupement(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    operation.supprimerLigneVenteGroupe(sessionServeur.getSystemManager(), pIdLigneVente);
  }
  
  /**
   * Modifie les adresses de facturation et de livraison d'un document.
   */
  @Override
  public void sauverAdresseDocumentVente(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Adresse pAdresseFacturation,
      Adresse pAdresseLivraison) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.sauverAdresseDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente, pAdresseFacturation, pAdresseLivraison);
  }
  
  /**
   * Dupliquer un document sans changer son état (par exemple un devis en devis)
   */
  @Override
  public IdDocumentVente dupliquerDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pAvecRecalculPrix)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    return operation.creerDocumentVenteParDuplication(sessionServeur.getSystemManager(), pIdDocumentVente,
        RpgDocumentVente.DUPLICATION_DOCUMENT, pAvecRecalculPrix);
  }
  
  /**
   * Créer un avoir par duplication d'un bon ou d'une facture
   */
  @Override
  public IdDocumentVente creerAvoir(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdTypeAvoir pIdTypeAvoir)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    return operation.creerAvoir(sessionServeur.getSystemManager(), pIdDocumentVente, pIdTypeAvoir);
  }
  
  /**
   * Dupliquer un document en faisant évoluer son état (par exemple devis en commande ou commande en bon ou bon en facture)
   */
  @Override
  public IdDocumentVente dupliquerDocumentEtatSuivant(IdSession pIdSession, IdDocumentVente pIdDocumentVente, boolean pAvecRecalculPrix)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    return operation.creerDocumentVenteParDuplication(sessionServeur.getSystemManager(), pIdDocumentVente,
        RpgDocumentVente.DUPLICATION_DOCUMENT_AVEC_ETAT__SUIVANT, pAvecRecalculPrix);
  }
  
  /**
   * Lit les règlements d'un document donné.
   */
  @Override
  public ListeReglement chargerListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, Date pDateTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    return operation.chargerListeReglement(sessionServeur.getSystemManager(), pIdDocumentVente, pDateTraitement);
  }
  
  /**
   * Ecrit la liste des règlements d'un document donné.
   */
  @Override
  public void sauverListeReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente, ListeReglement pListeReglement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.sauverListeReglement(sessionServeur.getSystemManager(), pIdDocumentVente, pListeReglement);
  }
  
  /**
   * Ajouter un document à la liste pour le règlement multiple.
   */
  @Override
  public void sauverDocumentVenteReglementMultiple(IdSession pIdSession, boolean pViderListe, IdDocumentVente pIdDocumentVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.sauverReglementMultipleDocumentVente(sessionServeur.getSystemManager(), pViderListe, pIdDocumentVente);
  }
  
  /**
   * Supprimer le règlement d'un document.
   */
  @Override
  public void supprimerReglement(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.supprimerReglement(sessionServeur.getSystemManager(), pIdDocumentVente);
  }
  
  /**
   * Solder le reliquat d'un devis ou d'une commande.
   */
  @Override
  public void solderReliquat(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.solderReliquat(sessionServeur.getSystemManager(), pIdDocumentVente);
  }
  
  /**
   * Recherche le prix flash d'un article pour un client donné.
   */
  @Override
  public PrixFlash chargerPrixFlash(IdSession pIdSession, IdLigneVente pIdLigneVente, IdArticle pIdArticle, IdClient pIdClient)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerPrixFlash traitement = new RpgChargerPrixFlash();
    return traitement.chargerPrixFlash(sessionServeur.getSystemManager(), pIdLigneVente, pIdArticle, pIdClient);
  }
  
  /**
   * Permet de changer le client d'un document.
   */
  @Override
  public void sauverDocumentVenteClientModifie(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClient,
      boolean pChangerClientFacture, boolean pChangerClientLivre) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.modifierClientDocumentVente(sessionServeur.getSystemManager(), pIdDocumentVente, pIdClient, pChangerClientFacture,
        pChangerClientLivre);
  }
  
  /**
   * Lit une demande de déblocage pour un document.
   */
  @Override
  public DemandeDeblocage chargerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocage operation = new SqlDemandeDeblocage(querym);
    return operation.chargerDemandeDeblocage(pIdDocumentVente);
  }
  
  /**
   * Ecrit une demande de déblocage pour un document.
   */
  @Override
  public void sauverDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocage operation = new SqlDemandeDeblocage(querym);
    operation.sauverDemandeDeblocage(pIdDocumentVente, pProfil);
  }
  
  /**
   * Supprime la demande de déblocage d'un document.
   */
  @Override
  public void supprimerDemandeDeblocage(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocage operation = new SqlDemandeDeblocage(querym);
    operation.supprimerDemandeDeblocage(pIdDocumentVente);
  }
  
  /**
   * Lit une demande de déblocage pour une vente sous le PRV.
   */
  @Override
  public DemandeDeblocage chargerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocageVenteSousPrv operation = new SqlDemandeDeblocageVenteSousPrv(querym);
    return operation.chargerDemandeDeblocageVenteSousPrv(pIdDocumentVente);
  }
  
  /**
   * Ecrit une demande de déblocage pour une vente sous le PRV.
   */
  @Override
  public void sauverDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente, String pProfil)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocageVenteSousPrv operation = new SqlDemandeDeblocageVenteSousPrv(querym);
    operation.sauverDemandeDeblocageVenteSousPrv(pIdDocumentVente, pProfil);
  }
  
  /**
   * Supprime la demande de déblocage pour une vente sous le PRV.
   */
  @Override
  public void supprimerDemandeDeblocageVenteSousPrv(IdSession pIdSession, IdDocumentVente pIdDocumentVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlDemandeDeblocageVenteSousPrv operation = new SqlDemandeDeblocageVenteSousPrv(querym);
    operation.supprimerDemandeDeblocage(pIdDocumentVente);
  }
  
  /**
   * Créer ligne dans un document avec extraction.
   */
  @Override
  public int sauverLigneVenteDocumentAvecExtraction(IdSession pIdSession, IdLigneVente pIdLigneVenteOrigine,
      IdLigneVente pIdLigneVenteDestination, int pTypeTraitement, BigDecimal pQuantiteDemandee, boolean pAvecRecalculPrix)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.sauverLigneVenteAvecExtraction(sessionServeur.getSystemManager(), pIdLigneVenteOrigine, pIdLigneVenteDestination,
        pTypeTraitement, pQuantiteDemandee, pAvecRecalculPrix);
  }
  
  /**
   * Valide l'extraction d'un document.
   */
  @Override
  public void validerExtractionDocumentVente(IdSession pIdSession, DocumentVente pDocumentVente, Date pDateTraitement,
      OptionsValidation pOptions) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.validerExtractionDocumentVente(sessionServeur.getSystemManager(), pDocumentVente, pDateTraitement, pOptions);
  }
  
  /**
   * Ecrit ou modifie une ligne commentaire dans le document donné.
   */
  @Override
  public int sauverLigneVenteCommentaire(IdSession pIdSession, LigneVente pLigneVente, DocumentVente pDocumentVente,
      boolean pIsEditionSurFactures, boolean pConserverSurAchat) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.sauverLigneVenteCommentaire(sessionServeur.getSystemManager(), pLigneVente, pDocumentVente, pIsEditionSurFactures,
        pConserverSurAchat);
  }
  
  /**
   * Lit une ligne commentaire dans le document donné.
   */
  @Override
  public LigneVente chargerLigneVenteCommentaire(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgLigneVente operation = new RpgLigneVente(pIdSession);
    return operation.chargerLigneVenteCommentaire(sessionServeur.getSystemManager(), pIdLigneVente);
  }
  
  @Override
  public void initialiserPrixClient(IdSession pIdSession, Client pClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlInitialiserPrixClient operation = new SqlInitialiserPrixClient(querym);
    operation.initialiserPrixClientArticle(pClient);
  }
  
  /**
   * Charge une liste de description de prodevis à partir de fichiers stockés dans l'IFS.
   */
  @Override
  public List<DescriptionProDevis> chargerListeDescriptionProDevis(IdSession pIdSession, CriteresRechercheProDevis pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    OperationsProDevis operation = new OperationsProDevis(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    return operation.chargerListeDescriptionProDevis(pCriteres);
  }
  
  /**
   * Importe un devis prodevis.
   */
  @Override
  public IdDocumentVente importerDevisProDevis(IdSession pIdSession, DescriptionProDevis pDescription, IdClient pIdClient)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    OperationsProDevis operation = new OperationsProDevis(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    return operation.importerDevis(pDescription, pIdClient);
  }
  
  /**
   * Modifier le statut d'un pro-devis
   */
  @Override
  public void modifierStatutProDevis(IdSession pIdSession, DescriptionProDevis pDescription, EnumStatutProDevis pNouveauStatut)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgModifierStatutProDevis operation = new RpgModifierStatutProDevis();
    operation.modifierStatutProDevis(sessionServeur.getSystemManager(), pDescription, pNouveauStatut);
  }
  
  /**
   * Lit une négociation pour une ligne article donnée.
   */
  @Override
  public Negociation chargerNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerNegociation operation = new RpgChargerNegociation();
    Negociation negociation =
        operation.chargerNegociation(sessionServeur.getSystemManager(), pLigneVente, pDateTraitement, pOptionTraitement);
    if (negociation == null) {
      throw new MessageErreurException("Aucune négociation n'a été trouvée");
    }
    return negociation;
  }
  
  /**
   * Ecrit une négociation pour une ligne article.
   */
  @Override
  public void sauverNegociation(IdSession pIdSession, LigneVente pLigneVente, Date pDateTraitement, String pOptionTraitement)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverNegociation operation = new RpgSauverNegociation();
    operation.sauverNegociation(sessionServeur.getSystemManager(), pLigneVente, pDateTraitement, pOptionTraitement);
  }
  
  /**
   * Lit une négociation d'achat pour un article dans un document donné.
   * Si la date est à null on cherchera la condition en cours.
   */
  @Override
  public ConditionAchat chargerConditionAchat(IdSession pIdSession, LigneVente pLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerConditionAchat traitement = new RpgChargerConditionAchat();
    return traitement.chargerConditionAchat(sessionServeur.getSystemManager(), pLigneVente);
  }
  
  /**
   * Lit une négociation d'achat pour un article dans un document d'achat donné.
   * Si la date est à null on cherchera la condition en cours.
   */
  @Override
  public ConditionAchat chargerNegociationAchat(IdSession pIdSession, LigneAchat pLigneAchat) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerNegociationAchat traitement = new RpgChargerNegociationAchat();
    return traitement.chargerNegociationAchat(sessionServeur.getSystemManager(), pLigneAchat);
  }
  
  /**
   * Lit la liste des avoirs non réglés pour un client
   */
  @Override
  public ListeAvoir chargerListeAvoir(IdSession pIdSession, IdClient pIdClient) throws RemoteException {
    /* Suite à la SNC-5473
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerListeAvoirClient operation = new RpgChargerListeAvoirClient();
    ListeAvoir listeAvoir = operation.chargerListeAvoirClient(sessionServeur.getSystemManager(), pIdClient);
    if (listeAvoir == null) {
      throw new MessageErreurException("Aucun avoir n'a été trouvé pour ce client");
    }
    return listeAvoir;*/
    return null;
  }
  
  /**
   * Rechercher la liste d'informations pour le réapprovisionnement des commandes client correspondants aux critères de recherche.
   */
  @Override
  public List<ReapprovisionnementClient> chargerListeArticlesAApprovisionner(IdSession pIdSession, List<IdLigneVente> pListeIdLigneVente,
      ParametresLireArticle pParametres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerListeArticlesAApprovisionner traitement = new RpgChargerListeArticlesAApprovisionner();
    return traitement.chargerListeArticlesAApprovisionner(sessionServeur.getSystemManager(), pListeIdLigneVente, pParametres);
  }
  
  /**
   * Charger la liste des id des lignes de vente dont les articles sont à approvisionner.
   */
  @Override
  public List<IdLigneVente> chargerListeIdLigneVenteAApprovisionner(IdSession pIdSession, CritereLigneVente pCriteres)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLigneVente operation = new SqlLigneVente(querym);
    return operation.chargerListeIdLigneVenteAApprovisionner(pCriteres);
  }
  
  /**
   * Charge la liste des lignes liées à la ligne passée en paramètre
   */
  @Override
  public ListeLienLigne chargerListeLienLigne(IdSession pIdSession, IdLienLigne pIdLigneOrigine) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerLienLigne operation = new RpgChargerLienLigne();
    return operation.chargerListeLienLigne(sessionServeur.getSystemManager(), pIdLigneOrigine);
  }
  
  /**
   * Charge la liste des lignes liées aux lignes passées en paramètre
   */
  @Override
  public List<ListeLienLigne> chargerListeLienLigne(IdSession pIdSession, ListeLigneVenteBase pListeLigneVenteBase)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerLienLigne operation = new RpgChargerLienLigne();
    return operation.chargerListeLienLigne(sessionServeur.getSystemManager(), pListeLigneVenteBase);
  }
  
  /**
   * Retourne si tous les articles des lignes de vente sont bien des articles direct usine
   * Cette information est une information en temps réel c'est à dire qu'elle
   */
  @Override
  public Boolean existeArticleDirectUsine(IdSession pIdSession, ListeLigneVente pListeLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlLigneVente operation = new SqlLigneVente(querym);
    return operation.existeArticleDirectUsine(pListeLigneVente);
  }
  
  /**
   * Créer un document de ventes.
   * La méthode retourne l'identifiant du document de ventes créés.
   */
  @Override
  public void genererCNVDepuisDocument(IdSession pIdSession, IdLigneVente pIdLigneVente, Date pDateDebut, Date pDateFin,
      String pReference) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Créer le document de ventes
    RpgGenererCNVDepuisDocument operation = new RpgGenererCNVDepuisDocument();
    operation.genererCNVDepuisDocument(sessionServeur.getSystemManager(), pIdLigneVente, pDateDebut, pDateFin, pReference);
    
  }
  
  /**
   * Changer le client sur un document en cours de création
   */
  @Override
  public void changerClientDocument(IdSession pIdSession, IdDocumentVente pIdDocumentVente, IdClient pIdClientOrigine,
      boolean isAvecRecalcul) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    // Créer le document de ventes
    RpgDocumentVente operation = new RpgDocumentVente();
    operation.changerClientDocument(sessionServeur.getSystemManager(), pIdDocumentVente, pIdClientOrigine, isAvecRecalcul);
  }
  
  /**
   * Sauver un carnet de bons de cour.
   */
  @Override
  public CarnetBonCour sauverCarnetBonCour(IdSession pIdSession, CarnetBonCour pCarnetBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    if (pCarnetBonCour != null && pCarnetBonCour.getId().isExistant()) {
      return sql.modifierCarnetBonCour(pCarnetBonCour);
    }
    else {
      return sql.creerCarnetBonCour(pCarnetBonCour);
    }
  }
  
  /**
   * Charger la liste des carnets des bons de cour correspondants aux critères de recherche.
   */
  @Override
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereCarnetBonCour pCritereCarnetBonCour)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerListeIdCarnetBonCour(pCritereCarnetBonCour);
  }
  
  /**
   * Charger une liste d'identifiants de carnets de bons de cour à partir des critères donnés
   */
  @Override
  public List<IdCarnetBonCour> chargerListeIdCarnetBonCour(IdSession pIdSession, CritereBonCour critereBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerListeIdCarnetBonCour(critereBonCour);
  }
  
  /**
   * Changer une liste d'identifiants de bons de cour à partir des critères donnés
   */
  @Override
  public List<IdBonCour> chargerListeIdBonCour(IdSession pIdSession, CritereBonCour critereBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerListeIdBonCour(critereBonCour);
  }
  
  /**
   * Charger la liste des carnets des bons de cour correspondants à la liste de leurs identifiants.
   */
  @Override
  public ListeCarnetBonCour chargerListeCarnetBonCour(IdSession pIdSession, List<IdCarnetBonCour> pListeIdCarnetBonCour)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerListeCarnetBonCour(pListeIdCarnetBonCour);
  }
  
  /**
   * Charger la liste des bons de cour correspondants à la liste de leurs identifiants.
   */
  @Override
  public ListeBonCour chargerListeBonCour(IdSession pIdSession, List<IdBonCour> pListeIdBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerListeBonCour(pListeIdBonCour);
  }
  
  /**
   * Charger un carnet de bons de cour à partir de son identifiant.
   */
  @Override
  public CarnetBonCour chargerCarnetBonCour(IdSession pIdSession, IdCarnetBonCour pIdCarnetBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerCarnetBonCour(pIdCarnetBonCour);
  }
  
  /**
   * Modifier un bon de cour à partir de son identifiant.
   */
  @Override
  public void modifierBonCour(IdSession pIdSession, BonCour pBonCour) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    sql.modifierBonCour(pBonCour);
  }
  
  /**
   * Charge un bon de cour à partir de son numéro.
   */
  @Override
  public BonCour chargerBonCour(IdSession pIdSession, IdEtablissement pIdEtablissement, IdMagasin pIdMagasin, int pNumeroBonCour)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlBonCour sql = new SqlBonCour(sessionServeur.getQueryManager());
    return sql.chargerBonCour(pIdEtablissement, pIdMagasin, pNumeroBonCour);
  }
  
  /**
   * Charger la liste des adresses de document de ventes correspondants à la liste de leurs identifiants.
   */
  @Override
  public List<IdAdresseDocument> chargerListeIdAdresseDocument(IdSession pIdSession, CritereAdresseDocument pCritere)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    return sql.chargerListeIdAdresseDocument(pCritere);
  }
  
  /**
   * Charger la liste des adresses de document de ventes correspondants à la liste de leurs identifiants.
   */
  @Override
  public ListeAdresseDocument chargerListeAdresseDocument(IdSession pIdSession, List<IdAdresseDocument> pListeIdAdresseDocument)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    return sql.chargerListeAdresseDocument(pListeIdAdresseDocument);
  }
  
  /**
   * 
   */
  @Override
  public void creerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    sql.creerAdresseDocument(pAdresseDocument);
  }
  
  @Override
  public int chercherPremierNumeroAdresseLibre(IdSession pIdSession, IdDocumentVente pIdDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    return sql.chercherPremierNumeroAdresseLibre(pIdDocument);
  }
  
  @Override
  public void supprimerAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    sql.supprimerAdresseDocument(pAdresseDocument);
  }
  
  @Override
  public void sauverAdresseDocument(IdSession pIdSession, AdresseDocument pAdresseDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlAdresseDocument sql = new SqlAdresseDocument(sessionServeur.getQueryManager());
    sql.sauverAdresseDocument(pAdresseDocument);
  }
  
  /**
   * Charge une liste d'identifiants de plans de pose Sipe.
   */
  @Override
  public List<IdPlanPoseSipe> chargerListeIdPlanPoseSipe(IdSession pIdSession, CritereSipe pCritereSipe, boolean pMiseAJourDepuisIfs)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ImportationPlanPoseSipe importationPlanPoseSipe =
        new ImportationPlanPoseSipe(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    return importationPlanPoseSipe.chargerListeIdPlanPoseSipe(pCritereSipe, pMiseAJourDepuisIfs);
  }
  
  /**
   * Charge une liste de plans de pose Sipe à partir d'une liste d'identifiants.
   */
  @Override
  public ListePlanPoseSipe chargerListePlanPoseSipe(IdSession pIdSession, List<IdPlanPoseSipe> pListeIpPlanPose) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ImportationPlanPoseSipe importationPlanPoseSipe =
        new ImportationPlanPoseSipe(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    return importationPlanPoseSipe.chargerListePlanPoseSipe(pListeIpPlanPose);
  }
  
  /**
   * Importe un plan de pose Sipe à partir du plan sélectionné.
   */
  @Override
  public void importerPlanPoseSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe, IdDocumentVente pIdDocument) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ImportationPlanPoseSipe importationPlanPoseSipe =
        new ImportationPlanPoseSipe(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    importationPlanPoseSipe.injecterPlanPose(pPlanPoseSipe, pIdDocument);
  }
  
  /**
   * Retourne le dossier des fichiers Sipe stockés dans l'IFS.
   */
  @Override
  public String retournerDossierSipe(IdSession pIdSession, EnumTypePlanPoseSipe pTypePlanPoseSipe, boolean pDossierSauvegarde)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ImportationPlanPoseSipe importationPlanPoseSipe =
        new ImportationPlanPoseSipe(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    return importationPlanPoseSipe.retournerDossierSipe(pTypePlanPoseSipe, pDossierSauvegarde);
  }
  
  /**
   * Supprime un fichier Sipe donné.
   */
  @Override
  public void supprimerFichierSipe(IdSession pIdSession, PlanPoseSipe pPlanPoseSipe) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    ImportationPlanPoseSipe importationPlanPoseSipe =
        new ImportationPlanPoseSipe(sessionServeur.getSystemManager(), sessionServeur.getQueryManager());
    importationPlanPoseSipe.supprimerFichier(pPlanPoseSipe);
  }
  
}
