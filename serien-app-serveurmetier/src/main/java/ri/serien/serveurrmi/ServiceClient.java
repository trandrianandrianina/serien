/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.serveurrmi;

import java.lang.reflect.Proxy;
import java.rmi.RemoteException;
import java.util.List;

import ri.serien.libas400.dao.exp.programs.contact.GM_LienContactAvecTiers;
import ri.serien.libas400.dao.gvm.programs.client.GM_Client;
import ri.serien.libas400.dao.gvm.programs.client.M_Client;
import ri.serien.libas400.dao.rpg.gescom.client.creation.RpgSauverClientComptant;
import ri.serien.libas400.dao.rpg.gescom.client.lecture.RpgChargerEncoursClient;
import ri.serien.libas400.dao.rpg.gescom.client.modification.RpgSauverClient;
import ri.serien.libas400.dao.rpg.gescom.documentvente.RpgDocumentVente;
import ri.serien.libas400.dao.rpg.gescom.historiqueventesclientarticle.RpgHistoriqueVentesClientArticle;
import ri.serien.libas400.dao.sql.gescom.client.banque.SqlBanqueClient;
import ri.serien.libas400.dao.sql.gescom.client.recherche.SqlDocumentsClient;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libcommun.gescom.achat.document.ligneachat.IdLigneAchat;
import ri.serien.libcommun.gescom.commun.article.IdArticle;
import ri.serien.libcommun.gescom.commun.client.Client;
import ri.serien.libcommun.gescom.commun.client.CritereClient;
import ri.serien.libcommun.gescom.commun.client.CriteresRechercheDocumentsHistoriqueVentesClientArticle;
import ri.serien.libcommun.gescom.commun.client.DocumentHistoriqueVenteClientArticle;
import ri.serien.libcommun.gescom.commun.client.EncoursClient;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.client.ListeClientBase;
import ri.serien.libcommun.gescom.commun.client.ListeIdClient;
import ri.serien.libcommun.gescom.commun.contact.ListeContact;
import ri.serien.libcommun.gescom.vente.chantier.Chantier;
import ri.serien.libcommun.gescom.vente.chantier.CritereChantier;
import ri.serien.libcommun.gescom.vente.chantier.IdChantier;
import ri.serien.libcommun.gescom.vente.chantier.ListeChantier;
import ri.serien.libcommun.gescom.vente.document.DocumentVente;
import ri.serien.libcommun.gescom.vente.ligne.IdLigneVente;
import ri.serien.libcommun.gescom.vente.reglement.CritereBanqueClient;
import ri.serien.libcommun.gescom.vente.reglement.ListeBanqueClient;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.session.IdSession;
import ri.serien.libcommun.rmi.InterfaceServiceClient;
import ri.serien.serveurrmi.clientdesktop.ManagerClientDesktop;

/**
 * Implémentation du service client.
 */
public class ServiceClient implements InterfaceServiceClient {
  private static InterfaceServiceClient service;
  
  /**
   * Constructeur privé pour empêcher l'instanciation autrement que par getService().
   */
  private ServiceClient() {
  }
  
  /**
   * Retourner l'implémentation du service client.
   */
  protected static InterfaceServiceClient getService() {
    if (service == null) {
      InterfaceServiceClient interfaceService = new ServiceClient();
      service = (InterfaceServiceClient) Proxy.newProxyInstance(interfaceService.getClass().getClassLoader(),
          new Class<?>[] { InterfaceServiceClient.class }, new InvocationHandlerServeurRMI(interfaceService));
    }
    return service;
  }
  
  /**
   * Permet de tester que le service Document est actif.
   */
  @Override
  public boolean ping() throws RemoteException {
    return true;
  }
  
  /**
   * Recherche les clients, avec leurs seules informations de base, correspondants aux critères passés.
   */
  @Override
  public ListeClientBase chargerListeClientBase(IdSession pIdSession, CritereClient pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    GM_Client gmclient = new GM_Client(querym);
    return gmclient.chargerListeClientBase(pCriteres);
  }
  
  /**
   * Charger une liste simplifiée de clients à partir d'une liste d'identifiants.
   */
  @Override
  public ListeClientBase chargerListeClientBase(IdSession pIdSession, ListeIdClient pListeIdClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    GM_Client gmclient = new GM_Client(querym);
    return gmclient.chargerListeClientBase(pListeIdClient);
  }
  
  /**
   * Lit les données (partielles ou non) d'un client donné.
   */
  @Override
  public Client chargerClient(IdSession pIdSession, IdClient pIdClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    
    // Charger le client
    M_Client mclient = new M_Client(querym);
    Client client = mclient.load(pIdClient);
    if (client == null) {
      // throw new MessageErreurException("Aucune information n'a été trouvée pour ce client");
      return null;
    }
    
    // Charger le contact principal du client
    client.chargerContactPrincipal(pIdSession);
    return client;
  }
  
  /**
   * Charge une liste d'identifiants chantiers à partir de critères donnés
   */
  @Override
  public List<IdChantier> chargerListeIdChantier(IdSession pIdSession, CritereChantier pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentsClient sql = new SqlDocumentsClient(sessionServeur.getQueryManager());
    return sql.chargerListeIdChantier(pCriteres);
  }
  
  /**
   * Charge les informations complètes des chantiers à partir d'une liste d'identifiants chantier
   */
  @Override
  public ListeChantier chargerListeChantier(IdSession pIdSession, List<IdChantier> pListeIdChantier) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentsClient sql = new SqlDocumentsClient(sessionServeur.getQueryManager());
    return sql.chargerListeChantier(pListeIdChantier);
  }
  
  /**
   * Charge les informations complètes d'un chantier à partir de son identifiant
   */
  @Override
  public Chantier chargerChantier(IdSession pIdSession, IdChantier pIdChantier) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentsClient sql = new SqlDocumentsClient(sessionServeur.getQueryManager());
    return sql.chargerChantier(pIdChantier);
  }
  
  /**
   * Charge la ligne d'achat générée pour approvisionner une ligne de vente
   */
  @Override
  public IdLigneAchat chargerLienLigneVenteLigneAchat(IdSession pIdSession, IdLigneVente pIdLigneVente) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    SqlDocumentsClient sql = new SqlDocumentsClient(sessionServeur.getQueryManager());
    return sql.chargerLienLigneVenteLigneAchat(pIdLigneVente);
  }
  
  /**
   * Lit l'encours d'un client donné.
   */
  @Override
  public EncoursClient chargerEncoursClient(IdSession pIdSession, IdClient pIdClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgChargerEncoursClient traitementEncours = new RpgChargerEncoursClient();
    return traitementEncours.chargerEncoursClient(sessionServeur.getSystemManager(), pIdClient);
  }
  
  /**
   * Controle l'encours du client à partir d'un document donné.
   */
  @Override
  public EncoursClient controlerEncoursClient(IdSession pIdSession, DocumentVente pDocumentVente, int pCollectifComptableClient)
      throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgDocumentVente traitementEncours = new RpgDocumentVente();
    EncoursClient encoursclient =
        traitementEncours.controlerEncoursClient(sessionServeur.getSystemManager(), pDocumentVente, pCollectifComptableClient);
    if (encoursclient == null) {
      throw new MessageErreurException("Aucune information d'encours n'a été trouvée pour ce client");
    }
    return encoursclient;
  }
  
  /**
   * Retourne la liste des contacts d'un client donné.
   */
  @Override
  public ListeContact chargerListeContact(IdSession pIdSession, IdClient pIdClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    GM_LienContactAvecTiers gmlientiers = new GM_LienContactAvecTiers(querym);
    return gmlientiers.chargerListeContact(pIdClient);
  }
  
  /**
   * Créé un client comptant.
   */
  @Override
  public Client sauverClientComptant(IdSession pIdSession, Client pClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverClientComptant operation = new RpgSauverClientComptant();
    pClient = operation.sauverClientComptant(sessionServeur.getSystemManager(), pClient);
    if (pClient == null) {
      throw new MessageErreurException("Le client n'a pas pu être créé");
    }
    
    // Il faut charger le client qui vient d'être créé car le service RPG initialise des champs qui ne sont pas renseignés en java
    // (cela peut poser des problèmes par exemple lors du règlement différé car le client est sauvegardé avec des données partielles)
    pClient = chargerClient(pIdSession, pClient.getId());
    
    return pClient;
  }
  
  /**
   * Modifie les informations d'un client donné.
   */
  @Override
  public void sauverClient(IdSession pIdSession, Client pClient) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    RpgSauverClient operation = new RpgSauverClient();
    operation.sauverClient(sessionServeur.getSystemManager(), pClient);
  }
  
  /**
   * Recherche les documents d'un client contenant un article donné.
   */
  @Override
  public List<DocumentHistoriqueVenteClientArticle> chargerDocumentHistoriqueVenteClientArticle(IdSession pIdSession, IdClient pIdClient,
      IdArticle pIdArticle, CriteresRechercheDocumentsHistoriqueVentesClientArticle pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    
    RpgHistoriqueVentesClientArticle traitementHistorique = new RpgHistoriqueVentesClientArticle();
    List<DocumentHistoriqueVenteClientArticle> histo = traitementHistorique
        .chargerDocumentHistoriqueVenteClientArticle(sessionServeur.getSystemManager(), pIdClient, pIdArticle, pCriteres);
    if (histo == null) {
      throw new MessageErreurException("L'historique des ventes client article n'a pas pu être lu");
    }
    return histo;
  }
  
  /**
   * Recherche liste des banques des clients.
   */
  @Override
  public ListeBanqueClient chargerListeBanqueClient(IdSession pIdSession, CritereBanqueClient pCriteres) throws RemoteException {
    SessionServeur sessionServeur = ManagerClientDesktop.getSession(pIdSession);
    QueryManager querym = sessionServeur.getQueryManager();
    SqlBanqueClient sqlBanqueClient = new SqlBanqueClient(querym);
    return sqlBanqueClient.chargerListeIdBanqueClient(pCriteres);
  }
  
}
