/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.tomcat;

import java.io.File;

import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.serveurmetier.serveur.ManagerAS400;

/**
 * Cette classe regroupe les méthodes pour gérer tomcat.
 */
public class Tomcat implements InterfaceModule {
  // Variables
  private static EnumModule enumModule = EnumModule.TOMCAT;
  private static boolean moduleActif = false;
  private static ListeParametreModule listeParametre = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return listeParametre;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return false;
  }
  
  /**
   * Démarre le module Tomcat.
   */
  @Override
  public void demarrer() {
    if (moduleActif) {
      Trace.alerte("Le module " + enumModule + " est déjà actif.");
      return;
    }
    Trace.info("Démarrage du module " + enumModule + '.');
    
    // Initialisation et contrôle des paramètres
    initialiserParametre();
    
    // Démarrage de Tomcat
    String cheminDemarrage = ManagerAS400.getDossierRacineServeur() + "/web/tomcat/bin/startup.sh";
    executerScript(cheminDemarrage);
    
    moduleActif = true;
    Trace.info("Le module " + enumModule + " est démarré.");
  }
  
  /**
   * Arrête le module Tomcat.
   */
  @Override
  public void arreter() {
    if (!moduleActif) {
      Trace.alerte("Le module " + enumModule + " n'est pas actif.");
      return;
    }
    
    // Arrêt de Tomcat
    String cheminArret = ManagerAS400.getDossierRacineServeur() + "/web/tomcat/bin/shutdown.sh";
    executerScript(cheminArret);
    
    moduleActif = false;
    Trace.info("Module " + enumModule + " stoppé.");
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return moduleActif;
  }
  
  /**
   * Envoi un ordre au module Tomcat.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + enumModule + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case TOMCAT_START:
          // Recherche les paramètres pour ce module dans le cas où ils auraient été modifié
          ManagerParametreModule.rechargerParametre(pEnumControleModule.getEnumModule());
          // Démarre le module
          demarrer();
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return null;
        
        case TOMCAT_STOP:
          arreter();
          return null;
        
        case TOMCAT_STATUS:
          String retour;
          if (moduleActif) {
            retour = "Le module " + enumModule + " est démarré.";
          }
          else {
            retour = "Le module " + enumModule + " est arrêté.";
          }
          Trace.info(retour);
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return retour;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + enumModule + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser les paramètres du module à partir de la liste des paramètres pour ce module.
   */
  private void initialiserParametre() {
    // Extraction des paramètres spécifiques pour ce module
    listeParametre = ManagerParametreModule.extraireParametreModule(enumModule);
  }
  
  /**
   * Exécution d'un script sh.
   */
  private void executerScript(String pCheminCompletScript) {
    pCheminCompletScript = Constantes.normerTexte(pCheminCompletScript);
    if (pCheminCompletScript.isEmpty()) {
      throw new MessageErreurException("Le script a exécuter est invalide.");
    }
    File script = new File(pCheminCompletScript);
    if (!script.exists()) {
      throw new MessageErreurException("Le script " + pCheminCompletScript + " a exécuter est introuvable.");
    }
    
    // Exécution du script
    String[] cmd = { pCheminCompletScript };
    try {
      Trace.info("Lancement du script " + pCheminCompletScript);
      Runtime.getRuntime().exec(cmd, null);
    }
    catch (Exception e) {
      throw new MessageErreurException(e, "Erreur lors de l'exécution du fichier script " + pCheminCompletScript + '.');
    }
  }
}
