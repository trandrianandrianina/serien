/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rendu;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.URI;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.avalon.framework.configuration.Configuration;
import org.apache.avalon.framework.configuration.ConfigurationException;
import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopConfParser;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.FopFactoryBuilder;
import org.apache.fop.apps.MimeConstants;
import org.apache.xalan.processor.TransformerFactoryImpl;
import org.xml.sax.SAXException;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;

/**
 * Génère le fichier final à partie d'une description FO
 */
public class GenerationFichierFinal {
  // Variables
  private EnumTypeDocument typeDocument = EnumTypeDocument.PDF;
  private String typeSortie = MimeConstants.MIME_PDF;
  private String fichierEntree = "output.fo";
  private String fichierSortie = null;
  private String configFile = "cfg.xml"; // Fichier cfg.xml contenant la config pour la transformation
  
  /**
   * Constructeur
   */
  public GenerationFichierFinal() {
  }
  
  /**
   * @return the typeSortie
   */
  public String getTypeSortie() {
    return typeSortie;
  }
  
  public void setTypeSortie(EnumTypeDocument pTypeDocument) {
    if (pTypeDocument == null) {
      return;
    }
    
    typeDocument = pTypeDocument;
    switch (pTypeDocument) {
      case PDF:
        typeSortie = MimeConstants.MIME_PDF;
        break;
      case POSTSCRIPT:
        typeSortie = MimeConstants.MIME_POSTSCRIPT;
        break;
      case PCL:
        typeSortie = MimeConstants.MIME_PCL;
        break;
      case TIFF:
        typeSortie = MimeConstants.MIME_TIFF;
        break;
      case PNG:
        typeSortie = MimeConstants.MIME_PNG;
        break;
      case AWT:
        typeSortie = MimeConstants.MIME_FOP_AWT_PREVIEW;
        break;
    }
  }
  
  public String getFichierEntree() {
    return fichierEntree;
  }
  
  public void setFichierEntree(String fichierEntree) {
    this.fichierEntree = fichierEntree;
  }
  
  public String getFichierSortie() {
    return fichierSortie;
  }
  
  /**
   * Note: il est préferable d'exécuter setTypeSortie avant cette méthode
   */
  public void setFichierSortie(String pFichiersortie) {
    pFichiersortie = Constantes.normerTexte(pFichiersortie);
    if (pFichiersortie.isEmpty()) {
      return;
    }
    fichierSortie = pFichiersortie;
    
    // On vérifie qu'il n'y est pas déjà l'extension
    if (!fichierSortie.toLowerCase().endsWith(typeDocument.getExtension())) {
      fichierSortie += '.' + typeDocument.getExtension();
    }
  }
  
  /**
   * Retourne le fichier de configuration.
   * @param pCheminFichierConfig
   * @return
   * @throws ConfigurationException
   * @throws SAXException
   * @throws IOException
   */
  private FopFactoryBuilder getConfigurationFile(String pCheminFichierConfig) {
    try {
      File cfg = null;
      if (pCheminFichierConfig == null) {
        cfg = new File("cfg.xml");
        if (cfg.exists()) {
          FopConfParser parser = new FopConfParser(cfg);
          // Trace.alerte("-1-" + cfg);
          return parser.getFopFactoryBuilder();
        }
      }
      else {
        cfg = new File(pCheminFichierConfig);
        if (cfg.exists()) {
          FopConfParser parser = new FopConfParser(cfg);
          // Trace.alerte("-2-" + cfg);
          return parser.getFopFactoryBuilder();
        }
        else {
          cfg = new File(pCheminFichierConfig.replaceAll("sim", "lib"));
          if (cfg.exists()) {
            FopConfParser parser = new FopConfParser(cfg);
            // Trace.alerte("-3-" + cfg);
            return parser.getFopFactoryBuilder();
          }
          else {
            Configuration configuration = (Configuration) getClass().getClassLoader().getResourceAsStream("/cfg/" + configFile);
            // Trace.alerte("-4-" + configuration);
            return new FopFactoryBuilder(URI.create("/")).setConfiguration(configuration);
          }
        }
      }
    }
    catch (Exception e) {
    }
    return null;
  }
  
  /**
   * Conversion du document depuis le serveur.
   * @return
   */
  public String renduFinal(String dossierCfg, String dossiertravail, String dataFO, float dpisrc, float dpidest) {
    if ((dataFO == null) || (dossiertravail == null)) {
      return null;
    }
    
    OutputStream out = null;
    // File dossierSim = new File(dossiertravail);
    
    // Step 1: Construct a FopFactory
    // (reuse if you plan to render multiple documents!)
    // FopFactory fopFactory = FopFactory.newInstance();
    
    // Step 2: Set up output stream.
    // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
    try {
      if (fichierSortie == null) {
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + typeDocument.getExtension());
      }
      out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      // Configuration cfg = getConfigurationFile(dossierSim.getParent() + File.separatorChar + configFile);
      // if (cfg != null) fopFactory.setUserConfig(cfg);
      
      // Step 3: Construct fop with desired output format
      // fopFactory.setBaseURL(dossiertravail);
      // fopFactory.setSourceResolution(dpisrc);
      // fopFactory.setTargetResolution(dpidest);
      
      // TODO attention crash si le dossiertravail ne contient rien
      FopFactoryBuilder builder = getConfigurationFile(dossierCfg + File.separatorChar + configFile);
      builder.setBaseURI(new File(dossiertravail).toURI());
      builder.setSourceResolution(dpisrc);
      builder.setTargetResolution(dpidest);
      FopFactory fopFactory = builder.build();
      
      Fop fop = fopFactory.newFop(typeSortie, out);
      // Fop fop = fopFactory.newFop(typeSortie, userAgent, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(new StringReader(dataFO));
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      Trace.erreur(e, "[GenerationFichierFinal]-(renduFinal)-> Erreur : ");
      return null;
    }
    
    return fichierSortie;
  }
  
  /**
   * Conversion du document depuis le rad.
   * @return
   */
  public String renduFinal(float dpisrc, float dpidest) {
    if (fichierEntree == null) {
      return null;
    }
    
    OutputStream out = null;
    File entree = new File(fichierEntree);
    
    // Step 1: Construct a FopFactory
    // (reuse if you plan to render multiple documents!)
    // FopFactory fopFactory = FopFactory.newInstance(new File(entree.getParent()).toURI()); // Attention avec la
    // version fop-1.1rc1 cela ne fonctionne plus
    
    // Step 2: Set up output stream.
    // Note: Using BufferedOutputStream for performance reasons (helpful with FileOutputStreams).
    try {
      if (fichierSortie == null) {
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + typeDocument.getExtension());
      }
      out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      /*
      URL url = getClass().getResource("/cfg/" + configFile);
      String chemin = url.getFile();
      File cfg = new File(chemin);
      if (!cfg.exists()) {
        if (chemin.contains(".jar!")) {
          cfg = new File(configFile);
        }
        else {
          chemin = chemin.substring(6);
          cfg = new File(chemin);
        }
      }
      FopConfParser parser = new FopConfParser(cfg);
      FopFactoryBuilder builder = parser.getFopFactoryBuilder();*/
      FopFactoryBuilder builder = getConfigurationFile("/cfg/" + configFile);
      builder.setBaseURI(new File(entree.getParent()).toURI());
      builder.setSourceResolution(dpisrc);
      builder.setTargetResolution(dpidest);
      FopFactory fopFactory = builder.build();
      
      // Step 3: Construct fop with desired output format
      // fopFactory.setBaseURL(entree.getParent());
      // fopFactory.setSourceResolution(dpisrc);
      // fopFactory.setTargetResolution(dpidest);
      Fop fop = fopFactory.newFop(typeSortie, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(entree);
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'appel de la méthode renduFinal.");
      return null;
    }
    
    return fichierSortie;
  }
  
  private Transformer getTransformer(StreamSource streamSource) {
    // setup the xslt transformer
    TransformerFactoryImpl impl = new TransformerFactoryImpl();
    
    try {
      return impl.newTransformer(streamSource);
      
    }
    catch (TransformerConfigurationException e) {
      Trace.erreur(e, "Erreur lors de l'appel de la méthode getTransformer.");
    }
    return null;
  }
  
  public String renduFinal2(String dossiersim, String xmlfile, String xslfile) {
    File xsltfile = new File(xslfile);
    // the XML file from which we take the name
    StreamSource source = new StreamSource(new File(xmlfile));
    
    // creation of transform source
    StreamSource transformSource = new StreamSource(xsltfile);
    // create an instance of fop factory
    FopFactory fopFactory = FopFactory.newInstance(new File(dossiersim).toURI());
    // a user agent is needed for transformation
    FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
    // to store output
    ByteArrayOutputStream outStream = new ByteArrayOutputStream();
    if (fichierSortie == null) {
      fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + typeDocument.getExtension());
    }
    
    Transformer xslfoTransformer;
    try {
      xslfoTransformer = getTransformer(transformSource);
      // Construct fop with desired output format
      Fop fop;
      try {
        // Configuration cfg = getConfigurationFile(dossiersim + File.separatorChar + configFile);
        // if (cfg != null) fopFactory.setUserConfig(cfg);
        
        // fopFactory.setBaseURL(dossiersim);
        // Configuration.put("baseDir", dossiertravail);
        fop = fopFactory.newFop(typeSortie, foUserAgent, outStream);
        // Resulting SAX events (the generated FO) must be piped through to FOP
        Result res = new SAXResult(fop.getDefaultHandler());
        
        // Start XSLT transformation and FOP processing
        try {
          // everything will happen here..
          xslfoTransformer.transform(source, res);
          // if you want to get the PDF bytes, use the following code
          // return outStream.toByteArray();
          
          // if you want to save PDF file use the following code
          File pdffile = new File(fichierSortie);
          OutputStream out = new FileOutputStream(pdffile);
          out = new java.io.BufferedOutputStream(out);
          FileOutputStream str = new FileOutputStream(pdffile);
          str.write(outStream.toByteArray());
          str.close();
          out.close();
        }
        catch (Exception e) {
          Trace.erreur(e, "Erreur lors de l'appel de la méthode renduFinal2 (étape 3).");
        }
      }
      catch (FOPException e) {
        Trace.erreur(e, "Erreur lors de l'appel de la méthode renduFinal2 (étape 2)");
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'appel de la méthode renduFinal2 (étape 1)");
    }
    
    return fichierSortie;
  }
  
  /**
   * Conversion du document
   */
  public String renduFinal(String dossiertravail, String xmlfile, String xslfile) {
    if (xmlfile == null || xslfile == null || dossiertravail == null) {
      return null;
    }
    
    // the XSL FO file
    File xsltFile = new File(xslfile);
    
    // the XML file which provides the input
    // StreamSource xmlSource = new StreamSource(new File(xmlfile));
    
    File dossierSim = new File(dossiertravail);
    
    try {
      // FopFactory fopFactory = FopFactory.newInstance(new File(dossiertravail).toURI());
      
      // Setup output
      if (fichierSortie == null) {
        fichierSortie = fichierEntree.replaceAll("\\.fo", "\\." + typeDocument.getExtension());
      }
      OutputStream out = new BufferedOutputStream(new FileOutputStream(new File(fichierSortie)));
      
      // Configuration cfg = getConfigurationFile(dossierSim.getParent() + File.separatorChar + configFile);
      // if (cfg != null) fopFactory.setUserConfig(cfg);
      
      // Step 3: Construct fop with desired output format
      // fopFactory.setBaseURL(dossiertravail);
      // fopFactory.setSourceResolution(dpisrc);
      // fopFactory.setTargetResolution(dpidest);
      FopFactoryBuilder builder = getConfigurationFile(dossierSim.getParent() + File.separatorChar + configFile);
      builder.setBaseURI(new File(dossiertravail).toURI());
      FopFactory fopFactory = builder.build();
      
      // a user agent is needed for transformation
      FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
      
      // Fop fop = fopFactory.newFop(typeSortie, out);
      Fop fop = fopFactory.newFop(typeSortie, foUserAgent, out);
      
      // Step 4: Setup JAXP using identity transformer
      TransformerFactory factory = TransformerFactory.newInstance();
      Transformer transformer = factory.newTransformer(new StreamSource(xsltFile)); // identity transformer
      
      // Step 5: Setup input and output for XSLT transformation
      // Setup input stream
      Source src = new StreamSource(new StringReader(xmlfile));
      
      // Resulting SAX events (the generated FO) must be piped through to FOP
      Result res = new SAXResult(fop.getDefaultHandler());
      
      // Step 6: Start XSLT transformation and FOP processing
      transformer.transform(src, res);
      out.close();
    }
    catch (Exception e) {
      Trace.erreur(e, "-[GenerationFichierFinal]-(renduFinal)-> Erreur : ");
      return null;
    }
    
    return fichierSortie;
  }
  
}
