/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.rendu;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.libcommun.exploitation.edition.SpoolOld;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.newsim.description.DescriptionCodeBarre;
import ri.serien.newsim.description.DescriptionCondition;
import ri.serien.newsim.description.DescriptionDocument;
import ri.serien.newsim.description.DescriptionImage;
import ri.serien.newsim.description.DescriptionLabel;
import ri.serien.newsim.description.DescriptionPage;
import ri.serien.newsim.description.DescriptionVariable;

/**
 * Génère un fichier FO à partir d'une description de document (.dds)
 */
public class GenerationFichierFO {
  // Constantes
  private static final String NOIMAGE = null;
  private static final String NONAME = "spool";
  private static final String EXTENSION = ".fo";
  private static final String PREFIXE_PAGE_MASTER = "A4-";
  
  public static final double HAUTEUR = ConstantesNewSim.A4_HEIGHT;
  public static final double LARGEUR = ConstantesNewSim.A4_WIDTH;
  // On calcule les dimensions avec prise en compte de 0,5 cm de marge de chaque côté
  public static final double HAUTEUR_MOINS_MARGES = ConstantesNewSim.A4_HEIGHT - 1.5;
  public static final double LARGEUR_MOINS_MARGES = ConstantesNewSim.A4_WIDTH - 1.5;
  
  // Variables
  private String encoding = System.getProperty("file.encoding");
  // private boolean isWindows = System.getProperty("os.name").startsWith("Windows");
  private DescriptionDocument document = null;
  private String[] listing = null;
  private SpoolOld spoolOld = null;
  private ArrayList<String> docfo = new ArrayList<String>();
  private double hauteurCm = ConstantesNewSim.A4_HEIGHT;
  private double largeurCm = ConstantesNewSim.A4_WIDTH;
  private Font police = null;
  private String dossierTravail = "";
  private InterpreterData id = new InterpreterData();
  private String nomFichierFO = null;
  private boolean affineLigneColonne = false;
  
  // private int compteur=0;
  
  private HashMap<String, DescriptionCondition> listeConditions = new HashMap<String, DescriptionCondition>();
  private ArrayList<DescriptionVariable> listeVariables = null;
  private ArrayList<DescriptionPage> listeDescriptionPage = new ArrayList<DescriptionPage>();
  
  // A recalculer car sous AS/400 les polices sont un peu plus grandes (calculer avec le programme test26)
  // private int[][] taillepolicewh_MACOSX={{2, 4}, {3, 6}, {4, 7}, {4, 8}, {5, 8}, {5, 10}, {6, 11}, {7, 12}, {7, 12}};
  // private int[][] taillepolicewh_LINUX={{2, 5}, {3, 7}, {4, 8}, {4, 9}, {5, 10}, {5, 12}, {6, 13}, {7, 14}, {7, 15}};
  // private int[][] taillepolicewh_WINDOWS =
  // { { 2, 6 }, { 3, 7 }, { 4, 8 }, { 4, 10 }, { 5, 11 }, { 5, 12 }, { 6, 14 }, { 7, 16 }, { 7, 17 } };
  
  /**
   * Constructeur
   * @param doc
   */
  public GenerationFichierFO(DescriptionDocument doc, String[] spool) {
    this(doc, spool, "");
  }
  
  /**
   * Constructeur
   * @param doc
   */
  public GenerationFichierFO(DescriptionDocument doc, String[] listing, String dossierTrav) {
    setDocument(doc);
    setListing(listing);
    setDossierTravail(dossierTrav);
    docfo.clear();
  }
  
  /**
   * Constructeur dans le cas où il n'y a pas de description
   * @param doc
   * 
   *          public GenerationFichierFO(int nbrligne, int nbrcolonne, String[] spool)
   *          {
   *          this(nbrligne, nbrcolonne, spool, "", false);
   *          }
   */
  
  /**
   * Constructeur dans le cas où il n'y a pas de description
   * @param doc
   */
  // public GenerationFichierFO(int nbrligne, int nbrcolonne, String[] spool, String dossierTrav, boolean affinecol)
  public GenerationFichierFO(SpoolOld spoolOld, String dossierTrav, boolean affinecol) {
    setSpool(spoolOld);
    setListing(spoolOld.getPages());
    setDossierTravail(dossierTrav);
    setAffineCalculLigneColonne(true);
    if (spoolOld.isPortrait()) {
      setDimension(HAUTEUR, LARGEUR);
    }
    else {
      setDimension(LARGEUR, HAUTEUR);
    }
    
    // En fonction du nombre de caractères par ligne/colonne on calibre la page
    if (police == null) {
      // police = new Font("Courier", Font.PLAIN, getTaillePolice(spool.getNbrLignes(), spool.getNbrColonnes(),
      // spool.isPortrait()));
      police = new Font("Monospaced", Font.PLAIN,
          getTaillePolice("Monospaced", spoolOld.getNbrLignes(), spoolOld.getNbrColonnes(), spoolOld.isPortrait()));
    }
    docfo.clear();
  }
  
  /**
   * Initialise la variable qui indique si on doit affiner les colonnes (largeur page)
   * @param affinecol
   */
  public void setAffineCalculLigneColonne(boolean affine) {
    affineLigneColonne = affine;
  }
  
  /**
   * Recherche au mieux la ligne maximum
   * @param nbrcol
   * @return
   */
  private int getAffineLigne(int nbrlig) {
    if (listing == null) {
      return nbrlig;
    }
    
    String[] ligne = null;
    nbrlig = 0;
    for (int i = 0; i < listing.length; i++) {
      ligne = listing[i].split(Constantes.crlf);
      if (nbrlig < ligne.length) {
        nbrlig = ligne.length;
      }
    }
    return nbrlig;
  }
  
  /**
   * Recherche au mieux la colonne maximum (tout en faisant un trim des blancs à droite)
   * @param nbrcol
   * @return
   */
  private int getAffineColonne(int nbrcol) {
    if (listing == null) {
      return nbrcol;
    }
    
    int val = 0;
    String[] lignes = null;
    StringBuffer newpage = new StringBuffer();
    String chaine;
    nbrcol = 0;
    for (int i = 0; i < listing.length; i++) {
      newpage.setLength(0);
      lignes = listing[i].split(Constantes.crlf);
      for (int j = 0; j < lignes.length; j++) {
        chaine = getTrimR(lignes[j]);
        newpage.append(chaine).append(Constantes.crlf);
        val = chaine.length();
        if (nbrcol < val) {
          nbrcol = val;
        }
      }
      listing[i] = newpage.toString();
    }
    return nbrcol;
  }
  
  /**
   * Evalue la taille de la police à utiliser et l'orientation portrait/paysage (dans le cas où il n'y a pas de description).
   */
  private int getTaillePolice(String pNomPolice, int nbrligne, int nbrcolonne, boolean isportrait) {
    int tailleMini = 4;
    int tailleMaxi = 18;
    
    // Si l'option est activé on recherche le nombre de caractères réellement utilisés
    if (affineLigneColonne) {
      nbrligne = getAffineLigne(nbrligne);
      nbrcolonne = getAffineColonne(nbrcolonne);
    }
    
    // Récupération des dimensions pour la police Monospaced utilisé dans ce cas de figure
    BufferedImage bi = new BufferedImage(593, 840, BufferedImage.TYPE_INT_ARGB);
    Graphics2D g = bi.createGraphics();
    int dpi = 96;
    try {
      // Ne fonctionne pas en mode headless (à voir si un jour on trouve une solution)
      dpi = Toolkit.getDefaultToolkit().getScreenResolution();
    }
    catch (HeadlessException e) {
      dpi = 72;
    }
    int largeurPixelA4;
    int hauteurPixelA4;
    if (isportrait) {
      largeurPixelA4 = (int) ((LARGEUR_MOINS_MARGES * dpi) / ConstantesNewSim.INCH);
      hauteurPixelA4 = (int) ((HAUTEUR_MOINS_MARGES * dpi) / ConstantesNewSim.INCH);
      Trace.info("Taille du dpi utilisé=" + dpi + " en mode portrait");
    }
    else {
      largeurPixelA4 = (int) ((HAUTEUR_MOINS_MARGES * dpi) / ConstantesNewSim.INCH);
      hauteurPixelA4 = (int) ((LARGEUR_MOINS_MARGES * dpi) / ConstantesNewSim.INCH);
      Trace.info("Taille du dpi utilisé=" + dpi + " en mode paysage");
    }
    
    for (int i = tailleMaxi; --i >= tailleMini;) {
      // FontMetrics fm = g.getFontMetrics(new Font("Courier New", Font.PLAIN, i));
      FontMetrics fm = g.getFontMetrics(new Font(pNomPolice, Font.PLAIN, i));
      // 05/05/2018 j'ai supprimé les 2 pixels car sinon la police est trop petite il faudra ajuster avec le nombre de ligne/colonne du
      // spool pour ajuster la polce
      // On ajoute 2 pixels sur la largeur et la hauteur
      // (vu sur le site d'Oracle https://docs.oracle.com/javase/tutorial/2d/text/measuringtext.html)
      int largeurLigne = ((nbrcolonne * fm.charWidth('M')));
      int hauteurLigne = ((nbrligne * fm.getHeight()));
      if (largeurLigne <= largeurPixelA4 && hauteurLigne <= hauteurPixelA4) {
        Trace.info("Taille de la police pour l'édition sans description=" + i);
        return i;
      }
    }
    
    return tailleMini;
  }
  
  /*
  // * Il s'agit de l'algo original, je ne l'efface pas en attendant de voir le comportement en clientèle (car c'est assez sensible)
  private int getTaillePolice(int nbrligne, int nbrcolonne, boolean isportrait) {
    double taille = 0;
    
    if (affineLigneColonne) {
      nbrligne = getAffineLigne(nbrligne);
      nbrcolonne = getAffineColonne(nbrcolonne);
    }
    
    double taille_h = 0;
    double taille_l = 0;
    int deltaportrait = 5; // Valeur pifométrique qui permet de calculer la taille de la police "idéale" (valeur précédente 4)
    int deltapaysage = 5; // Valeur pifométrique qui permet de calculer la taille de la police "idéale" (valeur précédente 4)
    
    // Calcul de la taille d'un caractère avec la police "Monospaced"
    
    for (int i = 0; i < taillepolicewh_WINDOWS.length; i++) {
      taille_l = nbrcolonne * taillepolicewh_WINDOWS[i][0];
      taille_h = nbrligne * taillepolicewh_WINDOWS[i][1];
      
      if (isportrait && (taille_l <= LARGEUR_AVEC_1CM_MARGE) && (taille_h <= HAUTEUR_AVEC_1CM_MARGE)) {
        taille = i + deltaportrait;
      }
      else {
        if (!isportrait && (taille_h <= LARGEUR_AVEC_1CM_MARGE) && (taille_l <= HAUTEUR_AVEC_1CM_MARGE)) {
          taille = i + deltapaysage;
        }
        else {
          break;
        }
      }
    }
    return (int) taille;
  }*/
  
  /**
   * @return the document
   */
  public DescriptionDocument getDocument() {
    return document;
  }
  
  /**
   * @param document the document to set
   */
  public void setDocument(DescriptionDocument document) {
    this.document = document;
    analyseDocument();
  }
  
  /**
   * @param spoolOld the spool to set
   */
  public void setListing(String[] listing) {
    this.listing = listing;
  }
  
  /**
   * @param spoolOld the spool to set
   */
  public void setSpool(SpoolOld spoolOld) {
    this.spoolOld = spoolOld;
  }
  
  /**
   * @return the spool
   */
  public SpoolOld getSpool() {
    return spoolOld;
  }
  
  /**
   * @param dossierTravail the dossierTravail to set
   */
  public void setDossierTravail(String dossierTravail) {
    this.dossierTravail = dossierTravail;
  }
  
  /**
   * @return the dossierTravail
   */
  public String getDossierTravail() {
    if (dossierTravail == null) {
      return "";
    }
    return dossierTravail;
  }
  
  /**
   * @param nomFichierFO the nomFichierFO to set
   */
  public void setNomFichierFO(String nomFichierFO) {
    this.nomFichierFO = nomFichierFO;
  }
  
  /**
   * @return the nomFichierFO
   */
  public String getNomFichierFO() {
    return nomFichierFO;
  }
  
  /**
   * Génère le fichier FO
   * @return
   */
  public String genereFOtoString() {
    // if (document == null) return null;
    if (listing == null) {
      return null;
    }
    boolean retour = true;
    
    retour = addEnteteFichier();
    if (retour) {
      retour = addEnteteDocument();
    }
    if (retour) {
      retour = addCorpsDocument(listing);
    }
    if (retour) {
      retour = addPiedFichier();
    }
    
    StringBuffer data = new StringBuffer(Constantes.TAILLE_BUFFER);
    for (int i = 0; i < docfo.size(); i++) {
      data.append(docfo.get(i));
    }
    
    return data.toString();
  }
  
  /**
   * Génère le fichier FO
   * @return
   */
  public boolean genereFOtoFile() {
    // if (document == null) return false;
    if (listing == null) {
      return false;
    }
    boolean retour = true;
    String nom = NONAME;
    
    retour = addEnteteFichier();
    if (retour) {
      retour = addEnteteDocument();
    }
    if (retour) {
      retour = addCorpsDocument(listing);
    }
    if (retour) {
      retour = addPiedFichier();
    }
    
    // for (int i=0; i<docfo.size(); i++)
    if ((document != null) && (document.getNom() != null)) {
      nom = document.getNom();
    }
    
    nomFichierFO = getDossierTravail() + File.separatorChar + nom.trim() + EXTENSION;
    if (retour) {
      retour = ecrireFichier(nomFichierFO);
    }
    return retour;
  }
  
  /**
   * Ecriture du fichier FO
   * @param fichier
   * @return
   */
  private boolean ecrireFichier(String fichier) {
    if (fichier == null) {
      return false;
    }
    
    GestionFichierTexte gft = new GestionFichierTexte(fichier);
    gft.setContenuFichier(docfo);
    int ret = gft.ecritureFichier();
    
    return ret == 0;
  }
  
  /**
   * Initialise les dimensions du document
   * @param largeur
   * @param hauteur
   */
  private void setDimension(double hauteur, double largeur) {
    this.hauteurCm = hauteur;
    this.largeurCm = largeur;
  }
  
  /**
   * Récupère les informations d'une page
   * @param page
   */
  private void recupInfosPage(DescriptionPage page) {
    if (page == null) {
      return;
    }
    
    // TODO A revoir ambiguité entre dim cm et px
    // if ((page.getHauteur() == 842) && (page.getLargeur() == 595))
    setDimension(hauteurCm, largeurCm);
    // else
    // setDimension(largeurCm, hauteurCm);
    
    // if (!images.contains(page.getImage()))
    // images.add(page.getImage());
  }
  
  /**
   * Analyse le document (dimension, nombre de fond de page différent)
   */
  private void analyseDocument() {
    // Si pas de description
    if (document == null) {
      return;
    }
    
    // Sinon
    // On parcourt le document (TODO A voir car c'est le dernier qui gagne)
    recupInfosPage(document.getPremiereDeCouverture());
    recupInfosPage(document.getDeuxiemeDeCouverture());
    if (document.getSequence() != null) {
      for (int i = 0; i < document.getSequence().size(); i++) {
        recupInfosPage(document.getSequence().get(i));
      }
    }
    recupInfosPage(document.getTroisiemeDeCouverture());
    recupInfosPage(document.getQuatriemeDeCouverture());
  }
  
  /**
   * On génère l'entête du fichier
   * @return
   */
  private boolean addEnteteFichier() {
    if (encoding.equals("ISO-8859-1") || encoding.equals("ISO8859_1") || encoding.toLowerCase().equals("cp1252")) {
      encoding = "ISO-8859-15";
    }
    
    docfo.add("<?xml version=\"1.0\" encoding=\"" + encoding + "\"?>");
    docfo.add("<fo:root xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" >");
    docfo.add("");
    
    return true;
  }
  
  /**
   * On génère le pied du fichier
   * @return
   */
  private boolean addPiedFichier() {
    docfo.add("</fo:root>");
    
    return true;
  }
  
  /***
   * On génère l'entête
   * @return
   */
  private boolean addEnteteDocument() {
    if (listing == null) {
      return false;
    }
    docfo.add("<!-- Déclaration de la mise en page -->");
    docfo.add("<fo:layout-master-set>");
    
    if (document != null) {
      addEnteteDocument_descriptionPage(document.getPremiereDeCouverture(), '1');
      addEnteteDocument_descriptionPage(document.getDeuxiemeDeCouverture(), '2');
      for (int j = 0; j < document.getSequence().size(); j++) {
        addEnteteDocument_descriptionPage(document.getSequence().get(j), 'S');
      }
      addEnteteDocument_descriptionPage(document.getTroisiemeDeCouverture(), '3');
      addEnteteDocument_descriptionPage(document.getQuatriemeDeCouverture(), '4');
    }
    else {
      // Si pas de description
      // docfo.add("<fo:simple-page-master master-name=\"A4-0\" page-height=\"29.7cm\" page-width=\"21.0cm\" margin-top=\"0.75cm\"
      // margin-left=\"0.75cm\" >");
      docfo.add("<fo:simple-page-master master-name=\"A4-0\" page-height=\"" + hauteurCm + "cm\" page-width=\"" + largeurCm
          + "cm\"  margin-top=\"0.75cm\" margin-left=\"0.75cm\" >");
      docfo.add("<fo:region-body />");
      docfo.add("</fo:simple-page-master>");
    }
    docfo.add("</fo:layout-master-set>");
    docfo.add("");
    
    return true;
  }
  
  /***
   * On génère l'entête d'une page
   * @return
   */
  private void addEnteteDocument_descriptionPage(DescriptionPage page, char pType) {
    if (page == null) {
      return;
    }
    
    docfo.add("\t<fo:simple-page-master master-name=\"" + PREFIXE_PAGE_MASTER + pType + '-' + page.getNomFichierDDP()
        + "\" page-height=\"" + page.getHauteur() + "cm\" page-width=\"" + page.getLargeur() + "cm\"  margin-top=\"" + page.getMarge_y()
        + "cm\" margin-left=\"" + page.getMarge_x() + "cm\" >");
    if (page.getImage() != NOIMAGE) {
      docfo.add("\t\t<fo:region-body background-image=\"" + page.getImage() + "\" background-repeat=\"no-repeat\" />");
    }
    else {
      docfo.add("\t\t<fo:region-body />");
    }
    docfo.add("\t</fo:simple-page-master>");
  }
  
  /**
   * Génération du corps du document (toutes les pages)
   * @return
   */
  private boolean addCorpsDocument(String[] spool) {
    if (spool == null) {
      return false;
    }
    int pageencours = -1;
    int pagesequence = 0;
    
    // Génération du corps du document
    docfo.add("<!-- Corps du document -->");
    
    // Avec la description
    if (document != null) {
      pagesequence = spool.length;
      // On calcule le nombre de page pour la séquence
      if (document.getPremiereDeCouverture() != null && document.getPremiereDeCouverture().isLienSpool()) {
        pagesequence--;
      }
      if (document.getDeuxiemeDeCouverture() != null && document.getDeuxiemeDeCouverture().isLienSpool()) {
        pagesequence--;
      }
      if (document.getTroisiemeDeCouverture() != null && document.getTroisiemeDeCouverture().isLienSpool()) {
        pagesequence--;
      }
      if (document.getQuatriemeDeCouverture() != null && document.getQuatriemeDeCouverture().isLienSpool()) {
        pagesequence--;
      }
      // On traite les pages
      pageencours = addPageDocument(document.getPremiereDeCouverture(), spool, pageencours, '1');
      pageencours = addPageDocument(document.getDeuxiemeDeCouverture(), spool, pageencours, '2');
      if (pagesequence > 0) {
        for (int i = 0; i < pagesequence; i++) {
          for (int j = 0; j < document.getSequence().size(); j++) {
            pageencours = addPageDocument(document.getSequence().get(j), spool, pageencours, 'S');
          }
        }
      }
      pageencours = addPageDocument(document.getTroisiemeDeCouverture(), spool, pageencours, '3');
      pageencours = addPageDocument(document.getQuatriemeDeCouverture(), spool, pageencours, '4');
    }
    else {
      // Sans la description
      addPageDocument(spool);
    }
    
    return true;
  }
  
  /* Ancienne méthode
    private boolean addCorpsDocument(String[] spool) {
      if (spool == null) {
        return false;
      }
      int pageencours = -1, pageagarder = 0;
      int pageatraiter = 0, pagesequence = 0;
      
      // Génération du corps du document
      docfo.add("<!-- Corps du document -->");
      
      // Avec la description
      if (document != null) {
        // On vérifie si on doit la séquence doit laisser des pages pour la troisiemedecouv & la quatriemedecouv
        if ((document.getTroisiemeDeCouverture() != null) && (document.getTroisiemeDeCouverture().isLienSpool())) {
          pageagarder++;
        }
        if ((document.getQuatriemeDeCouverture() != null) && (document.getQuatriemeDeCouverture().isLienSpool())) {
          pageagarder++;
        }
        if (document.getSequence() != null) {
          pageatraiter = spool.length - pageagarder;
          for (int j = 0; j < document.getSequence().size(); j++) {
            if (document.getSequence().get(j).isLienSpool()) {
              pagesequence++;
            }
          }
        }
        // Il y a un soucis avec la gestion des pages si une premiere de couv est utilisée
        // A corriger pour casanova
        pageencours = addPageDocument(document.getPremiereDeCouverture(), spool, pageencours);
        pageencours = addPageDocument(document.getDeuxiemeDeCouverture(), spool, pageencours);
        if (pageatraiter > 0) {
          pageatraiter = pageatraiter / pagesequence;
          for (int i = 0; i < pageatraiter; i++) {
            for (int j = 0; j < document.getSequence().size(); j++) {
              pageencours = addPageDocument(document.getSequence().get(j), spool, pageencours);
            }
          }
        }
        pageencours = addPageDocument(document.getTroisiemeDeCouverture(), spool, pageencours);
        pageencours = addPageDocument(document.getQuatriemeDeCouverture(), spool, pageencours);
      }
      else {
        // Sans la description
        addPageDocument(spool);
      }
      
      return true;
    }*/
  
  /**
   * Génération d'une page du document à partir d'une description.
   */
  private int addPageDocument(DescriptionPage page, String[] documentspool, int pageencours, char pType) {
    if (page == null) {
      return pageencours;
    }
    
    // Recherche s'il y a un fond de page
    if (page.isLienSpool()) {
      pageencours++;
      id.setDonnees(new StringBuffer(documentspool[pageencours]));
      addVariables(page);
      initConditions(page);
    }
    
    // On génère la description de la page
    String master = PREFIXE_PAGE_MASTER + pType + '-' + page.getNomFichierDDP();
    docfo.add("<fo:page-sequence master-reference=\"" + master + "\" >");
    docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"  wrap-option=\"wrap\" white-space=\"pre\" >");
    // On ajoute les composants uniquement si le DDD est lié au spool
    if (page.isLienSpool()) {
      for (int i = 0; i < page.getDetiquette().getDObject().size(); i++) {
        if (page.getDetiquette().getDObject().get(i) instanceof DescriptionLabel) {
          addLabelDocument((DescriptionLabel) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
        }
        else {
          if (page.getDetiquette().getDObject().get(i) instanceof DescriptionImage) {
            addImageDocument((DescriptionImage) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
          }
          else {
            if (page.getDetiquette().getDObject().get(i) instanceof DescriptionCodeBarre) {
              addCodeBarreDocument((DescriptionCodeBarre) page.getDetiquette().getDObject().get(i), documentspool[pageencours]);
            }
          }
        }
      }
      // Si aucun composant, on ajoute un bloc vide (pour pas avoir d'erreur de FOP)
      if (page.getDetiquette().getDObject().size() == 0) {
        addLabelVide();
      }
    }
    else {
      addLabelVide();
    }
    
    docfo.add("</fo:flow>");
    docfo.add("</fo:page-sequence>");
    docfo.add("");
    
    return pageencours;
  }
  
  /**
   * Génération d'un page du document sans description
   * @param page
   * @return
   */
  private int addPageDocument(String[] documentspool) {
    // Pas de fond de page
    String master = PREFIXE_PAGE_MASTER + "0";
    
    for (int i = 0; i < documentspool.length; i++) {
      // On génère la description de la page
      docfo.add("<fo:page-sequence master-reference=\"" + master + "\" >");
      docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"  wrap-option=\"wrap\" white-space=\"pre\" >");
      // docfo.add("<fo:flow flow-name=\"xsl-region-body\" white-space-collapse=\"false\"
      // white-space-treatment=\"ignore-if-before-linefeed\" >");
      addLabelDocument(documentspool[i], police);
      docfo.add("</fo:flow>");
      docfo.add("</fo:page-sequence>");
      docfo.add("");
    }
    return documentspool.length - 1;
  }
  
  /**
   * Génère une ligne vide dans le cas où il n'y a aucun composant dans la description.
   */
  private void addLabelVide() {
    docfo.add(
        "<fo:block-container position=\"absolute\" top=\"1cm\" left=\"1cm\" width=\"1cm\" height=\"1cm\" line-height=\"10%\" display-align=\"center\">");
    docfo.add("<fo:block />");
    docfo.add("</fo:block-container>");
  }
  
  /**
   * Génération d'un label dans le document à partir d'une description de spool.
   */
  private boolean addLabelDocument(DescriptionLabel dlabel, String pagespool) {
    if (dlabel == null) {
      return false;
    }
    if (dlabel.getTexte() == null) {
      Trace.alerte("Attention le label positionné en x= " + dlabel.getXPos_cm() + " et en y=" + dlabel.getYPos_cm()
          + " ne contient pas la propriété \"texte\", il faut le supprimer manuellement via un éditeur de texte.");
      return false;
    }
    
    String texte = id.analyseExpressionWOTrim(dlabel.getTexte());
    // Si le texte est vide le code nécessaire n'est pas ajouté
    if (texte.isEmpty()) {
      return true;
    }
    if (dlabel.getCondition() != null) {
      if (!listeConditions.get(dlabel.getCondition()).isResultat()) {
        return false;
      }
    }
    
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dlabel.getYPos_cm() + "cm\" left=\"" + dlabel.getXPos_cm()
        + "cm\" width=\"" + dlabel.getLargeur() + "cm\" height=\"" + dlabel.getHauteur() + "cm\" line-height=\"" + dlabel.getLineHeight()
        + "%\" display-align=\"" + getJustificationTexte(dlabel.getJustificationVTexte()) + "\">");
    docfo.add("\t<fo:block font-family=\"" + dlabel.getNomPolice() + "\" font-size=\"" + dlabel.getTaillePolice() + "pt\" "
        + getStyle(dlabel.getStylePolice()) + " " + getCouleur(dlabel.getForeGroundColor()) + " text-align=\""
        + getJustificationTexte(dlabel.getJustificationHTexte()) + "\">");
    texte = convertSymboleEuro(texte);
    docfo.add("<![CDATA[" + texte + "]]>");
    
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * Génération d'un label dans le document sans description.
   */
  private boolean addLabelDocument(String bloc, Font police) {
    if (bloc == null) {
      return false;
    }
    bloc = Constantes.normerTexte(bloc);
    // Si le texte est vide le code nécessaire n'est pas ajouté
    if (bloc.isEmpty()) {
      return true;
    }
    
    docfo.add("<fo:block-container width=\"100%\" >");
    docfo.add("\t<fo:block font-family=\"" + police.getName() + "\" font-size=\"" + police.getSize() + "\" " + getStyle(police.getStyle())
        + " " + getCouleur(Color.BLACK) + ">");
    bloc = convertSymboleEuro(bloc);
    docfo.add("<![CDATA[" + bloc + "]]>");
    
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * Génération d'une image dans le document à partir d'une description de spool.
   */
  private boolean addImageDocument(DescriptionImage dimage, String pagespool) {
    if (dimage == null) {
      return false;
    }
    if (dimage.getCondition() != null) {
      if (!listeConditions.get(dimage.getCondition()).isResultat()) {
        return false;
      }
    }
    
    String image = dimage.getCheminImage();
    if (listeVariables != null) {
      for (int i = listeVariables.size(); --i >= 0;) {
        image = image.replaceAll(listeVariables.get(i).getNom(), listeVariables.get(i).getValeur());
      }
    }
    // Si l'image est vide le code nécessaire n'est pas ajouté
    image = Constantes.normerTexte(image);
    if (image.isEmpty()) {
      return true;
    }
    
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dimage.getYPos_cm() + "cm\" left=\"" + dimage.getXPos_cm()
        + "cm\" line-height=\"3pt\" display-align=\"center\" >");
    docfo.add("\t<fo:block>");
    docfo.add("<fo:external-graphic src=\"" + image.trim() + "\"  width=\"" + dimage.getLargeur() + "cm\" height=\"" + dimage.getHauteur()
        + "cm\" content-height=\"scale-to-fit\" content-width=\"scale-to-fit\" />");
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * Génération d'un code barre dans le document à partir d'une description de spool.
   */
  private boolean addCodeBarreDocument(DescriptionCodeBarre dcodebarre, String pagespool) {
    if (dcodebarre == null) {
      return false;
    }
    
    String texte = id.analyseExpressionWOTrim(dcodebarre.getTexte());
    // Si le texte est vide le code nécessaire n'est pas ajouté
    if (texte.isEmpty()) {
      return true;
    }
    if (dcodebarre.getCondition() != null) {
      if (!listeConditions.get(dcodebarre.getCondition()).isResultat()) {
        return false;
      }
    }
    
    docfo.add("<fo:block-container position=\"absolute\" top=\"" + dcodebarre.getYPos_cm() + "cm\" left=\"" + dcodebarre.getXPos_cm()
        + "cm\" >");
    docfo.add("\t<fo:block>");
    docfo.add("\t\t<fo:instream-foreign-object>");
    docfo.add("\t\t\t<bc:barcode xmlns:bc=\"http://barcode4j.krysalis.org/ns\" message=\"" + texte + "\" orientation=\""
        + dcodebarre.getRotation() + "\">");
    docfo.add("\t\t\t\t<bc:" + dcodebarre.getTypeCodeBarre() + ">");
    docfo.add("\t\t\t\t\t<bc:human-readable>" + dcodebarre.getPositionMessage() + "</bc:human-readable>");
    docfo.add("\t\t\t\t\t<bc:height>" + dcodebarre.getHauteur() + "cm</bc:height>");
    if (dcodebarre.getLargeurModule() > 0) {
      docfo.add("\t\t\t\t\t<bc:module-width>" + dcodebarre.getLargeurModule() + "mm</bc:module-width>");
    }
    docfo.add("\t\t\t\t</bc:" + dcodebarre.getTypeCodeBarre() + ">");
    docfo.add("\t\t\t</bc:barcode>");
    docfo.add("\t\t</fo:instream-foreign-object>");
    docfo.add("\t</fo:block>");
    docfo.add("</fo:block-container>");
    
    return true;
  }
  
  /**
   * Retourne le style codé en chaine.
   */
  private String getStyle(int style) {
    String style_ch = "";
    switch (style) {
      case Font.ITALIC:
        style_ch = "font-style=\"italic\" font-weight=\"400\"";
        break;
      
      case Font.PLAIN:
        style_ch = "font-weight=\"400\"";
        break;
      
      case Font.BOLD:
        style_ch = "font-weight=\"700\"";
        break;
    }
    
    return style_ch;
  }
  
  /**
   * Retourne la couleur codée en chaine.
   */
  private String getCouleur(Color couleur) {
    if (couleur == Color.BLACK) {
      return "";
    }
    else {
      return "color=\"rgb(" + couleur.getRed() + "," + couleur.getGreen() + "," + couleur.getBlue() + ")\" ";
    }
  }
  
  /**
   * Retourne le texte exprimant la justification du texte
   * @param justification
   * @return
   */
  private String getJustificationTexte(int jht) {
    switch (jht) {
      case DescriptionLabel.GAUCHE:
        return "left";
      case DescriptionLabel.CENTRE:
        return "center";
      case DescriptionLabel.DROITE:
        return "right";
      // case DescriptionLabel.HAUT : return "before";
      // case DescriptionLabel.BAS : return "after";
      default:
        return "left";
    }
  }
  
  /**
   * Ajoute les variables d'une description de page
   * @param page
   */
  private void addVariables(DescriptionPage page) {
    if ((listeVariables == null) || (page.getListeDescriptionVariable() == null)) {
      return;
    }
    
    // On vérifie que l'on ne l'ai déjà pas traité
    if (listeDescriptionPage.contains(page)) {
      return;
    }
    listeDescriptionPage.add(page);
    
    String chaine = null;
    // On ajoute les variables dans la liste avec leur valeur
    for (int i = page.getListeDescriptionVariable().size(); --i >= 0;) {
      // TODO voir le analyseExpressionTrimR merde apparemment enleve pas les blancs à droite
      chaine = id.analyseExpression(page.getListeDescriptionVariable().get(i).getVariable());
      page.getListeDescriptionVariable().get(i).setValeur(chaine == null ? "" : chaine.trim());
      // listeVariable.put( ((DescriptionVariable) page.getListeDescriptionVariable().get(i)).getNom(),
      // id.analyseExpressionTrimR(((DescriptionVariable)page.getListeDescriptionVariable().get(i)).getVariable()));
      listeVariables.add(page.getListeDescriptionVariable().get(i));
    }
  }
  
  /**
   * Initialise les conditions pour la page courante
   * @param page
   */
  private void initConditions(DescriptionPage page) {
    if (page.getListeDescriptionCondition() == null) {
      return;
    }
    
    listeConditions.clear();
    for (int i = page.getListeDescriptionCondition().size(); --i >= 0;) {
      page.getListeDescriptionCondition().get(i).setResultat(id);
      listeConditions.put(page.getListeDescriptionCondition().get(i).getNom(), page.getListeDescriptionCondition().get(i));
    }
  }
  
  /**
   * Initialise la liste des variables
   * @param lstVar
   */
  // public void setListeVariable(HashMap<String, String> lstVar)
  public void setListeVariables(ArrayList<DescriptionVariable> lstvar) {
    listeVariables = lstvar;
    if (listeVariables == null) {
      return;
    }
    
    // Ajout des variables systèmes date & heure (au cas où)
    for (int i = 0; i <= 5; i++) {
      DescriptionVariable var = new DescriptionVariable();
      var.setNom(DateHeure.getNomVariable(i));
      var.setValeur(DateHeure.getFormateDateHeure(i));
      listeVariables.add(var);
    }
  }
  
  /**
   * Convertit le caractère 164 en euros (règle le pb avec les codes pages)
   * @param texte
   * @return
   */
  private String convertSymboleEuro(String texte) {
    // if (texte == null) return texte;
    
    int pos = texte.indexOf(164);
    if (pos == -1) {
      return texte;
    }
    
    char c = (char) Integer.parseInt("A4", 16);
    return texte.replace(c, '¤');
    // return texte.replace(c, '\u20AC');
  }
  
  /**
   * Retourne une chaine trimée à droite
   * @param texte
   * @return
   */
  private String getTrimR(String texte) {
    // compteur++;
    if (texte == null) {
      return texte;
    }
    
    int i = 0;
    char[] tab = texte.toCharArray();
    for (i = tab.length; --i > 0;) {
      if ((tab[i] == '\n') || (tab[i] == '\r')) {
        continue;
      }
      if (tab[i] != ' ') {
        break;
      }
    }
    if (i < 0) {
      return "";
    }
    // texte = new String(tab, 0, i+1);
    return new String(tab, 0, i + 1);
  }
}
