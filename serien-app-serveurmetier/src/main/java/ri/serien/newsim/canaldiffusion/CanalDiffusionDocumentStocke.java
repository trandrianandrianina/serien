/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

import java.io.File;

import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;

public class CanalDiffusionDocumentStocke extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Document stocké";
  
  // Variables
  // Le type fonctionnel du document
  private EnumTypeDocumentStocke typeDocumentStocke = null;
  // Nom du fichier sans extension
  private String nomFichierSansExtension = null;
  // Identifiant du document de vente pour la table des liens
  private IdDocumentVente idDocumentVente = null;
  // Identifiant du client pour la table des liens
  private IdClient idClient = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourne le nom du fichier avec son extension.
   */
  public String getNomFichierAvecExtension() {
    String nomFichier = "NouveauFichier";
    if (nomFichierSansExtension == null) {
      if (getFichierDescriptionDocumentSansExtension() != null) {
        nomFichier = getFichierDescriptionDocumentSansExtension();
      }
    }
    else {
      nomFichier = nomFichierSansExtension;
    }
    return nomFichier + '.' + getTypeDocument().getExtension();
  }
  
  /**
   * Retourne le chemin complet avec le nom du fichier, à l'aide du fichier généré.
   */
  public String getCheminCompletFichier(File pFichierGenere) {
    if (pFichierGenere == null) {
      return null;
    }
    String nomFichierAvecExtension = getNomFichierAvecExtension();
    if (nomFichierAvecExtension == null) {
      return null;
    }
    return pFichierGenere.getParent() + File.separatorChar + nomFichierAvecExtension;
  }
  
  // -- Accesseurs
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
  
  public EnumTypeDocumentStocke getTypeDocumentStocke() {
    return typeDocumentStocke;
  }
  
  public void setTypeDocumentStocke(EnumTypeDocumentStocke typeDocumentStocke) {
    this.typeDocumentStocke = typeDocumentStocke;
  }
  
  public String getNomFichierSansExtension() {
    return nomFichierSansExtension;
  }
  
  public void setNomFichierSansExtension(String nomFichierSansExtension) {
    this.nomFichierSansExtension = nomFichierSansExtension;
  }
  
  public IdDocumentVente getIdDocumentVente() {
    return idDocumentVente;
  }
  
  public void setIdDocumentVente(IdDocumentVente idDocumentVente) {
    this.idDocumentVente = idDocumentVente;
  }
  
  public IdClient getIdClient() {
    return idClient;
  }
  
  public void setIdClient(IdClient idClient) {
    this.idClient = idClient;
  }
}
