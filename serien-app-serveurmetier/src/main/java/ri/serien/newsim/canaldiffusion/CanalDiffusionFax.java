/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

import ri.serien.libcommun.exploitation.mail.IdMail;

public class CanalDiffusionFax extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Fax";
  
  // Variables
  private IdMail idMailPourFax = null;
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
  
  public IdMail getIdMailPourFax() {
    return idMailPourFax;
  }
  
  public void setIdMailPourFax(IdMail idMail) {
    this.idMailPourFax = idMail;
  }
}
