/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

import ri.serien.libcommun.exploitation.mail.IdMail;

public class CanalDiffusionMail extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Mail";
  
  // Variables
  private IdMail idMail = null;
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
  
  public IdMail getIdMail() {
    return idMail;
  }
  
  public void setIdMail(IdMail idMail) {
    this.idMail = idMail;
  }
}
