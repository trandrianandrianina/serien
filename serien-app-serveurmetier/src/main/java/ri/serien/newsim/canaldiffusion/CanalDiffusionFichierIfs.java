/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

/**
 * Description de la sortie de type fichier
 */
import java.io.File;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;

public class CanalDiffusionFichierIfs extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Fichier Ifs";
  
  // Variables
  // Nom du fichier sans extension
  private String nomFichierSansExtension = null;
  // Chemin du dossier complet sans le nom du fichier
  private String nomDossier = null;
  // Execution d'une application associé au document généré
  private String commande = null;
  // Active ou non la sauvegarde de l'ancien fichier s'il existe déja)
  private boolean sauvegardeAncien = false;
  
  /**
   * Constructeur.
   */
  public CanalDiffusionFichierIfs() {
    this(EnumTypeDocument.PDF);
  }
  
  /**
   * Constructeur.
   */
  public CanalDiffusionFichierIfs(EnumTypeDocument pTypeDocument) {
    setTypeDocument(pTypeDocument);
  }
  
  // -- Accesseurs
  
  public String getNomFichierAvecExtension() {
    String nomFichier = "NouveauFichier";
    if (nomFichierSansExtension == null) {
      if (getFichierDescriptionDocumentSansExtension() != null) {
        nomFichier = getFichierDescriptionDocumentSansExtension();
      }
    }
    else {
      nomFichier = nomFichierSansExtension;
    }
    return nomFichier + '.' + getTypeDocument().getExtension();
  }
  
  public void setNomFichierSansExtension(String pNomFichierSansExtension) {
    if (pNomFichierSansExtension == null) {
      this.nomFichierSansExtension = null;
    }
    else {
      this.nomFichierSansExtension = pNomFichierSansExtension.trim();
    }
  }
  
  public String getNomFichierSansExtension() {
    return nomFichierSansExtension;
  }
  
  public void setNomDossier(String pNomDossier) {
    if (pNomDossier != null) {
      pNomDossier = pNomDossier.trim();
      if (pNomDossier.endsWith("/") || pNomDossier.endsWith("\\")) {
        pNomDossier = pNomDossier.substring(0, pNomDossier.length() - 1);
      }
    }
    this.nomDossier = pNomDossier;
  }
  
  public String getNomDossier() {
    return nomDossier;
  }
  
  /**
   * Retourne le chemin complet avec le nom du fichier (avec les variables brutes cad non converties).
   */
  public String getCheminCompletFichier() {
    String nomFichierAvecExtension = getNomFichierAvecExtension();
    if (nomDossier == null || nomFichierAvecExtension == null || getTypeDocument() == null) {
      return "";
    }
    return nomDossier.trim() + File.separatorChar + nomFichierAvecExtension;
  }
  
  public void setCommande(String pCommande) {
    this.commande = pCommande;
  }
  
  public String getCommande() {
    if (commande == null) {
      commande = "";
    }
    return commande;
  }
  
  public boolean isSauvegardeAncien() {
    return sauvegardeAncien;
  }
  
  public void setSauvegardeAncien(Boolean sauvegardeAncien) {
    if (sauvegardeAncien == null) {
      return;
    }
    this.sauvegardeAncien = sauvegardeAncien;
  }
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
}
