/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

public class CanalDiffusionFusionFichier extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Fusion de fichiers";
  
  // Variables
  private String cheminCompletFichierDestinationFusion = null;
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
  
  public String getCheminCompletFichierDestinationFusion() {
    return cheminCompletFichierDestinationFusion;
  }
  
  public void setCheminCompletFichierDestinationFusion(String cheminCompletFichierDestinationFusion) {
    this.cheminCompletFichierDestinationFusion = cheminCompletFichierDestinationFusion;
  }
}
