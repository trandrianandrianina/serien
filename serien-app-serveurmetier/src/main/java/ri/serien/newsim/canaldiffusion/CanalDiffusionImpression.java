/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

import java.io.File;

import ri.serien.libcommun.exploitation.edition.EnumLangageImprimante;
import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.exploitation.edition.EnumTypeRectoVerso;
import ri.serien.libcommun.outils.Constantes;

/**
 * Description de la sortie de type imprimante
 */
public class CanalDiffusionImpression extends CanalDiffusion {
  // Constantes
  public static final String LIBELLE = "Impression";
  
  public final static String ESC = "<ESC>";
  public final static char ESC_CHAR = '\u001B';
  public final static int SCRIPT_AUCUN = 0;
  public final static int SCRIPT_PJL = 1;
  public final static int SCRIPT_PERSO = 2;
  
  // Variables
  // Nom du spool que l'on va générer sur l'AS/400
  private String nomSpool = null;
  // Nom de la Outq de l'imprimante
  private String nomOutq = null;
  // Langage de l'imprimante (ps, pjl, ...)
  private EnumLangageImprimante langageImprimante = null;
  // Tiroir d'entrée papier (=0 tiroir par défaut)
  private int tiroirEntree = 1;
  // Tiroir de sortie du papier (=0 tiroir par défaut)
  private int tiroirSortie = 0;
  private EnumTypeRectoVerso rectoVerso = EnumTypeRectoVerso.AUCUN;
  // Nombre d'exemplaire à générer
  private int nbrExemplaire = 1;
  // Qualité d'impression : *STD, *DRAFT, *NLQ, *FASTDRAFT
  // private String qualite=null;
  // Priorité : 1 (haute) à 9 (basse)
  private String priorite = null;
  // Holde le spool sur l'AS/400 après sa création (ps / pdf)
  private boolean suspendre = false;
  // Economie du toner
  private boolean economie = false;
  // Densité 1 à 5
  private int densite = 3;
  // Force le noir & blanc sur imprimante couleur
  private boolean noirblanc = false;
  // Translation sur X
  private int x_translate = 0;
  // Translation sur Y
  private int y_translate = 0;
  // Redimensionnement sur X
  private int x_scale = 100;
  // Redimensionnement sur Y
  private int y_scale = 100;
  // Type de script : aucun, pjl
  private int typeScript = SCRIPT_AUCUN;
  // Script avant le fichier
  private String scriptAvant = null;
  // Script après le fichier
  private String scriptApres = null;
  // Soumission pour l'impression directe
  private boolean soumission = false;
  // Chemin du dossier complet sans le nom du fichier (pour stocker le fichier)
  private String nomDossier = null;
  // Nom du fichier généré sans extension
  private String nomFichier = null;
  
  /**
   * Constructeur.
   */
  public CanalDiffusionImpression() {
    this(EnumTypeDocument.PDF);
  }
  
  /**
   * Constructeur.
   */
  public CanalDiffusionImpression(EnumTypeDocument pTypeDocument) {
    setTypeDocument(pTypeDocument);
  }
  
  // -- Méthodes publiques
  
  // -- Accesseurs
  
  /**
   * Retourne le chemin complet avec le nom du fichier (avec les variables brutes cad non converties)
   */
  public String getNomComplet() {
    if (nomDossier == null || nomFichier == null || getTypeDocument() == null) {
      return "";
    }
    return nomDossier.trim() + File.separatorChar + nomFichier.trim() + '.' + getTypeDocument().getExtension();
  }
  
  public EnumTypeRectoVerso getRectoVerso() {
    return rectoVerso;
  }
  
  public void setRectoVerso(EnumTypeRectoVerso rectoVerso) {
    this.rectoVerso = rectoVerso;
  }
  
  public void setNbrExemplaire(int nbrExemplaire) {
    this.nbrExemplaire = nbrExemplaire;
  }
  
  public int getNbrExemplaire() {
    return nbrExemplaire;
  }
  
  public void setNomOutq(String nomOutq) {
    if (this.nomOutq != null) {
      this.nomOutq = nomOutq.trim();
    }
    else {
      this.nomOutq = nomOutq;
    }
  }
  
  public String getNomOutq() {
    return nomOutq;
  }
  
  public void setLangageImprimante(EnumLangageImprimante lgimprimante) {
    this.langageImprimante = lgimprimante;
  }
  
  public EnumLangageImprimante getLangageImprimante() {
    return langageImprimante;
  }
  
  public int getTiroirEntree() {
    return tiroirEntree;
  }
  
  public void setTiroirEntree(int tiroirEntree) {
    this.tiroirEntree = tiroirEntree;
  }
  
  public void setTiroirSortie(int tiroirSortie) {
    this.tiroirSortie = tiroirSortie;
  }
  
  public int getTiroirSortie() {
    return tiroirSortie;
  }
  
  public String getPriorite() {
    return priorite;
  }
  
  public void setPriorite(String priorite) {
    this.priorite = priorite;
  }
  
  public boolean isSuspendre() {
    return suspendre;
  }
  
  public void setSuspendre(boolean suspendre) {
    this.suspendre = suspendre;
  }
  
  public String getNomSpool() {
    return nomSpool;
  }
  
  public void setNomSpool(String nomSpool) {
    this.nomSpool = nomSpool;
  }
  
  public int getTypeScript() {
    return typeScript;
  }
  
  public void setTypeScript(int typeScript) {
    this.typeScript = typeScript;
  }
  
  public String getScriptAvant() {
    return scriptAvant;
  }
  
  public String getScriptAvant(boolean convert) {
    if ((scriptAvant != null) && convert) {
      return scriptAvant.replaceAll(ESC, "" + ESC_CHAR);
    }
    else {
      return getScriptAvant();
    }
  }
  
  public void setScriptAvant(String scriptAvant) {
    this.scriptAvant = scriptAvant;
  }
  
  public String getScriptApres() {
    return scriptApres;
  }
  
  public String getScriptApres(boolean convert) {
    if ((scriptApres != null) && convert) {
      return scriptApres.replaceAll(ESC, "" + ESC_CHAR);
    }
    else {
      return getScriptApres();
    }
  }
  
  public void setScriptApres(String scriptApres) {
    this.scriptApres = scriptApres;
  }
  
  public boolean isEconomie() {
    return economie;
  }
  
  public void setEconomie(boolean economie) {
    this.economie = economie;
  }
  
  public int getDensite() {
    return densite;
  }
  
  public void setDensite(int densite) {
    this.densite = densite;
  }
  
  public void setNoirblanc(boolean noirblanc) {
    this.noirblanc = noirblanc;
  }
  
  public boolean isNoirblanc() {
    return noirblanc;
  }
  
  public int getX_translate() {
    return x_translate;
  }
  
  public void setX_translate(int xTranslate) {
    x_translate = xTranslate;
  }
  
  public int getY_translate() {
    return y_translate;
  }
  
  public void setY_translate(int yTranslate) {
    y_translate = yTranslate;
  }
  
  public int getX_scale() {
    return x_scale;
  }
  
  public void setX_scale(int xScale) {
    x_scale = xScale;
  }
  
  public int getY_scale() {
    return y_scale;
  }
  
  public void setY_scale(int yScale) {
    y_scale = yScale;
  }
  
  /**
   * Génération du script PJL
   */
  private void genereScriptDebutPjl() {
    StringBuffer script = new StringBuffer();
    
    script.append(CanalDiffusionImpression.ESC).append("%-12345X@PJL JOB").append(Constantes.crlf);
    script.append("@PJL SET MANUALFEED=ON").append(Constantes.crlf);
    
    switch (rectoVerso) {
      case AUCUN:
        script.append("@PJL SET DUPLEX=OFF").append(Constantes.crlf);
        break;
      
      case LONGEDGE:
        script.append("@PJL SET DUPLEX=ON").append(Constantes.crlf);
        script.append("@PJL SET DUPLEX=LONGEDGE").append(Constantes.crlf);
        break;
      
      case SHORTEDGE:
        script.append("@PJL SET DUPLEX=ON").append(Constantes.crlf);
        script.append("@PJL SET BINDING=SHORTEDGE").append(Constantes.crlf);
        break;
    }
    
    if (economie) {
      script.append("@PJL SET ECONOMODE=ON").append(Constantes.crlf);
      script.append("@PJL SET DENSITY=").append(densite).append(Constantes.crlf);
    }
    if (noirblanc) {
      script.append("@PJL SET RENDERMODE=GRAYSCALE").append(Constantes.crlf); // RENDERMODE=COLOR
    }
    if (tiroirEntree > 0) {
      script.append("@PJL SET MEDIASOURCE=TRAY").append(tiroirEntree).append(Constantes.crlf);
      script.append("@PJL SET LPAPERSOURCE=TRAY").append(tiroirEntree).append(Constantes.crlf);
    }
    if (tiroirSortie > 0) {
      script.append("@PJL SET OUTTRAY").append(tiroirSortie).append(Constantes.crlf);
    }
    switch (getTypeDocument()) {
      case PDF:
        script.append("@PJL ENTER LANGUAGE=PDF").append(Constantes.crlf);
        break;
      case POSTSCRIPT:
        script.append("@PJL ENTER LANGUAGE=POSTSCRIPT").append(Constantes.crlf);
        break;
      case PCL:
        script.append("@PJL ENTER LANGUAGE=PCL").append(Constantes.crlf);
        break;
      default:
    }
    setScriptAvant(script.toString());
  }
  
  /**
   * Génération du script PJL
   */
  private void genereScriptFinPjl() {
    StringBuffer script = new StringBuffer();
    
    script.append("@PJL SET MANUALFEED=OFF").append(Constantes.crlf);
    script.append("@PJL RESET").append(Constantes.crlf);
    script.append(CanalDiffusionImpression.ESC).append("%-12345X@PJL EOJ").append(Constantes.crlf);
    script.append(CanalDiffusionImpression.ESC).append("%-12345X").append(Constantes.crlf);
    
    setScriptApres(script.toString());
  }
  
  /**
   * Génération du script Postscript
   */
  private void genereScriptDebutPs() {
    StringBuffer script = new StringBuffer();
    
    script.append("%!PS").append(Constantes.crlf);
    switch (rectoVerso) {
      case LONGEDGE:
        script.append("%%BeginFeature: *Duplex DuplexTumble").append(Constantes.crlf);
        script.append("<</Duplex true /Tumble false>> setpagedevice").append(Constantes.crlf);
        script.append("%%EndFeature").append(Constantes.crlf);
        break;
      
      case SHORTEDGE:
        script.append("%%BeginFeature: *Duplex DuplexTumble").append(Constantes.crlf);
        script.append("<</Duplex true /Tumble true>> setpagedevice").append(Constantes.crlf);
        script.append("%%EndFeature").append(Constantes.crlf);
        break;
      
      default:
        break;
    }
    // A voir si ça fonctionne (pas eu l'occasion car pas de couleur)
    if (noirblanc) {
      script.append("%%BeginFeature: *ColorasGray").append(Constantes.crlf);
      script.append("<</ProcessColorModel /DeviceGray>> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    // A tester sur une imprimante (ne fonctionne pas sur LexMark) (postscript 3)
    if (tiroirEntree > 0) {
      script.append("%%BeginFeature:").append(Constantes.crlf);
      // /ManualFeed false à ajouter après /MediaPosition
      script.append("<</DeferredMediaSelection true  /MediaPosition " + tiroirEntree + ">> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    // A implémenter en GAP
    if ((x_translate > 0) || (y_translate > 0) || (x_scale < 100) || (y_scale < 100)) {
      script.append("%%BeginFeature:").append(Constantes.crlf);
      script.append("<</Install { " + x_translate + " " + y_translate + " translate " + (double) (x_scale / 100) + " "
          + (double) (y_scale / 100) + " scale } bind>> setpagedevice").append(Constantes.crlf);
      script.append("%%EndFeature").append(Constantes.crlf);
    }
    setScriptAvant(script.toString());
  }
  
  /**
   * Génère le script en fonction du langage de l'imprimante.
   */
  public void genereScript() {
    if (langageImprimante == null) {
      return;
    }
    switch (langageImprimante) {
      case PJL:
        genereScriptDebutPjl();
        genereScriptFinPjl();
        break;
      case POSTSCRIPT:
        genereScriptDebutPs();
        setScriptApres(null);
        break;
      
      default:
        setScriptAvant(null);
        setScriptApres(null);
        break;
    }
  }
  
  public void setSoumission(boolean soumission) {
    this.soumission = soumission;
  }
  
  public boolean isSoumission() {
    return soumission;
  }
  
  public String getNomDossier() {
    return nomDossier;
  }
  
  public void setNomDossier(String nomDossier) {
    this.nomDossier = nomDossier;
  }
  
  public String getNomFichier() {
    return nomFichier;
  }
  
  public void setNomFichier(String nomFichier) {
    this.nomFichier = nomFichier;
  }
  
  @Override
  public String getLibelle() {
    return LIBELLE;
  }
}
