/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.canaldiffusion;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;

/**
 * Description de la classe générique pour le canal de diffusion.
 * Un canal de diffusion définit ce qui va advenir d'un document, un seul document peut avoir plusieurs canaux de diffusion.
 */
public abstract class CanalDiffusion {
  // Variables
  // Description de la sortie
  private String description = null;
  private EnumTypeDocument typeDocument = EnumTypeDocument.PDF;
  // Nombre de dpi pour le document en sortie 72 pour le PDF
  private int dpiSortie = 72;
  // Le nom du fichier de la description de document (ddd ou xsl)
  private String fichierDescriptionDocumentSansExtension = null;
  
  // -- Méthodes abstract
  
  public abstract String getLibelle();
  
  // -- Accesseurs
  
  public void setDescription(String description) {
    this.description = description;
  }
  
  public String getDescription() {
    return description;
  }
  
  public String getExtension() {
    if (typeDocument == null) {
      return null;
    }
    return typeDocument.getExtension();
  }
  
  public EnumTypeDocument getTypeDocument() {
    return typeDocument;
  }
  
  public void setTypeDocument(EnumTypeDocument pTypeDocument) {
    this.typeDocument = pTypeDocument;
  }
  
  public int getDpiSortie() {
    return dpiSortie;
  }
  
  public void setDpiSortie(int dpiSortie) {
    this.dpiSortie = dpiSortie;
  }
  
  public String getFichierDescriptionDocumentSansExtension() {
    return fichierDescriptionDocumentSansExtension;
  }
  
  public void setFichierDescriptionDocumentSansExtension(String fichierDescriptionDocumentSansExtension) {
    this.fichierDescriptionDocumentSansExtension = fichierDescriptionDocumentSansExtension;
  }
}
