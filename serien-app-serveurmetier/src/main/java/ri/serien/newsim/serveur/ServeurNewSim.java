/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.serveur;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Date;

import ri.serien.commun.module.EnumControleModule;
import ri.serien.commun.module.InterfaceModule;
import ri.serien.commun.module.ManagerParametreModule;
import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.exploitation.module.EnumParametreModule;
import ri.serien.libcommun.exploitation.module.ListeParametreModule;
import ri.serien.libcommun.exploitation.module.ParametreModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.session.User;
import ri.serien.serveurmetier.serveur.ManagerAS400;

//=================================================================================================
//==>                                                                       26/05/2010 - 28/10/2020
//==> Serveur des éditions
//==> A faire:
//==>     - Voir comment gérer si plusieurs instance du serveur sur même machine=pb de chemin pour
//==>            LogMessage, rt, differentes version de seriem sur même AS/400, etc
//==>           (empêcher l'execution de plusieurs serveurs si même chemin racine)
//==> A savoir:
//==> Toutes les versions doivent être compatibles avec les versions précédantes pour les messages A0010 & A0011
//==> La version 1.10 amène la gestion du bac de sortie (à tester) - il faut mettre à jour le SEXP21LE
//==> La version 1.20 amène la gestion du multi édition pour un même document
//==> La version 1.21 amène l'édition directe de PDF sur un serveur installé Windows
//==> La version 1.23 corrige le problème du symbole euro
//==> La version 1.27 corrige un bug avec Série N lorsque le serveur NewSim est installé sur un PC
//==> La version 1.28 adaptation suite aux modifications des sources de commun400
//==> La version 1.29 corrige le bug lors de la coupure de l'AS400 qui empêche les éditions de fonctionner
//==>                 corrige les différences de rendu entre une édtion AS400 et Windows à cause des polices
//==>                    pour les editions sans affectations
//==>   La version 1.30 amène le nettoyage du dossier TMP au lancement du serveur
//==>   La version 1.30b corrige un bug récent lors de l'édition de doc PDF sans description qui rognait parfois
//==>                    le dernier caractère de chaque ligne
//==>   La version 1.30c corrige définitivement les sauts de ligne intempestifs et encore parfois le dernier carcatère rogné
//==>   La version 1.30d on renomme le projet et toutes les références à OndeEdition. Tout est remplacé par NewSim
//==>   La version 1.31 la liste des spools est transférée sous forme JSON au lieu de l'objet ListContains sérialisé
//==>   La version 1.31c on ne lit plus le port dans VGMDTA ce n'est plus d'actualité, on préférera utiliser -p=
//==>   La version 1.31d Modification de la méthode du nettoyage du dossier TMP qui se fait de manière récursive maintenant
//==>      001 mise à jour des apis pdfBox et fontBox
//==>      002 optimisation de lancement de la fenêtre de la Gestion des impressions
//==>   La version 1.31e Ajout du listage des outqueues (pour envoyer le spool dans une remoteoutq)
//==>   La version 1.31f Gestion des variables date/heure dans la sortie Fichier
//==>   La version 1.32 Gestion des éditions sans spool
//==>   La version 1.33 Gestion des pièces jointes lors de certaines éditions (top dans les factures) -> £NSIM = '3'
//==>   La version 1.34
//==>   La version 1.35 Ajoute l'impression directe pour les éditions V3 (ctrlInsertPHO, ctrlInsertWEB, SortieImprimante,
//==>                   analyseImprimeDocument, createDocumentWithoutSpool, imprimeDocumentWithoutSpool, modification du SEXP21LE)
//==>   La version 2.0 Renommage des packages, suppression des println, ...
//==>   La version 2.1 Migration vers fop 2.2
//==>   La version 2.2 Modification de l'algorithme qui permet de calculer la taille de la police pour les éditions sans DDD
//==>   La version 2.3 Amélioration de l'algorithme qui permet de calculer la taille de la police pour les éditions sans DDD
//==>   La version 2.4 Modification de la procédure d'arrêt du serveur
//==>   La version 2.5 Correction bug sur les premiere et deuxieme de couv
//==>   La version 2.6 Modifications pour la mise en place des nouveaux fichiers des URL
//==>   La version 2.7 Ajout d'un paramètre qui contient un préfixe possible pour les fichiers générés
//==>   La version 2.8 Ajout d'un paramètre qui contient la lettre d'execution uniauement dans le cas des éditions sans spool
//==>   La version 2.8b Ajout de trace lors des exceptions dans les classes principales
//==>                   Ajout de traces qui permettent d'avoir le nom du document et la taille du fichier xml pour les éditions v3
//==>                   Ajout d'un test sur la table contenant les lignes lors des éditions V3
//==>   La version 2.9 Adaptation suite à la modification de la classe User (suppression des variables conteannt le dossier racine)
//==>   La version 3.0 Migration vers fop 2.3
//==>   La version 3.5 Modifications radicales sur la manière d'échanger les paramètres entre Série N et NewSim
//==>   La version 3.6 Ajout de la possibilité de fusionner des documents PDF
//==>   La version 3.6b Le documents stocké dans les docments liés n'est provisoirement plus supprimé
//==>   La version 3.6c Correction d'un bug de syntaxe lors du listage des pièces jointes + amélioration des traces sur les pièces jointes
//==>   La version 4.0 Refonte en profondeur du code afin de mieux gérer les canaux de diffusion
//==>   La version 4.1 Correction d'un bug sur la suppresion des spools qui était désactivée
//==>   La version 5 Transformation de l'application en module
//==>   La version 5.1 Corrige des problème de thread pour la fusion de document
//=================================================================================================
public class ServeurNewSim implements InterfaceModule {
  // Constantes
  private static final String VERSION = "5.1";
  private static final int delaiHeure = 4;
  
  // Variables
  private User utilisateur = new User();
  private Integer port = null;
  private boolean modeDebug = false;
  private ServerSocket serveurSocket = null;
  private boolean attenteConnexion = true;
  private String[] listFileToRemoveInLib =
      new String[] { "NewSimSrv.version", "avalon-framework-4.2.0.jar", "batik-all-1.7.jar", "fontbox-1.8.4.jar", "pdfbox-1.8.4.jar",
          "xalan-2.7.0.jar", "xmlgraphics-commons-1.4.jar", "xercesImpl-2.7.1.jar", "fontbox-1.8.5.jar",
          // Nettoyage pour le passage en fop 2.2
          "testEdit.jar", "NewSimSrv.jar", "NewSimSrv.exe", "NewSimSrv.properties", "batik-all-1.8.jar", "fontbox-1.8.11.jar",
          "serializer-2.7.0.jar", "xmlgraphics-commons-2.1.jar",
          // Nettoyage pour le passage en fop 2.3
          "fop.jar", "batik-all-1.9.jar", "fontbox-2.0.4.jar", "xmlgraphics-commons-2.2.jar", "VeilleOutq.jar", "riComposants.jar",
          "GuiNewSimSrv.jar", "newsim_off.bat", "VerifImprimante.jar", "EditionEditor.jar",
          // Nettoyage pour la transformation en module
          "SerieNNewSim.jar" };
  
  private static EnumModule enumModule = EnumModule.NEWSIM;
  private static boolean moduleActif = false;
  private static ListeParametreModule listeParametre = null;
  private static ArrayList<SessionNewSim> listeSessionNewSim = new ArrayList<SessionNewSim>();
  
  /**
   * Constructeur de la classe
   */
  public ServeurNewSim() {
  }
  
  // -- Méthodes publiques
  
  /**
   * Initialiser le module.
   */
  public void lancerModule() {
    // Supprimer les fichiers obsolètes du dossier lib
    nettoyerDossierLib();
    
    // Supprimer tous les dossier du dossier temporaire
    nettoyerDossierTmpSim();
    
    // Supprimer les fichiers de logs lorsque le module était encore une application indépendante
    supprimerAncienFichierLog();
    
    // Conversion des fichiers sérialisés pour le passage en 2.14
    convertirFichiersSerialisesEn214();
    
    try {
      // Attente de la connection d'un client
      serveurSocket = new ServerSocket(port.intValue());
      
      moduleActif = true;
      Trace.info("Le module " + enumModule + " est démarré.");
      
      attenteConnexion = true;
      while (attenteConnexion) {
        Socket nouveauclient = serveurSocket.accept();
        
        // Suppression des dossiers temporaires du dossier tmp/sim qui ont plus d'heure
        nettoyerDossierTemporaire(delaiHeure);
        
        // Test de consommation de ram
        Trace.info("Nouvelle connexion entrante.");
        Trace.debugMemoire(ServeurNewSim.class, enumModule.getLibelle());
        
        User client = new User();
        client.setAdresseIP(utilisateur.getAdresseIP());
        client.setProfil(utilisateur.getProfil());
        client.setMotDePasse(utilisateur.getMotDePasse());
        SessionNewSim sessionNewSim = new SessionNewSim(ManagerAS400.getSystemeAS400(), nouveauclient, client, modeDebug);
        sessionNewSim.start();
      }
    }
    catch (IOException e) {
      if (serveurSocket != null && serveurSocket.isClosed()) {
        Trace.info("La socket pour Newsim est fermé.");
      }
      else {
        Trace.erreur(e, "Erreur socket survenue.");
      }
    }
    finally {
      fermerSocket();
    }
  }
  
  /**
   * Retourne le dossier racine du serveur.
   * A certainement supprimer.
   */
  public static String getDossierRacineSerieNServeur() {
    return ManagerAS400.getDossierRacineServeur();
  }
  
  /**
   * Retourne les paramètres propre à ce module.
   */
  @Override
  public ListeParametreModule listerParametre() {
    return listeParametre;
  }
  
  /**
   * Retourne si le module est l'application principale car cette dernière est considérée comme un "pseudo module".
   */
  @Override
  public boolean isApplication() {
    return false;
  }
  
  /**
   * Démarre le module Newsim.
   */
  @Override
  public void demarrer() {
    if (moduleActif) {
      Trace.alerte("Le module " + enumModule + " est déjà actif.");
      return;
    }
    Trace.info("Démarrage du module " + enumModule + '.');
    
    // Initialisation et contrôle des paramètres
    initialiserParametre();
    
    // Démarrage du module
    new Thread(new Runnable() {
      @Override
      public void run() {
        lancerModule();
      }
    }, enumModule.getLibelle()).start();
  }
  
  /**
   * Arrête le module Newsim.
   */
  @Override
  public void arreter() {
    if (!moduleActif) {
      Trace.alerte("Le module " + enumModule + " n'est pas actif.");
      return;
    }
    
    // Fermeture de la socket
    fermerSocket();
    
    // Arrêt des instances clients
    if (listeSessionNewSim == null) {
      return;
    }
    for (int i = 0; i < listeSessionNewSim.size(); i++) {
      if (listeSessionNewSim.get(i).isAlive()) {
        listeSessionNewSim.get(i).interrupt();
      }
    }
    listeSessionNewSim.clear();
    
    moduleActif = false;
    Trace.info("Module " + enumModule + " stoppé.");
  }
  
  /**
   * Retourne si le module est actif.
   */
  @Override
  public boolean isDemarre() {
    return moduleActif;
  }
  
  /**
   * Envoi un ordre au module Newsim.
   */
  @Override
  public Object envoyerControle(EnumControleModule pEnumControleModule, Object pParametre) {
    Trace.info("Message de contrôle '" + pEnumControleModule.getTexte() + "' reçu par " + enumModule + '.');
    
    // Retourner l'enum à partir du texte
    try {
      switch (pEnumControleModule) {
        case NEWSIM_START:
          // Recherche les paramètres pour ce module dans le cas où ils auraient été modifié
          ManagerParametreModule.rechargerParametre(pEnumControleModule.getEnumModule());
          // Démarre le module
          demarrer();
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return null;
        
        case NEWSIM_STOP:
          arreter();
          return null;
        
        case NEWSIM_STATUS:
          String retour;
          if (moduleActif) {
            retour = "Le module " + enumModule + " est démarré.";
          }
          else {
            retour = "Le module " + enumModule + " est arrêté.";
          }
          Trace.info(retour);
          // Trace la liste des paramètres du module
          tracerListeParametre();
          return retour;
        
        default:
          Trace.erreur("L'enumControleModule " + pEnumControleModule + " n'est pas pris en compte par " + enumModule + '.');
          break;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "");
    }
    return null;
  }
  
  /**
   * Ecrit la liste des paramètres du module dans les traces.
   */
  @Override
  public void tracerListeParametre() {
    if (listeParametre == null || listeParametre.isEmpty()) {
      Trace.alerte("La liste des paramètres en cours est vide.");
      return;
    }
    
    for (ParametreModule parametre : listeParametre) {
      String origine = "Non défini";
      if (parametre.getOrigine() != null) {
        origine = parametre.getOrigine().getLibelle();
      }
      Trace.info(String.format("%-50s: %-20s (%s)", parametre.getDescription(), parametre.getValeurEnString(), origine));
    }
  }
  
  // -- Méthodes privées
  
  /**
   * Initialiser les paramètres du module à partir de la liste des paramètres pour ce module.
   */
  private void initialiserParametre() {
    // Extraction des paramètres spécifiques pour ce module
    listeParametre = ManagerParametreModule.extraireParametreModule(enumModule);
    if (listeParametre == null || listeParametre.isEmpty()) {
      throw new MessageErreurException("Aucun paramètre pour le module " + enumModule + " n'a été trouvé.");
    }
    
    // Le port utilisé par le serveur
    port = listeParametre.retournerValeurEnInteger(EnumParametreModule.NEWSIM_PORT);
    if (port == null || port <= 0) {
      port = Integer.valueOf(ConstantesNewSim.SERVER_PORT);
    }
    
    // Le mode débug
    Integer valeur = listeParametre.retournerValeurEnInteger(EnumParametreModule.NEWSIM_MODE_DEBUG);
    if (valeur == null || valeur <= 0) {
      modeDebug = false;
    }
    else {
      modeDebug = true;
    }
    
    // Contrôle du port
    if (port == null || port <= 0) {
      throw new MessageErreurException("Le port pour " + enumModule + " est invalide.");
    }
  }
  
  /**
   * Ferme la connexion socket.
   */
  private void fermerSocket() {
    attenteConnexion = false;
    if (serveurSocket != null && !serveurSocket.isClosed()) {
      try {
        serveurSocket.close();
      }
      catch (IOException ioe) {
        Trace.erreur(ioe, "Erreur lors de la fermeture de la connexion réseau.");
      }
    }
  }
  
  /**
   * Lancement du contrôle du dossier tmp/sim afin de supprimer les dossiers temporaires qui ont été créé il y a plus d'une heure.
   */
  private void nettoyerDossierTemporaire(int pCycleEnHeure) {
    final long delai = pCycleEnHeure * 3600 * 1000;
    File dossierTemporaire = new File(getDossierRacineSerieNServeur() + File.separatorChar + Constantes.DOSSIER_TMP_SIM);
    
    Trace.info("Lancement du contrôle pour la suppression des dossiers temporaires de " + dossierTemporaire.getAbsolutePath()
        + " qui ont plus d'une heure d'existence.");
    
    // Si le dossier à surveiller n'existe pas, il est créé
    if (!dossierTemporaire.exists()) {
      dossierTemporaire.mkdirs();
    }
    
    // Balayage du contenu du dossier
    File[] listeContenu = dossierTemporaire.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File pDossier, String pNomFichier) {
        File fichier = new File(pDossier.getAbsoluteFile() + File.separator + pNomFichier);
        long delta = new Date().getTime() - fichier.lastModified();
        if (delta > delai) {
          return true;
        }
        return false;
      }
    });
    // Suppression de chaque élément (fichier ou dossier)
    if (listeContenu != null && listeContenu.length > 0) {
      Trace.info(listeContenu.length + " dossiers vont être supprimés.");
      for (File element : listeContenu) {
        FileNG.remove(element);
        Trace.info("Le dossier " + element.getAbsolutePath() + " a été supprimé.");
      }
    }
  }
  
  /**
   * Supprime tous les fichiers du dossier TMP de Newsim (de manière récursive).
   */
  private void nettoyerDossierTmpSim() {
    File dossierTmp = new File(getDossierRacineSerieNServeur() + File.separatorChar + Constantes.DOSSIER_TMP_SIM);
    if (!dossierTmp.exists()) {
      return;
    }
    File[] listeFichiers = dossierTmp.listFiles();
    if (listeFichiers == null) {
      return;
    }
    for (int i = 0; i < listeFichiers.length; i++) {
      FileNG.remove(listeFichiers[i]);
    }
    Trace.info("Nettoyage du dossier " + dossierTmp.getAbsolutePath() + " effectué.");
  }
  
  /**
   * Supprime tous les fichiers obsolètes du dossier LIB.
   */
  private void nettoyerDossierLib() {
    File dossierLib = new File(getDossierRacineSerieNServeur() + File.separatorChar + Constantes.DOSSIER_LIB);
    File[] listeFichiers = dossierLib.listFiles();
    if (listeFichiers == null) {
      return;
    }
    
    for (File file : listeFichiers) {
      for (String file2remove : listFileToRemoveInLib) {
        if (file.getName().equals(file2remove)) {
          if (file.delete()) {
            Trace.info("Suppression de  " + file.getAbsolutePath() + " effectuée.");
          }
        }
      }
    }
    Trace.info("Nettoyage du dossier " + dossierLib.getAbsolutePath() + " effectué.");
  }
  
  /**
   * Supprime tous les fichiers de logs de l'époque où Newsim était une application indépendante (version < 5).
   */
  private void supprimerAncienFichierLog() {
    File dossierLog = new File(Trace.getDossierTraces());
    if (!dossierLog.exists()) {
      return;
    }
    // Listage de tous les anciens fichiers de log
    File[] listeFichiers = dossierLog.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.startsWith("newsim.")) {
          return true;
        }
        return false;
      }
    });
    if (listeFichiers == null || listeFichiers.length == 0) {
      return;
    }
    for (int i = 0; i < listeFichiers.length; i++) {
      FileNG.remove(listeFichiers[i]);
    }
    Trace.info("Nettoyage du dossier " + dossierLog.getAbsolutePath() + " effectué.");
  }
  
  /**
   * Convertir les fichiers sérialisés pour la version 2.14.
   */
  private void convertirFichiersSerialisesEn214() {
    // Lister les fichiers sérialisés
    FileNG dossierSim = new FileNG(getDossierRacineSerieNServeur() + File.separatorChar + Constantes.DOSSIER_SIM);
    FilenameFilter filtre = new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        if (name.toLowerCase().endsWith(".ddd") || name.toLowerCase().endsWith(".ddp") || name.toLowerCase().endsWith(".dde")) {
          return true;
        }
        return false;
      }
    };
    ArrayList<File> listeXML = new ArrayList<File>();
    dossierSim.filtreFichier(listeXML, filtre);
    
    // Convertir si besoin
    for (File fichier : listeXML) {
      try {
        if (Constantes.convert213to214(fichier.getAbsolutePath(), "ri.serien.newsim.")) {
          Trace.info("Conversion du fichier sérialisé " + fichier.getAbsolutePath());
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Echec de la conversion du fichier sérialisé " + fichier.getAbsolutePath());
      }
    }
  }
  
  // -- Accesseurs
  
  public static ArrayList<SessionNewSim> getListeSessionNewSim() {
    return listeSessionNewSim;
  }
  
}
