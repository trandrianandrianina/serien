/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.serveur;

import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;

/**
 * Cette classe regroupe les informations d'un fichier généré pour un document.
 */
public class TypeDocumentGenere {
  // Variables
  private EnumTypeDocument typeDocument = null;
  private String nomFichierSansExtension = null;
  private String cheminCompletFichierTemporaire = null;
  private boolean verifie = false;
  private boolean utilisationEnSoumission = false;
  // Cette variable sera renseignée si le fichier a été copié dans la base documentaire
  private String cheminCompletBaseDocumentaire = null;
  // Cette variable sera renseignée si le fichier a été copié dans l'Ifs
  private String cheminCompletIfs = null;
  
  /**
   * Constructeur.
   */
  public TypeDocumentGenere(EnumTypeDocument pTypeDocument) {
    typeDocument = pTypeDocument;
  }
  
  // -- Accesseurs
  
  public EnumTypeDocument getTypeDocument() {
    return typeDocument;
  }
  
  public String getNomFichierSansExtension() {
    return nomFichierSansExtension;
  }
  
  public void setNomFichierSansExtension(String pNomFichierSansExtension) {
    this.nomFichierSansExtension = pNomFichierSansExtension;
  }
  
  public String getCheminCompletFichierTemporaire() {
    return cheminCompletFichierTemporaire;
  }
  
  public void setCheminCompletFichierTemporaire(String pCheminFichierTemporaire) {
    this.cheminCompletFichierTemporaire = pCheminFichierTemporaire;
  }
  
  public boolean isVerifie() {
    return verifie;
  }
  
  public void setVerifie(boolean pVerifie) {
    this.verifie = pVerifie;
  }
  
  public boolean isUtilisationEnSoumission() {
    return utilisationEnSoumission;
  }
  
  public void setUtilisationEnSoumission(boolean utilisationEnSoumission) {
    this.utilisationEnSoumission = utilisationEnSoumission;
  }
  
  public String getCheminCompletBaseDocumentaire() {
    return cheminCompletBaseDocumentaire;
  }
  
  public void setCheminCompletBaseDocumentaire(String cheminCompletBaseDocumentaire) {
    this.cheminCompletBaseDocumentaire = cheminCompletBaseDocumentaire;
  }
  
  public String getCheminCompletIfs() {
    return cheminCompletIfs;
  }
  
  public void setCheminCompletIfs(String cheminCompletIfs) {
    this.cheminCompletIfs = cheminCompletIfs;
  }
  
}
