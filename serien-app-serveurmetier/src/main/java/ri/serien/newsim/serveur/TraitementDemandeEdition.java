/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.serveur;

import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.print.PrintService;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import com.ibm.as400.access.AS400;
import com.ibm.as400.access.IFSFile;
import com.ibm.as400.access.OutputQueue;
import com.ibm.as400.access.SpooledFile;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ri.serien.libas400.basedocumentaire.BaseDocumentaire;
import ri.serien.libas400.dao.exp.programs.doclies.GM_DocumentLie;
import ri.serien.libas400.dao.sql.exploitation.edition.SqlDemandeEdition;
import ri.serien.libas400.dao.sql.exploitation.mail.SqlMail;
import ri.serien.libas400.dao.sql.gescom.parametres.SqlGescomParametres;
import ri.serien.libas400.database.QueryManager;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libas400.system.SystemeManager;
import ri.serien.libas400.system.bibliotheque.BibliothequeAS400;
import ri.serien.libas400.system.edition.GestionSpoolAS400;
import ri.serien.libcommun.exploitation.bibliotheque.Bibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.EnumTypeBibliotheque;
import ri.serien.libcommun.exploitation.bibliotheque.IdBibliotheque;
import ri.serien.libcommun.exploitation.documentstocke.DocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.EnumTypeDocumentStocke;
import ri.serien.libcommun.exploitation.documentstocke.IdDocumentStocke;
import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.libcommun.exploitation.edition.EnumActionSurSpool;
import ri.serien.libcommun.exploitation.edition.EnumCodeErreur;
import ri.serien.libcommun.exploitation.edition.EnumLangageImprimante;
import ri.serien.libcommun.exploitation.edition.EnumStatutEdition;
import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.exploitation.edition.EnumTypeParametreEdition;
import ri.serien.libcommun.exploitation.edition.EnumTypeRectoVerso;
import ri.serien.libcommun.exploitation.edition.ListeParametreEdition;
import ri.serien.libcommun.exploitation.edition.ParametreEdition;
import ri.serien.libcommun.exploitation.edition.SpoolOld;
import ri.serien.libcommun.exploitation.edition.demandeedition.DemandeEdition;
import ri.serien.libcommun.exploitation.edition.demandeedition.IdDemandeEdition;
import ri.serien.libcommun.exploitation.mail.EnumStatutMail;
import ri.serien.libcommun.exploitation.mail.IdMail;
import ri.serien.libcommun.exploitation.mail.IdPieceJointe;
import ri.serien.libcommun.exploitation.mail.Mail;
import ri.serien.libcommun.exploitation.mail.PieceJointeDocumentStocke;
import ri.serien.libcommun.exploitation.mail.PieceJointeFichier;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.EnumParametreSysteme;
import ri.serien.libcommun.gescom.personnalisation.parametresysteme.ListeParametreSysteme;
import ri.serien.libcommun.gescom.vente.document.IdDocumentVente;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.DateHeure;
import ri.serien.libcommun.outils.encodage.XMLTools;
import ri.serien.libcommun.outils.fichier.FileNG;
import ri.serien.libcommun.outils.fichier.GestionFichierTexte;
import ri.serien.libcommun.outils.session.User;
import ri.serien.newsim.canaldiffusion.CanalDiffusion;
import ri.serien.newsim.canaldiffusion.CanalDiffusionDocumentStocke;
import ri.serien.newsim.canaldiffusion.CanalDiffusionFax;
import ri.serien.newsim.canaldiffusion.CanalDiffusionFichierIfs;
import ri.serien.newsim.canaldiffusion.CanalDiffusionFusionFichier;
import ri.serien.newsim.canaldiffusion.CanalDiffusionImpression;
import ri.serien.newsim.canaldiffusion.CanalDiffusionMail;
import ri.serien.newsim.description.DescriptionDocument;
import ri.serien.newsim.description.DescriptionEdition;
import ri.serien.newsim.description.DescriptionVariable;
import ri.serien.newsim.rendu.GenerationFichierFO;
import ri.serien.newsim.rendu.GenerationFichierFinal;

/**
 * Traitement d'une demande d'édition.
 * Cette classe analyse la demande et effectue les transformations qui en découlent.
 */
public class TraitementDemandeEdition {
  // Constantes
  private final static String OUTQSRC = "/QSYS.LIB/QGPL.LIB/NEWSIM.OUTQ";
  
  // Variables
  private GestionSpoolAS400 gestionSpool = null;
  private SystemeManager systemeManager = null;
  private QueryManager queryManager = null;
  private User utilisateurNewSim = null;
  private ArrayList<DescriptionVariable> listeVariables = new ArrayList<DescriptionVariable>();
  private boolean modeDebug = false;
  private String dossierSimSerieNServeur = null;
  private String dossierTempUserSerieNServeur = null;
  private SqlDemandeEdition sqlDemandeEdition = null;
  
  private IdDemandeEdition idDemandeEdition = null;
  private DemandeEdition demandeEdition = null;
  
  /**
   * Constructeur
   */
  public TraitementDemandeEdition(AS400 pSysteme, User pUtilisateur, boolean pModeDebug) {
    utilisateurNewSim = pUtilisateur;
    dossierSimSerieNServeur = ServeurNewSim.getDossierRacineSerieNServeur() + File.separatorChar + ConstantesNewSim.DOSSIER_SIM;
    dossierTempUserSerieNServeur = ServeurNewSim.getDossierRacineSerieNServeur() + File.separatorChar + ConstantesNewSim.DOSSIER_TMP_SIM
        + File.separatorChar + utilisateurNewSim.getId();
    File dossierTempUser = new File(dossierTempUserSerieNServeur);
    if (!dossierTempUser.exists()) {
      dossierTempUser.mkdirs();
    }
    
    systemeManager = new SystemeManager(pSysteme, true);
    queryManager = new QueryManager(systemeManager.getDatabaseManager().getConnection());
    gestionSpool = new GestionSpoolAS400(pSysteme);
    modeDebug = pModeDebug;
  }
  
  // -- Méthodes publiques
  
  /**
   * Vérifie si la demande contient une fusion de documents.
   */
  public boolean isFusionDemandee(String pParametresBruts) {
    // Construction de la demande d'édition à partir des données stockées en table
    idDemandeEdition = construireIdDemandeAvecParametreBrut(pParametresBruts);
    demandeEdition = chargerDemandeEdition(idDemandeEdition);
    
    // Contrôle avec le nom du fichier final de fusion
    String nomFichierFusion = Constantes.normerTexte(demandeEdition.getCheminFichierDestinationFusion());
    if (!nomFichierFusion.isEmpty()) {
      Trace.info("Fichier de fusion : " + nomFichierFusion);
      return true;
    }
    return false;
  }
  
  /**
   * Traite la demande de l'utilisateur.
   */
  public String traiterDemandeEdition(String pParametresBruts) {
    // Contrôle si l'identifiant n'existe pas déjà
    if (idDemandeEdition == null) {
      // Construction de la demande d'édition à partir des données stockées en table
      idDemandeEdition = construireIdDemandeAvecParametreBrut(pParametresBruts);
      demandeEdition = chargerDemandeEdition(idDemandeEdition);
    }
    
    // Mise à jour du statut de la demande en début de traitement
    demandeEdition.setStatut(EnumStatutEdition.EN_COURS);
    DescriptionEdition dde = null;
    try {
      sqlDemandeEdition.sauverStatut(demandeEdition);
      
      // Création de la descrption d'édition à partir de la demande d'édition
      dde = creerDescriptionEdition(demandeEdition);
      
      // Génération des types de document nécessaires
      boolean succes = genererFichier(dde, demandeEdition);
      
      // Traitement des différents canaux de diffusion
      if (succes) {
        traiterCanalDiffusion(dde, demandeEdition);
      }
      
      // Suppression de la bibliothèque temporaire si nécessaire (cas édition sans spool)
      supprimerBibliothequeTemporaire(dde);
    }
    catch (Exception e) {
      demandeEdition.setStatut(EnumStatutEdition.ECHEC);
    }
    
    // Mise à jour du statut de la demande en fin de traitement
    sqlDemandeEdition.sauverStatut(demandeEdition);
    
    // Retourne le chemin du fichier généré dans le dossier temporaire qui ne sera pas détruit de suite
    // La destruction aura lieu au bout de 4 heures suivant la création du dossier ou au démarrage du serveur Newsim
    String cheminCompletARetourner = "";
    if (dde != null) {
      TypeDocumentGenere typeDocumentGenere = dde.getMapTypeDocument().get(EnumTypeDocument.PDF);
      if (typeDocumentGenere != null) {
        cheminCompletARetourner = Constantes.normerTexte(typeDocumentGenere.getCheminCompletFichierTemporaire());
      }
    }
    
    return cheminCompletARetourner;
  }
  
  // -- Méthodes privées
  
  /**
   * Construit l'identifiant de la demande à partir du paramètre brut.
   * Contrôle les paramètres reçus sur la socket permettant de charger la demande en table.
   * Le paramètre est une chaine de caractères qui contient dans cet ordre :
   * - la bibliothèque (10 caractères)
   * - le code établissement (3 caractères)
   * - le numéro de la demande (9 chiffres)
   * Ces données sont simplement concaténées sans caractères de séparation (les champs ne sont bien sur pas trimés).
   */
  private static IdDemandeEdition construireIdDemandeAvecParametreBrut(String pParametreBrut) {
    pParametreBrut = Constantes.normerTexte(pParametreBrut);
    if (pParametreBrut.isEmpty()) {
      throw new MessageErreurException("Cette demande d'édition a été ignorée car il n'y aucun paramètre.");
    }
    
    // Calcul de la longueur minimum du paramètre brut (bibliothèque + établissement + 1 chiffre pour le numéro de la
    // demande)
    int longueur = IdBibliotheque.LONGUEUR_NOM_BIBLIOTHEQUE + IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT + 1;
    if (pParametreBrut.length() < longueur) {
      throw new MessageErreurException(
          "Cette demande d'édition a été ignorée car il manque un ou plusieurs paramètres ou ils ne sont pas formattés correctement : '"
              + pParametreBrut + "'");
    }
    
    // Découpage du paramètre brut
    int posDebut = 0;
    int posFin = IdBibliotheque.LONGUEUR_NOM_BIBLIOTHEQUE;
    String nomBaseDeDonnees = pParametreBrut.substring(posDebut, posFin);
    IdBibliotheque idBaseDeDonnees = IdBibliotheque.getInstance(nomBaseDeDonnees);
    
    posDebut = posFin;
    posFin = posFin + IdEtablissement.LONGUEUR_CODE_ETABLISSEMENT;
    String codeEtablissement = pParametreBrut.substring(posDebut, posFin);
    IdEtablissement idEtablissement = IdEtablissement.getInstance(codeEtablissement);
    
    posDebut = posFin;
    String numero = pParametreBrut.substring(posDebut).trim();
    Integer numeroDemande = Integer.valueOf(numero);
    
    // Création de l'identifiant de la demande d'édition
    return IdDemandeEdition.getInstance(idEtablissement, numeroDemande, idBaseDeDonnees);
  }
  
  /**
   * Lit la demande en base de données à partir de son identifiant.
   */
  private DemandeEdition chargerDemandeEdition(IdDemandeEdition pIdDemandeEdition) {
    if (pIdDemandeEdition == null) {
      throw new MessageErreurException("L'identifiant de la demande d'édition est invalide.");
    }
    queryManager.setLibrary(pIdDemandeEdition.getIdBaseDeDonnees());
    
    sqlDemandeEdition = new SqlDemandeEdition(queryManager);
    return sqlDemandeEdition.chargerDemandeEdition(pIdDemandeEdition);
  }
  
  /**
   * Exporte les données des tables DB2 vers un fichier XML.
   * La méthode retourne le nom du document sans l'extension.
   */
  private String exporterBaseDonneesVersXML(String xmlfile, DescriptionEdition dde, DemandeEdition pDemandeEdition) {
    IdEtablissement idEtablissement = null;
    if (pDemandeEdition != null) {
      idEtablissement = pDemandeEdition.getId().getIdEtablissement();
    }
    
    String[] path1 = { "document" };
    String[] path2 = { "document", "body" };
    
    // Test sur la table des lignes afin de voir s'il n'y a pas un problème d'alimentation avec les services RPG
    // Un test sur l'entête ne suffit pas donc on teste les 2
    ArrayList<GenericRecord> listeRecord = queryManager.select("select * from " + dde.getLibTemp() + "." + dde.getFicLines());
    if (listeRecord == null || listeRecord.isEmpty()) {
      Trace.erreur("Aucun enregistrement trouvé dans la table contenant les lignes : " + dde.getLibTemp() + "." + dde.getFicLines());
      return null;
    }
    
    // Requêtes afin de récupérer les données dans les 2 fichiers
    ArrayList<Document> lstdoc = new ArrayList<Document>();
    listeRecord = queryManager.select("select * from " + dde.getLibTemp() + "." + dde.getFicHeaders() + " where EFNUM <> 0");
    if (listeRecord == null || listeRecord.isEmpty()) {
      Trace.erreur("Aucun enregistrement trouvé dans la table contenant les entêtes : " + dde.getLibTemp() + "." + dde.getFicHeaders());
      return null;
    }
    String nomFichierAGenererSansExtension = null;
    for (GenericRecord rcd : listeRecord) {
      Document doc = null;
      String efetb = ((String) rcd.getField("EFETB")).trim();
      String efdocx = ((String) rcd.getField("EFDOCX")).trim();
      // Si l'idEtablissement vaut null alors initialisation avec cette variable (c'est un cache misère £DTAFILE qui n'a pas été
      // initialisé correctement à l'origine)
      if (idEtablissement == null || idEtablissement.getCodeEtablissement().trim().isEmpty()) {
        idEtablissement = IdEtablissement.getInstance(efetb);
      }
      // Le nom est base sur le premier enregistrement (à améliorer)
      if (nomFichierAGenererSansExtension == null) {
        nomFichierAGenererSansExtension = efetb.trim() + efdocx.trim();
      }
      doc = queryManager.select2XML("select * from " + dde.getLibTemp() + "." + dde.getFicHeaders() + " where EFETB = '" + efetb
          + "' and EFDOCX = '" + efdocx + "'", path1, "header", doc);
      queryManager.select2XML(
          "select * from " + dde.getLibTemp() + "." + dde.getFicLines() + " where LFETB = '" + efetb + "' and LFDOCX = '" + efdocx + "'",
          path2, "line", doc);
      lstdoc.add(doc);
      
      // On refait une requête pour re-récupérer l'entête que l'on met dans le pied (pas bo à refaire)
      queryManager.select2XML("select * from " + dde.getLibTemp() + "." + dde.getFicHeaders() + " where EFETB = '" + efetb
          + "' and EFDOCX = '" + efdocx + "'", path1, "footer", doc);
    }
    
    // Fusion des documents
    Document folder = null;
    try {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      DocumentBuilder builder = factory.newDocumentBuilder();
      folder = builder.newDocument();
      Element root = folder.createElement("folder");
      folder.appendChild(root);
      for (Document d : lstdoc) {
        Node imported = folder.importNode(d.getDocumentElement(), true);
        root.appendChild(imported);
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la création d'un fichier XML à partir de la base de données");
    }
    
    // Lecture de la PS335 pour savoir s'il faut insérer les liens des fiches techniques et les photos
    Character ps335 = lecturePS335(idEtablissement);
    if (ps335 != null) {
      // Insertion des tags LFPHO
      if (ps335.charValue() == '2' || ps335.charValue() == '3') {
        insererTagLFPHODansLigne(folder);
      }
      // Insertion des tags LFWEB
      if (ps335.charValue() == '1' || ps335.charValue() == '3') {
        insererTagLFWEBDansLigne(folder);
      }
    }
    
    // Ecriture dans le fichier
    XMLTools.toFile(folder, xmlfile);
    if (xmlfile != null && folder != null) {
      File fichierXml = new File(xmlfile);
      if (fichierXml.exists()) {
        Trace.info("Le fichier " + xmlfile + " a une taille de " + fichierXml.length() + " octets.");
      }
      else {
        Trace.info("Le fichier " + xmlfile + " n'a pas été créé.");
      }
    }
    Trace.info("Le document aura pour nom " + nomFichierAGenererSansExtension);
    
    return nomFichierAGenererSansExtension;
  }
  
  /**
   * Supprime la bibliothèque temporaire si présente pour les éditions sans spool.
   */
  private void supprimerBibliothequeTemporaire(DescriptionEdition pDde) {
    if (pDde == null || Constantes.normerTexte(pDde.getLibTemp()).isEmpty()) {
      return;
    }
    
    Bibliotheque bibliotheque = new Bibliotheque(IdBibliotheque.getInstance(pDde.getLibTemp()), EnumTypeBibliotheque.TEMPORAIRE);
    BibliothequeAS400 bibliothequeAS400 = new BibliothequeAS400(systemeManager.getSystem(), bibliotheque);
    if (bibliothequeAS400.delete()) {
      Trace.info("Bibliothèque temporaire " + bibliotheque + " supprimée avec succés.");
    }
    else {
      Trace.alerte("Echec lors de la suppression de bibliothèque temporaire " + bibliotheque + ".");
    }
  }
  
  /**
   * Récupération de la PS335 qui indique les informations à ajouter à l'édition (lien fiche technique et/ou photos).
   */
  private Character lecturePS335(IdEtablissement pIdEtablissement) {
    if (pIdEtablissement == null) {
      return null;
    }
    try {
      SqlGescomParametres operation = new SqlGescomParametres(queryManager);
      ListeParametreSysteme listeParametreSysteme = operation.chargerListeParametreSysteme(pIdEtablissement);
      if (listeParametreSysteme == null || listeParametreSysteme.isEmpty()) {
        return null;
      }
      return listeParametreSysteme.getValeurParametreSysteme(pIdEtablissement, EnumParametreSysteme.PS335);
    }
    catch (Exception e) {
    }
    return null;
  }
  
  /**
   * Insère le tag LFPHO dans toute les lignes s'il existe une photo.
   * Attention la racine est en dur TODO à corriger.
   */
  private void insererTagLFPHODansLigne(Document pDocumentXml) {
    try {
      GM_DocumentLie gdl = new GM_DocumentLie(queryManager);
      
      NodeList nList = pDocumentXml.getElementsByTagName("line");
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        String lfpho_value = null;
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          String lfart = eElement.getElementsByTagName("LFART").item(0).getTextContent();
          if (!lfart.equals("")) {
            String lfetb = eElement.getElementsByTagName("LFETB").item(0).getTextContent();
            lfpho_value = gdl.getPathArticles(lfetb, lfart, "PHO", "ART");
            lfpho_value = controlerDossierImage(lfpho_value);
            if (lfpho_value != null) {
              Element lfpho = pDocumentXml.createElement("LFPHO");
              lfpho.appendChild(pDocumentXml.createTextNode(lfpho_value));
              nNode.appendChild(lfpho);
            }
          }
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'insertion des photos");
    }
  }
  
  /**
   * Insère le tag LFWEB dans toute les lignes s'il existe un lien.
   * Attention la racine est en dur TODO à corriger.
   */
  private void insererTagLFWEBDansLigne(Document pDocumentXml) {
    try {
      NodeList nList = pDocumentXml.getElementsByTagName("line");
      for (int temp = 0; temp < nList.getLength(); temp++) {
        Node nNode = nList.item(temp);
        if (nNode.getNodeType() == Node.ELEMENT_NODE) {
          Element eElement = (Element) nNode;
          String lfart = eElement.getElementsByTagName("LFART").item(0).getTextContent();
          if (!lfart.equals("")) {
            String lfetb = eElement.getElementsByTagName("LFETB").item(0).getTextContent();
            // Requête pour récupérer les URLs
            ArrayList<GenericRecord> lst = queryManager.select(
                "select * from " + queryManager.getLibrary() + ".PSEMFTCM where FTETB = '" + lfetb + "' and FTART = '" + lfart + "'");
            if ((lst == null) || (lst.size() == 0)) {
              continue;
            }
            for (int i = 0; i < lst.size(); i++) {
              // Le \n est pour que les URLs soient présentées les une en dessous des autres
              String suadr = ((String) lst.get(i).getField("FTURL")).trim() + '\n';
              if (suadr.length() > 0) {
                Element lfweb = pDocumentXml.createElement("LFWEB" + (i + 1));
                lfweb.appendChild(pDocumentXml.createTextNode(suadr));
                nNode.appendChild(lfweb);
              }
            }
          }
        }
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de l'insertion des liens html");
    }
  }
  
  /**
   * Contrôle l'existence du dossier et retourne le chemin d'une image formatée.
   */
  private String controlerDossierImage(String folder) {
    if ((folder == null) || (folder.trim().length() == 0)) {
      return null;
    }
    
    File path = new File(folder);
    if (!path.exists()) {
      return null;
    }
    
    File[] list = path.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(".gif") || name.toLowerCase().endsWith(".bmp") || name.toLowerCase().endsWith(".jpg")
            || name.toLowerCase().endsWith(".png");
      }
    });
    
    if (list.length > 0) {
      return "file://" + list[0].getAbsolutePath().replace('\\', '/');
    }
    return null;
    
  }
  
  /**
   * Génère un fichier à partir de la base de données.
   */
  private void creerDocumentAvecBdd(TypeDocumentGenere pTypeDocumentGenere, DescriptionEdition pDde) {
    // Initialisation du dossier contenant le fond de page
    String dossierFondDePage = dossierSimSerieNServeur + File.separatorChar + pDde.getNomSpool().toLowerCase();
    
    // Ajout du préfixe si présent
    if (!pDde.getPrefixeNomFichierGenere().isEmpty()) {
      pTypeDocumentGenere
          .setNomFichierSansExtension(pDde.getPrefixeNomFichierGenere() + pTypeDocumentGenere.getNomFichierSansExtension());
    }
    /*
    if (pCanalDiffusion instanceof CanalDiffusionDocumentLie) {
      ((CanalDiffusionDocumentLie) pCanalDiffusion).setNomFichier(nombon);
    }*/
    String cheminCompletFichierGenere =
        dossierTempUserSerieNServeur + File.separatorChar + pTypeDocumentGenere.getNomFichierSansExtension();
    
    // Récupération de la description du document s'il y en a une
    String nomFichierXSL = Constantes.normerTexte(pDde.getFichierDescriptionDocument());
    if (nomFichierXSL.isEmpty()) {
      Trace.erreur("Le fichier XSL est invalide");
      return;
    }
    File fichierXSL = new File(dossierFondDePage + File.separatorChar + nomFichierXSL);
    if (!fichierXSL.exists()) {
      Trace.erreur("Le fichier XSL n'a pas été trouvé à l'endroit indiqué " + fichierXSL.getAbsolutePath());
      return;
    }
    
    // Génération du fichier
    GenerationFichierFinal gff = new GenerationFichierFinal();
    gff.setTypeSortie(pTypeDocumentGenere.getTypeDocument());
    gff.setFichierSortie(cheminCompletFichierGenere);
    cheminCompletFichierGenere = gff.renduFinal2(dossierFondDePage, pDde.getNameDataFile(), fichierXSL.getAbsolutePath());
    pTypeDocumentGenere.setCheminCompletFichierTemporaire(cheminCompletFichierGenere);
  }
  
  /**
   * Génère un fichier à partir d'un spool.
   */
  private void creerDocumentAvecSpool(TypeDocumentGenere pTypeDocumentGenere, DescriptionEdition pDde, SpoolOld pSpool) {
    String dossierFondDePage = dossierSimSerieNServeur + File.separatorChar + pDde.getNomSpool().toLowerCase();
    String dossierCfg = ServeurNewSim.getDossierRacineSerieNServeur() + File.separatorChar + ConstantesNewSim.DOSSIER_LIB;
    
    String nomSpool = pDde.getNomSpool().toLowerCase();
    pTypeDocumentGenere.setNomFichierSansExtension(nomSpool);
    String cheminCompletFichierGenere = pTypeDocumentGenere.getCheminCompletFichierTemporaire();
    if (cheminCompletFichierGenere == null) {
      cheminCompletFichierGenere = dossierTempUserSerieNServeur + File.separatorChar + pTypeDocumentGenere.getNomFichierSansExtension();
    }
    // Ajout du préfixe si présent
    if (!pDde.getPrefixeNomFichierGenere().isEmpty()) {
      pTypeDocumentGenere
          .setNomFichierSansExtension(pDde.getPrefixeNomFichierGenere() + pTypeDocumentGenere.getNomFichierSansExtension());
    }
    
    // Récupération de la description du document s'il y en a une
    DescriptionDocument doc = null;
    String nomFichierDDD = Constantes.normerTexte(pDde.getFichierDescriptionDocument());
    if (!nomFichierDDD.isEmpty()) {
      File fichierDDD = new File(dossierFondDePage + File.separatorChar + nomFichierDDD);
      if (fichierDDD.exists()) {
        try {
          doc = (DescriptionDocument) XMLTools.decodeFromFile(fichierDDD.getAbsolutePath());
        }
        catch (Exception e) {
          Trace.erreur("Le fichier " + fichierDDD.getAbsolutePath() + " n'a pas pu être chargé");
        }
      }
      else {
        Trace.alerte("Le fichier " + fichierDDD.getAbsolutePath() + " n'existe pas");
      }
    }
    else {
      Trace.erreur("Le fichier DDD n'est pas indiqué");
    }
    
    GenerationFichierFO gffo = null;
    // Dans le cas où il n'y a pas de description de document
    if (doc == null) {
      gffo = new GenerationFichierFO(pSpool, dossierTempUserSerieNServeur, true);
    }
    // Dans le cas où il y a une description de document
    else {
      gffo = new GenerationFichierFO(doc, pSpool.getPages(), dossierTempUserSerieNServeur);
      listeVariables.clear();
      gffo.setListeVariables(listeVariables);
    }
    
    // Génération du fichier
    GenerationFichierFinal gff = new GenerationFichierFinal();
    gff.setTypeSortie(pTypeDocumentGenere.getTypeDocument());
    gff.setFichierSortie(cheminCompletFichierGenere);
    // gffo.genereFOtoFile(); //***> Si on le décommente on a le fichier FO mais le PDF est en erreur
    cheminCompletFichierGenere =
        gff.renduFinal(dossierCfg, dossierFondDePage, gffo.genereFOtoString(), doc == null ? 72 : doc.getSource_dpi(), 72);
    pTypeDocumentGenere.setCheminCompletFichierTemporaire(cheminCompletFichierGenere);
  }
  
  /**
   * Initialisation de la description d'édition à partir de la demande d'édition.
   */
  private DescriptionEdition creerDescriptionEdition(DemandeEdition pDemandeEdition) {
    // Contrôle l'existence des paramètres venant de la table des extensions de la demande
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    if (listeParametreEdition == null || listeParametreEdition.isEmpty()) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.PARAMETRE_EXTENSION_MANQUANT);
      throw new MessageErreurException(EnumCodeErreur.PARAMETRE_EXTENSION_MANQUANT.getLibelle());
    }
    // Contrôle le type de la source
    if (pDemandeEdition.getTypeSource() == null) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.TYPE_SOURCE_MANQUANT);
      throw new MessageErreurException(EnumCodeErreur.TYPE_SOURCE_MANQUANT.getLibelle());
    }
    
    DescriptionEdition pDde = new DescriptionEdition();
    pDde.setUtilisation(DescriptionEdition.DIRECTE);
    pDde.setProfil(pDemandeEdition.getProfil());
    // Le nom du spool correspond au nom de la description du document
    pDde.setNomSpool(pDemandeEdition.getNomDescriptionDeDocument());
    
    // Est-ce une édition à partir d'un spool ou de la base de données ?
    switch (pDemandeEdition.getTypeSource()) {
      case SPOOL:
        pDde.setSansSpool(false);
        break;
      case BDD:
        pDde.setSansSpool(true);
        pDde.setNameDataFile(dossierTempUserSerieNServeur + File.separatorChar + pDde.getNomSpool() + ".xml");
        break;
    }
    
    // Est-ce une impression ?
    boolean impression = false;
    if (pDemandeEdition.getImpression() != null && pDemandeEdition.getImpression().booleanValue()) {
      impression = true;
    }
    pDde.setImpression(impression);
    
    // Récupération du nom du job pour le spool
    ParametreEdition parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_JOB);
    if (parametreEdition != null) {
      pDde.setNomJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_JOB);
    if (parametreEdition != null) {
      pDde.setNumeroJob(parametreEdition.getStringValeur());
    }
    // Récupération de l'utilisateur du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_USER_JOB);
    if (parametreEdition != null) {
      pDde.setNomUserJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_SPOOL);
    if (parametreEdition != null) {
      pDde.setNumeroSpool(parametreEdition.getIntegerValeur());
    }
    
    // Nom de l'outq d'origine (spool)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_OUTQ_SOURCE);
    if (parametreEdition != null) {
      pDde.setNomOutQueue(parametreEdition.getStringValeur());
    }
    
    // Que fait on du spool ? Supprimé ou suspendu
    pDde.setActionSpool(EnumActionSurSpool.SUPPRIMER);
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTION_SUR_SPOOL);
    if (parametreEdition != null) {
      EnumActionSurSpool actionSpool = EnumActionSurSpool.valueOfByNumero(parametreEdition.getIntegerValeur());
      if (actionSpool != null) {
        pDde.setActionSpool(actionSpool);
      }
    }
    
    // Chemin complet du dossier contenant les pièces jointes
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.DOSSIER_PIECE_JOINTE);
    if (parametreEdition != null) {
      String dossier = parametreEdition.getStringValeur().replaceAll("//", "/");
      pDde.setPathPiecesJointes(dossier);
    }
    
    // Préfixe possible pour le nom du fichier généré si nécessaire
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.PREFIXE_NOM_FICHIER);
    if (parametreEdition != null) {
      pDde.setPrefixeNomFichierGenere(parametreEdition.getStringValeur());
    }
    
    // Bibliothèque temporaire de travail (puisque QTEMP est inacessible via SQL) - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.BIBLIOTHEQUE_TEMPORAIRE);
    if (parametreEdition != null) {
      pDde.setLibTemp(parametreEdition.getStringValeur());
    }
    
    // Nom du fichier contenant les entêtes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_ENTETE);
    if (parametreEdition != null) {
      pDde.setFicHeaders(parametreEdition.getStringValeur());
    }
    
    // Nom du fichier contenant les lignes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_LIGNE);
    if (parametreEdition != null) {
      pDde.setFicLines(parametreEdition.getStringValeur());
    }
    
    // Lettre d'exécution - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.LETTRE_ENVIRONNEMENT);
    if (parametreEdition != null) {
      pDde.setLettre(parametreEdition.getCharacterValeur());
    }
    if (pDde.isSansSpool()) {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION_SANS_SPOOL);
    }
    else {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION);
    }
    
    // Initialisation des canaux de diffusion
    // Le canal de diffusion impression
    if (impression) {
      CanalDiffusionImpression canalDiffusionImpression = retournerCanalDiffusionImpression(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionImpression);
    }
    
    // Le canal de diffusion fichier ifs lors de l'impression
    String dossierStockage = Constantes.normerTexte(pDemandeEdition.getCheminFichierAImprimer());
    if (!dossierStockage.isEmpty()) {
      CanalDiffusionFichierIfs canalDiffusionFichierIfs = retournerCanalDiffusionFichierIfs(pDemandeEdition, dossierStockage);
      if (canalDiffusionFichierIfs != null) {
        pDde.ajouterCanalDiffusion(canalDiffusionFichierIfs);
      }
    }
    
    // Le canal de diffusion fichier ifs hors impression
    dossierStockage = Constantes.normerTexte(pDemandeEdition.getCheminFichierAStocker());
    if (!dossierStockage.isEmpty()) {
      CanalDiffusionFichierIfs canalDiffusionFichierIfs = retournerCanalDiffusionFichierIfs(pDemandeEdition, dossierStockage);
      if (canalDiffusionFichierIfs != null) {
        pDde.ajouterCanalDiffusion(canalDiffusionFichierIfs);
      }
    }
    
    // Le canal de diffusion document stocké
    if (Constantes.equals(pDemandeEdition.getStockageDuDocument(), Boolean.TRUE)) {
      CanalDiffusionDocumentStocke canalDiffusionDocumentStocke = retournerCanalDiffusionDocumentStocke(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionDocumentStocke);
    }
    
    // Le canal de diffusion mail
    if (pDemandeEdition.getIdMail() != null) {
      CanalDiffusionMail canalDiffusionMail = retournerCanalDiffusionMail(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionMail);
    }
    
    // Le canal de diffusion fax
    if (pDemandeEdition.getIdMailPourFax() != null) {
      CanalDiffusionFax canalDiffusionFax = retournerCanalDiffusionFax(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionFax);
    }
    
    // Le canal de diffusion fusion de fichier
    String cheminFichierAFusionner = Constantes.normerTexte(pDemandeEdition.getCheminFichierDestinationFusion());
    if (!cheminFichierAFusionner.isEmpty()) {
      CanalDiffusionFusionFichier canalDiffusionFusionFichier = retournerCanalDiffusionFusionFichier(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionFusionFichier);
    }
    
    return pDde;
  }
  
  /**
   * Initialisation de la description d'édition pour la génération du fichier à partir de la demande d'édition.
   */
  /*
  private DescriptionEdition initialiserDescriptionEdition(DescriptionEdition pDde, DemandeEdition pDemandeEdition, boolean pAvecBdd) {
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    if (listeParametreEdition == null || listeParametreEdition.isEmpty()) {
      throw new MessageErreurException("Il n'y a pas de paramètres pour la demande d'édition " + pDemandeEdition.getId() + '.');
    }
    
    pDde.setSansSpool(pAvecBdd);
    pDde.setUtilisation(DescriptionEdition.DIRECTE);
    pDde.setProfil(pDemandeEdition.getProfil());
    
    // Récupération du nom du job pour le spool
    ParametreEdition parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_JOB);
    if (parametreEdition != null) {
      pDde.setNomJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_JOB);
    if (parametreEdition != null) {
      pDde.setNumeroJob(parametreEdition.getStringValeur());
    }
    // Récupération de l'utilisateur du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_USER_JOB);
    if (parametreEdition != null) {
      pDde.setNomUserJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_SPOOL);
    if (parametreEdition != null) {
      pDde.setNumeroSpool(parametreEdition.getIntegerValeur());
    }
    // Le nom du spool correspond au nom de la description du document
    pDde.setNomSpool(pDemandeEdition.getNomDescriptionDeDocument());
    
    // Nom de l'outq d'origine (spool)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_OUTQ_SOURCE);
    if (parametreEdition != null) {
      pDde.setNomOutQueue(parametreEdition.getStringValeur());
    }
    
    // Dossier où on va être généré le document
    CanalDiffusionFichierIfs canalDiffusionFichierIfs = new CanalDiffusionFichierIfs();
    pDde.ajouterCanalDiffusion(canalDiffusionFichierIfs);
    
    // Le type du document à générer (type de document généré: ps, pdf, ...)
    canalDiffusionFichierIfs.setTypeDocument(pDemandeEdition.getTypeDocument());
    canalDiffusionFichierIfs.setNomDossier(pDemandeEdition.getCheminFichierAStocker());
    // Nom du fichier (sans extension)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_FICHIER_SANS_EXTENSION);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setNomFichier(parametreEdition.getStringValeur());
    }
    // Si le nom du fichier n'est pas précisé alors l'identifiant du document est utilisé
    else {
      parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.IDENTIFIANT_DOCUMENT_EDITE);
      if (parametreEdition != null) {
        canalDiffusionFichierIfs.setNomFichier(parametreEdition.getStringValeur());
      }
      // A défaut le nom du fichier est initialisé avec le nom du spool (description)
      else {
        canalDiffusionFichierIfs.setNomFichier(pDemandeEdition.getNomDescriptionDeDocument());
      }
    }
    
    // Que fait on du spool ? Supprimé ou suspendu
    pDde.setActionSpool(EnumActionSurSpool.SUPPRIMER);
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTION_SUR_SPOOL);
    if (parametreEdition != null) {
      EnumActionSurSpool actionSpool = EnumActionSurSpool.valueOfByNumero(parametreEdition.getIntegerValeur());
      if (actionSpool != null) {
        pDde.setActionSpool(actionSpool);
      }
    }
    // Si non vide alors on lance une application associé au document généré
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.APPLICATION_ASSOCIEE);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setCommande(Constantes.normerTexte(parametreEdition.getStringValeur()));
    }
    // Activation de la sauvegarde du fichier PDF s'il existe déjà
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTIVATION_HISTORIQUE_FICHIER);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setSauvegardeAncien(parametreEdition.getBooleanValeur());
    }
    // Chemin complet du dossier contenant les pièces jointes
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.DOSSIER_PIECE_JOINTE);
    if (parametreEdition != null) {
      String dossier = parametreEdition.getStringValeur().replaceAll("//", "/");
      pDde.setPathPiecesJointes(dossier);
    }
    // Préfixe possible pour le nom du fichier généré si nécessaire
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.PREFIXE_NOM_FICHIER);
    if (parametreEdition != null) {
      pDde.setPrefixeNomFichierGenere(parametreEdition.getStringValeur());
    }
    // Bibliothèque temporaire de travail (puisque QTEMP est inacessible via SQL) - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.BIBLIOTHEQUE_TEMPORAIRE);
    if (parametreEdition != null) {
      pDde.setLibTemp(parametreEdition.getStringValeur());
    }
    // Nom du fichier contenant les entêtes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_ENTETE);
    if (parametreEdition != null) {
      pDde.setFicHeaders(parametreEdition.getStringValeur());
    }
    // Nom du fichier contenant les lignes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_LIGNE);
    if (parametreEdition != null) {
      pDde.setFicLines(parametreEdition.getStringValeur());
    }
    // Lettre d'exécution - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.LETTRE_ENVIRONNEMENT);
    if (parametreEdition != null) {
      pDde.setLettre(parametreEdition.getCharacterValeur());
    }
    if (pDde.isSansSpool()) {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION_SANS_SPOOL);
    }
    else {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION);
    }
    
    // Le canal de diffusion document stocké
    if (Constantes.equals(pDemandeEdition.getStockageDuDocument(), Boolean.TRUE)) {
      CanalDiffusionDocumentStocke canalDiffusionDocumentStocke = retournerCanalDiffusionDocumentStocke(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionDocumentStocke);
    }
    
    // Le canal de diffusion mail
    if (pDemandeEdition.getIdMail() != null) {
      CanalDiffusionMail canalDiffusionMail = retournerCanalDiffusionMail(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionMail);
    }
    
    // Le canal de diffusion fax
    if (pDemandeEdition.getIdMailPourFax() != null) {
      CanalDiffusionFax canalDiffusionFax = retournerCanalDiffusionFax(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionFax);
    }
    
    // Le canal de diffusion fusion de fichier
    String cheminFichierAFusionner = Constantes.normerTexte(pDemandeEdition.getCheminFichierDestinationFusion());
    if (!cheminFichierAFusionner.isEmpty()) {
      CanalDiffusionFusionFichier canalDiffusionFusionFichier = retournerCanalDiffusionFusionFichier(pDemandeEdition);
      pDde.ajouterCanalDiffusion(canalDiffusionFusionFichier);
    }
    
    return pDde;
  }*/
  
  /**
   * Initialisation de la description d'édition pour l'impression à partir de la demande d'édition.
   * Concerne les éditions à partir d'un spool ou non.
   */
  /*
  private DescriptionEdition initialiserDescriptionEditionPourImpression(DescriptionEdition pDde, DemandeEdition pDemandeEdition,
      boolean pAvecBdd) {
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    if (listeParametreEdition == null || listeParametreEdition.isEmpty()) {
      throw new MessageErreurException("Il n'y a pas de paramètres pour la demande d'édition " + pDemandeEdition.getId() + '.');
    }
    
    pDde.setSansSpool(pAvecBdd);
    pDde.setUtilisation(DescriptionEdition.DIRECTE);
    // Récupération du nom du job pour le spool
    ParametreEdition parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_JOB);
    if (parametreEdition != null) {
      pDde.setNomJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_JOB);
    if (parametreEdition != null) {
      pDde.setNumeroJob(parametreEdition.getStringValeur());
    }
    // Récupération de l'utilisateur du job pour le spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_USER_JOB);
    if (parametreEdition != null) {
      pDde.setNomUserJob(parametreEdition.getStringValeur());
    }
    // Récupération du numéro du spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_SPOOL);
    if (parametreEdition != null) {
      pDde.setNumeroSpool(parametreEdition.getIntegerValeur());
    }
    // Le nom du spool correspond au nom de la description du document
    pDde.setNomSpool(pDemandeEdition.getNomDescriptionDeDocument());
    
    CanalDiffusionImpression canalDiffusionImpression = new CanalDiffusionImpression();
    canalDiffusionImpression.setNomSpool(pDde.getNomSpool());
    
    // Le type du document à générer (type de document généré: ps, pdf, ...)
    canalDiffusionImpression.setTypeDocument(pDemandeEdition.getTypeDocument());
    
    // Langage de l'imprimante (ps, pjl, ...)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.LANGAGE_IMPRIMANTE);
    if (parametreEdition != null) {
      EnumLangageImprimante langageImprimante = EnumLangageImprimante.valueOfByCode(parametreEdition.getStringValeur());
      if (langageImprimante != null) {
        canalDiffusionImpression.setLangageImprimante(langageImprimante);
      }
    }
    // Nom de l'outq d'origine (spool)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_OUTQ_SOURCE);
    if (parametreEdition != null) {
      pDde.setNomOutQueue(parametreEdition.getStringValeur());
    }
    // Nom de l'outq de l'imprimante
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_OUTQ_IMPRIMANTE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNomOutq(parametreEdition.getStringValeur());
    }
    // Numéro du tiroir d'entrée
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_TIROIR_ENTREE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setTiroirEntree(parametreEdition.getIntegerValeur());
    }
    // Recto-verso 0:non 1:rectoverso longedge 2:rectoverso shortedge
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.MODE_RECTOVERSO);
    if (parametreEdition != null) {
      EnumTypeRectoVerso typeRectoVerso = EnumTypeRectoVerso.valueOfByNumero(parametreEdition.getIntegerValeur());
      if (typeRectoVerso == null) {
        canalDiffusionImpression.setRectoVerso(EnumTypeRectoVerso.AUCUN);
      }
      else {
        canalDiffusionImpression.setRectoVerso(typeRectoVerso);
      }
    }
    // Nombre de copie
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOMBRE_COPIE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNbrExemplaire(parametreEdition.getIntegerValeur());
    }
    // Que fait on du spool ? Supprimé ou suspendu
    pDde.setActionSpool(EnumActionSurSpool.SUPPRIMER);
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTION_SUR_SPOOL);
    if (parametreEdition != null) {
      EnumActionSurSpool actionSpool = EnumActionSurSpool.valueOfByNumero(parametreEdition.getIntegerValeur());
      if (actionSpool != null) {
        pDde.setActionSpool(actionSpool);
      }
    }
    // Mode brouillon false:non true:brouillon
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTIVATION_MODE_BROUILLON);
    if (parametreEdition != null) {
      canalDiffusionImpression.setEconomie(parametreEdition.getBooleanValeur());
    }
    // Densité de l'impression (qualité)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.QUALITE_IMPRESSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setDensite(parametreEdition.getIntegerValeur());
    }
    // Forcer le noir et blanc false:non true:force noir&blanc
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.EDITION_NOIR_ET_BLANC);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNoirblanc(parametreEdition.getBooleanValeur());
    }
    // On rend la main immédiatement
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.EDITION_NOIR_ET_BLANC);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNoirblanc(parametreEdition.getBooleanValeur());
    }
    // On rend la main immédiatement
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.SOUMISSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setSoumission(parametreEdition.getBooleanValeur());
    }
    // Numéro du tiroir de sortie
    // Numéro du tiroir d'entrée
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_TIROIR_SORTIE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setTiroirSortie(parametreEdition.getIntegerValeur());
    }
    // Chemin complet du dossier contenant les pièces jointes
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.DOSSIER_PIECE_JOINTE);
    if (parametreEdition != null) {
      String dossier = parametreEdition.getStringValeur().replaceAll("//", "/");
      pDde.setPathPiecesJointes(dossier);
    }
    
    // Nom du fichier (sans extension)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_FICHIER_SANS_EXTENSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNomFichier(parametreEdition.getStringValeur());
    }
    // Si le nom du fichier n'est pas précisé alors l'identifiant du document est utilisé
    else {
      parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.IDENTIFIANT_DOCUMENT_EDITE);
      if (parametreEdition != null) {
        canalDiffusionImpression.setNomFichier(parametreEdition.getStringValeur());
      }
      // A défaut le nom du fichier est initialisé avec le nom du spool (description)
      else {
        canalDiffusionImpression.setNomFichier(pDemandeEdition.getNomDescriptionDeDocument());
      }
    }
    
    // Dossier où on va être généré le document
    canalDiffusionImpression.setNomDossier(pDemandeEdition.getCheminFichierAImprimer());
    // Bibliothèque temporaire de travail (puisque QTEMP est inacessible via SQL) - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.BIBLIOTHEQUE_TEMPORAIRE);
    if (parametreEdition != null) {
      pDde.setLibTemp(parametreEdition.getStringValeur());
    }
    // Nom du fichier contenant les entêtes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_ENTETE);
    if (parametreEdition != null) {
      pDde.setFicHeaders(parametreEdition.getStringValeur());
    }
    // Nom du fichier contenant les lignes - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.TABLE_LIGNE);
    if (parametreEdition != null) {
      pDde.setFicLines(parametreEdition.getStringValeur());
    }
    // Lettre d'exécution - Pour les éditions sans spool
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.LETTRE_ENVIRONNEMENT);
    if (parametreEdition != null) {
      pDde.setLettre(parametreEdition.getCharacterValeur());
    }
    
    if (pDde.isSansSpool()) {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + ".xsl");
    }
    else {
      pDde.setFichierDescriptionDocument(pDde.getNomSpool().toLowerCase() + DescriptionDocument.EXTENSION);
    }
    canalDiffusionImpression.genereScript();
    pDde.ajouterCanalDiffusion(canalDiffusionImpression);
    
    return pDde;
  }*/
  
  /**
   * Ajouter le canal de diffusion impression.
   */
  private CanalDiffusionImpression retournerCanalDiffusionImpression(DemandeEdition pDemandeEdition) {
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    
    CanalDiffusionImpression canalDiffusionImpression = new CanalDiffusionImpression();
    canalDiffusionImpression.setNomSpool(pDemandeEdition.getNomDescriptionDeDocument());
    
    // Le type du document à générer (type de document généré: ps, pdf, ...)
    canalDiffusionImpression.setTypeDocument(pDemandeEdition.getTypeDocument());
    
    // Langage de l'imprimante (ps, pjl, ...)
    ParametreEdition parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.LANGAGE_IMPRIMANTE);
    if (parametreEdition != null) {
      EnumLangageImprimante langageImprimante = EnumLangageImprimante.valueOfByCode(parametreEdition.getStringValeur());
      if (langageImprimante != null) {
        canalDiffusionImpression.setLangageImprimante(langageImprimante);
      }
    }
    // Nom de l'outq de l'imprimante
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_OUTQ_IMPRIMANTE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNomOutq(parametreEdition.getStringValeur());
    }
    // Numéro du tiroir d'entrée
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_TIROIR_ENTREE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setTiroirEntree(parametreEdition.getIntegerValeur());
    }
    // Recto-verso 0:non 1:rectoverso longedge 2:rectoverso shortedge
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.MODE_RECTOVERSO);
    if (parametreEdition != null) {
      EnumTypeRectoVerso typeRectoVerso = EnumTypeRectoVerso.valueOfByNumero(parametreEdition.getIntegerValeur());
      if (typeRectoVerso == null) {
        canalDiffusionImpression.setRectoVerso(EnumTypeRectoVerso.AUCUN);
      }
      else {
        canalDiffusionImpression.setRectoVerso(typeRectoVerso);
      }
    }
    // Nombre de copie
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOMBRE_COPIE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNbrExemplaire(parametreEdition.getIntegerValeur());
    }
    // Mode brouillon false:non true:brouillon
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTIVATION_MODE_BROUILLON);
    if (parametreEdition != null) {
      canalDiffusionImpression.setEconomie(parametreEdition.getBooleanValeur());
    }
    // Densité de l'impression (qualité)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.QUALITE_IMPRESSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setDensite(parametreEdition.getIntegerValeur());
    }
    // Forcer le noir et blanc false:non true:force noir&blanc
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.EDITION_NOIR_ET_BLANC);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNoirblanc(parametreEdition.getBooleanValeur());
    }
    // On rend la main immédiatement
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.EDITION_NOIR_ET_BLANC);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNoirblanc(parametreEdition.getBooleanValeur());
    }
    // On rend la main immédiatement
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.SOUMISSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setSoumission(parametreEdition.getBooleanValeur());
    }
    // Numéro du tiroir de sortie
    // Numéro du tiroir d'entrée
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NUMERO_TIROIR_SORTIE);
    if (parametreEdition != null) {
      canalDiffusionImpression.setTiroirSortie(parametreEdition.getIntegerValeur());
    }
    
    // Nom du fichier (sans extension)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_FICHIER_SANS_EXTENSION);
    if (parametreEdition != null) {
      canalDiffusionImpression.setNomFichier(parametreEdition.getStringValeur());
    }
    // Si le nom du fichier n'est pas précisé alors l'identifiant du document est utilisé
    else {
      parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.IDENTIFIANT_DOCUMENT_EDITE);
      if (parametreEdition != null) {
        canalDiffusionImpression.setNomFichier(parametreEdition.getStringValeur());
      }
      // A défaut le nom du fichier est initialisé avec le nom du spool (description)
      else {
        canalDiffusionImpression.setNomFichier(pDemandeEdition.getNomDescriptionDeDocument());
      }
    }
    
    // Dossier où le document va être potentiellement être copié
    canalDiffusionImpression.setNomDossier(pDemandeEdition.getCheminFichierAImprimer());
    
    canalDiffusionImpression.genereScript();
    
    return canalDiffusionImpression;
  }
  
  /**
   * Ajouter le canal de diffusion fichier ifs.
   */
  private CanalDiffusionFichierIfs retournerCanalDiffusionFichierIfs(DemandeEdition pDemandeEdition, String pDossierStockage) {
    if (pDossierStockage == null || pDossierStockage.isEmpty()) {
      return null;
    }
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    
    // Dossier où on va être généré le document
    CanalDiffusionFichierIfs canalDiffusionFichierIfs = new CanalDiffusionFichierIfs();
    
    // Le type du document à générer (pour l'instant il s'agit obligatoirement d'un fichier Pdf)
    canalDiffusionFichierIfs.setTypeDocument(EnumTypeDocument.PDF);
    canalDiffusionFichierIfs.setNomDossier(pDossierStockage);
    
    // Nom du fichier (sans extension)
    ParametreEdition parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_FICHIER_SANS_EXTENSION);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setNomFichierSansExtension(parametreEdition.getStringValeur());
    }
    
    // Si non vide alors on lance une application associé au document généré
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.APPLICATION_ASSOCIEE);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setCommande(Constantes.normerTexte(parametreEdition.getStringValeur()));
    }
    // Activation de la sauvegarde du fichier PDF s'il existe déjà
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.ACTIVATION_HISTORIQUE_FICHIER);
    if (parametreEdition != null) {
      canalDiffusionFichierIfs.setSauvegardeAncien(parametreEdition.getBooleanValeur());
    }
    
    return canalDiffusionFichierIfs;
  }
  
  /**
   * Ajouter le canal de diffusion document stocké.
   */
  private CanalDiffusionDocumentStocke retournerCanalDiffusionDocumentStocke(DemandeEdition pDemandeEdition) {
    ListeParametreEdition listeParametreEdition = pDemandeEdition.getListeParametre();
    
    CanalDiffusionDocumentStocke canalDiffusionDocumentStocke = new CanalDiffusionDocumentStocke();
    
    // Le type du document à générer (pour l'instant il s'agit obligatoirement d'un fichier Pdf)
    canalDiffusionDocumentStocke.setTypeDocument(EnumTypeDocument.PDF);
    
    // Récupération du type fonctionnel du document édité
    ParametreEdition parametreEdition =
        pDemandeEdition.getListeParametre().get(EnumTypeParametreEdition.TYPE_FONCTIONNEL_DOCUMENT_STOCKE);
    if (parametreEdition == null) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.PARAMETRE_TYPE_FONCTIONNEL_MANQUANT);
      throw new MessageErreurException(EnumCodeErreur.PARAMETRE_TYPE_FONCTIONNEL_MANQUANT.getLibelle());
    }
    canalDiffusionDocumentStocke.setTypeDocumentStocke(EnumTypeDocumentStocke.valueOfByCode(parametreEdition.getIntegerValeur()));
    
    // Récupération de la description liée au document édité
    parametreEdition = pDemandeEdition.getListeParametre().get(EnumTypeParametreEdition.DESCRIPTION_BASE_DOCUMENTAIRE);
    if (parametreEdition != null) {
      canalDiffusionDocumentStocke.setDescription(parametreEdition.getStringValeur());
    }
    else {
      canalDiffusionDocumentStocke.setDescription("Stockage du document de type " + EnumTypeDocumentStocke.FACTURE_VENTES.getLibelle()
          + " le " + DateHeure.getJourHeure(DateHeure.JJ_MM_AAAA_A_HH_MM_SS));
    }
    
    // Nom du fichier (sans extension)
    parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.NOM_FICHIER_SANS_EXTENSION);
    if (parametreEdition != null) {
      canalDiffusionDocumentStocke.setNomFichierSansExtension(parametreEdition.getStringValeur());
    }
    // Si le nom du fichier n'est pas précisé alors l'identifiant du document est utilisé
    else {
      parametreEdition = listeParametreEdition.get(EnumTypeParametreEdition.IDENTIFIANT_DOCUMENT_EDITE);
      if (parametreEdition != null) {
        canalDiffusionDocumentStocke.setNomFichierSansExtension(parametreEdition.getStringValeur());
      }
      // A défaut le nom du fichier est initialisé avec le nom du spool (description)
      else {
        canalDiffusionDocumentStocke.setNomFichierSansExtension(pDemandeEdition.getNomDescriptionDeDocument());
      }
    }
    
    // Récupération des identifiants pour alimenter les liens avec les documents stockés
    switch (canalDiffusionDocumentStocke.getTypeDocumentStocke()) {
      case NON_DEFINI:
        break;
      case COMMANDE_ACHATS:
        break;
      case FACTURE_VENTES:
      case DEVIS_VENTES:
      case BON_VENTES:
      case COMMANDE_VENTES:
        // Récupération de l'identifiant du document de vente
        parametreEdition = pDemandeEdition.getListeParametre().get(EnumTypeParametreEdition.IDENTIFIANT_DOCUMENT_EDITE);
        if (parametreEdition == null) {
          pDemandeEdition.setCodeErreur(EnumCodeErreur.PARAMETRE_IDENTIFIANT_DOCUMENT_MANQUANT);
          throw new MessageErreurException(EnumCodeErreur.PARAMETRE_IDENTIFIANT_DOCUMENT_MANQUANT.getLibelle());
        }
        if (canalDiffusionDocumentStocke.getTypeDocumentStocke().equals(EnumTypeDocumentStocke.FACTURE_VENTES)) {
          canalDiffusionDocumentStocke
              .setIdDocumentVente(IdDocumentVente.getInstancePourFactureParIndicatif(parametreEdition.getStringValeur()));
        }
        else {
          canalDiffusionDocumentStocke.setIdDocumentVente(IdDocumentVente.getInstanceParIndicatif(parametreEdition.getStringValeur()));
        }
        // Récupération de l'identifiant du client
        parametreEdition = pDemandeEdition.getListeParametre().get(EnumTypeParametreEdition.IDENTIFIANT_CLIENT);
        if (parametreEdition == null) {
          pDemandeEdition.setCodeErreur(EnumCodeErreur.PARAMETRE_IDENTIFIANT_CLIENT_MANQUANT);
          Trace.alerte(EnumCodeErreur.PARAMETRE_IDENTIFIANT_CLIENT_MANQUANT.getLibelle());
          // throw new MessageErreurException(EnumCodeErreur.PARAMETRE_IDENTIFIANT_CLIENT_MANQUANT.getLibelle());
        }
        else {
          canalDiffusionDocumentStocke.setIdClient(IdClient.getInstanceParIndicatif(parametreEdition.getStringValeur()));
        }
    }
    
    return canalDiffusionDocumentStocke;
  }
  
  /**
   * Ajouter le canal de diffusion mail.
   */
  private CanalDiffusionMail retournerCanalDiffusionMail(DemandeEdition pDemandeEdition) {
    CanalDiffusionMail canalDiffusionMail = new CanalDiffusionMail();
    
    // Le type du document à générer (pour l'instant il s'agit obligatoirement d'un fichier Pdf)
    canalDiffusionMail.setTypeDocument(EnumTypeDocument.PDF);
    
    // L'identifiant du mail
    canalDiffusionMail.setIdMail(pDemandeEdition.getIdMail());
    
    return canalDiffusionMail;
  }
  
  /**
   * Ajouter le canal de diffusion fax.
   */
  private CanalDiffusionFax retournerCanalDiffusionFax(DemandeEdition pDemandeEdition) {
    CanalDiffusionFax canalDiffusionFax = new CanalDiffusionFax();
    
    // Le type du document à générer (pour l'instant il s'agit obligatoirement d'un fichier Pdf)
    canalDiffusionFax.setTypeDocument(EnumTypeDocument.PDF);
    
    // L'identifiant du mail
    canalDiffusionFax.setIdMailPourFax(pDemandeEdition.getIdMailPourFax());
    
    return canalDiffusionFax;
  }
  
  /**
   * Ajouter le canal de diffusion fusion de fichiers.
   */
  private CanalDiffusionFusionFichier retournerCanalDiffusionFusionFichier(DemandeEdition pDemandeEdition) {
    CanalDiffusionFusionFichier canalDiffusionFusionFichier = new CanalDiffusionFusionFichier();
    
    // Le type du document à générer (pour l'instant il s'agit obligatoirement d'un fichier Pdf)
    canalDiffusionFusionFichier.setTypeDocument(EnumTypeDocument.PDF);
    
    // Le chemin complet vers le fichier de destination pour la fusion
    String fichier = Constantes.normerTexte(pDemandeEdition.getCheminFichierDestinationFusion());
    canalDiffusionFusionFichier.setCheminCompletFichierDestinationFusion(fichier);
    
    return canalDiffusionFusionFichier;
  }
  
  /**
   * Controle la validité d'un fichier temporaire généré.
   */
  private FileNG controlerFichierTemporaire(TypeDocumentGenere pTypeDocumentGenere, DemandeEdition pDemandeEdition) {
    if (pTypeDocumentGenere == null) {
      throw new MessageErreurException("Le type de document généré est invalide");
    }
    
    String cheminCompletFichierTemporaire = Constantes.normerTexte(pTypeDocumentGenere.getCheminCompletFichierTemporaire());
    if (cheminCompletFichierTemporaire.isEmpty()) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.CHEMIN_FICHIER_GENERE_INVALIDE);
      throw new MessageErreurException(EnumCodeErreur.CHEMIN_FICHIER_GENERE_INVALIDE.getLibelle());
    }
    
    FileNG fichierGenereTemporaire = new FileNG(cheminCompletFichierTemporaire);
    if (!fichierGenereTemporaire.exists()) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.FICHIER_GENERE_INTROUVABLE);
      throw new MessageErreurException(
          EnumCodeErreur.FICHIER_GENERE_INTROUVABLE.getLibelle() + '(' + cheminCompletFichierTemporaire + ").");
    }
    
    return fichierGenereTemporaire;
  }
  
  /**
   * Génération des fichiers en fonction des types de document nécessaires.
   */
  private boolean genererFichier(DescriptionEdition pDde, DemandeEdition pDemandeEdition) {
    boolean reussite = true;
    
    // Parcourt de la liste des types de document
    HashMap<EnumTypeDocument, TypeDocumentGenere> mapTypeDocument = pDde.getMapTypeDocument();
    if (mapTypeDocument == null || mapTypeDocument.isEmpty()) {
      return false;
    }
    
    // Génération des documents à partir de la base de données
    if (pDde.isSansSpool()) {
      // Extraction des données des fichiers DB2 vers le fichier XML
      Trace.info("Avant extractData2XML ");
      long t1 = System.currentTimeMillis();
      String nomFichierAGenererSansExtension = exporterBaseDonneesVersXML(pDde.getNameDataFile(), pDde, pDemandeEdition);
      Trace.info("Après extractData2XML " + (System.currentTimeMillis() - t1) + " ms");
      nomFichierAGenererSansExtension = Constantes.normerTexte(nomFichierAGenererSansExtension);
      if (nomFichierAGenererSansExtension.isEmpty()) {
        Trace.erreur("Il y a eu un problème lors de l'extraction des données car le nom du document est invalide.");
        return false;
      }
      
      // Générer les documents
      for (Entry<EnumTypeDocument, TypeDocumentGenere> entry : mapTypeDocument.entrySet()) {
        TypeDocumentGenere typeDocumentGenere = entry.getValue();
        if (typeDocumentGenere == null) {
          continue;
        }
        typeDocumentGenere.setNomFichierSansExtension(nomFichierAGenererSansExtension);
        creerDocumentAvecBdd(typeDocumentGenere, pDde);
      }
    }
    // Générer le document à partir d'un spool
    else {
      EnumActionSurSpool actionSpool = pDde.getActionSpool();
      if (actionSpool == null) {
        actionSpool = EnumActionSurSpool.SUSPRENDRE;
      }
      String nomSpool = pDde.getNomSpool().toLowerCase();
      SpooledFile spoolFile;
      SpoolOld spoolOld;
      try {
        spoolFile = new SpooledFile(gestionSpool.getSystem(), nomSpool.toUpperCase(), pDde.getNumeroSpool(), pDde.getNomJob(),
            pDde.getNomUserJob(), pDde.getNumeroJob());
        spoolOld = gestionSpool.getSpool(spoolFile);
        if (spoolOld == null) {
          Trace.erreur("Problème lors de la récupération du spool : nom=" + nomSpool + " numéro=" + pDde.getNumeroSpool() + " travail="
              + pDde.getNomJob() + '/' + pDde.getNomUserJob() + '/' + pDde.getNumeroJob() + " message=" + gestionSpool.getMsgErreur());
          return false;
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Récupération du spool:");
        Trace.erreur("gestionSpool=" + gestionSpool);
        Trace.erreur("gestionSpool.getSystem()=" + gestionSpool.getSystem());
        Trace.erreur("Problème lors de la récupération du spool : nom=" + nomSpool + " numéro=" + pDde.getNumeroSpool() + " travail="
            + pDde.getNomJob() + '/' + pDde.getNomUserJob() + '/' + pDde.getNumeroJob() + " message=" + gestionSpool.getMsgErreur());
        return false;
      }
      
      // Générer les documents
      for (Entry<EnumTypeDocument, TypeDocumentGenere> entry : mapTypeDocument.entrySet()) {
        TypeDocumentGenere typeDocumentGenere = entry.getValue();
        if (typeDocumentGenere == null) {
          continue;
        }
        // Générer le document
        creerDocumentAvecSpool(typeDocumentGenere, pDde, spoolOld);
      }
      
      // Suppression du spool si nécessaire
      try {
        if (Constantes.equals(actionSpool, EnumActionSurSpool.SUPPRIMER)) {
          spoolFile.delete();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de la suppression du spool");
      }
    }
    
    // Contrôle de l'existence des fichiers générés
    for (Entry<EnumTypeDocument, TypeDocumentGenere> entry : mapTypeDocument.entrySet()) {
      TypeDocumentGenere typeDocumentGenere = entry.getValue();
      if (typeDocumentGenere == null) {
        continue;
      }
      String nomCompletFichierTemporaire = Constantes.normerTexte(typeDocumentGenere.getCheminCompletFichierTemporaire());
      // Contrôle du nom chemin
      if (nomCompletFichierTemporaire.isEmpty()) {
        Trace.erreur("Aucun fichier n'a été généré pour le type de sortie " + typeDocumentGenere.getTypeDocument() + '.');
        continue;
      }
      // Contrôle de l'existence réelle du fichier
      File fichier = new File(nomCompletFichierTemporaire);
      if (!fichier.exists()) {
        continue;
      }
      // Le fichier est considéré comme vérifié à ce stade
      typeDocumentGenere.setVerifie(true);
    }
    
    return reussite;
  }
  
  /**
   * Traitement des canaux de diffusion.
   */
  private void traiterCanalDiffusion(DescriptionEdition pDde, DemandeEdition pDemandeEdition) {
    // Contrôle de la liste des canaux de diffusion
    List<CanalDiffusion> listeCanalDiffusion = pDde.getCanalDiffusion();
    if (listeCanalDiffusion == null || listeCanalDiffusion.isEmpty()) {
      Trace.erreur("La liste des canaux de diffusion est invalide");
      return;
    }
    HashMap<EnumTypeDocument, TypeDocumentGenere> mapTypeDocument = pDde.getMapTypeDocument();
    if (mapTypeDocument == null || mapTypeDocument.isEmpty()) {
      Trace.erreur("La liste des fichiers générés est invalide");
      return;
    }
    
    // Parcourt de la liste des canaux de diffusion de la descriptioon en cours
    for (CanalDiffusion canalDiffusion : listeCanalDiffusion) {
      // Récupération des informations du type de document
      TypeDocumentGenere typeDocumentGenere = mapTypeDocument.get(canalDiffusion.getTypeDocument());
      if (typeDocumentGenere == null || !typeDocumentGenere.isVerifie()) {
        Trace.erreur("Le fichier généré pour le canal de diffusion " + canalDiffusion.getLibelle() + " est invalide");
        continue;
      }
      
      try {
        Trace.info("-- Début du traitement de la demande " + pDemandeEdition + " pour le canal de diffusion \""
            + canalDiffusion.getLibelle() + "\" avec le type " + typeDocumentGenere.getTypeDocument().getLibelle());
        
        // Le canal fichier Ifs
        if (canalDiffusion instanceof CanalDiffusionFichierIfs) {
          traiterFichierIfs((CanalDiffusionFichierIfs) canalDiffusion, typeDocumentGenere, pDemandeEdition);
        }
        // Le canal impression
        else if (canalDiffusion instanceof CanalDiffusionImpression) {
          // L'impression doit être soumise
          if (((CanalDiffusionImpression) canalDiffusion).isSoumission()) {
            typeDocumentGenere.setUtilisationEnSoumission(true);
            soumettreCanalDiffusionImpression((CanalDiffusionImpression) canalDiffusion, typeDocumentGenere, pDemandeEdition, pDde);
          }
          else {
            // L'impression n'est pas soumise
            traiterImpression((CanalDiffusionImpression) canalDiffusion, typeDocumentGenere, pDemandeEdition, pDde);
          }
        }
        // Le canal base documentaire
        else if (canalDiffusion instanceof CanalDiffusionDocumentStocke) {
          traiterDocumentStocke((CanalDiffusionDocumentStocke) canalDiffusion, typeDocumentGenere, pDemandeEdition);
        }
        // Le canal mail
        else if (canalDiffusion instanceof CanalDiffusionMail) {
          traiterMail((CanalDiffusionMail) canalDiffusion, typeDocumentGenere, pDemandeEdition);
        }
        // Le canal fax
        else if (canalDiffusion instanceof CanalDiffusionFax) {
          traiterFax((CanalDiffusionFax) canalDiffusion, typeDocumentGenere, pDemandeEdition);
        }
        // Le canal fusion de fichier
        else if (canalDiffusion instanceof CanalDiffusionFusionFichier) {
          traiterFusionFichier((CanalDiffusionFusionFichier) canalDiffusion, typeDocumentGenere, pDemandeEdition);
        }
        
        Trace.info("-- Fin du traitement du canal de diffusion " + canalDiffusion.getLibelle());
      }
      catch (Exception e) {
        // Ne rien faire
      }
    }
    
    pDemandeEdition.setStatut(EnumStatutEdition.SUCCES);
  }
  
  /**
   * Traitement le canal de diffusion Impression dans le cas d'une impression directe.
   * Note: comme le traitement est soumis le fichier tempo n'est pas détruit. Le dossier tmp sera nettoyé au démarrage de NewSim.
   */
  private void traiterImpression(CanalDiffusionImpression pCanalDiffusionImpression, TypeDocumentGenere pTypeDocumentGenere,
      DemandeEdition pDemandeEdition, DescriptionEdition pDde) {
    if (pCanalDiffusionImpression == null) {
      throw new MessageErreurException("Le canal de diffusion document stocké est invalide");
    }
    FileNG fichierGenereTemporaire = controlerFichierTemporaire(pTypeDocumentGenere, pDemandeEdition);
    
    // Vérification de la validité du fichier tempo (extension)
    if (!fichierGenereTemporaire.getExtension(false).toLowerCase().equals(pCanalDiffusionImpression.getExtension())) {
      throw new MessageErreurException("L'extension du nom du fichier généré est invalide.");
    }
    
    // Vérification que l'extension soit correcte
    if (!pCanalDiffusionImpression.getNomOutq().toLowerCase().endsWith(".outq")) {
      throw new MessageErreurException("Le nom de la outq est invalide.");
    }
    
    // Préparation de l'envoi du fichier dans la outq de l'imprimante
    OutputQueue outq = new OutputQueue(systemeManager.getSystem(), pCanalDiffusionImpression.getNomOutq());
    gestionSpool.setOutq(outq);
    // Construction du nom du spool à partir de l'établissemnt et du numéro de la demande d'édition (PNID de PSEMPNSIMM)
    String nomSpool = String.format("%s%07d", pDemandeEdition.getId().getCodeEtablissement().trim(), pDemandeEdition.getId().getNumero());
    SpooledFile splf =
        gestionSpool.cpyFichier2Spool(fichierGenereTemporaire.getAbsolutePath(), nomSpool, pCanalDiffusionImpression.getNbrExemplaire(),
            pCanalDiffusionImpression.isSuspendre(), null, pCanalDiffusionImpression.getPriorite(),
            pCanalDiffusionImpression.getScriptAvant(true), pCanalDiffusionImpression.getScriptApres(true));
    if (splf == null) {
      throw new MessageErreurException("Le spool généré à partir du fichier " + fichierGenereTemporaire.getAbsolutePath()
          + " est invalide. Vérifier que la outq " + pCanalDiffusionImpression.getNomOutq() + " existe.");
    }
    gestionSpool.libereSpool(splf);
    Trace
        .info("Réussite de la copie de " + fichierGenereTemporaire.getAbsolutePath() + " vers " + pCanalDiffusionImpression.getNomOutq());
    
    // Ajout des pièces jointes s'il y en a
    imprimerPiecesJointes(pCanalDiffusionImpression, pDde.getListPiecesJointes(), outq);
    
    pDemandeEdition.setStatut(EnumStatutEdition.SUCCES);
  }
  
  /**
   * Traitement du canal des fichiers dans l'IFS.
   */
  private void traiterFichierIfs(CanalDiffusionFichierIfs pCanalDiffusionFichierIfs, TypeDocumentGenere pTypeDocumentGenere,
      DemandeEdition pDemandeEdition) {
    // Contrôle des paramètres
    if (pCanalDiffusionFichierIfs == null) {
      throw new MessageErreurException("Le canal de diffusion fichier Ifs est invalide");
    }
    FileNG fichierGenereTemporaire = controlerFichierTemporaire(pTypeDocumentGenere, pDemandeEdition);
    
    // Controle s'il y a un nom de fichier sans extension de renseigné (via la demande), dans le cas contraire c'est le nom du fichier
    // généré qui est utilisé
    if (pCanalDiffusionFichierIfs.getNomFichierSansExtension() == null) {
      pCanalDiffusionFichierIfs.setNomFichierSansExtension(pTypeDocumentGenere.getNomFichierSansExtension());
    }
    
    // Le chemin complet du fichier pour la destination
    String cheminCompletDestination = Constantes.normerTexte(pCanalDiffusionFichierIfs.getCheminCompletFichier());
    if (cheminCompletDestination.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier destination est invalide");
    }
    
    // Vérification que le dossier de destination existe, sinon il est créé
    cheminCompletDestination = Constantes.code2unicode(cheminCompletDestination);
    File fichierDestination = new File(cheminCompletDestination);
    if (!fichierDestination.getParentFile().exists()) {
      fichierDestination.getParentFile().mkdirs();
    }
    // Sauvegarde du fichier s'il est déjà présent
    if (pCanalDiffusionFichierIfs.isSauvegardeAncien()) {
      sauvegarderFichier(new FileNG(cheminCompletDestination));
    }
    
    // Copie le fichier du dossier temporaire vers sa destination finale
    IFSFile files = new IFSFile(systemeManager.getSystem(), fichierGenereTemporaire.getAbsolutePath());
    try {
      if (!files.copyTo(cheminCompletDestination)) {
        throw new MessageErreurException(
            "Echec de la copie de " + fichierGenereTemporaire.getAbsolutePath() + " vers " + cheminCompletDestination);
      }
      else {
        Trace.info("Réussite de la copie de " + fichierGenereTemporaire.getAbsolutePath() + " vers " + cheminCompletDestination);
      }
      // Renseignement du chemin complet dans l'Ifs
      pTypeDocumentGenere.setCheminCompletIfs(cheminCompletDestination);
    }
    catch (Exception e) {
      Trace.erreur(e, "Si le fichier est nommé null.pdf: il y a un problème d'accès aux données du PGVMEFM et du PGVMLFM");
      throw new MessageErreurException("Problème lors de la copie du fichier.");
    }
  }
  
  /**
   * Traitement du canal document stocké.
   */
  private void traiterDocumentStocke(CanalDiffusionDocumentStocke pCanalDiffusionDocumentStocke, TypeDocumentGenere pTypeDocumentGenere,
      DemandeEdition pDemandeEdition) {
    if (pCanalDiffusionDocumentStocke == null) {
      throw new MessageErreurException("Le canal de diffusion document stocké est invalide.");
    }
    FileNG fichierGenereTemporaire = controlerFichierTemporaire(pTypeDocumentGenere, pDemandeEdition);
    
    // Seul les fichiers PDF sont stockés dans la base documentaire car c'est un format exploitable par des êtres humains
    if (!pTypeDocumentGenere.getTypeDocument().equals(EnumTypeDocument.PDF)) {
      throw new MessageErreurException("Le fichier " + pTypeDocumentGenere.getCheminCompletFichierTemporaire()
          + " n'est pas un document de type PDF, la demande de stockage en base documentaire est ignorée.");
    }
    
    // Le chemin complet du fichier avec le nom final attendu pour la duplication du fichier temporaire
    String cheminCompletDestination =
        Constantes.normerTexte(pCanalDiffusionDocumentStocke.getCheminCompletFichier(fichierGenereTemporaire));
    if (cheminCompletDestination.isEmpty()) {
      throw new MessageErreurException("Le nom du fichier destination est invalide.");
    }
    
    // Vérification que le dossier de destination existe, sinon il est créé
    cheminCompletDestination = Constantes.code2unicode(cheminCompletDestination);
    FileNG fichierDestination = new FileNG(cheminCompletDestination);
    if (!fichierDestination.getParentFile().exists()) {
      fichierDestination.getParentFile().mkdirs();
    }
    
    // Copie le fichier du dossier temporaire vers sa destination finale
    IFSFile files = new IFSFile(systemeManager.getSystem(), fichierGenereTemporaire.getAbsolutePath());
    try {
      if (!files.copyTo(cheminCompletDestination)) {
        throw new MessageErreurException(
            "Echec de la copie de " + fichierGenereTemporaire.getAbsolutePath() + " vers " + cheminCompletDestination + '.');
      }
      else {
        Trace.info("Réussite de la copie de " + fichierGenereTemporaire.getAbsolutePath() + " vers " + cheminCompletDestination + '.');
      }
      // Renseignement du chemin complet dans l'Ifs
      pTypeDocumentGenere.setCheminCompletIfs(cheminCompletDestination);
    }
    catch (Exception e) {
      Trace.erreur(e, "Si le fichier est nommé null.pdf: il y a un problème d'accès aux données du PGVMEFM et du PGVMLFM");
      throw new MessageErreurException("Problème lors de la copie du fichier.");
    }
    
    // Stockage en base documentaire en fonction du type fonctionnel
    String description = pCanalDiffusionDocumentStocke.getDescription();
    EnumTypeDocumentStocke typeDocumentStocke = pCanalDiffusionDocumentStocke.getTypeDocumentStocke();
    switch (pCanalDiffusionDocumentStocke.getTypeDocumentStocke()) {
      case NON_DEFINI:
        Trace.alerte("Le stockage des documents dont le type fonctionnel n'est pas défini n'est pas pris en compte pour l'instant");
        break;
      case COMMANDE_ACHATS:
        Trace.alerte("Le stockage des documents d'achat n'est pas pris en compte pour l'instant");
        break;
      case FACTURE_VENTES:
      case DEVIS_VENTES:
      case BON_VENTES:
      case COMMANDE_VENTES:
        // Pour l'instant le fichier original n'est pas supprimé des documents liés, il faudra se poser la question lors de l'abandon des
        // documents liés. Ce n'est pas le fichier temporaire qui est envoyé à la base documentaire afin d'avoir le bon nom de fichier et
        // non le nom du spool.
        DocumentStocke documentstocke = BaseDocumentaire.sauverDocumentStocke(queryManager, fichierDestination,
            pDemandeEdition.getId().getIdEtablissement(), pDemandeEdition.getProfil(), typeDocumentStocke, description, false);
        IdDocumentVente idDocumentVente = pCanalDiffusionDocumentStocke.getIdDocumentVente();
        if (idDocumentVente != null) {
          BaseDocumentaire.sauverLienDocumentStocke(queryManager, documentstocke.getId(), idDocumentVente);
        }
        IdClient idClient = pCanalDiffusionDocumentStocke.getIdClient();
        if (idClient != null) {
          BaseDocumentaire.sauverLienDocumentStocke(queryManager, documentstocke.getId(), idClient);
        }
        // Mise à jour de la demande d'édition en table afin de stocker l'identifiant du document stocké et le statut
        pDemandeEdition.setIdDocumentStocke(documentstocke.getId());
        sqlDemandeEdition.sauverDemandeEdition(pDemandeEdition, false);
        Trace.info("Le document " + idDocumentVente + " a été stocké dans la base documentaire (" + documentstocke.getId() + ").");
        // Renseignement du chemin complet dans la base documentaire
        pTypeDocumentGenere
            .setCheminCompletBaseDocumentaire(BaseDocumentaire.getCheminCompletDocumentStocke(queryManager, documentstocke));
        break;
      default:
        Trace.alerte("Le stockage des documents " + typeDocumentStocke.getLibelle() + " n'est pas pris en compte pour l'instant.");
    }
    
  }
  
  /**
   * Traitement du canal mail.
   */
  private void traiterMail(CanalDiffusionMail pCanalDiffusionMail, TypeDocumentGenere pTypeDocumentGenere,
      DemandeEdition pDemandeEdition) {
    if (pCanalDiffusionMail == null) {
      throw new MessageErreurException("Le canal de diffusion mail est invalide");
    }
    
    IdMail idMail = pCanalDiffusionMail.getIdMail();
    envoyerMail(idMail, pTypeDocumentGenere, pDemandeEdition);
  }
  
  /**
   * Traitement du canal fax.
   */
  private void traiterFax(CanalDiffusionFax pCanalDiffusionFax, TypeDocumentGenere pTypeDocumentGenere, DemandeEdition pDemandeEdition) {
    if (pCanalDiffusionFax == null) {
      throw new MessageErreurException("Le canal de diffusion fax est invalide");
    }
    
    IdMail idMail = pCanalDiffusionFax.getIdMailPourFax();
    envoyerMail(idMail, pTypeDocumentGenere, pDemandeEdition);
  }
  
  /**
   * Traitement du canal fusion de fichier.
   */
  private void traiterFusionFichier(CanalDiffusionFusionFichier pCanalDiffusionFusionFichier, TypeDocumentGenere pTypeDocumentGenere,
      DemandeEdition pDemandeEdition) {
    if (pCanalDiffusionFusionFichier == null) {
      throw new MessageErreurException("Le canal de diffusion fusion de fichier est invalide");
    }
    FileNG fichierGenereTemporaire = controlerFichierTemporaire(pTypeDocumentGenere, pDemandeEdition);
    
    // Création d'un fichier de lock afin de ne pas fusionner alors qu'un autre thread fusionne avec le fichier
    GestionFichierTexte fichierLock = creerFichierLock(pCanalDiffusionFusionFichier.getCheminCompletFichierDestinationFusion());
    
    // Fusion du document généré avec d'autres documents PDF
    try {
      fusionnerDocumentPDF(pCanalDiffusionFusionFichier.getCheminCompletFichierDestinationFusion(), fichierGenereTemporaire);
    }
    catch (Exception e) {
      // Ne rien faire car on n'arrête pas le traitement pour la fusion. Il y a une trace dans les logs et cela est suffisant.
      Trace.erreur("Erreur lors de la fusion du fichier");
    }
    
    // Suppression du fichier de lock
    if (fichierLock != null && fichierLock.getFichier() != null) {
      Trace.info("Suppression du fichier de lock " + fichierLock.getNomFichier() + ".");
      fichierLock.getFichier().delete();
    }
  }
  
  /**
   * Créer un fichier de lock.
   * Vérifie et attend avant de le créer qu'il n'existe pas déjà.
   */
  private GestionFichierTexte creerFichierLock(String pCheminCompletFichier) {
    pCheminCompletFichier = Constantes.normerTexte(pCheminCompletFichier);
    if (pCheminCompletFichier.isEmpty()) {
      return null;
    }
    
    GestionFichierTexte fichierLock = new GestionFichierTexte(pCheminCompletFichier + ".lock");
    fichierLock.setContenuFichier("");
    
    // Contrôle si présence d'un fichier de lock, attente si présent et si l'attente globale est inférieure à 2 minute
    int cpt = 0;
    while (fichierLock.getFichier().exists() && cpt < 1000) {
      try {
        Trace.info("Présence du fichier de lock " + fichierLock.getNomFichier() + " détectée donc mise en attente (" + cpt + ") de "
            + idDemandeEdition + '.');
        Thread.sleep(120);
        cpt++;
      }
      catch (InterruptedException e) {
      }
    }
    
    Trace.info("Ecriture du fichier de lock " + fichierLock + " pour " + idDemandeEdition + '.');
    fichierLock.ecritureFichier();
    
    return fichierLock;
  }
  
  /**
   * Envoyer un mail.
   */
  private void envoyerMail(IdMail pIdMail, TypeDocumentGenere pTypeDocumentGenere, DemandeEdition pDemandeEdition) {
    // Contrôle de l'identifiant du mail
    if (pIdMail == null) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.IDENTIFIANT_MAIL_INVALIDE);
      throw new MessageErreurException(EnumCodeErreur.IDENTIFIANT_MAIL_INVALIDE.getLibelle());
    }
    
    // Chargement du mail
    List<IdMail> listeIdMail = new ArrayList<IdMail>();
    listeIdMail.add(pIdMail);
    SqlMail sqlMail = new SqlMail(queryManager);
    List<Mail> listeMail = sqlMail.chargerListeMail(listeIdMail);
    if (listeMail == null || listeMail.isEmpty()) {
      pDemandeEdition.setCodeErreur(EnumCodeErreur.MAIL_NON_TROUVE);
      throw new MessageErreurException(EnumCodeErreur.MAIL_NON_TROUVE.getLibelle() + '(' + pIdMail.toString() + ").");
    }
    
    Mail mail = listeMail.get(0);
    try {
      // Création de l'identifiant de la pièce jointe
      IdPieceJointe idPieceJointe = IdPieceJointe.getInstanceAvecCreationId(mail.getId());
      
      // Pièce jointe de type document stocké
      if (pDemandeEdition.getIdDocumentStocke() != null) {
        IdDocumentStocke idDocumentStocke = pDemandeEdition.getIdDocumentStocke();
        DocumentStocke documentStocke = BaseDocumentaire.chargerDocumentStocke(queryManager, idDocumentStocke);
        if (documentStocke == null) {
          pDemandeEdition.setCodeErreur(EnumCodeErreur.CHARGEMENT_DOCUMENT_STOCKE_IMPOSSIBLE);
          throw new MessageErreurException(
              EnumCodeErreur.CHARGEMENT_DOCUMENT_STOCKE_IMPOSSIBLE.getLibelle() + '(' + idDocumentStocke.toString() + ").");
        }
        PieceJointeDocumentStocke pieceJointe = new PieceJointeDocumentStocke(idPieceJointe);
        pieceJointe.setDocumentStocke(documentStocke);
        mail.getListePieceJointe().add(pieceJointe);
      }
      // Pièce jointe de type fichier ifs
      else {
        String cheminCompletPieceJointe = pTypeDocumentGenere.getCheminCompletFichierTemporaire();
        if (pTypeDocumentGenere.getCheminCompletIfs() != null) {
          cheminCompletPieceJointe = pTypeDocumentGenere.getCheminCompletIfs();
        }
        PieceJointeFichier pieceJointe = new PieceJointeFichier(idPieceJointe);
        pieceJointe.setFichier(cheminCompletPieceJointe);
        mail.getListePieceJointe().add(pieceJointe);
      }
      
      // Sauvegarde du mail en base
      mail.setStatut(EnumStatutMail.A_ENVOYER);
      sqlMail.modifierMail(mail);
    }
    catch (Exception e) {
      sqlMail.mettreStatutMailErreurTraitement(mail.getId(), e.getMessage());
    }
  }
  
  /**
   * Traitement du canal de diffusion Impression dans le cas d'une impression directe soumise.
   */
  private void soumettreCanalDiffusionImpression(final CanalDiffusionImpression pCanalDiffusionImpression,
      final TypeDocumentGenere pTypeDocumentGenere, final DemandeEdition pDemandeEdition, final DescriptionEdition pDde) {
    new Thread() {
      @Override
      public void run() {
        Trace.info("Soumission de l'impression");
        traiterImpression(pCanalDiffusionImpression, pTypeDocumentGenere, pDemandeEdition, pDde);
      }
    }.start();
  }
  
  /**
   * Imprime les pièces jointes associées à un document.
   */
  private boolean imprimerPiecesJointes(CanalDiffusionImpression si, File[] pListePieceJointe, OutputQueue outq) {
    if (pListePieceJointe == null || pListePieceJointe.length == 0) {
      return true;
    }
    
    for (File file : pListePieceJointe) {
      Trace.info("Impression de la pièce jointe " + file.getAbsolutePath());
      String nomspool = si.getNomSpool();
      if (nomspool.length() > 8) {
        nomspool = nomspool.substring(0, 8) + "PJ";
      }
      else {
        nomspool = nomspool + "PJ";
      }
      SpooledFile splf = gestionSpool.cpyFichier2Spool(file.getAbsolutePath(), nomspool, 1, si.isSuspendre(), null, si.getPriorite(),
          si.getScriptAvant(true), si.getScriptApres(true));
      Trace.info("fichier=" + file.getAbsolutePath() + " nomspool=" + nomspool);
      if (splf == null) {
        return false;
      }
      gestionSpool.libereSpool(splf);
    }
    return true;
  }
  
  /**
   * Fusionne un document PDF avec un autre.
   */
  private void fusionnerDocumentPDF(String pDocumentDestinationFusion, File pFichierPdfGenere) {
    // Les contrôles
    if (pDocumentDestinationFusion == null) {
      throw new MessageErreurException("Impossible de fusionner le document généré car le document de fusion est invalide.");
    }
    if (!pDocumentDestinationFusion.toLowerCase().endsWith(EnumTypeDocument.PDF.getExtension())) {
      throw new MessageErreurException("Impossible de fusionner le document généré car le document n'est pas un PDF.");
    }
    File documentDestinationFusion = new File(pDocumentDestinationFusion);
    if (documentDestinationFusion.isDirectory()) {
      throw new MessageErreurException(
          "Le chemin du document dans lequel doit être fusionné le document généré doit être un fichier et non un dossier.");
    }
    if (pFichierPdfGenere == null) {
      throw new MessageErreurException(
          "Impossible de fusionner le document généré avec le document " + pDocumentDestinationFusion + " car il est invalide.");
    }
    if (!pFichierPdfGenere.exists()) {
      throw new MessageErreurException("Impossible de fusionner le document généré avec le document " + pDocumentDestinationFusion
          + " car le document généré n'existe pas.");
    }
    
    // La fusion du document généré dans un document destination
    PDFMergerUtility PDFmerger = new PDFMergerUtility();
    PDDocument documentDestination = null;
    try {
      // Création du dossier du fichier de destination
      if (!documentDestinationFusion.getParentFile().exists()) {
        documentDestinationFusion.getParentFile().mkdirs();
      }
      
      // Initialisation du document de destination si le fichier existe déjà
      if (documentDestinationFusion.exists()) {
        documentDestination = PDDocument.load(documentDestinationFusion);
      }
      // Initialisation du document de destination si le fichier n'existe pas
      else {
        documentDestination = new PDDocument();
      }
      PDDocument documentAFusionner = PDDocument.load(pFichierPdfGenere);
      PDFmerger.appendDocument(documentDestination, documentAFusionner);
      documentDestination.save(documentDestinationFusion);
      documentDestination.close();
      documentAFusionner.close();
      Trace.info("Fusion du document généré " + pFichierPdfGenere.getAbsolutePath() + " avec " + pDocumentDestinationFusion);
    }
    catch (Exception e) {
      throw new MessageErreurException(e,
          "Erreur lors de la fusion du fichier " + pFichierPdfGenere.getAbsolutePath() + " vers " + pDocumentDestinationFusion);
    }
  }
  
  /**
   * Execute la commande sur la machine où se trouve le serveur NewSim.
   */
  private boolean executerCommande(String cmd, String fichier) {
    try {
      final Runtime runtime = Runtime.getRuntime();
      runtime.exec(new String[] { cmd, fichier });
      return true;
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors du lancement de la commande: " + cmd);
    }
    return false;
  }
  
  /**
   * Imprime directement un fichier PDF grâce au driver natif des imprimantes.
   */
  private boolean imprimerPdf(String printerName, boolean silentPrint, String fichierpdf, boolean delete) throws Exception {
    boolean printerFound = false;
    PDDocument document = null;
    // On vérifie que le document soit bien un PDF
    if (fichierpdf == null || fichierpdf.toLowerCase().lastIndexOf(EnumTypeDocument.PDF.getExtension()) == -1) {
      return false;
    }
    File fichier = new File(fichierpdf);
    try {
      document = PDDocument.load(fichier);
      
      // if( document.isEncrypted()) document.decrypt( password );
      
      PrinterJob printJob = PrinterJob.getPrinterJob();
      printJob.setJobName(fichier.getName());
      
      if (printerName != null) {
        printerName = printerName.replaceAll("\"", "");
        PrintService[] printService = PrinterJob.lookupPrintServices();
        for (int i = 0; !printerFound && i < printService.length; i++) {
          if (printService[i].getName().indexOf(printerName) != -1) {
            printJob.setPrintService(printService[i]);
            printerFound = true;
          }
        }
      }
      
      if (silentPrint && printerFound) {
        document.silentPrint(printJob);
      }
      else {
        document.print(printJob);
      }
    }
    finally {
      if (document != null) {
        document.close();
      }
    }
    
    // On supprime le fichier si besoin et s'il existe
    if (delete && fichier.exists()) {
      if (fichier.delete()) {
        Trace.info("Réussite de la suppression de " + fichier.getAbsolutePath());
      }
      else {
        Trace.erreur("Echec de la suppression de " + fichier.getAbsolutePath());
      }
    }
    return false;
  }
  
  /**
   * Sauvegarde le fichier s'il existe déjà avant d'être écrasé.
   */
  private void sauvegarderFichier(FileNG pFichier) {
    if (pFichier == null || !pFichier.exists()) {
      return;
    }
    
    // Préparation des variables qui vont servir pour le filtrage
    final String extension = pFichier.getExtension(true);
    final int longueur = pFichier.getName().length();
    final String prefix = pFichier.getName().substring(0, longueur - extension.length());
    
    // Récupération des fichiers de sauvegarde
    String[] listeAnciens = pFichier.getParentFile().list(new FilenameFilter() {
      @Override
      public boolean accept(File repertoire, String nom) {
        return (nom.startsWith(prefix) && nom.endsWith(extension) && (nom.length() - longueur == 3));
      }
    });
    
    // Si aucun fichier de sauvegarde trouvé alors c'est le premier
    if (listeAnciens == null || listeAnciens.length == 0) {
      String chaine = pFichier.getParent() + File.separatorChar + prefix + ".00" + extension;
      boolean ret = pFichier.renameTo(new File(chaine));
      if (ret) {
        Trace.info("Réussite de la sauvegarde en " + chaine);
      }
      else {
        Trace.erreur("Echec de la sauvegarde en " + chaine);
      }
      return;
    }
    
    // Récupération du numéro le plus haut des fichiers de sauvegarde
    int valmax = 0;
    for (String sauve : listeAnciens) {
      int posd = sauve.indexOf('.') + 1;
      int posf = sauve.lastIndexOf(extension);
      if (posd >= posf) {
        continue;
      }
      String num = sauve.substring(posd, posf);
      try {
        int val = Integer.parseInt(num);
        if (val > valmax) {
          valmax = val;
        }
      }
      catch (NumberFormatException nfe) {
        Trace.erreur(nfe, "sauvegardeFichier");
      }
    }
    // Dans le cas où il arrive à 99 on incrémente plus
    if (valmax < 99) {
      valmax++;
    }
    
    // On genere le File
    File sauve = new File(pFichier.getParent() + File.separatorChar + prefix + (valmax < 10 ? ".0" : ".") + valmax + extension);
    sauve.delete();
    
    boolean ret = pFichier.renameTo(sauve);
    if (ret) {
      Trace.info("Réussite de la sauvegarde en " + sauve.getAbsolutePath());
    }
    else {
      Trace.erreur("Echec de la sauvegarde en " + sauve.getAbsolutePath());
    }
  }
}
