/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.serveur;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.Socket;

import com.ibm.as400.access.AS400;

import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;
import ri.serien.libcommun.exploitation.module.EnumModule;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.chaine.StringIP;
import ri.serien.libcommun.outils.session.User;

/**
 * Gestion socket pour le serveur des éditions
 */
public class SessionNewSim extends Thread {
  // Constantes erreurs de chargement
  private final static String CODEPAGE = "CP858";
  
  // Variables diverses
  public DataInputStream in;
  public DataOutputStream ou;
  public Socket socketclient;
  private boolean debug = false;
  private User utilisateur = null;
  private AS400 systeme = null;
  
  /**
   * Constructeur de la classe
   */
  public SessionNewSim(AS400 pSysteme, Socket pSocket, User pUser, boolean pModeDebug) {
    // Initialisation variables
    systeme = pSysteme;
    socketclient = pSocket;
    utilisateur = pUser;
    debug = pModeDebug;
  }
  
  /**
   * Boucle principale
   */
  @Override
  public void run() {
    StringIP client = new StringIP("");
    boolean boucle = true;
    byte buffer[] = null;
    String chaine;
    int idmessage;
    
    // Construction du nom du thread
    String nomThread = String.format(EnumModule.NEWSIM + "(%03d)", Thread.currentThread().getId());
    setName(nomThread);
    
    TraitementDemandeEdition traitementDemandeEdition = new TraitementDemandeEdition(systeme, utilisateur, debug);
    
    try {
      ServeurNewSim.getListeSessionNewSim().add(this);
      InetAddress inet = socketclient.getInetAddress();
      in = new DataInputStream(new BufferedInputStream(socketclient.getInputStream(), Constantes.TAILLE_BUFFER));
      ou = new DataOutputStream(new BufferedOutputStream(socketclient.getOutputStream(), Constantes.TAILLE_BUFFER));
      socketclient.setSoTimeout(Constantes.SOCKET_TIMEOUT);
      
      client.setIP(inet.toString());
      Trace.info("Connexion d'un nouveau client : " + client.getIP());
      Trace.info("Nombre de client(s) connecté(s) : " + ServeurNewSim.getListeSessionNewSim().size());
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur pendant l'exécution");
    }
    
    try {
      while (boucle) {
        idmessage = in.readInt();
        
        if (debug) {
          Trace.debug("Identifiant du message reçu :" + idmessage);
        }
        switch (idmessage) {
          // Le client Série N fait une demande d'édition (en RPG).
          // Les 2 traitements possibles sont ici : la demande et la déconnexion
          case ConstantesNewSim.RECOIT_DEMANDE_REQUETE_AS:
            Trace.info("Début de la demande d'édition d'un programme RPG (" + ConstantesNewSim.RECOIT_DEMANDE_REQUETE_AS + ")");
            chaine = Constantes.normerTexte(lectureSocketFromAS400());
            Trace.info("1-> " + chaine);
            if (chaine.isEmpty()) {
              // Envoi l'arrêt à la sessionEdition
              boucle = false;
              Trace.info("Déconnexion du client : " + client.getIP());
            }
            else {
              try {
                // Traitement et récupératon éventuelle du chemin du fichier généré
                chaine = traitementDemandeEdition.traiterDemandeEdition(chaine);
              }
              catch (Exception e) {
                Trace.erreur(e, "Erreur lors du traitement demandé par le SEXP21LE.");
                chaine = "";
              }
            }
            Trace.info("2-> " + chaine);
            EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE, chaine);
            Trace.info("Fin de la demande d'édition d'un programme RPG");
            break;
          
          // Le client Série N fait une demande d'édition (en java)
          case ConstantesNewSim.RECOIT_REQUETE:
            Trace.info("Début de la demande d'édition d'un programme java (" + ConstantesNewSim.RECOIT_REQUETE + ")");
            buffer = lectureSocket();
            chaine = new String(buffer, 0, buffer.length);
            Trace.info("1-> " + chaine);
            try {
              chaine = traitementDemandeEdition.traiterDemandeEdition(chaine);
            }
            catch (Exception e) {
              Trace.erreur(e, "Erreur lors du traitement demandé par un programme java.");
              chaine = "";
            }
            Trace.info("2-> " + chaine);
            EnvoiMessageSocket(ConstantesNewSim.DEMANDE_REQUETE, chaine);
            Trace.info("Fin de la demande d'édition d'un programme java");
            break;
          
          // Le client Série N fait une demande de déconnexion (en java)
          case ConstantesNewSim.RECOIT_REQUETE_DECONNEXION:
            Trace.info("Début de la demande de déconnexion (" + ConstantesNewSim.RECOIT_REQUETE_DECONNEXION + ")");
            buffer = lectureSocket();
            chaine = new String(buffer, 0, buffer.length);
            Trace.info("1-> " + chaine);
            // Envoi l'arrêt à la sessionEdition
            EnvoiMessageSocket(Constantes.STOP, null);
            boucle = false;
            Trace.info("2-> " + chaine);
            Trace.info("Déconnexion du client : " + client.getIP());
            break;
          
          // Valeur non traitée
          default:
            Trace.info("Données reçues mais non traitées");
            // Lecture du contenu du buffer pour pas bloquer l'instance
            buffer = new byte[in.available()];
            in.read(buffer);
            break;
        }
      }
    }
    catch (IOException e) {
      Trace.erreur(e, "Perte de " + client.getIP());
    }
    try {
      // Fermeture des sockets
      if (ou != null) {
        ou.close();
      }
      if (in != null) {
        in.close();
      }
      if (socketclient != null) {
        socketclient.close();
      }
      socketclient = null;
    }
    catch (IOException ioe) {
      Trace.erreur(ioe, "");
    }
    
    // Suppression de la sessionNewSim de la liste
    ServeurNewSim.getListeSessionNewSim().remove(this);
    
    // Test de consommation de ram
    Trace.debug(SessionNewSim.class, "run", "Fermer connexion", "connexion", ServeurNewSim.getListeSessionNewSim().size());
    Trace.debugMemoire(SessionNewSim.class, "run");
  }
  
  /**
   * Envoi un message au Client
   */
  public synchronized int EnvoiMessageSocket(int idmsg, String message) {
    int longueur = 0;
    try {
      ou.writeInt(idmsg);
      if (message != null) {
        message = message.trim(); // Modif à cause du blocage si
        // connexion a distance
        longueur = message.length(); // Modif à cause du blocage si
        // connexion a distance
        ou.writeInt(longueur);
        ou.write(message.getBytes(), 0, longueur);
      }
      ou.flush();
    }
    catch (Exception e) {
      Trace.erreur(e, "Problème lors de l'envoi du message sur la socket");
      return Constantes.ERREUR;
    }
    
    return Constantes.OK;
  }
  
  // -- Méthodes privées
  
  /**
   * Lecture des octets reçus sur la Socket lors d'une demande du serveur SGM
   */
  private synchronized String lectureSocketFromAS400() throws UnsupportedEncodingException {
    int octet_lus = 0;
    // Correspond à la taille de la variable Requete du SEXP21LE
    byte buffercomplet[] = new byte[512];
    
    // On récupère tous les octets envoyés par le client
    try {
      octet_lus = in.read(buffercomplet);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur pendant l'exécution");
      return null;
    }
    
    return new String(buffercomplet, 0, octet_lus - 1, CODEPAGE);
  }
  
  /**
   * Lecture des octets reçus sur la Socket
   */
  private synchronized byte[] lectureSocket() {
    int octet_alire = 0;
    int octet_lus = 0;
    int compteur = 0;
    byte buffercomplet[] = null;
    
    // On récupère tous les octets envoyés par le client
    try {
      octet_alire = in.readInt();
      buffercomplet = new byte[octet_alire];
      do {
        octet_lus = in.read(buffercomplet, compteur, octet_alire);
        compteur += octet_lus;
        octet_alire -= octet_lus;
      }
      while (octet_alire != 0);
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur pendant l'exécution");
      return null;
    }
    
    return buffercomplet;
  }
  
  // -- Accesseurs
  
}
