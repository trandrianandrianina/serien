/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libcommun.exploitation.edition.EnumActionSurSpool;
import ri.serien.libcommun.exploitation.edition.EnumTypeDocument;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.Trace;
import ri.serien.newsim.canaldiffusion.CanalDiffusion;
import ri.serien.newsim.serveur.TypeDocumentGenere;

/**
 * Description d'une édition d'un document.
 * TODO A Remmer avec un nom plus fin.
 */
public class DescriptionEdition {
  // Constantes
  public static final String EXTENSION = ".dde";
  public static final int NON_UTILISE = 0;
  public static final int VEILLEOUTQ = 1;
  public static final int DIRECTE = 2;
  public static final int LES_DEUX = 3;
  
  public static final double VERSION = 1.20;
  
  // Variables
  // Nom de cette description (le même que celui du fichier dde correspondant)
  private String nomFichierDDE = null;
  // Nom du spool à récupérer
  private String nomSpool = null;
  // Nom de la outqueue contenant le spool
  private String nomOutQueue = null;
  // Canal de diffusion: fichier, mail, impression, ...
  private ArrayList<CanalDiffusion> listeCanalDiffusion = new ArrayList<CanalDiffusion>();
  // Indique si cette description est utilisée ou pas et comment
  private int utilisation = NON_UTILISE;
  // Action sur le spool après la transformation
  private EnumActionSurSpool actionSpool = EnumActionSurSpool.SUSPRENDRE;
  // Version du protocole du fichier DDE (lié à la version du serveur NewSim)
  private double version = 0;
  // Permet de connaitre la méthode utiliser pour générer le document
  private boolean sansSpool = false;
  // Nom de la biliothèque temporaire
  private String libTemp = null;
  // Nom du fichier contenant les entêtes
  private String ficHeaders = null;
  // Nom du fichier contenant les lignes
  private String ficLines = null;
  // Nom du fichier contenant les données extraites au format XML
  private String nameDataFile = null;
  // Chemin complet du dossier contenant les pièces jointes
  private String pathPiecesJointes = null;
  // Précise le préfixe du fichier généré si nécessaire
  private String prefixeNomFichierGenere = "";
  // Lettre de l'environnement d'execution (passée par le programme RPG si édition sans spool uniquement)
  private char lettre = 'X';
  // Profil de celui qui a fait la demande d'édition
  private String profil = null;
  // Le numéro du spool (si la source des données est un spool)
  private Integer numeroSpool = null;
  // Les informations du job auquel est rattaché le spool
  private String nomJob = null;
  private String numeroJob = null;
  private String nomUserJob = null;
  
  private transient String nomFM = null; // Nom de la bibliothèque de travail
  private boolean impression = false;
  // Le nom du fichier de la description de document (ddd ou xsl)
  private String fichierDescriptionDocument = null;
  private HashMap<EnumTypeDocument, TypeDocumentGenere> mapTypeDocument = null;
  
  // -- Méthodes publiques
  
  /**
   * Retourne la liste des fichiers PDF contenus dans le dossier.
   */
  public File[] getListPiecesJointes() {
    if (pathPiecesJointes == null || pathPiecesJointes.trim().isEmpty()) {
      return null;
    }
    
    // Contrôle de l'existence du dossier
    File path = new File(pathPiecesJointes);
    if (!path.exists()) {
      Trace.alerte("Le dossier " + pathPiecesJointes + " n'existe pas.");
      return null;
    }
    
    // Scan du contenu du dossier afin de récupèrer la liste des pièces jointes
    File[] list = path.listFiles(new FilenameFilter() {
      @Override
      public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(EnumTypeDocument.PDF.getExtension());
      }
    });
    
    return list;
  }
  
  /**
   * Analyse la liste des canaux de diffusion afin d'en retirer une map des types de document utilisés et donc à générer.
   */
  private HashMap<EnumTypeDocument, TypeDocumentGenere> mapperTypeDocumentAGenerer() {
    // Liste des types de document à générer à partir de la liste des canaux
    if (listeCanalDiffusion == null || listeCanalDiffusion.isEmpty()) {
      return null;
    }
    
    // Parcourt de la liste des canaux de diffusion
    HashMap<EnumTypeDocument, TypeDocumentGenere> mapTypeDocument = new HashMap<EnumTypeDocument, TypeDocumentGenere>();
    for (CanalDiffusion canalDiffusion : listeCanalDiffusion) {
      TypeDocumentGenere typeDocumentGenere = new TypeDocumentGenere(canalDiffusion.getTypeDocument());
      if (mapTypeDocument.get(typeDocumentGenere.getTypeDocument()) == null) {
        mapTypeDocument.put(typeDocumentGenere.getTypeDocument(), typeDocumentGenere);
      }
    }
    
    return mapTypeDocument;
  }
  
  // -- Méthodes privées
  
  // -- Accesseurs
  
  public String getNomFichierDDE() {
    return nomFichierDDE;
  }
  
  public void setNomFichierDDE(String nom) {
    this.nomFichierDDE = nom;
  }
  
  public String getNomSpool() {
    return nomSpool;
  }
  
  public void setNomSpool(String nomSpool) {
    this.nomSpool = nomSpool;
  }
  
  public String getNomOutQueue() {
    return nomOutQueue;
  }
  
  public void setNomOutQueue(String nomOutQueue) {
    this.nomOutQueue = nomOutQueue;
  }
  
  public ArrayList<CanalDiffusion> getCanalDiffusion() {
    return listeCanalDiffusion;
  }
  
  public void setCanalDiffusion(ArrayList<CanalDiffusion> pCanalDiffusion) {
    this.listeCanalDiffusion = pCanalDiffusion;
  }
  
  public void ajouterCanalDiffusion(CanalDiffusion pCanalDiffusion) {
    if (pCanalDiffusion == null) {
      return;
    }
    this.listeCanalDiffusion.add(pCanalDiffusion);
  }
  
  public int getUtilisation() {
    return utilisation;
  }
  
  public void setUtilisation(int utilisation) {
    this.utilisation = utilisation;
  }
  
  public EnumActionSurSpool getActionSpool() {
    return actionSpool;
  }
  
  public void setActionSpool(EnumActionSurSpool actionSpool) {
    this.actionSpool = actionSpool;
  }
  
  public void setVersion(double version) {
    this.version = version;
  }
  
  public double getVersion() {
    return version;
  }
  
  public String getNomFM() {
    return nomFM;
  }
  
  public void setNomFM(String nomFM) {
    this.nomFM = nomFM;
  }
  
  public boolean isSansSpool() {
    return sansSpool;
  }
  
  public void setSansSpool(boolean sansSpool) {
    this.sansSpool = sansSpool;
  }
  
  public String getLibTemp() {
    return libTemp;
  }
  
  public void setLibTemp(String libTemp) {
    this.libTemp = libTemp;
  }
  
  public String getFicHeaders() {
    return ficHeaders;
  }
  
  public void setFicHeaders(String ficHeaders) {
    this.ficHeaders = ficHeaders;
  }
  
  public String getFicLines() {
    return ficLines;
  }
  
  public void setFicLines(String ficLines) {
    this.ficLines = ficLines;
  }
  
  public String getNameDataFile() {
    return nameDataFile;
  }
  
  public void setNameDataFile(String nameDataFile) {
    this.nameDataFile = nameDataFile;
  }
  
  public String getPathPiecesJointes() {
    return pathPiecesJointes;
  }
  
  public void setPathPiecesJointes(String pathPiecesJointes) {
    this.pathPiecesJointes = pathPiecesJointes;
  }
  
  public String getPrefixeNomFichierGenere() {
    return prefixeNomFichierGenere;
  }
  
  public void setPrefixeNomFichierGenere(String prefixeNomFichierGenere) {
    this.prefixeNomFichierGenere = Constantes.normerTexte(prefixeNomFichierGenere);
  }
  
  public char getLettre() {
    return lettre;
  }
  
  public void setLettre(char lettre) {
    this.lettre = lettre;
  }
  
  public String getProfil() {
    return profil;
  }
  
  public void setProfil(String profil) {
    this.profil = profil;
  }
  
  public Integer getNumeroSpool() {
    return numeroSpool;
  }
  
  public void setNumeroSpool(Integer numeroSpool) {
    this.numeroSpool = numeroSpool;
  }
  
  public String getNomJob() {
    return nomJob;
  }
  
  public void setNomJob(String nomJob) {
    this.nomJob = nomJob;
  }
  
  public String getNumeroJob() {
    return numeroJob;
  }
  
  public void setNumeroJob(String numeroJob) {
    this.numeroJob = numeroJob;
  }
  
  public String getNomUserJob() {
    return nomUserJob;
  }
  
  public void setNomUserJob(String nomUserJob) {
    this.nomUserJob = nomUserJob;
  }
  
  public boolean isImpression() {
    return impression;
  }
  
  public void setImpression(boolean impression) {
    this.impression = impression;
  }
  
  public String getFichierDescriptionDocument() {
    return fichierDescriptionDocument;
  }
  
  public void setFichierDescriptionDocument(String fichierDescriptionDocument) {
    this.fichierDescriptionDocument = fichierDescriptionDocument;
  }
  
  public HashMap<EnumTypeDocument, TypeDocumentGenere> getMapTypeDocument() {
    if (mapTypeDocument == null) {
      mapTypeDocument = mapperTypeDocumentAGenerer();
    }
    return mapTypeDocument;
  }
  
}
