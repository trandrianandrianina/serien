/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

/**
 * Description d'une image
 */
public class DescriptionImage extends DescriptionObject {
  // Variables
  private String cheminImage = null;
  private boolean keepRatio = true;
  
  /**
   * @param cheminImage the cheminImage to set
   */
  public void setCheminImage(String cheminImage) {
    this.cheminImage = cheminImage;
  }
  
  /**
   * @return the cheminImage
   */
  public String getCheminImage() {
    return cheminImage;
  }
  
  /**
   * @param keepRatio the keepRatio to set
   */
  public void setKeepRatio(boolean keepRatio) {
    this.keepRatio = keepRatio;
  }
  
  /**
   * @return the keepRatio
   */
  public boolean isKeepRatio() {
    return keepRatio;
  }
  
  /**
   * Retourne une image pour le Rad
   * @param dpi_aff
   * @param dpi_img
   * @return
   * 
   *         public GfxImage getGfxImage(int dpi_aff, int dpi_img, final GfxPageEditor planTravail)
   *         {
   *         GfxImage image = new GfxImage(this, dpi_aff, dpi_img, planTravail);
   *         return image;
   *         }
   */
  
}
