/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

import java.util.ArrayList;

import ri.serien.libcommun.exploitation.edition.ConstantesNewSim;

/**
 * Description d'une étiquette
 */
public class DescriptionEtiquette {
  // Variables
  protected float marge_x = 0; // Marge en cm
  protected float marge_y = 0; // Marge en cm
  private float largeur = ConstantesNewSim.A4_WIDTH; // Largeur en cm
  private float hauteur = ConstantesNewSim.A4_HEIGHT; // Hauteur en cm
  protected ArrayList<DescriptionObject> dobjet = new ArrayList<DescriptionObject>();
  
  /**
   * @return dligne
   */
  public ArrayList<DescriptionObject> getDObject() {
    return dobjet;
  }
  
  /**
   * @param dobj dligne à définir
   */
  public void setDObject(ArrayList<DescriptionObject> dobj) {
    this.dobjet = dobj;
  }
  
  /**
   * @param dobject à définir
   */
  public void addDObjet(DescriptionObject dobj) {
    if (this.dobjet == null) {
      this.dobjet = new ArrayList<DescriptionObject>();
    }
    this.dobjet.add(dobj);
  }
  
  /**
   * @return marge_x
   */
  public float getMarge_x() {
    return marge_x;
  }
  
  /**
   * @param marge_x marge_x à définir
   */
  public void setMarge_x(float marge_x) {
    this.marge_x = marge_x;
  }
  
  /**
   * @return marge_y
   */
  public float getMarge_y() {
    return marge_y;
  }
  
  /**
   * @param marge_y marge_y à définir
   */
  public void setMarge_y(float marge_y) {
    this.marge_y = marge_y;
  }
  
  /**
   * @return hauteur
   */
  public float getHauteur() {
    return hauteur;
  }
  
  /**
   * @param hauteur hauteur à définir
   */
  public void setHauteur(float hauteur) {
    this.hauteur = hauteur;
  }
  
  /**
   * @return largeur
   */
  public float getLargeur() {
    return largeur;
  }
  
  /**
   * @param largeur largeur à définir
   */
  public void setLargeur(float largeur) {
    this.largeur = largeur;
  }
  
  /**
   * Retourne une étiquette pour le Rad
   * @param dpi_aff
   * @param dpi_img
   * @return
   * 
   *         public GfxEtiquette getGfxEtiquette(int dpi_aff, int dpi_img, final GfxPageEditor planTravail)
   *         {
   *         GfxEtiquette etiquette = new GfxEtiquette(this, dpi_aff, dpi_img, planTravail);
   *         return etiquette;
   *         }
   */
  
}
