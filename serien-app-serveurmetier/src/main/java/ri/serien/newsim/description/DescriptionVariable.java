/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

/**
 * Description d'une variable
 */
public class DescriptionVariable {
  // Constante
  private static final char VAR_CHAR = '@';
  
  // Variables
  private static int compteur = 0;
  private String nom = null;
  private String variable = "";
  private String valeur = "";
  private boolean trimValeur = false;
  
  /**
   * Constructeur
   */
  public DescriptionVariable() {
    compteur++;
  }
  
  /**
   * @return the nom
   */
  public String getNom() {
    if (nom == null) {
      setNom("VAR_" + compteur);
    }
    return nom;
  }
  
  /**
   * Retourne le nom de la variable avec ou sans les @
   * @return the nom
   */
  public String getNom(boolean format) {
    if (format) {
      return getNom();
    }
    else {
      if (nom != null) {
        return nom.substring(1, nom.length() - 2);
      }
    }
    return nom;
  }
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    if ((nom != null) && (nom.trim().charAt(0) != VAR_CHAR)) {
      this.nom = VAR_CHAR + nom.trim().toUpperCase() + VAR_CHAR;
    }
    else {
      this.nom = nom;
    }
  }
  
  /**
   * @return the variable
   */
  public String getVariable() {
    return variable;
  }
  
  /**
   * @param variable the variable to set
   */
  public void setVariable(String variable) {
    this.variable = variable;
  }
  
  /**
   * @return the valeur
   */
  public String getValeur() {
    return valeur;
  }
  
  /**
   * @param valeur the valeur to set
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }
  
  /**
   * @return the trimValeur
   */
  public boolean isTrimValeur() {
    return trimValeur;
  }
  
  /**
   * @param trimValeur the trimValeur to set
   */
  public void setTrimValeur(boolean trimValeur) {
    this.trimValeur = trimValeur;
  }
}
