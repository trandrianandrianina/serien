/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

import java.awt.Color;

/**
 * Description d'une ligne de spool (pour étiquettes)
 */
public class DescriptionLabel extends DescriptionObject {
  // Constantes
  public static final int GAUCHE = 0;
  public static final int CENTRE = 1;
  public static final int DROITE = 2;
  public static final int HAUT = 1;
  public static final int BAS = 2;
  
  // Variables
  protected String texte = null;
  private boolean trimTexte = true;
  private int justificationHTexte = GAUCHE; // 0=Gauche 1=Centre 2=Droite
  private int justificationVTexte = CENTRE; // 0=Haut 1=Centre 2=Bas
  protected String nomPolice = "Monospaced";
  protected int stylePolice = 0; // 0=Plain 1=Bold 2=Italic
  protected float taillePolice = 10f;
  private int orientationTexte = 0; // 0=Horizontal 1=Vertical
  private Color foreGroundColor = Color.BLACK;
  private int lineHeight = 10; // En pourcentage
  
  /**
   * @return the nomPolice
   */
  public String getNomPolice() {
    return nomPolice;
  }
  
  /**
   * @param nomPolice the nomPolice to set
   */
  public void setNomPolice(String nomPolice) {
    this.nomPolice = nomPolice;
  }
  
  /**
   * @return the stylePolice
   */
  public int getStylePolice() {
    return stylePolice;
  }
  
  /**
   * @param stylePolice the stylePolice to set
   */
  public void setStylePolice(int stylePolice) {
    this.stylePolice = stylePolice;
  }
  
  /**
   * @return the taillePolice
   */
  public float getTaillePolice() {
    return taillePolice;
  }
  
  /**
   * @param taillePolice the taillePolice to set
   */
  public void setTaillePolice(float taillePolice) {
    this.taillePolice = taillePolice;
  }
  
  /**
   * @return the foreGroundColor
   */
  public Color getForeGroundColor() {
    return foreGroundColor;
  }
  
  /**
   * @param foreGroundColor the foreGroundColor to set
   */
  public void setForeGroundColor(Color foreGroundColor) {
    this.foreGroundColor = foreGroundColor;
  }
  
  /**
   * @return texte
   */
  public String getTexte() {
    if (trimTexte) {
      return texte == null ? texte : texte.trim();
    }
    else {
      return texte;
    }
  }
  
  /**
   * @param texte texte à définir
   */
  public void setTexte(String texte) {
    this.texte = texte;
  }
  
  /**
   * @return justificationHTexte
   */
  public int getJustificationHTexte() {
    return justificationHTexte;
  }
  
  /**
   * @param justificationHTexte justificationHTexte à définir
   */
  public void setJustificationHTexte(int justificationHTexte) {
    this.justificationHTexte = justificationHTexte;
  }
  
  /**
   * @return justificationVTexte
   */
  public int getJustificationVTexte() {
    return justificationVTexte;
  }
  
  /**
   * @param justificationVTexte justificationVTexte à définir
   */
  public void setJustificationVTexte(int justificationVTexte) {
    this.justificationVTexte = justificationVTexte;
  }
  
  public void setTrimTexte(boolean trimTexte) {
    this.trimTexte = trimTexte;
  }
  
  public boolean isTrimTexte() {
    return trimTexte;
  }
  
  /**
   * @param orientationTexte the orientationTexte to set
   */
  public void setOrientationTexte(int orientationTexte) {
    this.orientationTexte = orientationTexte;
  }
  
  /**
   * @return the orientationTexte
   */
  public int getOrientationTexte() {
    return orientationTexte;
  }
  
  /**
   * @param lineHeight the lineHeight to set
   */
  public void setLineHeight(int lineHeight) {
    this.lineHeight = lineHeight;
  }
  
  /**
   * @return the lineHeight
   */
  public int getLineHeight() {
    return lineHeight;
  }
  
  /**
   * Retourne un label pour le Rad
   * @param dpi_aff
   * @param dpi_img
   * @return
   * 
   *         public rad.composants.GfxLabel getGfxLabel(int dpi_aff, int dpi_img, final rad.PageEditor.GfxPageEditor
   *         planTravail)
   *         {
   *         rad.composants.GfxLabel label = new rad.composants.GfxLabel(this, dpi_aff, dpi_img, planTravail);
   *         return label;
   *         }
   */
  
}
