/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.newsim.description;

import ri.serien.newsim.rendu.InterpreterData;

/**
 * Description d'une condition (pour les objets label & image)
 */
public class DescriptionCondition {
  // Constantes
  public static final int EQUALS = 0;
  public static final int STARTWITH = 1;
  public static final int ENDWITH = 2;
  public static final int CONTAINS = 3;
  public static final int NOT_EQUALS = 4;
  public static final int NOT_STARTWITH = 5;
  public static final int NOT_ENDWITH = 6;
  public static final int NOT_CONTAINS = 7;
  
  // Variables
  private static int compteur = 0;
  private String nom = null;
  private String variable = "";
  private String valeur = "";
  private boolean distinctionMAJmin = true;
  private boolean trimVariable = false;
  private boolean trimValeur = false;
  private int operation = EQUALS;
  private boolean resultat = false;
  
  /**
   * Constructeur.
   */
  public DescriptionCondition() {
    compteur++;
  }
  
  /**
   * @return the nom
   */
  public String getNom() {
    if (nom == null) {
      setNom("COND_" + compteur);
    }
    return nom;
  }
  
  /**
   * @param nom the nom to set
   */
  public void setNom(String nom) {
    this.nom = nom;
  }
  
  /**
   * @return the variable
   */
  public String getVariable() {
    return variable;
  }
  
  /**
   * @param variable the variable to set
   */
  public void setVariable(String variable) {
    this.variable = variable;
  }
  
  /**
   * @return the valeur
   */
  public String getValeur() {
    return valeur;
  }
  
  /**
   * @param valeur the valeur to set
   */
  public void setValeur(String valeur) {
    this.valeur = valeur;
  }
  
  /**
   * @return the distinctionMAJmin
   */
  public boolean isDistinctionMAJmin() {
    return distinctionMAJmin;
  }
  
  /**
   * @param distinctionMAJmin the distinctionMAJmin to set
   */
  public void setDistinctionMAJmin(boolean distinctionMAJmin) {
    this.distinctionMAJmin = distinctionMAJmin;
  }
  
  /**
   * @return the trimVariable
   */
  public boolean isTrimVariable() {
    return trimVariable;
  }
  
  /**
   * @param trimVariable the trimVariable to set
   */
  public void setTrimVariable(boolean trimVariable) {
    this.trimVariable = trimVariable;
  }
  
  /**
   * @return the trimValeur
   */
  public boolean isTrimValeur() {
    return trimValeur;
  }
  
  /**
   * @param trimValeur the trimValeur to set
   */
  public void setTrimValeur(boolean trimValeur) {
    this.trimValeur = trimValeur;
  }
  
  /**
   * @return the operation
   */
  public int getOperation() {
    return operation;
  }
  
  /**
   * @param operation the operation to set
   */
  public void setOperation(int operation) {
    this.operation = operation;
  }
  
  /**
   * Retourne la traduction littérale de la condition
   * @return
   */
  public String getLibelleCondition() {
    StringBuffer chaine = new StringBuffer();
    chaine.append('[').append(getNom()).append("] ").append(variable);
    switch (operation) {
      case EQUALS:
        chaine.append(" est à ");
        break;
      case NOT_EQUALS:
        chaine.append(" n'est pas à ");
        break;
      case CONTAINS:
        chaine.append(" contient ");
        break;
      case NOT_CONTAINS:
        chaine.append(" ne contient pas ");
        break;
      case STARTWITH:
        chaine.append(" commence par ");
        break;
      case NOT_STARTWITH:
        chaine.append(" ne commence pas par ");
        break;
      case ENDWITH:
        chaine.append(" fini par ");
        break;
      case NOT_ENDWITH:
        chaine.append(" ne fini pas par ");
        break;
    }
    chaine.append('\'').append(valeur).append('\'');
    
    return chaine.toString();
  }
  
  /**
   * Analyse les conditions (à améliorer car contient pleins d'erreurs...)
   * @param resultat the resultat to set
   */
  public void setResultat(InterpreterData id) {
    resultat = false;
    if (id == null) {
      return;
    }
    String chaine = id.analyseExpression(variable); // <- TODO utiliser analyseExpressionWOTrim
    if (chaine == null) {
      return;
    }
    if (trimValeur) {
      chaine = chaine.trim();
    }
    if (!distinctionMAJmin) {
      chaine = chaine.toLowerCase();
    }
    switch (operation) {
      case EQUALS:
        if (distinctionMAJmin) {
          resultat = chaine.equals(valeur);
        }
        else {
          resultat = chaine.equalsIgnoreCase(valeur);
        }
        break;
      case NOT_EQUALS:
        if (distinctionMAJmin) {
          resultat = !chaine.equals(valeur);
        }
        else {
          resultat = !chaine.equalsIgnoreCase(valeur);
        }
        break;
      case CONTAINS:
        resultat = chaine.indexOf(valeur) != -1;
        break;
      case NOT_CONTAINS:
        resultat = chaine.indexOf(valeur) > -1; // <- TODO Faux mettre == à la place
        break;
      case STARTWITH:
        resultat = chaine.startsWith(valeur);
        break;
      case NOT_STARTWITH:
        resultat = !chaine.startsWith(valeur);
        break;
      case ENDWITH:
        resultat = chaine.endsWith(valeur);
        break;
      case NOT_ENDWITH:
        resultat = !chaine.endsWith(valeur);
        break;
    }
  }
  
  /**
   * @return the resultat
   */
  public boolean isResultat() {
    return resultat;
  }
}
