/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.crm.LAC_ActionCommerciale;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class Planning
 */
public class Planning extends HttpServlet {
  
  private LAC_ActionCommerciale gestionAC = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Planning() {
    super();
    gestionAC = new LAC_ActionCommerciale();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      if (isConnected(request, response)) {
        initialization(request);
        
        gestionAC.setUtilisateur((Utilisateur) request.getSession().getAttribute("utilisateur"));
        
        // On récupère la liste d'évènements
        request.setAttribute("listeActions", gestionAC.getListeActionsCommerciales(true));
        afficheListeActionCom(request, response);
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Vérifie que l'on est bien connecté
   * @param request
   * @param response
   * @return
   * @throws ServletException
   * @throws IOException
   */
  private boolean isConnected(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Object user = request.getSession().getAttribute("utilisateur");
    
    // Si on n'est pas connecté
    if ((user == null) || !(user instanceof Utilisateur)) {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialisation lors de la première utilisation
   * @param request
   */
  private void initialization(HttpServletRequest request) {
    // On passe l'utilisateur à la JSP pour le look générique
    request.setAttribute("utilisateur", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getLogin());
    request.setAttribute("bibli", ((Utilisateur) request.getSession().getAttribute("utilisateur")).getBaseDeDonnees());
    // Option CRM sélectionné par l'utilisateur
    // request.setAttribute("optionsCRM", "2");
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des actions commerciales
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  // private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response, String client, String
  // suffixe) throws ServletException, IOException
  private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "2");
    request.getSession().setAttribute("filRouge", "1");
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/planning.jsp").forward(request, response);
  }
  
}
