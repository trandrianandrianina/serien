/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.dao.exp.programs.contact.M_Contact;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class Contact
 */
public class Contact extends HttpServlet {
  
  // Constantes
  public static final int LIST = 10;
  
  // Variables
  private Utilisateur user = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public Contact() {
    super();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      if (!isConnected(request, response)) {
        return;
      }
      initialization(request);
      
      // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
      if (request.getParameter("action2do") != null) {
        String action2do = request.getParameter("action2do");
        String idcontact = request.getParameter("idcontact");
        int id = (idcontact == null) ? 0 : Integer.parseInt(idcontact);
        action2Do(Integer.parseInt(action2do.trim()), id, request, response);
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      if (isConnected(request, response)) {
        initialization(request);
        
        // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
        if (request.getParameter("action2do") != null) {
          String action2do = request.getParameter("action2do");
          String idcontact = request.getParameter("idcontact");
          int id = (idcontact == null) ? 0 : Integer.parseInt(idcontact);
          action2Do(Integer.parseInt(action2do.trim()), id, request, response);
        }
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Gestion des actions en fonction de la demande
   * @param action2do
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void action2Do(int action2do, int idcontact, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    switch (action2do) {
      case GestionFormulaire.CONSULTATION:
        user.getFormulaireCourant().activeConsultation();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.CREATION:
        user.getFormulaireCourant().activeCreation();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.MODIFICATION:
        user.getFormulaireCourant().activeModification();
        request.setAttribute("unContact", user.getGestionAC().retournerUnContact(idcontact));
        request.setAttribute("formulaire", user.getFormulaireCourant());
        afficheDetailContact(request, response);
        break;
      case GestionFormulaire.SUPPRESSION:
        user.getFormulaireCourant().activeConsultation();
        // Supprime l'action
        user.getGestionAC().supprimeContact(user.getGestionAC().getCCourant());
        // On réaffiche la liste des contacts
        listContact(request, response);
        break;
      case GestionFormulaire.ANNULATION:
        user.getFormulaireCourant().activeConsultation();
        // On réaffiche la liste des contacts
        listContact(request, response);
        break;
      case GestionFormulaire.VALIDATION:
        if (isValidatedData(request, user.getGestionAC().getCCourant(idcontact))) {
          if (idcontact == 0) { // Il s'agit d'un ajout
            if (!user.getGestionAC().insertContact(user.getGestionAC().getCCourant())) {
              // Affichage du formulaire
              afficheDetailContact(request, response);
            }
            else {
              user.getFormulaireCourant().activeConsultation();
              
              // On récupère la liste des contacts
              listContact(request, response);
            }
          }
          else { // Il s'agit d'une modification
            if (user.getGestionAC().updateContact(user.getGestionAC().getCCourant(idcontact))) {
              user.getFormulaireCourant().activeConsultation();
            }
            // On réaffiche le contact en mode consultation
            request.setAttribute("unContact", user.getGestionAC().getCCourant());
            request.setAttribute("formulaire", user.getFormulaireCourant());
            
            // Affichage du formulaire
            afficheDetailContact(request, response);
          }
        }
        break;
      
      case LIST:
        // Liste des contacts pour un client
        listContact(request, response);
        break;
    }
  }
  
  /**
   * Affiche la page avec la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void listContact(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("listeContacts", user.getGestionAC().getListeContactsClient4List(request.getParameter("idclient"),
        request.getParameter("clsuf"), receiveCookie(request, "/", "selectionContact")));
    request.setAttribute("urlRetour", receiveCookie(request, "/", "urlRetour"));
    afficheListeContacts(request, response);
  }
  
  /**
   * Contrôle la validité des données du formulaire
   * @param request
   * @param actioncom
   * @return
   */
  private boolean isValidatedData(HttpServletRequest request, M_Contact contact) {
    // Contrôle de la civilité
    String valeur = request.getParameter("c_civcontact");
    int indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < contact.getListCodeCivility().length) {
        contact.setRECIV(contact.getListCodeCivility()[indice]);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    
    // Contrôle du nom
    valeur = request.getParameter("c_namecontact");
    if ((valeur != null) && (valeur.trim().length() > 0)) {
      contact.setRENOM(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle du prénom
    valeur = request.getParameter("c_firstnamecontact");
    if (valeur != null) {
      contact.setREPRE(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle de la catégorie
    valeur = request.getParameter("c_catcontact");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < contact.getListCodeClass().length) {
        contact.setRECAT(contact.getListCodeClass()[indice]);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    
    // Contrôle du téléphone
    valeur = request.getParameter("c_tel1contact");
    if (valeur != null) {
      contact.setRETEL(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle du téléphone 2
    valeur = request.getParameter("c_tel2contact");
    if (valeur != null) {
      contact.setRETEL2(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle du fax
    valeur = request.getParameter("c_faxcontact");
    if (valeur != null) {
      contact.setREFAX(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle du mail 1
    valeur = request.getParameter("c_mail1contact");
    if (valeur != null) {
      contact.setRENET(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle du mail 2
    valeur = request.getParameter("c_mail2contact");
    if (valeur != null) {
      contact.setRENET2(valeur);
    }
    else {
      return false;
    }
    
    return true;
  }
  
  /**
   * Lecture des cookies sur le poste client
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private String receiveCookie(HttpServletRequest request, String path, String namecookie) throws ServletException, IOException {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        if (cookie.getName().equals(namecookie)) {
          return cookie.getValue();
        }
      }
    }
    return "";
  }
  
  /**
   * Vérifie que l'on est bien connecté
   * @param request
   * @param response
   * @return
   * @throws ServletException
   * @throws IOException
   */
  private boolean isConnected(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Object user = request.getSession().getAttribute("utilisateur");
    
    // Si on n'est pas connecté
    if ((user == null) || !(user instanceof Utilisateur)) {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialisation lors de la première utilisation
   * @param request
   */
  private void initialization(HttpServletRequest request) {
    // On passe l'utilisateur à la JSP pour le look générique
    user = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    
    request.setAttribute("utilisateur", user.getLogin());
    request.setAttribute("bibli", user.getBaseDeDonnees());
    // Option CRM sélectionné par l'utilisateur
    // request.setAttribute("optionsCRM", "1");
    
    user.getGestionAC().setUtilisateur((Utilisateur) request.getSession().getAttribute("utilisateur"));
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheListeContacts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'état du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    // request.setAttribute("moteurRecherche", "ActionsCom");
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "5");
    request.getSession().setAttribute("filRouge", "5");
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeContacts.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des contacts
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheDetailContact(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'état du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    
    // l'URL de provenance
    request.setAttribute("urlRetour", receiveCookie(request, "/", "urlRetour"));
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "5");
    request.getSession().setAttribute("filRouge", "5");
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/unContact.jsp").forward(request, response);
  }
  
}
