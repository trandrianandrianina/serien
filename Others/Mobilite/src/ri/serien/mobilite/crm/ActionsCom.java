/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.crm;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.dao.exp.programs.contact.M_EvenementContact;
import ri.serien.libas400.dao.gvm.programs.actioncommerciale.M_ActionCommerciale;
import ri.serien.libcommun.outils.Trace;
import ri.serien.libcommun.outils.dateheure.ConvertDate;
import ri.serien.libcommun.outils.dateheure.TimeOperations;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class ActionsCom
 */
public class ActionsCom extends HttpServlet {
  
  // Variables
  private TimeOperations timeOperations = new TimeOperations();
  private Utilisateur user = null;
  private boolean firstTimeInModif = true;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public ActionsCom() {
    super();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      if (!isConnected(request, response)) {
        return;
      }
      initialization(request);
      
      // Si on a choisi une action à effectuer sur une action en particulier
      if (request.getParameter("idaction") != null) {
        String action2do = request.getParameter("action2do");
        String idAction = request.getParameter("idaction");
        action2Do(action2do, idAction, request, response);
      }
      // Si on a choisi un client et donc on affiche la liste des actions commerciales qui le concerne
      else if (request.getParameter("client") != null && request.getParameter("suffixe") != null) {
        // On créé un cookie avec l'url pour pouvoir revenir avec le bouton Back
        sendCookie(response, "/", "urlRetour", "ActionsCom");
        
        // On met à jour le client courant
        user.getGestionAC().setClCourant(request.getParameter("client"), request.getParameter("suffixe"));
        
        // On récupère la liste des actions commerciales
        request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
        request.setAttribute("mclient", user.getGestionAC().getClCourant());
        
        // Affichage du formulaire
        afficheListeActionCom(request, response);
      }
      // Sinon on affiche la liste des clients
      else {
        // On récupère la liste des clients
        request.setAttribute("listeClients",
            user.getGestionAC().getListeClients(request.getParameter("tri"), request.getParameter("recherche")));
        
        // Affichage du formulaire
        afficheListeClients(request, response);
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      if (isConnected(request, response)) {
        initialization(request);
        
        // On effectue une recherche
        if (request.getParameter("recherche") != null) {
          request.setAttribute("moteurRecherche", "ActionsCom");
          
          // On récupère la liste des clients
          request.setAttribute("listeClients",
              user.getGestionAC().getListeClients(request.getParameter("tri"), request.getParameter("recherche")));
          
          // Affichage du formulaire
          afficheListeClients(request, response);
        }
        else {
          String action2do = request.getParameter("action2do");
          String idActionCom = request.getParameter("idaction");
          // Contrôle des paramètres
          action2Do(action2do, idActionCom, request, response);
        }
      }
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  // -- Méthodes privées ----------------------------------------------------
  
  /**
   * Contrôle la validité des données du formulaire
   * @param request
   * @param actioncom
   * @return
   */
  private boolean isValidatedData(HttpServletRequest request, M_ActionCommerciale actioncom) {
    
    // Contrôle de l'objet
    String valeur = request.getParameter("ac_objetaction");
    int indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < actioncom.getListCodeObjectAction().length) {
        actioncom.setACOBJ(actioncom.getListCodeObjectAction()[indice]);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    
    // Contrôle de l'état
    valeur = request.getParameter("ac_etataction");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < M_EvenementContact.getListState().length) {
        actioncom.getEvenementContact().setETETA((byte) indice);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    
    // Contrôle de l'état
    valeur = request.getParameter("ac_prioriteaction");
    indice = 0;
    if (ConvertDate.isNumeric(valeur)) {
      indice = Integer.parseInt(valeur.trim());
      if (indice < M_EvenementContact.getListPriority().length) {
        actioncom.getEvenementContact().setETCODP((byte) indice);
      }
      else {
        return false;
      }
    }
    else {
      return false;
    }
    
    // Contrôle de l'observation
    valeur = request.getParameter("ac_obsaction");
    if (valeur != null) {
      actioncom.getEvenementContact().setETOBS(valeur);
      
    }
    else {
      return false;
    }
    
    // Contrôle du temps passé
    valeur = request.getParameter("ac_tpspasseaction");
    if (timeOperations.validOperation(valeur)) {
      actioncom.getEvenementContact().setOperationETTOTP(valeur);
    }
    else {
      return false;
    }
    
    // Contrôle de la date de création
    valeur = request.getParameter("ac_dateCreation");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETDCR(indice);
    
    // Contrôle de l'heure de création
    valeur = request.getParameter("ac_heureCreation");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETHCR(indice);
    
    // Contrôle de la date de cloture
    valeur = request.getParameter("ac_dateLimite");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETDCL(indice);
    
    // Contrôle de l'heure de cloture
    valeur = request.getParameter("ac_heureLimite");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETHCL(indice);
    
    // Contrôle de la date de réalisation
    valeur = request.getParameter("ac_dateRealisation");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETDRL(indice);
    
    // Contrôle de l'heure de réalisation
    valeur = request.getParameter("ac_heureRealisation");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETHRL(indice);
    
    // Contrôle de la date de rappel
    valeur = request.getParameter("ac_dateRappel");
    indice = ConvertDate.convertDate4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETDRP(indice);
    
    // Contrôle de l'heure de réalisation
    valeur = request.getParameter("ac_heureRappel");
    indice = ConvertDate.convertHeure4DataBase(valeur.trim());
    if (indice == -1) {
      return false;
    }
    actioncom.getEvenementContact().setETHRP(indice);
    
    return true;
  }
  
  /**
   * Vérifie que l'on est bien connecté
   * @param request
   * @param response
   * @return
   * @throws ServletException
   * @throws IOException
   */
  private boolean isConnected(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    Object user = request.getSession().getAttribute("utilisateur");
    
    // Si on n'est pas connecté
    if ((user == null) || !(user instanceof Utilisateur)) {
      request.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      return false;
    }
    
    return true;
  }
  
  /**
   * Initialisation lors de la première utilisation
   * @param request
   */
  private void initialization(HttpServletRequest request) {
    // On passe l'utilisateur à la JSP pour le look générique
    user = ((Utilisateur) request.getSession().getAttribute("utilisateur"));
    
    request.setAttribute("utilisateur", user.getLogin());
    request.setAttribute("bibli", user.getBaseDeDonnees());
    
    user.getGestionAC().setUtilisateur((Utilisateur) request.getSession().getAttribute("utilisateur"));
  }
  
  /**
   * Gestion des actions à exécuter
   * @param action2do
   * @param idActionCom
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void action2Do(String action2do, String idActionCom, HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    M_ActionCommerciale actioncom = null;
    if (idActionCom == null) {
      idActionCom = "-1";
    }
    int idAction = Integer.parseInt(idActionCom);
    actioncom = user.getGestionAC().getAcCourante(idAction);
    // if( receiveCookie(request, "/", "selectionContact") == null)
    // if( firstTimeInModif )
    // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
    
    // LE ZERO c'est pour corriver vite fait le urlRetour qui renvoie un zéro dans l'action2do
    if (action2do == null || action2do.equals("0")) {
      actioncom = user.getGestionAC().retournerUneActionCom(idAction);
      
      // On définit l'état du formulaire
      if (actioncom.getACID() == 0) {
        user.getFormulaireCourant().activeCreation();
        user.getGestionAC().setContactsLies(actioncom.getListContact(), "");
        // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      }
      else {
        user.getFormulaireCourant().activeConsultation();
        // sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      }
      sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
      request.setAttribute("uneAction", actioncom);
      // Affichage du formulaire
      afficheDetailActionCom(request, response);
    }
    // En fonction de l'action à effectuer
    else {
      request.setAttribute("action2Do", action2do);
      switch (Integer.parseInt(action2do)) {
        
        case GestionFormulaire.CREATION:
          user.getFormulaireCourant().activeCreation();
          // Gestion des contacts sélectionnés
          user.getGestionAC().setContactsLies(actioncom.getListContact(), receiveCookie(request, "/", "selectionContact"));
          sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
          // On récupère l'action commerciale souhaitée
          request.setAttribute("uneAction", actioncom);
          // Affichage formulaire
          afficheDetailActionCom(request, response);
          break;
        
        // Supprimer une action commerciale
        case GestionFormulaire.SUPPRESSION:
          user.getFormulaireCourant().activeConsultation();
          // Supprime l'action
          user.getGestionAC().supprimeActionCom(idAction);
          // On récupère la liste des actions commerciales
          request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
          // Affichage du formulaire
          afficheListeActionCom(request, response);
          
          break;
        
        // On annule une action commerciale (ne veux pas dire supprimée, juste on annule les modif en cours)
        case GestionFormulaire.ANNULATION:
          user.getFormulaireCourant().activeConsultation();
          // Annulation de l'action
          sendCookie(response, "/", "selectionContact", "");
          
          // On récupère la liste des actions commerciales
          request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
          // Affichage du formulaire
          afficheListeActionCom(request, response);
          
          break;
        
        // Afficher le formulaire du détail d'une action commerciale en modification
        case GestionFormulaire.MODIFICATION:
          user.getFormulaireCourant().activeModification();
          // Gestion des contacts sélectionnés
          if (request.getParameter("annulModifsContact") != null) {
            sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
          }
          else {
            user.getGestionAC().setContactsLies(actioncom.getListContact(), receiveCookie(request, "/", "selectionContact"));
          }
          
          // On récupère l'action commerciale souhaitée
          request.setAttribute("uneAction", actioncom);
          // Affichage formulaire
          afficheDetailActionCom(request, response);
          firstTimeInModif = false;
          break;
        
        // Ajout ou modification d'une action commerciale
        case GestionFormulaire.VALIDATION:
          if (isValidatedData(request, actioncom)) {
            if (idAction == 0) { // Il s'agit d'un ajout
              if (!user.getGestionAC().insertActionCommerciale(actioncom)) {
                // Affichage du formulaire
                afficheDetailActionCom(request, response);
              }
              else {
                user.getFormulaireCourant().activeConsultation();
                
                // On récupère la liste des actions commerciales
                request.setAttribute("listeActions", user.getGestionAC().getListeActionsCommerciales(false));
                
                // Affichage de la liste des actions
                afficheListeActionCom(request, response);
              }
            }
            else { // Il s'agit d'une modification
              if (user.getGestionAC().updateActionCommerciale(actioncom)) {
                sendCookie(response, "/", "selectionContact", user.getGestionAC().getValueCookie4ContactsLies());
                user.getFormulaireCourant().activeConsultation();
              }
              // On récupère l'action commerciale souhaitée
              request.setAttribute("uneAction", actioncom);
              
              // Affichage du formulaire
              afficheDetailActionCom(request, response);
            }
          }
          break;
      }
    }
  }
  
  /**
   * Lecture des cookies sur le poste client
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private String receiveCookie(HttpServletRequest request, String path, String namecookie) {
    Cookie[] cookies = request.getCookies();
    if (cookies != null) {
      for (Cookie cookie : cookies) {
        // if( cookie.getPath() == null ) continue;
        if (cookie.getName().equals(namecookie)) { // && cookie.getPath().equals( path )){
          return cookie.getValue();
        }
      }
    }
    return "";
  }
  
  /**
   * On envoi un cookie au client
   * Pour le vider il suffit d'envoyer une chaine vide
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void sendCookie(HttpServletResponse response, String path, String namecookie, String valuecookie) {
    Cookie cookie = new Cookie(namecookie, valuecookie);
    // cookie.setPath(path);
    cookie.setValue(valuecookie);
    response.addCookie(cookie);
  }
  
  /**
   * Supprime un cookie
   * Pour le vider il suffit d'envoyer une chaine vide
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void deleteCookie(HttpServletResponse response, String path, String namecookie) {
    Cookie cookie = new Cookie(namecookie, "");
    cookie.setPath(path);
    cookie.setMaxAge(0);
    response.addCookie(cookie);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des clients
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheListeClients(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("moteurRecherche", "ActionsCom");
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "1");
    request.getSession().setAttribute("filRouge", "1");
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeClientsCom.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire de la liste des actions commerciales
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  // private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response, String client, String
  // suffixe) throws ServletException, IOException
  private void afficheListeActionCom(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    request.setAttribute("mclient", user.getGestionAC().getClCourant());
    
    // Niveau du fil rouge dans la navigation
    request.getSession().setAttribute("optionsCRM", "1");
    request.getSession().setAttribute("filRouge", "1");
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/listes.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/listeActionsCom.jsp").forward(request, response);
  }
  
  /**
   * Initialisations diverses et appel du formulaire d'une action commerciale
   * @param request
   * @param response
   * @throws ServletException
   * @throws IOException
   */
  private void afficheDetailActionCom(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    // L'état du formulaire courant
    request.setAttribute("formulaire", user.getFormulaireCourant());
    
    // Si le formulaire est en consultation on vide le cookie selectionContact
    if (user.getFormulaireCourant().isConsultation()) {
      // sendCookie(response, "/", "selectionContact", null);
      deleteCookie(response, "/", "selectionContact");
    }
    // On créé un cookie avec l'url pour pouvoir revenir
    sendCookie(response, "/", "urlRetour", "ActionsCom?idaction=" + user.getGestionAC().getAcCourante().getACID() + "&action2do="
        + user.getFormulaireCourant().getModeFormulaire());
    
    // Niveau du fil rouge dans la navigation
    String optionsCRM = (String) request.getSession().getAttribute("optionsCRM");
    // String filrouge = (String) request.getSession().getAttribute("filRouge");
    if (optionsCRM.equals("1")) {
      request.getSession().setAttribute("filRouge", "2");
    }
    else if (optionsCRM.equals("2")) {
      request.getSession().setAttribute("optionsCRM", "2");
      request.getSession().setAttribute("filRouge", "2");
    }
    
    // CSS spécifique liste
    request.setAttribute("cssSpecifique", "css/fiches.css");
    // Récupération du dispatcher + forward JSP
    request.getRequestDispatcher("WEB-INF/crm/uneActionCom.jsp").forward(request, response);
  }
}
