/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.outils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;

public class TriGenericRecords {
  
  private ArrayList<GenericRecord> listeRecords = null;
  private String cleRecherche = null;
  
  public TriGenericRecords(ArrayList<GenericRecord> liste) {
    listeRecords = liste;
  }
  
  public ArrayList<GenericRecord> ascendant(String cle) {
    if (cle == null) {
      return null;
    }
    cleRecherche = cle;
    Collections.sort(listeRecords, new Triliste(cleRecherche));
    
    return listeRecords;
  }
  
  public ArrayList<GenericRecord> descendant(String cle) {
    if (cle == null) {
      return null;
    }
    listeRecords = ascendant(cle);
    Collections.reverse(listeRecords);
    
    return listeRecords;
  }
  
}

/*
 *  Crée une classe de comparaison de GenericRecords qui va permettre de les trier.
 */
class Triliste implements Comparator<GenericRecord> {
  
  private String cleRecherche = null;
  
  public Triliste(String clef) {
    cleRecherche = clef;
  }
  
  @Override
  public int compare(GenericRecord arg0, GenericRecord arg1) {
    try {
      // si les deux valeurs existent on compare
      if (arg0.isPresentField(cleRecherche) && arg1.isPresentField(cleRecherche)) {
        // en mode BigDecimal
        if (arg0.isPresentField(cleRecherche) && arg0.getField(cleRecherche) instanceof BigDecimal) {
          return (arg0.getDecimal(cleRecherche)).compareTo(arg1.getDecimal(cleRecherche));
          // sinon on se fout en String huhuhu....
        }
        else {
          return (arg0.getString(cleRecherche)).compareTo(arg1.getString(cleRecherche));
        }
      }
      // sinon si c'est la valeur à comparer qui est vide
      else if (!arg0.isPresentField(cleRecherche)) {
        return -1;
      }
      else {
        return 1;
      }
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur de comparaison");
      return -1;
    }
  }
}
