/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;

import org.apache.commons.net.ftp.FTP;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.MonFTP;

public class GestionBOversion {
  private Utilisateur utilisateur = null;
  private int versionWAR = 0;
  private int versionBDD = 0;
  private MonFTP objetFTP = null;
  private File fichierWAR = null;
  
  public GestionBOversion(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  // Controler la version dans son ensemble : WAR et BDD
  protected int controlerVersionGLobale() {
    int pb = 0;
    // controler le WAR
    if (controlerVersionWAR() > 0) {
      pb++;
    }
    else if (controlerVersionBDD() == 0) {
      pb += 2;
    }
    
    return pb;
  }
  
  // Va vérifier la version du WAR sur le site de téléchargement
  protected int controlerVersionWAR() {
    objetFTP = new MonFTP();
    if (objetFTP.seConnecter(ConstantesEnvironnement.SERVEUR_FTP, ConstantesEnvironnement.CLIENT_FTP, ConstantesEnvironnement.MP_FTP)) {
      int versionDispo = 0;
      int versionMax = verifierVersionWAR();
      
      try {
        String[] dossiers = objetFTP.getMaConnexionFTP().listNames();
        
        for (int i = 0; i < dossiers.length; i++) {
          
          try {
            versionDispo = Integer.parseInt(dossiers[i]);
            if (versionDispo > versionMax) {
              versionMax = versionDispo;
            }
          }
          catch (Exception e) {
            e.printStackTrace();
          }
        }
      }
      catch (IOException e) {
        e.printStackTrace();
        return 0;
      }
      
      objetFTP.seDeconnecter();
      
      if (versionMax > versionWAR) {
        return versionMax;
      }
      else {
        return 0;
      }
      
    }
    else {
      return 0;
    }
  }
  
  /**
   * Contrôler si la version de la BDD SQLite est identique à celle du WAR.
   * @return 0=versions différentes, 1=versions identiques.
   */
  public int controlerVersionBDD() {
    // Récupérer les versions des WAR et la BDD
    int versionWAR = verifierVersionWAR();
    int versionBDD = verifierVersionBDD();
    
    // Retourner 0 si les versions ne sont pas égales
    if (versionBDD != versionWAR) {
      Trace.info("Les versions entre le WAR et la BDD SQLite sont différentes : versionWAR=" + versionWAR + ", versionBDD=" + versionBDD);
      return 0;
    }
    
    // Retourner 1 si les versions sont égales
    return 1;
  }
  
  // Va vérifier la version du WAR avant de l'attribuer à versionWAR
  public int verifierVersionWAR() {
    versionWAR = ConstantesEnvironnement.versionActuelle;
    return versionWAR;
  }
  
  // Va vérifier la version du WAR avant de l'attribuer à versionWAR
  public int verifierVersionBDD() {
    versionBDD = utilisateur.getBaseSQLITE().recupererVersionBDD();
    return versionBDD;
  }
  
  /**
   * Télécharger la version du serveur de téléchargement vers le serveur local SérieMobile
   */
  protected boolean telechargerVersion() {
    boolean okTransfertFTP = false;
    
    versionWAR = controlerVersionWAR();
    if (versionWAR != 0) {
      if (objetFTP == null) {
        objetFTP = new MonFTP();
      }
      
      if (objetFTP.seConnecter(ConstantesEnvironnement.SERVEUR_FTP, ConstantesEnvironnement.CLIENT_FTP, ConstantesEnvironnement.MP_FTP)) {
        try {
          objetFTP.getMaConnexionFTP().enterLocalPassiveMode();
          
          objetFTP.getMaConnexionFTP().setFileType(FTP.BINARY_FILE_TYPE);
          objetFTP.getMaConnexionFTP().doCommand("NAM", " 1");
          if (objetFTP.getMaConnexionFTP().changeWorkingDirectory(ConstantesEnvironnement.DOSSIER_VERSIONS_FTP + "/" + versionWAR)) {
            if (objetFTP.downloadFile("SerieNmobilite.war",
                ConstantesEnvironnement.DOSSIER_DEPLOIE + ConstantesEnvironnement.DOSSIER_VERSION_AS + "SerieNmobilite.war")) {
              okTransfertFTP = true;
            }
          }
        }
        catch (IOException e) {
          e.printStackTrace();
        }
        
        objetFTP.seDeconnecter();
      }
    }
    return okTransfertFTP;
  }
  
  /**
   * En se basant sur le fichier de version, on récupère le numéro de versions des sources dans le fichier .txt
   * et on met à jour le numéro de version en mémoire dans VERSION_DEV
   */
  public boolean majNumeroDeVersionSources() {
    // Tester si le fichier version actuelle est présent
    File fichier = new File(ConstantesEnvironnement.getNomCompletVersionActuelle());
    if (!fichier.isFile()) {
      return false;
    }
    
    // Lire le numéro de version dans le fichier
    int numeroVersion = 0;
    try {
      // Ouvrir le fichier
      InputStreamReader inputStreamReader = new InputStreamReader(new FileInputStream(fichier));
      LineNumberReader lineNumberReader = new LineNumberReader(inputStreamReader);
      
      // Parcourir le fichier
      String complet = "";
      String ligne = null;
      while ((ligne = lineNumberReader.readLine()) != null) {
        complet += ligne;
      }
      
      // Fermer le fichier
      lineNumberReader.close();
      inputStreamReader.close();
      
      // Convertir le numéro de version
      numeroVersion = Integer.parseInt(complet.trim());
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur lors de la lecture du numéro de version actuelle dans le WAR.");
      return false;
    }
    
    // Mettre à jour le numéro de version
    ConstantesEnvironnement.versionActuelle = numeroVersion;
    Trace.info("Version WAR = " + numeroVersion);
    return true;
  }
  
  /**
   * Mettre à jour la base de données SQLite.
   * @param true=mise à jour réussie, false=échec.
   */
  public boolean majBDD() {
    // Sauvegarder la BDD originale
    if (!planquerLaBDD()) {
      Trace.erreur("Erreur lors de la sauvegarde de la BDD SQLite avant sa mise à jour");
      return false;
    }
    
    // Récupérer tous les ordres du SQL de maj
    ArrayList<String> listSQL = recupererLesOrdresSQL();
    if (listSQL == null) {
      Trace.erreur("Erreur lors de la récupération des ordres SQL pour la mise à jour de la base SQLite");
      return false;
    }
    
    // Exécuter les ordres SQL de mise à jour
    if (!envoyerLesOrdres(listSQL)) {
      Trace.erreur("Erreur lors de l'exécution des ordres SQL pour la mise à jour de la base SQLite");
      return false;
    }
    
    // Mettre à jour le numéro de version
    majNumeroDeVersionSources();
    return true;
  }
  
  /**
   * Envoyer les ordres à la base de donnée SQLITE
   */
  private boolean envoyerLesOrdres(ArrayList<String> lesOrdresSQL) {
    if (lesOrdresSQL == null) {
      return false;
    }
    boolean isOk = true;
    
    for (int i = 0; i < lesOrdresSQL.size(); i++) {
      if (!utilisateur.getBaseSQLITE().envoyerRequeteModif(lesOrdresSQL.get(i))) {
        Trace.erreur("ECHEC requete: " + lesOrdresSQL.get(i));
        isOk = false;
      }
      if (!isOk) {
        break;
      }
    }
    
    return isOk;
  }
  
  /**
   * Récupérer dans les SQL sélectionnés la liste des ordres SQL à passer
   */
  private ArrayList<String> recupererLesOrdresSQL() {
    ArrayList<String> listeDossiers = recupererVersionsIntermediaires();
    if (listeDossiers == null || listeDossiers.size() == 0) {
      Trace.erreur("Aucun dossier de trouvé pour la mise à jour de la version de la BDD SQLite");
      return null;
    }
    
    return retournerListeDesOrdres(listeDossiers);
  }
  
  /**
   * Retourner dans les SQL sélectionnés la liste des ordres SQL à passer : UN PEU REDONDANTE AVEC recupererLesOrdresSQL
   * TODO
   */
  private ArrayList<String> retournerListeDesOrdres(ArrayList<String> listeDossiers) {
    if (listeDossiers == null || listeDossiers.isEmpty()) {
      return null;
    }
    File fichierMAJ = null;
    InputStreamReader flog = null;
    LineNumberReader llog = null;
    String chaine = null;
    String contenu = null;
    
    ArrayList<String> listeOrdres = new ArrayList<String>();
    for (int i = 0; i < listeDossiers.size(); i++) {
      fichierMAJ = new File(ConstantesEnvironnement.getDossierVersion() + listeDossiers.get(i) + ConstantesEnvironnement.SEPARATEUR
          + ConstantesEnvironnement.FICHIER_MAJBDD);
      if (fichierMAJ.isFile()) {
        chaine = "";
        try {
          flog = new InputStreamReader(new FileInputStream(fichierMAJ));
          llog = new LineNumberReader(flog);
          while ((contenu = llog.readLine()) != null) {
            chaine += contenu;
          }
          llog.close();
          flog.close();
          
          String[] tabChaine = chaine.split(";;;");
          for (int j = 0; j < tabChaine.length; j++) {
            listeOrdres.add(tabChaine[j] + ";");
          }
          
        }
        catch (Exception e) {
          System.err.println("Error : " + e.getMessage());
        }
      }
    }
    
    return listeOrdres;
  }
  
  /**
   * Récupérer la liste des dossiers de versions intermédiaires entre la version actuelle et la dernière version
   */
  private ArrayList<String> recupererVersionsIntermediaires() {
    // Récupérer le répertoire contenant les patchs de versions
    File dossierVersions = new File(ConstantesEnvironnement.getDossierVersion());
    if (!dossierVersions.isDirectory()) {
      Trace.erreur("Le dossier versions est invalide : " + dossierVersions.getAbsolutePath());
      return null;
    }
    
    // Lister le contenu du dossier versions
    File[] listeVersions = dossierVersions.listFiles();
    if (listeVersions == null) {
      Trace.erreur("Le dossier versions est vide : " + dossierVersions.getAbsolutePath());
      return null;
    }
    
    // Récupérer les versions des WAR et de la BDD SQLite pour savoir quelles dossiers version récupérer
    versionBDD = verifierVersionBDD();
    versionWAR = verifierVersionWAR();
    
    // scanner tous les dossiers de version
    ArrayList<String> liste = new ArrayList<String>();
    for (int i = 0; i < listeVersions.length; i++) {
      if (listeVersions[i].isDirectory()) {
        try {
          // récupérer les versions qui sont disponibles entre l'ancienne version de BDD et la version souhaitée
          if (Integer.parseInt(listeVersions[i].getName()) > versionBDD && Integer.parseInt(listeVersions[i].getName()) <= versionWAR) {
            liste.add(listeVersions[i].getName());
          }
        }
        catch (Exception e) {
        }
      }
    }
    
    return liste;
  }
  
  /**
   * Sauvegarder la BDD actuelle dans un fichier bdd_mobilite<numeroVersion>.
   * @return
   */
  private boolean planquerLaBDD() {
    FileInputStream inputStreamSource = null;
    FileOutputStream inputStreamDestination = null;
    try {
      // Construire la source
      String cheminSource = ConstantesEnvironnement.getNomCompletFichierSQLite();
      File fileSource = new File(cheminSource);
      if (!fileSource.isFile()) {
        Trace.erreur("Le chemin du fichier SQLLite à sauvegarder est invalide : " + cheminSource);
      }
      inputStreamSource = new FileInputStream(fileSource);
      
      // Construire la destination
      String cheminDestination = ConstantesEnvironnement.getDossierSauvegardeSQLite() + ConstantesEnvironnement.getNomCourtFichierSQLite()
          + ConstantesEnvironnement.versionActuelle;
      inputStreamDestination = new FileOutputStream(cheminDestination);
      
      // Tracer la copie
      Trace.info("Sauvegarder la BDD SQLite : cheminSource=" + cheminSource + ", cheminDestination=" + cheminDestination);
      
      // Copier
      byte buffer[] = new byte[512 * 1024];
      int nblecture;
      while ((nblecture = inputStreamSource.read(buffer)) != -1) {
        inputStreamDestination.write(buffer, 0, nblecture);
      }
      
      return true;
    }
    catch (FileNotFoundException nf) {
      Trace.erreur(nf, "Erreur lors de la sauvegarde de la BDD SQLite");
    }
    catch (IOException io) {
      Trace.erreur(io, "Erreur lors de la sauvegarde de la BDD SQLite");
    }
    finally {
      try {
        if (inputStreamSource != null) {
          inputStreamSource.close();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de la sauvegarde de la BDD SQLite");
      }
      try {
        if (inputStreamDestination != null) {
          inputStreamDestination.close();
        }
      }
      catch (Exception e) {
        Trace.erreur(e, "Erreur lors de la sauvegarde de la BDD SQLite");
      }
    }
    
    return false;
  }
  
  // +++++++++++++++++++++++++++++++++++ ACCESSEURS ++++++++++++++++++++++++++++++++++++++
  
  public int getVersionWAR() {
    return versionWAR;
  }
  
  public void setVersionWAR(int versionWAR) {
    this.versionWAR = versionWAR;
  }
  
  public int getVersionBDD() {
    return versionBDD;
  }
  
  public void setVersionBDD(int versionBDD) {
    this.versionBDD = versionBDD;
  }
  
  public MonFTP getObjetFTP() {
    return objetFTP;
  }
  
  public void setObjetFTP(MonFTP objetFTP) {
    this.objetFTP = objetFTP;
  }
  
  public File getFichierWAR() {
    return fichierWAR;
  }
  
  public void setFichierWAR(File fichierWAR) {
    this.fichierWAR = fichierWAR;
  }
  
}
