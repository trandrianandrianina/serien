/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import ri.serien.mobilite.environnement.Utilisateur;

public class GestionBOlogs {
  private Utilisateur utilisateur = null;
  
  public GestionBOlogs(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
