/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.util.ArrayList;

import ri.serien.mobilite.environnement.Utilisateur;

public class GestionBObdd {
  private Utilisateur utilisateur = null;
  private String requeteBase = "SELECT * FROM sqlite_master;";
  
  public GestionBObdd(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public ArrayList<ArrayList<String>> envoyerRequeteSQLITE(String requete) {
    return utilisateur.getBaseSQLITE().envoyerRequete(requete);
  }
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public String getRequeteBase() {
    return requeteBase;
  }
  
  public void setRequeteBase(String requeteBase) {
    this.requeteBase = requeteBase;
  }
  
}
