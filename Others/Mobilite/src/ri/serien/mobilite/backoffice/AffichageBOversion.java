/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.backoffice;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.affichage.PatternErgonomie;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageBOversion
 */
public class AffichageBOversion extends HttpServlet {
  
  private int niveauAcces = ConstantesEnvironnement.ACCES_ADMIN;
  private PatternErgonomie pattern = null;
  private int metier = -4;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageBOversion() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  @SuppressWarnings("unchecked")
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      int niveau = 2;
      String metaSpecifiques = "";
      String titreOptions = "Options";
      
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        if (utilisateur.getNiveauAcces() <= niveauAcces) {
          // ancrage dans la page
          String ancrage = request.getParameter("ancrage");
          
          if (utilisateur.getGestionBOversion() == null) {
            utilisateur.setGestionBOversion(new GestionBOversion(utilisateur));
          }
          
          // charger le look correspondant
          metaSpecifiques += "<link href='css/parametres.css' rel='stylesheet'/>";
          
          ServletOutputStream out = response.getOutputStream();
          out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
          out.println(pattern.afficherHeader(metier, niveau, null, utilisateur));
          out.println("<article id='contenu'>");
          
          if (request.getParameter("telechVersion") != null && request.getParameter("telechVersion").equals("1")
              && request.getParameter("ftp") == null && request.getParameter("zip") == null) {
            // utilisateur.deconnecterAS400();
            // Thread.sleep(10000);
            // request.getSession().invalidate();
            if (utilisateur.getGestionBOversion().telechargerVersion()) {
              getServletContext().getRequestDispatcher("/affichageBOversion?ftp=1").forward(request, response);
            }
            else {
              getServletContext().getRequestDispatcher("/affichageBOversion?ftp=2").forward(request, response);
            }
          }
          else {
            out.println(afficherInterfaceControle(utilisateur, request.getParameter("ftp"), request.getParameter("zip")));
          }
          
          out.println("</article>");
          out.println(pattern.afficherFin(titreOptions, null));
        }
        else {
          request.getSession().invalidate();
          getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
        }
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher l'interface de contrôle des versions
   */
  private String afficherInterfaceControle(Utilisateur utilisateur, String ftp, String zip) {
    String retour = "";
    
    try {
      // Si on valide un téléchargement de FTP
      if (zip != null && zip.equals("3")) {
        
        retour += "<section class='secContenu' id='controleVersion'>";
        retour += "<h1><span class='titreH1'>Installation de la dernière version</h1>";
        retour += "<div>Version " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().getVersionWAR())
            + " installée</div>";
        retour += "</section>";
        
      }
      else if (ftp != null) {
        String resultatTransfert = "téléchargée avec succès";
        if (ftp.equals("2")) {
          resultatTransfert = "n'a pu être téléchargée";
        }
        
        retour += "<section class='secContenu' id='controleVersion'>";
        retour += "<h1><span class='titreH1'>Telechargement de la dernière version</h1>";
        retour += "<div>Version " + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().getVersionWAR()) + " "
            + resultatTransfert + "</div>";
        retour += "</section>";
        
      }
      
      retour += ("<section class='secContenu' id='controleVersion'>");
      retour += "<h1><span class='titreH1'>Contrôle de version</h1>";
      retour += "<div>Votre version logicielle: "
          + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().verifierVersionWAR()) + "</div>";
      retour += "<div>Votre version Base de données: "
          + utilisateur.getOutils().traduireVersion(utilisateur.getGestionBOversion().verifierVersionBDD()) + "</div>";
      
      switch (utilisateur.getGestionBOversion().controlerVersionGLobale()) {
        case 0:
          retour += "<div>Votre version est à jour";
          break;
        case 1:
          retour += afficherInterfacePasAJour(utilisateur);
          break;
        case 2:
          retour += "La BDD est désynchronisée par rapport à votre version";
          break;
        default:
          retour += "";
      }
      
      retour += "</section>";
    }
    catch (Exception e) {
      e.printStackTrace();
      return retour;
    }
    
    return retour;
  }
  
  /**
   * Afficher l'interface du WAR pas à jour
   */
  private String afficherInterfacePasAJour(Utilisateur utilisateur) {
    String retour = "";
    
    retour += "<div>Votre version logicielle n'est pas à jour</div>";
    
    retour += "<a href='affichageBOversion?telechVersion=1'>Télécharger la dernière version</a>";
    
    return retour;
  }
  
  private String afficherInterfaceTelecharger() {
    String retour = "";
    
    retour += ("<section class='secContenu' id='controleVersion'>");
    retour += "<h1><span class='titreH1'>Téléchargement de la dernière version</h1>";
    retour += "<div>Telechargement de la version en cours</div>";
    retour += "</section>";
    
    return retour;
  }
  
  private String afficherInterfaceAppliquer(Utilisateur utilisateur) {
    String retour = "";
    
    retour += ("<section class='secContenu' id='controleVersion'>");
    retour += "<h1><span class='titreH1'>Application de la dernière version</h1>";
    
    retour += "</section>";
    
    return retour;
  }
}
