/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.crm;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import ri.serien.libas400.dao.exp.programs.contact.M_Contact;

public class LAF_ActionCommerciale extends LAC_ActionCommerciale {
  
  /**
   * Retourne la liste des contacts pour un client pour une liste
   * @param idclient
   * @param xclliv
   * @return
   */
  public LinkedHashMap<M_Contact, Boolean> getListeContactsClient4List(String idclient, String xclliv, String valuecookie) {
    // On recharge les contacts
    ArrayList<M_Contact> lst = getListeContactsClient(idclient, xclliv, true);
    if (lst == null) {
      return null;
    }
    
    // On rempli la hashmap avec les données
    LinkedHashMap<M_Contact, Boolean> hash = new LinkedHashMap<M_Contact, Boolean>(lst.size());
    for (M_Contact contact : lst) {
      hash.put(contact, false);
    }
    
    // On récupère les valeurs du cookie
    String[] selection = valuecookie.split("_");
    for (String cle : selection) {
      for (M_Contact contact : lst) {
        if (cle.equals(contact.getREETB() + "-" + contact.getRENUM())) {
          hash.put(contact, true);
        }
      }
    }
    
    return hash;
  }
  
  /**
   * Met à jour la liste des contacts pour une action commerciale
   * @param listeContacts
   * @param value
   */
  public void setContactsLies(ArrayList<M_Contact> listeContacts, String value) {
    // On supprime tous les contacts cibles
    int i = 0;
    while (i < listeContacts.size()) {
      if (listeContacts.get(i).getCodeTypeContact().equals("CI")) {
        listeContacts.remove(i);
      }
      else {
        i++;
      }
    }
    
    // On n'a pas trouvé de cookie donc pas de contacts
    if (value.equals("")) {
      return;
    }
    
    // On découpe la valeur du cookie
    String[] selection = value.split("_");
    for (String cle : selection) {
      for (M_Contact contact : clCourant.getListContact(false)) {
        if (cle.equals(contact.getREETB() + "-" + contact.getRENUM())) {
          contact.setCodeTypeContact("CI");
          listeContacts.add(contact);
        }
      }
    }
  }
  
  /**
   * Génére la valeur pour le cookie stockant les contacts liés à l'action commerciale
   * @return
   */
  public String getValueCookie4ContactsLies() {
    StringBuffer sb = new StringBuffer();
    for (M_Contact contact : acCourante.getFiltreContacts("CI")) {
      sb.append(contact.getREETB() + "-" + contact.getRENUM()).append('_');
    }
    if (sb.length() > 0) {
      sb.deleteCharAt(sb.length() - 1);
    }
    return sb.toString();
  }
  
}
