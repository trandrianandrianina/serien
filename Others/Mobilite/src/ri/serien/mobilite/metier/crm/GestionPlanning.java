/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.crm;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;

public class GestionPlanning {
  /**
   * Méthode qui retourne la liste des actions commerciales de l'utilisateur
   */
  public ArrayList<GenericRecord> retournerListeEvenements(Utilisateur utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    if (utilisateur.getManager() != null) {
      // TODO Traitement propre à la récupération de la requête
      // liste = utilisateur.getManager().select("SELECT CLCLI, CLLIV, CLNOM FROM " + utilisateur.getBibli() +
      // ".PGVMCLIM WHERE TRIM(CLNOM) <> '' ORDER BY CLNOM FETCH FIRST 10 ROWS ONLY ");
    }
    
    return liste;
  }
}
