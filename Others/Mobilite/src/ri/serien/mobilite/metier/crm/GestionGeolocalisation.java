/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.crm;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;

public class GestionGeolocalisation {
  
  /**
   * Méthode qui retourne les clients à proximité (dans le département passé en paramètre)
   */
  public ArrayList<GenericRecord> retournerListeClients(Utilisateur utilisateur, String departement) {
    if (utilisateur == null) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    if (utilisateur.getManager() != null && departement != "") {
      // TODO Traitement propre à la récupération de la requête
      liste = utilisateur.getManager()
          .select("SELECT CLCLI, CLLIV, CLNOM, CLCPL, CLRUE, CLLOC, CLVIL, CLPAY FROM " + utilisateur.getBaseDeDonnees()
              + ".PGVMCLIM WHERE TRIM(CLNOM) <> '' AND LENGTH(TRIM(CLNOM))>1 AND CLTNS <> 9 AND CLTOP = 0 AND CLETB = '"
              + utilisateur.getEtablissement().getCodeEtablissement() + "' AND CLPAY = '' AND CLCDP1 LIKE '" + departement
              + "%' ORDER BY CLNOM FETCH FIRST 15 ROWS ONLY ");
    }
    
    return liste;
  }
  
}
