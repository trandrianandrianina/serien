/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.contact;

import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.Gestion;
import ri.serien.mobilite.metier.MethodesGestion;
import ri.serien.mobilite.metier.Specifiques;
import ri.serien.mobilite.metier.Zone;
import ri.serien.mobilite.metier.client.DevisClients;

public class GestionContacts extends Gestion implements MethodesGestion {
  private DevisClients devisClient = null;
  
  public GestionContacts(Utilisateur user) {
    super(user);
    
    metier = 1;
    fichierParent = utilisateur.getBaseSQLITE().getFichierParent(metier);
    
    order = "ORDER BY CLNOM ";
    // champsBase = "CLCLI,CLETB,CLLIV,CLTNS,CLNOM,CLCPL,CLRUE,CLLOC,CLTEL,CLVIL";
    champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
    fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
        + fichierParent.getFichier_raccou() + " ";
    
    criteresBase = " CLTNS <> 9 ";
    
    // Gestion de l'état d'une fiche
    zoneEtatFiche = utilisateur.getBaseSQLITE().chargerZoneEtatDunMetier(metier);
    tousEtatsFiches = new HashMap<String, String>();
    
  }
  
  public ArrayList<GenericRecord> getContactPourUnClient(String clcli) {
    if (clcli == null) {
      return null;
    }
    clcli = clcli + "000";
    String requete = "SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,XICOD,XIETB,XINUM,XISUF FROM " + utilisateur.getBaseDeDonnees()
        + ".PSEMRTLM LEFT JOIN " + utilisateur.getBaseDeDonnees() + ".PSEMRTEM ON RLNUMT=RENUM LEFT JOIN "
        + utilisateur.getBaseDeDonnees() + ".PGVMXLIM ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' AND XITYP='72'" + " WHERE rletb='"
        + utilisateur.getEtablissement().getCodeEtablissement() + "' AND RLIND='" + clcli + "'AND RLCOD ='C'";
    return utilisateur.getManager().select(requete);
  }
  
  // liste les devis pour un contact
  /*public ArrayList<GenericRecord> listeDevisPourUnContact(String clcli, String contact) {
    if (clcli == null && contact == null) {
      ;
    }
    clcli = clcli + "000";
    ArrayList<GenericRecord> liste =
        utilisateur.getManager()
            .selectOld(
                "SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,RECL1,XICOD,XIETB,XINUM,XISUF FROM " + utilisateur.getBibli()
                    + ".PSEMRTLM LEFT JOIN "
                    + utilisateur.getBibli() + ".PSEMRTEM ON RLNUMT=RENUM LEFT JOIN " + utilisateur.getBibli()
                    + ".PGVMXLIM ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' AND XITYP='72'" + " WHERE rletb='" + utilisateur.getEtb()
                    + "' AND RLIND='" + clcli + "'AND RLCOD ='C' AND RLNUMT = '" + contact + "'");
    return liste;
  }*/
  
  // detail du devis
  
  public ArrayList<GenericRecord> detailDuDevis(String clcli, String contact, String numDevis) {
    ArrayList<GenericRecord> detailDevis = utilisateur.getManager()
        .select("SELECT L1NUM,L1ERL,L1QTE,L1MHT,L1ART FROM " + utilisateur.getBaseDeDonnees() + "PGVMLBCM LEFT JOIN"
            + utilisateur.getBaseDeDonnees() + ".PGVMXLIM ON XINUM=L1NUM  WHERE L1ERL='C' AND L1NUM='" + numDevis + "'");
    return detailDevis;
  }
  
  @Override
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase) {
    // TODO Stub de la méthode généré automatiquement
    return null;
  }
  
  @Override
  public void gestionVues(String tri) {
    // TODO Stub de la méthode généré automatiquement
    
  }
  
  @Override
  public ArrayList<GenericRecord> gestionFavoris(String tri) {
    // TODO Stub de la méthode généré automatiquement
    return null;
  }
  
  @Override
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri) {
    // TODO Stub de la méthode généré automatiquement
    return null;
  }
  
  @Override
  public boolean isUnFavori(boolean onInverse) {
    // TODO Stub de la méthode généré automatiquement
    return false;
  }
  
  @Override
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif) {
    // TODO Stub de la méthode généré automatiquement
    return null;
  }
  
}
