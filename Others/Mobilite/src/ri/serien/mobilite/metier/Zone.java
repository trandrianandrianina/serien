/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.HashMap;

public class Zone {
  // id SQLITE de la zone
  private int zone_id = 0;
  // fichier AS 400 d'où provient la zone
  private int fichier_id = 0;
  // cette zone fait-elle partie de l'ID
  private int zone_isId = 0;
  // code de la zone
  private String zone_code = null;
  // libellé de la zone pour les label
  private String zone_libelle = null;
  // longueur de la zone
  private int zone_long = 0;
  // cette zone est-elle une zone de base
  private int zone_base = 0;
  // calcul qui amène à cette zone
  private String zone_calcul = null;
  // cette zone descrit en détail la zone et son rôle
  private String zone_descri = null;
  // cette zone fait partie des champs de recherche
  private String zone_recherche = null;
  // cette zone est-elle un label interne ou non
  private int zone_lb_int = 0;
  // typer la zone: alpha, numérique, montant, numérique conditionnée, téléphone, mail....
  private int zone_type = 0;
  // raccourci SQL du fichier AS 400 d'où vient la zone
  private String raccourciFichier = null;
  
  // attributs propres à l'affichage de la zone dans sa vue
  // ordre dans lequel s'affiche la zone au sein de son bloc
  private int ordre_zones = -1;
  // A quel bloc appartient cette zone dans cet affichage
  private int bloc_zones = 0;
  // Cette zone est-elle une zone d'affichage de la liste
  private int liste_zones = 0;
  // Cette zone est-elle modifiable dans cet affichage
  private int is_modif = 0;
  // Type d'affichage de la zone: normal, important, alerte
  private int mode_aff = 0;
  // Zone de type Enum
  private HashMap<String, String> valeursEnum = null;
  
  /**
   * Constructeur classique pour les SQL
   * @param id
   * @param fichier
   * @param isId
   * @param code
   * @param libelle
   * @param longu
   * @param base
   * @param calcul
   * @param description
   * @param recherche
   * @param lb_int
   * @param type
   * @param racc
   * @param ordre
   * @param bloc
   * @param liste
   * @param modif
   * @param aff
   */
  public Zone(int id, int fichier, int isId, String code, String libelle, int longu, int base, String calcul, String description,
      String recherche, int lb_int, int type, String racc, int ordre, int bloc, int liste, int modif, int aff) {
    zone_id = id;
    zone_isId = isId;
    zone_code = code;
    zone_libelle = libelle;
    zone_long = longu;
    zone_base = base;
    zone_calcul = calcul;
    zone_descri = description;
    zone_recherche = recherche;
    zone_lb_int = lb_int;
    zone_type = type;
    // liaison fichier
    fichier_id = fichier;
    raccourciFichier = racc;
    // affichage dans la vue
    ordre_zones = ordre;
    bloc_zones = bloc;
    liste_zones = liste;
    is_modif = modif;
    mode_aff = aff;
  }
  
  /**
   * Constructeur utilisé lors d'appel direct à des programmes RPG
   * @param code
   * @param libelle
   * @param ordre
   * @param bloc
   * @param liste
   * @param modif
   * @param aff
   */
  public Zone(String code, String libelle, int ordre, int bloc, int liste, int modif, int aff) {
    zone_code = code;
    zone_libelle = libelle;
    
    // affichage dans la vue
    ordre_zones = ordre;
    bloc_zones = bloc;
    liste_zones = liste;
    is_modif = modif;
    mode_aff = aff;
  }
  
  public int getZone_id() {
    return zone_id;
  }
  
  public void setZone_id(int zone_id) {
    this.zone_id = zone_id;
  }
  
  public int getFichier_id() {
    return fichier_id;
  }
  
  public void setFichier_id(int fichier_id) {
    this.fichier_id = fichier_id;
  }
  
  public int getZone_isId() {
    return zone_isId;
  }
  
  public void setZone_isId(int zone_isId) {
    this.zone_isId = zone_isId;
  }
  
  public String getZone_code() {
    return zone_code;
  }
  
  public void setZone_code(String zone_code) {
    this.zone_code = zone_code;
  }
  
  public String getZone_libelle() {
    return zone_libelle;
  }
  
  public void setZone_libelle(String zone_libelle) {
    this.zone_libelle = zone_libelle;
  }
  
  public int getZone_long() {
    return zone_long;
  }
  
  public void setZone_long(int zone_long) {
    this.zone_long = zone_long;
  }
  
  public int getZone_base() {
    return zone_base;
  }
  
  public void setZone_base(int zone_base) {
    this.zone_base = zone_base;
  }
  
  public String getZone_calcul() {
    return zone_calcul;
  }
  
  public void setZone_calcul(String zone_calcul) {
    this.zone_calcul = zone_calcul;
  }
  
  public String getZone_descri() {
    return zone_descri;
  }
  
  public void setZone_descri(String zone_description) {
    this.zone_descri = zone_description;
  }
  
  public String getZone_recherche() {
    return zone_recherche;
  }
  
  public void setZone_recherche(String zone_recherche) {
    this.zone_recherche = zone_recherche;
  }
  
  public String getRaccourciFichier() {
    return raccourciFichier;
  }
  
  public void setRaccourciFichier(String raccourciFichier) {
    this.raccourciFichier = raccourciFichier;
  }
  
  public int getZone_lb_int() {
    return zone_lb_int;
  }
  
  public void setZone_lb_int(int zone_lb_int) {
    this.zone_lb_int = zone_lb_int;
  }
  
  public int getOrdre_zones() {
    return ordre_zones;
  }
  
  public void setOrdre_zones(int ordre_zones) {
    this.ordre_zones = ordre_zones;
  }
  
  public int getBloc_zones() {
    return bloc_zones;
  }
  
  public void setBloc_zones(int bloc_zones) {
    this.bloc_zones = bloc_zones;
  }
  
  public int getListe_zones() {
    return liste_zones;
  }
  
  public void setListe_zones(int liste_zones) {
    this.liste_zones = liste_zones;
  }
  
  public int getZone_type() {
    return zone_type;
  }
  
  public void setZone_type(int zone_type) {
    this.zone_type = zone_type;
  }
  
  public int getIs_modif() {
    return is_modif;
  }
  
  public void setIs_modif(int is_modif) {
    this.is_modif = is_modif;
  }
  
  public int getMode_aff() {
    return mode_aff;
  }
  
  public void setMode_aff(int mode_aff) {
    this.mode_aff = mode_aff;
  }
  
  public HashMap<String, String> getValeursEnum() {
    return valeursEnum;
  }
  
  public void setValeursEnum(HashMap<String, String> valeursEnum) {
    this.valeursEnum = valeursEnum;
  }
  
}
