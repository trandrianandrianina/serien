/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.math.BigDecimal;

import ri.serien.mobilite.environnement.Utilisateur;

public class InfosClient {
  // Constantes
  private final static String LIBCLI_DFT = "";
  
  private Utilisateur utilisateur = null;
  private String clnom = LIBCLI_DFT; // Conservation du nom du client
  private BigDecimal clcli = BigDecimal.ZERO; // Numéro du client sélectionné en cours
  private BigDecimal clliv = BigDecimal.ZERO;
  
  /**
   * Constructeur
   * @param utilisateur
   */
  public InfosClient(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  /**
   * Initialisation via les paramères de l'url dans AffichageArticle
   * @param clcli
   * @param clliv
   */
  public void setWithUrlParameters(String clcli, String clliv) {
    if ((clcli == null) && (clliv == null)) {
      return;
    }
    // On a demandé un reset des données du client
    if (clcli == null || clcli.equals("0")) {
      reset();
      return;
    }
    
    setClcli(clcli);
    setClliv(clliv);
    setClnom(utilisateur.getGestionArticles().getClientNameWithClcli(getClcli(), getClliv()));
  }
  
  /**
   * Initialise les valeurs par défaut
   */
  public void reset() {
    this.clnom = LIBCLI_DFT;
    setClcli(null);
    setClliv(null);
  }
  
  /**
   * @return le clcli
   */
  public BigDecimal getClcli() {
    return clcli;
  }
  
  /**
   * @param clcli le clcli à définir
   */
  public void setClcli(String clcli) {
    if (clcli == null) {
      this.clcli = BigDecimal.ZERO;
    }
    else {
      this.clcli = new BigDecimal(clcli);
    }
  }
  
  /**
   * @return le clliv
   */
  public BigDecimal getClliv() {
    return clliv;
  }
  
  /**
   * @param clliv le clliv à définir
   */
  public void setClliv(String clliv) {
    if (clliv == null) {
      this.clliv = BigDecimal.ZERO;
    }
    else {
      this.clliv = new BigDecimal(clliv);
    }
  }
  
  /**
   * @return le clnom
   */
  public String getClnom() {
    return clnom;
  }
  
  /**
   * @param clnom le clnom à définir
   */
  public void setClnom(String clnom) {
    if (clnom == null) {
      reset();
    }
    else {
      this.clnom = clnom.trim();
    }
  }
  
}
