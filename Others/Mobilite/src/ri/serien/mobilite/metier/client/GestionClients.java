/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Connexion_SQLite;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.Gestion;
import ri.serien.mobilite.metier.MethodesGestion;
import ri.serien.mobilite.metier.Specifiques;
import ri.serien.mobilite.metier.Zone;
import ri.serien.mobilite.outils.TriGenericRecords;

/**
 * Classe de gestion métier des clients
 * @author ritoudb
 * 
 */
public class GestionClients extends Gestion implements MethodesGestion {
  private EncoursClients encours = null;
  private ContactsClients contacts = null;
  private DevisClients devis = null;
  private CommandesClients commandes = null;
  private BLClients bonsLivr = null;
  private FacturesClients factures = null;
  private FacturesNR facturesNR = null;
  
  /**
   * Constructeur de la gestion des clients
   */
  public GestionClients(Utilisateur utilisateur) {
    super(utilisateur);
    
    metier = 1;
    fichierParent = utilisateur.getBaseSQLITE().getFichierParent(metier);
    
    order = "ORDER BY CLNOM ";
    // champsBase = "CLCLI,CLETB,CLLIV,CLTNS,CLNOM,CLCPL,CLRUE,CLLOC,CLTEL,CLVIL";
    champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
    fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
        + fichierParent.getFichier_raccou() + " ";
    
    criteresBase = " CLTNS <> 9 ";
    
    // Gestion de l'état d'une fiche
    zoneEtatFiche = utilisateur.getBaseSQLITE().chargerZoneEtatDunMetier(metier);
    tousEtatsFiches = new HashMap<String, String>();
    tousEtatsFiches.put("2", "Client interdit");
  }
  
  /**
   * récupère une liste de clients par recherche de mot clé
   */
  public ArrayList<GenericRecord> gestionMotCle() {
    // requeteMotCle = "SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND CLNOM LIKE '%" +
    // motCle.toUpperCase() + "%' " + order + limit;
    requeteMotCle =
        "SELECT " + champsBase + fromBase + " WHERE " + criteresBase + recuperationDesChampsMotsCle(metier, motCle) + order + limit;
    
    return utilisateur.getManager().select(requeteMotCle);
  }
  
  /**
   * Afficher le détail d'un client
   */
  @Override
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase) {
    if (ids == null || ids.length != 3) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    // Si on a pas de vue particulière
    if (modeDeBase) {
      champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
      fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
          + fichierParent.getFichier_raccou() + " ";
      criteresBase = " CLTNS <> 9 ";
      if (utilisateur.getCodeRepresentant() != null && !utilisateur.getCodeRepresentant().trim().equals("")) {
        criteresBase +=
            " AND (CLREP = '" + utilisateur.getCodeRepresentant() + "' OR CLREP2 = '" + utilisateur.getCodeRepresentant() + "' )";
      }
    }
    else {
      // A voir si il ne faut pas refaire une gestion des vues à ce niveau
      if (typeVue == 1) {
        // vue générique
      }
      // vue stocks disponibles
      else if (typeVue == 2) {
        if (encours != null) {
          encours.construireRequeteUnRecord();
        }
      }
      // vue contacts
      else if (typeVue == 3) {
        if (contacts != null) {
          contacts.construireRequeteUnRecord();
        }
      }
      
      // vue devis
      else if (typeVue == 4) {
        if (devis != null) {
          devis.construireRequeteUnRecord();
          
          // devis.constructionListeContact();
        }
      }
      // vue commandes
      else if (typeVue == 5) {
        if (commandes != null) {
          commandes.construireRequeteUnRecord();
        }
      }
      // vue livraison
      else if (typeVue == 6) {
        if (bonsLivr != null) {
          bonsLivr.construireRequeteUnRecord();
        }
      }
      // vue factures
      else if (typeVue == 7) {
        if (factures != null) {
          factures.construireRequeteUnRecord();
        }
      }
      // vue factures
      else if (typeVue == 8) {
        if (facturesNR != null) {
          facturesNR.construireRequeteUnRecord();
        }
      }
    }
    
    liste = utilisateur.getManager().select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND cl.CLETB = '" + ids[0]
        + "' AND cl.CLCLI = '" + ids[1] + "' AND cl.CLLIV = '" + ids[2] + "'");
    
    if (liste == null || liste.size() == 0) {
      if (!modeDeBase) {
        recordActuel = recupererUnRecord(ids, true);
      }
      else {
        return null;
      }
    }
    else {
      recordActuel = liste.get(0);
    }
    
    return recordActuel;
  }
  
  /**
   * Récupérer le détail d'un record via 3 codes
   */
  public GenericRecord recupererUnRecord(String code1, String code2, String code3) {
    if (code1 == null || code2 == null || code3 == null) {
      return null;
    }
    
    String[] tab = { code1, code2, code3 };
    
    return recupererUnRecord(tab, false);
  }
  
  /**
   * en fonction de la vue choisie charger les infos (champs et conditions) correspondantes pour la requête
   */
  @Override
  public void gestionVues(String tri) {
    gererTri(tri);
    
    // vue générique
    if (typeVue == 1) {
      // On ne fait rien
    }
    // vue Encours clients
    else if (typeVue == 2) {
      if (encours != null) {
        encours.construireRequeteListeRecords();
        vueCourante = encours;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "ENCOURS";
        sensTri = "DESC";
      }
    }
    // vue contacts clients
    else if (typeVue == 3) {
      if (contacts != null) {
        contacts.construireRequeteListeRecords();
        vueCourante = contacts;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    else if (typeVue == 4) {
      if (devis != null) {
        devis.construireRequeteListeRecords();
        vueCourante = devis;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    else if (typeVue == 5) {
      if (commandes != null) {
        commandes.construireRequeteListeRecords();
        vueCourante = commandes;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    else if (typeVue == 6) {
      if (bonsLivr != null) {
        bonsLivr.construireRequeteListeRecords();
        vueCourante = bonsLivr;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    else if (typeVue == 7) {
      if (factures != null) {
        factures.construireRequeteListeRecords();
        vueCourante = factures;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    else if (typeVue == 8) {
      if (facturesNR != null) {
        facturesNR.construireRequeteListeRecords();
        vueCourante = facturesNR;
      }
      // premier passage sans tri
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer correctement
        typeTri = "CLNOM";
        sensTri = "DESC";
      }
    }
    
    order = " ORDER BY " + typeTri + " " + sensTri;
    // order+= sensTri;
  }
  
  /**
   * récupère la liste des favoris de l'utilisateur
   */
  @Override
  public ArrayList<GenericRecord> gestionFavoris(String tri) {
    String[] tabIds = new String[3];
    ArrayList<GenericRecord> listeFinale = new ArrayList<GenericRecord>();
    // on vérifie d'abord qu'il ne s'agisse pas d'un tri de la liste existente
    if (tri != null && !tri.equals("") && !tri.equals("MEMELIGNES") && !tri.equals("PLUSLIGNES") && !tri.equals("VUE")) {
      TriGenericRecords tg = new TriGenericRecords(listeRecords);
      if (sensTri.equals("ASC") || sensTri.equals("")) {
        listeFinale = tg.descendant(tri);
      }
      else {
        listeFinale = tg.ascendant(tri);
      }
    }
    else {
      ArrayList<GenericRecord> liste = null;
      ArrayList<String> listeCLients = utilisateur.getBaseSQLITE().recupererLesClientsFavoris(utilisateur, sensTri);
      if (listeCLients != null && listeCLients.size() > 0) {
        // On scanne la liste de clients existants et on la régénère avec les nouvelles données
        for (int i = 0; i < listeCLients.size(); i++) {
          utilisateur.getBaseSQLITE();
          // découper l'ID en AIETB et A1ART
          tabIds = listeCLients.get(i).split(Connexion_SQLite.SEPARATEUR_IDS);
          // si on a bien récupéré les IDS SQL sur DB2
          if (tabIds != null && tabIds.length == 3) {
            liste = utilisateur.getManager().select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND CLETB = '"
                + tabIds[0] + "' AND CLCLI = '" + tabIds[1] + "' AND CLLIV = '" + tabIds[2] + "' ");
            // Si la requête contient toutes le données demandées
            if (liste != null && liste.size() > 0) {
              listeFinale.add(liste.get(0));
            }
            else {
              liste = utilisateur.getManager()
                  .select("SELECT CLETB, CLCLI, CLLIV, CLNOM FROM " + utilisateur.getBaseDeDonnees() + "."
                      + fichierParent.getFichier_libelle() + " " + fichierParent.getFichier_raccou() + " WHERE CLETB = '" + tabIds[0]
                      + "' AND CLCLI = '" + tabIds[1] + "' AND CLLIV = '" + tabIds[2] + "' ");
              listeFinale.add(liste.get(0));
            }
          }
        }
      }
    }
    
    return listeFinale;
  }
  
  /**
   * Ce client est il un favori ? On modifie son état
   */
  @Override
  public boolean isUnFavori(boolean onInverse) {
    return utilisateur.getBaseSQLITE().gererUnClientFavori(utilisateur, onInverse);
  }
  
  /**
   * Ce client est il un favori ? On ne modifie pas son état
   */
  public boolean isUnFavori(GenericRecord record) {
    return utilisateur.getBaseSQLITE().isUnClientFavori(utilisateur, record);
  }
  
  /**
   * Récupérer les même clients mais charger des données différentes propre à la vue demandée
   */
  @Override
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri) {
    if (listeRecords == null) {
      return null;
    }
    
    ArrayList<GenericRecord> listeFinale = null;
    // GenericRecord record = null;
    
    // monter la liste au lieu de la descendre si il y a un tri
    int i = 0;
    int fin = listeRecords.size();
    int increment = 1;
    if (tri != null && !tri.equals("VUE")) {
      i = listeRecords.size() - 1;
      fin = -1;
      increment = -1;
    }
    
    gestionVues(tri);
    
    String complementWhere = "";
    
    // adapter le order à la liste
    if (listeRecords.size() > 0) {
      order = " ORDER BY CASE CLCLI ";
      
      int j = 1;
      
      // On scanne la liste de records existants et on la régénère avec les nouvelles données
      while (i != fin) {
        if ((fin == -1 && i == listeRecords.size() - 1) || (fin == listeRecords.size() && i == 0)) {
          complementWhere += " AND ( ";
        }
        else {
          complementWhere += " OR ";
        }
        
        complementWhere += " (cl.CLETB = '" + listeRecords.get(i).getField("CLETB").toString() + "'  AND cl.CLCLI = '"
            + listeRecords.get(i).getField("CLCLI").toString() + "' AND cl.CLLIV ='" + listeRecords.get(i).getField("CLLIV").toString()
            + "' ) ";
        order += "WHEN '" + listeRecords.get(i).getField("CLCLI").toString() + "' THEN " + j + " ";
        
        // finir le bloc de conditions
        if ((fin == -1 && i == 0) || (fin == listeRecords.size() && i == listeRecords.size() - 1)) {
          complementWhere += " ) ";
          
          criteresBase += complementWhere;
          order += " END ";
        }
        
        i += increment;
        j++;
      }
      
      listeFinale = utilisateur.getManager().select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + order);
    }
    else {
      order = " ORDER BY CLCLI FETCH FIRST 1 ROWS ONLY ";
      listeFinale = new ArrayList<GenericRecord>();
    }
    
    return listeFinale;
  }
  
  /**
   * Charger la classe de la vue correspondante
   */
  @Override
  public void setVue(int vue) {
    typeVue = vue;
    switch (typeVue) {
      case 1:
        break;
      case 2:
        if (this.getEncours() == null) {
          this.setEncours(new EncoursClients());
        }
        vueCourante = this.getEncours();
        break;
      case 3:
        if (this.getContacts() == null) {
          this.setContacts(new ContactsClients());
        }
        vueCourante = this.getContacts();
        break;
      case 4:
        if (this.getDevis() == null) {
          this.setDevis(new DevisClients());
        }
        vueCourante = this.getDevis();
        break;
      case 5:
        if (this.getCommandes() == null) {
          this.setCommandes(new CommandesClients());
        }
        vueCourante = this.getCommandes();
        break;
      case 6:
        if (this.getBonsLivr() == null) {
          this.setBonsLivr(new BLClients());
        }
        vueCourante = this.getBonsLivr();
        break;
      case 7:
        if (this.getFactures() == null) {
          this.setFactures(new FacturesClients());
        }
        vueCourante = this.getFactures();
        break;
      case 8:
        if (this.getFacturesNR() == null) {
          this.setFacturesNR(new FacturesNR());
        }
        vueCourante = this.getFacturesNR();
        break;
    }
  }
  
  /**
   * Traitement spécifique aux zones articles
   */
  @Override
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif) {
    String retour = "";
    
    // PAS de spécifique pour le moment
    
    retour += spec.retournerTypeZone(utilisateur.getGestionClients().getRecordActuel(), zone, isEnModif);
    
    return retour;
  }
  
  public void traiterDonnees(Map<String, String[]> parameterMap) {
  }
  
  @Override
  public void setCriteresBase(String criteresBase) {
    if (utilisateur.getCodeRepresentant() != null && !utilisateur.getCodeRepresentant().trim().equals("")) {
      this.criteresBase = " (CLREP = '" + utilisateur.getCodeRepresentant() + "' OR CLREP2 = '" + utilisateur.getCodeRepresentant()
          + "' ) AND " + criteresBase;
    }
    else {
      this.criteresBase = criteresBase;
    }
    
    // this.criteresBase = criteresBase;
  }
  
  /*//liste les contacts pour un client
  public ArrayList<GenericRecord> getContactPourUnClient(String clcli)
  {
  if (clcli == null)return null;
  clcli =clcli+"000";
    String requete = "SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,XICOD,XIETB,XINUM,XISUF FROM "+utilisateur.getBibli()+".PSEMRTLM LEFT JOIN " +
            utilisateur.getBibli()+".PSEMRTEM ON RLNUMT=RENUM LEFT JOIN "+utilisateur.getBibli()+".PGVMXLIM ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' AND XITYP='72'" +
            " WHERE rletb='"+utilisateur.getEtb()+"' AND RLIND='"+clcli+"'AND RLCOD ='C'" ;
    return utilisateur.getManager().select(requete);
  }*/
  // liste les devis pour un contact
  /*public GenericRecord listeDevisPourUnContact(String clcli, String contact)
  {
    if(clcli == null && contact == null);
    clcli =clcli+"000";
    ArrayList<GenericRecord> liste = utilisateur.getManager().select("SELECT RLCOD,RLETB,RLIND,RENUM,REPAC,XICOD,XIETB,XINUM,XISUF FROM "+utilisateur.getBibli()+".PSEMRTLM LEFT JOIN " +
        utilisateur.getBibli()+".PSEMRTEM ON RLNUMT=RENUM LEFT JOIN "+utilisateur.getBibli()+".PGVMXLIM ON XILIB=REETB || DIGITS (RENUM) AND XICOD='D' AND XITYP='72'" +
        " WHERE rletb='"+utilisateur.getEtb()+"' AND RLIND='"+clcli+"'AND RLCOD ='C' AND RLNUMT = '"+contact+"'" );
    return liste.get(0);
  }*/
  
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ GETTER SETTER ++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public EncoursClients getEncours() {
    return encours;
  }
  
  public void setEncours(EncoursClients encours) {
    this.encours = encours;
    if (this.encours != null) {
      this.encours.setUtilisateur(utilisateur);
    }
  }
  
  public ContactsClients getContacts() {
    return contacts;
  }
  
  public void setContacts(ContactsClients contacts) {
    this.contacts = contacts;
    if (this.contacts != null) {
      this.contacts.setUtilisateur(utilisateur);
    }
  }
  
  public DevisClients getDevis() {
    return devis;
  }
  
  public void setDevis(DevisClients devis) {
    this.devis = devis;
    if (this.devis != null) {
      this.devis.setUtilisateur(utilisateur);
    }
  }
  
  public CommandesClients getCommandes() {
    return commandes;
  }
  
  public void setCommandes(CommandesClients commandes) {
    this.commandes = commandes;
    if (this.commandes != null) {
      this.commandes.setUtilisateur(utilisateur);
    }
  }
  
  public BLClients getBonsLivr() {
    return bonsLivr;
  }
  
  public void setBonsLivr(BLClients bonsLivr) {
    this.bonsLivr = bonsLivr;
    if (this.bonsLivr != null) {
      this.bonsLivr.setUtilisateur(utilisateur);
    }
  }
  
  public FacturesClients getFactures() {
    return factures;
  }
  
  public void setFactures(FacturesClients factures) {
    this.factures = factures;
    if (this.factures != null) {
      this.factures.setUtilisateur(utilisateur);
    }
  }
  
  public FacturesNR getFacturesNR() {
    return facturesNR;
  }
  
  public void setFacturesNR(FacturesNR facturesNR) {
    this.facturesNR = facturesNR;
    if (this.facturesNR != null) {
      this.facturesNR.setUtilisateur(utilisateur);
    }
  }
  
}
