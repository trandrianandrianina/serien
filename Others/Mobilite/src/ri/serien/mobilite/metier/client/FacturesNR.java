/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.client;

import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.metier.MethodesSpecifiques;
import ri.serien.mobilite.metier.Specifiques;

public class FacturesNR extends Specifiques implements MethodesSpecifiques {
  public FacturesNR() {
    vue = 12;
  }
  
  @Override
  public void construireRequeteListeRecords() {
    // maj des fichiers concernés par cette vue
    mettreAjourFichiersListe(utilisateur.getGestionClients().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    }
    // maj de la matrice des zones de la liste
    if (matriceZonesListe == null) {
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionClients().getMetier(), vue);
    }
    
    // maj des zones de clé et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases nécessaires à cette vue
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesListe));
    // maj des fichiers attaqués en SQL et leurs jointures
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersListe));
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, false, true));
    // maj des critéres de tri et de sélection
    // utilisateur.getGestionClients().setCriteresBase(" cl.CLTNS <> 9 " );
    // utilisateur.getGestionClients().setCriteresBase(recupererWhere(utilisateur.getGestionClients().getFichierParent(),vue));
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  @Override
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionClients().getFichierParent());
    
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionClients().getFichierParent(), vue);
    }
    
    if (matriceZonesFiche == null) {
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionClients().getMetier(), vue);
    }
    
    utilisateur.getGestionClients().setChampsBase(recupererCodeZones(matriceZonesFiche));
    
    // utilisateur.getGestionClients().setFromBase(recupererFichiersFrom(listeFichiersFiche));
    utilisateur.getGestionClients()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionClients().getFichierParent(), vue, true, true));
    
    utilisateur.getGestionClients().setCriteresBase(recupererWhere(listeFichiersFiche));
    
  }
  
  @Override
  public String afficherListeRecords(String apport, int nbLignes) {
    if (apport.equals("")) {
      if (utilisateur.getGestionClients().getListeRecords().size() < nbLignes) {
        apport = utilisateur.getGestionClients().getListeRecords().size() + " r&eacute;sultats";
      }
      else {
        apport = "Plus de " + (utilisateur.getGestionClients().getListeRecords().size() - 1) + " r&eacute;sultats";
      }
    }
    String retour = "";
    retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Factures à régler</span><span class='apportListe'>" + apport
        + "</span><div class='triTetieres'><a href='clients?tri=" + clePrincipale.getZone_code()
        + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a><a href='clients?tri="
        + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle()
        + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    
    // Si la liste est vide afficher un message
    if (utilisateur.getGestionClients().getListeRecords().size() == 0) {
      retour += "<p class='messageListes'>Pas de client pour ces critères</p>";
    }
    
    boolean isFavori = false;
    String etatFiche = null;
    // si la liste n'est pas vide
    for (int i = 0; i < utilisateur.getGestionClients().getListeRecords().size(); i++) {
      isFavori = utilisateur.getGestionClients().isUnFavori(utilisateur.getGestionClients().getListeRecords().get(i));
      etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getListeRecords().get(i));
      
      if (i % 2 == 0) {
        retour += "<div class='listesClassiques'>";
      }
      else {
        retour += "<div class='listesClassiquesF'>";
      }
      // si la ligne est inférieur au nombre maximum de lignes
      if (i < nbLignes - 1) {
        retour += "<a id='client" + i + "' href='"
            + retournerLienVersDetail("clients?", utilisateur.getGestionClients().getListeRecords().get(i), matriceZonesIds)
            + "' class='detailsListe'>"
            + retournerStructureAffichageListe(utilisateur.getGestionClients().getListeRecords().get(i), isFavori, etatFiche) + "</a>";
        
        retour += "<a href='#' class='"
            + utilisateur.getGestionClients().retournerClasseSelection(
                utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
                    + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim(),
                "selectionClient", "selectionClientF")
            + "' onClick=\"switchSelection(this,'selectionClient','"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLETB").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLCLI").toString().trim() + "-"
            + utilisateur.getGestionClients().getListeRecords().get(i).getField("CLLIV").toString().trim() + "');\"></a>";
      }
      // si il existe plus d'enregistrements que le max affichable
      else {
        retour += "<a id='plusResultats' href='clients?ancrage=client" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de résultats</a>";
      }
      
      retour += "</div>";
    }
    return retour;
  }
  
  @Override
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) {
    String retour = "";
    retour += "<h1><span class='titreH1'><span class='superflus'>Factures à régler: </span> "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLNOM")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='clients' id='formFiche' method='post'>";
    
    // Gestion de l'état de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionClients(), utilisateur.getGestionClients().getRecordActuel());
    if (etatFiche != null) {
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    }
    
    boolean chgtBoite = false;
    boolean gestionFac = (utilisateur.getGestionClients().getRecordActuel().isPresentField("NBFACTURES")
        && !utilisateur.getGestionClients().getRecordActuel().getField("NBFACTURES").toString().equals("0"));
    boolean affPasDeFac = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      // Gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Dernière facture</h2>";
      }
      
      if (matriceZonesFiche.get(i).getBloc_zones() == 2 && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori) {
          retour += "<h2>Détails client<span id='isUnFavori'></span></h2>";
        }
        else {
          retour += "<h2>Détails client<span id='isPasUnFavori'></span></h2>";
        }
      }
      
      // Gestion du contenu ++++++++++++++++++++
      if (matriceZonesFiche.get(i).getBloc_zones() == 1 && !gestionFac) {
        if (!affPasDeFac) {
          retour += "<p>Aucune facture non réglée pour ce client</p>";
          affPasDeFac = true;
        }
      }
      else {
        // Spécifique
        retour += utilisateur.getGestionClients().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
      }
      
      if (i == matriceZonesFiche.size() - 1) {
        retour += "</div>";
      }
    }
    
    retour += "</form>";
    
    retour += afficherLeModule("clients?module=13&", utilisateur.getGestionClients().getRecordActuel(), "dernières factures", "FROM "
        + utilisateur.getBaseDeDonnees() + ".PGVMCLIM cl LEFT OUTER JOIN  " + utilisateur.getBaseDeDonnees()
        + ".PGVMEBCM cv ON cl.CLCLI = cv.E1CLLP AND cl.CLLIV = cv.E1CLLS WHERE cv.E1COD = 'E' AND cv.E1TOP = 0 AND cv.E1ETA > 4 AND cl.CLCLI = "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI") + " AND cl.CLLIV = "
        + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV") + " AND cl.CLETB='"
        + utilisateur.getEtablissement().getCodeEtablissement() + "' ORDER BY cv.E1NUM DESC, cv.E1SUF DESC FETCH FIRST "
        + ConstantesAffichage.NB_LIGNES_MODULES + " ROWS ONLY");
    
    return retour;
  }
  
}
