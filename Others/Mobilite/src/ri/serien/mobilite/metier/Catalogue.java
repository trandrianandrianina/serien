/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

/**
 * Un catalogue pemret de séparer les vues en dommaine fonctionnel.
 * 
 * Suivant le paramètrage, un catalogue peut être associé ou non à un utilisateur.
 * 
 * Il y a deux catalogues actuellement :
 * - Commercial (associé par défaut aux nouveaux utilisateurs)
 * - Magasinier
 */
public class Catalogue {
  private int id = 0;
  private String libelle = null;
  private String urlIcone = null;
  
  /**
   * Constructeur.
   * @param Identifiant unique du catalogue.
   */
  public Catalogue(int pId) {
    id = pId;
  }
  
  /**
   * Identifiant unique du catalogue.
   */
  public int getId() {
    return id;
  }
  
  /**
   * Modifier l'identifiant unique du catalogue.
   */
  public void setId(int pId) {
    id = pId;
  }
  
  /**
   * Libellé du catalogue.
   * 
   * Cette information correspond au champ "catalogue.cata_label" de la base SQLite.
   */
  public String getLibelle() {
    return libelle;
  }
  
  /**
   * Modifier le libellé du catalogue.
   */
  public void setLibelle(String pLibelle) {
    libelle = pLibelle;
  }
  
  /**
   * URL de l'icône du catalogue.
   * 
   * Cette information correspond au champ "catalogue.cata_icone" de la base SQLite.
   */
  public String getUrlIcone() {
    return urlIcone;
  }
  
  /**
   * Modifier l'URL de l'icône du catalogue.
   */
  public void setUrlIcone(String pUrlIcone) {
    urlIcone = pUrlIcone;
  }
  
}
