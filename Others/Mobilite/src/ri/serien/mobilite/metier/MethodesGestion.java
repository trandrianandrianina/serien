/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;

public interface MethodesGestion {
  /**
   * Permet de récupérer un record avec ses ID
   */
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase);
  
  /**
   * Permet de switcher sur les spécifiques métier en fonction de la vue métier
   */
  public void gestionVues(String tri);
  
  /**
   * Construire l'objet spécifique demandé en fonction de la vue
   */
  public void setVue(int vue);
  
  /**
   * Permet de charger la liste des records favoris pour un métier
   */
  public ArrayList<GenericRecord> gestionFavoris(String tri);
  
  /**
   * Permet de récuperer la même liste de records avec des données différentes
   */
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri);
  
  /**
   * Retourne si le record en cours est un favori ou non en fonction de si on doit modifier son état ou non.
   */
  public boolean isUnFavori(boolean onInverse);
  
  /**
   * Avant d'afficher la zone et son contenu vérifier les traitements spécifiques au métier
   */
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif);
  
}
