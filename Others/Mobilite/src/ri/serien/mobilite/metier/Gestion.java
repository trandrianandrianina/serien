/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.article.GestionArticles;
import ri.serien.mobilite.metier.client.GestionClients;
import ri.serien.mobilite.outils.TriGenericRecords;

public abstract class Gestion {
  protected Utilisateur utilisateur = null;
  
  protected Fichier fichierParent = null;
  
  protected String champsBase = "";
  protected String fromBase = "";
  protected String criteresBase = "";
  
  protected String champsSupp = "";
  protected String fromSupp = "";
  protected String citeresSupp = "";
  protected String order = "";
  protected String limit = "";
  
  protected String sensTri = null;
  protected String typeTri = null;
  protected String motCle = "";
  protected String[] listeSelection = null;
  protected String rechercheSpeciale = null;
  protected int nbLignesMax = 0;
  protected String requeteMotCle = null;
  protected int typeVue = 0;
  protected int niveauMetier = 0;
  protected int metier = 0;
  protected String[] libIdsMetier = null;
  
  protected ArrayList<GenericRecord> listeRecords = null;
  
  protected ArrayList<GenericRecord> favorisRecords = null;
  protected GenericRecord recordActuel = null;
  protected ArrayList<Zone> champsDeRecherche = null;
  protected Zone zoneEtatFiche = null;
  protected HashMap<String, String> tousEtatsFiches = null;
  
  protected Specifiques vueCourante = null;
  protected Module moduleEnCours = null;
  
  public Gestion() {
  }
  
  public Gestion(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
    // initialiser le sens du tri
    sensTri = "";
    // initialiser la vue à la vue générique
    typeVue = 1;
    // initialiser la demande au niveau de la liste des records
    niveauMetier = 1;
    
    rechercheSpeciale = "fav";
    
    // initialiser le nb de lignes d'une liste
    nbLignesMax = utilisateur.getNbLignesListes() + 1; // Pourquoi +1 ?
    limit = " FETCH FIRST " + nbLignesMax + " ROWS ONLY";
    
  }
  
  /**
   * Initialiser les données propres à la gestion des clients
   */
  public void initDonnes() {
    listeRecords = null;
    // recordActuel = null;
    motCle = "";
  }
  
  /**
   * Chercher des records en fonction des paramètres passés
   */
  public ArrayList<GenericRecord> chercherDesRecords(String rech, String tri, String special, int nbLignes) {
    // Gestion des vues spécifique
    if (this instanceof GestionArticles) {
      ((GestionArticles) this).gestionVues(tri);
    }
    else if (this instanceof GestionClients) {
      ((GestionClients) this).gestionVues(tri);
    }
    // mettre à jour les variables globales
    if (tri == null) {
      rechercheSpeciale = special;
      motCle = rech;
    }
    else {
      if (special == null) {
        special = rechercheSpeciale;
      }
      if (rech == null) {
        rech = motCle;
      }
    }
    
    nbLignesMax = nbLignes;
    // mettre à jour le nb de lignes recherchées
    limit = " FETCH FIRST " + nbLignes + " ROWS ONLY ";
    
    // Premier passage
    if (listeRecords == null) {
      listeRecords = gestionGlobaleFavoris(tri);
      special = "fav";
      rechercheSpeciale = special;
    }
    // demande par mot clé
    else if (rech != null && !rech.equals("")) {
      listeRecords = gestionGlobaleMotsCles();
    }
    else if (special != null) {
      if (special.equals("fav")) {
        listeRecords = gestionGlobaleFavoris(tri);
      }
      else {
        listeRecords = gestionGLobaleSpecials(tri);
      }
    }
    // On recharge la même vue
    else {
      listeRecords = gestionChangementVue(tri);
    }
    
    return listeRecords;
  }
  
  /**
   * Gestion des demandes de records favoris
   */
  private ArrayList<GenericRecord> gestionGlobaleFavoris(String tri) {
    if (this instanceof GestionArticles) {
      listeRecords = ((GestionArticles) this).gestionFavoris(tri);
    }
    else if (this instanceof GestionClients) {
      listeRecords = ((GestionClients) this).gestionFavoris(tri);
    }
    
    return listeRecords;
  }
  
  /**
   * Gestion des demandes de records par mot clé
   */
  private ArrayList<GenericRecord> gestionGlobaleMotsCles() {
    ArrayList<GenericRecord> liste = null;
    
    if (this instanceof GestionArticles) {
      liste = ((GestionArticles) this).gestionMotCle();
    }
    else if (this instanceof GestionClients) {
      liste = ((GestionClients) this).gestionMotCle();
    }
    
    return liste;
  }
  
  /**
   * Gestion des changement de vue d'une même liste de records
   */
  private ArrayList<GenericRecord> gestionChangementVue(String tri) {
    ArrayList<GenericRecord> liste = null;
    
    if (this instanceof GestionArticles) {
      liste = ((GestionArticles) this).recuperationDesMemeRecords(tri);
    }
    else if (this instanceof GestionClients) {
      liste = ((GestionClients) this).recuperationDesMemeRecords(tri);
    }
    
    return liste;
  }
  
  /**
   * Gestion des demandes spéciales de records
   */
  private ArrayList<GenericRecord> gestionGLobaleSpecials(String tri) {
    ArrayList<GenericRecord> liste = null;
    
    if (tri != null && !tri.equals("") && !tri.equals("MEMELIGNES") && !tri.equals("PLUSLIGNES")) {
      TriGenericRecords tg = new TriGenericRecords(listeRecords);
      if (sensTri.equals("ASC")) {
        liste = tg.descendant(tri);
      }
      else {
        liste = tg.ascendant(tri);
      }
    }
    else {
      // Traiter le cas de la sélection de records
      if (listeSelection != null && rechercheSpeciale.equals("sel")) {
        liste = new ArrayList<GenericRecord>();
        GenericRecord record = null;
        String[] codes = null;
        
        // scanner les records sélectionnés dans le cookie et les afficher
        for (int i = 0; i < listeSelection.length; i++) {
          codes = listeSelection[i].split("-");
          // chercher un record
          if (this instanceof GestionArticles) {
            record = ((GestionArticles) this).recupererUnRecord(codes, false);
          }
          else if (this instanceof GestionClients) {
            record = ((GestionClients) this).recupererUnRecord(codes, false);
          }
          if (record != null) {
            liste.add(record);
          }
        }
      }
    }
    
    return liste;
  }
  
  /**
   * En fonction de la vue charger la classe spécifique demandée. A Surcharger dans la classe enfant
   */
  public void setVue(int vue) {
    
  }
  
  /**
   * permet de changer dynamiquement la classe du client dans la liste afin de voir s'il fait parti de la sélection ou
   * non
   */
  public String retournerClasseSelection(String id, String classe, String classeF) {
    String selection = classe;
    // controler que la selection de clients ne soit pas vide
    if (listeSelection != null) {
      int i = 0;
      while (i < listeSelection.length) {
        // si on trouve changer la classe du client
        if (id.equals(listeSelection[i])) {
          selection = classeF;
          i = listeSelection.length;
        }
        i++;
      }
    }
    
    return selection;
  }
  
  /**
   * Gestion du tri des listes
   */
  public void gererTri(String tri) {
    if (tri != null && !tri.equals("PLUSLIGNES") && !tri.equals("MEMELIGNES") && !tri.equals("VUE")) {
      // tester si tri descendant ou ascendant
      if (sensTri != null && sensTri.equals("DESC")) {
        sensTri = "ASC";
      }
      else if (sensTri != null && sensTri.equals("ASC")) {
        sensTri = "DESC";
      }
      
      typeTri = tri;
    }
    else {
      if (sensTri != null && sensTri.equals("DESC")) {
        sensTri = "DESC";
      }
      else if (sensTri != null && sensTri.equals("ASC")) {
        sensTri = "ASC";
      }
      else {
        sensTri = "DESC";
      }
      
      if (tri != null && (tri.equals("PLUSLIGNES") || tri.equals("MEMELIGNES") || tri.equals("VUE"))) {
        tri = typeTri;
      }
    }
  }
  
  /**
   * recupération des champs dans lequels chercher le mot clé
   */
  public String recuperationDesChampsMotsCle(int metier, String motCle) {
    String retour = null;
    
    if (champsDeRecherche == null) {
      champsDeRecherche = utilisateur.getBaseSQLITE().recupererChampsMotsCle(metier);
    }
    
    if (champsDeRecherche != null) {
      retour = " AND (";
      for (int i = 0; i < champsDeRecherche.size(); i++) {
        if (i != 0) {
          retour += " OR ";
        }
        
        retour += champsDeRecherche.get(i).getZone_code();
        retour += " LIKE '%" + motCle.toUpperCase() + "%' ";
      }
      retour += ") ";
    }
    
    return retour;
  }
  
  /**
   * Génère le message à envoyer avec le mailto
   * @return
   */
  public String retournerInfosBrutesFiche() {
    if (vueCourante == null || recordActuel == null) {
      return null;
    }
    
    StringBuffer buffer = new StringBuffer();
    
    for (int i = 0; i < vueCourante.getMatriceZonesFiche().size(); i++) {
      if (vueCourante.getMatriceZonesFiche().get(i).getBloc_zones() != 0) {
        if (recordActuel.isPresentField(vueCourante.getMatriceZonesFiche().get(i).getZone_code())) {
          buffer.append(vueCourante.getMatriceZonesFiche().get(i).getZone_descri() + ": "
              + recordActuel.getField(vueCourante.getMatriceZonesFiche().get(i).getZone_code()) + "%0D%0A");
        }
      }
    }
    
    // On remplace le caractère apostrophe qui tronque le texte dans le mailto
    String chaine = buffer.toString();
    chaine = chaine.replaceAll("'", "&#39;");
    
    return chaine;
  }
  
  /**
   * gestion spécifique aux modules
   */
  public void gestionModule(int IdModule, Map<String, String[]> donnees) {
    if (moduleEnCours == null) {
      moduleEnCours = new Module(utilisateur);
    }
    
    moduleEnCours.majModule(IdModule, donnees);
  }
  
  // +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++ GETTER SETTER ++++++++++++++++++++++++++++++++++++++++++++*/
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  public ArrayList<GenericRecord> getListeRecords() {
    return listeRecords;
  }
  
  public void setListeRecords(ArrayList<GenericRecord> listeRecords) {
    this.listeRecords = listeRecords;
  }
  
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
  public String getOrder() {
    return order;
  }
  
  public void setOrder(String order) {
    this.order = order;
  }
  
  public String getChampsBase() {
    return champsBase;
  }
  
  public void setChampsBase(String champsBase) {
    this.champsBase = champsBase;
  }
  
  public String getCriteresBase() {
    return criteresBase;
  }
  
  public void setCriteresBase(String criteresBase) {
    this.criteresBase = criteresBase;
  }
  
  public String getFromSupp() {
    return fromSupp;
  }
  
  public void setFromSupp(String from) {
    this.fromSupp = from;
  }
  
  public String getLimit() {
    return limit;
  }
  
  public void setLimit(String limit) {
    this.limit = limit;
  }
  
  public String getSensTri() {
    return sensTri;
  }
  
  public void setSensTri(String sensTri) {
    this.sensTri = sensTri;
  }
  
  public String getMotCle() {
    return motCle;
  }
  
  public void setMotCle(String motCle) {
    this.motCle = motCle;
  }
  
  public ArrayList<GenericRecord> getFavorisRecords() {
    return favorisRecords;
  }
  
  public void setFavorisRecords(ArrayList<GenericRecord> favorisRecords) {
    this.favorisRecords = favorisRecords;
  }
  
  public GenericRecord getRecordActuel() {
    return recordActuel;
  }
  
  public void setRecordActuel(GenericRecord recordActuel) {
    this.recordActuel = recordActuel;
  }
  
  public int getTypeVue() {
    return typeVue;
  }
  
  public void setTypeVue(int typeVue) {
    this.typeVue = typeVue;
  }
  
  public String[] getListeSelection() {
    return listeSelection;
  }
  
  public void setListeSelection(String[] listeSelection) {
    this.listeSelection = listeSelection;
  }
  
  public String getRechercheSpeciale() {
    return rechercheSpeciale;
  }
  
  public void setRechercheSpeciale(String rechercheSpeciale) {
    this.rechercheSpeciale = rechercheSpeciale;
  }
  
  public String getRequeteMotCle() {
    return requeteMotCle;
  }
  
  public void setRequeteMotCle(String requeteMotCle) {
    this.requeteMotCle = requeteMotCle;
  }
  
  public int getNiveauMetier() {
    return niveauMetier;
  }
  
  public void setNiveauMetier(int niveauMetier) {
    this.niveauMetier = niveauMetier;
  }
  
  public int getNbLignesMax() {
    return nbLignesMax;
  }
  
  public void setNbLignesMax(int nbLignesMax) {
    this.nbLignesMax = nbLignesMax;
  }
  
  public String getTypeTri() {
    return typeTri;
  }
  
  public void setTypeTri(String typeTri) {
    this.typeTri = typeTri;
  }
  
  public int getMetier() {
    return metier;
  }
  
  public void setMetier(int metier) {
    this.metier = metier;
  }
  
  public String getFromBase() {
    return fromBase;
  }
  
  public void setFromBase(String fromBase) {
    this.fromBase = fromBase;
  }
  
  public String getChampsSupp() {
    return champsSupp;
  }
  
  public void setChampsSupp(String champsSupp) {
    this.champsSupp = champsSupp;
  }
  
  public String getCiteresSupp() {
    return citeresSupp;
  }
  
  public void setCiteresSupp(String citeresSupp) {
    this.citeresSupp = citeresSupp;
  }
  
  public Fichier getFichierParent() {
    return fichierParent;
  }
  
  public void setFichierParent(Fichier fichierParent) {
    this.fichierParent = fichierParent;
  }
  
  public String[] getLibIdsMetier() {
    return libIdsMetier;
  }
  
  public void setLibIdsMetier(String[] libIdsMetier) {
    this.libIdsMetier = libIdsMetier;
  }
  
  public ArrayList<Zone> getChampsDeRecherche() {
    return champsDeRecherche;
  }
  
  public void setChampsDeRecherche(ArrayList<Zone> champsDeRecherche) {
    this.champsDeRecherche = champsDeRecherche;
  }
  
  public Zone getZoneEtatFiche() {
    return zoneEtatFiche;
  }
  
  public void setZoneEtatFiche(Zone zoneEtatFiche) {
    this.zoneEtatFiche = zoneEtatFiche;
  }
  
  public HashMap<String, String> getTousEtatsFiches() {
    return tousEtatsFiches;
  }
  
  public void setTousEtatsFiches(HashMap<String, String> tousEtatsFiches) {
    this.tousEtatsFiches = tousEtatsFiches;
  }
  
  public Specifiques getVueCourante() {
    return vueCourante;
  }
  
  public void setVueCourante(Specifiques vueCourante) {
    this.vueCourante = vueCourante;
  }
  
  public Module getModuleEnCours() {
    return moduleEnCours;
  }
  
  public void setModuleEnCours(Module moduleEnCours) {
    this.moduleEnCours = moduleEnCours;
  }
  
}
