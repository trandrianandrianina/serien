/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.article;

import java.util.ArrayList;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.MethodesSpecifiques;
import ri.serien.mobilite.metier.Specifiques;

public class StockDispo extends Specifiques implements MethodesSpecifiques {
  
  public StockDispo() {
    vue = 3;
  }
  
  /**
   * @overwrite
   */
  @Override
  public void construireRequeteListeRecords() {
    mettreAjourFichiersListe(utilisateur.getGestionArticles().getFichierParent());
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    }
    
    if (matriceZonesListe == null) {
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionArticles().getMetier(), vue);
    }
    
    // maj des zones de clé et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesListe));
    
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, false, true));
    // A dynamiter
    // utilisateur.getGestionArticles().setCriteresBase("(ar.A1ETB = '<code>CODEETB</code>' and ar.A1FAM <> '' and
    // ar.A1NPU <> '1')" );
    // utilisateur.getGestionArticles().setCriteresBase(recupererWhere(utilisateur.getGestionArticles().getFichierParent(),vue));
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  /**
   * @overwrite
   */
  @Override
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionArticles().getFichierParent());
    
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    }
    
    if (matriceZonesFiche == null) {
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionArticles().getMetier(), vue);
    }
    
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesFiche));
    
    // utilisateur.getGestionArticles().setFromBase(recupererFichiersFrom(listeFichiersFiche));
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, true, true));
    
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersFiche));
    // TODO Enlever le ORDER en dur et changer la requête pour éviter le problème des listes où la fiche apparait
    // plusieurs fois (car le ORDER est déjà défini dans les listes)
    utilisateur.getGestionArticles().setOrder(" ORDER BY DTATTEN ASC FETCH FIRST 1 ROWS ONLY ");
  }
  
  /**
   * Afficher la liste des articles en vue stock disponible
   */
  @Override
  public String afficherListeRecords(String apport, int nbLignes) {
    // traitement du magasin
    
    // affichage des records
    if (apport.equals("")) {
      if (utilisateur.getGestionArticles().getListeRecords().size() < nbLignes) {
        apport = utilisateur.getGestionArticles().getListeRecords().size() + " r&eacute;sultats";
      }
      else {
        apport = "Plus de " + (utilisateur.getGestionArticles().getListeRecords().size() - 1) + " r&eacute;sultats";
      }
    }
    String retour = "";
    
    if (ConstantesEnvironnement.afficheTousLesStocks) {
      retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Stock disponible (" + utilisateur.getMagasin().getCodeMagasin()
          + ") </span>"
          + "<a class='afficheStock' href=\"javascript:void(0)\" id='gestionDesStocks' onClick=\"gererOuvertureStocks(divInterneMags);\" ><span>Tous les stocks</span></a>"
          + "<span class='apportListe'>" + apport + "</span><div class='triTetieres'><a href='articles?tri="
          + clePrincipale.getZone_code() + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle()
          + "</a><a href='articles?tri=" + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>"
          + valeurPrincipale.getZone_libelle()
          + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    }
    else {
      retour = "<h1 class='tetiereListes'><span class='titreTetiere'>Stock disponible (" + utilisateur.getMagasin().getCodeMagasin()
          + ") </span>"
          // +
          // "<a class='afficheStock' href=\"javascript:void(0)\" id='gestionDesStocks' onClick=\"gererOuvertureStocks(divInterneMags);\"
          // ><span>Tous les stocks</span></a>"
          + "<span class='apportListe'>" + apport + "</span><div class='triTetieres'><a href='articles?tri="
          + clePrincipale.getZone_code() + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle()
          + "</a><a href='articles?tri=" + valeurPrincipale.getZone_code() + "' class='teteListe' id='triValeur'>"
          + valeurPrincipale.getZone_libelle()
          + "</a></div><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
          
    }
    
    retour += "" + "<div id='magasinsPourStocks' >" + afficheListeMagasin(utilisateur) + "</div>";
    
    // Si la liste est vide afficher un message
    if (utilisateur.getGestionArticles().getListeRecords().size() == 0) {
      retour += "<p class='messageListes'>Pas d'article pour ces critères</p>";
    }
    
    boolean isFavori = false;
    String etatFiche = null;
    
    // si la liste n'est pas vide
    for (int i = 0; i < utilisateur.getGestionArticles().getListeRecords().size(); i++) {
      isFavori = utilisateur.getGestionArticles().isUnFavori(utilisateur.getGestionArticles().getListeRecords().get(i));
      etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), utilisateur.getGestionArticles().getListeRecords().get(i));
      
      if (i % 2 == 0) {
        retour += "<div class='listesClassiques'>";
      }
      else {
        retour += "<div class='listesClassiquesF'>";
      }
      if (i < nbLignes - 1) {
        retour += "<a id='article" + i + "' href='"
            + retournerLienVersDetail("articles?", utilisateur.getGestionArticles().getListeRecords().get(i), matriceZonesIds)
            + "' class='detailsListe'>"
            + retournerStructureAffichageListe(utilisateur.getGestionArticles().getListeRecords().get(i), isFavori, etatFiche) + "</a>";
        retour += "<a href='#' class='"
            + utilisateur.getGestionArticles().retournerClasseSelection(
                utilisateur.getGestionArticles().getListeRecords().get(i).getField("A1ETB").toString().trim() + "-"
                    + utilisateur.getGestionArticles().getListeRecords().get(i).getField("A1ART").toString().trim(),
                "selectionArticle", "selectionArticleF")
            + "' onClick=\"switchSelection(this,'selectionArticle','"
            + utilisateur.getGestionArticles().getListeRecords().get(i).getField("A1ETB").toString().trim() + "-"
            + utilisateur.getGestionArticles().getListeRecords().get(i).getField("A1ART").toString().trim() + "');\"></a>";
        
      }
      // si il existe plus d'enregistrements que le max affichable
      else {
        retour += "<a id='plusResultats' href='articles?ancrage=article" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de résultats</a>";
      }
      
      retour += "</div>";
    }
    
    retour +=
        "<script>" + "function gererOuvertureStocks(id){" + "if (document.getElementById('magasinsPourStocks').style.display == 'block') "
            + "  document.getElementById('magasinsPourStocks').style.display = 'none';" + " else"
            + "  document.getElementById('magasinsPourStocks').style.display = 'block';" + "}" + "</script>";
    
    return retour;
    
  }
  
  /**
   * Afficher la fiche article en vue stock disponible
   */
  @Override
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) {
    // Traitement famille
    String codeUnite = "";
    String libUnite = "";
    
    /*try
    {
      famille = utilisateur.getFparametre().getRecordsByType_Code(utilisateur.getEtb(), "FA", utilisateur.getGestionArticles().getRecordActuel().getField("A1FAM").toString().trim()).get(0).getField(5).toString().trim();
    } catch (Exception e) {utilisateur.getLogs().setLog("ECHEC recup&egrave;ration unit&eacute;", "STOCKDISPO", "E"); e.printStackTrace();}*/
    // utilisateur.getMagasin().getCodeMagasin());
    if (utilisateur.getGestionArticles().getRecordActuel().isPresentField("A1UNS")) {
      codeUnite = utilisateur.getGestionArticles().getRecordActuel().getField("A1UNS").toString().trim();
      try {
        IdEtablissement idEtablissement = IdEtablissement.getInstancePourEtablissementVide();
        libUnite =
            utilisateur.getFparametre().getRecordsByType_Code(idEtablissement, "UN", codeUnite).get(0).getField(5).toString().trim();
      }
      catch (Exception e) {
        Trace.erreur(e, "ECHEC recup&egrave;ration unit&eacute;");
      }
    }
    
    String retour = "";
    retour += "<h1><span class='titreH1'><span class='superflus'>Stocks </span>"
        + utilisateur.getGestionArticles().getRecordActuel().getField("A1LIB")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='articles' method='post'>";
    
    // Gestion de l'état de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), utilisateur.getGestionArticles().getRecordActuel());
    if (etatFiche != null) {
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    }
    
    boolean chgtBoite = false;
    boolean gestionStock = utilisateur.getGestionArticles().getRecordActuel().isPresentField("STOCK");
    boolean affPasDeStock = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      // Gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        retour += "<h2>Stocks " + utilisateur.getMagasin().getLibelleMagasin() + "<span id='bonusBoite1'>" + libUnite + "</span></h2>";
      }
      
      if (matriceZonesFiche.get(i).getBloc_zones() == 2 && !chgtBoite) {
        
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori) {
          retour += "<h2>Détails article<span id='isUnFavori'></span></h2>";
        }
        else {
          retour += "<h2>Détails article<span id='isPasUnFavori'></span></h2>";
        }
        
      }
      // Gestion du contenu ++++++++++++++++++++
      // Si il n'y a pas de données suffisantes à l'affichage des stocks
      if (matriceZonesFiche.get(i).getBloc_zones() == 1 && !gestionStock) {
        if (!affPasDeStock) {
          retour += "<p>Pas de gestion de stock pour cet article</p>";
          affPasDeStock = true;
        }
      }
      else {
        // tester si il esxiste un traitement spécifique et retourner la bonne valeur
        retour += utilisateur.getGestionArticles().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
        
      }
      
      if (i == matriceZonesFiche.size() - 1) {
        retour += "</div>";
      }
    }
    if (ConstantesEnvironnement.afficheTousLesStocks) {
      // retour += afficheStockDesMagasins(utilisateur,
      // utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim(),
      // utilisateur.getEtb());
      retour += afficheCumulStockDesMagasins(utilisateur,
          utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim(), utilisateur.getEtablissement().getId());
    }
    
    retour += "</form>";
    
    return retour;
  }
  
  /**
   * Affiche cumul de tous les stocks
   */
  
  public String afficheCumulStockDesMagasins(Utilisateur utilisateur, String article, IdEtablissement pIdEtablissement) {
    
    if (utilisateur == null || article == null) {
      return null;
    }
    String retour = "";
    
    ArrayList<GenericRecord> listeMag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    GenericRecord stockTousMagasin = utilisateur.getGestionArticles().retourneCumulStock(pIdEtablissement, article, listeMag);
    
    // String magEnCours = "";
    String stock = "";
    if (stockTousMagasin != null) {
      retour += "<div id='deuxBoites3' class='sousBoite' >";
      retour += "<h2>Stock des autres magasins<span id='bonusBoite1'></span></h2>";
      retour += "<label class='label200'> Stock  de tous les magasins</label>";
      retour += "<input id='STOCK' class='sortieNum' maxlenght='10' disabled='' value='"
          + utilisateur.getOutils().afficherValeurCorrectement((stockTousMagasin.getField("STK").toString().trim())) + "' />";
      stock = utilisateur.getOutils().afficherValeurCorrectement((stockTousMagasin.getField("STK").toString().trim()));
      
    }
    if (stock.equals("")) {
      retour += "<p>Pas de gestion de stock pour cet article</p>";
    }
    
    retour += "</div>";
    return retour;
  }
  
  /**
   * Affiche les stocks des magasins pour la fiche article
   */
  public String afficheStockDesMagasins(Utilisateur utilisateur, String article, String etb) {
    if (utilisateur == null || article == null) {
      return null;
    }
    String retour = "";
    ArrayList<GenericRecord> listeMag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    String magUser = utilisateur.getMagasin().getCodeMagasin();
    
    ArrayList<GenericRecord> stockTousMagasin = utilisateur.getGestionArticles().retourneStock(etb, article, listeMag);
    
    String magEnCours = "";
    String stock = "";
    if (stockTousMagasin != null && stockTousMagasin.size() > 0) {
      retour += "<div id='deuxBoites3' class='sousBoite' >";
      retour += "<h2>Stock des autres magasins<span id='bonusBoite1'></span></h2>";
      for (int i = 0; i < stockTousMagasin.size(); i++) {
        // on ajoute le libelle magasin
        
        magEnCours = stockTousMagasin.get(i).getField("S1MAG").toString().trim();
        for (int j = 0; j < listeMag.size(); j++) {
          if (stockTousMagasin.get(i).getField("S1MAG").toString().trim().equals(listeMag.get(j).getField("PARIND").toString().trim())) {
            stockTousMagasin.get(i).setField("LIBMAG", listeMag.get(j).getField("LIBETB").toString().trim().toLowerCase());
            
          }
        }
        if (!magUser.equals(magEnCours)) {
          retour += "<label class='label200'> Stock  " + stockTousMagasin.get(i).getField("LIBMAG").toString().trim().toLowerCase() + " ("
              + stockTousMagasin.get(i).getField("S1MAG").toString().trim() + ")</label>";
          retour += "<input id='STOCK' class='sortieNum' maxlenght='10' disabled='' value='"
              + utilisateur.getOutils().afficherValeurCorrectement((stockTousMagasin.get(i).getField("STK").toString().trim())) + "' />";
          stock = utilisateur.getOutils().afficherValeurCorrectement((stockTousMagasin.get(i).getField("STK").toString().trim()));
        }
        else {
          if (stock.equals("")) {
            retour += "<p>Pas de gestion de stock pour cet article</p>";
          }
        }
      }
    }
    else {
      retour += "<p>Pas de gestion de stock pour cet article</p>";
    }
    retour += "</div>";
    return retour;
  }
  
  /**
   * Affiche les stocks des magasins pour la liste
   */
  public String afficheListeMagasin(Utilisateur utilisateur) {
    String retour = "";
    
    ArrayList<GenericRecord> listeMag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    retour += "<form action='articles' name='unMag'>" + "<div id='divInterneMags'>";
    // + "<a href=\"javascript:void(0)\" class='changeMags' onClick=\"gererOuvertureStocks();\" ></a>";
    String lien = null;
    if (listeMag != null && listeMag.size() > 0) {
      for (int i = 1; i < listeMag.size(); i++) {
        lien = "articles?chgMagasin=";
        if (!utilisateur.getMagasin().getCodeMagasin().equals(listeMag.get(i).getField("PARIND").toString().trim())) {
          retour += "<a name='stock' onClick='envoiMagasin()' class='changeMags' href='" + lien
              + listeMag.get(i).getField("PARIND").toString().trim() + "'>"// ;' href='javascript:void(0)'
              + " <span class='libMagChoix' id='pouetMag'>Stock : " + listeMag.get(i).getField("LIBETB").toString().toLowerCase() + " ("
              + listeMag.get(i).getField("PARIND").toString() + ")" + "</span><span class='choisirNvMag'>&nbsp;</span></a>";
          
        }
      }
      
    }
    
    retour += "</div>";
    
    retour += "</form>";
    
    retour += "<script>" + "envoiMagasin(){ var mag=document.getElementById('chgMagasin').value;"
        + "document.forms['unMag'].method='get';" + "document.forms['unMag'].submit();}" + "</script>";
    
    return retour;
    
  }
  
  /*//afficher liste d'article pour le magasin selectionné
  public String afficheListeArticle(Utilisateur utilisateur, String mag) {
      
    GenericRecord listeArticleUnMag= utilisateur.getGestionArticles().retourneUnStock(utilisateur.getEtb(),
        utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim(), mag);
    String stock = listeArticleUnMag.getField("STK").toString().trim();
    String article = listeArticleUnMag.getField("A1ART").toString().trim();
    
    return listeArticleUnMag.toString();
    }*/
  
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++ GETTER SETTER ++++++++++++++++++++++++++++++++++++++++++++*/
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
}
