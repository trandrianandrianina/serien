/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.article;

import java.math.BigDecimal;
import java.util.ArrayList;

import ri.serien.libas400.dao.gvm.programs.GestionTarif;
import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.constantes.ConstantesMetier;
import ri.serien.mobilite.metier.MethodesSpecifiques;
import ri.serien.mobilite.metier.Specifiques;
import ri.serien.mobilite.metier.Zone;

public class PrixVente extends Specifiques implements MethodesSpecifiques {
  // Constantes
  private static final BigDecimal DIVIDOR = new BigDecimal(100);
  private static final BigDecimal DIVIDOR_4_DECIMALES = new BigDecimal(10000);
  
  // Variables
  private GestionTarif gestionTarif = null;
  
  /**
   * Constructeur par défaut
   */
  public PrixVente() {
    vue = 4;
  }
  
  /**
   * @overwrite
   */
  @Override
  public void construireRequeteListeRecords() {
    // maj des fichiers concernés par cette vue
    mettreAjourFichiersListe(utilisateur.getGestionArticles().getFichierParent());
    // maj de la matrice des zones qui sont des ids
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    }
    
    // maj des champs de bases nécessaires à cette vue
    if (matriceZonesListe == null) {
      matriceZonesListe = utilisateur.getBaseSQLITE().getChampsListe(utilisateur.getGestionArticles().getMetier(), vue);
    }
    
    // maj des zones de clé et de valeurs de la liste
    majCleValeursListe(matriceZonesListe);
    
    // maj des champs de bases nécessaires à cette vue
    // TODO A1UNV PROBLEME
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesListe) + ", A1UNV ");
    // maj des fichiers attaqués en SQL et leurs jointures
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, false, true));
    
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersListe));
  }
  
  /**
   * @overwrite
   */
  @Override
  public void construireRequeteUnRecord() {
    mettreAjourFichiersFiche(utilisateur.getGestionArticles().getFichierParent());
    
    if (matriceZonesIds == null) {
      matriceZonesIds = utilisateur.getBaseSQLITE().recupererIdsMetier(utilisateur.getGestionArticles().getFichierParent(), vue);
    }
    
    if (matriceZonesFiche == null) {
      matriceZonesFiche = utilisateur.getBaseSQLITE().getChampsFiche(utilisateur.getGestionArticles().getMetier(), vue);
    }
    
    utilisateur.getGestionArticles().setChampsBase(recupererCodeZones(matriceZonesFiche) + ", A1UNV ");
    
    utilisateur.getGestionArticles()
        .setFromBase(recupererFichiersFrom(utilisateur.getGestionArticles().getFichierParent(), vue, false, true));
    
    utilisateur.getGestionArticles().setCriteresBase(recupererWhere(listeFichiersFiche));
    
    utilisateur.getGestionArticles().setOrder(" FETCH FIRST 1 ROWS ONLY ");
  }
  
  /**
   * @overwrite
   */
  @Override
  public String afficherListeRecords(String apport, int nbLignes) {
    final ArrayList<GenericRecord> listerecord = utilisateur.getGestionArticles().getListeRecords();
    
    if (apport.equals("")) {
      if (listerecord.size() < nbLignes) {
        apport = listerecord.size() + " r&eacute;sultats";
      }
      else {
        apport = "Plus de " + (listerecord.size() - 1) + " r&eacute;sultats";
      }
    }
    
    // On force la valeur principale puisque nous n'obtenons plus le tarif par une requête mais par un appel direct au
    // RPG
    valeurPrincipale = new Zone("PRIXCLIENT", getTitreTetiere(true), 0, 1, 0, 0, 1);
    valeurPrincipale.setZone_type(ConstantesAffichage.TYPE_MONTANT);
    // Gymnastique de merde afin de remettre la valeur principale avant la secondaire sinon problème d'affichage si pas
    // de tarif...
    cleValPriSec.remove(ConstantesAffichage.structureListe[3]);
    cleValPriSec.put(ConstantesAffichage.structureListe[2], valeurPrincipale);
    cleValPriSec.put(ConstantesAffichage.structureListe[3], valeurSecondaire);
    
    // Construction tétière
    String retour = "<h1 class='tetiereListes'>" + "<span class='titreTetiere'>Prix de vente</span>" + "<span class='apportListe'>"
        + apport + "</span>" + "<div class='triTetieres'>" + "<a href='articles?tri=" + clePrincipale.getZone_code()
        + "' class='teteListe' id='triClePrincip'>" + clePrincipale.getZone_libelle() + "</a>" + "<span" + valeurPrincipale.getZone_code()
        + "' class='teteListe' id='triValeur'>" + valeurPrincipale.getZone_libelle() + "</span>" + "</div>"
        + "<a class='optionsListe' href='#' onClick=\"switchOptionsListe();\">"
        + "<img id='imgOptionsListe' src='images/optionsListe.png'/>" + "</a>" + "</h1>" + "<div id='bdrechCli' >"
        + "<div id='borderrechCli'>"
        + "<input name='recherchecli' id='rechCli' type='search' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' placeholder='Recherche client' value='"
        + utilisateur.getInfosClient().getClnom() + "' pattern='[\\S].{1,}[\\S]' onkeyup=\"rechercheCli('rechCli');\" />"
        + "<a id='resetClient' href=\"articles?vue=3&clcli=0\" > </a>" + "<div id='listeClient'> </div>" + "</div>" + "</div>";
    
    // Si la liste est vide afficher un message
    if (listerecord.size() == 0) {
      retour += "<p class='messageListes'>Pas d'article pour ces critères</p>";
    }
    
    boolean isFavori = false;
    String etatFiche = null;
    // Si la liste n'est pas vide
    for (int i = 0; i < listerecord.size(); i++) {
      // TODO A1UNV PROBLEME
      String unite = null;
      if (listerecord.get(i).isPresentField("A1UNV")) {
        unite = listerecord.get(i).getField("A1UNV").toString().trim();
      }
      
      BigDecimal valeur = recherchePrixClient(utilisateur.getInfosClient().getClcli(), utilisateur.getInfosClient().getClliv(),
          (String) listerecord.get(i).getField("A1ART"), unite);
      if (valeur != null) {
        listerecord.get(i).setField("PRIXCLIENT", valeur);
        System.out
            .println("-PrixVente (afficherListeRecords)->" + listerecord.get(i).getField("PRIXCLIENT").toString() + " valeur:" + valeur);
      }
      
      isFavori = utilisateur.getGestionArticles().isUnFavori(listerecord.get(i));
      etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), listerecord.get(i));
      
      if (i % 2 == 0) {
        retour += "<div class='listesClassiques'>";
      }
      else {
        retour += "<div class='listesClassiquesF'>";
      }
      // Si la ligne est inférieur au nombre maximum de lignes
      if (i < nbLignes - 1) {
        retour += "<a id='article" + i + "' href='" + retournerLienVersDetail("articles?", listerecord.get(i), matriceZonesIds)
            + "' class='detailsListe'>" + retournerStructureAffichageListe(listerecord.get(i), isFavori, etatFiche) + "</a>";
        
        retour +=
            "<a href='#' class='"
                + utilisateur.getGestionArticles()
                    .retournerClasseSelection(listerecord.get(i).getField("A1ETB").toString().trim() + "-"
                        + listerecord.get(i).getField("A1ART").toString().trim(), "selectionArticle", "selectionArticleF")
                + "' onClick=\"switchSelection(this,'selectionArticle','" + listerecord.get(i).getField("A1ETB").toString().trim() + "-"
                + listerecord.get(i).getField("A1ART").toString().trim() + "');\"></a>";
      }
      // si il existe plus d'enregistrements que le max affichable
      else {
        retour += "<a id='plusResultats' href='articles?ancrage=article" + i + "&tri=PLUSLIGNES&plusDeLignes=" + nbLignes
            + "'>Afficher plus de résultats</a>";
      }
      
      retour += "</div>";
    }
    return retour;
  }
  
  /**
   * @overwrite
   */
  @Override
  public String afficherLeRecord(boolean isUnFavori, boolean isEnModif) {
    
    String retour = "<h1><span class='titreH1'><span class='superflus'>Prix de vente </span>"
        + utilisateur.getGestionArticles().getRecordActuel().getField("A1LIB")
        + "</span><a class='optionsListe' href='#' onClick=\"switchOptionsListe();\"><img id='imgOptionsListe' src='images/optionsListe.png'/></a></h1>";
    retour += "<form action='articles' method='post'>";
    
    // Gestion de l'état de la fiche et du message d'alerte
    String etatFiche = gestionEtatFiche(utilisateur.getGestionArticles(), utilisateur.getGestionArticles().getRecordActuel());
    if (etatFiche != null) {
      retour += "<div id='alerteFiche'>" + etatFiche + "</div>";
    }
    
    boolean chgtBoite = false;
    boolean gestionPrix = false;
    
    // TODO A1UNV PROBLEME
    String unite = null;
    if (utilisateur.getGestionArticles().getRecordActuel().isPresentField("A1UNV")) {
      unite = utilisateur.getGestionArticles().getRecordActuel().getField("A1UNV").toString().trim();
    }
    
    BigDecimal valeur = recherchePrixClient(utilisateur.getInfosClient().getClcli(), utilisateur.getInfosClient().getClliv(),
        (String) utilisateur.getGestionArticles().getRecordActuel().getField("A1ART"), unite);
    
    if (valeur != null) {
      gestionPrix = true;
      utilisateur.getGestionArticles().getRecordActuel().setField("PRIXCLIENT", valeur);
      // Contrôle de l'existence de la zone dans la matrice et insertion au bon endroit sinon
      boolean trouve = false;
      int index = -1;
      for (int i = 0; i < matriceZonesFiche.size(); i++) {
        if ((index == -1) && (matriceZonesFiche.get(i).getBloc_zones() == 1)) {
          index = i;
        }
        if (matriceZonesFiche.get(i).getZone_code().equals("PRIXCLIENT")) {
          trouve = true;
          break;
        }
      }
      if (!trouve) {
        matriceZonesFiche.add(index, new Zone("PRIXCLIENT", getTitreTetiere(false), 0, 1, 3, 0, 1));
        matriceZonesFiche.get(index).setZone_type(ConstantesAffichage.TYPE_MONTANT);
      }
    }
    boolean affPasDePrix = false;
    
    for (int i = 0; i < matriceZonesFiche.size(); i++) {
      
      // Gestion des boites +++++++++++++++++++
      if (i == 0) {
        retour += "<div class='sousBoite' id='deuxBoites1'>";
        if (gestionPrix) {
          retour += "<h2>Prix de vente<span id='bonusBoite1'>" + ConstantesMetier.MONNAIE + " / HT</span></h2>";
        }
        else {
          retour += "<h2>Prix de vente<span id='bonusBoite1'> HT</span></h2>";
        }
      }
      
      if ((matriceZonesFiche.get(i).getBloc_zones() == 2) && !chgtBoite) {
        chgtBoite = true;
        retour += "</div>";
        retour += "<div class='sousBoite' id='deuxBoites2'>";
        if (isUnFavori) {
          retour += "<h2>Détails article<span id='isUnFavori'></span></h2>";
        }
        else {
          retour += "<h2>Détails article<span id='isPasUnFavori'></span></h2>";
        }
      }
      
      // Gestion du contenu ++++++++++++++++++++
      if ((matriceZonesFiche.get(i).getBloc_zones() == 1) && !gestionPrix) {
        if (!affPasDePrix) {
          retour += "<p>Pas de gestion de prix pour cet article</p>";
          affPasDePrix = true;
        }
      }
      else {
        // tester s'il existe un traitement spécifique et retourner la bonne valeur
        retour += utilisateur.getGestionArticles().traitementSpecMetier(this, matriceZonesFiche.get(i), isEnModif);
      }
      
      if (i == matriceZonesFiche.size() - 1) {
        retour += "</div>";
      }
    }
    
    retour += "</form>";
    
    return retour;
  }
  
  /**
   * Recherche le prix d'un article pour le client en cours
   * @return
   */
  private BigDecimal recherchePrixClient(BigDecimal numcli, BigDecimal livcli, String codeart, String unite) {
    System.out
        .println("-PrixVente (recherchePrixClient)->nimcli:" + numcli + " livcli:" + livcli + " codeart:" + codeart + " unite:" + unite);
    // Instanciation le premier coup
    if (gestionTarif == null) {
      gestionTarif = new GestionTarif(utilisateur.getSysteme(), utilisateur.getLettre_env().charAt(0), utilisateur.getBaseDeDonnees(),
          utilisateur.getBibliothequeEnvironnement());
      gestionTarif.init();
    }
    if (gestionTarif.execute(utilisateur.getEtablissement().getId(), numcli, livcli, codeart, new BigDecimal(1))) {
      BigDecimal valeur = gestionTarif.getTarif().getValue();
      if (valeur.equals(BigDecimal.ZERO)) {
        System.out
            .println(" RETOUR A NULL DE getTARIF de nimcli:" + numcli + " livcli:" + livcli + " codeart:" + codeart + " unite:" + unite);
        return null;
      }
      
      if (unite != null && unite.endsWith("*") && numcli.intValue() > 0) {
        return valeur = valeur.divide(DIVIDOR_4_DECIMALES);
      }
      return valeur = valeur.divide(DIVIDOR);
    }
    return null;
  }
  
  /**
   * Retourne le titre de la tétière concernant les tarifs
   * @param affInfoTVA
   * @return
   */
  private String getTitreTetiere(boolean affInfoTVA) {
    String titre = "Prix client";
    if (utilisateur.getInfosClient().getClnom().equals("")) {
      titre = "Prix public";
    }
    if (affInfoTVA) {
      return titre + " HT";
    }
    return titre;
    
  }
}
