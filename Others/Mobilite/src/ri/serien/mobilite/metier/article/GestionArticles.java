/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.metier.article;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.mobilite.environnement.Connexion_SQLite;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.Gestion;
import ri.serien.mobilite.metier.MethodesGestion;
import ri.serien.mobilite.metier.Specifiques;
import ri.serien.mobilite.metier.Zone;
import ri.serien.mobilite.outils.TriGenericRecords;

/**
 * Classe de gestion métier des articles. Etend la classe de Gestion globale
 * @author ritoudb
 * 
 */
public class GestionArticles extends Gestion implements MethodesGestion {
  private StockDispo stockdispo = null;
  private PrixVente prixVente = null;
  
  /**
   * Constructeur de la gestion des clients
   */
  public GestionArticles(Utilisateur utilisateur) {
    // constructeur de Gestion
    super(utilisateur);
    
    metier = 2;
    fichierParent = utilisateur.getBaseSQLITE().getFichierParent(metier);
    
    order = "ORDER BY a.A1LIB ";
    /*champsBase = "a.A1ETB, a.A1ART, a.A1LIB";
    fromBase = " FROM " + utilisateur.getBibli() + "." + MarbreMetier.FICHIER_ARTICLES + " a ";*/
    
    champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
    fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
        + fichierParent.getFichier_raccou() + " ";
    
    criteresBase = "(" + fichierParent.getFichier_raccou() + ".A1ETB = '" + utilisateur.getEtablissement().getCodeEtablissement()
        + "' and " + fichierParent.getFichier_raccou() + ".A1FAM <> '' and " + fichierParent.getFichier_raccou() + ".A1NPU <> '1')";
    
    // Gestion de l'état d'une fiche
    zoneEtatFiche = utilisateur.getBaseSQLITE().chargerZoneEtatDunMetier(metier);
    tousEtatsFiches = new HashMap<String, String>();
    tousEtatsFiches.put("2", "Article épuisé");
    tousEtatsFiches.put("5", "Article pré-fin de série");
    tousEtatsFiches.put("6", "Article fin de série");
  }
  
  /**
   * Récupérer une liste de records via un mot-clé
   */
  public ArrayList<GenericRecord> gestionMotCle() {
    
    requeteMotCle =
        "SELECT " + champsBase + fromBase + " WHERE " + criteresBase + recuperationDesChampsMotsCle(metier, motCle) + order + limit;
    
    try {
      return utilisateur.getManager().select(requeteMotCle);
    }
    catch (Exception e) {
      
      e.printStackTrace();
      return null;
    }
  }
  
  /**
   * récupérer le détail d'un record
   */
  @Override
  public GenericRecord recupererUnRecord(String[] ids, boolean modeDeBase) {
    if (ids == null || ids.length != 2) {
      return null;
    }
    
    ArrayList<GenericRecord> liste = null;
    
    // Si on a pas de vue particulière
    if (modeDeBase) {
      
      champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
      fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
          + fichierParent.getFichier_raccou() + " ";
      criteresBase = " " + fichierParent.getFichier_raccou() + ".A1ETB <> ''";
      order = "";
    }
    else {
      // A voir si il ne faut pas refaire une gestion des vues à ce niveau
      if (typeVue == 1) {
        // vue générique
      }
      // vue stocks disponibles
      else if (typeVue == 2) {
        if (stockdispo != null) {
          stockdispo.construireRequeteUnRecord();
        }
      }
      // vue Prix
      else if (typeVue == 3) {
        if (prixVente != null) {
          prixVente.construireRequeteUnRecord();
        }
      }
    }
    
    liste = utilisateur.getManager()
        .select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND " + fichierParent.getFichier_raccou() + ".A1ETB = '"
            + ids[0] + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + ids[1] + "' " + order);
    if (liste == null || liste.size() == 0) {
      if (!modeDeBase) {
        recordActuel = recupererUnRecord(ids, true);
      }
      else {
        return null;
      }
    }
    else {
      recordActuel = liste.get(0);
    }
    
    return recordActuel;
  }
  
  /**
   * Récupérer le détail d'un record via deux codes
   */
  public GenericRecord recupererUnRecord(String code1, String code2) {
    if (code1 == null || code2 == null) {
      return null;
    }
    
    String[] tab = { code1, code2 };
    
    return recupererUnRecord(tab, false);
  }
  
  /**
   * En fonction de la vue choisie charger les infos correspondantes
   */
  @Override
  public void gestionVues(String tri) {
    gererTri(tri);
    
    // vue générique
    if (typeVue == 1) {
      champsBase = utilisateur.getBaseSQLITE().getChampsBase(metier);
      fromBase = " FROM " + utilisateur.getBaseDeDonnees() + "." + fichierParent.getFichier_libelle() + " "
          + fichierParent.getFichier_raccou() + " ";
    }
    // vue stocks disponibles
    else if (typeVue == 2) {
      if (stockdispo != null) {
        stockdispo.construireRequeteListeRecords();
        vueCourante = stockdispo;
      }
      
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer coorectement
        typeTri = "A1ART";
        sensTri = "ASC";
      }
    }
    else if (typeVue == 3) {
      if (prixVente != null) {
        prixVente.construireRequeteListeRecords();
        vueCourante = prixVente;
      }
      if (tri == null || tri.equals("")) {
        // TODO A paramétrer coorectement
        typeTri = "A1ART";
        sensTri = "ASC";
      }
    }
    
    order = " ORDER BY " + typeTri + " " + sensTri;
    
  }
  
  /**
   * récupère la liste des favoris de l'utilisateur
   */
  @Override
  public ArrayList<GenericRecord> gestionFavoris(String tri) {
    String[] tabIds = new String[2];
    ArrayList<GenericRecord> listeFinale = new ArrayList<GenericRecord>();
    // Si il existe un tri réel
    if (tri != null && !tri.equals("") && !tri.equals("MEMELIGNES") && !tri.equals("PLUSLIGNES") && !tri.equals("VUE")) {
      TriGenericRecords tg = new TriGenericRecords(listeRecords);
      if (sensTri.equals("ASC") || sensTri.equals("")) {
        listeFinale = tg.descendant(tri);
        // sensTri = "DESC";
      }
      else {
        listeFinale = tg.ascendant(tri);
        // sensTri = "ASC";
      }
    }
    else {
      ArrayList<GenericRecord> liste = null;
      ArrayList<String> listeArticles = utilisateur.getBaseSQLITE().recupererLesArticlesFavoris(utilisateur, sensTri);
      if (listeArticles != null && listeArticles.size() > 0) {
        // On scanne la liste d'articles existante et on la régénère avec les nouvelles données
        for (int i = 0; i < listeArticles.size(); i++) {
          utilisateur.getBaseSQLITE();
          // découper l'ID en AIETB et A1ART
          tabIds = listeArticles.get(i).split(Connexion_SQLite.SEPARATEUR_IDS);
          // si on bien récupérer les IDS SQL sur DB2
          if (tabIds != null && tabIds.length == 2) {
            liste = utilisateur.getManager()
                .select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + " AND " + fichierParent.getFichier_raccou()
                    + ".A1ETB = '" + tabIds[0] + "' AND " + fichierParent.getFichier_raccou() + ".A1ART = '" + tabIds[1] + "' ");
            // Si la requête contient toutes le données demandées
            if (liste != null && liste.size() > 0) {
              listeFinale.add(liste.get(0));
            }
            else {
              // TODO remplacer par les champs de base quand il y aura des champs pas de base
              liste = utilisateur.getManager()
                  .select("SELECT ar.A1ETB, ar.A1ART, ar.A1LIB FROM " + utilisateur.getBaseDeDonnees() + "."
                      + fichierParent.getFichier_libelle() + " " + fichierParent.getFichier_raccou() + " WHERE "
                      + fichierParent.getFichier_raccou() + ".A1ETB = '" + tabIds[0] + "' AND " + fichierParent.getFichier_raccou()
                      + ".A1ART = '" + tabIds[1] + "' ");
              listeFinale.add(liste.get(0));
            }
          }
        }
        
      }
    }
    return listeFinale;
  }
  
  /**
   * Retourne si l'article en cours est un favori ou non en fonction de si on a modifié son état ou non.
   */
  @Override
  public boolean isUnFavori(boolean onInverse) {
    return utilisateur.getBaseSQLITE().gererUnArticleFavori(utilisateur, onInverse);
  }
  
  /**
   * Retourne si l'article en cours est un favori ou non PAS DE MOFIF
   */
  public boolean isUnFavori(GenericRecord record) {
    return utilisateur.getBaseSQLITE().isUnArticleFavori(utilisateur, record);
  }
  
  // Lààà il faut trouver une solution pour cette méthode qui est beaucoup trop lourde et mal fichue... A changer
  // POUAAAAAAAH !!!
  /**
   * Récupère la liste d'articles actuels
   */
  @Override
  public ArrayList<GenericRecord> recuperationDesMemeRecords(String tri) {
    if (listeRecords == null) {
      return null;
    }
    
    ArrayList<GenericRecord> listeFinale = null; // new ArrayList<GenericRecord>();
    
    // monter la liste au lieu de la descendre si il y a un tri
    int i = 0;
    int fin = listeRecords.size();
    int increment = 1;
    if (tri != null && !tri.equals("VUE")) {
      i = listeRecords.size() - 1;
      fin = -1;
      increment = -1;
    }
    
    gestionVues(tri);
    
    // On scanne la liste d'articles existante et on la régénère avec les nouvelles données
    String complementWhere = "";
    
    // adapter le order à la liste
    if (listeRecords.size() > 0) {
      order = " ORDER BY CASE A1ART ";
      
      int j = 1;
      
      while (i != fin) {
        if ((fin == -1 && i == listeRecords.size() - 1) || (fin == listeRecords.size() && i == 0)) {
          complementWhere += " AND ( ";
        }
        else {
          complementWhere += " OR ";
        }
        
        complementWhere += " (ar.A1ETB = '" + listeRecords.get(i).getField("A1ETB").toString() + "'  AND ar.A1ART = '"
            + listeRecords.get(i).getField("A1ART").toString() + "') ";
        order += "WHEN '" + listeRecords.get(i).getField("A1ART").toString() + "' THEN " + j + " ";
        
        // finir le bloc de conditions
        if ((fin == -1 && i == 0) || (fin == listeRecords.size() && i == listeRecords.size() - 1)) {
          complementWhere += " ) ";
          
          criteresBase += complementWhere;
          order += " END ";
        }
        
        i += increment;
        j++;
      }
      
      listeFinale = utilisateur.getManager().select("SELECT " + champsBase + fromBase + " WHERE " + criteresBase + order);
    }
    else {
      order = " ORDER BY A1ART FETCH FIRST 1 ROWS ONLY ";
      listeFinale = new ArrayList<GenericRecord>();
    }
    
    // order);
    
    return listeFinale;
  }
  
  /**
   * En fonction de la vue charger le spécifique demandé
   */
  @Override
  public void setVue(int vue) {
    typeVue = vue;
    switch (typeVue) {
      case 1:
        break;
      case 2:
        if (this.getStockdispo() == null) {
          this.setStockdispo(new StockDispo());
        }
        vueCourante = this.getStockdispo();
        break;
      case 3:
        if (this.getPrixVente() == null) {
          this.setPrixVente(new PrixVente());
        }
        vueCourante = this.getPrixVente();
        break;
    }
  }
  
  // TODO A remplacer par la classe d'affichage numérique
  /**
   * Gérer la décimale stock
   */
  public String gererDecimaleStock(String stock) {
    stock = stock.replaceAll(".000", " ");
    
    return stock;
  }
  
  /**
   * Traitement spécifique aux zones articles
   */
  @Override
  public String traitementSpecMetier(Specifiques spec, Zone zone, boolean isEnModif) {
    String retour = "";
    
    retour += spec.retournerTypeZone(utilisateur.getGestionArticles().getRecordActuel(), zone, isEnModif);
    
    return retour;
  }
  
  /**
   * Requête qui permet de rechercher une liste de client par leur nom
   * @param val
   * @return
   */
  public ArrayList<GenericRecord> traitementRechercheClient(String val) {
    if (val == null) {
      return null;
    }
    String requete = "SELECT CLNOM, CLCLI, CLLIV FROM " + utilisateur.getBaseDeDonnees() + ".PGVMCLIM WHERE CLETB='"
        + utilisateur.getEtablissement().getCodeEtablissement() + "' AND CLNOM like '" + val.toUpperCase() + "%' order by CLNOM, CLLIV";
    
    return utilisateur.getManager().select(requete);
  }
  
  /**
   * Requête qui permet de rechercher une liste de client par leur nom
   * @param clcli
   * @param clliv
   * @return
   */
  public String getClientNameWithClcli(BigDecimal clcli, BigDecimal clliv) {
    if ((clcli == null) || (clliv == null)) {
      return null;
    }
    String requete = "SELECT CLNOM FROM " + utilisateur.getBaseDeDonnees() + ".PGVMCLIM WHERE CLETB='"
        + utilisateur.getEtablissement().getCodeEtablissement() + "' AND CLCLI = " + clcli.intValue() + " AND CLLIV = "
        + clliv.intValue();
    
    return utilisateur.getManager().firstEnrgSelect(requete, "CLNOM");
  }
  
  /**
   * retourne le stock de plusieurs magasins
   */
  public ArrayList<GenericRecord> retourneStock(String etb, String article, ArrayList<GenericRecord> mag) {
    if (etb == null || article == null || mag == null) {
      return null;
    }
    mag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    String mag1 = "";
    String mag2 = "";
    
    if (mag != null && mag.size() > 0) {
      mag1 = mag.get(1).getField("PARIND").toString().trim();
      mag2 = mag.get(2).getField("PARIND").toString().trim();
      
    }
    String requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + " + "S1QEM + S1QSM + S1QDM +"
        + " S1QES + S1QSS + S1QDS- S1RES) AS STK, S1MAG " + " FROM " + utilisateur.getBaseDeDonnees() + ".PGVMSTKM  WHERE S1ETB = '"
        + utilisateur.getEtablissement().getCodeEtablissement() + "' AND S1ART = '" + article + "' " + " AND S1MAG IN ('" + mag1 + "','"
        + mag2 + "') GROUP BY S1MAG,S1ART,S1ETB ";
    utilisateur.getManager().select(requete);
    
    return utilisateur.getManager().select(requete);
  }
  
  /**
   * retourne le cumul de tous les stocks
   */
  public GenericRecord retourneCumulStock(IdEtablissement pIdEtablissement, String article, ArrayList<GenericRecord> mag) {
    if (pIdEtablissement == null || article == null || mag == null) {
      return null;
    }
    mag = utilisateur.getMagasin().retournerListeDesMagasins(utilisateur);
    
    String mag1 = "";
    String mag2 = "";
    
    if (mag != null && mag.size() > 0) {
      mag1 = mag.get(1).getField("PARIND").toString().trim();
      mag2 = mag.get(2).getField("PARIND").toString().trim();
      
    }
    String requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + " + "S1QEM + S1QSM + S1QDM +" + " S1QES + S1QSS + S1QDS- S1RES) AS STK "
        + " FROM " + utilisateur.getBaseDeDonnees() + ".PGVMSTKM  WHERE S1ETB = '" + utilisateur.getEtablissement().getCodeEtablissement()
        + "' AND S1ART = '" + article + "' " + " AND S1MAG IN ('" + mag1 + "','" + mag2 + "')";
    ArrayList<GenericRecord> cumulStock = utilisateur.getManager().select(requete);
    
    return cumulStock.get(0);
  }
  
  /**
   * retourne le stock d'un magasin et d'un article
   */
  public GenericRecord retourneUnStock(String etb, String article, String mag) {
    
    if (etb == null || article == null || mag == null) {
      return null;
    }
    
    String requete = "SELECT SUM(S1STD+ S1QEE + S1QSE + S1QDE + S1QEM + S1QSM + S1QDM +" + " S1QES + S1QSS + S1QDS- S1RES) AS STK, S1MAG "
        + " FROM " + utilisateur.getBaseDeDonnees() + ".PGVMSTKM  WHERE S1ETB = '" + utilisateur.getEtablissement().getCodeEtablissement()
        + "' AND S1ART = '" + article + "' " + " AND S1MAG = '" + mag + "' GROUP BY S1MAG,S1ART,S1ETB ";
    ArrayList<GenericRecord> unStock = utilisateur.getManager().select(requete);
    // utilisateur.getManager().select(requete);
    
    return unStock.get(0);
  }
  
  // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  /*+++++++++++++++++++++++++++++++++++++++++++++ GETTER SETTER ++++++++++++++++++++++++++++++++++++++++++++*/
  /*++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++*/
  
  public StockDispo getStockdispo() {
    return stockdispo;
  }
  
  public void setStockdispo(StockDispo stock) {
    this.stockdispo = stock;
    if (this.stockdispo != null) {
      this.stockdispo.setUtilisateur(utilisateur);
    }
  }
  
  public PrixVente getPrixVente() {
    return prixVente;
  }
  
  public void setPrixVente(PrixVente prixVente) {
    this.prixVente = prixVente;
    if (this.prixVente != null) {
      this.prixVente.setUtilisateur(utilisateur);
    }
  }
  
}
