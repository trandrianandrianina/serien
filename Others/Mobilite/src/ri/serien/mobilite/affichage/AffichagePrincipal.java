/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.backoffice.GestionBOversion;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Classe d'affichage de l'accueil de Série Mobile
 */
public class AffichagePrincipal extends HttpServlet {
  
  private static final String URLReport = "http://resolution-informatique.com:8050";
  
  private String[] infosRecherche = null;
  
  private PatternErgonomie pattern = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichagePrincipal() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse) {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(pHttpServletRequest));
      
      // Effectuer le traitement
      traiterPOSTouGET(pHttpServletRequest, pHttpServletResponse);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(pHttpServletResponse, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(pHttpServletRequest));
      
      // Effectuer le traitement
      traiterPOSTouGET(pHttpServletRequest, pHttpServletResponse);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(pHttpServletResponse, e);
    }
  }
  
  /**
   * Méthode générique permettant de traiter le GET et le POST
   * @throws IOException
   * @throws ServletException
   */
  private void traiterPOSTouGET(HttpServletRequest pHttpServletRequest, HttpServletResponse pHttpServletResponse)
      throws IOException, ServletException {
    ServletOutputStream out = pHttpServletResponse.getOutputStream();
    
    // Récupérer l'utilisateur associé à la session
    Object objetUtilisateur = pHttpServletRequest.getSession().getAttribute("utilisateur");
    if (objetUtilisateur == null || !(objetUtilisateur instanceof Utilisateur)) {
      // Afficher la page de connexion avec erreur
      pHttpServletRequest.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(pHttpServletRequest, pHttpServletResponse);
      return;
    }
    Utilisateur utilisateur = (Utilisateur) objetUtilisateur;
    
    // Contrôler la version du WAR et de la base de données
    int etatVersions = comparerVersionWarEtBdd(utilisateur);
    if (etatVersions == 0) {
      // Afficher la page de mise à jour de la base de données
      out.println(afficherPageMiseAJourBdd((Utilisateur) pHttpServletRequest.getSession().getAttribute("utilisateur")));
      return;
    }
    else if (etatVersions != 1) {
      // Afficher la page de connexion avec erreur
      pHttpServletRequest.getSession().invalidate();
      getServletContext().getRequestDispatcher("/connexion?echec=1").forward(pHttpServletRequest, pHttpServletResponse);
      return;
    }
    
    // Déterminer la page d'accueil à afficher
    String page = "";
    String menu = pHttpServletRequest.getParameter("menu");
    if (menu == null || !menu.equals("G")) {
      page = afficherPageAccueilMobilite(utilisateur, null);
    }
    else {
      page = afficherPageAccueilGeneral(utilisateur, null);
    }
    
    // Vérifier l'encodage de la page
    StringBuffer pageCorrige = new StringBuffer(page);
    for (int i = 0; i < pageCorrige.length(); i++) {
      if (pageCorrige.charAt(i) >= 256) {
        int debutExtrait = Math.max(i - 50, 0);
        int finExtrait = Math.min(i + 50, pageCorrige.length() - 1);
        
        Trace.erreur("Encodage ISO-8859-1 invalide : position=" + i + ", caractère=" + (int) (pageCorrige.charAt(i)) + ", extrait="
            + page.substring(debutExtrait, finExtrait));
        pageCorrige.setCharAt(i, '_');
      }
    }
    
    // Envoyer la page au serveur d'application
    out.println(pageCorrige.toString());
  }
  
  /**
   * Afficher la page d'accueil général.
   */
  private String afficherPageAccueilGeneral(Utilisateur utilisateur, String message) {
    // Tracer l'affichage de cette page
    Trace.info("Afficher la page d'accueil général");
    
    String chaine = "";
    chaine += pattern.afficherEnTete("<link href='css/accueilGeneral.css' rel='stylesheet'/>", null);
    chaine += pattern.afficherHeader(-100, 0, infosRecherche, utilisateur);
    chaine += pattern.afficherOptions(utilisateur, -2, -1, 1, false);
    
    chaine += "<article id='contenu'>";
    if (message != null) {
      chaine += "<div id='messageAccueil'>" + message + "</div>";
    }
    chaine += "<section id='gestionAccueil'>";
    
    // Ajouter le bouton "Mobilité"
    chaine +=
        "<div class='favorisAccueil'><a class='lienFavorisAccueil' href='accueil'><div class='clmob'><div class='grostit'>Mobilité</div><div class='petittit'>Consultation</div></div></a></div>";
    /*
     * Désactivation des boutons WebShop, CRM et Report One car ces boutons ne sont pas totalement opérationnel
     * 
    chaine +=
        "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='mobilitePluch'><div class='clmobplus'><div class='grostit'>Mobilité +</div><div class='petittit'>Saisie devis/Commande</div></div></a></div>";      chaine +=
        "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='crm'><div class='clcrm'><div class='grostit'>CRM</div><div class='petittit'>Action commerciale</div></div></a></div>";
    chaine += "<div class='favorisAccueil'><a  class='lienFavorisAccueil' onclick='return false' id='mobilitePlus' href='" + URLReport
        + "' target='_blank'><div class='clreport'><div class='grostit'>Report <span id='cacheMoi'>Mobilité</span></div><div class='petittit'>Tableau de bord</div></div></a></div>";
    */
    chaine += "</section>";
    
    chaine += "</article> ";
    
    chaine += "</body>";
    chaine += "</html>";
    
    return chaine;
  }
  
  /**
   * Afficher la page d'accueil
   */
  private String afficherPageAccueilMobilite(Utilisateur utilisateur, String message) {
    // Tracer l'affichage de cette page
    Trace.info("Afficher la page d'accueil mobilité");
    
    String chaine = "";
    chaine += pattern.afficherEnTete("<link href='css/accueil.css' rel='stylesheet'/>", null);
    chaine += pattern.afficherHeader(0, 0, infosRecherche, utilisateur);
    chaine += pattern.afficherOptions(utilisateur, -2, -1, 1, false);
    
    chaine += "<article id='contenu'>";
    if (message != null) {
      chaine += "<div id='messageAccueil'>" + message + "</div>";
    }
    chaine += "<section id='gestionAccueil'>";
    if (utilisateur.getListeVue() != null) {
      int nbBlocs = ConstantesAffichage.VUES_ACCUEIL;
      if (utilisateur.getListeVue().size() >= nbBlocs) {
        nbBlocs += ConstantesAffichage.VUES_LIGNES_ACCUEIL;
      }
      
      // Afficher les boutons de vue ou les boutons vides
      for (int i = 0; i < nbBlocs; i++) {
        if (i >= utilisateur.getListeVue().size()) {
          chaine += "<div class='favorisAccueilVide'><a href='catalogue'><span class='superf'>Ajouter une </span>nouvelle vue</a></div>";
        }
        else {
          chaine += "<div class='favorisAccueil'><a href='" + utilisateur.getListeVue().get(i).getUrl() + "'><img src='"
              + utilisateur.getListeVue().get(i).getUrlIcone() + "'><p>" + utilisateur.getListeVue().get(i).getLibelleHTML()
              + "</p></a></div>";
        }
      }
    }
    
    chaine += "</section>";
    
    chaine += "</article> ";
    
    chaine += "</body>";
    chaine += "</html>";
    
    return chaine;
  }
  
  /**
   * Afficher la page au sujet de la mise à jour de la BDD SQLite.
   * 
   * @param pUtilisateur Utilisateur connecté.
   * @return Page générée.
   */
  private String afficherPageMiseAJourBdd(Utilisateur pUtilisateur) {
    String chaine = "";
    
    // Mettre à jour la base de données SQLite
    if (!pUtilisateur.getGestionBOversion().majBDD()) {
      // Afficher un messsage d'erreur car la mise à jour s'est mal passée
      String messageErreur = "Problème lors de la mise à jour de la BDD SQLite. Merci de contacter le service assistance.";
      Trace.erreur(messageErreur);
      chaine += "<div>" + messageErreur + "</div>";
    }
    else {
      // Afficher la page qui récapitule la mise à jour de la BDD
      chaine += afficherPageAccueilMobilite(pUtilisateur, "Nouvelle version "
          + pUtilisateur.getOutils().traduireVersion(ConstantesEnvironnement.versionActuelle) + " installée avec succès");
    }
    
    return chaine;
  }
  
  /**
   * Contrôle les versions de War et de BDD afin de savoir si une mise à jour est nécessaire:
   * - -1 On a un problème de versions on ne met pas à jour
   * - 0 on a une version qui n'est pas à jour
   * - 1 on a une version à jour
   */
  private int comparerVersionWarEtBdd(Utilisateur pUtilisateur) {
    // Instancier la classe de gestion des versions
    if (pUtilisateur.getGestionBOversion() == null) {
      pUtilisateur.setGestionBOversion(new GestionBOversion(pUtilisateur));
    }
    
    // Récupérer la version située dans le WAR et la stocker dans ConstantesEnvironnement.versionActuelle
    if (!pUtilisateur.getGestionBOversion().majNumeroDeVersionSources()) {
      return -1;
    }
    
    // Contrôler la version de la BDD
    return pUtilisateur.getGestionBOversion().controlerVersionBDD();
  }
  
}
