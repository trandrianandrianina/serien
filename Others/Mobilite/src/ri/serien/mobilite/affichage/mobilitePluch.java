/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class mobilitePluch
 */
public class mobilitePluch extends HttpServlet {
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public mobilitePluch() {
    super();
    // TODO Auto-generated constructor stub
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        /*URL url = new URL("http://88.188.171.20:8080/WebShop/accueil");
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();
        
        //connection.setDoOutput( true);
        
        connection.setRequestMethod("POST");
        
        
        /*OutputStreamWriter outputWriter = new OutputStreamWriter(connection.getOutputStream());
        outputWriter.write("pouet");
        outputWriter.flush();
        outputWriter.close();*/
        
        // ServletOutputStream out = response.getOutputStream() ;
        // getServletContext().getRequestDispatcher("../WebShop/accueil").forward(request, response);
        // response.encodeRedirectURL("http://88.188.171.20:8080/WebShop/accueil");
        // request.setAttribute("passUser", "gar1972"); //passUser=gar1972&loginUser=JFSINGA
        // request.setAttribute("loginUser", "JFSINGA");
        // request.getRequestDispatcher("WEB-INF/crm/unContact.jsphttp://88.188.171.20:8080/WebShop/accueil").forward(request,
        // response);
        // response.sendRedirect(response.encodeURL("http://88.188.171.20:8080/WebShop/accueil"));
        
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("text/html");
        out.println("<HTML>");
        out.println("<HEAD>");
        out.println("<meta charset='windows-1252'>");
        out.println("</HEAD>");
        out.println("<BODY style='background-color:#2b2a30;'>");
        out.println("<form action='../WebShop/accueil' name='mobilitePluch' method='POST'>");
        out.println("<input type='hidden' name='loginUser' value='"
            + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getLogin() + "'/>");
        out.println("<input type='hidden' name='passUser' value='"
            + ((Utilisateur) request.getSession().getAttribute("utilisateur")).getMdp() + "'/>");
        out.println("<input type='hidden' name='mobilitePluch' value='1'/>");
        out.println("</form>");
        out.println("<script>");
        out.println("document.forms[\"mobilitePluch\"].submit();");
        out.println("</script>");
        out.println("</BODY>");
        out.println("</HTML>");
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
}
