/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.constantes.ConstantesAffichage;
import ri.serien.mobilite.environnement.Utilisateur;
import ri.serien.mobilite.metier.client.GestionClients;
import ri.serien.mobilite.outils.Outils;

/**
 * Servlet implementation class AffichageContact
 */
public class AffichageContact extends HttpServlet {
  
  private String[] infosRecherche = { "clients", "formClients", "RECHERCHE CLIENT" };
  private PatternErgonomie pattern = null;
  
  /**
   * @see HttpServlet#HttpServlet()
   */
  public AffichageContact() {
    super();
    pattern = new PatternErgonomie();
  }
  
  /**
   * Appelée lors de la construction du servlet par le serveur d'applications.
   * 
   * Cette méthode est appelée une seule fois après l'instanciation de la servlet. Aucun traitement ne peut être effectué par la
   * servlet tant que l'exécution de cette méthode n'est pas terminée.
   */
  @Override
  public void init(ServletConfig config) {
    final String methode = getClass().getSimpleName() + ".init";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode);
      
      // Effectuer le traitement
      super.init(config);
      
      // Tracer de succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
    }
  }
  
  /**
   * Appelée par le serveur d'applications lors d'une demande de récupération d'une ressource web.
   * 
   * C'est la méthode utilisée par le client pour récupérer une ressource web du serveur via une URL. Lorsqu'on saisit une adresse
   * dans la barre d'adresses du navigateur, le navigateur envoie une requête GET pour récupérer la page correspondant à cette adresse
   * et le serveur la lui renvoie. La même chose se passe lorsque si on clique sur un lien.
   * 
   * Lorsqu'il reçoit une telle demande, le serveur ne fait pas que retourner la ressource demandée, il en profite pour l'accompagner
   * d'informations diverses à son sujet, dans ce qui s'appelle les en-têtes ou headers HTTP : typiquement, on y trouve des informations
   * comme la longueur des données renvoyées ou encore la date d'envoi.
   * 
   * Il est possible de transmettre des données au serveur lorsque l'on effectue une requête GET, au travers de paramètres directement
   * placés après l'URL (paramètres nommés query strings) ou de cookies placés dans les en-têtes de la requête. La limite de ce système
   * est que, comme la taille d'une URL est limitée, on ne peut pas utiliser cette méthode pour envoyer des données volumineuses au
   * serveur, par exemple un fichier.
   */
  @Override
  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    final String methode = getClass().getSimpleName() + ".doGet";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * Appelée par le serveur d'applications pour transférer des informations volumineuses au serveur.
   * 
   * La taille du corps du message d'une requête POST n'est pas limitée, c'est donc cette méthode qu'il faut utiliser pour soumettre
   * au serveur des données de tailles variables, ou que l'on sait volumineuses. C'est parfait pour envoyer des fichiers par exemple.
   * 
   * Toujours selon les recommandations d'usage, cette méthode doit être utilisée pour réaliser les opérations qui ont un effet sur
   * la ressource, et qui ne peuvent par conséquent pas être répétées sans l'autorisation explicite de l'utilisateur. Vous avez
   * probablement déjà reçu de votre navigateur un message d'alerte après avoir actualisé une page web, vous prévenant qu'un
   * rafraîchissement de la page entraînera un renvoi des informations : eh bien c'est simplement parce que la page que vous souhaitez
   * recharger a été récupérée via la méthode POST, et que le navigateur vous demande confirmation avant de renvoyer à nouveau la requête.
   */
  @Override
  protected void doPost(HttpServletRequest request, HttpServletResponse response) {
    final String methode = getClass().getSimpleName() + ".doPost";
    
    try {
      // Tracer de début de la méthode
      Trace.soustitre("ENTREE " + methode + " " + Outils.getURL(request));
      
      // Effectuer le traitement
      traiterPOSTouGET(request, response);
      
      // Tracer le succès de la méthode
      Trace.soustitre("SUCCES " + methode);
    }
    catch (Exception e) {
      // Tracer l'échec de la méthode
      Trace.erreur(e, "ECHEC " + methode);
      Outils.afficherPageErreur(response, e);
    }
  }
  
  /**
   * méthode générique au traitements des paramètres passés dans le GET ou le POST
   * @param request
   * @param response
   */
  private void traiterPOSTouGET(HttpServletRequest request, HttpServletResponse response) {
    try {
      // String[] liensOptions = {"#","mailto:","#"};
      String metaSpecifiques = null;
      // verification de session
      if (request.getSession().getAttribute("utilisateur") != null
          && request.getSession().getAttribute("utilisateur") instanceof Utilisateur) {
        ServletOutputStream out = response.getOutputStream();
        // On créé un handler temporaire pour la session
        Utilisateur utilisateur = (Utilisateur) request.getSession().getAttribute("utilisateur");
        // si ce n'est pas fait, accéder à la gestion métier des clients
        if (utilisateur.getGestionClients() == null) {
          utilisateur.setGestionClients(new GestionClients(utilisateur));
        }
        
        // variables
        int vue = 0;
        
        // ++++++++++++++++++++++++++++++++++++ paramètres passés ++++++++++++++++++++++++++++++++++++++++
        
        // Données d'un établissement
        String idSoc = request.getParameter("CLCLI");
        String cont = request.getParameter("numberContact");
        // ancrage dans la page
        String ancrage = request.getParameter("ancrage");
        // niveau métier pour le fil rouge (liste, fiche...)
        if (request.getParameter("niveau") != null) {
          utilisateur.getGestionClients().setNiveauMetier(Integer.parseInt(request.getParameter("niveau")));
        }
        // passer la vue demandée à la gestion des clients
        if (request.getParameter("vue") != null) {
          try {
            vue = Integer.parseInt(request.getParameter("vue"));
          }
          catch (Exception e) {
            vue = 1;
            e.printStackTrace();
          }
          // attribuer la vue à la gestion clients
          utilisateur.getGestionClients().setVue(vue);
        }
        
        // ++++++++++++++++++++++++++++++++++++ Traitements ++++++++++++++++++++++++++++++++++++++++
        
        // Gestion des cookies client
        utilisateur.getGestionClients().setListeSelection(gestionCookies(request));
        
        // sinon changer le niveau métier vers la liste
        utilisateur.getGestionClients().setNiveauMetier(1);
        idSoc = utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim();
        
        metaSpecifiques += "<link href='css/devisClients.css' rel='stylesheet'/>";
        
        out.println(pattern.afficherEnTete(metaSpecifiques, ancrage));
        
        out.println(pattern.afficherHeader(ConstantesAffichage.METIER_CLIENTS, 1, infosRecherche, utilisateur));
        out.println(pattern.afficherOptions(utilisateur, ConstantesAffichage.METIER_CLIENTS, utilisateur.getGestionClients().getTypeVue(),
            utilisateur.getGestionClients().getNiveauMetier(), false));
        
        out.println(afficherDevisPourUnContact(utilisateur, idSoc, cont));
        /*if (request.getParameter("XINUM")!=null)
          //afficher le détail du devis
          out.println(afficherDetailDevis(utilisateur,idSoc, cont,devis ));*/
      }
      else {
        request.getSession().invalidate();
        getServletContext().getRequestDispatcher("/connexion?echec=1").forward(request, response);
      }
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
  
  /**
   * Afficher le détail du client sélectionné
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherUnClient(ServletOutputStream out, Utilisateur utilisateur, String etb, String idSoc, String suff, String page,
      String options, boolean isEnModif) {
    try {
      boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      out.println("<section class='secContenu' id='ficheMetier'>");
      
      utilisateur.getGestionClients().recupererUnRecord(etb, idSoc, suff);
      // traiter les favoris
      isUnFavori = utilisateur.getGestionClients().isUnFavori(options != null && options.equals("favoris"));
      // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
      
      if (utilisateur.getGestionClients().getRecordActuel() == null) {
        out.println("Le client n'a pas &eacute;t&eacute; trouv&eacute;");
      }
      else {
        // récupérer la vue demandée
        switch (utilisateur.getGestionClients().getTypeVue()) {
          case 1:
            break;
          case 2:
            out.println(utilisateur.getGestionClients().getEncours().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 3:
            out.println(utilisateur.getGestionClients().getContacts().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 4:
            out.println(utilisateur.getGestionClients().getDevis().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 5:
            out.println(utilisateur.getGestionClients().getCommandes().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 6:
            out.println(utilisateur.getGestionClients().getBonsLivr().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 7:
            out.println(utilisateur.getGestionClients().getFactures().afficherLeRecord(isUnFavori, isEnModif));
            break;
          case 8:
            out.println(utilisateur.getGestionClients().getFacturesNR().afficherLeRecord(isUnFavori, isEnModif));
            break;
        }
        // out.println(afficherListeContactParClient(utilisateur,idSoc, request));
        out.println(majTailleFiche("ficheMetier"));
        
      }
      out.println("</section>");
      
      out.println("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnClient()");
    }
  }
  
  /**
   * Afficher le détail du client sélectionné
   * @param out
   * @param etb
   * @param idSoc
   * @param suff
   */
  public void afficherDetailModule(ServletOutputStream out, Utilisateur utilisateur, String module, String etb, String idSoc, String suff,
      String page, String options, Map<String, String[]> mapDonnees) {
    try {
      int intModule = 0;
      
      if (module != null) {
        intModule = Integer.parseInt(module);
      }
      // out.println("<a id='boutonRetourModule' href='javascript:window.history.go(-1)'>Retour</a>");
      out.println("<a id='boutonRetourModule' href='javascript:history.back()'>Retour</a>");
      
      // boolean isUnFavori = false;
      out.println("<article id='contenu'>");
      
      out.println("<section class='secContenu' id='moduleMetier'>");
      // PARAMETRER LE MODUUUUUULE PAS BOOOOOOO !!
      utilisateur.getGestionClients().gestionModule(intModule, mapDonnees);
      
      out.println(utilisateur.getGestionClients().getModuleEnCours().afficherLeRecord());
      
      out.println(majTailleFiche("moduleMetier"));
      
      out.println("</section>");
      
      out.println("</article>");
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherDetailModule()");
    }
  }
  
  /**
   * récupère les cookies du navigateur pour en récupérer les valeurs correspondantes à notre page
   */
  private String[] gestionCookies(HttpServletRequest request) {
    String[] idClients = null;
    // récupération des cookies du navigateur
    Cookie[] cookies = request.getCookies();
    // trouver notre cookie et récupérer ses valeurs
    for (int i = 0; i < cookies.length; i++) {
      if (cookies[i].getName().equals("selectionClient")) {
        String valeurs = cookies[i].getValue();
        if (valeurs != null && !valeurs.trim().equals("")) {
          idClients = valeurs.split("_");
        }
      }
    }
    
    return idClients;
  }
  
  private String majTailleFiche(String id) {
    String retour = "<script>";
    retour += "majTailleFiche(\"" + id + "\");";
    retour += "</script>";
    
    return retour;
  }
  
  public String afficherDevisPourUnContact(Utilisateur utilisateur, String idSoc, String numContact) {
    
    String retour = "";
    try {
      retour += ("<article id='contenu'>");
      retour += ("<section class='secContenu' id='ficheMetier'>");
      ArrayList<GenericRecord> listeDevis = utilisateur.getGestionClients().getDevis().listeDevisPourUnContact(idSoc, numContact);
      
      String nomCli = (String) listeDevis.get(0).getField("RECL1");
      
      retour += "<div id='module' class='sousBoite'>";
      // retour += "<span class='grosTitre'> Les devis</span>";
      retour += "<a id='boutonRetourModule' href='javascript:history.back()'>Retour</a>";
      // retour += "<span class= 'titreH1'>";
      retour += "<span class='grosTitre'>Devis du contact : " + nomCli + "</span>";
      
      retour +=
          "<div class='enteteListeModule'>" + "<span id='titreNumero'>Numéro </span>" + "<span id='titreRefLongue'>Réf. longue</span>"
              + "<span id='titreValidité'>date de validité</span>" + "<span id='titreMontant'>Montant HT en Euro</span></div>";
      
      for (int i = 0; i < listeDevis.size(); i++) {
        if (listeDevis.get(i).getField("XINUM") != null) {
          // Entête de liste de module
          
          retour += "<div class='listes'>";
          // le numéro de devis
          retour += "<a class='titreNumero' href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";//
          // clients?module=7&E1COD=D&E1ETB=002&E1NUM=116914&E1SUF=0
          
          if (listeDevis.get(i).isPresentField("XINUM")) {
            retour += listeDevis.get(i).getField("XINUM").toString().trim();
          }
          else {
            retour += ("aucune");
          }
          retour += ("</a>");
          
          // la ref longue
          retour += "<a class='titreRefLongue' href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";
          if (listeDevis.get(i).isPresentField("E1RCC")) {
            retour += listeDevis.get(i).getField("E1RCC").toString().trim();
          }
          else {
            retour += ("aucune");
          }
          retour += ("</a>");
          
          // la validité du devis
          retour += "<a class='titreValidité 'href='clients?module=7&E1COD=D&E1ETB=" + listeDevis.get(i).getField("E1ETB") + "&E1NUM="
              + listeDevis.get(i).getField("XINUM") + "&E1SUF=" + listeDevis.get(i).getField("E1SUF") + "'>";
          if (listeDevis.get(i).isPresentField("E1HOM")) {
            retour += utilisateur.getOutils().TransformerEnDateHumaine(listeDevis.get(i).getField("E1HOM").toString().trim());
          }
          else {
            retour += ("aucune");
          }
          retour += ("</a>");
          
          // le montant ht par ligne
          retour += "<a class='titreMontant' href='#'>";
          if (listeDevis.get(i).isPresentField("E1THTL")) {
            retour += listeDevis.get(i).getField("E1THTL").toString().trim();
          }
          else {
            retour += ("aucune");
          }
          retour += ("</a>");
          
        }
        else {
          retour += "<p>Aucun devis pour ce contact</p>";
        }
        // afficher les devis de tout le monde
        retour += "</div>";
      }
      
      retour += ("</section>");
      
      retour += ("</article>");
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnClient()");
    }
    return retour;
  }
  
  public String afficherDetailDevis(Utilisateur utilisateur, String idSoc, String numContact, String devis) {
    String retour = "";
    try {
      retour += ("<article id='contenu'>");
      // retour+=("<section class='secContenu' id='ficheMetier'>");
      ArrayList<GenericRecord> listeDetailDevis = utilisateur.getGestionClients().getDevis().detailDuDevis(idSoc, numContact, devis);
      
      for (int i = 0; i < listeDetailDevis.size(); i++) {
        retour += "<label class='labelNumero' >Numéro/suffixe</label>";
        retour += "<input type='text' name=nbrDevis value='" + listeDetailDevis.get(i).getField("XINUM") + "'/>";
        
      }
      retour += ("</article>");
      
    }
    catch (Exception e) {
      Trace.erreur(e, "Erreur sur afficherUnClient()");
    }
    return retour;
  }
}
