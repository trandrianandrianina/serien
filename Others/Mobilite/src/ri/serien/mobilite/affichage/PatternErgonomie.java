/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.affichage;

import java.util.ArrayList;

import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.environnement.Utilisateur;

public class PatternErgonomie {
  private ArrayList<FilRouge> filsRouges = null;
  private ArrayList<String> recherchesListes = null;
  private Options fenetreOptions = null;
  // ce pattern doit commencer par un caractère non blanc puis autant de caractères que souhaités pour finalement
  // terminé par un caractère non blanc
  private String patternSaisie = "[\\S].{1,}[\\S]";
  
  /**
   * Constructeur par défaut
   */
  public PatternErgonomie() {
    filsRouges = gestionFilRouge();
    // recherchesListes = gestionRecherches();
    fenetreOptions = new Options();
  }
  
  /**
   * Afficher l'entête de la page Web
   */
  public String afficherEnTete(String metaSpecifiques, String ancrage) {
    String chaine = "";
    chaine += "<!DOCTYPE html>";
    chaine += "<html lang='fr'>";
    chaine += "<head>";
    chaine += gestionModeApp();
    /*("<meta charset='utf-8'>");*/
    chaine += "<meta charset='windows-1252'>";
    chaine += "<meta name='apple-mobile-web-app-capable' content='yes'>";
    chaine += "<meta HTTP-EQUIV='CACHE-CONTROL' content='NO-CACHE'>";
    chaine += "<meta HTTP-EQUIV='PRAGMA' content='NO-CACHE'>";
    chaine += "<meta HTTP-EQUIV='EXPIRES' content='0'>";
    chaine += "<meta name='ROBOTS' content='NONE'> ";
    chaine += "<meta name='GOOGLEBOT' content='NOARCHIVE'>";
    chaine +=
        "<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />";
    // chaine += "<meta name='viewport' content='width=device-width; user-scalable=no; minimum-scale = 1; maximum-scale
    // = 1;' />"; height=device-height,
    chaine += "<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />";
    chaine += "<title>S&eacute;rie N mobilit&eacute;</title>";
    chaine += "<script src='scripts/general.js?" + ConstantesEnvironnement.VERSION_JS + "'></script>";
    chaine += "<link href='css/css_mobile.css' rel='stylesheet'/>";
    chaine += "<link rel=\"apple-touch-icon\" href=\"touch-icon-iphone.png\">";
    chaine += "<link rel=\"apple-touch-icon\" sizes=\"76x76\" href=\"touch-icon-ipad.png\">";
    chaine += "<link rel=\"apple-touch-icon\" sizes=\"120x120\" href=\"touch-icon-iphone-retina.png\">";
    chaine += "<link rel=\"apple-touch-icon\" sizes=\"152x152\" href=\"touch-icon-ipad-retina.png\">";
    chaine += "<link rel=\"shortcut icon\" href=\"images/favicon.png\" />";
    
    if (metaSpecifiques != null) {
      chaine += metaSpecifiques;
    }
    chaine += "</head>";
    
    if (ancrage == null || ancrage.trim().equals("")) {
      ancrage = "onLoad=\"fixerViewPort(); \"";
    }
    else {
      ancrage = "onLoad=\"fixerViewPort(); location.hash='#" + ancrage + "'\"";
    }
    chaine += "<body " + ancrage + ">";
    return chaine;
  }
  
  /**
   * Afficher le bloc propre au header, fil rouge et à la recherche globale
   */
  public String afficherHeader(int metier, int niveau, String[] infosRecherche, Utilisateur utilisateur) {
    String chaine = "";
    chaine += "<header>";
    chaine += "<img id='figRI' src='images/ri.png' alt='r&eacute;solution informatique'>";
    if (metier == -2) {
      chaine += "<h1><span id='h1_BO'>Back-office </span>S&eacute;rie N <span>mobilit&eacute;</span></h1>";
    }
    else {
      chaine += "<h1><span id='suiteH1'>Suite &nbsp;</span>S&eacute;rie N <span>mobilit&eacute;</span></h1>";
    }
    if (metier != -9999) {
      if (metier == -100) {
        chaine += "<p id='session'>" + utilisateur.getLogin() + " (" + utilisateur.getBaseDeDonnees() + ")</p>";
        chaine += "<nav id='filRouge'>";
        chaine += "<span class=\"fils\" id=\"filSelect\">Accueil</span>";
        chaine +=
            "<a href=\"connexion?connexion=stop\" class=\"filsLien\"><span class=\"textFil\">Quitter<span class=\"superf\"> l'application</span></span><img class=\"imgFil\" src=\"images/quitter.png\"></a>";
        chaine +=
            "<div id='versionLogiciel'>" + utilisateur.getOutils().traduireVersion(ConstantesEnvironnement.versionActuelle) + "</div>";
        chaine += "</nav>";
      }
      else {
        // Pour le menu général
        chaine += "<p id='session'>" + utilisateur.getLogin() + " (" + utilisateur.getBaseDeDonnees() + ")</p>";
        chaine += "<nav id='filRouge'>";
        chaine += afficherFilRouge(metier, niveau, utilisateur);
        if (infosRecherche != null) {
          chaine += "<form action='" + infosRecherche[0] + "' id='formRechGlob' name='" + infosRecherche[1]
              + "' method='post' onSubmit='return controlerSaisieRecherche(this,\"rechGlob\");'><input name ='recherche' id='rechGlob' type='search' autocomplete='off' autocorrect='off' autocapitalize='off' spellcheck='false' placeholder='"
              + infosRecherche[2] + "' title='minimum 3 caract&egrave;res alphanum&eacute;riques non blancs' pattern='" + patternSaisie
              + "' onFocus=\" gererFocus(this); \" /></form>";
        }
        else if (metier == 0) {
          chaine +=
              "<div id='versionLogiciel'>" + utilisateur.getOutils().traduireVersion(ConstantesEnvironnement.versionActuelle) + "</div>";
        }
        // chaine += "";
        chaine += "</nav>";
      }
    }
    chaine += "</header>";
    
    return chaine;
  }
  
  /**
   * Afficher le fil rouge de l'application en fonction du niveau métier : liste, fiche...
   */
  protected String afficherFilRouge(int metier, int niveau, Utilisateur utilisateur) {
    String liens = "";
    if (filsRouges == null) {
      return "";
      // int increment =1;
    }
    
    // si on est pas à l'accueil
    if (niveau != 0) {
      // scanner l'arbre des fils rouges
      int i = 0;
      while (i < filsRouges.size() && i >= 0) {
        // Mobilité niveau 0
        if (i == 1) {
          liens = "<a href='" + filsRouges.get(i).getLien() + "' class='filsLien'><span class='textFil'>"
              + filsRouges.get(i).getDenomination() + "</span><img class='imgFil' src='" + filsRouges.get(i).getImage() + "'/></a>";
          // increment ++;
        }
        // si on est dans la branche métier recherchée
        else if (filsRouges.get(i).getMetier() == metier) {
          // si on est le niveau courant
          if (filsRouges.get(i).getNiveauFil() == niveau) {
            liens += "<span class='fils' id='filSelect'>" + filsRouges.get(i).getDenomination() + "</span>";
            i = -2;
          }
          else {
            // denom="+filsRouges.get(i).getDenomination());
            liens += "<a href='" + filsRouges.get(i).getLien() + "' class='filsLien'><span class='textFil'>"
                + filsRouges.get(i).getDenomination() + "</span><img class='imgFil' src='" + filsRouges.get(i).getImage() + "'/></a>";
          }
          // increment ++;
        }
        i++;
      }
    }
    // Gestion de l'accueil
    else {
      int i = 0;
      while (i < filsRouges.size()) {
        if (i == 1) {
          liens += "<span class='fils' id='filSelect'>" + filsRouges.get(i).getDenomination() + "</span>";
        }
        else if (filsRouges.get(i).getMetier() == metier) {
          // tester si l'utilisateur a accès à cet onglet
          if (filsRouges.get(i).getNiveauAcces() >= utilisateur.getNiveauAcces()) {
            liens += "<a href='" + filsRouges.get(i).getLien() + "' class='filsLien'><span class='textFil'>"
                + filsRouges.get(i).getDenomination() + "</span><img class='imgFil' src='" + filsRouges.get(i).getImage() + "'/></a>";
            // denom="+filsRouges.get(i).getDenomination());
            // increment ++;
          }
        }
        i++;
      }
    }
    
    return liens;
  }
  
  /**
   * Afficher le footer de la page Web
   */
  public String afficherFin(String titre, String[] liensOptions) {
    String chaine = "";
    if (liensOptions != null) {
      chaine += fenetreOptions.retournerFenetreOptions(titre, liensOptions);
    }
    chaine += "  </body>";
    chaine += "</html>";
    
    return chaine;
  }
  
  /**
   * Donne accès aux fil rouge logique
   * a revoir avec un ficher XML ou la bdd light
   */
  private ArrayList<FilRouge> gestionFilRouge() {
    ArrayList<FilRouge> liste = new ArrayList<FilRouge>();
    
    FilRouge filR = new FilRouge(1, 0, "Accueil", "affichagePrincipal?menu=G", "images/accueil.png", 3);
    FilRouge fil0 = new FilRouge(0, 0, "Mobilité", "accueil", "images/mobiliteP.png", 3); // ?menu=G
    FilRouge fil01 = new FilRouge(1, 0, "Param&egrave;tres", "parametres", "images/parametres.png", 2);
    // FilRouge fil01 = new FilRouge(1, 0, "Param&egrave;tres", "parametres", "images/parametres.png");
    FilRouge fil02 = new FilRouge(1, 0, "<span class='superf'>Catalogue de </span>vues", "catalogue", "images/catalogueMini.png", 3);
    // FilRouge fil03 = new FilRouge(1, 0, "Quitter<span class='superf'> l'application</span>",
    // "connexion?connexion=stop", "images/quitter.png",4);
    FilRouge fil1 = new FilRouge(1, 1, "Liste <span class='superf'>de clients</span>", "clients?page=retour", "images/liste.png", 3);
    FilRouge fil2 = new FilRouge(2, 1, "Fiche <span class='superf'>client</span>", "clients", null, 3);
    FilRouge fil3 = new FilRouge(1, -1, "<span class='superf'>Catalogue de </span>vues", "catalogue", "images/catalogueMini.png", 3);
    FilRouge fil4 = new FilRouge(2, -1, "Fiches", "catalogue?page=catalogue", "images/liste.png", 3);
    FilRouge fil5 = new FilRouge(3, -1, "Vues", "catalogue", "images/liste.png", 3);
    FilRouge fil6 = new FilRouge(1, 2, "Liste <span class='superf'>d' articles</span>", "articles?page=retour", "images/liste.png", 3);
    FilRouge fil7 = new FilRouge(2, 2, "Fiche <span class='superf'>article</span>", "articles", null, 3);
    FilRouge fil8 = new FilRouge(1, -2, "Les param&egrave;tres", "parametres", "images/parametres.png", 3);
    FilRouge fil9 = new FilRouge(2, -2, "<span class='superf'>Les </span>utilisateurs", "affichageBOutilisateurs", "images/liste.png", 3);
    FilRouge fil10 =
        new FilRouge(3, -2, "Fiche <span class='superf'>utilisateur</span>", "affichageBOutilisateurs", "images/catalogueMini.png", 3);
    FilRouge fil11 = new FilRouge(1, -3, "Les param&egrave;tres", "parametres", "images/parametres.png", 3);
    FilRouge fil12 = new FilRouge(2, -3, "Tous les logs", "affichageBOlogs", "images/liste.png", 3);
    FilRouge fil13 = new FilRouge(3, -3, "Un log", "affichageBOlogs", "images/catalogueMini.png", 3);
    
    liste.add(filR);
    liste.add(fil0);
    liste.add(fil02);
    liste.add(fil01);
    // liste.add(fil03);
    liste.add(fil1);
    liste.add(fil2);
    liste.add(fil3);
    liste.add(fil4);
    liste.add(fil5);
    liste.add(fil6);
    liste.add(fil7);
    liste.add(fil8);
    liste.add(fil9);
    liste.add(fil10);
    liste.add(fil11);
    liste.add(fil12);
    liste.add(fil13);
    
    return liste;
  }
  
  /**
   * Afficher les options inhérentes à la page chargée: vues et recherches
   */
  protected String afficherOptions(Utilisateur utilisateur, int metier, int vue, int niveau, boolean isModeAjax) {
    String options = "";
    if (!isModeAjax) {
      options += "<nav id='options'>";
    }
    // Gestion des vues
    /*if(niveau==1)
      options+= "<div class='h1Options'>Vues</div>";*/
    for (int i = 0; i < utilisateur.getListeVue().size(); i++) {
      // trier les options en fonction du métier demandé ou alors page d'accueil
      if ((utilisateur.getListeVue().get(i).getIdGroupeMetier() == metier && utilisateur.getListeVue().get(i).getCode() != vue)
          || (metier <= 0 && utilisateur.getListeVue().get(i).getCode() != 1)) {
        // utilisateur.getMesVues().get(i).getIdVue()!=1)
        options += "<a class='lienBtLinks' href='" + utilisateur.getListeVue().get(i).getUrl() + "'><img src='"
            + utilisateur.getListeVue().get(i).getUrlIcone() + "'><span class='labelLinks'>"
            + utilisateur.getListeVue().get(i).getLibelleHTML() + "</a>";
      }
      else if (utilisateur.getListeVue().get(i).getIdGroupeMetier() == metier && utilisateur.getListeVue().get(i).getCode() == vue) {
        options +=
            "<div class='lienBtLinks'><img src='" + accederImageVersionOptionInverse(utilisateur.getListeVue().get(i).getUrlIcone())
                + "'><span class='labelLinks'>" + utilisateur.getListeVue().get(i).getLibelleHTML() + "</div>";
      }
    }
    
    // Gestion des recherches/tri
    recherchesListes = gestionRecherches(metier);
    if (niveau == 1) {
      boolean existeUneSelection = false;
      if (metier == 1 && utilisateur.getGestionClients().getListeSelection() != null
          && utilisateur.getGestionClients().getListeSelection().length > 0) {
        existeUneSelection = true;
      }
      else if (metier == 2 && utilisateur.getGestionArticles().getListeSelection() != null
          && utilisateur.getGestionArticles().getListeSelection().length > 0) {
        existeUneSelection = true;
      }
      
      options += "<div class='h1Options'></div>";
      // afficher les tris
      for (int i = 0; i < recherchesListes.size(); i++) {
        // A AMELIOREEEEEEER !!
        // montrer ou non le tri en fonction des cookies
        if ((existeUneSelection && i == 0) || i > 0) {
          options += "<a class='lienBtLinks' " + recherchesListes.get(i) + "</a>";
        }
        else {
          options += "<a class='lienBtLinksC' " + recherchesListes.get(i) + "</a>";
        }
      }
    }
    options += "<div class='h1Options'></div>";
    // Options de navigation dans la suite
    // special ARINFO on laisse active que la mobilite, le reste on désactive
    // Pour faire apparaitre les liens changer le nom de la class "lienBtLinksC" en "lienBtLinks"
    options += "<a class='lienBtLinksC' href='#'><img src='images/mobilite+.png'><span class='labelLinks'>Mobilité +</span></a>";
    options += "<a class='lienBtLinksC' href='#'><img src='images/report.png'><span class='labelLinks'>Report One</span></a>";
    options += "<a class='lienBtLinksC' href='#'><img src='images/crmGros.png'><span class='labelLinks'>CRM</span></a>";// crm
    
    if (!isModeAjax) {
      options += "</nav>";
    }
    return options;
  }
  
  /**
   * permet d'accéder à la version catalogue de l'image demandée
   */
  public String accederImageVersionOptionInverse(String image) {
    if (image == null) {
      return null;
    }
    
    image = image.replace(".png", "_inverse.png");
    
    return image;
  }
  
  /**
   * retourne la liste des tris
   * A confier à un fichier XML ou une BDD light
   */
  private ArrayList<String> gestionRecherches(int metier) {
    String lienMetier = "#";
    switch (metier) {
      case 1:
        lienMetier = "clients";
        break;
      case 2:
        lienMetier = "articles";
        break;
    }
    
    ArrayList<String> liste = new ArrayList<String>();
    String rech1 = "href='" + lienMetier
        + "?special=sel' id='rechSelection'><img src='images/rechercheSelection.png'><span class='labelLinks'><span class='superf'>Ma </span>s&eacute;lection";
    String rech2 = "href='" + lienMetier
        + "?special=fav' id='rechFavoris'><img src='images/rechercheFavoris.png'><span class='labelLinks'><span class='superf'>Mes </span>favoris";
    // String rech3 = "href='#' id='rechMiens'><img src=''><span class='labelLinks'>Mes clients";
    // String rech4 = "href='#' id='rechAvance'><img src=''><span class='labelLinks'>Recherche avancée";
    
    liste.add(rech1);
    liste.add(rech2);
    // liste.add(rech3);
    // liste.add(rech4);
    
    return liste;
  }
  
  private String gestionModeApp() {
    String retour = "";
    
    retour += "<script type=\"text/javascript\">";
    // liens internes
    retour += "(function(document,navigator,standalone) {";
    retour += "if ((standalone in navigator) && navigator [standalone] ) { ";
    retour += "var curnode, location=document.location, stop=/^(a|html)$/i;";
    retour += "document.addEventListener('click', function(e) {";
    retour += "curnode=e.target;";
    retour += "while (!(stop).test(curnode.nodeName)) {";
    retour += "curnode=curnode.parentNode;}";
    retour += "if('href' in curnode && ( curnode.href.indexOf('http') || ~curnode.href.indexOf(location.host) ) ) {";
    retour += "e.preventDefault();";
    retour += "location.href = curnode.href;}";
    retour += "},false);}";
    retour += "})(document,window.navigator,'standalone');";
    
    retour += "</script>";
    
    return retour;
  }
}
