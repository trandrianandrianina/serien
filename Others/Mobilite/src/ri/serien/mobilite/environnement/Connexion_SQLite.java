/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.environnement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import ri.serien.libas400.database.record.GenericRecord;
import ri.serien.libcommun.outils.MessageErreurException;
import ri.serien.libcommun.outils.Trace;
import ri.serien.mobilite.backoffice.Parametre;
import ri.serien.mobilite.constantes.ConstantesEnvironnement;
import ri.serien.mobilite.metier.Catalogue;
import ri.serien.mobilite.metier.Fichier;
import ri.serien.mobilite.metier.Metier;
import ri.serien.mobilite.metier.Vue;
import ri.serien.mobilite.metier.Zone;

public class Connexion_SQLite {
  private static final String PREFIXE_SQL = "[SQLITE] ";
  
  private Connection maConnexion = null;
  private String cheminDataBase = null;
  private Utilisateur utilisateur = null;
  public static final String SEPARATEUR_IDS = "@-@";
  
  /**
   * Constructeur de la connexion à la base SQLITE
   */
  public Connexion_SQLite(String database, Utilisateur utilisateur) {
    cheminDataBase = database;
    maConnexion = createConnection();
    try {
      if (maConnexion != null && !maConnexion.isClosed()) {
        // stat = maConnexion.createStatement();
        if (utilisateur != null) {
          this.utilisateur = utilisateur;
        }
      }
      else {
        Trace.erreur("ECHEC connexion SQLITE de l'utilisateur 1");
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC connexion SQLITE de l'utilisateur 2");
    }
  }
  
  /**
   * Vérifier si la connexion SQLITE est OP
   */
  private boolean isConnecteCorrectement() {
    try {
      return ((maConnexion != null) && !maConnexion.isClosed());
    }
    catch (SQLException e) {
      Trace.erreur(e, "isConnecteCorrectement()");
      return false;
    }
  }
  
  /**
   * Recréer la connexion et le statement
   */
  private void majDesConnexionsSQLITE() {
    // LA CONNEXION EST MOISIE
    if (!isConnecteCorrectement()) {
      if (stopConnection()) {
        maConnexion = createConnection();
      }
    }
  }
  
  /**
   * Permet de se connecter via un JDBC à la BDD SQLITE
   */
  public Connection createConnection() {
    try {
      Class.forName("org.sqlite.JDBC");
      return DriverManager.getConnection("jdbc:sqlite:" + cheminDataBase);
    }
    catch (Exception e) {
      Trace.erreur(e, "ECHEC createConnection()");
      return null;
    }
  }
  
  /**
   * Fermer la connection à la BDD SQLITE
   */
  public boolean stopConnection() {
    try {
      // resultSet.close();
      // if(stat!=null) stat.close();
      if (maConnexion != null) {
        maConnexion.close();
      }
      return true;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC stopConnection()");
      return false;
    }
  }
  
  /**
   * Exécuter une requête de lecture de données (SELECT) avec gestion des traces.
   */
  private ResultSet executerSelect(Statement pStatement, String pRequeteSql) throws SQLException {
    try {
      Trace.info(PREFIXE_SQL + pRequeteSql);
      return pStatement.executeQuery(pRequeteSql);
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors de l'exécution de la requête SQL");
      throw e;
    }
  }
  
  /**
   * Exécuter une requête de mise à jour de données (INSERT, UPDATE, DELETE) avec gestion des traces.
   */
  private int executerUpdate(Statement pStatement, String pRequeteSql) throws SQLException {
    try {
      Trace.info(PREFIXE_SQL + pRequeteSql);
      return pStatement.executeUpdate(pRequeteSql);
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors de l'exécution de la requête SQL");
      throw e;
    }
  }
  
  /**
   * Charger l'utilisateur.
   * 
   * Vérifie la présence de l'utilisateur dans la BDD SQLITE et récupère les données inhérentes à celui-ci. Si l'utilisateur n'existe
   * pas, il ets créé.
   */
  public void chargerUtilisateur(Utilisateur pUtilisateur) {
    // Vérifier les paramètres
    if (pUtilisateur == null) {
      throw new MessageErreurException("L'utilisateur est invalide");
    }
    
    try {
      // Mettre à jour la connexion à SQLite
      majDesConnexionsSQLITE();
      
      Statement statement = maConnexion.createStatement();
      
      // Exécuter la requête
      ResultSet resultSet = executerSelect(statement,
          "SELECT util_id, util_acces, util_super, util_repres FROM utilisateur WHERE util_login = '" + pUtilisateur.getLogin() + "';");
      
      // Tester s'il y a un résultat
      if (resultSet.next()) {
        // Renseigner l'identifiant de l'utilisateur
        pUtilisateur.setId(resultSet.getInt(1));
        
        // Renseigner le niveau d'accès aux paramètres de la mobilité
        pUtilisateur.setNiveauAcces(resultSet.getInt(2));
        
        // Renseigner si l'utilisateur est un superutilisateur
        pUtilisateur.setUnSuperUtilisateur(resultSet.getInt(3) == 1);
        
        // Renseigner le représentant
        String codeRepresentant = resultSet.getString(4);
        if (codeRepresentant != null && !codeRepresentant.trim().isEmpty()) {
          pUtilisateur.setCodeRepresentant(codeRepresentant);
        }
      }
      else {
        // Créer un nouvel utilisateur
        if (creerUtilisateur(pUtilisateur)) {
          if (associerUtiliseurCataloguePreRequis(pUtilisateur)) {
            insererVuesPreRequis(pUtilisateur);
          }
        }
      }
      
      // Charger la liste des vues de l'utilisateur
      List<Vue> listeVue = chargerListeVueUtilisateur(pUtilisateur);
      pUtilisateur.setListeVue(listeVue);
      
      // Fermer la session SQL
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors du chargement de l'utilisateur");
    }
  }
  
  /**
   * Retourner les vues de l'utilisateur
   */
  public List<Vue> chargerListeVueUtilisateur(Utilisateur pUtilisateur) {
    // Vérifier les paramètres
    if (pUtilisateur == null) {
      throw new MessageErreurException("L'utilisateur est invalide");
    }
    
    // Mettre à jour la connexion à SQLite
    majDesConnexionsSQLITE();
    
    // Liste des vues
    List<Vue> liste = null;
    try {
      // Exécuter la requête
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT * FROM vue v, vue_util u WHERE v.vue_id = u.vue_id AND u.util_id = '"
          + pUtilisateur.getId() + "' ORDER BY v.metier_id;");
      
      // Générer la liste des vues
      liste = new ArrayList<Vue>();
      while (resultSet.next()) {
        // Créer la vue
        Vue vue = new Vue(resultSet.getInt(5));
        vue.setIdGroupeMetier(resultSet.getInt(7));
        vue.setCode(resultSet.getInt(1));
        vue.setLibelle(resultSet.getString(6));
        vue.setLibelleHTML(resultSet.getString(4));
        vue.setUrl(resultSet.getString(3));
        vue.setUrlIcone(resultSet.getString(2));
        
        // Ajouter la vue à la liste
        liste.add(vue);
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors du chargement des vues de l'utilisateur");
    }
    
    return liste;
  }
  
  /**
   * Retourner une vue en connaissant son ID
   */
  public Vue retournerUneVue(int id) {
    Vue vue = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT * FROM vue WHERE vue_id = '" + id + "';");
      
      // traitement des vues : rajouter le filtre utilisateur
      while (resultSet.next()) {
        // Créer la vue
        vue = new Vue(resultSet.getInt(5));
        vue.setIdGroupeMetier(resultSet.getInt(7));
        vue.setCode(resultSet.getInt(1));
        vue.setLibelle(resultSet.getString(6));
        vue.setLibelleHTML(resultSet.getString(4));
        vue.setUrl(resultSet.getString(3));
        vue.setUrlIcone(resultSet.getString(2));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerUneVue()");
    }
    
    return vue;
  }
  
  /**
   * Rajouter une vue à la liste de l'utilisateur
   */
  public boolean seRajouterUneVue(Utilisateur utilisateur, String id) {
    if (id == null) {
      return false;
    }
    boolean requeteOK = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      // Rentrer un nouvelle vue pour l'utilisateur dans la BDD
      if (executerUpdate(statement, "INSERT INTO vue_util VALUES ('" + utilisateur.getId() + "','" + Integer.parseInt(id) + "')") == 1) {
        requeteOK = true;
      }
      else {
        Trace.erreur("ECHEC INSERT seRajouterUneVue()");
      }
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL seRajouterUneVue()");
      return requeteOK;
    }
    
    return requeteOK;
  }
  
  /**
   * Enlever une vue de la liste de l'utilisateur
   */
  public boolean sEnleverUneVue(Utilisateur utilisateur, String id) {
    if (id == null) {
      return false;
    }
    boolean requeteOK = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Rentrer un nouvelle vue pour l'utilisateur dans la BDD
      if (executerUpdate(statement,
          "DELETE FROM vue_util WHERE util_id='" + utilisateur.getId() + "' AND vue_id='" + Integer.parseInt(id) + "'") == 1) {
        requeteOK = true;
      }
      else {
        Trace.erreur("ECHEC DELETE sEnleverUneVue()");
      }
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL sEnleverUneVue()");
      return requeteOK;
    }
    
    return requeteOK;
  }
  
  /**
   * Créer un utilisateur dans la BDD SQLITE.
   */
  public boolean creerUtilisateur(Utilisateur pUtilisateur) {
    // Vérifier les paramètres
    if (pUtilisateur == null) {
      throw new MessageErreurException("L'utilisateur est invalide");
    }
    
    try {
      // Mettre à jour la connexion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Récupérer le dernier identifiant attribué
      ResultSet resultSet = executerSelect(statement, "select MAX(util_id) from utilisateur;");
      
      // Déterminer l'identifiant à attribuer au nouvel utilisateur
      int id = 1;
      if (resultSet.isBeforeFirst()) {
        id = resultSet.getInt(1) + 1;
      }
      
      // Générer la requête
      String sql = "INSERT INTO utilisateur VALUES ('0','" + ConstantesEnvironnement.ACCES_MOBILITE + "', '" + id + "', '"
          + pUtilisateur.getLogin() + "','')";
      if (executerUpdate(statement, sql) != 1) {
        resultSet.close();
        statement.close();
        throw new MessageErreurException("Impossible de créer un nouvel utilisateur dans la base de données SQLite");
      }
      
      // Compléter l'utilisateur
      pUtilisateur.setId(id);
      pUtilisateur.setNiveauAcces(ConstantesEnvironnement.ACCES_MOBILITE);
      
      // Fermer la session SQL
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors de la création d'un nouvel utilisateur");
      return false;
    }
    
    return true;
  }
  
  /**
   * Insérer un nouvelle bibli dans la BDD SQLITE
   */
  public boolean gererBibli(Utilisateur utilisateur) {
    if (utilisateur == null || utilisateur.getBaseDeDonnees() == null) {
      return false;
    }
    
    boolean isOk = false;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet =
          executerSelect(statement, "SELECT bib_id FROM bibli WHERE bib_libelle = '" + utilisateur.getBaseDeDonnees() + "' ;");
      // si la bibli existe
      if (resultSet.next()) {
        isOk = true;
      }
      else {
        int nouvelId = 0;
        resultSet = executerSelect(statement, "select MAX(bib_id) from bibli;");
        if (resultSet.next()) {
          nouvelId = resultSet.getInt(1) + 1;
        }
        // Rentrer un nouvel utilisateur dans la BDD
        if (executerUpdate(statement, "INSERT INTO bibli VALUES ( '" + nouvelId + "', '" + utilisateur.getBaseDeDonnees() + "')") == 1) {
          isOk = true;
        }
        else {
          Trace.erreur("ECHEC INSERT gererBibli()");
        }
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL gererBibli()");
      return false;
    }
    
    return isOk;
  }
  
  /**
   * Installer une liste de vues Pré requises pour l'utilisateur
   */
  private boolean insererVuesPreRequis(Utilisateur utilisateur) {
    boolean isOk = true;
    ArrayList<Vue> listeVuesPreRequis = retournerVuesPreRequises();
    
    // si la liste est vide
    if (listeVuesPreRequis == null) {
      return false;
    }
    
    try {
      // S'assurer de la connextion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      int i = 0;
      while (i < listeVuesPreRequis.size() && isOk) {
        if (executerUpdate(statement,
            "INSERT INTO vue_util VALUES ( " + utilisateur.getId() + ", " + listeVuesPreRequis.get(i).getId() + ")") == 1) {
          i++;
        }
        else {
          Trace.erreur("ECHEC INSERT insererVuesPreRequis() indice " + i);
          isOk = false;
          break;
        }
      }
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL insererVuesPreRequis()");
      return false;
    }
    
    return isOk;
  }
  
  /**
   * Charger la liste des catalogues liés par défaut à un utilisateur lors de sa création.
   * Ce sont les catalogues avec le champs "cata_access" différent de 0.
   */
  private List<Catalogue> chargerListeCataloguePreRequis() {
    List<Catalogue> listeCatalogue = null;
    
    try {
      // S'assurer de la connextion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Exécuter la requête SQL
      String sql = "SELECT c.cata_id, c.cata_label, c.cata_icone FROM catalogue c WHERE c.cata_acces > 0 ;";
      ResultSet resultSet = executerSelect(statement, sql);
      
      // Créer la liste de catalogue
      listeCatalogue = new ArrayList<Catalogue>();
      while (resultSet.next()) {
        // Créer un catalogue
        Catalogue catalogue = new Catalogue(resultSet.getInt(1));
        catalogue.setLibelle(resultSet.getString(2));
        catalogue.setUrlIcone(resultSet.getString(3));
        
        // Ajouter le catalogue à la liste
        listeCatalogue.add(catalogue);
      }
      
      // Fermer la session SQL
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerCataloguesPreRequis()");
      return null;
    }
    
    return listeCatalogue;
  }
  
  /**
   * Associer un utilisateur à la liste de catalogues prérequis.
   */
  private boolean associerUtiliseurCataloguePreRequis(Utilisateur pUtilisateur) {
    // Vérifier les paramètres
    if (pUtilisateur == null) {
      throw new MessageErreurException("L'utilisateur est invalide");
    }
    
    boolean isOk = true;
    
    try {
      // S'assurer de la connextion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Charger la liste des catalogues pré-requis
      List<Catalogue> listeCataloguesPreRequis = chargerListeCataloguePreRequis();
      
      for (Catalogue calalogue : listeCataloguesPreRequis) {
        // Exécuter la requête
        String sql = "INSERT INTO cata_util VALUES ( '" + pUtilisateur.getId() + "', " + calalogue.getId() + ")";
        int resultat = executerUpdate(statement, sql);
        
        // Vérifier le résultat
        if (resultat != 1) {
          statement.close();
          throw new MessageErreurException("Impossible d'associer l'utilisateur à un catalogue");
        }
      }
      
      // Fermer la session SQL
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL insererCataloguesPreRequis()");
      return false;
    }
    
    return isOk;
  }
  
  /**
   * retourner la liste des catalogues disponibles
   */
  public List<Catalogue> chargerListeCatalogue() {
    ArrayList<Catalogue> liste = null;
    try {
      // S'assurer de la connextion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Exécuter la reqête
      String sql =
          "SELECT c.cata_id, c.cata_label, c.cata_icone FROM catalogue c, cata_util u WHERE c.cata_id = u.cata_id AND u.util_id = '"
              + utilisateur.getId() + "' ;";
      ResultSet resultSet = executerSelect(statement, sql);
      
      liste = new ArrayList<Catalogue>();
      // traitement des vues : rajouter le filtre utilisateur
      while (resultSet.next()) {
        // Créer un catalogue
        Catalogue catalogue = new Catalogue(resultSet.getInt(1));
        catalogue.setLibelle(resultSet.getString(2));
        catalogue.setUrlIcone(resultSet.getString(3));
        
        // Ajouter le catalogue à la liste
        liste.add(catalogue);
      }
      
      // Fermer la session SQL
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerListeCatalogues()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Charger un catalogue via son identifiant.
   * @param pIdCatalogue Identifant du catalogue à charger.
   * @return Catalogue chargé.
   */
  public Catalogue chargerCatalogue(String pIdCatalogue) {
    /// Tester les paramètres
    if (pIdCatalogue == null || pIdCatalogue.isEmpty()) {
      throw new MessageErreurException("L'identfiaint du catalogu eest invalide");
    }
    
    try {
      // Vérifier la connexion SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Convertir l'identifiant en entier
      int idCatalogue = Integer.parseInt(pIdCatalogue);
      
      // Exécuter la requête
      String sql = "SELECT cata_id, cata_label, cata_icone FROM catalogue WHERE cata_id = '" + idCatalogue + "';";
      ResultSet resultSet = executerSelect(statement, sql);
      
      // Tester s'il y a un résultat
      Catalogue catalogue = null;
      while (resultSet.next()) {
        // Créer le catalogue
        catalogue = new Catalogue(resultSet.getInt(1));
        catalogue.setLibelle(resultSet.getString(2));
        catalogue.setUrlIcone(resultSet.getString(3));
      }
      
      // Fermer la session SQL
      resultSet.close();
      statement.close();
      return catalogue;
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors du chargement d'un catalogue");
      return null;
    }
  }
  
  /**
   * retourne la liste des métiers proposés par le catalogue sélectionné
   */
  public ArrayList<Metier> retournerListeMetiers(String catalogue) {
    ArrayList<Metier> liste = null;
    try {
      majDesConnexionsSQLITE();
      
      int cata = 0;
      if (catalogue == null) {
        return null;
      }
      
      cata = Integer.parseInt(catalogue);
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT DISTINCT(m.metier_id), m.metier_icone, metier_label FROM metier m, vue v, vue_catalogue c WHERE m.metier_id = v.metier_id AND v.vue_id = c.vue_id AND metier_actif = 1 AND c.cata_id = '"
              + cata + "';");
      
      liste = new ArrayList<Metier>();
      
      while (resultSet.next()) {
        liste.add(new Metier(resultSet.getString(2), resultSet.getInt(1), resultSet.getString(3)));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerListeMetiers()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner la liste des vues pour un catalogue donné
   */
  public ArrayList<Vue> retournerListeVues(String catalogue, String metier) {
    ArrayList<Vue> liste = null;
    
    try {
      // Vérifier la connexion SQLite
      majDesConnexionsSQLITE();
      
      // Exécuter la requête
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT v.vue_id, v.metier_id, v.vue_code, v.vue_name, v.vue_link, v.vue_icone FROM vue v, vue_catalogue c WHERE v.vue_id = c.vue_id AND c.cata_id = '"
              + catalogue + "' AND metier_id = '" + metier + "' ORDER BY vue_code");
      
      // Générer la liste des vues
      liste = new ArrayList<Vue>();
      while (resultSet.next()) {
        // Créer la vue
        Vue vue = new Vue(resultSet.getInt(1));
        vue.setIdGroupeMetier(resultSet.getInt(2));
        vue.setCode(resultSet.getInt(3));
        vue.setLibelle(resultSet.getString(4));
        vue.setUrl(resultSet.getString(5));
        vue.setUrlIcone(resultSet.getString(6));
        
        // Ajouterl a vue à la liste
        liste.add(vue);
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors du chargement de la liste des vues d'un catalogue");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner un métier en fonction de son id
   */
  public Metier retournerUnMetier(String metierString) {
    Metier metier = null;
    try {
      majDesConnexionsSQLITE();
      
      int met = 0;
      if (metierString == null) {
        return null;
      }
      met = Integer.parseInt(metierString);
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet =
          executerSelect(statement, "SELECT metier_icone, metier_id, metier_label  FROM metier WHERE metier_id = '" + met + "';");
      
      while (resultSet.next()) {
        metier = new Metier(resultSet.getString(1), resultSet.getInt(2), resultSet.getString(3));
      }
      
      resultSet.close();
      statement.close();
      return metier;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerUnMetier()");
      return null;
    }
  }
  
  /**
   * retourner si l'article est un favori ou non suivant si on le modifie ou non
   * @param utilisateur
   * @param onInverse
   * @return
   */
  public boolean gererUnArticleFavori(Utilisateur utilisateur, boolean onInverse) {
    // On va tester la présence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '"
              + utilisateur.getBaseDeDonnees() + "' AND article_id = '"
              + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        statement.close();
        // on veut le supprimer des favoris
        if (onInverse) {
          if (executerUpdate(statement,
              "DELETE FROM favoris_articles WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '" + utilisateur.getBaseDeDonnees()
                  + "' AND article_id = '" + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim()
                  + "'; ") == 1) {
            return false;
          }
          else {
            Trace.erreur("ECHEC DELETE gererUnArticleFavori()");
            return true;
          }
        }
        // sinon on retourne sa présence
        else {
          return true;
        }
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        statement.close();
        // on veut le rajouter aux favoris
        if (onInverse) {
          if (executerUpdate(statement,
              "INSERT INTO favoris_articles VALUES ('"
                  + utilisateur.getGestionArticles().getRecordActuel().getField("A1ETB").toString().trim() + SEPARATEUR_IDS
                  + utilisateur.getGestionArticles().getRecordActuel().getField("A1ART").toString().trim() + "', '" + utilisateur.getId()
                  + "', '" + utilisateur.getBaseDeDonnees() + "'); ") == 1) {
            return true;
          }
          else {
            Trace.erreur("ECHEC INSERT gererUnArticleFavori()");
            return false;
          }
        }
        // sinon on retourne sa non présence
        else {
          return false;
        }
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL gererUnArticleFavori()");
      return false;
    }
  }
  
  /**
   * Retourner si l'article est un favori
   */
  public boolean isUnArticleFavori(Utilisateur utilisateur, GenericRecord record) {
    // On va tester la présence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '"
              + utilisateur.getBaseDeDonnees() + "' AND article_id = '" + record.getField("A1ETB").toString().trim() + SEPARATEUR_IDS
              + record.getField("A1ART").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        statement.close();
        return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        statement.close();
        return false;
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL isUnArticleFavori()");
      return false;
    }
  }
  
  /**
   * Permet de récupérer tous les articles favoris de l'utilisateur dans sa bibli active
   * @param utilisateur
   */
  public ArrayList<String> recupererLesArticlesFavoris(Utilisateur utilisateur, String tri) {
    if (utilisateur == null) {
      return null;
    }
    ArrayList<String> liste = new ArrayList<String>();
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT article_id FROM favoris_articles WHERE util_id = '" + utilisateur.getId()
          + "' AND bib_id = '" + utilisateur.getBaseDeDonnees() + "' ORDER BY article_id " + tri + ";");
      while (resultSet.next()) {
        liste.add(resultSet.getString(1));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererLesArticlesFavoris()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * retourner si le client est un favori ou non suivant si on le modifie ou non
   * @param utilisateur
   * @param onInverse
   * @return
   */
  public boolean gererUnClientFavori(Utilisateur utilisateur, boolean onInverse) {
    // On va tester la présence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '"
              + utilisateur.getBaseDeDonnees() + "' AND client_id = '"
              + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + SEPARATEUR_IDS
              + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        statement.close();
        // on veut le supprimer des favoris
        if (onInverse) {
          if (executerUpdate(statement,
              "DELETE FROM favoris_clients WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '" + utilisateur.getBaseDeDonnees()
                  + "' AND client_id = '" + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim()
                  + SEPARATEUR_IDS + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim()
                  + "'; ") == 1) {
            return false;
          }
          else {
            Trace.erreur("ECHEC DELETE gererUnClientFavori()");
            return true;
          }
        }
        // sinon on retourne sa présence
        else {
          return true;
        }
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        statement.close();
        // on veut le rajouter aux favoris
        if (onInverse) {
          if (executerUpdate(statement,
              "INSERT INTO favoris_clients VALUES ('"
                  + utilisateur.getGestionClients().getRecordActuel().getField("CLETB").toString().trim() + SEPARATEUR_IDS
                  + utilisateur.getGestionClients().getRecordActuel().getField("CLCLI").toString().trim() + SEPARATEUR_IDS
                  + utilisateur.getGestionClients().getRecordActuel().getField("CLLIV").toString().trim() + "', '" + utilisateur.getId()
                  + "', '" + utilisateur.getBaseDeDonnees() + "'); ") == 1) {
            return true;
          }
          else {
            Trace.erreur("ECHEC INSERT gererUnClientFavori()");
            return false;
          }
        }
        // sinon on retourne sa non présence
        else {
          return false;
        }
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL gererUnClientFavori()");
      return false;
    }
  }
  
  /**
   * Retourner si le client est un favori
   */
  public boolean isUnClientFavori(Utilisateur utilisateur, GenericRecord record) {
    // On va tester la présence de cet article dans les favoris
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getId() + "' AND bib_id = '"
              + utilisateur.getBaseDeDonnees() + "' AND client_id = '" + record.getField("CLETB").toString().trim() + SEPARATEUR_IDS
              + record.getField("CLCLI").toString().trim() + SEPARATEUR_IDS + record.getField("CLLIV").toString().trim() + "';");
      // si il existe
      if (resultSet.next()) {
        resultSet.close();
        statement.close();
        return true;
      }
      // Si il n'existe pas
      else {
        resultSet.close();
        statement.close();
        return false;
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL isUnClientFavori()");
      return false;
    }
  }
  
  /**
   * Permet de récupérer tous les clients favoris de l'utilisateur dans sa bibli active
   * @param utilisateur
   */
  public ArrayList<String> recupererLesClientsFavoris(Utilisateur utilisateur, String tri) {
    if (utilisateur == null) {
      return null;
    }
    ArrayList<String> liste = new ArrayList<String>();
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT client_id FROM favoris_clients WHERE util_id = '" + utilisateur.getId()
          + "' AND bib_id = '" + utilisateur.getBaseDeDonnees() + "' ORDER BY client_id " + tri + ";");
      while (resultSet.next()) {
        liste.add(resultSet.getString(1));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererLesClientsFavoris()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Liste les paramètres du back-office
   */
  public ArrayList<Parametre> recupererLesParametres(Utilisateur utilisateur) {
    if (utilisateur == null) {
      return null;
    }
    ArrayList<Parametre> liste = new ArrayList<Parametre>();
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT para_id,para_libelle,para_acces,para_lien FROM parametre WHERE para_acces >= '" + utilisateur.getNiveauAcces() + "';");
      while (resultSet.next()) {
        liste.add(new Parametre(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4)));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererLesParametres()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner un paramètre en fonction de son ID
   */
  public Parametre retournerUnParametre(int para) {
    Parametre parametre = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet =
          executerSelect(statement, "SELECT para_id,para_libelle,para_acces,para_lien FROM parametre WHERE para_id = '" + para + "';");
      while (resultSet.next()) {
        parametre = new Parametre(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getString(4));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerUnParametre()");
      return null;
    }
    
    return parametre;
  }
  
  /**
   * Recupérer la liste de tous les utilisateurs
   */
  public ArrayList<Utilisateur> recupererLesUtilisateurs() {
    ArrayList<Utilisateur> liste = new ArrayList<Utilisateur>();
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT util_id, util_login, util_acces, util_super, util_repres FROM utilisateur ORDER BY util_login;");
      while (resultSet.next()) {
        liste.add(new Utilisateur(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),
            resultSet.getString(5)));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererLesUtilisateurs()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Récupérer un utilisateur par son Id
   */
  public Utilisateur recupererUnUtilisateur(int id) {
    Utilisateur utilisateur = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT util_id,util_login, util_acces, util_super, util_repres FROM utilisateur WHERE util_id = '" + id + "';");
      while (resultSet.next()) {
        utilisateur = new Utilisateur(resultSet.getInt(1), resultSet.getString(2), resultSet.getInt(3), resultSet.getInt(4),
            resultSet.getString(5));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererUnUtilisateur()");
      return null;
    }
    
    return utilisateur;
  }
  
  /**
   * Modifier les propriétés d'un utilisateur
   */
  public boolean modifierUnUtilisateur(HttpServletRequest request) {
    
    try {
      majDesConnexionsSQLITE();
      
      String util_super = "0";
      if (request.getParameter("util_super") != null) {
        util_super = "1";
      }
      
      String majRepresentant = "";
      if (request.getParameter("util_repres") != null) {
        majRepresentant = ", util_repres = '" + request.getParameter("util_repres").trim() + "' ";
      }
      
      Statement statement = maConnexion.createStatement();
      boolean ret = (executerUpdate(statement, "UPDATE utilisateur SET util_acces = '" + request.getParameter("util_acces")
          + "', util_super = '" + util_super + "' " + majRepresentant + " WHERE util_id='" + request.getParameter("utilId") + "'") == 1);
      statement.close();
      return ret;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL modifierUnUtilisateur()");
      return false;
    }
  }
  
  /**
   * Choper les champs de base du métier demandé
   */
  public String getChampsBase(int metier) {
    String retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT f.fichier_raccou, z.zone_code FROM fichier f, metier m, zone z WHERE f.fichier_id = z.fichier_id AND f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "' AND z.zone_base > 0 ");
      while (resultSet.next()) {
        if (retour != null) {
          retour += "," + resultSet.getString(1) + "." + resultSet.getString(2);
        }
        else {
          retour = resultSet.getString(1) + "." + resultSet.getString(2);
        }
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getChampsBase()");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper le fichier AS400 maître du métier correspondant: article, client, fournisseur...
   */
  public Fichier getFichierParent(int metier) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f, metier m WHERE f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "'");
      while (resultSet.next()) {
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getFichierParent()");
      return null;
    }
    return retour;
  }
  
  public Fichier getFichierParentModule(int module) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f, metier m, vue v WHERE f.fichier_id = m.fichier_id AND m.metier_id = v.metier_id AND v.vue_id = "
              + module);
      while (resultSet.next()) {
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getFichierParent()");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper le fichier AS400 maître du métier correspondant: article, client, fournisseur...
   */
  public Fichier getUnFichier(int id) {
    Fichier retour = null;
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT f.fichier_id, f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where FROM fichier f WHERE f.fichier_id = '"
              + id + "'");
      while (resultSet.next()) {
        retour = new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(1));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getUnFichier()");
      return null;
    }
    return retour;
  }
  
  /**
   * Choper les champs à sélectionner dans la liste de la vue correspondante
   */
  public ArrayList<Zone> getChampsListe(int metier, int vue) {
    
    ArrayList<Zone> matrice = null;
    try {
      majDesConnexionsSQLITE();
      
      matrice = new ArrayList<Zone>();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE f.fichier_id = z.fichier_id AND z.zone_id = v.zone_id AND v.vue_id= '"
              + vue
              + "' AND (v.liste_zones > 0 OR (z.fichier_id = (SELECT f.fichier_id FROM fichier f, metier m WHERE f.fichier_id = m.fichier_id AND m.metier_id = '"
              + metier + "' LIMIT 1)  AND z.zone_base > 0)) ORDER BY v.liste_zones ");
      
      while (resultSet.next()) {
        // resultSet.getInt(7)+ resultSet.getString(8));
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getChampsListe()");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * Choper les champs à sélectionner dans la fiche de la vue correspondante
   */
  public ArrayList<Zone> getChampsFiche(int metier, int vue) {
    ArrayList<Zone> matrice = null;
    try {
      majDesConnexionsSQLITE();
      
      matrice = new ArrayList<Zone>();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE f.fichier_id = z.fichier_id AND z.zone_id = v.zone_id AND v.vue_id= '"
              + vue + "' ORDER BY v.bloc_zones, v.ordre_zones ");
      
      while (resultSet.next()) {
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL getChampsFiche()");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * Retourner la liste des fichiers AS 400 attachés à un fichier principal
   */
  public ArrayList<Fichier> retournerListeFichiers(boolean fiche, Fichier fichierParent, int vue) {
    ArrayList<Fichier> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      
      liste = new ArrayList<Fichier>();
      String triZonesListes = null;
      if (fiche) {
        triZonesListes = "";
      }
      else {
        triZonesListes = " AND (zv.liste_zones > 0 OR z.zone_base > 0) ";
      }
      
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT DISTINCT(f.fichier_id), f.fichier_libelle, f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where, zv.fic_lien FROM fichier f, zones_vues zv, zone z WHERE zv.zone_id = z.zone_id AND z.fichier_id = f.fichier_id AND f.fichier_id <> '"
              + fichierParent.getFichier_id() + "' AND zv.fic_lien = " + fichierParent.getFichier_id() + " AND zv.vue_id = '" + vue + "' "
              + triZonesListes);
      // f.fichier_raccou, f.fichier_descri, f.fichier_from, f.fichier_where, zv.fic_lien FROM fichier f, zones_vues zv,
      // zone z WHERE zv.zone_id = z.zone_id AND z.fichier_id = f.fichier_id AND f.fichier_id <> '" +
      // fichierParent.getFichier_id() + "' AND zv.fic_lien = " + fichierParent.getFichier_id() + " AND zv.vue_id = '" +
      // vue + "' " + triZonesListes);
      
      while (resultSet.next()) {
        // resultSet.getString(2));
        liste.add(new Fichier(resultSet.getInt(1), resultSet.getString(2), resultSet.getString(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getString(6), resultSet.getInt(7)));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerListeFichiers()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * retourner le morceau de SQL qui fait la jointure entre deux fichiers
   */
  public String retournerJointureFichiers(Fichier fichierParent, Fichier fichierLie, int vue) {
    if (fichierParent == null || fichierLie == null) {
      return null;
    }
    String retour = null;
    
    try {
      majDesConnexionsSQLITE();
      
      // Checker les jointures dans le fichier de liaison
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT CASE WHEN (l.calcul_zone = '') THEN z.zone_code ELSE l.calcul_zone END AS caclcul1, CASE WHEN (l.calcul_fic_zone = '') THEN z2.zone_code ELSE l.calcul_fic_zone END AS caclcul2 FROM zone z,zone z2,  liaison_fichier l WHERE l.zone_id = z.zone_id AND l.fic_zone_id = z2.zone_id AND l.fichier_id = '"
              + fichierParent.getFichier_id() + "' AND l.fic_fichier_id = '" + fichierLie.getFichier_id() + "' ");
      
      while (resultSet.next()) {
        if (retour != null) {
          retour += " AND ";
        }
        else {
          retour = "";
        }
        
        retour += resultSet.getString(1) + " = " + resultSet.getString(2);
      }
      
      resultSet.close();
      
      // rajouter les spécificités de la vue
      resultSet = executerSelect(statement,
          "SELECT fic_from FROM vues_fichier WHERE vue_id = " + vue + " AND fichier_id = " + fichierLie.getFichier_id());
      
      while (resultSet.next()) {
        retour += " AND " + resultSet.getString(1);
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerJointureFichiers()");
      return null;
    }
    
    return retour;
  }
  
  /**
   * Récupérer les zones ID du métier sélectionné
   */
  public ArrayList<Zone> recupererIdsMetier(Fichier fichier, int vue) {
    ArrayList<Zone> matrice = null;
    try {
      matrice = new ArrayList<Zone>();
      
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT DISTINCT(z.zone_id), z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM fichier f, zone z, zones_vues v WHERE z.zone_id = v.zone_id AND z.fichier_id = f.fichier_id  AND z.zone_isId >0 AND f.fichier_id = "
              + fichier.getFichier_id() + " AND v.vue_id = " + vue + " ORDER BY zone_isId ");
      
      while (resultSet.next()) {
        matrice.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4),
            resultSet.getString(5), resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9),
            resultSet.getString(10), resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14),
            resultSet.getInt(15), resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererIdsMetier()");
      return null;
    }
    
    return matrice;
  }
  
  /**
   * Récupérer les zones concernées par la recherche par mot clé
   */
  public ArrayList<Zone> recupererChampsMotsCle(int metier) {
    ArrayList<Zone> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      
      liste = new ArrayList<Zone>();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT DISTINCT(z.zone_id), z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou FROM fichier f, zone z, metier m WHERE f.fichier_id = m.fichier_id AND f.fichier_id = z.fichier_id AND m.metier_id = "
              + metier + " AND z.zone_rech > 0 ");
      
      while (resultSet.next()) {
        liste.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), 0, 0, 0, 0, 0));
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererChampsMotsCle()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * recupérer la valeur d'une constante via sa clé
   */
  public String recupererConstante(String cle) {
    String valeur = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT c.const_valeur FROM constante c WHERE c.const_cle = '" + cle + "' LIMIT 1");
      
      while (resultSet.next()) {
        valeur = resultSet.getString(1);
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererConstante()");
      return null;
    }
    
    return valeur;
  }
  
  /**
   * Sauver l'adresse IP du serveur IBM dans les paramètres SQLite.
   * 
   * @param pAdresseIP Adresse IP du serveur IBM à sauvegarder.
   * @param pProfil Profil de l'utilisateur. Seul l'administrateur peut effectuer cette opération. C'est le profil avec un identifiant
   *          égale à 1.
   * @return true=sauvegarde effectuée, false=erreur lors de la sauvegarde.
   */
  public boolean sauverAdresseIPServeurIBM(String pAdresseIP, String pProfil) {
    boolean retour = false;
    try {
      // S'assurer de la connextion à SQLite
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      
      // Récupérer l'identifiant du profil
      ResultSet resultSet =
          executerSelect(statement, "SELECT util_id FROM utilisateur WHERE util_login = '" + pProfil.trim() + "' LIMIT 1");
      
      // Rechercher l'utilisateur avec l'identifiant 1
      while (resultSet.next()) {
        if (resultSet.getInt(1) == 1) {
          // Mettre à jour l'adresse IP du serveur IBM dans les paramètres SQLite
          int nbLigne = executerUpdate(statement, "UPDATE constante SET const_valeur = '" + pAdresseIP + "' WHERE const_cle='IP_AS400'");
          retour = (nbLigne == 1);
        }
      }
      
      // Fermer les ressources
      resultSet.close();
      statement.close();
      return retour;
    }
    catch (SQLException e) {
      Trace.erreur(e, "Erreur lors de la sauvegarde de l'adresse IP du serveur IBM dans SQLite");
      return false;
    }
  }
  
  /**
   * Retourner la liste des zones d'un module
   */
  public ArrayList<Zone> retournerZonesListeModule(int vue) {
    ArrayList<Zone> liste = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou, v.ordre_zones, v.bloc_zones, v.liste_zones, v.is_Modif, v.mode_aff FROM zone z, fichier f, zones_vues v WHERE z.fichier_id = f.fichier_id AND z.zone_id = v.zone_id AND v.vue_id = (SELECT module FROM vue WHERE vue_id ="
              + vue + " LIMIT 1) ORDER BY v.liste_zones ");
      
      while (resultSet.next()) {
        if (liste == null) {
          liste = new ArrayList<Zone>();
        }
        
        liste.add(new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), resultSet.getInt(14), resultSet.getInt(15),
            resultSet.getInt(16), resultSet.getInt(17), resultSet.getInt(18)));
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "retournerZonesListeModule()");
      return null;
    }
    
    return liste;
  }
  
  /**
   * Retourner la liste des vues prérequises à la création d'un utilisateur
   */
  private ArrayList<Vue> retournerVuesPreRequises() {
    ArrayList<Vue> vues = null;
    
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT v.vue_id, v.metier_id, v.vue_code, v.vue_name, v.vue_link, v.vue_icone FROM vue v WHERE vue_requis > 0 AND vue_code > 0 ORDER BY vue_id");
      
      // Générer la liste des vues
      vues = new ArrayList<Vue>();
      while (resultSet.next()) {
        // Créer la vue
        Vue vue = new Vue(resultSet.getInt(1));
        vue.setIdGroupeMetier(resultSet.getInt(2));
        vue.setCode(resultSet.getInt(3));
        vue.setLibelle(resultSet.getString(4));
        vue.setUrl(resultSet.getString(5));
        vue.setUrlIcone(resultSet.getString(6));
        
        // Ajouter la vue à la liste
        vues.add(vue);
      }
      
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerVuesPreRequises()");
      return null;
    }
    
    return vues;
  }
  
  /**
   * recuperer le numéro de version
   */
  public int recupererVersionBDD() {
    try {
      majDesConnexionsSQLITE();
      
      int version = 0;
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT const_valeur FROM constante WHERE const_cle = 'VERSION';");
      
      if (resultSet.next()) {
        try {
          version = Integer.parseInt(resultSet.getString(1));
          Trace.info("Version BDD SQLite = " + version);
        }
        catch (SQLException e) {
          Trace.erreur(e, "recupererVersionBDD()");
          return 0;
        }
      }
      
      resultSet.close();
      statement.close();
      return version;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererVersionBDD()");
      return 0;
    }
    
  }
  
  /**
   * Récupérer la lettre d'environnement
   */
  public boolean recupererLettreEnvironnement() {
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT const_valeur FROM constante WHERE const_cle = 'LETTRE_ENV';");
      if (resultSet.next()) {
        try {
          utilisateur.setLettre_env(resultSet.getString(1));
        }
        catch (SQLException e) {
          Trace.erreur(e, "recupererLettreEnvironnement()");
          return false;
        }
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL recupererLettreEnvironnement()");
      return false;
    }
    return true;
  }
  
  public ArrayList<ArrayList<String>> envoyerRequete(String requete) {
    ArrayList<ArrayList<String>> listeFinale = null;
    
    if (requete == null) {
      return null;
    }
    
    if (requete.trim().toUpperCase().startsWith("SELECT")) {
      try {
        majDesConnexionsSQLITE();
        Statement statement = maConnexion.createStatement();
        ResultSet resultSet = executerSelect(statement, requete);
        ResultSetMetaData metadata = resultSet.getMetaData();
        int nombreColonnes = metadata.getColumnCount();
        
        listeFinale = new ArrayList<ArrayList<String>>();
        ArrayList<String> listeResultat = new ArrayList<String>();
        
        for (int i = 0; i < nombreColonnes; i++) {
          listeResultat.add(metadata.getColumnName(i + 1));
        }
        listeFinale.add(listeResultat);
        
        while (resultSet.next()) {
          listeResultat = new ArrayList<String>();
          for (int i = 0; i < nombreColonnes; i++) {
            listeResultat.add(resultSet.getString(i + 1));
          }
          listeFinale.add(listeResultat);
        }
        
        resultSet.close();
        statement.close();
      }
      catch (SQLException e) {
        Trace.erreur(e, "ECHEC SQL envoyerRequete()");
        return null;
      }
    }
    else {
      if (envoyerRequeteModif(requete)) {
        ArrayList<String> listeResultat = new ArrayList<String>();
        listeResultat.add(ConstantesEnvironnement.SQLITE_ISOK);
        listeFinale = new ArrayList<ArrayList<String>>();
        listeFinale.add(listeResultat);
      }
    }
    
    return listeFinale;
  }
  
  /**
   * Envoyer une requete SQLITE en UPDATE OU EN INSERT -> retourne le succès ou l'échec de la procédure sous forme de
   * boolean
   */
  public boolean envoyerRequeteModif(String requete) {
    try {
      majDesConnexionsSQLITE();
      Statement statement = maConnexion.createStatement();
      int retour = executerUpdate(statement, requete);
      statement.close();
      
      if (requete.trim().toUpperCase().startsWith("UPDATE") || requete.trim().toUpperCase().startsWith("INSERT")
          || requete.trim().toUpperCase().startsWith("DELETE")) {
        return (retour != 0);
      }
      else if (requete.trim().toUpperCase().startsWith("CREATE ") || requete.trim().toUpperCase().startsWith("DROP TABLE")
          || requete.trim().toUpperCase().startsWith("ALTER TABLE")) {
        return (retour != 0);
      }
      else {
        return false;
      }
    }
    catch (SQLException e) {
      Trace.erreur(e, "envoyerRequeteModif()");
      return false;
    }
  }
  
  /**
   * retourne la zone qui se charge de l'état de la fiche métier
   */
  public Zone chargerZoneEtatDunMetier(int metier) {
    Zone zoneEtat = null;
    majDesConnexionsSQLITE();
    try {
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement,
          "SELECT z.zone_id, z.fichier_id, z.zone_isId, z.zone_code, z.zone_libelle, z.zone_long, z.zone_base, z.zone_calcul, z.zone_descri, z.zone_rech, z.zone_lb_int, z.zone_type, f.fichier_raccou FROM fichier f, zone z WHERE z.fichier_id = f.fichier_id AND f.fichier_id = "
              + metier + " AND z.zone_base = 2 ");
      
      while (resultSet.next()) {
        zoneEtat = new Zone(resultSet.getInt(1), resultSet.getInt(2), resultSet.getInt(3), resultSet.getString(4), resultSet.getString(5),
            resultSet.getInt(6), resultSet.getInt(7), resultSet.getString(8), resultSet.getString(9), resultSet.getString(10),
            resultSet.getInt(11), resultSet.getInt(12), resultSet.getString(13), 0, 0, 0, 0, 0);
      }
      resultSet.close();
      statement.close();
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL chargerZoneEtatDunMetier");
      return null;
    }
    
    return zoneEtat;
  }
  
  /**
   * Si le module possède un sous-modèle, retourne l'ID du sous module sinon 0
   */
  public int retournerIdSousModule(int vue) {
    int sousModule = 0;
    
    try {
      majDesConnexionsSQLITE();
      
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT module FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next()) {
        sousModule = resultSet.getInt(1);
      }
      
      resultSet.close();
      statement.close();
      return sousModule;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerIdSousModule()");
      return 0;
    }
  }
  
  /**
   * retourne le titre de la vue sélectionnée
   */
  public String retournerTitreVue(int vue) {
    String retour = "";
    
    try {
      majDesConnexionsSQLITE();
      
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT vue_name FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next()) {
        retour = resultSet.getString(1);
      }
      
      resultSet.close();
      statement.close();
      return retour;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerTitreVue()");
      return "";
    }
  }
  
  public String retournerLabelVue(int vue) {
    String retour = "";
    
    try {
      majDesConnexionsSQLITE();
      
      Statement statement = maConnexion.createStatement();
      ResultSet resultSet = executerSelect(statement, "SELECT vue_label FROM vue WHERE vue_id = " + vue + " ;");
      
      while (resultSet.next()) {
        retour = resultSet.getString(1);
      }
      
      resultSet.close();
      statement.close();
      return retour;
    }
    catch (SQLException e) {
      Trace.erreur(e, "ECHEC SQL retournerLabelVue()");
      return "";
    }
  }
  
  // +++++++++++++++++++++++++++++++++++++++++++++ GETTERS et SETTERS +++++++++++++++++++++++++++++++++++++++++++++++++
  
  /**
   * Choper L'utilisateur principal
   */
  public Utilisateur getUtilisateur() {
    return utilisateur;
  }
  
  /**
   * Attribuer l'utilisateur principal
   */
  public void setUtilisateur(Utilisateur utilisateur) {
    this.utilisateur = utilisateur;
  }
  
}
