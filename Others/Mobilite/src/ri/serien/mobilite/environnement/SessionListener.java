/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.environnement;

import java.util.ArrayList;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import ri.serien.libcommun.outils.Trace;

public class SessionListener implements HttpSessionListener {
  
  @Override
  public void sessionCreated(HttpSessionEvent moteur) {
    
  }
  
  @Override
  public void sessionDestroyed(HttpSessionEvent moteur) {
    
    // si il s'agit d'une session utilisateur la loger sinon .... fuck
    if ((Utilisateur) moteur.getSession().getAttribute("utilisateur") != null) {
      ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).deconnecterAS400();
      ((Utilisateur) moteur.getSession().getAttribute("utilisateur")).getBaseSQLITE().stopConnection();
      
      Trace.info("Fin de la session : idSession=" + moteur.getSession().getId());
    }
    
    // supprimer la session de la liste des sessions du contexte
    if (moteur.getSession().getServletContext().getAttribute("sessions") != null) {
      int trouve = -1;
      int i = 0;
      // liste des sessions
      ArrayList<HttpSession> listeSessions = ((ArrayList<HttpSession>) moteur.getSession().getServletContext().getAttribute("sessions"));
      while (i < listeSessions.size() && trouve < 0) {
        // si on la trouve on la note pour la supprimer
        if (listeSessions.get(i).getId().equals(moteur.getSession().getId())) {
          trouve = i;
        }
        i++;
      }
      // on supprime
      if (trouve > -1) {
        listeSessions.remove(trouve);
      }
    }
  }
  
}
