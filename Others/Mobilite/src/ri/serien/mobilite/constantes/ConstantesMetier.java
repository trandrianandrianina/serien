/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.constantes;

/**
 * Classe de constantes propres au métier
 * @author ritoudb
 * 
 */
public class ConstantesMetier {
  public final static String FICHIER_CLIENTS = "pgvmclim";
  public final static String FICHIER_ARTICLES = "pgvmart";
  public final static String FICHIER_EXTA = "pgvmeaa";
  /*public final static String FICHIER_CNA = "pgvmcna0";*/
  public final static String FICHIER_STOCK = "pgvmstk";
  public final static String FICHIER_TARIFS = "pgvmtarm";
  public final static String FICHIER_FOURNISSEUR = "pgvmfrs";
  public final static String FICHIER_LIENCLIENTCONTACTS = "psemrtlm";
  public final static String FICHIER_CONTACTS = "psemrtem";
  public final static String MONNAIE = "&#128;";
  
  public final static String PARAM_FAM = "A1FAM";
  public final static String PARAM_UNIT = "A1UNS";
  public final static String PARAM_UNIT_L = "L1UNV";
  public final static String PARAM_DEV_C = "CLDEV";
  public final static String PARAM_DEV_T = "ATDEV";
  // catégorie de contact (fonction)
  public final static String PARAM_RECAT = "RECAT";
  
  public final static String CODE_DEVISE = "";
}
