/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.mobilite.constantes;

/**
 * Classe de constantes propres à l'affichage
 * @author ritoudb
 * 
 */
public class ConstantesAffichage {
  public final static int METIER_ACCUEIL = 0;
  public final static int METIER_CLIENTS = 1;
  public final static int METIER_ARTICLES = 2;
  public final static int VUES_ACCUEIL = 6;
  public final static int VUES_LIGNES_ACCUEIL = 3;
  public final static int LONGUEUR_MAX_ZONES = 310;
  // public final static String MESSAGE_VIDE = "Non g&eacute;r&eacute; ";
  public final static String MESSAGE_VIDE = "&nbsp;-&nbsp;";
  
  // Type de zones à l'affichage
  public final static int TYPE_ALPHA = 0;
  public final static int TYPE_NUME = 1;
  public final static int TYPE_MONTANT = 2;
  public final static int TYPE_PARA = 3;
  public final static int TYPE_DATE = 4;
  public final static int TYPE_TEL = 5;
  public final static int TYPE_GENCOD = 6;
  public final static int TYPE_MAIL = 7;
  
  // Rôle d'une zone dans la liste de niveau 1
  public final static String[] structureListe = { "clePrincip", "cleSecond", "valeurPrincip", "valeurSecond" };
  public final static int PAS_DANS_LISTE = 0;
  public final static int CLE_PRIMAIRE = 1;
  public final static int CLE_SECOND = 2;
  public final static int VAL_PRIMAIRE = 3;
  public final static int VAL_SECOND = 4;
  
  public final static int AFFICH_NORMAL = 0;
  public final static int AFFICH_IMPORT = 1;
  public final static int AFFICH_ALERTE = 2;
  public final static int AFFICH_BOUTON = 3;
  
  public final static int LABEL_INTERN = 1;
  
  public static final int NB_LIGNES_MODULES = 40;
}
