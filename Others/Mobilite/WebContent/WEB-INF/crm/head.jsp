<!DOCTYPE html>
	<html lang='fr'>
		<head>
			<meta charset='windows-1252'>
			<meta name='apple-mobile-web-app-capable' content='yes'>
			<meta HTTP-EQUIV='CACHE-CONTROL' content='NO-CACHE'>
			<meta HTTP-EQUIV='PRAGMA' content='NO-CACHE'>
			<meta HTTP-EQUIV='EXPIRES' content='0'>
			<meta name='ROBOTS' content='NONE'> 
			<meta name='GOOGLEBOT' content='NOARCHIVE'>
			<meta name='viewport' content='width=device-width, initial-scale=1.0, minimum-scale = 1, maximum-scale=1.0, target-densityDpi=device-dpi' />
			<meta name='viewport' content='width=device-width; user-scalable=no; minimum-scale = 1; maximum-scale = 1;' />
			<meta name='apple-mobile-web-app-status-bar-style' content='black-translucent' />
			<title>S&eacute;rie N mobilit&eacute;</title>
			<script src='scripts/general.js'></script>
			<link href='css/css_mobile.css' rel='stylesheet'/>
			<link rel="apple-touch-icon" href="touch-icon-iphone.png">
			<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
			<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
			<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
			<link rel="shortcut icon" href="images/favicon.png" />
			<% if(request.getAttribute("cssSpecifique") != null) {%>
					<link href='<%=request.getAttribute("cssSpecifique") %>' rel='stylesheet'/> 
			<% } %>
			<% if(request.getAttribute("cssSpecifique2") != null) {%>
				<link href='<%=request.getAttribute("cssSpecifique2") %>' rel='stylesheet'/> 
			<% } %>
			<link href='css/crm.css' rel='stylesheet'/>
			
			
			<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
			<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=true&language=fr&key=AIzaSyB2sOMBR1p1NWF418mAz0n7qwr3_BHcdWo"></script>

			

			
			
		</head>
			
		<body>
			<jsp:include page="presentation.jsp" />
			<article id='contenu'>
		
		