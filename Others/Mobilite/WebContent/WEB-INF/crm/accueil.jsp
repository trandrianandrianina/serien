<jsp:include page="head.jsp" />

<section id='gestionAccueil'>
	<div class='favorisAccueil'><a href='ActionsCom'><img src='images/actions.png'/><p>Actions comm.</p></a></div>
	<div class='favorisAccueil'><a href='planning'><img src='images/planning.png'/><p>Planning</p></a></div>
	<div class='favorisAccueil'><a href='itineraire'><img src='images/itineraire.png'/><p>Itin�raire</p></a></div>
	<div class='favorisAccueil'><a onclick='getLocation()'><img src='images/geoloc.png'/><p>G�olocalisation</p></a></div> 


</section>

<script>

//initialisations pour google map
var geocoder = new google.maps.Geocoder();
var pos;

//on cherche la position de l'utilisateur de l'	application
function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.watchPosition(showPosition);
       
    } else { 
        alert('Erreur : Votre navigateur ne supporte pas la g�olocalisation');
    }
}
    
//On d�fini la position actuelle en latitude et longitude, puis on r�cup�re le d�partement pour la requ�te qui va chercher les cliants � proximit�
function showPosition(position) {
	var address;
	//position de l'utilisateur de l'application en latitude et longitude
    pos = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
    geocoder.geocode({'latLng':pos}, function(data)
			 {
    			// on se sert du g�ocoder Google pour trouver le d�partement o� se trouve l'utilisateur (2 premiers caract�res du code postal)
				address = extractFromAdress(data[0].address_components, "postal_code").substring(0, 2);
				// envoi en post en post du num�ro de d�partement trouv�
				envoyerDonneesEnPOST('geolocalisation', 'departement', address);

			 });
    	// wait pour laisser le temps � l'API de r�pondre
		wait(100);  
}

//L'API renvoi un tableau de donn�es sur l'adresse de l'utilisateur : on prend juste le champs pass� en param�tre
function extractFromAdress(components, type){
    for (var i=0; i<components.length; i++)
        for (var j=0; j<components[i].types.length; j++)
            if (components[i].types[j]==type) return components[i].long_name;
    return "";
}

// impl�mente le wait en javascript
function wait(ms){
   var start = new Date().getTime();
   var end = start;
   while(end < start + ms) {
     end = new Date().getTime();
  }
}

// envoi en post
function envoyerDonneesEnPOST(page,nameData1,data1)
{
    var form = document.createElement('form');
    form.setAttribute('action', page);
    form.setAttribute('method', 'post');
    form.setAttribute('accept-charset', 'utf-8');
    var inputvar1 = document.createElement('input');
    inputvar1.setAttribute('type', 'hidden');
    inputvar1.setAttribute('name', nameData1);
    inputvar1.setAttribute('value', data1);
    form.appendChild(inputvar1);
   
    document.body.appendChild(form);
    form.submit();
}

</script>

<jsp:include page="footer.jsp" />