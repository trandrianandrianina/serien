<%@page import="java.util.LinkedHashMap"%>
<%@page import="java.util.Map.Entry"%>
<%@page import="ri.serien.libas400.dao.exp.programs.contact.M_Contact"%>
<%@page import="ri.serien.mobilite.crm.GestionFormulaire"%>
<% 
	LinkedHashMap<M_Contact, Boolean> listeContacts	= (LinkedHashMap<M_Contact, Boolean>)request.getAttribute("listeContacts");
	String urlRetour 								= (String)request.getAttribute("urlRetour");
%>

<jsp:include page="head.jsp" />

<section class='secContenu' id='listeMetier'>
	<h1 class='tetiereListes'>
		<span class='titreTetiere'>Liste contacts du client</span><span class='apportListe'>
				<%-- <a href='<%=urlRetour %>' id='action2Do'>Valider</a> --%>
		</span>
		<div class='triTetieres'>
			<a href='ActionsCom?tri=REPAC' class='teteListe' id='triClePrincip'>Nom</a>
			<a href='ActionsCom?tri=RETEL' class='teteListe' id='trivaleurPrincip'>Coordonn�es</a>
		</div>
	</h1>
		<% 	if( (listeContacts != null) && (listeContacts.size() > 0) ) {
				for(Entry<M_Contact, Boolean> entry : listeContacts.entrySet()) { 
					M_Contact elt = entry.getKey();
					boolean selected = entry.getValue();
		%>	
			<div class='listesClassiques'>
				<a href='Contact?action2do=<%=GestionFormulaire.CONSULTATION %>&idcontact=<%=elt.getRENUM() %>' class='detailsListe'>
					<div class="clePrincipCRM"><%=elt.getRECIV() + " " +elt.getREPAC() %></div>
					<div class="cleSecondCRM"><%=elt.getRECAT() %></div>
					<div class="valeurPrincipCRM"><%=elt.getRETEL() %></div>
				</a>
				<a href="#" class="selectionContact<%=selected?"F":"" %>" onclick="switchSelection(this,'selectionContact', '<%=elt.getREETB()+"-"+elt.getRENUM() %>' );"></a>
			</div>
			<%} %>
		<%} else {%>
			<p class="messageListes">Pas de contact pour ces crit�res</p>
		<%} %>
		<%-- <div class='listesClassiques'>
			<a href='Contact?action2do=<%=GestionFormulaire.CREATION %>' class='detailsListe'>
				<div class="clePrincipCRM">- Ajouter un contact -</div>
			</a>
		</div> --%>
		
		<div id='navFlottanteCRM'>
			<a class='lienNavFlottanteCRM' href='<%=urlRetour %>&annulModifsContact=1'  id='urlretour3'><img src='images/urlretour.png'/></a>
			<a class='lienNavFlottanteCRM' href='<%=urlRetour %>'  id='validerAction3'><img src='images/validerAction.png'/></a>
			<a class='lienNavFlottanteCRM' href='Contact?action2do=<%=GestionFormulaire.CREATION %>' id='nouvAction3'><img src='images/nouvAction.png'/></a>
		</div>
	
</section>

<jsp:include page="footer.jsp" />