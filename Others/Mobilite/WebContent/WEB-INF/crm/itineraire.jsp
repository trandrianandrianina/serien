<%@page import="ri.serien.libas400.database.record.GenericRecord"%>
<%
	GenericRecord unClient = (GenericRecord)request.getAttribute("unClient");

	String adresseArrivee = "\"";

	int indice = 0; 
	
	String[] tabAdresses = {"CLNOM","CLCPL","CLRUE","CLLOC","CLVIL"};
	for(int i= 0; i < tabAdresses.length; i++)
	{
		if(unClient.isPresentField(tabAdresses[i]) && !unClient.getField(tabAdresses[i]).toString().trim().equals(""))
		{
			if(indice>0)
				adresseArrivee += ",";
			indice++;
			adresseArrivee += unClient.getField(tabAdresses[i]).toString().trim();
		}
	}
	adresseArrivee += "\"";
	
	/*adresseArrivee = "\"5 RUE LOUIS COUMBA, KOUROU, FRANCE\"";
	adresseArrivee = "\"25 rue Darboussier, Toulouse\"";
	adresseArrivee = "\"5 rue louis Coumba, KOUROU\"";*/
%>
<jsp:include page="head.jsp" />



<section class='secContenu'>

	<script>
		// LES SCRIPTS SUIVANTS PERMETTENT :

		// - D'AFFICHER LA CARTE DANS LA PAGE
		// - DE PLACER LA POSITION ACTUELLE DE L'UTILISATEUR 
		// - DE CALCULER ET TRACER L'ITINERAIRE VERS L'ADRESSE DU CLIENT CHOISI A L'ECRAN PRECEDENT (listeItineraire.jsp)
	
		// adresse de d�part (position actuelle)
		var depart;
		// adresse de destination pour test
		var arrivee = <%=adresseArrivee%>;

		// renderer pour l'API direction
		directionsDisplay = new google.maps.DirectionsRenderer({
			draggable : true
		});
		// Au chargement on lance l'initialisation du processus de trac� de la carte
		onload = 'initialize()';
	</script>

	<h1>
		<span class='titreH1'>Itineraire <%=unClient.getField("CLNOM")%></span>
		<a class='optionsListe' href='#'><img id='imgOptionsListe' src='images/optionsListe.png' /></a>
	</h1>
		<%
			if (unClient != null) {
		%>

		<!-- appel de l'API Google -->
		<script src="https://maps.googleapis.com/maps/api/js?callback=initialize" async defer></script>

	<form action='itineraire' method='post' name='monItineraire' id='monItineraire'>
		<div id='carteItineraire'></div>
		
		<!-- Formulaire d'affichage de l'adresse de destination -->
		<div id='blocSaisieItineraire'>
			<label class='labelsSaisie'>Soci�t� ou raison sociale</label> 
			<input type="text" name="iti_societe" maxlength="30" value="<%=unClient.getField("CLNOM")%>" class='inputPleins' disabled="disabled" /> 
			<label class='labelsSaisie'>Compl�ment de nom</label> 
			<input type="text" name="iti_cpl" maxlength="30" value="<%=unClient.getField("CLCPL")%>" class='inputPleins' disabled="disabled" /> 
			<label class='labelsSaisie'>Adresse</label> 
			<input type="text" name="iti_adresse1" maxlength="30" value="<%=unClient.getField("CLRUE")%>" class='inputPleins' id='adresse1' disabled="disabled" />
			<!-- <label class='labelsSaisie'>Adresse 2</label>  -->
			<input type="text" name="iti_adresse2" maxlength="30" value="<%=unClient.getField("CLLOC")%>" class='inputPleins' id='adresse2'  disabled="disabled" />  
			<!-- <label class='labelsSaisie' id='lbCDP'>Code postal</label> --> 
			<%-- <input type="text" name="iti_codePostal" maxlength="5" value="<%=unClient.getField("CLCDP1")%>" id='zoneCDP' disabled="disabled" />  --%>
			<!-- <label class='labelsSaisie'>Ville</label> --> 
			<input type="text" name="iti_ville" maxlength="30" value="<%=unClient.getField("CLVIL")%>" class='inputPleins' disabled="disabled" /> 
			<label class='labelsSaisie'>T�l�phone</label> 
			<input type="tel" name="iti_telephone" maxlength="20" value="" class='inputPleins' disabled="disabled" /> 
			<!-- <label class='labelsSaisie'>Mail</label> 
			<input type="email" name="iti_email" maxlength="300" value="" class='inputPleins' disabled="disabled" />  -->
			<input type="hidden" name="rechercheItineraire" value="1" />
		</div>
	</form>
		<%
			} else {
		%>
		<!-- CARTE GEOLOCALISATION PAR DEFAUT -->
		<p>Pas de donn�es pour ce client</p>
		<%
			}
		%>
</section>

<script>
	//
	// MAP GOOGLE //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

	// Cr�ation de la carte en entr�e sur la page	
	function initialize(hauteur, largeur, adresse, pays) {

		// Objets n�cessaires � l'API direction
		var directionsService = new google.maps.DirectionsService;
		var directionsDisplay = new google.maps.DirectionsRenderer;

		// Options d'affichage de la carte
		var mapOptions = {
			zoom : 14
		};

		// int�gration de la carte � la DIV
		map = new google.maps.Map(document.getElementById('carteItineraire'),
				mapOptions);

		// API direction sur la carte
		directionsDisplay.setMap(map);

		// Try HTML5 geolocation
		if (navigator.geolocation) {
			navigator.geolocation.getCurrentPosition(function(position) {

				pos = new google.maps.LatLng(position.coords.latitude,
						position.coords.longitude);

				// marker de la position actuelle de l'utilisateur
				var infowindow = new google.maps.Marker({
					map : map,
					animation : google.maps.Animation.DROP,
					title : 'Votre position actuelle',
					path : google.maps.SymbolPath.CIRCLE,
					scale : 5,
					fillColor : "#00F",
					fillOpacity : 0.7,
					strokeWeight : 1,
					position : pos
				});

				// on fixe le centre de la carte
				map.setCenter(pos);

				//tests d'erreurs
			}, function() {
				handleNoGeolocation(true);
			});
		} else {
			// le navigateur ne supporte pas la g�olocalisation
			handleNoGeolocation(false);
		}

		// Appel de la fonction de calcul et tra�age d'itin�raire
		calculateAndDisplayRoute(directionsService, directionsDisplay);
	}

	// Si pas de g�oloc
	function handleNoGeolocation(errorFlag) {
		if (errorFlag) {
			var content = 'Error: The Geolocation service failed.';
		} else {
			var content = 'Error: Your browser doesn\'t support geolocation.';
		}

		var options = {
			map : map,
			position : new google.maps.LatLng(48.833, 2.333),
			content : content
		};

		var infowindow = new google.maps.InfoWindow(options);
		map.setCenter(options.position);
	}

	// Calcule et affiche l'itin�raire
	function calculateAndDisplayRoute(directionsService, directionsDisplay) {
		// on fixe le d�part � la position actuelle de l'utilisateur, d�tect�e par le sensor
		navigator.geolocation.getCurrentPosition(function(position) {
			var depart = "";
			depart = new google.maps.LatLng(position.coords.latitude,
					position.coords.longitude);

			var infowindow = new google.maps.Marker({
				map : map,
				position : depart
			});

			// itin�raire
			directionsService.route({
				origin : depart,
				destination : arrivee,
				travelMode : google.maps.TravelMode.DRIVING
			}, function(response, status) {
				if (status === google.maps.DirectionsStatus.OK) {
					directionsDisplay.setDirections(response);
				} else {
					window.alert('Directions request failed due to ' + status);
				}
			});
		})

	}
</script>

<jsp:include page="footer.jsp" />