
package ri.serien.libecranrpg.s163.S163100F;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class S163100F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public S163100F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    pnlbpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@@TITPG2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    

  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miMisePlanningActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    pnlbpresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlRepertoirePrincipal = new SNPanelTitre();
    lbRepertoirePrincipal = new SNLabelTitre();
    REP00 = new XRiTextField();
    LBR00 = new XRiTextField();
    btpINTERFACES = new JTabbedPane();
    pnlInterfacesDescendantes = new SNPanel();
    pnlRepertoireInterfaceDescendante = new SNPanelTitre();
    lbRepertoirInterfaceDescendante = new SNLabelTitre();
    RPO00 = new XRiTextField();
    LBO00 = new XRiTextField();
    pnlDefinitionInterfacesDescendantes = new SNPanelTitre();
    lbRepertoireArticleInterfaceDescendante = new SNLabelTitre();
    lbLabelFichierInterfacesDescendantes = new SNLabelTitre();
    lbRepertoireArticles_Interfacesdescendantes = new SNLabelChamp();
    RPO01 = new XRiTextField();
    lbInterface03 = new SNLabelChamp();
    LFO01 = new XRiTextField();
    lbRepertoireAchats_Interfacesdescendantes = new SNLabelChamp();
    RPO04 = new XRiTextField();
    lbInterface14 = new SNLabelChamp();
    LFO05 = new XRiTextField();
    lbRepertoireVentes_Interfacesdescendantes = new SNLabelChamp();
    RPO05 = new XRiTextField();
    lbInterface16 = new SNLabelChamp();
    LFO06 = new XRiTextField();
    lbRepertoireStocks_Interfacesdescendantes = new SNLabelChamp();
    RPO06 = new XRiTextField();
    lbInterface17 = new SNLabelChamp();
    LFO07 = new XRiTextField();
    pnlInterfacesRemontantes = new SNPanel();
    pnlRepertoireInterfaceRemontantes2 = new SNPanelTitre();
    lbRepertoirInterfaceRemontante = new SNLabelTitre();
    RPI00 = new XRiTextField();
    LBI00 = new XRiTextField();
    pnlDéfinitionInterfacesRemontantes = new SNPanelTitre();
    lbRepertoireArticleInterfaceRemontantes = new SNLabelTitre();
    lbLabelFichierInterfacesremontantes = new SNLabelTitre();
    lbRepertoireAchats_Interfacesremontantes = new SNLabelChamp();
    RPI02 = new XRiTextField();
    lbInterfaceS3 = new SNLabelChamp();
    LFI02 = new XRiTextField();
    lbRepertoireVentes_Interfacesremontantes = new SNLabelChamp();
    RPI03 = new XRiTextField();
    lbInterfaceS4 = new SNLabelChamp();
    LFI04 = new XRiTextField();
    lbRepertoireStocks_Interfacesremontantes = new SNLabelChamp();
    RPI04 = new XRiTextField();
    lbInterface55 = new SNLabelChamp();
    LFI06 = new XRiTextField();
    lbInterface62 = new SNLabelChamp();
    LFI07 = new XRiTextField();
    lbCodePlanning = new SNLabelTitre();
    lbExecutionTravail = new SNLabelTitre();
    lbExecutionTravail2 = new SNLabelTitre();
    PLAI02 = new XRiTextField();
    DLJI02 = new XRiTextField();
    ARRI02 = new XRiTextField();
    PLAI04 = new XRiTextField();
    DLJI04 = new XRiTextField();
    ARRI04 = new XRiTextField();
    lbInterface52 = new SNLabelChamp();
    LFI05 = new XRiTextField();
    PLAI05 = new XRiTextField();
    DLJI05 = new XRiTextField();
    ARRI05 = new XRiTextField();
    PLAI06 = new XRiTextField();
    DLJI06 = new XRiTextField();
    ARRI06 = new XRiTextField();
    PLAI07 = new XRiTextField();
    DLJI07 = new XRiTextField();
    ARRI07 = new XRiTextField();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    miMisePlanning = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- pnlbpresentation ----
      pnlbpresentation.setText("@TITPG1@@TITPG2@");
      pnlbpresentation.setName("pnlbpresentation");
      pnlBandeau.add(pnlbpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

      //======== pnlRepertoirePrincipal ========
      {
        pnlRepertoirePrincipal.setName("pnlRepertoirePrincipal");
        pnlRepertoirePrincipal.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlRepertoirePrincipal.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
        ((GridBagLayout)pnlRepertoirePrincipal.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlRepertoirePrincipal.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
        ((GridBagLayout)pnlRepertoirePrincipal.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //---- lbRepertoirePrincipal ----
        lbRepertoirePrincipal.setText("R\u00e9pertoire principal");
        lbRepertoirePrincipal.setVerticalAlignment(SwingConstants.CENTER);
        lbRepertoirePrincipal.setName("lbRepertoirePrincipal");
        pnlRepertoirePrincipal.add(lbRepertoirePrincipal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- REP00 ----
        REP00.setFont(new Font("sansserif", Font.PLAIN, 14));
        REP00.setMaximumSize(new Dimension(140, 30));
        REP00.setMinimumSize(new Dimension(140, 30));
        REP00.setPreferredSize(new Dimension(140, 30));
        REP00.setName("REP00");
        pnlRepertoirePrincipal.add(REP00, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- LBR00 ----
        LBR00.setFont(new Font("sansserif", Font.PLAIN, 14));
        LBR00.setMaximumSize(new Dimension(400, 30));
        LBR00.setMinimumSize(new Dimension(400, 30));
        LBR00.setRequestFocusEnabled(false);
        LBR00.setPreferredSize(new Dimension(400, 30));
        LBR00.setName("LBR00");
        pnlRepertoirePrincipal.add(LBR00, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlRepertoirePrincipal, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== btpINTERFACES ========
      {
        btpINTERFACES.setFont(new Font("sansserif", Font.PLAIN, 14));
        btpINTERFACES.setName("btpINTERFACES");

        //======== pnlInterfacesDescendantes ========
        {
          pnlInterfacesDescendantes.setName("pnlInterfacesDescendantes");
          pnlInterfacesDescendantes.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInterfacesDescendantes.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlInterfacesDescendantes.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInterfacesDescendantes.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlInterfacesDescendantes.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

          //======== pnlRepertoireInterfaceDescendante ========
          {
            pnlRepertoireInterfaceDescendante.setName("pnlRepertoireInterfaceDescendante");
            pnlRepertoireInterfaceDescendante.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlRepertoireInterfaceDescendante.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlRepertoireInterfaceDescendante.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlRepertoireInterfaceDescendante.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlRepertoireInterfaceDescendante.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbRepertoirInterfaceDescendante ----
            lbRepertoirInterfaceDescendante.setText("R\u00e9pertoire interfaces descendantes");
            lbRepertoirInterfaceDescendante.setMaximumSize(new Dimension(250, 30));
            lbRepertoirInterfaceDescendante.setMinimumSize(new Dimension(250, 30));
            lbRepertoirInterfaceDescendante.setPreferredSize(new Dimension(250, 30));
            lbRepertoirInterfaceDescendante.setVerticalAlignment(SwingConstants.CENTER);
            lbRepertoirInterfaceDescendante.setName("lbRepertoirInterfaceDescendante");
            pnlRepertoireInterfaceDescendante.add(lbRepertoirInterfaceDescendante, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- RPO00 ----
            RPO00.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPO00.setMaximumSize(new Dimension(140, 30));
            RPO00.setMinimumSize(new Dimension(140, 30));
            RPO00.setPreferredSize(new Dimension(140, 30));
            RPO00.setName("RPO00");
            pnlRepertoireInterfaceDescendante.add(RPO00, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- LBO00 ----
            LBO00.setFont(new Font("sansserif", Font.PLAIN, 14));
            LBO00.setMaximumSize(new Dimension(400, 30));
            LBO00.setMinimumSize(new Dimension(400, 30));
            LBO00.setRequestFocusEnabled(false);
            LBO00.setPreferredSize(new Dimension(400, 30));
            LBO00.setName("LBO00");
            pnlRepertoireInterfaceDescendante.add(LBO00, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInterfacesDescendantes.add(pnlRepertoireInterfaceDescendante, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlDefinitionInterfacesDescendantes ========
          {
            pnlDefinitionInterfacesDescendantes.setName("pnlDefinitionInterfacesDescendantes");
            pnlDefinitionInterfacesDescendantes.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDefinitionInterfacesDescendantes.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)pnlDefinitionInterfacesDescendantes.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlDefinitionInterfacesDescendantes.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDefinitionInterfacesDescendantes.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- lbRepertoireArticleInterfaceDescendante ----
            lbRepertoireArticleInterfaceDescendante.setText("R\u00e9pertoire");
            lbRepertoireArticleInterfaceDescendante.setMaximumSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceDescendante.setMinimumSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceDescendante.setPreferredSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceDescendante.setHorizontalAlignment(SwingConstants.CENTER);
            lbRepertoireArticleInterfaceDescendante.setName("lbRepertoireArticleInterfaceDescendante");
            pnlDefinitionInterfacesDescendantes.add(lbRepertoireArticleInterfaceDescendante, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLabelFichierInterfacesDescendantes ----
            lbLabelFichierInterfacesDescendantes.setText("Label Fichiers");
            lbLabelFichierInterfacesDescendantes.setMaximumSize(new Dimension(180, 30));
            lbLabelFichierInterfacesDescendantes.setMinimumSize(new Dimension(180, 30));
            lbLabelFichierInterfacesDescendantes.setPreferredSize(new Dimension(180, 30));
            lbLabelFichierInterfacesDescendantes.setHorizontalAlignment(SwingConstants.CENTER);
            lbLabelFichierInterfacesDescendantes.setName("lbLabelFichierInterfacesDescendantes");
            pnlDefinitionInterfacesDescendantes.add(lbLabelFichierInterfacesDescendantes, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRepertoireArticles_Interfacesdescendantes ----
            lbRepertoireArticles_Interfacesdescendantes.setText("ARTICLES");
            lbRepertoireArticles_Interfacesdescendantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireArticles_Interfacesdescendantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireArticles_Interfacesdescendantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireArticles_Interfacesdescendantes.setName("lbRepertoireArticles_Interfacesdescendantes");
            pnlDefinitionInterfacesDescendantes.add(lbRepertoireArticles_Interfacesdescendantes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPO01 ----
            RPO01.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPO01.setMaximumSize(new Dimension(140, 30));
            RPO01.setMinimumSize(new Dimension(140, 30));
            RPO01.setPreferredSize(new Dimension(140, 30));
            RPO01.setName("RPO01");
            pnlDefinitionInterfacesDescendantes.add(RPO01, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterface03 ----
            lbInterface03.setText("Interface 03");
            lbInterface03.setMaximumSize(new Dimension(120, 30));
            lbInterface03.setMinimumSize(new Dimension(120, 30));
            lbInterface03.setPreferredSize(new Dimension(120, 30));
            lbInterface03.setName("lbInterface03");
            pnlDefinitionInterfacesDescendantes.add(lbInterface03, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFO01 ----
            LFO01.setMaximumSize(new Dimension(140, 30));
            LFO01.setMinimumSize(new Dimension(140, 30));
            LFO01.setPreferredSize(new Dimension(140, 30));
            LFO01.setDoubleBuffered(true);
            LFO01.setName("LFO01");
            pnlDefinitionInterfacesDescendantes.add(LFO01, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRepertoireAchats_Interfacesdescendantes ----
            lbRepertoireAchats_Interfacesdescendantes.setText("ACHATS");
            lbRepertoireAchats_Interfacesdescendantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesdescendantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesdescendantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesdescendantes.setName("lbRepertoireAchats_Interfacesdescendantes");
            pnlDefinitionInterfacesDescendantes.add(lbRepertoireAchats_Interfacesdescendantes, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPO04 ----
            RPO04.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPO04.setMaximumSize(new Dimension(140, 30));
            RPO04.setMinimumSize(new Dimension(140, 30));
            RPO04.setPreferredSize(new Dimension(140, 30));
            RPO04.setName("RPO04");
            pnlDefinitionInterfacesDescendantes.add(RPO04, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterface14 ----
            lbInterface14.setText("Interface 14");
            lbInterface14.setMaximumSize(new Dimension(120, 30));
            lbInterface14.setMinimumSize(new Dimension(120, 30));
            lbInterface14.setPreferredSize(new Dimension(120, 30));
            lbInterface14.setName("lbInterface14");
            pnlDefinitionInterfacesDescendantes.add(lbInterface14, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFO05 ----
            LFO05.setMaximumSize(new Dimension(140, 30));
            LFO05.setMinimumSize(new Dimension(140, 30));
            LFO05.setPreferredSize(new Dimension(140, 30));
            LFO05.setName("LFO05");
            pnlDefinitionInterfacesDescendantes.add(LFO05, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRepertoireVentes_Interfacesdescendantes ----
            lbRepertoireVentes_Interfacesdescendantes.setText("VENTES");
            lbRepertoireVentes_Interfacesdescendantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesdescendantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesdescendantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesdescendantes.setName("lbRepertoireVentes_Interfacesdescendantes");
            pnlDefinitionInterfacesDescendantes.add(lbRepertoireVentes_Interfacesdescendantes, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPO05 ----
            RPO05.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPO05.setMaximumSize(new Dimension(140, 30));
            RPO05.setMinimumSize(new Dimension(140, 30));
            RPO05.setPreferredSize(new Dimension(140, 30));
            RPO05.setName("RPO05");
            pnlDefinitionInterfacesDescendantes.add(RPO05, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterface16 ----
            lbInterface16.setText("Interface 16");
            lbInterface16.setMaximumSize(new Dimension(120, 30));
            lbInterface16.setMinimumSize(new Dimension(120, 30));
            lbInterface16.setPreferredSize(new Dimension(120, 30));
            lbInterface16.setName("lbInterface16");
            pnlDefinitionInterfacesDescendantes.add(lbInterface16, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFO06 ----
            LFO06.setMaximumSize(new Dimension(140, 30));
            LFO06.setMinimumSize(new Dimension(140, 30));
            LFO06.setPreferredSize(new Dimension(140, 30));
            LFO06.setName("LFO06");
            pnlDefinitionInterfacesDescendantes.add(LFO06, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbRepertoireStocks_Interfacesdescendantes ----
            lbRepertoireStocks_Interfacesdescendantes.setText("STOCKS");
            lbRepertoireStocks_Interfacesdescendantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesdescendantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesdescendantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesdescendantes.setName("lbRepertoireStocks_Interfacesdescendantes");
            pnlDefinitionInterfacesDescendantes.add(lbRepertoireStocks_Interfacesdescendantes, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- RPO06 ----
            RPO06.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPO06.setMaximumSize(new Dimension(140, 30));
            RPO06.setMinimumSize(new Dimension(140, 30));
            RPO06.setPreferredSize(new Dimension(140, 30));
            RPO06.setName("RPO06");
            pnlDefinitionInterfacesDescendantes.add(RPO06, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbInterface17 ----
            lbInterface17.setText("Interface 17");
            lbInterface17.setMaximumSize(new Dimension(120, 30));
            lbInterface17.setMinimumSize(new Dimension(120, 30));
            lbInterface17.setPreferredSize(new Dimension(120, 30));
            lbInterface17.setName("lbInterface17");
            pnlDefinitionInterfacesDescendantes.add(lbInterface17, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- LFO07 ----
            LFO07.setMaximumSize(new Dimension(140, 30));
            LFO07.setMinimumSize(new Dimension(140, 30));
            LFO07.setPreferredSize(new Dimension(140, 30));
            LFO07.setName("LFO07");
            pnlDefinitionInterfacesDescendantes.add(LFO07, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInterfacesDescendantes.add(pnlDefinitionInterfacesDescendantes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        btpINTERFACES.addTab("Interfaces descendantes", pnlInterfacesDescendantes);

        //======== pnlInterfacesRemontantes ========
        {
          pnlInterfacesRemontantes.setName("pnlInterfacesRemontantes");
          pnlInterfacesRemontantes.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlInterfacesRemontantes.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlInterfacesRemontantes.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlInterfacesRemontantes.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)pnlInterfacesRemontantes.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

          //======== pnlRepertoireInterfaceRemontantes2 ========
          {
            pnlRepertoireInterfaceRemontantes2.setName("pnlRepertoireInterfaceRemontantes2");
            pnlRepertoireInterfaceRemontantes2.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlRepertoireInterfaceRemontantes2.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlRepertoireInterfaceRemontantes2.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlRepertoireInterfaceRemontantes2.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};
            ((GridBagLayout)pnlRepertoireInterfaceRemontantes2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- lbRepertoirInterfaceRemontante ----
            lbRepertoirInterfaceRemontante.setText("R\u00e9pertoire interfaces remontantes");
            lbRepertoirInterfaceRemontante.setMaximumSize(new Dimension(250, 30));
            lbRepertoirInterfaceRemontante.setMinimumSize(new Dimension(250, 30));
            lbRepertoirInterfaceRemontante.setPreferredSize(new Dimension(250, 30));
            lbRepertoirInterfaceRemontante.setVerticalAlignment(SwingConstants.CENTER);
            lbRepertoirInterfaceRemontante.setName("lbRepertoirInterfaceRemontante");
            pnlRepertoireInterfaceRemontantes2.add(lbRepertoirInterfaceRemontante, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- RPI00 ----
            RPI00.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPI00.setMaximumSize(new Dimension(140, 30));
            RPI00.setMinimumSize(new Dimension(140, 30));
            RPI00.setPreferredSize(new Dimension(140, 30));
            RPI00.setName("RPI00");
            pnlRepertoireInterfaceRemontantes2.add(RPI00, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- LBI00 ----
            LBI00.setFont(new Font("sansserif", Font.PLAIN, 14));
            LBI00.setMaximumSize(new Dimension(400, 30));
            LBI00.setMinimumSize(new Dimension(400, 30));
            LBI00.setRequestFocusEnabled(false);
            LBI00.setPreferredSize(new Dimension(400, 30));
            LBI00.setName("LBI00");
            pnlRepertoireInterfaceRemontantes2.add(LBI00, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInterfacesRemontantes.add(pnlRepertoireInterfaceRemontantes2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== pnlDéfinitionInterfacesRemontantes ========
          {
            pnlDéfinitionInterfacesRemontantes.setName("pnlD\u00e9finitionInterfacesRemontantes");
            pnlDéfinitionInterfacesRemontantes.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDéfinitionInterfacesRemontantes.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlDéfinitionInterfacesRemontantes.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlDéfinitionInterfacesRemontantes.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDéfinitionInterfacesRemontantes.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- lbRepertoireArticleInterfaceRemontantes ----
            lbRepertoireArticleInterfaceRemontantes.setText("R\u00e9pertoire");
            lbRepertoireArticleInterfaceRemontantes.setMaximumSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceRemontantes.setMinimumSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceRemontantes.setPreferredSize(new Dimension(120, 30));
            lbRepertoireArticleInterfaceRemontantes.setHorizontalAlignment(SwingConstants.CENTER);
            lbRepertoireArticleInterfaceRemontantes.setName("lbRepertoireArticleInterfaceRemontantes");
            pnlDéfinitionInterfacesRemontantes.add(lbRepertoireArticleInterfaceRemontantes, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbLabelFichierInterfacesremontantes ----
            lbLabelFichierInterfacesremontantes.setText("Label Fichiers");
            lbLabelFichierInterfacesremontantes.setMaximumSize(new Dimension(180, 30));
            lbLabelFichierInterfacesremontantes.setMinimumSize(new Dimension(180, 30));
            lbLabelFichierInterfacesremontantes.setPreferredSize(new Dimension(180, 30));
            lbLabelFichierInterfacesremontantes.setHorizontalAlignment(SwingConstants.CENTER);
            lbLabelFichierInterfacesremontantes.setName("lbLabelFichierInterfacesremontantes");
            pnlDéfinitionInterfacesRemontantes.add(lbLabelFichierInterfacesremontantes, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbRepertoireAchats_Interfacesremontantes ----
            lbRepertoireAchats_Interfacesremontantes.setText("ACHATS");
            lbRepertoireAchats_Interfacesremontantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesremontantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesremontantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireAchats_Interfacesremontantes.setName("lbRepertoireAchats_Interfacesremontantes");
            pnlDéfinitionInterfacesRemontantes.add(lbRepertoireAchats_Interfacesremontantes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPI02 ----
            RPI02.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPI02.setMaximumSize(new Dimension(140, 30));
            RPI02.setMinimumSize(new Dimension(140, 30));
            RPI02.setPreferredSize(new Dimension(140, 30));
            RPI02.setName("RPI02");
            pnlDéfinitionInterfacesRemontantes.add(RPI02, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterfaceS3 ----
            lbInterfaceS3.setText("Interface S3");
            lbInterfaceS3.setMaximumSize(new Dimension(120, 30));
            lbInterfaceS3.setMinimumSize(new Dimension(120, 30));
            lbInterfaceS3.setPreferredSize(new Dimension(120, 30));
            lbInterfaceS3.setName("lbInterfaceS3");
            pnlDéfinitionInterfacesRemontantes.add(lbInterfaceS3, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFI02 ----
            LFI02.setMaximumSize(new Dimension(140, 30));
            LFI02.setMinimumSize(new Dimension(140, 30));
            LFI02.setPreferredSize(new Dimension(140, 30));
            LFI02.setName("LFI02");
            pnlDéfinitionInterfacesRemontantes.add(LFI02, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbRepertoireVentes_Interfacesremontantes ----
            lbRepertoireVentes_Interfacesremontantes.setText("VENTES");
            lbRepertoireVentes_Interfacesremontantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesremontantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesremontantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireVentes_Interfacesremontantes.setName("lbRepertoireVentes_Interfacesremontantes");
            pnlDéfinitionInterfacesRemontantes.add(lbRepertoireVentes_Interfacesremontantes, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPI03 ----
            RPI03.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPI03.setMaximumSize(new Dimension(140, 30));
            RPI03.setMinimumSize(new Dimension(140, 30));
            RPI03.setPreferredSize(new Dimension(140, 30));
            RPI03.setName("RPI03");
            pnlDéfinitionInterfacesRemontantes.add(RPI03, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterfaceS4 ----
            lbInterfaceS4.setText("Interface S4");
            lbInterfaceS4.setMaximumSize(new Dimension(120, 30));
            lbInterfaceS4.setMinimumSize(new Dimension(120, 30));
            lbInterfaceS4.setPreferredSize(new Dimension(120, 30));
            lbInterfaceS4.setName("lbInterfaceS4");
            pnlDéfinitionInterfacesRemontantes.add(lbInterfaceS4, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFI04 ----
            LFI04.setMaximumSize(new Dimension(140, 30));
            LFI04.setMinimumSize(new Dimension(140, 30));
            LFI04.setPreferredSize(new Dimension(140, 30));
            LFI04.setName("LFI04");
            pnlDéfinitionInterfacesRemontantes.add(LFI04, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbRepertoireStocks_Interfacesremontantes ----
            lbRepertoireStocks_Interfacesremontantes.setText("STOCKS");
            lbRepertoireStocks_Interfacesremontantes.setMaximumSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesremontantes.setMinimumSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesremontantes.setPreferredSize(new Dimension(110, 30));
            lbRepertoireStocks_Interfacesremontantes.setName("lbRepertoireStocks_Interfacesremontantes");
            pnlDéfinitionInterfacesRemontantes.add(lbRepertoireStocks_Interfacesremontantes, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- RPI04 ----
            RPI04.setFont(new Font("sansserif", Font.PLAIN, 14));
            RPI04.setMaximumSize(new Dimension(140, 30));
            RPI04.setMinimumSize(new Dimension(140, 30));
            RPI04.setPreferredSize(new Dimension(140, 30));
            RPI04.setName("RPI04");
            pnlDéfinitionInterfacesRemontantes.add(RPI04, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterface55 ----
            lbInterface55.setText("Interface 55");
            lbInterface55.setMaximumSize(new Dimension(120, 30));
            lbInterface55.setMinimumSize(new Dimension(120, 30));
            lbInterface55.setPreferredSize(new Dimension(120, 30));
            lbInterface55.setName("lbInterface55");
            pnlDéfinitionInterfacesRemontantes.add(lbInterface55, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFI06 ----
            LFI06.setMaximumSize(new Dimension(140, 30));
            LFI06.setMinimumSize(new Dimension(140, 30));
            LFI06.setPreferredSize(new Dimension(140, 30));
            LFI06.setName("LFI06");
            pnlDéfinitionInterfacesRemontantes.add(LFI06, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbInterface62 ----
            lbInterface62.setText("Interface 62");
            lbInterface62.setMaximumSize(new Dimension(120, 30));
            lbInterface62.setMinimumSize(new Dimension(120, 30));
            lbInterface62.setPreferredSize(new Dimension(120, 30));
            lbInterface62.setName("lbInterface62");
            pnlDéfinitionInterfacesRemontantes.add(lbInterface62, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- LFI07 ----
            LFI07.setMaximumSize(new Dimension(140, 30));
            LFI07.setMinimumSize(new Dimension(140, 30));
            LFI07.setPreferredSize(new Dimension(140, 30));
            LFI07.setName("LFI07");
            pnlDéfinitionInterfacesRemontantes.add(LFI07, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbCodePlanning ----
            lbCodePlanning.setText("Code Planning");
            lbCodePlanning.setMaximumSize(new Dimension(120, 30));
            lbCodePlanning.setMinimumSize(new Dimension(120, 30));
            lbCodePlanning.setPreferredSize(new Dimension(120, 30));
            lbCodePlanning.setHorizontalAlignment(SwingConstants.CENTER);
            lbCodePlanning.setName("lbCodePlanning");
            pnlDéfinitionInterfacesRemontantes.add(lbCodePlanning, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbExecutionTravail ----
            lbExecutionTravail.setText("D\u00e9lai ex\u00e9cution");
            lbExecutionTravail.setHorizontalAlignment(SwingConstants.CENTER);
            lbExecutionTravail.setMaximumSize(new Dimension(110, 30));
            lbExecutionTravail.setMinimumSize(new Dimension(110, 30));
            lbExecutionTravail.setPreferredSize(new Dimension(110, 30));
            lbExecutionTravail.setName("lbExecutionTravail");
            pnlDéfinitionInterfacesRemontantes.add(lbExecutionTravail, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- lbExecutionTravail2 ----
            lbExecutionTravail2.setText("Heure arr\u00eat");
            lbExecutionTravail2.setMaximumSize(new Dimension(100, 30));
            lbExecutionTravail2.setMinimumSize(new Dimension(100, 30));
            lbExecutionTravail2.setPreferredSize(new Dimension(100, 30));
            lbExecutionTravail2.setHorizontalAlignment(SwingConstants.CENTER);
            lbExecutionTravail2.setName("lbExecutionTravail2");
            pnlDéfinitionInterfacesRemontantes.add(lbExecutionTravail2, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- PLAI02 ----
            PLAI02.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLAI02.setMaximumSize(new Dimension(140, 30));
            PLAI02.setMinimumSize(new Dimension(140, 30));
            PLAI02.setPreferredSize(new Dimension(140, 30));
            PLAI02.setComponentPopupMenu(BTD);
            PLAI02.setName("PLAI02");
            pnlDéfinitionInterfacesRemontantes.add(PLAI02, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- DLJI02 ----
            DLJI02.setMaximumSize(new Dimension(50, 30));
            DLJI02.setMinimumSize(new Dimension(50, 30));
            DLJI02.setPreferredSize(new Dimension(50, 30));
            DLJI02.setName("DLJI02");
            pnlDéfinitionInterfacesRemontantes.add(DLJI02, new GridBagConstraints(5, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARRI02 ----
            ARRI02.setMaximumSize(new Dimension(70, 30));
            ARRI02.setMinimumSize(new Dimension(70, 30));
            ARRI02.setPreferredSize(new Dimension(70, 30));
            ARRI02.setName("ARRI02");
            pnlDéfinitionInterfacesRemontantes.add(ARRI02, new GridBagConstraints(6, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- PLAI04 ----
            PLAI04.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLAI04.setMaximumSize(new Dimension(140, 30));
            PLAI04.setMinimumSize(new Dimension(140, 30));
            PLAI04.setPreferredSize(new Dimension(140, 30));
            PLAI04.setComponentPopupMenu(BTD);
            PLAI04.setName("PLAI04");
            pnlDéfinitionInterfacesRemontantes.add(PLAI04, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- DLJI04 ----
            DLJI04.setMaximumSize(new Dimension(50, 30));
            DLJI04.setMinimumSize(new Dimension(50, 30));
            DLJI04.setPreferredSize(new Dimension(50, 30));
            DLJI04.setName("DLJI04");
            pnlDéfinitionInterfacesRemontantes.add(DLJI04, new GridBagConstraints(5, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARRI04 ----
            ARRI04.setMaximumSize(new Dimension(70, 30));
            ARRI04.setMinimumSize(new Dimension(70, 30));
            ARRI04.setPreferredSize(new Dimension(70, 30));
            ARRI04.setName("ARRI04");
            pnlDéfinitionInterfacesRemontantes.add(ARRI04, new GridBagConstraints(6, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- lbInterface52 ----
            lbInterface52.setText("Interface 52");
            lbInterface52.setMaximumSize(new Dimension(120, 30));
            lbInterface52.setMinimumSize(new Dimension(120, 30));
            lbInterface52.setPreferredSize(new Dimension(120, 30));
            lbInterface52.setName("lbInterface52");
            pnlDéfinitionInterfacesRemontantes.add(lbInterface52, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- LFI05 ----
            LFI05.setMaximumSize(new Dimension(140, 30));
            LFI05.setMinimumSize(new Dimension(140, 30));
            LFI05.setPreferredSize(new Dimension(140, 30));
            LFI05.setName("LFI05");
            pnlDéfinitionInterfacesRemontantes.add(LFI05, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLAI05 ----
            PLAI05.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLAI05.setMaximumSize(new Dimension(140, 30));
            PLAI05.setMinimumSize(new Dimension(140, 30));
            PLAI05.setPreferredSize(new Dimension(140, 30));
            PLAI05.setComponentPopupMenu(BTD);
            PLAI05.setName("PLAI05");
            pnlDéfinitionInterfacesRemontantes.add(PLAI05, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- DLJI05 ----
            DLJI05.setMaximumSize(new Dimension(50, 30));
            DLJI05.setMinimumSize(new Dimension(50, 30));
            DLJI05.setPreferredSize(new Dimension(50, 30));
            DLJI05.setName("DLJI05");
            pnlDéfinitionInterfacesRemontantes.add(DLJI05, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARRI05 ----
            ARRI05.setMaximumSize(new Dimension(70, 30));
            ARRI05.setMinimumSize(new Dimension(70, 30));
            ARRI05.setPreferredSize(new Dimension(70, 30));
            ARRI05.setName("ARRI05");
            pnlDéfinitionInterfacesRemontantes.add(ARRI05, new GridBagConstraints(6, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- PLAI06 ----
            PLAI06.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLAI06.setMaximumSize(new Dimension(140, 30));
            PLAI06.setMinimumSize(new Dimension(140, 30));
            PLAI06.setPreferredSize(new Dimension(140, 30));
            PLAI06.setComponentPopupMenu(BTD);
            PLAI06.setName("PLAI06");
            pnlDéfinitionInterfacesRemontantes.add(PLAI06, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- DLJI06 ----
            DLJI06.setMaximumSize(new Dimension(50, 30));
            DLJI06.setMinimumSize(new Dimension(50, 30));
            DLJI06.setPreferredSize(new Dimension(50, 30));
            DLJI06.setName("DLJI06");
            pnlDéfinitionInterfacesRemontantes.add(DLJI06, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARRI06 ----
            ARRI06.setMaximumSize(new Dimension(70, 30));
            ARRI06.setMinimumSize(new Dimension(70, 30));
            ARRI06.setPreferredSize(new Dimension(70, 30));
            ARRI06.setName("ARRI06");
            pnlDéfinitionInterfacesRemontantes.add(ARRI06, new GridBagConstraints(6, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- PLAI07 ----
            PLAI07.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLAI07.setMaximumSize(new Dimension(140, 30));
            PLAI07.setMinimumSize(new Dimension(140, 30));
            PLAI07.setPreferredSize(new Dimension(140, 30));
            PLAI07.setComponentPopupMenu(BTD);
            PLAI07.setName("PLAI07");
            pnlDéfinitionInterfacesRemontantes.add(PLAI07, new GridBagConstraints(4, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- DLJI07 ----
            DLJI07.setMaximumSize(new Dimension(50, 30));
            DLJI07.setMinimumSize(new Dimension(50, 30));
            DLJI07.setPreferredSize(new Dimension(50, 30));
            DLJI07.setName("DLJI07");
            pnlDéfinitionInterfacesRemontantes.add(DLJI07, new GridBagConstraints(5, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARRI07 ----
            ARRI07.setMaximumSize(new Dimension(70, 30));
            ARRI07.setMinimumSize(new Dimension(70, 30));
            ARRI07.setPreferredSize(new Dimension(70, 30));
            ARRI07.setName("ARRI07");
            pnlDéfinitionInterfacesRemontantes.add(ARRI07, new GridBagConstraints(6, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlInterfacesRemontantes.add(pnlDéfinitionInterfacesRemontantes, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        btpINTERFACES.addTab("Interfaces remontantes", pnlInterfacesRemontantes);
      }
      pnlContenu.add(btpINTERFACES, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- miMisePlanning ----
      miMisePlanning.setText("Mise au planning");
      miMisePlanning.setMaximumSize(new Dimension(120, 30));
      miMisePlanning.setPreferredSize(new Dimension(120, 30));
      miMisePlanning.setMinimumSize(new Dimension(120, 30));
      miMisePlanning.setFont(new Font("sansserif", Font.PLAIN, 14));
      miMisePlanning.setName("miMisePlanning");
      miMisePlanning.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miMisePlanningActionPerformed(e);
        }
      });
      BTD.add(miMisePlanning);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre pnlbpresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlRepertoirePrincipal;
  private SNLabelTitre lbRepertoirePrincipal;
  private XRiTextField REP00;
  private XRiTextField LBR00;
  private JTabbedPane btpINTERFACES;
  private SNPanel pnlInterfacesDescendantes;
  private SNPanelTitre pnlRepertoireInterfaceDescendante;
  private SNLabelTitre lbRepertoirInterfaceDescendante;
  private XRiTextField RPO00;
  private XRiTextField LBO00;
  private SNPanelTitre pnlDefinitionInterfacesDescendantes;
  private SNLabelTitre lbRepertoireArticleInterfaceDescendante;
  private SNLabelTitre lbLabelFichierInterfacesDescendantes;
  private SNLabelChamp lbRepertoireArticles_Interfacesdescendantes;
  private XRiTextField RPO01;
  private SNLabelChamp lbInterface03;
  private XRiTextField LFO01;
  private SNLabelChamp lbRepertoireAchats_Interfacesdescendantes;
  private XRiTextField RPO04;
  private SNLabelChamp lbInterface14;
  private XRiTextField LFO05;
  private SNLabelChamp lbRepertoireVentes_Interfacesdescendantes;
  private XRiTextField RPO05;
  private SNLabelChamp lbInterface16;
  private XRiTextField LFO06;
  private SNLabelChamp lbRepertoireStocks_Interfacesdescendantes;
  private XRiTextField RPO06;
  private SNLabelChamp lbInterface17;
  private XRiTextField LFO07;
  private SNPanel pnlInterfacesRemontantes;
  private SNPanelTitre pnlRepertoireInterfaceRemontantes2;
  private SNLabelTitre lbRepertoirInterfaceRemontante;
  private XRiTextField RPI00;
  private XRiTextField LBI00;
  private SNPanelTitre pnlDéfinitionInterfacesRemontantes;
  private SNLabelTitre lbRepertoireArticleInterfaceRemontantes;
  private SNLabelTitre lbLabelFichierInterfacesremontantes;
  private SNLabelChamp lbRepertoireAchats_Interfacesremontantes;
  private XRiTextField RPI02;
  private SNLabelChamp lbInterfaceS3;
  private XRiTextField LFI02;
  private SNLabelChamp lbRepertoireVentes_Interfacesremontantes;
  private XRiTextField RPI03;
  private SNLabelChamp lbInterfaceS4;
  private XRiTextField LFI04;
  private SNLabelChamp lbRepertoireStocks_Interfacesremontantes;
  private XRiTextField RPI04;
  private SNLabelChamp lbInterface55;
  private XRiTextField LFI06;
  private SNLabelChamp lbInterface62;
  private XRiTextField LFI07;
  private SNLabelTitre lbCodePlanning;
  private SNLabelTitre lbExecutionTravail;
  private SNLabelTitre lbExecutionTravail2;
  private XRiTextField PLAI02;
  private XRiTextField DLJI02;
  private XRiTextField ARRI02;
  private XRiTextField PLAI04;
  private XRiTextField DLJI04;
  private XRiTextField ARRI04;
  private SNLabelChamp lbInterface52;
  private XRiTextField LFI05;
  private XRiTextField PLAI05;
  private XRiTextField DLJI05;
  private XRiTextField ARRI05;
  private XRiTextField PLAI06;
  private XRiTextField DLJI06;
  private XRiTextField ARRI06;
  private XRiTextField PLAI07;
  private XRiTextField DLJI07;
  private XRiTextField ARRI07;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem miMisePlanning;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
