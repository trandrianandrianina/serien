
package ri.serien.libecranrpg.rgvx.RGVX14FM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.plagedate.EnumDefilementPlageDate;
import ri.serien.libswing.composant.primitif.plagedate.SNPlageDate;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVX14FM_AI extends SNPanelEcranRPG implements ioFrame {
  
  
  // Boutons
  private static final String BOUTON_CREATION = "Créer";
  
  public RGVX14FM_AI(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    // Plage de dates
    snPlageDate.setDefilement(EnumDefilementPlageDate.DEFILEMENT_MOIS);
    snPlageDate.setDateDebutParChampRPG(lexique, "ARG7A");
    snPlageDate.setDateFinParChampRPG(lexique, "PLA7A");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.setTousAutorise(true);
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "ARG5A");
    
    // Magasinier
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "ARG10A");
    
    if (snEtablissement.getIdSelection() != null) {
      p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snPlageDate.renseignerChampRPGDebut(lexique, "ARG7A");
    snPlageDate.renseignerChampRPGFin(lexique, "PLA7A");
    snVendeur.renseignerChampRPG(lexique, "ARG10A");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
      snMagasin.charger(true);
      snMagasin.setSelectionParChampRPG(lexique, "ARG5A");
      if (snEtablissement.getIdSelection() != null) {
        p_bpresentation.setCodeEtablissement(snEtablissement.getIdSelection().getCodeEtablissement());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlPrincipal = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    INDNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDAT1X = new XRiCalendrier();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlCriteres = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbDateCreation = new SNLabelChamp();
    snPlageDate = new SNPlageDate();
    lbMagasinier = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbNumeroDebut = new SNLabelChamp();
    ARG2N = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Inventaires saisis");
      p_bpresentation.setName("p_bpresentation");
      pnlBandeau.add(p_bpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbNumero ----
          lbNumero.setText("Acc\u00e8s direct par num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setMinimumSize(new Dimension(70, 30));
          INDNUM.setMaximumSize(new Dimension(70, 30));
          INDNUM.setPreferredSize(new Dimension(70, 30));
          INDNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDNUM.setName("INDNUM");
          pnlGauche.add(INDNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateTraitement ----
          lbDateTraitement.setText("Date de traitement");
          lbDateTraitement.setMaximumSize(new Dimension(200, 30));
          lbDateTraitement.setMinimumSize(new Dimension(200, 30));
          lbDateTraitement.setPreferredSize(new Dimension(200, 30));
          lbDateTraitement.setInheritsPopupMenu(false);
          lbDateTraitement.setName("lbDateTraitement");
          pnlGauche.add(lbDateTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setMaximumSize(new Dimension(110, 30));
          WDAT1X.setMinimumSize(new Dimension(110, 30));
          WDAT1X.setPreferredSize(new Dimension(110, 30));
          WDAT1X.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDAT1X.setName("WDAT1X");
          pnlGauche.add(WDAT1X, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      pnlPrincipal.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlCriteres ========
      {
        pnlCriteres.setTitre("Crit\u00e8res de s\u00e9lection");
        pnlCriteres.setName("pnlCriteres");
        pnlCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlCriteres.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
        ((GridBagLayout)pnlCriteres.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlCriteres.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

        //---- lbMagasin ----
        lbMagasin.setText("Magasin inventori\u00e9");
        lbMagasin.setName("lbMagasin");
        pnlCriteres.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snMagasin ----
        snMagasin.setMaximumSize(new Dimension(500, 30));
        snMagasin.setMinimumSize(new Dimension(400, 30));
        snMagasin.setPreferredSize(new Dimension(400, 30));
        snMagasin.setName("snMagasin");
        pnlCriteres.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbDateCreation ----
        lbDateCreation.setText("Date de cr\u00e9ation");
        lbDateCreation.setName("lbDateCreation");
        pnlCriteres.add(lbDateCreation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snPlageDate ----
        snPlageDate.setName("snPlageDate");
        pnlCriteres.add(snPlageDate, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbMagasinier ----
        lbMagasinier.setText("Magasinier");
        lbMagasinier.setName("lbMagasinier");
        pnlCriteres.add(lbMagasinier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //---- snVendeur ----
        snVendeur.setPreferredSize(new Dimension(400, 30));
        snVendeur.setMinimumSize(new Dimension(400, 30));
        snVendeur.setMaximumSize(new Dimension(500, 30));
        snVendeur.setName("snVendeur");
        pnlCriteres.add(snVendeur, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //---- lbNumeroDebut ----
        lbNumeroDebut.setText("Num\u00e9ro de d\u00e9but");
        lbNumeroDebut.setName("lbNumeroDebut");
        pnlCriteres.add(lbNumeroDebut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //---- ARG2N ----
        ARG2N.setComponentPopupMenu(null);
        ARG2N.setPreferredSize(new Dimension(70, 30));
        ARG2N.setMinimumSize(new Dimension(70, 30));
        ARG2N.setMaximumSize(new Dimension(70, 30));
        ARG2N.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARG2N.setName("ARG2N");
        pnlCriteres.add(ARG2N, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
          GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlCriteres, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel pnlBandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField INDNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDAT1X;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlCriteres;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbDateCreation;
  private SNPlageDate snPlageDate;
  private SNLabelChamp lbMagasinier;
  private SNVendeur snVendeur;
  private SNLabelChamp lbNumeroDebut;
  private XRiTextField ARG2N;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
