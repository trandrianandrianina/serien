
package ri.serien.libecranrpg.rgvx.RGVXPXFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVXPXFM_PX extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RGVXPXFM_PX(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setValeurs("O", WTP01_GRP);
    WTP01_non.setValeurs("N");
    WTP02.setValeurs("O", WTP02_GRP);
    WTP02_non.setValeurs("N");
    WTP03.setValeurs("O", WTP03_GRP);
    WTP03_non.setValeurs("N");
    WTP04.setValeurs("O", WTP04_GRP);
    WTP04_non.setValeurs("N");
    WTP05.setValeurs("O", WTP05_GRP);
    WTP05_non.setValeurs("N");
    WTP06.setValeurs("O", WTP06_GRP);
    WTP06_non.setValeurs("N");
    WTP07.setValeurs("O", WTP07_GRP);
    WTP07_non.setValeurs("N");
    WTP08.setValeurs("O", WTP08_GRP);
    WTP08_non.setValeurs("N");
    WTP09.setValeurs("O", WTP09_GRP);
    WTP09_non.setValeurs("N");
    WTP10.setValeurs("O", WTP10_GRP);
    WTP10_non.setValeurs("N");
    WTP11.setValeurs("O", WTP11_GRP);
    WTP11_non.setValeurs("N");
    WTP12.setValeurs("O", WTP12_GRP);
    WTP12_non.setValeurs("N");
    WTP13.setValeurs("O", WTP13_GRP);
    WTP13_non.setValeurs("N");
    WTP14.setValeurs("O", WTP14_GRP);
    WTP14_non.setValeurs("N");
    WTP15.setValeurs("O", WTP15_GRP);
    WTP15_non.setValeurs("N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    xTitledPanel1.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@")).trim());
    LD01A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01A@")).trim());
    LD01B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01B@")).trim());
    LD02A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02A@")).trim());
    LD02B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02B@")).trim());
    LD03A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03A@")).trim());
    LD03B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03B@")).trim());
    LD04A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04A@")).trim());
    LD04B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04B@")).trim());
    LD05A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05A@")).trim());
    LD05B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05B@")).trim());
    LD06A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06A@")).trim());
    LD06B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06B@")).trim());
    LD07A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07A@")).trim());
    LD07B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07B@")).trim());
    LD08A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08A@")).trim());
    LD08B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08B@")).trim());
    LD09A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09A@")).trim());
    LD09B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09B@")).trim());
    LD10A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10A@")).trim());
    LD10B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10B@")).trim());
    LD11A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11A@")).trim());
    LD11B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11B@")).trim());
    LD12A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12A@")).trim());
    LD12B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12B@")).trim());
    LD13A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13A@")).trim());
    LD13B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13B@")).trim());
    LD14A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14A@")).trim());
    LD14B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14B@")).trim());
    LD15A.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15A@")).trim());
    LD15B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15B@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    boolean isLigne1 = !lexique.HostFieldGetData("LD01A").trim().equalsIgnoreCase("");
    boolean isLigne2 = !lexique.HostFieldGetData("LD02A").trim().equalsIgnoreCase("");
    boolean isLigne3 = !lexique.HostFieldGetData("LD03A").trim().equalsIgnoreCase("");
    boolean isLigne4 = !lexique.HostFieldGetData("LD04A").trim().equalsIgnoreCase("");
    boolean isLigne5 = !lexique.HostFieldGetData("LD05A").trim().equalsIgnoreCase("");
    boolean isLigne6 = !lexique.HostFieldGetData("LD06A").trim().equalsIgnoreCase("");
    boolean isLigne7 = !lexique.HostFieldGetData("LD07A").trim().equalsIgnoreCase("");
    boolean isLigne8 = !lexique.HostFieldGetData("LD08A").trim().equalsIgnoreCase("");
    boolean isLigne9 = !lexique.HostFieldGetData("LD09A").trim().equalsIgnoreCase("");
    boolean isLigne10 = !lexique.HostFieldGetData("LD10A").trim().equalsIgnoreCase("");
    boolean isLigne11 = !lexique.HostFieldGetData("LD11A").trim().equalsIgnoreCase("");
    boolean isLigne12 = !lexique.HostFieldGetData("LD12A").trim().equalsIgnoreCase("");
    boolean isLigne13 = !lexique.HostFieldGetData("LD13A").trim().equalsIgnoreCase("");
    boolean isLigne14 = !lexique.HostFieldGetData("LD14A").trim().equalsIgnoreCase("");
    boolean isLigne15 = !lexique.HostFieldGetData("LD15A").trim().equalsIgnoreCase("");
    
    LD01A.setVisible(isLigne1);
    LD01B.setVisible(isLigne1);
    WTP01.setVisible(isLigne1);
    WTP01_non.setVisible(isLigne1);
    
    LD02A.setVisible(isLigne2);
    LD02B.setVisible(isLigne2);
    WTP02.setVisible(isLigne2);
    WTP02_non.setVisible(isLigne2);
    
    LD03A.setVisible(isLigne3);
    LD03B.setVisible(isLigne3);
    WTP03.setVisible(isLigne3);
    WTP03_non.setVisible(isLigne3);
    
    LD04A.setVisible(isLigne4);
    LD04B.setVisible(isLigne4);
    WTP04.setVisible(isLigne4);
    WTP04_non.setVisible(isLigne4);
    
    LD05A.setVisible(isLigne5);
    LD05B.setVisible(isLigne5);
    WTP05.setVisible(isLigne5);
    WTP05_non.setVisible(isLigne5);
    
    LD06A.setVisible(isLigne6);
    LD06B.setVisible(isLigne6);
    WTP06.setVisible(isLigne6);
    WTP06_non.setVisible(isLigne6);
    
    LD07A.setVisible(isLigne7);
    LD07B.setVisible(isLigne7);
    WTP07.setVisible(isLigne7);
    WTP07_non.setVisible(isLigne7);
    
    LD08A.setVisible(isLigne8);
    LD08B.setVisible(isLigne8);
    WTP08.setVisible(isLigne8);
    WTP08_non.setVisible(isLigne8);
    
    LD09A.setVisible(isLigne9);
    LD09B.setVisible(isLigne9);
    WTP09.setVisible(isLigne9);
    WTP09_non.setVisible(isLigne9);
    
    LD10A.setVisible(isLigne10);
    LD10B.setVisible(isLigne10);
    WTP10.setVisible(isLigne10);
    WTP10_non.setVisible(isLigne10);
    
    LD11A.setVisible(isLigne11);
    LD11B.setVisible(isLigne11);
    WTP11.setVisible(isLigne11);
    WTP11_non.setVisible(isLigne11);
    
    LD12A.setVisible(isLigne12);
    LD12B.setVisible(isLigne12);
    WTP12.setVisible(isLigne12);
    WTP12_non.setVisible(isLigne12);
    
    LD13A.setVisible(isLigne13);
    LD13B.setVisible(isLigne13);
    WTP13.setVisible(isLigne13);
    WTP13_non.setVisible(isLigne13);
    
    LD14A.setVisible(isLigne14);
    LD14B.setVisible(isLigne14);
    WTP14.setVisible(isLigne14);
    WTP14_non.setVisible(isLigne14);
    
    LD15A.setVisible(isLigne15);
    LD15B.setVisible(isLigne15);
    WTP15.setVisible(isLigne15);
    WTP15_non.setVisible(isLigne15);
    
    p_bpresentation.setCodeEtablissement(PARETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(PARETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_44 = new JLabel();
    PARETB = new XRiTextField();
    OBJ_46 = new JLabel();
    PARTYP = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    LD01A = new RiZoneSortie();
    LD01B = new RiZoneSortie();
    LD02A = new RiZoneSortie();
    LD02B = new RiZoneSortie();
    WTP02 = new XRiRadioButton();
    LD03A = new RiZoneSortie();
    LD03B = new RiZoneSortie();
    WTP03 = new XRiRadioButton();
    LD04A = new RiZoneSortie();
    LD04B = new RiZoneSortie();
    WTP04 = new XRiRadioButton();
    LD05A = new RiZoneSortie();
    LD05B = new RiZoneSortie();
    WTP05 = new XRiRadioButton();
    LD06A = new RiZoneSortie();
    LD06B = new RiZoneSortie();
    WTP06 = new XRiRadioButton();
    LD07A = new RiZoneSortie();
    LD07B = new RiZoneSortie();
    WTP07 = new XRiRadioButton();
    LD08A = new RiZoneSortie();
    LD08B = new RiZoneSortie();
    WTP08 = new XRiRadioButton();
    LD09A = new RiZoneSortie();
    LD09B = new RiZoneSortie();
    WTP09 = new XRiRadioButton();
    LD10A = new RiZoneSortie();
    LD10B = new RiZoneSortie();
    WTP10 = new XRiRadioButton();
    LD11A = new RiZoneSortie();
    LD11B = new RiZoneSortie();
    WTP11 = new XRiRadioButton();
    LD12A = new RiZoneSortie();
    LD12B = new RiZoneSortie();
    WTP12 = new XRiRadioButton();
    LD13A = new RiZoneSortie();
    LD13B = new RiZoneSortie();
    WTP13 = new XRiRadioButton();
    LD14A = new RiZoneSortie();
    LD14B = new RiZoneSortie();
    WTP14 = new XRiRadioButton();
    LD15A = new RiZoneSortie();
    LD15B = new RiZoneSortie();
    WTP15 = new XRiRadioButton();
    label1 = new JLabel();
    label2 = new JLabel();
    WTP01 = new XRiRadioButton();
    WTP02_non = new XRiRadioButton();
    WTP03_non = new XRiRadioButton();
    WTP04_non = new XRiRadioButton();
    WTP05_non = new XRiRadioButton();
    WTP06_non = new XRiRadioButton();
    WTP07_non = new XRiRadioButton();
    WTP08_non = new XRiRadioButton();
    WTP09_non = new XRiRadioButton();
    WTP10_non = new XRiRadioButton();
    WTP11_non = new XRiRadioButton();
    WTP12_non = new XRiRadioButton();
    WTP13_non = new XRiRadioButton();
    WTP14_non = new XRiRadioButton();
    WTP15_non = new XRiRadioButton();
    WTP01_non = new XRiRadioButton();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    WTP02_GRP = new ButtonGroup();
    WTP03_GRP = new ButtonGroup();
    WTP04_GRP = new ButtonGroup();
    WTP05_GRP = new ButtonGroup();
    WTP06_GRP = new ButtonGroup();
    WTP07_GRP = new ButtonGroup();
    WTP08_GRP = new ButtonGroup();
    WTP09_GRP = new ButtonGroup();
    WTP10_GRP = new ButtonGroup();
    WTP11_GRP = new ButtonGroup();
    WTP12_GRP = new ButtonGroup();
    WTP13_GRP = new ButtonGroup();
    WTP14_GRP = new ButtonGroup();
    WTP15_GRP = new ButtonGroup();
    WTP01_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion commerciale");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_44 ----
          OBJ_44.setText("Etablissement");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(5, 5, 93, 18);

          //---- PARETB ----
          PARETB.setName("PARETB");
          p_tete_gauche.add(PARETB);
          PARETB.setBounds(100, 0, 40, PARETB.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Code");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(160, 5, 36, 18);

          //---- PARTYP ----
          PARTYP.setName("PARTYP");
          p_tete_gauche.add(PARTYP);
          PARTYP.setBounds(200, 0, 30, PARTYP.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(670, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("@TITRE@");
            xTitledPanel1.setBorder(new DropShadowBorder());
            xTitledPanel1.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- LD01A ----
            LD01A.setText("@LD01A@");
            LD01A.setName("LD01A");
            xTitledPanel1ContentContainer.add(LD01A);
            LD01A.setBounds(45, 45, 36, LD01A.getPreferredSize().height);

            //---- LD01B ----
            LD01B.setText("@LD01B@");
            LD01B.setName("LD01B");
            xTitledPanel1ContentContainer.add(LD01B);
            LD01B.setBounds(85, 45, 332, LD01B.getPreferredSize().height);

            //---- LD02A ----
            LD02A.setText("@LD02A@");
            LD02A.setName("LD02A");
            xTitledPanel1ContentContainer.add(LD02A);
            LD02A.setBounds(45, 70, 36, LD02A.getPreferredSize().height);

            //---- LD02B ----
            LD02B.setText("@LD02B@");
            LD02B.setName("LD02B");
            xTitledPanel1ContentContainer.add(LD02B);
            LD02B.setBounds(85, 70, 332, LD02B.getPreferredSize().height);

            //---- WTP02 ----
            WTP02.setText("Oui");
            WTP02.setName("WTP02");
            xTitledPanel1ContentContainer.add(WTP02);
            WTP02.setBounds(440, 70, 55, 24);

            //---- LD03A ----
            LD03A.setText("@LD03A@");
            LD03A.setName("LD03A");
            xTitledPanel1ContentContainer.add(LD03A);
            LD03A.setBounds(45, 95, 36, LD03A.getPreferredSize().height);

            //---- LD03B ----
            LD03B.setText("@LD03B@");
            LD03B.setName("LD03B");
            xTitledPanel1ContentContainer.add(LD03B);
            LD03B.setBounds(85, 95, 332, LD03B.getPreferredSize().height);

            //---- WTP03 ----
            WTP03.setText("Oui");
            WTP03.setName("WTP03");
            xTitledPanel1ContentContainer.add(WTP03);
            WTP03.setBounds(440, 95, 55, 24);

            //---- LD04A ----
            LD04A.setText("@LD04A@");
            LD04A.setName("LD04A");
            xTitledPanel1ContentContainer.add(LD04A);
            LD04A.setBounds(45, 120, 36, LD04A.getPreferredSize().height);

            //---- LD04B ----
            LD04B.setText("@LD04B@");
            LD04B.setName("LD04B");
            xTitledPanel1ContentContainer.add(LD04B);
            LD04B.setBounds(85, 120, 332, LD04B.getPreferredSize().height);

            //---- WTP04 ----
            WTP04.setText("Oui");
            WTP04.setName("WTP04");
            xTitledPanel1ContentContainer.add(WTP04);
            WTP04.setBounds(440, 120, 55, 24);

            //---- LD05A ----
            LD05A.setText("@LD05A@");
            LD05A.setName("LD05A");
            xTitledPanel1ContentContainer.add(LD05A);
            LD05A.setBounds(45, 145, 36, LD05A.getPreferredSize().height);

            //---- LD05B ----
            LD05B.setText("@LD05B@");
            LD05B.setName("LD05B");
            xTitledPanel1ContentContainer.add(LD05B);
            LD05B.setBounds(85, 145, 332, LD05B.getPreferredSize().height);

            //---- WTP05 ----
            WTP05.setText("Oui");
            WTP05.setName("WTP05");
            xTitledPanel1ContentContainer.add(WTP05);
            WTP05.setBounds(440, 145, 55, 24);

            //---- LD06A ----
            LD06A.setText("@LD06A@");
            LD06A.setName("LD06A");
            xTitledPanel1ContentContainer.add(LD06A);
            LD06A.setBounds(45, 170, 36, LD06A.getPreferredSize().height);

            //---- LD06B ----
            LD06B.setText("@LD06B@");
            LD06B.setName("LD06B");
            xTitledPanel1ContentContainer.add(LD06B);
            LD06B.setBounds(85, 170, 332, LD06B.getPreferredSize().height);

            //---- WTP06 ----
            WTP06.setText("Oui");
            WTP06.setName("WTP06");
            xTitledPanel1ContentContainer.add(WTP06);
            WTP06.setBounds(440, 170, 55, 24);

            //---- LD07A ----
            LD07A.setText("@LD07A@");
            LD07A.setName("LD07A");
            xTitledPanel1ContentContainer.add(LD07A);
            LD07A.setBounds(45, 195, 36, LD07A.getPreferredSize().height);

            //---- LD07B ----
            LD07B.setText("@LD07B@");
            LD07B.setName("LD07B");
            xTitledPanel1ContentContainer.add(LD07B);
            LD07B.setBounds(85, 195, 332, LD07B.getPreferredSize().height);

            //---- WTP07 ----
            WTP07.setText("Oui");
            WTP07.setName("WTP07");
            xTitledPanel1ContentContainer.add(WTP07);
            WTP07.setBounds(440, 195, 55, 24);

            //---- LD08A ----
            LD08A.setText("@LD08A@");
            LD08A.setName("LD08A");
            xTitledPanel1ContentContainer.add(LD08A);
            LD08A.setBounds(45, 220, 36, LD08A.getPreferredSize().height);

            //---- LD08B ----
            LD08B.setText("@LD08B@");
            LD08B.setName("LD08B");
            xTitledPanel1ContentContainer.add(LD08B);
            LD08B.setBounds(85, 220, 332, LD08B.getPreferredSize().height);

            //---- WTP08 ----
            WTP08.setText("Oui");
            WTP08.setName("WTP08");
            xTitledPanel1ContentContainer.add(WTP08);
            WTP08.setBounds(440, 220, 55, 24);

            //---- LD09A ----
            LD09A.setText("@LD09A@");
            LD09A.setName("LD09A");
            xTitledPanel1ContentContainer.add(LD09A);
            LD09A.setBounds(45, 245, 36, LD09A.getPreferredSize().height);

            //---- LD09B ----
            LD09B.setText("@LD09B@");
            LD09B.setName("LD09B");
            xTitledPanel1ContentContainer.add(LD09B);
            LD09B.setBounds(85, 245, 332, LD09B.getPreferredSize().height);

            //---- WTP09 ----
            WTP09.setText("Oui");
            WTP09.setName("WTP09");
            xTitledPanel1ContentContainer.add(WTP09);
            WTP09.setBounds(440, 245, 55, 24);

            //---- LD10A ----
            LD10A.setText("@LD10A@");
            LD10A.setName("LD10A");
            xTitledPanel1ContentContainer.add(LD10A);
            LD10A.setBounds(45, 270, 36, LD10A.getPreferredSize().height);

            //---- LD10B ----
            LD10B.setText("@LD10B@");
            LD10B.setName("LD10B");
            xTitledPanel1ContentContainer.add(LD10B);
            LD10B.setBounds(85, 270, 332, LD10B.getPreferredSize().height);

            //---- WTP10 ----
            WTP10.setText("Oui");
            WTP10.setName("WTP10");
            xTitledPanel1ContentContainer.add(WTP10);
            WTP10.setBounds(440, 270, 55, 24);

            //---- LD11A ----
            LD11A.setText("@LD11A@");
            LD11A.setName("LD11A");
            xTitledPanel1ContentContainer.add(LD11A);
            LD11A.setBounds(45, 295, 36, LD11A.getPreferredSize().height);

            //---- LD11B ----
            LD11B.setText("@LD11B@");
            LD11B.setName("LD11B");
            xTitledPanel1ContentContainer.add(LD11B);
            LD11B.setBounds(85, 295, 332, LD11B.getPreferredSize().height);

            //---- WTP11 ----
            WTP11.setText("Oui");
            WTP11.setName("WTP11");
            xTitledPanel1ContentContainer.add(WTP11);
            WTP11.setBounds(440, 295, 55, 24);

            //---- LD12A ----
            LD12A.setText("@LD12A@");
            LD12A.setName("LD12A");
            xTitledPanel1ContentContainer.add(LD12A);
            LD12A.setBounds(45, 320, 36, LD12A.getPreferredSize().height);

            //---- LD12B ----
            LD12B.setText("@LD12B@");
            LD12B.setName("LD12B");
            xTitledPanel1ContentContainer.add(LD12B);
            LD12B.setBounds(85, 320, 332, LD12B.getPreferredSize().height);

            //---- WTP12 ----
            WTP12.setText("Oui");
            WTP12.setName("WTP12");
            xTitledPanel1ContentContainer.add(WTP12);
            WTP12.setBounds(440, 320, 55, 24);

            //---- LD13A ----
            LD13A.setText("@LD13A@");
            LD13A.setName("LD13A");
            xTitledPanel1ContentContainer.add(LD13A);
            LD13A.setBounds(45, 345, 36, LD13A.getPreferredSize().height);

            //---- LD13B ----
            LD13B.setText("@LD13B@");
            LD13B.setName("LD13B");
            xTitledPanel1ContentContainer.add(LD13B);
            LD13B.setBounds(85, 345, 332, LD13B.getPreferredSize().height);

            //---- WTP13 ----
            WTP13.setText("Oui");
            WTP13.setName("WTP13");
            xTitledPanel1ContentContainer.add(WTP13);
            WTP13.setBounds(440, 345, 55, 24);

            //---- LD14A ----
            LD14A.setText("@LD14A@");
            LD14A.setName("LD14A");
            xTitledPanel1ContentContainer.add(LD14A);
            LD14A.setBounds(45, 370, 36, LD14A.getPreferredSize().height);

            //---- LD14B ----
            LD14B.setText("@LD14B@");
            LD14B.setName("LD14B");
            xTitledPanel1ContentContainer.add(LD14B);
            LD14B.setBounds(85, 370, 332, LD14B.getPreferredSize().height);

            //---- WTP14 ----
            WTP14.setText("Oui");
            WTP14.setName("WTP14");
            xTitledPanel1ContentContainer.add(WTP14);
            WTP14.setBounds(440, 370, 55, 24);

            //---- LD15A ----
            LD15A.setText("@LD15A@");
            LD15A.setName("LD15A");
            xTitledPanel1ContentContainer.add(LD15A);
            LD15A.setBounds(45, 395, 36, LD15A.getPreferredSize().height);

            //---- LD15B ----
            LD15B.setText("@LD15B@");
            LD15B.setName("LD15B");
            xTitledPanel1ContentContainer.add(LD15B);
            LD15B.setBounds(85, 395, 332, LD15B.getPreferredSize().height);

            //---- WTP15 ----
            WTP15.setText("Oui");
            WTP15.setName("WTP15");
            xTitledPanel1ContentContainer.add(WTP15);
            WTP15.setBounds(440, 395, 55, 24);

            //---- label1 ----
            label1.setText("N\u00b0");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            xTitledPanel1ContentContainer.add(label1);
            label1.setBounds(45, 25, 36, 20);

            //---- label2 ----
            label2.setText("Nom de zone");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            xTitledPanel1ContentContainer.add(label2);
            label2.setBounds(85, 25, 332, 20);

            //---- WTP01 ----
            WTP01.setText("Oui");
            WTP01.setName("WTP01");
            xTitledPanel1ContentContainer.add(WTP01);
            WTP01.setBounds(440, 45, 55, 24);

            //---- WTP02_non ----
            WTP02_non.setText("Non");
            WTP02_non.setName("WTP02_non");
            xTitledPanel1ContentContainer.add(WTP02_non);
            WTP02_non.setBounds(515, 70, 55, 24);

            //---- WTP03_non ----
            WTP03_non.setText("Non");
            WTP03_non.setName("WTP03_non");
            xTitledPanel1ContentContainer.add(WTP03_non);
            WTP03_non.setBounds(515, 95, 55, 24);

            //---- WTP04_non ----
            WTP04_non.setText("Non");
            WTP04_non.setName("WTP04_non");
            xTitledPanel1ContentContainer.add(WTP04_non);
            WTP04_non.setBounds(515, 120, 55, 24);

            //---- WTP05_non ----
            WTP05_non.setText("Non");
            WTP05_non.setName("WTP05_non");
            xTitledPanel1ContentContainer.add(WTP05_non);
            WTP05_non.setBounds(515, 145, 55, 24);

            //---- WTP06_non ----
            WTP06_non.setText("Non");
            WTP06_non.setName("WTP06_non");
            xTitledPanel1ContentContainer.add(WTP06_non);
            WTP06_non.setBounds(515, 170, 55, 24);

            //---- WTP07_non ----
            WTP07_non.setText("Non");
            WTP07_non.setName("WTP07_non");
            xTitledPanel1ContentContainer.add(WTP07_non);
            WTP07_non.setBounds(515, 195, 55, 24);

            //---- WTP08_non ----
            WTP08_non.setText("Non");
            WTP08_non.setName("WTP08_non");
            xTitledPanel1ContentContainer.add(WTP08_non);
            WTP08_non.setBounds(515, 220, 55, 24);

            //---- WTP09_non ----
            WTP09_non.setText("Non");
            WTP09_non.setName("WTP09_non");
            xTitledPanel1ContentContainer.add(WTP09_non);
            WTP09_non.setBounds(515, 245, 55, 24);

            //---- WTP10_non ----
            WTP10_non.setText("Non");
            WTP10_non.setName("WTP10_non");
            xTitledPanel1ContentContainer.add(WTP10_non);
            WTP10_non.setBounds(515, 270, 55, 24);

            //---- WTP11_non ----
            WTP11_non.setText("Non");
            WTP11_non.setName("WTP11_non");
            xTitledPanel1ContentContainer.add(WTP11_non);
            WTP11_non.setBounds(515, 295, 55, 24);

            //---- WTP12_non ----
            WTP12_non.setText("Non");
            WTP12_non.setName("WTP12_non");
            xTitledPanel1ContentContainer.add(WTP12_non);
            WTP12_non.setBounds(515, 320, 55, 24);

            //---- WTP13_non ----
            WTP13_non.setText("Non");
            WTP13_non.setName("WTP13_non");
            xTitledPanel1ContentContainer.add(WTP13_non);
            WTP13_non.setBounds(515, 345, 55, 24);

            //---- WTP14_non ----
            WTP14_non.setText("Non");
            WTP14_non.setName("WTP14_non");
            xTitledPanel1ContentContainer.add(WTP14_non);
            WTP14_non.setBounds(515, 370, 55, 24);

            //---- WTP15_non ----
            WTP15_non.setText("Non");
            WTP15_non.setName("WTP15_non");
            xTitledPanel1ContentContainer.add(WTP15_non);
            WTP15_non.setBounds(515, 395, 55, 24);

            //---- WTP01_non ----
            WTP01_non.setText("Non");
            WTP01_non.setName("WTP01_non");
            xTitledPanel1ContentContainer.add(WTP01_non);
            WTP01_non.setBounds(515, 45, 55, 24);

            //---- label3 ----
            label3.setText("Choisir");
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            xTitledPanel1ContentContainer.add(label3);
            label3.setBounds(440, 25, 130, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 650, Short.MAX_VALUE)
                .addGap(6, 6, 6))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(xTitledPanel1, GroupLayout.DEFAULT_SIZE, 472, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- WTP02_GRP ----
    WTP02_GRP.add(WTP02);
    WTP02_GRP.add(WTP02_non);

    //---- WTP03_GRP ----
    WTP03_GRP.add(WTP03);
    WTP03_GRP.add(WTP03_non);

    //---- WTP04_GRP ----
    WTP04_GRP.add(WTP04);
    WTP04_GRP.add(WTP04_non);

    //---- WTP05_GRP ----
    WTP05_GRP.add(WTP05);
    WTP05_GRP.add(WTP05_non);

    //---- WTP06_GRP ----
    WTP06_GRP.add(WTP06);
    WTP06_GRP.add(WTP06_non);

    //---- WTP07_GRP ----
    WTP07_GRP.add(WTP07);
    WTP07_GRP.add(WTP07_non);

    //---- WTP08_GRP ----
    WTP08_GRP.add(WTP08);
    WTP08_GRP.add(WTP08_non);

    //---- WTP09_GRP ----
    WTP09_GRP.add(WTP09);
    WTP09_GRP.add(WTP09_non);

    //---- WTP10_GRP ----
    WTP10_GRP.add(WTP10);
    WTP10_GRP.add(WTP10_non);

    //---- WTP11_GRP ----
    WTP11_GRP.add(WTP11);
    WTP11_GRP.add(WTP11_non);

    //---- WTP12_GRP ----
    WTP12_GRP.add(WTP12);
    WTP12_GRP.add(WTP12_non);

    //---- WTP13_GRP ----
    WTP13_GRP.add(WTP13);
    WTP13_GRP.add(WTP13_non);

    //---- WTP14_GRP ----
    WTP14_GRP.add(WTP14);
    WTP14_GRP.add(WTP14_non);

    //---- WTP15_GRP ----
    WTP15_GRP.add(WTP15);
    WTP15_GRP.add(WTP15_non);

    //---- WTP01_GRP ----
    WTP01_GRP.add(WTP01);
    WTP01_GRP.add(WTP01_non);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_44;
  private XRiTextField PARETB;
  private JLabel OBJ_46;
  private XRiTextField PARTYP;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie LD01A;
  private RiZoneSortie LD01B;
  private RiZoneSortie LD02A;
  private RiZoneSortie LD02B;
  private XRiRadioButton WTP02;
  private RiZoneSortie LD03A;
  private RiZoneSortie LD03B;
  private XRiRadioButton WTP03;
  private RiZoneSortie LD04A;
  private RiZoneSortie LD04B;
  private XRiRadioButton WTP04;
  private RiZoneSortie LD05A;
  private RiZoneSortie LD05B;
  private XRiRadioButton WTP05;
  private RiZoneSortie LD06A;
  private RiZoneSortie LD06B;
  private XRiRadioButton WTP06;
  private RiZoneSortie LD07A;
  private RiZoneSortie LD07B;
  private XRiRadioButton WTP07;
  private RiZoneSortie LD08A;
  private RiZoneSortie LD08B;
  private XRiRadioButton WTP08;
  private RiZoneSortie LD09A;
  private RiZoneSortie LD09B;
  private XRiRadioButton WTP09;
  private RiZoneSortie LD10A;
  private RiZoneSortie LD10B;
  private XRiRadioButton WTP10;
  private RiZoneSortie LD11A;
  private RiZoneSortie LD11B;
  private XRiRadioButton WTP11;
  private RiZoneSortie LD12A;
  private RiZoneSortie LD12B;
  private XRiRadioButton WTP12;
  private RiZoneSortie LD13A;
  private RiZoneSortie LD13B;
  private XRiRadioButton WTP13;
  private RiZoneSortie LD14A;
  private RiZoneSortie LD14B;
  private XRiRadioButton WTP14;
  private RiZoneSortie LD15A;
  private RiZoneSortie LD15B;
  private XRiRadioButton WTP15;
  private JLabel label1;
  private JLabel label2;
  private XRiRadioButton WTP01;
  private XRiRadioButton WTP02_non;
  private XRiRadioButton WTP03_non;
  private XRiRadioButton WTP04_non;
  private XRiRadioButton WTP05_non;
  private XRiRadioButton WTP06_non;
  private XRiRadioButton WTP07_non;
  private XRiRadioButton WTP08_non;
  private XRiRadioButton WTP09_non;
  private XRiRadioButton WTP10_non;
  private XRiRadioButton WTP11_non;
  private XRiRadioButton WTP12_non;
  private XRiRadioButton WTP13_non;
  private XRiRadioButton WTP14_non;
  private XRiRadioButton WTP15_non;
  private XRiRadioButton WTP01_non;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  private ButtonGroup WTP02_GRP;
  private ButtonGroup WTP03_GRP;
  private ButtonGroup WTP04_GRP;
  private ButtonGroup WTP05_GRP;
  private ButtonGroup WTP06_GRP;
  private ButtonGroup WTP07_GRP;
  private ButtonGroup WTP08_GRP;
  private ButtonGroup WTP09_GRP;
  private ButtonGroup WTP10_GRP;
  private ButtonGroup WTP11_GRP;
  private ButtonGroup WTP12_GRP;
  private ButtonGroup WTP13_GRP;
  private ButtonGroup WTP14_GRP;
  private ButtonGroup WTP15_GRP;
  private ButtonGroup WTP01_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
