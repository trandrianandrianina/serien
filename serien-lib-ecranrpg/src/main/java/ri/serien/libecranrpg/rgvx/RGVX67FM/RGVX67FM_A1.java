
package ri.serien.libecranrpg.rgvx.RGVX67FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVX67FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] WTYPD_Value = { "", "AVR", "DIF", "GBA" };
  
  public RGVX67FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    WTYPD.setValeurs(WTYPD_Value, null);
    
    snBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut(BTD.getInvoker().getName());
      lexique.HostScreenSendKey(this, "F4");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnllContenu = new SNPanelContenu();
    pnlGauche = new SNPanel();
    sNPanelTitre1 = new SNPanelTitre();
    lbDerogation = new SNLabelChamp();
    ARG2A = new XRiTextField();
    lbRattachement = new SNLabelChamp();
    pnlRattachement = new SNPanel();
    ARG3A = new XRiTextField();
    ARG4A = new XRiTextField();
    lbDateExpe = new SNLabelChamp();
    pnlDateExpe = new SNPanel();
    ARG5A = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PLA5A = new XRiCalendrier();
    lbTypologie = new SNLabelChamp();
    WTYPD = new XRiComboBox();
    lbDateReception = new SNLabelChamp();
    pnlDateExpe2 = new SNPanel();
    WDRECDX = new XRiCalendrier();
    lbAu2 = new SNLabelChamp();
    WDRECFX = new XRiCalendrier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Recherche des ventes par d\u00e9rogations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnllContenu ========
    {
      pnllContenu.setName("pnllContenu");
      pnllContenu.setLayout(new GridLayout());

      //======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== sNPanelTitre1 ========
        {
          sNPanelTitre1.setTitre("Crit\u00e8res de s\u00e9lection");
          sNPanelTitre1.setName("sNPanelTitre1");
          sNPanelTitre1.setLayout(new GridBagLayout());
          ((GridBagLayout)sNPanelTitre1.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)sNPanelTitre1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
          ((GridBagLayout)sNPanelTitre1.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)sNPanelTitre1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

          //---- lbDerogation ----
          lbDerogation.setText("D\u00e9rogation");
          lbDerogation.setName("lbDerogation");
          sNPanelTitre1.add(lbDerogation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- ARG2A ----
          ARG2A.setComponentPopupMenu(BTD);
          ARG2A.setMinimumSize(new Dimension(80, 30));
          ARG2A.setMaximumSize(new Dimension(80, 30));
          ARG2A.setPreferredSize(new Dimension(80, 30));
          ARG2A.setFont(new Font("sansserif", Font.PLAIN, 14));
          ARG2A.setName("ARG2A");
          sNPanelTitre1.add(ARG2A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbRattachement ----
          lbRattachement.setText("Rattachement");
          lbRattachement.setName("lbRattachement");
          sNPanelTitre1.add(lbRattachement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlRattachement ========
          {
            pnlRattachement.setName("pnlRattachement");
            pnlRattachement.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlRattachement.getLayout()).columnWidths = new int[] {0, 0, 0};
            ((GridBagLayout)pnlRattachement.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlRattachement.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlRattachement.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- ARG3A ----
            ARG3A.setComponentPopupMenu(BTD);
            ARG3A.setMinimumSize(new Dimension(30, 30));
            ARG3A.setMaximumSize(new Dimension(30, 30));
            ARG3A.setPreferredSize(new Dimension(30, 30));
            ARG3A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG3A.setName("ARG3A");
            pnlRattachement.add(ARG3A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(BTD);
            ARG4A.setMinimumSize(new Dimension(250, 30));
            ARG4A.setMaximumSize(new Dimension(250, 30));
            ARG4A.setPreferredSize(new Dimension(250, 30));
            ARG4A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG4A.setName("ARG4A");
            pnlRattachement.add(ARG4A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlRattachement, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateExpe ----
          lbDateExpe.setText("Date d'exp\u00e9dition du");
          lbDateExpe.setName("lbDateExpe");
          sNPanelTitre1.add(lbDateExpe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== pnlDateExpe ========
          {
            pnlDateExpe.setName("pnlDateExpe");
            pnlDateExpe.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDateExpe.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlDateExpe.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlDateExpe.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDateExpe.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- ARG5A ----
            ARG5A.setMaximumSize(new Dimension(120, 30));
            ARG5A.setMinimumSize(new Dimension(120, 30));
            ARG5A.setPreferredSize(new Dimension(120, 30));
            ARG5A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG5A.setName("ARG5A");
            pnlDateExpe.add(ARG5A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbAu ----
            lbAu.setText("au");
            lbAu.setMaximumSize(new Dimension(30, 30));
            lbAu.setMinimumSize(new Dimension(30, 30));
            lbAu.setPreferredSize(new Dimension(30, 30));
            lbAu.setName("lbAu");
            pnlDateExpe.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- PLA5A ----
            PLA5A.setMaximumSize(new Dimension(120, 30));
            PLA5A.setMinimumSize(new Dimension(120, 30));
            PLA5A.setPreferredSize(new Dimension(120, 30));
            PLA5A.setFont(new Font("sansserif", Font.PLAIN, 14));
            PLA5A.setName("PLA5A");
            pnlDateExpe.add(PLA5A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlDateExpe, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTypologie ----
          lbTypologie.setText("Typologie de d\u00e9rogation");
          lbTypologie.setMaximumSize(new Dimension(200, 30));
          lbTypologie.setMinimumSize(new Dimension(200, 30));
          lbTypologie.setPreferredSize(new Dimension(200, 30));
          lbTypologie.setName("lbTypologie");
          sNPanelTitre1.add(lbTypologie, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- WTYPD ----
          WTYPD.setFont(new Font("sansserif", Font.PLAIN, 14));
          WTYPD.setBackground(Color.white);
          WTYPD.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Demandes d'avoir",
            "Achats diff\u00e9r\u00e9s",
            "GBA"
          }));
          WTYPD.setName("WTYPD");
          sNPanelTitre1.add(WTYPD, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateReception ----
          lbDateReception.setText("Date de r\u00e9ception du");
          lbDateReception.setName("lbDateReception");
          sNPanelTitre1.add(lbDateReception, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //======== pnlDateExpe2 ========
          {
            pnlDateExpe2.setName("pnlDateExpe2");
            pnlDateExpe2.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlDateExpe2.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
            ((GridBagLayout)pnlDateExpe2.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)pnlDateExpe2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)pnlDateExpe2.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- WDRECDX ----
            WDRECDX.setMaximumSize(new Dimension(120, 30));
            WDRECDX.setMinimumSize(new Dimension(120, 30));
            WDRECDX.setPreferredSize(new Dimension(120, 30));
            WDRECDX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDRECDX.setName("WDRECDX");
            pnlDateExpe2.add(WDRECDX, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- lbAu2 ----
            lbAu2.setText("au");
            lbAu2.setMaximumSize(new Dimension(30, 30));
            lbAu2.setMinimumSize(new Dimension(30, 30));
            lbAu2.setPreferredSize(new Dimension(30, 30));
            lbAu2.setName("lbAu2");
            pnlDateExpe2.add(lbAu2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- WDRECFX ----
            WDRECFX.setMaximumSize(new Dimension(120, 30));
            WDRECFX.setMinimumSize(new Dimension(120, 30));
            WDRECFX.setPreferredSize(new Dimension(120, 30));
            WDRECFX.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDRECFX.setName("WDRECFX");
            pnlDateExpe2.add(WDRECFX, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          sNPanelTitre1.add(pnlDateExpe2, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlGauche.add(sNPanelTitre1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllContenu.add(pnlGauche);

      //======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnllContenu.add(pnlDroite);
    }
    add(pnllContenu, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnllContenu;
  private SNPanel pnlGauche;
  private SNPanelTitre sNPanelTitre1;
  private SNLabelChamp lbDerogation;
  private XRiTextField ARG2A;
  private SNLabelChamp lbRattachement;
  private SNPanel pnlRattachement;
  private XRiTextField ARG3A;
  private XRiTextField ARG4A;
  private SNLabelChamp lbDateExpe;
  private SNPanel pnlDateExpe;
  private XRiCalendrier ARG5A;
  private SNLabelChamp lbAu;
  private XRiCalendrier PLA5A;
  private SNLabelChamp lbTypologie;
  private XRiComboBox WTYPD;
  private SNLabelChamp lbDateReception;
  private SNPanel pnlDateExpe2;
  private XRiCalendrier WDRECDX;
  private SNLabelChamp lbAu2;
  private XRiCalendrier WDRECFX;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
