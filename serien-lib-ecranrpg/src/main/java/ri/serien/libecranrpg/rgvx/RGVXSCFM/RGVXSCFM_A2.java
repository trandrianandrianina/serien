/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.rgvx.RGVXSCFM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.article.EnumFiltreArticleConsigne;
import ri.serien.libcommun.gescom.commun.client.IdClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.gescom.commun.fournisseur.IdFournisseur;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.chantier.snchantier.SNChantier;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * Liste des articles consignés et liste des mouvements des articles consignés.
 * 
 * "[GVM121] Gestion des ventes -> Fiches permanentes -> Clients -> Clients" puis "Articles consignés"
 * "[GAM12] Gestion des achats -> Fiches permanentes -> Fournisseurs" puis "Articles consignés"
 */
public class RGVXSCFM_A2 extends SNPanelEcranRPG implements ioFrame {
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1L", };
  private String[][] _WTP01_Data = { { "LDL01", }, { "LDL02", }, { "LDL03", }, { "LDL04", }, { "LDL05", }, { "LDL06", }, { "LDL07", },
      { "LDL08", }, { "LDL09", }, { "LDL10", }, { "LDL11", }, { "LDL12", }, { "LDL13", }, { "LDL14", }, { "LDL15", }, };
  private int[] _WTP01_Width = { 541, };
  
  // Boutons
  private static final String BOUTON_DETAIL_MOUVEMENTS = "Afficher mouvements";
  private static final String BOUTON_DETAIL_LIGNES = "Afficher articles";
  private static final String BOUTON_GERER_STOCKS = "Gérer stocks consignes";
  private static final String BOUTON_EXPORTER_LISTE = "Exporter liste";
  
  private IdEtablissement idEtablissement = null;
  private IdClient idClient = null;
  private IdFournisseur idFournisseur = null;
  
  /**
   * Constructeur.
   */
  public RGVXSCFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    bt_PFin.setIcon(lexique.chargerImage("images/pfin20.png", true));
    bt_PDeb.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    snBarreBouton.ajouterBouton(BOUTON_DETAIL_MOUVEMENTS, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_DETAIL_LIGNES, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_GERER_STOCKS, 'g', true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER_LISTE, 'x', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Configurer la barre de boutons de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WSOR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOR@")).trim());
    WENT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENT@")).trim());
    WSLD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSLD@")).trim());
  }
  
  /**
   * Rafraîchir les informations de l'écran en fonction des champs RPG.
   */
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Construire l'identifiant de l'établissement
    idEtablissement = IdEtablissement.getInstance(lexique.HostFieldGetData("WETB"));
    
    // Construire l'identifiant du client
    idClient = null;
    if (lexique.isTrue("21")) {
      idClient = IdClient.getInstance(idEtablissement, lexique.HostFieldGetData("WTI2"), lexique.HostFieldGetData("WTI3"));
    }
    
    // Construire l'identifiant du fournisseur
    idFournisseur = null;
    if (lexique.isTrue("22")) {
      idFournisseur = IdFournisseur.getInstance(idEtablissement, lexique.HostFieldGetData("WTI1"), lexique.HostFieldGetData("WTI2"));
    }
    
    // Rafraîchir les informations
    rafraichirTitre();
    rafraichirArticle();
    rafraichirMagasin();
    rafraichirChantier();
    rafraichirBoutonDetail();
    rafraichirBoutonGererStock();
  }
  
  /**
   * Mettre à jour les champs RPG à partir deses informations de l'écran.
   */
  @Override
  public void getData() {
    super.getData();
    
    // Article
    snArticle.renseignerChampRPG(lexique, "WART");
    // Magasin
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    // Chantier
    snChantier.renseignerChampRPG(lexique, "WCHAN");
  }
  
  /**
   * Rafraîchir le titre de l'écran.
   */
  private void rafraichirTitre() {
    String titre = "";
    
    // Sélectionner le titre en fonction de l'écran qui est affiché
    if (lexique.HostFieldGetData("DETAIL").trim().equals("0")) {
      titre += "Liste des articles consignés";
    }
    else {
      titre += "Mouvements des articles consignés";
    }
    
    // Tester si c'est pour les clients
    if (idClient != null) {
      String nomClient = lexique.HostFieldGetData("WNOM").trim();
      titre += " du client : " + nomClient + " (" + idClient + ")";
    }
    // Tester si c'est pour les fournisseurs
    else if (idFournisseur != null) {
      String nomFournisseur = lexique.HostFieldGetData("WNOM").trim();
      titre += " du fournisseur : " + nomFournisseur + " (" + idFournisseur + ")";
    }
    
    // Mettre à jour le titre
    p_bpresentation.setText(titre);
    
  }
  
  /**
   * Rafraîchir le composant article.
   */
  private void rafraichirArticle() {
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(idEtablissement);
    snArticle.setFiltreArticleConsigne(EnumFiltreArticleConsigne.OPTION_LES_ARTICLES_CONSIGNES_UNIQUEMENT);
    snArticle.setVerrouillerCriteres(true);
    snArticle.charger(false);
    snArticle.setVerrouillerCriteres(true);
    snArticle.setSelectionParChampRPG(lexique, "WART");
    
  }
  
  /**
   * Rafraîchir le composant magasin.
   */
  private void rafraichirMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(idEtablissement);
    snMagasin.charger(false);
    snMagasin.setTousAutorise(true);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
  }
  
  /**
   * Rafraîchir le composant chantier.
   */
  private void rafraichirChantier() {
    Boolean afficherChantier = lexique.isTrue("60");
    snChantier.setVisible(afficherChantier);
    lbChantier.setVisible(afficherChantier);
    
    if (idClient != null) {
      snChantier.setSession(getSession());
      snChantier.setIdEtablissement(idEtablissement);
      snChantier.setIdClient(idClient);
      snChantier.charger(false);
      snChantier.setSelectionParChampRPG(lexique, "WCHAN");
    }
  }
  
  /**
   * Rafraîchir le bouton détail.
   */
  private void rafraichirBoutonDetail() {
    snBarreBouton.activerBouton(BOUTON_DETAIL_MOUVEMENTS, (lexique.HostFieldGetData("DETAIL").trim().equals("0")));
    snBarreBouton.activerBouton(BOUTON_DETAIL_LIGNES, (!lexique.HostFieldGetData("DETAIL").trim().equals("0")));
  }
  
  /**
   * Rafraîchir le bouton stock.
   */
  private void rafraichirBoutonGererStock() {
    boolean gererStock = lexique.isTrue("61");
    MiGestion.setVisible(gererStock);
    snBarreBouton.activerBouton(BOUTON_GERER_STOCKS, gererStock);
  }
  
  /**
   * Traiter les clics sur les boutons.
   * 
   * @param pSNBouton Bouton qui a été cliqué.
   */
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_DETAIL_MOUVEMENTS)) {
        lexique.HostScreenSendKey(this, "F20");
      }
      else if (pSNBouton.isBouton(BOUTON_DETAIL_LIGNES)) {
        lexique.HostScreenSendKey(this, "F20");
      }
      else if (pSNBouton.isBouton(BOUTON_GERER_STOCKS)) {
        lexique.HostScreenSendKey(this, "F21");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER_LISTE)) {
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        initialiserRecherche();
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Initialiser les critères de recherche.
   */
  private void initialiserRecherche() {
    snArticle.initialiserRecherche();
    snMagasin.setSelection(null);
    snChantier.initialiserRecherche();
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void restrictionArticleActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void MiRestrictionChantierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void MiAnnulationActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("0");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void MiAjustementActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void MiGestionActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_PDebActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_PFinActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    pnlSud = new SNPanel();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlRecherche = new SNPanel();
    pnlCritereGauche = new SNPanel();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbChantier = new SNLabelChamp();
    snChantier = new SNChantier();
    pnlCritereDroite = new SNPanel();
    snBarreRecherche = new SNBarreRecherche();
    pnlResultat = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    sNPanel1 = new SNPanel();
    bt_PDeb = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    bt_PFin = new JButton();
    pnlTotaux = new SNPanelTitre();
    lbSorties = new SNLabelChamp();
    WSOR = new RiZoneSortie();
    lbRetours = new SNLabelChamp();
    WENT = new RiZoneSortie();
    lbSoldes = new SNLabelChamp();
    WSLD = new RiZoneSortie();
    BTD = new JPopupMenu();
    MiAjustement = new JMenuItem();
    restrictionArticle = new JMenuItem();
    MiRestrictionChantier = new JMenuItem();
    MiAnnulation = new JMenuItem();
    MiGestion = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Liste des articles consign\u00e9s (palettes...)");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== pnlSud ========
    {
      pnlSud.setName("pnlSud");
      pnlSud.setLayout(new BorderLayout());
      
      // ---- snBarreBouton ----
      snBarreBouton.setName("snBarreBouton");
      pnlSud.add(snBarreBouton, BorderLayout.SOUTH);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlRecherche ========
        {
          pnlRecherche.setName("pnlRecherche");
          pnlRecherche.setLayout(new GridLayout(1, 0, 5, 5));
          
          // ======== pnlCritereGauche ========
          {
            pnlCritereGauche.setName("pnlCritereGauche");
            pnlCritereGauche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlCritereGauche.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlCritereGauche.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbArticle ----
            lbArticle.setText("Article");
            lbArticle.setName("lbArticle");
            pnlCritereGauche.add(lbArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snArticle ----
            snArticle.setName("snArticle");
            pnlCritereGauche.add(snArticle, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbChantier ----
            lbChantier.setText("Chantier");
            lbChantier.setName("lbChantier");
            pnlCritereGauche.add(lbChantier, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snChantier ----
            snChantier.setName("snChantier");
            pnlCritereGauche.add(snChantier, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlCritereGauche);
          
          // ======== pnlCritereDroite ========
          {
            pnlCritereDroite.setName("pnlCritereDroite");
            pnlCritereDroite.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDroite.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlCritereDroite.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlCritereDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDroite.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ---- snBarreRecherche ----
            snBarreRecherche.setName("snBarreRecherche");
            pnlCritereDroite.add(snBarreRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTH,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlRecherche.add(pnlCritereDroite);
        }
        pnlContenu.add(pnlRecherche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlResultat ========
        {
          pnlResultat.setName("pnlResultat");
          pnlResultat.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlResultat.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlResultat.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlResultat.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlResultat.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setPreferredSize(new Dimension(456, 280));
            SCROLLPANE_LIST.setMinimumSize(new Dimension(456, 280));
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
            
            // ---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          pnlResultat.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== sNPanel1 ========
          {
            sNPanel1.setName("sNPanel1");
            sNPanel1.setLayout(new GridBagLayout());
            ((GridBagLayout) sNPanel1.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) sNPanel1.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) sNPanel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- bt_PDeb ----
            bt_PDeb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_PDeb.setMaximumSize(new Dimension(25, 30));
            bt_PDeb.setMinimumSize(new Dimension(25, 30));
            bt_PDeb.setPreferredSize(new Dimension(25, 30));
            bt_PDeb.setName("bt_PDeb");
            bt_PDeb.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_PDebActionPerformed(e);
              }
            });
            sNPanel1.add(bt_PDeb, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(25, 100));
            BT_PGUP.setMinimumSize(new Dimension(25, 100));
            BT_PGUP.setPreferredSize(new Dimension(25, 100));
            BT_PGUP.setName("BT_PGUP");
            sNPanel1.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(25, 100));
            BT_PGDOWN.setMinimumSize(new Dimension(25, 100));
            BT_PGDOWN.setPreferredSize(new Dimension(25, 100));
            BT_PGDOWN.setName("BT_PGDOWN");
            sNPanel1.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- bt_PFin ----
            bt_PFin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_PFin.setMaximumSize(new Dimension(25, 30));
            bt_PFin.setMinimumSize(new Dimension(25, 30));
            bt_PFin.setPreferredSize(new Dimension(25, 30));
            bt_PFin.setName("bt_PFin");
            bt_PFin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_PFinActionPerformed(e);
              }
            });
            sNPanel1.add(bt_PFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlResultat.add(sNPanel1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlResultat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlTotaux ========
        {
          pnlTotaux.setTitre("Totaux");
          pnlTotaux.setName("pnlTotaux");
          pnlTotaux.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlTotaux.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlTotaux.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlTotaux.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
          
          // ---- lbSorties ----
          lbSorties.setText("Sorties");
          lbSorties.setName("lbSorties");
          pnlTotaux.add(lbSorties, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WSOR ----
          WSOR.setText("@WSOR@");
          WSOR.setHorizontalAlignment(SwingConstants.RIGHT);
          WSOR.setPreferredSize(new Dimension(100, 30));
          WSOR.setMinimumSize(new Dimension(100, 30));
          WSOR.setMaximumSize(new Dimension(100, 30));
          WSOR.setFont(new Font("sansserif", Font.PLAIN, 14));
          WSOR.setName("WSOR");
          pnlTotaux.add(WSOR, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbRetours ----
          lbRetours.setText("Retours");
          lbRetours.setName("lbRetours");
          pnlTotaux.add(lbRetours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- WENT ----
          WENT.setText("@WENT@");
          WENT.setHorizontalAlignment(SwingConstants.RIGHT);
          WENT.setPreferredSize(new Dimension(100, 30));
          WENT.setMinimumSize(new Dimension(100, 30));
          WENT.setMaximumSize(new Dimension(100, 30));
          WENT.setFont(new Font("sansserif", Font.PLAIN, 14));
          WENT.setName("WENT");
          pnlTotaux.add(WENT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbSoldes ----
          lbSoldes.setText("Solde");
          lbSoldes.setFont(lbSoldes.getFont().deriveFont(lbSoldes.getFont().getStyle() | Font.BOLD));
          lbSoldes.setName("lbSoldes");
          pnlTotaux.add(lbSoldes, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WSLD ----
          WSLD.setText("@WSLD@");
          WSLD.setHorizontalAlignment(SwingConstants.RIGHT);
          WSLD.setPreferredSize(new Dimension(100, 30));
          WSLD.setMinimumSize(new Dimension(100, 30));
          WSLD.setMaximumSize(new Dimension(100, 30));
          WSLD.setFont(new Font("sansserif", Font.BOLD, 14));
          WSLD.setName("WSLD");
          pnlTotaux.add(WSLD, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlTotaux, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlSud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(pnlSud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- MiAjustement ----
      MiAjustement.setText("Ajustement stock");
      MiAjustement.setName("MiAjustement");
      MiAjustement.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MiAjustementActionPerformed(e);
        }
      });
      BTD.add(MiAjustement);
      
      // ---- restrictionArticle ----
      restrictionArticle.setText("Restriction article");
      restrictionArticle.setName("restrictionArticle");
      restrictionArticle.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          restrictionArticleActionPerformed(e);
        }
      });
      BTD.add(restrictionArticle);
      
      // ---- MiRestrictionChantier ----
      MiRestrictionChantier.setText("Restriction chantier");
      MiRestrictionChantier.setName("MiRestrictionChantier");
      MiRestrictionChantier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MiRestrictionChantierActionPerformed(e);
        }
      });
      BTD.add(MiRestrictionChantier);
      
      // ---- MiAnnulation ----
      MiAnnulation.setText("Annulation restrictions");
      MiAnnulation.setName("MiAnnulation");
      MiAnnulation.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MiAnnulationActionPerformed(e);
        }
      });
      BTD.add(MiAnnulation);
      
      // ---- MiGestion ----
      MiGestion.setText("G\u00e9rer stocks consignes");
      MiGestion.setName("MiGestion");
      MiGestion.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          MiGestionActionPerformed(e);
        }
      });
      BTD.add(MiGestion);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNPanel pnlSud;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlRecherche;
  private SNPanel pnlCritereGauche;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbChantier;
  private SNChantier snChantier;
  private SNPanel pnlCritereDroite;
  private SNBarreRecherche snBarreRecherche;
  private SNPanel pnlResultat;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel sNPanel1;
  private JButton bt_PDeb;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton bt_PFin;
  private SNPanelTitre pnlTotaux;
  private SNLabelChamp lbSorties;
  private RiZoneSortie WSOR;
  private SNLabelChamp lbRetours;
  private RiZoneSortie WENT;
  private SNLabelChamp lbSoldes;
  private RiZoneSortie WSLD;
  private JPopupMenu BTD;
  private JMenuItem MiAjustement;
  private JMenuItem restrictionArticle;
  private JMenuItem MiRestrictionChantier;
  private JMenuItem MiAnnulation;
  private JMenuItem MiGestion;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
