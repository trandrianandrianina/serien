
package ri.serien.libecranrpg.rgvx.RGVX14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JPanel;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVX14FM_AD extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_CREATION = "Créer";
  
  private String[] ARG6A1_Value = { "", "%", "V", };
  private String[] ARG6A2_Value = { "", "P", "R", "E", };
  private String[] ARG4A_Value = { "", "O", "1", };
  
  public RGVX14FM_AD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    ARG6A1.setValeurs(ARG6A1_Value, null);
    ARG6A2.setValeurs(ARG6A2_Value, null);
    ARG4A.setValeurs(ARG4A_Value, null);
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'c', true);
    snBarreBouton.ajouterBouton(EnumBouton.RECHERCHER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "INDETB");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlContenu = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    INDNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDAT1X = new XRiCalendrier();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlCriteres = new SNPanelTitre();
    sNLabelChamp1 = new SNLabelChamp();
    ARG6A1 = new XRiComboBox();
    sNLabelChamp2 = new SNLabelChamp();
    ARG4A = new XRiComboBox();
    lbPrix = new SNLabelChamp();
    ARG6A2 = new XRiComboBox();
    sNLabelChamp5 = new SNLabelChamp();
    pnlDateCreation = new SNPanel();
    ARG7A = new XRiCalendrier();
    lbAu = new SNLabelChamp();
    PLA7A = new XRiCalendrier();
    sNLabelChamp6 = new SNLabelChamp();
    pnlDateValidation = new SNPanel();
    ARG8A = new XRiCalendrier();
    lbAu2 = new SNLabelChamp();
    PLA8A = new XRiCalendrier();
    sNLabelChamp9 = new SNLabelChamp();
    ARG2N = new XRiTextField();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Recherche des bordereaux de d\u00e9pr\u00e9ciation");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbNumero ----
          lbNumero.setText("Acc\u00e8s direct par num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setMinimumSize(new Dimension(70, 30));
          INDNUM.setMaximumSize(new Dimension(70, 30));
          INDNUM.setPreferredSize(new Dimension(70, 30));
          INDNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDNUM.setName("INDNUM");
          pnlGauche.add(INDNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbDateTraitement ----
          lbDateTraitement.setText("Date de traitement");
          lbDateTraitement.setMaximumSize(new Dimension(200, 30));
          lbDateTraitement.setMinimumSize(new Dimension(200, 30));
          lbDateTraitement.setPreferredSize(new Dimension(200, 30));
          lbDateTraitement.setInheritsPopupMenu(false);
          lbDateTraitement.setName("lbDateTraitement");
          pnlGauche.add(lbDateTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setMaximumSize(new Dimension(110, 30));
          WDAT1X.setMinimumSize(new Dimension(110, 30));
          WDAT1X.setPreferredSize(new Dimension(110, 30));
          WDAT1X.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDAT1X.setName("WDAT1X");
          pnlGauche.add(WDAT1X, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      pnlContenu.add(pnlInformations,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlCriteres ========
      {
        pnlCriteres.setTitre("Crit\u00e8res de s\u00e9lection");
        pnlCriteres.setName("pnlCriteres");
        pnlCriteres.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCriteres.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCriteres.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCriteres.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCriteres.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- sNLabelChamp1 ----
        sNLabelChamp1.setText("Type de d\u00e9pr\u00e9ciation");
        sNLabelChamp1.setMaximumSize(new Dimension(200, 30));
        sNLabelChamp1.setMinimumSize(new Dimension(200, 30));
        sNLabelChamp1.setPreferredSize(new Dimension(200, 30));
        sNLabelChamp1.setName("sNLabelChamp1");
        pnlCriteres.add(sNLabelChamp1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ARG6A1 ----
        ARG6A1.setModel(new DefaultComboBoxModel(new String[] { "Tous", "Pourcentage", "Valeur" }));
        ARG6A1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARG6A1.setBackground(Color.white);
        ARG6A1.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARG6A1.setPreferredSize(new Dimension(400, 30));
        ARG6A1.setMinimumSize(new Dimension(400, 30));
        ARG6A1.setMaximumSize(new Dimension(500, 30));
        ARG6A1.setName("ARG6A1");
        pnlCriteres.add(ARG6A1, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp2 ----
        sNLabelChamp2.setText("Etat");
        sNLabelChamp2.setName("sNLabelChamp2");
        pnlCriteres.add(sNLabelChamp2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ARG4A ----
        ARG4A.setModel(
            new DefaultComboBoxModel(new String[] { "Attente et homologu\u00e9", "Attente seulement", "Homologu\u00e9 seulement" }));
        ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARG4A.setBackground(Color.white);
        ARG4A.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARG4A.setPreferredSize(new Dimension(400, 30));
        ARG4A.setMinimumSize(new Dimension(400, 30));
        ARG4A.setMaximumSize(new Dimension(500, 30));
        ARG4A.setName("ARG4A");
        pnlCriteres.add(ARG4A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbPrix ----
        lbPrix.setText("Prix");
        lbPrix.setName("lbPrix");
        pnlCriteres.add(lbPrix, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ARG6A2 ----
        ARG6A2.setModel(new DefaultComboBoxModel(
            new String[] { "Tous", "Prix unitaire moyen pond\u00e9r\u00e9", "Prix de revient", "Prix de derni\u00e8re entr\u00e9e" }));
        ARG6A2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARG6A2.setBackground(Color.white);
        ARG6A2.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARG6A2.setPreferredSize(new Dimension(400, 30));
        ARG6A2.setMinimumSize(new Dimension(400, 30));
        ARG6A2.setMaximumSize(new Dimension(500, 30));
        ARG6A2.setName("ARG6A2");
        pnlCriteres.add(ARG6A2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp5 ----
        sNLabelChamp5.setText("Date de cr\u00e9ation du");
        sNLabelChamp5.setName("sNLabelChamp5");
        pnlCriteres.add(sNLabelChamp5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlDateCreation ========
        {
          pnlDateCreation.setName("pnlDateCreation");
          pnlDateCreation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateCreation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateCreation.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateCreation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateCreation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- ARG7A ----
          ARG7A.setMaximumSize(new Dimension(110, 30));
          ARG7A.setMinimumSize(new Dimension(110, 30));
          ARG7A.setPreferredSize(new Dimension(110, 30));
          ARG7A.setName("ARG7A");
          pnlDateCreation.add(ARG7A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbAu ----
          lbAu.setText("au");
          lbAu.setMaximumSize(new Dimension(30, 30));
          lbAu.setMinimumSize(new Dimension(30, 30));
          lbAu.setPreferredSize(new Dimension(30, 30));
          lbAu.setName("lbAu");
          pnlDateCreation.add(lbAu, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- PLA7A ----
          PLA7A.setMaximumSize(new Dimension(110, 30));
          PLA7A.setMinimumSize(new Dimension(110, 30));
          PLA7A.setPreferredSize(new Dimension(110, 30));
          PLA7A.setName("PLA7A");
          pnlDateCreation.add(PLA7A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlDateCreation, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp6 ----
        sNLabelChamp6.setText("Date de validation du");
        sNLabelChamp6.setName("sNLabelChamp6");
        pnlCriteres.add(sNLabelChamp6, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ======== pnlDateValidation ========
        {
          pnlDateValidation.setName("pnlDateValidation");
          pnlDateValidation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDateValidation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
          ((GridBagLayout) pnlDateValidation.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDateValidation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlDateValidation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- ARG8A ----
          ARG8A.setMaximumSize(new Dimension(110, 30));
          ARG8A.setMinimumSize(new Dimension(110, 30));
          ARG8A.setPreferredSize(new Dimension(110, 30));
          ARG8A.setName("ARG8A");
          pnlDateValidation.add(ARG8A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbAu2 ----
          lbAu2.setText("au");
          lbAu2.setMaximumSize(new Dimension(30, 30));
          lbAu2.setMinimumSize(new Dimension(30, 30));
          lbAu2.setPreferredSize(new Dimension(30, 30));
          lbAu2.setName("lbAu2");
          pnlDateValidation.add(lbAu2, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- PLA8A ----
          PLA8A.setMaximumSize(new Dimension(110, 30));
          PLA8A.setMinimumSize(new Dimension(110, 30));
          PLA8A.setPreferredSize(new Dimension(110, 30));
          PLA8A.setName("PLA8A");
          pnlDateValidation.add(PLA8A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCriteres.add(pnlDateValidation, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- sNLabelChamp9 ----
        sNLabelChamp9.setText("Num\u00e9ro de d\u00e9but");
        sNLabelChamp9.setName("sNLabelChamp9");
        pnlCriteres.add(sNLabelChamp9, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- ARG2N ----
        ARG2N.setMinimumSize(new Dimension(70, 30));
        ARG2N.setMaximumSize(new Dimension(70, 30));
        ARG2N.setPreferredSize(new Dimension(70, 30));
        ARG2N.setName("ARG2N");
        pnlCriteres.add(ARG2N, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCriteres,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField INDNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDAT1X;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlCriteres;
  private SNLabelChamp sNLabelChamp1;
  private XRiComboBox ARG6A1;
  private SNLabelChamp sNLabelChamp2;
  private XRiComboBox ARG4A;
  private SNLabelChamp lbPrix;
  private XRiComboBox ARG6A2;
  private SNLabelChamp sNLabelChamp5;
  private SNPanel pnlDateCreation;
  private XRiCalendrier ARG7A;
  private SNLabelChamp lbAu;
  private XRiCalendrier PLA7A;
  private SNLabelChamp sNLabelChamp6;
  private SNPanel pnlDateValidation;
  private XRiCalendrier ARG8A;
  private SNLabelChamp lbAu2;
  private XRiCalendrier PLA8A;
  private SNLabelChamp sNLabelChamp9;
  private XRiTextField ARG2N;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
