
package ri.serien.libecranrpg.rgvx.RGVX86FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVX86FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public RGVX86FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    bt_PFin.setIcon(lexique.chargerImage("images/pfin20.png", true));
    bt_PDeb.setIcon(lexique.chargerImage("images/pdeb20.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDART@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    btnOuvrirURL1.setVisible(!lexique.HostFieldGetData("LD01").trim().equals(""));
    btnOuvrirURL2.setVisible(!lexique.HostFieldGetData("LD02").trim().equals(""));
    btnOuvrirURL3.setVisible(!lexique.HostFieldGetData("LD03").trim().equals(""));
    btnOuvrirURL4.setVisible(!lexique.HostFieldGetData("LD04").trim().equals(""));
    btnOuvrirURL5.setVisible(!lexique.HostFieldGetData("LD05").trim().equals(""));
    btnOuvrirURL6.setVisible(!lexique.HostFieldGetData("LD06").trim().equals(""));
    btnOuvrirURL7.setVisible(!lexique.HostFieldGetData("LD07").trim().equals(""));
    btnOuvrirURL8.setVisible(!lexique.HostFieldGetData("LD08").trim().equals(""));
    btnOuvrirURL9.setVisible(!lexique.HostFieldGetData("LD09").trim().equals(""));
    btnOuvrirURL10.setVisible(!lexique.HostFieldGetData("LD10").trim().equals(""));
    btnOuvrirURL11.setVisible(!lexique.HostFieldGetData("LD11").trim().equals(""));
    btnOuvrirURL12.setVisible(!lexique.HostFieldGetData("LD12").trim().equals(""));
    btnOuvrirURL13.setVisible(!lexique.HostFieldGetData("LD13").trim().equals(""));
    btnOuvrirURL14.setVisible(!lexique.HostFieldGetData("LD14").trim().equals(""));
    btnOuvrirURL15.setVisible(!lexique.HostFieldGetData("LD15").trim().equals(""));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bt_PDebActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_PFinActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ATTRIBUERActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btnOuvrirURL1ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD01"));
  }
  
  private void btnOuvrirURL2ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD02"));
  }
  
  private void btnOuvrirURL3ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD03"));
  }
  
  private void btnOuvrirURL4ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD04"));
  }
  
  private void btnOuvrirURL5ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD05"));
  }
  
  private void btnOuvrirURL6ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD06"));
  }
  
  private void btnOuvrirURL7ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD07"));
  }
  
  private void btnOuvrirURL8ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD08"));
  }
  
  private void btnOuvrirURL9ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD09"));
  }
  
  private void btnOuvrirURL10ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD10"));
  }
  
  private void btnOuvrirURL11ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD11"));
  }
  
  private void btnOuvrirURL12ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD12"));
  }
  
  private void btnOuvrirURL13ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD13"));
  }
  
  private void btnOuvrirURL14ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD14"));
  }
  
  private void btnOuvrirURL15ActionPerformed(ActionEvent e) {
    lexique.ouvrirURL(lexique.HostFieldGetData("LD15"));
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_67 = new JLabel();
    OBJ_46 = new JLabel();
    INDART = new RiZoneSortie();
    WETB = new RiZoneSortie();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    bt_PDeb = new JButton();
    bt_PFin = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    ATTRIBUER = new JMenuItem();
    btnOuvrirURL1 = new SNBoutonDetail();
    btnOuvrirURL2 = new SNBoutonDetail();
    btnOuvrirURL3 = new SNBoutonDetail();
    btnOuvrirURL4 = new SNBoutonDetail();
    btnOuvrirURL5 = new SNBoutonDetail();
    btnOuvrirURL6 = new SNBoutonDetail();
    btnOuvrirURL7 = new SNBoutonDetail();
    btnOuvrirURL8 = new SNBoutonDetail();
    btnOuvrirURL9 = new SNBoutonDetail();
    btnOuvrirURL10 = new SNBoutonDetail();
    btnOuvrirURL11 = new SNBoutonDetail();
    btnOuvrirURL12 = new SNBoutonDetail();
    btnOuvrirURL13 = new SNBoutonDetail();
    btnOuvrirURL14 = new SNBoutonDetail();
    btnOuvrirURL15 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des fiches techniques");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(1300, 32));
          p_tete_gauche.setMinimumSize(new Dimension(900, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_67 ----
          OBJ_67.setText("Etablissement");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(5, 5, 100, 20);

          //---- OBJ_46 ----
          OBJ_46.setText("Article");
          OBJ_46.setName("OBJ_46");
          p_tete_gauche.add(OBJ_46);
          OBJ_46.setBounds(170, 6, 50, 18);

          //---- INDART ----
          INDART.setComponentPopupMenu(null);
          INDART.setText("@INDART@");
          INDART.setHorizontalAlignment(SwingConstants.LEADING);
          INDART.setOpaque(false);
          INDART.setName("INDART");
          p_tete_gauche.add(INDART);
          INDART.setBounds(220, 3, 210, INDART.getPreferredSize().height);

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(105, 3, 40, WETB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(800, 340));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(800, 328));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(25, 20, 690, 269);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(725, 53, 25, 100);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(725, 156, 25, 100);

            //---- bt_PDeb ----
            bt_PDeb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_PDeb.setName("bt_PDeb");
            bt_PDeb.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_PDebActionPerformed(e);
              }
            });
            panel1.add(bt_PDeb);
            bt_PDeb.setBounds(725, 20, 25, 30);

            //---- bt_PFin ----
            bt_PFin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            bt_PFin.setName("bt_PFin");
            bt_PFin.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bt_PFinActionPerformed(e);
              }
            });
            panel1.add(bt_PFin);
            bt_PFin.setBounds(725, 259, 25, 30);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 774, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 312, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- ATTRIBUER ----
      ATTRIBUER.setText("Attribuer au WebShop");
      ATTRIBUER.setName("ATTRIBUER");
      ATTRIBUER.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          ATTRIBUERActionPerformed(e);
        }
      });
      BTD.add(ATTRIBUER);
    }

    //---- btnOuvrirURL1 ----
    btnOuvrirURL1.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL1.setName("btnOuvrirURL1");
    btnOuvrirURL1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL1ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL2 ----
    btnOuvrirURL2.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL2.setName("btnOuvrirURL2");
    btnOuvrirURL2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL2ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL3 ----
    btnOuvrirURL3.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL3.setName("btnOuvrirURL3");
    btnOuvrirURL3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL3ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL4 ----
    btnOuvrirURL4.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL4.setName("btnOuvrirURL4");
    btnOuvrirURL4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL4ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL5 ----
    btnOuvrirURL5.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL5.setName("btnOuvrirURL5");
    btnOuvrirURL5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL5ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL6 ----
    btnOuvrirURL6.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL6.setName("btnOuvrirURL6");
    btnOuvrirURL6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL6ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL7 ----
    btnOuvrirURL7.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL7.setName("btnOuvrirURL7");
    btnOuvrirURL7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL7ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL8 ----
    btnOuvrirURL8.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL8.setName("btnOuvrirURL8");
    btnOuvrirURL8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL8ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL9 ----
    btnOuvrirURL9.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL9.setName("btnOuvrirURL9");
    btnOuvrirURL9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL9ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL10 ----
    btnOuvrirURL10.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL10.setName("btnOuvrirURL10");
    btnOuvrirURL10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL10ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL11 ----
    btnOuvrirURL11.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL11.setName("btnOuvrirURL11");
    btnOuvrirURL11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL11ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL12 ----
    btnOuvrirURL12.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL12.setName("btnOuvrirURL12");
    btnOuvrirURL12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL12ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL13 ----
    btnOuvrirURL13.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL13.setName("btnOuvrirURL13");
    btnOuvrirURL13.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL13ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL14 ----
    btnOuvrirURL14.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL14.setName("btnOuvrirURL14");
    btnOuvrirURL14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL14ActionPerformed(e);
      }
    });

    //---- btnOuvrirURL15 ----
    btnOuvrirURL15.setToolTipText("Ouvrir l'URL");
    btnOuvrirURL15.setName("btnOuvrirURL15");
    btnOuvrirURL15.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        btnOuvrirURL15ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_67;
  private JLabel OBJ_46;
  private RiZoneSortie INDART;
  private RiZoneSortie WETB;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton bt_PDeb;
  private JButton bt_PFin;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem ATTRIBUER;
  private SNBoutonDetail btnOuvrirURL1;
  private SNBoutonDetail btnOuvrirURL2;
  private SNBoutonDetail btnOuvrirURL3;
  private SNBoutonDetail btnOuvrirURL4;
  private SNBoutonDetail btnOuvrirURL5;
  private SNBoutonDetail btnOuvrirURL6;
  private SNBoutonDetail btnOuvrirURL7;
  private SNBoutonDetail btnOuvrirURL8;
  private SNBoutonDetail btnOuvrirURL9;
  private SNBoutonDetail btnOuvrirURL10;
  private SNBoutonDetail btnOuvrirURL11;
  private SNBoutonDetail btnOuvrirURL12;
  private SNBoutonDetail btnOuvrirURL13;
  private SNBoutonDetail btnOuvrirURL14;
  private SNBoutonDetail btnOuvrirURL15;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
