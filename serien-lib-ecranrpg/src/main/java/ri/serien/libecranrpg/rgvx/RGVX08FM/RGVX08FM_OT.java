
package ri.serien.libecranrpg.rgvx.RGVX08FM;
// Nom Fichier: pop_RGVX08FM_FMTOT_FMTF1_1007.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class RGVX08FM_OT extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public RGVX08FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_RETOUR);
    
    
    BT_RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_6_OBJ_6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    OBJ_8_OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    OBJ_8_OBJ_8.setVisible(lexique.isPresent("TOT2"));
    OBJ_6_OBJ_6.setVisible(lexique.isPresent("TOT1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITRE@"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void BT_RETOURActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_5_OBJ_5 = new JLabel();
    OBJ_7_OBJ_7 = new JLabel();
    OBJ_6_OBJ_6 = new JLabel();
    OBJ_8_OBJ_8 = new JLabel();
    BT_RETOUR = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setPreferredSize(new Dimension(340, 150));
    setMinimumSize(new Dimension(340, 150));
    setMaximumSize(new Dimension(340, 150));
    setName("this");

    //---- OBJ_5_OBJ_5 ----
    OBJ_5_OBJ_5.setText("Nombre d'articles total");
    OBJ_5_OBJ_5.setForeground(Color.white);
    OBJ_5_OBJ_5.setFont(new Font("sansserif", Font.BOLD, 16));
    OBJ_5_OBJ_5.setName("OBJ_5_OBJ_5");

    //---- OBJ_7_OBJ_7 ----
    OBJ_7_OBJ_7.setText("dont d\u00e9sactiv\u00e9s");
    OBJ_7_OBJ_7.setForeground(Color.white);
    OBJ_7_OBJ_7.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_7_OBJ_7.setName("OBJ_7_OBJ_7");

    //---- OBJ_6_OBJ_6 ----
    OBJ_6_OBJ_6.setText("@TOT1@");
    OBJ_6_OBJ_6.setForeground(Color.white);
    OBJ_6_OBJ_6.setFont(new Font("sansserif", Font.BOLD, 16));
    OBJ_6_OBJ_6.setHorizontalAlignment(SwingConstants.RIGHT);
    OBJ_6_OBJ_6.setName("OBJ_6_OBJ_6");

    //---- OBJ_8_OBJ_8 ----
    OBJ_8_OBJ_8.setText("@TOT2@");
    OBJ_8_OBJ_8.setForeground(Color.white);
    OBJ_8_OBJ_8.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_8_OBJ_8.setHorizontalAlignment(SwingConstants.RIGHT);
    OBJ_8_OBJ_8.setName("OBJ_8_OBJ_8");

    //---- BT_RETOUR ----
    BT_RETOUR.setText(" Retour");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed();
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(16, 16, 16)
          .addComponent(OBJ_5_OBJ_5, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
          .addGap(0, 0, 0)
          .addComponent(OBJ_6_OBJ_6, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(16, 16, 16)
          .addComponent(OBJ_7_OBJ_7, GroupLayout.PREFERRED_SIZE, 154, GroupLayout.PREFERRED_SIZE)
          .addGap(40, 40, 40)
          .addComponent(OBJ_8_OBJ_8, GroupLayout.PREFERRED_SIZE, 106, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(80, 80, 80)
          .addComponent(BT_RETOUR, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_5_OBJ_5, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_6_OBJ_6, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
          .addGap(12, 12, 12)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_7_OBJ_7, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_8_OBJ_8, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
          .addGap(22, 22, 22)
          .addComponent(BT_RETOUR, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
    );
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_5_OBJ_5;
  private JLabel OBJ_7_OBJ_7;
  private JLabel OBJ_6_OBJ_6;
  private JLabel OBJ_8_OBJ_8;
  private JButton BT_RETOUR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
