/*
 * Created by JFormDesigner on Fri Apr 05 10:37:58 CEST 2013
 */

package ri.serien.libecranrpg.rgvx.RGVX08FM;

import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGVX08FM_A5 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public RGVX08FM_A5(ArrayList<?> param) {
    super(param);
    initComponents();
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC1@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC2@")).trim());
    label18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITG@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP05", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP06", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP07", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP08", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP09", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void button13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    button1 = new JButton();
    button2 = new JButton();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button7 = new JButton();
    button8 = new JButton();
    button9 = new JButton();
    button10 = new JButton();
    button11 = new JButton();
    button12 = new JButton();
    ARG2A = new XRiTextField();
    label13 = new JLabel();
    label14 = new JLabel();
    ARG3A = new XRiTextField();
    label15 = new JLabel();
    ARG4A = new XRiTextField();
    label16 = new JLabel();
    ARG5A = new XRiTextField();
    ARG33A = new XRiTextField();
    label17 = new JLabel();
    ARG6A = new XRiTextField();
    button13 = new JButton();
    label18 = new JLabel();
    button14 = new JButton();

    //======== this ========
    setName("this");
    setLayout(null);

    //---- button1 ----
    button1.setText("-->");
    button1.setFont(new Font("Courier New", Font.PLAIN, 12));
    button1.setName("button1");
    button1.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button1ActionPerformed(e);
      }
    });
    add(button1);
    button1.setBounds(15, 150, 68, 35);

    //---- button2 ----
    button2.setText("-->");
    button2.setFont(new Font("Courier New", Font.PLAIN, 12));
    button2.setName("button2");
    button2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button2ActionPerformed(e);
      }
    });
    add(button2);
    button2.setBounds(15, 190, 68, 35);

    //---- label1 ----
    label1.setText("@LD01@");
    label1.setName("label1");
    add(label1);
    label1.setBounds(95, 150, 795, 35);

    //---- label2 ----
    label2.setText("@LD02@");
    label2.setName("label2");
    add(label2);
    label2.setBounds(95, 190, 795, 35);

    //---- label3 ----
    label3.setText("@LD03@");
    label3.setName("label3");
    add(label3);
    label3.setBounds(95, 230, 795, 35);

    //---- label4 ----
    label4.setText("@LD04@");
    label4.setName("label4");
    add(label4);
    label4.setBounds(95, 270, 795, 35);

    //---- label5 ----
    label5.setText("@LD05@");
    label5.setName("label5");
    add(label5);
    label5.setBounds(95, 310, 795, 35);

    //---- label6 ----
    label6.setText("@LD06@");
    label6.setName("label6");
    add(label6);
    label6.setBounds(95, 350, 795, 35);

    //---- label7 ----
    label7.setText("@LD07@");
    label7.setName("label7");
    add(label7);
    label7.setBounds(95, 390, 795, 35);

    //---- label8 ----
    label8.setText("@LD08@");
    label8.setName("label8");
    add(label8);
    label8.setBounds(95, 430, 795, 35);

    //---- label9 ----
    label9.setText("@LD09@");
    label9.setName("label9");
    add(label9);
    label9.setBounds(95, 470, 795, 35);

    //---- label10 ----
    label10.setText("@LD10@");
    label10.setName("label10");
    add(label10);
    label10.setBounds(95, 510, 795, 35);

    //---- label11 ----
    label11.setText("@LD11@");
    label11.setName("label11");
    add(label11);
    label11.setBounds(95, 550, 795, 35);

    //---- label12 ----
    label12.setText("@LD12@");
    label12.setName("label12");
    add(label12);
    label12.setBounds(95, 590, 795, 35);

    //---- button3 ----
    button3.setText("-->");
    button3.setFont(new Font("Courier New", Font.PLAIN, 12));
    button3.setName("button3");
    button3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button3ActionPerformed(e);
      }
    });
    add(button3);
    button3.setBounds(15, 230, 68, 35);

    //---- button4 ----
    button4.setText("-->");
    button4.setFont(new Font("Courier New", Font.PLAIN, 12));
    button4.setName("button4");
    button4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button4ActionPerformed(e);
      }
    });
    add(button4);
    button4.setBounds(15, 270, 68, 35);

    //---- button5 ----
    button5.setText("-->");
    button5.setFont(new Font("Courier New", Font.PLAIN, 12));
    button5.setName("button5");
    button5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button5ActionPerformed(e);
      }
    });
    add(button5);
    button5.setBounds(15, 310, 68, 35);

    //---- button6 ----
    button6.setText("-->");
    button6.setFont(new Font("Courier New", Font.PLAIN, 12));
    button6.setName("button6");
    button6.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button6ActionPerformed(e);
      }
    });
    add(button6);
    button6.setBounds(15, 350, 68, 35);

    //---- button7 ----
    button7.setText("-->");
    button7.setFont(new Font("Courier New", Font.PLAIN, 12));
    button7.setName("button7");
    button7.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button7ActionPerformed(e);
      }
    });
    add(button7);
    button7.setBounds(15, 390, 68, 35);

    //---- button8 ----
    button8.setText("-->");
    button8.setFont(new Font("Courier New", Font.PLAIN, 12));
    button8.setName("button8");
    button8.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button8ActionPerformed(e);
      }
    });
    add(button8);
    button8.setBounds(15, 430, 68, 35);

    //---- button9 ----
    button9.setText("-->");
    button9.setFont(new Font("Courier New", Font.PLAIN, 12));
    button9.setName("button9");
    button9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button9ActionPerformed(e);
      }
    });
    add(button9);
    button9.setBounds(15, 470, 68, 35);

    //---- button10 ----
    button10.setText("-->");
    button10.setFont(new Font("Courier New", Font.PLAIN, 12));
    button10.setName("button10");
    button10.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button10ActionPerformed(e);
      }
    });
    add(button10);
    button10.setBounds(15, 510, 68, 35);

    //---- button11 ----
    button11.setText("-->");
    button11.setFont(new Font("Courier New", Font.PLAIN, 12));
    button11.setName("button11");
    button11.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button11ActionPerformed(e);
      }
    });
    add(button11);
    button11.setBounds(15, 550, 68, 35);

    //---- button12 ----
    button12.setText("-->");
    button12.setFont(new Font("Courier New", Font.PLAIN, 12));
    button12.setName("button12");
    button12.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button12ActionPerformed(e);
      }
    });
    add(button12);
    button12.setBounds(15, 590, 68, 35);

    //---- ARG2A ----
    ARG2A.setName("ARG2A");
    add(ARG2A);
    ARG2A.setBounds(120, 9, 210, ARG2A.getPreferredSize().height);

    //---- label13 ----
    label13.setText("Code Article");
    label13.setName("label13");
    add(label13);
    label13.setBounds(10, 13, 105, 20);

    //---- label14 ----
    label14.setText("@TMC1@");
    label14.setName("label14");
    add(label14);
    label14.setBounds(10, 49, 105, 20);

    //---- ARG3A ----
    ARG3A.setName("ARG3A");
    add(ARG3A);
    ARG3A.setBounds(120, 45, 210, 28);

    //---- label15 ----
    label15.setText("@TMC2@");
    label15.setName("label15");
    add(label15);
    label15.setBounds(10, 84, 105, 20);

    //---- ARG4A ----
    ARG4A.setName("ARG4A");
    add(ARG4A);
    ARG4A.setBounds(120, 80, 210, 28);

    //---- label16 ----
    label16.setText("Groupe, Famille ");
    label16.setName("label16");
    add(label16);
    label16.setBounds(475, 14, 105, 20);

    //---- ARG5A ----
    ARG5A.setName("ARG5A");
    add(ARG5A);
    ARG5A.setBounds(585, 10, 70, 28);

    //---- ARG33A ----
    ARG33A.setName("ARG33A");
    add(ARG33A);
    ARG33A.setBounds(665, 10, 35, 28);

    //---- label17 ----
    label17.setText("Sous-Famille ");
    label17.setName("label17");
    add(label17);
    label17.setBounds(475, 49, 105, 20);

    //---- ARG6A ----
    ARG6A.setName("ARG6A");
    add(ARG6A);
    ARG6A.setBounds(585, 45, 115, 28);

    //---- button13 ----
    button13.setText("Retour");
    button13.setName("button13");
    button13.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button13ActionPerformed(e);
      }
    });
    add(button13);
    button13.setBounds(825, 45, 100, 30);

    //---- label18 ----
    label18.setText("@TITG@");
    label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
    label18.setName("label18");
    add(label18);
    label18.setBounds(95, 130, 795, label18.getPreferredSize().height);

    //---- button14 ----
    button14.setText("Valider");
    button14.setName("button14");
    button14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        button14ActionPerformed(e);
      }
    });
    add(button14);
    button14.setBounds(825, 15, 100, 30);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JButton button1;
  private JButton button2;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  private JButton button8;
  private JButton button9;
  private JButton button10;
  private JButton button11;
  private JButton button12;
  private XRiTextField ARG2A;
  private JLabel label13;
  private JLabel label14;
  private XRiTextField ARG3A;
  private JLabel label15;
  private XRiTextField ARG4A;
  private JLabel label16;
  private XRiTextField ARG5A;
  private XRiTextField ARG33A;
  private JLabel label17;
  private XRiTextField ARG6A;
  private JButton button13;
  private JLabel label18;
  private JButton button14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
