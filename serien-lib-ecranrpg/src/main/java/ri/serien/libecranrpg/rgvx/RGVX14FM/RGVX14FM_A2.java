
package ri.serien.libecranrpg.rgvx.RGVX14FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGVX14FM_A2 extends SNPanelEcranRPG implements ioFrame {
  private static final String BOUTON_CREATION = "Créer";
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 541, };
  
  public RGVX14FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    bt_PFin.setIcon(lexique.chargerImage("images/pfin20.png", true));
    bt_PDeb.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    
    // Initialisation de la barre de boutons
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'c', false);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, true);
    snBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClickBouton(pSNBouton);
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Bordereaux de stocks @SOUTIT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Code établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    snEtablissement.setEnabled(false);
    
    
    
    if (lexique.isTrue("92")) {
      p_bpresentation.setText("Liste des bordereaux de stocks : inventaires saisis");
    }
    else if (lexique.isTrue("93")) {
      p_bpresentation.setText("Liste des bordereaux de stocks : avec affectation analytique");
    }
    else if (lexique.isTrue("95")) {
      p_bpresentation.setText("Liste des bordereaux de stocks : dépréciation de stock");
    }
    else if (lexique.isTrue("97")) {
      p_bpresentation.setText("Liste des bordereaux de stocks : provision pour hausse de prix");
    }
    else if (lexique.isTrue("98")) {
      p_bpresentation.setText("Liste des bordereaux de stocks : saisie de prix");
    }
    else {
      p_bpresentation.setText("Liste des bordereaux de stocks");
    }
    
    // Visibilité
    traiterVisibiliteBouton();
    
    // Logo
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  /**
   * Traiter la visibilité des boutons
   */
  private void traiterVisibiliteBouton() {
    // Validation
    boolean actif = true;
    // Le bouton est inactif si un numéro pour une accès direct n'a été saisi et si aucune ligne n'a été sélectionnée
    if (INDNUM.getText().trim().isEmpty() && WTP01.getSelectionModel().isSelectionEmpty()) {
      actif = false;
    }
    // Le bouton est inactif si une ligne a été sélectionnée mais que celle-ci ne contient aucune donnée
    else if (WTP01.isLigneSelectioneeVide()) {
      actif = false;
    }
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, actif);
    
    // Création
    actif = true;
    if (lexique.getMode() == Lexical.MODE_CREATION) {
      actif = false;
    }
    snBarreBouton.activerBouton(BOUTON_CREATION, actif);
  }
  
  /**
   * Traitement des actions des boutons
   */
  private void btTraiterClickBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        lexique.HostScreenSendKey(this, "F13");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    traiterVisibiliteBouton();
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_PDebActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bt_PFinActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void INDNUMKeyReleased(KeyEvent e) {
    try {
      traiterVisibiliteBouton();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlBandeau = new SNPanel();
    p_bpresentation = new SNBandeauTitre();
    snBarreBouton = new SNBarreBouton();
    pnlPrincipal = new SNPanelContenu();
    pnlInformations = new SNPanel();
    pnlGauche = new SNPanel();
    lbNumero = new SNLabelChamp();
    INDNUM = new XRiTextField();
    lbDateTraitement = new SNLabelChamp();
    WDAT1X = new XRiCalendrier();
    pnlDroite = new SNPanel();
    lbE1ETB = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlListeResultats = new SNPanelTitre();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlDefilement = new SNPanel();
    bt_PDeb = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    bt_PFin = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    TRIAGE = new JMenuItem();
    OBJ_26 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== pnlBandeau ========
    {
      pnlBandeau.setName("pnlBandeau");
      pnlBandeau.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Bordereaux de stocks @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      pnlBandeau.add(p_bpresentation);
    }
    add(pnlBandeau, BorderLayout.NORTH);

    //---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);

    //======== pnlPrincipal ========
    {
      pnlPrincipal.setName("pnlPrincipal");
      pnlPrincipal.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlPrincipal.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlPrincipal.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

      //======== pnlInformations ========
      {
        pnlInformations.setName("pnlInformations");
        pnlInformations.setLayout(new GridLayout(1, 2, 5, 0));

        //======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlGauche.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).rowHeights = new int[] {0, 0, 0};
          ((GridBagLayout)pnlGauche.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
          ((GridBagLayout)pnlGauche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

          //---- lbNumero ----
          lbNumero.setText("Acc\u00e8s direct par num\u00e9ro");
          lbNumero.setMaximumSize(new Dimension(200, 30));
          lbNumero.setMinimumSize(new Dimension(200, 30));
          lbNumero.setPreferredSize(new Dimension(200, 30));
          lbNumero.setInheritsPopupMenu(false);
          lbNumero.setName("lbNumero");
          pnlGauche.add(lbNumero, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setMinimumSize(new Dimension(70, 30));
          INDNUM.setMaximumSize(new Dimension(70, 30));
          INDNUM.setPreferredSize(new Dimension(70, 30));
          INDNUM.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDNUM.setName("INDNUM");
          pnlGauche.add(INDNUM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbDateTraitement ----
          lbDateTraitement.setText("Date de traitement");
          lbDateTraitement.setMaximumSize(new Dimension(200, 30));
          lbDateTraitement.setMinimumSize(new Dimension(200, 30));
          lbDateTraitement.setPreferredSize(new Dimension(200, 30));
          lbDateTraitement.setInheritsPopupMenu(false);
          lbDateTraitement.setName("lbDateTraitement");
          pnlGauche.add(lbDateTraitement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setMaximumSize(new Dimension(110, 30));
          WDAT1X.setMinimumSize(new Dimension(110, 30));
          WDAT1X.setPreferredSize(new Dimension(110, 30));
          WDAT1X.setFont(new Font("sansserif", Font.PLAIN, 14));
          WDAT1X.setName("WDAT1X");
          pnlGauche.add(WDAT1X, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlGauche);

        //======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDroite.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlDroite.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlDroite.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbE1ETB ----
          lbE1ETB.setText("Etablissement");
          lbE1ETB.setMaximumSize(new Dimension(200, 30));
          lbE1ETB.setMinimumSize(new Dimension(200, 30));
          lbE1ETB.setPreferredSize(new Dimension(200, 30));
          lbE1ETB.setInheritsPopupMenu(false);
          lbE1ETB.setName("lbE1ETB");
          pnlDroite.add(lbE1ETB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setName("snEtablissement");
          pnlDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlInformations.add(pnlDroite);
      }
      pnlPrincipal.add(pnlInformations, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlListeResultats ========
      {
        pnlListeResultats.setTitre("R\u00e9sultat de la recherche");
        pnlListeResultats.setName("pnlListeResultats");
        pnlListeResultats.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlListeResultats.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlListeResultats.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)pnlListeResultats.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlListeResultats.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setMinimumSize(new Dimension(350, 275));
          SCROLLPANE_LIST.setPreferredSize(new Dimension(350, 275));
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlListeResultats.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 5), 0, 0));

        //======== pnlDefilement ========
        {
          pnlDefilement.setMaximumSize(new Dimension(28, 2147483647));
          pnlDefilement.setName("pnlDefilement");
          pnlDefilement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlDefilement.getLayout()).columnWidths = new int[] {0, 0};
          ((GridBagLayout)pnlDefilement.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
          ((GridBagLayout)pnlDefilement.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
          ((GridBagLayout)pnlDefilement.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0, 0.0, 1.0E-4};

          //---- bt_PDeb ----
          bt_PDeb.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_PDeb.setMaximumSize(new Dimension(28, 30));
          bt_PDeb.setMinimumSize(new Dimension(28, 30));
          bt_PDeb.setPreferredSize(new Dimension(28, 30));
          bt_PDeb.setName("bt_PDeb");
          bt_PDeb.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_PDebActionPerformed(e);
            }
          });
          pnlDefilement.add(bt_PDeb, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setMaximumSize(new Dimension(28, 30));
          BT_PGUP.setMinimumSize(new Dimension(28, 30));
          BT_PGUP.setPreferredSize(new Dimension(28, 30));
          BT_PGUP.setName("BT_PGUP");
          pnlDefilement.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setPreferredSize(new Dimension(28, 30));
          BT_PGDOWN.setMinimumSize(new Dimension(28, 30));
          BT_PGDOWN.setMaximumSize(new Dimension(28, 30));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlDefilement.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- bt_PFin ----
          bt_PFin.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          bt_PFin.setMaximumSize(new Dimension(28, 30));
          bt_PFin.setMinimumSize(new Dimension(28, 30));
          bt_PFin.setPreferredSize(new Dimension(28, 30));
          bt_PFin.setName("bt_PFin");
          bt_PFin.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bt_PFinActionPerformed(e);
            }
          });
          pnlDefilement.add(bt_PFin, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlListeResultats.add(pnlDefilement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlPrincipal.add(pnlListeResultats, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlPrincipal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Rappel");
      CHOISIR.setFont(new Font("sansserif", Font.PLAIN, 14));
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Dupliquer");
      OBJ_18.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Interroger");
      OBJ_19.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- TRIAGE ----
      TRIAGE.setText("Tri\u00e9 par ...");
      TRIAGE.setFont(new Font("sansserif", Font.PLAIN, 14));
      TRIAGE.setName("TRIAGE");
      BTD.add(TRIAGE);

      //---- OBJ_26 ----
      OBJ_26.setText("D\u00e9verrouillage");
      OBJ_26.setFont(new Font("sansserif", Font.PLAIN, 14));
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanel pnlBandeau;
  private SNBandeauTitre p_bpresentation;
  private SNBarreBouton snBarreBouton;
  private SNPanelContenu pnlPrincipal;
  private SNPanel pnlInformations;
  private SNPanel pnlGauche;
  private SNLabelChamp lbNumero;
  private XRiTextField INDNUM;
  private SNLabelChamp lbDateTraitement;
  private XRiCalendrier WDAT1X;
  private SNPanel pnlDroite;
  private SNLabelChamp lbE1ETB;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlListeResultats;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel pnlDefilement;
  private JButton bt_PDeb;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton bt_PFin;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem TRIAGE;
  private JMenuItem OBJ_26;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
