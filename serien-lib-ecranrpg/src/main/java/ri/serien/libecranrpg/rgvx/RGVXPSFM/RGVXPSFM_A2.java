
package ri.serien.libecranrpg.rgvx.RGVXPSFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGVXPSFM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] WMDC_Value = { "", "CLIENTS", "PROSPECTS", "ARTICLES", "NOMENCLATURES", "FOURNISSEURS", "CONDITIONS D'ACHATS",
      "BONS DE VENTE (ENTÊTES)", "BONS DE VENTE (LIGNES)", "CONDITIONS DE VENTE", "BONS D'ACHATS", "STOCKS", "PREVISIONS CONSOMMATIONS",
      "REGLEMENTS", "SERVICE APRES VENTE", "EDITEUR", "TRAITEMENT GLOBAL DES COMMANDE", "FACTURATION DIFFEREE", "ARRETE MENSUEL GVM/GAM",
      "JOURNAL FISCAL DES VENTES", "COMPTABILISATION", "STATISTIQUES", "COMMISSIONNEMENT REPRESENTANT", "EDITIONS BONS VENTE",
      "EDITIONS FACTURES", "EDITION DES TRAITES", "EDITIONS BONS D'ACHAT" };
  
  public RGVXPSFM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WMDC.setValeurs(WMDC_Value, null);
    WMDC.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        lexique.HostScreenSendKey(lexique.getPanel(), "ENTER");
      }
    });
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator1.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD01B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator2.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD02B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator3.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD03B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator4.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD04B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator5.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD05B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator6.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD06B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator7.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD07B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator8.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD08B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator9.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD09B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator10.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD10B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator11.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD11B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator12.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD12B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator13.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD13B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator14.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD14B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator15.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD15B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator16.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD16B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator17.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD17B@")).trim());
    DefaultComponentFactory.setTextAndMnemonic((JLabel)separator18.getComponent(0), lexique.TranslationTable(interpreteurD.analyseExpression("@LD18B@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    panel2.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LDTIT@")).trim()));
    OBJ_102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03B@")).trim());
    OBJ_102.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03C@")).trim());
    OBJ_101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02B@")).trim());
    OBJ_101.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02C@")).trim());
    OBJ_100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01B@")).trim());
    OBJ_100.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01C@")).trim());
    OBJ_103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04B@")).trim());
    OBJ_103.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04C@")).trim());
    OBJ_104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05B@")).trim());
    OBJ_104.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05C@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06B@")).trim());
    OBJ_105.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06C@")).trim());
    OBJ_106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07B@")).trim());
    OBJ_106.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07C@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08B@")).trim());
    OBJ_107.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08C@")).trim());
    OBJ_108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09B@")).trim());
    OBJ_108.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09C@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10B@")).trim());
    OBJ_109.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10C@")).trim());
    OBJ_110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11B@")).trim());
    OBJ_110.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11C@")).trim());
    OBJ_111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12B@")).trim());
    OBJ_111.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12C@")).trim());
    OBJ_112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13B@")).trim());
    OBJ_112.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13C@")).trim());
    OBJ_113.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14B@")).trim());
    OBJ_113.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14C@")).trim());
    OBJ_114.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15B@")).trim());
    OBJ_114.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15C@")).trim());
    OBJ_115.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD16B@")).trim());
    OBJ_115.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD16C@")).trim());
    OBJ_116.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD17B@")).trim());
    OBJ_116.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD17C@")).trim());
    OBJ_117.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD18B@")).trim());
    OBJ_117.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD18C@")).trim());
    OBJ_216.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD17A@")).trim());
    OBJ_201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD02A@")).trim());
    OBJ_202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD03A@")).trim());
    OBJ_203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD04A@")).trim());
    OBJ_204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD05A@")).trim());
    OBJ_205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD06A@")).trim());
    OBJ_206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD07A@")).trim());
    OBJ_207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD08A@")).trim());
    OBJ_208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD09A@")).trim());
    OBJ_209.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD10A@")).trim());
    OBJ_210.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD11A@")).trim());
    OBJ_211.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD12A@")).trim());
    OBJ_212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD13A@")).trim());
    OBJ_213.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD14A@")).trim());
    OBJ_214.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD15A@")).trim());
    OBJ_215.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD16A@")).trim());
    OBJ_217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD18A@")).trim());
    OBJ_200.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LD01A@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    /*
    String choix = lexique.HostFieldGetData("WMDC").trim();
    for (int i = 0; i < WMDC_Value.length; i++)
    {
      if (choix == WMDC_Value[i])
        WMDC.setSelectedIndex(i);
    }
    */
    
    WTP18.setVisible(!lexique.HostFieldGetData("LD18B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP18"));
    WTP01.setVisible(!lexique.HostFieldGetData("LD01B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP01"));
    WTP17.setVisible(!lexique.HostFieldGetData("LD17B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP17"));
    WTP16.setVisible(!lexique.HostFieldGetData("LD16B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP16"));
    WTP15.setVisible(!lexique.HostFieldGetData("LD15B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP15"));
    WTP14.setVisible(!lexique.HostFieldGetData("LD14B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP14"));
    WTP13.setVisible(!lexique.HostFieldGetData("LD13B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP13"));
    WTP12.setVisible(!lexique.HostFieldGetData("LD12B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP12"));
    WTP11.setVisible(!lexique.HostFieldGetData("LD11B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP11"));
    WTP10.setVisible(!lexique.HostFieldGetData("LD10B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP10"));
    WTP09.setVisible(!lexique.HostFieldGetData("LD09B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP09"));
    WTP08.setVisible(!lexique.HostFieldGetData("LD08B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP08"));
    WTP07.setVisible(!lexique.HostFieldGetData("LD07B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP07"));
    WTP06.setVisible(!lexique.HostFieldGetData("LD06B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP06"));
    WTP05.setVisible(!lexique.HostFieldGetData("LD05B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP05"));
    WTP04.setVisible(!lexique.HostFieldGetData("LD04B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP04"));
    WTP03.setVisible(!lexique.HostFieldGetData("LD03B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP03"));
    WTP02.setVisible(!lexique.HostFieldGetData("LD02B").trim().equalsIgnoreCase("") & lexique.isPresent("WTP02"));
    INDTYP.setEnabled(lexique.isPresent("LD18B"));
    
    // WNPS.setVisible(lexique.isPresent("WNPS"));
    INDETB.setEnabled(lexique.isPresent("LD18B"));
    // WTEXT.setVisible(lexique.isPresent("WTEXT"));
    
    // on met les zones E/S en table pour pouvoir les manipuler
    XRiTextField[] tabTf =
        { WTP01, WTP02, WTP03, WTP04, WTP05, WTP06, WTP07, WTP08, WTP09, WTP10, WTP11, WTP12, WTP13, WTP14, WTP15, WTP16, WTP17, WTP18 };
    JComponent[] tabSe = { separator1, separator2, separator3, separator4, separator5, separator6, separator7, separator8, separator9,
        separator10, separator11, separator12, separator13, separator14, separator15, separator16, separator17, separator18 };
    JComponent[] tabZs = { OBJ_100, OBJ_101, OBJ_102, OBJ_103, OBJ_104, OBJ_105, OBJ_106, OBJ_107, OBJ_108, OBJ_109, OBJ_110, OBJ_111,
        OBJ_112, OBJ_113, OBJ_114, OBJ_115, OBJ_116, OBJ_117 };
    JComponent[] tabBd = { OBJ_200, OBJ_201, OBJ_202, OBJ_203, OBJ_204, OBJ_205, OBJ_206, OBJ_207, OBJ_208, OBJ_209, OBJ_210, OBJ_211,
        OBJ_212, OBJ_213, OBJ_214, OBJ_215, OBJ_216, OBJ_217 };
    
    for (int i = 0; i < 18; i++) {
      tabTf[i].setEditable(lexique.getMode() != Lexical.MODE_CONSULTATION);
    }
    
    // Si c'est un titre on met un séparator à la place des zones de sortie
    for (int i = 0; i < 18; i++) {
      tabSe[i].setVisible(lexique.isTrue(String.valueOf(i + 21))
          && !lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)) + "B").trim().equalsIgnoreCase(""));
      tabZs[i].setVisible(!lexique.isTrue(String.valueOf(i + 21)));
      tabBd[i].setVisible(!lexique.isTrue(String.valueOf(i + 21)));
    }
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("WMDC", 0, WMDC_Value[WMDC.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    if (lexique.isTrue("53")) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    else {
      lexique.HostScreenSendKey(this, "F3");
    }
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(23, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(8, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(9, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(10, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(13, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_75ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(14, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_78ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(15, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_81ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(16, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_84ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(17, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_87ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_90ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(19, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_93ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_96ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(21, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_99ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(22, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_124ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(24, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(7, 80);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_33 = new JLabel();
    INDETB = new XRiTextField();
    INDTYP = new XRiTextField();
    OBJ_41 = new JLabel();
    WNPS = new XRiTextField();
    OBJ_44 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WTEXT = new XRiTextField();
    OBJ_126 = new JLabel();
    WMDC = new XRiComboBox();
    panel2 = new JPanel();
    OBJ_102 = new RiZoneSortie();
    OBJ_101 = new RiZoneSortie();
    OBJ_100 = new RiZoneSortie();
    OBJ_103 = new RiZoneSortie();
    OBJ_104 = new RiZoneSortie();
    OBJ_105 = new RiZoneSortie();
    OBJ_106 = new RiZoneSortie();
    OBJ_107 = new RiZoneSortie();
    OBJ_108 = new RiZoneSortie();
    OBJ_109 = new RiZoneSortie();
    OBJ_110 = new RiZoneSortie();
    OBJ_111 = new RiZoneSortie();
    OBJ_112 = new RiZoneSortie();
    OBJ_113 = new RiZoneSortie();
    OBJ_114 = new RiZoneSortie();
    OBJ_115 = new RiZoneSortie();
    OBJ_116 = new RiZoneSortie();
    OBJ_117 = new RiZoneSortie();
    OBJ_216 = new SNBoutonDetail();
    OBJ_201 = new SNBoutonDetail();
    OBJ_202 = new SNBoutonDetail();
    OBJ_203 = new SNBoutonDetail();
    OBJ_204 = new SNBoutonDetail();
    OBJ_205 = new SNBoutonDetail();
    OBJ_206 = new SNBoutonDetail();
    OBJ_207 = new SNBoutonDetail();
    OBJ_208 = new SNBoutonDetail();
    OBJ_209 = new SNBoutonDetail();
    OBJ_210 = new SNBoutonDetail();
    OBJ_211 = new SNBoutonDetail();
    OBJ_212 = new SNBoutonDetail();
    OBJ_213 = new SNBoutonDetail();
    OBJ_214 = new SNBoutonDetail();
    OBJ_215 = new SNBoutonDetail();
    OBJ_217 = new SNBoutonDetail();
    WTP02 = new XRiTextField();
    WTP03 = new XRiTextField();
    WTP04 = new XRiTextField();
    WTP05 = new XRiTextField();
    WTP06 = new XRiTextField();
    WTP07 = new XRiTextField();
    WTP08 = new XRiTextField();
    WTP09 = new XRiTextField();
    WTP10 = new XRiTextField();
    WTP11 = new XRiTextField();
    WTP12 = new XRiTextField();
    WTP13 = new XRiTextField();
    WTP14 = new XRiTextField();
    WTP15 = new XRiTextField();
    WTP16 = new XRiTextField();
    WTP17 = new XRiTextField();
    WTP01 = new XRiTextField();
    WTP18 = new XRiTextField();
    OBJ_200 = new SNBoutonDetail();
    separator1 = compFactory.createSeparator("@LD01B@");
    separator2 = compFactory.createSeparator("@LD02B@");
    separator3 = compFactory.createSeparator("@LD03B@");
    separator4 = compFactory.createSeparator("@LD04B@");
    separator5 = compFactory.createSeparator("@LD05B@");
    separator6 = compFactory.createSeparator("@LD06B@");
    separator7 = compFactory.createSeparator("@LD07B@");
    separator8 = compFactory.createSeparator("@LD08B@");
    separator9 = compFactory.createSeparator("@LD09B@");
    separator10 = compFactory.createSeparator("@LD10B@");
    separator11 = compFactory.createSeparator("@LD11B@");
    separator12 = compFactory.createSeparator("@LD12B@");
    separator13 = compFactory.createSeparator("@LD13B@");
    separator14 = compFactory.createSeparator("@LD14B@");
    separator15 = compFactory.createSeparator("@LD15B@");
    separator16 = compFactory.createSeparator("@LD16B@");
    separator17 = compFactory.createSeparator("@LD17B@");
    separator18 = compFactory.createSeparator("@LD18B@");
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalisation de la gestion des ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_33 ----
          OBJ_33.setText("Etablissement");
          OBJ_33.setName("OBJ_33");
          p_tete_gauche.add(OBJ_33);
          OBJ_33.setBounds(5, 5, 93, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);

          //---- INDTYP ----
          INDTYP.setComponentPopupMenu(BTD);
          INDTYP.setName("INDTYP");
          p_tete_gauche.add(INDTYP);
          INDTYP.setBounds(210, 0, 40, INDTYP.getPreferredSize().height);

          //---- OBJ_41 ----
          OBJ_41.setText("Num\u00e9ro PS");
          OBJ_41.setName("OBJ_41");
          p_tete_gauche.add(OBJ_41);
          OBJ_41.setBounds(270, 4, 74, 20);

          //---- WNPS ----
          WNPS.setComponentPopupMenu(BTD);
          WNPS.setName("WNPS");
          p_tete_gauche.add(WNPS);
          WNPS.setBounds(350, 0, 40, WNPS.getPreferredSize().height);

          //---- OBJ_44 ----
          OBJ_44.setText("Code");
          OBJ_44.setName("OBJ_44");
          p_tete_gauche.add(OBJ_44);
          OBJ_44.setBounds(165, 5, 36, 18);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTEXT ----
            WTEXT.setComponentPopupMenu(BTD);
            WTEXT.setName("WTEXT");
            panel1.add(WTEXT);
            WTEXT.setBounds(450, 13, 310, WTEXT.getPreferredSize().height);

            //---- OBJ_126 ----
            OBJ_126.setText("ou texte");
            OBJ_126.setName("OBJ_126");
            panel1.add(OBJ_126);
            OBJ_126.setBounds(390, 17, 55, 20);

            //---- WMDC ----
            WMDC.setModel(new DefaultComboBoxModel(new String[] {
              "Tout afficher ",
              "Clients",
              "Prospects",
              "Articles",
              "Nomenclatures",
              "Fournisseurs",
              "Conditions d'achats",
              "Bons de vente (ent\u00eates)",
              "Bons de vente (lignes)",
              "Conditions de vente",
              "Bons d'achats",
              "Stocks",
              "Pr\u00e9visions de consomations",
              "R\u00e9glements",
              "Service apr\u00e8s vente",
              "Editeurs",
              "Traitement global des commandes",
              "Facturation diff\u00e9r\u00e9e",
              "Arr\u00eat\u00e9 mensuel de gestion des ventes",
              "Journal fiscal des ventes",
              "Comptabilisation",
              "Statistiques",
              "Commissionnement repr\u00e9sentants",
              "Edition des bons de vente",
              "Edition des factures",
              "Edition des traites",
              "Edition des bons d'achat"
            }));
            WMDC.setName("WMDC");
            panel1.add(WMDC);
            WMDC.setBounds(35, 14, 335, WMDC.getPreferredSize().height);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("@LDTIT@"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_102 ----
            OBJ_102.setText("@LD03B@");
            OBJ_102.setToolTipText("@LD03C@");
            OBJ_102.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_102.setName("OBJ_102");
            panel2.add(OBJ_102);
            OBJ_102.setBounds(140, 82, 540, OBJ_102.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("@LD02B@");
            OBJ_101.setToolTipText("@LD02C@");
            OBJ_101.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_101.setName("OBJ_101");
            panel2.add(OBJ_101);
            OBJ_101.setBounds(140, 57, 540, OBJ_101.getPreferredSize().height);

            //---- OBJ_100 ----
            OBJ_100.setText("@LD01B@");
            OBJ_100.setToolTipText("@LD01C@");
            OBJ_100.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_100.setName("OBJ_100");
            panel2.add(OBJ_100);
            OBJ_100.setBounds(140, 32, 540, OBJ_100.getPreferredSize().height);

            //---- OBJ_103 ----
            OBJ_103.setText("@LD04B@");
            OBJ_103.setToolTipText("@LD04C@");
            OBJ_103.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_103.setName("OBJ_103");
            panel2.add(OBJ_103);
            OBJ_103.setBounds(140, 107, 540, OBJ_103.getPreferredSize().height);

            //---- OBJ_104 ----
            OBJ_104.setText("@LD05B@");
            OBJ_104.setToolTipText("@LD05C@");
            OBJ_104.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_104.setName("OBJ_104");
            panel2.add(OBJ_104);
            OBJ_104.setBounds(140, 132, 540, OBJ_104.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("@LD06B@");
            OBJ_105.setToolTipText("@LD06C@");
            OBJ_105.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_105.setName("OBJ_105");
            panel2.add(OBJ_105);
            OBJ_105.setBounds(140, 157, 540, OBJ_105.getPreferredSize().height);

            //---- OBJ_106 ----
            OBJ_106.setText("@LD07B@");
            OBJ_106.setToolTipText("@LD07C@");
            OBJ_106.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_106.setName("OBJ_106");
            panel2.add(OBJ_106);
            OBJ_106.setBounds(140, 182, 540, OBJ_106.getPreferredSize().height);

            //---- OBJ_107 ----
            OBJ_107.setText("@LD08B@");
            OBJ_107.setToolTipText("@LD08C@");
            OBJ_107.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_107.setName("OBJ_107");
            panel2.add(OBJ_107);
            OBJ_107.setBounds(140, 207, 540, OBJ_107.getPreferredSize().height);

            //---- OBJ_108 ----
            OBJ_108.setText("@LD09B@");
            OBJ_108.setToolTipText("@LD09C@");
            OBJ_108.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_108.setName("OBJ_108");
            panel2.add(OBJ_108);
            OBJ_108.setBounds(140, 232, 540, OBJ_108.getPreferredSize().height);

            //---- OBJ_109 ----
            OBJ_109.setText("@LD10B@");
            OBJ_109.setToolTipText("@LD10C@");
            OBJ_109.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_109.setName("OBJ_109");
            panel2.add(OBJ_109);
            OBJ_109.setBounds(140, 257, 540, OBJ_109.getPreferredSize().height);

            //---- OBJ_110 ----
            OBJ_110.setText("@LD11B@");
            OBJ_110.setToolTipText("@LD11C@");
            OBJ_110.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_110.setName("OBJ_110");
            panel2.add(OBJ_110);
            OBJ_110.setBounds(140, 282, 540, OBJ_110.getPreferredSize().height);

            //---- OBJ_111 ----
            OBJ_111.setText("@LD12B@");
            OBJ_111.setToolTipText("@LD12C@");
            OBJ_111.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_111.setName("OBJ_111");
            panel2.add(OBJ_111);
            OBJ_111.setBounds(140, 307, 540, OBJ_111.getPreferredSize().height);

            //---- OBJ_112 ----
            OBJ_112.setText("@LD13B@");
            OBJ_112.setToolTipText("@LD13C@");
            OBJ_112.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_112.setName("OBJ_112");
            panel2.add(OBJ_112);
            OBJ_112.setBounds(140, 332, 540, OBJ_112.getPreferredSize().height);

            //---- OBJ_113 ----
            OBJ_113.setText("@LD14B@");
            OBJ_113.setToolTipText("@LD14C@");
            OBJ_113.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_113.setName("OBJ_113");
            panel2.add(OBJ_113);
            OBJ_113.setBounds(140, 357, 540, OBJ_113.getPreferredSize().height);

            //---- OBJ_114 ----
            OBJ_114.setText("@LD15B@");
            OBJ_114.setToolTipText("@LD15C@");
            OBJ_114.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_114.setName("OBJ_114");
            panel2.add(OBJ_114);
            OBJ_114.setBounds(140, 382, 540, OBJ_114.getPreferredSize().height);

            //---- OBJ_115 ----
            OBJ_115.setText("@LD16B@");
            OBJ_115.setToolTipText("@LD16C@");
            OBJ_115.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_115.setName("OBJ_115");
            panel2.add(OBJ_115);
            OBJ_115.setBounds(140, 407, 540, OBJ_115.getPreferredSize().height);

            //---- OBJ_116 ----
            OBJ_116.setText("@LD17B@");
            OBJ_116.setToolTipText("@LD17C@");
            OBJ_116.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_116.setName("OBJ_116");
            panel2.add(OBJ_116);
            OBJ_116.setBounds(140, 432, 540, OBJ_116.getPreferredSize().height);

            //---- OBJ_117 ----
            OBJ_117.setText("@LD18B@");
            OBJ_117.setToolTipText("@LD18C@");
            OBJ_117.setFont(new Font("Courier New", Font.PLAIN, 12));
            OBJ_117.setName("OBJ_117");
            panel2.add(OBJ_117);
            OBJ_117.setBounds(140, 457, 540, OBJ_117.getPreferredSize().height);

            //---- OBJ_216 ----
            OBJ_216.setText("@LD17A@");
            OBJ_216.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_216.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_216.setFont(OBJ_216.getFont().deriveFont(OBJ_216.getFont().getStyle() | Font.BOLD));
            OBJ_216.setName("OBJ_216");
            OBJ_216.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_25ActionPerformed(e);
              }
            });
            panel2.add(OBJ_216);
            OBJ_216.setBounds(60, 432, 75, 25);

            //---- OBJ_201 ----
            OBJ_201.setText("@LD02A@");
            OBJ_201.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_201.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_201.setFont(OBJ_201.getFont().deriveFont(OBJ_201.getFont().getStyle() | Font.BOLD));
            OBJ_201.setName("OBJ_201");
            OBJ_201.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_58ActionPerformed(e);
              }
            });
            panel2.add(OBJ_201);
            OBJ_201.setBounds(60, 57, 75, 25);

            //---- OBJ_202 ----
            OBJ_202.setText("@LD03A@");
            OBJ_202.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_202.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_202.setFont(OBJ_202.getFont().deriveFont(OBJ_202.getFont().getStyle() | Font.BOLD));
            OBJ_202.setName("OBJ_202");
            OBJ_202.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_60ActionPerformed(e);
              }
            });
            panel2.add(OBJ_202);
            OBJ_202.setBounds(60, 82, 75, 25);

            //---- OBJ_203 ----
            OBJ_203.setText("@LD04A@");
            OBJ_203.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_203.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_203.setFont(OBJ_203.getFont().deriveFont(OBJ_203.getFont().getStyle() | Font.BOLD));
            OBJ_203.setName("OBJ_203");
            OBJ_203.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_63ActionPerformed(e);
              }
            });
            panel2.add(OBJ_203);
            OBJ_203.setBounds(60, 107, 75, 25);

            //---- OBJ_204 ----
            OBJ_204.setText("@LD05A@");
            OBJ_204.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_204.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_204.setFont(OBJ_204.getFont().deriveFont(OBJ_204.getFont().getStyle() | Font.BOLD));
            OBJ_204.setName("OBJ_204");
            OBJ_204.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_66ActionPerformed(e);
              }
            });
            panel2.add(OBJ_204);
            OBJ_204.setBounds(60, 132, 75, 25);

            //---- OBJ_205 ----
            OBJ_205.setText("@LD06A@");
            OBJ_205.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_205.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_205.setFont(OBJ_205.getFont().deriveFont(OBJ_205.getFont().getStyle() | Font.BOLD));
            OBJ_205.setName("OBJ_205");
            OBJ_205.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_69ActionPerformed(e);
              }
            });
            panel2.add(OBJ_205);
            OBJ_205.setBounds(60, 157, 75, 25);

            //---- OBJ_206 ----
            OBJ_206.setText("@LD07A@");
            OBJ_206.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_206.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_206.setFont(OBJ_206.getFont().deriveFont(OBJ_206.getFont().getStyle() | Font.BOLD));
            OBJ_206.setName("OBJ_206");
            OBJ_206.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_72ActionPerformed(e);
              }
            });
            panel2.add(OBJ_206);
            OBJ_206.setBounds(60, 182, 75, 25);

            //---- OBJ_207 ----
            OBJ_207.setText("@LD08A@");
            OBJ_207.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_207.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_207.setFont(OBJ_207.getFont().deriveFont(OBJ_207.getFont().getStyle() | Font.BOLD));
            OBJ_207.setName("OBJ_207");
            OBJ_207.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_75ActionPerformed(e);
              }
            });
            panel2.add(OBJ_207);
            OBJ_207.setBounds(60, 207, 75, 25);

            //---- OBJ_208 ----
            OBJ_208.setText("@LD09A@");
            OBJ_208.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_208.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_208.setFont(OBJ_208.getFont().deriveFont(OBJ_208.getFont().getStyle() | Font.BOLD));
            OBJ_208.setName("OBJ_208");
            OBJ_208.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_78ActionPerformed(e);
              }
            });
            panel2.add(OBJ_208);
            OBJ_208.setBounds(60, 232, 75, 25);

            //---- OBJ_209 ----
            OBJ_209.setText("@LD10A@");
            OBJ_209.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_209.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_209.setFont(OBJ_209.getFont().deriveFont(OBJ_209.getFont().getStyle() | Font.BOLD));
            OBJ_209.setName("OBJ_209");
            OBJ_209.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_81ActionPerformed(e);
              }
            });
            panel2.add(OBJ_209);
            OBJ_209.setBounds(60, 257, 75, 25);

            //---- OBJ_210 ----
            OBJ_210.setText("@LD11A@");
            OBJ_210.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_210.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_210.setFont(OBJ_210.getFont().deriveFont(OBJ_210.getFont().getStyle() | Font.BOLD));
            OBJ_210.setName("OBJ_210");
            OBJ_210.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_84ActionPerformed(e);
              }
            });
            panel2.add(OBJ_210);
            OBJ_210.setBounds(60, 282, 75, 25);

            //---- OBJ_211 ----
            OBJ_211.setText("@LD12A@");
            OBJ_211.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_211.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_211.setFont(OBJ_211.getFont().deriveFont(OBJ_211.getFont().getStyle() | Font.BOLD));
            OBJ_211.setName("OBJ_211");
            OBJ_211.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_87ActionPerformed(e);
              }
            });
            panel2.add(OBJ_211);
            OBJ_211.setBounds(60, 307, 75, 25);

            //---- OBJ_212 ----
            OBJ_212.setText("@LD13A@");
            OBJ_212.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_212.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_212.setFont(OBJ_212.getFont().deriveFont(OBJ_212.getFont().getStyle() | Font.BOLD));
            OBJ_212.setName("OBJ_212");
            OBJ_212.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_90ActionPerformed(e);
              }
            });
            panel2.add(OBJ_212);
            OBJ_212.setBounds(60, 332, 75, 25);

            //---- OBJ_213 ----
            OBJ_213.setText("@LD14A@");
            OBJ_213.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_213.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_213.setFont(OBJ_213.getFont().deriveFont(OBJ_213.getFont().getStyle() | Font.BOLD));
            OBJ_213.setName("OBJ_213");
            OBJ_213.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_93ActionPerformed(e);
              }
            });
            panel2.add(OBJ_213);
            OBJ_213.setBounds(60, 357, 75, 25);

            //---- OBJ_214 ----
            OBJ_214.setText("@LD15A@");
            OBJ_214.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_214.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_214.setFont(OBJ_214.getFont().deriveFont(OBJ_214.getFont().getStyle() | Font.BOLD));
            OBJ_214.setName("OBJ_214");
            OBJ_214.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_96ActionPerformed(e);
              }
            });
            panel2.add(OBJ_214);
            OBJ_214.setBounds(60, 382, 75, 25);

            //---- OBJ_215 ----
            OBJ_215.setText("@LD16A@");
            OBJ_215.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_215.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_215.setFont(OBJ_215.getFont().deriveFont(OBJ_215.getFont().getStyle() | Font.BOLD));
            OBJ_215.setName("OBJ_215");
            OBJ_215.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_99ActionPerformed(e);
              }
            });
            panel2.add(OBJ_215);
            OBJ_215.setBounds(60, 407, 75, 25);

            //---- OBJ_217 ----
            OBJ_217.setText("@LD18A@");
            OBJ_217.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_217.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_217.setFont(OBJ_217.getFont().deriveFont(OBJ_217.getFont().getStyle() | Font.BOLD));
            OBJ_217.setName("OBJ_217");
            OBJ_217.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_124ActionPerformed(e);
              }
            });
            panel2.add(OBJ_217);
            OBJ_217.setBounds(60, 457, 75, 25);

            //---- WTP02 ----
            WTP02.setToolTipText("Voir aide ou documentation");
            WTP02.setComponentPopupMenu(BTD);
            WTP02.setName("WTP02");
            panel2.add(WTP02);
            WTP02.setBounds(35, 55, 24, WTP02.getPreferredSize().height);

            //---- WTP03 ----
            WTP03.setToolTipText("Voir aide ou documentation");
            WTP03.setComponentPopupMenu(BTD);
            WTP03.setName("WTP03");
            panel2.add(WTP03);
            WTP03.setBounds(35, 80, 24, WTP03.getPreferredSize().height);

            //---- WTP04 ----
            WTP04.setToolTipText("Voir aide ou documentation");
            WTP04.setComponentPopupMenu(BTD);
            WTP04.setName("WTP04");
            panel2.add(WTP04);
            WTP04.setBounds(35, 105, 24, WTP04.getPreferredSize().height);

            //---- WTP05 ----
            WTP05.setToolTipText("Voir aide ou documentation");
            WTP05.setComponentPopupMenu(BTD);
            WTP05.setName("WTP05");
            panel2.add(WTP05);
            WTP05.setBounds(35, 130, 24, WTP05.getPreferredSize().height);

            //---- WTP06 ----
            WTP06.setToolTipText("Voir aide ou documentation");
            WTP06.setComponentPopupMenu(BTD);
            WTP06.setName("WTP06");
            panel2.add(WTP06);
            WTP06.setBounds(35, 155, 24, WTP06.getPreferredSize().height);

            //---- WTP07 ----
            WTP07.setToolTipText("Voir aide ou documentation");
            WTP07.setComponentPopupMenu(BTD);
            WTP07.setName("WTP07");
            panel2.add(WTP07);
            WTP07.setBounds(35, 180, 24, WTP07.getPreferredSize().height);

            //---- WTP08 ----
            WTP08.setToolTipText("Voir aide ou documentation");
            WTP08.setComponentPopupMenu(BTD);
            WTP08.setName("WTP08");
            panel2.add(WTP08);
            WTP08.setBounds(35, 205, 24, WTP08.getPreferredSize().height);

            //---- WTP09 ----
            WTP09.setToolTipText("Voir aide ou documentation");
            WTP09.setComponentPopupMenu(BTD);
            WTP09.setName("WTP09");
            panel2.add(WTP09);
            WTP09.setBounds(35, 230, 24, WTP09.getPreferredSize().height);

            //---- WTP10 ----
            WTP10.setToolTipText("Voir aide ou documentation");
            WTP10.setComponentPopupMenu(BTD);
            WTP10.setName("WTP10");
            panel2.add(WTP10);
            WTP10.setBounds(35, 255, 24, WTP10.getPreferredSize().height);

            //---- WTP11 ----
            WTP11.setToolTipText("Voir aide ou documentation");
            WTP11.setComponentPopupMenu(BTD);
            WTP11.setName("WTP11");
            panel2.add(WTP11);
            WTP11.setBounds(35, 280, 24, WTP11.getPreferredSize().height);

            //---- WTP12 ----
            WTP12.setToolTipText("Voir aide ou documentation");
            WTP12.setComponentPopupMenu(BTD);
            WTP12.setName("WTP12");
            panel2.add(WTP12);
            WTP12.setBounds(35, 305, 24, WTP12.getPreferredSize().height);

            //---- WTP13 ----
            WTP13.setToolTipText("Voir aide ou documentation");
            WTP13.setComponentPopupMenu(BTD);
            WTP13.setName("WTP13");
            panel2.add(WTP13);
            WTP13.setBounds(35, 330, 24, WTP13.getPreferredSize().height);

            //---- WTP14 ----
            WTP14.setToolTipText("Voir aide ou documentation");
            WTP14.setComponentPopupMenu(BTD);
            WTP14.setName("WTP14");
            panel2.add(WTP14);
            WTP14.setBounds(35, 355, 24, WTP14.getPreferredSize().height);

            //---- WTP15 ----
            WTP15.setToolTipText("Voir aide ou documentation");
            WTP15.setComponentPopupMenu(BTD);
            WTP15.setName("WTP15");
            panel2.add(WTP15);
            WTP15.setBounds(35, 380, 24, WTP15.getPreferredSize().height);

            //---- WTP16 ----
            WTP16.setToolTipText("Voir aide ou documentation");
            WTP16.setComponentPopupMenu(BTD);
            WTP16.setName("WTP16");
            panel2.add(WTP16);
            WTP16.setBounds(35, 405, 24, WTP16.getPreferredSize().height);

            //---- WTP17 ----
            WTP17.setToolTipText("Voir aide ou documentation");
            WTP17.setComponentPopupMenu(BTD);
            WTP17.setName("WTP17");
            panel2.add(WTP17);
            WTP17.setBounds(35, 430, 24, WTP17.getPreferredSize().height);

            //---- WTP01 ----
            WTP01.setToolTipText("Voir aide ou documentation");
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            panel2.add(WTP01);
            WTP01.setBounds(35, 30, 24, WTP01.getPreferredSize().height);

            //---- WTP18 ----
            WTP18.setToolTipText("Voir aide ou documentation");
            WTP18.setComponentPopupMenu(BTD);
            WTP18.setName("WTP18");
            panel2.add(WTP18);
            WTP18.setBounds(35, 455, 24, WTP18.getPreferredSize().height);

            //---- OBJ_200 ----
            OBJ_200.setText("@LD01A@");
            OBJ_200.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_200.setHorizontalAlignment(SwingConstants.TRAILING);
            OBJ_200.setFont(OBJ_200.getFont().deriveFont(OBJ_200.getFont().getStyle() | Font.BOLD));
            OBJ_200.setName("OBJ_200");
            OBJ_200.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_48ActionPerformed(e);
              }
            });
            panel2.add(OBJ_200);
            OBJ_200.setBounds(60, 30, 75, 25);

            //---- separator1 ----
            separator1.setName("separator1");
            panel2.add(separator1);
            separator1.setBounds(15, 37, 685, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            panel2.add(separator2);
            separator2.setBounds(15, 62, 685, separator2.getPreferredSize().height);

            //---- separator3 ----
            separator3.setName("separator3");
            panel2.add(separator3);
            separator3.setBounds(15, 87, 685, separator3.getPreferredSize().height);

            //---- separator4 ----
            separator4.setName("separator4");
            panel2.add(separator4);
            separator4.setBounds(15, 112, 685, separator4.getPreferredSize().height);

            //---- separator5 ----
            separator5.setName("separator5");
            panel2.add(separator5);
            separator5.setBounds(15, 137, 685, separator5.getPreferredSize().height);

            //---- separator6 ----
            separator6.setName("separator6");
            panel2.add(separator6);
            separator6.setBounds(15, 162, 685, separator6.getPreferredSize().height);

            //---- separator7 ----
            separator7.setName("separator7");
            panel2.add(separator7);
            separator7.setBounds(15, 187, 685, separator7.getPreferredSize().height);

            //---- separator8 ----
            separator8.setName("separator8");
            panel2.add(separator8);
            separator8.setBounds(15, 212, 685, separator8.getPreferredSize().height);

            //---- separator9 ----
            separator9.setName("separator9");
            panel2.add(separator9);
            separator9.setBounds(15, 237, 685, separator9.getPreferredSize().height);

            //---- separator10 ----
            separator10.setName("separator10");
            panel2.add(separator10);
            separator10.setBounds(15, 262, 685, separator10.getPreferredSize().height);

            //---- separator11 ----
            separator11.setName("separator11");
            panel2.add(separator11);
            separator11.setBounds(15, 287, 685, separator11.getPreferredSize().height);

            //---- separator12 ----
            separator12.setName("separator12");
            panel2.add(separator12);
            separator12.setBounds(15, 312, 685, separator12.getPreferredSize().height);

            //---- separator13 ----
            separator13.setName("separator13");
            panel2.add(separator13);
            separator13.setBounds(15, 337, 685, separator13.getPreferredSize().height);

            //---- separator14 ----
            separator14.setName("separator14");
            panel2.add(separator14);
            separator14.setBounds(15, 362, 685, separator14.getPreferredSize().height);

            //---- separator15 ----
            separator15.setName("separator15");
            panel2.add(separator15);
            separator15.setBounds(15, 387, 685, separator15.getPreferredSize().height);

            //---- separator16 ----
            separator16.setName("separator16");
            panel2.add(separator16);
            separator16.setBounds(15, 412, 685, separator16.getPreferredSize().height);

            //---- separator17 ----
            separator17.setName("separator17");
            panel2.add(separator17);
            separator17.setBounds(15, 437, 685, separator17.getPreferredSize().height);

            //---- separator18 ----
            separator18.setName("separator18");
            panel2.add(separator18);
            separator18.setBounds(15, 462, 685, separator18.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(725, 30, 25, 203);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(725, 278, 25, 203);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 780, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 495, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_33;
  private XRiTextField INDETB;
  private XRiTextField INDTYP;
  private JLabel OBJ_41;
  private XRiTextField WNPS;
  private JLabel OBJ_44;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WTEXT;
  private JLabel OBJ_126;
  private XRiComboBox WMDC;
  private JPanel panel2;
  private RiZoneSortie OBJ_102;
  private RiZoneSortie OBJ_101;
  private RiZoneSortie OBJ_100;
  private RiZoneSortie OBJ_103;
  private RiZoneSortie OBJ_104;
  private RiZoneSortie OBJ_105;
  private RiZoneSortie OBJ_106;
  private RiZoneSortie OBJ_107;
  private RiZoneSortie OBJ_108;
  private RiZoneSortie OBJ_109;
  private RiZoneSortie OBJ_110;
  private RiZoneSortie OBJ_111;
  private RiZoneSortie OBJ_112;
  private RiZoneSortie OBJ_113;
  private RiZoneSortie OBJ_114;
  private RiZoneSortie OBJ_115;
  private RiZoneSortie OBJ_116;
  private RiZoneSortie OBJ_117;
  private SNBoutonDetail OBJ_216;
  private SNBoutonDetail OBJ_201;
  private SNBoutonDetail OBJ_202;
  private SNBoutonDetail OBJ_203;
  private SNBoutonDetail OBJ_204;
  private SNBoutonDetail OBJ_205;
  private SNBoutonDetail OBJ_206;
  private SNBoutonDetail OBJ_207;
  private SNBoutonDetail OBJ_208;
  private SNBoutonDetail OBJ_209;
  private SNBoutonDetail OBJ_210;
  private SNBoutonDetail OBJ_211;
  private SNBoutonDetail OBJ_212;
  private SNBoutonDetail OBJ_213;
  private SNBoutonDetail OBJ_214;
  private SNBoutonDetail OBJ_215;
  private SNBoutonDetail OBJ_217;
  private XRiTextField WTP02;
  private XRiTextField WTP03;
  private XRiTextField WTP04;
  private XRiTextField WTP05;
  private XRiTextField WTP06;
  private XRiTextField WTP07;
  private XRiTextField WTP08;
  private XRiTextField WTP09;
  private XRiTextField WTP10;
  private XRiTextField WTP11;
  private XRiTextField WTP12;
  private XRiTextField WTP13;
  private XRiTextField WTP14;
  private XRiTextField WTP15;
  private XRiTextField WTP16;
  private XRiTextField WTP17;
  private XRiTextField WTP01;
  private XRiTextField WTP18;
  private SNBoutonDetail OBJ_200;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JComponent separator4;
  private JComponent separator5;
  private JComponent separator6;
  private JComponent separator7;
  private JComponent separator8;
  private JComponent separator9;
  private JComponent separator10;
  private JComponent separator11;
  private JComponent separator12;
  private JComponent separator13;
  private JComponent separator14;
  private JComponent separator15;
  private JComponent separator16;
  private JComponent separator17;
  private JComponent separator18;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
