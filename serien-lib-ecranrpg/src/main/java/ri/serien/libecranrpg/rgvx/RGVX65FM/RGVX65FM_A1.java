
package ri.serien.libecranrpg.rgvx.RGVX65FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVX65FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  public RGVX65FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_70 = new JLabel();
    OBJ_72 = new JLabel();
    INDNUM = new XRiTextField();
    INDETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_42 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_48 = new JLabel();
    ARG5A = new XRiCalendrier();
    PLA5A = new XRiCalendrier();
    ARG6A = new XRiCalendrier();
    PLA6A = new XRiCalendrier();
    ARG7A = new XRiTextField();
    OBJ_40 = new JLabel();
    OBJ_44 = new JLabel();
    ARG3N1 = new XRiTextField();
    ARG4N1 = new XRiTextField();
    ARG8A = new XRiTextField();
    ARG4N2 = new XRiTextField();
    NOMCLI = new XRiTextField();
    NOMFRS = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD2 = new JPopupMenu();
    menuItem2 = new JMenuItem();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Recherche des conditions de d\u00e9rogations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(5, 5, 93, 20);

          //---- OBJ_72 ----
          OBJ_72.setText("Num\u00e9ro");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche.add(OBJ_72);
          OBJ_72.setBounds(170, 6, 55, 18);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(225, 1, 60, INDNUM.getPreferredSize().height);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 1, 40, INDETB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setPreferredSize(new Dimension(600, 275));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Crit\u00e8res de s\u00e9lection"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_42 ----
            OBJ_42.setText("Date de fin entre");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(35, 140, 150, 20);

            //---- OBJ_38 ----
            OBJ_38.setText("Date de d\u00e9but entre");
            OBJ_38.setName("OBJ_38");
            panel1.add(OBJ_38);
            OBJ_38.setBounds(35, 105, 150, 20);

            //---- OBJ_36 ----
            OBJ_36.setText("Fournisseur");
            OBJ_36.setName("OBJ_36");
            panel1.add(OBJ_36);
            OBJ_36.setBounds(35, 70, 150, 20);

            //---- OBJ_33 ----
            OBJ_33.setText("Client");
            OBJ_33.setName("OBJ_33");
            panel1.add(OBJ_33);
            OBJ_33.setBounds(35, 35, 150, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("Num\u00e9ro d'autorisation");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(35, 175, 150, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Responsable");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(35, 210, 150, 20);

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD2);
            ARG5A.setName("ARG5A");
            panel1.add(ARG5A);
            ARG5A.setBounds(200, 100, 105, ARG5A.getPreferredSize().height);

            //---- PLA5A ----
            PLA5A.setComponentPopupMenu(BTD2);
            PLA5A.setName("PLA5A");
            panel1.add(PLA5A);
            PLA5A.setBounds(345, 100, 105, PLA5A.getPreferredSize().height);

            //---- ARG6A ----
            ARG6A.setComponentPopupMenu(BTD2);
            ARG6A.setName("ARG6A");
            panel1.add(ARG6A);
            ARG6A.setBounds(200, 135, 105, ARG6A.getPreferredSize().height);

            //---- PLA6A ----
            PLA6A.setComponentPopupMenu(BTD2);
            PLA6A.setName("PLA6A");
            panel1.add(PLA6A);
            PLA6A.setBounds(345, 135, 105, PLA6A.getPreferredSize().height);

            //---- ARG7A ----
            ARG7A.setComponentPopupMenu(BTD2);
            ARG7A.setName("ARG7A");
            panel1.add(ARG7A);
            ARG7A.setBounds(200, 170, 110, ARG7A.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setText("et");
            OBJ_40.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(310, 105, 29, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("et");
            OBJ_44.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(310, 140, 29, 20);

            //---- ARG3N1 ----
            ARG3N1.setComponentPopupMenu(BTD);
            ARG3N1.setName("ARG3N1");
            panel1.add(ARG3N1);
            ARG3N1.setBounds(200, 30, 60, ARG3N1.getPreferredSize().height);

            //---- ARG4N1 ----
            ARG4N1.setComponentPopupMenu(BTD);
            ARG4N1.setName("ARG4N1");
            panel1.add(ARG4N1);
            ARG4N1.setBounds(200, 65, 20, ARG4N1.getPreferredSize().height);

            //---- ARG8A ----
            ARG8A.setComponentPopupMenu(BTD);
            ARG8A.setName("ARG8A");
            panel1.add(ARG8A);
            ARG8A.setBounds(200, 205, 110, ARG8A.getPreferredSize().height);

            //---- ARG4N2 ----
            ARG4N2.setComponentPopupMenu(BTD);
            ARG4N2.setName("ARG4N2");
            panel1.add(ARG4N2);
            ARG4N2.setBounds(220, 65, 60, ARG4N2.getPreferredSize().height);

            //---- NOMCLI ----
            NOMCLI.setComponentPopupMenu(BTD);
            NOMCLI.setPreferredSize(new Dimension(100, 28));
            NOMCLI.setMinimumSize(new Dimension(100, 28));
            NOMCLI.setMaximumSize(new Dimension(100, 28));
            NOMCLI.setName("NOMCLI");
            panel1.add(NOMCLI);
            NOMCLI.setBounds(265, 30, 280, NOMCLI.getPreferredSize().height);

            //---- NOMFRS ----
            NOMFRS.setComponentPopupMenu(BTD);
            NOMFRS.setPreferredSize(new Dimension(100, 28));
            NOMFRS.setMinimumSize(new Dimension(100, 28));
            NOMFRS.setMaximumSize(new Dimension(100, 28));
            NOMFRS.setName("NOMFRS");
            panel1.add(NOMFRS);
            NOMFRS.setBounds(285, 65, 280, NOMFRS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(10, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- menuItem2 ----
      menuItem2.setText("Aide en ligne");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD2.add(menuItem2);
    }

    //======== riMenu2 ========
    {
      riMenu2.setName("riMenu2");

      //---- riMenu_bt2 ----
      riMenu_bt2.setText("Options");
      riMenu_bt2.setName("riMenu_bt2");
      riMenu2.add(riMenu_bt2);
    }

    //======== riSousMenu6 ========
    {
      riSousMenu6.setName("riSousMenu6");

      //---- riSousMenu_bt6 ----
      riSousMenu_bt6.setText("Stock. personnalisation");
      riSousMenu_bt6.setToolTipText("Stockage de la personnalisation");
      riSousMenu_bt6.setName("riSousMenu_bt6");
      riSousMenu_bt6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt6ActionPerformed(e);
        }
      });
      riSousMenu6.add(riSousMenu_bt6);
    }

    //======== riSousMenuF_dupli ========
    {
      riSousMenuF_dupli.setName("riSousMenuF_dupli");

      //---- riSousMenu_bt_dupli ----
      riSousMenu_bt_dupli.setText("Duplication");
      riSousMenu_bt_dupli.setToolTipText("Duplication");
      riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
      riSousMenuF_dupli.add(riSousMenu_bt_dupli);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_70;
  private JLabel OBJ_72;
  private XRiTextField INDNUM;
  private XRiTextField INDETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_42;
  private JLabel OBJ_38;
  private JLabel OBJ_36;
  private JLabel OBJ_33;
  private JLabel OBJ_46;
  private JLabel OBJ_48;
  private XRiCalendrier ARG5A;
  private XRiCalendrier PLA5A;
  private XRiCalendrier ARG6A;
  private XRiCalendrier PLA6A;
  private XRiTextField ARG7A;
  private JLabel OBJ_40;
  private JLabel OBJ_44;
  private XRiTextField ARG3N1;
  private XRiTextField ARG4N1;
  private XRiTextField ARG8A;
  private XRiTextField ARG4N2;
  private XRiTextField NOMCLI;
  private XRiTextField NOMFRS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD2;
  private JMenuItem menuItem2;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
