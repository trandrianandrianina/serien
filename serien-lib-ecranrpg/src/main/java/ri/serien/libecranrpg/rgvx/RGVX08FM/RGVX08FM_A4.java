/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.rgvx.RGVX08FM;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.sncategoriezonepersonnalisee.SNCategorieZonePersonnalisee;
import ri.serien.libswing.composant.metier.referentiel.article.snfamille.SNFamille;
import ri.serien.libswing.composant.metier.referentiel.article.sngroupe.SNGroupe;
import ri.serien.libswing.composant.metier.referentiel.article.snsousfamille.SNSousFamille;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreRecherche;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * Ecran de recherche des articles.
 * [GVM131] Gestion des ventes -> Fiches permanentes -> Articles -> Articles
 */
public class RGVX08FM_A4 extends SNPanelEcranRPG implements ioFrame {
  // Constante
  private static final int NOMBRE_ZP_LONGUES = 18;
  // Boutons
  private static final String BOUTON_CREATION = "Créer un article";
  private static final String BOUTON_DUPLICATION = "Dupliquer un article";
  private static final String BOUTON_RETOURNER_RECHERCHE = "Retourner en recherche";
  private static final String BOUTON_AUTRE_VUE = "Changer vue liste";
  private static final String BOUTON_OPTIONS_ARTICLE = "Voir options article";
  private static final String BOUTON_DEVERROUILLER_ARTICLE = "Déverrouiller l'article";
  private static final String BOUTON_HISTORIQUE_MODIFICATIONS = "Voir historique modifications";
  private static final String BOUTON_PRIX_CHANTIER = "Voir prix chantier";
  private static final String BOUTON_ARTICLES_VERROUILLES = "Articles vérrouillés";
  private static final String BOUTON_EXPORTER_LISTE = "Exporter la liste";
  
  private Icon iegale = null;
  private Icon idifferent = null;
  
  // Etat
  // "" = Tous
  // 1 = Désactivé
  // 2 = Epuisé
  // 5 = Pré-fin de série
  // 6 = Fin de série
  private String[] ARG10A_Value = { "", "1", "2", "5", "6", };
  
  // Type de saisie
  // "" = standard
  // T = Article transport
  // 0 = Article sur-conditionnement (palette)
  // K = Article kit commentaire
  // S = Par numéro de série
  // V = En valeur
  // Q = En double quantité
  // 1 = En longueur
  // 2 = En surface
  // 3 = En volume
  // 4 = En surface et nombre
  private String[] ARG23A_Value = { "", "T", "0", "K", "S", "V", "Q", "1", "2", "4", "3" };
  
  // Spécial
  // "" = Tous
  // "1" = Article spécial
  // "0" = Article non spécial
  private String[] ARG24A_Value = { "", "1", "0" };
  
  // Type de réapprovisionnement
  // 0 = Rupture sur stock minimum
  // 1 = Sur consommation moyenne
  // 2 = Manuel
  // 3 = Sur consommation prévue
  // 4 = Sur consommation moyenne plafonnée
  // 5 = Plafond converte globale
  // 6 = Complément stock maximum
  // 7 = Plafond de converture/magasin
  // 8 = Non géré
  private String[] ARG29A_Value = { "0", "1", "2", "3", "4", "5", "6", "7", "8" };
  
  // Gestion des lots
  // "" = Tous
  // 1 = Géré par lots
  // 2 = Géré par sous lots
  private String[] ARG31A_Value = { "", "1", "2", };
  
  // Mode de gestion
  // "" = Négoce
  // A = Acheté, non vendu
  // V = Vendu, non acheté
  // F = Fabriqué
  private String[] ARG32A_Value = { "", "A", "V", "F" };
  
  // Disponbilité
  // "" = Tous
  // D = Disponible
  // T = Non disponible
  // S = En stock
  // R = En rupture de stock
  private String[] DISPO_Value = { "", "D", "T", "S", "R", };
  
  // Déprécié
  // "" = Tous
  // E = Article non déprécié
  // N = Article déprécié
  private String[] SCAN53_Value = { "", "E", "N" };
  
  // Liste des articles
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15" };
  private String[] _WTP01_Title = { "TITG" };
  private String[][] _WTP01_Data = { { "LDG01" }, { "LDG02" }, { "LDG03" }, { "LDG04" }, { "LDG05" }, { "LDG06" }, { "LDG07" },
      { "LDG08" }, { "LDG09" }, { "LDG10" }, { "LDG11" }, { "LDG12" }, { "LDG13" }, { "LDG14" }, { "LDG15" } };
  private int[] _WTP01_Width = { 1050, };
  
  private String[] saisieZPLongue = { "CAD01", "CAD02", "CAD03", "CAD04", "CAD05", "CAD06", "CAD07", "CAD08", };
  
  private boolean isModeCreation = false;
  private boolean isModeDuplication = false;
  private boolean isArticleSelectionne = false;
  
  // Zones personnalisées sélectionnées par les comboBox (si les ZP longues sont paramétrées)
  private ZPLongue zpLongueSelectionnee1 = null;
  private ZPLongue zpLongueSelectionnee2 = null;
  private ZPLongue zpLongueSelectionnee3 = null;
  
  /**
   * Constructeur.
   */
  public RGVX08FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    
    // Etat
    ARG10A.setValeurs(ARG10A_Value, null);
    
    // Type de saisie
    ARG23A.setValeurs(ARG23A_Value, null);
    
    // Spécial
    ARG24A.setValeurs(ARG24A_Value, null);
    ARG24A.setSelectedIndex(2);
    
    // Type de réapprovisionnement
    ARG29A.setValeurs(ARG29A_Value, null);
    
    // Gestion des lots
    ARG31A.setValeurs(ARG31A_Value, null);
    
    // Mode de gestion
    ARG32A.setValeurs(ARG32A_Value, null);
    
    // Disponbilité
    DISPO.setValeurs(DISPO_Value, null);
    
    // Déprécié
    SCAN73.setValeurs(SCAN53_Value, null);
    
    // Autres valeurs
    ARG13A.setValeursSelection("1", "0");
    ARG25A.setValeursSelection("1", " ");
    WTGCD.setValeursSelection("1", " ");
    OPT8.setValeursSelection("P", " ");
    ARG67A.setValeursSelection("1", "");
    
    // Liste des articles
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
    // Initialiser les boutons
    snBarreBouton.ajouterBouton(BOUTON_CREATION, 'c', true);
    snBarreBouton.ajouterBouton(BOUTON_RETOURNER_RECHERCHE, 'r', false);
    snBarreBouton.ajouterBouton(BOUTON_DUPLICATION, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_OPTIONS_ARTICLE, 'o', false);
    snBarreBouton.ajouterBouton(BOUTON_DEVERROUILLER_ARTICLE, 'r', false);
    snBarreBouton.ajouterBouton(BOUTON_HISTORIQUE_MODIFICATIONS, 'h', false);
    snBarreBouton.ajouterBouton(BOUTON_PRIX_CHANTIER, 'p', false);
    snBarreBouton.ajouterBouton(BOUTON_AUTRE_VUE, 'v', true);
    snBarreBouton.ajouterBouton(BOUTON_ARTICLES_VERROUILLES, 'd', true);
    snBarreBouton.ajouterBouton(BOUTON_EXPORTER_LISTE, 'x', true);
    snBarreBouton.ajouterBouton(EnumBouton.CONSULTER, false);
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, false);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Initialiser la barre de recherche
    snBarreRecherche.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    
    // Activer la mémorisation de la ligne sélectionnée
    WTP01.setMemorisationLigneSelectionnee(true);
    
    OBJ_DEBL.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    OBJ_FINL.setIcon(lexique.chargerImage("images/pfin20.png", true));
    iegale = lexique.chargerImage("images/egale.png", true);
    idifferent = lexique.chargerImage("images/different.png", true);
    
    // Activation de la touche Entrée (dans la zone de saisie) sur les champs dates de création de la recherche
    ARG20D.setActiverToucheEntreeZoneSaisie(true);
    PLA20D.setActiverToucheEntreeZoneSaisie(true);
    
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lbMotClassement1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC1@")).trim());
    lbMotClassement2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TMC2@")).trim());
    TIDX15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL01@")).trim());
    TIDX16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL02@")).trim());
    TIDX17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL03@")).trim());
    TIDX18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL04@")).trim());
    TIDX19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ZPL05@")).trim());
    snLabelTitreTableau.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Liste des articles (@NBRERL@)")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    setDiverses();
    ManagerSessionClient.getInstance().setIdEtablissement(getIdSession(),
        IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB")));
    
    // Afficher le logo
    pnlBpresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // N'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Afficher les erreurs dans une boîte de dialogue
    gererLesErreurs("19");
    
    // Définir le mode actif
    isModeDuplication = lexique.isTrue("56");
    isModeCreation = (lexique.getMode() == Lexical.MODE_CREATION) && !isModeDuplication;
    
    // Dispatcher en fonction du mode actif
    if (isModeCreation) {
      setDataModeCreation();
    }
    else if (isModeDuplication) {
      setDataModeDuplication();
    }
    else {
      setDataModeRecherche();
      setInitialiserModeListe();
    }
    
    // Initialisation des ZP longues
    setZpLongues();
  }
  
  private void initialiserRecherche() {
    // Effacement des lignes sélectionnées ou mémorisée dans la liste
    WTP01.getSelectionModel().clearSelection();
    WTP01.effacerMemorisationLigne();
    
    // Onglet : Critères pincipaux
    lexique.HostFieldPutData("ARG76A", 0, "");
    lexique.HostFieldPutData("ARG2A", 0, "");
    lexique.HostFieldPutData("ARG3A", 0, "");
    lexique.HostFieldPutData("ARG4A", 0, "");
    snEtablissement.setSelectionParChampRPG(lexique, "");
    lexique.HostFieldPutData("ARG5A", 0, "");
    lexique.HostFieldPutData("ARG6A", 0, "");
    
    // Onglet : Critères article
    lexique.HostFieldPutData("SCAN10", 0, "N");
    btnSCAN10.setIcon(idifferent);
    lexique.HostFieldPutData("ARG10A", 0, "1");
    
    lexique.HostFieldPutData("SCAN32", 0, "");
    btnSCAN32.setIcon(iegale);
    lexique.HostFieldPutData("ARG32A", 0, "0");
    
    lexique.HostFieldPutData("SCAN23", 0, "");
    btnSCAN23.setIcon(iegale);
    lexique.HostFieldPutData("ARG23A", 0, "");
    
    cbClasse.setSelectedIndex(0);
    lexique.HostFieldPutData("SCAN30", 0, "N");
    btnSCAN30.setIcon(idifferent);
    lexique.HostFieldPutData("ARG30A", 0, "R");
    
    lexique.HostFieldPutData("ARG67A", 0, "");
    lexique.HostFieldPutData("ARG25A", 0, "");
    lexique.HostFieldPutData("ARG13A", 0, "");
    
    lexique.HostFieldPutData("ARG20D", 0, "");
    lexique.HostFieldPutData("PLA20D", 0, "");
    lexique.HostFieldPutData("ARG11A", 0, "");
    lexique.HostFieldPutData("ARG9N", 0, "");
    lexique.HostFieldPutData("WTGCD", 0, "");
    
    // Onglet : Critères fournisseur
    snFournisseur.setSelectionParChampRPG(lexique, "", "");
    lexique.HostFieldPutData("OPT8", 0, " ");
    lexique.HostFieldPutData("WFRCLA", 0, "");
    lexique.HostFieldPutData("ARG38A", 0, "");
    lexique.HostFieldPutData("ARG36A", 0, "");
    lexique.HostFieldPutData("ARG37A", 0, "");
    
    // Onglet : Critères achats/stock
    lexique.HostFieldPutData("DISPO", 0, "");
    lexique.HostFieldPutData("SCAN29", 0, "");
    btnSCAN29.setIcon(iegale);
    lexique.HostFieldPutData("ARG31A", 0, "");
    lexique.HostFieldPutData("SCAN73", 0, "");
    lexique.HostFieldPutData("ARG12D", 0, "");
    lexique.HostFieldPutData("PLA12D", 0, "");
    lexique.HostFieldPutData("ARG26A", 0, "");
    lexique.HostFieldPutData("ARG71A", 0, "");
    
    // Onglet : Critères ventes
    lexique.HostFieldPutData("ARG7A", 0, "");
    lexique.HostFieldPutData("ARG21A", 0, "");
    lexique.HostFieldPutData("ARG14A", 0, "");
    lexique.HostFieldPutData("ARG22D", 0, "");
    lexique.HostFieldPutData("PLA22D", 0, "");
    lexique.HostFieldPutData("ARG27A", 0, "");
    lexique.HostFieldPutData("ARG28A", 0, "");
    
    // Onglet : zones personnalisées
    lexique.HostFieldPutData("ARG15A", 0, "");
    lexique.HostFieldPutData("ARG16A", 0, "");
    lexique.HostFieldPutData("ARG17A", 0, "");
    lexique.HostFieldPutData("ARG18A", 0, "");
    lexique.HostFieldPutData("ARG19A", 0, "");
    
    setData();
  }
  
  /**
   * Initialise le mode d'action dans la liste (Consultation, Modification).
   * Par défaut le mode activé est le mode Consultation, si l'indicateur 52 est levé alors la liste passe en mode Modification.
   */
  private void setInitialiserModeListe() {
    boolean isModeModification = lexique.isTrue("52");
    boolean isModeConsultation = !isModeModification;
    btModeConsultation.setVisible(isModeConsultation);
    btModeModification.setVisible(isModeModification);
  }
  
  /**
   * Détermine l'indice d'une ZP dans une comboBox
   */
  private int getSelectedIndex(ArrayList<ZPLongue> pCombo, ZPLongue pZpSelectionnee) {
    if (pCombo == null || pZpSelectionnee == null) {
      return 0;
    }
    for (int i = 0; i < pCombo.size(); i++) {
      if (pCombo.get(i).getNomChamps().equals(pZpSelectionnee.getNomChamps())
          && pCombo.get(i).getNomChampsSaisie().equals(pZpSelectionnee.getNomChampsSaisie())) {
        return i + 1;
      }
    }
    return 0;
  }
  
  private void setDataModeCreation() {
    CardLayout cardLayout = (CardLayout) pnlContenu.getLayout();
    cardLayout.show(pnlContenu, "cardCreation");
    
    // Masquer le panneau concernant l'article dupliqué en création pure
    pnlCreationArticleDuplique.setVisible(false);
    
    // Modifier le bandeau de titre
    pnlBpresentation.setText("Création d'un article");
    
    // Charger les données de l'établissement de l'article créé
    snEtablissementArticleCree.setSession(getSession());
    snEtablissementArticleCree.charger(false);
    snEtablissementArticleCree.setSelectionParChampRPG(lexique, "INDETB");
    
    // Sélection le groupe
    snGroupeCreation.setSession(getSession());
    if (snEtablissementArticleCree.getIdSelection() != null) {
      snGroupeCreation.setIdEtablissement(snEtablissementArticleCree.getIdSelection());
      snGroupeCreation.setTousAutorise(true);
      snGroupeCreation.charger(false);
    }
    
    // Sélection le groupe
    snFamilleCreation.setSession(getSession());
    if (snEtablissementArticleCree.getIdSelection() != null) {
      snFamilleCreation.setIdEtablissement(snEtablissementArticleCree.getIdSelection());
      snFamilleCreation.setTousAutorise(true);
      snFamilleCreation.charger(false);
      snFamilleCreation.setSelectionParChampRPG(lexique, "WFAM");
    }
    
    snGroupeCreation.lierComposantFamille(snFamilleCreation);
    
    // Afficher la famille, obligatoire pour la création de l'article
    lbFamilleArticleCree.setVisible(true);
    
    // Rafraîchir l'affichage des boutons
    snBarreBouton.activerBouton(BOUTON_CREATION, false);
    snBarreBouton.activerBouton(BOUTON_DUPLICATION, false);
    snBarreBouton.activerBouton(BOUTON_RETOURNER_RECHERCHE, true);
    snBarreBouton.activerBouton(BOUTON_OPTIONS_ARTICLE, false);
    snBarreBouton.activerBouton(BOUTON_DEVERROUILLER_ARTICLE, false);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, false);
    snBarreBouton.activerBouton(BOUTON_PRIX_CHANTIER, false);
    snBarreBouton.activerBouton(BOUTON_AUTRE_VUE, false);
    snBarreBouton.activerBouton(BOUTON_ARTICLES_VERROUILLES, false);
    snBarreBouton.activerBouton(BOUTON_EXPORTER_LISTE, false);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, true);
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, false);
  }
  
  /**
   * Affiche le panneau pour dupliquer un article
   * Mets à jour les informations de base (code, établissement, famille) concernant l'article nouvellement créé
   * .
   */
  private void setDataModeDuplication() {
    CardLayout cardLayout = (CardLayout) pnlContenu.getLayout();
    cardLayout.show(pnlContenu, "cardCreation");
    
    // Affiché le panneau concernant l'article dupliqué
    pnlCreationArticleDuplique.setVisible(true);
    
    // Modifier le bandeau de titre
    pnlBpresentation.setText("Création d'un article par duplication");
    
    // Charger les données de l'établissement de l'article dupliqué
    snEtablissementArticleDuplique.setSession(getSession());
    snEtablissementArticleDuplique.charger(false);
    snEtablissementArticleDuplique.setSelectionParChampRPG(lexique, "INDETB");
    
    // Renseigner le code de l'article dupliqué
    if (isArticleSelectionne) {
      int ligneSelectionne = WTP01.getSelectedRow();
      // Tester si un article a été sélectionné
      if (ligneSelectionne != -1) {
        // Remplir le champ du code article dupliqué par le code de l'article sélectionné
        WIART3.setText(Constantes.normerTexte((String) WTP01.getModel().getValueAt(ligneSelectionne, 0)));
      }
    }
    
    // Charger les données de l'établissement de l'article créé
    snEtablissementArticleCree.setSession(getSession());
    snEtablissementArticleCree.charger(false);
    snEtablissementArticleCree.setSelectionParChampRPG(lexique, "WIETB3");
    
    // Masquer la famille, héritée de l'article dupliqué
    lbFamilleArticleCree.setVisible(false);
    snFamilleCreation.setVisible(false);
    
    // Rafraîchir l'affichage des boutons
    snBarreBouton.activerBouton(BOUTON_CREATION, false);
    snBarreBouton.activerBouton(BOUTON_DUPLICATION, false);
    snBarreBouton.activerBouton(BOUTON_RETOURNER_RECHERCHE, true);
    snBarreBouton.activerBouton(BOUTON_OPTIONS_ARTICLE, false);
    snBarreBouton.activerBouton(BOUTON_DEVERROUILLER_ARTICLE, false);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, false);
    snBarreBouton.activerBouton(BOUTON_PRIX_CHANTIER, false);
    snBarreBouton.activerBouton(BOUTON_AUTRE_VUE, false);
    snBarreBouton.activerBouton(BOUTON_ARTICLES_VERROUILLES, false);
    snBarreBouton.activerBouton(BOUTON_EXPORTER_LISTE, false);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, true);
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, false);
  }
  
  private void setDataModeRecherche() {
    CardLayout cardLayout = (CardLayout) pnlContenu.getLayout();
    cardLayout.show(pnlContenu, "cardRecherche");
    
    // Mettre à jour l'établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    // Sélectioner le fournisseur
    snFournisseur.setSession(getSession());
    snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseur.charger(false);
    snFournisseur.setSelectionParChampRPG(lexique, "ARG8N1", "ARG8N2");
    
    // Sélectioner la catégorie de ZP
    snCategorieZP.setSession(getSession());
    snCategorieZP.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieZP.charger(false);
    snCategorieZP.setSelectionParChampRPG(lexique, "ARG33A");
    
    // Sélection la famille
    snFamille.setSession(getSession());
    snFamille.setIdEtablissement(snEtablissement.getIdSelection());
    snFamille.setTousAutorise(true);
    snFamille.charger(false);
    snFamille.setSelectionParChampRPG(lexique, "ARG5A");
    
    // Sélection la Sous-famille
    snSousFamille.setSession(getSession());
    snSousFamille.setIdEtablissement(snEtablissement.getIdSelection());
    snSousFamille.setTousAutorise(true);
    snSousFamille.charger(false);
    snSousFamille.setSelectionParChampRPG(lexique, "ARG6A");
    snSousFamille.lierComposantFamille(snFamille);
    
    // Tester le type d'affich a ge des articles
    String typeArticle = lexique.HostFieldGetData("LIBPG").trim();
    
    // Tester si on affi che des articles standards
    boolean isModeArticleStandard = typeArticle.equals("");
    
    // Tester si on est en mode hors gamme
    boolean isModeHorsGamme = lexique.HostFieldGetData("WGHG").trim().equalsIgnoreCase("H");
    
    // Tester si la liste est pleine
    boolean isListePleine = lexique.isTrue("64");
    
    // Tester si on est en articles commentaires
    boolean isCommentaire = lexique.isTrue("74");
    
    // Modifier le titre du bandeau
    String titre = "Recherche articles";
    if (!isModeArticleStandard) {
      titre += " " + typeArticle;
    }
    if (isModeHorsGamme) {
      titre += " (hors gamme)";
    }
    pnlBpresentation.setText(titre);
    
    // Sélectionner l'onglet mémorisé
    if (lexique.getValeurVariableGlobale("ongletEnCours_RGVX08FM_A4_C3") != null) {
      pnlFiltre.setSelectedIndex((Integer) lexique.getValeurVariableGlobale("ongletEnCours_RGVX08FM_A4_C3"));
    }
    
    // Est ce qu'un article est selectionné dans la liste
    isArticleSelectionne = WTP01.getSelectedRow() >= 0;
    
    // affichage systématique de tous les critères
    lexique.SetOff(61);
    
    // Contruction du nom du fichier si export tableur (VEXP0A)
    lexique.setNomFichierTableur("Recherche article");
    
    // Si on n'est pas nomenclature
    if (!typeArticle.contains("Nomenclatures")) {
      ARG13A.setValeursSelection("1", " ");
    }
    
    // tri sur les options de la liste
    miDeverrouillage.setVisible(isModeArticleStandard);
    miHistorique.setVisible(isModeArticleStandard);
    
    // Est-on en articles commentaires ?
    if (isCommentaire) {
      lbFamille.setVisible(false);
      snFamille.setVisible(false);
      
      lbSousFamille.setVisible(false);
      snSousFamille.setVisible(false);
      
      lbTarif.setVisible(false);
      ARG7A.setVisible(false);
    }
    
    if (lexique.HostFieldGetData("SCAN23").trim().equals("N")) {
      btnSCAN23.setIcon(idifferent);
    }
    else {
      btnSCAN23.setIcon(iegale);
    }
    
    if (lexique.HostFieldGetData("ARG30A").trim().equals("R")) {
      if (lexique.HostFieldGetData("SCAN30").trim().equals("N")) {
        cbClasse.setSelectedIndex(0);
      }
      else {
        cbClasse.setSelectedIndex(1);
      }
    }
    else {
      cbClasse.setSelectedIndex(2);
    }
    rafraichirClasse();
    
    // Signe égal de la classe personnalisée
    if (lexique.HostFieldGetData("SCAN30").trim().equals("N")) {
      btnSCAN30.setIcon(idifferent);
    }
    else {
      btnSCAN30.setIcon(iegale);
    }
    
    if (lexique.HostFieldGetData("SCAN29").trim().equals("N")) {
      btnSCAN29.setIcon(idifferent);
    }
    else {
      btnSCAN29.setIcon(iegale);
    }
    
    if (lexique.HostFieldGetData("SCAN32").trim().equals("N")) {
      btnSCAN32.setIcon(idifferent);
    }
    else {
      btnSCAN32.setIcon(iegale);
    }
    
    if (lexique.HostFieldGetData("SCAN10").trim().equals("N")) {
      btnSCAN10.setIcon(idifferent);
    }
    else {
      btnSCAN10.setIcon(iegale);
    }
    
    // Rafraîchir l'affichage des boutons
    snBarreBouton.activerBouton(BOUTON_CREATION, true);
    snBarreBouton.activerBouton(BOUTON_DUPLICATION, true);
    snBarreBouton.activerBouton(BOUTON_RETOURNER_RECHERCHE, false);
    snBarreBouton.activerBouton(BOUTON_OPTIONS_ARTICLE, isArticleSelectionne);
    snBarreBouton.activerBouton(BOUTON_DEVERROUILLER_ARTICLE, isArticleSelectionne);
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, isArticleSelectionne);
    snBarreBouton.activerBouton(BOUTON_PRIX_CHANTIER, isArticleSelectionne);
    snBarreBouton.activerBouton(BOUTON_AUTRE_VUE, isListePleine);
    snBarreBouton.activerBouton(BOUTON_EXPORTER_LISTE, isListePleine);
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, WTP01.getSelectedRow() >= 0);
    snBarreBouton.activerBouton(EnumBouton.VALIDER, false);
  }
  
  /*
   * Initialisation des combo box qui permettent de filtrer le résultat de recherche sur les zones personnalisées longues
   */
  private void setZpLongues() {
    // On vide les combo
    cbZPLongue1.removeAllItems();
    cbZPLongue2.removeAllItems();
    cbZPLongue3.removeAllItems();
    
    // Liste des 18 ZP possibles
    ArrayList<ZPLongue> listeZpLongue = new ArrayList<ZPLongue>();
    // On rempli la liste avec les ZP existantes
    for (int i = 0; i < NOMBRE_ZP_LONGUES; i++) {
      if (i < 9) {
        if (!lexique.HostFieldGetData("WZP0" + (i + 1)).trim().isEmpty()) {
          // Si champs alpha
          String nomChampsSaisie = "CAD0" + (i + 1);
          // Si champs numérique
          if (lexique.HostFieldGetData("WZTT0" + (i + 1)).equalsIgnoreCase("N")) {
            nomChampsSaisie = "CND0" + (i + 1);
          }
          listeZpLongue
              .add(new ZPLongue(lexique.HostFieldGetData("WZP0" + (i + 1)), nomChampsSaisie, lexique.HostFieldGetData(nomChampsSaisie)));
        }
      }
      else {
        if (!lexique.HostFieldGetData("WZP" + (i + 1)).trim().isEmpty()) {
          // Si champs alpha
          String nomChampsSaisie = "CAD" + (i + 1);
          // Si champs numérique
          if (lexique.HostFieldGetData("WZTT" + (i + 1)).equalsIgnoreCase("N")) {
            nomChampsSaisie = "CND" + (i + 1);
          }
          listeZpLongue
              .add(new ZPLongue(lexique.HostFieldGetData("WZP" + (i + 1)), nomChampsSaisie, lexique.HostFieldGetData(nomChampsSaisie)));
        }
      }
    }
    
    // Si la liste n'est pas vide c'est que les ZP longues sont paramétrées
    if (!listeZpLongue.isEmpty()) {
      
      // On rajoute un premier choix à blanc
      cbZPLongue1.addItem(null);
      cbZPLongue2.addItem(null);
      cbZPLongue3.addItem(null);
      
      // On ajoute les ZP existantes dans les combos
      for (int i = 0; i < listeZpLongue.size(); i++) {
        cbZPLongue1.addItem(listeZpLongue.get(i));
        cbZPLongue2.addItem(listeZpLongue.get(i));
        cbZPLongue3.addItem(listeZpLongue.get(i));
      }
      
      // Initialisation des objets de sélection si ranvoyés par le GAP
      for (int i = 0; i < listeZpLongue.size(); i++) {
        if (!listeZpLongue.get(i).getValeur().trim().isEmpty() && zpLongueSelectionnee1 == null) {
          zpLongueSelectionnee1 = listeZpLongue.get(i);
          continue;
        }
        if (!listeZpLongue.get(i).getValeur().trim().isEmpty() && zpLongueSelectionnee2 == null) {
          zpLongueSelectionnee2 = listeZpLongue.get(i);
          continue;
        }
        if (!listeZpLongue.get(i).getValeur().trim().isEmpty() && zpLongueSelectionnee3 == null) {
          zpLongueSelectionnee3 = listeZpLongue.get(i);
          break;
        }
      }
      
      // Pour rechargement des valeurs précédemment saisies : On initialise l'indice de sélection des combos
      cbZPLongue1.setSelectedIndex(getSelectedIndex(listeZpLongue, zpLongueSelectionnee1));
      cbZPLongue2.setSelectedIndex(getSelectedIndex(listeZpLongue, zpLongueSelectionnee2));
      cbZPLongue3.setSelectedIndex(getSelectedIndex(listeZpLongue, zpLongueSelectionnee3));
      
      // Pour rechargement des valeurs précédemment saisies : On initialise le texte recherché pour chaque ZP
      if (zpLongueSelectionnee1 != null) {
        tfZPLongue1.setText(zpLongueSelectionnee1.getValeur());
      }
      else {
        tfZPLongue1.setText("");
      }
      if (zpLongueSelectionnee2 != null) {
        tfZPLongue2.setText(zpLongueSelectionnee2.getValeur());
      }
      else {
        tfZPLongue2.setText("");
      }
      if (zpLongueSelectionnee3 != null) {
        tfZPLongue3.setText(zpLongueSelectionnee3.getValeur());
      }
      else {
        tfZPLongue3.setText("");
      }
    }
    
    // Visibilité des composants correspondants aux ZP
    cbZPLongue1.setVisible(!listeZpLongue.isEmpty());
    cbZPLongue2.setVisible(!listeZpLongue.isEmpty());
    cbZPLongue3.setVisible(!listeZpLongue.isEmpty());
    tfZPLongue1.setVisible(!listeZpLongue.isEmpty());
    tfZPLongue2.setVisible(!listeZpLongue.isEmpty());
    tfZPLongue3.setVisible(!listeZpLongue.isEmpty());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Renseigner l'établissement à partir du composant SNEtablissement
    if (isModeCreation) {
      snEtablissementArticleCree.renseignerChampRPG(lexique, "INDETB");
    }
    else if (isModeDuplication) {
      snEtablissementArticleDuplique.renseignerChampRPG(lexique, "WIETB3");
      snEtablissementArticleCree.renseignerChampRPG(lexique, "INDETB");
    }
    else {
      snEtablissement.renseignerChampRPG(lexique, "INDETB");
    }
    
    // Renseigner le fournisseur à partir du composant SNFournisseur
    snFournisseur.renseignerChampRPG(lexique, "ARG8N1", "ARG8N2");
    
    // Renseigner le la catégorie des ZP longues à partir du composant SNCategorieZonePersonnalisee
    snCategorieZP.renseignerChampRPG(lexique, "ARG33A");
    
    // Renseigne la famille à parti du composant SNFamille
    snFamille.renseignerChampRPG(lexique, "ARG5A");
    
    // Renseigne la Sous-famille à parti du composant SNSousFamille
    snSousFamille.renseignerChampRPG(lexique, "ARG6A");
    
    // Renseigne la famille à partir de snFamilleCreation visible lors de la création
    if (isModeCreation) {
      snFamilleCreation.renseignerChampRPG(lexique, "WFAM");
    }
    
    // Mémoriser l'onglet sélectionné
    lexique.addVariableGlobale("ongletEnCours_RGVX08FM_A4_C3", pnlFiltre.getSelectedIndex());
    
    if (cbZPLongue1.getSelectedItem() != null && zpLongueSelectionnee1 != null) {
      lexique.HostFieldPutData(zpLongueSelectionnee1.getNomChampsSaisie(), 0, zpLongueSelectionnee1.getValeur());
    }
    if (cbZPLongue2.getSelectedItem() != null && zpLongueSelectionnee2 != null) {
      lexique.HostFieldPutData(zpLongueSelectionnee2.getNomChampsSaisie(), 0, zpLongueSelectionnee2.getValeur());
    }
    if (cbZPLongue3.getSelectedItem() != null && zpLongueSelectionnee3 != null) {
      lexique.HostFieldPutData(zpLongueSelectionnee3.getNomChampsSaisie(), 0, zpLongueSelectionnee3.getValeur());
    }
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.INITIALISER_RECHERCHE)) {
        initialiserRecherche();
      }
      else if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        // Effacement des lignes sélectionnées ou mémorisée dans la liste
        WTP01.getSelectionModel().clearSelection();
        WTP01.effacerMemorisationLigne();
        // Envoi de la touche de fonction qui lance la recherche
        lexique.HostScreenSendKey(this, "F8");
      }
      else if (pSNBouton.isBouton(EnumBouton.CONSULTER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_CREATION)) {
        // Ne plus sélectionner un article dans la liste d'articles
        WTP01.getSelectionModel().clearSelection();
        // Supprimer la ligne mémorisée
        WTP01.effacerMemorisationLigne();
        lexique.HostScreenSendKey(this, "F13");
      }
      else if (pSNBouton.isBouton(BOUTON_RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F15");
      }
      else if (pSNBouton.isBouton(BOUTON_DUPLICATION)) {
        lexique.HostScreenSendKey(this, "F18");
      }
      else if (pSNBouton.isBouton(BOUTON_OPTIONS_ARTICLE)) {
        WTP01.setValeurTop("?");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_DEVERROUILLER_ARTICLE)) {
        WTP01.setValeurTop("D");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_HISTORIQUE_MODIFICATIONS)) {
        WTP01.setValeurTop("H");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_PRIX_CHANTIER)) {
        WTP01.setValeurTop("C");
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(BOUTON_AUTRE_VUE)) {
        lexique.HostScreenSendKey(this, "F24");
      }
      else if (pSNBouton.isBouton(BOUTON_ARTICLES_VERROUILLES)) {
        lexique.HostCursorPut(3, 64);
        lexique.HostScreenSendKey(this, "F4");
      }
      else if (pSNBouton.isBouton(BOUTON_EXPORTER_LISTE)) {
        lexique.HostScreenSendKey(this, "F9");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void miAideEnLigneActionPerformed(ActionEvent e) {
    
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void miChoixPossiblesActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  /**
   * Méthode appelée lors de la sélection d'un article
   * .
   * 
   * @param e : indique qu'une action de la souris s'est produite
   */
  private void WTP01MouseClicked(final MouseEvent e) {
    // Tester si un article a été sélectionné
    if (WTP01.getSelectedRow() == -1 || WTP01.getSelectedColumn() == -1) {
      // Désélectionner un article
      isArticleSelectionne = false;
    }
    else {
      // Sélectionner un article
      isArticleSelectionne = true;
    }
    snBarreBouton.activerBouton(BOUTON_OPTIONS_ARTICLE, (!isModeCreation && !isModeDuplication && isArticleSelectionne));
    snBarreBouton.activerBouton(BOUTON_DEVERROUILLER_ARTICLE, (!isModeCreation && !isModeDuplication && isArticleSelectionne));
    snBarreBouton.activerBouton(BOUTON_HISTORIQUE_MODIFICATIONS, (!isModeCreation && !isModeDuplication && isArticleSelectionne));
    snBarreBouton.activerBouton(BOUTON_PRIX_CHANTIER, (!isModeCreation && !isModeDuplication && isArticleSelectionne));
    snBarreBouton.activerBouton(EnumBouton.CONSULTER, (!isModeCreation && isArticleSelectionne));
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void miOptionsActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miDeverrouillageActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miHistoriqueActionPerformed(ActionEvent e) {
    // lexique.validSelection(_LIST_, _LIST_Top, "H", "Enter");
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_DEBLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_FINLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void miMemorisationCurseurActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void miMemorisationCurseur2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void miAideEnLigne2ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void miPrixChantierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void btnSCAN30ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("SCAN30").trim().equals("N")) {
      lexique.HostFieldPutData("SCAN30", 0, "E");
      btnSCAN30.setIcon(iegale);
    }
    else {
      lexique.HostFieldPutData("SCAN30", 0, "N");
      btnSCAN30.setIcon(idifferent);
    }
  }
  
  private void btnSCAN29ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("SCAN29").trim().equals("N")) {
      lexique.HostFieldPutData("SCAN29", 0, "E");
      btnSCAN29.setIcon(iegale);
    }
    else {
      lexique.HostFieldPutData("SCAN29", 0, "N");
      btnSCAN29.setIcon(idifferent);
    }
  }
  
  private void btnSCAN32ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("SCAN32").trim().equals("N")) {
      lexique.HostFieldPutData("SCAN32", 0, "E");
      btnSCAN32.setIcon(iegale);
    }
    else {
      lexique.HostFieldPutData("SCAN32", 0, "N");
      btnSCAN32.setIcon(idifferent);
    }
  }
  
  private void btnSCAN23ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("SCAN23").trim().equals("N")) {
      lexique.HostFieldPutData("SCAN23", 0, "E");
      btnSCAN23.setIcon(iegale);
    }
    else {
      lexique.HostFieldPutData("SCAN23", 0, "N");
      btnSCAN23.setIcon(idifferent);
    }
  }
  
  private void btnSCAN10ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("SCAN10").trim().equals("N")) {
      lexique.HostFieldPutData("SCAN10", 0, "E");
      btnSCAN10.setIcon(iegale);
    }
    else {
      lexique.HostFieldPutData("SCAN10", 0, "N");
      btnSCAN10.setIcon(idifferent);
    }
  }
  
  private void cbTriListeItemStateChanged(ItemEvent e) {
    modifierTriListe();
  }
  
  private void modifierTriListe() {
    // Effacement des lignes sélectionnées ou mémorisée dans la liste
    WTP01.getSelectionModel().clearSelection();
    WTP01.effacerMemorisationLigne();
    
    // Gestion du tri
    switch (cbTriListe.getSelectedIndex()) {
      case 0:
        lexique.HostFieldPutData("RBC", 0, "2");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 1:
        lexique.HostFieldPutData("RBC", 0, "3");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 2:
        lexique.HostFieldPutData("RBC", 0, "4");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 3:
        lexique.HostFieldPutData("RBC", 0, "5");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 4:
        lexique.HostFieldPutData("RBC", 0, "6");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 5:
        lexique.HostFieldPutData("RBC", 0, "7");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 6:
        lexique.HostFieldPutData("RBC", 0, "26");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 7:
        lexique.HostFieldPutData("RBC", 0, "27");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 8:
        lexique.HostFieldPutData("RBC", 0, "28");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 9:
        lexique.HostFieldPutData("RBC", 0, "9");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 10:
        lexique.HostFieldPutData("RBC", 0, "11");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 11:
        lexique.HostFieldPutData("RBC", 0, "14");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 12:
        lexique.HostFieldPutData("RBC", 0, "21");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 13:
        lexique.HostFieldPutData("RBC", 0, "20");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 14:
        lexique.HostFieldPutData("RBC", 0, "12");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 15:
        lexique.HostFieldPutData("RBC", 0, "22");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      case 16:
        lexique.HostFieldPutData("RBC", 0, "8");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
      
      default:
        lexique.HostFieldPutData("RBC", 0, "2");
        lexique.HostScreenSendKey(this, "ENTER");
        break;
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snFournisseur.setIdEtablissement(snEtablissement.getIdSelection());
      snFournisseur.charger(false);
      snCategorieZP.setIdEtablissement(snEtablissement.getIdSelection());
      snCategorieZP.charger(false);
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Sélection d'un état.
   */
  private void ARG10AItemStateChanged(ItemEvent e) {
    try {
      // Si l'état sélectionné est "Tous", on n'affiche pas le signe égal/différent
      if (ARG10A.getSelectedIndex() == 0) {
        btnSCAN10.setVisible(false);
      }
      else {
        btnSCAN10.setVisible(true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void rafraichirClasse() {
    int index = cbClasse.getSelectedIndex();
    if (index == 0) {
      ARG30A.setText("R");
      lexique.HostFieldPutData("SCAN30", 0, "N");
      btnSCAN30.setIcon(idifferent);
      ARG30A.setEnabled(false);
      btnSCAN30.setEnabled(false);
    }
    else if (index == 1) {
      ARG30A.setText("R");
      lexique.HostFieldPutData("SCAN30", 0, "E");
      btnSCAN30.setIcon(iegale);
      ARG30A.setEnabled(false);
      btnSCAN30.setEnabled(false);
    }
    else {
      ARG30A.setEnabled(true);
      btnSCAN30.setEnabled(true);
    }
  }
  
  private void cbClasseItemStateChanged(ItemEvent e) {
    try {
      rafraichirClasse();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbZPLongue1ItemStateChanged(ItemEvent e) {
    try {
      if (cbZPLongue1.getSelectedItem() instanceof ZPLongue) {
        if (zpLongueSelectionnee1 != null) {
          lexique.HostFieldPutData(zpLongueSelectionnee1.getNomChampsSaisie(), 0, "");
        }
        zpLongueSelectionnee1 = (ZPLongue) cbZPLongue1.getSelectedItem();
      }
      else {
        zpLongueSelectionnee1 = null;
      }
      tfZPLongue1.setEnabled(zpLongueSelectionnee1 != null);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbZPLongue2ItemStateChanged(ItemEvent e) {
    try {
      if (cbZPLongue2.getSelectedItem() instanceof ZPLongue) {
        if (zpLongueSelectionnee2 != null) {
          lexique.HostFieldPutData(zpLongueSelectionnee2.getNomChampsSaisie(), 0, "");
        }
        zpLongueSelectionnee2 = (ZPLongue) cbZPLongue2.getSelectedItem();
      }
      else {
        zpLongueSelectionnee2 = null;
      }
      tfZPLongue2.setEnabled(zpLongueSelectionnee2 != null);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbZPLongue3ItemStateChanged(ItemEvent e) {
    try {
      if (cbZPLongue3.getSelectedItem() instanceof ZPLongue) {
        if (zpLongueSelectionnee3 != null) {
          lexique.HostFieldPutData(zpLongueSelectionnee3.getNomChampsSaisie(), 0, "");
        }
        zpLongueSelectionnee3 = (ZPLongue) cbZPLongue3.getSelectedItem();
      }
      else {
        zpLongueSelectionnee3 = null;
      }
      tfZPLongue3.setEnabled(zpLongueSelectionnee3 != null);
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfZPLongue1CaretUpdate(CaretEvent e) {
    try {
      if (cbZPLongue1.getSelectedItem() != null) {
        zpLongueSelectionnee1.setValeur(tfZPLongue1.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfZPLongue2CaretUpdate(CaretEvent e) {
    try {
      if (cbZPLongue2.getSelectedItem() != null) {
        zpLongueSelectionnee2.setValeur(tfZPLongue2.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void tfZPLongue3CaretUpdate(CaretEvent e) {
    try {
      if (cbZPLongue3.getSelectedItem() != null) {
        zpLongueSelectionnee3.setValeur(tfZPLongue3.getText());
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snCategorieZPValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btModeConsultationActionPerformed(ActionEvent e) {
    try {
      // Lorsque l'on est en mode consultation, le fait de cliquer sur le bouton active le mode modification
      lexique.HostScreenSendKey(this, "F14");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void btModeModificationActionPerformed(ActionEvent e) {
    try {
      // Lorsque l'on est en mode modification, le fait de cliquer sur le bouton active le mode consultation
      lexique.HostScreenSendKey(this, "F15");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Permet de charger les composant snGroupeCreation et snFamilleCreation lors du changement d'etablissement.
   */
  private void snEtablissementArticleCreeValueChanged(SNComposantEvent e) {
    // Sélection le groupe
    snGroupeCreation.setSession(getSession());
    snGroupeCreation.setIdEtablissement(snEtablissementArticleCree.getIdSelection());
    snGroupeCreation.setTousAutorise(true);
    snGroupeCreation.charger(false);
    
    // Sélection le groupe
    snFamilleCreation.setSession(getSession());
    snFamilleCreation.setIdEtablissement(snEtablissementArticleCree.getIdSelection());
    snFamilleCreation.setTousAutorise(true);
    snFamilleCreation.charger(false);
    snFamilleCreation.setSelectionParChampRPG(lexique, "WFAM");
  }
  
  /**
   * Permet de lancer la recherche directement en appuyant sur Entrée sur la date de début de création.
   */
  private void ARG20DActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * Permet de lancer la recherche directement en appuyant sur Entrée sur la date de fin de création.
   */
  private void PLA20DActionPerformed(ActionEvent e) {
    try {
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  @SuppressWarnings("deprecation")
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    pnlNord = new JPanel();
    pnlBpresentation = new SNBandeauTitre();
    pnlContenu = new JPanel();
    pnlCardRecherche = new JPanel();
    pnlFiltre = new JTabbedPane();
    pnlOngletPrincipal = new JPanel();
    pnlPrincipalGauche = new JPanel();
    lbRechercheGenerique = new JLabel();
    ARG76A = new XRiTextField();
    lbCodeArticle = new JLabel();
    ARG2A = new XRiTextField();
    lbMotClassement1 = new JLabel();
    ARG3A = new XRiTextField();
    lbMotClassement2 = new JLabel();
    ARG4A = new XRiTextField();
    pnlPrincipalDroite = new JPanel();
    lbEtablissement = new JLabel();
    snEtablissement = new SNEtablissement();
    lbFamille = new JLabel();
    snFamille = new SNFamille();
    lbSousFamille = new JLabel();
    snSousFamille = new SNSousFamille();
    pnlOngletArticle = new JPanel();
    pnlOngletArticleGauche = new JPanel();
    lbEtat = new JLabel();
    btnSCAN10 = new JButton();
    ARG10A = new XRiComboBox();
    lbModeGestion = new JLabel();
    btnSCAN32 = new JButton();
    ARG32A = new XRiComboBox();
    lbTypeSaisie = new JLabel();
    btnSCAN23 = new JButton();
    ARG23A = new XRiComboBox();
    lbClasse = new JLabel();
    pnClasse = new JPanel();
    cbClasse = new XRiComboBox();
    btnSCAN30 = new JButton();
    ARG30A = new XRiTextField();
    lbSpecial = new JLabel();
    ARG24A = new XRiComboBox();
    pnlOngletArticleDroite = new JPanel();
    lbDateCreation = new JLabel();
    pnlDateCreation = new JPanel();
    ARG20D = new XRiCalendrier();
    PLA20D = new XRiCalendrier();
    lbArticleSubstitution = new JLabel();
    ARG11A = new XRiTextField();
    pnlGencode = new JPanel();
    lbGencode = new JLabel();
    ARG9N = new XRiTextField();
    WTGCD = new XRiCheckBox();
    pnlOngletArticleCentre = new JPanel();
    ARG67A = new XRiCheckBox();
    ARG25A = new XRiCheckBox();
    ARG13A = new XRiCheckBox();
    pnlOngletFournisseur = new JPanel();
    pnlOngletFournisseurGauche = new JPanel();
    pnlNumeroFournisseur = new JPanel();
    lbFournisseur = new JLabel();
    snFournisseur = new SNFournisseur();
    OPT8 = new XRiCheckBox();
    lbClassementFournisseur = new JLabel();
    WFRCLA = new XRiTextField();
    pnlOngletFournisseurDroite = new JPanel();
    OBJ_243_OBJ_243 = new JLabel();
    ARG38N = new XRiTextField();
    OBJ_237_OBJ_237 = new JLabel();
    ARG36A = new XRiTextField();
    OBJ_240_OBJ_240 = new JLabel();
    ARG37A = new XRiTextField();
    pnlOngletAchat = new JPanel();
    pnlOngletAchatGauche = new JPanel();
    lbDisponibilite = new JLabel();
    DISPO = new XRiComboBox();
    lbTypeReapprovisionnement = new JLabel();
    btnSCAN29 = new JButton();
    ARG29A = new XRiComboBox();
    lbGestionLot = new JLabel();
    ARG31A = new XRiComboBox();
    lbTypeArticle2 = new JLabel();
    SCAN73 = new XRiComboBox();
    pnlOngletAchatDroite = new JPanel();
    lbDateDernierAchat = new JLabel();
    pnlDateDernierAchat = new JPanel();
    ARG12D = new XRiCalendrier();
    PLA12D = new XRiCalendrier();
    lbUniteStock = new JLabel();
    ARG26A = new XRiTextField();
    lbFicheStockMagasin = new JLabel();
    ARG71A = new XRiTextField();
    pnlOngletVente = new JPanel();
    pnlOngletVenteGauche = new JPanel();
    lbTarif = new JLabel();
    ARG7A = new XRiTextField();
    lbRattachementCNV = new JLabel();
    ARG21A = new XRiTextField();
    lbRegroupementStatistique = new JLabel();
    ARG14A = new XRiTextField();
    pnlOngletVenteDroite = new JPanel();
    lbDateDerniereVente = new JLabel();
    panel1 = new JPanel();
    ARG22D = new XRiCalendrier();
    PLA22D = new XRiCalendrier();
    lbUniteVente = new JLabel();
    ARG27A = new XRiTextField();
    lbUniteConditionnementVente = new JLabel();
    ARG28A = new XRiTextField();
    pnlOngletZonePersonnalisee = new JPanel();
    pnlOngletZonePersonnaliseeGauche = new JPanel();
    TIDX15 = new JLabel();
    ARG15A = new XRiTextField();
    ARG16A = new XRiTextField();
    TIDX16 = new JLabel();
    TIDX17 = new JLabel();
    ARG17A = new XRiTextField();
    ARG18A = new XRiTextField();
    TIDX18 = new JLabel();
    TIDX19 = new JLabel();
    ARG19A = new XRiTextField();
    pnlOngletZonePersonnaliseeDroite = new JPanel();
    lbCategorieZP = new JLabel();
    snCategorieZP = new SNCategorieZonePersonnalisee();
    cbZPLongue1 = new XRiComboBox();
    tfZPLongue1 = new XRiTextField();
    cbZPLongue2 = new XRiComboBox();
    tfZPLongue2 = new XRiTextField();
    cbZPLongue3 = new XRiComboBox();
    tfZPLongue3 = new XRiTextField();
    pnlCentral = new JPanel();
    pnlConteneurResultatRecherche = new JPanel();
    snLabelTitreTableau = new SNLabelTitre();
    lbTriListe = new JLabel();
    cbTriListe = new XRiComboBox();
    snBarreRecherche = new SNBarreRecherche();
    pnlChoixModeListe = new JPanel();
    btModeConsultation = new JButton();
    btModeModification = new JButton();
    pnlListe = new JPanel();
    SCROLLPANE__LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlBoutonListe = new JPanel();
    OBJ_DEBL = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_FINL = new JButton();
    pnlCardCreation = new JPanel();
    pnlCreationArticleDuplique = new JPanel();
    lbEtablissementArticleDuplique = new JLabel();
    snEtablissementArticleDuplique = new SNEtablissement();
    lbCodeArticleDuplique = new JLabel();
    WIART3 = new XRiTextField();
    pnlCreationArticleCree = new JPanel();
    lbEtablissementArticleCree = new JLabel();
    snEtablissementArticleCree = new SNEtablissement();
    lbCodeArticleCree = new JLabel();
    INDART = new XRiTextField();
    lbGroupeArticle = new SNLabelChamp();
    snGroupeCreation = new SNGroupe();
    lbFamilleArticleCree = new JLabel();
    snFamilleCreation = new SNFamille();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    miChoixPossibles = new JMenuItem();
    miAideEnLigne = new JMenuItem();
    BTD2 = new JPopupMenu();
    miOptions = new JMenuItem();
    miDeverrouillage = new JMenuItem();
    miHistorique = new JMenuItem();
    miPrixChantier = new JMenuItem();
    BTD3 = new JPopupMenu();
    miAideEnLigne2 = new JMenuItem();
    label1 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 600));
    setPreferredSize(new Dimension(1100, 600));
    setBackground(new Color(239, 239, 222));
    setAutoscrolls(true);
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== pnlNord ========
    {
      pnlNord.setBackground(new Color(239, 239, 222));
      pnlNord.setMinimumSize(new Dimension(950, 55));
      pnlNord.setPreferredSize(new Dimension(950, 55));
      pnlNord.setName("pnlNord");
      pnlNord.setLayout(new VerticalLayout());
      
      // ---- pnlBpresentation ----
      pnlBpresentation.setText("Recherche articles");
      pnlBpresentation.setName("pnlBpresentation");
      pnlNord.add(pnlBpresentation);
    }
    add(pnlNord, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setPreferredSize(new Dimension(1000, 400));
      pnlContenu.setBorder(null);
      pnlContenu.setBackground(new Color(239, 239, 222));
      pnlContenu.setMinimumSize(new Dimension(1000, 400));
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new CardLayout());
      
      // ======== pnlCardRecherche ========
      {
        pnlCardRecherche.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlCardRecherche.setBackground(new Color(239, 239, 222));
        pnlCardRecherche.setName("pnlCardRecherche");
        pnlCardRecherche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCardRecherche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCardRecherche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlCardRecherche.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
        ((GridBagLayout) pnlCardRecherche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlFiltre ========
        {
          pnlFiltre.setMinimumSize(new Dimension(450, 230));
          pnlFiltre.setBackground(new Color(239, 239, 222));
          pnlFiltre.setPreferredSize(new Dimension(450, 230));
          pnlFiltre.setBorder(null);
          pnlFiltre.setMaximumSize(new Dimension(450, 230));
          pnlFiltre.setName("pnlFiltre");
          
          // ======== pnlOngletPrincipal ========
          {
            pnlOngletPrincipal.setMinimumSize(new Dimension(840, 250));
            pnlOngletPrincipal.setMaximumSize(new Dimension(840, 5000));
            pnlOngletPrincipal.setBackground(new Color(239, 239, 222));
            pnlOngletPrincipal.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletPrincipal.setPreferredSize(new Dimension(915, 210));
            pnlOngletPrincipal.setName("pnlOngletPrincipal");
            pnlOngletPrincipal.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlPrincipalGauche ========
            {
              pnlPrincipalGauche.setMinimumSize(new Dimension(450, 200));
              pnlPrincipalGauche.setBackground(new Color(239, 239, 222));
              pnlPrincipalGauche.setNextFocusableComponent(pnlOngletAchat);
              pnlPrincipalGauche.setPreferredSize(new Dimension(450, 200));
              pnlPrincipalGauche.setBorder(null);
              pnlPrincipalGauche.setName("pnlPrincipalGauche");
              pnlPrincipalGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWidths = new int[] { 185, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbRechercheGenerique ----
              lbRechercheGenerique.setText("Recherche g\u00e9n\u00e9rique");
              lbRechercheGenerique.setToolTipText("Recherche sur le code article, ses libell\u00e9s et r\u00e9f\u00e9rences");
              lbRechercheGenerique.setPreferredSize(new Dimension(200, 30));
              lbRechercheGenerique.setMinimumSize(new Dimension(200, 30));
              lbRechercheGenerique.setFont(lbRechercheGenerique.getFont().deriveFont(lbRechercheGenerique.getFont().getSize() + 2f));
              lbRechercheGenerique.setHorizontalAlignment(SwingConstants.TRAILING);
              lbRechercheGenerique.setMaximumSize(new Dimension(200, 30));
              lbRechercheGenerique.setName("lbRechercheGenerique");
              pnlPrincipalGauche.add(lbRechercheGenerique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG76A ----
              ARG76A.setComponentPopupMenu(BTD3);
              ARG76A.setMinimumSize(new Dimension(250, 30));
              ARG76A.setPreferredSize(new Dimension(250, 30));
              ARG76A.setToolTipText(
                  "<html>Recherche sur :<br>\n- libell\u00e9 article (milieu)<br>\n- code article (d\u00e9but)<br>\n- mot de classement 1 (d\u00e9but)<br>\n- mot de classement 2 (d\u00e9but)<br>\n- gencode (d\u00e9but)<br>\n- gencode compl\u00e9mentaire (d\u00e9but)<br>\n- raison sociale fournisseur (d\u00e9but)<br>\n- r\u00e9f\u00e9rence fournisseur de la condition d'achats (d\u00e9but)<br>\n- r\u00e9f\u00e9rence constructeur de la condition d'achats (d\u00e9but).<br>\n</html>");
              ARG76A.setFont(ARG76A.getFont().deriveFont(ARG76A.getFont().getSize() + 2f));
              ARG76A.setName("ARG76A");
              pnlPrincipalGauche.add(ARG76A, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbCodeArticle ----
              lbCodeArticle.setText("Code article");
              lbCodeArticle.setComponentPopupMenu(null);
              lbCodeArticle.setMaximumSize(new Dimension(150, 30));
              lbCodeArticle.setMinimumSize(new Dimension(150, 30));
              lbCodeArticle.setPreferredSize(new Dimension(150, 30));
              lbCodeArticle.setFont(lbCodeArticle.getFont().deriveFont(lbCodeArticle.getFont().getSize() + 2f));
              lbCodeArticle.setHorizontalAlignment(SwingConstants.TRAILING);
              lbCodeArticle.setName("lbCodeArticle");
              pnlPrincipalGauche.add(lbCodeArticle, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG2A ----
              ARG2A.setComponentPopupMenu(BTD3);
              ARG2A.setMinimumSize(new Dimension(200, 30));
              ARG2A.setPreferredSize(new Dimension(200, 30));
              ARG2A.setFont(ARG2A.getFont().deriveFont(ARG2A.getFont().getSize() + 2f));
              ARG2A.setName("ARG2A");
              pnlPrincipalGauche.add(ARG2A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMotClassement1 ----
              lbMotClassement1.setText("@TMC1@");
              lbMotClassement1.setComponentPopupMenu(null);
              lbMotClassement1.setMaximumSize(new Dimension(150, 30));
              lbMotClassement1.setMinimumSize(new Dimension(150, 30));
              lbMotClassement1.setPreferredSize(new Dimension(150, 30));
              lbMotClassement1.setFont(lbMotClassement1.getFont().deriveFont(lbMotClassement1.getFont().getSize() + 2f));
              lbMotClassement1.setHorizontalAlignment(SwingConstants.TRAILING);
              lbMotClassement1.setName("lbMotClassement1");
              pnlPrincipalGauche.add(lbMotClassement1, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG3A ----
              ARG3A.setComponentPopupMenu(BTD3);
              ARG3A.setMinimumSize(new Dimension(200, 30));
              ARG3A.setPreferredSize(new Dimension(200, 30));
              ARG3A.setFont(ARG3A.getFont().deriveFont(ARG3A.getFont().getSize() + 2f));
              ARG3A.setName("ARG3A");
              pnlPrincipalGauche.add(ARG3A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbMotClassement2 ----
              lbMotClassement2.setText("@TMC2@");
              lbMotClassement2.setComponentPopupMenu(null);
              lbMotClassement2.setMaximumSize(new Dimension(150, 30));
              lbMotClassement2.setMinimumSize(new Dimension(150, 30));
              lbMotClassement2.setPreferredSize(new Dimension(150, 30));
              lbMotClassement2.setFont(lbMotClassement2.getFont().deriveFont(lbMotClassement2.getFont().getSize() + 2f));
              lbMotClassement2.setHorizontalAlignment(SwingConstants.TRAILING);
              lbMotClassement2.setName("lbMotClassement2");
              pnlPrincipalGauche.add(lbMotClassement2, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG4A ----
              ARG4A.setComponentPopupMenu(BTD3);
              ARG4A.setMinimumSize(new Dimension(200, 30));
              ARG4A.setPreferredSize(new Dimension(200, 30));
              ARG4A.setFont(ARG4A.getFont().deriveFont(ARG4A.getFont().getSize() + 2f));
              ARG4A.setName("ARG4A");
              pnlPrincipalGauche.add(ARG4A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletPrincipal.add(pnlPrincipalGauche);
            
            // ======== pnlPrincipalDroite ========
            {
              pnlPrincipalDroite.setPreferredSize(new Dimension(450, 200));
              pnlPrincipalDroite.setMinimumSize(new Dimension(450, 200));
              pnlPrincipalDroite.setOpaque(false);
              pnlPrincipalDroite.setBorder(null);
              pnlPrincipalDroite.setName("pnlPrincipalDroite");
              pnlPrincipalDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWidths = new int[] { 185, 313, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlPrincipalDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtablissement ----
              lbEtablissement.setText("Etablissement");
              lbEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
              lbEtablissement.setHorizontalAlignment(SwingConstants.RIGHT);
              lbEtablissement.setMinimumSize(new Dimension(150, 30));
              lbEtablissement.setPreferredSize(new Dimension(150, 30));
              lbEtablissement.setMaximumSize(new Dimension(150, 30));
              lbEtablissement.setName("lbEtablissement");
              pnlPrincipalDroite.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snEtablissement ----
              snEtablissement.setMinimumSize(new Dimension(320, 30));
              snEtablissement.setPreferredSize(new Dimension(320, 30));
              snEtablissement.setName("snEtablissement");
              snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snEtablissementValueChanged(e);
                }
              });
              pnlPrincipalDroite.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFamille ----
              lbFamille.setText("Famille");
              lbFamille.setComponentPopupMenu(null);
              lbFamille.setMaximumSize(new Dimension(150, 30));
              lbFamille.setMinimumSize(new Dimension(150, 30));
              lbFamille.setPreferredSize(new Dimension(150, 30));
              lbFamille.setFont(lbFamille.getFont().deriveFont(lbFamille.getFont().getSize() + 2f));
              lbFamille.setHorizontalAlignment(SwingConstants.TRAILING);
              lbFamille.setName("lbFamille");
              pnlPrincipalDroite.add(lbFamille, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snFamille ----
              snFamille.setName("snFamille");
              pnlPrincipalDroite.add(snFamille, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbSousFamille ----
              lbSousFamille.setText("Sous-famille");
              lbSousFamille.setComponentPopupMenu(null);
              lbSousFamille.setMaximumSize(new Dimension(150, 30));
              lbSousFamille.setMinimumSize(new Dimension(150, 30));
              lbSousFamille.setPreferredSize(new Dimension(150, 30));
              lbSousFamille.setFont(lbSousFamille.getFont().deriveFont(lbSousFamille.getFont().getSize() + 2f));
              lbSousFamille.setHorizontalAlignment(SwingConstants.TRAILING);
              lbSousFamille.setName("lbSousFamille");
              pnlPrincipalDroite.add(lbSousFamille, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snSousFamille ----
              snSousFamille.setName("snSousFamille");
              pnlPrincipalDroite.add(snSousFamille, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlOngletPrincipal.add(pnlPrincipalDroite);
          }
          pnlFiltre.addTab("Crit\u00e8res principaux", pnlOngletPrincipal);
          
          // ======== pnlOngletArticle ========
          {
            pnlOngletArticle.setBackground(new Color(239, 239, 222));
            pnlOngletArticle.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletArticle.setPreferredSize(new Dimension(1075, 230));
            pnlOngletArticle.setMinimumSize(new Dimension(1075, 230));
            pnlOngletArticle.setName("pnlOngletArticle");
            pnlOngletArticle.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlOngletArticle.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
            ((GridBagLayout) pnlOngletArticle.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) pnlOngletArticle.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlOngletArticle.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== pnlOngletArticleGauche ========
            {
              pnlOngletArticleGauche.setBorder(null);
              pnlOngletArticleGauche.setOpaque(false);
              pnlOngletArticleGauche.setMinimumSize(new Dimension(400, 215));
              pnlOngletArticleGauche.setPreferredSize(new Dimension(400, 215));
              pnlOngletArticleGauche.setName("pnlOngletArticleGauche");
              pnlOngletArticleGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletArticleGauche.getLayout()).columnWidths = new int[] { 0, 0, 96, 0 };
              ((GridBagLayout) pnlOngletArticleGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletArticleGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletArticleGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbEtat ----
              lbEtat.setText("Etat");
              lbEtat.setPreferredSize(new Dimension(150, 30));
              lbEtat.setMinimumSize(new Dimension(150, 30));
              lbEtat.setMaximumSize(new Dimension(150, 30));
              lbEtat.setFont(lbEtat.getFont().deriveFont(lbEtat.getFont().getSize() + 2f));
              lbEtat.setHorizontalAlignment(SwingConstants.TRAILING);
              lbEtat.setInheritsPopupMenu(false);
              lbEtat.setName("lbEtat");
              pnlOngletArticleGauche.add(lbEtat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- btnSCAN10 ----
              btnSCAN10.setMaximumSize(new Dimension(20, 30));
              btnSCAN10.setMinimumSize(new Dimension(20, 30));
              btnSCAN10.setPreferredSize(new Dimension(20, 30));
              btnSCAN10.setBorder(null);
              btnSCAN10.setBorderPainted(false);
              btnSCAN10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btnSCAN10.setOpaque(true);
              btnSCAN10.setBackground(new Color(239, 239, 222));
              btnSCAN10.setContentAreaFilled(false);
              btnSCAN10.setIcon(UIManager.getIcon("JXDatePicker.arrowIcon"));
              btnSCAN10.setName("btnSCAN10");
              btnSCAN10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btnSCAN10ActionPerformed(e);
                }
              });
              pnlOngletArticleGauche.add(btnSCAN10, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG10A ----
              ARG10A.setModel(new DefaultComboBoxModel(
                  new String[] { "Tous", "D\u00e9sactiv\u00e9", "Epuis\u00e9", "Pr\u00e9-fin de s\u00e9rie", "Fin de s\u00e9rie" }));
              ARG10A.setComponentPopupMenu(BTD);
              ARG10A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG10A.setFont(ARG10A.getFont().deriveFont(ARG10A.getFont().getSize() + 2f));
              ARG10A.setMinimumSize(new Dimension(200, 30));
              ARG10A.setPreferredSize(new Dimension(200, 30));
              ARG10A.setMaximumSize(new Dimension(200, 30));
              ARG10A.setName("ARG10A");
              ARG10A.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  ARG10AItemStateChanged(e);
                }
              });
              pnlOngletArticleGauche.add(ARG10A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbModeGestion ----
              lbModeGestion.setText("Mode de gestion");
              lbModeGestion.setMinimumSize(new Dimension(150, 30));
              lbModeGestion.setPreferredSize(new Dimension(150, 30));
              lbModeGestion.setFont(lbModeGestion.getFont().deriveFont(lbModeGestion.getFont().getSize() + 2f));
              lbModeGestion.setHorizontalAlignment(SwingConstants.TRAILING);
              lbModeGestion.setMaximumSize(new Dimension(150, 30));
              lbModeGestion.setName("lbModeGestion");
              pnlOngletArticleGauche.add(lbModeGestion, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- btnSCAN32 ----
              btnSCAN32.setMaximumSize(new Dimension(20, 30));
              btnSCAN32.setMinimumSize(new Dimension(20, 30));
              btnSCAN32.setPreferredSize(new Dimension(20, 30));
              btnSCAN32.setBorder(null);
              btnSCAN32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btnSCAN32.setIcon(UIManager.getIcon("JXDatePicker.arrowIcon"));
              btnSCAN32.setBorderPainted(false);
              btnSCAN32.setContentAreaFilled(false);
              btnSCAN32.setName("btnSCAN32");
              btnSCAN32.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btnSCAN32ActionPerformed(e);
                }
              });
              pnlOngletArticleGauche.add(btnSCAN32, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG32A ----
              ARG32A.setModel(new DefaultComboBoxModel(
                  new String[] { "N\u00e9goce", "Achet\u00e9, non vendu", "Vendu, non achet\u00e9", "Fabriqu\u00e9" }));
              ARG32A.setComponentPopupMenu(BTD);
              ARG32A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG32A.setFont(ARG32A.getFont().deriveFont(ARG32A.getFont().getSize() + 2f));
              ARG32A.setPreferredSize(new Dimension(200, 30));
              ARG32A.setMinimumSize(new Dimension(200, 30));
              ARG32A.setName("ARG32A");
              pnlOngletArticleGauche.add(ARG32A, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeSaisie ----
              lbTypeSaisie.setText("Type de saisie");
              lbTypeSaisie.setMinimumSize(new Dimension(150, 30));
              lbTypeSaisie.setPreferredSize(new Dimension(150, 30));
              lbTypeSaisie.setFont(lbTypeSaisie.getFont().deriveFont(lbTypeSaisie.getFont().getSize() + 2f));
              lbTypeSaisie.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeSaisie.setMaximumSize(new Dimension(150, 30));
              lbTypeSaisie.setName("lbTypeSaisie");
              pnlOngletArticleGauche.add(lbTypeSaisie, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- btnSCAN23 ----
              btnSCAN23.setMaximumSize(new Dimension(20, 30));
              btnSCAN23.setMinimumSize(new Dimension(20, 30));
              btnSCAN23.setPreferredSize(new Dimension(20, 30));
              btnSCAN23.setBorder(null);
              btnSCAN23.setBorderPainted(false);
              btnSCAN23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btnSCAN23.setOpaque(true);
              btnSCAN23.setBackground(new Color(239, 239, 222));
              btnSCAN23.setContentAreaFilled(false);
              btnSCAN23.setIcon(UIManager.getIcon("JXDatePicker.arrowIcon"));
              btnSCAN23.setName("btnSCAN23");
              btnSCAN23.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btnSCAN23ActionPerformed(e);
                }
              });
              pnlOngletArticleGauche.add(btnSCAN23, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG23A ----
              ARG23A.setModel(new DefaultComboBoxModel(new String[] { "Standard", "Article transport",
                  "Article sur-conditionnement (palette)", "Article kit commentaire", "Par num\u00e9ro de s\u00e9rie", "En valeur",
                  "En double quantit\u00e9", "En longueur", "En surface", "En surface et nombre", "En volume" }));
              ARG23A.setComponentPopupMenu(BTD);
              ARG23A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG23A.setFont(ARG23A.getFont().deriveFont(ARG23A.getFont().getSize() + 2f));
              ARG23A.setPreferredSize(new Dimension(260, 30));
              ARG23A.setMinimumSize(new Dimension(260, 30));
              ARG23A.setMaximumRowCount(15);
              ARG23A.setName("ARG23A");
              pnlOngletArticleGauche.add(ARG23A, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbClasse ----
              lbClasse.setText("Classe");
              lbClasse.setComponentPopupMenu(null);
              lbClasse.setMaximumSize(new Dimension(150, 30));
              lbClasse.setMinimumSize(new Dimension(150, 30));
              lbClasse.setPreferredSize(new Dimension(150, 30));
              lbClasse.setFont(lbClasse.getFont().deriveFont(lbClasse.getFont().getSize() + 2f));
              lbClasse.setHorizontalAlignment(SwingConstants.TRAILING);
              lbClasse.setName("lbClasse");
              pnlOngletArticleGauche.add(lbClasse, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnClasse ========
              {
                pnClasse.setOpaque(false);
                pnClasse.setName("pnClasse");
                pnClasse.setLayout(new GridBagLayout());
                ((GridBagLayout) pnClasse.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
                ((GridBagLayout) pnClasse.getLayout()).rowHeights = new int[] { 0, 0 };
                ((GridBagLayout) pnClasse.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnClasse.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
                
                // ---- cbClasse ----
                cbClasse.setModel(new DefaultComboBoxModel(new String[] { "Gamme", "Hors gamme", "Personnalis\u00e9e" }));
                cbClasse.setComponentPopupMenu(BTD);
                cbClasse.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                cbClasse.setFont(cbClasse.getFont().deriveFont(cbClasse.getFont().getSize() + 2f));
                cbClasse.setMinimumSize(new Dimension(200, 30));
                cbClasse.setPreferredSize(new Dimension(200, 30));
                cbClasse.setMaximumSize(new Dimension(200, 30));
                cbClasse.setName("cbClasse");
                cbClasse.addItemListener(new ItemListener() {
                  @Override
                  public void itemStateChanged(ItemEvent e) {
                    ARG10AItemStateChanged(e);
                    cbClasseItemStateChanged(e);
                  }
                });
                pnClasse.add(cbClasse, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- btnSCAN30 ----
                btnSCAN30.setMaximumSize(new Dimension(20, 30));
                btnSCAN30.setMinimumSize(new Dimension(20, 30));
                btnSCAN30.setPreferredSize(new Dimension(20, 30));
                btnSCAN30.setBorder(null);
                btnSCAN30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                btnSCAN30.setIcon(UIManager.getIcon("JXDatePicker.arrowIcon"));
                btnSCAN30.setBorderPainted(false);
                btnSCAN30.setContentAreaFilled(false);
                btnSCAN30.setName("btnSCAN30");
                btnSCAN30.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    btnSCAN30ActionPerformed(e);
                  }
                });
                pnClasse.add(btnSCAN30, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                
                // ---- ARG30A ----
                ARG30A.setComponentPopupMenu(BTD3);
                ARG30A.setMinimumSize(new Dimension(30, 30));
                ARG30A.setPreferredSize(new Dimension(30, 30));
                ARG30A.setFont(ARG30A.getFont().deriveFont(ARG30A.getFont().getSize() + 2f));
                ARG30A.setName("ARG30A");
                pnClasse.add(ARG30A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlOngletArticleGauche.add(pnClasse, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbSpecial ----
              lbSpecial.setText("Sp\u00e9cial");
              lbSpecial.setComponentPopupMenu(null);
              lbSpecial.setMaximumSize(new Dimension(150, 30));
              lbSpecial.setMinimumSize(new Dimension(150, 30));
              lbSpecial.setPreferredSize(new Dimension(150, 30));
              lbSpecial.setFont(lbSpecial.getFont().deriveFont(lbSpecial.getFont().getSize() + 2f));
              lbSpecial.setHorizontalAlignment(SwingConstants.TRAILING);
              lbSpecial.setName("lbSpecial");
              pnlOngletArticleGauche.add(lbSpecial, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG24A ----
              ARG24A.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Ne pas afficher les sp\u00e9ciaux", "Uniquement les sp\u00e9ciaux" }));
              ARG24A.setComponentPopupMenu(BTD);
              ARG24A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG24A.setFont(ARG24A.getFont().deriveFont(ARG24A.getFont().getSize() + 2f));
              ARG24A.setPreferredSize(new Dimension(260, 30));
              ARG24A.setMinimumSize(new Dimension(260, 30));
              ARG24A.setMaximumRowCount(15);
              ARG24A.setToolTipText(
                  "<html>L'attribut \"Sp\u00e9cial\" se configure dans les informations diverses, onglet \"Divers stock/unit\u00e9\" d'un article.</html>\n");
              ARG24A.setName("ARG24A");
              pnlOngletArticleGauche.add(ARG24A, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletArticle.add(pnlOngletArticleGauche, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlOngletArticleDroite ========
            {
              pnlOngletArticleDroite.setOpaque(false);
              pnlOngletArticleDroite.setName("pnlOngletArticleDroite");
              pnlOngletArticleDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletArticleDroite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletArticleDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletArticleDroite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletArticleDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbDateCreation ----
              lbDateCreation.setText("Date de cr\u00e9ation");
              lbDateCreation.setComponentPopupMenu(null);
              lbDateCreation.setMaximumSize(new Dimension(200, 30));
              lbDateCreation.setMinimumSize(new Dimension(200, 30));
              lbDateCreation.setPreferredSize(new Dimension(200, 30));
              lbDateCreation.setFont(lbDateCreation.getFont().deriveFont(lbDateCreation.getFont().getSize() + 2f));
              lbDateCreation.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDateCreation.setName("lbDateCreation");
              pnlOngletArticleDroite.add(lbDateCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlDateCreation ========
              {
                pnlDateCreation.setOpaque(false);
                pnlDateCreation.setName("pnlDateCreation");
                pnlDateCreation.setLayout(new GridLayout(1, 2, 5, 5));
                
                // ---- ARG20D ----
                ARG20D.setComponentPopupMenu(BTD3);
                ARG20D.setMinimumSize(new Dimension(110, 30));
                ARG20D.setPreferredSize(new Dimension(110, 30));
                ARG20D.setFont(ARG20D.getFont().deriveFont(ARG20D.getFont().getSize() + 2f));
                ARG20D.setName("ARG20D");
                pnlDateCreation.add(ARG20D);
                
                // ---- PLA20D ----
                PLA20D.setComponentPopupMenu(BTD3);
                PLA20D.setMinimumSize(new Dimension(110, 30));
                PLA20D.setPreferredSize(new Dimension(110, 30));
                PLA20D.setFont(PLA20D.getFont().deriveFont(PLA20D.getFont().getSize() + 2f));
                PLA20D.setName("PLA20D");
                pnlDateCreation.add(PLA20D);
              }
              pnlOngletArticleDroite.add(pnlDateCreation, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- lbArticleSubstitution ----
              lbArticleSubstitution.setText("Code article de substitution");
              lbArticleSubstitution.setComponentPopupMenu(null);
              lbArticleSubstitution.setMaximumSize(new Dimension(200, 30));
              lbArticleSubstitution.setMinimumSize(new Dimension(200, 30));
              lbArticleSubstitution.setPreferredSize(new Dimension(200, 30));
              lbArticleSubstitution.setFont(lbArticleSubstitution.getFont().deriveFont(lbArticleSubstitution.getFont().getSize() + 2f));
              lbArticleSubstitution.setHorizontalAlignment(SwingConstants.TRAILING);
              lbArticleSubstitution.setName("lbArticleSubstitution");
              pnlOngletArticleDroite.add(lbArticleSubstitution, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG11A ----
              ARG11A.setComponentPopupMenu(BTD3);
              ARG11A.setMinimumSize(new Dimension(200, 30));
              ARG11A.setPreferredSize(new Dimension(200, 30));
              ARG11A.setFont(ARG11A.getFont().deriveFont(ARG11A.getFont().getSize() + 2f));
              ARG11A.setName("ARG11A");
              pnlOngletArticleDroite.add(ARG11A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlGencode ========
              {
                pnlGencode.setOpaque(false);
                pnlGencode.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
                    new Font("sansserif", Font.BOLD, 12)));
                pnlGencode.setName("pnlGencode");
                pnlGencode.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlGencode.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlGencode.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlGencode.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlGencode.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbGencode ----
                lbGencode.setText("Gencode");
                lbGencode.setComponentPopupMenu(null);
                lbGencode.setMaximumSize(new Dimension(200, 30));
                lbGencode.setMinimumSize(new Dimension(200, 30));
                lbGencode.setPreferredSize(new Dimension(200, 30));
                lbGencode.setFont(lbGencode.getFont().deriveFont(lbGencode.getFont().getSize() + 2f));
                lbGencode.setHorizontalAlignment(SwingConstants.TRAILING);
                lbGencode.setName("lbGencode");
                pnlGencode.add(lbGencode, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                    GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- ARG9N ----
                ARG9N.setComponentPopupMenu(BTD3);
                ARG9N.setMinimumSize(new Dimension(150, 30));
                ARG9N.setPreferredSize(new Dimension(200, 30));
                ARG9N.setFont(ARG9N.getFont().deriveFont(ARG9N.getFont().getSize() + 2f));
                ARG9N.setName("ARG9N");
                pnlGencode.add(ARG9N, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- WTGCD ----
                WTGCD.setText("Gencode secondaire");
                WTGCD.setComponentPopupMenu(null);
                WTGCD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                WTGCD.setMinimumSize(new Dimension(100, 30));
                WTGCD.setPreferredSize(new Dimension(100, 30));
                WTGCD.setFont(WTGCD.getFont().deriveFont(WTGCD.getFont().getSize() + 2f));
                WTGCD.setToolTipText(
                    "<html>Non coch\u00e9 = Recherche sur le gencode principal de la fiche article. <br>\nCoch\u00e9 = Recherche dans les gencodes secondaires de l'article (juqu'\u00e0 5 gencodes secondaires suppl\u00e9mentaires).<br>\n</html>");
                WTGCD.setName("WTGCD");
                pnlGencode.add(WTGCD, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlOngletArticleDroite.add(pnlGencode, new GridBagConstraints(0, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletArticle.add(pnlOngletArticleDroite, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            
            // ======== pnlOngletArticleCentre ========
            {
              pnlOngletArticleCentre.setOpaque(false);
              pnlOngletArticleCentre.setName("pnlOngletArticleCentre");
              pnlOngletArticleCentre.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletArticleCentre.getLayout()).columnWidths = new int[] { 0, 0 };
              ((GridBagLayout) pnlOngletArticleCentre.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletArticleCentre.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletArticleCentre.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- ARG67A ----
              ARG67A.setText("Direct usine");
              ARG67A.setMinimumSize(new Dimension(200, 30));
              ARG67A.setPreferredSize(new Dimension(200, 30));
              ARG67A.setFont(ARG67A.getFont().deriveFont(ARG67A.getFont().getSize() + 2f));
              ARG67A.setName("ARG67A");
              pnlOngletArticleCentre.add(ARG67A, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG25A ----
              ARG25A.setText("Non stock\u00e9");
              ARG25A.setComponentPopupMenu(null);
              ARG25A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG25A.setMinimumSize(new Dimension(150, 30));
              ARG25A.setPreferredSize(new Dimension(150, 30));
              ARG25A.setFont(ARG25A.getFont().deriveFont(ARG25A.getFont().getSize() + 2f));
              ARG25A.setMaximumSize(new Dimension(150, 30));
              ARG25A.setName("ARG25A");
              pnlOngletArticleCentre.add(ARG25A, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG13A ----
              ARG13A.setText("Nomenclature");
              ARG13A.setComponentPopupMenu(null);
              ARG13A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG13A.setMinimumSize(new Dimension(150, 30));
              ARG13A.setPreferredSize(new Dimension(150, 30));
              ARG13A.setFont(ARG13A.getFont().deriveFont(ARG13A.getFont().getSize() + 2f));
              ARG13A.setMaximumSize(new Dimension(150, 30));
              ARG13A.setName("ARG13A");
              pnlOngletArticleCentre.add(ARG13A, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletArticle.add(pnlOngletArticleCentre, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlFiltre.addTab("Crit\u00e8res article", pnlOngletArticle);
          
          // ======== pnlOngletFournisseur ========
          {
            pnlOngletFournisseur.setMinimumSize(new Dimension(450, 200));
            pnlOngletFournisseur.setMaximumSize(new Dimension(484, 5000));
            pnlOngletFournisseur.setBackground(new Color(239, 239, 222));
            pnlOngletFournisseur.setPreferredSize(new Dimension(450, 200));
            pnlOngletFournisseur.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletFournisseur.setName("pnlOngletFournisseur");
            pnlOngletFournisseur.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlOngletFournisseurGauche ========
            {
              pnlOngletFournisseurGauche
                  .setFont(pnlOngletFournisseurGauche.getFont().deriveFont(pnlOngletFournisseurGauche.getFont().getSize() + 2f));
              pnlOngletFournisseurGauche.setOpaque(false);
              pnlOngletFournisseurGauche.setBorder(null);
              pnlOngletFournisseurGauche.setName("pnlOngletFournisseurGauche");
              pnlOngletFournisseurGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletFournisseurGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlOngletFournisseurGauche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlOngletFournisseurGauche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletFournisseurGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              
              // ======== pnlNumeroFournisseur ========
              {
                pnlNumeroFournisseur.setOpaque(false);
                pnlNumeroFournisseur.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
                    new Font("sansserif", Font.BOLD, 12)));
                pnlNumeroFournisseur.setName("pnlNumeroFournisseur");
                pnlNumeroFournisseur.setLayout(new GridBagLayout());
                ((GridBagLayout) pnlNumeroFournisseur.getLayout()).columnWidths = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlNumeroFournisseur.getLayout()).rowHeights = new int[] { 0, 0, 0 };
                ((GridBagLayout) pnlNumeroFournisseur.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                ((GridBagLayout) pnlNumeroFournisseur.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
                
                // ---- lbFournisseur ----
                lbFournisseur.setText("Fournisseur");
                lbFournisseur.setFont(lbFournisseur.getFont().deriveFont(lbFournisseur.getFont().getSize() + 2f));
                lbFournisseur.setMinimumSize(new Dimension(150, 30));
                lbFournisseur.setPreferredSize(new Dimension(150, 30));
                lbFournisseur.setHorizontalAlignment(SwingConstants.TRAILING);
                lbFournisseur.setMaximumSize(new Dimension(150, 30));
                lbFournisseur.setName("lbFournisseur");
                pnlNumeroFournisseur.add(lbFournisseur, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
                
                // ---- snFournisseur ----
                snFournisseur.setName("snFournisseur");
                pnlNumeroFournisseur.add(snFournisseur, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
                
                // ---- OPT8 ----
                OPT8.setText("Uniquement fournisseur principal");
                OPT8.setComponentPopupMenu(null);
                OPT8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                OPT8.setMinimumSize(new Dimension(100, 30));
                OPT8.setPreferredSize(new Dimension(100, 30));
                OPT8.setFont(OPT8.getFont().deriveFont(OPT8.getFont().getSize() + 2f));
                OPT8.setToolTipText(
                    "<html>Non coch\u00e9 = Recherche le num\u00e9ro fournisseur dans les conditions d'achats, donc parmi le fournisseur principal et les fournisseurs secondaires.<br>\nCoch\u00e9 = Recherche dans le num\u00e9ro fournisseur associ\u00e9 \u00e0 la fiche article, donc uniquement le fournisseur principal. <br>\n</html>");
                OPT8.setName("OPT8");
                pnlNumeroFournisseur.add(OPT8, new GridBagConstraints(1, 1, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
              }
              pnlOngletFournisseurGauche.add(pnlNumeroFournisseur, new GridBagConstraints(0, 0, 2, 1, 1.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbClassementFournisseur ----
              lbClassementFournisseur.setText("Classement fournisseur");
              lbClassementFournisseur
                  .setFont(lbClassementFournisseur.getFont().deriveFont(lbClassementFournisseur.getFont().getSize() + 2f));
              lbClassementFournisseur.setMinimumSize(new Dimension(150, 30));
              lbClassementFournisseur.setPreferredSize(new Dimension(150, 30));
              lbClassementFournisseur.setHorizontalAlignment(SwingConstants.TRAILING);
              lbClassementFournisseur.setMaximumSize(new Dimension(150, 30));
              lbClassementFournisseur.setName("lbClassementFournisseur");
              pnlOngletFournisseurGauche.add(lbClassementFournisseur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- WFRCLA ----
              WFRCLA.setComponentPopupMenu(null);
              WFRCLA.setFont(WFRCLA.getFont().deriveFont(WFRCLA.getFont().getSize() + 2f));
              WFRCLA.setMinimumSize(new Dimension(200, 30));
              WFRCLA.setPreferredSize(new Dimension(200, 30));
              WFRCLA.setName("WFRCLA");
              pnlOngletFournisseurGauche.add(WFRCLA, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletFournisseur.add(pnlOngletFournisseurGauche);
            
            // ======== pnlOngletFournisseurDroite ========
            {
              pnlOngletFournisseurDroite
                  .setFont(pnlOngletFournisseurDroite.getFont().deriveFont(pnlOngletFournisseurDroite.getFont().getSize() + 2f));
              pnlOngletFournisseurDroite.setOpaque(false);
              pnlOngletFournisseurDroite.setBorder(null);
              pnlOngletFournisseurDroite.setName("pnlOngletFournisseurDroite");
              pnlOngletFournisseurDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletFournisseurDroite.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlOngletFournisseurDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletFournisseurDroite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletFournisseurDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- OBJ_243_OBJ_243 ----
              OBJ_243_OBJ_243.setText("Gencode article fournisseur");
              OBJ_243_OBJ_243.setFont(OBJ_243_OBJ_243.getFont().deriveFont(OBJ_243_OBJ_243.getFont().getSize() + 2f));
              OBJ_243_OBJ_243.setMinimumSize(new Dimension(200, 30));
              OBJ_243_OBJ_243.setPreferredSize(new Dimension(200, 30));
              OBJ_243_OBJ_243.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_243_OBJ_243.setName("OBJ_243_OBJ_243");
              pnlOngletFournisseurDroite.add(OBJ_243_OBJ_243, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG38N ----
              ARG38N.setComponentPopupMenu(null);
              ARG38N.setFont(ARG38N.getFont().deriveFont(ARG38N.getFont().getSize() + 2f));
              ARG38N.setMinimumSize(new Dimension(200, 30));
              ARG38N.setPreferredSize(new Dimension(200, 30));
              ARG38N.setToolTipText("Gencode de l'article chez le fournisseur, d\u00e9fini dans la condition d'achats.");
              ARG38N.setName("ARG38N");
              pnlOngletFournisseurDroite.add(ARG38N, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_237_OBJ_237 ----
              OBJ_237_OBJ_237.setText("R\u00e9f\u00e9rence article fournisseur");
              OBJ_237_OBJ_237.setFont(OBJ_237_OBJ_237.getFont().deriveFont(OBJ_237_OBJ_237.getFont().getSize() + 2f));
              OBJ_237_OBJ_237.setMinimumSize(new Dimension(200, 30));
              OBJ_237_OBJ_237.setPreferredSize(new Dimension(200, 30));
              OBJ_237_OBJ_237.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_237_OBJ_237.setName("OBJ_237_OBJ_237");
              pnlOngletFournisseurDroite.add(OBJ_237_OBJ_237, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG36A ----
              ARG36A.setComponentPopupMenu(null);
              ARG36A.setFont(ARG36A.getFont().deriveFont(ARG36A.getFont().getSize() + 2f));
              ARG36A.setMinimumSize(new Dimension(200, 30));
              ARG36A.setPreferredSize(new Dimension(200, 30));
              ARG36A.setName("ARG36A");
              pnlOngletFournisseurDroite.add(ARG36A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- OBJ_240_OBJ_240 ----
              OBJ_240_OBJ_240.setText("R\u00e9f\u00e9rence article fabricant");
              OBJ_240_OBJ_240.setFont(OBJ_240_OBJ_240.getFont().deriveFont(OBJ_240_OBJ_240.getFont().getSize() + 2f));
              OBJ_240_OBJ_240.setMinimumSize(new Dimension(200, 30));
              OBJ_240_OBJ_240.setPreferredSize(new Dimension(200, 30));
              OBJ_240_OBJ_240.setHorizontalAlignment(SwingConstants.TRAILING);
              OBJ_240_OBJ_240.setName("OBJ_240_OBJ_240");
              pnlOngletFournisseurDroite.add(OBJ_240_OBJ_240, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG37A ----
              ARG37A.setComponentPopupMenu(null);
              ARG37A.setFont(ARG37A.getFont().deriveFont(ARG37A.getFont().getSize() + 2f));
              ARG37A.setMinimumSize(new Dimension(200, 30));
              ARG37A.setPreferredSize(new Dimension(200, 30));
              ARG37A.setName("ARG37A");
              pnlOngletFournisseurDroite.add(ARG37A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletFournisseur.add(pnlOngletFournisseurDroite);
          }
          pnlFiltre.addTab("Crit\u00e8res fournisseur", pnlOngletFournisseur);
          
          // ======== pnlOngletAchat ========
          {
            pnlOngletAchat.setMinimumSize(new Dimension(450, 250));
            pnlOngletAchat.setBackground(new Color(239, 239, 222));
            pnlOngletAchat.setPreferredSize(new Dimension(450, 250));
            pnlOngletAchat.setMaximumSize(new Dimension(450, 250));
            pnlOngletAchat.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletAchat.setName("pnlOngletAchat");
            pnlOngletAchat.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlOngletAchatGauche ========
            {
              pnlOngletAchatGauche.setMinimumSize(new Dimension(450, 250));
              pnlOngletAchatGauche.setPreferredSize(new Dimension(450, 250));
              pnlOngletAchatGauche.setBorder(null);
              pnlOngletAchatGauche.setOpaque(false);
              pnlOngletAchatGauche.setName("pnlOngletAchatGauche");
              pnlOngletAchatGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletAchatGauche.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletAchatGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletAchatGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletAchatGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbDisponibilite ----
              lbDisponibilite.setText("Disponibilit\u00e9");
              lbDisponibilite.setMinimumSize(new Dimension(200, 30));
              lbDisponibilite.setPreferredSize(new Dimension(200, 30));
              lbDisponibilite.setFont(lbDisponibilite.getFont().deriveFont(lbDisponibilite.getFont().getSize() + 2f));
              lbDisponibilite.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDisponibilite.setName("lbDisponibilite");
              pnlOngletAchatGauche.add(lbDisponibilite, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- DISPO ----
              DISPO.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Disponible", "Non disponible", "En stock", "En rupture de stock" }));
              DISPO.setComponentPopupMenu(BTD);
              DISPO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              DISPO.setFont(DISPO.getFont().deriveFont(DISPO.getFont().getSize() + 2f));
              DISPO.setMinimumSize(new Dimension(300, 30));
              DISPO.setPreferredSize(new Dimension(300, 30));
              DISPO.setToolTipText(
                  "<html>Disponible = Le stock disponible \u00e0 la vente est positif.<br>\nNon disponible = Le stock disponible \u00e0 la vente est n\u00e9gatif ou nul.<br>\nEn stock = Le stock physique est positif.<br>\nEn rupture de stock =  Le stock physique est n\u00e9gatif ou nul.\n</html>");
              DISPO.setName("DISPO");
              pnlOngletAchatGauche.add(DISPO, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeReapprovisionnement ----
              lbTypeReapprovisionnement.setText("Type de r\u00e9approvisionnement");
              lbTypeReapprovisionnement.setMinimumSize(new Dimension(200, 30));
              lbTypeReapprovisionnement.setPreferredSize(new Dimension(200, 30));
              lbTypeReapprovisionnement
                  .setFont(lbTypeReapprovisionnement.getFont().deriveFont(lbTypeReapprovisionnement.getFont().getSize() + 2f));
              lbTypeReapprovisionnement.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeReapprovisionnement.setMaximumSize(new Dimension(200, 30));
              lbTypeReapprovisionnement.setInheritsPopupMenu(false);
              lbTypeReapprovisionnement.setName("lbTypeReapprovisionnement");
              pnlOngletAchatGauche.add(lbTypeReapprovisionnement, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- btnSCAN29 ----
              btnSCAN29.setMaximumSize(new Dimension(20, 30));
              btnSCAN29.setMinimumSize(new Dimension(20, 30));
              btnSCAN29.setPreferredSize(new Dimension(20, 30));
              btnSCAN29.setBorder(null);
              btnSCAN29.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              btnSCAN29.setIcon(UIManager.getIcon("JXDatePicker.arrowIcon"));
              btnSCAN29.setBorderPainted(false);
              btnSCAN29.setContentAreaFilled(false);
              btnSCAN29.setName("btnSCAN29");
              btnSCAN29.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  btnSCAN29ActionPerformed(e);
                }
              });
              pnlOngletAchatGauche.add(btnSCAN29, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG29A ----
              ARG29A.setModel(new DefaultComboBoxModel(new String[] { "Rupture sur stock minimum", "Sur consommation moyenne", "Manuel",
                  "Sur consommation pr\u00e9vue", "Sur consommation moyenne plafonn\u00e9e", "Plafond converte globale",
                  "Compl\u00e9ment stock maximum", "Plafond de converture par magasin", "Non g\u00e9r\u00e9" }));
              ARG29A.setComponentPopupMenu(BTD);
              ARG29A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG29A.setFont(ARG29A.getFont().deriveFont(ARG29A.getFont().getSize() + 2f));
              ARG29A.setMinimumSize(new Dimension(300, 30));
              ARG29A.setPreferredSize(new Dimension(300, 30));
              ARG29A.setMaximumRowCount(15);
              ARG29A.setName("ARG29A");
              pnlOngletAchatGauche.add(ARG29A, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbGestionLot ----
              lbGestionLot.setText("Gestion des lots");
              lbGestionLot.setMaximumSize(new Dimension(200, 30));
              lbGestionLot.setMinimumSize(new Dimension(200, 30));
              lbGestionLot.setPreferredSize(new Dimension(200, 30));
              lbGestionLot.setFont(lbGestionLot.getFont().deriveFont(lbGestionLot.getFont().getSize() + 2f));
              lbGestionLot.setHorizontalAlignment(SwingConstants.TRAILING);
              lbGestionLot.setName("lbGestionLot");
              pnlOngletAchatGauche.add(lbGestionLot, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG31A ----
              ARG31A
                  .setModel(new DefaultComboBoxModel(new String[] { "Tous", "G\u00e9r\u00e9 par lots", "G\u00e9r\u00e9 par sous lots" }));
              ARG31A.setComponentPopupMenu(BTD);
              ARG31A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG31A.setFont(ARG31A.getFont().deriveFont(ARG31A.getFont().getSize() + 2f));
              ARG31A.setMinimumSize(new Dimension(200, 30));
              ARG31A.setPreferredSize(new Dimension(200, 30));
              ARG31A.setMaximumSize(new Dimension(200, 30));
              ARG31A.setName("ARG31A");
              pnlOngletAchatGauche.add(ARG31A, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbTypeArticle2 ----
              lbTypeArticle2.setText("D\u00e9pr\u00e9ciation");
              lbTypeArticle2.setMinimumSize(new Dimension(200, 30));
              lbTypeArticle2.setPreferredSize(new Dimension(200, 30));
              lbTypeArticle2.setFont(lbTypeArticle2.getFont().deriveFont(lbTypeArticle2.getFont().getSize() + 2f));
              lbTypeArticle2.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTypeArticle2.setMaximumSize(new Dimension(200, 30));
              lbTypeArticle2.setName("lbTypeArticle2");
              pnlOngletAchatGauche.add(lbTypeArticle2, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- SCAN73 ----
              SCAN73.setModel(new DefaultComboBoxModel(
                  new String[] { "Tous", "Article non d\u00e9pr\u00e9ci\u00e9", "Article d\u00e9pr\u00e9ci\u00e9" }));
              SCAN73.setComponentPopupMenu(BTD);
              SCAN73.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              SCAN73.setFont(SCAN73.getFont().deriveFont(SCAN73.getFont().getSize() + 2f));
              SCAN73.setPreferredSize(new Dimension(200, 30));
              SCAN73.setMinimumSize(new Dimension(200, 30));
              SCAN73.setMaximumRowCount(15);
              SCAN73.setToolTipText(
                  "<html>D\u00e9pri\u00e9c\u00e9 = S\u00e9lectionner les articles d\u00e9pr\u00e9ci\u00e9s (pourcentage de d\u00e9pr\u00e9ciation > 0).<br>\nNon d\u00e9pri\u00e9c\u00e9 = S\u00e9lectionner les articles non d\u00e9pr\u00e9ci\u00e9s (pourcentage de d\u00e9pr\u00e9ciation = 0).<br>\n</html>\n");
              SCAN73.setName("SCAN73");
              pnlOngletAchatGauche.add(SCAN73, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletAchat.add(pnlOngletAchatGauche);
            
            // ======== pnlOngletAchatDroite ========
            {
              pnlOngletAchatDroite.setMinimumSize(new Dimension(450, 220));
              pnlOngletAchatDroite.setPreferredSize(new Dimension(450, 220));
              pnlOngletAchatDroite.setBorder(null);
              pnlOngletAchatDroite.setOpaque(false);
              pnlOngletAchatDroite.setName("pnlOngletAchatDroite");
              pnlOngletAchatDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletAchatDroite.getLayout()).columnWidths = new int[] { 164, 0, 0 };
              ((GridBagLayout) pnlOngletAchatDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletAchatDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletAchatDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbDateDernierAchat ----
              lbDateDernierAchat.setText("Date de dernier achat");
              lbDateDernierAchat.setComponentPopupMenu(null);
              lbDateDernierAchat.setMaximumSize(new Dimension(200, 30));
              lbDateDernierAchat.setMinimumSize(new Dimension(200, 30));
              lbDateDernierAchat.setPreferredSize(new Dimension(200, 30));
              lbDateDernierAchat.setFont(lbDateDernierAchat.getFont().deriveFont(lbDateDernierAchat.getFont().getSize() + 2f));
              lbDateDernierAchat.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDateDernierAchat.setName("lbDateDernierAchat");
              pnlOngletAchatDroite.add(lbDateDernierAchat, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlDateDernierAchat ========
              {
                pnlDateDernierAchat.setOpaque(false);
                pnlDateDernierAchat.setName("pnlDateDernierAchat");
                pnlDateDernierAchat.setLayout(new GridLayout(1, 2, 5, 5));
                
                // ---- ARG12D ----
                ARG12D.setComponentPopupMenu(BTD3);
                ARG12D.setMinimumSize(new Dimension(110, 30));
                ARG12D.setPreferredSize(new Dimension(110, 30));
                ARG12D.setFont(ARG12D.getFont().deriveFont(ARG12D.getFont().getSize() + 2f));
                ARG12D.setName("ARG12D");
                pnlDateDernierAchat.add(ARG12D);
                
                // ---- PLA12D ----
                PLA12D.setComponentPopupMenu(BTD3);
                PLA12D.setMinimumSize(new Dimension(110, 30));
                PLA12D.setPreferredSize(new Dimension(110, 30));
                PLA12D.setFont(PLA12D.getFont().deriveFont(PLA12D.getFont().getSize() + 2f));
                PLA12D.setName("PLA12D");
                pnlDateDernierAchat.add(PLA12D);
              }
              pnlOngletAchatDroite.add(pnlDateDernierAchat, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbUniteStock ----
              lbUniteStock.setText("Unit\u00e9 de stock (US)");
              lbUniteStock.setComponentPopupMenu(null);
              lbUniteStock.setMinimumSize(new Dimension(200, 30));
              lbUniteStock.setPreferredSize(new Dimension(200, 30));
              lbUniteStock.setFont(lbUniteStock.getFont().deriveFont(lbUniteStock.getFont().getSize() + 2f));
              lbUniteStock.setHorizontalAlignment(SwingConstants.TRAILING);
              lbUniteStock.setMaximumSize(new Dimension(200, 30));
              lbUniteStock.setName("lbUniteStock");
              pnlOngletAchatDroite.add(lbUniteStock, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG26A ----
              ARG26A.setComponentPopupMenu(BTD);
              ARG26A.setMinimumSize(new Dimension(50, 30));
              ARG26A.setPreferredSize(new Dimension(50, 30));
              ARG26A.setFont(ARG26A.getFont().deriveFont(ARG26A.getFont().getSize() + 2f));
              ARG26A.setName("ARG26A");
              pnlOngletAchatDroite.add(ARG26A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbFicheStockMagasin ----
              lbFicheStockMagasin.setText("Magasin (fiche stock pr\u00e9sente)");
              lbFicheStockMagasin.setFont(lbFicheStockMagasin.getFont().deriveFont(lbFicheStockMagasin.getFont().getSize() + 2f));
              lbFicheStockMagasin.setHorizontalAlignment(SwingConstants.TRAILING);
              lbFicheStockMagasin.setMinimumSize(new Dimension(200, 30));
              lbFicheStockMagasin.setMaximumSize(new Dimension(200, 30));
              lbFicheStockMagasin.setPreferredSize(new Dimension(200, 30));
              lbFicheStockMagasin.setName("lbFicheStockMagasin");
              pnlOngletAchatDroite.add(lbFicheStockMagasin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG71A ----
              ARG71A.setComponentPopupMenu(BTD);
              ARG71A.setMinimumSize(new Dimension(50, 30));
              ARG71A.setPreferredSize(new Dimension(50, 30));
              ARG71A.setFont(ARG71A.getFont().deriveFont(ARG71A.getFont().getSize() + 2f));
              ARG71A.setToolTipText(
                  "L'article est s\u00e9lectionn\u00e9 s'il poss\u00e8de une fiche stock pour le magasin saisi, y compris si le stock est \u00e9gal \u00e0 z\u00e9ro dans cette fiche.");
              ARG71A.setName("ARG71A");
              pnlOngletAchatDroite.add(ARG71A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletAchat.add(pnlOngletAchatDroite);
          }
          pnlFiltre.addTab("Crit\u00e8res achats/stock", pnlOngletAchat);
          
          // ======== pnlOngletVente ========
          {
            pnlOngletVente.setOpaque(false);
            pnlOngletVente.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletVente.setName("pnlOngletVente");
            pnlOngletVente.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlOngletVenteGauche ========
            {
              pnlOngletVenteGauche.setOpaque(false);
              pnlOngletVenteGauche.setName("pnlOngletVenteGauche");
              pnlOngletVenteGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletVenteGauche.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletVenteGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletVenteGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletVenteGauche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbTarif ----
              lbTarif.setText("Tarif de ventes");
              lbTarif.setComponentPopupMenu(null);
              lbTarif.setMaximumSize(new Dimension(175, 28));
              lbTarif.setMinimumSize(new Dimension(200, 30));
              lbTarif.setPreferredSize(new Dimension(200, 30));
              lbTarif.setFont(lbTarif.getFont().deriveFont(lbTarif.getFont().getSize() + 2f));
              lbTarif.setHorizontalAlignment(SwingConstants.TRAILING);
              lbTarif.setName("lbTarif");
              pnlOngletVenteGauche.add(lbTarif, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG7A ----
              ARG7A.setComponentPopupMenu(BTD);
              ARG7A.setPreferredSize(new Dimension(100, 30));
              ARG7A.setMinimumSize(new Dimension(100, 30));
              ARG7A.setFont(ARG7A.getFont().deriveFont(ARG7A.getFont().getSize() + 2f));
              ARG7A.setName("ARG7A");
              pnlOngletVenteGauche.add(ARG7A, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbRattachementCNV ----
              lbRattachementCNV.setText("Rattachement CNV");
              lbRattachementCNV.setComponentPopupMenu(null);
              lbRattachementCNV.setMaximumSize(new Dimension(200, 30));
              lbRattachementCNV.setMinimumSize(new Dimension(200, 30));
              lbRattachementCNV.setPreferredSize(new Dimension(200, 30));
              lbRattachementCNV.setFont(lbRattachementCNV.getFont().deriveFont(lbRattachementCNV.getFont().getSize() + 2f));
              lbRattachementCNV.setHorizontalAlignment(SwingConstants.TRAILING);
              lbRattachementCNV.setName("lbRattachementCNV");
              pnlOngletVenteGauche.add(lbRattachementCNV, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG21A ----
              ARG21A.setComponentPopupMenu(BTD3);
              ARG21A.setPreferredSize(new Dimension(150, 30));
              ARG21A.setMinimumSize(new Dimension(150, 30));
              ARG21A.setFont(ARG21A.getFont().deriveFont(ARG21A.getFont().getSize() + 2f));
              ARG21A.setName("ARG21A");
              pnlOngletVenteGauche.add(ARG21A, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbRegroupementStatistique ----
              lbRegroupementStatistique.setText("Regroupement statistique");
              lbRegroupementStatistique.setComponentPopupMenu(null);
              lbRegroupementStatistique.setMaximumSize(new Dimension(200, 30));
              lbRegroupementStatistique.setMinimumSize(new Dimension(200, 30));
              lbRegroupementStatistique.setPreferredSize(new Dimension(200, 30));
              lbRegroupementStatistique
                  .setFont(lbRegroupementStatistique.getFont().deriveFont(lbRegroupementStatistique.getFont().getSize() + 2f));
              lbRegroupementStatistique.setHorizontalAlignment(SwingConstants.TRAILING);
              lbRegroupementStatistique.setName("lbRegroupementStatistique");
              pnlOngletVenteGauche.add(lbRegroupementStatistique, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG14A ----
              ARG14A.setComponentPopupMenu(BTD3);
              ARG14A.setMinimumSize(new Dimension(150, 30));
              ARG14A.setPreferredSize(new Dimension(150, 30));
              ARG14A.setFont(ARG14A.getFont().deriveFont(ARG14A.getFont().getSize() + 2f));
              ARG14A.setName("ARG14A");
              pnlOngletVenteGauche.add(ARG14A, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlOngletVente.add(pnlOngletVenteGauche);
            
            // ======== pnlOngletVenteDroite ========
            {
              pnlOngletVenteDroite.setOpaque(false);
              pnlOngletVenteDroite.setName("pnlOngletVenteDroite");
              pnlOngletVenteDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletVenteDroite.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletVenteDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletVenteDroite.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletVenteDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbDateDerniereVente ----
              lbDateDerniereVente.setText("Date de derni\u00e8re vente");
              lbDateDerniereVente.setComponentPopupMenu(null);
              lbDateDerniereVente.setMaximumSize(new Dimension(175, 28));
              lbDateDerniereVente.setMinimumSize(new Dimension(200, 30));
              lbDateDerniereVente.setPreferredSize(new Dimension(200, 30));
              lbDateDerniereVente.setFont(lbDateDerniereVente.getFont().deriveFont(lbDateDerniereVente.getFont().getSize() + 2f));
              lbDateDerniereVente.setHorizontalAlignment(SwingConstants.TRAILING);
              lbDateDerniereVente.setName("lbDateDerniereVente");
              pnlOngletVenteDroite.add(lbDateDerniereVente, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== panel1 ========
              {
                panel1.setOpaque(false);
                panel1.setName("panel1");
                panel1.setLayout(new GridLayout(1, 2, 5, 5));
                
                // ---- ARG22D ----
                ARG22D.setComponentPopupMenu(BTD3);
                ARG22D.setMinimumSize(new Dimension(110, 30));
                ARG22D.setPreferredSize(new Dimension(110, 30));
                ARG22D.setFont(ARG22D.getFont().deriveFont(ARG22D.getFont().getSize() + 2f));
                ARG22D.setName("ARG22D");
                panel1.add(ARG22D);
                
                // ---- PLA22D ----
                PLA22D.setComponentPopupMenu(BTD3);
                PLA22D.setMinimumSize(new Dimension(110, 30));
                PLA22D.setPreferredSize(new Dimension(110, 30));
                PLA22D.setFont(PLA22D.getFont().deriveFont(PLA22D.getFont().getSize() + 2f));
                PLA22D.setName("PLA22D");
                panel1.add(PLA22D);
              }
              pnlOngletVenteDroite.add(panel1, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbUniteVente ----
              lbUniteVente.setText("Unit\u00e9 de vente (UV)");
              lbUniteVente.setComponentPopupMenu(null);
              lbUniteVente.setPreferredSize(new Dimension(200, 30));
              lbUniteVente.setMinimumSize(new Dimension(200, 30));
              lbUniteVente.setFont(lbUniteVente.getFont().deriveFont(lbUniteVente.getFont().getSize() + 2f));
              lbUniteVente.setHorizontalAlignment(SwingConstants.TRAILING);
              lbUniteVente.setName("lbUniteVente");
              pnlOngletVenteDroite.add(lbUniteVente, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG27A ----
              ARG27A.setComponentPopupMenu(BTD);
              ARG27A.setMinimumSize(new Dimension(50, 30));
              ARG27A.setPreferredSize(new Dimension(50, 30));
              ARG27A.setFont(ARG27A.getFont().deriveFont(ARG27A.getFont().getSize() + 2f));
              ARG27A.setName("ARG27A");
              pnlOngletVenteDroite.add(ARG27A, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- lbUniteConditionnementVente ----
              lbUniteConditionnementVente.setText("Unit\u00e9 conditionnement vente (UCV)");
              lbUniteConditionnementVente.setComponentPopupMenu(null);
              lbUniteConditionnementVente.setPreferredSize(new Dimension(250, 30));
              lbUniteConditionnementVente.setMinimumSize(new Dimension(250, 30));
              lbUniteConditionnementVente
                  .setFont(lbUniteConditionnementVente.getFont().deriveFont(lbUniteConditionnementVente.getFont().getSize() + 2f));
              lbUniteConditionnementVente.setHorizontalAlignment(SwingConstants.TRAILING);
              lbUniteConditionnementVente.setName("lbUniteConditionnementVente");
              pnlOngletVenteDroite.add(lbUniteConditionnementVente, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
                  GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG28A ----
              ARG28A.setComponentPopupMenu(BTD);
              ARG28A.setMinimumSize(new Dimension(50, 30));
              ARG28A.setPreferredSize(new Dimension(50, 30));
              ARG28A.setFont(ARG28A.getFont().deriveFont(ARG28A.getFont().getSize() + 2f));
              ARG28A.setName("ARG28A");
              pnlOngletVenteDroite.add(ARG28A, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            }
            pnlOngletVente.add(pnlOngletVenteDroite);
          }
          pnlFiltre.addTab("Crit\u00e8res ventes", pnlOngletVente);
          
          // ======== pnlOngletZonePersonnalisee ========
          {
            pnlOngletZonePersonnalisee.setBackground(new Color(239, 239, 222));
            pnlOngletZonePersonnalisee.setBorder(new EmptyBorder(5, 5, 5, 5));
            pnlOngletZonePersonnalisee.setName("pnlOngletZonePersonnalisee");
            pnlOngletZonePersonnalisee.setLayout(new GridLayout(1, 2, 5, 5));
            
            // ======== pnlOngletZonePersonnaliseeGauche ========
            {
              pnlOngletZonePersonnaliseeGauche.setBorder(null);
              pnlOngletZonePersonnaliseeGauche.setOpaque(false);
              pnlOngletZonePersonnaliseeGauche.setName("pnlOngletZonePersonnaliseeGauche");
              pnlOngletZonePersonnaliseeGauche.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletZonePersonnaliseeGauche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeGauche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeGauche.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeGauche.getLayout()).rowWeights =
                  new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- TIDX15 ----
              TIDX15.setText("@ZPL01@");
              TIDX15.setComponentPopupMenu(BTD);
              TIDX15.setFont(TIDX15.getFont().deriveFont(TIDX15.getFont().getSize() + 2f));
              TIDX15.setMinimumSize(new Dimension(225, 30));
              TIDX15.setPreferredSize(new Dimension(225, 30));
              TIDX15.setHorizontalAlignment(SwingConstants.TRAILING);
              TIDX15.setMaximumSize(new Dimension(225, 30));
              TIDX15.setName("TIDX15");
              pnlOngletZonePersonnaliseeGauche.add(TIDX15, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG15A ----
              ARG15A.setComponentPopupMenu(BTD);
              ARG15A.setFont(ARG15A.getFont().deriveFont(ARG15A.getFont().getSize() + 2f));
              ARG15A.setPreferredSize(new Dimension(50, 30));
              ARG15A.setMinimumSize(new Dimension(50, 30));
              ARG15A.setName("ARG15A");
              pnlOngletZonePersonnaliseeGauche.add(ARG15A, new GridBagConstraints(1, 0, 1, 1, 1.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG16A ----
              ARG16A.setComponentPopupMenu(BTD);
              ARG16A.setFont(ARG16A.getFont().deriveFont(ARG16A.getFont().getSize() + 2f));
              ARG16A.setPreferredSize(new Dimension(50, 30));
              ARG16A.setMinimumSize(new Dimension(50, 30));
              ARG16A.setName("ARG16A");
              pnlOngletZonePersonnaliseeGauche.add(ARG16A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- TIDX16 ----
              TIDX16.setText("@ZPL02@");
              TIDX16.setComponentPopupMenu(BTD);
              TIDX16.setFont(TIDX16.getFont().deriveFont(TIDX16.getFont().getSize() + 2f));
              TIDX16.setMinimumSize(new Dimension(225, 30));
              TIDX16.setPreferredSize(new Dimension(225, 30));
              TIDX16.setHorizontalAlignment(SwingConstants.TRAILING);
              TIDX16.setMaximumSize(new Dimension(225, 30));
              TIDX16.setName("TIDX16");
              pnlOngletZonePersonnaliseeGauche.add(TIDX16, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- TIDX17 ----
              TIDX17.setText("@ZPL03@");
              TIDX17.setComponentPopupMenu(BTD);
              TIDX17.setFont(TIDX17.getFont().deriveFont(TIDX17.getFont().getSize() + 2f));
              TIDX17.setMinimumSize(new Dimension(225, 30));
              TIDX17.setPreferredSize(new Dimension(225, 30));
              TIDX17.setHorizontalAlignment(SwingConstants.TRAILING);
              TIDX17.setMaximumSize(new Dimension(225, 30));
              TIDX17.setName("TIDX17");
              pnlOngletZonePersonnaliseeGauche.add(TIDX17, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- ARG17A ----
              ARG17A.setComponentPopupMenu(BTD);
              ARG17A.setFont(ARG17A.getFont().deriveFont(ARG17A.getFont().getSize() + 2f));
              ARG17A.setPreferredSize(new Dimension(50, 30));
              ARG17A.setMinimumSize(new Dimension(50, 30));
              ARG17A.setName("ARG17A");
              pnlOngletZonePersonnaliseeGauche.add(ARG17A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- ARG18A ----
              ARG18A.setComponentPopupMenu(BTD);
              ARG18A.setFont(ARG18A.getFont().deriveFont(ARG18A.getFont().getSize() + 2f));
              ARG18A.setPreferredSize(new Dimension(50, 30));
              ARG18A.setMinimumSize(new Dimension(50, 30));
              ARG18A.setName("ARG18A");
              pnlOngletZonePersonnaliseeGauche.add(ARG18A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- TIDX18 ----
              TIDX18.setText("@ZPL04@");
              TIDX18.setComponentPopupMenu(BTD);
              TIDX18.setFont(TIDX18.getFont().deriveFont(TIDX18.getFont().getSize() + 2f));
              TIDX18.setMinimumSize(new Dimension(225, 30));
              TIDX18.setPreferredSize(new Dimension(225, 30));
              TIDX18.setHorizontalAlignment(SwingConstants.TRAILING);
              TIDX18.setMaximumSize(new Dimension(225, 30));
              TIDX18.setName("TIDX18");
              pnlOngletZonePersonnaliseeGauche.add(TIDX18, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- TIDX19 ----
              TIDX19.setText("@ZPL05@");
              TIDX19.setComponentPopupMenu(BTD);
              TIDX19.setFont(TIDX19.getFont().deriveFont(TIDX19.getFont().getSize() + 2f));
              TIDX19.setMinimumSize(new Dimension(225, 30));
              TIDX19.setPreferredSize(new Dimension(225, 30));
              TIDX19.setHorizontalAlignment(SwingConstants.TRAILING);
              TIDX19.setMaximumSize(new Dimension(225, 30));
              TIDX19.setName("TIDX19");
              pnlOngletZonePersonnaliseeGauche.add(TIDX19, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- ARG19A ----
              ARG19A.setComponentPopupMenu(BTD);
              ARG19A.setFont(ARG19A.getFont().deriveFont(ARG19A.getFont().getSize() + 2f));
              ARG19A.setPreferredSize(new Dimension(50, 30));
              ARG19A.setMinimumSize(new Dimension(50, 30));
              ARG19A.setName("ARG19A");
              pnlOngletZonePersonnaliseeGauche.add(ARG19A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletZonePersonnalisee.add(pnlOngletZonePersonnaliseeGauche);
            
            // ======== pnlOngletZonePersonnaliseeDroite ========
            {
              pnlOngletZonePersonnaliseeDroite.setMinimumSize(new Dimension(450, 220));
              pnlOngletZonePersonnaliseeDroite.setPreferredSize(new Dimension(450, 220));
              pnlOngletZonePersonnaliseeDroite.setBorder(null);
              pnlOngletZonePersonnaliseeDroite.setOpaque(false);
              pnlOngletZonePersonnaliseeDroite.setName("pnlOngletZonePersonnaliseeDroite");
              pnlOngletZonePersonnaliseeDroite.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlOngletZonePersonnaliseeDroite.getLayout()).columnWidths = new int[] { 164, 0, 0 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeDroite.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeDroite.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlOngletZonePersonnaliseeDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
              
              // ---- lbCategorieZP ----
              lbCategorieZP.setText("Cat\u00e9gorie de zones personnalis\u00e9es longues");
              lbCategorieZP.setMinimumSize(new Dimension(278, 30));
              lbCategorieZP.setFont(lbCategorieZP.getFont().deriveFont(lbCategorieZP.getFont().getSize() + 2f));
              lbCategorieZP.setHorizontalAlignment(SwingConstants.TRAILING);
              lbCategorieZP.setPreferredSize(new Dimension(278, 30));
              lbCategorieZP.setMaximumSize(new Dimension(278, 30));
              lbCategorieZP.setName("lbCategorieZP");
              pnlOngletZonePersonnaliseeDroite.add(lbCategorieZP, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- snCategorieZP ----
              snCategorieZP.setName("snCategorieZP");
              snCategorieZP.addSNComposantListener(new InterfaceSNComposantListener() {
                @Override
                public void valueChanged(SNComposantEvent e) {
                  snCategorieZPValueChanged(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(snCategorieZP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- cbZPLongue1 ----
              cbZPLongue1.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Disponible", "Non disponible", "En stock", "En rupture de stock" }));
              cbZPLongue1.setComponentPopupMenu(BTD);
              cbZPLongue1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              cbZPLongue1.setFont(cbZPLongue1.getFont().deriveFont(cbZPLongue1.getFont().getSize() + 2f));
              cbZPLongue1.setMinimumSize(new Dimension(300, 30));
              cbZPLongue1.setPreferredSize(new Dimension(300, 30));
              cbZPLongue1.setToolTipText(
                  "<html>Disponible = Le stock disponible \u00e0 la vente est positif.<br>\nNon disponible = Le stock disponible \u00e0 la vente est n\u00e9gatif ou nul.<br>\nEn stock = Le stock physique est positif.<br>\nEn rupture de stock =  Le stock physique est n\u00e9gatif ou nul.\n</html>");
              cbZPLongue1.setName("cbZPLongue1");
              cbZPLongue1.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  cbZPLongue1ItemStateChanged(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(cbZPLongue1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfZPLongue1 ----
              tfZPLongue1.setComponentPopupMenu(null);
              tfZPLongue1.setMinimumSize(new Dimension(200, 30));
              tfZPLongue1.setPreferredSize(new Dimension(200, 30));
              tfZPLongue1.setFont(new Font("sansserif", Font.PLAIN, 14));
              tfZPLongue1.setName("tfZPLongue1");
              tfZPLongue1.addCaretListener(new CaretListener() {
                @Override
                public void caretUpdate(CaretEvent e) {
                  tfZPLongue1CaretUpdate(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(tfZPLongue1, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- cbZPLongue2 ----
              cbZPLongue2.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Disponible", "Non disponible", "En stock", "En rupture de stock" }));
              cbZPLongue2.setComponentPopupMenu(BTD);
              cbZPLongue2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              cbZPLongue2.setFont(cbZPLongue2.getFont().deriveFont(cbZPLongue2.getFont().getSize() + 2f));
              cbZPLongue2.setMinimumSize(new Dimension(300, 30));
              cbZPLongue2.setPreferredSize(new Dimension(300, 30));
              cbZPLongue2.setToolTipText(
                  "<html>Disponible = Le stock disponible \u00e0 la vente est positif.<br>\nNon disponible = Le stock disponible \u00e0 la vente est n\u00e9gatif ou nul.<br>\nEn stock = Le stock physique est positif.<br>\nEn rupture de stock =  Le stock physique est n\u00e9gatif ou nul.\n</html>");
              cbZPLongue2.setName("cbZPLongue2");
              cbZPLongue2.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  cbZPLongue2ItemStateChanged(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(cbZPLongue2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ---- tfZPLongue2 ----
              tfZPLongue2.setComponentPopupMenu(null);
              tfZPLongue2.setMinimumSize(new Dimension(200, 30));
              tfZPLongue2.setPreferredSize(new Dimension(200, 30));
              tfZPLongue2.setFont(new Font("sansserif", Font.PLAIN, 14));
              tfZPLongue2.setName("tfZPLongue2");
              tfZPLongue2.addCaretListener(new CaretListener() {
                @Override
                public void caretUpdate(CaretEvent e) {
                  tfZPLongue2CaretUpdate(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(tfZPLongue2, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
              
              // ---- cbZPLongue3 ----
              cbZPLongue3.setModel(
                  new DefaultComboBoxModel(new String[] { "Tous", "Disponible", "Non disponible", "En stock", "En rupture de stock" }));
              cbZPLongue3.setComponentPopupMenu(BTD);
              cbZPLongue3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              cbZPLongue3.setFont(cbZPLongue3.getFont().deriveFont(cbZPLongue3.getFont().getSize() + 2f));
              cbZPLongue3.setMinimumSize(new Dimension(300, 30));
              cbZPLongue3.setPreferredSize(new Dimension(300, 30));
              cbZPLongue3.setToolTipText(
                  "<html>Disponible = Le stock disponible \u00e0 la vente est positif.<br>\nNon disponible = Le stock disponible \u00e0 la vente est n\u00e9gatif ou nul.<br>\nEn stock = Le stock physique est positif.<br>\nEn rupture de stock =  Le stock physique est n\u00e9gatif ou nul.\n</html>");
              cbZPLongue3.setName("cbZPLongue3");
              cbZPLongue3.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                  cbZPLongue3ItemStateChanged(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(cbZPLongue3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfZPLongue3 ----
              tfZPLongue3.setComponentPopupMenu(null);
              tfZPLongue3.setMinimumSize(new Dimension(200, 30));
              tfZPLongue3.setPreferredSize(new Dimension(200, 30));
              tfZPLongue3.setFont(new Font("sansserif", Font.PLAIN, 14));
              tfZPLongue3.setName("tfZPLongue3");
              tfZPLongue3.addCaretListener(new CaretListener() {
                @Override
                public void caretUpdate(CaretEvent e) {
                  tfZPLongue3CaretUpdate(e);
                }
              });
              pnlOngletZonePersonnaliseeDroite.add(tfZPLongue3, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                  GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlOngletZonePersonnalisee.add(pnlOngletZonePersonnaliseeDroite);
          }
          pnlFiltre.addTab("Zones personnalis\u00e9es", pnlOngletZonePersonnalisee);
          
          pnlFiltre.setSelectedIndex(0);
        }
        pnlCardRecherche.add(pnlFiltre, new GridBagConstraints(0, 0, 1, 1, 1.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCentral ========
        {
          pnlCentral.setBackground(new Color(239, 239, 222));
          pnlCentral.setName("pnlCentral");
          pnlCentral.setLayout(new GridLayout(1, 2, 5, 5));
          
          // ======== pnlConteneurResultatRecherche ========
          {
            pnlConteneurResultatRecherche.setBackground(new Color(239, 239, 222));
            pnlConteneurResultatRecherche.setPreferredSize(new Dimension(365, 65));
            pnlConteneurResultatRecherche.setMinimumSize(new Dimension(365, 65));
            pnlConteneurResultatRecherche.setName("pnlConteneurResultatRecherche");
            pnlConteneurResultatRecherche.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlConteneurResultatRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlConteneurResultatRecherche.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlConteneurResultatRecherche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlConteneurResultatRecherche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- snLabelTitreTableau ----
            snLabelTitreTableau.setText("Liste des articles (@NBRERL@)");
            snLabelTitreTableau.setName("snLabelTitreTableau");
            pnlConteneurResultatRecherche.add(snLabelTitreTableau, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbTriListe ----
            lbTriListe.setText("Trier par");
            lbTriListe.setMaximumSize(new Dimension(60, 30));
            lbTriListe.setMinimumSize(new Dimension(60, 30));
            lbTriListe.setPreferredSize(new Dimension(60, 30));
            lbTriListe.setFont(lbTriListe.getFont().deriveFont(lbTriListe.getFont().getSize() + 2f));
            lbTriListe.setHorizontalTextPosition(SwingConstants.LEFT);
            lbTriListe.setName("lbTriListe");
            pnlConteneurResultatRecherche.add(lbTriListe, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- cbTriListe ----
            cbTriListe.setPreferredSize(new Dimension(300, 30));
            cbTriListe.setMinimumSize(new Dimension(300, 30));
            cbTriListe.setModel(new DefaultComboBoxModel(
                new String[] { "Code article", "Mot de classement 1", "Mot de classement 2", "Groupe/famille", "Sous-famille",
                    "R\u00e9f\u00e9rence tarif", "Unit\u00e9 de stock", "Unit\u00e9 de vente", "Unit\u00e9 de conditionnement",
                    "Code barres", "Article de substitution", "Regroupement statistique", "Rattachement condition de vente",
                    "Date de cr\u00e9ation", "Date de dernier achat", "Date de derni\u00e8re vente", "Num\u00e9ro de fournisseur" }));
            cbTriListe.setFont(cbTriListe.getFont().deriveFont(cbTriListe.getFont().getSize() + 2f));
            cbTriListe.setMaximumSize(new Dimension(300, 30));
            cbTriListe.setName("cbTriListe");
            cbTriListe.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTriListeItemStateChanged(e);
              }
            });
            pnlConteneurResultatRecherche.add(cbTriListe, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlCentral.add(pnlConteneurResultatRecherche);
          
          // ---- snBarreRecherche ----
          snBarreRecherche.setName("snBarreRecherche");
          pnlCentral.add(snBarreRecherche);
        }
        pnlCardRecherche.add(pnlCentral, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlChoixModeListe ========
        {
          pnlChoixModeListe.setOpaque(false);
          pnlChoixModeListe.setMinimumSize(new Dimension(24, 24));
          pnlChoixModeListe.setPreferredSize(new Dimension(24, 24));
          pnlChoixModeListe.setName("pnlChoixModeListe");
          pnlChoixModeListe.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlChoixModeListe.getLayout()).columnWidths = new int[] { 0, 0, 0, 35, 0 };
          ((GridBagLayout) pnlChoixModeListe.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlChoixModeListe.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlChoixModeListe.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ---- btModeConsultation ----
          btModeConsultation.setMaximumSize(new Dimension(24, 24));
          btModeConsultation.setMinimumSize(new Dimension(24, 24));
          btModeConsultation.setPreferredSize(new Dimension(24, 24));
          btModeConsultation.setIcon(new ImageIcon(getClass().getResource("/images/modeConsultation-24.png")));
          btModeConsultation.setSelectedIcon(null);
          btModeConsultation.setToolTipText("Cliquer pour activer le mode modification");
          btModeConsultation.setBorderPainted(false);
          btModeConsultation.setFocusPainted(false);
          btModeConsultation.setContentAreaFilled(false);
          btModeConsultation.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btModeConsultation.setName("btModeConsultation");
          btModeConsultation.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btModeConsultationActionPerformed(e);
            }
          });
          pnlChoixModeListe.add(btModeConsultation, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- btModeModification ----
          btModeModification.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          btModeModification.setSelectedIcon(null);
          btModeModification.setIcon(new ImageIcon(getClass().getResource("/images/modeModification-21.png")));
          btModeModification.setContentAreaFilled(false);
          btModeModification.setFocusPainted(false);
          btModeModification.setBorderPainted(false);
          btModeModification.setPreferredSize(new Dimension(24, 24));
          btModeModification.setMinimumSize(new Dimension(26, 26));
          btModeModification.setToolTipText("Cliquer pour activer le mode consultation");
          btModeModification.setName("btModeModification");
          btModeModification.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              btModeModificationActionPerformed(e);
            }
          });
          pnlChoixModeListe.add(btModeModification, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCardRecherche.add(pnlChoixModeListe, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlListe ========
        {
          pnlListe.setMinimumSize(new Dimension(1020, 300));
          pnlListe.setBackground(new Color(239, 239, 222));
          pnlListe.setBorder(null);
          pnlListe.setPreferredSize(new Dimension(1020, 300));
          pnlListe.setAutoscrolls(true);
          pnlListe.setName("pnlListe");
          pnlListe.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlListe.getLayout()).columnWidths = new int[] { 224, 0, 0 };
          ((GridBagLayout) pnlListe.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlListe.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlListe.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== SCROLLPANE__LIST ========
          {
            SCROLLPANE__LIST.setMinimumSize(new Dimension(500, 268));
            SCROLLPANE__LIST.setPreferredSize(new Dimension(500, 268));
            SCROLLPANE__LIST.setName("SCROLLPANE__LIST");
            
            // ---- WTP01 ----
            WTP01.setPreferredScrollableViewportSize(new Dimension(450, 300));
            WTP01.setComponentPopupMenu(BTD2);
            WTP01.setMinimumSize(new Dimension(1050, 238));
            WTP01.setPreferredSize(new Dimension(1050, 238));
            WTP01.setMaximumSize(new Dimension(1050, 238));
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE__LIST.setViewportView(WTP01);
          }
          pnlListe.add(SCROLLPANE__LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 1.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlBoutonListe ========
          {
            pnlBoutonListe.setOpaque(false);
            pnlBoutonListe.setPreferredSize(new Dimension(28, 268));
            pnlBoutonListe.setMinimumSize(new Dimension(28, 268));
            pnlBoutonListe.setName("pnlBoutonListe");
            pnlBoutonListe.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBoutonListe.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlBoutonListe.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlBoutonListe.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBoutonListe.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- OBJ_DEBL ----
            OBJ_DEBL.setMaximumSize(new Dimension(28, 30));
            OBJ_DEBL.setMinimumSize(new Dimension(28, 30));
            OBJ_DEBL.setPreferredSize(new Dimension(28, 30));
            OBJ_DEBL.setName("OBJ_DEBL");
            OBJ_DEBL.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_DEBLActionPerformed(e);
              }
            });
            pnlBoutonListe.add(OBJ_DEBL, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setMaximumSize(new Dimension(28, 50));
            BT_PGUP.setMinimumSize(new Dimension(28, 50));
            BT_PGUP.setPreferredSize(new Dimension(28, 50));
            BT_PGUP.setName("BT_PGUP");
            pnlBoutonListe.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.5, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setMaximumSize(new Dimension(28, 50));
            BT_PGDOWN.setMinimumSize(new Dimension(28, 50));
            BT_PGDOWN.setPreferredSize(new Dimension(28, 50));
            BT_PGDOWN.setName("BT_PGDOWN");
            pnlBoutonListe.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.5, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- OBJ_FINL ----
            OBJ_FINL.setText("");
            OBJ_FINL.setMaximumSize(new Dimension(28, 30));
            OBJ_FINL.setMinimumSize(new Dimension(28, 30));
            OBJ_FINL.setPreferredSize(new Dimension(28, 30));
            OBJ_FINL.setName("OBJ_FINL");
            OBJ_FINL.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_FINLActionPerformed(e);
              }
            });
            pnlBoutonListe.add(OBJ_FINL, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlListe.add(pnlBoutonListe, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
              GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCardRecherche.add(pnlListe, new GridBagConstraints(0, 3, 1, 1, 1.0, 0.0, GridBagConstraints.NORTH,
            GridBagConstraints.HORIZONTAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCardRecherche, "cardRecherche");
      
      // ======== pnlCardCreation ========
      {
        pnlCardCreation.setBackground(new Color(239, 239, 222));
        pnlCardCreation.setBorder(new EmptyBorder(10, 10, 10, 10));
        pnlCardCreation.setName("pnlCardCreation");
        pnlCardCreation.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlCardCreation.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlCardCreation.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlCardCreation.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlCardCreation.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlCreationArticleDuplique ========
        {
          pnlCreationArticleDuplique.setOpaque(false);
          pnlCreationArticleDuplique.setBorder(new TitledBorder(null, "Article dupliqu\u00e9", TitledBorder.LEADING,
              TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 12)));
          pnlCreationArticleDuplique.setName("pnlCreationArticleDuplique");
          pnlCreationArticleDuplique.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCreationArticleDuplique.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCreationArticleDuplique.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCreationArticleDuplique.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCreationArticleDuplique.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissementArticleDuplique ----
          lbEtablissementArticleDuplique.setText("Etablissement article dupliqu\u00e9");
          lbEtablissementArticleDuplique
              .setFont(lbEtablissementArticleDuplique.getFont().deriveFont(lbEtablissementArticleDuplique.getFont().getSize() + 2f));
          lbEtablissementArticleDuplique.setPreferredSize(new Dimension(200, 30));
          lbEtablissementArticleDuplique.setMinimumSize(new Dimension(200, 30));
          lbEtablissementArticleDuplique.setHorizontalAlignment(SwingConstants.TRAILING);
          lbEtablissementArticleDuplique.setMaximumSize(new Dimension(200, 30));
          lbEtablissementArticleDuplique.setName("lbEtablissementArticleDuplique");
          pnlCreationArticleDuplique.add(lbEtablissementArticleDuplique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissementArticleDuplique ----
          snEtablissementArticleDuplique.setPreferredSize(new Dimension(420, 30));
          snEtablissementArticleDuplique.setMaximumSize(new Dimension(500, 30));
          snEtablissementArticleDuplique.setName("snEtablissementArticleDuplique");
          pnlCreationArticleDuplique.add(snEtablissementArticleDuplique, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodeArticleDuplique ----
          lbCodeArticleDuplique.setText("Code article dupliqu\u00e9");
          lbCodeArticleDuplique.setFont(lbCodeArticleDuplique.getFont().deriveFont(lbCodeArticleDuplique.getFont().getSize() + 2f));
          lbCodeArticleDuplique.setPreferredSize(new Dimension(200, 30));
          lbCodeArticleDuplique.setMinimumSize(new Dimension(200, 30));
          lbCodeArticleDuplique.setHorizontalAlignment(SwingConstants.TRAILING);
          lbCodeArticleDuplique.setMaximumSize(new Dimension(200, 30));
          lbCodeArticleDuplique.setName("lbCodeArticleDuplique");
          pnlCreationArticleDuplique.add(lbCodeArticleDuplique, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WIART3 ----
          WIART3.setComponentPopupMenu(BTD);
          WIART3.setMinimumSize(new Dimension(250, 30));
          WIART3.setPreferredSize(new Dimension(250, 30));
          WIART3.setFont(WIART3.getFont().deriveFont(WIART3.getFont().getSize() + 2f));
          WIART3.setName("WIART3");
          pnlCreationArticleDuplique.add(WIART3, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCardCreation.add(pnlCreationArticleDuplique, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlCreationArticleCree ========
        {
          pnlCreationArticleCree.setOpaque(false);
          pnlCreationArticleCree.setBorder(new TitledBorder(null, "Article cr\u00e9\u00e9", TitledBorder.LEADING,
              TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 12)));
          pnlCreationArticleCree.setName("pnlCreationArticleCree");
          pnlCreationArticleCree.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlCreationArticleCree.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlCreationArticleCree.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlCreationArticleCree.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlCreationArticleCree.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- lbEtablissementArticleCree ----
          lbEtablissementArticleCree.setText("Etablissement nouvel article");
          lbEtablissementArticleCree
              .setFont(lbEtablissementArticleCree.getFont().deriveFont(lbEtablissementArticleCree.getFont().getSize() + 2f));
          lbEtablissementArticleCree.setPreferredSize(new Dimension(200, 30));
          lbEtablissementArticleCree.setMinimumSize(new Dimension(200, 30));
          lbEtablissementArticleCree.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbEtablissementArticleCree.setHorizontalAlignment(SwingConstants.TRAILING);
          lbEtablissementArticleCree.setName("lbEtablissementArticleCree");
          pnlCreationArticleCree.add(lbEtablissementArticleCree, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snEtablissementArticleCree ----
          snEtablissementArticleCree.setPreferredSize(new Dimension(420, 30));
          snEtablissementArticleCree.setMaximumSize(new Dimension(500, 30));
          snEtablissementArticleCree.setName("snEtablissementArticleCree");
          snEtablissementArticleCree.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementArticleCreeValueChanged(e);
            }
          });
          pnlCreationArticleCree.add(snEtablissementArticleCree, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbCodeArticleCree ----
          lbCodeArticleCree.setText("Code nouvel article");
          lbCodeArticleCree.setFont(lbCodeArticleCree.getFont().deriveFont(lbCodeArticleCree.getFont().getSize() + 2f));
          lbCodeArticleCree.setPreferredSize(new Dimension(200, 30));
          lbCodeArticleCree.setMinimumSize(new Dimension(200, 30));
          lbCodeArticleCree.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbCodeArticleCree.setHorizontalAlignment(SwingConstants.TRAILING);
          lbCodeArticleCree.setName("lbCodeArticleCree");
          pnlCreationArticleCree.add(lbCodeArticleCree, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- INDART ----
          INDART.setComponentPopupMenu(BTD);
          INDART.setFont(INDART.getFont().deriveFont(INDART.getFont().getSize() + 2f));
          INDART.setPreferredSize(new Dimension(250, 30));
          INDART.setMinimumSize(new Dimension(250, 30));
          INDART.setName("INDART");
          pnlCreationArticleCree.add(INDART, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbGroupeArticle ----
          lbGroupeArticle.setText("Groupe nouvel article");
          lbGroupeArticle.setName("lbGroupeArticle");
          pnlCreationArticleCree.add(lbGroupeArticle, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- snGroupeCreation ----
          snGroupeCreation.setMaximumSize(new Dimension(500, 30));
          snGroupeCreation.setPreferredSize(new Dimension(420, 30));
          snGroupeCreation.setName("snGroupeCreation");
          pnlCreationArticleCree.add(snGroupeCreation, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbFamilleArticleCree ----
          lbFamilleArticleCree.setText("Famille nouvel article");
          lbFamilleArticleCree.setFont(lbFamilleArticleCree.getFont().deriveFont(lbFamilleArticleCree.getFont().getSize() + 2f));
          lbFamilleArticleCree.setPreferredSize(new Dimension(200, 30));
          lbFamilleArticleCree.setMinimumSize(new Dimension(200, 30));
          lbFamilleArticleCree.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbFamilleArticleCree.setHorizontalAlignment(SwingConstants.TRAILING);
          lbFamilleArticleCree.setName("lbFamilleArticleCree");
          pnlCreationArticleCree.add(lbFamilleArticleCree, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.EAST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- snFamilleCreation ----
          snFamilleCreation.setPreferredSize(new Dimension(420, 30));
          snFamilleCreation.setMaximumSize(new Dimension(500, 30));
          snFamilleCreation.setName("snFamilleCreation");
          pnlCreationArticleCree.add(snFamilleCreation, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlCardCreation.add(pnlCreationArticleCree, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlCardCreation, "cardCreation");
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- miChoixPossibles ----
      miChoixPossibles.setText("Choix possibles");
      miChoixPossibles.setName("miChoixPossibles");
      miChoixPossibles.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miChoixPossiblesActionPerformed(e);
        }
      });
      BTD.add(miChoixPossibles);
      
      // ---- miAideEnLigne ----
      miAideEnLigne.setText("Aide en ligne");
      miAideEnLigne.setName("miAideEnLigne");
      miAideEnLigne.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigneActionPerformed(e);
        }
      });
      BTD.add(miAideEnLigne);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- miOptions ----
      miOptions.setText("Voir options article");
      miOptions.setName("miOptions");
      miOptions.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miOptionsActionPerformed(e);
        }
      });
      BTD2.add(miOptions);
      
      // ---- miDeverrouillage ----
      miDeverrouillage.setText("D\u00e9verrouiller l'article");
      miDeverrouillage.setName("miDeverrouillage");
      miDeverrouillage.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miDeverrouillageActionPerformed(e);
        }
      });
      BTD2.add(miDeverrouillage);
      
      // ---- miHistorique ----
      miHistorique.setText("Voir historique modifications");
      miHistorique.setName("miHistorique");
      miHistorique.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miHistoriqueActionPerformed(e);
        }
      });
      BTD2.add(miHistorique);
      
      // ---- miPrixChantier ----
      miPrixChantier.setText("Voir prix chantier");
      miPrixChantier.setToolTipText("Prix chantier client");
      miPrixChantier.setName("miPrixChantier");
      miPrixChantier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miPrixChantierActionPerformed(e);
        }
      });
      BTD2.add(miPrixChantier);
    }
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- miAideEnLigne2 ----
      miAideEnLigne2.setText("Aide en ligne");
      miAideEnLigne2.setName("miAideEnLigne2");
      miAideEnLigne2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAideEnLigne2ActionPerformed(e);
        }
      });
      BTD3.add(miAideEnLigne2);
    }
    
    // ---- label1 ----
    label1.setText("pnlContenu est un CardLayout");
    label1.setBackground(new Color(255, 153, 153));
    label1.setOpaque(true);
    label1.setName("label1");
    // //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel pnlNord;
  private SNBandeauTitre pnlBpresentation;
  private JPanel pnlContenu;
  private JPanel pnlCardRecherche;
  private JTabbedPane pnlFiltre;
  private JPanel pnlOngletPrincipal;
  private JPanel pnlPrincipalGauche;
  private JLabel lbRechercheGenerique;
  private XRiTextField ARG76A;
  private JLabel lbCodeArticle;
  private XRiTextField ARG2A;
  private JLabel lbMotClassement1;
  private XRiTextField ARG3A;
  private JLabel lbMotClassement2;
  private XRiTextField ARG4A;
  private JPanel pnlPrincipalDroite;
  private JLabel lbEtablissement;
  private SNEtablissement snEtablissement;
  private JLabel lbFamille;
  private SNFamille snFamille;
  private JLabel lbSousFamille;
  private SNSousFamille snSousFamille;
  private JPanel pnlOngletArticle;
  private JPanel pnlOngletArticleGauche;
  private JLabel lbEtat;
  private JButton btnSCAN10;
  private XRiComboBox ARG10A;
  private JLabel lbModeGestion;
  private JButton btnSCAN32;
  private XRiComboBox ARG32A;
  private JLabel lbTypeSaisie;
  private JButton btnSCAN23;
  private XRiComboBox ARG23A;
  private JLabel lbClasse;
  private JPanel pnClasse;
  private XRiComboBox cbClasse;
  private JButton btnSCAN30;
  private XRiTextField ARG30A;
  private JLabel lbSpecial;
  private XRiComboBox ARG24A;
  private JPanel pnlOngletArticleDroite;
  private JLabel lbDateCreation;
  private JPanel pnlDateCreation;
  private XRiCalendrier ARG20D;
  private XRiCalendrier PLA20D;
  private JLabel lbArticleSubstitution;
  private XRiTextField ARG11A;
  private JPanel pnlGencode;
  private JLabel lbGencode;
  private XRiTextField ARG9N;
  private XRiCheckBox WTGCD;
  private JPanel pnlOngletArticleCentre;
  private XRiCheckBox ARG67A;
  private XRiCheckBox ARG25A;
  private XRiCheckBox ARG13A;
  private JPanel pnlOngletFournisseur;
  private JPanel pnlOngletFournisseurGauche;
  private JPanel pnlNumeroFournisseur;
  private JLabel lbFournisseur;
  private SNFournisseur snFournisseur;
  private XRiCheckBox OPT8;
  private JLabel lbClassementFournisseur;
  private XRiTextField WFRCLA;
  private JPanel pnlOngletFournisseurDroite;
  private JLabel OBJ_243_OBJ_243;
  private XRiTextField ARG38N;
  private JLabel OBJ_237_OBJ_237;
  private XRiTextField ARG36A;
  private JLabel OBJ_240_OBJ_240;
  private XRiTextField ARG37A;
  private JPanel pnlOngletAchat;
  private JPanel pnlOngletAchatGauche;
  private JLabel lbDisponibilite;
  private XRiComboBox DISPO;
  private JLabel lbTypeReapprovisionnement;
  private JButton btnSCAN29;
  private XRiComboBox ARG29A;
  private JLabel lbGestionLot;
  private XRiComboBox ARG31A;
  private JLabel lbTypeArticle2;
  private XRiComboBox SCAN73;
  private JPanel pnlOngletAchatDroite;
  private JLabel lbDateDernierAchat;
  private JPanel pnlDateDernierAchat;
  private XRiCalendrier ARG12D;
  private XRiCalendrier PLA12D;
  private JLabel lbUniteStock;
  private XRiTextField ARG26A;
  private JLabel lbFicheStockMagasin;
  private XRiTextField ARG71A;
  private JPanel pnlOngletVente;
  private JPanel pnlOngletVenteGauche;
  private JLabel lbTarif;
  private XRiTextField ARG7A;
  private JLabel lbRattachementCNV;
  private XRiTextField ARG21A;
  private JLabel lbRegroupementStatistique;
  private XRiTextField ARG14A;
  private JPanel pnlOngletVenteDroite;
  private JLabel lbDateDerniereVente;
  private JPanel panel1;
  private XRiCalendrier ARG22D;
  private XRiCalendrier PLA22D;
  private JLabel lbUniteVente;
  private XRiTextField ARG27A;
  private JLabel lbUniteConditionnementVente;
  private XRiTextField ARG28A;
  private JPanel pnlOngletZonePersonnalisee;
  private JPanel pnlOngletZonePersonnaliseeGauche;
  private JLabel TIDX15;
  private XRiTextField ARG15A;
  private XRiTextField ARG16A;
  private JLabel TIDX16;
  private JLabel TIDX17;
  private XRiTextField ARG17A;
  private XRiTextField ARG18A;
  private JLabel TIDX18;
  private JLabel TIDX19;
  private XRiTextField ARG19A;
  private JPanel pnlOngletZonePersonnaliseeDroite;
  private JLabel lbCategorieZP;
  private SNCategorieZonePersonnalisee snCategorieZP;
  private XRiComboBox cbZPLongue1;
  private XRiTextField tfZPLongue1;
  private XRiComboBox cbZPLongue2;
  private XRiTextField tfZPLongue2;
  private XRiComboBox cbZPLongue3;
  private XRiTextField tfZPLongue3;
  private JPanel pnlCentral;
  private JPanel pnlConteneurResultatRecherche;
  private SNLabelTitre snLabelTitreTableau;
  private JLabel lbTriListe;
  private XRiComboBox cbTriListe;
  private SNBarreRecherche snBarreRecherche;
  private JPanel pnlChoixModeListe;
  private JButton btModeConsultation;
  private JButton btModeModification;
  private JPanel pnlListe;
  private JScrollPane SCROLLPANE__LIST;
  private XRiTable WTP01;
  private JPanel pnlBoutonListe;
  private JButton OBJ_DEBL;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_FINL;
  private JPanel pnlCardCreation;
  private JPanel pnlCreationArticleDuplique;
  private JLabel lbEtablissementArticleDuplique;
  private SNEtablissement snEtablissementArticleDuplique;
  private JLabel lbCodeArticleDuplique;
  private XRiTextField WIART3;
  private JPanel pnlCreationArticleCree;
  private JLabel lbEtablissementArticleCree;
  private SNEtablissement snEtablissementArticleCree;
  private JLabel lbCodeArticleCree;
  private XRiTextField INDART;
  private SNLabelChamp lbGroupeArticle;
  private SNGroupe snGroupeCreation;
  private JLabel lbFamilleArticleCree;
  private SNFamille snFamilleCreation;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem miChoixPossibles;
  private JMenuItem miAideEnLigne;
  private JPopupMenu BTD2;
  private JMenuItem miOptions;
  private JMenuItem miDeverrouillage;
  private JMenuItem miHistorique;
  private JMenuItem miPrixChantier;
  private JPopupMenu BTD3;
  private JMenuItem miAideEnLigne2;
  private JLabel label1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}

/**
 * Classe interne représentant une ZP Longue
 */
class ZPLongue {
  private String nomChamps;
  private String nomChampsSaisie;
  private String valeur;
  
  public ZPLongue(String pNomChamps, String pNomChampsSaisie, String pValeur) {
    nomChamps = pNomChamps;
    nomChampsSaisie = pNomChampsSaisie;
    valeur = pValeur;
  }
  
  public String getNomChamps() {
    return nomChamps;
  }
  
  public String getNomChampsSaisie() {
    return nomChampsSaisie;
  }
  
  public String getValeur() {
    return valeur;
  }
  
  public void setValeur(String pValeurZp) {
    valeur = pValeurZp;
  }
  
  @Override
  public String toString() {
    return nomChamps;
  }
}
