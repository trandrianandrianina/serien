
package ri.serien.libecranrpg.sprm.SPRM04FM;
// Nom Fichier: b_SPRM04FM_FMTB1_FMTF1_71.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SPRM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SPRM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTOUE.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETALIB@")).trim());
    OBJ_50.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_53.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBD@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBF@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AALIB@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ETAT.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    ETAT.setEnabled(lexique.isPresent("ETAT"));
    REPFIN.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("REPFIN"));
    REPDEB.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("REPDEB"));
    OBJ_67.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    ACTION.setEnabled(lexique.isPresent("ACTION"));
    WETB.setVisible(lexique.isPresent("WETB"));
    OBJ_69.setVisible(lexique.isPresent("ETALIB"));
    OBJ_57.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("RPLIBD"));
    OBJ_53.setVisible(lexique.isPresent("WENCX"));
    OBJ_50.setVisible(lexique.isPresent("DGNOM"));
    
    // WTOUE.setSelected(lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!WTOUE.isSelected());
    
    // WTOU.setSelected(lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!WTOU.isSelected());
    
    ETAT.setEnabled(lexique.isPresent("ETAT"));
    
    if (lexique.isTrue("93")) {
      OBJ_39.setTitle("Plage de Dates (JJMMAA ou JJ.MM.AA)");
      OBJ_44.setText("Date Début");
      OBJ_45.setText("Date Fin");
      // DATFIN.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") &
      // lexique.isPresent("DATFIN"));
      // DATDEB.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") &
      // lexique.isPresent("DATDEB"));
      
    }
    
    if (lexique.isTrue("95")) {
      OBJ_39.setTitle("Plage de numéros d'Affaires");
      OBJ_44.setText("Numéro Début");
      OBJ_45.setText("Numéro Fin");
      NUMDEB.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMDEB"));
      NUMFIN.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMFIN"));
      
    }
    if (lexique.isTrue("96")) {
      OBJ_39.setTitle("Plage de numéros Prospects");
      OBJ_44.setText("Numéro Début");
      OBJ_45.setText("Numéro Fin");
      NUMDEB.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMDEB"));
      NUMFIN.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMFIN"));
      NUMLIF.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMLIF"));
      NUMLID.setVisible(!lexique.HostFieldGetData("WTOU").trim().equalsIgnoreCase("**") & lexique.isPresent("NUMLID"));
      
    }
    
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @(TITLE)@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (WTOUE.isSelected())
    // lexique.HostFieldPutData("WTOUE", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUE", 0, " ");
    // if (WTOU.isSelected())
    // lexique.HostFieldPutData("WTOU", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOU", 0, " ");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sprm04"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void WTOUEActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    OBJ_39 = new JXTitledSeparator();
    OBJ_36 = new JXTitledSeparator();
    OBJ_49 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_69 = new JLabel();
    OBJ_67 = new JLabel();
    ETAT = new XRiTextField();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    WTOU = new XRiCheckBox();
    WTOUE = new XRiCheckBox();
    OBJ_63 = new JLabel();
    WETB = new XRiTextField();
    ACTION = new XRiTextField();
    BT_ChgSoc = new JButton();
    P_SEL1 = new JPanel();
    OBJ_57 = new JLabel();
    OBJ_44 = new JLabel();
    NUMFIN = new XRiTextField();
    NUMDEB = new XRiTextField();
    NUMLID = new XRiTextField();
    NUMLIF = new XRiTextField();
    REPDEB = new XRiTextField();
    REPFIN = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_62 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    label1 = new JLabel();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap(881, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addComponent(bt_Fonctions)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_39 ----
      OBJ_39.setTitle("Plage de codes repr\u00e9sentants");
      OBJ_39.setName("OBJ_39");

      //---- OBJ_36 ----
      OBJ_36.setTitle("Actions \u00e0 s\u00e9lectionner");
      OBJ_36.setName("OBJ_36");

      //---- OBJ_49 ----
      OBJ_49.setTitle("Etablissement s\u00e9lectionn\u00e9");
      OBJ_49.setName("OBJ_49");

      //======== P_SEL0 ========
      {
        P_SEL0.setBorder(new TitledBorder(""));
        P_SEL0.setName("P_SEL0");
        P_SEL0.setLayout(null);

        //---- OBJ_69 ----
        OBJ_69.setText("@ETALIB@");
        OBJ_69.setName("OBJ_69");
        P_SEL0.add(OBJ_69);
        OBJ_69.setBounds(140, 20, 226, 20);

        //---- OBJ_67 ----
        OBJ_67.setText("Etat");
        OBJ_67.setName("OBJ_67");
        P_SEL0.add(OBJ_67);
        OBJ_67.setBounds(15, 20, 27, 20);

        //---- ETAT ----
        ETAT.setComponentPopupMenu(BTD);
        ETAT.setName("ETAT");
        P_SEL0.add(ETAT);
        ETAT.setBounds(85, 16, 20, ETAT.getPreferredSize().height);
      }

      //---- OBJ_50 ----
      OBJ_50.setText("@DGNOM@");
      OBJ_50.setName("OBJ_50");

      //---- OBJ_53 ----
      OBJ_53.setText("@WENCX@");
      OBJ_53.setName("OBJ_53");

      //---- WTOU ----
      WTOU.setText("S\u00e9lection compl\u00e8te");
      WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOU.setName("WTOU");
      WTOU.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUActionPerformed(e);
        }
      });

      //---- WTOUE ----
      WTOUE.setText("S\u00e9lection compl\u00e8te");
      WTOUE.setComponentPopupMenu(BTD);
      WTOUE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOUE.setName("WTOUE");
      WTOUE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUEActionPerformed(e);
        }
      });

      //---- OBJ_63 ----
      OBJ_63.setText("Type d'action");
      OBJ_63.setName("OBJ_63");

      //---- WETB ----
      WETB.setComponentPopupMenu(BTD);
      WETB.setName("WETB");

      //---- ACTION ----
      ACTION.setComponentPopupMenu(BTD);
      ACTION.setName("ACTION");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      //======== P_SEL1 ========
      {
        P_SEL1.setBorder(new TitledBorder(""));
        P_SEL1.setName("P_SEL1");
        P_SEL1.setLayout(null);

        //---- OBJ_57 ----
        OBJ_57.setText("@RPLIBD@");
        OBJ_57.setName("OBJ_57");
        P_SEL1.add(OBJ_57);
        OBJ_57.setBounds(315, 25, 270, 20);

        //---- OBJ_44 ----
        OBJ_44.setText("Repr\u00e9sentant D\u00e9but");
        OBJ_44.setName("OBJ_44");
        P_SEL1.add(OBJ_44);
        OBJ_44.setBounds(20, 25, 121, 20);

        //---- NUMFIN ----
        NUMFIN.setComponentPopupMenu(BTD);
        NUMFIN.setName("NUMFIN");
        P_SEL1.add(NUMFIN);
        NUMFIN.setBounds(140, 55, 58, NUMFIN.getPreferredSize().height);

        //---- NUMDEB ----
        NUMDEB.setComponentPopupMenu(BTD);
        NUMDEB.setName("NUMDEB");
        P_SEL1.add(NUMDEB);
        NUMDEB.setBounds(140, 20, 58, NUMDEB.getPreferredSize().height);

        //---- NUMLID ----
        NUMLID.setComponentPopupMenu(BTD);
        NUMLID.setName("NUMLID");
        P_SEL1.add(NUMLID);
        NUMLID.setBounds(270, 20, 34, NUMLID.getPreferredSize().height);

        //---- NUMLIF ----
        NUMLIF.setComponentPopupMenu(BTD);
        NUMLIF.setName("NUMLIF");
        P_SEL1.add(NUMLIF);
        NUMLIF.setBounds(270, 55, 34, NUMLIF.getPreferredSize().height);

        //---- REPDEB ----
        REPDEB.setComponentPopupMenu(BTD);
        REPDEB.setName("REPDEB");
        P_SEL1.add(REPDEB);
        REPDEB.setBounds(140, 20, 30, REPDEB.getPreferredSize().height);

        //---- REPFIN ----
        REPFIN.setComponentPopupMenu(BTD);
        REPFIN.setName("REPFIN");
        P_SEL1.add(REPFIN);
        REPFIN.setBounds(140, 55, 30, REPFIN.getPreferredSize().height);

        //---- OBJ_45 ----
        OBJ_45.setText("Repr\u00e9sentant Fin");
        OBJ_45.setName("OBJ_45");
        P_SEL1.add(OBJ_45);
        OBJ_45.setBounds(20, 60, 121, 20);

        //---- OBJ_62 ----
        OBJ_62.setText("@RPLIBF@");
        OBJ_62.setName("OBJ_62");
        P_SEL1.add(OBJ_62);
        OBJ_62.setBounds(315, 60, 270, 20);

        //---- DATDEB ----
        DATDEB.setName("DATDEB");
        P_SEL1.add(DATDEB);
        DATDEB.setBounds(140, 20, 115, DATDEB.getPreferredSize().height);

        //---- DATFIN ----
        DATFIN.setName("DATFIN");
        P_SEL1.add(DATFIN);
        DATFIN.setBounds(140, 55, 115, DATFIN.getPreferredSize().height);
      }

      //---- label1 ----
      label1.setText("@AALIB@");
      label1.setName("label1");

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
            .addGap(92, 92, 92)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
            .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 700, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 103, GroupLayout.PREFERRED_SIZE)
            .addGap(57, 57, 57)
            .addComponent(ACTION, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(10, 10, 10)
            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(WTOUE, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 485, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(9, 9, 9)
                .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(15, 15, 15)
            .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(21, 21, 21)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(40, 40, 40)
                .addComponent(WTOU, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
            .addGap(20, 20, 20)
            .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(ACTION, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label1)))
            .addGap(27, 27, 27)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addComponent(WTOUE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_39;
  private JXTitledSeparator OBJ_36;
  private JXTitledSeparator OBJ_49;
  private JPanel P_SEL0;
  private JLabel OBJ_69;
  private JLabel OBJ_67;
  private XRiTextField ETAT;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOUE;
  private JLabel OBJ_63;
  private XRiTextField WETB;
  private XRiTextField ACTION;
  private JButton BT_ChgSoc;
  private JPanel P_SEL1;
  private JLabel OBJ_57;
  private JLabel OBJ_44;
  private XRiTextField NUMFIN;
  private XRiTextField NUMDEB;
  private XRiTextField NUMLID;
  private XRiTextField NUMLIF;
  private XRiTextField REPDEB;
  private XRiTextField REPFIN;
  private JLabel OBJ_45;
  private JLabel OBJ_62;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JLabel label1;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
