
package ri.serien.libecranrpg.sprm.SPRM02FM;
// Nom Fichier: b_SPRM02FM_FMTB1_FMTF1_61.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SPRM02FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SPRM02FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTOUE.setValeursSelection("**", "  ");
    WTOUR.setValeursSelection("**", "  ");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBD@")).trim());
    OBJ_34.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIBF@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ETAT2.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    ETAT2.setEnabled(lexique.isPresent("ETAT2"));
    ETAT1.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    ETAT1.setEnabled(lexique.isPresent("ETAT1"));
    OBJ_36.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    OBJ_35.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    REPFIN.setVisible(!lexique.HostFieldGetData("WTOUR").trim().equalsIgnoreCase("**"));
    REPFIN.setEnabled(lexique.isPresent("REPFIN"));
    REPDEB.setVisible(!lexique.HostFieldGetData("WTOUR").trim().equalsIgnoreCase("**"));
    REPDEB.setEnabled(lexique.isPresent("REPDEB"));
    WETB.setVisible(lexique.isPresent("WETB"));
    OBJ_37.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    OBJ_32.setVisible(!lexique.HostFieldGetData("WTOUR").trim().equalsIgnoreCase("**"));
    // WTOUE.setVisible( lexique.isPresent("WTOUE"));
    // WTOUE.setEnabled( lexique.isPresent("WTOUE"));
    // WTOUE.setSelected(lexique.HostFieldGetData("WTOUE").equalsIgnoreCase("**"));
    // WTOUR.setVisible( lexique.isPresent("WTOUR"));
    // WTOUR.setEnabled( lexique.isPresent("WTOUR"));
    // WTOUR.setSelected(lexique.HostFieldGetData("WTOUR").equalsIgnoreCase("**"));
    OBJ_28.setVisible(!lexique.HostFieldGetData("WTOUR").trim().equalsIgnoreCase("**"));
    OBJ_34.setVisible(lexique.isPresent("RPLIBF"));
    OBJ_31.setVisible(lexique.isPresent("RPLIBD"));
    OBJ_27.setVisible(lexique.isPresent("WENCX"));
    OBJ_25.setVisible(lexique.isPresent("DGNOM"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOUR").trim().equalsIgnoreCase("**"));
    REPFIN.setEnabled(lexique.isPresent("REPFIN"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    ETAT2.setEnabled(lexique.isPresent("ETAT2"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @(TITLE)@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (WTOUE.isSelected())
    // lexique.HostFieldPutData("WTOUE", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUE", 0, " ");
    // if (WTOUR.isSelected())
    // lexique.HostFieldPutData("WTOUR", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUR", 0, " ");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sprm02"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void WTOURActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUEActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    P_Centre = new JPanel();
    OBJ_22 = new JXTitledSeparator();
    OBJ_24 = new JXTitledSeparator();
    OBJ_19 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_37 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    ETAT1 = new XRiTextField();
    ETAT2 = new XRiTextField();
    P_SEL1 = new JPanel();
    OBJ_31 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_32 = new JLabel();
    REPDEB = new XRiTextField();
    REPFIN = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_27 = new JLabel();
    WTOUR = new XRiCheckBox();
    WTOUE = new XRiCheckBox();
    WETB = new XRiTextField();
    BT_ChgSoc = new JButton();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap(881, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addComponent(bt_Fonctions)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //---- OBJ_22 ----
      OBJ_22.setTitle("Plage de Codes Repr\u00e9sentants");
      OBJ_22.setName("OBJ_22");

      //---- OBJ_24 ----
      OBJ_24.setTitle("Etablissement s\u00e9lectionn\u00e9");
      OBJ_24.setName("OBJ_24");

      //---- OBJ_19 ----
      OBJ_19.setTitle(" ");
      OBJ_19.setName("OBJ_19");

      //======== P_SEL0 ========
      {
        P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL0.setName("P_SEL0");
        P_SEL0.setLayout(null);

        //---- OBJ_37 ----
        OBJ_37.setText("Etat des affaires");
        OBJ_37.setName("OBJ_37");
        P_SEL0.add(OBJ_37);
        OBJ_37.setBounds(20, 35, 99, 20);

        //---- OBJ_35 ----
        OBJ_35.setText("1");
        OBJ_35.setName("OBJ_35");
        P_SEL0.add(OBJ_35);
        OBJ_35.setBounds(185, 10, 20, 20);

        //---- OBJ_36 ----
        OBJ_36.setText("2");
        OBJ_36.setName("OBJ_36");
        P_SEL0.add(OBJ_36);
        OBJ_36.setBounds(215, 10, 20, 20);

        //---- ETAT1 ----
        ETAT1.setComponentPopupMenu(BTD);
        ETAT1.setName("ETAT1");
        P_SEL0.add(ETAT1);
        ETAT1.setBounds(180, 35, 20, ETAT1.getPreferredSize().height);

        //---- ETAT2 ----
        ETAT2.setComponentPopupMenu(BTD);
        ETAT2.setName("ETAT2");
        P_SEL0.add(ETAT2);
        ETAT2.setBounds(210, 35, 20, ETAT2.getPreferredSize().height);
      }

      //======== P_SEL1 ========
      {
        P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
        P_SEL1.setName("P_SEL1");
        P_SEL1.setLayout(null);

        //---- OBJ_31 ----
        OBJ_31.setText("@RPLIBD@");
        OBJ_31.setName("OBJ_31");
        P_SEL1.add(OBJ_31);
        OBJ_31.setBounds(215, 19, 232, 20);

        //---- OBJ_34 ----
        OBJ_34.setText("@RPLIBF@");
        OBJ_34.setName("OBJ_34");
        P_SEL1.add(OBJ_34);
        OBJ_34.setBounds(215, 49, 232, 20);

        //---- OBJ_28 ----
        OBJ_28.setText("Repr\u00e9sentant de d\u00e9but");
        OBJ_28.setName("OBJ_28");
        P_SEL1.add(OBJ_28);
        OBJ_28.setBounds(20, 19, 141, 20);

        //---- OBJ_32 ----
        OBJ_32.setText("Repr\u00e9sentant de fin");
        OBJ_32.setName("OBJ_32");
        P_SEL1.add(OBJ_32);
        OBJ_32.setBounds(20, 49, 120, 20);

        //---- REPDEB ----
        REPDEB.setComponentPopupMenu(BTD);
        REPDEB.setName("REPDEB");
        P_SEL1.add(REPDEB);
        REPDEB.setBounds(180, 15, 30, REPDEB.getPreferredSize().height);

        //---- REPFIN ----
        REPFIN.setComponentPopupMenu(BTD);
        REPFIN.setName("REPFIN");
        P_SEL1.add(REPFIN);
        REPFIN.setBounds(180, 45, 30, REPFIN.getPreferredSize().height);
      }

      //---- OBJ_25 ----
      OBJ_25.setText("@DGNOM@");
      OBJ_25.setName("OBJ_25");

      //---- OBJ_27 ----
      OBJ_27.setText("@WENCX@");
      OBJ_27.setName("OBJ_27");

      //---- WTOUR ----
      WTOUR.setText("S\u00e9lection compl\u00e8te");
      WTOUR.setComponentPopupMenu(BTD);
      WTOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOUR.setName("WTOUR");
      WTOUR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOURActionPerformed(e);
        }
      });

      //---- WTOUE ----
      WTOUE.setText("S\u00e9lection compl\u00e8te");
      WTOUE.setComponentPopupMenu(BTD);
      WTOUE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      WTOUE.setName("WTOUE");
      WTOUE.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          WTOUEActionPerformed(e);
        }
      });

      //---- WETB ----
      WETB.setComponentPopupMenu(BTD);
      WETB.setName("WETB");

      //---- BT_ChgSoc ----
      BT_ChgSoc.setText("");
      BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
      BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ChgSoc.setName("BT_ChgSoc");
      BT_ChgSoc.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ChgSocActionPerformed(e);
        }
      });

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(50, 50, 50)
            .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(5, 5, 5)
            .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
            .addGap(112, 112, 112)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 426, GroupLayout.PREFERRED_SIZE)
              .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE)))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(50, 50, 50)
            .addComponent(WTOUR, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
            .addGap(24, 24, 24)
            .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, 650, GroupLayout.PREFERRED_SIZE))
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(50, 50, 50)
            .addComponent(WTOUE, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
            .addGap(24, 24, 24)
            .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 455, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(35, 35, 35)
            .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(11, 11, 11)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE))
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(OBJ_27, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            .addGap(15, 15, 15)
            .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(16, 16, 16)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(WTOUR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL1, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE))
            .addGap(10, 10, 10)
            .addComponent(OBJ_19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            .addGap(21, 21, 21)
            .addGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addComponent(WTOUE, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JPanel P_Centre;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_19;
  private JPanel P_SEL0;
  private JLabel OBJ_37;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private XRiTextField ETAT1;
  private XRiTextField ETAT2;
  private JPanel P_SEL1;
  private JLabel OBJ_31;
  private JLabel OBJ_34;
  private JLabel OBJ_28;
  private JLabel OBJ_32;
  private XRiTextField REPDEB;
  private XRiTextField REPFIN;
  private JLabel OBJ_25;
  private JLabel OBJ_27;
  private XRiCheckBox WTOUR;
  private XRiCheckBox WTOUE;
  private XRiTextField WETB;
  private JButton BT_ChgSoc;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
