
package ri.serien.libecranrpg.sprm.SPRM05FM;
// Nom Fichier: b_SPRM05FM_FMTB2_FMTF1_74.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.LayoutStyle;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SPRM05FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] CACETA_Value = { "", "1", "2", "3", };
  private String[] CAFETA_Value = { "", "1", "2", "3", "4", "5", };
  
  public SPRM05FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CAFETA.setValeurs(CAFETA_Value, null);
    CACETA.setValeurs(CACETA_Value, null);
    CMMAIL.setValeursSelection("OUI", "NON");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    DGNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBORI@")).trim());
    OBJ_109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBACT@")).trim());
    OBJ_118.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAFOBJ@")).trim());
    OBJ_122.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAFREP@")).trim());
    OBJ_125.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAFETA@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    CAFREP.setEnabled(lexique.isPresent("CAFREP"));
    CLIVF.setEnabled(lexique.isPresent("CLIVF"));
    CLIVD.setEnabled(lexique.isPresent("CLIVD"));
    CCOZP5.setEnabled(lexique.isPresent("CCOZP5"));
    CCOZP4.setEnabled(lexique.isPresent("CCOZP4"));
    CCOZP3.setEnabled(lexique.isPresent("CCOZP3"));
    CCOZP2.setEnabled(lexique.isPresent("CCOZP2"));
    CCOZP1.setEnabled(lexique.isPresent("CCOZP1"));
    CCOCA5.setEnabled(lexique.isPresent("CCOCA5"));
    CCOCA4.setEnabled(lexique.isPresent("CCOCA4"));
    CCOCA3.setEnabled(lexique.isPresent("CCOCA3"));
    CCOCA2.setEnabled(lexique.isPresent("CCOCA2"));
    CCOCAT.setEnabled(lexique.isPresent("CCOCAT"));
    CAFGF4.setEnabled(lexique.isPresent("CAFGF4"));
    CAFGF3.setEnabled(lexique.isPresent("CAFGF3"));
    CAFGF2.setEnabled(lexique.isPresent("CAFGF2"));
    CAFGF1.setEnabled(lexique.isPresent("CAFGF1"));
    CACTYP.setEnabled(lexique.isPresent("CACTYP"));
    CPREFF.setEnabled(lexique.isPresent("CPREFF"));
    CPREFD.setEnabled(lexique.isPresent("CPREFD"));
    CPRORI.setEnabled(lexique.isPresent("CPRORI"));
    CPRCTF.setEnabled(lexique.isPresent("CPRCTF"));
    CPRCTD.setEnabled(lexique.isPresent("CPRCTD"));
    WETB.setVisible(lexique.isPresent("WETB"));
    CPNAPF.setEnabled(lexique.isPresent("CPNAPF"));
    CPNAPD.setEnabled(lexique.isPresent("CPNAPD"));
    CPRCPF.setEnabled(lexique.isPresent("CPRCPF"));
    CPRCPD.setEnabled(lexique.isPresent("CPRCPD"));
    CPROF.setEnabled(lexique.isPresent("CPROF"));
    CPROD.setEnabled(lexique.isPresent("CPROD"));
    // CAFDEX.setEnabled( lexique.isPresent("CAFDEX"));
    // CAFFIX.setEnabled( lexique.isPresent("CAFFIX"));
    // CACDAX.setEnabled( lexique.isPresent("CACDAX"));
    CAFOBJ.setEnabled(lexique.isPresent("CAFOBJ"));
    CPRZGF.setEnabled(lexique.isPresent("CPRZGF"));
    CPRZGD.setEnabled(lexique.isPresent("CPRZGD"));
    CPRCAF.setEnabled(lexique.isPresent("CPRCAF"));
    CPRCAD.setEnabled(lexique.isPresent("CPRCAD"));
    LAFGF4.setVisible(lexique.isPresent("LAFGF4"));
    LAFGF3.setVisible(lexique.isPresent("LAFGF3"));
    LAFGF2.setVisible(lexique.isPresent("LAFGF2"));
    LAFGF1.setVisible(lexique.isPresent("LAFGF1"));
    OBJ_109.setVisible(lexique.isPresent("LIBACT"));
    OBJ_80.setVisible(lexique.isPresent("LIBORI"));
    CAFAR4.setEnabled(lexique.isPresent("CAFAR4"));
    CAFAR3.setEnabled(lexique.isPresent("CAFAR3"));
    CAFAR2.setEnabled(lexique.isPresent("CAFAR2"));
    CAFAR1.setEnabled(lexique.isPresent("CAFAR1"));
    OBJ_125.setVisible(lexique.isPresent("LAFETA"));
    OBJ_122.setVisible(lexique.isPresent("LAFREP"));
    OBJ_118.setVisible(lexique.isPresent("LAFOBJ"));
    CACCOD.setEnabled(lexique.isPresent("CACCOD"));
    // CMMAIL.setEnabled( lexique.isPresent("CMMAIL"));
    // CMMAIL.setSelected(lexique.HostFieldGetData("CMMAIL").equalsIgnoreCase("OUI"));
    CMAOBJ.setEnabled(lexique.isPresent("CMAOBJ"));
    CMAREF.setEnabled(lexique.isPresent("CMAREF"));
    // CACETA.setSelectedIndex(getIndice("CACETA", CACETA_Value));
    // CACETA.setEnabled( lexique.isPresent("CACETA"));
    // CAFETA.setSelectedIndex(getIndice("CAFETA", CAFETA_Value));
    // CAFETA.setEnabled( lexique.isPresent("CAFETA"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(ManagerSessionClient.getInstance().chargerLogoEtablissement(WETB.getText()));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (CMMAIL.isSelected())
    // lexique.HostFieldPutData("CMMAIL", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CMMAIL", 0, "NON");
    // lexique.HostFieldPutData("CACETA", 0, CACETA_Value[CACETA.getSelectedIndex()]);
    // lexique.HostFieldPutData("CAFETA", 0, CAFETA_Value[CAFETA.getSelectedIndex()]);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sprm05"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    DGNOM = new JLabel();
    WETB = new XRiTextField();
    bt_Fonctions = new JButton();
    label1 = new JLabel();
    P_Centre = new JPanel();
    OBJ_38 = new JTabbedPane();
    OBJ_39 = new JPanel();
    panel1 = new JPanel();
    CMAREF = new XRiTextField();
    CMAOBJ = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_46 = new JPanel();
    panel2 = new JPanel();
    OBJ_80 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_68 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_74 = new JLabel();
    OBJ_70 = new JLabel();
    CPRCAD = new XRiTextField();
    CPRCAF = new XRiTextField();
    CPRZGD = new XRiTextField();
    CPRZGF = new XRiTextField();
    CPROD = new XRiTextField();
    CPROF = new XRiTextField();
    CPRCPD = new XRiTextField();
    CPRCPF = new XRiTextField();
    CPNAPD = new XRiTextField();
    CPNAPF = new XRiTextField();
    CPRCTD = new XRiTextField();
    CPRCTF = new XRiTextField();
    CPRORI = new XRiTextField();
    CPREFD = new XRiTextField();
    CPREFF = new XRiTextField();
    CLIVD = new XRiTextField();
    CLIVF = new XRiTextField();
    OBJ_53 = new JLabel();
    OBJ_58 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_66 = new JLabel();
    OBJ_69 = new JLabel();
    OBJ_72 = new JLabel();
    OBJ_76 = new JLabel();
    panel3 = new JPanel();
    CMMAIL = new XRiCheckBox();
    OBJ_91 = new JLabel();
    OBJ_84 = new JLabel();
    CCOCAT = new XRiTextField();
    CCOCA2 = new XRiTextField();
    CCOCA3 = new XRiTextField();
    CCOCA4 = new XRiTextField();
    CCOCA5 = new XRiTextField();
    CCOZP1 = new XRiTextField();
    CCOZP2 = new XRiTextField();
    CCOZP3 = new XRiTextField();
    CCOZP4 = new XRiTextField();
    CCOZP5 = new XRiTextField();
    OBJ_99 = new JPanel();
    panel4 = new JPanel();
    CACETA = new XRiComboBox();
    CACCOD = new XRiTextField();
    OBJ_109 = new JLabel();
    OBJ_110 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_107 = new JLabel();
    CACTYP = new XRiTextField();
    OBJ_105 = new JLabel();
    CACDAX = new XRiCalendrier();
    OBJ_112 = new JPanel();
    panel5 = new JPanel();
    CAFETA = new XRiComboBox();
    OBJ_118 = new JLabel();
    OBJ_122 = new JLabel();
    OBJ_125 = new JLabel();
    OBJ_120 = new JLabel();
    OBJ_123 = new JLabel();
    OBJ_126 = new JLabel();
    CAFOBJ = new XRiTextField();
    OBJ_116 = new JLabel();
    CAFREP = new XRiTextField();
    OBJ_121 = new JLabel();
    CAFDEX = new XRiCalendrier();
    CAFFIX = new XRiCalendrier();
    OBJ_128 = new JPanel();
    panel6 = new JPanel();
    CAFAR1 = new XRiTextField();
    CAFAR2 = new XRiTextField();
    CAFAR3 = new XRiTextField();
    CAFAR4 = new XRiTextField();
    LAFGF1 = new XRiTextField();
    LAFGF2 = new XRiTextField();
    LAFGF3 = new XRiTextField();
    LAFGF4 = new XRiTextField();
    OBJ_132 = new JLabel();
    OBJ_133 = new JLabel();
    CAFGF1 = new XRiTextField();
    CAFGF2 = new XRiTextField();
    CAFGF3 = new XRiTextField();
    CAFGF4 = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== this ========
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("@TITPG1@ @TITPG2@");
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setPreferredSize(new Dimension(66, 35));
        P_Infos.setName("P_Infos");

        //---- DGNOM ----
        DGNOM.setText("@DGNOM@");
        DGNOM.setName("DGNOM");

        //---- WETB ----
        WETB.setComponentPopupMenu(BTD);
        WETB.setName("WETB");

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });

        //---- label1 ----
        label1.setText("Etablissement");
        label1.setName("label1");

        GroupLayout P_InfosLayout = new GroupLayout(P_Infos);
        P_Infos.setLayout(P_InfosLayout);
        P_InfosLayout.setHorizontalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, P_InfosLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(label1)
              .addGap(5, 5, 5)
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
              .addGap(18, 18, 18)
              .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, 339, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 387, Short.MAX_VALUE)
              .addComponent(bt_Fonctions, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
        );
        P_InfosLayout.setVerticalGroup(
          P_InfosLayout.createParallelGroup()
            .addGroup(P_InfosLayout.createSequentialGroup()
              .addGroup(P_InfosLayout.createParallelGroup()
                .addComponent(bt_Fonctions)
                .addGroup(P_InfosLayout.createSequentialGroup()
                  .addGap(6, 6, 6)
                  .addComponent(label1))
                .addGroup(P_InfosLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");

      //======== OBJ_38 ========
      {
        OBJ_38.setName("OBJ_38");

        //======== OBJ_39 ========
        {
          OBJ_39.setName("OBJ_39");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- CMAREF ----
            CMAREF.setComponentPopupMenu(BTD);
            CMAREF.setName("CMAREF");
            panel1.add(CMAREF);
            CMAREF.setBounds(135, 25, 310, CMAREF.getPreferredSize().height);

            //---- CMAOBJ ----
            CMAOBJ.setComponentPopupMenu(BTD);
            CMAOBJ.setName("CMAOBJ");
            panel1.add(CMAOBJ);
            CMAOBJ.setBounds(135, 55, 310, CMAOBJ.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("R\u00e9f\u00e9rence");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(60, 30, 66, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("Objet");
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(60, 59, 36, 20);
          }

          GroupLayout OBJ_39Layout = new GroupLayout(OBJ_39);
          OBJ_39.setLayout(OBJ_39Layout);
          OBJ_39Layout.setHorizontalGroup(
            OBJ_39Layout.createParallelGroup()
              .addGroup(OBJ_39Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 510, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(177, Short.MAX_VALUE))
          );
          OBJ_39Layout.setVerticalGroup(
            OBJ_39Layout.createParallelGroup()
              .addGroup(OBJ_39Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 113, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(166, Short.MAX_VALUE))
          );
        }
        OBJ_38.addTab("Requ\u00eate", OBJ_39);

        //======== OBJ_46 ========
        {
          OBJ_46.setName("OBJ_46");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Crit\u00e8res de s\u00e9lection"));
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_80 ----
            OBJ_80.setText("@LIBORI@");
            OBJ_80.setName("OBJ_80");
            panel2.add(OBJ_80);
            OBJ_80.setBounds(520, 129, 150, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("Chiffre d'affaire");
            OBJ_56.setName("OBJ_56");
            panel2.add(OBJ_56);
            OBJ_56.setBounds(355, 39, 88, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Code postal");
            OBJ_60.setName("OBJ_60");
            panel2.add(OBJ_60);
            OBJ_60.setBounds(15, 69, 76, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("N\u00b0 prospect");
            OBJ_50.setName("OBJ_50");
            panel2.add(OBJ_50);
            OBJ_50.setBounds(15, 39, 73, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("Zone g\u00e9o.");
            OBJ_64.setName("OBJ_64");
            panel2.add(OBJ_64);
            OBJ_64.setBounds(355, 69, 64, 20);

            //---- OBJ_68 ----
            OBJ_68.setText("Code Ape");
            OBJ_68.setName("OBJ_68");
            panel2.add(OBJ_68);
            OBJ_68.setBounds(15, 99, 64, 20);

            //---- OBJ_78 ----
            OBJ_78.setText("Origine");
            OBJ_78.setName("OBJ_78");
            panel2.add(OBJ_78);
            OBJ_78.setBounds(355, 129, 64, 20);

            //---- OBJ_74 ----
            OBJ_74.setText("Cat\u00e9gorie");
            OBJ_74.setName("OBJ_74");
            panel2.add(OBJ_74);
            OBJ_74.setBounds(15, 129, 63, 20);

            //---- OBJ_70 ----
            OBJ_70.setText("Effectif");
            OBJ_70.setName("OBJ_70");
            panel2.add(OBJ_70);
            OBJ_70.setBounds(355, 99, 61, 20);

            //---- CPRCAD ----
            CPRCAD.setComponentPopupMenu(BTD);
            CPRCAD.setName("CPRCAD");
            panel2.add(CPRCAD);
            CPRCAD.setBounds(450, 35, 84, CPRCAD.getPreferredSize().height);

            //---- CPRCAF ----
            CPRCAF.setComponentPopupMenu(BTD);
            CPRCAF.setName("CPRCAF");
            panel2.add(CPRCAF);
            CPRCAF.setBounds(535, 35, 84, CPRCAF.getPreferredSize().height);

            //---- CPRZGD ----
            CPRZGD.setComponentPopupMenu(BTD);
            CPRZGD.setName("CPRZGD");
            panel2.add(CPRZGD);
            CPRZGD.setBounds(450, 65, 60, CPRZGD.getPreferredSize().height);

            //---- CPRZGF ----
            CPRZGF.setComponentPopupMenu(BTD);
            CPRZGF.setName("CPRZGF");
            panel2.add(CPRZGF);
            CPRZGF.setBounds(535, 65, 60, CPRZGF.getPreferredSize().height);

            //---- CPROD ----
            CPROD.setComponentPopupMenu(BTD);
            CPROD.setName("CPROD");
            panel2.add(CPROD);
            CPROD.setBounds(100, 35, 60, CPROD.getPreferredSize().height);

            //---- CPROF ----
            CPROF.setComponentPopupMenu(BTD);
            CPROF.setName("CPROF");
            panel2.add(CPROF);
            CPROF.setBounds(220, 35, 60, CPROF.getPreferredSize().height);

            //---- CPRCPD ----
            CPRCPD.setComponentPopupMenu(BTD);
            CPRCPD.setName("CPRCPD");
            panel2.add(CPRCPD);
            CPRCPD.setBounds(100, 65, 52, CPRCPD.getPreferredSize().height);

            //---- CPRCPF ----
            CPRCPF.setComponentPopupMenu(BTD);
            CPRCPF.setName("CPRCPF");
            panel2.add(CPRCPF);
            CPRCPF.setBounds(220, 65, 52, CPRCPF.getPreferredSize().height);

            //---- CPNAPD ----
            CPNAPD.setComponentPopupMenu(BTD);
            CPNAPD.setName("CPNAPD");
            panel2.add(CPNAPD);
            CPNAPD.setBounds(100, 95, 60, CPNAPD.getPreferredSize().height);

            //---- CPNAPF ----
            CPNAPF.setComponentPopupMenu(BTD);
            CPNAPF.setName("CPNAPF");
            panel2.add(CPNAPF);
            CPNAPF.setBounds(220, 95, 60, CPNAPF.getPreferredSize().height);

            //---- CPRCTD ----
            CPRCTD.setComponentPopupMenu(BTD);
            CPRCTD.setName("CPRCTD");
            panel2.add(CPRCTD);
            CPRCTD.setBounds(100, 125, 40, CPRCTD.getPreferredSize().height);

            //---- CPRCTF ----
            CPRCTF.setComponentPopupMenu(BTD);
            CPRCTF.setName("CPRCTF");
            panel2.add(CPRCTF);
            CPRCTF.setBounds(220, 125, 40, CPRCTF.getPreferredSize().height);

            //---- CPRORI ----
            CPRORI.setComponentPopupMenu(BTD);
            CPRORI.setName("CPRORI");
            panel2.add(CPRORI);
            CPRORI.setBounds(450, 125, 40, CPRORI.getPreferredSize().height);

            //---- CPREFD ----
            CPREFD.setComponentPopupMenu(BTD);
            CPREFD.setName("CPREFD");
            panel2.add(CPREFD);
            CPREFD.setBounds(450, 95, 44, CPREFD.getPreferredSize().height);

            //---- CPREFF ----
            CPREFF.setComponentPopupMenu(BTD);
            CPREFF.setName("CPREFF");
            panel2.add(CPREFF);
            CPREFF.setBounds(535, 95, 44, CPREFF.getPreferredSize().height);

            //---- CLIVD ----
            CLIVD.setComponentPopupMenu(BTD);
            CLIVD.setName("CLIVD");
            panel2.add(CLIVD);
            CLIVD.setBounds(160, 35, 34, CLIVD.getPreferredSize().height);

            //---- CLIVF ----
            CLIVF.setComponentPopupMenu(BTD);
            CLIVF.setName("CLIVF");
            panel2.add(CLIVF);
            CLIVF.setBounds(280, 35, 36, CLIVF.getPreferredSize().height);

            //---- OBJ_53 ----
            OBJ_53.setText("/");
            OBJ_53.setName("OBJ_53");
            panel2.add(OBJ_53);
            OBJ_53.setBounds(205, 39, 9, 20);

            //---- OBJ_58 ----
            OBJ_58.setText("/");
            OBJ_58.setName("OBJ_58");
            panel2.add(OBJ_58);
            OBJ_58.setBounds(520, 35, 9, 20);

            //---- OBJ_62 ----
            OBJ_62.setText("/");
            OBJ_62.setName("OBJ_62");
            panel2.add(OBJ_62);
            OBJ_62.setBounds(205, 69, 9, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("/");
            OBJ_66.setName("OBJ_66");
            panel2.add(OBJ_66);
            OBJ_66.setBounds(520, 69, 9, 20);

            //---- OBJ_69 ----
            OBJ_69.setText("/");
            OBJ_69.setName("OBJ_69");
            panel2.add(OBJ_69);
            OBJ_69.setBounds(205, 99, 9, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("/");
            OBJ_72.setName("OBJ_72");
            panel2.add(OBJ_72);
            OBJ_72.setBounds(520, 99, 9, 20);

            //---- OBJ_76 ----
            OBJ_76.setText("/");
            OBJ_76.setName("OBJ_76");
            panel2.add(OBJ_76);
            OBJ_76.setBounds(205, 129, 9, 20);
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Contact"));
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- CMMAIL ----
            CMMAIL.setText("Adresse email obligatoire");
            CMMAIL.setComponentPopupMenu(BTD);
            CMMAIL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CMMAIL.setName("CMMAIL");
            panel3.add(CMMAIL);
            CMMAIL.setBounds(355, 39, 181, 20);

            //---- OBJ_91 ----
            OBJ_91.setText("Zones Perso");
            OBJ_91.setName("OBJ_91");
            panel3.add(OBJ_91);
            OBJ_91.setBounds(15, 69, 85, 20);

            //---- OBJ_84 ----
            OBJ_84.setText("Fonction");
            OBJ_84.setName("OBJ_84");
            panel3.add(OBJ_84);
            OBJ_84.setBounds(15, 39, 73, 20);

            //---- CCOCAT ----
            CCOCAT.setComponentPopupMenu(BTD);
            CCOCAT.setName("CCOCAT");
            panel3.add(CCOCAT);
            CCOCAT.setBounds(100, 35, 40, CCOCAT.getPreferredSize().height);

            //---- CCOCA2 ----
            CCOCA2.setComponentPopupMenu(BTD);
            CCOCA2.setName("CCOCA2");
            panel3.add(CCOCA2);
            CCOCA2.setBounds(140, 35, 40, CCOCA2.getPreferredSize().height);

            //---- CCOCA3 ----
            CCOCA3.setComponentPopupMenu(BTD);
            CCOCA3.setName("CCOCA3");
            panel3.add(CCOCA3);
            CCOCA3.setBounds(180, 35, 40, CCOCA3.getPreferredSize().height);

            //---- CCOCA4 ----
            CCOCA4.setComponentPopupMenu(BTD);
            CCOCA4.setName("CCOCA4");
            panel3.add(CCOCA4);
            CCOCA4.setBounds(220, 35, 40, CCOCA4.getPreferredSize().height);

            //---- CCOCA5 ----
            CCOCA5.setComponentPopupMenu(BTD);
            CCOCA5.setName("CCOCA5");
            panel3.add(CCOCA5);
            CCOCA5.setBounds(260, 35, 40, CCOCA5.getPreferredSize().height);

            //---- CCOZP1 ----
            CCOZP1.setComponentPopupMenu(BTD);
            CCOZP1.setName("CCOZP1");
            panel3.add(CCOZP1);
            CCOZP1.setBounds(100, 65, 30, CCOZP1.getPreferredSize().height);

            //---- CCOZP2 ----
            CCOZP2.setComponentPopupMenu(BTD);
            CCOZP2.setName("CCOZP2");
            panel3.add(CCOZP2);
            CCOZP2.setBounds(140, 65, 30, CCOZP2.getPreferredSize().height);

            //---- CCOZP3 ----
            CCOZP3.setComponentPopupMenu(BTD);
            CCOZP3.setName("CCOZP3");
            panel3.add(CCOZP3);
            CCOZP3.setBounds(180, 65, 30, CCOZP3.getPreferredSize().height);

            //---- CCOZP4 ----
            CCOZP4.setComponentPopupMenu(BTD);
            CCOZP4.setName("CCOZP4");
            panel3.add(CCOZP4);
            CCOZP4.setBounds(220, 65, 30, CCOZP4.getPreferredSize().height);

            //---- CCOZP5 ----
            CCOZP5.setComponentPopupMenu(BTD);
            CCOZP5.setName("CCOZP5");
            panel3.add(CCOZP5);
            CCOZP5.setBounds(260, 65, 30, CCOZP5.getPreferredSize().height);
          }

          GroupLayout OBJ_46Layout = new GroupLayout(OBJ_46);
          OBJ_46.setLayout(OBJ_46Layout);
          OBJ_46Layout.setHorizontalGroup(
            OBJ_46Layout.createParallelGroup()
              .addGroup(OBJ_46Layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(OBJ_46Layout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 670, GroupLayout.PREFERRED_SIZE)))
          );
          OBJ_46Layout.setVerticalGroup(
            OBJ_46Layout.createParallelGroup()
              .addGroup(OBJ_46Layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE))
          );
        }
        OBJ_38.addTab("Crit\u00e8res", OBJ_46);

        //======== OBJ_99 ========
        {
          OBJ_99.setName("OBJ_99");

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Action commerciale"));
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- CACETA ----
            CACETA.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes",
              "A effectuer",
              "En cours",
              "Effectu\u00e9e"
            }));
            CACETA.setComponentPopupMenu(BTD);
            CACETA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CACETA.setName("CACETA");
            panel4.add(CACETA);
            CACETA.setBounds(155, 70, 79, CACETA.getPreferredSize().height);

            //---- CACCOD ----
            CACCOD.setComponentPopupMenu(BTD);
            CACCOD.setName("CACCOD");
            panel4.add(CACCOD);
            CACCOD.setBounds(155, 40, 210, CACCOD.getPreferredSize().height);

            //---- OBJ_109 ----
            OBJ_109.setText("@LIBACT@");
            OBJ_109.setName("OBJ_109");
            panel4.add(OBJ_109);
            OBJ_109.setBounds(375, 73, 123, 20);

            //---- OBJ_110 ----
            OBJ_110.setText("Date de l'action");
            OBJ_110.setName("OBJ_110");
            panel4.add(OBJ_110);
            OBJ_110.setBounds(50, 104, 96, 20);

            //---- OBJ_104 ----
            OBJ_104.setText("Code");
            OBJ_104.setName("OBJ_104");
            panel4.add(OBJ_104);
            OBJ_104.setBounds(50, 44, 36, 20);

            //---- OBJ_107 ----
            OBJ_107.setText("Type");
            OBJ_107.setName("OBJ_107");
            panel4.add(OBJ_107);
            OBJ_107.setBounds(285, 73, 36, 20);

            //---- CACTYP ----
            CACTYP.setComponentPopupMenu(BTD);
            CACTYP.setName("CACTYP");
            panel4.add(CACTYP);
            CACTYP.setBounds(325, 69, 40, CACTYP.getPreferredSize().height);

            //---- OBJ_105 ----
            OBJ_105.setText("Etat");
            OBJ_105.setName("OBJ_105");
            panel4.add(OBJ_105);
            OBJ_105.setBounds(50, 73, 27, 20);

            //---- CACDAX ----
            CACDAX.setName("CACDAX");
            panel4.add(CACDAX);
            CACDAX.setBounds(155, 100, 115, CACDAX.getPreferredSize().height);
          }

          GroupLayout OBJ_99Layout = new GroupLayout(OBJ_99);
          OBJ_99.setLayout(OBJ_99Layout);
          OBJ_99Layout.setHorizontalGroup(
            OBJ_99Layout.createParallelGroup()
              .addGroup(OBJ_99Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 520, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(162, Short.MAX_VALUE))
          );
          OBJ_99Layout.setVerticalGroup(
            OBJ_99Layout.createParallelGroup()
              .addGroup(OBJ_99Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 156, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(133, Short.MAX_VALUE))
          );
        }
        OBJ_38.addTab("Action", OBJ_99);

        //======== OBJ_112 ========
        {
          OBJ_112.setName("OBJ_112");

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("Affaire commerciale"));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- CAFETA ----
            CAFETA.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes",
              "Affaires en cours",
              "Affaires en attente",
              "Affaires perdues",
              "Affaires sign\u00e9es",
              "Voir bloc-notes"
            }));
            CAFETA.setComponentPopupMenu(BTD);
            CAFETA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            CAFETA.setName("CAFETA");
            panel5.add(CAFETA);
            CAFETA.setBounds(135, 130, 117, CAFETA.getPreferredSize().height);

            //---- OBJ_118 ----
            OBJ_118.setText("@LAFOBJ@");
            OBJ_118.setName("OBJ_118");
            panel5.add(OBJ_118);
            OBJ_118.setBounds(280, 44, 180, 20);

            //---- OBJ_122 ----
            OBJ_122.setText("@LAFREP@");
            OBJ_122.setName("OBJ_122");
            panel5.add(OBJ_122);
            OBJ_122.setBounds(280, 104, 180, 20);

            //---- OBJ_125 ----
            OBJ_125.setText("@LAFETA@");
            OBJ_125.setName("OBJ_125");
            panel5.add(OBJ_125);
            OBJ_125.setBounds(280, 133, 180, 20);

            //---- OBJ_120 ----
            OBJ_120.setText("Date d\u00e9but");
            OBJ_120.setName("OBJ_120");
            panel5.add(OBJ_120);
            OBJ_120.setBounds(40, 74, 75, 20);

            //---- OBJ_123 ----
            OBJ_123.setText("Repr\u00e9sentant");
            OBJ_123.setName("OBJ_123");
            panel5.add(OBJ_123);
            OBJ_123.setBounds(40, 104, 84, 20);

            //---- OBJ_126 ----
            OBJ_126.setText("Code \u00e9tat");
            OBJ_126.setName("OBJ_126");
            panel5.add(OBJ_126);
            OBJ_126.setBounds(40, 133, 63, 20);

            //---- CAFOBJ ----
            CAFOBJ.setComponentPopupMenu(BTD);
            CAFOBJ.setName("CAFOBJ");
            panel5.add(CAFOBJ);
            CAFOBJ.setBounds(135, 40, 60, CAFOBJ.getPreferredSize().height);

            //---- OBJ_116 ----
            OBJ_116.setText("Objet");
            OBJ_116.setName("OBJ_116");
            panel5.add(OBJ_116);
            OBJ_116.setBounds(40, 44, 36, 20);

            //---- CAFREP ----
            CAFREP.setComponentPopupMenu(BTD);
            CAFREP.setName("CAFREP");
            panel5.add(CAFREP);
            CAFREP.setBounds(135, 100, 30, CAFREP.getPreferredSize().height);

            //---- OBJ_121 ----
            OBJ_121.setText("Date fin");
            OBJ_121.setName("OBJ_121");
            panel5.add(OBJ_121);
            OBJ_121.setBounds(280, 74, 65, 20);

            //---- CAFDEX ----
            CAFDEX.setName("CAFDEX");
            panel5.add(CAFDEX);
            CAFDEX.setBounds(135, 70, 115, CAFDEX.getPreferredSize().height);

            //---- CAFFIX ----
            CAFFIX.setName("CAFFIX");
            panel5.add(CAFFIX);
            CAFFIX.setBounds(355, 70, 115, CAFFIX.getPreferredSize().height);
          }

          GroupLayout OBJ_112Layout = new GroupLayout(OBJ_112);
          OBJ_112.setLayout(OBJ_112Layout);
          OBJ_112Layout.setHorizontalGroup(
            OBJ_112Layout.createParallelGroup()
              .addGroup(OBJ_112Layout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 530, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(155, Short.MAX_VALUE))
          );
          OBJ_112Layout.setVerticalGroup(
            OBJ_112Layout.createParallelGroup()
              .addGroup(OBJ_112Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 184, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(104, Short.MAX_VALUE))
          );
        }
        OBJ_38.addTab("Affaire", OBJ_112);

        //======== OBJ_128 ========
        {
          OBJ_128.setName("OBJ_128");

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("Int\u00e9r\u00eats pour"));
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- CAFAR1 ----
            CAFAR1.setComponentPopupMenu(BTD);
            CAFAR1.setName("CAFAR1");
            panel6.add(CAFAR1);
            CAFAR1.setBounds(225, 55, 210, CAFAR1.getPreferredSize().height);

            //---- CAFAR2 ----
            CAFAR2.setComponentPopupMenu(BTD);
            CAFAR2.setName("CAFAR2");
            panel6.add(CAFAR2);
            CAFAR2.setBounds(225, 85, 210, CAFAR2.getPreferredSize().height);

            //---- CAFAR3 ----
            CAFAR3.setComponentPopupMenu(BTD);
            CAFAR3.setName("CAFAR3");
            panel6.add(CAFAR3);
            CAFAR3.setBounds(225, 115, 210, CAFAR3.getPreferredSize().height);

            //---- CAFAR4 ----
            CAFAR4.setComponentPopupMenu(BTD);
            CAFAR4.setName("CAFAR4");
            panel6.add(CAFAR4);
            CAFAR4.setBounds(225, 145, 210, CAFAR4.getPreferredSize().height);

            //---- LAFGF1 ----
            LAFGF1.setComponentPopupMenu(BTD);
            LAFGF1.setName("LAFGF1");
            panel6.add(LAFGF1);
            LAFGF1.setBounds(85, 55, 92, LAFGF1.getPreferredSize().height);

            //---- LAFGF2 ----
            LAFGF2.setComponentPopupMenu(BTD);
            LAFGF2.setName("LAFGF2");
            panel6.add(LAFGF2);
            LAFGF2.setBounds(85, 85, 92, LAFGF2.getPreferredSize().height);

            //---- LAFGF3 ----
            LAFGF3.setComponentPopupMenu(BTD);
            LAFGF3.setName("LAFGF3");
            panel6.add(LAFGF3);
            LAFGF3.setBounds(85, 115, 92, LAFGF3.getPreferredSize().height);

            //---- LAFGF4 ----
            LAFGF4.setComponentPopupMenu(BTD);
            LAFGF4.setName("LAFGF4");
            panel6.add(LAFGF4);
            LAFGF4.setBounds(85, 145, 92, LAFGF4.getPreferredSize().height);

            //---- OBJ_132 ----
            OBJ_132.setText("Groupe / Famille");
            OBJ_132.setComponentPopupMenu(BTD);
            OBJ_132.setName("OBJ_132");
            panel6.add(OBJ_132);
            OBJ_132.setBounds(45, 35, 111, 16);

            //---- OBJ_133 ----
            OBJ_133.setText("Article");
            OBJ_133.setName("OBJ_133");
            panel6.add(OBJ_133);
            OBJ_133.setBounds(225, 35, 58, 16);

            //---- CAFGF1 ----
            CAFGF1.setComponentPopupMenu(BTD);
            CAFGF1.setName("CAFGF1");
            panel6.add(CAFGF1);
            CAFGF1.setBounds(45, 55, 40, CAFGF1.getPreferredSize().height);

            //---- CAFGF2 ----
            CAFGF2.setComponentPopupMenu(BTD);
            CAFGF2.setName("CAFGF2");
            panel6.add(CAFGF2);
            CAFGF2.setBounds(45, 85, 40, CAFGF2.getPreferredSize().height);

            //---- CAFGF3 ----
            CAFGF3.setComponentPopupMenu(BTD);
            CAFGF3.setName("CAFGF3");
            panel6.add(CAFGF3);
            CAFGF3.setBounds(45, 115, 40, CAFGF3.getPreferredSize().height);

            //---- CAFGF4 ----
            CAFGF4.setComponentPopupMenu(BTD);
            CAFGF4.setName("CAFGF4");
            panel6.add(CAFGF4);
            CAFGF4.setBounds(45, 145, 40, CAFGF4.getPreferredSize().height);
          }

          GroupLayout OBJ_128Layout = new GroupLayout(OBJ_128);
          OBJ_128.setLayout(OBJ_128Layout);
          OBJ_128Layout.setHorizontalGroup(
            OBJ_128Layout.createParallelGroup()
              .addGroup(OBJ_128Layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 490, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(197, Short.MAX_VALUE))
          );
          OBJ_128Layout.setVerticalGroup(
            OBJ_128Layout.createParallelGroup()
              .addGroup(OBJ_128Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 200, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(88, Short.MAX_VALUE))
          );
        }
        OBJ_38.addTab("Int\u00earet pour", OBJ_128);
      }

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(40, 40, 40)
            .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, 710, GroupLayout.PREFERRED_SIZE))
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(65, 65, 65)
            .addComponent(OBJ_38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
      );
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JLabel DGNOM;
  private XRiTextField WETB;
  private JButton bt_Fonctions;
  private JLabel label1;
  private JPanel P_Centre;
  private JTabbedPane OBJ_38;
  private JPanel OBJ_39;
  private JPanel panel1;
  private XRiTextField CMAREF;
  private XRiTextField CMAOBJ;
  private JLabel OBJ_42;
  private JLabel OBJ_44;
  private JPanel OBJ_46;
  private JPanel panel2;
  private JLabel OBJ_80;
  private JLabel OBJ_56;
  private JLabel OBJ_60;
  private JLabel OBJ_50;
  private JLabel OBJ_64;
  private JLabel OBJ_68;
  private JLabel OBJ_78;
  private JLabel OBJ_74;
  private JLabel OBJ_70;
  private XRiTextField CPRCAD;
  private XRiTextField CPRCAF;
  private XRiTextField CPRZGD;
  private XRiTextField CPRZGF;
  private XRiTextField CPROD;
  private XRiTextField CPROF;
  private XRiTextField CPRCPD;
  private XRiTextField CPRCPF;
  private XRiTextField CPNAPD;
  private XRiTextField CPNAPF;
  private XRiTextField CPRCTD;
  private XRiTextField CPRCTF;
  private XRiTextField CPRORI;
  private XRiTextField CPREFD;
  private XRiTextField CPREFF;
  private XRiTextField CLIVD;
  private XRiTextField CLIVF;
  private JLabel OBJ_53;
  private JLabel OBJ_58;
  private JLabel OBJ_62;
  private JLabel OBJ_66;
  private JLabel OBJ_69;
  private JLabel OBJ_72;
  private JLabel OBJ_76;
  private JPanel panel3;
  private XRiCheckBox CMMAIL;
  private JLabel OBJ_91;
  private JLabel OBJ_84;
  private XRiTextField CCOCAT;
  private XRiTextField CCOCA2;
  private XRiTextField CCOCA3;
  private XRiTextField CCOCA4;
  private XRiTextField CCOCA5;
  private XRiTextField CCOZP1;
  private XRiTextField CCOZP2;
  private XRiTextField CCOZP3;
  private XRiTextField CCOZP4;
  private XRiTextField CCOZP5;
  private JPanel OBJ_99;
  private JPanel panel4;
  private XRiComboBox CACETA;
  private XRiTextField CACCOD;
  private JLabel OBJ_109;
  private JLabel OBJ_110;
  private JLabel OBJ_104;
  private JLabel OBJ_107;
  private XRiTextField CACTYP;
  private JLabel OBJ_105;
  private XRiCalendrier CACDAX;
  private JPanel OBJ_112;
  private JPanel panel5;
  private XRiComboBox CAFETA;
  private JLabel OBJ_118;
  private JLabel OBJ_122;
  private JLabel OBJ_125;
  private JLabel OBJ_120;
  private JLabel OBJ_123;
  private JLabel OBJ_126;
  private XRiTextField CAFOBJ;
  private JLabel OBJ_116;
  private XRiTextField CAFREP;
  private JLabel OBJ_121;
  private XRiCalendrier CAFDEX;
  private XRiCalendrier CAFFIX;
  private JPanel OBJ_128;
  private JPanel panel6;
  private XRiTextField CAFAR1;
  private XRiTextField CAFAR2;
  private XRiTextField CAFAR3;
  private XRiTextField CAFAR4;
  private XRiTextField LAFGF1;
  private XRiTextField LAFGF2;
  private XRiTextField LAFGF3;
  private XRiTextField LAFGF4;
  private JLabel OBJ_132;
  private JLabel OBJ_133;
  private XRiTextField CAFGF1;
  private XRiTextField CAFGF2;
  private XRiTextField CAFGF3;
  private XRiTextField CAFGF4;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
