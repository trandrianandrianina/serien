
package ri.serien.libecranrpg.sprm.SPRM05FM;
// Nom Fichier: pop_SPRM05FM_FMTB3_FMTF1_75.java

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;

/**
 * @author Stéphane Vénéri
 */
public class SPRM05FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SPRM05FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    TRI1.setValeurs("1", "RB");
    TRI2.setValeurs("2", "RB");
    TRI3.setValeurs("3", "RB");
    TRI4.setValeurs("4", "RB");
    TRI5.setValeurs("5", "RB");
    TRI6.setValeurs("6", "RB");
    
    // Bouton par défaut
    setDefaultButton(OBJ_22);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_22.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_23.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TRI5.isSelected())
    // lexique.HostFieldPutData("RB", 0, "5");
    // if (TRI4.isSelected())
    // lexique.HostFieldPutData("RB", 0, "4");
    // if (TRI2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TRI1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TRI6.isSelected())
    // lexique.HostFieldPutData("RB", 0, "6");
    // if (TRI3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TRI7.isSelected())
    // lexique.HostFieldPutData("P", 0, "X");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sprm05"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void TRI1ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_PnlOpts = new JPanel();
    OBJ_22 = new JButton();
    OBJ_23 = new JButton();
    panel1 = new JPanel();
    TRI7 = new XRiRadioButton();
    TRI3 = new XRiRadioButton();
    TRI6 = new XRiRadioButton();
    TRI1 = new XRiRadioButton();
    TRI2 = new XRiRadioButton();
    TRI4 = new XRiRadioButton();
    TRI5 = new XRiRadioButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setName("this");
    setLayout(null);

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      //---- OBJ_22 ----
      OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_22);
      OBJ_22.setBounds(65, 5, 56, 40);

      //---- OBJ_23 ----
      OBJ_23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_23);
      OBJ_23.setBounds(5, 5, 56, 40);
    }
    add(P_PnlOpts);
    P_PnlOpts.setBounds(145, 250, 125, 50);

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("Tri par"));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- TRI7 ----
      TRI7.setText("Num\u00e9ro prospection");
      TRI7.setComponentPopupMenu(BTD);
      TRI7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI7.setName("TRI7");
      panel1.add(TRI7);
      TRI7.setBounds(15, 200, 147, 20);

      //---- TRI3 ----
      TRI3.setText("Zone g\u00e9ographique");
      TRI3.setComponentPopupMenu(BTD);
      TRI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI3.setName("TRI3");
      panel1.add(TRI3);
      TRI3.setBounds(15, 85, 142, 20);

      //---- TRI6 ----
      TRI6.setText("Origine prospection");
      TRI6.setComponentPopupMenu(BTD);
      TRI6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI6.setName("TRI6");
      panel1.add(TRI6);
      TRI6.setBounds(15, 175, 142, 20);

      //---- TRI1 ----
      TRI1.setText("Nom prospection");
      TRI1.setComponentPopupMenu(BTD);
      TRI1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI1.setName("TRI1");
      TRI1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          TRI1ActionPerformed(e);
        }
      });
      panel1.add(TRI1);
      TRI1.setBounds(15, 30, 129, 20);

      //---- TRI2 ----
      TRI2.setText("Code Postal");
      TRI2.setComponentPopupMenu(BTD);
      TRI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI2.setName("TRI2");
      panel1.add(TRI2);
      TRI2.setBounds(15, 60, 96, 20);

      //---- TRI4 ----
      TRI4.setText("Code Ape");
      TRI4.setComponentPopupMenu(BTD);
      TRI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI4.setName("TRI4");
      panel1.add(TRI4);
      TRI4.setBounds(15, 115, 82, 20);

      //---- TRI5 ----
      TRI5.setText("Cat\u00e9gorie");
      TRI5.setComponentPopupMenu(BTD);
      TRI5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TRI5.setName("TRI5");
      panel1.add(TRI5);
      TRI5.setBounds(15, 145, 81, 20);
    }
    add(panel1);
    panel1.setBounds(10, 10, 200, 240);

    setPreferredSize(new Dimension(270, 300));

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);
      OBJ_4.addSeparator();

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TRI3);
    RB_GRP.add(TRI6);
    RB_GRP.add(TRI1);
    RB_GRP.add(TRI2);
    RB_GRP.add(TRI4);
    RB_GRP.add(TRI5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_PnlOpts;
  private JButton OBJ_22;
  private JButton OBJ_23;
  private JPanel panel1;
  private XRiRadioButton TRI7;
  private XRiRadioButton TRI3;
  private XRiRadioButton TRI6;
  private XRiRadioButton TRI1;
  private XRiRadioButton TRI2;
  private XRiRadioButton TRI4;
  private XRiRadioButton TRI5;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
