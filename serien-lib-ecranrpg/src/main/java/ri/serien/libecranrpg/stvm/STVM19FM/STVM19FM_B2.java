
package ri.serien.libecranrpg.stvm.STVM19FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class STVM19FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM19FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    panel1.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim()));
    NCLFA1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLFA1@")).trim());
    NCLFA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLFA2@")).trim());
    NCLFA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLFA3@")).trim());
    NCLFA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNF1@")).trim());
    NCLFA5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNF2@")).trim());
    NCLFA6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNF3@")).trim());
    NCLFA7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLBA1@")).trim());
    NCLFA8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLBA2@")).trim());
    NCLFA9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLBA3@")).trim());
    NCLFA10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNO1@")).trim());
    NCLFA11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNO2@")).trim());
    NCLFA12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NCLNO3@")).trim());
    NCLFA13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPRNO1@")).trim());
    NCLFA14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPRNO2@")).trim());
    NCLFA15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPRNO3@")).trim());
    NCLFA16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLNO1@")).trim());
    NCLFA17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLNO2@")).trim());
    NCLFA18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CCLNO3@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("devise : @LIBDEV@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    
    label7.setVisible(lexique.isPresent("LIBDEV"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Synthèse"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    NCLFA1 = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    NCLFA2 = new RiZoneSortie();
    NCLFA3 = new RiZoneSortie();
    NCLFA4 = new RiZoneSortie();
    NCLFA5 = new RiZoneSortie();
    NCLFA6 = new RiZoneSortie();
    NCLFA7 = new RiZoneSortie();
    NCLFA8 = new RiZoneSortie();
    NCLFA9 = new RiZoneSortie();
    NCLFA10 = new RiZoneSortie();
    NCLFA11 = new RiZoneSortie();
    NCLFA12 = new RiZoneSortie();
    NCLFA13 = new RiZoneSortie();
    NCLFA14 = new RiZoneSortie();
    NCLFA15 = new RiZoneSortie();
    NCLFA16 = new RiZoneSortie();
    NCLFA17 = new RiZoneSortie();
    NCLFA18 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Ann\u00e9e en cours");
    separator2 = compFactory.createSeparator("Ann\u00e9e -1");
    separator3 = compFactory.createSeparator("Ann\u00e9e -2");
    label7 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 330));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Repr\u00e9sentation graphique");
            riSousMenu_bt6.setToolTipText("Repr\u00e9sentation graphique");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("@LIBPOS@"));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- NCLFA1 ----
          NCLFA1.setText("@NCLFA1@");
          NCLFA1.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA1.setName("NCLFA1");

          //---- label1 ----
          label1.setText("nombre de clients factur\u00e9s");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("nombre de clients non factur\u00e9s");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("nombre total de clients");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("nombre de clients nouveaux");
          label4.setName("label4");

          //---- label5 ----
          label5.setText("nombre de prospects");
          label5.setName("label5");

          //---- label6 ----
          label6.setText("chiffre d'affaires sur nouveaux clients");
          label6.setName("label6");

          //---- NCLFA2 ----
          NCLFA2.setText("@NCLFA2@");
          NCLFA2.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA2.setName("NCLFA2");

          //---- NCLFA3 ----
          NCLFA3.setText("@NCLFA3@");
          NCLFA3.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA3.setName("NCLFA3");

          //---- NCLFA4 ----
          NCLFA4.setText("@NCLNF1@");
          NCLFA4.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA4.setName("NCLFA4");

          //---- NCLFA5 ----
          NCLFA5.setText("@NCLNF2@");
          NCLFA5.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA5.setName("NCLFA5");

          //---- NCLFA6 ----
          NCLFA6.setText("@NCLNF3@");
          NCLFA6.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA6.setName("NCLFA6");

          //---- NCLFA7 ----
          NCLFA7.setText("@NCLBA1@");
          NCLFA7.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA7.setName("NCLFA7");

          //---- NCLFA8 ----
          NCLFA8.setText("@NCLBA2@");
          NCLFA8.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA8.setName("NCLFA8");

          //---- NCLFA9 ----
          NCLFA9.setText("@NCLBA3@");
          NCLFA9.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA9.setName("NCLFA9");

          //---- NCLFA10 ----
          NCLFA10.setText("@NCLNO1@");
          NCLFA10.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA10.setName("NCLFA10");

          //---- NCLFA11 ----
          NCLFA11.setText("@NCLNO2@");
          NCLFA11.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA11.setName("NCLFA11");

          //---- NCLFA12 ----
          NCLFA12.setText("@NCLNO3@");
          NCLFA12.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA12.setName("NCLFA12");

          //---- NCLFA13 ----
          NCLFA13.setText("@NPRNO1@");
          NCLFA13.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA13.setName("NCLFA13");

          //---- NCLFA14 ----
          NCLFA14.setText("@NPRNO2@");
          NCLFA14.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA14.setName("NCLFA14");

          //---- NCLFA15 ----
          NCLFA15.setText("@NPRNO3@");
          NCLFA15.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA15.setName("NCLFA15");

          //---- NCLFA16 ----
          NCLFA16.setText("@CCLNO1@");
          NCLFA16.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA16.setName("NCLFA16");

          //---- NCLFA17 ----
          NCLFA17.setText("@CCLNO2@");
          NCLFA17.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA17.setName("NCLFA17");

          //---- NCLFA18 ----
          NCLFA18.setText("@CCLNO3@");
          NCLFA18.setHorizontalAlignment(SwingConstants.RIGHT);
          NCLFA18.setName("NCLFA18");

          //---- separator1 ----
          separator1.setName("separator1");

          //---- separator2 ----
          separator2.setName("separator2");

          //---- separator3 ----
          separator3.setName("separator3");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(276, 276, 276)
                .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(separator3, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA1, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA3, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA4, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA5, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA6, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA7, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA8, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA9, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label4, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA10, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA11, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA12, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label5, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA13, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA14, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA15, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(label6, GroupLayout.PREFERRED_SIZE, 250, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(NCLFA16, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA17, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(NCLFA18, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(separator1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(separator2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(separator3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label1))
                  .addComponent(NCLFA1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label2))
                  .addComponent(NCLFA4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label3))
                  .addComponent(NCLFA7, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA8, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA9, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label4))
                  .addComponent(NCLFA10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label5))
                  .addComponent(NCLFA13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(13, 13, 13)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(label6))
                  .addComponent(NCLFA16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(NCLFA18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 735, 295);

        //---- label7 ----
        label7.setText("devise : @LIBDEV@");
        label7.setHorizontalAlignment(SwingConstants.RIGHT);
        label7.setName("label7");
        p_contenu.add(label7);
        label7.setBounds(470, 305, 275, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie NCLFA1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private RiZoneSortie NCLFA2;
  private RiZoneSortie NCLFA3;
  private RiZoneSortie NCLFA4;
  private RiZoneSortie NCLFA5;
  private RiZoneSortie NCLFA6;
  private RiZoneSortie NCLFA7;
  private RiZoneSortie NCLFA8;
  private RiZoneSortie NCLFA9;
  private RiZoneSortie NCLFA10;
  private RiZoneSortie NCLFA11;
  private RiZoneSortie NCLFA12;
  private RiZoneSortie NCLFA13;
  private RiZoneSortie NCLFA14;
  private RiZoneSortie NCLFA15;
  private RiZoneSortie NCLFA16;
  private RiZoneSortie NCLFA17;
  private RiZoneSortie NCLFA18;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JLabel label7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
