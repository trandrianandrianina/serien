
package ri.serien.libecranrpg.stvm.STVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class STVM18FM_B5 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM18FM_B5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    REPON3.setValeursSelection("OUI", "NON");
    REPON2.setValeursSelection("OUI", "NON");
    RAZNEG.setValeursSelection("OUI", "NON");
    STKON.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // REPON3.setSelected(lexique.HostFieldGetData("REPON3").equalsIgnoreCase("OUI"));
    // REPON2.setSelected(lexique.HostFieldGetData("REPON2").equalsIgnoreCase("OUI"));
    // RAZNEG.setSelected(lexique.HostFieldGetData("RAZNEG").equalsIgnoreCase("OUI"));
    // STKON.setSelected(lexique.HostFieldGetData("STKON").equalsIgnoreCase("OUI"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REPON3.isSelected())
    // lexique.HostFieldPutData("REPON3", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON3", 0, "NON");
    // if (REPON2.isSelected())
    // lexique.HostFieldPutData("REPON2", 0, "OUI");
    // else
    // lexique.HostFieldPutData("REPON2", 0, "NON");
    // if (RAZNEG.isSelected())
    // lexique.HostFieldPutData("RAZNEG", 0, "OUI");
    // else
    // lexique.HostFieldPutData("RAZNEG", 0, "NON");
    // if (STKON.isSelected())
    // lexique.HostFieldPutData("STKON", 0, "OUI");
    // else
    // lexique.HostFieldPutData("STKON", 0, "NON");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_16 = new JPanel();
    OBJ_17 = new JPanel();
    STKON = new XRiCheckBox();
    RAZNEG = new XRiCheckBox();
    REPON2 = new XRiCheckBox();
    REPON3 = new XRiCheckBox();
    LIBVAL = new XRiTextField();
    OBJ_20 = new JLabel();
    OBJ_18 = new JLabel();
    DATSTK = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(690, 345));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_16 ========
        {
          OBJ_16.setBorder(new TitledBorder("Le dernier calcul de stock par le planning des travaux de SERIE M est"));
          OBJ_16.setOpaque(false);
          OBJ_16.setName("OBJ_16");
          OBJ_16.setLayout(null);

          //======== OBJ_17 ========
          {
            OBJ_17.setOpaque(false);
            OBJ_17.setName("OBJ_17");
            OBJ_17.setLayout(null);

            //---- STKON ----
            STKON.setText("R\u00e9cup\u00e9rer ce calcul de stock pour l'int\u00e9grer \u00e0 votre tableau de bord");
            STKON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            STKON.setName("STKON");
            OBJ_17.add(STKON);
            STKON.setBounds(7, 13, 425, 20);
          }
          OBJ_16.add(OBJ_17);
          OBJ_17.setBounds(15, 230, 478, 46);

          //---- RAZNEG ----
          RAZNEG.setText("Remise \u00e0 z\u00e9ro des n\u00e9gatifs");
          RAZNEG.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          RAZNEG.setName("RAZNEG");
          OBJ_16.add(RAZNEG);
          RAZNEG.setBounds(20, 120, 188, 20);

          //---- REPON2 ----
          REPON2.setText("Quantit\u00e9 r\u00e9serv\u00e9e");
          REPON2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON2.setName("REPON2");
          OBJ_16.add(REPON2);
          REPON2.setBounds(20, 157, 188, 20);

          //---- REPON3 ----
          REPON3.setText("Avec articles d\u00e9sactiv\u00e9s");
          REPON3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          REPON3.setName("REPON3");
          OBJ_16.add(REPON3);
          REPON3.setBounds(20, 194, 188, 20);

          //---- LIBVAL ----
          LIBVAL.setName("LIBVAL");
          OBJ_16.add(LIBVAL);
          LIBVAL.setBounds(230, 76, 215, LIBVAL.getPreferredSize().height);

          //---- OBJ_20 ----
          OBJ_20.setText("avec une valorisation au");
          OBJ_20.setName("OBJ_20");
          OBJ_16.add(OBJ_20);
          OBJ_20.setBounds(20, 80, 148, 20);

          //---- OBJ_18 ----
          OBJ_18.setText("En date du");
          OBJ_18.setName("OBJ_18");
          OBJ_16.add(OBJ_18);
          OBJ_18.setBounds(20, 40, 67, 20);

          //---- DATSTK ----
          DATSTK.setName("DATSTK");
          OBJ_16.add(DATSTK);
          DATSTK.setBounds(230, 36, 65, DATSTK.getPreferredSize().height);
        }
        p_contenu.add(OBJ_16);
        OBJ_16.setBounds(10, 10, 501, 325);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_16;
  private JPanel OBJ_17;
  private XRiCheckBox STKON;
  private XRiCheckBox RAZNEG;
  private XRiCheckBox REPON2;
  private XRiCheckBox REPON3;
  private XRiTextField LIBVAL;
  private JLabel OBJ_20;
  private JLabel OBJ_18;
  private XRiTextField DATSTK;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
