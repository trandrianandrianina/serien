
package ri.serien.libecranrpg.stvm.STVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STVM25FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM25FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    FACB.setValeursSelection("X", " ");
    EXPB.setValeursSelection("X", " ");
    HOMB.setValeursSelection("X", " ");
    CPTB.setValeursSelection("X", " ");
    WTOUE.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    WTOUV.setValeursSelection("**", "  ");
    WTOUV2.setValeursSelection("**", "  ");
    WTOUC.setValeursSelection("**", "  ");
    WTOUM.setValeursSelection("**", "  ");
    WTOUB.setValeursSelection("**", "  ");
    WTOUF.setValeursSelection("**", "  ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WTOUV2.setSelected(lexique.HostFieldGetData("WTOUV").equalsIgnoreCase("**"));
    P_SEL2.setVisible(!lexique.HostFieldGetData("WTOUF").trim().equalsIgnoreCase("**"));
    P_SEL1.setVisible(!lexique.HostFieldGetData("WTOUB").trim().equalsIgnoreCase("**"));
    P_SEL4.setVisible(!lexique.HostFieldGetData("WTOUC").trim().equalsIgnoreCase("**"));
    P_SEL5.setVisible(!lexique.HostFieldGetData("WTOUA").trim().equalsIgnoreCase("**"));
    P_SEL3.setVisible(!lexique.HostFieldGetData("WTOUM").trim().equalsIgnoreCase("**"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUE").trim().equalsIgnoreCase("**"));
    FACB.setSelected(lexique.HostFieldGetData("FACB").equalsIgnoreCase("X"));
    xTitledPanel8.setVisible(lexique.isTrue("N20"));
    xTitledPanel11.setVisible(lexique.isTrue("20"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @TITPG2@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (WTOUV2.isSelected()) {
      lexique.HostFieldPutData("WTOUV", 0, "**");
    }
    else {
      lexique.HostFieldPutData("WTOUV", 0, "  ");
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUBActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOUFActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void WTOUMActionPerformed(ActionEvent e) {
    P_SEL3.setVisible(!P_SEL3.isVisible());
  }
  
  private void WTOUCActionPerformed(ActionEvent e) {
    P_SEL4.setVisible(!P_SEL4.isVisible());
  }
  
  private void WTOUAActionPerformed(ActionEvent e) {
    P_SEL5.setVisible(!P_SEL5.isVisible());
  }
  
  private void WTOUEActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    xTitledPanel3 = new JXTitledPanel();
    WTOUF = new XRiCheckBox();
    P_SEL2 = new JPanel();
    FACDEB = new XRiTextField();
    FACFIN = new XRiTextField();
    OBJ_38 = new JLabel();
    OBJ_40 = new JLabel();
    xTitledPanel4 = new JXTitledPanel();
    WTOUB = new XRiCheckBox();
    P_SEL1 = new JPanel();
    BONDEB = new XRiTextField();
    BONFIN = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_31 = new JLabel();
    xTitledPanel9 = new JXTitledPanel();
    DTBDEB = new XRiCalendrier();
    DTBFIN = new XRiCalendrier();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    xTitledPanel10 = new JXTitledPanel();
    WTOUM = new XRiCheckBox();
    P_SEL3 = new JPanel();
    OBJ_52 = new JLabel();
    MAGDEB = new XRiTextField();
    MAGFIN = new XRiTextField();
    OBJ_55 = new JLabel();
    xTitledPanel7 = new JXTitledPanel();
    WTOUC = new XRiCheckBox();
    P_SEL4 = new JPanel();
    CLIDEB = new XRiTextField();
    CLIFIN = new XRiTextField();
    OBJ_60 = new JLabel();
    OBJ_62 = new JLabel();
    xTitledPanel8 = new JXTitledPanel();
    WTOUV = new XRiCheckBox();
    OBJ_80 = new JLabel();
    VNDDEB = new XRiTextField();
    OBJ_82 = new JLabel();
    VNDFIN = new XRiTextField();
    xTitledPanel5 = new JXTitledPanel();
    WTOUA = new XRiCheckBox();
    P_SEL5 = new JPanel();
    AFFDEB = new XRiTextField();
    AFFFIN = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_70 = new JLabel();
    xTitledPanel6 = new JXTitledPanel();
    WTOUE = new XRiCheckBox();
    P_SEL0 = new JPanel();
    CPTB = new XRiCheckBox();
    HOMB = new XRiCheckBox();
    EXPB = new XRiCheckBox();
    FACB = new XRiCheckBox();
    xTitledPanel11 = new JXTitledPanel();
    WTOUV2 = new XRiCheckBox();
    OBJ_81 = new JLabel();
    REPDEB = new XRiTextField();
    OBJ_83 = new JLabel();
    REPFIN = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord sur les bons de vente");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vendeurs/repr\u00e9sentants");
              riSousMenu_bt6.setToolTipText("Choix entre l'affichage par vendeurs ou l'affichage par repr\u00e9sentants");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");
          p_contenu.add(sep_etablissement);
          sep_etablissement.setBounds(35, 35, 945, sep_etablissement.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");
          p_contenu.add(z_dgnom_);
          z_dgnom_.setBounds(205, 60, 260, z_dgnom_.getPreferredSize().height);

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");
          p_contenu.add(z_wencx_);
          z_wencx_.setBounds(205, 90, 260, z_wencx_.getPreferredSize().height);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setName("z_etablissement_");
          p_contenu.add(z_etablissement_);
          z_etablissement_.setBounds(50, 77, 40, z_etablissement_.getPreferredSize().height);

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });
          p_contenu.add(bouton_etablissement);
          bouton_etablissement.setBounds(new Rectangle(new Point(95, 75), bouton_etablissement.getPreferredSize()));

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Num\u00e9ros de factures \u00e0 traiter");
            xTitledPanel3.setBorder(new DropShadowBorder());
            xTitledPanel3.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- WTOUF ----
            WTOUF.setText("S\u00e9lection compl\u00e8te");
            WTOUF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUF.setName("WTOUF");
            WTOUF.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUFActionPerformed(e);
              }
            });
            xTitledPanel3ContentContainer.add(WTOUF);
            WTOUF.setBounds(15, 5, 140, 20);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new TitledBorder(""));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- FACDEB ----
              FACDEB.setComponentPopupMenu(null);
              FACDEB.setName("FACDEB");
              P_SEL2.add(FACDEB);
              FACDEB.setBounds(60, 6, 68, FACDEB.getPreferredSize().height);

              //---- FACFIN ----
              FACFIN.setComponentPopupMenu(null);
              FACFIN.setName("FACFIN");
              P_SEL2.add(FACFIN);
              FACFIN.setBounds(200, 6, 68, FACFIN.getPreferredSize().height);

              //---- OBJ_38 ----
              OBJ_38.setText("D\u00e9but");
              OBJ_38.setName("OBJ_38");
              P_SEL2.add(OBJ_38);
              OBJ_38.setBounds(15, 10, 39, 20);

              //---- OBJ_40 ----
              OBJ_40.setText("Fin");
              OBJ_40.setName("OBJ_40");
              P_SEL2.add(OBJ_40);
              OBJ_40.setBounds(160, 10, 21, 20);
            }
            xTitledPanel3ContentContainer.add(P_SEL2);
            P_SEL2.setBounds(15, 30, 286, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel3);
          xTitledPanel3.setBounds(340, 130, 320, 105);

          //======== xTitledPanel4 ========
          {
            xTitledPanel4.setTitle("Num\u00e9ros de  bons \u00e0 traiter");
            xTitledPanel4.setBorder(new DropShadowBorder());
            xTitledPanel4.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel4.setName("xTitledPanel4");
            Container xTitledPanel4ContentContainer = xTitledPanel4.getContentContainer();
            xTitledPanel4ContentContainer.setLayout(null);

            //---- WTOUB ----
            WTOUB.setText("S\u00e9lection compl\u00e8te");
            WTOUB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUB.setName("WTOUB");
            WTOUB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUBActionPerformed(e);
              }
            });
            xTitledPanel4ContentContainer.add(WTOUB);
            WTOUB.setBounds(15, 5, 140, 20);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new TitledBorder(""));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- BONDEB ----
              BONDEB.setComponentPopupMenu(null);
              BONDEB.setName("BONDEB");
              P_SEL1.add(BONDEB);
              BONDEB.setBounds(60, 5, 60, BONDEB.getPreferredSize().height);

              //---- BONFIN ----
              BONFIN.setComponentPopupMenu(null);
              BONFIN.setName("BONFIN");
              P_SEL1.add(BONFIN);
              BONFIN.setBounds(200, 5, 60, BONFIN.getPreferredSize().height);

              //---- OBJ_29 ----
              OBJ_29.setText("D\u00e9but");
              OBJ_29.setName("OBJ_29");
              P_SEL1.add(OBJ_29);
              OBJ_29.setBounds(15, 9, 39, 20);

              //---- OBJ_31 ----
              OBJ_31.setText("Fin");
              OBJ_31.setName("OBJ_31");
              P_SEL1.add(OBJ_31);
              OBJ_31.setBounds(160, 9, 21, 20);
            }
            xTitledPanel4ContentContainer.add(P_SEL1);
            P_SEL1.setBounds(15, 30, 286, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel4ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel4ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel4ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel4ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel4ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel4);
          xTitledPanel4.setBounds(15, 130, 320, 105);

          //======== xTitledPanel9 ========
          {
            xTitledPanel9.setTitle("Dates \u00e0 s\u00e9lectionner");
            xTitledPanel9.setBorder(new DropShadowBorder());
            xTitledPanel9.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel9.setName("xTitledPanel9");
            Container xTitledPanel9ContentContainer = xTitledPanel9.getContentContainer();
            xTitledPanel9ContentContainer.setLayout(null);

            //---- DTBDEB ----
            DTBDEB.setName("DTBDEB");
            xTitledPanel9ContentContainer.add(DTBDEB);
            DTBDEB.setBounds(60, 25, 105, DTBDEB.getPreferredSize().height);

            //---- DTBFIN ----
            DTBFIN.setName("DTBFIN");
            xTitledPanel9ContentContainer.add(DTBFIN);
            DTBFIN.setBounds(195, 25, 105, DTBFIN.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("D\u00e9but");
            OBJ_44.setName("OBJ_44");
            xTitledPanel9ContentContainer.add(OBJ_44);
            OBJ_44.setBounds(15, 30, 39, 18);

            //---- OBJ_46 ----
            OBJ_46.setText("Fin");
            OBJ_46.setName("OBJ_46");
            xTitledPanel9ContentContainer.add(OBJ_46);
            OBJ_46.setBounds(175, 30, 21, 19);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel9ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel9ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel9ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel9ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel9ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel9);
          xTitledPanel9.setBounds(15, 250, 320, 105);

          //======== xTitledPanel10 ========
          {
            xTitledPanel10.setTitle("Magasin");
            xTitledPanel10.setBorder(new DropShadowBorder());
            xTitledPanel10.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel10.setName("xTitledPanel10");
            Container xTitledPanel10ContentContainer = xTitledPanel10.getContentContainer();
            xTitledPanel10ContentContainer.setLayout(null);

            //---- WTOUM ----
            WTOUM.setText("S\u00e9lection compl\u00e8te");
            WTOUM.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUM.setName("WTOUM");
            WTOUM.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUMActionPerformed(e);
              }
            });
            xTitledPanel10ContentContainer.add(WTOUM);
            WTOUM.setBounds(15, 5, 140, 20);

            //======== P_SEL3 ========
            {
              P_SEL3.setBorder(new TitledBorder(""));
              P_SEL3.setOpaque(false);
              P_SEL3.setName("P_SEL3");
              P_SEL3.setLayout(null);

              //---- OBJ_52 ----
              OBJ_52.setText("D\u00e9but");
              OBJ_52.setName("OBJ_52");
              P_SEL3.add(OBJ_52);
              OBJ_52.setBounds(10, 9, 39, 20);

              //---- MAGDEB ----
              MAGDEB.setComponentPopupMenu(BTD);
              MAGDEB.setName("MAGDEB");
              P_SEL3.add(MAGDEB);
              MAGDEB.setBounds(60, 5, 30, MAGDEB.getPreferredSize().height);

              //---- MAGFIN ----
              MAGFIN.setComponentPopupMenu(BTD);
              MAGFIN.setName("MAGFIN");
              P_SEL3.add(MAGFIN);
              MAGFIN.setBounds(200, 5, 30, MAGFIN.getPreferredSize().height);

              //---- OBJ_55 ----
              OBJ_55.setText("Fin");
              OBJ_55.setName("OBJ_55");
              P_SEL3.add(OBJ_55);
              OBJ_55.setBounds(160, 9, 21, 20);
            }
            xTitledPanel10ContentContainer.add(P_SEL3);
            P_SEL3.setBounds(15, 30, 286, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel10ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel10ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel10ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel10ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel10ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel10);
          xTitledPanel10.setBounds(340, 250, 320, 105);

          //======== xTitledPanel7 ========
          {
            xTitledPanel7.setTitle("Num\u00e9ro client");
            xTitledPanel7.setBorder(new DropShadowBorder());
            xTitledPanel7.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel7.setName("xTitledPanel7");
            Container xTitledPanel7ContentContainer = xTitledPanel7.getContentContainer();
            xTitledPanel7ContentContainer.setLayout(null);

            //---- WTOUC ----
            WTOUC.setText("S\u00e9lection compl\u00e8te");
            WTOUC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUC.setName("WTOUC");
            WTOUC.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUCActionPerformed(e);
              }
            });
            xTitledPanel7ContentContainer.add(WTOUC);
            WTOUC.setBounds(15, 5, 140, 20);

            //======== P_SEL4 ========
            {
              P_SEL4.setBorder(new TitledBorder(""));
              P_SEL4.setOpaque(false);
              P_SEL4.setName("P_SEL4");
              P_SEL4.setLayout(null);

              //---- CLIDEB ----
              CLIDEB.setComponentPopupMenu(BTD);
              CLIDEB.setName("CLIDEB");
              P_SEL4.add(CLIDEB);
              CLIDEB.setBounds(60, 5, 68, CLIDEB.getPreferredSize().height);

              //---- CLIFIN ----
              CLIFIN.setComponentPopupMenu(BTD);
              CLIFIN.setName("CLIFIN");
              P_SEL4.add(CLIFIN);
              CLIFIN.setBounds(200, 5, 68, CLIFIN.getPreferredSize().height);

              //---- OBJ_60 ----
              OBJ_60.setText("D\u00e9but");
              OBJ_60.setName("OBJ_60");
              P_SEL4.add(OBJ_60);
              OBJ_60.setBounds(10, 9, 39, 20);

              //---- OBJ_62 ----
              OBJ_62.setText("Fin");
              OBJ_62.setName("OBJ_62");
              P_SEL4.add(OBJ_62);
              OBJ_62.setBounds(160, 9, 21, 20);
            }
            xTitledPanel7ContentContainer.add(P_SEL4);
            P_SEL4.setBounds(15, 30, 286, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel7ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel7ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel7ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel7ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel7ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel7);
          xTitledPanel7.setBounds(15, 370, 320, 105);

          //======== xTitledPanel8 ========
          {
            xTitledPanel8.setTitle("Vendeur");
            xTitledPanel8.setBorder(new DropShadowBorder());
            xTitledPanel8.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel8.setName("xTitledPanel8");
            Container xTitledPanel8ContentContainer = xTitledPanel8.getContentContainer();
            xTitledPanel8ContentContainer.setLayout(null);

            //---- WTOUV ----
            WTOUV.setText("S\u00e9lection compl\u00e8te");
            WTOUV.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUV.setName("WTOUV");
            xTitledPanel8ContentContainer.add(WTOUV);
            WTOUV.setBounds(15, 5, 140, 17);

            //---- OBJ_80 ----
            OBJ_80.setText("D\u00e9but");
            OBJ_80.setName("OBJ_80");
            xTitledPanel8ContentContainer.add(OBJ_80);
            OBJ_80.setBounds(30, 45, 39, OBJ_80.getPreferredSize().height);

            //---- VNDDEB ----
            VNDDEB.setComponentPopupMenu(BTD);
            VNDDEB.setName("VNDDEB");
            xTitledPanel8ContentContainer.add(VNDDEB);
            VNDDEB.setBounds(80, 39, 36, VNDDEB.getPreferredSize().height);

            //---- OBJ_82 ----
            OBJ_82.setText("Fin");
            OBJ_82.setName("OBJ_82");
            xTitledPanel8ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(180, 46, 21, 15);

            //---- VNDFIN ----
            VNDFIN.setComponentPopupMenu(BTD);
            VNDFIN.setName("VNDFIN");
            xTitledPanel8ContentContainer.add(VNDFIN);
            VNDFIN.setBounds(220, 39, 36, VNDFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel8ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel8ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel8ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel8ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel8ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel8);
          xTitledPanel8.setBounds(665, 250, 320, 105);

          //======== xTitledPanel5 ========
          {
            xTitledPanel5.setTitle("Affaire");
            xTitledPanel5.setBorder(new DropShadowBorder());
            xTitledPanel5.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel5.setName("xTitledPanel5");
            Container xTitledPanel5ContentContainer = xTitledPanel5.getContentContainer();
            xTitledPanel5ContentContainer.setLayout(null);

            //---- WTOUA ----
            WTOUA.setText("S\u00e9lection compl\u00e8te");
            WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUA.setName("WTOUA");
            WTOUA.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUAActionPerformed(e);
              }
            });
            xTitledPanel5ContentContainer.add(WTOUA);
            WTOUA.setBounds(15, 5, 140, 20);

            //======== P_SEL5 ========
            {
              P_SEL5.setBorder(new TitledBorder(""));
              P_SEL5.setOpaque(false);
              P_SEL5.setName("P_SEL5");
              P_SEL5.setLayout(null);

              //---- AFFDEB ----
              AFFDEB.setComponentPopupMenu(BTD);
              AFFDEB.setName("AFFDEB");
              P_SEL5.add(AFFDEB);
              AFFDEB.setBounds(60, 5, 44, AFFDEB.getPreferredSize().height);

              //---- AFFFIN ----
              AFFFIN.setComponentPopupMenu(BTD);
              AFFFIN.setName("AFFFIN");
              P_SEL5.add(AFFFIN);
              AFFFIN.setBounds(200, 5, 44, AFFFIN.getPreferredSize().height);

              //---- OBJ_67 ----
              OBJ_67.setText("D\u00e9but");
              OBJ_67.setName("OBJ_67");
              P_SEL5.add(OBJ_67);
              OBJ_67.setBounds(10, 9, 39, 20);

              //---- OBJ_70 ----
              OBJ_70.setText("Fin");
              OBJ_70.setName("OBJ_70");
              P_SEL5.add(OBJ_70);
              OBJ_70.setBounds(160, 9, 21, 20);
            }
            xTitledPanel5ContentContainer.add(P_SEL5);
            P_SEL5.setBounds(15, 30, 286, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel5ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel5ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel5ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel5ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel5ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel5);
          xTitledPanel5.setBounds(665, 130, 320, 105);

          //======== xTitledPanel6 ========
          {
            xTitledPanel6.setTitle("Etat des bons");
            xTitledPanel6.setBorder(new DropShadowBorder());
            xTitledPanel6.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel6.setName("xTitledPanel6");
            Container xTitledPanel6ContentContainer = xTitledPanel6.getContentContainer();
            xTitledPanel6ContentContainer.setLayout(null);

            //---- WTOUE ----
            WTOUE.setText("S\u00e9lection compl\u00e8te");
            WTOUE.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUE.setName("WTOUE");
            WTOUE.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUEActionPerformed(e);
              }
            });
            xTitledPanel6ContentContainer.add(WTOUE);
            WTOUE.setBounds(15, 25, 140, 20);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new TitledBorder(""));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- CPTB ----
              CPTB.setText("Comptabilis\u00e9");
              CPTB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              CPTB.setName("CPTB");
              P_SEL0.add(CPTB);
              CPTB.setBounds(300, 10, 105, 19);

              //---- HOMB ----
              HOMB.setText("Homologu\u00e9");
              HOMB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              HOMB.setName("HOMB");
              P_SEL0.add(HOMB);
              HOMB.setBounds(105, 10, 95, 19);

              //---- EXPB ----
              EXPB.setText("Exp\u00e9di\u00e9");
              EXPB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              EXPB.setName("EXPB");
              P_SEL0.add(EXPB);
              EXPB.setBounds(15, 10, 75, 19);

              //---- FACB ----
              FACB.setText("Factur\u00e9");
              FACB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              FACB.setName("FACB");
              P_SEL0.add(FACB);
              FACB.setBounds(210, 10, 80, 19);
            }
            xTitledPanel6ContentContainer.add(P_SEL0);
            P_SEL0.setBounds(190, 15, 423, 41);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel6ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel6ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel6ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel6ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel6ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel6);
          xTitledPanel6.setBounds(340, 370, 645, 105);

          //======== xTitledPanel11 ========
          {
            xTitledPanel11.setTitle("Repr\u00e9sentant");
            xTitledPanel11.setBorder(new DropShadowBorder());
            xTitledPanel11.setTitleFont(new Font("sansserif", Font.BOLD, 12));
            xTitledPanel11.setName("xTitledPanel11");
            Container xTitledPanel11ContentContainer = xTitledPanel11.getContentContainer();
            xTitledPanel11ContentContainer.setLayout(null);

            //---- WTOUV2 ----
            WTOUV2.setText("S\u00e9lection compl\u00e8te");
            WTOUV2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOUV2.setName("WTOUV2");
            xTitledPanel11ContentContainer.add(WTOUV2);
            WTOUV2.setBounds(15, 5, 140, 17);

            //---- OBJ_81 ----
            OBJ_81.setText("D\u00e9but");
            OBJ_81.setName("OBJ_81");
            xTitledPanel11ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(30, 45, 39, OBJ_81.getPreferredSize().height);

            //---- REPDEB ----
            REPDEB.setComponentPopupMenu(BTD);
            REPDEB.setName("REPDEB");
            xTitledPanel11ContentContainer.add(REPDEB);
            REPDEB.setBounds(80, 39, 36, REPDEB.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Fin");
            OBJ_83.setName("OBJ_83");
            xTitledPanel11ContentContainer.add(OBJ_83);
            OBJ_83.setBounds(180, 46, 21, 15);

            //---- REPFIN ----
            REPFIN.setComponentPopupMenu(BTD);
            REPFIN.setName("REPFIN");
            xTitledPanel11ContentContainer.add(REPFIN);
            REPFIN.setBounds(220, 39, 36, REPFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel11ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel11ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel11ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel11ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel11ContentContainer.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(xTitledPanel11);
          xTitledPanel11.setBounds(665, 250, 320, 105);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledPanel xTitledPanel3;
  private XRiCheckBox WTOUF;
  private JPanel P_SEL2;
  private XRiTextField FACDEB;
  private XRiTextField FACFIN;
  private JLabel OBJ_38;
  private JLabel OBJ_40;
  private JXTitledPanel xTitledPanel4;
  private XRiCheckBox WTOUB;
  private JPanel P_SEL1;
  private XRiTextField BONDEB;
  private XRiTextField BONFIN;
  private JLabel OBJ_29;
  private JLabel OBJ_31;
  private JXTitledPanel xTitledPanel9;
  private XRiCalendrier DTBDEB;
  private XRiCalendrier DTBFIN;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private JXTitledPanel xTitledPanel10;
  private XRiCheckBox WTOUM;
  private JPanel P_SEL3;
  private JLabel OBJ_52;
  private XRiTextField MAGDEB;
  private XRiTextField MAGFIN;
  private JLabel OBJ_55;
  private JXTitledPanel xTitledPanel7;
  private XRiCheckBox WTOUC;
  private JPanel P_SEL4;
  private XRiTextField CLIDEB;
  private XRiTextField CLIFIN;
  private JLabel OBJ_60;
  private JLabel OBJ_62;
  private JXTitledPanel xTitledPanel8;
  private XRiCheckBox WTOUV;
  private JLabel OBJ_80;
  private XRiTextField VNDDEB;
  private JLabel OBJ_82;
  private XRiTextField VNDFIN;
  private JXTitledPanel xTitledPanel5;
  private XRiCheckBox WTOUA;
  private JPanel P_SEL5;
  private XRiTextField AFFDEB;
  private XRiTextField AFFFIN;
  private JLabel OBJ_67;
  private JLabel OBJ_70;
  private JXTitledPanel xTitledPanel6;
  private XRiCheckBox WTOUE;
  private JPanel P_SEL0;
  private XRiCheckBox CPTB;
  private XRiCheckBox HOMB;
  private XRiCheckBox EXPB;
  private XRiCheckBox FACB;
  private JXTitledPanel xTitledPanel11;
  private XRiCheckBox WTOUV2;
  private JLabel OBJ_81;
  private XRiTextField REPDEB;
  private JLabel OBJ_83;
  private XRiTextField REPFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
