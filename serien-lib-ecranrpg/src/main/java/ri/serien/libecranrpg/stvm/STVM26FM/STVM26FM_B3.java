
package ri.serien.libecranrpg.stvm.STVM26FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STVM26FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _M01_Title = { "HM01", "Libellé", "Vente HT", "Nb.Bons", "V.Revient", "Marge", "Mrg.%", };
  private String[][] _M01_Data = { { "M01", "LE01", "F01", "B01", "G01", "H01", "J01", },
      { "M02", "LE02", "F02", "B02", "G02", "H02", "J02", }, { "M03", "LE03", "F03", "B03", "G03", "H03", "J03", },
      { "M04", "LE04", "F04", "B04", "G04", "H04", "J04", }, { "M05", "LE05", "F05", "B05", "G05", "H05", "J05", },
      { "M06", "LE06", "F06", "B06", "G06", "H06", "J06", }, { "M07", "LE07", "F07", "B07", "G07", "H07", "J07", },
      { "M08", "LE08", "F08", "B08", "G08", "H08", "J08", }, { "M09", "LE09", "F09", "B09", "G09", "H09", "J09", },
      { "M10", "LE10", "F10", "B10", "G10", "H10", "J10", }, { "M11", "LE11", "F11", "B11", "G11", "H11", "J11", },
      { "M12", "LE12", "F12", "B12", "G12", "H12", "J12", }, { "M13", "LE13", "F13", "B13", "G13", "H13", "J13", },
      { "M14", "LE14", "F14", "B14", "G14", "H14", "J14", }, { "M15", "LE15", "F15", "B15", "G15", "H15", "J15", }, };
  private int[] _M01_Width = { 26, 142, 68, 59, 67, 67, 53, };
  // private String[] _LIST_Top=null;
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT,
      SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private STVM26FM_G04 histo = null;
  private STVM26FM_G05 camem = null;
  
  public STVM26FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    M01.setAspectTable(null, _M01_Title, _M01_Data, _M01_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TF40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TF40@")).trim());
    TG40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TG40@")).trim());
    TH40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TH40@")).trim());
    TB40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TB40@")).trim());
    TJ40.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TJ40@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LIBPOS@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (histo != null) {
      histo.reveiller();
    }
    else {
      histo = new STVM26FM_G04(this, lexique, interpreteurD);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (camem != null) {
      camem.reveiller();
    }
    else {
      camem = new STVM26FM_G05(this, lexique, interpreteurD);
    }
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    M01 = new XRiTable();
    TF40 = new RiZoneSortie();
    TG40 = new RiZoneSortie();
    TH40 = new RiZoneSortie();
    TB40 = new RiZoneSortie();
    TJ40 = new RiZoneSortie();
    OBJ_25 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(840, 335));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Histogramme");
            riSousMenu_bt6.setToolTipText("Repr\u00e9sentation graphique sous forme d'histogramme");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("En parts");
            riSousMenu_bt7.setToolTipText("Repr\u00e9sentation graphique sous forme de camemberts");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- M01 ----
            M01.setName("M01");
            SCROLLPANE_LIST.setViewportView(M01);
          }
          panel2.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(5, 5, 600, 275);

          //---- TF40 ----
          TF40.setText("@TF40@");
          TF40.setHorizontalAlignment(SwingConstants.RIGHT);
          TF40.setName("TF40");
          panel2.add(TF40);
          TF40.setBounds(210, 285, 76, TF40.getPreferredSize().height);

          //---- TG40 ----
          TG40.setText("@TG40@");
          TG40.setHorizontalAlignment(SwingConstants.RIGHT);
          TG40.setName("TG40");
          panel2.add(TG40);
          TG40.setBounds(370, 285, 76, TG40.getPreferredSize().height);

          //---- TH40 ----
          TH40.setText("@TH40@");
          TH40.setHorizontalAlignment(SwingConstants.RIGHT);
          TH40.setName("TH40");
          panel2.add(TH40);
          TH40.setBounds(455, 285, 76, TH40.getPreferredSize().height);

          //---- TB40 ----
          TB40.setText("@TB40@");
          TB40.setHorizontalAlignment(SwingConstants.RIGHT);
          TB40.setName("TB40");
          panel2.add(TB40);
          TB40.setBounds(295, 285, 68, TB40.getPreferredSize().height);

          //---- TJ40 ----
          TJ40.setText("@TJ40@");
          TJ40.setHorizontalAlignment(SwingConstants.RIGHT);
          TJ40.setName("TJ40");
          panel2.add(TJ40);
          TJ40.setBounds(540, 285, 60, TJ40.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Total");
          OBJ_25.setFont(OBJ_25.getFont().deriveFont(OBJ_25.getFont().getStyle() | Font.BOLD));
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(55, 287, 155, 20);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          BT_PGUP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PGUPActionPerformed(e);
            }
          });
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(610, 5, 25, 135);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          BT_PGDOWN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PGDOWNActionPerformed(e);
            }
          });
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(610, 145, 25, 124);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(5, 5, 655, 320);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable M01;
  private RiZoneSortie TF40;
  private RiZoneSortie TG40;
  private RiZoneSortie TH40;
  private RiZoneSortie TB40;
  private RiZoneSortie TJ40;
  private JLabel OBJ_25;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
