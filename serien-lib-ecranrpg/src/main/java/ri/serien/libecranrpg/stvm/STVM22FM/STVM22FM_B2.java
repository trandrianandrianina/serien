
package ri.serien.libecranrpg.stvm.STVM22FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STVM22FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM22FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Devise : @LIBDEV@")).trim());
    LB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB01@")).trim());
    LB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB02@")).trim());
    LB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB03@")).trim());
    LB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB04@")).trim());
    LB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB05@")).trim());
    LB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB06@")).trim());
    LB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB07@")).trim());
    LB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB08@")).trim());
    LB09.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB09@")).trim());
    LB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB10@")).trim());
    LB11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB11@")).trim());
    LB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB13@")).trim());
    LB14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB14@")).trim());
    LB15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB15@")).trim());
    LB16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB16@")).trim());
    LB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB17@")).trim());
    LB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("          @LB18@")).trim());
    LB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB12@")).trim());
    NB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB01@")).trim());
    MT01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    MA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA01@")).trim());
    NB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB02@")).trim());
    MT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    MA2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA02@")).trim());
    NB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB03@")).trim());
    MT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    MA3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA03@")).trim());
    NB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB04@")).trim());
    MT4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    MA4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA04@")).trim());
    NB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB05@")).trim());
    MT5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05@")).trim());
    MA5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA05@")).trim());
    NB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB06@")).trim());
    MT6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06@")).trim());
    MA6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA06@")).trim());
    NB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB07@")).trim());
    MT7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    MA7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA07@")).trim());
    NB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB08@")).trim());
    MT8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    MA8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA08@")).trim());
    NB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB09@")).trim());
    MT9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
    MA9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA09@")).trim());
    NB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB10@")).trim());
    MT10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT10@")).trim());
    MA10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA10@")).trim());
    NB11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB11@")).trim());
    MT11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT11@")).trim());
    MA11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA11@")).trim());
    MT12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT12@")).trim());
    NB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB13@")).trim());
    MT13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT13@")).trim());
    MA13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA13@")).trim());
    NB14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB14@")).trim());
    MT14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT14@")).trim());
    MA14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA14@")).trim());
    NB15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB15@")).trim());
    MT15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT15@")).trim());
    MA15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA15@")).trim());
    NB16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB16@")).trim());
    MT16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT16@")).trim());
    MA16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA16@")).trim());
    NB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB17@")).trim());
    MT17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT17@")).trim());
    MA17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA17@")).trim());
    NB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NB18@")).trim());
    MT18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT18@")).trim());
    MA18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MA18@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    JButton[] tableauBoutons =
        { LB01, LB02, LB03, LB04, LB05, LB06, LB07, LB08, LB09, LB10, LB11, LB12, LB13, LB14, LB15, LB16, LB17, LB18 };
    
    
    for (int i = 0; i < tableauBoutons.length; i++) {
      tableauBoutons[i].setText(tableauBoutons[i].getText().replaceAll("_", "").trim());
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("SERIE M - Statistiques devis"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void runPopup(int lgn) {
    new OPTION_PANEL(this, lexique, interpreteurD, lgn);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    runPopup(4);
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    runPopup(5);
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    runPopup(6);
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    runPopup(7);
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    runPopup(8);
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    runPopup(9);
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    runPopup(10);
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    runPopup(11);
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    runPopup(12);
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    runPopup(13);
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    runPopup(14);
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    runPopup(15);
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    runPopup(16);
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    runPopup(17);
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    runPopup(18);
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    runPopup(19);
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    runPopup(20);
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    runPopup(21);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    z_etablissement_ = new RiZoneSortie();
    z_dgnom_ = new RiZoneSortie();
    label2 = new JLabel();
    p_tete_droite = new JPanel();
    label1 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LB01 = new JButton();
    LB02 = new JButton();
    LB03 = new JButton();
    LB04 = new JButton();
    LB05 = new JButton();
    LB06 = new JButton();
    LB07 = new JButton();
    LB08 = new JButton();
    LB09 = new JButton();
    LB10 = new JButton();
    LB11 = new JButton();
    LB13 = new JButton();
    LB14 = new JButton();
    LB15 = new JButton();
    LB16 = new JButton();
    LB17 = new JButton();
    LB18 = new JButton();
    LB12 = new JButton();
    NB01 = new RiZoneSortie();
    MT01 = new RiZoneSortie();
    MA01 = new RiZoneSortie();
    NB2 = new RiZoneSortie();
    MT2 = new RiZoneSortie();
    MA2 = new RiZoneSortie();
    NB3 = new RiZoneSortie();
    MT3 = new RiZoneSortie();
    MA3 = new RiZoneSortie();
    NB4 = new RiZoneSortie();
    MT4 = new RiZoneSortie();
    MA4 = new RiZoneSortie();
    NB5 = new RiZoneSortie();
    MT5 = new RiZoneSortie();
    MA5 = new RiZoneSortie();
    NB6 = new RiZoneSortie();
    MT6 = new RiZoneSortie();
    MA6 = new RiZoneSortie();
    NB7 = new RiZoneSortie();
    MT7 = new RiZoneSortie();
    MA7 = new RiZoneSortie();
    NB8 = new RiZoneSortie();
    MT8 = new RiZoneSortie();
    MA8 = new RiZoneSortie();
    NB9 = new RiZoneSortie();
    MT9 = new RiZoneSortie();
    MA9 = new RiZoneSortie();
    NB10 = new RiZoneSortie();
    MT10 = new RiZoneSortie();
    MA10 = new RiZoneSortie();
    NB11 = new RiZoneSortie();
    MT11 = new RiZoneSortie();
    MA11 = new RiZoneSortie();
    MT12 = new RiZoneSortie();
    NB13 = new RiZoneSortie();
    MT13 = new RiZoneSortie();
    MA13 = new RiZoneSortie();
    NB14 = new RiZoneSortie();
    MT14 = new RiZoneSortie();
    MA14 = new RiZoneSortie();
    NB15 = new RiZoneSortie();
    MT15 = new RiZoneSortie();
    MA15 = new RiZoneSortie();
    NB16 = new RiZoneSortie();
    MT16 = new RiZoneSortie();
    MA16 = new RiZoneSortie();
    NB17 = new RiZoneSortie();
    MT17 = new RiZoneSortie();
    MA17 = new RiZoneSortie();
    NB18 = new RiZoneSortie();
    MT18 = new RiZoneSortie();
    MA18 = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Nombre", SwingConstants.CENTER);
    separator2 = compFactory.createSeparator("Valeur", SwingConstants.CENTER);
    separator3 = compFactory.createSeparator("Marge", SwingConstants.CENTER);
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WSOC@");
          z_etablissement_.setOpaque(false);
          z_etablissement_.setName("z_etablissement_");
          p_tete_gauche.add(z_etablissement_);
          z_etablissement_.setBounds(100, 0, 40, z_etablissement_.getPreferredSize().height);

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setOpaque(false);
          z_dgnom_.setName("z_dgnom_");
          p_tete_gauche.add(z_dgnom_);
          z_dgnom_.setBounds(150, 0, 260, z_dgnom_.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Etablissement");
          label2.setName("label2");
          p_tete_gauche.add(label2);
          label2.setBounds(5, 0, label2.getPreferredSize().width, 24);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label1 ----
          label1.setText("Devise : @LIBDEV@");
          label1.setName("label1");
          p_tete_droite.add(label1);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 650));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LB01 ----
            LB01.setText("@LB01@");
            LB01.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB01.setBackground(new Color(240, 240, 228));
            LB01.setHorizontalAlignment(SwingConstants.LEFT);
            LB01.setFont(LB01.getFont().deriveFont(LB01.getFont().getStyle() | Font.BOLD));
            LB01.setName("LB01");
            LB01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_45ActionPerformed(e);
              }
            });
            panel1.add(LB01);
            LB01.setBounds(15, 45, 291, 30);

            //---- LB02 ----
            LB02.setText("@LB02@");
            LB02.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB02.setBackground(new Color(240, 240, 228));
            LB02.setHorizontalAlignment(SwingConstants.LEFT);
            LB02.setFont(LB02.getFont().deriveFont(LB02.getFont().getStyle() | Font.BOLD));
            LB02.setName("LB02");
            LB02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_50ActionPerformed(e);
              }
            });
            panel1.add(LB02);
            LB02.setBounds(15, 85, 291, 30);

            //---- LB03 ----
            LB03.setText("          @LB03@");
            LB03.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB03.setBackground(new Color(240, 240, 228));
            LB03.setHorizontalAlignment(SwingConstants.LEFT);
            LB03.setName("LB03");
            LB03.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_51ActionPerformed(e);
              }
            });
            panel1.add(LB03);
            LB03.setBounds(15, 112, 291, 30);

            //---- LB04 ----
            LB04.setText("          @LB04@");
            LB04.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB04.setBackground(new Color(240, 240, 228));
            LB04.setHorizontalAlignment(SwingConstants.LEFT);
            LB04.setName("LB04");
            LB04.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_52ActionPerformed(e);
              }
            });
            panel1.add(LB04);
            LB04.setBounds(15, 139, 291, 30);

            //---- LB05 ----
            LB05.setText("          @LB05@");
            LB05.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB05.setBackground(new Color(240, 240, 228));
            LB05.setHorizontalAlignment(SwingConstants.LEFT);
            LB05.setName("LB05");
            LB05.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_53ActionPerformed(e);
              }
            });
            panel1.add(LB05);
            LB05.setBounds(15, 166, 291, 30);

            //---- LB06 ----
            LB06.setText("@LB06@");
            LB06.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB06.setBackground(new Color(240, 240, 228));
            LB06.setHorizontalAlignment(SwingConstants.LEFT);
            LB06.setFont(LB06.getFont().deriveFont(LB06.getFont().getStyle() | Font.BOLD));
            LB06.setName("LB06");
            LB06.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_54ActionPerformed(e);
              }
            });
            panel1.add(LB06);
            LB06.setBounds(15, 205, 291, 30);

            //---- LB07 ----
            LB07.setText("          @LB07@");
            LB07.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB07.setBackground(new Color(240, 240, 228));
            LB07.setHorizontalAlignment(SwingConstants.LEFT);
            LB07.setName("LB07");
            LB07.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_55ActionPerformed(e);
              }
            });
            panel1.add(LB07);
            LB07.setBounds(15, 232, 291, 30);

            //---- LB08 ----
            LB08.setText("          @LB08@");
            LB08.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB08.setBackground(new Color(240, 240, 228));
            LB08.setHorizontalAlignment(SwingConstants.LEFT);
            LB08.setName("LB08");
            LB08.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_56ActionPerformed(e);
              }
            });
            panel1.add(LB08);
            LB08.setBounds(15, 259, 291, 30);

            //---- LB09 ----
            LB09.setText("          @LB09@");
            LB09.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB09.setBackground(new Color(240, 240, 228));
            LB09.setHorizontalAlignment(SwingConstants.LEFT);
            LB09.setName("LB09");
            LB09.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_57ActionPerformed(e);
              }
            });
            panel1.add(LB09);
            LB09.setBounds(15, 286, 291, 30);

            //---- LB10 ----
            LB10.setText("          @LB10@");
            LB10.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB10.setBackground(new Color(240, 240, 228));
            LB10.setHorizontalAlignment(SwingConstants.LEFT);
            LB10.setName("LB10");
            LB10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_58ActionPerformed(e);
              }
            });
            panel1.add(LB10);
            LB10.setBounds(15, 313, 291, 30);

            //---- LB11 ----
            LB11.setText("          @LB11@");
            LB11.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB11.setBackground(new Color(240, 240, 228));
            LB11.setHorizontalAlignment(SwingConstants.LEFT);
            LB11.setName("LB11");
            LB11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_59ActionPerformed(e);
              }
            });
            panel1.add(LB11);
            LB11.setBounds(15, 340, 291, 30);

            //---- LB13 ----
            LB13.setText("@LB13@");
            LB13.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB13.setBackground(new Color(240, 240, 228));
            LB13.setHorizontalAlignment(SwingConstants.LEFT);
            LB13.setFont(LB13.getFont().deriveFont(LB13.getFont().getStyle() | Font.BOLD));
            LB13.setName("LB13");
            LB13.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_61ActionPerformed(e);
              }
            });
            panel1.add(LB13);
            LB13.setBounds(15, 420, 291, 30);

            //---- LB14 ----
            LB14.setText("          @LB14@");
            LB14.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB14.setBackground(new Color(240, 240, 228));
            LB14.setHorizontalAlignment(SwingConstants.LEFT);
            LB14.setName("LB14");
            LB14.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_62ActionPerformed(e);
              }
            });
            panel1.add(LB14);
            LB14.setBounds(15, 447, 291, 30);

            //---- LB15 ----
            LB15.setText("          @LB15@");
            LB15.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB15.setBackground(new Color(240, 240, 228));
            LB15.setHorizontalAlignment(SwingConstants.LEFT);
            LB15.setName("LB15");
            LB15.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_63ActionPerformed(e);
              }
            });
            panel1.add(LB15);
            LB15.setBounds(15, 474, 291, 30);

            //---- LB16 ----
            LB16.setText("          @LB16@");
            LB16.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB16.setBackground(new Color(240, 240, 228));
            LB16.setHorizontalAlignment(SwingConstants.LEFT);
            LB16.setName("LB16");
            LB16.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_64ActionPerformed(e);
              }
            });
            panel1.add(LB16);
            LB16.setBounds(15, 501, 291, 30);

            //---- LB17 ----
            LB17.setText("          @LB17@");
            LB17.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB17.setBackground(new Color(240, 240, 228));
            LB17.setHorizontalAlignment(SwingConstants.LEFT);
            LB17.setName("LB17");
            LB17.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_65ActionPerformed(e);
              }
            });
            panel1.add(LB17);
            LB17.setBounds(15, 528, 291, 30);

            //---- LB18 ----
            LB18.setText("          @LB18@");
            LB18.setToolTipText("<HTML>Cliquez pour <BR>obtenir le d\u00e9tail</HTML>");
            LB18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LB18.setBackground(new Color(240, 240, 228));
            LB18.setHorizontalAlignment(SwingConstants.LEFT);
            LB18.setName("LB18");
            LB18.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_66ActionPerformed(e);
              }
            });
            panel1.add(LB18);
            LB18.setBounds(15, 555, 291, 30);

            //---- LB12 ----
            LB12.setText("@LB12@");
            LB12.setBackground(new Color(240, 240, 228));
            LB12.setHorizontalAlignment(SwingConstants.LEFT);
            LB12.setFont(LB12.getFont().deriveFont(LB12.getFont().getStyle() | Font.BOLD));
            LB12.setName("LB12");
            panel1.add(LB12);
            LB12.setBounds(15, 380, 291, 30);

            //---- NB01 ----
            NB01.setText("@NB01@");
            NB01.setHorizontalAlignment(SwingConstants.RIGHT);
            NB01.setName("NB01");
            panel1.add(NB01);
            NB01.setBounds(325, 48, 130, NB01.getPreferredSize().height);

            //---- MT01 ----
            MT01.setText("@MT01@");
            MT01.setHorizontalAlignment(SwingConstants.RIGHT);
            MT01.setName("MT01");
            panel1.add(MT01);
            MT01.setBounds(475, 48, 185, MT01.getPreferredSize().height);

            //---- MA01 ----
            MA01.setText("@MA01@");
            MA01.setHorizontalAlignment(SwingConstants.RIGHT);
            MA01.setName("MA01");
            panel1.add(MA01);
            MA01.setBounds(680, 48, 155, MA01.getPreferredSize().height);

            //---- NB2 ----
            NB2.setText("@NB02@");
            NB2.setHorizontalAlignment(SwingConstants.RIGHT);
            NB2.setName("NB2");
            panel1.add(NB2);
            NB2.setBounds(325, 88, 130, NB2.getPreferredSize().height);

            //---- MT2 ----
            MT2.setText("@MT02@");
            MT2.setHorizontalAlignment(SwingConstants.RIGHT);
            MT2.setName("MT2");
            panel1.add(MT2);
            MT2.setBounds(475, 88, 185, MT2.getPreferredSize().height);

            //---- MA2 ----
            MA2.setText("@MA02@");
            MA2.setHorizontalAlignment(SwingConstants.RIGHT);
            MA2.setName("MA2");
            panel1.add(MA2);
            MA2.setBounds(680, 88, 155, MA2.getPreferredSize().height);

            //---- NB3 ----
            NB3.setText("@NB03@");
            NB3.setHorizontalAlignment(SwingConstants.RIGHT);
            NB3.setName("NB3");
            panel1.add(NB3);
            NB3.setBounds(325, 115, 130, NB3.getPreferredSize().height);

            //---- MT3 ----
            MT3.setText("@MT03@");
            MT3.setHorizontalAlignment(SwingConstants.RIGHT);
            MT3.setName("MT3");
            panel1.add(MT3);
            MT3.setBounds(475, 115, 185, MT3.getPreferredSize().height);

            //---- MA3 ----
            MA3.setText("@MA03@");
            MA3.setHorizontalAlignment(SwingConstants.RIGHT);
            MA3.setName("MA3");
            panel1.add(MA3);
            MA3.setBounds(680, 115, 155, MA3.getPreferredSize().height);

            //---- NB4 ----
            NB4.setText("@NB04@");
            NB4.setHorizontalAlignment(SwingConstants.RIGHT);
            NB4.setName("NB4");
            panel1.add(NB4);
            NB4.setBounds(325, 142, 130, NB4.getPreferredSize().height);

            //---- MT4 ----
            MT4.setText("@MT04@");
            MT4.setHorizontalAlignment(SwingConstants.RIGHT);
            MT4.setName("MT4");
            panel1.add(MT4);
            MT4.setBounds(475, 142, 185, MT4.getPreferredSize().height);

            //---- MA4 ----
            MA4.setText("@MA04@");
            MA4.setHorizontalAlignment(SwingConstants.RIGHT);
            MA4.setName("MA4");
            panel1.add(MA4);
            MA4.setBounds(680, 142, 155, MA4.getPreferredSize().height);

            //---- NB5 ----
            NB5.setText("@NB05@");
            NB5.setHorizontalAlignment(SwingConstants.RIGHT);
            NB5.setName("NB5");
            panel1.add(NB5);
            NB5.setBounds(325, 169, 130, NB5.getPreferredSize().height);

            //---- MT5 ----
            MT5.setText("@MT05@");
            MT5.setHorizontalAlignment(SwingConstants.RIGHT);
            MT5.setName("MT5");
            panel1.add(MT5);
            MT5.setBounds(475, 169, 185, MT5.getPreferredSize().height);

            //---- MA5 ----
            MA5.setText("@MA05@");
            MA5.setHorizontalAlignment(SwingConstants.RIGHT);
            MA5.setName("MA5");
            panel1.add(MA5);
            MA5.setBounds(680, 169, 155, MA5.getPreferredSize().height);

            //---- NB6 ----
            NB6.setText("@NB06@");
            NB6.setHorizontalAlignment(SwingConstants.RIGHT);
            NB6.setName("NB6");
            panel1.add(NB6);
            NB6.setBounds(325, 208, 130, NB6.getPreferredSize().height);

            //---- MT6 ----
            MT6.setText("@MT06@");
            MT6.setHorizontalAlignment(SwingConstants.RIGHT);
            MT6.setName("MT6");
            panel1.add(MT6);
            MT6.setBounds(475, 208, 185, MT6.getPreferredSize().height);

            //---- MA6 ----
            MA6.setText("@MA06@");
            MA6.setHorizontalAlignment(SwingConstants.RIGHT);
            MA6.setName("MA6");
            panel1.add(MA6);
            MA6.setBounds(680, 208, 155, MA6.getPreferredSize().height);

            //---- NB7 ----
            NB7.setText("@NB07@");
            NB7.setHorizontalAlignment(SwingConstants.RIGHT);
            NB7.setName("NB7");
            panel1.add(NB7);
            NB7.setBounds(325, 235, 130, NB7.getPreferredSize().height);

            //---- MT7 ----
            MT7.setText("@MT07@");
            MT7.setHorizontalAlignment(SwingConstants.RIGHT);
            MT7.setName("MT7");
            panel1.add(MT7);
            MT7.setBounds(475, 235, 185, MT7.getPreferredSize().height);

            //---- MA7 ----
            MA7.setText("@MA07@");
            MA7.setHorizontalAlignment(SwingConstants.RIGHT);
            MA7.setName("MA7");
            panel1.add(MA7);
            MA7.setBounds(680, 235, 155, MA7.getPreferredSize().height);

            //---- NB8 ----
            NB8.setText("@NB08@");
            NB8.setHorizontalAlignment(SwingConstants.RIGHT);
            NB8.setName("NB8");
            panel1.add(NB8);
            NB8.setBounds(325, 262, 130, NB8.getPreferredSize().height);

            //---- MT8 ----
            MT8.setText("@MT08@");
            MT8.setHorizontalAlignment(SwingConstants.RIGHT);
            MT8.setName("MT8");
            panel1.add(MT8);
            MT8.setBounds(475, 262, 185, MT8.getPreferredSize().height);

            //---- MA8 ----
            MA8.setText("@MA08@");
            MA8.setHorizontalAlignment(SwingConstants.RIGHT);
            MA8.setName("MA8");
            panel1.add(MA8);
            MA8.setBounds(680, 262, 155, MA8.getPreferredSize().height);

            //---- NB9 ----
            NB9.setText("@NB09@");
            NB9.setHorizontalAlignment(SwingConstants.RIGHT);
            NB9.setName("NB9");
            panel1.add(NB9);
            NB9.setBounds(325, 289, 130, NB9.getPreferredSize().height);

            //---- MT9 ----
            MT9.setText("@MT09@");
            MT9.setHorizontalAlignment(SwingConstants.RIGHT);
            MT9.setName("MT9");
            panel1.add(MT9);
            MT9.setBounds(475, 289, 185, MT9.getPreferredSize().height);

            //---- MA9 ----
            MA9.setText("@MA09@");
            MA9.setHorizontalAlignment(SwingConstants.RIGHT);
            MA9.setName("MA9");
            panel1.add(MA9);
            MA9.setBounds(680, 289, 155, MA9.getPreferredSize().height);

            //---- NB10 ----
            NB10.setText("@NB10@");
            NB10.setHorizontalAlignment(SwingConstants.RIGHT);
            NB10.setName("NB10");
            panel1.add(NB10);
            NB10.setBounds(325, 316, 130, NB10.getPreferredSize().height);

            //---- MT10 ----
            MT10.setText("@MT10@");
            MT10.setHorizontalAlignment(SwingConstants.RIGHT);
            MT10.setName("MT10");
            panel1.add(MT10);
            MT10.setBounds(475, 316, 185, MT10.getPreferredSize().height);

            //---- MA10 ----
            MA10.setText("@MA10@");
            MA10.setHorizontalAlignment(SwingConstants.RIGHT);
            MA10.setName("MA10");
            panel1.add(MA10);
            MA10.setBounds(680, 316, 155, MA10.getPreferredSize().height);

            //---- NB11 ----
            NB11.setText("@NB11@");
            NB11.setHorizontalAlignment(SwingConstants.RIGHT);
            NB11.setName("NB11");
            panel1.add(NB11);
            NB11.setBounds(325, 343, 130, NB11.getPreferredSize().height);

            //---- MT11 ----
            MT11.setText("@MT11@");
            MT11.setHorizontalAlignment(SwingConstants.RIGHT);
            MT11.setName("MT11");
            panel1.add(MT11);
            MT11.setBounds(475, 343, 185, MT11.getPreferredSize().height);

            //---- MA11 ----
            MA11.setText("@MA11@");
            MA11.setHorizontalAlignment(SwingConstants.RIGHT);
            MA11.setName("MA11");
            panel1.add(MA11);
            MA11.setBounds(680, 343, 155, MA11.getPreferredSize().height);

            //---- MT12 ----
            MT12.setText("@MT12@");
            MT12.setHorizontalAlignment(SwingConstants.RIGHT);
            MT12.setName("MT12");
            panel1.add(MT12);
            MT12.setBounds(475, 383, 185, MT12.getPreferredSize().height);

            //---- NB13 ----
            NB13.setText("@NB13@");
            NB13.setHorizontalAlignment(SwingConstants.RIGHT);
            NB13.setName("NB13");
            panel1.add(NB13);
            NB13.setBounds(325, 423, 130, NB13.getPreferredSize().height);

            //---- MT13 ----
            MT13.setText("@MT13@");
            MT13.setHorizontalAlignment(SwingConstants.RIGHT);
            MT13.setName("MT13");
            panel1.add(MT13);
            MT13.setBounds(475, 423, 185, MT13.getPreferredSize().height);

            //---- MA13 ----
            MA13.setText("@MA13@");
            MA13.setHorizontalAlignment(SwingConstants.RIGHT);
            MA13.setName("MA13");
            panel1.add(MA13);
            MA13.setBounds(680, 423, 155, MA13.getPreferredSize().height);

            //---- NB14 ----
            NB14.setText("@NB14@");
            NB14.setHorizontalAlignment(SwingConstants.RIGHT);
            NB14.setName("NB14");
            panel1.add(NB14);
            NB14.setBounds(325, 450, 130, NB14.getPreferredSize().height);

            //---- MT14 ----
            MT14.setText("@MT14@");
            MT14.setHorizontalAlignment(SwingConstants.RIGHT);
            MT14.setName("MT14");
            panel1.add(MT14);
            MT14.setBounds(475, 450, 185, MT14.getPreferredSize().height);

            //---- MA14 ----
            MA14.setText("@MA14@");
            MA14.setHorizontalAlignment(SwingConstants.RIGHT);
            MA14.setName("MA14");
            panel1.add(MA14);
            MA14.setBounds(680, 450, 155, MA14.getPreferredSize().height);

            //---- NB15 ----
            NB15.setText("@NB15@");
            NB15.setHorizontalAlignment(SwingConstants.RIGHT);
            NB15.setName("NB15");
            panel1.add(NB15);
            NB15.setBounds(325, 477, 130, NB15.getPreferredSize().height);

            //---- MT15 ----
            MT15.setText("@MT15@");
            MT15.setHorizontalAlignment(SwingConstants.RIGHT);
            MT15.setName("MT15");
            panel1.add(MT15);
            MT15.setBounds(475, 477, 185, MT15.getPreferredSize().height);

            //---- MA15 ----
            MA15.setText("@MA15@");
            MA15.setHorizontalAlignment(SwingConstants.RIGHT);
            MA15.setName("MA15");
            panel1.add(MA15);
            MA15.setBounds(680, 477, 155, MA15.getPreferredSize().height);

            //---- NB16 ----
            NB16.setText("@NB16@");
            NB16.setHorizontalAlignment(SwingConstants.RIGHT);
            NB16.setName("NB16");
            panel1.add(NB16);
            NB16.setBounds(325, 504, 130, NB16.getPreferredSize().height);

            //---- MT16 ----
            MT16.setText("@MT16@");
            MT16.setHorizontalAlignment(SwingConstants.RIGHT);
            MT16.setName("MT16");
            panel1.add(MT16);
            MT16.setBounds(475, 504, 185, MT16.getPreferredSize().height);

            //---- MA16 ----
            MA16.setText("@MA16@");
            MA16.setHorizontalAlignment(SwingConstants.RIGHT);
            MA16.setName("MA16");
            panel1.add(MA16);
            MA16.setBounds(680, 504, 155, MA16.getPreferredSize().height);

            //---- NB17 ----
            NB17.setText("@NB17@");
            NB17.setHorizontalAlignment(SwingConstants.RIGHT);
            NB17.setName("NB17");
            panel1.add(NB17);
            NB17.setBounds(325, 531, 130, NB17.getPreferredSize().height);

            //---- MT17 ----
            MT17.setText("@MT17@");
            MT17.setHorizontalAlignment(SwingConstants.RIGHT);
            MT17.setName("MT17");
            panel1.add(MT17);
            MT17.setBounds(475, 531, 185, MT17.getPreferredSize().height);

            //---- MA17 ----
            MA17.setText("@MA17@");
            MA17.setHorizontalAlignment(SwingConstants.RIGHT);
            MA17.setName("MA17");
            panel1.add(MA17);
            MA17.setBounds(680, 531, 155, MA17.getPreferredSize().height);

            //---- NB18 ----
            NB18.setText("@NB18@");
            NB18.setHorizontalAlignment(SwingConstants.RIGHT);
            NB18.setName("NB18");
            panel1.add(NB18);
            NB18.setBounds(325, 558, 130, NB18.getPreferredSize().height);

            //---- MT18 ----
            MT18.setText("@MT18@");
            MT18.setHorizontalAlignment(SwingConstants.RIGHT);
            MT18.setName("MT18");
            panel1.add(MT18);
            MT18.setBounds(475, 558, 185, MT18.getPreferredSize().height);

            //---- MA18 ----
            MA18.setText("@MA18@");
            MA18.setHorizontalAlignment(SwingConstants.RIGHT);
            MA18.setName("MA18");
            panel1.add(MA18);
            MA18.setBounds(680, 558, 155, MA18.getPreferredSize().height);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(325, 25, 130, separator1.getPreferredSize().height);

            //---- separator2 ----
            separator2.setName("separator2");
            panel1.add(separator2);
            separator2.setBounds(475, 25, 185, separator2.getPreferredSize().height);

            //---- separator3 ----
            separator3.setName("separator3");
            panel1.add(separator3);
            separator3.setBounds(680, 25, 155, separator3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(5, -5, 850, 595);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie z_etablissement_;
  private RiZoneSortie z_dgnom_;
  private JLabel label2;
  private JPanel p_tete_droite;
  private JLabel label1;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton LB01;
  private JButton LB02;
  private JButton LB03;
  private JButton LB04;
  private JButton LB05;
  private JButton LB06;
  private JButton LB07;
  private JButton LB08;
  private JButton LB09;
  private JButton LB10;
  private JButton LB11;
  private JButton LB13;
  private JButton LB14;
  private JButton LB15;
  private JButton LB16;
  private JButton LB17;
  private JButton LB18;
  private JButton LB12;
  private RiZoneSortie NB01;
  private RiZoneSortie MT01;
  private RiZoneSortie MA01;
  private RiZoneSortie NB2;
  private RiZoneSortie MT2;
  private RiZoneSortie MA2;
  private RiZoneSortie NB3;
  private RiZoneSortie MT3;
  private RiZoneSortie MA3;
  private RiZoneSortie NB4;
  private RiZoneSortie MT4;
  private RiZoneSortie MA4;
  private RiZoneSortie NB5;
  private RiZoneSortie MT5;
  private RiZoneSortie MA5;
  private RiZoneSortie NB6;
  private RiZoneSortie MT6;
  private RiZoneSortie MA6;
  private RiZoneSortie NB7;
  private RiZoneSortie MT7;
  private RiZoneSortie MA7;
  private RiZoneSortie NB8;
  private RiZoneSortie MT8;
  private RiZoneSortie MA8;
  private RiZoneSortie NB9;
  private RiZoneSortie MT9;
  private RiZoneSortie MA9;
  private RiZoneSortie NB10;
  private RiZoneSortie MT10;
  private RiZoneSortie MA10;
  private RiZoneSortie NB11;
  private RiZoneSortie MT11;
  private RiZoneSortie MA11;
  private RiZoneSortie MT12;
  private RiZoneSortie NB13;
  private RiZoneSortie MT13;
  private RiZoneSortie MA13;
  private RiZoneSortie NB14;
  private RiZoneSortie MT14;
  private RiZoneSortie MA14;
  private RiZoneSortie NB15;
  private RiZoneSortie MT15;
  private RiZoneSortie MA15;
  private RiZoneSortie NB16;
  private RiZoneSortie MT16;
  private RiZoneSortie MA16;
  private RiZoneSortie NB17;
  private RiZoneSortie MT17;
  private RiZoneSortie MA17;
  private RiZoneSortie NB18;
  private RiZoneSortie MT18;
  private RiZoneSortie MA18;
  private JComponent separator1;
  private JComponent separator2;
  private JComponent separator3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
