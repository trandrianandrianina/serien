
package ri.serien.libecranrpg.stvm.STVM22FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class OPTION_PANEL extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private int ligne = 0;
  
  public OPTION_PANEL(JPanel panel, Lexical lex, iData iD, int lgn) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    ligne = lgn;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Autres options"));
  }
  
  public void getData() {
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  public void reveil() {
    setVisible(true);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 5);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 6);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 7);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 8);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button5ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 9);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button6ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 10);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void button7ActionPerformed(ActionEvent e) {
    setVisible(false);
    lexique.HostCursorPut(ligne, 11);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    panel3 = new JPanel();
    OBJ_10 = new JButton();
    P_Centre = new JPanel();
    panel2 = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    button6 = new JButton();
    button7 = new JButton();
    
    // ======== this ========
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== panel1 ========
    {
      panel1.setMinimumSize(new Dimension(35, 35));
      panel1.setPreferredSize(new Dimension(0, 45));
      panel1.setBackground(new Color(238, 238, 210));
      panel1.setName("panel1");
      
      // ======== panel3 ========
      {
        panel3.setOpaque(false);
        panel3.setName("panel3");
        panel3.setLayout(null);
        
        // ---- OBJ_10 ----
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setToolTipText("Retour");
        OBJ_10.setText("Retour");
        OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel3.add(OBJ_10);
        OBJ_10.setBounds(110, 5, 140, 40);
      }
      
      GroupLayout panel1Layout = new GroupLayout(panel1);
      panel1.setLayout(panel1Layout);
      panel1Layout.setHorizontalGroup(
          panel1Layout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING, panel1Layout.createSequentialGroup()
              .addContainerGap(69, Short.MAX_VALUE).addComponent(panel3, GroupLayout.PREFERRED_SIZE, 273, GroupLayout.PREFERRED_SIZE)));
      panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
          .addGroup(panel1Layout.createSequentialGroup().addComponent(panel3, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    }
    contentPane.add(panel1, BorderLayout.SOUTH);
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);
        
        // ---- button1 ----
        button1.setText("Par repr\u00e9sentants");
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed(e);
          }
        });
        panel2.add(button1);
        button1.setBounds(10, 5, 300, 35);
        
        // ---- button2 ----
        button2.setText("Par vendeurs");
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed(e);
          }
        });
        panel2.add(button2);
        button2.setBounds(10, 45, 300, 35);
        
        // ---- button3 ----
        button3.setText("Par d\u00e9partements");
        button3.setName("button3");
        button3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button3ActionPerformed(e);
          }
        });
        panel2.add(button3);
        button3.setBounds(10, 85, 300, 35);
        
        // ---- button4 ----
        button4.setText("par zones g\u00e9ographiques");
        button4.setName("button4");
        button4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button4ActionPerformed(e);
          }
        });
        panel2.add(button4);
        button4.setBounds(10, 125, 300, 35);
        
        // ---- button5 ----
        button5.setText("Par centrales");
        button5.setName("button5");
        button5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button5ActionPerformed(e);
          }
        });
        panel2.add(button5);
        button5.setBounds(10, 165, 300, 35);
        
        // ---- button6 ----
        button6.setText("Par motifs de devis perdus");
        button6.setName("button6");
        button6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button6ActionPerformed(e);
          }
        });
        panel2.add(button6);
        button6.setBounds(10, 205, 300, 35);
        
        // ---- button7 ----
        button7.setText("Par espoirs r\u00e9alisation");
        button7.setName("button7");
        button7.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button7ActionPerformed(e);
          }
        });
        panel2.add(button7);
        button7.setBounds(10, 245, 300, 35);
      }
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 320, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup().addGroup(GroupLayout.Alignment.TRAILING,
          P_CentreLayout.createSequentialGroup().addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 298, GroupLayout.PREFERRED_SIZE).addGap(62, 62, 62)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel panel1;
  private JPanel panel3;
  private JButton OBJ_10;
  private JPanel P_Centre;
  private JPanel panel2;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  private JButton button6;
  private JButton button7;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
