
package ri.serien.libecranrpg.stvm.STVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STVM18FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private STVM18FM_MNU mnu = null;
  
  public STVM18FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    // setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    LB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB01@")).trim());
    LB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB02@")).trim());
    LB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB03@")).trim());
    LB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB04@")).trim());
    LB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB05@")).trim());
    LB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB06@")).trim());
    LB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB07@")).trim());
    LB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB08@")).trim());
    LB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB12@")).trim());
    LB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB17@")).trim());
    LB18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT17@")).trim());
    LB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT01@")).trim());
    LB3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT02@")).trim());
    LB4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT03@")).trim());
    LB5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT04@")).trim());
    LB6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT05@")).trim());
    LB7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT06@")).trim());
    LB8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT07@")).trim());
    LB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT08@")).trim());
    LB13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT12@")).trim());
    LB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTT1@")).trim());
    LB14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MT09@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    LB1.setSelected(false);
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Tableau de bord commercial"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void LB17ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB17");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB17");
    }
  }
  
  private void LB01ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB01");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB01");
    }
  }
  
  private void LB02ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB02");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB02");
    }
  }
  
  private void LB03ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB03");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB03");
    }
  }
  
  private void LB04ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB04");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB04");
    }
  }
  
  private void LB05ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB05");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB05");
    }
  }
  
  private void LB06ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB06");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB06");
    }
  }
  
  private void LB11ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB09");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB09");
    }
  }
  
  private void LB07ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB07");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB07");
    }
  }
  
  private void LB08ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB08");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB08");
    }
  }
  
  private void LB12ActionPerformed(ActionEvent e) {
    if (mnu != null) {
      mnu.reveiller("LB12");
    }
    else {
      mnu = new STVM18FM_MNU(lexique, interpreteurD, "LB12");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_52 = new JLabel();
    WSOC = new XRiTextField();
    bouton_etablissement = new SNBoutonRecherche();
    riZoneSortie1 = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    LB01 = new JButton();
    LB02 = new JButton();
    LB03 = new JButton();
    LB04 = new JButton();
    LB05 = new JButton();
    LB06 = new JButton();
    LB07 = new JButton();
    LB08 = new JButton();
    LB12 = new JButton();
    LB17 = new JButton();
    LB1 = new JButton();
    LB18 = new RiZoneSortie();
    LB2 = new RiZoneSortie();
    LB3 = new RiZoneSortie();
    LB4 = new RiZoneSortie();
    LB5 = new RiZoneSortie();
    LB6 = new RiZoneSortie();
    LB7 = new RiZoneSortie();
    LB8 = new RiZoneSortie();
    LB9 = new RiZoneSortie();
    LB13 = new RiZoneSortie();
    LB10 = new RiZoneSortie();
    LB09 = new JButton();
    LB14 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord commercial");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_52 ----
          OBJ_52.setText("Societ\u00e9");
          OBJ_52.setName("OBJ_52");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@DGNOM@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_52))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recalcul");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- LB01 ----
            LB01.setText("@LB01@");
            LB01.setHorizontalAlignment(SwingConstants.LEFT);
            LB01.setBackground(new Color(239, 239, 222));
            LB01.setName("LB01");
            LB01.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB01ActionPerformed(e);
              }
            });
            panel1.add(LB01);
            LB01.setBounds(30, 10, 330, 28);

            //---- LB02 ----
            LB02.setText("@LB02@");
            LB02.setHorizontalAlignment(SwingConstants.LEFT);
            LB02.setBackground(new Color(239, 239, 222));
            LB02.setName("LB02");
            LB02.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB02ActionPerformed(e);
              }
            });
            panel1.add(LB02);
            LB02.setBounds(30, 40, 330, 28);

            //---- LB03 ----
            LB03.setText("@LB03@");
            LB03.setHorizontalAlignment(SwingConstants.LEFT);
            LB03.setBackground(new Color(239, 239, 222));
            LB03.setName("LB03");
            LB03.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB03ActionPerformed(e);
              }
            });
            panel1.add(LB03);
            LB03.setBounds(30, 70, 330, 28);

            //---- LB04 ----
            LB04.setText("@LB04@");
            LB04.setHorizontalAlignment(SwingConstants.LEFT);
            LB04.setBackground(new Color(239, 239, 222));
            LB04.setName("LB04");
            LB04.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB04ActionPerformed(e);
              }
            });
            panel1.add(LB04);
            LB04.setBounds(30, 98, 330, 28);

            //---- LB05 ----
            LB05.setText("@LB05@");
            LB05.setHorizontalAlignment(SwingConstants.LEFT);
            LB05.setBackground(new Color(239, 239, 222));
            LB05.setName("LB05");
            LB05.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB05ActionPerformed(e);
              }
            });
            panel1.add(LB05);
            LB05.setBounds(30, 268, 330, 28);

            //---- LB06 ----
            LB06.setText("@LB06@");
            LB06.setHorizontalAlignment(SwingConstants.LEFT);
            LB06.setBackground(new Color(239, 239, 222));
            LB06.setName("LB06");
            LB06.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB06ActionPerformed(e);
              }
            });
            panel1.add(LB06);
            LB06.setBounds(30, 298, 330, 28);

            //---- LB07 ----
            LB07.setText("@LB07@");
            LB07.setHorizontalAlignment(SwingConstants.LEFT);
            LB07.setBackground(new Color(239, 239, 222));
            LB07.setName("LB07");
            LB07.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB07ActionPerformed(e);
              }
            });
            panel1.add(LB07);
            LB07.setBounds(30, 388, 330, 28);

            //---- LB08 ----
            LB08.setText("@LB08@");
            LB08.setHorizontalAlignment(SwingConstants.LEFT);
            LB08.setBackground(new Color(239, 239, 222));
            LB08.setName("LB08");
            LB08.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB08ActionPerformed(e);
              }
            });
            panel1.add(LB08);
            LB08.setBounds(30, 448, 330, 28);

            //---- LB12 ----
            LB12.setText("@LB12@");
            LB12.setHorizontalAlignment(SwingConstants.LEFT);
            LB12.setBackground(new Color(239, 239, 222));
            LB12.setName("LB12");
            LB12.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB12ActionPerformed(e);
              }
            });
            panel1.add(LB12);
            LB12.setBounds(30, 508, 330, 28);

            //---- LB17 ----
            LB17.setText("@LB17@");
            LB17.setHorizontalAlignment(SwingConstants.LEFT);
            LB17.setBackground(new Color(239, 239, 222));
            LB17.setName("LB17");
            LB17.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB17ActionPerformed(e);
              }
            });
            panel1.add(LB17);
            LB17.setBounds(30, 220, 330, 28);

            //---- LB1 ----
            LB1.setText("CHIFFRE D'AFFAIRES");
            LB1.setHorizontalAlignment(SwingConstants.LEFT);
            LB1.setBackground(new Color(239, 239, 222));
            LB1.setFocusable(false);
            LB1.setFocusPainted(false);
            LB1.setVerifyInputWhenFocusTarget(false);
            LB1.setRolloverEnabled(false);
            LB1.setRequestFocusEnabled(false);
            LB1.setName("LB1");
            panel1.add(LB1);
            LB1.setBounds(30, 148, 330, 28);

            //---- LB18 ----
            LB18.setText("@MT17@");
            LB18.setHorizontalAlignment(SwingConstants.RIGHT);
            LB18.setName("LB18");
            panel1.add(LB18);
            LB18.setBounds(370, 222, 165, LB18.getPreferredSize().height);

            //---- LB2 ----
            LB2.setText("@MT01@");
            LB2.setHorizontalAlignment(SwingConstants.RIGHT);
            LB2.setName("LB2");
            panel1.add(LB2);
            LB2.setBounds(370, 12, 165, LB2.getPreferredSize().height);

            //---- LB3 ----
            LB3.setText("@MT02@");
            LB3.setHorizontalAlignment(SwingConstants.RIGHT);
            LB3.setName("LB3");
            panel1.add(LB3);
            LB3.setBounds(370, 40, 165, 28);

            //---- LB4 ----
            LB4.setText("@MT03@");
            LB4.setHorizontalAlignment(SwingConstants.RIGHT);
            LB4.setName("LB4");
            panel1.add(LB4);
            LB4.setBounds(370, 72, 165, LB4.getPreferredSize().height);

            //---- LB5 ----
            LB5.setText("@MT04@");
            LB5.setHorizontalAlignment(SwingConstants.RIGHT);
            LB5.setName("LB5");
            panel1.add(LB5);
            LB5.setBounds(370, 100, 165, LB5.getPreferredSize().height);

            //---- LB6 ----
            LB6.setText("@MT05@");
            LB6.setHorizontalAlignment(SwingConstants.RIGHT);
            LB6.setName("LB6");
            panel1.add(LB6);
            LB6.setBounds(370, 270, 165, LB6.getPreferredSize().height);

            //---- LB7 ----
            LB7.setText("@MT06@");
            LB7.setHorizontalAlignment(SwingConstants.RIGHT);
            LB7.setName("LB7");
            panel1.add(LB7);
            LB7.setBounds(370, 330, 165, LB7.getPreferredSize().height);

            //---- LB8 ----
            LB8.setText("@MT07@");
            LB8.setHorizontalAlignment(SwingConstants.RIGHT);
            LB8.setName("LB8");
            panel1.add(LB8);
            LB8.setBounds(370, 390, 165, LB8.getPreferredSize().height);

            //---- LB9 ----
            LB9.setText("@MT08@");
            LB9.setHorizontalAlignment(SwingConstants.RIGHT);
            LB9.setName("LB9");
            panel1.add(LB9);
            LB9.setBounds(370, 450, 165, LB9.getPreferredSize().height);

            //---- LB13 ----
            LB13.setText("@MT12@");
            LB13.setHorizontalAlignment(SwingConstants.RIGHT);
            LB13.setName("LB13");
            panel1.add(LB13);
            LB13.setBounds(370, 510, 165, LB13.getPreferredSize().height);

            //---- LB10 ----
            LB10.setText("@MTT1@");
            LB10.setHorizontalAlignment(SwingConstants.RIGHT);
            LB10.setName("LB10");
            panel1.add(LB10);
            LB10.setBounds(370, 150, 165, LB10.getPreferredSize().height);

            //---- LB09 ----
            LB09.setText("R\u00e9sultat pour l'ann\u00e9e");
            LB09.setHorizontalAlignment(SwingConstants.LEFT);
            LB09.setBackground(new Color(239, 239, 222));
            LB09.setName("LB09");
            LB09.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                LB11ActionPerformed(e);
              }
            });
            panel1.add(LB09);
            LB09.setBounds(30, 328, 330, 28);

            //---- LB14 ----
            LB14.setText("@MT09@");
            LB14.setHorizontalAlignment(SwingConstants.RIGHT);
            LB14.setName("LB14");
            panel1.add(LB14);
            LB14.setBounds(370, 300, 165, LB14.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("D\u00e9tail");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_52;
  private XRiTextField WSOC;
  private SNBoutonRecherche bouton_etablissement;
  private RiZoneSortie riZoneSortie1;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton LB01;
  private JButton LB02;
  private JButton LB03;
  private JButton LB04;
  private JButton LB05;
  private JButton LB06;
  private JButton LB07;
  private JButton LB08;
  private JButton LB12;
  private JButton LB17;
  private JButton LB1;
  private RiZoneSortie LB18;
  private RiZoneSortie LB2;
  private RiZoneSortie LB3;
  private RiZoneSortie LB4;
  private RiZoneSortie LB5;
  private RiZoneSortie LB6;
  private RiZoneSortie LB7;
  private RiZoneSortie LB8;
  private RiZoneSortie LB9;
  private RiZoneSortie LB13;
  private RiZoneSortie LB10;
  private JButton LB09;
  private RiZoneSortie LB14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
