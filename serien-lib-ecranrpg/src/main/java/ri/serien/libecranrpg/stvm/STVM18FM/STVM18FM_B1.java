
package ri.serien.libecranrpg.stvm.STVM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STVM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    T17.setValeursSelection("X", " ");
    T12.setValeursSelection("X", " ");
    T08.setValeursSelection("X", " ");
    T07.setValeursSelection("X", " ");
    T06.setValeursSelection("X", " ");
    T05.setValeursSelection("X", " ");
    T04.setValeursSelection("X", " ");
    T03.setValeursSelection("X", " ");
    T02.setValeursSelection("X", " ");
    T01.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    LB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB01@")).trim());
    LB02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB02@")).trim());
    LB03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB03@")).trim());
    LB04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB04@")).trim());
    LB05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB05@")).trim());
    LB06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB06@")).trim());
    LB07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB07@")).trim());
    LB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB08@")).trim());
    LB12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB12@")).trim());
    LB17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LB17@")).trim());
    D01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D01@")).trim());
    D02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D02@")).trim());
    D03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D03@")).trim());
    D04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D04@")).trim());
    D05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D05@")).trim());
    D06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D06@")).trim());
    D07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D07@")).trim());
    D08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D08@")).trim());
    D12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D12@")).trim());
    D17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@D17@")).trim());
    H01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H01@")).trim());
    H02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H02@")).trim());
    H03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H03@")).trim());
    H04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H04@")).trim());
    H5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H05@")).trim());
    H06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H06@")).trim());
    H7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H07@")).trim());
    H08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H08@")).trim());
    H12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H12@")).trim());
    H17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H17@")).trim());
    H201.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H201@")).trim());
    H202.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H202@")).trim());
    H203.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H203@")).trim());
    H204.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H204@")).trim());
    H206.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H206@")).trim());
    H208.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H208@")).trim());
    H212.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H212@")).trim());
    H217.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H217@")).trim());
    H205.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H205@")).trim());
    H207.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@H207@")).trim());
    P01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P01@")).trim());
    P02.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P02@")).trim());
    P03.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P03@")).trim());
    P04.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P04@")).trim());
    P05.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P05@")).trim());
    P06.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P06@")).trim());
    P07.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P07@")).trim());
    P08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P08@")).trim());
    P12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P12@")).trim());
    P17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@P17@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // T17.setSelected(lexique.HostFieldGetData("T17").equalsIgnoreCase("X"));
    // T12.setSelected(lexique.HostFieldGetData("T12").equalsIgnoreCase("X"));
    // T08.setSelected(lexique.HostFieldGetData("T08").equalsIgnoreCase("X"));
    // T07.setSelected(lexique.HostFieldGetData("T07").equalsIgnoreCase("X"));
    // T06.setSelected(lexique.HostFieldGetData("T06").equalsIgnoreCase("X"));
    // T05.setSelected(lexique.HostFieldGetData("T05").equalsIgnoreCase("X"));
    // T04.setSelected(lexique.HostFieldGetData("T04").equalsIgnoreCase("X"));
    // T03.setSelected(lexique.HostFieldGetData("T03").equalsIgnoreCase("X"));
    // T02.setSelected(lexique.HostFieldGetData("T02").equalsIgnoreCase("X"));
    // T01.setSelected(lexique.HostFieldGetData("T01").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @TITPG2@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (T17.isSelected())
    // lexique.HostFieldPutData("T17", 0, "X");
    // else
    // lexique.HostFieldPutData("T17", 0, " ");
    // if (T08.isSelected())
    // lexique.HostFieldPutData("T08", 0, "X");
    // else
    // lexique.HostFieldPutData("T08", 0, " ");
    // if (T07.isSelected())
    // lexique.HostFieldPutData("T07", 0, "X");
    // else
    // lexique.HostFieldPutData("T07", 0, " ");
    // if (T06.isSelected())
    // lexique.HostFieldPutData("T06", 0, "X");
    // else
    // lexique.HostFieldPutData("T06", 0, " ");
    // if (T05.isSelected())
    // lexique.HostFieldPutData("T05", 0, "X");
    // else
    // lexique.HostFieldPutData("T05", 0, " ");
    // if (T12.isSelected())
    // lexique.HostFieldPutData("T12", 0, "X");
    // else
    // lexique.HostFieldPutData("T12", 0, " ");
    // if (T04.isSelected())
    // lexique.HostFieldPutData("T04", 0, "X");
    // else
    // lexique.HostFieldPutData("T04", 0, " ");
    // if (T03.isSelected())
    // lexique.HostFieldPutData("T03", 0, "X");
    // else
    // lexique.HostFieldPutData("T03", 0, " ");
    // if (T02.isSelected())
    // lexique.HostFieldPutData("T02", 0, "X");
    // else
    // lexique.HostFieldPutData("T02", 0, " ");
    // if (T01.isSelected())
    // lexique.HostFieldPutData("T01", 0, "X");
    // else
    // lexique.HostFieldPutData("T01", 0, " ");
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WRECAL", 0, "**");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_52 = new JLabel();
    WSOC = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    p_tete_droite = new JPanel();
    label6 = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    T01 = new XRiCheckBox();
    T02 = new XRiCheckBox();
    T03 = new XRiCheckBox();
    T04 = new XRiCheckBox();
    T05 = new XRiCheckBox();
    T06 = new XRiCheckBox();
    T07 = new XRiCheckBox();
    T08 = new XRiCheckBox();
    T12 = new XRiCheckBox();
    T17 = new XRiCheckBox();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    LB01 = new RiZoneSortie();
    LB02 = new RiZoneSortie();
    LB03 = new RiZoneSortie();
    LB04 = new RiZoneSortie();
    LB05 = new RiZoneSortie();
    LB06 = new RiZoneSortie();
    LB07 = new RiZoneSortie();
    LB08 = new RiZoneSortie();
    LB12 = new RiZoneSortie();
    LB17 = new RiZoneSortie();
    D01 = new RiZoneSortie();
    D02 = new RiZoneSortie();
    D03 = new RiZoneSortie();
    D04 = new RiZoneSortie();
    D05 = new RiZoneSortie();
    D06 = new RiZoneSortie();
    D07 = new RiZoneSortie();
    D08 = new RiZoneSortie();
    D12 = new RiZoneSortie();
    D17 = new RiZoneSortie();
    H01 = new RiZoneSortie();
    H02 = new RiZoneSortie();
    H03 = new RiZoneSortie();
    H04 = new RiZoneSortie();
    H5 = new RiZoneSortie();
    H06 = new RiZoneSortie();
    H7 = new RiZoneSortie();
    H08 = new RiZoneSortie();
    H12 = new RiZoneSortie();
    H17 = new RiZoneSortie();
    H201 = new RiZoneSortie();
    H202 = new RiZoneSortie();
    H203 = new RiZoneSortie();
    H204 = new RiZoneSortie();
    H206 = new RiZoneSortie();
    H208 = new RiZoneSortie();
    H212 = new RiZoneSortie();
    H217 = new RiZoneSortie();
    H205 = new RiZoneSortie();
    H207 = new RiZoneSortie();
    P01 = new RiZoneSortie();
    P02 = new RiZoneSortie();
    P03 = new RiZoneSortie();
    P04 = new RiZoneSortie();
    P05 = new RiZoneSortie();
    P06 = new RiZoneSortie();
    P07 = new RiZoneSortie();
    P08 = new RiZoneSortie();
    P12 = new RiZoneSortie();
    P17 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_52 ----
          OBJ_52.setText("Societ\u00e9");
          OBJ_52.setName("OBJ_52");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@DGNOM@");
          riZoneSortie1.setOpaque(false);
          riZoneSortie1.setName("riZoneSortie1");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_52))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- label6 ----
          label6.setText("@WENCX@");
          label6.setName("label6");
          p_tete_droite.add(label6);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recalcul g\u00e9n\u00e9ral");
              riSousMenu_bt6.setToolTipText("Recalcul de tous les postes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Etat de mise \u00e0 jour du tableau de bord"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- T01 ----
            T01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T01.setName("T01");
            panel1.add(T01);
            T01.setBounds(15, 53, 20, 24);

            //---- T02 ----
            T02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T02.setName("T02");
            panel1.add(T02);
            T02.setBounds(15, 86, 20, 24);

            //---- T03 ----
            T03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T03.setName("T03");
            panel1.add(T03);
            T03.setBounds(15, 115, 20, 24);

            //---- T04 ----
            T04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T04.setName("T04");
            panel1.add(T04);
            T04.setBounds(15, 146, 20, 24);

            //---- T05 ----
            T05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T05.setName("T05");
            panel1.add(T05);
            T05.setBounds(15, 176, 20, 24);

            //---- T06 ----
            T06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T06.setName("T06");
            panel1.add(T06);
            T06.setBounds(15, 205, 20, 24);

            //---- T07 ----
            T07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T07.setName("T07");
            panel1.add(T07);
            T07.setBounds(15, 235, 20, 24);

            //---- T08 ----
            T08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T08.setName("T08");
            panel1.add(T08);
            T08.setBounds(15, 265, 20, 24);

            //---- T12 ----
            T12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T12.setName("T12");
            panel1.add(T12);
            T12.setBounds(15, 329, 20, 24);

            //---- T17 ----
            T17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            T17.setName("T17");
            panel1.add(T17);
            T17.setBounds(15, 385, 20, 24);

            //---- label1 ----
            label1.setText("Poste du tableau de bord");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(41, 30, 330, 25);

            //---- label2 ----
            label2.setText("Date");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(377, 30, 70, 25);

            //---- label3 ----
            label3.setText("D\u00e9but");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(473, 30, 70, 25);

            //---- label4 ----
            label4.setText("Fin");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(564, 30, 85, 25);

            //---- label5 ----
            label5.setText("Temps");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(655, 30, 85, 25);

            //---- LB01 ----
            LB01.setText("@LB01@");
            LB01.setName("LB01");
            panel1.add(LB01);
            LB01.setBounds(41, 53, 330, LB01.getPreferredSize().height);

            //---- LB02 ----
            LB02.setText("@LB02@");
            LB02.setName("LB02");
            panel1.add(LB02);
            LB02.setBounds(41, 84, 330, 28);

            //---- LB03 ----
            LB03.setText("@LB03@");
            LB03.setName("LB03");
            panel1.add(LB03);
            LB03.setBounds(41, 115, 330, LB03.getPreferredSize().height);

            //---- LB04 ----
            LB04.setText("@LB04@");
            LB04.setName("LB04");
            panel1.add(LB04);
            LB04.setBounds(41, 146, 330, LB04.getPreferredSize().height);

            //---- LB05 ----
            LB05.setText("@LB05@");
            LB05.setName("LB05");
            panel1.add(LB05);
            LB05.setBounds(41, 176, 330, LB05.getPreferredSize().height);

            //---- LB06 ----
            LB06.setText("@LB06@");
            LB06.setName("LB06");
            panel1.add(LB06);
            LB06.setBounds(41, 205, 330, LB06.getPreferredSize().height);

            //---- LB07 ----
            LB07.setText("@LB07@");
            LB07.setName("LB07");
            panel1.add(LB07);
            LB07.setBounds(41, 235, 330, LB07.getPreferredSize().height);

            //---- LB08 ----
            LB08.setText("@LB08@");
            LB08.setName("LB08");
            panel1.add(LB08);
            LB08.setBounds(41, 265, 330, LB08.getPreferredSize().height);

            //---- LB12 ----
            LB12.setText("@LB12@");
            LB12.setName("LB12");
            panel1.add(LB12);
            LB12.setBounds(41, 329, 330, LB12.getPreferredSize().height);

            //---- LB17 ----
            LB17.setText("@LB17@");
            LB17.setName("LB17");
            panel1.add(LB17);
            LB17.setBounds(41, 385, 330, LB17.getPreferredSize().height);

            //---- D01 ----
            D01.setText("@D01@");
            D01.setHorizontalAlignment(SwingConstants.CENTER);
            D01.setName("D01");
            panel1.add(D01);
            D01.setBounds(377, 53, 90, D01.getPreferredSize().height);

            //---- D02 ----
            D02.setText("@D02@");
            D02.setHorizontalAlignment(SwingConstants.CENTER);
            D02.setName("D02");
            panel1.add(D02);
            D02.setBounds(377, 84, 90, D02.getPreferredSize().height);

            //---- D03 ----
            D03.setText("@D03@");
            D03.setHorizontalAlignment(SwingConstants.CENTER);
            D03.setName("D03");
            panel1.add(D03);
            D03.setBounds(377, 115, 90, D03.getPreferredSize().height);

            //---- D04 ----
            D04.setText("@D04@");
            D04.setHorizontalAlignment(SwingConstants.CENTER);
            D04.setName("D04");
            panel1.add(D04);
            D04.setBounds(377, 146, 90, D04.getPreferredSize().height);

            //---- D05 ----
            D05.setText("@D05@");
            D05.setHorizontalAlignment(SwingConstants.CENTER);
            D05.setName("D05");
            panel1.add(D05);
            D05.setBounds(377, 176, 90, D05.getPreferredSize().height);

            //---- D06 ----
            D06.setText("@D06@");
            D06.setHorizontalAlignment(SwingConstants.CENTER);
            D06.setName("D06");
            panel1.add(D06);
            D06.setBounds(377, 205, 90, D06.getPreferredSize().height);

            //---- D07 ----
            D07.setText("@D07@");
            D07.setHorizontalAlignment(SwingConstants.CENTER);
            D07.setName("D07");
            panel1.add(D07);
            D07.setBounds(377, 235, 90, D07.getPreferredSize().height);

            //---- D08 ----
            D08.setText("@D08@");
            D08.setHorizontalAlignment(SwingConstants.CENTER);
            D08.setName("D08");
            panel1.add(D08);
            D08.setBounds(377, 265, 90, D08.getPreferredSize().height);

            //---- D12 ----
            D12.setText("@D12@");
            D12.setHorizontalAlignment(SwingConstants.CENTER);
            D12.setName("D12");
            panel1.add(D12);
            D12.setBounds(377, 329, 90, D12.getPreferredSize().height);

            //---- D17 ----
            D17.setText("@D17@");
            D17.setHorizontalAlignment(SwingConstants.CENTER);
            D17.setName("D17");
            panel1.add(D17);
            D17.setBounds(377, 385, 90, D17.getPreferredSize().height);

            //---- H01 ----
            H01.setText("@H01@");
            H01.setHorizontalAlignment(SwingConstants.CENTER);
            H01.setName("H01");
            panel1.add(H01);
            H01.setBounds(473, 53, 85, H01.getPreferredSize().height);

            //---- H02 ----
            H02.setText("@H02@");
            H02.setHorizontalAlignment(SwingConstants.CENTER);
            H02.setName("H02");
            panel1.add(H02);
            H02.setBounds(473, 84, 85, H02.getPreferredSize().height);

            //---- H03 ----
            H03.setText("@H03@");
            H03.setHorizontalAlignment(SwingConstants.CENTER);
            H03.setName("H03");
            panel1.add(H03);
            H03.setBounds(473, 115, 85, H03.getPreferredSize().height);

            //---- H04 ----
            H04.setText("@H04@");
            H04.setHorizontalAlignment(SwingConstants.CENTER);
            H04.setName("H04");
            panel1.add(H04);
            H04.setBounds(473, 146, 85, H04.getPreferredSize().height);

            //---- H5 ----
            H5.setText("@H05@");
            H5.setHorizontalAlignment(SwingConstants.CENTER);
            H5.setName("H5");
            panel1.add(H5);
            H5.setBounds(473, 176, 85, H5.getPreferredSize().height);

            //---- H06 ----
            H06.setText("@H06@");
            H06.setHorizontalAlignment(SwingConstants.CENTER);
            H06.setName("H06");
            panel1.add(H06);
            H06.setBounds(473, 205, 85, H06.getPreferredSize().height);

            //---- H7 ----
            H7.setText("@H07@");
            H7.setHorizontalAlignment(SwingConstants.CENTER);
            H7.setName("H7");
            panel1.add(H7);
            H7.setBounds(473, 235, 85, H7.getPreferredSize().height);

            //---- H08 ----
            H08.setText("@H08@");
            H08.setHorizontalAlignment(SwingConstants.CENTER);
            H08.setName("H08");
            panel1.add(H08);
            H08.setBounds(473, 265, 85, H08.getPreferredSize().height);

            //---- H12 ----
            H12.setText("@H12@");
            H12.setHorizontalAlignment(SwingConstants.CENTER);
            H12.setName("H12");
            panel1.add(H12);
            H12.setBounds(473, 329, 85, H12.getPreferredSize().height);

            //---- H17 ----
            H17.setText("@H17@");
            H17.setHorizontalAlignment(SwingConstants.CENTER);
            H17.setName("H17");
            panel1.add(H17);
            H17.setBounds(473, 385, 85, H17.getPreferredSize().height);

            //---- H201 ----
            H201.setText("@H201@");
            H201.setHorizontalAlignment(SwingConstants.CENTER);
            H201.setName("H201");
            panel1.add(H201);
            H201.setBounds(564, 53, 85, H201.getPreferredSize().height);

            //---- H202 ----
            H202.setText("@H202@");
            H202.setHorizontalAlignment(SwingConstants.CENTER);
            H202.setName("H202");
            panel1.add(H202);
            H202.setBounds(564, 84, 85, H202.getPreferredSize().height);

            //---- H203 ----
            H203.setText("@H203@");
            H203.setHorizontalAlignment(SwingConstants.CENTER);
            H203.setName("H203");
            panel1.add(H203);
            H203.setBounds(564, 115, 85, H203.getPreferredSize().height);

            //---- H204 ----
            H204.setText("@H204@");
            H204.setHorizontalAlignment(SwingConstants.CENTER);
            H204.setName("H204");
            panel1.add(H204);
            H204.setBounds(564, 146, 85, H204.getPreferredSize().height);

            //---- H206 ----
            H206.setText("@H206@");
            H206.setHorizontalAlignment(SwingConstants.CENTER);
            H206.setName("H206");
            panel1.add(H206);
            H206.setBounds(564, 205, 85, H206.getPreferredSize().height);

            //---- H208 ----
            H208.setText("@H208@");
            H208.setHorizontalAlignment(SwingConstants.CENTER);
            H208.setName("H208");
            panel1.add(H208);
            H208.setBounds(564, 265, 85, H208.getPreferredSize().height);

            //---- H212 ----
            H212.setText("@H212@");
            H212.setHorizontalAlignment(SwingConstants.CENTER);
            H212.setName("H212");
            panel1.add(H212);
            H212.setBounds(564, 329, 85, H212.getPreferredSize().height);

            //---- H217 ----
            H217.setText("@H217@");
            H217.setHorizontalAlignment(SwingConstants.CENTER);
            H217.setName("H217");
            panel1.add(H217);
            H217.setBounds(564, 385, 85, H217.getPreferredSize().height);

            //---- H205 ----
            H205.setText("@H205@");
            H205.setHorizontalAlignment(SwingConstants.CENTER);
            H205.setName("H205");
            panel1.add(H205);
            H205.setBounds(564, 176, 85, H205.getPreferredSize().height);

            //---- H207 ----
            H207.setText("@H207@");
            H207.setHorizontalAlignment(SwingConstants.CENTER);
            H207.setName("H207");
            panel1.add(H207);
            H207.setBounds(564, 235, 85, H207.getPreferredSize().height);

            //---- P01 ----
            P01.setText("@P01@");
            P01.setName("P01");
            panel1.add(P01);
            P01.setBounds(655, 53, 150, P01.getPreferredSize().height);

            //---- P02 ----
            P02.setText("@P02@");
            P02.setName("P02");
            panel1.add(P02);
            P02.setBounds(655, 84, 150, P02.getPreferredSize().height);

            //---- P03 ----
            P03.setText("@P03@");
            P03.setName("P03");
            panel1.add(P03);
            P03.setBounds(655, 115, 150, P03.getPreferredSize().height);

            //---- P04 ----
            P04.setText("@P04@");
            P04.setName("P04");
            panel1.add(P04);
            P04.setBounds(655, 146, 150, P04.getPreferredSize().height);

            //---- P05 ----
            P05.setText("@P05@");
            P05.setName("P05");
            panel1.add(P05);
            P05.setBounds(655, 176, 150, P05.getPreferredSize().height);

            //---- P06 ----
            P06.setText("@P06@");
            P06.setName("P06");
            panel1.add(P06);
            P06.setBounds(655, 205, 150, P06.getPreferredSize().height);

            //---- P07 ----
            P07.setText("@P07@");
            P07.setName("P07");
            panel1.add(P07);
            P07.setBounds(655, 235, 150, P07.getPreferredSize().height);

            //---- P08 ----
            P08.setText("@P08@");
            P08.setName("P08");
            panel1.add(P08);
            P08.setBounds(655, 265, 150, P08.getPreferredSize().height);

            //---- P12 ----
            P12.setText("@P12@");
            P12.setName("P12");
            panel1.add(P12);
            P12.setBounds(655, 329, 150, P12.getPreferredSize().height);

            //---- P17 ----
            P17.setText("@P17@");
            P17.setName("P17");
            panel1.add(P17);
            P17.setBounds(655, 385, 150, P17.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 845, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 437, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_52;
  private XRiTextField WSOC;
  private RiZoneSortie riZoneSortie1;
  private SNBoutonRecherche bouton_etablissement;
  private JPanel p_tete_droite;
  private JLabel label6;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiCheckBox T01;
  private XRiCheckBox T02;
  private XRiCheckBox T03;
  private XRiCheckBox T04;
  private XRiCheckBox T05;
  private XRiCheckBox T06;
  private XRiCheckBox T07;
  private XRiCheckBox T08;
  private XRiCheckBox T12;
  private XRiCheckBox T17;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private RiZoneSortie LB01;
  private RiZoneSortie LB02;
  private RiZoneSortie LB03;
  private RiZoneSortie LB04;
  private RiZoneSortie LB05;
  private RiZoneSortie LB06;
  private RiZoneSortie LB07;
  private RiZoneSortie LB08;
  private RiZoneSortie LB12;
  private RiZoneSortie LB17;
  private RiZoneSortie D01;
  private RiZoneSortie D02;
  private RiZoneSortie D03;
  private RiZoneSortie D04;
  private RiZoneSortie D05;
  private RiZoneSortie D06;
  private RiZoneSortie D07;
  private RiZoneSortie D08;
  private RiZoneSortie D12;
  private RiZoneSortie D17;
  private RiZoneSortie H01;
  private RiZoneSortie H02;
  private RiZoneSortie H03;
  private RiZoneSortie H04;
  private RiZoneSortie H5;
  private RiZoneSortie H06;
  private RiZoneSortie H7;
  private RiZoneSortie H08;
  private RiZoneSortie H12;
  private RiZoneSortie H17;
  private RiZoneSortie H201;
  private RiZoneSortie H202;
  private RiZoneSortie H203;
  private RiZoneSortie H204;
  private RiZoneSortie H206;
  private RiZoneSortie H208;
  private RiZoneSortie H212;
  private RiZoneSortie H217;
  private RiZoneSortie H205;
  private RiZoneSortie H207;
  private RiZoneSortie P01;
  private RiZoneSortie P02;
  private RiZoneSortie P03;
  private RiZoneSortie P04;
  private RiZoneSortie P05;
  private RiZoneSortie P06;
  private RiZoneSortie P07;
  private RiZoneSortie P08;
  private RiZoneSortie P12;
  private RiZoneSortie P17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
