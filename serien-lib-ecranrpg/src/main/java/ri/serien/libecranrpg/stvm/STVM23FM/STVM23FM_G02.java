
package ri.serien.libecranrpg.stvm.STVM23FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STVM23FM_G02 extends JDialog {
   
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private String typeZone = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  // chiffre d'affaire
  private String[] ca1 = new String[17];
  private String[] ligneDonnee = new String[15];
  private String[] libelle = new String[15];
  
  public STVM23FM_G02(JPanel panel, Lexical lex, iData iD, String type) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    typeZone = type;
    initComponents();
    setData(typeZone);
    setVisible(true);
  }
  
  public void setData(String tz) {
    
    // Valeur
    // label2.setText(lexique.HostFieldGetData("MG01"));
    
    // GRAPHE
    
    String[] libelle = new String[15];
    String[] ligneDonnee = new String[15];
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("LI" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
    }
    
    // Chargement des données
    for (int i = 0; i < ligneDonnee.length; i++) {
      
      // on charge toute les zones
      ligneDonnee[i] = lexique.HostFieldGetNumericData(tz + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
    }
    
    // Préparation des données
    Object[][] data = new Object[libelle.length][2];
    
    for (int i = 0; i < libelle.length; i++) {
      data[i][0] = libelle[i];
      ligneDonnee[i] = ligneDonnee[i].replaceAll("\\s", "0");
      data[i][1] = Double.parseDouble(ligneDonnee[i]);
    }
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe("Répartition par " + lexique.HostFieldGetData("LIBTIT"), false);
    
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression(lexique.HostFieldGetData("LIBPOS")));
  }
  
  public void getData() {
    
  }
  
  public void reveiller(String typeZ) {
    setData(typeZ);
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    OBJ_10 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());

    //======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");

      //---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");

      //---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });

      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 1080, Short.MAX_VALUE))
          .addGroup(GroupLayout.Alignment.TRAILING, P_CentreLayout.createSequentialGroup()
            .addContainerGap(940, Short.MAX_VALUE)
            .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
            .addContainerGap())
      );
      P_CentreLayout.setVerticalGroup(
        P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
            .addGap(15, 15, 15)
            .addComponent(l_graphe, GroupLayout.DEFAULT_SIZE, 593, Short.MAX_VALUE)
            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
            .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
            .addGap(8, 8, 8))
      );
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
