
package ri.serien.libecranrpg.stvm.STVM19FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STVM19FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _B4NB02_Title = { "LIBDEV", "Nombre", "CA.en cours", "Marge", "Nb.Fac", "Fact.moy.", };
  private String[][] _B4NB02_Data = { { "24 derniers mois", "B4NB02", "B4CA02", "B4MA02", "B4FA02", "B4MY02", },
      { "12 derniers mois", "B4NB03", "B4CA03", "B4MA03", "B4FA03", "B4MY03", },
      { "6 derniers mois", "B4NB04", "B4CA04", "B4MA04", "B4FA04", "B4MY04", },
      { "3 derniers mois", "B4NB05", "B4CA05", "B4MA05", "B4FA05", "B4MY05", },
      { "2 derniers mois", "B4NB06", "B4CA06", "B4MA06", "B4FA06", "B4MY06", },
      { "Dernier mois", "B4NB07", "B4CA07", "B4MA07", "B4FA07", "B4MY07", }, };
  private int[] _B4NB02_Width = { 143, 47, 89, 88, 46, 72, };
  private int[] _LIST_Justification =
      { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public STVM19FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    B4NB02.setAspectTable(null, _B4NB02_Title, _B4NB02_Data, _B4NB02_Width, false, _LIST_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_15.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBPOS@")).trim()));
    B4NB01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4NB01@")).trim());
    B4CA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4CA01@")).trim());
    B4MA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4MA01@")).trim());
    B4MY01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4MY01@")).trim());
    B4FA01.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4FA01@")).trim());
    B4NB08.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4NB08@")).trim());
    B4NB9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4NB09@")).trim());
    B4NB10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@B4NB10@")).trim());
    NUREPR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NUREPR@")).trim());
    LIREPR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIREPR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), LIST_Top, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    NUREPR.setVisible(lexique.isPresent("NUREPR"));
    LIREPR.setVisible(lexique.isPresent("LIREPR"));
    
    // TODO Icones
    BT_PGDOWN.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    BT_PGUP.setIcon(lexique.chargerImage("images/pgup20.png", true));
    ;
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Synthèse"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2", true);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_15 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    SCROLLPANE_LIST = new JScrollPane();
    B4NB02 = new XRiTable();
    label1 = new JLabel();
    OBJ_39 = new JLabel();
    B4NB01 = new RiZoneSortie();
    B4CA01 = new RiZoneSortie();
    B4MA01 = new RiZoneSortie();
    B4MY01 = new RiZoneSortie();
    B4FA01 = new RiZoneSortie();
    OBJ_16 = new JPanel();
    B4NB08 = new RiZoneSortie();
    OBJ_36 = new JLabel();
    B4NB9 = new RiZoneSortie();
    B4NB10 = new RiZoneSortie();
    OBJ_37 = new JLabel();
    OBJ_38 = new JLabel();
    NUREPR = new RiZoneSortie();
    LIREPR = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_10 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(970, 380));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Repr\u00e9sentation graphique");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_15 ========
        {
          OBJ_15.setBorder(new TitledBorder("@LIBPOS@"));
          OBJ_15.setOpaque(false);
          OBJ_15.setName("OBJ_15");
          OBJ_15.setLayout(null);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          BT_PGUP.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PGUPActionPerformed(e);
            }
          });
          OBJ_15.add(BT_PGUP);
          BT_PGUP.setBounds(740, 90, 25, 60);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          BT_PGDOWN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_PGDOWNActionPerformed(e);
            }
          });
          OBJ_15.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(740, 160, 25, 60);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- B4NB02 ----
            B4NB02.setName("B4NB02");
            SCROLLPANE_LIST.setViewportView(B4NB02);
          }
          OBJ_15.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(15, 90, 715, 130);

          //---- label1 ----
          label1.setText("Nombre de clients factur\u00e9s");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          OBJ_15.add(label1);
          label1.setBounds(15, 70, 265, label1.getPreferredSize().height);

          //---- OBJ_39 ----
          OBJ_39.setText("Nombre total de clients");
          OBJ_39.setName("OBJ_39");
          OBJ_15.add(OBJ_39);
          OBJ_39.setBounds(15, 30, 175, 20);

          //---- B4NB01 ----
          B4NB01.setText("@B4NB01@");
          B4NB01.setHorizontalAlignment(SwingConstants.RIGHT);
          B4NB01.setName("B4NB01");
          OBJ_15.add(B4NB01);
          B4NB01.setBounds(195, 28, 75, B4NB01.getPreferredSize().height);

          //---- B4CA01 ----
          B4CA01.setText("@B4CA01@");
          B4CA01.setHorizontalAlignment(SwingConstants.RIGHT);
          B4CA01.setName("B4CA01");
          OBJ_15.add(B4CA01);
          B4CA01.setBounds(291, 28, 100, B4CA01.getPreferredSize().height);

          //---- B4MA01 ----
          B4MA01.setText("@B4MA01@");
          B4MA01.setHorizontalAlignment(SwingConstants.RIGHT);
          B4MA01.setName("B4MA01");
          OBJ_15.add(B4MA01);
          B4MA01.setBounds(new Rectangle(new Point(412, 28), B4MA01.getPreferredSize()));

          //---- B4MY01 ----
          B4MY01.setText("@B4MY01@");
          B4MY01.setHorizontalAlignment(SwingConstants.RIGHT);
          B4MY01.setName("B4MY01");
          OBJ_15.add(B4MY01);
          B4MY01.setBounds(new Rectangle(new Point(629, 28), B4MY01.getPreferredSize()));

          //---- B4FA01 ----
          B4FA01.setText("@B4FA01@");
          B4FA01.setHorizontalAlignment(SwingConstants.RIGHT);
          B4FA01.setName("B4FA01");
          OBJ_15.add(B4FA01);
          B4FA01.setBounds(533, 28, 75, B4FA01.getPreferredSize().height);
        }
        p_contenu.add(OBJ_15);
        OBJ_15.setBounds(10, 15, 780, 235);

        //======== OBJ_16 ========
        {
          OBJ_16.setBorder(new TitledBorder(""));
          OBJ_16.setOpaque(false);
          OBJ_16.setName("OBJ_16");
          OBJ_16.setLayout(null);

          //---- B4NB08 ----
          B4NB08.setText("@B4NB08@");
          B4NB08.setHorizontalAlignment(SwingConstants.RIGHT);
          B4NB08.setName("B4NB08");
          OBJ_16.add(B4NB08);
          B4NB08.setBounds(225, 10, 75, B4NB08.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("Nombre de clients jamais factur\u00e9s");
          OBJ_36.setName("OBJ_36");
          OBJ_16.add(OBJ_36);
          OBJ_36.setBounds(15, 12, 205, 20);

          //---- B4NB9 ----
          B4NB9.setText("@B4NB09@");
          B4NB9.setHorizontalAlignment(SwingConstants.RIGHT);
          B4NB9.setName("B4NB9");
          OBJ_16.add(B4NB9);
          B4NB9.setBounds(225, 42, 75, B4NB9.getPreferredSize().height);

          //---- B4NB10 ----
          B4NB10.setText("@B4NB10@");
          B4NB10.setHorizontalAlignment(SwingConstants.RIGHT);
          B4NB10.setName("B4NB10");
          OBJ_16.add(B4NB10);
          B4NB10.setBounds(225, 74, 75, B4NB10.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Nombre de prospects total");
          OBJ_37.setName("OBJ_37");
          OBJ_16.add(OBJ_37);
          OBJ_37.setBounds(15, 44, 205, 20);

          //---- OBJ_38 ----
          OBJ_38.setText("Nombre de prospects factur\u00e9s");
          OBJ_38.setName("OBJ_38");
          OBJ_16.add(OBJ_38);
          OBJ_38.setBounds(15, 76, 205, 20);

          //---- NUREPR ----
          NUREPR.setText("@NUREPR@");
          NUREPR.setName("NUREPR");
          OBJ_16.add(NUREPR);
          NUREPR.setBounds(315, 42, 30, NUREPR.getPreferredSize().height);

          //---- LIREPR ----
          LIREPR.setText("@LIREPR@");
          LIREPR.setName("LIREPR");
          OBJ_16.add(LIREPR);
          LIREPR.setBounds(350, 42, 415, LIREPR.getPreferredSize().height);
        }
        p_contenu.add(OBJ_16);
        OBJ_16.setBounds(10, 255, 780, 115);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Choix possibles");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel OBJ_15;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable B4NB02;
  private JLabel label1;
  private JLabel OBJ_39;
  private RiZoneSortie B4NB01;
  private RiZoneSortie B4CA01;
  private RiZoneSortie B4MA01;
  private RiZoneSortie B4MY01;
  private RiZoneSortie B4FA01;
  private JPanel OBJ_16;
  private RiZoneSortie B4NB08;
  private JLabel OBJ_36;
  private RiZoneSortie B4NB9;
  private RiZoneSortie B4NB10;
  private JLabel OBJ_37;
  private JLabel OBJ_38;
  private RiZoneSortie NUREPR;
  private RiZoneSortie LIREPR;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
