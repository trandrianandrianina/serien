
package ri.serien.libecranrpg.stvm.STVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class STVM25FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM25FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    // setTitle(???);
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_4 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(340, 120));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBackground(new Color(239, 239, 222));
          p_recup.setPreferredSize(new Dimension(340, 120));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- OBJ_4 ----
          OBJ_4.setText("Calcul en cours...");
          OBJ_4.setFont(OBJ_4.getFont().deriveFont(OBJ_4.getFont().getStyle() | Font.BOLD, OBJ_4.getFont().getSize() + 3f));
          OBJ_4.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_4.setName("OBJ_4");
          p_recup.add(OBJ_4);
          OBJ_4.setBounds(0, 43, 340, 32);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(458, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
