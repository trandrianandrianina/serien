
package ri.serien.libecranrpg.stvm.STVM18FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.border.SoftBevelBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;

import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STVM18FM_MNU extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private String typeBouton = null;
  private int ligne = 0;
  private int colonne1 = 0;
  private int colonne2 = 0;
  private int colonne3 = 0;
  private int colonne4 = 0;
  private int colonne5 = 0;
  
  public STVM18FM_MNU(Lexical lex, iData iD, String type) {
    typeBouton = type;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setVisible(false);
    setData();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    
    button1.setText("Détail");
    button2.setVisible(true);
    button3.setVisible(true);
    button4.setVisible(true);
    button5.setVisible(true);
    
    if (typeBouton == "LB01") {
      ligne = 3;
      colonne1 = 50;
      colonne2 = 68;
      colonne3 = 72;
      colonne4 = 76;
      button5.setVisible(false);
      button2.setText("Sélection");
      button3.setText("Par vendeur");
      button4.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB02") {
      ligne = 4;
      colonne1 = 50;
      colonne2 = 68;
      colonne3 = 63;
      colonne4 = 72;
      colonne5 = 76;
      button2.setText("Sélection");
      button3.setText("Statistiques");
      button4.setText("Par vendeur");
      button5.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB03") {
      ligne = 5;
      colonne1 = 50;
      colonne2 = 68;
      colonne3 = 72;
      colonne4 = 76;
      button5.setVisible(false);
      button2.setText("Sélection");
      button3.setText("Par vendeur");
      button4.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB04") {
      ligne = 6;
      colonne1 = 50;
      colonne2 = 68;
      colonne3 = 63;
      colonne4 = 72;
      colonne5 = 76;
      button2.setText("Sélection");
      button3.setText("Statistiques");
      button4.setText("Par vendeur");
      button5.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB17") {
      ligne = 2;
      colonne1 = 50;
      colonne2 = 68;
      colonne3 = 72;
      colonne4 = 76;
      button5.setVisible(false);
      button2.setText("Sélection");
      button3.setText("Par vendeur");
      button4.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB05") {
      dodo();
      lexique.HostCursorPut(8, 68);
      lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    }
    
    if (typeBouton == "LB06") {
      dodo();
      lexique.HostCursorPut(9, 50);
      lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    }
    
    if (typeBouton == "LB09") {
      dodo();
      lexique.HostCursorPut(9, 68);
      lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    }
    
    if (typeBouton == "LB07") {
      ligne = 12;
      colonne1 = 68;
      colonne2 = 78;
      button3.setVisible(false);
      button4.setVisible(false);
      button5.setVisible(false);
      button1.setText("Global");
      button2.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB08") {
      ligne = 14;
      colonne1 = 68;
      colonne2 = 78;
      button3.setVisible(false);
      button4.setVisible(false);
      button5.setVisible(false);
      button1.setText("Global");
      button2.setText("Par représentant");
      setVisible(true);
    }
    
    if (typeBouton == "LB12") {
      ligne = 17;
      colonne1 = 50;
      colonne2 = 54;
      colonne3 = 58;
      colonne4 = 62;
      colonne5 = 66;
      button1.setText("Par magasins");
      button2.setText("Par familles");
      button3.setText("Par groupes");
      button4.setText("Par sous-familles");
      button5.setText("Par fournisseurs");
      setVisible(true);
    }
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques"));
  }
  
  public void getData() {
    
  }
  
  public void reveiller(String type) {
    typeBouton = type;
    setData();
  }
  
  public void dodo() {
    getData();
    setVisible(false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(ligne, colonne1);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dodo();
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(ligne, colonne2);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dodo();
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(ligne, colonne3);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dodo();
  }
  
  private void button4ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(ligne, colonne4);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dodo();
  }
  
  private void button5ActionPerformed(ActionEvent e) {
    if (typeBouton == "LB12") {
      ligne = ligne + 1;
    }
    lexique.HostCursorPut(ligne, colonne5);
    lexique.HostScreenSendKey(lexique.getPanel(), "F4", false);
    dodo();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    P_Centre = new JPanel();
    separator1 = compFactory.createSeparator("");
    panel1 = new JPanel();
    OBJ_10 = new JButton();
    panel2 = new JPanel();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    button4 = new JButton();
    button5 = new JButton();
    
    // ======== this ========
    setMinimumSize(new Dimension(265, 350));
    setResizable(false);
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setPreferredSize(new Dimension(260, 345));
      P_Centre.setName("P_Centre");
      
      // ---- separator1 ----
      separator1.setName("separator1");
      
      // ======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);
        
        // ---- OBJ_10 ----
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setToolTipText("Retour");
        OBJ_10.setText("Retour");
        OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
        OBJ_10.setBackground(new Color(238, 238, 210));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel1.add(OBJ_10);
        OBJ_10.setBounds(85, 5, 140, 40);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      
      // ======== panel2 ========
      {
        panel2.setOpaque(false);
        panel2.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
        panel2.setName("panel2");
        
        // ---- button1 ----
        button1.setText("text");
        button1.setBackground(new Color(238, 238, 210));
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed(e);
          }
        });
        
        // ---- button2 ----
        button2.setText("text");
        button2.setBackground(new Color(238, 238, 210));
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed(e);
          }
        });
        
        // ---- button3 ----
        button3.setText("text");
        button3.setBackground(new Color(238, 238, 210));
        button3.setName("button3");
        button3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button3ActionPerformed(e);
          }
        });
        
        // ---- button4 ----
        button4.setText("text");
        button4.setBackground(new Color(238, 238, 210));
        button4.setName("button4");
        button4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button4ActionPerformed(e);
          }
        });
        
        // ---- button5 ----
        button5.setText("text");
        button5.setBackground(new Color(238, 238, 210));
        button5.setName("button5");
        button5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button5ActionPerformed(e);
          }
        });
        
        GroupLayout panel2Layout = new GroupLayout(panel2);
        panel2.setLayout(panel2Layout);
        panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup().addGap(7, 7, 7).addGroup(
                panel2Layout.createParallelGroup().addComponent(button1, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button2, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button3, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button4, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                    .addComponent(button5, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE))));
        panel2Layout.setVerticalGroup(panel2Layout.createParallelGroup()
            .addGroup(panel2Layout.createSequentialGroup().addGap(12, 12, 12)
                .addComponent(button1, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                .addComponent(button2, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                .addComponent(button3, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                .addComponent(button4, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE).addGap(9, 9, 9)
                .addComponent(button5, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)));
      }
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addContainerGap()
              .addGroup(P_CentreLayout.createParallelGroup().addComponent(panel2, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                  .addComponent(separator1, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE)
                  .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 233, Short.MAX_VALUE))
              .addContainerGap()));
      P_CentreLayout.setVerticalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup().addGap(11, 11, 11)
              .addComponent(separator1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 243, GroupLayout.PREFERRED_SIZE)
              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JComponent separator1;
  private JPanel panel1;
  private JButton OBJ_10;
  private JPanel panel2;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JButton button4;
  private JButton button5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
