
package ri.serien.libecranrpg.stvm.STVM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STVM25FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STVM25FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTFAC@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTHOM@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MTNFAC@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_143.setVisible(lexique.HostFieldGetData("INDGRF").equalsIgnoreCase("+"));
    OBJ_97.setVisible(lexique.HostFieldGetData("INDVND").equalsIgnoreCase("+"));
    OBJ_92.setVisible(lexique.HostFieldGetData("INDREG").equalsIgnoreCase("+"));
    OBJ_89.setVisible(lexique.HostFieldGetData("INDBAS").equalsIgnoreCase("+"));
    if (lexique.isTrue("20")) {
      OBJ_98.setText("Représentants");
    }
    else {
      OBJ_98.setText("Vendeur");
    }
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @TITPG2@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_89ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 19);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_92ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(11, 66);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_97ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(12, 32);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_143ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 32);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void label1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(8, 23);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void label2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(8, 71);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void label3ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(10, 72);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_36 = new JLabel();
    WSOC = new XRiTextField();
    OBJ_183 = new JLabel();
    DTBDEB = new XRiCalendrier();
    OBJ_184 = new JLabel();
    DTBFIN = new XRiCalendrier();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    MTTTC = new XRiTextField();
    MTPRV = new XRiTextField();
    MTMRG = new XRiTextField();
    MTREM = new XRiTextField();
    MTHT = new XRiTextField();
    NBTOT = new XRiTextField();
    PRMRG = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_50 = new JLabel();
    panel3 = new JPanel();
    LIREG1 = new XRiTextField();
    LIREG2 = new XRiTextField();
    LIREG3 = new XRiTextField();
    LIREG4 = new XRiTextField();
    MTREG1 = new XRiTextField();
    MTREG2 = new XRiTextField();
    MTREG3 = new XRiTextField();
    MTREG4 = new XRiTextField();
    OBJ_68 = new JLabel();
    OBJ_65 = new JLabel();
    NBFAC = new XRiTextField();
    NBNFAC = new XRiTextField();
    OBJ_66 = new JLabel();
    MTBAS1 = new XRiTextField();
    MTBAS2 = new XRiTextField();
    MTBAS3 = new XRiTextField();
    MTBAS4 = new XRiTextField();
    OBJ_81 = new JLabel();
    LIBAS1 = new XRiTextField();
    LIBAS2 = new XRiTextField();
    LIBAS3 = new XRiTextField();
    LIBAS4 = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_89 = new SNBoutonDetail();
    OBJ_92 = new SNBoutonDetail();
    label1 = new JButton();
    label2 = new JButton();
    label3 = new JButton();
    label4 = new JLabel();
    panel4 = new JPanel();
    VL1 = new XRiTextField();
    VL2 = new XRiTextField();
    VL3 = new XRiTextField();
    VL4 = new XRiTextField();
    VL5 = new XRiTextField();
    VHT1 = new XRiTextField();
    VHT2 = new XRiTextField();
    VHT3 = new XRiTextField();
    VHT4 = new XRiTextField();
    VHT5 = new XRiTextField();
    VNB1 = new XRiTextField();
    VPR1 = new XRiTextField();
    VNB2 = new XRiTextField();
    VPR2 = new XRiTextField();
    VNB3 = new XRiTextField();
    VPR3 = new XRiTextField();
    VNB4 = new XRiTextField();
    VPR4 = new XRiTextField();
    VNB5 = new XRiTextField();
    VPR5 = new XRiTextField();
    VMG1 = new XRiTextField();
    VMG2 = new XRiTextField();
    VMG3 = new XRiTextField();
    VMG4 = new XRiTextField();
    VMG5 = new XRiTextField();
    VPM1 = new XRiTextField();
    VPM2 = new XRiTextField();
    VPM3 = new XRiTextField();
    VPM4 = new XRiTextField();
    VPM5 = new XRiTextField();
    OBJ_101 = new JLabel();
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    OBJ_98 = new JLabel();
    VC1 = new XRiTextField();
    VC2 = new XRiTextField();
    VC3 = new XRiTextField();
    VC4 = new XRiTextField();
    VC5 = new XRiTextField();
    OBJ_102 = new JLabel();
    OBJ_103 = new JLabel();
    OBJ_97 = new SNBoutonDetail();
    panel5 = new JPanel();
    GL3 = new XRiTextField();
    GL1 = new XRiTextField();
    GL2 = new XRiTextField();
    GL4 = new XRiTextField();
    GL5 = new XRiTextField();
    GHT1 = new XRiTextField();
    GHT2 = new XRiTextField();
    GHT3 = new XRiTextField();
    GHT4 = new XRiTextField();
    GHT5 = new XRiTextField();
    GPR1 = new XRiTextField();
    GPR2 = new XRiTextField();
    GPR3 = new XRiTextField();
    GPR4 = new XRiTextField();
    GPR5 = new XRiTextField();
    OBJ_144 = new JLabel();
    GMG1 = new XRiTextField();
    GMG2 = new XRiTextField();
    GMG3 = new XRiTextField();
    GMG4 = new XRiTextField();
    GMG5 = new XRiTextField();
    GPM1 = new XRiTextField();
    GPM2 = new XRiTextField();
    GPM3 = new XRiTextField();
    GPM4 = new XRiTextField();
    GPM5 = new XRiTextField();
    OBJ_146 = new JLabel();
    OBJ_145 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_148 = new JLabel();
    GC3 = new XRiTextField();
    OBJ_143 = new SNBoutonDetail();
    GC1 = new XRiTextField();
    GC2 = new XRiTextField();
    GC4 = new XRiTextField();
    GC5 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de bord sur les bons de vente");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_36 ----
          OBJ_36.setText("Etablissement");
          OBJ_36.setName("OBJ_36");

          //---- WSOC ----
          WSOC.setComponentPopupMenu(BTD);
          WSOC.setName("WSOC");

          //---- OBJ_183 ----
          OBJ_183.setText("Du");
          OBJ_183.setName("OBJ_183");

          //---- DTBDEB ----
          DTBDEB.setComponentPopupMenu(BTD);
          DTBDEB.setName("DTBDEB");

          //---- OBJ_184 ----
          OBJ_184.setText("au");
          OBJ_184.setName("OBJ_184");

          //---- DTBFIN ----
          DTBFIN.setComponentPopupMenu(BTD);
          DTBFIN.setName("DTBFIN");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_183, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(DTBDEB, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_184, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                .addGap(7, 7, 7)
                .addComponent(DTBFIN, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_36, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_183))
              .addComponent(DTBDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(OBJ_184))
              .addComponent(DTBFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Statistiques SN");
              riSousMenu_bt6.setToolTipText("Acc\u00e8s statistiques SN");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- MTTTC ----
            MTTTC.setComponentPopupMenu(BTD);
            MTTTC.setName("MTTTC");

            //---- MTPRV ----
            MTPRV.setComponentPopupMenu(BTD);
            MTPRV.setName("MTPRV");

            //---- MTMRG ----
            MTMRG.setComponentPopupMenu(BTD);
            MTMRG.setName("MTMRG");

            //---- MTREM ----
            MTREM.setComponentPopupMenu(BTD);
            MTREM.setName("MTREM");

            //---- MTHT ----
            MTHT.setComponentPopupMenu(BTD);
            MTHT.setName("MTHT");

            //---- NBTOT ----
            NBTOT.setComponentPopupMenu(BTD);
            NBTOT.setName("NBTOT");

            //---- PRMRG ----
            PRMRG.setComponentPopupMenu(BTD);
            PRMRG.setName("PRMRG");

            //---- OBJ_45 ----
            OBJ_45.setText("Vente TTC");
            OBJ_45.setFont(OBJ_45.getFont().deriveFont(OBJ_45.getFont().getStyle() | Font.BOLD));
            OBJ_45.setName("OBJ_45");

            //---- OBJ_48 ----
            OBJ_48.setText("Val.revient");
            OBJ_48.setName("OBJ_48");

            //---- OBJ_49 ----
            OBJ_49.setText("Marge");
            OBJ_49.setName("OBJ_49");

            //---- OBJ_51 ----
            OBJ_51.setText("Remises");
            OBJ_51.setName("OBJ_51");

            //---- OBJ_47 ----
            OBJ_47.setText("Vente HT");
            OBJ_47.setName("OBJ_47");

            //---- OBJ_46 ----
            OBJ_46.setText("Nb. de bons");
            OBJ_46.setName("OBJ_46");

            //---- OBJ_50 ----
            OBJ_50.setText("Mrg.%");
            OBJ_50.setName("OBJ_50");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                      .addGap(25, 25, 25)
                      .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                      .addGap(27, 27, 27)
                      .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(40, 40, 40)
                      .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                      .addGap(34, 34, 34)
                      .addComponent(OBJ_50, GroupLayout.PREFERRED_SIZE, 41, GroupLayout.PREFERRED_SIZE)
                      .addGap(69, 69, 69)
                      .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(MTTTC, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(34, 34, 34)
                      .addComponent(NBTOT, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                      .addGap(27, 27, 27)
                      .addComponent(MTHT, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(MTPRV, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(14, 14, 14)
                      .addComponent(MTMRG, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                      .addGap(24, 24, 24)
                      .addComponent(PRMRG, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(50, 50, 50)
                      .addComponent(MTREM, GroupLayout.PREFERRED_SIZE, 82, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(OBJ_45)
                    .addComponent(OBJ_46)
                    .addComponent(OBJ_47)
                    .addComponent(OBJ_48)
                    .addComponent(OBJ_49)
                    .addComponent(OBJ_50)
                    .addComponent(OBJ_51))
                  .addGap(9, 9, 9)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(MTTTC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(NBTOT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(MTHT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(MTPRV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(MTMRG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(PRMRG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(MTREM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- LIREG1 ----
            LIREG1.setComponentPopupMenu(BTD);
            LIREG1.setName("LIREG1");
            panel3.add(LIREG1);
            LIREG1.setBounds(315, 30, 172, LIREG1.getPreferredSize().height);

            //---- LIREG2 ----
            LIREG2.setComponentPopupMenu(BTD);
            LIREG2.setName("LIREG2");
            panel3.add(LIREG2);
            LIREG2.setBounds(315, 55, 172, LIREG2.getPreferredSize().height);

            //---- LIREG3 ----
            LIREG3.setComponentPopupMenu(BTD);
            LIREG3.setName("LIREG3");
            panel3.add(LIREG3);
            LIREG3.setBounds(315, 80, 172, LIREG3.getPreferredSize().height);

            //---- LIREG4 ----
            LIREG4.setComponentPopupMenu(BTD);
            LIREG4.setName("LIREG4");
            panel3.add(LIREG4);
            LIREG4.setBounds(315, 105, 172, LIREG4.getPreferredSize().height);

            //---- MTREG1 ----
            MTREG1.setComponentPopupMenu(BTD);
            MTREG1.setName("MTREG1");
            panel3.add(MTREG1);
            MTREG1.setBounds(505, 30, 76, MTREG1.getPreferredSize().height);

            //---- MTREG2 ----
            MTREG2.setComponentPopupMenu(BTD);
            MTREG2.setName("MTREG2");
            panel3.add(MTREG2);
            MTREG2.setBounds(505, 55, 76, 28);

            //---- MTREG3 ----
            MTREG3.setComponentPopupMenu(BTD);
            MTREG3.setName("MTREG3");
            panel3.add(MTREG3);
            MTREG3.setBounds(505, 80, 76, 28);

            //---- MTREG4 ----
            MTREG4.setComponentPopupMenu(BTD);
            MTREG4.setName("MTREG4");
            panel3.add(MTREG4);
            MTREG4.setBounds(505, 105, 76, 28);

            //---- OBJ_68 ----
            OBJ_68.setText("Montant homologu\u00e9");
            OBJ_68.setName("OBJ_68");
            panel3.add(OBJ_68);
            OBJ_68.setBounds(615, 15, 121, 15);

            //---- OBJ_65 ----
            OBJ_65.setText("Bases");
            OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
            OBJ_65.setName("OBJ_65");
            panel3.add(OBJ_65);
            OBJ_65.setBounds(10, 10, 80, 15);

            //---- NBFAC ----
            NBFAC.setComponentPopupMenu(BTD);
            NBFAC.setName("NBFAC");
            panel3.add(NBFAC);
            NBFAC.setBounds(215, 55, 76, NBFAC.getPreferredSize().height);

            //---- NBNFAC ----
            NBNFAC.setComponentPopupMenu(BTD);
            NBNFAC.setName("NBNFAC");
            panel3.add(NBNFAC);
            NBNFAC.setBounds(615, 105, 76, NBNFAC.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setText("Montant factur\u00e9");
            OBJ_66.setName("OBJ_66");
            panel3.add(OBJ_66);
            OBJ_66.setBounds(215, 15, 93, 15);

            //---- MTBAS1 ----
            MTBAS1.setComponentPopupMenu(BTD);
            MTBAS1.setName("MTBAS1");
            panel3.add(MTBAS1);
            MTBAS1.setBounds(120, 30, 76, MTBAS1.getPreferredSize().height);

            //---- MTBAS2 ----
            MTBAS2.setComponentPopupMenu(BTD);
            MTBAS2.setName("MTBAS2");
            panel3.add(MTBAS2);
            MTBAS2.setBounds(120, 55, 76, 28);

            //---- MTBAS3 ----
            MTBAS3.setComponentPopupMenu(BTD);
            MTBAS3.setName("MTBAS3");
            panel3.add(MTBAS3);
            MTBAS3.setBounds(120, 80, 76, 28);

            //---- MTBAS4 ----
            MTBAS4.setComponentPopupMenu(BTD);
            MTBAS4.setName("MTBAS4");
            panel3.add(MTBAS4);
            MTBAS4.setBounds(120, 105, 76, 28);

            //---- OBJ_81 ----
            OBJ_81.setText("Non factur\u00e9");
            OBJ_81.setName("OBJ_81");
            panel3.add(OBJ_81);
            OBJ_81.setBounds(615, 65, 76, 15);

            //---- LIBAS1 ----
            LIBAS1.setComponentPopupMenu(BTD);
            LIBAS1.setName("LIBAS1");
            panel3.add(LIBAS1);
            LIBAS1.setBounds(10, 30, 60, LIBAS1.getPreferredSize().height);

            //---- LIBAS2 ----
            LIBAS2.setComponentPopupMenu(BTD);
            LIBAS2.setName("LIBAS2");
            panel3.add(LIBAS2);
            LIBAS2.setBounds(10, 55, 60, LIBAS2.getPreferredSize().height);

            //---- LIBAS3 ----
            LIBAS3.setComponentPopupMenu(BTD);
            LIBAS3.setName("LIBAS3");
            panel3.add(LIBAS3);
            LIBAS3.setBounds(10, 80, 60, LIBAS3.getPreferredSize().height);

            //---- LIBAS4 ----
            LIBAS4.setComponentPopupMenu(BTD);
            LIBAS4.setName("LIBAS4");
            panel3.add(LIBAS4);
            LIBAS4.setBounds(10, 105, 60, LIBAS4.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setText("R\u00e8glements");
            OBJ_67.setName("OBJ_67");
            panel3.add(OBJ_67);
            OBJ_67.setBounds(315, 15, 76, 14);

            //---- OBJ_89 ----
            OBJ_89.setText("");
            OBJ_89.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_89.setName("OBJ_89");
            OBJ_89.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_89ActionPerformed(e);
              }
            });
            panel3.add(OBJ_89);
            OBJ_89.setBounds(200, 109, 17, 20);

            //---- OBJ_92 ----
            OBJ_92.setText("");
            OBJ_92.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_92.setName("OBJ_92");
            OBJ_92.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_92ActionPerformed(e);
              }
            });
            panel3.add(OBJ_92);
            OBJ_92.setBounds(585, 109, 17, 20);

            //---- label1 ----
            label1.setText("@MTFAC@");
            label1.setHorizontalAlignment(SwingConstants.RIGHT);
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() - 1f));
            label1.setName("label1");
            label1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                label1ActionPerformed(e);
              }
            });
            panel3.add(label1);
            label1.setBounds(213, 30, 80, 28);

            //---- label2 ----
            label2.setText("@MTHOM@");
            label2.setHorizontalAlignment(SwingConstants.RIGHT);
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() - 1f));
            label2.setName("label2");
            label2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                label2ActionPerformed(e);
              }
            });
            panel3.add(label2);
            label2.setBounds(613, 30, 80, 28);

            //---- label3 ----
            label3.setText("@MTNFAC@");
            label3.setHorizontalAlignment(SwingConstants.RIGHT);
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() - 1f));
            label3.setName("label3");
            label3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                label3ActionPerformed(e);
              }
            });
            panel3.add(label3);
            label3.setBounds(613, 80, 80, 28);

            //---- label4 ----
            label4.setText("Taxes");
            label4.setName("label4");
            panel3.add(label4);
            label4.setBounds(new Rectangle(new Point(120, 14), label4.getPreferredSize()));
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- VL1 ----
            VL1.setComponentPopupMenu(BTD);
            VL1.setName("VL1");
            panel4.add(VL1);
            VL1.setBounds(50, 30, 236, VL1.getPreferredSize().height);

            //---- VL2 ----
            VL2.setComponentPopupMenu(BTD);
            VL2.setName("VL2");
            panel4.add(VL2);
            VL2.setBounds(50, 55, 236, VL2.getPreferredSize().height);

            //---- VL3 ----
            VL3.setComponentPopupMenu(BTD);
            VL3.setName("VL3");
            panel4.add(VL3);
            VL3.setBounds(50, 80, 236, VL3.getPreferredSize().height);

            //---- VL4 ----
            VL4.setComponentPopupMenu(BTD);
            VL4.setName("VL4");
            panel4.add(VL4);
            VL4.setBounds(50, 105, 236, VL4.getPreferredSize().height);

            //---- VL5 ----
            VL5.setComponentPopupMenu(BTD);
            VL5.setName("VL5");
            panel4.add(VL5);
            VL5.setBounds(50, 130, 236, VL5.getPreferredSize().height);

            //---- VHT1 ----
            VHT1.setComponentPopupMenu(BTD);
            VHT1.setName("VHT1");
            panel4.add(VHT1);
            VHT1.setBounds(315, 30, 76, VHT1.getPreferredSize().height);

            //---- VHT2 ----
            VHT2.setComponentPopupMenu(BTD);
            VHT2.setName("VHT2");
            panel4.add(VHT2);
            VHT2.setBounds(315, 55, 76, VHT2.getPreferredSize().height);

            //---- VHT3 ----
            VHT3.setComponentPopupMenu(BTD);
            VHT3.setName("VHT3");
            panel4.add(VHT3);
            VHT3.setBounds(315, 80, 76, VHT3.getPreferredSize().height);

            //---- VHT4 ----
            VHT4.setComponentPopupMenu(BTD);
            VHT4.setName("VHT4");
            panel4.add(VHT4);
            VHT4.setBounds(315, 105, 76, VHT4.getPreferredSize().height);

            //---- VHT5 ----
            VHT5.setComponentPopupMenu(BTD);
            VHT5.setName("VHT5");
            panel4.add(VHT5);
            VHT5.setBounds(315, 130, 76, VHT5.getPreferredSize().height);

            //---- VNB1 ----
            VNB1.setComponentPopupMenu(BTD);
            VNB1.setName("VNB1");
            panel4.add(VNB1);
            VNB1.setBounds(410, 30, 76, VNB1.getPreferredSize().height);

            //---- VPR1 ----
            VPR1.setComponentPopupMenu(BTD);
            VPR1.setName("VPR1");
            panel4.add(VPR1);
            VPR1.setBounds(505, 30, 76, VPR1.getPreferredSize().height);

            //---- VNB2 ----
            VNB2.setComponentPopupMenu(BTD);
            VNB2.setName("VNB2");
            panel4.add(VNB2);
            VNB2.setBounds(410, 55, 76, VNB2.getPreferredSize().height);

            //---- VPR2 ----
            VPR2.setComponentPopupMenu(BTD);
            VPR2.setName("VPR2");
            panel4.add(VPR2);
            VPR2.setBounds(505, 55, 76, VPR2.getPreferredSize().height);

            //---- VNB3 ----
            VNB3.setComponentPopupMenu(BTD);
            VNB3.setName("VNB3");
            panel4.add(VNB3);
            VNB3.setBounds(410, 80, 76, VNB3.getPreferredSize().height);

            //---- VPR3 ----
            VPR3.setComponentPopupMenu(BTD);
            VPR3.setName("VPR3");
            panel4.add(VPR3);
            VPR3.setBounds(505, 80, 76, VPR3.getPreferredSize().height);

            //---- VNB4 ----
            VNB4.setComponentPopupMenu(BTD);
            VNB4.setName("VNB4");
            panel4.add(VNB4);
            VNB4.setBounds(410, 105, 76, VNB4.getPreferredSize().height);

            //---- VPR4 ----
            VPR4.setComponentPopupMenu(BTD);
            VPR4.setName("VPR4");
            panel4.add(VPR4);
            VPR4.setBounds(505, 105, 76, VPR4.getPreferredSize().height);

            //---- VNB5 ----
            VNB5.setComponentPopupMenu(BTD);
            VNB5.setName("VNB5");
            panel4.add(VNB5);
            VNB5.setBounds(410, 130, 76, VNB5.getPreferredSize().height);

            //---- VPR5 ----
            VPR5.setComponentPopupMenu(BTD);
            VPR5.setName("VPR5");
            panel4.add(VPR5);
            VPR5.setBounds(505, 130, 76, VPR5.getPreferredSize().height);

            //---- VMG1 ----
            VMG1.setComponentPopupMenu(BTD);
            VMG1.setName("VMG1");
            panel4.add(VMG1);
            VMG1.setBounds(620, 30, 76, VMG1.getPreferredSize().height);

            //---- VMG2 ----
            VMG2.setComponentPopupMenu(BTD);
            VMG2.setName("VMG2");
            panel4.add(VMG2);
            VMG2.setBounds(620, 55, 76, VMG2.getPreferredSize().height);

            //---- VMG3 ----
            VMG3.setComponentPopupMenu(BTD);
            VMG3.setName("VMG3");
            panel4.add(VMG3);
            VMG3.setBounds(620, 80, 76, VMG3.getPreferredSize().height);

            //---- VMG4 ----
            VMG4.setComponentPopupMenu(BTD);
            VMG4.setName("VMG4");
            panel4.add(VMG4);
            VMG4.setBounds(620, 105, 76, VMG4.getPreferredSize().height);

            //---- VMG5 ----
            VMG5.setComponentPopupMenu(BTD);
            VMG5.setName("VMG5");
            panel4.add(VMG5);
            VMG5.setBounds(620, 130, 76, VMG5.getPreferredSize().height);

            //---- VPM1 ----
            VPM1.setComponentPopupMenu(BTD);
            VPM1.setName("VPM1");
            panel4.add(VPM1);
            VPM1.setBounds(710, 30, 60, VPM1.getPreferredSize().height);

            //---- VPM2 ----
            VPM2.setComponentPopupMenu(BTD);
            VPM2.setName("VPM2");
            panel4.add(VPM2);
            VPM2.setBounds(710, 55, 60, VPM2.getPreferredSize().height);

            //---- VPM3 ----
            VPM3.setComponentPopupMenu(BTD);
            VPM3.setName("VPM3");
            panel4.add(VPM3);
            VPM3.setBounds(710, 80, 60, VPM3.getPreferredSize().height);

            //---- VPM4 ----
            VPM4.setComponentPopupMenu(BTD);
            VPM4.setName("VPM4");
            panel4.add(VPM4);
            VPM4.setBounds(710, 105, 60, VPM4.getPreferredSize().height);

            //---- VPM5 ----
            VPM5.setComponentPopupMenu(BTD);
            VPM5.setName("VPM5");
            panel4.add(VPM5);
            VPM5.setBounds(710, 130, 60, VPM5.getPreferredSize().height);

            //---- OBJ_101 ----
            OBJ_101.setText("V.Revient");
            OBJ_101.setName("OBJ_101");
            panel4.add(OBJ_101);
            OBJ_101.setBounds(505, 10, 61, 14);

            //---- OBJ_99 ----
            OBJ_99.setText("Vente HT");
            OBJ_99.setName("OBJ_99");
            panel4.add(OBJ_99);
            OBJ_99.setBounds(315, 10, 60, 14);

            //---- OBJ_100 ----
            OBJ_100.setText("Nb.bons");
            OBJ_100.setName("OBJ_100");
            panel4.add(OBJ_100);
            OBJ_100.setBounds(410, 10, 60, 14);

            //---- OBJ_98 ----
            OBJ_98.setText("Vendeur");
            OBJ_98.setFont(OBJ_98.getFont().deriveFont(OBJ_98.getFont().getStyle() | Font.BOLD));
            OBJ_98.setName("OBJ_98");
            panel4.add(OBJ_98);
            OBJ_98.setBounds(10, 10, 270, 14);

            //---- VC1 ----
            VC1.setComponentPopupMenu(BTD);
            VC1.setName("VC1");
            panel4.add(VC1);
            VC1.setBounds(10, 30, 36, VC1.getPreferredSize().height);

            //---- VC2 ----
            VC2.setComponentPopupMenu(BTD);
            VC2.setName("VC2");
            panel4.add(VC2);
            VC2.setBounds(10, 55, 36, VC2.getPreferredSize().height);

            //---- VC3 ----
            VC3.setComponentPopupMenu(BTD);
            VC3.setName("VC3");
            panel4.add(VC3);
            VC3.setBounds(10, 80, 36, VC3.getPreferredSize().height);

            //---- VC4 ----
            VC4.setComponentPopupMenu(BTD);
            VC4.setName("VC4");
            panel4.add(VC4);
            VC4.setBounds(10, 105, 36, VC4.getPreferredSize().height);

            //---- VC5 ----
            VC5.setComponentPopupMenu(BTD);
            VC5.setName("VC5");
            panel4.add(VC5);
            VC5.setBounds(10, 130, 36, VC5.getPreferredSize().height);

            //---- OBJ_102 ----
            OBJ_102.setText("Marge");
            OBJ_102.setName("OBJ_102");
            panel4.add(OBJ_102);
            OBJ_102.setBounds(625, 10, 42, 14);

            //---- OBJ_103 ----
            OBJ_103.setText("Mrg.%");
            OBJ_103.setName("OBJ_103");
            panel4.add(OBJ_103);
            OBJ_103.setBounds(710, 10, 42, 14);

            //---- OBJ_97 ----
            OBJ_97.setText("");
            OBJ_97.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_97.setName("OBJ_97");
            OBJ_97.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_97ActionPerformed(e);
              }
            });
            panel4.add(OBJ_97);
            OBJ_97.setBounds(295, 8, 17, 19);
          }

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder(""));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- GL3 ----
            GL3.setComponentPopupMenu(BTD);
            GL3.setName("GL3");
            panel5.add(GL3);
            GL3.setBounds(50, 75, 252, 28);

            //---- GL1 ----
            GL1.setComponentPopupMenu(BTD);
            GL1.setName("GL1");
            panel5.add(GL1);
            GL1.setBounds(50, 25, 252, GL1.getPreferredSize().height);

            //---- GL2 ----
            GL2.setComponentPopupMenu(BTD);
            GL2.setName("GL2");
            panel5.add(GL2);
            GL2.setBounds(50, 50, 252, 28);

            //---- GL4 ----
            GL4.setComponentPopupMenu(BTD);
            GL4.setName("GL4");
            panel5.add(GL4);
            GL4.setBounds(50, 100, 252, 28);

            //---- GL5 ----
            GL5.setComponentPopupMenu(BTD);
            GL5.setName("GL5");
            panel5.add(GL5);
            GL5.setBounds(50, 125, 252, 28);

            //---- GHT1 ----
            GHT1.setComponentPopupMenu(BTD);
            GHT1.setName("GHT1");
            panel5.add(GHT1);
            GHT1.setBounds(315, 25, 76, GHT1.getPreferredSize().height);

            //---- GHT2 ----
            GHT2.setComponentPopupMenu(BTD);
            GHT2.setName("GHT2");
            panel5.add(GHT2);
            GHT2.setBounds(315, 50, 76, GHT2.getPreferredSize().height);

            //---- GHT3 ----
            GHT3.setComponentPopupMenu(BTD);
            GHT3.setName("GHT3");
            panel5.add(GHT3);
            GHT3.setBounds(315, 75, 76, GHT3.getPreferredSize().height);

            //---- GHT4 ----
            GHT4.setComponentPopupMenu(BTD);
            GHT4.setName("GHT4");
            panel5.add(GHT4);
            GHT4.setBounds(315, 100, 76, GHT4.getPreferredSize().height);

            //---- GHT5 ----
            GHT5.setComponentPopupMenu(BTD);
            GHT5.setName("GHT5");
            panel5.add(GHT5);
            GHT5.setBounds(315, 125, 76, GHT5.getPreferredSize().height);

            //---- GPR1 ----
            GPR1.setComponentPopupMenu(BTD);
            GPR1.setName("GPR1");
            panel5.add(GPR1);
            GPR1.setBounds(505, 25, 76, GPR1.getPreferredSize().height);

            //---- GPR2 ----
            GPR2.setComponentPopupMenu(BTD);
            GPR2.setName("GPR2");
            panel5.add(GPR2);
            GPR2.setBounds(505, 50, 76, GPR2.getPreferredSize().height);

            //---- GPR3 ----
            GPR3.setComponentPopupMenu(BTD);
            GPR3.setName("GPR3");
            panel5.add(GPR3);
            GPR3.setBounds(505, 75, 76, GPR3.getPreferredSize().height);

            //---- GPR4 ----
            GPR4.setComponentPopupMenu(BTD);
            GPR4.setName("GPR4");
            panel5.add(GPR4);
            GPR4.setBounds(505, 100, 76, GPR4.getPreferredSize().height);

            //---- GPR5 ----
            GPR5.setComponentPopupMenu(BTD);
            GPR5.setName("GPR5");
            panel5.add(GPR5);
            GPR5.setBounds(505, 125, 76, GPR5.getPreferredSize().height);

            //---- OBJ_144 ----
            OBJ_144.setText("Groupe articles");
            OBJ_144.setFont(OBJ_144.getFont().deriveFont(OBJ_144.getFont().getStyle() | Font.BOLD));
            OBJ_144.setName("OBJ_144");
            panel5.add(OBJ_144);
            OBJ_144.setBounds(10, 5, 135, 14);

            //---- GMG1 ----
            GMG1.setComponentPopupMenu(BTD);
            GMG1.setName("GMG1");
            panel5.add(GMG1);
            GMG1.setBounds(620, 25, 76, GMG1.getPreferredSize().height);

            //---- GMG2 ----
            GMG2.setComponentPopupMenu(BTD);
            GMG2.setName("GMG2");
            panel5.add(GMG2);
            GMG2.setBounds(620, 50, 76, GMG2.getPreferredSize().height);

            //---- GMG3 ----
            GMG3.setComponentPopupMenu(BTD);
            GMG3.setName("GMG3");
            panel5.add(GMG3);
            GMG3.setBounds(620, 75, 76, GMG3.getPreferredSize().height);

            //---- GMG4 ----
            GMG4.setComponentPopupMenu(BTD);
            GMG4.setName("GMG4");
            panel5.add(GMG4);
            GMG4.setBounds(620, 100, 76, GMG4.getPreferredSize().height);

            //---- GMG5 ----
            GMG5.setComponentPopupMenu(BTD);
            GMG5.setName("GMG5");
            panel5.add(GMG5);
            GMG5.setBounds(620, 125, 76, GMG5.getPreferredSize().height);

            //---- GPM1 ----
            GPM1.setComponentPopupMenu(BTD);
            GPM1.setName("GPM1");
            panel5.add(GPM1);
            GPM1.setBounds(710, 25, 60, GPM1.getPreferredSize().height);

            //---- GPM2 ----
            GPM2.setComponentPopupMenu(BTD);
            GPM2.setName("GPM2");
            panel5.add(GPM2);
            GPM2.setBounds(710, 50, 60, GPM2.getPreferredSize().height);

            //---- GPM3 ----
            GPM3.setComponentPopupMenu(BTD);
            GPM3.setName("GPM3");
            panel5.add(GPM3);
            GPM3.setBounds(710, 75, 60, GPM3.getPreferredSize().height);

            //---- GPM4 ----
            GPM4.setComponentPopupMenu(BTD);
            GPM4.setName("GPM4");
            panel5.add(GPM4);
            GPM4.setBounds(710, 100, 60, GPM4.getPreferredSize().height);

            //---- GPM5 ----
            GPM5.setComponentPopupMenu(BTD);
            GPM5.setName("GPM5");
            panel5.add(GPM5);
            GPM5.setBounds(710, 125, 60, GPM5.getPreferredSize().height);

            //---- OBJ_146 ----
            OBJ_146.setText("V.Revient");
            OBJ_146.setName("OBJ_146");
            panel5.add(OBJ_146);
            OBJ_146.setBounds(505, 5, 61, 14);

            //---- OBJ_145 ----
            OBJ_145.setText("Vente HT");
            OBJ_145.setName("OBJ_145");
            panel5.add(OBJ_145);
            OBJ_145.setBounds(315, 5, 60, 14);

            //---- OBJ_147 ----
            OBJ_147.setText("Marge");
            OBJ_147.setName("OBJ_147");
            panel5.add(OBJ_147);
            OBJ_147.setBounds(625, 5, 42, 14);

            //---- OBJ_148 ----
            OBJ_148.setText("Mrg.%");
            OBJ_148.setName("OBJ_148");
            panel5.add(OBJ_148);
            OBJ_148.setBounds(710, 5, 42, 14);

            //---- GC3 ----
            GC3.setComponentPopupMenu(BTD);
            GC3.setName("GC3");
            panel5.add(GC3);
            GC3.setBounds(10, 75, 20, GC3.getPreferredSize().height);

            //---- OBJ_143 ----
            OBJ_143.setText("");
            OBJ_143.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_143.setName("OBJ_143");
            OBJ_143.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_143ActionPerformed(e);
              }
            });
            panel5.add(OBJ_143);
            OBJ_143.setBounds(295, 4, 17, 17);

            //---- GC1 ----
            GC1.setComponentPopupMenu(BTD);
            GC1.setName("GC1");
            panel5.add(GC1);
            GC1.setBounds(10, 25, 20, GC1.getPreferredSize().height);

            //---- GC2 ----
            GC2.setComponentPopupMenu(BTD);
            GC2.setName("GC2");
            panel5.add(GC2);
            GC2.setBounds(10, 50, 20, GC2.getPreferredSize().height);

            //---- GC4 ----
            GC4.setComponentPopupMenu(BTD);
            GC4.setName("GC4");
            panel5.add(GC4);
            GC4.setBounds(10, 100, 20, GC4.getPreferredSize().height);

            //---- GC5 ----
            GC5.setComponentPopupMenu(BTD);
            GC5.setName("GC5");
            panel5.add(GC5);
            GC5.setBounds(10, 125, 20, GC5.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 790, GroupLayout.PREFERRED_SIZE))
                .addContainerGap(16, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 140, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 165, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_36;
  private XRiTextField WSOC;
  private JLabel OBJ_183;
  private XRiCalendrier DTBDEB;
  private JLabel OBJ_184;
  private XRiCalendrier DTBFIN;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField MTTTC;
  private XRiTextField MTPRV;
  private XRiTextField MTMRG;
  private XRiTextField MTREM;
  private XRiTextField MTHT;
  private XRiTextField NBTOT;
  private XRiTextField PRMRG;
  private JLabel OBJ_45;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_51;
  private JLabel OBJ_47;
  private JLabel OBJ_46;
  private JLabel OBJ_50;
  private JPanel panel3;
  private XRiTextField LIREG1;
  private XRiTextField LIREG2;
  private XRiTextField LIREG3;
  private XRiTextField LIREG4;
  private XRiTextField MTREG1;
  private XRiTextField MTREG2;
  private XRiTextField MTREG3;
  private XRiTextField MTREG4;
  private JLabel OBJ_68;
  private JLabel OBJ_65;
  private XRiTextField NBFAC;
  private XRiTextField NBNFAC;
  private JLabel OBJ_66;
  private XRiTextField MTBAS1;
  private XRiTextField MTBAS2;
  private XRiTextField MTBAS3;
  private XRiTextField MTBAS4;
  private JLabel OBJ_81;
  private XRiTextField LIBAS1;
  private XRiTextField LIBAS2;
  private XRiTextField LIBAS3;
  private XRiTextField LIBAS4;
  private JLabel OBJ_67;
  private SNBoutonDetail OBJ_89;
  private SNBoutonDetail OBJ_92;
  private JButton label1;
  private JButton label2;
  private JButton label3;
  private JLabel label4;
  private JPanel panel4;
  private XRiTextField VL1;
  private XRiTextField VL2;
  private XRiTextField VL3;
  private XRiTextField VL4;
  private XRiTextField VL5;
  private XRiTextField VHT1;
  private XRiTextField VHT2;
  private XRiTextField VHT3;
  private XRiTextField VHT4;
  private XRiTextField VHT5;
  private XRiTextField VNB1;
  private XRiTextField VPR1;
  private XRiTextField VNB2;
  private XRiTextField VPR2;
  private XRiTextField VNB3;
  private XRiTextField VPR3;
  private XRiTextField VNB4;
  private XRiTextField VPR4;
  private XRiTextField VNB5;
  private XRiTextField VPR5;
  private XRiTextField VMG1;
  private XRiTextField VMG2;
  private XRiTextField VMG3;
  private XRiTextField VMG4;
  private XRiTextField VMG5;
  private XRiTextField VPM1;
  private XRiTextField VPM2;
  private XRiTextField VPM3;
  private XRiTextField VPM4;
  private XRiTextField VPM5;
  private JLabel OBJ_101;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private JLabel OBJ_98;
  private XRiTextField VC1;
  private XRiTextField VC2;
  private XRiTextField VC3;
  private XRiTextField VC4;
  private XRiTextField VC5;
  private JLabel OBJ_102;
  private JLabel OBJ_103;
  private SNBoutonDetail OBJ_97;
  private JPanel panel5;
  private XRiTextField GL3;
  private XRiTextField GL1;
  private XRiTextField GL2;
  private XRiTextField GL4;
  private XRiTextField GL5;
  private XRiTextField GHT1;
  private XRiTextField GHT2;
  private XRiTextField GHT3;
  private XRiTextField GHT4;
  private XRiTextField GHT5;
  private XRiTextField GPR1;
  private XRiTextField GPR2;
  private XRiTextField GPR3;
  private XRiTextField GPR4;
  private XRiTextField GPR5;
  private JLabel OBJ_144;
  private XRiTextField GMG1;
  private XRiTextField GMG2;
  private XRiTextField GMG3;
  private XRiTextField GMG4;
  private XRiTextField GMG5;
  private XRiTextField GPM1;
  private XRiTextField GPM2;
  private XRiTextField GPM3;
  private XRiTextField GPM4;
  private XRiTextField GPM5;
  private JLabel OBJ_146;
  private JLabel OBJ_145;
  private JLabel OBJ_147;
  private JLabel OBJ_148;
  private XRiTextField GC3;
  private SNBoutonDetail OBJ_143;
  private XRiTextField GC1;
  private XRiTextField GC2;
  private XRiTextField GC4;
  private XRiTextField GC5;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
