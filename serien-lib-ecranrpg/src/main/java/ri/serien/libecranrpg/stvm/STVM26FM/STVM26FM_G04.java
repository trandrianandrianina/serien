
package ri.serien.libecranrpg.stvm.STVM26FM;
// Nom Fichier: pop_null_CGM05DOM.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.LayoutStyle;
import javax.swing.SwingConstants;

import org.jfree.data.DefaultKeyedValues;

import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.moteur.interpreteur.Lexical;
import ri.serien.libswing.moteur.interpreteur.iData;

/**
 * @author Stéphane Vénéri
 */
public class STVM26FM_G04 extends JDialog {
  
  private Lexical lexique = null;
  private iData interpreteurD = null;
  private JPanel master = null;
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_COMBINE);
  private RiGraphe graphe1 = new RiGraphe(RiGraphe.GRAPHE_BAR3D);
  private boolean[] forme = { true, false, false };
  private boolean[] axis = { true, true, true };
  private Color[] couleursCourbes = { new Color(25, 100, 184), new Color(9, 172, 166), new Color(157, 54, 86) };
  
  public STVM26FM_G04(JPanel panel, Lexical lex, iData iD) {
    master = panel;
    lexique = lex;
    interpreteurD = iD;
    initComponents();
    setData();
    setVisible(true);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  public void mettreAJourVariableLibelle() {
  }
  
  public void setData() {
    mettreAJourVariableLibelle();
    
    // GRAPHE
    String[] libelle = new String[15];
    String[] donnee = new String[15];
    String[] donnee1 = new String[15];
    String[] donnee2 = new String[15];
    String[] donnee3 = new String[15];
    
    // Chargement des libellés
    for (int i = 0; i < libelle.length; i++) {
      libelle[i] = lexique.HostFieldGetData("LE" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1)));
    }
    
    // Chargement des données
    for (int i = 0; i < donnee.length; i++) {
      donnee[i] = lexique.HostFieldGetNumericData("F" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
      donnee1[i] = lexique.HostFieldGetNumericData("G" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
      donnee2[i] = lexique.HostFieldGetNumericData("H" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
      donnee3[i] = lexique.HostFieldGetNumericData("B" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).trim();
    }
    
    // Préparation des données
    Object[][] data = new Object[3][3];
    Object[][] data1 = new Object[libelle.length][2];
    DefaultKeyedValues dkv1 = new DefaultKeyedValues();
    DefaultKeyedValues dkv2 = new DefaultKeyedValues();
    DefaultKeyedValues dkv3 = new DefaultKeyedValues();
    
    for (int i = 0; i < libelle.length; i++) {
      donnee[i] = donnee[i].replaceAll("\\s", "0");
      dkv1.addValue(libelle[i], Double.parseDouble(donnee[i]));
      donnee1[i] = donnee1[i].replaceAll("\\s", "0");
      dkv2.addValue(libelle[i], Double.parseDouble(donnee1[i]));
      donnee2[i] = donnee2[i].replaceAll("\\s", "0");
      dkv3.addValue(libelle[i], Double.parseDouble(donnee2[i]));
      data1[i][0] = libelle[i];
      donnee3[i] = donnee3[i].replaceAll("\\s", "0");
      data1[i][1] = Double.parseDouble(donnee3[i]);
    }
    
    for (int i = 0; i < libelle.length; i++) {
      dkv1.addValue(libelle[i], Double.parseDouble(donnee[i]));
      dkv2.addValue(libelle[i], Double.parseDouble(donnee1[i]));
    }
    data[0][0] = "Vente HT";
    data[1][0] = "V revient";
    data[2][0] = "Marge";
    data[0][1] = dkv1;
    data[1][1] = dkv2;
    data[2][1] = dkv3;
    
    graphe.setGraphCombiForme(forme);
    graphe.setGraphCombiColor(couleursCourbes);
    graphe.setGraphCombiRangeAxis(axis);
    
    graphe.setDonnee(data, "", false);
    graphe.getGraphe(lexique.HostFieldGetData("LIBPOS"), true);
    l_graphe.setIcon(graphe.getPicture(l_graphe.getWidth(), l_graphe.getHeight()));
    
    graphe1.setOrientation(0);
    graphe1.setDonnee(data1, "", false);
    graphe1.getGraphe("Nombre de bons", false);
    l_graphe2.setIcon(graphe1.getPicture(l_graphe2.getWidth(), l_graphe2.getHeight()));
    
    
    
    // TODO Icones
    OBJ_10.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Détail des statistiques : " + lexique.HostFieldGetData("LIBPOS")));
  }
  
  public void getData() {
    
  }
  
  public void reveiller() {
    setVisible(true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    getData();
    setVisible(false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    P_Centre = new JPanel();
    l_graphe = new JLabel();
    OBJ_10 = new JButton();
    l_graphe2 = new JLabel();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 690));
    setName("this");
    Container contentPane = getContentPane();
    contentPane.setLayout(new BorderLayout());
    
    // ======== P_Centre ========
    {
      P_Centre.setBackground(new Color(238, 238, 210));
      P_Centre.setName("P_Centre");
      
      // ---- l_graphe ----
      l_graphe.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe.setComponentPopupMenu(null);
      l_graphe.setBackground(new Color(214, 217, 223));
      l_graphe.setName("l_graphe");
      
      // ---- OBJ_10 ----
      OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_10.setToolTipText("Retour");
      OBJ_10.setText("Retour");
      OBJ_10.setFont(OBJ_10.getFont().deriveFont(OBJ_10.getFont().getStyle() | Font.BOLD, OBJ_10.getFont().getSize() + 3f));
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      
      // ---- l_graphe2 ----
      l_graphe2.setHorizontalAlignment(SwingConstants.CENTER);
      l_graphe2.setComponentPopupMenu(null);
      l_graphe2.setBackground(new Color(214, 217, 223));
      l_graphe2.setName("l_graphe2");
      
      GroupLayout P_CentreLayout = new GroupLayout(P_Centre);
      P_Centre.setLayout(P_CentreLayout);
      P_CentreLayout.setHorizontalGroup(P_CentreLayout.createParallelGroup()
          .addGroup(P_CentreLayout.createSequentialGroup()
              .addGroup(P_CentreLayout.createParallelGroup()
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(940, 940, 940).addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE,
                      140, GroupLayout.PREFERRED_SIZE))
                  .addGroup(P_CentreLayout.createSequentialGroup().addGap(7, 7, 7)
                      .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 713, GroupLayout.PREFERRED_SIZE)
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                      .addComponent(l_graphe2, GroupLayout.PREFERRED_SIZE, 358, GroupLayout.PREFERRED_SIZE)))
              .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
      P_CentreLayout
          .setVerticalGroup(P_CentreLayout.createParallelGroup()
              .addGroup(P_CentreLayout.createSequentialGroup().addGap(15, 15, 15)
                  .addGroup(P_CentreLayout.createParallelGroup()
                      .addComponent(l_graphe, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE)
                      .addGroup(P_CentreLayout.createSequentialGroup()
                          .addComponent(l_graphe2, GroupLayout.PREFERRED_SIZE, 580, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                          .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)))
                  .addGap(8, 8, 8)));
    }
    contentPane.add(P_Centre, BorderLayout.CENTER);
    pack();
    setLocationRelativeTo(getOwner());
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel P_Centre;
  private JLabel l_graphe;
  private JButton OBJ_10;
  private JLabel l_graphe2;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
