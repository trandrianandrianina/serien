
package ri.serien.libecranrpg.stvm.STVM23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class STVM23FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private int[] _LIST_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private String[] _LI01_Title = { "Désignation", "Nombre", "Valeur", "Marge", };
  private String[][] _LI01_Data =
      { { "LI01", "NB01", "MT01", "MA01", }, { "LI02", "NB02", "MT02", "MA02", }, { "LI03", "NB03", "MT03", "MA03", },
          { "LI04", "NB04", "MT04", "MA04", }, { "LI05", "NB05", "MT05", "MA05", }, { "LI06", "NB06", "MT06", "MA06", },
          { "LI07", "NB07", "MT07", "MA07", }, { "LI08", "NB08", "MT08", "MA08", }, { "LI09", "NB09", "MT09", "MA09", },
          { "LI10", "NB10", "MT10", "MA10", }, { "LI11", "NB11", "MT11", "MA11", }, { "LI12", "NB12", "MT12", "MA12", },
          { "LI13", "NB13", "MT13", "MA13", }, { "LI14", "NB14", "MT14", "MA14", }, { "LI15", "NB15", "MT15", "MA15", }, };
  private int[] _LI01_Width = { 248, 62, 75, 83, };
  private STVM23FM_G01 g01 = null;
  private STVM23FM_G02 g02 = null;
  
  public STVM23FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LI01.setAspectTable(null, _LI01_Title, _LI01_Data, _LI01_Width, false, _LIST_Justification, null, null, null);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_14.setBorder(new TitledBorder(null, lexique.TranslationTable(interpreteurD.analyseExpression("@LIBTIT@")).trim(),
        TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("sansserif", Font.BOLD, 14)));
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), null, _LIST_Justification);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LIBPOS@"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (g01 != null) {
      g01.reveiller("NB");
    }
    else {
      g01 = new STVM23FM_G01(this, lexique, interpreteurD, "NB");
    }
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    if (g01 != null) {
      g01.reveiller("MT");
    }
    else {
      g01 = new STVM23FM_G01(this, lexique, interpreteurD, "MT");
    }
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    if (g01 != null) {
      g01.reveiller("MA");
    }
    else {
      g01 = new STVM23FM_G01(this, lexique, interpreteurD, "MA");
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (g02 != null) {
      g02.reveiller("NB");
    }
    else {
      g02 = new STVM23FM_G02(this, lexique, interpreteurD, "NB");
    }
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (g02 != null) {
      g02.reveiller("MT");
    }
    else {
      g02 = new STVM23FM_G02(this, lexique, interpreteurD, "MT");
    }
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    if (g02 != null) {
      g02.reveiller("MA");
    }
    else {
      g02 = new STVM23FM_G02(this, lexique, interpreteurD, "MA");
    }
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    OBJ_14 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    LI01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(775, 375));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Comparaison nombre");
            riSousMenu_bt6.setToolTipText("Repr\u00e9sentation graphique sous forme d'histogramme");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
          
          // ======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");
            
            // ---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Comparaison valeur");
            riSousMenu_bt8.setToolTipText("Repr\u00e9sentation graphique sous forme d'histogramme");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
          
          // ======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");
            
            // ---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Comparaison marge");
            riSousMenu_bt9.setToolTipText("Repr\u00e9sentation graphique sous forme d'histogramme");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
          
          // ======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");
            
            // ---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Parts en nombre");
            riSousMenu_bt7.setToolTipText("Repr\u00e9sentation graphique sous forme de camembert");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
          
          // ======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");
            
            // ---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Parts en valeur");
            riSousMenu_bt10.setToolTipText("Repr\u00e9sentation graphique sous forme de camembert");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);
          
          // ======== riSousMenu11 ========
          {
            riSousMenu11.setName("riSousMenu11");
            
            // ---- riSousMenu_bt11 ----
            riSousMenu_bt11.setText("Parts en marge");
            riSousMenu_bt11.setToolTipText("Repr\u00e9sentation graphique sous forme de camembert");
            riSousMenu_bt11.setName("riSousMenu_bt11");
            riSousMenu_bt11.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt11ActionPerformed(e);
              }
            });
            riSousMenu11.add(riSousMenu_bt11);
          }
          menus_haut.add(riSousMenu11);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== OBJ_14 ========
        {
          OBJ_14.setBorder(new TitledBorder(null, "@LIBTIT@", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION,
              new Font("sansserif", Font.BOLD, 14)));
          OBJ_14.setOpaque(false);
          OBJ_14.setName("OBJ_14");
          OBJ_14.setLayout(null);
          
          // ======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
            
            // ---- LI01 ----
            LI01.setName("LI01");
            SCROLLPANE_LIST.setViewportView(LI01);
          }
          OBJ_14.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(25, 45, 494, 275);
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          OBJ_14.add(BT_PGUP);
          BT_PGUP.setBounds(530, 45, 25, 130);
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          OBJ_14.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(530, 190, 25, 130);
        }
        p_contenu.add(OBJ_14);
        OBJ_14.setBounds(10, 15, 578, 348);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private JPanel p_contenu;
  private JPanel OBJ_14;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable LI01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
