
package ri.serien.libecranrpg.ogpm.OGPM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JRadioButton;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Emmanuel MARCQ
 */
public class OGPM21FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
  
  public OGPM21FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WARTR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WARTR@")).trim());
    EBLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBLIB@")).trim());
    WTP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT1@    @LOPT1@ @LMES1@")).trim());
    WTP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT2@    @LOPT2@ @LMES2@")).trim());
    WTP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT3@    @LOPT3@ @LMES3@")).trim());
    WTP4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT4@    @LOPT4@ @LMES4@")).trim());
    WTP8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT8@    @LOPT8@ @LMES8@")).trim());
    WTP7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT7@    @LOPT7@ @LMES7@")).trim());
    WTP6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT6@    @LOPT6@ @LMES6@")).trim());
    WTP5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT5@    @LOPT5@ @LMES5@")).trim());
    WTP9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT9@    @LOPT9@ @LMES9@")).trim());
    WTP10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT10@    @LOPT10@ @LMES10@")).trim());
    WTP11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT11@    @LOPT11@ @LMES11@")).trim());
    WTP12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT12@    @LOPT12@ @LMES12@")).trim());
    WTP13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT13@    @LOPT13@ @LMES13@")).trim());
    WTP14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("  @OPT14@    @LOPT14@ @LMES14@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WTP1.setVisible(lexique.isPresent("WTP1") && !lexique.HostFieldGetData("LOPT1").trim().equalsIgnoreCase(""));
    WTP2.setVisible(lexique.isPresent("WTP2") && !lexique.HostFieldGetData("LOPT2").trim().equalsIgnoreCase(""));
    WTP3.setVisible(lexique.isPresent("WTP3") && !lexique.HostFieldGetData("LOPT3").trim().equalsIgnoreCase(""));
    WTP4.setVisible(lexique.isPresent("WTP4") && !lexique.HostFieldGetData("LOPT4").trim().equalsIgnoreCase(""));
    WTP5.setVisible(lexique.isPresent("WTP5") && !lexique.HostFieldGetData("LOPT5").trim().equalsIgnoreCase(""));
    WTP6.setVisible(lexique.isPresent("WTP6") && !lexique.HostFieldGetData("LOPT6").trim().equalsIgnoreCase(""));
    WTP7.setVisible(lexique.isPresent("WTP7") && !lexique.HostFieldGetData("LOPT7").trim().equalsIgnoreCase(""));
    WTP8.setVisible(lexique.isPresent("WTP8") && !lexique.HostFieldGetData("LOPT8").trim().equalsIgnoreCase(""));
    WTP9.setVisible(lexique.isPresent("WTP9") && !lexique.HostFieldGetData("LOPT9").trim().equalsIgnoreCase(""));
    WTP10.setVisible(lexique.isPresent("WTP10") && !lexique.HostFieldGetData("LOPT10").trim().equalsIgnoreCase(""));
    WTP11.setVisible(lexique.isPresent("WTP11") && !lexique.HostFieldGetData("LOPT11").trim().equalsIgnoreCase(""));
    WTP12.setVisible(lexique.isPresent("WTP12") && !lexique.HostFieldGetData("LOPT12").trim().equalsIgnoreCase(""));
    WTP13.setVisible(lexique.isPresent("WTP13") && !lexique.HostFieldGetData("LOPT13").trim().equalsIgnoreCase(""));
    WTP14.setVisible(lexique.isPresent("WTP14") && !lexique.HostFieldGetData("LOPT14").trim().equalsIgnoreCase(""));
    
    WTP16.setSelected(lexique.HostFieldGetData("WTP15").trim().equals("?"));
    WTP17.setSelected(lexique.HostFieldGetData("WTP15").trim().equals("1"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("ORDRES DE FABRICATION N° @EBNUM@ @EBSUF@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (WTP16.isSelected()) {
      lexique.HostFieldPutData("WTP15", 1, "?");
    }
    else if (WTP17.isSelected()) {
      lexique.HostFieldPutData("WPT15", 1, "1");
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void riBouton1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP1", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP2", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP3", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP4", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP5", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP6", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP7", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP8", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP9", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP13", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBouton14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP14", 0, "X");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_25 = new JLabel();
    WARTR = new RiZoneSortie();
    EBLIB = new RiZoneSortie();
    panel2 = new JPanel();
    WTP1 = new SNBoutonLeger();
    WTP2 = new SNBoutonLeger();
    WTP3 = new SNBoutonLeger();
    WTP4 = new SNBoutonLeger();
    WTP8 = new SNBoutonLeger();
    WTP7 = new SNBoutonLeger();
    WTP6 = new SNBoutonLeger();
    WTP5 = new SNBoutonLeger();
    WTP9 = new SNBoutonLeger();
    WTP10 = new SNBoutonLeger();
    WTP11 = new SNBoutonLeger();
    WTP12 = new SNBoutonLeger();
    WTP13 = new SNBoutonLeger();
    WTP14 = new SNBoutonLeger();
    WTP16 = new JRadioButton();
    WTP17 = new JRadioButton();
    BTD2 = new JPopupMenu();
    OBJ_5 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 555));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_25 ----
          OBJ_25.setText("Article");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(15, 12, 40, 20);

          //---- WARTR ----
          WARTR.setText("@WARTR@");
          WARTR.setName("WARTR");
          panel1.add(WARTR);
          WARTR.setBounds(80, 10, 214, WARTR.getPreferredSize().height);

          //---- EBLIB ----
          EBLIB.setText("@EBLIB@");
          EBLIB.setName("EBLIB");
          panel1.add(EBLIB);
          EBLIB.setBounds(310, 10, 225, EBLIB.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 620, 50);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Options de fin de traitement"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- WTP1 ----
          WTP1.setText("  @OPT1@    @LOPT1@ @LMES1@");
          WTP1.setHorizontalAlignment(SwingConstants.LEADING);
          WTP1.setMaximumSize(new Dimension(200, 26));
          WTP1.setMinimumSize(new Dimension(20, 26));
          WTP1.setPreferredSize(new Dimension(20, 26));
          WTP1.setName("WTP1");
          WTP1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton1ActionPerformed(e);
            }
          });
          panel2.add(WTP1);
          WTP1.setBounds(25, 35, 570, WTP1.getPreferredSize().height);

          //---- WTP2 ----
          WTP2.setText("  @OPT2@    @LOPT2@ @LMES2@");
          WTP2.setHorizontalAlignment(SwingConstants.LEFT);
          WTP2.setMaximumSize(new Dimension(200, 26));
          WTP2.setMinimumSize(new Dimension(20, 26));
          WTP2.setPreferredSize(new Dimension(20, 26));
          WTP2.setName("WTP2");
          WTP2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton2ActionPerformed(e);
            }
          });
          panel2.add(WTP2);
          WTP2.setBounds(25, 60, 570, WTP2.getPreferredSize().height);

          //---- WTP3 ----
          WTP3.setText("  @OPT3@    @LOPT3@ @LMES3@");
          WTP3.setHorizontalAlignment(SwingConstants.LEFT);
          WTP3.setMaximumSize(new Dimension(200, 26));
          WTP3.setMinimumSize(new Dimension(20, 26));
          WTP3.setPreferredSize(new Dimension(20, 26));
          WTP3.setName("WTP3");
          WTP3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton3ActionPerformed(e);
            }
          });
          panel2.add(WTP3);
          WTP3.setBounds(25, 85, 570, WTP3.getPreferredSize().height);

          //---- WTP4 ----
          WTP4.setText("  @OPT4@    @LOPT4@ @LMES4@");
          WTP4.setHorizontalAlignment(SwingConstants.LEFT);
          WTP4.setMaximumSize(new Dimension(200, 26));
          WTP4.setMinimumSize(new Dimension(20, 26));
          WTP4.setPreferredSize(new Dimension(20, 26));
          WTP4.setName("WTP4");
          WTP4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton4ActionPerformed(e);
            }
          });
          panel2.add(WTP4);
          WTP4.setBounds(25, 110, 570, WTP4.getPreferredSize().height);

          //---- WTP8 ----
          WTP8.setText("  @OPT8@    @LOPT8@ @LMES8@");
          WTP8.setHorizontalAlignment(SwingConstants.LEFT);
          WTP8.setMaximumSize(new Dimension(200, 26));
          WTP8.setMinimumSize(new Dimension(20, 26));
          WTP8.setPreferredSize(new Dimension(20, 26));
          WTP8.setName("WTP8");
          WTP8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTP8ActionPerformed(e);
            }
          });
          panel2.add(WTP8);
          WTP8.setBounds(25, 210, 570, WTP8.getPreferredSize().height);

          //---- WTP7 ----
          WTP7.setText("  @OPT7@    @LOPT7@ @LMES7@");
          WTP7.setHorizontalAlignment(SwingConstants.LEFT);
          WTP7.setMaximumSize(new Dimension(200, 26));
          WTP7.setMinimumSize(new Dimension(20, 26));
          WTP7.setPreferredSize(new Dimension(20, 26));
          WTP7.setName("WTP7");
          WTP7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTP7ActionPerformed(e);
            }
          });
          panel2.add(WTP7);
          WTP7.setBounds(25, 185, 570, WTP7.getPreferredSize().height);

          //---- WTP6 ----
          WTP6.setText("  @OPT6@    @LOPT6@ @LMES6@");
          WTP6.setHorizontalAlignment(SwingConstants.LEFT);
          WTP6.setMaximumSize(new Dimension(200, 26));
          WTP6.setMinimumSize(new Dimension(20, 26));
          WTP6.setPreferredSize(new Dimension(20, 26));
          WTP6.setName("WTP6");
          WTP6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTP6ActionPerformed(e);
            }
          });
          panel2.add(WTP6);
          WTP6.setBounds(25, 160, 570, WTP6.getPreferredSize().height);

          //---- WTP5 ----
          WTP5.setText("  @OPT5@    @LOPT5@ @LMES5@");
          WTP5.setHorizontalAlignment(SwingConstants.LEFT);
          WTP5.setMaximumSize(new Dimension(200, 26));
          WTP5.setMinimumSize(new Dimension(20, 26));
          WTP5.setPreferredSize(new Dimension(20, 26));
          WTP5.setName("WTP5");
          WTP5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTP5ActionPerformed(e);
            }
          });
          panel2.add(WTP5);
          WTP5.setBounds(25, 135, 570, WTP5.getPreferredSize().height);

          //---- WTP9 ----
          WTP9.setText("  @OPT9@    @LOPT9@ @LMES9@");
          WTP9.setHorizontalAlignment(SwingConstants.LEFT);
          WTP9.setMaximumSize(new Dimension(200, 26));
          WTP9.setMinimumSize(new Dimension(20, 26));
          WTP9.setPreferredSize(new Dimension(20, 26));
          WTP9.setName("WTP9");
          WTP9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton9ActionPerformed(e);
            }
          });
          panel2.add(WTP9);
          WTP9.setBounds(25, 235, 570, WTP9.getPreferredSize().height);

          //---- WTP10 ----
          WTP10.setText("  @OPT10@    @LOPT10@ @LMES10@");
          WTP10.setHorizontalAlignment(SwingConstants.LEFT);
          WTP10.setMaximumSize(new Dimension(200, 26));
          WTP10.setMinimumSize(new Dimension(20, 26));
          WTP10.setPreferredSize(new Dimension(20, 26));
          WTP10.setName("WTP10");
          WTP10.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton10ActionPerformed(e);
            }
          });
          panel2.add(WTP10);
          WTP10.setBounds(25, 260, 570, WTP10.getPreferredSize().height);

          //---- WTP11 ----
          WTP11.setText("  @OPT11@    @LOPT11@ @LMES11@");
          WTP11.setHorizontalAlignment(SwingConstants.LEFT);
          WTP11.setMaximumSize(new Dimension(200, 26));
          WTP11.setMinimumSize(new Dimension(20, 26));
          WTP11.setPreferredSize(new Dimension(20, 26));
          WTP11.setName("WTP11");
          WTP11.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton11ActionPerformed(e);
            }
          });
          panel2.add(WTP11);
          WTP11.setBounds(25, 285, 570, WTP11.getPreferredSize().height);

          //---- WTP12 ----
          WTP12.setText("  @OPT12@    @LOPT12@ @LMES12@");
          WTP12.setHorizontalAlignment(SwingConstants.LEFT);
          WTP12.setMaximumSize(new Dimension(200, 26));
          WTP12.setMinimumSize(new Dimension(20, 26));
          WTP12.setPreferredSize(new Dimension(20, 26));
          WTP12.setName("WTP12");
          WTP12.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton12ActionPerformed(e);
            }
          });
          panel2.add(WTP12);
          WTP12.setBounds(25, 310, 570, WTP12.getPreferredSize().height);

          //---- WTP13 ----
          WTP13.setText("  @OPT13@    @LOPT13@ @LMES13@");
          WTP13.setHorizontalAlignment(SwingConstants.LEFT);
          WTP13.setMaximumSize(new Dimension(200, 26));
          WTP13.setMinimumSize(new Dimension(20, 26));
          WTP13.setPreferredSize(new Dimension(20, 26));
          WTP13.setName("WTP13");
          WTP13.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton13ActionPerformed(e);
            }
          });
          panel2.add(WTP13);
          WTP13.setBounds(25, 335, 570, WTP13.getPreferredSize().height);

          //---- WTP14 ----
          WTP14.setText("  @OPT14@    @LOPT14@ @LMES14@");
          WTP14.setHorizontalAlignment(SwingConstants.LEFT);
          WTP14.setMaximumSize(new Dimension(200, 26));
          WTP14.setMinimumSize(new Dimension(20, 26));
          WTP14.setPreferredSize(new Dimension(20, 26));
          WTP14.setName("WTP14");
          WTP14.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBouton14ActionPerformed(e);
            }
          });
          panel2.add(WTP14);
          WTP14.setBounds(25, 360, 570, WTP14.getPreferredSize().height);

          //---- WTP16 ----
          WTP16.setText("  Edition de l'ordre de fabrication (en mode normal)");
          WTP16.setHorizontalAlignment(SwingConstants.LEFT);
          WTP16.setMaximumSize(new Dimension(200, 26));
          WTP16.setMinimumSize(new Dimension(20, 26));
          WTP16.setPreferredSize(new Dimension(20, 26));
          WTP16.setBackground(SystemColor.info);
          WTP16.setName("WTP16");
          panel2.add(WTP16);
          WTP16.setBounds(25, 405, 570, WTP16.getPreferredSize().height);

          //---- WTP17 ----
          WTP17.setText("  Edition de l'ordre de fabrication (en mode batch)");
          WTP17.setHorizontalAlignment(SwingConstants.LEFT);
          WTP17.setMaximumSize(new Dimension(200, 26));
          WTP17.setMinimumSize(new Dimension(20, 26));
          WTP17.setPreferredSize(new Dimension(20, 26));
          WTP17.setBackground(SystemColor.info);
          WTP17.setName("WTP17");
          panel2.add(WTP17);
          WTP17.setBounds(25, 435, 570, WTP17.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(5, 60, 620, 485);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_5);
    }

    //---- WTP15 ----
    ButtonGroup WTP15 = new ButtonGroup();
    WTP15.add(WTP16);
    WTP15.add(WTP17);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_25;
  private RiZoneSortie WARTR;
  private RiZoneSortie EBLIB;
  private JPanel panel2;
  private SNBoutonLeger WTP1;
  private SNBoutonLeger WTP2;
  private SNBoutonLeger WTP3;
  private SNBoutonLeger WTP4;
  private SNBoutonLeger WTP8;
  private SNBoutonLeger WTP7;
  private SNBoutonLeger WTP6;
  private SNBoutonLeger WTP5;
  private SNBoutonLeger WTP9;
  private SNBoutonLeger WTP10;
  private SNBoutonLeger WTP11;
  private SNBoutonLeger WTP12;
  private SNBoutonLeger WTP13;
  private SNBoutonLeger WTP14;
  private JRadioButton WTP16;
  private JRadioButton WTP17;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
