
package ri.serien.libecranrpg.sgpm.SGPM24EF;
// Nom Fichier: pop_SGPM24EF_FMTB1_148.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SGPM24EF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _LPR11_Title = { "Standard", "Proportionnel", "Fixe", "Fabrication", "Unitaire", };
  private String[][] _LPR11_Data = { { "LPR11", "STPP1", "STPF1", "STPE1", "STPU1", }, { "LPR21", "STPP2", "STPF2", "STPE2", "STPU2", },
      { "LPR31", "STPP3", "STPF3", "STPE3", "STPU3", }, { "LPR41", "STPP4", "STPF4", "STPE4", "STPU4", },
      { "LPR51", "STPP5", "STPF5", "STPE5", "STPU5", }, };
  private int[] _LPR11_Justification =
      { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private int[] _LPR11_Width = { 159, 120, 120, 120, 123, };
  private String[] _LPR1_Title = { "Actuel", "Proportionnel", "Fixe", "Fabrication", "Unitaire", };
  private String[][] _LPR1_Data = { { "LPR1", "WTPP1", "WTPF1", "WTPE1", "WTPU1", }, { "LPR2", "WTPP2", "WTPF2", "WTPE2", "WTPU2", },
      { "LPR3", "WTPP3", "WTPF3", "WTPE3", "WTPU3", }, { "LPR4", "WTPP4", "WTPF4", "WTPE4", "WTPU4", },
      { "LPR5", "WTPP5", "WTPF5", "WTPE5", "WTPU5", }, };
  private int[] _LPR1_Justification =
      { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  private int[] _LPR1_Width = { 159, 120, 120, 120, 127, };
  
  public SGPM24EF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LPR11.setAspectTable(null, _LPR11_Title, _LPR11_Data, _LPR11_Width, false, _LPR11_Justification, null, null, null);
    LPR1.setAspectTable(null, _LPR1_Title, _LPR1_Data, _LPR1_Width, false, _LPR1_Justification, null, null, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Chiffrage du prix de revient"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD1.getInvoker().getName());
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel8 = new JPanel();
    panel1 = new JPanel();
    OBJ_30 = new JLabel();
    ENETB = new XRiTextField();
    OBJ_32 = new JLabel();
    ENNUM = new XRiTextField();
    OBJ_34 = new JLabel();
    ENSUF = new XRiTextField();
    panel2 = new JPanel();
    OBJ_39 = new JLabel();
    WNL1D = new XRiTextField();
    WNL2D = new XRiTextField();
    WNL3D = new XRiTextField();
    OBJ_43 = new JLabel();
    WNL1F = new XRiTextField();
    WNL2F = new XRiTextField();
    WNL3F = new XRiTextField();
    panel3 = new JPanel();
    P14QTC = new XRiTextField();
    panel4 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    LPR1 = new XRiTable();
    OBJ_54 = new JLabel();
    WTPPG = new XRiTextField();
    WTPFG = new XRiTextField();
    WTPEG = new XRiTextField();
    WTPUG = new XRiTextField();
    panel5 = new JPanel();
    SCROLLPANE_LIST3 = new JScrollPane();
    LPR11 = new XRiTable();
    OBJ_65 = new JLabel();
    STPPG = new XRiTextField();
    STPFG = new XRiTextField();
    STPEG = new XRiTextField();
    STPUG = new XRiTextField();
    panel6 = new JPanel();
    OBJ_70 = new JLabel();
    ETPPG = new XRiTextField();
    ETPFG = new XRiTextField();
    ETPEG = new XRiTextField();
    ETPUG = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD1 = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_23 = new JMenuItem();
    OBJ_22 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 575));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel8 ========
        {
          panel8.setOpaque(false);
          panel8.setName("panel8");
          panel8.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_30 ----
            OBJ_30.setText("Etb");
            OBJ_30.setName("OBJ_30");
            panel1.add(OBJ_30);
            OBJ_30.setBounds(20, 15, 24, 18);

            //---- ENETB ----
            ENETB.setName("ENETB");
            panel1.add(ENETB);
            ENETB.setBounds(60, 10, 40, ENETB.getPreferredSize().height);

            //---- OBJ_32 ----
            OBJ_32.setText("Num\u00e9ro");
            OBJ_32.setName("OBJ_32");
            panel1.add(OBJ_32);
            OBJ_32.setBounds(110, 15, 51, 18);

            //---- ENNUM ----
            ENNUM.setName("ENNUM");
            panel1.add(ENNUM);
            ENNUM.setBounds(165, 10, 60, ENNUM.getPreferredSize().height);

            //---- OBJ_34 ----
            OBJ_34.setText("S");
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(235, 15, 12, 18);

            //---- ENSUF ----
            ENSUF.setName("ENSUF");
            panel1.add(ENSUF);
            ENSUF.setBounds(245, 10, 20, ENSUF.getPreferredSize().height);
          }
          panel8.add(panel1);
          panel1.setBounds(10, 25, 305, 50);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Niveaux"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_39 ----
            OBJ_39.setText("D\u00e9but");
            OBJ_39.setName("OBJ_39");
            panel2.add(OBJ_39);
            OBJ_39.setBounds(20, 34, 39, 20);

            //---- WNL1D ----
            WNL1D.setName("WNL1D");
            panel2.add(WNL1D);
            WNL1D.setBounds(60, 30, 36, WNL1D.getPreferredSize().height);

            //---- WNL2D ----
            WNL2D.setName("WNL2D");
            panel2.add(WNL2D);
            WNL2D.setBounds(100, 30, 36, WNL2D.getPreferredSize().height);

            //---- WNL3D ----
            WNL3D.setName("WNL3D");
            panel2.add(WNL3D);
            WNL3D.setBounds(140, 30, 36, WNL3D.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Fin");
            OBJ_43.setName("OBJ_43");
            panel2.add(OBJ_43);
            OBJ_43.setBounds(220, 34, 21, 20);

            //---- WNL1F ----
            WNL1F.setName("WNL1F");
            panel2.add(WNL1F);
            WNL1F.setBounds(245, 30, 36, WNL1F.getPreferredSize().height);

            //---- WNL2F ----
            WNL2F.setName("WNL2F");
            panel2.add(WNL2F);
            WNL2F.setBounds(285, 30, 36, WNL2F.getPreferredSize().height);

            //---- WNL3F ----
            WNL3F.setName("WNL3F");
            panel2.add(WNL3F);
            WNL3F.setBounds(325, 30, 36, WNL3F.getPreferredSize().height);
          }
          panel8.add(panel2);
          panel2.setBounds(10, 85, 400, 75);

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Quantit\u00e9 fabriqu\u00e9e"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- P14QTC ----
            P14QTC.setName("P14QTC");
            panel3.add(P14QTC);
            P14QTC.setBounds(10, 25, 108, P14QTC.getPreferredSize().height);
          }
          panel8.add(panel3);
          panel3.setBounds(555, 10, 130, 65);

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- LPR1 ----
              LPR1.setFont(new Font("Courier New", Font.PLAIN, 12));
              LPR1.setCellSelectionEnabled(true);
              LPR1.setComponentPopupMenu(BTD);
              LPR1.setName("LPR1");
              SCROLLPANE_LIST2.setViewportView(LPR1);
            }
            panel4.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(5, 5, 660, 110);

            //---- OBJ_54 ----
            OBJ_54.setText("Totaux");
            OBJ_54.setName("OBJ_54");
            panel4.add(OBJ_54);
            OBJ_54.setBounds(10, 119, 63, 20);

            //---- WTPPG ----
            WTPPG.setName("WTPPG");
            panel4.add(WTPPG);
            WTPPG.setBounds(170, 115, 120, WTPPG.getPreferredSize().height);

            //---- WTPFG ----
            WTPFG.setName("WTPFG");
            panel4.add(WTPFG);
            WTPFG.setBounds(294, 115, 120, WTPFG.getPreferredSize().height);

            //---- WTPEG ----
            WTPEG.setName("WTPEG");
            panel4.add(WTPEG);
            WTPEG.setBounds(418, 115, 120, WTPEG.getPreferredSize().height);

            //---- WTPUG ----
            WTPUG.setName("WTPUG");
            panel4.add(WTPUG);
            WTPUG.setBounds(542, 115, 120, WTPUG.getPreferredSize().height);
          }
          panel8.add(panel4);
          panel4.setBounds(10, 170, 675, 155);

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder(""));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //======== SCROLLPANE_LIST3 ========
            {
              SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

              //---- LPR11 ----
              LPR11.setComponentPopupMenu(BTD);
              LPR11.setName("LPR11");
              SCROLLPANE_LIST3.setViewportView(LPR11);
            }
            panel5.add(SCROLLPANE_LIST3);
            SCROLLPANE_LIST3.setBounds(5, 15, 658, 115);

            //---- OBJ_65 ----
            OBJ_65.setText("Totaux");
            OBJ_65.setName("OBJ_65");
            panel5.add(OBJ_65);
            OBJ_65.setBounds(20, 125, 63, 20);

            //---- STPPG ----
            STPPG.setName("STPPG");
            panel5.add(STPPG);
            STPPG.setBounds(170, 130, 120, STPPG.getPreferredSize().height);

            //---- STPFG ----
            STPFG.setName("STPFG");
            panel5.add(STPFG);
            STPFG.setBounds(294, 130, 120, STPFG.getPreferredSize().height);

            //---- STPEG ----
            STPEG.setName("STPEG");
            panel5.add(STPEG);
            STPEG.setBounds(418, 130, 120, STPEG.getPreferredSize().height);

            //---- STPUG ----
            STPUG.setName("STPUG");
            panel5.add(STPUG);
            STPUG.setBounds(542, 130, 120, STPUG.getPreferredSize().height);
          }
          panel8.add(panel5);
          panel5.setBounds(10, 335, 675, 165);

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder(""));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- OBJ_70 ----
            OBJ_70.setText("Ecart");
            OBJ_70.setName("OBJ_70");
            panel6.add(OBJ_70);
            OBJ_70.setBounds(20, 10, 63, 20);

            //---- ETPPG ----
            ETPPG.setName("ETPPG");
            panel6.add(ETPPG);
            ETPPG.setBounds(170, 5, 120, ETPPG.getPreferredSize().height);

            //---- ETPFG ----
            ETPFG.setName("ETPFG");
            panel6.add(ETPFG);
            ETPFG.setBounds(290, 5, 120, ETPFG.getPreferredSize().height);

            //---- ETPEG ----
            ETPEG.setName("ETPEG");
            panel6.add(ETPEG);
            ETPEG.setBounds(410, 5, 120, ETPEG.getPreferredSize().height);

            //---- ETPUG ----
            ETPUG.setName("ETPUG");
            panel6.add(ETPUG);
            ETPUG.setBounds(542, 5, 120, ETPUG.getPreferredSize().height);
          }
          panel8.add(panel6);
          panel6.setBounds(10, 510, 675, 45);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel8.getComponentCount(); i++) {
              Rectangle bounds = panel8.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel8.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel8.setMinimumSize(preferredSize);
            panel8.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel8);
        panel8.setBounds(5, 5, 695, 565);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD1 ========
    {
      BTD1.setName("BTD1");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD1.add(OBJ_20);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_23);

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_22);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel8;
  private JPanel panel1;
  private JLabel OBJ_30;
  private XRiTextField ENETB;
  private JLabel OBJ_32;
  private XRiTextField ENNUM;
  private JLabel OBJ_34;
  private XRiTextField ENSUF;
  private JPanel panel2;
  private JLabel OBJ_39;
  private XRiTextField WNL1D;
  private XRiTextField WNL2D;
  private XRiTextField WNL3D;
  private JLabel OBJ_43;
  private XRiTextField WNL1F;
  private XRiTextField WNL2F;
  private XRiTextField WNL3F;
  private JPanel panel3;
  private XRiTextField P14QTC;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable LPR1;
  private JLabel OBJ_54;
  private XRiTextField WTPPG;
  private XRiTextField WTPFG;
  private XRiTextField WTPEG;
  private XRiTextField WTPUG;
  private JPanel panel5;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable LPR11;
  private JLabel OBJ_65;
  private XRiTextField STPPG;
  private XRiTextField STPFG;
  private XRiTextField STPEG;
  private XRiTextField STPUG;
  private JPanel panel6;
  private JLabel OBJ_70;
  private XRiTextField ETPPG;
  private XRiTextField ETPFG;
  private XRiTextField ETPEG;
  private XRiTextField ETPUG;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD1;
  private JMenuItem OBJ_20;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_22;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
