
package ri.serien.libecranrpg.sgpm.SGPM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class SGPM21FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  public SGPM21FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    EBIN4.setValeursSelection("1", " ");
    
    // Ajout
    initDiverses();
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MALIBR@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBLIB@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EBART@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_25.setVisible(G21FAB.isVisible());
    if (lexique.isPresent("EBDDPX")) {
      lbDate.setText("Date de lancement");
    }
    else {
      lbDate.setText("Date de fin");
    }
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    MES36 = new XRiTextField();
    OBJ_12 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_13 = new JLabel();
    OBJ_35 = new JLabel();
    EBQTR = new XRiTextField();
    WQMI = new XRiTextField();
    MQEC = new XRiTextField();
    EBQTE = new XRiTextField();
    EBMAG = new XRiTextField();
    G21FAB = new XRiTextField();
    OBJ_25 = new JLabel();
    panel1 = new JPanel();
    EBIN4 = new XRiCheckBox();
    OBJ_29 = new JLabel();
    OBJ_20 = new JLabel();
    lbDate = new JLabel();
    EBUNS = new XRiTextField();
    EBDDPX = new XRiCalendrier();
    EBDFSX = new XRiCalendrier();
    EBDFPX = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_6 = new JMenuItem();
    OBJ_5 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(720, 425));
    setPreferredSize(new Dimension(720, 425));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");
            
            // ---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);
          
          // ======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");
            
            // ---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);
          
          // ======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");
            
            // ---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);
          
          // ======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");
            
            // ---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);
          
          // ======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");
            
            // ---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);
          
          // ======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");
            
            // ---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);
          
          // ======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");
            
            // ---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);
          
          // ======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");
            
            // ---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);
          
          // ======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");
            
            // ---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel3 ========
        {
          panel3.setBorder(
              new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.DEFAULT_POSITION, new Font("Tahoma", Font.PLAIN, 11)));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- MES36 ----
          MES36.setName("MES36");
          panel3.add(MES36);
          MES36.setBounds(15, 220, 498, MES36.getPreferredSize().height);
          
          // ---- OBJ_12 ----
          OBJ_12.setText("@MALIBR@");
          OBJ_12.setName("OBJ_12");
          panel3.add(OBJ_12);
          OBJ_12.setBounds(210, 44, 288, 20);
          
          // ---- OBJ_33 ----
          OBJ_33.setText("@EBLIB@");
          OBJ_33.setName("OBJ_33");
          panel3.add(OBJ_33);
          OBJ_33.setBounds(175, 15, 231, 20);
          
          // ---- OBJ_28 ----
          OBJ_28.setText("@EBART@");
          OBJ_28.setName("OBJ_28");
          panel3.add(OBJ_28);
          OBJ_28.setBounds(15, 15, 161, 20);
          
          // ---- OBJ_15 ----
          OBJ_15.setText("Quantit\u00e9 \u00e9conomique");
          OBJ_15.setName("OBJ_15");
          panel3.add(OBJ_15);
          OBJ_15.setBounds(15, 104, 130, 20);
          
          // ---- OBJ_22 ----
          OBJ_22.setText("Quantit\u00e9 \u00e0 fabriquer");
          OBJ_22.setName("OBJ_22");
          panel3.add(OBJ_22);
          OBJ_22.setBounds(15, 134, 120, 20);
          
          // ---- OBJ_24 ----
          OBJ_24.setText("Quantit\u00e9 fabriqu\u00e9e");
          OBJ_24.setName("OBJ_24");
          panel3.add(OBJ_24);
          OBJ_24.setBounds(15, 164, 112, 20);
          
          // ---- OBJ_13 ----
          OBJ_13.setText("Quantit\u00e9 minimale");
          OBJ_13.setName("OBJ_13");
          panel3.add(OBJ_13);
          OBJ_13.setBounds(15, 74, 109, 20);
          
          // ---- OBJ_35 ----
          OBJ_35.setText("Magasin");
          OBJ_35.setName("OBJ_35");
          panel3.add(OBJ_35);
          OBJ_35.setBounds(15, 44, 109, 20);
          
          // ---- EBQTR ----
          EBQTR.setComponentPopupMenu(BTD);
          EBQTR.setName("EBQTR");
          panel3.add(EBQTR);
          EBQTR.setBounds(175, 160, 108, EBQTR.getPreferredSize().height);
          
          // ---- WQMI ----
          WQMI.setComponentPopupMenu(BTD);
          WQMI.setName("WQMI");
          panel3.add(WQMI);
          WQMI.setBounds(175, 70, 108, WQMI.getPreferredSize().height);
          
          // ---- MQEC ----
          MQEC.setComponentPopupMenu(BTD);
          MQEC.setName("MQEC");
          panel3.add(MQEC);
          MQEC.setBounds(175, 100, 108, MQEC.getPreferredSize().height);
          
          // ---- EBQTE ----
          EBQTE.setComponentPopupMenu(BTD);
          EBQTE.setName("EBQTE");
          panel3.add(EBQTE);
          EBQTE.setBounds(175, 130, 108, EBQTE.getPreferredSize().height);
          
          // ---- EBMAG ----
          EBMAG.setName("EBMAG");
          panel3.add(EBMAG);
          EBMAG.setBounds(175, 40, 30, EBMAG.getPreferredSize().height);
          
          // ---- G21FAB ----
          G21FAB.setComponentPopupMenu(BTD);
          G21FAB.setName("G21FAB");
          panel3.add(G21FAB);
          G21FAB.setBounds(175, 190, 108, G21FAB.getPreferredSize().height);
          
          // ---- OBJ_25 ----
          OBJ_25.setText("Quantit\u00e9 fabricable");
          OBJ_25.setName("OBJ_25");
          panel3.add(OBJ_25);
          OBJ_25.setBounds(15, 194, 112, 20);
        }
        p_contenu.add(panel3);
        panel3.setBounds(10, 10, 530, 260);
        
        // ======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Dates de fabrication"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);
          
          // ---- EBIN4 ----
          EBIN4.setText("Forcer \u00e0 z\u00e9ro");
          EBIN4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          EBIN4.setName("EBIN4");
          panel1.add(EBIN4);
          EBIN4.setBounds(15, 100, 105, 20);
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Unit\u00e9 de stock");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(175, 100, 88, 20);
          
          // ---- OBJ_20 ----
          OBJ_20.setText("Fin pr\u00e9vue");
          OBJ_20.setName("OBJ_20");
          panel1.add(OBJ_20);
          OBJ_20.setBounds(15, 65, 155, 28);
          
          // ---- lbDate ----
          lbDate.setText("Date de lancement");
          lbDate.setName("lbDate");
          panel1.add(lbDate);
          lbDate.setBounds(15, 30, 115, 28);
          
          // ---- EBUNS ----
          EBUNS.setName("EBUNS");
          panel1.add(EBUNS);
          EBUNS.setBounds(305, 96, 33, EBUNS.getPreferredSize().height);
          
          // ---- EBDDPX ----
          EBDDPX.setName("EBDDPX");
          panel1.add(EBDDPX);
          EBDDPX.setBounds(175, 30, 115, EBDDPX.getPreferredSize().height);
          
          // ---- EBDFSX ----
          EBDFSX.setName("EBDFSX");
          panel1.add(EBDFSX);
          EBDFSX.setBounds(175, 65, 115, EBDFSX.getPreferredSize().height);
          
          // ---- EBDFPX ----
          EBDFPX.setName("EBDFPX");
          panel1.add(EBDFPX);
          EBDFPX.setBounds(175, 30, 115, EBDFPX.getPreferredSize().height);
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 275, 530, 140);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_6 ----
      OBJ_6.setText("Choix possibles");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      BTD.add(OBJ_6);
      
      // ---- OBJ_5 ----
      OBJ_5.setText("Aide en ligne");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      BTD.add(OBJ_5);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private JPanel p_contenu;
  private JPanel panel3;
  private XRiTextField MES36;
  private RiZoneSortie OBJ_12;
  private RiZoneSortie OBJ_33;
  private JLabel OBJ_28;
  private JLabel OBJ_15;
  private JLabel OBJ_22;
  private JLabel OBJ_24;
  private JLabel OBJ_13;
  private JLabel OBJ_35;
  private XRiTextField EBQTR;
  private XRiTextField WQMI;
  private XRiTextField MQEC;
  private XRiTextField EBQTE;
  private XRiTextField EBMAG;
  private XRiTextField G21FAB;
  private JLabel OBJ_25;
  private JPanel panel1;
  private XRiCheckBox EBIN4;
  private JLabel OBJ_29;
  private JLabel OBJ_20;
  private JLabel lbDate;
  private XRiTextField EBUNS;
  private XRiCalendrier EBDDPX;
  private XRiCalendrier EBDFSX;
  private XRiCalendrier EBDFPX;
  private JPopupMenu BTD;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_5;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
