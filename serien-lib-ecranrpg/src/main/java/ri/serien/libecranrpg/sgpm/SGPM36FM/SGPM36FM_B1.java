
package ri.serien.libecranrpg.sgpm.SGPM36FM;
// Nom Fichier: b_SGPM36FM_FMTB1_FMTF1_107.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGPM36FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM36FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTOU1.setValeursSelection("**", "  ");
    WOPT1.setValeursSelection("OUI", "NON");
    WTOU2.setValeursSelection("**", "  ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_26.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@CODE@")).trim());
    LOPED.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOPED@")).trim());
    LOPEF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOPEF@")).trim());
    LIBF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBF@")).trim());
    LARGF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LARGF@")).trim());
    LATEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LATEL@")).trim());
    LAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LAFF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    
    OBJ_44.setVisible(lexique.isTrue("94"));
    LAFF.setVisible(lexique.isTrue("94"));
    OBJ_52.setVisible(lexique.isTrue("92"));
    LATEL.setVisible(lexique.isTrue("92"));
    
    if (lexique.isTrue("94")) {
      OBJ_26.setTitle("Plage des codes qualication");
    }
    if (lexique.isTrue("92")) {
      OBJ_26.setTitle("Plage de postes");
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOU2ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_22 = new JXTitledSeparator();
    OBJ_26 = new JXTitledSeparator();
    P_SEL1 = new JPanel();
    LOPED = new RiZoneSortie();
    LOPEF = new RiZoneSortie();
    OPED = new XRiTextField();
    OPEF = new XRiTextField();
    OBJ_54 = new JLabel();
    OBJ_56 = new JLabel();
    P_SEL0 = new JPanel();
    LIBF = new RiZoneSortie();
    LARGF = new RiZoneSortie();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    ARGD = new XRiTextField();
    ARGF = new XRiTextField();
    OBJ_24 = new JXTitledSeparator();
    OBJ_25 = new JXTitledSeparator();
    LATEL = new RiZoneSortie();
    LAFF = new RiZoneSortie();
    WTOU2 = new XRiCheckBox();
    WOPT1 = new XRiCheckBox();
    OBJ_50 = new JLabel();
    OBJ_44 = new JLabel();
    WTOU1 = new XRiCheckBox();
    OBJ_52 = new JLabel();
    ATEL = new XRiTextField();
    NUMAFF = new XRiTextField();
    OBJ_51 = new JLabel();
    DATD = new XRiCalendrier();
    DATF = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(830, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(10, 10, 785, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(180, 35, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(180, 65, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(25, 50, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(70, 50), bouton_etablissement.getPreferredSize()));

            //---- OBJ_22 ----
            OBJ_22.setTitle("Op\u00e9rations");
            OBJ_22.setName("OBJ_22");
            panel1.add(OBJ_22);
            OBJ_22.setBounds(10, 365, 785, OBJ_22.getPreferredSize().height);

            //---- OBJ_26 ----
            OBJ_26.setTitle("@CODE@");
            OBJ_26.setName("OBJ_26");
            panel1.add(OBJ_26);
            OBJ_26.setBounds(10, 180, 785, OBJ_26.getPreferredSize().height);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- LOPED ----
              LOPED.setText("@LOPED@");
              LOPED.setName("LOPED");
              P_SEL1.add(LOPED);
              LOPED.setBounds(345, 10, 231, LOPED.getPreferredSize().height);

              //---- LOPEF ----
              LOPEF.setText("@LOPEF@");
              LOPEF.setName("LOPEF");
              P_SEL1.add(LOPEF);
              LOPEF.setBounds(345, 40, 231, LOPEF.getPreferredSize().height);

              //---- OPED ----
              OPED.setComponentPopupMenu(BTD);
              OPED.setName("OPED");
              P_SEL1.add(OPED);
              OPED.setBounds(130, 10, 210, OPED.getPreferredSize().height);

              //---- OPEF ----
              OPEF.setComponentPopupMenu(BTD);
              OPEF.setName("OPEF");
              P_SEL1.add(OPEF);
              OPEF.setBounds(130, 40, 210, OPEF.getPreferredSize().height);

              //---- OBJ_54 ----
              OBJ_54.setText("Code de d\u00e9but");
              OBJ_54.setName("OBJ_54");
              P_SEL1.add(OBJ_54);
              OBJ_54.setBounds(10, 11, 115, 26);

              //---- OBJ_56 ----
              OBJ_56.setText("Code de fin");
              OBJ_56.setName("OBJ_56");
              P_SEL1.add(OBJ_56);
              OBJ_56.setBounds(10, 41, 115, 26);
            }
            panel1.add(P_SEL1);
            P_SEL1.setBounds(180, 390, 595, 75);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- LIBF ----
              LIBF.setText("@LIBF@");
              LIBF.setName("LIBF");
              P_SEL0.add(LIBF);
              LIBF.setBounds(195, 12, 306, LIBF.getPreferredSize().height);

              //---- LARGF ----
              LARGF.setText("@LARGF@");
              LARGF.setName("LARGF");
              P_SEL0.add(LARGF);
              LARGF.setBounds(195, 42, 306, LARGF.getPreferredSize().height);

              //---- OBJ_47 ----
              OBJ_47.setText("Code de d\u00e9but");
              OBJ_47.setName("OBJ_47");
              P_SEL0.add(OBJ_47);
              OBJ_47.setBounds(10, 12, 120, 24);

              //---- OBJ_48 ----
              OBJ_48.setText("Code de fin");
              OBJ_48.setName("OBJ_48");
              P_SEL0.add(OBJ_48);
              OBJ_48.setBounds(10, 42, 120, 24);

              //---- ARGD ----
              ARGD.setComponentPopupMenu(BTD);
              ARGD.setName("ARGD");
              P_SEL0.add(ARGD);
              ARGD.setBounds(130, 10, 60, ARGD.getPreferredSize().height);

              //---- ARGF ----
              ARGF.setComponentPopupMenu(BTD);
              ARGF.setName("ARGF");
              P_SEL0.add(ARGF);
              ARGF.setBounds(130, 40, 60, ARGF.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(180, 205, 595, 75);

            //---- OBJ_24 ----
            OBJ_24.setTitle("");
            OBJ_24.setName("OBJ_24");
            panel1.add(OBJ_24);
            OBJ_24.setBounds(10, 300, 785, OBJ_24.getPreferredSize().height);

            //---- OBJ_25 ----
            OBJ_25.setTitle("");
            OBJ_25.setName("OBJ_25");
            panel1.add(OBJ_25);
            OBJ_25.setBounds(10, 115, 785, 9);

            //---- LATEL ----
            LATEL.setText("@LATEL@");
            LATEL.setName("LATEL");
            panel1.add(LATEL);
            LATEL.setBounds(245, 315, 231, LATEL.getPreferredSize().height);

            //---- LAFF ----
            LAFF.setText("@LAFF@");
            LAFF.setName("LAFF");
            panel1.add(LAFF);
            LAFF.setBounds(205, 492, 235, LAFF.getPreferredSize().height);

            //---- WTOU2 ----
            WTOU2.setText("S\u00e9lection compl\u00e8te");
            WTOU2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU2.setName("WTOU2");
            WTOU2.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU2ActionPerformed(e);
              }
            });
            panel1.add(WTOU2);
            WTOU2.setBounds(25, 405, 150, 19);

            //---- WOPT1 ----
            WOPT1.setText("Edition d\u00e9taill\u00e9e");
            WOPT1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WOPT1.setName("WOPT1");
            panel1.add(WOPT1);
            WOPT1.setBounds(560, 139, 121, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("P\u00e9riode r\u00e9f\u00e9rence du");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(25, 139, 150, 20);

            //---- OBJ_44 ----
            OBJ_44.setText("Num\u00e9ro affectation");
            OBJ_44.setName("OBJ_44");
            panel1.add(OBJ_44);
            OBJ_44.setBounds(25, 494, 150, 20);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e8te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel1.add(WTOU1);
            WTOU1.setBounds(25, 225, 150, 15);

            //---- OBJ_52 ----
            OBJ_52.setText("Atelier");
            OBJ_52.setName("OBJ_52");
            panel1.add(OBJ_52);
            OBJ_52.setBounds(25, 317, 150, 20);

            //---- ATEL ----
            ATEL.setComponentPopupMenu(BTD);
            ATEL.setName("ATEL");
            panel1.add(ATEL);
            ATEL.setBounds(180, 313, 60, ATEL.getPreferredSize().height);

            //---- NUMAFF ----
            NUMAFF.setComponentPopupMenu(BTD);
            NUMAFF.setName("NUMAFF");
            panel1.add(NUMAFF);
            NUMAFF.setBounds(180, 490, 20, NUMAFF.getPreferredSize().height);

            //---- OBJ_51 ----
            OBJ_51.setText("au");
            OBJ_51.setName("OBJ_51");
            panel1.add(OBJ_51);
            OBJ_51.setBounds(297, 139, 25, 20);

            //---- DATD ----
            DATD.setName("DATD");
            panel1.add(DATD);
            DATD.setBounds(180, 135, 105, DATD.getPreferredSize().height);

            //---- DATF ----
            DATF.setName("DATF");
            panel1.add(DATF);
            DATF.setBounds(334, 135, 105, DATF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 804, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 532, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_26;
  private JPanel P_SEL1;
  private RiZoneSortie LOPED;
  private RiZoneSortie LOPEF;
  private XRiTextField OPED;
  private XRiTextField OPEF;
  private JLabel OBJ_54;
  private JLabel OBJ_56;
  private JPanel P_SEL0;
  private RiZoneSortie LIBF;
  private RiZoneSortie LARGF;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private XRiTextField ARGD;
  private XRiTextField ARGF;
  private JXTitledSeparator OBJ_24;
  private JXTitledSeparator OBJ_25;
  private RiZoneSortie LATEL;
  private RiZoneSortie LAFF;
  private XRiCheckBox WTOU2;
  private XRiCheckBox WOPT1;
  private JLabel OBJ_50;
  private JLabel OBJ_44;
  private XRiCheckBox WTOU1;
  private JLabel OBJ_52;
  private XRiTextField ATEL;
  private XRiTextField NUMAFF;
  private JLabel OBJ_51;
  private XRiCalendrier DATD;
  private XRiCalendrier DATF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
