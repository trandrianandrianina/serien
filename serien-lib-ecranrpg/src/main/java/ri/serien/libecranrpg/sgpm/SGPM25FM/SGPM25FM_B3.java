
package ri.serien.libecranrpg.sgpm.SGPM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SGPM25FM_B3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM25FM_B3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_24.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB1@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB2@")).trim());
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART1@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART2@")).trim());
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPNBR@")).trim());
    OBJ_29.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPQB1@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPBA1@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XPAF1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // OBJ_37.setVisible( lexique.isPresent("XPBA1"));
    // OBJ_28.setVisible( lexique.isPresent("XPQB1"));
    // OBJ_32.setVisible( lexique.isPresent("XPAF1"));
    // OBJ_39.setVisible( lexique.isPresent("XPBA1"));
    // OBJ_38.setVisible( lexique.isPresent("XPQB1"));
    // OBJ_36.setVisible( lexique.isPresent("XPNBR"));
    // OBJ_30.setVisible( lexique.isPresent("XPBA1"));
    // OBJ_31.setVisible( lexique.isPresent("XPAF1"));
    // OBJ_29.setVisible( lexique.isPresent("XPQB1"));
    // OBJ_21.setVisible( lexique.isPresent("WART2"));
    // OBJ_27.setVisible( lexique.isPresent("XPNBR"));
    // OBJ_25.setVisible( lexique.isPresent("WART2"));
    // OBJ_23.setVisible( lexique.isPresent("WART1"));
    // OBJ_26.setVisible( lexique.isPresent("WLIB2"));
    // OBJ_24.setVisible( lexique.isPresent("WLIB1"));
    
    setTitle("Remplacement Code Composant");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_24 = new RiZoneSortie();
    OBJ_26 = new RiZoneSortie();
    OBJ_19 = new JXTitledSeparator();
    OBJ_23 = new RiZoneSortie();
    OBJ_25 = new RiZoneSortie();
    OBJ_27 = new RiZoneSortie();
    OBJ_21 = new JLabel();
    OBJ_22 = new JLabel();
    OBJ_29 = new RiZoneSortie();
    OBJ_31 = new JLabel();
    OBJ_30 = new RiZoneSortie();
    OBJ_36 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    OBJ_32 = new RiZoneSortie();
    OBJ_28 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_18 = new JXTitledSeparator();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 270));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_24 ----
          OBJ_24.setText("@WLIB1@");
          OBJ_24.setName("OBJ_24");
          panel1.add(OBJ_24);
          OBJ_24.setBounds(325, 45, 231, OBJ_24.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("@WLIB2@");
          OBJ_26.setName("OBJ_26");
          panel1.add(OBJ_26);
          OBJ_26.setBounds(325, 110, 231, OBJ_26.getPreferredSize().height);

          //---- OBJ_19 ----
          OBJ_19.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_19.setTitle("@COMP@");
          OBJ_19.setName("OBJ_19");
          panel1.add(OBJ_19);
          OBJ_19.setBounds(20, 15, 545, 20);

          //---- OBJ_23 ----
          OBJ_23.setText("@WART1@");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(155, 45, 151, OBJ_23.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("@WART2@");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(155, 110, 151, OBJ_25.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("@XPNBR@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(20, 170, 85, OBJ_27.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Code article");
          OBJ_21.setName("OBJ_21");
          panel1.add(OBJ_21);
          OBJ_21.setBounds(20, 110, 75, 20);

          //---- OBJ_22 ----
          OBJ_22.setText("Code article");
          OBJ_22.setName("OBJ_22");
          panel1.add(OBJ_22);
          OBJ_22.setBounds(20, 47, 90, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("@XPQB1@");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(155, 170, 79, OBJ_29.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Affectation");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(430, 147, 65, 20);

          //---- OBJ_30 ----
          OBJ_30.setText("@XPBA1@");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(325, 170, 80, OBJ_30.getPreferredSize().height);

          //---- OBJ_36 ----
          OBJ_36.setText("Nombre");
          OBJ_36.setName("OBJ_36");
          panel1.add(OBJ_36);
          OBJ_36.setBounds(20, 150, 52, 15);

          //---- OBJ_38 ----
          OBJ_38.setText("Quantit\u00e9");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(155, 150, 52, 15);

          //---- OBJ_39 ----
          OBJ_39.setText("Base");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(325, 150, 36, 15);

          //---- OBJ_32 ----
          OBJ_32.setText("@XPAF1@");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(430, 170, 70, OBJ_32.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setText("x");
          OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(120, 172, 20, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("/");
          OBJ_37.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(265, 172, 30, 20);

          //---- OBJ_18 ----
          OBJ_18.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_18.setTitle("@COMP2@");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(20, 80, 545, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 610, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 248, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_24;
  private RiZoneSortie OBJ_26;
  private JXTitledSeparator OBJ_19;
  private RiZoneSortie OBJ_23;
  private RiZoneSortie OBJ_25;
  private RiZoneSortie OBJ_27;
  private JLabel OBJ_21;
  private JLabel OBJ_22;
  private RiZoneSortie OBJ_29;
  private JLabel OBJ_31;
  private RiZoneSortie OBJ_30;
  private JLabel OBJ_36;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private RiZoneSortie OBJ_32;
  private JLabel OBJ_28;
  private JLabel OBJ_37;
  private JXTitledSeparator OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
