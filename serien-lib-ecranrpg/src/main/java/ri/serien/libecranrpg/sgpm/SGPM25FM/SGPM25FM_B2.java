
package ri.serien.libecranrpg.sgpm.SGPM25FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGPM25FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM25FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WREP.setValeursSelection("OUI", "NON");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_27.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB1@")).trim());
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // XPAF1.setEnabled( lexique.isPresent("XPAF1"));
    // XPBA1.setEnabled( lexique.isPresent("XPBA1"));
    // XPQB1.setEnabled( lexique.isPresent("XPQB1"));
    // XPNBR.setEnabled( lexique.isPresent("XPNBR"));
    // WART2.setEnabled( lexique.isPresent("WART2"));
    // WART1.setEnabled( lexique.isPresent("WART1"));
    // WREP.setEnabled( lexique.isPresent("WREP"));
    // WREP.setSelected(lexique.HostFieldGetData("WREP").equalsIgnoreCase("OUI"));
    // OBJ_31.setVisible( lexique.isPresent("WLIB2"));
    // OBJ_27.setVisible( lexique.isPresent("WLIB1"));
    
    setTitle("Remplacement Code Composant");
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WREP.isSelected())
    // lexique.HostFieldPutData("WREP", 0, "OUI");
    // else
    // lexique.HostFieldPutData("WREP", 0, "NON");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_27 = new RiZoneSortie();
    OBJ_31 = new RiZoneSortie();
    WREP = new XRiCheckBox();
    WART1 = new XRiTextField();
    WART2 = new XRiTextField();
    OBJ_28 = new JXTitledSeparator();
    XPNBR = new XRiTextField();
    XPQB1 = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_37 = new JLabel();
    XPBA1 = new XRiTextField();
    OBJ_32 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_44 = new JLabel();
    XPAF1 = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_30 = new JXTitledSeparator();
    BTD2 = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(810, 305));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("@WLIB1@");
          OBJ_27.setName("OBJ_27");
          panel1.add(OBJ_27);
          OBJ_27.setBounds(340, 47, 231, OBJ_27.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("@WLIB2@");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(340, 122, 231, OBJ_31.getPreferredSize().height);

          //---- WREP ----
          WREP.setText("Mise \u00e0 jour du libell\u00e9");
          WREP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WREP.setName("WREP");
          panel1.add(WREP);
          WREP.setBounds(30, 220, 200, 20);

          //---- WART1 ----
          WART1.setComponentPopupMenu(BTD2);
          WART1.setName("WART1");
          panel1.add(WART1);
          WART1.setBounds(120, 45, 210, WART1.getPreferredSize().height);

          //---- WART2 ----
          WART2.setComponentPopupMenu(BTD2);
          WART2.setName("WART2");
          panel1.add(WART2);
          WART2.setBounds(120, 120, 210, WART2.getPreferredSize().height);

          //---- OBJ_28 ----
          OBJ_28.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_28.setTitle("Nouveau composant");
          OBJ_28.setName("OBJ_28");
          panel1.add(OBJ_28);
          OBJ_28.setBounds(20, 90, 565, 20);

          //---- XPNBR ----
          XPNBR.setComponentPopupMenu(BTD2);
          XPNBR.setName("XPNBR");
          panel1.add(XPNBR);
          XPNBR.setBounds(30, 175, 98, XPNBR.getPreferredSize().height);

          //---- XPQB1 ----
          XPQB1.setComponentPopupMenu(BTD2);
          XPQB1.setName("XPQB1");
          panel1.add(XPQB1);
          XPQB1.setBounds(150, 175, 98, XPQB1.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Code article");
          OBJ_25.setName("OBJ_25");
          panel1.add(OBJ_25);
          OBJ_25.setBounds(35, 49, 85, 20);

          //---- OBJ_29 ----
          OBJ_29.setText("Code article");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(35, 124, 85, 20);

          //---- OBJ_37 ----
          OBJ_37.setText("Affectation");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(350, 179, 70, 20);

          //---- XPBA1 ----
          XPBA1.setComponentPopupMenu(BTD2);
          XPBA1.setName("XPBA1");
          panel1.add(XPBA1);
          XPBA1.setBounds(275, 175, 58, XPBA1.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Nombre");
          OBJ_32.setName("OBJ_32");
          panel1.add(OBJ_32);
          OBJ_32.setBounds(35, 155, 52, 15);

          //---- OBJ_43 ----
          OBJ_43.setText("Quantit\u00e9");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(155, 155, 52, 15);

          //---- OBJ_44 ----
          OBJ_44.setText("Base");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(280, 155, 36, 15);

          //---- XPAF1 ----
          XPAF1.setComponentPopupMenu(BTD2);
          XPAF1.setName("XPAF1");
          panel1.add(XPAF1);
          XPAF1.setBounds(420, 175, 20, XPAF1.getPreferredSize().height);

          //---- OBJ_34 ----
          OBJ_34.setText("x");
          OBJ_34.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_34.setName("OBJ_34");
          panel1.add(OBJ_34);
          OBJ_34.setBounds(130, 179, 15, 20);

          //---- OBJ_42 ----
          OBJ_42.setText("/");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(260, 179, 9, 20);

          //---- OBJ_30 ----
          OBJ_30.setFont(new Font("sansserif", Font.BOLD, 12));
          OBJ_30.setTitle("Composant existant");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(20, 15, 565, 20);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 605, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 265, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie OBJ_27;
  private RiZoneSortie OBJ_31;
  private XRiCheckBox WREP;
  private XRiTextField WART1;
  private XRiTextField WART2;
  private JXTitledSeparator OBJ_28;
  private XRiTextField XPNBR;
  private XRiTextField XPQB1;
  private JLabel OBJ_25;
  private JLabel OBJ_29;
  private JLabel OBJ_37;
  private XRiTextField XPBA1;
  private JLabel OBJ_32;
  private JLabel OBJ_43;
  private JLabel OBJ_44;
  private XRiTextField XPAF1;
  private JLabel OBJ_34;
  private JLabel OBJ_42;
  private JXTitledSeparator OBJ_30;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
