
package ri.serien.libecranrpg.sgpm.SGPM21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class SGPM21FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM21FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    V06F.setValeursSelection("OUI", "NON");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Dates"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    V06F = new XRiCheckBox();
    OBJ_14 = new JLabel();
    OBJ_25 = new JLabel();
    P10DDX = new XRiCalendrier();
    P10DFX = new XRiCalendrier();
    P10HDH = new XRiTextField();
    P10HDM = new XRiTextField();
    P10HFH = new XRiTextField();
    P10HFM = new XRiTextField();
    OBJ_23 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_7 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(580, 155));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("Dates d'affectation"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- V06F ----
          V06F.setText("Affectation d\u00e9finitive");
          V06F.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          V06F.setName("V06F");
          panel2.add(V06F);
          V06F.setBounds(15, 95, 147, 20);

          //---- OBJ_14 ----
          OBJ_14.setText("D\u00e9but production");
          OBJ_14.setName("OBJ_14");
          panel2.add(OBJ_14);
          OBJ_14.setBounds(15, 34, 105, 20);

          //---- OBJ_25 ----
          OBJ_25.setText("Fin production");
          OBJ_25.setName("OBJ_25");
          panel2.add(OBJ_25);
          OBJ_25.setBounds(15, 64, 87, 20);

          //---- P10DDX ----
          P10DDX.setComponentPopupMenu(BTD);
          P10DDX.setName("P10DDX");
          panel2.add(P10DDX);
          P10DDX.setBounds(135, 30, 105, P10DDX.getPreferredSize().height);

          //---- P10DFX ----
          P10DFX.setComponentPopupMenu(BTD);
          P10DFX.setName("P10DFX");
          panel2.add(P10DFX);
          P10DFX.setBounds(135, 60, 105, P10DFX.getPreferredSize().height);

          //---- P10HDH ----
          P10HDH.setComponentPopupMenu(BTD);
          P10HDH.setName("P10HDH");
          panel2.add(P10HDH);
          P10HDH.setBounds(285, 30, 28, P10HDH.getPreferredSize().height);

          //---- P10HDM ----
          P10HDM.setComponentPopupMenu(BTD);
          P10HDM.setName("P10HDM");
          panel2.add(P10HDM);
          P10HDM.setBounds(335, 30, 28, P10HDM.getPreferredSize().height);

          //---- P10HFH ----
          P10HFH.setComponentPopupMenu(BTD);
          P10HFH.setName("P10HFH");
          panel2.add(P10HFH);
          P10HFH.setBounds(285, 60, 28, P10HFH.getPreferredSize().height);

          //---- P10HFM ----
          P10HFM.setComponentPopupMenu(BTD);
          P10HFM.setName("P10HFM");
          panel2.add(P10HFM);
          P10HFM.setBounds(335, 60, 28, P10HFM.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("\u00e0");
          OBJ_23.setName("OBJ_23");
          panel2.add(OBJ_23);
          OBJ_23.setBounds(205, 30, 12, 20);

          //---- OBJ_24 ----
          OBJ_24.setText("h");
          OBJ_24.setName("OBJ_24");
          panel2.add(OBJ_24);
          OBJ_24.setBounds(320, 34, 12, 20);

          //---- OBJ_26 ----
          OBJ_26.setText("\u00e0");
          OBJ_26.setName("OBJ_26");
          panel2.add(OBJ_26);
          OBJ_26.setBounds(205, 60, 12, 20);

          //---- OBJ_27 ----
          OBJ_27.setText("h");
          OBJ_27.setName("OBJ_27");
          panel2.add(OBJ_27);
          OBJ_27.setBounds(320, 64, 12, 20);
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 390, 135);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_7 ----
      OBJ_7.setText("Aide en ligne");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      BTD.add(OBJ_7);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiCheckBox V06F;
  private JLabel OBJ_14;
  private JLabel OBJ_25;
  private XRiCalendrier P10DDX;
  private XRiCalendrier P10DFX;
  private XRiTextField P10HDH;
  private XRiTextField P10HDM;
  private XRiTextField P10HFH;
  private XRiTextField P10HFM;
  private JLabel OBJ_23;
  private JLabel OBJ_24;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JPopupMenu BTD;
  private JMenuItem OBJ_7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
