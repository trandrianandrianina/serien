
package ri.serien.libecranrpg.sgpm.SGPM14EF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libcommun.outils.session.sessionclient.ManagerSessionClient;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiGraphe;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Emmanuel MARCQ
 */
public class SGPM14EF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _LPR1_Title = { "Actuel", "Proportionnel", "Fixe", "Economique", };
  private String[][] _LPR1_Data =
      { { "LPR1", "WTPP1", "WTPF1", "WTPE1" }, { "LPR2", "WTPP2", "WTPF2", "WTPE2" }, { "LPR3", "WTPP3", "WTPF3", "WTPE3" },
          { "LPR4", "WTPP4", "WTPF4", "WTPE4" }, { "LPR5", "WTPP5", "WTPF5", "WTPE5" }, { "Totaux", "WTPPG", "WTPFG", "WTPEG" } };
  private int[] _LPR1_Width = { 150, 102, 102, 102, };
  private int[] _LPR1_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  private String[] _LPR11_Title = { "Standard", "Proportionnel", "Fixe", "Economique", };
  private String[][] _LPR11_Data =
      { { "LPR11", "STPP1", "STPF1", "STPE1" }, { "LPR21", "STPP2", "STPF2", "STPE2" }, { "LPR31", "STPP3", "STPF3", "STPE3" },
          { "LPR41", "STPP4", "STPF4", "STPE4" }, { "LPR51", "STPP5", "STPF5", "STPE5" }, { "Totaux", "STPPG", "STPFG", "STPEG" } };
  private int[] _LPR11_Width = { 150, 102, 102, 102, };
  private int[] _LPR11_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  private RiGraphe graphe = new RiGraphe(RiGraphe.GRAPHE_PIE3D);
  private Object[][] data = new Object[5][5];
  
  public SGPM14EF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    setDialog(true);
    
    // Ajout
    initDiverses();
    LPR1.setAspectTable(null, _LPR1_Title, _LPR1_Data, _LPR1_Width, false, _LPR1_Justification, null, null, null);
    LPR11.setAspectTable(null, _LPR11_Title, _LPR11_Data, _LPR11_Width, false, _LPR11_Justification, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB2@")).trim());
    LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19", lexique.HostFieldGetData("MESERR"));
    


    
    p_Niveaux.setVisible(lexique.isTrue("91"));
    p_Lib.setVisible(!p_Niveaux.isVisible());
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Chiffrage prix de revient"));
    
    

    
    // riMenu_bt2.setIcon(lexique.getImage("images/options.png",true));
    // riMenu_bt3.setIcon(lexique.getImage("images/outils.png", true));
    // riMenu_bt4.setIcon(lexique.getImage("images/fonctions.png", true));
    
    // InfoBulles pour les listes
    genereToolTipsActuel();
    genereToolTipsStandard();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  /**
   * Génère le tooltip de la liste avec les camenberts pour les valeurs Actuel
   */
  private void genereToolTipsActuel() {
    // On remplit avec les libellés
    for (int i = 0; i < data.length; i++) {
      data[i][0] = lexique.HostFieldGetData("LPR" + (i + 1));
    }
    genereImages("Proportionnel", "WTPP");
    genereImages("Fixe", "WTPF");
    genereImages("Economique", "WTPE");
    
    LPR1.setToolTipText("<html><img src=\"" + ManagerSessionClient.getInstance().getUrlImage("WTPP.png", false) + "\"</img><img src=\""
        + ManagerSessionClient.getInstance().getUrlImage("WTPF.png", false) + "\"</img><img src=\""
        + ManagerSessionClient.getInstance().getUrlImage("WTPE.png", false) + "\"</img></html> ");
  }
  
  /**
   * Génère le tooltip de la liste avec les camenberts pour les valeurs Standard
   */
  private void genereToolTipsStandard() {
    // On remplit avec les libellés
    for (int i = 0; i < data.length; i++) {
      data[i][0] = lexique.HostFieldGetData("LPR" + (i + 1) + "1");
    }
    genereImages("Proportionnel", "STPP");
    genereImages("Fixe", "STPF");
    genereImages("Economique", "STPE");
    
    LPR11.setToolTipText("<html><img src=\"" + ManagerSessionClient.getInstance().getUrlImage("STPP.png", false) + "\"</img><img src=\""
        + ManagerSessionClient.getInstance().getUrlImage("STPF.png", false) + "\"</img><img src=\""
        + ManagerSessionClient.getInstance().getUrlImage("STPE.png", false) + "\"</img></html> ");
  }
  
  /**
   * Génère une image à partir d'une liste de valeurs
   * @param titre
   * @param prefixevaleurs
   */
  private void genereImages(String titre, String prefixevaleurs) {
    String chaine = "";
    for (int i = 0; i < data.length; i++) {
      chaine = lexique.HostFieldGetData(prefixevaleurs + (i + 1)).trim().replace(',', '.');
      data[i][1] = Double.parseDouble(chaine.equals("") ? "0" : chaine);
    }
    graphe.setDonnee(data, "", false);
    graphe.getGraphe(titre, false, true);
    try {
      ImageIO.write(graphe.getImage(375, 244), "png",
          new File(ManagerSessionClient.getInstance().getEnvUser().getDossierDesktop().getNomDossierTempUtilisateur() + File.separatorChar
              + prefixevaleurs + ".png"));
    }
    catch (IOException e) {
      e.printStackTrace();
    }
    
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    SCROLLPANE_Actuel = new JScrollPane();
    LPR1 = new XRiTable();
    SCROLLPANE_Standard = new JScrollPane();
    LPR11 = new XRiTable();
    ETPPG = new XRiTextField();
    ETPFG = new XRiTextField();
    ETPEG = new XRiTextField();
    OBJ_67 = new JLabel();
    p_Entete = new JPanel();
    OBJ_38 = new JLabel();
    P14QTE = new XRiTextField();
    p_Niveaux = new JPanel();
    OBJ_39 = new JLabel();
    WNL1D = new XRiTextField();
    WNL2D = new XRiTextField();
    WNL3D = new XRiTextField();
    WNL1F = new XRiTextField();
    WNL2F = new XRiTextField();
    WNL3F = new XRiTextField();
    OBJ_43 = new JLabel();
    p_Lib = new JPanel();
    LIB2 = new RiZoneSortie();
    LIB1 = new RiZoneSortie();
    barre_tete = new JMenuBar();
    panel1 = new JPanel();
    OBJ_34 = new JLabel();
    ENETB = new XRiTextField();
    OBJ_35 = new JLabel();
    ENNUM = new XRiTextField();
    OBJ_36 = new JLabel();
    ENSUF = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(800, 480));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== SCROLLPANE_Actuel ========
          {
            SCROLLPANE_Actuel.setName("SCROLLPANE_Actuel");

            //---- LPR1 ----
            LPR1.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            LPR1.setName("LPR1");
            SCROLLPANE_Actuel.setViewportView(LPR1);
          }
          p_recup.add(SCROLLPANE_Actuel);
          SCROLLPANE_Actuel.setBounds(10, 10, 585, 125);

          //======== SCROLLPANE_Standard ========
          {
            SCROLLPANE_Standard.setName("SCROLLPANE_Standard");

            //---- LPR11 ----
            LPR11.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            LPR11.setPreferredSize(new Dimension(487, 96));
            LPR11.setName("LPR11");
            SCROLLPANE_Standard.setViewportView(LPR11);
          }
          p_recup.add(SCROLLPANE_Standard);
          SCROLLPANE_Standard.setBounds(10, 140, 585, 125);

          //---- ETPPG ----
          ETPPG.setName("ETPPG");
          p_recup.add(ETPPG);
          ETPPG.setBounds(225, 276, 102, ETPPG.getPreferredSize().height);

          //---- ETPFG ----
          ETPFG.setName("ETPFG");
          p_recup.add(ETPFG);
          ETPFG.setBounds(360, 276, 102, ETPFG.getPreferredSize().height);

          //---- ETPEG ----
          ETPEG.setName("ETPEG");
          p_recup.add(ETPEG);
          ETPEG.setBounds(493, 276, 102, ETPEG.getPreferredSize().height);

          //---- OBJ_67 ----
          OBJ_67.setText("Ecart");
          OBJ_67.setFont(OBJ_67.getFont().deriveFont(OBJ_67.getFont().getStyle() | Font.BOLD));
          OBJ_67.setName("OBJ_67");
          p_recup.add(OBJ_67);
          OBJ_67.setBounds(15, 280, 130, 20);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(10, 120, 605, 320);

        //======== p_Entete ========
        {
          p_Entete.setOpaque(false);
          p_Entete.setName("p_Entete");
          p_Entete.setLayout(null);

          //---- OBJ_38 ----
          OBJ_38.setText("Q.Economique");
          OBJ_38.setName("OBJ_38");
          p_Entete.add(OBJ_38);
          OBJ_38.setBounds(493, 25, 93, 18);

          //---- P14QTE ----
          P14QTE.setName("P14QTE");
          p_Entete.add(P14QTE);
          P14QTE.setBounds(493, 45, 91, P14QTE.getPreferredSize().height);

          //======== p_Niveaux ========
          {
            p_Niveaux.setBorder(new TitledBorder("Niveaux"));
            p_Niveaux.setOpaque(false);
            p_Niveaux.setName("p_Niveaux");
            p_Niveaux.setLayout(null);

            //---- OBJ_39 ----
            OBJ_39.setText("D\u00e9but");
            OBJ_39.setName("OBJ_39");
            p_Niveaux.add(OBJ_39);
            OBJ_39.setBounds(10, 39, 39, 20);

            //---- WNL1D ----
            WNL1D.setName("WNL1D");
            p_Niveaux.add(WNL1D);
            WNL1D.setBounds(65, 35, 36, WNL1D.getPreferredSize().height);

            //---- WNL2D ----
            WNL2D.setName("WNL2D");
            p_Niveaux.add(WNL2D);
            WNL2D.setBounds(115, 35, 36, WNL2D.getPreferredSize().height);

            //---- WNL3D ----
            WNL3D.setName("WNL3D");
            p_Niveaux.add(WNL3D);
            WNL3D.setBounds(165, 35, 36, WNL3D.getPreferredSize().height);

            //---- WNL1F ----
            WNL1F.setName("WNL1F");
            p_Niveaux.add(WNL1F);
            WNL1F.setBounds(245, 35, 36, WNL1F.getPreferredSize().height);

            //---- WNL2F ----
            WNL2F.setName("WNL2F");
            p_Niveaux.add(WNL2F);
            WNL2F.setBounds(295, 35, 36, WNL2F.getPreferredSize().height);

            //---- WNL3F ----
            WNL3F.setName("WNL3F");
            p_Niveaux.add(WNL3F);
            WNL3F.setBounds(345, 35, 36, WNL3F.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Fin");
            OBJ_43.setName("OBJ_43");
            p_Niveaux.add(OBJ_43);
            OBJ_43.setBounds(215, 39, 21, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_Niveaux.getComponentCount(); i++) {
                Rectangle bounds = p_Niveaux.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_Niveaux.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_Niveaux.setMinimumSize(preferredSize);
              p_Niveaux.setPreferredSize(preferredSize);
            }
          }
          p_Entete.add(p_Niveaux);
          p_Niveaux.setBounds(15, 10, 415, 85);

          //======== p_Lib ========
          {
            p_Lib.setBorder(new TitledBorder(""));
            p_Lib.setOpaque(false);
            p_Lib.setName("p_Lib");
            p_Lib.setLayout(null);

            //---- LIB2 ----
            LIB2.setText("@LIB2@");
            LIB2.setName("LIB2");
            p_Lib.add(LIB2);
            LIB2.setBounds(15, 45, 380, 20);

            //---- LIB1 ----
            LIB1.setText("@LIB1@");
            LIB1.setName("LIB1");
            p_Lib.add(LIB1);
            LIB1.setBounds(15, 20, 380, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < p_Lib.getComponentCount(); i++) {
                Rectangle bounds = p_Lib.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = p_Lib.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              p_Lib.setMinimumSize(preferredSize);
              p_Lib.setPreferredSize(preferredSize);
            }
          }
          p_Entete.add(p_Lib);
          p_Lib.setBounds(10, 10, 415, 85);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_Entete.getComponentCount(); i++) {
              Rectangle bounds = p_Entete.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_Entete.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_Entete.setMinimumSize(preferredSize);
            p_Entete.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(p_Entete);
        p_Entete.setBounds(10, 15, 605, 105);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== panel1 ========
      {
        panel1.setOpaque(false);
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_34 ----
        OBJ_34.setText("Etablissement");
        OBJ_34.setName("OBJ_34");
        panel1.add(OBJ_34);
        OBJ_34.setBounds(5, 1, 80, 24);

        //---- ENETB ----
        ENETB.setName("ENETB");
        panel1.add(ENETB);
        ENETB.setBounds(90, -1, 40, ENETB.getPreferredSize().height);

        //---- OBJ_35 ----
        OBJ_35.setText("Num\u00e9ro");
        OBJ_35.setName("OBJ_35");
        panel1.add(OBJ_35);
        OBJ_35.setBounds(135, 1, 51, 24);

        //---- ENNUM ----
        ENNUM.setName("ENNUM");
        panel1.add(ENNUM);
        ENNUM.setBounds(185, -1, 60, ENNUM.getPreferredSize().height);

        //---- OBJ_36 ----
        OBJ_36.setText("S");
        OBJ_36.setName("OBJ_36");
        panel1.add(OBJ_36);
        OBJ_36.setBounds(255, 1, 12, 24);

        //---- ENSUF ----
        ENSUF.setName("ENSUF");
        panel1.add(ENSUF);
        ENSUF.setBounds(275, -1, 18, ENSUF.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(panel1);
    }
    add(barre_tete, BorderLayout.NORTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JScrollPane SCROLLPANE_Actuel;
  private XRiTable LPR1;
  private JScrollPane SCROLLPANE_Standard;
  private XRiTable LPR11;
  private XRiTextField ETPPG;
  private XRiTextField ETPFG;
  private XRiTextField ETPEG;
  private JLabel OBJ_67;
  private JPanel p_Entete;
  private JLabel OBJ_38;
  private XRiTextField P14QTE;
  private JPanel p_Niveaux;
  private JLabel OBJ_39;
  private XRiTextField WNL1D;
  private XRiTextField WNL2D;
  private XRiTextField WNL3D;
  private XRiTextField WNL1F;
  private XRiTextField WNL2F;
  private XRiTextField WNL3F;
  private JLabel OBJ_43;
  private JPanel p_Lib;
  private RiZoneSortie LIB2;
  private RiZoneSortie LIB1;
  private JMenuBar barre_tete;
  private JPanel panel1;
  private JLabel OBJ_34;
  private XRiTextField ENETB;
  private JLabel OBJ_35;
  private XRiTextField ENNUM;
  private JLabel OBJ_36;
  private XRiTextField ENSUF;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
