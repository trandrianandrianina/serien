
package ri.serien.libecranrpg.sgpm.SGPM61FM;
// Nom Fichier: b_SGPM61FM_FMTB1_FMTF1_163.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGPM61FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM61FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    WTOU.setValeursSelection("**", "  ");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    OBJ_60.setTitle(lexique.TranslationTable(interpreteurD.analyseExpression("@PLAG@")).trim());
    LDETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LDETA@")).trim());
    OBJ_63.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARTI@")).trim());
    OBJ_62.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ARTI2@")).trim());
    LIBD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBD@")).trim());
    LIBF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    if (lexique.isTrue("91")) {
      OBJ_60.setTitle("Plage d'articles à traiter");
      OBJ_63.setText("Article début");
      OBJ_62.setText("Article fin");
      ARTF.setEnabled(lexique.isPresent("ARTF"));
      ARTD.setEnabled(lexique.isPresent("ARTD"));
      
    }
    if (lexique.isTrue("92")) {
      OBJ_60.setTitle("Plage de postes de travail à traiter");
      OBJ_63.setText("Poste début");
      OBJ_62.setText("Poste fin");
      POSTF.setEnabled(lexique.isPresent("POSTF"));
      POSTD.setEnabled(lexique.isPresent("POSTD"));
      
    }
    if (lexique.isTrue("94")) {
      OBJ_60.setTitle("Plage d'ateliers à traiter");
      OBJ_63.setText("Code atelier début");
      OBJ_62.setText("Code atelier fin");
      ATED.setVisible(lexique.isPresent("ATEF"));
      ATEF.setVisible(lexique.isPresent("ATED"));
      
    }
    if (lexique.isTrue("95")) {
      OBJ_60.setTitle("Plage d'opérations à traiter");
      OBJ_63.setText("Opération début");
      OBJ_62.setText("Opération fin");
      OPEF.setVisible(lexique.isPresent("OPEF"));
      OPED.setVisible(lexique.isPresent("OPED"));
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_60 = new JXTitledSeparator();
    OBJ_66 = new JXTitledSeparator();
    OBJ_67 = new JXTitledSeparator();
    OBJ_83 = new JLabel();
    WTOU = new XRiCheckBox();
    LDETA = new RiZoneSortie();
    OBJ_85 = new JLabel();
    WETA = new XRiTextField();
    OBJ_86 = new JLabel();
    OBJ_87 = new JLabel();
    DATDX = new XRiCalendrier();
    DATFX = new XRiCalendrier();
    P_SEL0 = new JPanel();
    OBJ_63 = new JLabel();
    OBJ_62 = new JLabel();
    ATED = new XRiTextField();
    ATEF = new XRiTextField();
    POSTD = new XRiTextField();
    POSTF = new XRiTextField();
    OPED = new XRiTextField();
    OPEF = new XRiTextField();
    ARTD = new XRiTextField();
    ARTF = new XRiTextField();
    LIBD = new RiZoneSortie();
    LIBF = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(810, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(10, 10, 765, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(180, 35, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(180, 65, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(25, 50, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(70, 50), bouton_etablissement.getPreferredSize()));

            //---- OBJ_60 ----
            OBJ_60.setTitle("@PLAG@");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(10, 115, 765, OBJ_60.getPreferredSize().height);

            //---- OBJ_66 ----
            OBJ_66.setTitle("");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(10, 340, 765, OBJ_66.getPreferredSize().height);

            //---- OBJ_67 ----
            OBJ_67.setTitle("");
            OBJ_67.setName("OBJ_67");
            panel1.add(OBJ_67);
            OBJ_67.setBounds(10, 275, 765, OBJ_67.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Etat des ordres de fabrication");
            OBJ_83.setName("OBJ_83");
            panel1.add(OBJ_83);
            OBJ_83.setBounds(25, 375, 180, 20);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel1.add(WTOU);
            WTOU.setBounds(25, 145, 155, 20);

            //---- LDETA ----
            LDETA.setText("@LDETA@");
            LDETA.setName("LDETA");
            panel1.add(LDETA);
            LDETA.setBounds(260, 375, 130, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("P\u00e9riode");
            OBJ_85.setName("OBJ_85");
            panel1.add(OBJ_85);
            OBJ_85.setBounds(25, 300, 51, 20);

            //---- WETA ----
            WETA.setComponentPopupMenu(BTD);
            WETA.setName("WETA");
            panel1.add(WETA);
            WETA.setBounds(225, 370, 20, WETA.getPreferredSize().height);

            //---- OBJ_86 ----
            OBJ_86.setText("du");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(120, 300, 18, 20);

            //---- OBJ_87 ----
            OBJ_87.setText("au");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(370, 300, 18, 20);

            //---- DATDX ----
            DATDX.setName("DATDX");
            panel1.add(DATDX);
            DATDX.setBounds(225, 295, 115, DATDX.getPreferredSize().height);

            //---- DATFX ----
            DATFX.setName("DATFX");
            panel1.add(DATFX);
            DATFX.setBounds(420, 295, 115, DATFX.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_63 ----
              OBJ_63.setText("@ARTI@");
              OBJ_63.setName("OBJ_63");
              P_SEL0.add(OBJ_63);
              OBJ_63.setBounds(20, 14, 140, 20);

              //---- OBJ_62 ----
              OBJ_62.setText("@ARTI2@");
              OBJ_62.setName("OBJ_62");
              P_SEL0.add(OBJ_62);
              OBJ_62.setBounds(20, 44, 140, 20);

              //---- ATED ----
              ATED.setComponentPopupMenu(BTD);
              ATED.setName("ATED");
              P_SEL0.add(ATED);
              ATED.setBounds(160, 10, 60, ATED.getPreferredSize().height);

              //---- ATEF ----
              ATEF.setComponentPopupMenu(BTD);
              ATEF.setName("ATEF");
              P_SEL0.add(ATEF);
              ATEF.setBounds(160, 40, 60, ATEF.getPreferredSize().height);

              //---- POSTD ----
              POSTD.setComponentPopupMenu(BTD);
              POSTD.setName("POSTD");
              P_SEL0.add(POSTD);
              POSTD.setBounds(160, 10, 60, POSTD.getPreferredSize().height);

              //---- POSTF ----
              POSTF.setComponentPopupMenu(BTD);
              POSTF.setName("POSTF");
              P_SEL0.add(POSTF);
              POSTF.setBounds(160, 40, 60, POSTF.getPreferredSize().height);

              //---- OPED ----
              OPED.setComponentPopupMenu(BTD);
              OPED.setName("OPED");
              P_SEL0.add(OPED);
              OPED.setBounds(160, 10, 210, OPED.getPreferredSize().height);

              //---- OPEF ----
              OPEF.setComponentPopupMenu(BTD);
              OPEF.setName("OPEF");
              P_SEL0.add(OPEF);
              OPEF.setBounds(160, 40, 210, OPEF.getPreferredSize().height);

              //---- ARTD ----
              ARTD.setComponentPopupMenu(BTD);
              ARTD.setName("ARTD");
              P_SEL0.add(ARTD);
              ARTD.setBounds(160, 10, 210, ARTD.getPreferredSize().height);

              //---- ARTF ----
              ARTF.setComponentPopupMenu(BTD);
              ARTF.setName("ARTF");
              P_SEL0.add(ARTF);
              ARTF.setBounds(160, 40, 210, ARTF.getPreferredSize().height);

              //---- LIBD ----
              LIBD.setText("@LIBD@");
              LIBD.setName("LIBD");
              P_SEL0.add(LIBD);
              LIBD.setBounds(395, 12, 290, LIBD.getPreferredSize().height);

              //---- LIBF ----
              LIBF.setText("@LIBF@");
              LIBF.setName("LIBF");
              P_SEL0.add(LIBF);
              LIBF.setBounds(395, 42, 290, LIBF.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(25, 175, 705, 75);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 790, 420);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_60;
  private JXTitledSeparator OBJ_66;
  private JXTitledSeparator OBJ_67;
  private JLabel OBJ_83;
  private XRiCheckBox WTOU;
  private RiZoneSortie LDETA;
  private JLabel OBJ_85;
  private XRiTextField WETA;
  private JLabel OBJ_86;
  private JLabel OBJ_87;
  private XRiCalendrier DATDX;
  private XRiCalendrier DATFX;
  private JPanel P_SEL0;
  private JLabel OBJ_63;
  private JLabel OBJ_62;
  private XRiTextField ATED;
  private XRiTextField ATEF;
  private XRiTextField POSTD;
  private XRiTextField POSTF;
  private XRiTextField OPED;
  private XRiTextField OPEF;
  private XRiTextField ARTD;
  private XRiTextField ARTF;
  private RiZoneSortie LIBD;
  private RiZoneSortie LIBF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
