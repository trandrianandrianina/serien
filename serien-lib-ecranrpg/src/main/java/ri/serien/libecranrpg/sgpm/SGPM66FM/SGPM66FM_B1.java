
package ri.serien.libecranrpg.sgpm.SGPM66FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class SGPM66FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGPM66FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    WTOU3.setValeursSelection("**", "  ");
    WTOU1.setValeursSelection("**", "  ");
    WTOU.setValeursSelection("**", "  ");
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Contruction du nom du fichier si F10 (VEXP0A)
    if (riSousMenu_bt_export instanceof RiSousMenu_bt) {
      lexique.setNomFichierTableur(lexique.HostFieldGetData("TITPG1").trim() + " " + lexique.HostFieldGetData("TITPG2").trim());
    }
    
    

    
    p_bpresentation.setCodeEtablissement(z_etablissement_.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(z_etablissement_.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt_exportActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void WTOU1ActionPerformed(ActionEvent e) {
    P_SEL1.setVisible(!P_SEL1.isVisible());
  }
  
  private void WTOU3ActionPerformed(ActionEvent e) {
    P_SEL2.setVisible(!P_SEL2.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt_export = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_73 = new JXTitledSeparator();
    OBJ_65 = new JXTitledSeparator();
    OBJ_40 = new JXTitledSeparator();
    P_SEL2 = new JPanel();
    ARTDEB = new XRiTextField();
    ARTFIN = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_80 = new JLabel();
    P_SEL1 = new JPanel();
    OBJ_68 = new JLabel();
    OBJ_71 = new JLabel();
    FAMDEB = new XRiTextField();
    FAMFIN = new XRiTextField();
    P_SEL0 = new JPanel();
    MA01 = new XRiTextField();
    MA02 = new XRiTextField();
    MA03 = new XRiTextField();
    MA04 = new XRiTextField();
    MA05 = new XRiTextField();
    MA06 = new XRiTextField();
    MA07 = new XRiTextField();
    MA08 = new XRiTextField();
    MA09 = new XRiTextField();
    MA10 = new XRiTextField();
    MA11 = new XRiTextField();
    MA12 = new XRiTextField();
    OBJ_62 = new JXTitledSeparator();
    OBJ_87 = new JLabel();
    WTOU = new XRiCheckBox();
    WTOU1 = new XRiCheckBox();
    WTOU3 = new XRiCheckBox();
    OBJ_45 = new JLabel();
    OBJ_63 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_61 = new JLabel();
    MAGDES = new JTextField();
    MMDEB = new XRiTextField();
    MMFIN = new XRiTextField();
    DELAI = new XRiTextField();
    OBJ_43 = new JLabel();
    PERDEB = new XRiCalendrier();
    PERFIN = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 200));
            menus_haut.setPreferredSize(new Dimension(160, 200));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt_export ----
              riSousMenu_bt_export.setText("Exportation tableur");
              riSousMenu_bt_export.setToolTipText("Exportation vers tableur");
              riSousMenu_bt_export.setName("riSousMenu_bt_export");
              riSousMenu_bt_export.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt_exportActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt_export);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(770, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- sep_etablissement ----
            sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
            sep_etablissement.setName("sep_etablissement");
            panel1.add(sep_etablissement);
            sep_etablissement.setBounds(15, 30, 625, sep_etablissement.getPreferredSize().height);

            //---- z_dgnom_ ----
            z_dgnom_.setText("@DGNOM@");
            z_dgnom_.setName("z_dgnom_");
            panel1.add(z_dgnom_);
            z_dgnom_.setBounds(185, 55, 260, z_dgnom_.getPreferredSize().height);

            //---- z_wencx_ ----
            z_wencx_.setText("@WENCX@");
            z_wencx_.setName("z_wencx_");
            panel1.add(z_wencx_);
            z_wencx_.setBounds(185, 85, 260, z_wencx_.getPreferredSize().height);

            //---- z_etablissement_ ----
            z_etablissement_.setComponentPopupMenu(null);
            z_etablissement_.setText("@WETB@");
            z_etablissement_.setName("z_etablissement_");
            panel1.add(z_etablissement_);
            z_etablissement_.setBounds(30, 70, 40, z_etablissement_.getPreferredSize().height);

            //---- bouton_etablissement ----
            bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
            bouton_etablissement.setName("bouton_etablissement");
            bouton_etablissement.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_etablissementActionPerformed(e);
              }
            });
            panel1.add(bouton_etablissement);
            bouton_etablissement.setBounds(new Rectangle(new Point(75, 70), bouton_etablissement.getPreferredSize()));

            //---- OBJ_73 ----
            OBJ_73.setTitle("Plage de codes articles");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(15, 375, 725, OBJ_73.getPreferredSize().height);

            //---- OBJ_65 ----
            OBJ_65.setTitle("Plage de codes groupe-famille");
            OBJ_65.setName("OBJ_65");
            panel1.add(OBJ_65);
            OBJ_65.setBounds(15, 255, 725, OBJ_65.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setTitle("Code(s) magasin(s) \u00e0 traiter");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(15, 125, 725, OBJ_40.getPreferredSize().height);

            //======== P_SEL2 ========
            {
              P_SEL2.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL2.setOpaque(false);
              P_SEL2.setName("P_SEL2");
              P_SEL2.setLayout(null);

              //---- ARTDEB ----
              ARTDEB.setComponentPopupMenu(BTD);
              ARTDEB.setName("ARTDEB");
              P_SEL2.add(ARTDEB);
              ARTDEB.setBounds(185, 5, 210, ARTDEB.getPreferredSize().height);

              //---- ARTFIN ----
              ARTFIN.setComponentPopupMenu(BTD);
              ARTFIN.setName("ARTFIN");
              P_SEL2.add(ARTFIN);
              ARTFIN.setBounds(185, 30, 210, ARTFIN.getPreferredSize().height);

              //---- OBJ_77 ----
              OBJ_77.setText("Article de d\u00e9but");
              OBJ_77.setName("OBJ_77");
              P_SEL2.add(OBJ_77);
              OBJ_77.setBounds(25, 9, 97, 20);

              //---- OBJ_80 ----
              OBJ_80.setText("Article de fin");
              OBJ_80.setName("OBJ_80");
              P_SEL2.add(OBJ_80);
              OBJ_80.setBounds(25, 34, 79, 20);
            }
            panel1.add(P_SEL2);
            P_SEL2.setBounds(205, 405, 410, 66);

            //======== P_SEL1 ========
            {
              P_SEL1.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL1.setOpaque(false);
              P_SEL1.setName("P_SEL1");
              P_SEL1.setLayout(null);

              //---- OBJ_68 ----
              OBJ_68.setText("Groupe-famille de d\u00e9but");
              OBJ_68.setName("OBJ_68");
              P_SEL1.add(OBJ_68);
              OBJ_68.setBounds(25, 14, 153, 20);

              //---- OBJ_71 ----
              OBJ_71.setText("Groupe-famille de fin");
              OBJ_71.setName("OBJ_71");
              P_SEL1.add(OBJ_71);
              OBJ_71.setBounds(25, 39, 135, 20);

              //---- FAMDEB ----
              FAMDEB.setComponentPopupMenu(BTD);
              FAMDEB.setName("FAMDEB");
              P_SEL1.add(FAMDEB);
              FAMDEB.setBounds(185, 10, 40, FAMDEB.getPreferredSize().height);

              //---- FAMFIN ----
              FAMFIN.setComponentPopupMenu(BTD);
              FAMFIN.setName("FAMFIN");
              P_SEL1.add(FAMFIN);
              FAMFIN.setBounds(185, 35, 40, FAMFIN.getPreferredSize().height);
            }
            panel1.add(P_SEL1);
            P_SEL1.setBounds(205, 290, 410, 70);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- MA01 ----
              MA01.setComponentPopupMenu(BTD);
              MA01.setName("MA01");
              P_SEL0.add(MA01);
              MA01.setBounds(25, 10, 30, MA01.getPreferredSize().height);

              //---- MA02 ----
              MA02.setComponentPopupMenu(BTD);
              MA02.setName("MA02");
              P_SEL0.add(MA02);
              MA02.setBounds(55, 10, 30, MA02.getPreferredSize().height);

              //---- MA03 ----
              MA03.setComponentPopupMenu(BTD);
              MA03.setName("MA03");
              P_SEL0.add(MA03);
              MA03.setBounds(85, 10, 30, MA03.getPreferredSize().height);

              //---- MA04 ----
              MA04.setComponentPopupMenu(BTD);
              MA04.setName("MA04");
              P_SEL0.add(MA04);
              MA04.setBounds(115, 10, 30, MA04.getPreferredSize().height);

              //---- MA05 ----
              MA05.setComponentPopupMenu(BTD);
              MA05.setName("MA05");
              P_SEL0.add(MA05);
              MA05.setBounds(145, 10, 30, MA05.getPreferredSize().height);

              //---- MA06 ----
              MA06.setComponentPopupMenu(BTD);
              MA06.setName("MA06");
              P_SEL0.add(MA06);
              MA06.setBounds(175, 10, 30, MA06.getPreferredSize().height);

              //---- MA07 ----
              MA07.setComponentPopupMenu(BTD);
              MA07.setName("MA07");
              P_SEL0.add(MA07);
              MA07.setBounds(205, 10, 30, MA07.getPreferredSize().height);

              //---- MA08 ----
              MA08.setComponentPopupMenu(BTD);
              MA08.setName("MA08");
              P_SEL0.add(MA08);
              MA08.setBounds(235, 10, 30, MA08.getPreferredSize().height);

              //---- MA09 ----
              MA09.setComponentPopupMenu(BTD);
              MA09.setName("MA09");
              P_SEL0.add(MA09);
              MA09.setBounds(265, 10, 30, MA09.getPreferredSize().height);

              //---- MA10 ----
              MA10.setComponentPopupMenu(BTD);
              MA10.setName("MA10");
              P_SEL0.add(MA10);
              MA10.setBounds(295, 10, 30, MA10.getPreferredSize().height);

              //---- MA11 ----
              MA11.setComponentPopupMenu(BTD);
              MA11.setName("MA11");
              P_SEL0.add(MA11);
              MA11.setBounds(325, 10, 30, MA11.getPreferredSize().height);

              //---- MA12 ----
              MA12.setComponentPopupMenu(BTD);
              MA12.setName("MA12");
              P_SEL0.add(MA12);
              MA12.setBounds(355, 10, 30, MA12.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(205, 155, 410, 50);

            //---- OBJ_62 ----
            OBJ_62.setTitle("");
            OBJ_62.setName("OBJ_62");
            panel1.add(OBJ_62);
            OBJ_62.setBounds(15, 210, 725, OBJ_62.getPreferredSize().height);

            //---- OBJ_87 ----
            OBJ_87.setText("D\u00e9lai en semaines (si pas dans CNA)");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(50, 520, 226, 20);

            //---- WTOU ----
            WTOU.setText("S\u00e9lection compl\u00e8te");
            WTOU.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU.setName("WTOU");
            WTOU.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOUActionPerformed(e);
              }
            });
            panel1.add(WTOU);
            WTOU.setBounds(50, 170, 157, 20);

            //---- WTOU1 ----
            WTOU1.setText("S\u00e9lection compl\u00e8te");
            WTOU1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU1.setName("WTOU1");
            WTOU1.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU1ActionPerformed(e);
              }
            });
            panel1.add(WTOU1);
            WTOU1.setBounds(50, 315, 157, 20);

            //---- WTOU3 ----
            WTOU3.setText("S\u00e9lection compl\u00e8te");
            WTOU3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTOU3.setName("WTOU3");
            WTOU3.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTOU3ActionPerformed(e);
              }
            });
            panel1.add(WTOU3);
            WTOU3.setBounds(50, 425, 157, 20);

            //---- OBJ_45 ----
            OBJ_45.setText("P\u00e9riode possible \u00e0 traiter");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(50, 490, 153, 20);

            //---- OBJ_63 ----
            OBJ_63.setText("Magasin de destination");
            OBJ_63.setName("OBJ_63");
            panel1.add(OBJ_63);
            OBJ_63.setBounds(50, 225, 142, 20);

            //---- OBJ_60 ----
            OBJ_60.setText("Mois de d\u00e9but");
            OBJ_60.setName("OBJ_60");
            panel1.add(OBJ_60);
            OBJ_60.setBounds(510, 490, 91, 20);

            //---- OBJ_61 ----
            OBJ_61.setText("Mois de fin");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(630, 490, 68, 20);

            //---- MAGDES ----
            MAGDES.setComponentPopupMenu(BTD);
            MAGDES.setName("MAGDES");
            panel1.add(MAGDES);
            MAGDES.setBounds(230, 220, 30, MAGDES.getPreferredSize().height);

            //---- MMDEB ----
            MMDEB.setComponentPopupMenu(BTD);
            MMDEB.setName("MMDEB");
            panel1.add(MMDEB);
            MMDEB.setBounds(595, 485, 28, MMDEB.getPreferredSize().height);

            //---- MMFIN ----
            MMFIN.setComponentPopupMenu(BTD);
            MMFIN.setName("MMFIN");
            panel1.add(MMFIN);
            MMFIN.setBounds(695, 485, 28, MMFIN.getPreferredSize().height);

            //---- DELAI ----
            DELAI.setComponentPopupMenu(BTD);
            DELAI.setName("DELAI");
            panel1.add(DELAI);
            DELAI.setBounds(290, 516, 28, DELAI.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("\u00e0");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(342, 490, 11, 20);

            //---- PERDEB ----
            PERDEB.setTypeSaisie(6);
            PERDEB.setName("PERDEB");
            panel1.add(PERDEB);
            PERDEB.setBounds(240, 486, 90, PERDEB.getPreferredSize().height);

            //---- PERFIN ----
            PERFIN.setTypeSaisie(6);
            PERFIN.setName("PERFIN");
            panel1.add(PERFIN);
            PERFIN.setBounds(375, 486, 90, PERFIN.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 750, 550);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt_export;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_73;
  private JXTitledSeparator OBJ_65;
  private JXTitledSeparator OBJ_40;
  private JPanel P_SEL2;
  private XRiTextField ARTDEB;
  private XRiTextField ARTFIN;
  private JLabel OBJ_77;
  private JLabel OBJ_80;
  private JPanel P_SEL1;
  private JLabel OBJ_68;
  private JLabel OBJ_71;
  private XRiTextField FAMDEB;
  private XRiTextField FAMFIN;
  private JPanel P_SEL0;
  private XRiTextField MA01;
  private XRiTextField MA02;
  private XRiTextField MA03;
  private XRiTextField MA04;
  private XRiTextField MA05;
  private XRiTextField MA06;
  private XRiTextField MA07;
  private XRiTextField MA08;
  private XRiTextField MA09;
  private XRiTextField MA10;
  private XRiTextField MA11;
  private XRiTextField MA12;
  private JXTitledSeparator OBJ_62;
  private JLabel OBJ_87;
  private XRiCheckBox WTOU;
  private XRiCheckBox WTOU1;
  private XRiCheckBox WTOU3;
  private JLabel OBJ_45;
  private JLabel OBJ_63;
  private JLabel OBJ_60;
  private JLabel OBJ_61;
  private JTextField MAGDES;
  private XRiTextField MMDEB;
  private XRiTextField MMFIN;
  private XRiTextField DELAI;
  private JLabel OBJ_43;
  private XRiCalendrier PERDEB;
  private XRiCalendrier PERFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
