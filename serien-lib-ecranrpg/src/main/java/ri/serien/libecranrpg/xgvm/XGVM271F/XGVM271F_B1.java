
package ri.serien.libecranrpg.xgvm.XGVM271F;
// Nom Fichier: pop_XGVM271F_FMTB1_1150.java

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class XGVM271F_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public XGVM271F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    CONF.setValeursSelection("OUI", "NON");
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
    OBJ_21.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@U1DATX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    DELAI.setEnabled(lexique.isPresent("DELAI"));
    OBJ_16.setVisible(lexique.isPresent("*TIME"));
    OBJ_21.setVisible(lexique.isPresent("U1DATX"));
    // CONF.setEnabled( lexique.isPresent("CONF"));
    // CONF.setSelected(lexique.HostFieldGetData("CONF").equalsIgnoreCase("OUI"));
    
    // TODO Icones
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_26.setIcon(lexique.chargerImage("images/retour.png", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (CONF.isSelected())
    // lexique.HostFieldPutData("CONF", 0, "OUI");
    // else
    // lexique.HostFieldPutData("CONF", 0, "NON");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="enter"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_Touche")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgvm"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel3 = new JPanel();
    panel2 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_26 = new JButton();
    panel4 = new JPanel();
    panel1 = new JPanel();
    OBJ_16 = new JLabel();
    OBJ_18 = new JLabel();
    DELAI = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_20 = new JLabel();
    CONF = new XRiCheckBox();
    OBJ_4 = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_24 = new JLabel();
    P_PnlOpts = new JPanel();

    //======== this ========
    setPreferredSize(new Dimension(395, 200));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel3 ========
    {
      panel3.setName("panel3");
      panel3.setLayout(null);

      //======== panel2 ========
      {
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("Ok");
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_25ActionPerformed(e);
          }
        });
        panel2.add(BT_ENTER);
        BT_ENTER.setBounds(5, 5, 56, 40);

        //---- OBJ_26 ----
        OBJ_26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_26.setToolTipText("Retour");
        OBJ_26.setName("OBJ_26");
        OBJ_26.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_26ActionPerformed(e);
          }
        });
        panel2.add(OBJ_26);
        OBJ_26.setBounds(60, 5, 56, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel3.add(panel2);
      panel2.setBounds(275, 0, 120, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel3.getComponentCount(); i++) {
          Rectangle bounds = panel3.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel3.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel3.setMinimumSize(preferredSize);
        panel3.setPreferredSize(preferredSize);
      }
    }
    add(panel3, BorderLayout.SOUTH);

    //======== panel4 ========
    {
      panel4.setName("panel4");
      panel4.setLayout(null);

      //======== panel1 ========
      {
        panel1.setBorder(new TitledBorder("Facturation temporairement suspendue"));
        panel1.setName("panel1");
        panel1.setLayout(null);

        //---- OBJ_16 ----
        OBJ_16.setText("@*TIME@");
        OBJ_16.setName("OBJ_16");
        panel1.add(OBJ_16);
        OBJ_16.setBounds(285, 0, 70, 18);

        //---- OBJ_18 ----
        OBJ_18.setText("D\u00e9lai en jour(s)");
        OBJ_18.setName("OBJ_18");
        panel1.add(OBJ_18);
        OBJ_18.setBounds(25, 30, 111, 20);

        //---- DELAI ----
        DELAI.setComponentPopupMenu(BTD);
        DELAI.setName("DELAI");
        panel1.add(DELAI);
        DELAI.setBounds(180, 30, 26, DELAI.getPreferredSize().height);

        //---- OBJ_21 ----
        OBJ_21.setText("@U1DATX@");
        OBJ_21.setName("OBJ_21");
        panel1.add(OBJ_21);
        OBJ_21.setBounds(180, 60, 68, 20);

        //---- OBJ_20 ----
        OBJ_20.setText("Date de facturation");
        OBJ_20.setName("OBJ_20");
        panel1.add(OBJ_20);
        OBJ_20.setBounds(25, 60, 133, 20);

        //---- CONF ----
        CONF.setText("Confirmation");
        CONF.setComponentPopupMenu(BTD);
        CONF.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        CONF.setName("CONF");
        panel1.add(CONF);
        CONF.setBounds(30, 90, 112, 20);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel1.getComponentCount(); i++) {
            Rectangle bounds = panel1.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel1.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel1.setMinimumSize(preferredSize);
          panel1.setPreferredSize(preferredSize);
        }
      }
      panel4.add(panel1);
      panel1.setBounds(10, 10, 375, 135);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel4.getComponentCount(); i++) {
          Rectangle bounds = panel4.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel4.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel4.setMinimumSize(preferredSize);
        panel4.setPreferredSize(preferredSize);
      }
    }
    add(panel4, BorderLayout.CENTER);

    //======== OBJ_4 ========
    {
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Exploitation");
      OBJ_9.setName("OBJ_9");
      OBJ_4.add(OBJ_9);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Invite");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }

    //---- OBJ_24 ----
    OBJ_24.setText("Facturation temporairement suspendue");
    OBJ_24.setName("OBJ_24");

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      P_PnlOpts.setPreferredSize(new Dimension(55, 516));
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel3;
  private JPanel panel2;
  private JButton BT_ENTER;
  private JButton OBJ_26;
  private JPanel panel4;
  private JPanel panel1;
  private JLabel OBJ_16;
  private JLabel OBJ_18;
  private XRiTextField DELAI;
  private JLabel OBJ_21;
  private JLabel OBJ_20;
  private XRiCheckBox CONF;
  private JPopupMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JPopupMenu BTD;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JLabel OBJ_24;
  private JPanel P_PnlOpts;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
