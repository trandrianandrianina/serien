
package ri.serien.libecranrpg.rgpm.RGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGPM13FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  // private boolean isKit = false;
  
  public RGPM13FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX3.setValeurs("3", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    ARG13.setEnabled(lexique.isPresent("ARG13"));
    ARG12.setEnabled(lexique.isPresent("ARG12"));
    ARG11.setEnabled(lexique.isPresent("ARG11"));
    TIDX3.setEnabled(!lexique.HostFieldGetData("ARG2").trim().equalsIgnoreCase(""));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX1.setVisible( lexique.isPresent("RB"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX2.setVisible( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    ARG3.setEnabled(!lexique.HostFieldGetData("ARG2").trim().equalsIgnoreCase("") & lexique.isPresent("ARG3"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("LIGNES DE NOMENCLATURE"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARG3 = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG11 = new XRiTextField();
    ARG12 = new XRiTextField();
    ARG13 = new XRiTextField();
    ARG2 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(620, 205));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ARG3 ----
          ARG3.setComponentPopupMenu(BTD);
          ARG3.setName("ARG3");
          panel1.add(ARG3);
          ARG3.setBounds(160, 95, 210, ARG3.getPreferredSize().height);

          //---- TIDX2 ----
          TIDX2.setText("Type de ligne");
          TIDX2.setToolTipText("Tri\u00e9 par");
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");
          panel1.add(TIDX2);
          TIDX2.setBounds(25, 69, 130, 20);

          //---- TIDX1 ----
          TIDX1.setText("Niveau");
          TIDX1.setToolTipText("Tri\u00e9 par");
          TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX1.setName("TIDX1");
          panel1.add(TIDX1);
          TIDX1.setBounds(25, 39, 130, 20);

          //---- TIDX3 ----
          TIDX3.setText("Ordre");
          TIDX3.setToolTipText("Tri\u00e9 par");
          TIDX3.setComponentPopupMenu(BTD);
          TIDX3.setName("TIDX3");
          panel1.add(TIDX3);
          TIDX3.setBounds(25, 99, 130, 20);

          //---- ARG11 ----
          ARG11.setComponentPopupMenu(BTD);
          ARG11.setName("ARG11");
          panel1.add(ARG11);
          ARG11.setBounds(160, 35, 40, ARG11.getPreferredSize().height);

          //---- ARG12 ----
          ARG12.setComponentPopupMenu(BTD);
          ARG12.setName("ARG12");
          panel1.add(ARG12);
          ARG12.setBounds(205, 35, 40, ARG12.getPreferredSize().height);

          //---- ARG13 ----
          ARG13.setComponentPopupMenu(BTD);
          ARG13.setName("ARG13");
          panel1.add(ARG13);
          ARG13.setBounds(250, 35, 40, ARG13.getPreferredSize().height);

          //---- ARG2 ----
          ARG2.setComponentPopupMenu(BTD);
          ARG2.setName("ARG2");
          panel1.add(ARG2);
          ARG2.setBounds(160, 65, 40, ARG2.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 400, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(25, 25, 25)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG11;
  private XRiTextField ARG12;
  private XRiTextField ARG13;
  private XRiTextField ARG2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
