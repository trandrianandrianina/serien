
package ri.serien.libecranrpg.rgpm.RGPM14FM;
// Nom Fichier: pop_RGPM14FM_FMTF2_FMTF1_227.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGPM14FM_F2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RGPM14FM_F2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_32);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_31.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CETT@")).trim());
    OBJ_30.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ATTE@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_32.setVisible(lexique.HostFieldGetData("ATTE").equalsIgnoreCase(""));
    OBJ_37.setVisible(lexique.HostFieldGetData("ATTE").equalsIgnoreCase(""));
    WARTS.setEnabled(lexique.isPresent("WARTS"));
    A1LB4S.setEnabled(lexique.isPresent("A1LB4S"));
    A1LB3S.setEnabled(lexique.isPresent("A1LB3S"));
    A1LB2S.setEnabled(lexique.isPresent("A1LB2S"));
    A1LB1S.setEnabled(lexique.isPresent("A1LB1S"));
    OBJ_9.setVisible(lexique.HostFieldGetData("ATTE").equalsIgnoreCase(""));
    
    // TODO Icones
    OBJ_38.setIcon(lexique.chargerImage("images/icones_3381.gif", true));
    OBJ_37.setIcon(lexique.chargerImage("images/icones_2381.gif", true));
    OBJ_32.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_36.setIcon(lexique.chargerImage("images/fin.png", true));
    OBJ_33.setIcon(lexique.chargerImage("images/modifhe.gif", true));
    
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "rgpm14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "rgpm14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    OBJ_31 = new JLabel();
    A1LB1S = new XRiTextField();
    A1LB2S = new XRiTextField();
    A1LB3S = new XRiTextField();
    A1LB4S = new XRiTextField();
    WARTS = new XRiTextField();
    OBJ_25 = new JLabel();
    OBJ_23 = new JLabel();
    OBJ_30 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_37 = new JButton();
    OBJ_38 = new JButton();
    panel2 = new JPanel();
    OBJ_33 = new JButton();
    OBJ_36 = new JButton();
    OBJ_32 = new JButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("Confirmation cr\u00e9ation d'article nomenclature"));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- OBJ_31 ----
      OBJ_31.setText("@CETT@");
      OBJ_31.setName("OBJ_31");
      panel1.add(OBJ_31);
      OBJ_31.setBounds(115, 155, 262, 20);

      //---- A1LB1S ----
      A1LB1S.setComponentPopupMenu(BTD);
      A1LB1S.setName("A1LB1S");
      panel1.add(A1LB1S);
      A1LB1S.setBounds(110, 50, 310, A1LB1S.getPreferredSize().height);

      //---- A1LB2S ----
      A1LB2S.setComponentPopupMenu(BTD);
      A1LB2S.setName("A1LB2S");
      panel1.add(A1LB2S);
      A1LB2S.setBounds(110, 75, 310, A1LB2S.getPreferredSize().height);

      //---- A1LB3S ----
      A1LB3S.setComponentPopupMenu(BTD);
      A1LB3S.setName("A1LB3S");
      panel1.add(A1LB3S);
      A1LB3S.setBounds(110, 100, 310, A1LB3S.getPreferredSize().height);

      //---- A1LB4S ----
      A1LB4S.setComponentPopupMenu(BTD);
      A1LB4S.setName("A1LB4S");
      panel1.add(A1LB4S);
      A1LB4S.setBounds(110, 125, 310, A1LB4S.getPreferredSize().height);

      //---- WARTS ----
      WARTS.setComponentPopupMenu(BTD);
      WARTS.setName("WARTS");
      panel1.add(WARTS);
      WARTS.setBounds(110, 25, 210, WARTS.getPreferredSize().height);

      //---- OBJ_25 ----
      OBJ_25.setText("D\u00e9signations");
      OBJ_25.setName("OBJ_25");
      panel1.add(OBJ_25);
      OBJ_25.setBounds(15, 54, 82, 20);

      //---- OBJ_23 ----
      OBJ_23.setText("Code article");
      OBJ_23.setName("OBJ_23");
      panel1.add(OBJ_23);
      OBJ_23.setBounds(15, 29, 75, 20);

      //---- OBJ_30 ----
      OBJ_30.setText("@ATTE@");
      OBJ_30.setName("OBJ_30");
      panel1.add(OBJ_30);
      OBJ_30.setBounds(15, 155, 75, 20);
    }

    //======== P_PnlOpts ========
    {
      P_PnlOpts.setName("P_PnlOpts");
      P_PnlOpts.setLayout(null);

      //---- OBJ_37 ----
      OBJ_37.setText("");
      OBJ_37.setToolTipText("Acc\u00e8s fiche article apr\u00e9s cr\u00e9ation");
      OBJ_37.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_37);
      OBJ_37.setBounds(5, 6, 40, 40);

      //---- OBJ_38 ----
      OBJ_38.setText("");
      OBJ_38.setToolTipText("Produits approchants");
      OBJ_38.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      P_PnlOpts.add(OBJ_38);
      OBJ_38.setBounds(5, 45, 40, 40);
    }

    //======== panel2 ========
    {
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- OBJ_33 ----
      OBJ_33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      panel2.add(OBJ_33);
      OBJ_33.setBounds(70, 10, 56, 40);

      //---- OBJ_36 ----
      OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      panel2.add(OBJ_36);
      OBJ_36.setBounds(15, 10, 56, 40);

      //---- OBJ_32 ----
      OBJ_32.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      panel2.add(OBJ_32);
      OBJ_32.setBounds(125, 10, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(5, 5, 5)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 430, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(305, 305, 305)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(490, 490, 490)
          .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(5, 5, 5)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
          .addGap(30, 30, 30)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
        .addComponent(P_PnlOpts, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE)
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
      OBJ_4.addSeparator();

      //---- OBJ_9 ----
      OBJ_9.setText("Acc\u00e8s fiche article apr\u00e9s cr\u00e9ation");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);

      //---- OBJ_10 ----
      OBJ_10.setText("Produits approchants");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);
      OBJ_4.addSeparator();

      //---- OBJ_11 ----
      OBJ_11.setText("Exploitation");
      OBJ_11.setName("OBJ_11");
      OBJ_4.add(OBJ_11);
    }

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_13);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Invite");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel OBJ_31;
  private XRiTextField A1LB1S;
  private XRiTextField A1LB2S;
  private XRiTextField A1LB3S;
  private XRiTextField A1LB4S;
  private XRiTextField WARTS;
  private JLabel OBJ_25;
  private JLabel OBJ_23;
  private JLabel OBJ_30;
  private JPanel P_PnlOpts;
  private JButton OBJ_37;
  private JButton OBJ_38;
  private JPanel panel2;
  private JButton OBJ_33;
  private JButton OBJ_36;
  private JButton OBJ_32;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_13;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
