
package ri.serien.libecranrpg.rgpm.RGPM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Emmanuel MARCQ
 */
public class RGPM03FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "Code opération       Libellé                        Poste Clé classement", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 144, };
  // private String[][] _LIST2_Title_Data_Brut=null;
  
  public RGPM03FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  @SuppressWarnings("static-access")
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    barre_tete2.setVisible(lexique.getMode() == Lexical.MODE_DUPLICATION);
    
    
    OBJ_70.setVisible(lexique.HostFieldGetData("LD15").trim().contains("."));
    OBJ_69.setVisible(lexique.HostFieldGetData("LD14").trim().contains("."));
    OBJ_68.setVisible(lexique.HostFieldGetData("LD13").trim().contains("."));
    OBJ_67.setVisible(lexique.HostFieldGetData("LD12").trim().contains("."));
    OBJ_66.setVisible(lexique.HostFieldGetData("LD11").trim().contains("."));
    OBJ_65.setVisible(lexique.HostFieldGetData("LD10").trim().contains("."));
    OBJ_64.setVisible(lexique.HostFieldGetData("LD09").trim().contains("."));
    OBJ_63.setVisible(lexique.HostFieldGetData("LD08").trim().contains("."));
    OBJ_62.setVisible(lexique.HostFieldGetData("LD07").trim().contains("."));
    OBJ_61.setVisible(lexique.HostFieldGetData("LD06").trim().contains("."));
    OBJ_60.setVisible(lexique.HostFieldGetData("LD05").trim().contains("."));
    OBJ_59.setVisible(lexique.HostFieldGetData("LD04").trim().contains("."));
    OBJ_58.setVisible(lexique.HostFieldGetData("LD03").trim().contains("."));
    OBJ_57.setVisible(lexique.HostFieldGetData("LD02").trim().contains("."));
    OBJ_56.setVisible(lexique.HostFieldGetData("LD01").trim().contains("."));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP02", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP03", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP04", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP05", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP06", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP07", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP08", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP09", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP10", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP11", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP12", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP13", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP14", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_70ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP15", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_27 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_71 = new JLabel();
    INDCOD = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_tete2 = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_28 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_72 = new JLabel();
    IN3COD = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel1 = new JPanel();
    OBJ_56 = new SNBoutonDetail();
    OBJ_57 = new SNBoutonDetail();
    OBJ_58 = new SNBoutonDetail();
    OBJ_59 = new SNBoutonDetail();
    OBJ_60 = new SNBoutonDetail();
    OBJ_61 = new SNBoutonDetail();
    OBJ_62 = new SNBoutonDetail();
    OBJ_63 = new SNBoutonDetail();
    OBJ_64 = new SNBoutonDetail();
    OBJ_65 = new SNBoutonDetail();
    OBJ_66 = new SNBoutonDetail();
    OBJ_67 = new SNBoutonDetail();
    OBJ_68 = new SNBoutonDetail();
    OBJ_69 = new SNBoutonDetail();
    OBJ_70 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_29 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des op\u00e9rations");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setText("Etablissement");
          OBJ_27.setName("OBJ_27");
          p_tete_gauche.add(OBJ_27);
          OBJ_27.setBounds(5, 5, 93, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(120, 0, 40, INDETB.getPreferredSize().height);

          //---- OBJ_71 ----
          OBJ_71.setText("Code op\u00e9ration");
          OBJ_71.setName("OBJ_71");
          p_tete_gauche.add(OBJ_71);
          OBJ_71.setBounds(190, 5, 99, 18);

          //---- INDCOD ----
          INDCOD.setComponentPopupMenu(BTD);
          INDCOD.setName("INDCOD");
          p_tete_gauche.add(INDCOD);
          INDCOD.setBounds(305, 0, 210, INDCOD.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_tete2 ========
      {
        barre_tete2.setMinimumSize(new Dimension(111, 34));
        barre_tete2.setPreferredSize(new Dimension(111, 34));
        barre_tete2.setName("barre_tete2");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("Par duplication de");
          OBJ_28.setName("OBJ_28");
          p_tete_gauche2.add(OBJ_28);
          OBJ_28.setBounds(5, 5, 115, 18);

          //---- IN3ETB ----
          IN3ETB.setComponentPopupMenu(BTD);
          IN3ETB.setName("IN3ETB");
          p_tete_gauche2.add(IN3ETB);
          IN3ETB.setBounds(120, 0, 40, IN3ETB.getPreferredSize().height);

          //---- OBJ_72 ----
          OBJ_72.setText("Code op\u00e9ration");
          OBJ_72.setName("OBJ_72");
          p_tete_gauche2.add(OBJ_72);
          OBJ_72.setBounds(190, 5, 99, 18);

          //---- IN3COD ----
          IN3COD.setComponentPopupMenu(BTD);
          IN3COD.setName("IN3COD");
          p_tete_gauche2.add(IN3COD);
          IN3COD.setBounds(305, 0, 210, IN3COD.getPreferredSize().height);
        }
        barre_tete2.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete2.add(p_tete_droite2);
      }
      p_nord.add(barre_tete2);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(880, 390));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(880, 390));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Op\u00e9rations"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(45, 60, 745, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(800, 60, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(800, 205, 25, 125);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- OBJ_56 ----
              OBJ_56.setText("");
              OBJ_56.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_56.setName("OBJ_56");
              OBJ_56.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_56ActionPerformed(e);
                }
              });
              panel1.add(OBJ_56);
              OBJ_56.setBounds(20, 10, 13, 16);

              //---- OBJ_57 ----
              OBJ_57.setText("");
              OBJ_57.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_57.setName("OBJ_57");
              OBJ_57.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_57ActionPerformed(e);
                }
              });
              panel1.add(OBJ_57);
              OBJ_57.setBounds(20, 26, 13, 16);

              //---- OBJ_58 ----
              OBJ_58.setText("");
              OBJ_58.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_58.setName("OBJ_58");
              OBJ_58.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_58ActionPerformed(e);
                }
              });
              panel1.add(OBJ_58);
              OBJ_58.setBounds(20, 42, 13, 16);

              //---- OBJ_59 ----
              OBJ_59.setText("");
              OBJ_59.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_59.setName("OBJ_59");
              OBJ_59.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_59ActionPerformed(e);
                }
              });
              panel1.add(OBJ_59);
              OBJ_59.setBounds(20, 58, 13, 16);

              //---- OBJ_60 ----
              OBJ_60.setText("");
              OBJ_60.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_60.setName("OBJ_60");
              OBJ_60.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_60ActionPerformed(e);
                }
              });
              panel1.add(OBJ_60);
              OBJ_60.setBounds(20, 74, 13, 16);

              //---- OBJ_61 ----
              OBJ_61.setText("");
              OBJ_61.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_61.setName("OBJ_61");
              OBJ_61.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_61ActionPerformed(e);
                }
              });
              panel1.add(OBJ_61);
              OBJ_61.setBounds(20, 90, 13, 16);

              //---- OBJ_62 ----
              OBJ_62.setText("");
              OBJ_62.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_62.setName("OBJ_62");
              OBJ_62.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_62ActionPerformed(e);
                }
              });
              panel1.add(OBJ_62);
              OBJ_62.setBounds(20, 106, 13, 16);

              //---- OBJ_63 ----
              OBJ_63.setText("");
              OBJ_63.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_63.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_63.setName("OBJ_63");
              OBJ_63.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_63ActionPerformed(e);
                }
              });
              panel1.add(OBJ_63);
              OBJ_63.setBounds(20, 122, 13, 16);

              //---- OBJ_64 ----
              OBJ_64.setText("");
              OBJ_64.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_64.setName("OBJ_64");
              OBJ_64.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_64ActionPerformed(e);
                }
              });
              panel1.add(OBJ_64);
              OBJ_64.setBounds(20, 138, 13, 16);

              //---- OBJ_65 ----
              OBJ_65.setText("");
              OBJ_65.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_65.setName("OBJ_65");
              OBJ_65.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_65ActionPerformed(e);
                }
              });
              panel1.add(OBJ_65);
              OBJ_65.setBounds(20, 154, 13, 16);

              //---- OBJ_66 ----
              OBJ_66.setText("");
              OBJ_66.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_66.setName("OBJ_66");
              OBJ_66.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_66ActionPerformed(e);
                }
              });
              panel1.add(OBJ_66);
              OBJ_66.setBounds(20, 170, 13, 16);

              //---- OBJ_67 ----
              OBJ_67.setText("");
              OBJ_67.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_67.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_67.setName("OBJ_67");
              OBJ_67.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_67ActionPerformed(e);
                }
              });
              panel1.add(OBJ_67);
              OBJ_67.setBounds(20, 186, 13, 16);

              //---- OBJ_68 ----
              OBJ_68.setText("");
              OBJ_68.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_68.setName("OBJ_68");
              OBJ_68.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_68ActionPerformed(e);
                }
              });
              panel1.add(OBJ_68);
              OBJ_68.setBounds(20, 202, 13, 16);

              //---- OBJ_69 ----
              OBJ_69.setText("");
              OBJ_69.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_69.setName("OBJ_69");
              OBJ_69.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_69ActionPerformed(e);
                }
              });
              panel1.add(OBJ_69);
              OBJ_69.setBounds(20, 218, 13, 16);

              //---- OBJ_70 ----
              OBJ_70.setText("");
              OBJ_70.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_70.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_70.setName("OBJ_70");
              OBJ_70.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_70ActionPerformed(e);
                }
              });
              panel1.add(OBJ_70);
              OBJ_70.setBounds(20, 234, 13, 16);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel2.add(panel1);
            panel1.setBounds(5, 75, 35, 265);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 854, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 362, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_22 ----
      OBJ_22.setText("Modifier");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Annuler");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_23);

      //---- OBJ_24 ----
      OBJ_24.setText("Interroger");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);
      BTD2.addSeparator();

      //---- OBJ_30 ----
      OBJ_30.setText("Choix possibles");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_30);

      //---- OBJ_29 ----
      OBJ_29.setText("Aide en ligne");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_29);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_27;
  private XRiTextField INDETB;
  private JLabel OBJ_71;
  private XRiTextField INDCOD;
  private JPanel p_tete_droite;
  private JMenuBar barre_tete2;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_28;
  private XRiTextField IN3ETB;
  private JLabel OBJ_72;
  private XRiTextField IN3COD;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel1;
  private SNBoutonDetail OBJ_56;
  private SNBoutonDetail OBJ_57;
  private SNBoutonDetail OBJ_58;
  private SNBoutonDetail OBJ_59;
  private SNBoutonDetail OBJ_60;
  private SNBoutonDetail OBJ_61;
  private SNBoutonDetail OBJ_62;
  private SNBoutonDetail OBJ_63;
  private SNBoutonDetail OBJ_64;
  private SNBoutonDetail OBJ_65;
  private SNBoutonDetail OBJ_66;
  private SNBoutonDetail OBJ_67;
  private SNBoutonDetail OBJ_68;
  private SNBoutonDetail OBJ_69;
  private SNBoutonDetail OBJ_70;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_29;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
