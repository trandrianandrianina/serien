
package ri.serien.libecranrpg.rgpm.RGPM14FM;
// Nom Fichier: pop_RGPM14FM_FMTA3_FMTF1_226.java

import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGPM14FM_A3 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RGPM14FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX3.setValeurs("3", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX2.setValeurs("2", "RB");
    
    // Bouton par défaut
    setDefaultButton(OBJ_35);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    ARG2.setEnabled(lexique.isPresent("ARG2"));
    ARG13.setEnabled(lexique.isPresent("ARG13"));
    ARG12.setEnabled(lexique.isPresent("ARG12"));
    ARG11.setEnabled(lexique.isPresent("ARG11"));
    // TIDX3.setVisible( lexique.isPresent("RB"));
    // TIDX3.setEnabled( lexique.isPresent("RB"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("3"));
    // TIDX1.setEnabled( lexique.isPresent("RB"));
    // TIDX1.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("1"));
    // TIDX2.setVisible( lexique.isPresent("RB"));
    // TIDX2.setEnabled( lexique.isPresent("RB"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RB").equalsIgnoreCase("2"));
    ARG3.setEnabled(lexique.isPresent("ARG3"));
    
    // TODO Icones
    OBJ_35.setIcon(lexique.chargerImage("images/OK.png", true));
    OBJ_36.setIcon(lexique.chargerImage("images/retour.png", true));
    // V07F
    
    // Titre
    // setTitle(???);
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RB", 0, "3");
    // if (TIDX1.isSelected())
    // lexique.HostFieldPutData("RB", 0, "1");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RB", 0, "2");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="ENTER"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", true);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "rgpm14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTDA.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "rgpm14"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    ARG3 = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    ARG11 = new XRiTextField();
    ARG12 = new XRiTextField();
    ARG13 = new XRiTextField();
    ARG2 = new XRiTextField();
    panel2 = new JPanel();
    OBJ_36 = new JButton();
    OBJ_35 = new JButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setName("this");

    //======== panel1 ========
    {
      panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- ARG3 ----
      ARG3.setComponentPopupMenu(BTD);
      ARG3.setName("ARG3");
      panel1.add(ARG3);
      ARG3.setBounds(135, 80, 210, ARG3.getPreferredSize().height);

      //---- TIDX2 ----
      TIDX2.setText("Type de ligne");
      TIDX2.setComponentPopupMenu(BTD);
      TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX2.setName("TIDX2");
      panel1.add(TIDX2);
      TIDX2.setBounds(10, 55, 108, 20);

      //---- TIDX1 ----
      TIDX1.setText("Niveaux");
      TIDX1.setComponentPopupMenu(BTD);
      TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX1.setName("TIDX1");
      panel1.add(TIDX1);
      TIDX1.setBounds(10, 30, 74, 20);

      //---- TIDX3 ----
      TIDX3.setText("Ordre");
      TIDX3.setComponentPopupMenu(BTD);
      TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      TIDX3.setName("TIDX3");
      panel1.add(TIDX3);
      TIDX3.setBounds(10, 80, 59, 20);

      //---- ARG11 ----
      ARG11.setComponentPopupMenu(BTD);
      ARG11.setName("ARG11");
      panel1.add(ARG11);
      ARG11.setBounds(135, 30, 40, ARG11.getPreferredSize().height);

      //---- ARG12 ----
      ARG12.setComponentPopupMenu(BTD);
      ARG12.setName("ARG12");
      panel1.add(ARG12);
      ARG12.setBounds(180, 30, 40, ARG12.getPreferredSize().height);

      //---- ARG13 ----
      ARG13.setComponentPopupMenu(BTD);
      ARG13.setName("ARG13");
      panel1.add(ARG13);
      ARG13.setBounds(225, 30, 40, ARG13.getPreferredSize().height);

      //---- ARG2 ----
      ARG2.setComponentPopupMenu(BTD);
      ARG2.setName("ARG2");
      panel1.add(ARG2);
      ARG2.setBounds(135, 55, 40, ARG2.getPreferredSize().height);
    }

    //======== panel2 ========
    {
      panel2.setName("panel2");
      panel2.setLayout(null);

      //---- OBJ_36 ----
      OBJ_36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      panel2.add(OBJ_36);
      OBJ_36.setBounds(5, 5, 56, 40);

      //---- OBJ_35 ----
      OBJ_35.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      panel2.add(OBJ_35);
      OBJ_35.setBounds(65, 5, 56, 40);
    }

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 360, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(250, 250, 250)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 130, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(10, 10, 10)
          .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 122, GroupLayout.PREFERRED_SIZE)
          .addGap(13, 13, 13)
          .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
    );

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_9);
      OBJ_4.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_14);
      OBJ_4.addSeparator();

      //---- OBJ_15 ----
      OBJ_15.setText("Exploitation");
      OBJ_15.setName("OBJ_15");
      OBJ_4.add(OBJ_15);
    }

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_17);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Invite");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX3);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private XRiTextField ARG3;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG11;
  private XRiTextField ARG12;
  private XRiTextField ARG13;
  private XRiTextField ARG2;
  private JPanel panel2;
  private JButton OBJ_36;
  private JButton OBJ_35;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_15;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
