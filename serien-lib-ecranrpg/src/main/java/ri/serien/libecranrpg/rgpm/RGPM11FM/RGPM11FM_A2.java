
package ri.serien.libecranrpg.rgpm.RGPM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

// Nom Fichier: b_RGPM11FM_FMTA2_FMTF1_2.java

import javax.swing.JSeparator;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGPM11FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "LLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 150, };
  private boolean isKit = false;
  
  public RGPM11FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST2, LIST2.get_LIST_Title_Data_Brut(), _WTP01_Top);
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // bouton info
    
    
    
    // est on en mode kit ou nomenclature ?
    isKit = lexique.isTrue("87");
    
    if (isKit) {
      p_bpresentation.setText("Gestion des kits");
    }
    
    riBoutonDetailListe15.setVisible(lexique.HostFieldGetData("LD15").trim().contains("·")); // le code "·" est dispo au
                                                                                             // clavier Windows sous son
                                                                                             // code ASCII : ALT + 0183
    riBoutonDetailListe14.setVisible(lexique.HostFieldGetData("LD14").trim().contains("·"));
    riBoutonDetailListe13.setVisible(lexique.HostFieldGetData("LD13").trim().contains("·"));
    riBoutonDetailListe12.setVisible(lexique.HostFieldGetData("LD12").trim().contains("·"));
    riBoutonDetailListe11.setVisible(lexique.HostFieldGetData("LD11").trim().contains("·"));
    riBoutonDetailListe10.setVisible(lexique.HostFieldGetData("LD10").trim().contains("·"));
    riBoutonDetailListe9.setVisible(lexique.HostFieldGetData("LD09").trim().contains("·"));
    riBoutonDetailListe8.setVisible(lexique.HostFieldGetData("LD08").trim().contains("·"));
    riBoutonDetailListe7.setVisible(lexique.HostFieldGetData("LD07").trim().contains("·"));
    riBoutonDetailListe6.setVisible(lexique.HostFieldGetData("LD06").trim().contains("·"));
    riBoutonDetailListe5.setVisible(lexique.HostFieldGetData("LD05").trim().contains("·"));
    riBoutonDetailListe4.setVisible(lexique.HostFieldGetData("LD04").trim().contains("·"));
    riBoutonDetailListe3.setVisible(lexique.HostFieldGetData("LD03").trim().contains("·"));
    riBoutonDetailListe2.setVisible(lexique.HostFieldGetData("LD02").trim().contains("·"));
    riBoutonDetailListe1.setVisible(lexique.HostFieldGetData("LD01").trim().contains("·"));
    INDETB.setEnabled(lexique.isPresent("INDETB"));
    INDNUM.setEnabled(lexique.isPresent("INDNUM"));
    OBJ_44.setVisible(INDNUM.isVisible());
    OBJ_45.setVisible(lexique.isPresent("V01F"));
    ARG4.setEnabled(lexique.isPresent("ARG4"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("L");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void riBoutonDetailListe1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riBoutonDetailListe2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP02", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP03", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe4ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP04", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe5ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP05", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe6ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP06", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP07", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP08", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP09", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP10", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP11", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP12", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP13", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP14", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void riBoutonDetailListe15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP15", 1, "B");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(WTP01.getSelectedRow() + 7, 4);
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_42 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_45 = new JLabel();
    ARG4 = new XRiTextField();
    OBJ_44 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    riBoutonDetailListe1 = new SNBoutonDetail();
    riBoutonDetailListe2 = new SNBoutonDetail();
    riBoutonDetailListe3 = new SNBoutonDetail();
    riBoutonDetailListe4 = new SNBoutonDetail();
    riBoutonDetailListe5 = new SNBoutonDetail();
    riBoutonDetailListe6 = new SNBoutonDetail();
    riBoutonDetailListe7 = new SNBoutonDetail();
    riBoutonDetailListe8 = new SNBoutonDetail();
    riBoutonDetailListe9 = new SNBoutonDetail();
    riBoutonDetailListe10 = new SNBoutonDetail();
    riBoutonDetailListe11 = new SNBoutonDetail();
    riBoutonDetailListe12 = new SNBoutonDetail();
    riBoutonDetailListe13 = new SNBoutonDetail();
    riBoutonDetailListe14 = new SNBoutonDetail();
    riBoutonDetailListe15 = new SNBoutonDetail();
    BTD = new JPopupMenu();
    OBJ_21 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    menuItem1 = new JMenuItem();
    separator1 = new JSeparator();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_27 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion des nomenclatures");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setMaximumSize(new Dimension(615, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_42 ----
          OBJ_42.setText("Etablissement");
          OBJ_42.setName("OBJ_42");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- OBJ_45 ----
          OBJ_45.setText("Article");
          OBJ_45.setName("OBJ_45");

          //---- ARG4 ----
          ARG4.setComponentPopupMenu(null);
          ARG4.setName("ARG4");

          //---- OBJ_44 ----
          OBJ_44.setText("Num\u00e9ro");
          OBJ_44.setName("OBJ_44");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(2, 2, 2)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(ARG4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 300));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autres vues");
              riSousMenu_bt10.setToolTipText("Autres vues");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Historique des versions");
              riSousMenu_bt11.setToolTipText("Historique des versions");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("+/- article composant");
              riSousMenu_bt6.setToolTipText("Ajout, suppression ou remplacement d'article composant");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(930, 440));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setRowHeight(20);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(55, 40, 770, 330);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(835, 40, 25, 155);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(835, 215, 25, 155);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- riBoutonDetailListe1 ----
              riBoutonDetailListe1.setToolTipText("Information technique");
              riBoutonDetailListe1.setName("riBoutonDetailListe1");
              riBoutonDetailListe1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe1ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe1);
              riBoutonDetailListe1.setBounds(new Rectangle(new Point(5, 5), riBoutonDetailListe1.getPreferredSize()));

              //---- riBoutonDetailListe2 ----
              riBoutonDetailListe2.setToolTipText("Information technique");
              riBoutonDetailListe2.setName("riBoutonDetailListe2");
              riBoutonDetailListe2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe2ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe2);
              riBoutonDetailListe2.setBounds(5, 25, 15, 15);

              //---- riBoutonDetailListe3 ----
              riBoutonDetailListe3.setToolTipText("Information technique");
              riBoutonDetailListe3.setName("riBoutonDetailListe3");
              riBoutonDetailListe3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe3ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe3);
              riBoutonDetailListe3.setBounds(5, 45, 15, 15);

              //---- riBoutonDetailListe4 ----
              riBoutonDetailListe4.setToolTipText("Information technique");
              riBoutonDetailListe4.setName("riBoutonDetailListe4");
              riBoutonDetailListe4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe4ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe4);
              riBoutonDetailListe4.setBounds(5, 65, 15, 15);

              //---- riBoutonDetailListe5 ----
              riBoutonDetailListe5.setToolTipText("Information technique");
              riBoutonDetailListe5.setName("riBoutonDetailListe5");
              riBoutonDetailListe5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe5ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe5);
              riBoutonDetailListe5.setBounds(5, 85, 15, 15);

              //---- riBoutonDetailListe6 ----
              riBoutonDetailListe6.setToolTipText("Information technique");
              riBoutonDetailListe6.setName("riBoutonDetailListe6");
              riBoutonDetailListe6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe6ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe6);
              riBoutonDetailListe6.setBounds(5, 105, 15, 15);

              //---- riBoutonDetailListe7 ----
              riBoutonDetailListe7.setToolTipText("Information technique");
              riBoutonDetailListe7.setName("riBoutonDetailListe7");
              riBoutonDetailListe7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe7ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe7);
              riBoutonDetailListe7.setBounds(5, 125, 15, 15);

              //---- riBoutonDetailListe8 ----
              riBoutonDetailListe8.setToolTipText("Information technique");
              riBoutonDetailListe8.setName("riBoutonDetailListe8");
              riBoutonDetailListe8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe8ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe8);
              riBoutonDetailListe8.setBounds(5, 145, 15, 15);

              //---- riBoutonDetailListe9 ----
              riBoutonDetailListe9.setToolTipText("Information technique");
              riBoutonDetailListe9.setName("riBoutonDetailListe9");
              riBoutonDetailListe9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe9ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe9);
              riBoutonDetailListe9.setBounds(5, 165, 15, 15);

              //---- riBoutonDetailListe10 ----
              riBoutonDetailListe10.setToolTipText("Information technique");
              riBoutonDetailListe10.setName("riBoutonDetailListe10");
              riBoutonDetailListe10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe10ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe10);
              riBoutonDetailListe10.setBounds(5, 185, 15, 15);

              //---- riBoutonDetailListe11 ----
              riBoutonDetailListe11.setToolTipText("Information technique");
              riBoutonDetailListe11.setName("riBoutonDetailListe11");
              riBoutonDetailListe11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe11ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe11);
              riBoutonDetailListe11.setBounds(5, 205, 15, 15);

              //---- riBoutonDetailListe12 ----
              riBoutonDetailListe12.setToolTipText("Information technique");
              riBoutonDetailListe12.setName("riBoutonDetailListe12");
              riBoutonDetailListe12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe12ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe12);
              riBoutonDetailListe12.setBounds(5, 225, 15, 15);

              //---- riBoutonDetailListe13 ----
              riBoutonDetailListe13.setToolTipText("Information technique");
              riBoutonDetailListe13.setName("riBoutonDetailListe13");
              riBoutonDetailListe13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe13ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe13);
              riBoutonDetailListe13.setBounds(5, 245, 15, 15);

              //---- riBoutonDetailListe14 ----
              riBoutonDetailListe14.setToolTipText("Information technique");
              riBoutonDetailListe14.setName("riBoutonDetailListe14");
              riBoutonDetailListe14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe14ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe14);
              riBoutonDetailListe14.setBounds(5, 265, 15, 15);

              //---- riBoutonDetailListe15 ----
              riBoutonDetailListe15.setToolTipText("Information technique");
              riBoutonDetailListe15.setName("riBoutonDetailListe15");
              riBoutonDetailListe15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riBoutonDetailListe15ActionPerformed(e);
                }
              });
              panel3.add(riBoutonDetailListe15);
              riBoutonDetailListe15.setBounds(5, 285, 15, 15);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(25, 65, 25, 300);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 892, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 397, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_21 ----
      OBJ_21.setText("Choix possibles");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_24 ----
      OBJ_24.setText("Modifier");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("Annuler");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- menuItem1 ----
      menuItem1.setText("D\u00e9verrouiller");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      BTD2.add(menuItem1);

      //---- separator1 ----
      separator1.setName("separator1");
      BTD2.add(separator1);

      //---- OBJ_28 ----
      OBJ_28.setText("Lignes composantes");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Attendu/Command\u00e9");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_29);

      //---- OBJ_27 ----
      OBJ_27.setText("Bloc-notes");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_27);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_42;
  private XRiTextField INDETB;
  private JLabel OBJ_45;
  private XRiTextField ARG4;
  private JLabel OBJ_44;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private SNBoutonDetail riBoutonDetailListe1;
  private SNBoutonDetail riBoutonDetailListe2;
  private SNBoutonDetail riBoutonDetailListe3;
  private SNBoutonDetail riBoutonDetailListe4;
  private SNBoutonDetail riBoutonDetailListe5;
  private SNBoutonDetail riBoutonDetailListe6;
  private SNBoutonDetail riBoutonDetailListe7;
  private SNBoutonDetail riBoutonDetailListe8;
  private SNBoutonDetail riBoutonDetailListe9;
  private SNBoutonDetail riBoutonDetailListe10;
  private SNBoutonDetail riBoutonDetailListe11;
  private SNBoutonDetail riBoutonDetailListe12;
  private SNBoutonDetail riBoutonDetailListe13;
  private SNBoutonDetail riBoutonDetailListe14;
  private SNBoutonDetail riBoutonDetailListe15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_20;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem menuItem1;
  private JSeparator separator1;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_27;
  // JFormDesigner - End of variables declaration  //GEN-END:variables


}
