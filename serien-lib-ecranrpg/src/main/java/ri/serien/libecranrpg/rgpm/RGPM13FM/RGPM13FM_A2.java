
package ri.serien.libecranrpg.rgpm.RGPM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGPM13FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "LLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 15, };
  private boolean isKit = false;
  
  public RGPM13FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNUM@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDSUF@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    WLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    WUNS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WUNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST2, LIST2.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    isKit = lexique.isTrue("87");
    
    if (isKit) {
      p_bpresentation.setText("Composition du kit");
      OBJ_76.setText("Kit");
    }
    
    riSousMenu8.setEnabled(!isKit);
    
    OBJ_105.setVisible(!lexique.HostFieldGetData("P15").trim().equalsIgnoreCase(""));
    OBJ_104.setVisible(!lexique.HostFieldGetData("P14").trim().equalsIgnoreCase(""));
    OBJ_102.setVisible(!lexique.HostFieldGetData("P13").trim().equalsIgnoreCase(""));
    OBJ_101.setVisible(!lexique.HostFieldGetData("P12").trim().equalsIgnoreCase(""));
    OBJ_100.setVisible(!lexique.HostFieldGetData("P11").trim().equalsIgnoreCase(""));
    OBJ_99.setVisible(!lexique.HostFieldGetData("P10").trim().equalsIgnoreCase(""));
    OBJ_98.setVisible(!lexique.HostFieldGetData("P09").trim().equalsIgnoreCase(""));
    OBJ_97.setVisible(!lexique.HostFieldGetData("P08").trim().equalsIgnoreCase(""));
    OBJ_95.setVisible(!lexique.HostFieldGetData("P07").trim().equalsIgnoreCase(""));
    OBJ_94.setVisible(!lexique.HostFieldGetData("P06").trim().equalsIgnoreCase(""));
    OBJ_91.setVisible(!lexique.HostFieldGetData("P05").trim().equalsIgnoreCase(""));
    OBJ_90.setVisible(!lexique.HostFieldGetData("P04").trim().equalsIgnoreCase(""));
    OBJ_89.setVisible(!lexique.HostFieldGetData("P03").trim().equalsIgnoreCase(""));
    OBJ_88.setVisible(!lexique.HostFieldGetData("P02").trim().equalsIgnoreCase(""));
    OBJ_87.setVisible(!lexique.HostFieldGetData("P01").trim().equalsIgnoreCase(""));
    INDSUF.setVisible(lexique.isPresent("INDNUM"));
    INDETB.setVisible(lexique.isPresent("INDETB"));
    WUNS.setVisible(lexique.isPresent("WUNS"));
    INDNUM.setVisible(lexique.isPresent("INDNUM"));
    OBJ_67.setVisible(lexique.isPresent("INDNUM"));
    WLIB.setVisible(lexique.isPresent("WLIB"));
    WART.setVisible(lexique.isPresent("WART"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "2", "Enter");
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "4", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "5", "Enter");
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "C", "Enter");
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST2, _WTP01_Top, "A", "Enter");
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST2, _WTP01_Top, "1", "ENTER", e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_87ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_89ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 2, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_90ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 3, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_91ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 4, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_94ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 5, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_95ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 6, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_97ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 7, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_98ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 8, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_99ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 9, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_100ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 10, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_101ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 11, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_102ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 12, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_104ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 13, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_105ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTP01", 14, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_66 = new JLabel();
    OBJ_67 = new JLabel();
    INDNUM = new RiZoneSortie();
    INDETB = new RiZoneSortie();
    INDSUF = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    WART = new RiZoneSortie();
    WLIB = new RiZoneSortie();
    OBJ_76 = new JLabel();
    WUNS = new RiZoneSortie();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    label1 = new JLabel();
    panel3 = new JPanel();
    OBJ_87 = new SNBoutonDetail();
    OBJ_88 = new SNBoutonDetail();
    OBJ_89 = new SNBoutonDetail();
    OBJ_90 = new SNBoutonDetail();
    OBJ_91 = new SNBoutonDetail();
    OBJ_94 = new SNBoutonDetail();
    OBJ_95 = new SNBoutonDetail();
    OBJ_97 = new SNBoutonDetail();
    OBJ_98 = new SNBoutonDetail();
    OBJ_99 = new SNBoutonDetail();
    OBJ_100 = new SNBoutonDetail();
    OBJ_101 = new SNBoutonDetail();
    OBJ_102 = new SNBoutonDetail();
    OBJ_104 = new SNBoutonDetail();
    OBJ_105 = new SNBoutonDetail();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Affichage de la nomenclature");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_66 ----
          OBJ_66.setText("Etablissement");
          OBJ_66.setName("OBJ_66");
          p_tete_gauche.add(OBJ_66);
          OBJ_66.setBounds(5, 5, 95, 18);

          //---- OBJ_67 ----
          OBJ_67.setText("Num\u00e9ro");
          OBJ_67.setName("OBJ_67");
          p_tete_gauche.add(OBJ_67);
          OBJ_67.setBounds(170, 5, 60, 18);

          //---- INDNUM ----
          INDNUM.setOpaque(false);
          INDNUM.setText("@INDNUM@");
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(230, 2, 60, INDNUM.getPreferredSize().height);

          //---- INDETB ----
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 2, 40, INDETB.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setOpaque(false);
          INDSUF.setText("@INDSUF@");
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(295, 2, 20, INDSUF.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Afficher ligne");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Quitter");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Autres vues");
              riSousMenu_bt9.setToolTipText("Autres vues");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Nouvelle ligne");
              riSousMenu_bt11.setToolTipText("Nouvelle ligne");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Composants pleine page");
              riSousMenu_bt10.setToolTipText("Cr\u00e9ation de composants pleine page");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("D\u00e9tail nomenclature");
              riSousMenu_bt8.setToolTipText("Affichage d\u00e9tail nomenclature");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Exportation tableur");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(910, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Composition"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD2);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setRowHeight(20);
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(55, 90, 730, 330);

            //---- WART ----
            WART.setText("@WART@");
            WART.setFont(WART.getFont().deriveFont(WART.getFont().getStyle() | Font.BOLD));
            WART.setName("WART");
            panel1.add(WART);
            WART.setBounds(160, 44, 210, WART.getPreferredSize().height);

            //---- WLIB ----
            WLIB.setText("@WLIB@");
            WLIB.setFont(WLIB.getFont().deriveFont(WLIB.getFont().getStyle() | Font.BOLD));
            WLIB.setName("WLIB");
            panel1.add(WLIB);
            WLIB.setBounds(375, 44, 310, WLIB.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Article compos\u00e9");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(55, 45, 105, 22);

            //---- WUNS ----
            WUNS.setText("@WUNS@");
            WUNS.setName("WUNS");
            panel1.add(WUNS);
            WUNS.setBounds(755, 44, 30, WUNS.getPreferredSize().height);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(795, 90, 25, 145);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(795, 275, 25, 145);

            //---- label1 ----
            label1.setText("US");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(720, 46, 30, 20);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- OBJ_87 ----
              OBJ_87.setToolTipText("Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_87.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_87.setPreferredSize(new Dimension(20, 20));
              OBJ_87.setMinimumSize(new Dimension(20, 20));
              OBJ_87.setName("OBJ_87");
              OBJ_87.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_87ActionPerformed(e);
                }
              });
              panel3.add(OBJ_87);
              OBJ_87.setBounds(5, 5, 20, 20);

              //---- OBJ_88 ----
              OBJ_88.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_88.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_88.setName("OBJ_88");
              OBJ_88.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_88ActionPerformed(e);
                }
              });
              panel3.add(OBJ_88);
              OBJ_88.setBounds(5, 25, 20, 20);

              //---- OBJ_89 ----
              OBJ_89.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_89.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_89.setName("OBJ_89");
              OBJ_89.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_89ActionPerformed(e);
                }
              });
              panel3.add(OBJ_89);
              OBJ_89.setBounds(5, 45, 20, 20);

              //---- OBJ_90 ----
              OBJ_90.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_90.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_90.setName("OBJ_90");
              OBJ_90.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_90ActionPerformed(e);
                }
              });
              panel3.add(OBJ_90);
              OBJ_90.setBounds(5, 65, 20, 20);

              //---- OBJ_91 ----
              OBJ_91.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_91.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_91.setName("OBJ_91");
              OBJ_91.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_91ActionPerformed(e);
                }
              });
              panel3.add(OBJ_91);
              OBJ_91.setBounds(5, 85, 20, 20);

              //---- OBJ_94 ----
              OBJ_94.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_94.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_94.setName("OBJ_94");
              OBJ_94.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_94ActionPerformed(e);
                }
              });
              panel3.add(OBJ_94);
              OBJ_94.setBounds(5, 105, 20, 20);

              //---- OBJ_95 ----
              OBJ_95.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_95.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_95.setName("OBJ_95");
              OBJ_95.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_95ActionPerformed(e);
                }
              });
              panel3.add(OBJ_95);
              OBJ_95.setBounds(5, 125, 20, 20);

              //---- OBJ_97 ----
              OBJ_97.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_97.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_97.setName("OBJ_97");
              OBJ_97.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_97ActionPerformed(e);
                }
              });
              panel3.add(OBJ_97);
              OBJ_97.setBounds(5, 145, 20, 20);

              //---- OBJ_98 ----
              OBJ_98.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_98.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_98.setName("OBJ_98");
              OBJ_98.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_98ActionPerformed(e);
                }
              });
              panel3.add(OBJ_98);
              OBJ_98.setBounds(5, 165, 20, 20);

              //---- OBJ_99 ----
              OBJ_99.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_99.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_99.setName("OBJ_99");
              OBJ_99.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_99ActionPerformed(e);
                }
              });
              panel3.add(OBJ_99);
              OBJ_99.setBounds(5, 185, 20, 20);

              //---- OBJ_100 ----
              OBJ_100.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_100.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_100.setName("OBJ_100");
              OBJ_100.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_100ActionPerformed(e);
                }
              });
              panel3.add(OBJ_100);
              OBJ_100.setBounds(5, 205, 20, 20);

              //---- OBJ_101 ----
              OBJ_101.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_101.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_101.setName("OBJ_101");
              OBJ_101.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_101ActionPerformed(e);
                }
              });
              panel3.add(OBJ_101);
              OBJ_101.setBounds(5, 225, 20, 20);

              //---- OBJ_102 ----
              OBJ_102.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_102.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_102.setName("OBJ_102");
              OBJ_102.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_102ActionPerformed(e);
                }
              });
              panel3.add(OBJ_102);
              OBJ_102.setBounds(5, 245, 20, 20);

              //---- OBJ_104 ----
              OBJ_104.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_104.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_104.setName("OBJ_104");
              OBJ_104.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_104ActionPerformed(e);
                }
              });
              panel3.add(OBJ_104);
              OBJ_104.setBounds(5, 265, 20, 20);

              //---- OBJ_105 ----
              OBJ_105.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              OBJ_105.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_105.setName("OBJ_105");
              OBJ_105.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_105ActionPerformed(e);
                }
              });
              panel3.add(OBJ_105);
              OBJ_105.setBounds(5, 285, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(22, 111, 30, 305);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 850, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(31, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 458, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(33, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_24 ----
      OBJ_24.setText("Modifier");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("Annuler");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_25);

      //---- OBJ_26 ----
      OBJ_26.setText("Interroger");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("Chiffrage");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("Options article");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_29);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_66;
  private JLabel OBJ_67;
  private RiZoneSortie INDNUM;
  private RiZoneSortie INDETB;
  private RiZoneSortie INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private RiZoneSortie WART;
  private RiZoneSortie WLIB;
  private JLabel OBJ_76;
  private RiZoneSortie WUNS;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel label1;
  private JPanel panel3;
  private SNBoutonDetail OBJ_87;
  private SNBoutonDetail OBJ_88;
  private SNBoutonDetail OBJ_89;
  private SNBoutonDetail OBJ_90;
  private SNBoutonDetail OBJ_91;
  private SNBoutonDetail OBJ_94;
  private SNBoutonDetail OBJ_95;
  private SNBoutonDetail OBJ_97;
  private SNBoutonDetail OBJ_98;
  private SNBoutonDetail OBJ_99;
  private SNBoutonDetail OBJ_100;
  private SNBoutonDetail OBJ_101;
  private SNBoutonDetail OBJ_102;
  private SNBoutonDetail OBJ_104;
  private SNBoutonDetail OBJ_105;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
