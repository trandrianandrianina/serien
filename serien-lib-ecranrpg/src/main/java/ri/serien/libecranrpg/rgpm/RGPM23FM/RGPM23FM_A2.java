
package ri.serien.libecranrpg.rgpm.RGPM23FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class RGPM23FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "LLD", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 15, };
  private JButton[] listeBouton = null;
  
  public RGPM23FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    listeBouton = new JButton[] { p1, p2, p3, p4, p5, p6, p7, p8, p9, p10, p11, p12, p13, p14, p15 };
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Ordres de fabrication @LIBPG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WART@")).trim());
    WLIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WLIB@")).trim());
    WQTEB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WQTEB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // Visibilité des boutons suivant les données de la ligne
    for (int i = 0; i < listeBouton.length; i++) {
      listeBouton[i].setIcon(lexique.chargerImage("images/detailsMini2.png", true));
      listeBouton[i].setVisible(
          !lexique.HostFieldGetData("LD" + ((i + 1) < 10 ? "0" + (i + 1) : (i + 1))).substring(3, 4).trim().equalsIgnoreCase(""));
    }
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITRE@ @LIBPG@"));
    
    

    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("L");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_77ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 1, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_78ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 2, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_79ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 3, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_81ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 4, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_82ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 5, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_83ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 6, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_84ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 7, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_85ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 8, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_86ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 9, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_88ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 10, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_89ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 11, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_90ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 12, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_92ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 13, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void OBJ_93ActionPerformed(ActionEvent e) {
    WTP01.setClearTop(false);
    lexique.HostFieldPutData("WTP01", 14, "1");
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_112 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_113 = new JLabel();
    INDETB = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_120 = new JLabel();
    WART = new RiZoneSortie();
    WLIB = new RiZoneSortie();
    WQTEB = new RiZoneSortie();
    OBJ_122 = new JLabel();
    SCROLLPANE_LIST2 = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel3 = new JPanel();
    p1 = new JButton();
    p2 = new JButton();
    p3 = new JButton();
    p4 = new JButton();
    p5 = new JButton();
    p6 = new JButton();
    p7 = new JButton();
    p8 = new JButton();
    p9 = new JButton();
    p10 = new JButton();
    p11 = new JButton();
    p12 = new JButton();
    p13 = new JButton();
    p14 = new JButton();
    p15 = new JButton();
    BTD = new JPopupMenu();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    BTD2 = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_26 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_34 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ordres de fabrication @LIBPG@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_112 ----
          OBJ_112.setText("Etablissement");
          OBJ_112.setName("OBJ_112");
          p_tete_gauche.add(OBJ_112);
          OBJ_112.setBounds(10, 5, 93, 18);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(210, 0, 60, INDNUM.getPreferredSize().height);

          //---- OBJ_113 ----
          OBJ_113.setText("Num\u00e9ro");
          OBJ_113.setName("OBJ_113");
          p_tete_gauche.add(OBJ_113);
          OBJ_113.setBounds(150, 5, 51, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(105, 0, 40, INDETB.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(270, 0, 20, INDSUF.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour ent\u00eate");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Autres vues");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Fin d'ordre de fabrication");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Affichage de l'OF");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie pleine page");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");

              //---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Chemin critique");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Fonctions");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Saisie des temps");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Consommations mati\u00e8res");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Ordres de fabrication"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_120 ----
            OBJ_120.setText("Article compos\u00e9");
            OBJ_120.setName("OBJ_120");
            panel1.add(OBJ_120);
            OBJ_120.setBounds(60, 45, 105, 22);

            //---- WART ----
            WART.setText("@WART@");
            WART.setName("WART");
            panel1.add(WART);
            WART.setBounds(180, 44, 249, WART.getPreferredSize().height);

            //---- WLIB ----
            WLIB.setText("@WLIB@");
            WLIB.setName("WLIB");
            panel1.add(WLIB);
            WLIB.setBounds(440, 44, 260, WLIB.getPreferredSize().height);

            //---- WQTEB ----
            WQTEB.setText("@WQTEB@");
            WQTEB.setName("WQTEB");
            panel1.add(WQTEB);
            WQTEB.setBounds(180, 78, 123, WQTEB.getPreferredSize().height);

            //---- OBJ_122 ----
            OBJ_122.setText("Quantit\u00e9 \u00e0 fabriquer");
            OBJ_122.setName("OBJ_122");
            panel1.add(OBJ_122);
            OBJ_122.setBounds(60, 80, 110, 20);

            //======== SCROLLPANE_LIST2 ========
            {
              SCROLLPANE_LIST2.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST2.setName("SCROLLPANE_LIST2");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD2);
              WTP01.setRowHeight(18);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST2.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST2);
            SCROLLPANE_LIST2.setBounds(60, 120, 605, 300);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(675, 120, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(675, 295, 25, 125);

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- p1 ----
              p1.setText("");
              p1.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p1.setBorder(null);
              p1.setName("p1");
              p1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_74ActionPerformed(e);
                }
              });
              panel3.add(p1);
              p1.setBounds(20, 10, 13, 13);

              //---- p2 ----
              p2.setText("");
              p2.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p2.setName("p2");
              p2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_77ActionPerformed(e);
                }
              });
              panel3.add(p2);
              p2.setBounds(20, 28, 13, 13);

              //---- p3 ----
              p3.setText("");
              p3.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p3.setName("p3");
              p3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_78ActionPerformed(e);
                }
              });
              panel3.add(p3);
              p3.setBounds(20, 46, 13, 13);

              //---- p4 ----
              p4.setText("");
              p4.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p4.setName("p4");
              p4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_79ActionPerformed(e);
                }
              });
              panel3.add(p4);
              p4.setBounds(20, 64, 13, 13);

              //---- p5 ----
              p5.setText("");
              p5.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p5.setName("p5");
              p5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_81ActionPerformed(e);
                }
              });
              panel3.add(p5);
              p5.setBounds(20, 82, 13, 13);

              //---- p6 ----
              p6.setText("");
              p6.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p6.setName("p6");
              p6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_82ActionPerformed(e);
                }
              });
              panel3.add(p6);
              p6.setBounds(20, 100, 13, 13);

              //---- p7 ----
              p7.setText("");
              p7.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p7.setName("p7");
              p7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_83ActionPerformed(e);
                }
              });
              panel3.add(p7);
              p7.setBounds(20, 118, 13, 13);

              //---- p8 ----
              p8.setText("");
              p8.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p8.setName("p8");
              p8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_84ActionPerformed(e);
                }
              });
              panel3.add(p8);
              p8.setBounds(20, 136, 13, 13);

              //---- p9 ----
              p9.setText("");
              p9.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p9.setName("p9");
              p9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_85ActionPerformed(e);
                }
              });
              panel3.add(p9);
              p9.setBounds(20, 154, 13, 13);

              //---- p10 ----
              p10.setText("");
              p10.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p10.setName("p10");
              p10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_86ActionPerformed(e);
                }
              });
              panel3.add(p10);
              p10.setBounds(20, 172, 13, 13);

              //---- p11 ----
              p11.setText("");
              p11.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p11.setName("p11");
              p11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_88ActionPerformed(e);
                }
              });
              panel3.add(p11);
              p11.setBounds(20, 190, 13, 13);

              //---- p12 ----
              p12.setText("");
              p12.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p12.setName("p12");
              p12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_89ActionPerformed(e);
                }
              });
              panel3.add(p12);
              p12.setBounds(20, 208, 13, 13);

              //---- p13 ----
              p13.setText("");
              p13.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p13.setName("p13");
              p13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_90ActionPerformed(e);
                }
              });
              panel3.add(p13);
              p13.setBounds(20, 226, 13, 13);

              //---- p14 ----
              p14.setText("");
              p14.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p14.setName("p14");
              p14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_92ActionPerformed(e);
                }
              });
              panel3.add(p14);
              p14.setBounds(20, 244, 13, 13);

              //---- p15 ----
              p15.setText("");
              p15.setToolTipText("<HTML>Des informations techniques<BR>existent pour cette op\u00e9ration</HTML>");
              p15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              p15.setName("p15");
              p15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  OBJ_93ActionPerformed(e);
                }
              });
              panel3.add(p15);
              p15.setBounds(20, 262, 13, 13);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel3);
            panel3.setBounds(20, 135, 35, 285);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 734, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 452, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_22 ----
      OBJ_22.setText("Aide en ligne");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Choix possibles");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD2.add(CHOISIR);

      //---- OBJ_26 ----
      OBJ_26.setText("Modifier");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_26);

      //---- OBJ_27 ----
      OBJ_27.setText("Annuler");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("Interroger");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Chiffrage");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("R\u00e9alis\u00e9");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("Options article");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_31);

      //---- OBJ_32 ----
      OBJ_32.setText("D\u00e9tail lots");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_32);

      //---- OBJ_33 ----
      OBJ_33.setText("Affectation d'adresse de stock");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_33);
      BTD2.addSeparator();

      //---- OBJ_35 ----
      OBJ_35.setText("Choix possibles");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_35);

      //---- OBJ_34 ----
      OBJ_34.setText("Aide en ligne");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_34);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_112;
  private XRiTextField INDNUM;
  private JLabel OBJ_113;
  private XRiTextField INDETB;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_120;
  private RiZoneSortie WART;
  private RiZoneSortie WLIB;
  private RiZoneSortie WQTEB;
  private JLabel OBJ_122;
  private JScrollPane SCROLLPANE_LIST2;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel3;
  private JButton p1;
  private JButton p2;
  private JButton p3;
  private JButton p4;
  private JButton p5;
  private JButton p6;
  private JButton p7;
  private JButton p8;
  private JButton p9;
  private JButton p10;
  private JButton p11;
  private JButton p12;
  private JButton p13;
  private JButton p14;
  private JButton p15;
  private JPopupMenu BTD;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JPopupMenu BTD2;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_26;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_34;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
