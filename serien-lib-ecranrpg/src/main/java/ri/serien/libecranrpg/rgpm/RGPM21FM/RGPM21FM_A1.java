
package ri.serien.libecranrpg.rgpm.RGPM21FM;
// Nom Fichier: b_RGPM21FM_FMTA1_FMTF1_8.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGPM21FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] ARG5_Value = { "", "0", "2", "4", "6", };
  
  public RGPM21FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX5.setValeurs("5", "RB");
    TIDX1.setValeurs("1", "RB");
    TIDX4.setValeurs("4", "RB");
    TIDX3.setValeurs("3", "RB");
    TIDX2.setValeurs("2", "RB");
    TIDX7.setValeurs("7", "RB");
    TIDX8.setValeurs("8", "RB");
    TIDX6.setValeurs("6", "RB");
    ARG5.setValeurs(ARG5_Value, null);
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    TIDX2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC1@")).trim());
    TIDX3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LMC2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    barre_dup.setVisible(lexique.getMode() == Lexical.MODE_DUPLICATION);
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Ordres de fabrication"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_41 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_43 = new JLabel();
    INDNUM = new XRiTextField();
    INDSUF = new XRiTextField();
    p_tete_droite = new JPanel();
    barre_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_42 = new JLabel();
    IN3ETB = new XRiTextField();
    OBJ_44 = new JLabel();
    IN3NUM = new XRiTextField();
    IN3SUF = new XRiTextField();
    p_tete_droite2 = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARG5 = new XRiComboBox();
    TIDX6 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    ARG2 = new XRiTextField();
    ARG3 = new XRiTextField();
    ARG4 = new XRiTextField();
    TIDX2 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX1 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    ARG6 = new XRiCalendrier();
    ARG6F = new XRiCalendrier();
    ARG7 = new XRiCalendrier();
    ARG7F = new XRiCalendrier();
    ARG8 = new XRiCalendrier();
    ARG8F = new XRiCalendrier();
    ARG1 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_19 = new JMenuItem();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Ordres de fabrication");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 15));
          p_tete_gauche.setMinimumSize(new Dimension(700, 15));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_41 ----
          OBJ_41.setText("Etablissement");
          OBJ_41.setName("OBJ_41");

          //---- INDETB ----
          INDETB.setName("INDETB");

          //---- OBJ_43 ----
          OBJ_43.setText("Num\u00e9ro d'OF");
          OBJ_43.setName("OBJ_43");

          //---- INDNUM ----
          INDNUM.setName("INDNUM");

          //---- INDSUF ----
          INDSUF.setName("INDSUF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 93, GroupLayout.PREFERRED_SIZE)
                .addGap(22, 22, 22)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);

      //======== barre_dup ========
      {
        barre_dup.setMinimumSize(new Dimension(111, 34));
        barre_dup.setPreferredSize(new Dimension(111, 34));
        barre_dup.setName("barre_dup");

        //======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 15));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 15));
          p_tete_gauche2.setName("p_tete_gauche2");

          //---- OBJ_42 ----
          OBJ_42.setText("Par duplication de");
          OBJ_42.setName("OBJ_42");

          //---- IN3ETB ----
          IN3ETB.setName("IN3ETB");

          //---- OBJ_44 ----
          OBJ_44.setText("Num\u00e9ro d'OF");
          OBJ_44.setName("OBJ_44");

          //---- IN3NUM ----
          IN3NUM.setName("IN3NUM");

          //---- IN3SUF ----
          IN3SUF.setName("IN3SUF");

          GroupLayout p_tete_gauche2Layout = new GroupLayout(p_tete_gauche2);
          p_tete_gauche2.setLayout(p_tete_gauche2Layout);
          p_tete_gauche2Layout.setHorizontalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40)
                .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(IN3NUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(IN3SUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gauche2Layout.setVerticalGroup(
            p_tete_gauche2Layout.createParallelGroup()
              .addComponent(IN3ETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(IN3NUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(IN3SUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gauche2Layout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gauche2Layout.createParallelGroup()
                  .addComponent(OBJ_42, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_dup.add(p_tete_gauche2);

        //======== p_tete_droite2 ========
        {
          p_tete_droite2.setOpaque(false);
          p_tete_droite2.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite2.setPreferredSize(new Dimension(150, 0));
          p_tete_droite2.setMinimumSize(new Dimension(150, 0));
          p_tete_droite2.setName("p_tete_droite2");
          p_tete_droite2.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_dup.add(p_tete_droite2);
      }
      p_nord.add(barre_dup);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche multi-crit\u00e8res"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ARG5 ----
            ARG5.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "En attente",
              "R\u00e9serv\u00e9",
              "En fabrication",
              "En stock"
            }));
            ARG5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG5.setName("ARG5");
            panel1.add(ARG5);
            ARG5.setBounds(265, 152, 135, ARG5.getPreferredSize().height);

            //---- TIDX6 ----
            TIDX6.setText("Date de d\u00e9but de production du");
            TIDX6.setToolTipText("Tri\u00e9 par");
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(55, 225, 205, 20);

            //---- TIDX8 ----
            TIDX8.setText("Date de fin de production du");
            TIDX8.setToolTipText("Tri\u00e9 par");
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(55, 295, 205, 20);

            //---- TIDX7 ----
            TIDX7.setText("Date de fin souhait\u00e9e du");
            TIDX7.setToolTipText("Tri\u00e9 par");
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(55, 259, 205, 20);

            //---- ARG2 ----
            ARG2.setName("ARG2");
            panel1.add(ARG2);
            ARG2.setBounds(265, 51, 210, ARG2.getPreferredSize().height);

            //---- ARG3 ----
            ARG3.setName("ARG3");
            panel1.add(ARG3);
            ARG3.setBounds(265, 86, 160, ARG3.getPreferredSize().height);

            //---- ARG4 ----
            ARG4.setName("ARG4");
            panel1.add(ARG4);
            ARG4.setBounds(265, 121, 210, ARG4.getPreferredSize().height);

            //---- TIDX2 ----
            TIDX2.setText("@LMC1@");
            TIDX2.setToolTipText("Tri\u00e9 par");
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(55, 55, 205, 20);

            //---- TIDX3 ----
            TIDX3.setText("@LMC2@");
            TIDX3.setToolTipText("Tri\u00e9 par");
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(55, 90, 205, 20);

            //---- TIDX4 ----
            TIDX4.setText("Nomenclature");
            TIDX4.setToolTipText("Tri\u00e9 par");
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(55, 125, 205, 20);

            //---- TIDX1 ----
            TIDX1.setText("Num\u00e9ro d'OF");
            TIDX1.setToolTipText("Tri\u00e9 par");
            TIDX1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX1.setName("TIDX1");
            panel1.add(TIDX1);
            TIDX1.setBounds(55, 190, 205, 20);

            //---- TIDX5 ----
            TIDX5.setText("Etat");
            TIDX5.setToolTipText("Tri\u00e9 par");
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(55, 155, 205, 20);

            //---- ARG6 ----
            ARG6.setName("ARG6");
            panel1.add(ARG6);
            ARG6.setBounds(265, 221, 105, ARG6.getPreferredSize().height);

            //---- ARG6F ----
            ARG6F.setName("ARG6F");
            panel1.add(ARG6F);
            ARG6F.setBounds(415, 221, 105, ARG6F.getPreferredSize().height);

            //---- ARG7 ----
            ARG7.setName("ARG7");
            panel1.add(ARG7);
            ARG7.setBounds(265, 255, 105, ARG7.getPreferredSize().height);

            //---- ARG7F ----
            ARG7F.setName("ARG7F");
            panel1.add(ARG7F);
            ARG7F.setBounds(415, 255, 105, ARG7F.getPreferredSize().height);

            //---- ARG8 ----
            ARG8.setName("ARG8");
            panel1.add(ARG8);
            ARG8.setBounds(265, 291, 105, ARG8.getPreferredSize().height);

            //---- ARG8F ----
            ARG8F.setName("ARG8F");
            panel1.add(ARG8F);
            ARG8F.setBounds(415, 291, 105, ARG8F.getPreferredSize().height);

            //---- ARG1 ----
            ARG1.setName("ARG1");
            panel1.add(ARG1);
            ARG1.setBounds(265, 186, 58, ARG1.getPreferredSize().height);

            //---- label1 ----
            label1.setText("au");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(375, 227, 35, label1.getPreferredSize().height);

            //---- label2 ----
            label2.setText("au");
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(375, 261, 35, 16);

            //---- label3 ----
            label3.setText("au");
            label3.setHorizontalAlignment(SwingConstants.CENTER);
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(375, 297, 35, 16);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 574, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 352, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_19 ----
      OBJ_19.setText("Choix possibles");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_18 ----
      OBJ_18.setText("Aide en ligne");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- RB_GRP ----
    ButtonGroup RB_GRP = new ButtonGroup();
    RB_GRP.add(TIDX6);
    RB_GRP.add(TIDX8);
    RB_GRP.add(TIDX7);
    RB_GRP.add(TIDX2);
    RB_GRP.add(TIDX3);
    RB_GRP.add(TIDX4);
    RB_GRP.add(TIDX1);
    RB_GRP.add(TIDX5);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_41;
  private XRiTextField INDETB;
  private JLabel OBJ_43;
  private XRiTextField INDNUM;
  private XRiTextField INDSUF;
  private JPanel p_tete_droite;
  private JMenuBar barre_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_42;
  private XRiTextField IN3ETB;
  private JLabel OBJ_44;
  private XRiTextField IN3NUM;
  private XRiTextField IN3SUF;
  private JPanel p_tete_droite2;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox ARG5;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG2;
  private XRiTextField ARG3;
  private XRiTextField ARG4;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX1;
  private XRiRadioButton TIDX5;
  private XRiCalendrier ARG6;
  private XRiCalendrier ARG6F;
  private XRiCalendrier ARG7;
  private XRiCalendrier ARG7F;
  private XRiCalendrier ARG8;
  private XRiCalendrier ARG8F;
  private XRiTextField ARG1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
