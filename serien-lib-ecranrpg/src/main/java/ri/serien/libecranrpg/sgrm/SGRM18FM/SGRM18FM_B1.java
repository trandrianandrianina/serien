
package ri.serien.libecranrpg.sgrm.SGRM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SGRM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SGRM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_6 = new JPanel();
    OBJ_7 = new JLabel();
    OBJ_13 = new JLabel();
    CNC02 = new XRiTextField();
    CNC01 = new XRiTextField();
    ETB02 = new XRiTextField();
    ETB01 = new XRiTextField();
    OBJ_12 = new JLabel();
    OBJ_11 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(585, 175));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_6 ========
        {
          OBJ_6.setBorder(new TitledBorder(""));
          OBJ_6.setOpaque(false);
          OBJ_6.setName("OBJ_6");
          OBJ_6.setLayout(null);

          //---- OBJ_7 ----
          OBJ_7.setText("Condition de commissionnement");
          OBJ_7.setName("OBJ_7");
          OBJ_6.add(OBJ_7);
          OBJ_7.setBounds(15, 40, 198, 18);

          //---- OBJ_13 ----
          OBJ_13.setText("Par duplication de");
          OBJ_13.setName("OBJ_13");
          OBJ_6.add(OBJ_13);
          OBJ_13.setBounds(15, 75, 111, 18);

          //---- CNC02 ----
          CNC02.setName("CNC02");
          OBJ_6.add(CNC02);
          CNC02.setBounds(289, 35, 64, CNC02.getPreferredSize().height);

          //---- CNC01 ----
          CNC01.setName("CNC01");
          OBJ_6.add(CNC01);
          CNC01.setBounds(289, 70, 64, CNC01.getPreferredSize().height);

          //---- ETB02 ----
          ETB02.setName("ETB02");
          OBJ_6.add(ETB02);
          ETB02.setBounds(237, 35, 44, ETB02.getPreferredSize().height);

          //---- ETB01 ----
          ETB01.setName("ETB01");
          OBJ_6.add(ETB01);
          ETB01.setBounds(237, 70, 44, ETB01.getPreferredSize().height);

          //---- OBJ_12 ----
          OBJ_12.setText("Code");
          OBJ_12.setName("OBJ_12");
          OBJ_6.add(OBJ_12);
          OBJ_12.setBounds(299, 12, 46, 18);

          //---- OBJ_11 ----
          OBJ_11.setText("Etb");
          OBJ_11.setName("OBJ_11");
          OBJ_6.add(OBJ_11);
          OBJ_11.setBounds(244, 12, 24, 18);
        }
        p_contenu.add(OBJ_6);
        OBJ_6.setBounds(20, 25, 374, 128);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_6;
  private JLabel OBJ_7;
  private JLabel OBJ_13;
  private XRiTextField CNC02;
  private XRiTextField CNC01;
  private XRiTextField ETB02;
  private XRiTextField ETB01;
  private JLabel OBJ_12;
  private JLabel OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
