
package ri.serien.libecranrpg.stim.STIM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STIM40FM_B4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STIM40FM_B4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    CTS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS001@")).trim());
    CTN001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN001@")).trim());
    CTS002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS002@")).trim());
    CTN002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN002@")).trim());
    CTS003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS003@")).trim());
    CTN003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN003@")).trim());
    CTS004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS004@")).trim());
    CTN004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN004@")).trim());
    CTS005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS005@")).trim());
    CTN005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN005@")).trim());
    CTS006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS006@")).trim());
    CTN006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN006@")).trim());
    CTS007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS007@")).trim());
    CTN007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN007@")).trim());
    CTS008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS008@")).trim());
    CTN008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN008@")).trim());
    CTS009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS009@")).trim());
    CTN009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN009@")).trim());
    CTS010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS010@")).trim());
    CTN010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN010@")).trim());
    CTS011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS011@")).trim());
    CTN011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN011@")).trim());
    CTS012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS012@")).trim());
    CTN012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN012@")).trim());
    CTS013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS013@")).trim());
    CTN013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN013@")).trim());
    CTS014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS014@")).trim());
    CTN014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN014@")).trim());
    CTS015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS015@")).trim());
    CTN015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN015@")).trim());
    CTS016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS016@")).trim());
    CTN016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTN016@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panelEtb = new JPanel();
    CTS001 = new RiZoneSortie();
    CTN001 = new RiZoneSortie();
    CTS002 = new RiZoneSortie();
    CTN002 = new RiZoneSortie();
    CTS003 = new RiZoneSortie();
    CTN003 = new RiZoneSortie();
    CTS004 = new RiZoneSortie();
    CTN004 = new RiZoneSortie();
    CTS005 = new RiZoneSortie();
    CTN005 = new RiZoneSortie();
    label2 = new JLabel();
    CTS006 = new RiZoneSortie();
    CTN006 = new RiZoneSortie();
    CTS007 = new RiZoneSortie();
    CTN007 = new RiZoneSortie();
    CTS008 = new RiZoneSortie();
    CTN008 = new RiZoneSortie();
    CTS009 = new RiZoneSortie();
    CTN009 = new RiZoneSortie();
    CTS010 = new RiZoneSortie();
    CTN010 = new RiZoneSortie();
    CTS011 = new RiZoneSortie();
    CTN011 = new RiZoneSortie();
    CTS012 = new RiZoneSortie();
    CTN012 = new RiZoneSortie();
    CTS013 = new RiZoneSortie();
    CTN013 = new RiZoneSortie();
    CTS014 = new RiZoneSortie();
    CTN014 = new RiZoneSortie();
    CTS015 = new RiZoneSortie();
    CTN015 = new RiZoneSortie();
    CTS016 = new RiZoneSortie();
    CTN016 = new RiZoneSortie();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(450, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setBorder(new TitledBorder(""));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panelEtb ========
            {
              panelEtb.setOpaque(false);
              panelEtb.setName("panelEtb");
              panelEtb.setLayout(null);

              //---- CTS001 ----
              CTS001.setText("@CTS001@");
              CTS001.setName("CTS001");
              panelEtb.add(CTS001);
              CTS001.setBounds(35, 20, 40, CTS001.getPreferredSize().height);

              //---- CTN001 ----
              CTN001.setText("@CTN001@");
              CTN001.setName("CTN001");
              panelEtb.add(CTN001);
              CTN001.setBounds(80, 20, 260, CTN001.getPreferredSize().height);

              //---- CTS002 ----
              CTS002.setText("@CTS002@");
              CTS002.setName("CTS002");
              panelEtb.add(CTS002);
              CTS002.setBounds(35, 45, 40, CTS002.getPreferredSize().height);

              //---- CTN002 ----
              CTN002.setText("@CTN002@");
              CTN002.setName("CTN002");
              panelEtb.add(CTN002);
              CTN002.setBounds(80, 45, 260, CTN002.getPreferredSize().height);

              //---- CTS003 ----
              CTS003.setText("@CTS003@");
              CTS003.setName("CTS003");
              panelEtb.add(CTS003);
              CTS003.setBounds(35, 70, 40, CTS003.getPreferredSize().height);

              //---- CTN003 ----
              CTN003.setText("@CTN003@");
              CTN003.setName("CTN003");
              panelEtb.add(CTN003);
              CTN003.setBounds(80, 70, 260, CTN003.getPreferredSize().height);

              //---- CTS004 ----
              CTS004.setText("@CTS004@");
              CTS004.setName("CTS004");
              panelEtb.add(CTS004);
              CTS004.setBounds(35, 95, 40, CTS004.getPreferredSize().height);

              //---- CTN004 ----
              CTN004.setText("@CTN004@");
              CTN004.setName("CTN004");
              panelEtb.add(CTN004);
              CTN004.setBounds(80, 95, 260, CTN004.getPreferredSize().height);

              //---- CTS005 ----
              CTS005.setText("@CTS005@");
              CTS005.setName("CTS005");
              panelEtb.add(CTS005);
              CTS005.setBounds(35, 120, 40, CTS005.getPreferredSize().height);

              //---- CTN005 ----
              CTN005.setText("@CTN005@");
              CTN005.setName("CTN005");
              panelEtb.add(CTN005);
              CTN005.setBounds(80, 120, 260, CTN005.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Etb.");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panelEtb.add(label2);
              label2.setBounds(35, 0, 40, 20);

              //---- CTS006 ----
              CTS006.setText("@CTS006@");
              CTS006.setName("CTS006");
              panelEtb.add(CTS006);
              CTS006.setBounds(35, 145, 40, CTS006.getPreferredSize().height);

              //---- CTN006 ----
              CTN006.setText("@CTN006@");
              CTN006.setName("CTN006");
              panelEtb.add(CTN006);
              CTN006.setBounds(80, 145, 260, CTN006.getPreferredSize().height);

              //---- CTS007 ----
              CTS007.setText("@CTS007@");
              CTS007.setName("CTS007");
              panelEtb.add(CTS007);
              CTS007.setBounds(35, 170, 40, CTS007.getPreferredSize().height);

              //---- CTN007 ----
              CTN007.setText("@CTN007@");
              CTN007.setName("CTN007");
              panelEtb.add(CTN007);
              CTN007.setBounds(80, 170, 260, CTN007.getPreferredSize().height);

              //---- CTS008 ----
              CTS008.setText("@CTS008@");
              CTS008.setName("CTS008");
              panelEtb.add(CTS008);
              CTS008.setBounds(35, 195, 40, CTS008.getPreferredSize().height);

              //---- CTN008 ----
              CTN008.setText("@CTN008@");
              CTN008.setName("CTN008");
              panelEtb.add(CTN008);
              CTN008.setBounds(80, 195, 260, CTN008.getPreferredSize().height);

              //---- CTS009 ----
              CTS009.setText("@CTS009@");
              CTS009.setName("CTS009");
              panelEtb.add(CTS009);
              CTS009.setBounds(35, 220, 40, CTS009.getPreferredSize().height);

              //---- CTN009 ----
              CTN009.setText("@CTN009@");
              CTN009.setName("CTN009");
              panelEtb.add(CTN009);
              CTN009.setBounds(80, 220, 260, CTN009.getPreferredSize().height);

              //---- CTS010 ----
              CTS010.setText("@CTS010@");
              CTS010.setName("CTS010");
              panelEtb.add(CTS010);
              CTS010.setBounds(35, 245, 40, CTS010.getPreferredSize().height);

              //---- CTN010 ----
              CTN010.setText("@CTN010@");
              CTN010.setName("CTN010");
              panelEtb.add(CTN010);
              CTN010.setBounds(80, 245, 260, CTN010.getPreferredSize().height);

              //---- CTS011 ----
              CTS011.setText("@CTS011@");
              CTS011.setName("CTS011");
              panelEtb.add(CTS011);
              CTS011.setBounds(35, 270, 40, CTS011.getPreferredSize().height);

              //---- CTN011 ----
              CTN011.setText("@CTN011@");
              CTN011.setName("CTN011");
              panelEtb.add(CTN011);
              CTN011.setBounds(80, 270, 260, CTN011.getPreferredSize().height);

              //---- CTS012 ----
              CTS012.setText("@CTS012@");
              CTS012.setName("CTS012");
              panelEtb.add(CTS012);
              CTS012.setBounds(35, 295, 40, CTS012.getPreferredSize().height);

              //---- CTN012 ----
              CTN012.setText("@CTN012@");
              CTN012.setName("CTN012");
              panelEtb.add(CTN012);
              CTN012.setBounds(80, 295, 260, CTN012.getPreferredSize().height);

              //---- CTS013 ----
              CTS013.setText("@CTS013@");
              CTS013.setName("CTS013");
              panelEtb.add(CTS013);
              CTS013.setBounds(35, 320, 40, CTS013.getPreferredSize().height);

              //---- CTN013 ----
              CTN013.setText("@CTN013@");
              CTN013.setName("CTN013");
              panelEtb.add(CTN013);
              CTN013.setBounds(80, 320, 260, CTN013.getPreferredSize().height);

              //---- CTS014 ----
              CTS014.setText("@CTS014@");
              CTS014.setName("CTS014");
              panelEtb.add(CTS014);
              CTS014.setBounds(35, 345, 40, CTS014.getPreferredSize().height);

              //---- CTN014 ----
              CTN014.setText("@CTN014@");
              CTN014.setName("CTN014");
              panelEtb.add(CTN014);
              CTN014.setBounds(80, 345, 260, CTN014.getPreferredSize().height);

              //---- CTS015 ----
              CTS015.setText("@CTS015@");
              CTS015.setName("CTS015");
              panelEtb.add(CTS015);
              CTS015.setBounds(35, 370, 40, CTS015.getPreferredSize().height);

              //---- CTN015 ----
              CTN015.setText("@CTN015@");
              CTN015.setName("CTN015");
              panelEtb.add(CTN015);
              CTN015.setBounds(80, 370, 260, CTN015.getPreferredSize().height);

              //---- CTS016 ----
              CTS016.setText("@CTS016@");
              CTS016.setName("CTS016");
              panelEtb.add(CTS016);
              CTS016.setBounds(35, 395, 40, CTS016.getPreferredSize().height);

              //---- CTN016 ----
              CTN016.setText("@CTN016@");
              CTN016.setName("CTN016");
              panelEtb.add(CTN016);
              CTN016.setBounds(80, 395, 260, CTN016.getPreferredSize().height);

              //---- label3 ----
              label3.setText("Identification");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setName("label3");
              panelEtb.add(label3);
              label3.setBounds(80, 0, 260, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panelEtb.getComponentCount(); i++) {
                  Rectangle bounds = panelEtb.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelEtb.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelEtb.setMinimumSize(preferredSize);
                panelEtb.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panelEtb);
            panelEtb.setBounds(30, 20, 375, 430);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panelEtb;
  private RiZoneSortie CTS001;
  private RiZoneSortie CTN001;
  private RiZoneSortie CTS002;
  private RiZoneSortie CTN002;
  private RiZoneSortie CTS003;
  private RiZoneSortie CTN003;
  private RiZoneSortie CTS004;
  private RiZoneSortie CTN004;
  private RiZoneSortie CTS005;
  private RiZoneSortie CTN005;
  private JLabel label2;
  private RiZoneSortie CTS006;
  private RiZoneSortie CTN006;
  private RiZoneSortie CTS007;
  private RiZoneSortie CTN007;
  private RiZoneSortie CTS008;
  private RiZoneSortie CTN008;
  private RiZoneSortie CTS009;
  private RiZoneSortie CTN009;
  private RiZoneSortie CTS010;
  private RiZoneSortie CTN010;
  private RiZoneSortie CTS011;
  private RiZoneSortie CTN011;
  private RiZoneSortie CTS012;
  private RiZoneSortie CTN012;
  private RiZoneSortie CTS013;
  private RiZoneSortie CTN013;
  private RiZoneSortie CTS014;
  private RiZoneSortie CTN014;
  private RiZoneSortie CTS015;
  private RiZoneSortie CTN015;
  private RiZoneSortie CTS016;
  private RiZoneSortie CTN016;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
