
package ri.serien.libecranrpg.stim.STIM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STIM40FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STIM40FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    EDTHLD.setValeursSelection("OUI", "NON");
    EDTERR.setValeursSelection("OUI", "NON");
    WTETB.setValeursSelection("**", "");
    ETT001.setValeursSelection("1", "");
    ETT002.setValeursSelection("1", "");
    ETT003.setValeursSelection("1", "");
    ETT004.setValeursSelection("1", "");
    ETT005.setValeursSelection("1", "");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    WNUPAS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNUPAS@")).trim());
    ETS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS001@")).trim());
    ETN001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN001@")).trim());
    ETE001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE001@")).trim());
    ETS002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS002@")).trim());
    ETN002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN002@")).trim());
    ETE002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE002@")).trim());
    ETS003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS003@")).trim());
    ETN003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN003@")).trim());
    ETE003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE003@")).trim());
    ETS004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS004@")).trim());
    ETN004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN004@")).trim());
    ETE004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE004@")).trim());
    ETS005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETS005@")).trim());
    ETN005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETN005@")).trim());
    ETE005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETE005@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    panelEtb.setVisible(!WTETB.isSelected());
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void WTETBActionPerformed(ActionEvent e) {
    panelEtb.setVisible(!panelEtb.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WNUPAS = new RiZoneSortie();
    label1 = new JLabel();
    EDTHLD = new XRiCheckBox();
    EDTERR = new XRiCheckBox();
    WTETB = new XRiCheckBox();
    separator1 = compFactory.createSeparator("Choix de l'\u00e9tablissement \u00e0 traiter");
    panelEtb = new JPanel();
    ETT001 = new XRiCheckBox();
    ETS001 = new RiZoneSortie();
    ETN001 = new RiZoneSortie();
    ETE001 = new RiZoneSortie();
    ETT002 = new XRiCheckBox();
    ETS002 = new RiZoneSortie();
    ETN002 = new RiZoneSortie();
    ETE002 = new RiZoneSortie();
    ETT003 = new XRiCheckBox();
    ETS003 = new RiZoneSortie();
    ETN003 = new RiZoneSortie();
    ETE003 = new RiZoneSortie();
    ETT004 = new XRiCheckBox();
    ETS004 = new RiZoneSortie();
    ETN004 = new RiZoneSortie();
    ETE004 = new RiZoneSortie();
    ETT005 = new XRiCheckBox();
    ETS005 = new RiZoneSortie();
    ETN005 = new RiZoneSortie();
    ETE005 = new RiZoneSortie();
    label2 = new JLabel();
    label3 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(590, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setBorder(new TitledBorder(""));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WNUPAS ----
            WNUPAS.setText("@WNUPAS@");
            WNUPAS.setName("WNUPAS");
            panel1.add(WNUPAS);
            WNUPAS.setBounds(405, 40, 40, WNUPAS.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Dernier num\u00e9ro de passage en comptabilit\u00e9");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(40, 40, 290, 24);

            //---- EDTHLD ----
            EDTHLD.setText("Suspension de l'\u00e9dition");
            EDTHLD.setName("EDTHLD");
            panel1.add(EDTHLD);
            EDTHLD.setBounds(40, 80, 290, 24);

            //---- EDTERR ----
            EDTERR.setText("Editer uniquement les erreurs");
            EDTERR.setName("EDTERR");
            panel1.add(EDTERR);
            EDTERR.setBounds(40, 115, 290, 24);

            //---- WTETB ----
            WTETB.setText("Tous les \u00e9tablissements");
            WTETB.setName("WTETB");
            WTETB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTETBActionPerformed(e);
              }
            });
            panel1.add(WTETB);
            WTETB.setBounds(40, 190, 290, 24);

            //---- separator1 ----
            separator1.setName("separator1");
            panel1.add(separator1);
            separator1.setBounds(30, 160, 520, separator1.getPreferredSize().height);

            //======== panelEtb ========
            {
              panelEtb.setOpaque(false);
              panelEtb.setName("panelEtb");
              panelEtb.setLayout(null);

              //---- ETT001 ----
              ETT001.setName("ETT001");
              panelEtb.add(ETT001);
              ETT001.setBounds(5, 20, 30, 24);

              //---- ETS001 ----
              ETS001.setText("@ETS001@");
              ETS001.setName("ETS001");
              panelEtb.add(ETS001);
              ETS001.setBounds(35, 20, 40, ETS001.getPreferredSize().height);

              //---- ETN001 ----
              ETN001.setText("@ETN001@");
              ETN001.setName("ETN001");
              panelEtb.add(ETN001);
              ETN001.setBounds(80, 20, 260, ETN001.getPreferredSize().height);

              //---- ETE001 ----
              ETE001.setText("@ETE001@");
              ETE001.setName("ETE001");
              panelEtb.add(ETE001);
              ETE001.setBounds(345, 20, 160, ETE001.getPreferredSize().height);

              //---- ETT002 ----
              ETT002.setName("ETT002");
              panelEtb.add(ETT002);
              ETT002.setBounds(5, 45, 30, 24);

              //---- ETS002 ----
              ETS002.setText("@ETS002@");
              ETS002.setName("ETS002");
              panelEtb.add(ETS002);
              ETS002.setBounds(35, 45, 40, ETS002.getPreferredSize().height);

              //---- ETN002 ----
              ETN002.setText("@ETN002@");
              ETN002.setName("ETN002");
              panelEtb.add(ETN002);
              ETN002.setBounds(80, 45, 260, ETN002.getPreferredSize().height);

              //---- ETE002 ----
              ETE002.setText("@ETE002@");
              ETE002.setName("ETE002");
              panelEtb.add(ETE002);
              ETE002.setBounds(345, 45, 160, ETE002.getPreferredSize().height);

              //---- ETT003 ----
              ETT003.setName("ETT003");
              panelEtb.add(ETT003);
              ETT003.setBounds(5, 70, 30, 24);

              //---- ETS003 ----
              ETS003.setText("@ETS003@");
              ETS003.setName("ETS003");
              panelEtb.add(ETS003);
              ETS003.setBounds(35, 70, 40, ETS003.getPreferredSize().height);

              //---- ETN003 ----
              ETN003.setText("@ETN003@");
              ETN003.setName("ETN003");
              panelEtb.add(ETN003);
              ETN003.setBounds(80, 70, 260, ETN003.getPreferredSize().height);

              //---- ETE003 ----
              ETE003.setText("@ETE003@");
              ETE003.setName("ETE003");
              panelEtb.add(ETE003);
              ETE003.setBounds(345, 70, 160, ETE003.getPreferredSize().height);

              //---- ETT004 ----
              ETT004.setName("ETT004");
              panelEtb.add(ETT004);
              ETT004.setBounds(5, 95, 30, 24);

              //---- ETS004 ----
              ETS004.setText("@ETS004@");
              ETS004.setName("ETS004");
              panelEtb.add(ETS004);
              ETS004.setBounds(35, 95, 40, ETS004.getPreferredSize().height);

              //---- ETN004 ----
              ETN004.setText("@ETN004@");
              ETN004.setName("ETN004");
              panelEtb.add(ETN004);
              ETN004.setBounds(80, 95, 260, ETN004.getPreferredSize().height);

              //---- ETE004 ----
              ETE004.setText("@ETE004@");
              ETE004.setName("ETE004");
              panelEtb.add(ETE004);
              ETE004.setBounds(345, 95, 160, ETE004.getPreferredSize().height);

              //---- ETT005 ----
              ETT005.setName("ETT005");
              panelEtb.add(ETT005);
              ETT005.setBounds(5, 120, 30, 24);

              //---- ETS005 ----
              ETS005.setText("@ETS005@");
              ETS005.setName("ETS005");
              panelEtb.add(ETS005);
              ETS005.setBounds(35, 120, 40, ETS005.getPreferredSize().height);

              //---- ETN005 ----
              ETN005.setText("@ETN005@");
              ETN005.setName("ETN005");
              panelEtb.add(ETN005);
              ETN005.setBounds(80, 120, 260, ETN005.getPreferredSize().height);

              //---- ETE005 ----
              ETE005.setText("@ETE005@");
              ETE005.setName("ETE005");
              panelEtb.add(ETE005);
              ETE005.setBounds(345, 120, 160, ETE005.getPreferredSize().height);

              //---- label2 ----
              label2.setText("Etablissement");
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");
              panelEtb.add(label2);
              label2.setBounds(35, 0, 305, 20);

              //---- label3 ----
              label3.setText("En cours");
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
              label3.setName("label3");
              panelEtb.add(label3);
              label3.setBounds(345, 0, 160, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panelEtb.getComponentCount(); i++) {
                  Rectangle bounds = panelEtb.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelEtb.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelEtb.setMinimumSize(preferredSize);
                panelEtb.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panelEtb);
            panelEtb.setBounds(35, 220, 510, 150);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 392, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie WNUPAS;
  private JLabel label1;
  private XRiCheckBox EDTHLD;
  private XRiCheckBox EDTERR;
  private XRiCheckBox WTETB;
  private JComponent separator1;
  private JPanel panelEtb;
  private XRiCheckBox ETT001;
  private RiZoneSortie ETS001;
  private RiZoneSortie ETN001;
  private RiZoneSortie ETE001;
  private XRiCheckBox ETT002;
  private RiZoneSortie ETS002;
  private RiZoneSortie ETN002;
  private RiZoneSortie ETE002;
  private XRiCheckBox ETT003;
  private RiZoneSortie ETS003;
  private RiZoneSortie ETN003;
  private RiZoneSortie ETE003;
  private XRiCheckBox ETT004;
  private RiZoneSortie ETS004;
  private RiZoneSortie ETN004;
  private RiZoneSortie ETE004;
  private XRiCheckBox ETT005;
  private RiZoneSortie ETS005;
  private RiZoneSortie ETN005;
  private RiZoneSortie ETE005;
  private JLabel label2;
  private JLabel label3;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
