
package ri.serien.libecranrpg.stim.STIM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class STIM40FM_B6 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STIM40FM_B6(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    CTS001.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS001@")).trim());
    CTS002.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS002@")).trim());
    CTS003.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS003@")).trim());
    CTS004.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS004@")).trim());
    CTS005.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS005@")).trim());
    CTS006.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS006@")).trim());
    CTS007.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS007@")).trim());
    CTS008.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS008@")).trim());
    CTS009.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS009@")).trim());
    CTS010.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS010@")).trim());
    CTS011.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS011@")).trim());
    CTS012.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS012@")).trim());
    CTS013.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS013@")).trim());
    CTS014.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS014@")).trim());
    CTS015.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS015@")).trim());
    CTS016.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS016@")).trim());
    CTS017.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS017@")).trim());
    CTS018.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS018@")).trim());
    CTS019.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS019@")).trim());
    CTS020.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS020@")).trim());
    CTS021.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS021@")).trim());
    CTS022.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS022@")).trim());
    CTS023.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS023@")).trim());
    CTS024.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS024@")).trim());
    CTS025.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS025@")).trim());
    CTS026.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS026@")).trim());
    CTS027.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS027@")).trim());
    CTS028.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS028@")).trim());
    CTS029.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS029@")).trim());
    CTS030.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS030@")).trim());
    CTS031.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS031@")).trim());
    CTS032.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS032@")).trim());
    CTS033.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS033@")).trim());
    CTS034.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS034@")).trim());
    CTS035.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS035@")).trim());
    CTS036.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS036@")).trim());
    CTS037.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS037@")).trim());
    CTS038.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS038@")).trim());
    CTS039.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS039@")).trim());
    CTS040.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS040@")).trim());
    CTS041.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS041@")).trim());
    CTS042.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS042@")).trim());
    CTS043.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS043@")).trim());
    CTS044.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS044@")).trim());
    CTS045.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS045@")).trim());
    CTS046.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS046@")).trim());
    CTS047.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS047@")).trim());
    CTS048.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS048@")).trim());
    CTS049.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS049@")).trim());
    CTS050.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS050@")).trim());
    CTS051.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS051@")).trim());
    CTS052.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS052@")).trim());
    CTS053.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS053@")).trim());
    CTS054.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS054@")).trim());
    CTS055.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS055@")).trim());
    CTS056.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS056@")).trim());
    CTS057.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS057@")).trim());
    CTS058.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS058@")).trim());
    CTS059.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS059@")).trim());
    CTS060.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS060@")).trim());
    CTS061.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS061@")).trim());
    CTS062.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS062@")).trim());
    CTS063.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS063@")).trim());
    CTS064.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS064@")).trim());
    CTS065.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS065@")).trim());
    CTS066.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS066@")).trim());
    CTS067.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS067@")).trim());
    CTS068.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS068@")).trim());
    CTS069.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS069@")).trim());
    CTS070.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS070@")).trim());
    CTS071.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS071@")).trim());
    CTS072.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS072@")).trim());
    CTS073.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS073@")).trim());
    CTS074.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS074@")).trim());
    CTS075.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS075@")).trim());
    CTS076.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS076@")).trim());
    CTS077.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS077@")).trim());
    CTS078.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS078@")).trim());
    CTS079.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS079@")).trim());
    CTS080.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS080@")).trim());
    CTS081.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS081@")).trim());
    CTS082.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS082@")).trim());
    CTS083.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS083@")).trim());
    CTS084.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS084@")).trim());
    CTS085.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS085@")).trim());
    CTS086.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS086@")).trim());
    CTS087.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS087@")).trim());
    CTS088.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS088@")).trim());
    CTS089.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS089@")).trim());
    CTS090.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS090@")).trim());
    CTS091.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS091@")).trim());
    CTS092.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS092@")).trim());
    CTS093.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS093@")).trim());
    CTS094.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS094@")).trim());
    CTS095.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS095@")).trim());
    CTS096.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS096@")).trim());
    CTS097.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS097@")).trim());
    CTS098.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS098@")).trim());
    CTS099.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS099@")).trim());
    CTS100.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS100@")).trim());
    CTS101.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS101@")).trim());
    CTS102.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS102@")).trim());
    CTS103.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS103@")).trim());
    CTS104.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS104@")).trim());
    CTS105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS105@")).trim());
    CTS106.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS106@")).trim());
    CTS107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS107@")).trim());
    CTS108.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS108@")).trim());
    CTS109.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS109@")).trim());
    CTS110.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS110@")).trim());
    CTS111.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS111@")).trim());
    CTS112.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CTS112@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    panelEtb = new JPanel();
    CTS001 = new RiZoneSortie();
    CTS002 = new RiZoneSortie();
    CTS003 = new RiZoneSortie();
    CTS004 = new RiZoneSortie();
    CTS005 = new RiZoneSortie();
    CTS006 = new RiZoneSortie();
    CTS007 = new RiZoneSortie();
    CTS008 = new RiZoneSortie();
    CTS009 = new RiZoneSortie();
    CTS010 = new RiZoneSortie();
    CTS011 = new RiZoneSortie();
    CTS012 = new RiZoneSortie();
    CTS013 = new RiZoneSortie();
    CTS014 = new RiZoneSortie();
    CTS015 = new RiZoneSortie();
    CTS016 = new RiZoneSortie();
    CTS017 = new RiZoneSortie();
    CTS018 = new RiZoneSortie();
    CTS019 = new RiZoneSortie();
    CTS020 = new RiZoneSortie();
    CTS021 = new RiZoneSortie();
    CTS022 = new RiZoneSortie();
    CTS023 = new RiZoneSortie();
    CTS024 = new RiZoneSortie();
    CTS025 = new RiZoneSortie();
    CTS026 = new RiZoneSortie();
    CTS027 = new RiZoneSortie();
    CTS028 = new RiZoneSortie();
    CTS029 = new RiZoneSortie();
    CTS030 = new RiZoneSortie();
    CTS031 = new RiZoneSortie();
    CTS032 = new RiZoneSortie();
    CTS033 = new RiZoneSortie();
    CTS034 = new RiZoneSortie();
    CTS035 = new RiZoneSortie();
    CTS036 = new RiZoneSortie();
    CTS037 = new RiZoneSortie();
    CTS038 = new RiZoneSortie();
    CTS039 = new RiZoneSortie();
    CTS040 = new RiZoneSortie();
    CTS041 = new RiZoneSortie();
    CTS042 = new RiZoneSortie();
    CTS043 = new RiZoneSortie();
    CTS044 = new RiZoneSortie();
    CTS045 = new RiZoneSortie();
    CTS046 = new RiZoneSortie();
    CTS047 = new RiZoneSortie();
    CTS048 = new RiZoneSortie();
    CTS049 = new RiZoneSortie();
    CTS050 = new RiZoneSortie();
    CTS051 = new RiZoneSortie();
    CTS052 = new RiZoneSortie();
    CTS053 = new RiZoneSortie();
    CTS054 = new RiZoneSortie();
    CTS055 = new RiZoneSortie();
    CTS056 = new RiZoneSortie();
    CTS057 = new RiZoneSortie();
    CTS058 = new RiZoneSortie();
    CTS059 = new RiZoneSortie();
    CTS060 = new RiZoneSortie();
    CTS061 = new RiZoneSortie();
    CTS062 = new RiZoneSortie();
    CTS063 = new RiZoneSortie();
    CTS064 = new RiZoneSortie();
    CTS065 = new RiZoneSortie();
    CTS066 = new RiZoneSortie();
    CTS067 = new RiZoneSortie();
    CTS068 = new RiZoneSortie();
    CTS069 = new RiZoneSortie();
    CTS070 = new RiZoneSortie();
    CTS071 = new RiZoneSortie();
    CTS072 = new RiZoneSortie();
    CTS073 = new RiZoneSortie();
    CTS074 = new RiZoneSortie();
    CTS075 = new RiZoneSortie();
    CTS076 = new RiZoneSortie();
    CTS077 = new RiZoneSortie();
    CTS078 = new RiZoneSortie();
    CTS079 = new RiZoneSortie();
    CTS080 = new RiZoneSortie();
    CTS081 = new RiZoneSortie();
    CTS082 = new RiZoneSortie();
    CTS083 = new RiZoneSortie();
    CTS084 = new RiZoneSortie();
    CTS085 = new RiZoneSortie();
    CTS086 = new RiZoneSortie();
    CTS087 = new RiZoneSortie();
    CTS088 = new RiZoneSortie();
    CTS089 = new RiZoneSortie();
    CTS090 = new RiZoneSortie();
    CTS091 = new RiZoneSortie();
    CTS092 = new RiZoneSortie();
    CTS093 = new RiZoneSortie();
    CTS094 = new RiZoneSortie();
    CTS095 = new RiZoneSortie();
    CTS096 = new RiZoneSortie();
    CTS097 = new RiZoneSortie();
    CTS098 = new RiZoneSortie();
    CTS099 = new RiZoneSortie();
    CTS100 = new RiZoneSortie();
    CTS101 = new RiZoneSortie();
    CTS102 = new RiZoneSortie();
    CTS103 = new RiZoneSortie();
    CTS104 = new RiZoneSortie();
    CTS105 = new RiZoneSortie();
    CTS106 = new RiZoneSortie();
    CTS107 = new RiZoneSortie();
    CTS108 = new RiZoneSortie();
    CTS109 = new RiZoneSortie();
    CTS110 = new RiZoneSortie();
    CTS111 = new RiZoneSortie();
    CTS112 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(620, 570));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setBorder(new TitledBorder("R\u00e9capitulatif des \u00e9tablissements \u00e0 comptabiliser"));
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== panelEtb ========
            {
              panelEtb.setOpaque(false);
              panelEtb.setName("panelEtb");
              panelEtb.setLayout(null);

              //---- CTS001 ----
              CTS001.setText("@CTS001@");
              CTS001.setName("CTS001");
              panelEtb.add(CTS001);
              CTS001.setBounds(35, 5, 40, CTS001.getPreferredSize().height);

              //---- CTS002 ----
              CTS002.setText("@CTS002@");
              CTS002.setName("CTS002");
              panelEtb.add(CTS002);
              CTS002.setBounds(115, 5, 40, CTS002.getPreferredSize().height);

              //---- CTS003 ----
              CTS003.setText("@CTS003@");
              CTS003.setName("CTS003");
              panelEtb.add(CTS003);
              CTS003.setBounds(190, 5, 40, CTS003.getPreferredSize().height);

              //---- CTS004 ----
              CTS004.setText("@CTS004@");
              CTS004.setName("CTS004");
              panelEtb.add(CTS004);
              CTS004.setBounds(265, 5, 40, CTS004.getPreferredSize().height);

              //---- CTS005 ----
              CTS005.setText("@CTS005@");
              CTS005.setName("CTS005");
              panelEtb.add(CTS005);
              CTS005.setBounds(345, 5, 40, CTS005.getPreferredSize().height);

              //---- CTS006 ----
              CTS006.setText("@CTS006@");
              CTS006.setName("CTS006");
              panelEtb.add(CTS006);
              CTS006.setBounds(420, 5, 40, CTS006.getPreferredSize().height);

              //---- CTS007 ----
              CTS007.setText("@CTS007@");
              CTS007.setName("CTS007");
              panelEtb.add(CTS007);
              CTS007.setBounds(500, 5, 40, CTS007.getPreferredSize().height);

              //---- CTS008 ----
              CTS008.setText("@CTS008@");
              CTS008.setName("CTS008");
              panelEtb.add(CTS008);
              CTS008.setBounds(35, 35, 40, CTS008.getPreferredSize().height);

              //---- CTS009 ----
              CTS009.setText("@CTS009@");
              CTS009.setName("CTS009");
              panelEtb.add(CTS009);
              CTS009.setBounds(115, 35, 40, CTS009.getPreferredSize().height);

              //---- CTS010 ----
              CTS010.setText("@CTS010@");
              CTS010.setName("CTS010");
              panelEtb.add(CTS010);
              CTS010.setBounds(190, 35, 40, CTS010.getPreferredSize().height);

              //---- CTS011 ----
              CTS011.setText("@CTS011@");
              CTS011.setName("CTS011");
              panelEtb.add(CTS011);
              CTS011.setBounds(265, 35, 40, CTS011.getPreferredSize().height);

              //---- CTS012 ----
              CTS012.setText("@CTS012@");
              CTS012.setName("CTS012");
              panelEtb.add(CTS012);
              CTS012.setBounds(345, 35, 40, CTS012.getPreferredSize().height);

              //---- CTS013 ----
              CTS013.setText("@CTS013@");
              CTS013.setName("CTS013");
              panelEtb.add(CTS013);
              CTS013.setBounds(420, 35, 40, CTS013.getPreferredSize().height);

              //---- CTS014 ----
              CTS014.setText("@CTS014@");
              CTS014.setName("CTS014");
              panelEtb.add(CTS014);
              CTS014.setBounds(500, 35, 40, CTS014.getPreferredSize().height);

              //---- CTS015 ----
              CTS015.setText("@CTS015@");
              CTS015.setName("CTS015");
              panelEtb.add(CTS015);
              CTS015.setBounds(35, 65, 40, CTS015.getPreferredSize().height);

              //---- CTS016 ----
              CTS016.setText("@CTS016@");
              CTS016.setName("CTS016");
              panelEtb.add(CTS016);
              CTS016.setBounds(115, 65, 40, CTS016.getPreferredSize().height);

              //---- CTS017 ----
              CTS017.setText("@CTS017@");
              CTS017.setName("CTS017");
              panelEtb.add(CTS017);
              CTS017.setBounds(190, 65, 40, CTS017.getPreferredSize().height);

              //---- CTS018 ----
              CTS018.setText("@CTS018@");
              CTS018.setName("CTS018");
              panelEtb.add(CTS018);
              CTS018.setBounds(265, 65, 40, CTS018.getPreferredSize().height);

              //---- CTS019 ----
              CTS019.setText("@CTS019@");
              CTS019.setName("CTS019");
              panelEtb.add(CTS019);
              CTS019.setBounds(345, 65, 40, CTS019.getPreferredSize().height);

              //---- CTS020 ----
              CTS020.setText("@CTS020@");
              CTS020.setName("CTS020");
              panelEtb.add(CTS020);
              CTS020.setBounds(420, 65, 40, CTS020.getPreferredSize().height);

              //---- CTS021 ----
              CTS021.setText("@CTS021@");
              CTS021.setName("CTS021");
              panelEtb.add(CTS021);
              CTS021.setBounds(500, 65, 40, CTS021.getPreferredSize().height);

              //---- CTS022 ----
              CTS022.setText("@CTS022@");
              CTS022.setName("CTS022");
              panelEtb.add(CTS022);
              CTS022.setBounds(35, 95, 40, CTS022.getPreferredSize().height);

              //---- CTS023 ----
              CTS023.setText("@CTS023@");
              CTS023.setName("CTS023");
              panelEtb.add(CTS023);
              CTS023.setBounds(115, 95, 40, CTS023.getPreferredSize().height);

              //---- CTS024 ----
              CTS024.setText("@CTS024@");
              CTS024.setName("CTS024");
              panelEtb.add(CTS024);
              CTS024.setBounds(190, 95, 40, CTS024.getPreferredSize().height);

              //---- CTS025 ----
              CTS025.setText("@CTS025@");
              CTS025.setName("CTS025");
              panelEtb.add(CTS025);
              CTS025.setBounds(265, 95, 40, CTS025.getPreferredSize().height);

              //---- CTS026 ----
              CTS026.setText("@CTS026@");
              CTS026.setName("CTS026");
              panelEtb.add(CTS026);
              CTS026.setBounds(345, 95, 40, CTS026.getPreferredSize().height);

              //---- CTS027 ----
              CTS027.setText("@CTS027@");
              CTS027.setName("CTS027");
              panelEtb.add(CTS027);
              CTS027.setBounds(420, 95, 40, CTS027.getPreferredSize().height);

              //---- CTS028 ----
              CTS028.setText("@CTS028@");
              CTS028.setName("CTS028");
              panelEtb.add(CTS028);
              CTS028.setBounds(500, 95, 40, CTS028.getPreferredSize().height);

              //---- CTS029 ----
              CTS029.setText("@CTS029@");
              CTS029.setName("CTS029");
              panelEtb.add(CTS029);
              CTS029.setBounds(35, 125, 40, CTS029.getPreferredSize().height);

              //---- CTS030 ----
              CTS030.setText("@CTS030@");
              CTS030.setName("CTS030");
              panelEtb.add(CTS030);
              CTS030.setBounds(115, 125, 40, CTS030.getPreferredSize().height);

              //---- CTS031 ----
              CTS031.setText("@CTS031@");
              CTS031.setName("CTS031");
              panelEtb.add(CTS031);
              CTS031.setBounds(190, 125, 40, CTS031.getPreferredSize().height);

              //---- CTS032 ----
              CTS032.setText("@CTS032@");
              CTS032.setName("CTS032");
              panelEtb.add(CTS032);
              CTS032.setBounds(265, 125, 40, CTS032.getPreferredSize().height);

              //---- CTS033 ----
              CTS033.setText("@CTS033@");
              CTS033.setName("CTS033");
              panelEtb.add(CTS033);
              CTS033.setBounds(345, 125, 40, CTS033.getPreferredSize().height);

              //---- CTS034 ----
              CTS034.setText("@CTS034@");
              CTS034.setName("CTS034");
              panelEtb.add(CTS034);
              CTS034.setBounds(420, 125, 40, CTS034.getPreferredSize().height);

              //---- CTS035 ----
              CTS035.setText("@CTS035@");
              CTS035.setName("CTS035");
              panelEtb.add(CTS035);
              CTS035.setBounds(500, 125, 40, CTS035.getPreferredSize().height);

              //---- CTS036 ----
              CTS036.setText("@CTS036@");
              CTS036.setName("CTS036");
              panelEtb.add(CTS036);
              CTS036.setBounds(35, 155, 40, CTS036.getPreferredSize().height);

              //---- CTS037 ----
              CTS037.setText("@CTS037@");
              CTS037.setName("CTS037");
              panelEtb.add(CTS037);
              CTS037.setBounds(115, 155, 40, CTS037.getPreferredSize().height);

              //---- CTS038 ----
              CTS038.setText("@CTS038@");
              CTS038.setName("CTS038");
              panelEtb.add(CTS038);
              CTS038.setBounds(190, 155, 40, CTS038.getPreferredSize().height);

              //---- CTS039 ----
              CTS039.setText("@CTS039@");
              CTS039.setName("CTS039");
              panelEtb.add(CTS039);
              CTS039.setBounds(265, 155, 40, CTS039.getPreferredSize().height);

              //---- CTS040 ----
              CTS040.setText("@CTS040@");
              CTS040.setName("CTS040");
              panelEtb.add(CTS040);
              CTS040.setBounds(345, 155, 40, CTS040.getPreferredSize().height);

              //---- CTS041 ----
              CTS041.setText("@CTS041@");
              CTS041.setName("CTS041");
              panelEtb.add(CTS041);
              CTS041.setBounds(420, 155, 40, CTS041.getPreferredSize().height);

              //---- CTS042 ----
              CTS042.setText("@CTS042@");
              CTS042.setName("CTS042");
              panelEtb.add(CTS042);
              CTS042.setBounds(500, 155, 40, CTS042.getPreferredSize().height);

              //---- CTS043 ----
              CTS043.setText("@CTS043@");
              CTS043.setName("CTS043");
              panelEtb.add(CTS043);
              CTS043.setBounds(35, 185, 40, CTS043.getPreferredSize().height);

              //---- CTS044 ----
              CTS044.setText("@CTS044@");
              CTS044.setName("CTS044");
              panelEtb.add(CTS044);
              CTS044.setBounds(115, 185, 40, CTS044.getPreferredSize().height);

              //---- CTS045 ----
              CTS045.setText("@CTS045@");
              CTS045.setName("CTS045");
              panelEtb.add(CTS045);
              CTS045.setBounds(190, 185, 40, CTS045.getPreferredSize().height);

              //---- CTS046 ----
              CTS046.setText("@CTS046@");
              CTS046.setName("CTS046");
              panelEtb.add(CTS046);
              CTS046.setBounds(265, 185, 40, CTS046.getPreferredSize().height);

              //---- CTS047 ----
              CTS047.setText("@CTS047@");
              CTS047.setName("CTS047");
              panelEtb.add(CTS047);
              CTS047.setBounds(345, 185, 40, CTS047.getPreferredSize().height);

              //---- CTS048 ----
              CTS048.setText("@CTS048@");
              CTS048.setName("CTS048");
              panelEtb.add(CTS048);
              CTS048.setBounds(420, 185, 40, CTS048.getPreferredSize().height);

              //---- CTS049 ----
              CTS049.setText("@CTS049@");
              CTS049.setName("CTS049");
              panelEtb.add(CTS049);
              CTS049.setBounds(500, 185, 40, CTS049.getPreferredSize().height);

              //---- CTS050 ----
              CTS050.setText("@CTS050@");
              CTS050.setName("CTS050");
              panelEtb.add(CTS050);
              CTS050.setBounds(35, 215, 40, CTS050.getPreferredSize().height);

              //---- CTS051 ----
              CTS051.setText("@CTS051@");
              CTS051.setName("CTS051");
              panelEtb.add(CTS051);
              CTS051.setBounds(115, 215, 40, CTS051.getPreferredSize().height);

              //---- CTS052 ----
              CTS052.setText("@CTS052@");
              CTS052.setName("CTS052");
              panelEtb.add(CTS052);
              CTS052.setBounds(190, 215, 40, CTS052.getPreferredSize().height);

              //---- CTS053 ----
              CTS053.setText("@CTS053@");
              CTS053.setName("CTS053");
              panelEtb.add(CTS053);
              CTS053.setBounds(265, 215, 40, CTS053.getPreferredSize().height);

              //---- CTS054 ----
              CTS054.setText("@CTS054@");
              CTS054.setName("CTS054");
              panelEtb.add(CTS054);
              CTS054.setBounds(345, 215, 40, CTS054.getPreferredSize().height);

              //---- CTS055 ----
              CTS055.setText("@CTS055@");
              CTS055.setName("CTS055");
              panelEtb.add(CTS055);
              CTS055.setBounds(420, 215, 40, CTS055.getPreferredSize().height);

              //---- CTS056 ----
              CTS056.setText("@CTS056@");
              CTS056.setName("CTS056");
              panelEtb.add(CTS056);
              CTS056.setBounds(500, 215, 40, CTS056.getPreferredSize().height);

              //---- CTS057 ----
              CTS057.setText("@CTS057@");
              CTS057.setName("CTS057");
              panelEtb.add(CTS057);
              CTS057.setBounds(35, 245, 40, CTS057.getPreferredSize().height);

              //---- CTS058 ----
              CTS058.setText("@CTS058@");
              CTS058.setName("CTS058");
              panelEtb.add(CTS058);
              CTS058.setBounds(115, 245, 40, CTS058.getPreferredSize().height);

              //---- CTS059 ----
              CTS059.setText("@CTS059@");
              CTS059.setName("CTS059");
              panelEtb.add(CTS059);
              CTS059.setBounds(190, 245, 40, CTS059.getPreferredSize().height);

              //---- CTS060 ----
              CTS060.setText("@CTS060@");
              CTS060.setName("CTS060");
              panelEtb.add(CTS060);
              CTS060.setBounds(265, 245, 40, CTS060.getPreferredSize().height);

              //---- CTS061 ----
              CTS061.setText("@CTS061@");
              CTS061.setName("CTS061");
              panelEtb.add(CTS061);
              CTS061.setBounds(345, 245, 40, CTS061.getPreferredSize().height);

              //---- CTS062 ----
              CTS062.setText("@CTS062@");
              CTS062.setName("CTS062");
              panelEtb.add(CTS062);
              CTS062.setBounds(420, 245, 40, CTS062.getPreferredSize().height);

              //---- CTS063 ----
              CTS063.setText("@CTS063@");
              CTS063.setName("CTS063");
              panelEtb.add(CTS063);
              CTS063.setBounds(500, 245, 40, CTS063.getPreferredSize().height);

              //---- CTS064 ----
              CTS064.setText("@CTS064@");
              CTS064.setName("CTS064");
              panelEtb.add(CTS064);
              CTS064.setBounds(35, 275, 40, CTS064.getPreferredSize().height);

              //---- CTS065 ----
              CTS065.setText("@CTS065@");
              CTS065.setName("CTS065");
              panelEtb.add(CTS065);
              CTS065.setBounds(115, 275, 40, CTS065.getPreferredSize().height);

              //---- CTS066 ----
              CTS066.setText("@CTS066@");
              CTS066.setName("CTS066");
              panelEtb.add(CTS066);
              CTS066.setBounds(190, 275, 40, CTS066.getPreferredSize().height);

              //---- CTS067 ----
              CTS067.setText("@CTS067@");
              CTS067.setName("CTS067");
              panelEtb.add(CTS067);
              CTS067.setBounds(265, 275, 40, CTS067.getPreferredSize().height);

              //---- CTS068 ----
              CTS068.setText("@CTS068@");
              CTS068.setName("CTS068");
              panelEtb.add(CTS068);
              CTS068.setBounds(345, 275, 40, CTS068.getPreferredSize().height);

              //---- CTS069 ----
              CTS069.setText("@CTS069@");
              CTS069.setName("CTS069");
              panelEtb.add(CTS069);
              CTS069.setBounds(420, 275, 40, CTS069.getPreferredSize().height);

              //---- CTS070 ----
              CTS070.setText("@CTS070@");
              CTS070.setName("CTS070");
              panelEtb.add(CTS070);
              CTS070.setBounds(500, 275, 40, CTS070.getPreferredSize().height);

              //---- CTS071 ----
              CTS071.setText("@CTS071@");
              CTS071.setName("CTS071");
              panelEtb.add(CTS071);
              CTS071.setBounds(35, 305, 40, CTS071.getPreferredSize().height);

              //---- CTS072 ----
              CTS072.setText("@CTS072@");
              CTS072.setName("CTS072");
              panelEtb.add(CTS072);
              CTS072.setBounds(115, 305, 40, CTS072.getPreferredSize().height);

              //---- CTS073 ----
              CTS073.setText("@CTS073@");
              CTS073.setName("CTS073");
              panelEtb.add(CTS073);
              CTS073.setBounds(190, 305, 40, CTS073.getPreferredSize().height);

              //---- CTS074 ----
              CTS074.setText("@CTS074@");
              CTS074.setName("CTS074");
              panelEtb.add(CTS074);
              CTS074.setBounds(265, 305, 40, CTS074.getPreferredSize().height);

              //---- CTS075 ----
              CTS075.setText("@CTS075@");
              CTS075.setName("CTS075");
              panelEtb.add(CTS075);
              CTS075.setBounds(345, 305, 40, CTS075.getPreferredSize().height);

              //---- CTS076 ----
              CTS076.setText("@CTS076@");
              CTS076.setName("CTS076");
              panelEtb.add(CTS076);
              CTS076.setBounds(420, 305, 40, CTS076.getPreferredSize().height);

              //---- CTS077 ----
              CTS077.setText("@CTS077@");
              CTS077.setName("CTS077");
              panelEtb.add(CTS077);
              CTS077.setBounds(500, 305, 40, CTS077.getPreferredSize().height);

              //---- CTS078 ----
              CTS078.setText("@CTS078@");
              CTS078.setName("CTS078");
              panelEtb.add(CTS078);
              CTS078.setBounds(35, 335, 40, CTS078.getPreferredSize().height);

              //---- CTS079 ----
              CTS079.setText("@CTS079@");
              CTS079.setName("CTS079");
              panelEtb.add(CTS079);
              CTS079.setBounds(115, 335, 40, CTS079.getPreferredSize().height);

              //---- CTS080 ----
              CTS080.setText("@CTS080@");
              CTS080.setName("CTS080");
              panelEtb.add(CTS080);
              CTS080.setBounds(190, 335, 40, CTS080.getPreferredSize().height);

              //---- CTS081 ----
              CTS081.setText("@CTS081@");
              CTS081.setName("CTS081");
              panelEtb.add(CTS081);
              CTS081.setBounds(265, 335, 40, CTS081.getPreferredSize().height);

              //---- CTS082 ----
              CTS082.setText("@CTS082@");
              CTS082.setName("CTS082");
              panelEtb.add(CTS082);
              CTS082.setBounds(345, 335, 40, CTS082.getPreferredSize().height);

              //---- CTS083 ----
              CTS083.setText("@CTS083@");
              CTS083.setName("CTS083");
              panelEtb.add(CTS083);
              CTS083.setBounds(420, 335, 40, CTS083.getPreferredSize().height);

              //---- CTS084 ----
              CTS084.setText("@CTS084@");
              CTS084.setName("CTS084");
              panelEtb.add(CTS084);
              CTS084.setBounds(500, 335, 40, CTS084.getPreferredSize().height);

              //---- CTS085 ----
              CTS085.setText("@CTS085@");
              CTS085.setName("CTS085");
              panelEtb.add(CTS085);
              CTS085.setBounds(35, 365, 40, CTS085.getPreferredSize().height);

              //---- CTS086 ----
              CTS086.setText("@CTS086@");
              CTS086.setName("CTS086");
              panelEtb.add(CTS086);
              CTS086.setBounds(115, 365, 40, CTS086.getPreferredSize().height);

              //---- CTS087 ----
              CTS087.setText("@CTS087@");
              CTS087.setName("CTS087");
              panelEtb.add(CTS087);
              CTS087.setBounds(190, 365, 40, CTS087.getPreferredSize().height);

              //---- CTS088 ----
              CTS088.setText("@CTS088@");
              CTS088.setName("CTS088");
              panelEtb.add(CTS088);
              CTS088.setBounds(265, 365, 40, CTS088.getPreferredSize().height);

              //---- CTS089 ----
              CTS089.setText("@CTS089@");
              CTS089.setName("CTS089");
              panelEtb.add(CTS089);
              CTS089.setBounds(345, 365, 40, CTS089.getPreferredSize().height);

              //---- CTS090 ----
              CTS090.setText("@CTS090@");
              CTS090.setName("CTS090");
              panelEtb.add(CTS090);
              CTS090.setBounds(420, 365, 40, CTS090.getPreferredSize().height);

              //---- CTS091 ----
              CTS091.setText("@CTS091@");
              CTS091.setName("CTS091");
              panelEtb.add(CTS091);
              CTS091.setBounds(500, 365, 40, CTS091.getPreferredSize().height);

              //---- CTS092 ----
              CTS092.setText("@CTS092@");
              CTS092.setName("CTS092");
              panelEtb.add(CTS092);
              CTS092.setBounds(35, 395, 40, CTS092.getPreferredSize().height);

              //---- CTS093 ----
              CTS093.setText("@CTS093@");
              CTS093.setName("CTS093");
              panelEtb.add(CTS093);
              CTS093.setBounds(115, 395, 40, CTS093.getPreferredSize().height);

              //---- CTS094 ----
              CTS094.setText("@CTS094@");
              CTS094.setName("CTS094");
              panelEtb.add(CTS094);
              CTS094.setBounds(190, 395, 40, CTS094.getPreferredSize().height);

              //---- CTS095 ----
              CTS095.setText("@CTS095@");
              CTS095.setName("CTS095");
              panelEtb.add(CTS095);
              CTS095.setBounds(265, 395, 40, CTS095.getPreferredSize().height);

              //---- CTS096 ----
              CTS096.setText("@CTS096@");
              CTS096.setName("CTS096");
              panelEtb.add(CTS096);
              CTS096.setBounds(345, 395, 40, CTS096.getPreferredSize().height);

              //---- CTS097 ----
              CTS097.setText("@CTS097@");
              CTS097.setName("CTS097");
              panelEtb.add(CTS097);
              CTS097.setBounds(420, 395, 40, CTS097.getPreferredSize().height);

              //---- CTS098 ----
              CTS098.setText("@CTS098@");
              CTS098.setName("CTS098");
              panelEtb.add(CTS098);
              CTS098.setBounds(500, 395, 40, CTS098.getPreferredSize().height);

              //---- CTS099 ----
              CTS099.setText("@CTS099@");
              CTS099.setName("CTS099");
              panelEtb.add(CTS099);
              CTS099.setBounds(35, 425, 40, CTS099.getPreferredSize().height);

              //---- CTS100 ----
              CTS100.setText("@CTS100@");
              CTS100.setName("CTS100");
              panelEtb.add(CTS100);
              CTS100.setBounds(115, 425, 40, CTS100.getPreferredSize().height);

              //---- CTS101 ----
              CTS101.setText("@CTS101@");
              CTS101.setName("CTS101");
              panelEtb.add(CTS101);
              CTS101.setBounds(190, 425, 40, CTS101.getPreferredSize().height);

              //---- CTS102 ----
              CTS102.setText("@CTS102@");
              CTS102.setName("CTS102");
              panelEtb.add(CTS102);
              CTS102.setBounds(265, 425, 40, CTS102.getPreferredSize().height);

              //---- CTS103 ----
              CTS103.setText("@CTS103@");
              CTS103.setName("CTS103");
              panelEtb.add(CTS103);
              CTS103.setBounds(345, 425, 40, CTS103.getPreferredSize().height);

              //---- CTS104 ----
              CTS104.setText("@CTS104@");
              CTS104.setName("CTS104");
              panelEtb.add(CTS104);
              CTS104.setBounds(420, 425, 40, CTS104.getPreferredSize().height);

              //---- CTS105 ----
              CTS105.setText("@CTS105@");
              CTS105.setName("CTS105");
              panelEtb.add(CTS105);
              CTS105.setBounds(500, 425, 40, CTS105.getPreferredSize().height);

              //---- CTS106 ----
              CTS106.setText("@CTS106@");
              CTS106.setName("CTS106");
              panelEtb.add(CTS106);
              CTS106.setBounds(35, 455, 40, CTS106.getPreferredSize().height);

              //---- CTS107 ----
              CTS107.setText("@CTS107@");
              CTS107.setName("CTS107");
              panelEtb.add(CTS107);
              CTS107.setBounds(115, 455, 40, CTS107.getPreferredSize().height);

              //---- CTS108 ----
              CTS108.setText("@CTS108@");
              CTS108.setName("CTS108");
              panelEtb.add(CTS108);
              CTS108.setBounds(190, 455, 40, CTS108.getPreferredSize().height);

              //---- CTS109 ----
              CTS109.setText("@CTS109@");
              CTS109.setName("CTS109");
              panelEtb.add(CTS109);
              CTS109.setBounds(265, 455, 40, CTS109.getPreferredSize().height);

              //---- CTS110 ----
              CTS110.setText("@CTS110@");
              CTS110.setName("CTS110");
              panelEtb.add(CTS110);
              CTS110.setBounds(345, 455, 40, CTS110.getPreferredSize().height);

              //---- CTS111 ----
              CTS111.setText("@CTS111@");
              CTS111.setName("CTS111");
              panelEtb.add(CTS111);
              CTS111.setBounds(420, 455, 40, CTS111.getPreferredSize().height);

              //---- CTS112 ----
              CTS112.setText("@CTS112@");
              CTS112.setName("CTS112");
              panelEtb.add(CTS112);
              CTS112.setBounds(500, 455, 40, CTS112.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panelEtb.getComponentCount(); i++) {
                  Rectangle bounds = panelEtb.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panelEtb.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panelEtb.setMinimumSize(preferredSize);
                panelEtb.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panelEtb);
            panelEtb.setBounds(10, 35, 575, 490);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 599, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 542, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JPanel panelEtb;
  private RiZoneSortie CTS001;
  private RiZoneSortie CTS002;
  private RiZoneSortie CTS003;
  private RiZoneSortie CTS004;
  private RiZoneSortie CTS005;
  private RiZoneSortie CTS006;
  private RiZoneSortie CTS007;
  private RiZoneSortie CTS008;
  private RiZoneSortie CTS009;
  private RiZoneSortie CTS010;
  private RiZoneSortie CTS011;
  private RiZoneSortie CTS012;
  private RiZoneSortie CTS013;
  private RiZoneSortie CTS014;
  private RiZoneSortie CTS015;
  private RiZoneSortie CTS016;
  private RiZoneSortie CTS017;
  private RiZoneSortie CTS018;
  private RiZoneSortie CTS019;
  private RiZoneSortie CTS020;
  private RiZoneSortie CTS021;
  private RiZoneSortie CTS022;
  private RiZoneSortie CTS023;
  private RiZoneSortie CTS024;
  private RiZoneSortie CTS025;
  private RiZoneSortie CTS026;
  private RiZoneSortie CTS027;
  private RiZoneSortie CTS028;
  private RiZoneSortie CTS029;
  private RiZoneSortie CTS030;
  private RiZoneSortie CTS031;
  private RiZoneSortie CTS032;
  private RiZoneSortie CTS033;
  private RiZoneSortie CTS034;
  private RiZoneSortie CTS035;
  private RiZoneSortie CTS036;
  private RiZoneSortie CTS037;
  private RiZoneSortie CTS038;
  private RiZoneSortie CTS039;
  private RiZoneSortie CTS040;
  private RiZoneSortie CTS041;
  private RiZoneSortie CTS042;
  private RiZoneSortie CTS043;
  private RiZoneSortie CTS044;
  private RiZoneSortie CTS045;
  private RiZoneSortie CTS046;
  private RiZoneSortie CTS047;
  private RiZoneSortie CTS048;
  private RiZoneSortie CTS049;
  private RiZoneSortie CTS050;
  private RiZoneSortie CTS051;
  private RiZoneSortie CTS052;
  private RiZoneSortie CTS053;
  private RiZoneSortie CTS054;
  private RiZoneSortie CTS055;
  private RiZoneSortie CTS056;
  private RiZoneSortie CTS057;
  private RiZoneSortie CTS058;
  private RiZoneSortie CTS059;
  private RiZoneSortie CTS060;
  private RiZoneSortie CTS061;
  private RiZoneSortie CTS062;
  private RiZoneSortie CTS063;
  private RiZoneSortie CTS064;
  private RiZoneSortie CTS065;
  private RiZoneSortie CTS066;
  private RiZoneSortie CTS067;
  private RiZoneSortie CTS068;
  private RiZoneSortie CTS069;
  private RiZoneSortie CTS070;
  private RiZoneSortie CTS071;
  private RiZoneSortie CTS072;
  private RiZoneSortie CTS073;
  private RiZoneSortie CTS074;
  private RiZoneSortie CTS075;
  private RiZoneSortie CTS076;
  private RiZoneSortie CTS077;
  private RiZoneSortie CTS078;
  private RiZoneSortie CTS079;
  private RiZoneSortie CTS080;
  private RiZoneSortie CTS081;
  private RiZoneSortie CTS082;
  private RiZoneSortie CTS083;
  private RiZoneSortie CTS084;
  private RiZoneSortie CTS085;
  private RiZoneSortie CTS086;
  private RiZoneSortie CTS087;
  private RiZoneSortie CTS088;
  private RiZoneSortie CTS089;
  private RiZoneSortie CTS090;
  private RiZoneSortie CTS091;
  private RiZoneSortie CTS092;
  private RiZoneSortie CTS093;
  private RiZoneSortie CTS094;
  private RiZoneSortie CTS095;
  private RiZoneSortie CTS096;
  private RiZoneSortie CTS097;
  private RiZoneSortie CTS098;
  private RiZoneSortie CTS099;
  private RiZoneSortie CTS100;
  private RiZoneSortie CTS101;
  private RiZoneSortie CTS102;
  private RiZoneSortie CTS103;
  private RiZoneSortie CTS104;
  private RiZoneSortie CTS105;
  private RiZoneSortie CTS106;
  private RiZoneSortie CTS107;
  private RiZoneSortie CTS108;
  private RiZoneSortie CTS109;
  private RiZoneSortie CTS110;
  private RiZoneSortie CTS111;
  private RiZoneSortie CTS112;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
