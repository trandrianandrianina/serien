
package ri.serien.libecranrpg.vcrm.VCRM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class VCRM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VCRM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDialog(true);
    
    panel2.setRightDecoration(TCI2);
    panel3.setRightDecoration(TCI3);
    panel4.setRightDecoration(TCI4);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    riZoneSortie1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCAT@")).trim());
    riZoneSortie2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBORI @")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
    RECIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RECIV@")).trim());
    REPAC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@REPAC@")).trim());
    RECAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RECAT@")).trim());
    RETEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RETEL@")).trim());
    AFDTDX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFDTDX@")).trim());
    AFOBJ.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AFOBJ@")).trim());
    ACDACX.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACDACX@")).trim());
    ACNUA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACNUA@")).trim());
    ACREP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACREP@")).trim());
    ACTYP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACTYP@")).trim());
    ACETA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACETA@")).trim());
    riZoneSortie3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ETALIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    l_AucuneAffaire.setVisible(lexique.isTrue("N23", "FMTB1"));
    panel5.setVisible(!l_AucuneAffaire.isVisible());
    l_AucuneAction.setVisible(lexique.isTrue("N24", "FMTB1"));
    panel6.setVisible(!l_AucuneAction.isVisible());
    
    boolean isInterro = lexique.isTrue("53");
    
    if (isInterro) {
      if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
        TCI2.setEnabled(false);
      }
      else {
        TCI2.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
        TCI3.setEnabled(false);
      }
      else {
        TCI3.setEnabled(true);
      }
      if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
        TCI4.setEnabled(false);
      }
      else {
        TCI4.setEnabled(true);
      }
    }
    else {
      TCI2.setEnabled(true);
      TCI3.setEnabled(true);
      TCI4.setEnabled(true);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Fiche prospect"));
    
    // TODO Icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void TCI2ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI2").trim().equals("")) {
      lexique.HostFieldPutData("TCI2", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI2"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI3ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI3").trim().equals("")) {
      lexique.HostFieldPutData("TCI3", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI3"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void TCI4ActionPerformed(ActionEvent e) {
    if (lexique.HostFieldGetData("TCI4").trim().equals("")) {
      lexique.HostFieldPutData("TCI4", 0, "X");
    }
    else {
      lexique.HostFieldPutData("V06FO", 0, lexique.HostFieldGetData("TCI4"));
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    if (lexique.getMode() == Lexical.MODE_CONSULTATION) {
      lexique.HostFieldPutData("V06FO", 0, "T");
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    PRCAT = new XRiTextField();
    riZoneSortie1 = new RiZoneSortie();
    PRORI = new XRiTextField();
    riZoneSortie2 = new RiZoneSortie();
    PRENV = new XRiTextField();
    PRACT = new XRiTextField();
    PREFF = new XRiTextField();
    PRCAF = new XRiTextField();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    PRTOP1 = new XRiTextField();
    PRTOP2 = new XRiTextField();
    PRTOP3 = new XRiTextField();
    PRTOP4 = new XRiTextField();
    PRTOP5 = new XRiTextField();
    panel2 = new JXTitledPanel();
    RECIV = new RiZoneSortie();
    REPAC = new RiZoneSortie();
    RECAT = new RiZoneSortie();
    RETEL = new RiZoneSortie();
    label12 = new JLabel();
    xTitledSeparator4 = new JXTitledSeparator();
    panel3 = new JXTitledPanel();
    panel5 = new JPanel();
    label14 = new JLabel();
    AFDTDX = new RiZoneSortie();
    label15 = new JLabel();
    AFOBJ = new RiZoneSortie();
    AFOBSR = new XRiTextField();
    label13 = new JLabel();
    l_AucuneAffaire = new JLabel();
    panel4 = new JXTitledPanel();
    panel6 = new JPanel();
    label17 = new JLabel();
    ACDACX = new RiZoneSortie();
    label18 = new JLabel();
    ACNUA = new RiZoneSortie();
    ACREP = new RiZoneSortie();
    label20 = new JLabel();
    ACTYP = new RiZoneSortie();
    label16 = new JLabel();
    label19 = new JLabel();
    ACETA = new RiZoneSortie();
    riZoneSortie3 = new RiZoneSortie();
    l_AucuneAction = new JLabel();
    TCI2 = new SNBoutonDetail();
    TCI3 = new SNBoutonDetail();
    TCI4 = new SNBoutonDetail();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1150, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("@V01F@");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Appel ZP longues");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Qualification prospect");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Appels t\u00e9l\u00e9phoniques");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);

          //======== riMenu3 ========
          {
            riMenu3.setName("riMenu3");

            //---- riMenu_bt3 ----
            riMenu_bt3.setText("Outils");
            riMenu_bt3.setName("riMenu_bt3");
            riMenu3.add(riMenu_bt3);
          }
          menus_haut.add(riMenu3);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Bloc-Notes");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("M\u00e9mo");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- label1 ----
          label1.setText("Cat\u00e9gorie ");
          label1.setName("label1");

          //---- label2 ----
          label2.setText("Origine du prospect ");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("Envoy\u00e9 par ");
          label3.setName("label3");

          //---- label4 ----
          label4.setText("Activit\u00e9 ");
          label4.setName("label4");

          //---- label5 ----
          label5.setText("Effectif ");
          label5.setName("label5");

          //---- label6 ----
          label6.setText("C.Af ");
          label6.setName("label6");

          //---- PRCAT ----
          PRCAT.setName("PRCAT");

          //---- riZoneSortie1 ----
          riZoneSortie1.setText("@LIBCAT@");
          riZoneSortie1.setName("riZoneSortie1");

          //---- PRORI ----
          PRORI.setName("PRORI");

          //---- riZoneSortie2 ----
          riZoneSortie2.setText("@LIBORI @");
          riZoneSortie2.setName("riZoneSortie2");

          //---- PRENV ----
          PRENV.setName("PRENV");

          //---- PRACT ----
          PRACT.setName("PRACT");

          //---- PREFF ----
          PREFF.setName("PREFF");

          //---- PRCAF ----
          PRCAF.setName("PRCAF");

          //---- label7 ----
          label7.setText("@WTI1@");
          label7.setName("label7");

          //---- label8 ----
          label8.setText("@WTI2@");
          label8.setName("label8");

          //---- label9 ----
          label9.setText("@WTI3@");
          label9.setName("label9");

          //---- label10 ----
          label10.setText("@WTI4@");
          label10.setName("label10");

          //---- label11 ----
          label11.setText("@WTI5@");
          label11.setName("label11");

          //---- PRTOP1 ----
          PRTOP1.setName("PRTOP1");

          //---- PRTOP2 ----
          PRTOP2.setName("PRTOP2");

          //---- PRTOP3 ----
          PRTOP3.setName("PRTOP3");

          //---- PRTOP4 ----
          PRTOP4.setName("PRTOP4");

          //---- PRTOP5 ----
          PRTOP5.setName("PRTOP5");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(9, 9, 9)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label1)
                    .addGap(13, 13, 13)
                    .addComponent(PRCAT, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, 142, GroupLayout.PREFERRED_SIZE)
                    .addGap(48, 48, 48)
                    .addComponent(label2)
                    .addGap(14, 14, 14)
                    .addComponent(PRORI, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label3)
                    .addGap(7, 7, 7)
                    .addComponent(PRENV, GroupLayout.PREFERRED_SIZE, 194, GroupLayout.PREFERRED_SIZE)
                    .addGap(41, 41, 41)
                    .addComponent(label4)
                    .addGap(85, 85, 85)
                    .addComponent(PRACT, GroupLayout.PREFERRED_SIZE, 314, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(label5)
                    .addGap(31, 31, 31)
                    .addComponent(PREFF, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(label6)
                    .addGap(5, 5, 5)
                    .addComponent(PRCAF, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE)
                    .addGap(41, 41, 41)
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(PRTOP1, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(PRTOP2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(label9, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(PRTOP3, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(label10, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(PRTOP4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(label11, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                    .addGap(12, 12, 12)
                    .addComponent(PRTOP5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(PRCAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRORI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(label1, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(riZoneSortie1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label2, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(riZoneSortie2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(PRENV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRACT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label4, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE))))
                .addGap(7, 7, 7)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(PREFF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRCAF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRTOP1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRTOP2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRTOP3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRTOP4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(PRTOP5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addGroup(panel1Layout.createParallelGroup()
                      .addComponent(label5, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label6, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label7, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label8, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label9, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label10, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                      .addComponent(label11, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)))))
          );
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 15, 965, 145);

        //======== panel2 ========
        {
          panel2.setBorder(new DropShadowBorder());
          panel2.setTitle("Contact");
          panel2.setName("panel2");
          Container panel2ContentContainer = panel2.getContentContainer();
          panel2ContentContainer.setLayout(null);

          //---- RECIV ----
          RECIV.setComponentPopupMenu(null);
          RECIV.setFont(RECIV.getFont().deriveFont(Font.PLAIN));
          RECIV.setText("@RECIV@");
          RECIV.setName("RECIV");
          panel2ContentContainer.add(RECIV);
          RECIV.setBounds(15, 7, 40, RECIV.getPreferredSize().height);

          //---- REPAC ----
          REPAC.setFont(REPAC.getFont().deriveFont(Font.PLAIN));
          REPAC.setText("@REPAC@");
          REPAC.setName("REPAC");
          panel2ContentContainer.add(REPAC);
          REPAC.setBounds(75, 7, 164, REPAC.getPreferredSize().height);

          //---- RECAT ----
          RECAT.setFont(RECAT.getFont().deriveFont(Font.PLAIN));
          RECAT.setText("@RECAT@");
          RECAT.setName("RECAT");
          panel2ContentContainer.add(RECAT);
          RECAT.setBounds(259, 7, 40, RECAT.getPreferredSize().height);

          //---- RETEL ----
          RETEL.setText("@RETEL@");
          RETEL.setName("RETEL");
          panel2ContentContainer.add(RETEL);
          RETEL.setBounds(90, 70, 210, RETEL.getPreferredSize().height);

          //---- label12 ----
          label12.setText("T\u00e9l\u00e9phone");
          label12.setName("label12");
          panel2ContentContainer.add(label12);
          label12.setBounds(15, 70, 70, 28);

          //---- xTitledSeparator4 ----
          xTitledSeparator4.setTitle("Coordonn\u00e9es de la soci\u00e9t\u00e9");
          xTitledSeparator4.setName("xTitledSeparator4");
          panel2ContentContainer.add(xTitledSeparator4);
          xTitledSeparator4.setBounds(15, 45, 285, xTitledSeparator4.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = panel2ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2ContentContainer.setMinimumSize(preferredSize);
            panel2ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(5, 170, 320, 153);

        //======== panel3 ========
        {
          panel3.setBorder(new DropShadowBorder());
          panel3.setTitle("Affaire");
          panel3.setName("panel3");
          Container panel3ContentContainer = panel3.getContentContainer();
          panel3ContentContainer.setLayout(null);

          //======== panel5 ========
          {
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- label14 ----
            label14.setText("du");
            label14.setName("label14");
            panel5.add(label14);
            label14.setBounds(15, 5, 20, 24);

            //---- AFDTDX ----
            AFDTDX.setComponentPopupMenu(null);
            AFDTDX.setFont(AFDTDX.getFont().deriveFont(AFDTDX.getFont().getStyle() & ~Font.BOLD));
            AFDTDX.setText("@AFDTDX@");
            AFDTDX.setName("AFDTDX");
            panel5.add(AFDTDX);
            AFDTDX.setBounds(40, 5, 94, AFDTDX.getPreferredSize().height);

            //---- label15 ----
            label15.setText("Objet");
            label15.setName("label15");
            panel5.add(label15);
            label15.setBounds(145, 5, 35, 24);

            //---- AFOBJ ----
            AFOBJ.setFont(AFOBJ.getFont().deriveFont(AFOBJ.getFont().getStyle() & ~Font.BOLD));
            AFOBJ.setText("@AFOBJ@");
            AFOBJ.setName("AFOBJ");
            panel5.add(AFOBJ);
            AFOBJ.setBounds(185, 5, 64, AFOBJ.getPreferredSize().height);

            //---- AFOBSR ----
            AFOBSR.setName("AFOBSR");
            panel5.add(AFOBSR);
            AFOBSR.setBounds(95, 70, 214, AFOBSR.getPreferredSize().height);

            //---- label13 ----
            label13.setText("Observations");
            label13.setName("label13");
            panel5.add(label13);
            label13.setBounds(15, 75, 80, 24);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel5.getComponentCount(); i++) {
                Rectangle bounds = panel5.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel5.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel5.setMinimumSize(preferredSize);
              panel5.setPreferredSize(preferredSize);
            }
          }
          panel3ContentContainer.add(panel5);
          panel5.setBounds(0, 0, 315, 105);

          //---- l_AucuneAffaire ----
          l_AucuneAffaire.setText("(aucune affaire en cours)");
          l_AucuneAffaire.setHorizontalAlignment(SwingConstants.CENTER);
          l_AucuneAffaire.setFont(l_AucuneAffaire.getFont().deriveFont(l_AucuneAffaire.getFont().getStyle() | Font.BOLD, l_AucuneAffaire.getFont().getSize() + 3f));
          l_AucuneAffaire.setName("l_AucuneAffaire");
          panel3ContentContainer.add(l_AucuneAffaire);
          l_AucuneAffaire.setBounds(20, 5, 275, 115);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel3ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = panel3ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3ContentContainer.setMinimumSize(preferredSize);
            panel3ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(330, 170, 320, 150);

        //======== panel4 ========
        {
          panel4.setBorder(new DropShadowBorder());
          panel4.setTitle("Actions");
          panel4.setName("panel4");
          Container panel4ContentContainer = panel4.getContentContainer();
          panel4ContentContainer.setLayout(null);

          //======== panel6 ========
          {
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(null);

            //---- label17 ----
            label17.setText("du");
            label17.setName("label17");
            panel6.add(label17);
            label17.setBounds(15, 5, 20, 24);

            //---- ACDACX ----
            ACDACX.setComponentPopupMenu(null);
            ACDACX.setFont(ACDACX.getFont().deriveFont(ACDACX.getFont().getStyle() & ~Font.BOLD));
            ACDACX.setText("@ACDACX@");
            ACDACX.setName("ACDACX");
            panel6.add(ACDACX);
            ACDACX.setBounds(50, 5, 94, ACDACX.getPreferredSize().height);

            //---- label18 ----
            label18.setText("Affaire");
            label18.setName("label18");
            panel6.add(label18);
            label18.setBounds(150, 5, 75, 24);

            //---- ACNUA ----
            ACNUA.setFont(ACNUA.getFont().deriveFont(ACNUA.getFont().getStyle() & ~Font.BOLD));
            ACNUA.setText("@ACNUA@");
            ACNUA.setName("ACNUA");
            panel6.add(ACNUA);
            ACNUA.setBounds(235, 5, 60, ACNUA.getPreferredSize().height);

            //---- ACREP ----
            ACREP.setText("@ACREP@");
            ACREP.setName("ACREP");
            panel6.add(ACREP);
            ACREP.setBounds(235, 40, 40, ACREP.getPreferredSize().height);

            //---- label20 ----
            label20.setText("Repr\u00e9sentant");
            label20.setName("label20");
            panel6.add(label20);
            label20.setBounds(150, 40, 85, 24);

            //---- ACTYP ----
            ACTYP.setText("@ACTYP@");
            ACTYP.setName("ACTYP");
            panel6.add(ACTYP);
            ACTYP.setBounds(50, 40, 40, ACTYP.getPreferredSize().height);

            //---- label16 ----
            label16.setText("Type");
            label16.setName("label16");
            panel6.add(label16);
            label16.setBounds(15, 40, 35, 24);

            //---- label19 ----
            label19.setText("Etat");
            label19.setName("label19");
            panel6.add(label19);
            label19.setBounds(15, 70, 35, 24);

            //---- ACETA ----
            ACETA.setText("@ACETA@");
            ACETA.setName("ACETA");
            panel6.add(ACETA);
            ACETA.setBounds(50, 70, 24, ACETA.getPreferredSize().height);

            //---- riZoneSortie3 ----
            riZoneSortie3.setText("@ETALIB@");
            riZoneSortie3.setName("riZoneSortie3");
            panel6.add(riZoneSortie3);
            riZoneSortie3.setBounds(80, 70, 154, riZoneSortie3.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel6.getComponentCount(); i++) {
                Rectangle bounds = panel6.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel6.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel6.setMinimumSize(preferredSize);
              panel6.setPreferredSize(preferredSize);
            }
          }
          panel4ContentContainer.add(panel6);
          panel6.setBounds(0, 0, 315, 105);

          //---- l_AucuneAction ----
          l_AucuneAction.setText("(aucune action commerciale) ");
          l_AucuneAction.setHorizontalAlignment(SwingConstants.CENTER);
          l_AucuneAction.setFont(l_AucuneAction.getFont().deriveFont(l_AucuneAction.getFont().getStyle() | Font.BOLD, l_AucuneAction.getFont().getSize() + 3f));
          l_AucuneAction.setName("l_AucuneAction");
          panel4ContentContainer.add(l_AucuneAction);
          l_AucuneAction.setBounds(20, 5, 275, 115);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel4ContentContainer.getComponentCount(); i++) {
              Rectangle bounds = panel4ContentContainer.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel4ContentContainer.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel4ContentContainer.setMinimumSize(preferredSize);
            panel4ContentContainer.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel4);
        panel4.setBounds(655, 170, 320, 150);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- TCI2 ----
    TCI2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI2.setBorder(null);
    TCI2.setName("TCI2");
    TCI2.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI2ActionPerformed(e);
      }
    });

    //---- TCI3 ----
    TCI3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI3.setBorder(null);
    TCI3.setName("TCI3");
    TCI3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI3ActionPerformed(e);
      }
    });

    //---- TCI4 ----
    TCI4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    TCI4.setBorder(null);
    TCI4.setName("TCI4");
    TCI4.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        TCI4ActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private XRiTextField PRCAT;
  private RiZoneSortie riZoneSortie1;
  private XRiTextField PRORI;
  private RiZoneSortie riZoneSortie2;
  private XRiTextField PRENV;
  private XRiTextField PRACT;
  private XRiTextField PREFF;
  private XRiTextField PRCAF;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private XRiTextField PRTOP1;
  private XRiTextField PRTOP2;
  private XRiTextField PRTOP3;
  private XRiTextField PRTOP4;
  private XRiTextField PRTOP5;
  private JXTitledPanel panel2;
  private RiZoneSortie RECIV;
  private RiZoneSortie REPAC;
  private RiZoneSortie RECAT;
  private RiZoneSortie RETEL;
  private JLabel label12;
  private JXTitledSeparator xTitledSeparator4;
  private JXTitledPanel panel3;
  private JPanel panel5;
  private JLabel label14;
  private RiZoneSortie AFDTDX;
  private JLabel label15;
  private RiZoneSortie AFOBJ;
  private XRiTextField AFOBSR;
  private JLabel label13;
  private JLabel l_AucuneAffaire;
  private JXTitledPanel panel4;
  private JPanel panel6;
  private JLabel label17;
  private RiZoneSortie ACDACX;
  private JLabel label18;
  private RiZoneSortie ACNUA;
  private RiZoneSortie ACREP;
  private JLabel label20;
  private RiZoneSortie ACTYP;
  private JLabel label16;
  private JLabel label19;
  private RiZoneSortie ACETA;
  private RiZoneSortie riZoneSortie3;
  private JLabel l_AucuneAction;
  private SNBoutonDetail TCI2;
  private SNBoutonDetail TCI3;
  private SNBoutonDetail TCI4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
