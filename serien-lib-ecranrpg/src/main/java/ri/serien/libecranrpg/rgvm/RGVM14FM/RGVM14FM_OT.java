
package ri.serien.libecranrpg.rgvm.RGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGVM14FM_OT extends SNPanelEcranRPG implements ioFrame {
  
  
  public RGVM14FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("TOTAUX"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_7 = new JLabel();
    NBLIG = new XRiTextField();
    TOT1 = new XRiTextField();
    OBJ_9 = new JLabel();
    OBJ_11 = new JLabel();
    TOT3 = new XRiTextField();
    TOT4 = new XRiTextField();
    OBJ_13 = new JLabel();
    OBJ_15 = new JLabel();
    TOT5 = new XRiTextField();
    TOT6 = new XRiTextField();
    OBJ_17 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(525, 245));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Totalisation li\u00e9e \u00e0 la demande"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_7 ----
          OBJ_7.setText("Nombre de lignes");
          OBJ_7.setName("OBJ_7");
          panel1.add(OBJ_7);
          OBJ_7.setBounds(15, 34, 136, 20);

          //---- NBLIG ----
          NBLIG.setName("NBLIG");
          panel1.add(NBLIG);
          NBLIG.setBounds(190, 30, 120, NBLIG.getPreferredSize().height);

          //---- TOT1 ----
          TOT1.setName("TOT1");
          panel1.add(TOT1);
          TOT1.setBounds(190, 59, 120, TOT1.getPreferredSize().height);

          //---- OBJ_9 ----
          OBJ_9.setText("Chiffre d'affaires H.T");
          OBJ_9.setName("OBJ_9");
          panel1.add(OBJ_9);
          OBJ_9.setBounds(15, 63, 142, 20);

          //---- OBJ_11 ----
          OBJ_11.setText("Chiffre d'affaires TTC");
          OBJ_11.setName("OBJ_11");
          panel1.add(OBJ_11);
          OBJ_11.setBounds(15, 92, 148, 20);

          //---- TOT3 ----
          TOT3.setName("TOT3");
          panel1.add(TOT3);
          TOT3.setBounds(190, 88, 120, TOT3.getPreferredSize().height);

          //---- TOT4 ----
          TOT4.setName("TOT4");
          panel1.add(TOT4);
          TOT4.setBounds(190, 117, 120, TOT4.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("Prix de revient");
          OBJ_13.setName("OBJ_13");
          panel1.add(OBJ_13);
          OBJ_13.setBounds(15, 121, 112, 20);

          //---- OBJ_15 ----
          OBJ_15.setText("Marge r\u00e9elle");
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(15, 150, 103, 20);

          //---- TOT5 ----
          TOT5.setName("TOT5");
          panel1.add(TOT5);
          TOT5.setBounds(190, 146, 120, TOT5.getPreferredSize().height);

          //---- TOT6 ----
          TOT6.setName("TOT6");
          panel1.add(TOT6);
          TOT6.setBounds(190, 175, 120, TOT6.getPreferredSize().height);

          //---- OBJ_17 ----
          OBJ_17.setText("Marge r\u00e9elle en %");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(15, 179, 136, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 15, 330, 215);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_7;
  private XRiTextField NBLIG;
  private XRiTextField TOT1;
  private JLabel OBJ_9;
  private JLabel OBJ_11;
  private XRiTextField TOT3;
  private XRiTextField TOT4;
  private JLabel OBJ_13;
  private JLabel OBJ_15;
  private XRiTextField TOT5;
  private XRiTextField TOT6;
  private JLabel OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
