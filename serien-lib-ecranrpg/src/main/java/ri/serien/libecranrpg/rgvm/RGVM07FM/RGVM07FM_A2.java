
package ri.serien.libecranrpg.rgvm.RGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM07FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] ARG2A_Value = { "?", " ", "Q", "D", "F", "N", "H", "A" };
  private String[] ARG4A_Value = { "", "A", "T", "F", "S", "G", "*", "R", "L", "1", "2", };
  private String[] ARG4A_Text = { "Tous", "Article", "Tarif", "Famille", "Sous-famille", "Groupe", "Emboîtage ou général",
      "Ensemble d'articles", "Num de ligne (rang)", "Regroupement achat", "Fournisseur", };
  private String[] ARG4A2_Value = { "", "A", "T", "3", "4", "5", "*", "R", "L", "1", "2", };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 539, };
  
  public RGVM07FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARG2A.setValeurs(ARG2A_Value, null);
    ARG4A.setValeurs(ARG4A_Value, ARG4A_Text);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    SCAN2.setValeurs("E", SCAN2_GRP);
    SCAN2_B.setValeurs("N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITP1@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCNV@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    ARG5A.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRAT@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRAT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    WETB.setVisible(lexique.isPresent("WETB"));
    ARG3A.setVisible(lexique.isPresent("ARG3A"));
    riSousMenu7.setVisible(!lexique.HostFieldGetData("DETQTE").trim().equalsIgnoreCase(""));
    riSousMenu8.setVisible(lexique.HostFieldGetData("DETQTE").trim().equalsIgnoreCase(""));
    
    ARG5A.setVisible(lexique.isPresent("ARG5A"));
    OBJ_83.setVisible(lexique.isPresent("CDNAU"));
    CDNAU.setEnabled(lexique.isTrue("95"));
    // ARG2A.setVisible(!lexique.HostFieldGetData("ARG2A").trim().equalsIgnoreCase("D") & lexique.isPresent("ARG2A"));
    // ARG4A.setVisible( lexique.isPresent("ARG4A"));
    
    ARG2A.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SCAN2.setVisible(ARG2A.getSelectedIndex() != 0);
        SCAN2_B.setVisible(ARG2A.getSelectedIndex() != 0);
        if (ARG2A.getSelectedIndex() == 0) {
          lexique.HostFieldPutData("SCAN2", 0, "E");
        }
      }
    });
    
    // TODO Icones
    OBJ_78.setIcon(lexique.chargerImage("images/pfin20.png", true));
    OBJ_69.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    BT_ChgSoc.setIcon(lexique.chargerImage("images/changer.png", true));
    
    ARG4A.setVisible(lexique.isTrue("N78"));
    ARG4A2.setVisible(lexique.isTrue("78"));
    
    if ((!lexique.HostFieldGetData("LRRAT1").trim().equalsIgnoreCase("")) && lexique.isTrue("78")) {
      ARG4A2.removeAllItems();
      ARG4A2.addItem("Tous");
      ARG4A2.addItem("Article");
      ARG4A2.addItem("Tarif");
      ARG4A2.addItem(lexique.HostFieldGetData("LRRAT1"));
      ARG4A2.addItem(lexique.HostFieldGetData("LRRAT2"));
      ARG4A2.addItem(lexique.HostFieldGetData("LRRAT3"));
      ARG4A2.addItem("Emboîtage ou général");
      ARG4A2.addItem("Ensemble d'articles");
      ARG4A2.addItem("Numéro de ligne");
      ARG4A2.addItem("Regroupement d'achat");
      ARG4A2.addItem("Fournisseur");
    }
    
    if (lexique.isTrue("78")) {
      ARG4A2.setSelectedIndex(getIndice("ARG4A", ARG4A2_Value));
    }
    if (lexique.HostFieldGetData("TITP1").trim().equals("Recherche CNV")) {
      p_bpresentation.setText("Recherche condition de ventes");
    }
    
    if (lexique.HostFieldGetData("ARG8N").trim().equalsIgnoreCase("0")) {
      labelCNDannulees.setText("Conditions actives");
    }
    else {
      labelCNDannulees.setText("Conditions annulées");
    }
    
    if (lexique.HostFieldGetData("NVALID").trim().equalsIgnoreCase("1")) {
      labelDates.setText("Dates valides");
    }
    else {
      labelDates.setText("Dates dépassées");
    }
    if (lexique.isTrue("35")) {
      lbCode.setText("Code CN");
    }
    else {
      lbCode.setText("Client");
    }
    if (lexique.isTrue("95")) {
      lbCode.setText("Dérogation");
      p_bpresentation.setText("Recherche de dérogation");
      OBJ_47.setVisible(false);
      ARG2A.setVisible(false);
      SCAN2.setVisible(false);
      SCAN2_B.setVisible(false);
      riSousMenu7.setVisible(false);
      riSousMenu8.setVisible(false);
      riSousMenu_bt7.setVisible(false);
      riSousMenu_bt8.setVisible(false);
    }
    
    if (lexique.HostFieldGetData("ARG8N").trim().equals("9")) {
      WTP01.setComponentPopupMenu(BTD3);
    }
    else {
      WTP01.setComponentPopupMenu(BTD);
    }
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (lexique.isTrue("78")) {
      lexique.HostFieldPutData("ARG4A", 0, ARG4A2_Value[ARG4A2.getSelectedIndex()]);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("DETQTE", 0, " ");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("DETQTE", 0, "1");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_78ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD2.getInvoker().getName());
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    ARG3A = new XRiTextField();
    lbCode = new JLabel();
    WETB = new XRiTextField();
    OBJ_43 = new JLabel();
    BT_ChgSoc = new JButton();
    OBJ_57 = new JLabel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_46 = new JLabel();
    ARG5A = new XRiTextField();
    OBJ_58 = new RiZoneSortie();
    OBJ_45 = new JLabel();
    OBJ_47 = new JLabel();
    ARG2A = new XRiComboBox();
    OBJ_83 = new JLabel();
    CDNAU = new XRiTextField();
    SCAN2 = new XRiRadioButton();
    SCAN2_B = new XRiRadioButton();
    ARG4A = new XRiComboBox();
    ARG4A2 = new JComboBox();
    OBJ_84 = new JLabel();
    ARG10A = new XRiTextField();
    panel4 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_69 = new JButton();
    OBJ_78 = new JButton();
    labelCNDannulees = new JLabel();
    labelDates = new JLabel();
    labelCNDannulees2 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_32 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    BTD3 = new JPopupMenu();
    CHOISIR2 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    SCAN2_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(1085, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITP1@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- ARG3A ----
          ARG3A.setToolTipText("Code condition");
          ARG3A.setComponentPopupMenu(BTD2);
          ARG3A.setFont(new Font("sansserif", Font.PLAIN, 14));
          ARG3A.setName("ARG3A");
          p_tete_gauche.add(ARG3A);
          ARG3A.setBounds(305, 0, 70, ARG3A.getPreferredSize().height);

          //---- lbCode ----
          lbCode.setText("Code CN");
          lbCode.setPreferredSize(new Dimension(170, 20));
          lbCode.setMinimumSize(new Dimension(170, 20));
          lbCode.setMaximumSize(new Dimension(170, 20));
          lbCode.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbCode.setHorizontalTextPosition(SwingConstants.RIGHT);
          lbCode.setName("lbCode");
          p_tete_gauche.add(lbCode);
          lbCode.setBounds(220, 5, 86, 19);

          //---- WETB ----
          WETB.setToolTipText("Etablissement");
          WETB.setComponentPopupMenu(BTD2);
          WETB.setFont(new Font("sansserif", Font.PLAIN, 14));
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(100, 0, 40, WETB.getPreferredSize().height);

          //---- OBJ_43 ----
          OBJ_43.setText("Etablissement");
          OBJ_43.setPreferredSize(new Dimension(95, 20));
          OBJ_43.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_43.setName("OBJ_43");
          p_tete_gauche.add(OBJ_43);
          OBJ_43.setBounds(5, 5, 95, 19);

          //---- BT_ChgSoc ----
          BT_ChgSoc.setText("");
          BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
            }
          });
          p_tete_gauche.add(BT_ChgSoc);
          BT_ChgSoc.setBounds(145, 0, 32, 29);

          //---- OBJ_57 ----
          OBJ_57.setText("@LIBCNV@");
          OBJ_57.setPreferredSize(new Dimension(300, 16));
          OBJ_57.setMinimumSize(new Dimension(300, 16));
          OBJ_57.setMaximumSize(new Dimension(300, 20));
          OBJ_57.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_57.setName("OBJ_57");
          p_tete_gauche.add(OBJ_57);
          OBJ_57.setBounds(385, 6, 300, OBJ_57.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Sans d\u00e9tail condition");
              riSousMenu_bt7.setToolTipText("D\u00e9sactive le d\u00e9tail sur les conditions quantitatives");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Avec d\u00e9tail condition ");
              riSousMenu_bt8.setToolTipText("Active le d\u00e9tail sur les conditions quantitatives");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Conditions annul\u00e9es");
              riSousMenu_bt9.setToolTipText("Active/D\u00e9sactive l'affichage des conditions annul\u00e9es");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autres vues");
              riSousMenu_bt10.setToolTipText("Autres vues");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Export tableur");
              riSousMenu_bt11.setToolTipText("Exporter dans une feuille de calcul de tableur");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Validit\u00e9 d\u00e9pass\u00e9e");
              riSousMenu_bt12.setToolTipText("Activation ou d\u00e9sactivation de la date de validit\u00e9");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(850, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(850, 580));
          p_contenu.setName("p_contenu");

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- OBJ_46 ----
            OBJ_46.setText("Rattachement");
            OBJ_46.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_46.setName("OBJ_46");
            panel3.add(OBJ_46);
            OBJ_46.setBounds(40, 50, 90, 28);

            //---- ARG5A ----
            ARG5A.setToolTipText("@LIBRAT@");
            ARG5A.setComponentPopupMenu(BTD2);
            ARG5A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG5A.setName("ARG5A");
            panel3.add(ARG5A);
            ARG5A.setBounds(205, 50, 210, ARG5A.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("@LIBRAT@");
            OBJ_58.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_58.setMaximumSize(new Dimension(200, 23));
            OBJ_58.setMinimumSize(new Dimension(200, 23));
            OBJ_58.setPreferredSize(new Dimension(200, 24));
            OBJ_58.setName("OBJ_58");
            panel3.add(OBJ_58);
            OBJ_58.setBounds(445, 52, 300, OBJ_58.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Type de rattachement");
            OBJ_45.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_45.setName("OBJ_45");
            panel3.add(OBJ_45);
            OBJ_45.setBounds(40, 14, 142, 28);

            //---- OBJ_47 ----
            OBJ_47.setText("Cat\u00e9gorie");
            OBJ_47.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_47.setName("OBJ_47");
            panel3.add(OBJ_47);
            OBJ_47.setBounds(40, 89, 80, 28);

            //---- ARG2A ----
            ARG2A.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes cat\u00e9gories",
              "Conditions normales",
              "Conditions quantitatives",
              "D\u00e9rogations",
              "Conditions flash",
              "Conditions n\u00e9goci\u00e9es",
              "Chantiers",
              "Affaires"
            }));
            ARG2A.setComponentPopupMenu(BTD);
            ARG2A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG2A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG2A.setName("ARG2A");
            panel3.add(ARG2A);
            ARG2A.setBounds(205, 90, 210, ARG2A.getPreferredSize().height);

            //---- OBJ_83 ----
            OBJ_83.setText("Num\u00e9ro d'autorisation");
            OBJ_83.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_83.setName("OBJ_83");
            panel3.add(OBJ_83);
            OBJ_83.setBounds(450, 130, 150, 28);

            //---- CDNAU ----
            CDNAU.setComponentPopupMenu(BTD2);
            CDNAU.setFont(new Font("sansserif", Font.PLAIN, 14));
            CDNAU.setName("CDNAU");
            panel3.add(CDNAU);
            CDNAU.setBounds(600, 130, 210, CDNAU.getPreferredSize().height);

            //---- SCAN2 ----
            SCAN2.setText("Inclus");
            SCAN2.setName("SCAN2");
            panel3.add(SCAN2);
            SCAN2.setBounds(445, 90, 75, 26);

            //---- SCAN2_B ----
            SCAN2_B.setText("Exclus");
            SCAN2_B.setName("SCAN2_B");
            panel3.add(SCAN2_B);
            SCAN2_B.setBounds(515, 90, 75, 26);

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(BTD);
            ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG4A.setName("ARG4A");
            panel3.add(ARG4A);
            ARG4A.setBounds(205, 15, 210, ARG4A.getPreferredSize().height);

            //---- ARG4A2 ----
            ARG4A2.setModel(new DefaultComboBoxModel(new String[] {
              "Article",
              "Tarif",
              "@\"@LRRAT1@\"",
              "@\"@LRRAT2@\"",
              "@\"@LRRAT3@\"",
              "Embo\u00eetage ou g\u00e9n\u00e9ral",
              "Ensemble d'articles",
              "Num de ligne (rang)",
              "Regroupement achat",
              "Fournisseur"
            }));
            ARG4A2.setComponentPopupMenu(BTD);
            ARG4A2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A2.setName("ARG4A2");
            panel3.add(ARG4A2);
            ARG4A2.setBounds(205, 15, 210, ARG4A2.getPreferredSize().height);

            //---- OBJ_84 ----
            OBJ_84.setText("R\u00e9f\u00e9rence");
            OBJ_84.setFont(new Font("sansserif", Font.PLAIN, 14));
            OBJ_84.setName("OBJ_84");
            panel3.add(OBJ_84);
            OBJ_84.setBounds(40, 130, 134, 28);

            //---- ARG10A ----
            ARG10A.setComponentPopupMenu(BTD2);
            ARG10A.setFont(new Font("sansserif", Font.PLAIN, 14));
            ARG10A.setName("ARG10A");
            panel3.add(ARG10A);
            ARG10A.setBounds(205, 130, 210, ARG10A.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel4.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(35, 35, 740, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel4.add(BT_PGUP);
            BT_PGUP.setBounds(785, 70, 25, 95);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel4.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(785, 175, 25, 95);

            //---- OBJ_69 ----
            OBJ_69.setText("");
            OBJ_69.setToolTipText("D\u00e9but de la liste");
            OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_69.setName("OBJ_69");
            OBJ_69.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_69ActionPerformed(e);
              }
            });
            panel4.add(OBJ_69);
            OBJ_69.setBounds(785, 35, 25, 30);

            //---- OBJ_78 ----
            OBJ_78.setText("");
            OBJ_78.setToolTipText("Fin de la liste");
            OBJ_78.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_78.setName("OBJ_78");
            OBJ_78.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_78ActionPerformed(e);
              }
            });
            panel4.add(OBJ_78);
            OBJ_78.setBounds(785, 275, 25, 30);

            //---- labelCNDannulees ----
            labelCNDannulees.setText("Conditions annul\u00e9es");
            labelCNDannulees.setFont(labelCNDannulees.getFont().deriveFont(labelCNDannulees.getFont().getStyle() | Font.BOLD));
            labelCNDannulees.setHorizontalAlignment(SwingConstants.RIGHT);
            labelCNDannulees.setMaximumSize(new Dimension(80, 16));
            labelCNDannulees.setMinimumSize(new Dimension(80, 16));
            labelCNDannulees.setPreferredSize(new Dimension(80, 16));
            labelCNDannulees.setName("labelCNDannulees");
            panel4.add(labelCNDannulees);
            labelCNDannulees.setBounds(545, 305, 120, 25);

            //---- labelDates ----
            labelDates.setText("Toutes dates");
            labelDates.setFont(labelDates.getFont().deriveFont(labelDates.getFont().getStyle() | Font.BOLD));
            labelDates.setHorizontalAlignment(SwingConstants.RIGHT);
            labelDates.setMaximumSize(new Dimension(120, 16));
            labelDates.setMinimumSize(new Dimension(120, 16));
            labelDates.setPreferredSize(new Dimension(120, 16));
            labelDates.setName("labelDates");
            panel4.add(labelDates);
            labelDates.setBounds(675, 305, 135, 25);

            //---- labelCNDannulees2 ----
            labelCNDannulees2.setText("Restrictions :");
            labelCNDannulees2.setFont(labelCNDannulees2.getFont().deriveFont(labelCNDannulees2.getFont().getStyle() | Font.BOLD));
            labelCNDannulees2.setHorizontalAlignment(SwingConstants.RIGHT);
            labelCNDannulees2.setMaximumSize(new Dimension(80, 16));
            labelCNDannulees2.setMinimumSize(new Dimension(80, 16));
            labelCNDannulees2.setPreferredSize(new Dimension(80, 16));
            labelCNDannulees2.setName("labelCNDannulees2");
            panel4.add(labelCNDannulees2);
            labelCNDannulees2.setBounds(405, 305, 120, 25);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap(36, Short.MAX_VALUE)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 341, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_17 ----
      OBJ_17.setText("Modifier");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Annuler");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Consulter");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Article");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      BTD.addSeparator();

      //---- OBJ_24 ----
      OBJ_24.setText("Aide en ligne");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_32 ----
      OBJ_32.setText("Choix possibles");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_32);

      //---- OBJ_31 ----
      OBJ_31.setText("Aide en ligne");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_31);
    }

    //======== BTD3 ========
    {
      BTD3.setName("BTD3");

      //---- CHOISIR2 ----
      CHOISIR2.setText("Choisir");
      CHOISIR2.setName("CHOISIR2");
      CHOISIR2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD3.add(CHOISIR2);

      //---- OBJ_21 ----
      OBJ_21.setText("R\u00e9activer");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_21);

      //---- OBJ_23 ----
      OBJ_23.setText("Afficher");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_23);

      //---- OBJ_25 ----
      OBJ_25.setText("Supprimer");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_25);
      BTD3.addSeparator();

      //---- OBJ_27 ----
      OBJ_27.setText("Aide en ligne");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD3.add(OBJ_27);
    }

    //---- SCAN2_GRP ----
    SCAN2_GRP.add(SCAN2);
    SCAN2_GRP.add(SCAN2_B);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private XRiTextField ARG3A;
  private JLabel lbCode;
  private XRiTextField WETB;
  private JLabel OBJ_43;
  private JButton BT_ChgSoc;
  private JLabel OBJ_57;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel OBJ_46;
  private XRiTextField ARG5A;
  private RiZoneSortie OBJ_58;
  private JLabel OBJ_45;
  private JLabel OBJ_47;
  private XRiComboBox ARG2A;
  private JLabel OBJ_83;
  private XRiTextField CDNAU;
  private XRiRadioButton SCAN2;
  private XRiRadioButton SCAN2_B;
  private XRiComboBox ARG4A;
  private JComboBox ARG4A2;
  private JLabel OBJ_84;
  private XRiTextField ARG10A;
  private JPanel panel4;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_69;
  private JButton OBJ_78;
  private JLabel labelCNDannulees;
  private JLabel labelDates;
  private JLabel labelCNDannulees2;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_24;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_31;
  private JPopupMenu BTD3;
  private JMenuItem CHOISIR2;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_27;
  private ButtonGroup SCAN2_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
