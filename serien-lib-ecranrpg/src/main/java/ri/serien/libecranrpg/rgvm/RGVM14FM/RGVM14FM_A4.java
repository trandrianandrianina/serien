
package ri.serien.libecranrpg.rgvm.RGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM14FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 538, };
  private String[] ARGbon_Value = { " ", "1", "3", "4", "6", "7" };
  private String[] ARGbon_Title = { "Tous", "Validés", "Réservés", "Expédiés", "Facturés", "Comptabilisés", };
  private String[] PLAbon_Value = { "1", "3", "4", "6", "7" };
  private String[] PLAbon_Title = { "Validés", "Réservés", "Expédiés", "Facturés", "Comptabilisés", };
  private String[] PLAdevis_Value = { "0", "1", "2", "3", "4", "5", "6" };
  private String[] ARGdevis_Value = { "9", "1", "2", "3", "4", "5", "6", "0" };
  
  public RGVM14FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    PLA26A.setValeurs(PLAbon_Value, PLAbon_Title);
    ARG26A.setValeurs(ARGbon_Value, ARGbon_Title);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITRE@ @GESTION@ @SOUTIT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    A1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LIB@")).trim());
    A1LB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@A1LB1@")).trim());
    WCNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WCNV@")).trim());
    WTRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTRA@")).trim());
    WRAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WRAT@")).trim());
    WPXCNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPXCNV@")).trim());
    CLTAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTAR@")).trim());
    WPXTAR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WPXTAR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    if (lexique.isTrue("20")) {
      ARGdevis.setSelectedIndex(getIndice("ARG26A", ARGdevis_Value));
      PLAdevis.setSelectedIndex(getIndice("PLA26A", PLAdevis_Value));
    }
    
    panel3.setVisible(lexique.isTrue("N20"));
    panel4.setVisible(lexique.isTrue("N20"));
    panel8.setVisible(lexique.isTrue("N20"));
    panel11.setVisible(lexique.isTrue("20"));
    
    if (lexique.isTrue("N20")) {
      riSousMenu_bt6.setText("Historique des devis");
      riSousMenu_bt6.setToolTipText("Afficher l'historique des devis");
    }
    else {
      riSousMenu_bt6.setText("Historique des ventes");
      riSousMenu_bt6.setToolTipText("Afficher l'historique des ventes");
    }
    
    lbPositionListe.setText(lexique.HostFieldGetData("V03F").trim());
    lbNombreLignesTrouvees.setText(lexique.HostFieldGetData("NBRERL").trim() + " lignes trouvées");
    
    

    p_bpresentation.setCodeEtablissement(WETB.getText());
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    if (lexique.isTrue("20")) {
      lexique.HostFieldPutData("ARG26A", 0, ARGdevis_Value[ARGdevis.getSelectedIndex()]);
      lexique.HostFieldPutData("PLA26A", 0, PLAdevis_Value[PLAdevis.getSelectedIndex()]);
    }
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void menuItem4ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void BT_DEBActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_FINActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    WETB = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    lbPositionListe = new JLabel();
    BT_DEB = new JButton();
    BT_FIN = new JButton();
    lbNombreLignesTrouvees = new JLabel();
    panel1 = new JPanel();
    ARG16N1 = new XRiTextField();
    OBJ_61 = new JLabel();
    ARG16N2 = new XRiTextField();
    OBJ_64 = new JLabel();
    ARG3A = new XRiTextField();
    WNOM = new RiZoneSortie();
    A1LIB = new RiZoneSortie();
    A1LB1 = new RiZoneSortie();
    panel3 = new JPanel();
    NUM1 = new XRiTextField();
    SUF1 = new XRiTextField();
    label3 = new JLabel();
    DAT1 = new XRiCalendrier();
    QT1 = new XRiTextField();
    PV1 = new XRiTextField();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    panel4 = new JPanel();
    NUM2 = new XRiTextField();
    SUF2 = new XRiTextField();
    label7 = new JLabel();
    DAT2 = new XRiCalendrier();
    QT2 = new XRiTextField();
    PV2 = new XRiTextField();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    panel5 = new JPanel();
    NUM3 = new XRiTextField();
    SUF3 = new XRiTextField();
    label11 = new JLabel();
    DAT3 = new XRiCalendrier();
    QT3 = new XRiTextField();
    PV3 = new XRiTextField();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    panel6 = new JPanel();
    NUM4 = new XRiTextField();
    SUF4 = new XRiTextField();
    label15 = new JLabel();
    DAT4 = new XRiCalendrier();
    QT4 = new XRiTextField();
    PV4 = new XRiTextField();
    label16 = new JLabel();
    label17 = new JLabel();
    label18 = new JLabel();
    panel10 = new JPanel();
    panel9 = new JPanel();
    WCNV = new RiZoneSortie();
    WTRA = new RiZoneSortie();
    WRAT = new RiZoneSortie();
    label1 = new JLabel();
    WPXCNV = new RiZoneSortie();
    panel7 = new JPanel();
    label19 = new JLabel();
    CLTAR = new RiZoneSortie();
    label2 = new JLabel();
    WPXTAR = new RiZoneSortie();
    panel11 = new JPanel();
    ARGdevis = new JComboBox();
    PLAdevis = new JComboBox();
    label20 = new JLabel();
    panel8 = new JPanel();
    ARG26A = new XRiComboBox();
    PLA26A = new XRiComboBox();
    label21 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    popupMenu1 = new JPopupMenu();
    menuItem4 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1215, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITRE@ @GESTION@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49 ----
          OBJ_49.setText("Etablissement");
          OBJ_49.setName("OBJ_49");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setName("WETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(240, 240, 240))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Historique devis");
              riSousMenu_bt6.setToolTipText("Afficher l'historique des devis");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1015, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMaximumSize(new Dimension(20, 20));
          p_contenu.setMinimumSize(new Dimension(1015, 580));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(popupMenu1);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");

            //---- lbPositionListe ----
            lbPositionListe.setText("message position liste");
            lbPositionListe.setHorizontalAlignment(SwingConstants.RIGHT);
            lbPositionListe.setName("lbPositionListe");

            //---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_DEBActionPerformed(e);
              }
            });

            //---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                BT_FINActionPerformed(e);
              }
            });

            //---- lbNombreLignesTrouvees ----
            lbNombreLignesTrouvees.setText("Nombre d'enregistrements");
            lbNombreLignesTrouvees.setName("lbNombreLignesTrouvees");

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(18, 18, 18)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 615, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(BT_DEB, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(BT_FIN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(lbNombreLignesTrouvees, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                      .addGap(175, 175, 175)
                      .addComponent(lbPositionListe, GroupLayout.PREFERRED_SIZE, 215, GroupLayout.PREFERRED_SIZE))))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(8, 8, 8)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(BT_DEB, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(BT_FIN, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(lbNombreLignesTrouvees, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                    .addComponent(lbPositionListe, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
            );
          }

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ARG16N1 ----
            ARG16N1.setComponentPopupMenu(null);
            ARG16N1.setName("ARG16N1");
            panel1.add(ARG16N1);
            ARG16N1.setBounds(120, 5, 70, ARG16N1.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Client");
            OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(15, 9, 100, 20);

            //---- ARG16N2 ----
            ARG16N2.setComponentPopupMenu(null);
            ARG16N2.setName("ARG16N2");
            panel1.add(ARG16N2);
            ARG16N2.setBounds(195, 5, 40, ARG16N2.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Article");
            OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(15, 39, 100, 20);

            //---- ARG3A ----
            ARG3A.setComponentPopupMenu(null);
            ARG3A.setName("ARG3A");
            panel1.add(ARG3A);
            ARG3A.setBounds(120, 35, 210, 28);

            //---- WNOM ----
            WNOM.setText("@WNOM@");
            WNOM.setName("WNOM");
            panel1.add(WNOM);
            WNOM.setBounds(245, 7, 225, WNOM.getPreferredSize().height);

            //---- A1LIB ----
            A1LIB.setText("@A1LIB@");
            A1LIB.setName("A1LIB");
            panel1.add(A1LIB);
            A1LIB.setBounds(335, 37, 225, A1LIB.getPreferredSize().height);

            //---- A1LB1 ----
            A1LB1.setText("@A1LB1@");
            A1LB1.setName("A1LB1");
            panel1.add(A1LB1);
            A1LB1.setBounds(565, 37, 225, A1LB1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Derni\u00e8re facture"));
            panel3.setOpaque(false);
            panel3.setName("panel3");

            //---- NUM1 ----
            NUM1.setName("NUM1");

            //---- SUF1 ----
            SUF1.setName("SUF1");

            //---- label3 ----
            label3.setText("le");
            label3.setName("label3");

            //---- DAT1 ----
            DAT1.setName("DAT1");

            //---- QT1 ----
            QT1.setHorizontalAlignment(SwingConstants.RIGHT);
            QT1.setName("QT1");

            //---- PV1 ----
            PV1.setHorizontalAlignment(SwingConstants.RIGHT);
            PV1.setName("PV1");

            //---- label4 ----
            label4.setText("Qt\u00e9");
            label4.setName("label4");

            //---- label5 ----
            label5.setText("Prix");
            label5.setName("label5");

            //---- label6 ----
            label6.setText("Num");
            label6.setName("label6");

            GroupLayout panel3Layout = new GroupLayout(panel3);
            panel3.setLayout(panel3Layout);
            panel3Layout.setHorizontalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(label6, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(NUM1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(SUF1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DAT1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addComponent(label4, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(QT1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label5, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(PV1, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
            );
            panel3Layout.setVerticalGroup(
              panel3Layout.createParallelGroup()
                .addGroup(panel3Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(NUM1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SUF1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DAT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(label6)
                        .addComponent(label3))))
                  .addGap(2, 2, 2)
                  .addGroup(panel3Layout.createParallelGroup()
                    .addComponent(QT1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(PV1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel3Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel3Layout.createParallelGroup()
                        .addComponent(label4)
                        .addComponent(label5)))))
            );
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Derni\u00e8re commande"));
            panel4.setOpaque(false);
            panel4.setName("panel4");

            //---- NUM2 ----
            NUM2.setName("NUM2");

            //---- SUF2 ----
            SUF2.setName("SUF2");

            //---- label7 ----
            label7.setText("le");
            label7.setName("label7");

            //---- DAT2 ----
            DAT2.setName("DAT2");

            //---- QT2 ----
            QT2.setHorizontalAlignment(SwingConstants.RIGHT);
            QT2.setName("QT2");

            //---- PV2 ----
            PV2.setHorizontalAlignment(SwingConstants.RIGHT);
            PV2.setName("PV2");

            //---- label8 ----
            label8.setText("Qt\u00e9");
            label8.setName("label8");

            //---- label9 ----
            label9.setText("Prix");
            label9.setName("label9");

            //---- label10 ----
            label10.setText("Num");
            label10.setName("label10");

            GroupLayout panel4Layout = new GroupLayout(panel4);
            panel4.setLayout(panel4Layout);
            panel4Layout.setHorizontalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(label10, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(NUM2, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(SUF2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label7, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DAT2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addComponent(label8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(QT2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label9, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(PV2, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
            );
            panel4Layout.setVerticalGroup(
              panel4Layout.createParallelGroup()
                .addGroup(panel4Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(NUM2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SUF2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DAT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(label10)
                        .addComponent(label7))))
                  .addGap(2, 2, 2)
                  .addGroup(panel4Layout.createParallelGroup()
                    .addComponent(QT2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(PV2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel4Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel4Layout.createParallelGroup()
                        .addComponent(label8)
                        .addComponent(label9)))))
            );
          }

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("Prix le plus bas"));
            panel5.setOpaque(false);
            panel5.setName("panel5");

            //---- NUM3 ----
            NUM3.setName("NUM3");

            //---- SUF3 ----
            SUF3.setName("SUF3");

            //---- label11 ----
            label11.setText("le");
            label11.setName("label11");

            //---- DAT3 ----
            DAT3.setName("DAT3");

            //---- QT3 ----
            QT3.setHorizontalAlignment(SwingConstants.RIGHT);
            QT3.setName("QT3");

            //---- PV3 ----
            PV3.setHorizontalAlignment(SwingConstants.RIGHT);
            PV3.setName("PV3");

            //---- label12 ----
            label12.setText("Qt\u00e9");
            label12.setName("label12");

            //---- label13 ----
            label13.setText("Prix");
            label13.setName("label13");

            //---- label14 ----
            label14.setText("Num");
            label14.setName("label14");

            GroupLayout panel5Layout = new GroupLayout(panel5);
            panel5.setLayout(panel5Layout);
            panel5Layout.setHorizontalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addComponent(label14, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(NUM3, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(SUF3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label11, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DAT3, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addComponent(label12, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(QT3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label13, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(PV3, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
            );
            panel5Layout.setVerticalGroup(
              panel5Layout.createParallelGroup()
                .addGroup(panel5Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(NUM3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SUF3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DAT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel5Layout.createParallelGroup()
                        .addComponent(label14)
                        .addComponent(label11))))
                  .addGap(2, 2, 2)
                  .addGroup(panel5Layout.createParallelGroup()
                    .addComponent(QT3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(PV3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel5Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel5Layout.createParallelGroup()
                        .addComponent(label12)
                        .addComponent(label13)))))
            );
          }

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("Prix le plus haut"));
            panel6.setOpaque(false);
            panel6.setName("panel6");

            //---- NUM4 ----
            NUM4.setName("NUM4");

            //---- SUF4 ----
            SUF4.setName("SUF4");

            //---- label15 ----
            label15.setText("le");
            label15.setName("label15");

            //---- DAT4 ----
            DAT4.setName("DAT4");

            //---- QT4 ----
            QT4.setHorizontalAlignment(SwingConstants.RIGHT);
            QT4.setName("QT4");

            //---- PV4 ----
            PV4.setHorizontalAlignment(SwingConstants.RIGHT);
            PV4.setName("PV4");

            //---- label16 ----
            label16.setText("Qt\u00e9");
            label16.setName("label16");

            //---- label17 ----
            label17.setText("Prix");
            label17.setName("label17");

            //---- label18 ----
            label18.setText("Num");
            label18.setName("label18");

            GroupLayout panel6Layout = new GroupLayout(panel6);
            panel6.setLayout(panel6Layout);
            panel6Layout.setHorizontalGroup(
              panel6Layout.createParallelGroup()
                .addGroup(panel6Layout.createSequentialGroup()
                  .addGap(4, 4, 4)
                  .addGroup(panel6Layout.createParallelGroup()
                    .addGroup(panel6Layout.createSequentialGroup()
                      .addComponent(label18, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(NUM4, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(SUF4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label15, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(DAT4, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel6Layout.createSequentialGroup()
                      .addComponent(label16, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                      .addGap(0, 0, 0)
                      .addComponent(QT4, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(label17, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(PV4, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))))
            );
            panel6Layout.setVerticalGroup(
              panel6Layout.createParallelGroup()
                .addGroup(panel6Layout.createSequentialGroup()
                  .addGap(5, 5, 5)
                  .addGroup(panel6Layout.createParallelGroup()
                    .addComponent(NUM4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(SUF4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(DAT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel6Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel6Layout.createParallelGroup()
                        .addComponent(label18)
                        .addComponent(label15))))
                  .addGap(2, 2, 2)
                  .addGroup(panel6Layout.createParallelGroup()
                    .addComponent(QT4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(PV4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel6Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addGroup(panel6Layout.createParallelGroup()
                        .addComponent(label16)
                        .addComponent(label17))))
                  .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            );
          }

          //======== panel10 ========
          {
            panel10.setOpaque(false);
            panel10.setName("panel10");
            panel10.setLayout(null);

            //======== panel9 ========
            {
              panel9.setBorder(new TitledBorder("Condition"));
              panel9.setOpaque(false);
              panel9.setName("panel9");

              //---- WCNV ----
              WCNV.setText("@WCNV@");
              WCNV.setName("WCNV");

              //---- WTRA ----
              WTRA.setText("@WTRA@");
              WTRA.setName("WTRA");

              //---- WRAT ----
              WRAT.setText("@WRAT@");
              WRAT.setName("WRAT");

              //---- label1 ----
              label1.setText("Prix");
              label1.setName("label1");

              //---- WPXCNV ----
              WPXCNV.setText("@WPXCNV@");
              WPXCNV.setHorizontalAlignment(SwingConstants.RIGHT);
              WPXCNV.setName("WPXCNV");

              GroupLayout panel9Layout = new GroupLayout(panel9);
              panel9.setLayout(panel9Layout);
              panel9Layout.setHorizontalGroup(
                panel9Layout.createParallelGroup()
                  .addGroup(panel9Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(20, 20, 20)
                    .addComponent(WTRA, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WRAT, GroupLayout.PREFERRED_SIZE, 225, GroupLayout.PREFERRED_SIZE)
                    .addGap(90, 90, 90)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WPXCNV, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(33, Short.MAX_VALUE))
              );
              panel9Layout.setVerticalGroup(
                panel9Layout.createParallelGroup()
                  .addGroup(panel9Layout.createSequentialGroup()
                    .addGroup(panel9Layout.createParallelGroup()
                      .addComponent(WCNV, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel9Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(WTRA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel9Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(WRAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel9Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label1))
                      .addGroup(panel9Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(WPXCNV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              );
            }
            panel10.add(panel9);
            panel9.setBounds(5, 5, 685, 76);

            //======== panel7 ========
            {
              panel7.setBorder(new TitledBorder("Tarif"));
              panel7.setOpaque(false);
              panel7.setName("panel7");

              //---- label19 ----
              label19.setText("Colonne");
              label19.setName("label19");

              //---- CLTAR ----
              CLTAR.setText("@CLTAR@");
              CLTAR.setName("CLTAR");

              //---- label2 ----
              label2.setText("Prix tarif");
              label2.setName("label2");

              //---- WPXTAR ----
              WPXTAR.setText("@WPXTAR@");
              WPXTAR.setHorizontalAlignment(SwingConstants.RIGHT);
              WPXTAR.setName("WPXTAR");

              GroupLayout panel7Layout = new GroupLayout(panel7);
              panel7.setLayout(panel7Layout);
              panel7Layout.setHorizontalGroup(
                panel7Layout.createParallelGroup()
                  .addGroup(panel7Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(label19, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(CLTAR, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addGap(28, 28, 28)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(WPXTAR, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(29, Short.MAX_VALUE))
              );
              panel7Layout.setVerticalGroup(
                panel7Layout.createParallelGroup()
                  .addGroup(panel7Layout.createSequentialGroup()
                    .addGroup(panel7Layout.createParallelGroup()
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label19))
                      .addComponent(CLTAR, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(label2))
                      .addGroup(panel7Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(WPXTAR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
              );
            }
            panel10.add(panel7);
            panel7.setBounds(5, 80, 384, 76);

            //======== panel11 ========
            {
              panel11.setBorder(new TitledBorder("Etat des devis"));
              panel11.setOpaque(false);
              panel11.setName("panel11");
              panel11.setLayout(null);

              //---- ARGdevis ----
              ARGdevis.setModel(new DefaultComboBoxModel(new String[] {
                "Tous",
                "Attente",
                "Valid\u00e9",
                "Envoy\u00e9",
                "Sign\u00e9",
                "Val.d\u00e9pass\u00e9e",
                "Perdu",
                "Clotur\u00e9"
              }));
              ARGdevis.setComponentPopupMenu(BTD);
              ARGdevis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARGdevis.setName("ARGdevis");
              ARGdevis.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  ARG4AActionPerformed(e);
                }
              });
              panel11.add(ARGdevis);
              ARGdevis.setBounds(15, 35, 110, ARGdevis.getPreferredSize().height);

              //---- PLAdevis ----
              PLAdevis.setModel(new DefaultComboBoxModel(new String[] {
                "Attente",
                "Valid\u00e9",
                "Envoy\u00e9",
                "Sign\u00e9",
                "Valid. d\u00e9pass\u00e9e",
                "Perdu",
                "Clotur\u00e9",
                " "
              }));
              PLAdevis.setComponentPopupMenu(BTD);
              PLAdevis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLAdevis.setName("PLAdevis");
              panel11.add(PLAdevis);
              PLAdevis.setBounds(155, 35, 110, PLAdevis.getPreferredSize().height);

              //---- label20 ----
              label20.setText("\u00e0");
              label20.setHorizontalAlignment(SwingConstants.CENTER);
              label20.setName("label20");
              panel11.add(label20);
              label20.setBounds(135, 38, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel11.getComponentCount(); i++) {
                  Rectangle bounds = panel11.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel11.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel11.setMinimumSize(preferredSize);
                panel11.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel11);
            panel11.setBounds(405, 80, 286, 76);

            //======== panel8 ========
            {
              panel8.setBorder(new TitledBorder("Etat des bons"));
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- ARG26A ----
              ARG26A.setModel(new DefaultComboBoxModel(new String[] {
                "Tous",
                "Valid\u00e9s",
                "R\u00e9serv\u00e9s",
                "Exp\u00e9di\u00e9s",
                "Factur\u00e9s",
                "Comptabilis\u00e9s"
              }));
              ARG26A.setComponentPopupMenu(null);
              ARG26A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              ARG26A.setName("ARG26A");
              panel8.add(ARG26A);
              ARG26A.setBounds(15, 35, 120, ARG26A.getPreferredSize().height);

              //---- PLA26A ----
              PLA26A.setModel(new DefaultComboBoxModel(new String[] {
                " ",
                "Valid\u00e9s",
                "R\u00e9serv\u00e9s",
                "Exp\u00e9di\u00e9s",
                "Factur\u00e9s",
                "Comptabilis\u00e9s"
              }));
              PLA26A.setComponentPopupMenu(null);
              PLA26A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              PLA26A.setName("PLA26A");
              panel8.add(PLA26A);
              PLA26A.setBounds(155, 35, 120, PLA26A.getPreferredSize().height);

              //---- label21 ----
              label21.setText("\u00e0");
              label21.setHorizontalAlignment(SwingConstants.CENTER);
              label21.setName("label21");
              panel8.add(label21);
              label21.setBounds(135, 38, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel8);
            panel8.setBounds(405, 80, 286, 76);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel10.getComponentCount(); i++) {
                Rectangle bounds = panel10.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel10.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel10.setMinimumSize(preferredSize);
              panel10.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                      .addComponent(panel10, GroupLayout.PREFERRED_SIZE, 695, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(p_contenuLayout.createParallelGroup()
                      .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(panel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(12, 18, Short.MAX_VALUE))
                  .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 989, Short.MAX_VALUE)
                    .addContainerGap())))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 74, GroupLayout.PREFERRED_SIZE)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(13, 13, 13)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addComponent(panel6, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(2, 2, 2)
                    .addComponent(panel10, GroupLayout.PREFERRED_SIZE, 159, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(578, 578, 578))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 2, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("S\u00e9lection");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Affichage du bon");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Options article");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem4 ----
      menuItem4.setText("Options article");
      menuItem4.setName("menuItem4");
      menuItem4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem4ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem4);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private XRiTextField WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JLabel lbPositionListe;
  private JButton BT_DEB;
  private JButton BT_FIN;
  private JLabel lbNombreLignesTrouvees;
  private JPanel panel1;
  private XRiTextField ARG16N1;
  private JLabel OBJ_61;
  private XRiTextField ARG16N2;
  private JLabel OBJ_64;
  private XRiTextField ARG3A;
  private RiZoneSortie WNOM;
  private RiZoneSortie A1LIB;
  private RiZoneSortie A1LB1;
  private JPanel panel3;
  private XRiTextField NUM1;
  private XRiTextField SUF1;
  private JLabel label3;
  private XRiCalendrier DAT1;
  private XRiTextField QT1;
  private XRiTextField PV1;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JPanel panel4;
  private XRiTextField NUM2;
  private XRiTextField SUF2;
  private JLabel label7;
  private XRiCalendrier DAT2;
  private XRiTextField QT2;
  private XRiTextField PV2;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JPanel panel5;
  private XRiTextField NUM3;
  private XRiTextField SUF3;
  private JLabel label11;
  private XRiCalendrier DAT3;
  private XRiTextField QT3;
  private XRiTextField PV3;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JPanel panel6;
  private XRiTextField NUM4;
  private XRiTextField SUF4;
  private JLabel label15;
  private XRiCalendrier DAT4;
  private XRiTextField QT4;
  private XRiTextField PV4;
  private JLabel label16;
  private JLabel label17;
  private JLabel label18;
  private JPanel panel10;
  private JPanel panel9;
  private RiZoneSortie WCNV;
  private RiZoneSortie WTRA;
  private RiZoneSortie WRAT;
  private JLabel label1;
  private RiZoneSortie WPXCNV;
  private JPanel panel7;
  private JLabel label19;
  private RiZoneSortie CLTAR;
  private JLabel label2;
  private RiZoneSortie WPXTAR;
  private JPanel panel11;
  private JComboBox ARGdevis;
  private JComboBox PLAdevis;
  private JLabel label20;
  private JPanel panel8;
  private XRiComboBox ARG26A;
  private XRiComboBox PLA26A;
  private JLabel label21;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem4;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
