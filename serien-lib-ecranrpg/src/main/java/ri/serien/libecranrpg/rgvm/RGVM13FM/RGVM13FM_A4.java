
package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_A4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] ARG4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "9" };
  private String[] ARG4A_devis_Title = { "Attente", "Validé", "Envoyé", "Signé", "Validité dépassée", "Perdu", "Cloturé", "Tous" };
  private String[] PLA4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "7" };
  private String[] PLA4A_devis_Title = { "Attente", "Validé", "Envoyé", "Signé", "Validité dépassée", "Perdu", "Cloturé", "Tous" };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private int[] _WTP01_Width = { 650, };
  
  /**
   * Constructeur.
   */
  public RGVM13FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    ARG4A.setValeurs(ARG4A_devis_Value, ARG4A_devis_Title);
    PLA4A.setValeurs(PLA4A_devis_Value, PLA4A_devis_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    tous.setSelected(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("9"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@GESTION@").trim() + " " + interpreteurD.analyseExpression("@SOUTIT@").trim());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      // Si on est en mode selection multiple
      if (lexique.isTrue("93")) {
        // Si la ligne est déjà toppée
        if (isSelectionnee(WTP01.getSelectedRow())) {
          WTP01.setValeurTop("-");
        }
        else {
          WTP01.setValeurTop("+");
        }
        
        lexique.HostScreenSendKey(this, "Enter");
      }
      else {
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
  }
  
  /**
   * Vérifier que la ligne a été sélectionnée dans l'application (non graphiquement).
   * C'est pourri mais j'ai rien trouvé de mieux que ce bouzin.
   */
  private boolean isSelectionnee(int ligne) {
    boolean retour = false;
    switch (ligne) {
      case 0:
        retour = lexique.HostFieldGetData("BG01").equals("YL");
        break;
      case 1:
        retour = lexique.HostFieldGetData("BG02").equals("YL");
        break;
      case 2:
        retour = lexique.HostFieldGetData("BG03").equals("YL");
        break;
      case 3:
        retour = lexique.HostFieldGetData("BG04").equals("YL");
        break;
      case 4:
        retour = lexique.HostFieldGetData("BG05").equals("YL");
        break;
      case 5:
        retour = lexique.HostFieldGetData("BG06").equals("YL");
        break;
      case 6:
        retour = lexique.HostFieldGetData("BG07").equals("YL");
        break;
      case 7:
        retour = lexique.HostFieldGetData("BG08").equals("YL");
        break;
      case 8:
        retour = lexique.HostFieldGetData("BG09").equals("YL");
        break;
      case 9:
        retour = lexique.HostFieldGetData("BG10").equals("YL");
        break;
      case 10:
        retour = lexique.HostFieldGetData("BG11").equals("YL");
        break;
      case 11:
        retour = lexique.HostFieldGetData("BG12").equals("YL");
        break;
      case 12:
        retour = lexique.HostFieldGetData("BG13").equals("YL");
        break;
      case 13:
        retour = lexique.HostFieldGetData("BG14").equals("YL");
        break;
      case 14:
        retour = lexique.HostFieldGetData("BG15").equals("YL");
        break;
      
      default:
        retour = false;
        break;
    }
    return retour;
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_129ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    tous.setSelected(ARG4A.getSelectedIndex() == 7);
  }
  
  private void tousActionPerformed(ActionEvent e) {
    if (tous.isSelected()) {
      ARG4A.setSelectedIndex(7);
    }
    else {
      ARG4A.setSelectedIndex(0);
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_84 = new JLabel();
    ARG2N = new XRiTextField();
    ARG36N = new XRiTextField();
    ARG4A = new XRiComboBox();
    PLA4A = new XRiComboBox();
    OBJ_93 = new JLabel();
    OBJ_99 = new JLabel();
    tous = new JCheckBox();
    ARGDOC = new XRiTextField();
    OBJ_66 = new JLabel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_DEB = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1200, 415));
    setPreferredSize(new Dimension(1200, 415));
    setMaximumSize(new Dimension(1200, 415));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu10 ========
          {
            riSousMenu10.setName("riSousMenu10");

            //---- riSousMenu_bt10 ----
            riSousMenu_bt10.setText("Autres vues");
            riSousMenu_bt10.setToolTipText("Autres vues");
            riSousMenu_bt10.setName("riSousMenu_bt10");
            riSousMenu_bt10.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt10ActionPerformed(e);
              }
            });
            riSousMenu10.add(riSousMenu_bt10);
          }
          menus_haut.add(riSousMenu10);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(new GridBagLayout());
        ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1031, 0};
        ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {110, 322, 0};
        ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {0.0, 1.0E-4};
        ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_84 ----
          OBJ_84.setText("Num\u00e9ro d\u00e9but");
          OBJ_84.setName("OBJ_84");
          panel1.add(OBJ_84);
          OBJ_84.setBounds(25, 20, 105, 20);

          //---- ARG2N ----
          ARG2N.setComponentPopupMenu(null);
          ARG2N.setName("ARG2N");
          panel1.add(ARG2N);
          ARG2N.setBounds(130, 15, 70, ARG2N.getPreferredSize().height);

          //---- ARG36N ----
          ARG36N.setComponentPopupMenu(null);
          ARG36N.setName("ARG36N");
          panel1.add(ARG36N);
          ARG36N.setBounds(130, 15, 80, ARG36N.getPreferredSize().height);

          //---- ARG4A ----
          ARG4A.setComponentPopupMenu(null);
          ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG4A.setName("ARG4A");
          ARG4A.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ARG4AActionPerformed(e);
            }
          });
          panel1.add(ARG4A);
          ARG4A.setBounds(155, 50, 135, ARG4A.getPreferredSize().height);

          //---- PLA4A ----
          PLA4A.setComponentPopupMenu(null);
          PLA4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PLA4A.setName("PLA4A");
          panel1.add(PLA4A);
          PLA4A.setBounds(310, 50, 140, PLA4A.getPreferredSize().height);

          //---- OBJ_93 ----
          OBJ_93.setText("Etats");
          OBJ_93.setName("OBJ_93");
          panel1.add(OBJ_93);
          OBJ_93.setBounds(25, 53, 96, 20);

          //---- OBJ_99 ----
          OBJ_99.setText("\u00e0");
          OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_99.setName("OBJ_99");
          panel1.add(OBJ_99);
          OBJ_99.setBounds(290, 53, 20, 20);

          //---- tous ----
          tous.setText("");
          tous.setToolTipText("Tous");
          tous.setComponentPopupMenu(null);
          tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tous.setName("tous");
          tous.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tousActionPerformed(e);
            }
          });
          panel1.add(tous);
          tous.setBounds(130, 53, 20, 20);

          //---- ARGDOC ----
          ARGDOC.setComponentPopupMenu(null);
          ARGDOC.setName("ARGDOC");
          panel1.add(ARGDOC);
          ARGDOC.setBounds(725, 49, 210, ARGDOC.getPreferredSize().height);

          //---- OBJ_66 ----
          OBJ_66.setText("Recherche document");
          OBJ_66.setName("OBJ_66");
          panel1.add(OBJ_66);
          OBJ_66.setBounds(570, 53, 145, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(10, 10, 5, 10), 0, 0));

        //======== panel2 ========
        {
          panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

            //---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          panel2.add(SCROLLPANE_LIST);
          SCROLLPANE_LIST.setBounds(15, 30, 960, 270);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel2.add(BT_PGUP);
          BT_PGUP.setBounds(980, 60, 25, 95);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel2.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(980, 175, 25, 95);

          //---- BT_DEB ----
          BT_DEB.setText("");
          BT_DEB.setToolTipText("D\u00e9but de la liste");
          BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_DEB.setName("BT_DEB");
          BT_DEB.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_111ActionPerformed(e);
            }
          });
          panel2.add(BT_DEB);
          BT_DEB.setBounds(980, 30, 25, 30);

          //---- BT_FIN ----
          BT_FIN.setText("");
          BT_FIN.setToolTipText("Fin de la liste");
          BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_FIN.setName("BT_FIN");
          BT_FIN.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_129ActionPerformed(e);
            }
          });
          panel2.add(BT_FIN);
          BT_FIN.setBounds(980, 270, 25, 30);
        }
        p_contenu.add(panel2, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 10, 10, 10), 0, 0));
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_84;
  private XRiTextField ARG2N;
  private XRiTextField ARG36N;
  private XRiComboBox ARG4A;
  private XRiComboBox PLA4A;
  private JLabel OBJ_93;
  private JLabel OBJ_99;
  private JCheckBox tous;
  private XRiTextField ARGDOC;
  private JLabel OBJ_66;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_DEB;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
