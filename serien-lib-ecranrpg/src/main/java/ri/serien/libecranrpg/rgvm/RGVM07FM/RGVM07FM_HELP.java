
package ri.serien.libecranrpg.rgvm.RGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class RGVM07FM_HELP extends SNPanelEcranRPG implements ioFrame {
   
  
  public RGVM07FM_HELP(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    button1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRRAT1@")).trim());
    button2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRRAT2@")).trim());
    button3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRRAT3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // TODO Icones
    OBJ_17.setIcon(new ImageIcon("images/msgbox04.gif"));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
    OBJ_20.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Conditions de vente"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  @Override
  public void dispose() {
    getJDialog().dispose();
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTCN", 0, "C");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WTCN", 0, "X");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "A");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "G");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "F");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "S");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "T");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "1");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "2");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void OKActionPerformed(ActionEvent e) {
    getData();
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void button1ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "3");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void button2ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "4");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
    dispose();
  }
  
  private void button3ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("ARG4A", 0, "5");
    closePopupLinkWithBuffer(true);
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    panel1 = new JPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    panel2 = new JPanel();
    OBJ_9 = new JButton();
    OBJ_10 = new JButton();
    OBJ_11 = new JButton();
    OBJ_12 = new JButton();
    OBJ_13 = new JButton();
    OBJ_14 = new JButton();
    OBJ_15 = new JButton();
    OBJ_21 = new JButton();
    OBJ_22 = new JButton();
    button1 = new JButton();
    button2 = new JButton();
    button3 = new JButton();
    OBJ_16 = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_17 = new JLabel();
    panel5 = new JPanel();
    panel4 = new JPanel();
    BT_ENTER = new JButton();
    OBJ_20 = new JButton();

    //======== this ========
    setBackground(new Color(238, 238, 210));
    setName("this");
    setLayout(new BorderLayout());

    //======== panel1 ========
    {
      panel1.setBackground(new Color(238, 238, 210));
      panel1.setName("panel1");
      panel1.setLayout(null);

      //---- label1 ----
      label1.setText("Vous avez choisi de rechercher une condition afin de l'interroger, la modifier ou la dupliquer.");
      label1.setFont(new Font("sansserif", Font.BOLD, 14));
      label1.setName("label1");
      panel1.add(label1);
      label1.setBounds(40, 10, 665, 35);

      //---- label2 ----
      label2.setText("Quel est le type de cette condition ?");
      label2.setFont(new Font("sansserif", Font.BOLD, 14));
      label2.setName("label2");
      panel1.add(label2);
      label2.setBounds(230, 40, 280, 30);

      //======== panel2 ========
      {
        panel2.setBorder(new TitledBorder("Vous recherchez ..."));
        panel2.setOpaque(false);
        panel2.setName("panel2");
        panel2.setLayout(null);

        //---- OBJ_9 ----
        OBJ_9.setText("Une condition attribu\u00e9e \u00e0 un client");
        OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_9.setName("OBJ_9");
        OBJ_9.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_9ActionPerformed(e);
          }
        });
        panel2.add(OBJ_9);
        OBJ_9.setBounds(45, 30, 523, 29);

        //---- OBJ_10 ----
        OBJ_10.setText("Une condition param\u00e9tr\u00e9e");
        OBJ_10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_10.setName("OBJ_10");
        OBJ_10.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_10ActionPerformed(e);
          }
        });
        panel2.add(OBJ_10);
        OBJ_10.setBounds(45, 60, 523, 29);

        //---- OBJ_11 ----
        OBJ_11.setText("Une condition rattach\u00e9e \u00e0 un article");
        OBJ_11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_11.setName("OBJ_11");
        OBJ_11.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_11ActionPerformed(e);
          }
        });
        panel2.add(OBJ_11);
        OBJ_11.setBounds(45, 90, 523, 29);

        //---- OBJ_12 ----
        OBJ_12.setText("Une condition rattach\u00e9e \u00e0 un groupe");
        OBJ_12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_12.setName("OBJ_12");
        OBJ_12.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_12ActionPerformed(e);
          }
        });
        panel2.add(OBJ_12);
        OBJ_12.setBounds(45, 120, 523, 29);

        //---- OBJ_13 ----
        OBJ_13.setText("Une condition rattach\u00e9e \u00e0 une famille");
        OBJ_13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_13.setName("OBJ_13");
        OBJ_13.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_13ActionPerformed(e);
          }
        });
        panel2.add(OBJ_13);
        OBJ_13.setBounds(45, 150, 523, 29);

        //---- OBJ_14 ----
        OBJ_14.setText("Une condition rattach\u00e9e \u00e0 une sous-famille");
        OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_14.setName("OBJ_14");
        OBJ_14.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_14ActionPerformed(e);
          }
        });
        panel2.add(OBJ_14);
        OBJ_14.setBounds(45, 180, 523, 29);

        //---- OBJ_15 ----
        OBJ_15.setText("Une condition rattach\u00e9e \u00e0 un tarif");
        OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_15.setName("OBJ_15");
        OBJ_15.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_15ActionPerformed(e);
          }
        });
        panel2.add(OBJ_15);
        OBJ_15.setBounds(45, 210, 523, 29);

        //---- OBJ_21 ----
        OBJ_21.setText("Une condition rattach\u00e9e \u00e0 un regroupement achat");
        OBJ_21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_21.setName("OBJ_21");
        OBJ_21.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_21ActionPerformed(e);
          }
        });
        panel2.add(OBJ_21);
        OBJ_21.setBounds(45, 240, 523, 29);

        //---- OBJ_22 ----
        OBJ_22.setText("Une condition rattach\u00e9e \u00e0 un fournisseur");
        OBJ_22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_22.setName("OBJ_22");
        OBJ_22.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_22ActionPerformed(e);
          }
        });
        panel2.add(OBJ_22);
        OBJ_22.setBounds(45, 270, 523, 29);

        //---- button1 ----
        button1.setText("@LRRAT1@");
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button1ActionPerformed(e);
          }
        });
        panel2.add(button1);
        button1.setBounds(45, 300, 523, 29);

        //---- button2 ----
        button2.setText("@LRRAT2@");
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button2ActionPerformed(e);
          }
        });
        panel2.add(button2);
        button2.setBounds(45, 330, 523, 29);

        //---- button3 ----
        button3.setText("@LRRAT3@");
        button3.setName("button3");
        button3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            button3ActionPerformed(e);
          }
        });
        panel2.add(button3);
        button3.setBounds(45, 360, 523, 29);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel2.getComponentCount(); i++) {
            Rectangle bounds = panel2.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel2.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel2.setMinimumSize(preferredSize);
          panel2.setPreferredSize(preferredSize);
        }
      }
      panel1.add(panel2);
      panel2.setBounds(70, 70, 620, 400);

      //======== OBJ_16 ========
      {
        OBJ_16.setOpaque(false);
        OBJ_16.setName("OBJ_16");
        OBJ_16.setLayout(null);

        //---- OBJ_18 ----
        OBJ_18.setText("Si vous appuyez sur OK sans choisir d'option, vous afficherez la liste compl\u00e8te des conditions de ventes de type 'CN'");
        OBJ_18.setName("OBJ_18");
        OBJ_16.add(OBJ_18);
        OBJ_18.setBounds(89, 10, 651, 33);

        //---- OBJ_17 ----
        OBJ_17.setIcon(new ImageIcon("images/msgbox04.gif"));
        OBJ_17.setName("OBJ_17");
        OBJ_16.add(OBJ_17);
        OBJ_17.setBounds(10, 0, 65, 60);
      }
      panel1.add(OBJ_16);
      OBJ_16.setBounds(5, 465, 755, 61);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel1.getComponentCount(); i++) {
          Rectangle bounds = panel1.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel1.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel1.setMinimumSize(preferredSize);
        panel1.setPreferredSize(preferredSize);
      }
    }
    add(panel1, BorderLayout.CENTER);

    //======== panel5 ========
    {
      panel5.setBackground(new Color(238, 238, 210));
      panel5.setName("panel5");
      panel5.setLayout(null);

      //======== panel4 ========
      {
        panel4.setBackground(new Color(238, 238, 210));
        panel4.setName("panel4");
        panel4.setLayout(null);

        //---- BT_ENTER ----
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setToolTipText("OK");
        BT_ENTER.setText("Valider");
        BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
        BT_ENTER.setHorizontalAlignment(SwingConstants.LEFT);
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OKActionPerformed(e);
          }
        });
        panel4.add(BT_ENTER);
        BT_ENTER.setBounds(50, 5, 120, 40);

        //---- OBJ_20 ----
        OBJ_20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        OBJ_20.setToolTipText("Retour");
        OBJ_20.setText("Retour");
        OBJ_20.setFont(OBJ_20.getFont().deriveFont(OBJ_20.getFont().getStyle() | Font.BOLD, OBJ_20.getFont().getSize() + 2f));
        OBJ_20.setHorizontalAlignment(SwingConstants.LEFT);
        OBJ_20.setName("OBJ_20");
        OBJ_20.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            OBJ_20ActionPerformed(e);
          }
        });
        panel4.add(OBJ_20);
        OBJ_20.setBounds(180, 5, 120, 40);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < panel4.getComponentCount(); i++) {
            Rectangle bounds = panel4.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = panel4.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          panel4.setMinimumSize(preferredSize);
          panel4.setPreferredSize(preferredSize);
        }
      }
      panel5.add(panel4);
      panel4.setBounds(445, 5, 320, 50);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < panel5.getComponentCount(); i++) {
          Rectangle bounds = panel5.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = panel5.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        panel5.setMinimumSize(preferredSize);
        panel5.setPreferredSize(preferredSize);
      }
    }
    add(panel5, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel panel1;
  private JLabel label1;
  private JLabel label2;
  private JPanel panel2;
  private JButton OBJ_9;
  private JButton OBJ_10;
  private JButton OBJ_11;
  private JButton OBJ_12;
  private JButton OBJ_13;
  private JButton OBJ_14;
  private JButton OBJ_15;
  private JButton OBJ_21;
  private JButton OBJ_22;
  private JButton button1;
  private JButton button2;
  private JButton button3;
  private JPanel OBJ_16;
  private JLabel OBJ_18;
  private JLabel OBJ_17;
  private JPanel panel5;
  private JPanel panel4;
  private JButton BT_ENTER;
  private JButton OBJ_20;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
