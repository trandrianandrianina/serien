//$$david$$ ££12/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_AC extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] ARG3A_Value = { "E", "D", };
  private String[] ARG4A_bons_Value = { "0", "1", "3", "4", "6", "7", "9", };
  private String[] PLA4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "7" };
  private String[] PLA4A_bons_Value = { "0", "1", "3", "4", "6", "7", };
  private String[] ARG4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "9" };
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 558, };
  private boolean isVerrou = false;
  private boolean isTousCriteres = false;
  
  /**
   * Constructeur.
   */
  public RGVM13FM_AC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARG3A.setValeurs(ARG3A_Value, null);
    VERR.setValeursSelection("1", "0");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion de @GESTION@ @SOUTIT@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    lib_client.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMCLI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    riSousMenu_crea.setVisible(true);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Mode bons verrouillés
    isVerrou = VERR.isSelected();
    
    // Mode bon ou mode devis
    
    ARG4A_devis.setSelectedIndex(getIndice("ARG4A", ARG4A_devis_Value));
    PLA4A_devis.setSelectedIndex(getIndice("PLA4A", PLA4A_devis_Value));
    PLA4A_bons.setSelectedIndex(getIndice("PLA4A", PLA4A_bons_Value));
    ARG4A_bons.setSelectedIndex(getIndice("ARG4A", ARG4A_bons_Value));
    
    tous.setSelected(lexique.HostFieldGetData("ARG4A").equals("9"));
    
    WETB.setEnabled(lexique.isPresent("WETB"));
    ARG9N2.setEnabled(lexique.isPresent("ARG9N2"));
    WSUIS.setEnabled(lexique.isPresent("WSUIS"));
    WP2.setEnabled(lexique.isPresent("WP2"));
    OBJ_73.setVisible(lexique.isPresent("INDNUM"));
    ARG9N1.setEnabled(lexique.isPresent("ARG9N1"));
    ARG36N.setEnabled(lexique.isPresent("ARG36N"));
    ARG2N.setVisible(lexique.isPresent("ARG2N"));
    ARG15A.setEnabled(lexique.isPresent("ARG15A"));
    isTousCriteres = lexique.isTrue("N61");
    OBJ_95.setVisible(isTousCriteres);
    ARG3A.setVisible(isTousCriteres);
    
    lib_client.setVisible(!lexique.HostFieldGetData("NOMCLI").trim().isEmpty());
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "E", "Enter");
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "Z", "Enter");
    WTP01.setValeurTop("Z");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "D", "Enter");
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter",e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_129ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void tousActionPerformed(ActionEvent e) {
    if (tous.isSelected()) {
      ARG4A_bons.setSelectedIndex(6);
      ARG4A_devis.setSelectedIndex(7);
    }
    else {
      ARG4A_bons.setSelectedIndex(0);
      ARG4A_devis.setSelectedIndex(0);
    }
  }
  
  private void ARG3AActionPerformed(ActionEvent e) {
    // switchMode(ARG3A.getSelectedIndex());
    
  }
  
  private void VERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ARG4A_devisActionPerformed(ActionEvent e) {
    tous.setSelected(ARG4A_devis.getSelectedIndex() == 7);
  }
  
  private void ARG4A_bonsActionPerformed(ActionEvent e) {
    tous.setSelected(ARG4A_bons.getSelectedIndex() == 6);
  }
  
  private void trans_bonsActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "3", "Enter");
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "A", "Enter");
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_70 = new JLabel();
    VERR = new XRiCheckBox();
    INDNUM = new XRiTextField();
    OBJ_73 = new JLabel();
    WETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    lib_client = new RiZoneSortie();
    ARG9N1 = new XRiTextField();
    OBJ_87 = new JLabel();
    ARG9N2 = new XRiTextField();
    ARG39A = new XRiTextField();
    label1 = new JLabel();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_DEB = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    trans_bons = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_67 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    WSUIS = new XRiTextField();
    WP2 = new XRiTextField();
    TRIAGE2 = new JMenuItem();
    OBJ_99 = new JLabel();
    PLA4A_bons = new JComboBox();
    PLA4A_devis = new JComboBox();
    ARG4A_devis = new JComboBox();
    ARG4A_bons = new JComboBox();
    OBJ_93 = new JLabel();
    tous = new JCheckBox();
    ARG3A = new XRiComboBox();
    OBJ_95 = new JLabel();
    OBJ_104 = new JLabel();
    OBJ_84 = new JLabel();
    ARG15A = new XRiTextField();
    ARG2N = new XRiTextField();
    ARG36N = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(940, 660));
    setPreferredSize(new Dimension(940, 660));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de @GESTION@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");

          //---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(BTD);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");
          VERR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              VERRActionPerformed(e);
            }
          });

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");

          //---- OBJ_73 ----
          OBJ_73.setText("Num\u00e9ro");
          OBJ_73.setName("OBJ_73");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE)
                .addGap(352, 352, 352))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Exportation tableur");
              riSousMenu_bt7.setToolTipText("Transfert de la liste obtenue vers le tableur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Personnalisation");
              riSousMenu_bt8.setToolTipText("Stockage de la personnalisation programme");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(850, 575));
        p_centrage.setMinimumSize(new Dimension(850, 605));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(740, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(740, 540));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- lib_client ----
            lib_client.setText("@NOMCLI@");
            lib_client.setName("lib_client");
            panel1.add(lib_client);
            lib_client.setBounds(250, 57, 241, lib_client.getPreferredSize().height);

            //---- ARG9N1 ----
            ARG9N1.setComponentPopupMenu(null);
            ARG9N1.setHorizontalAlignment(SwingConstants.RIGHT);
            ARG9N1.setName("ARG9N1");
            panel1.add(ARG9N1);
            ARG9N1.setBounds(145, 55, 58, ARG9N1.getPreferredSize().height);

            //---- OBJ_87 ----
            OBJ_87.setText("Client");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(20, 59, 55, 20);

            //---- ARG9N2 ----
            ARG9N2.setComponentPopupMenu(null);
            ARG9N2.setName("ARG9N2");
            panel1.add(ARG9N2);
            ARG9N2.setBounds(205, 55, 40, ARG9N2.getPreferredSize().height);

            //---- ARG39A ----
            ARG39A.setName("ARG39A");
            panel1.add(ARG39A);
            ARG39A.setBounds(145, 20, 160, ARG39A.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Nom du chantier");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(20, 22, 120, 25);
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(20, 40, 570, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(595, 70, 25, 95);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(595, 180, 25, 95);

            //---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_111ActionPerformed(e);
              }
            });
            panel2.add(BT_DEB);
            BT_DEB.setBounds(595, 40, 25, 30);

            //---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_129ActionPerformed(e);
              }
            });
            panel2.add(BT_FIN);
            BT_FIN.setBounds(595, 280, 25, 30);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(44, 44, 44)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 340, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- trans_bons ----
      trans_bons.setText("Dupliquer le chantier");
      trans_bons.setName("trans_bons");
      trans_bons.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          trans_bonsActionPerformed(e);
        }
      });
      BTD.add(trans_bons);

      //---- OBJ_40 ----
      OBJ_40.setText("Afficher");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      BTD.add(OBJ_40);

      //---- OBJ_34 ----
      OBJ_34.setText("Edition");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_67 ----
      OBJ_67.setText("Exportation tableur");
      OBJ_67.setName("OBJ_67");
      OBJ_67.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_67);

      //---- OBJ_37 ----
      OBJ_37.setText("D\u00e9verrouillage");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);
    }

    //---- WSUIS ----
    WSUIS.setComponentPopupMenu(BTD);
    WSUIS.setName("WSUIS");

    //---- WP2 ----
    WP2.setComponentPopupMenu(BTD);
    WP2.setName("WP2");

    //---- TRIAGE2 ----
    TRIAGE2.setText("Tri\u00e9 par ...");
    TRIAGE2.setName("TRIAGE2");

    //---- OBJ_99 ----
    OBJ_99.setText("\u00e0");
    OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_99.setName("OBJ_99");

    //---- PLA4A_bons ----
    PLA4A_bons.setModel(new DefaultComboBoxModel(new String[] {
      "Attente",
      "Valid\u00e9",
      "R\u00e9serv\u00e9",
      "Exp\u00e9di\u00e9",
      "Factur\u00e9",
      "Comptabilis\u00e9"
    }));
    PLA4A_bons.setComponentPopupMenu(BTD);
    PLA4A_bons.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    PLA4A_bons.setName("PLA4A_bons");

    //---- PLA4A_devis ----
    PLA4A_devis.setModel(new DefaultComboBoxModel(new String[] {
      "Attente",
      "Valid\u00e9",
      "Envoy\u00e9",
      "Sign\u00e9",
      "Validit\u00e9 d\u00e9pass\u00e9e",
      "Perdu",
      "Clotur\u00e9",
      "Tous"
    }));
    PLA4A_devis.setComponentPopupMenu(BTD);
    PLA4A_devis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    PLA4A_devis.setName("PLA4A_devis");

    //---- ARG4A_devis ----
    ARG4A_devis.setModel(new DefaultComboBoxModel(new String[] {
      "Attente",
      "Valid\u00e9",
      "Envoy\u00e9",
      "Sign\u00e9",
      "Validit\u00e9 d\u00e9pass\u00e9e",
      "Perdu",
      "Clotur\u00e9",
      "Tous"
    }));
    ARG4A_devis.setComponentPopupMenu(BTD);
    ARG4A_devis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG4A_devis.setName("ARG4A_devis");
    ARG4A_devis.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ARG4A_devisActionPerformed(e);
      }
    });

    //---- ARG4A_bons ----
    ARG4A_bons.setModel(new DefaultComboBoxModel(new String[] {
      "Attente",
      "Valid\u00e9",
      "R\u00e9serv\u00e9",
      "Exp\u00e9di\u00e9",
      "Factur\u00e9",
      "Comptabilis\u00e9",
      "Tous"
    }));
    ARG4A_bons.setComponentPopupMenu(BTD);
    ARG4A_bons.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG4A_bons.setName("ARG4A_bons");
    ARG4A_bons.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ARG4A_bonsActionPerformed(e);
      }
    });

    //---- OBJ_93 ----
    OBJ_93.setText("Etats des bons");
    OBJ_93.setName("OBJ_93");

    //---- tous ----
    tous.setText("");
    tous.setToolTipText("Tous");
    tous.setComponentPopupMenu(BTD);
    tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    tous.setName("tous");
    tous.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        tousActionPerformed(e);
      }
    });

    //---- ARG3A ----
    ARG3A.setModel(new DefaultComboBoxModel(new String[] {
      "Bons",
      "Devis"
    }));
    ARG3A.setComponentPopupMenu(BTD);
    ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG3A.setName("ARG3A");
    ARG3A.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        ARG3AActionPerformed(e);
      }
    });

    //---- OBJ_95 ----
    OBJ_95.setText("Type");
    OBJ_95.setName("OBJ_95");

    //---- OBJ_104 ----
    OBJ_104.setText("R\u00e9f\u00e9rence client");
    OBJ_104.setName("OBJ_104");

    //---- OBJ_84 ----
    OBJ_84.setText("Num\u00e9ro d\u00e9but");
    OBJ_84.setName("OBJ_84");

    //---- ARG15A ----
    ARG15A.setComponentPopupMenu(null);
    ARG15A.setName("ARG15A");

    //---- ARG2N ----
    ARG2N.setComponentPopupMenu(null);
    ARG2N.setName("ARG2N");

    //---- ARG36N ----
    ARG36N.setComponentPopupMenu(null);
    ARG36N.setName("ARG36N");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_70;
  private XRiCheckBox VERR;
  private XRiTextField INDNUM;
  private JLabel OBJ_73;
  private RiZoneSortie WETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie lib_client;
  private XRiTextField ARG9N1;
  private JLabel OBJ_87;
  private XRiTextField ARG9N2;
  private XRiTextField ARG39A;
  private JLabel label1;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_DEB;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem trans_bons;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_67;
  private JMenuItem OBJ_37;
  private XRiTextField WSUIS;
  private XRiTextField WP2;
  private JMenuItem TRIAGE2;
  private JLabel OBJ_99;
  private JComboBox PLA4A_bons;
  private JComboBox PLA4A_devis;
  private JComboBox ARG4A_devis;
  private JComboBox ARG4A_bons;
  private JLabel OBJ_93;
  private JCheckBox tous;
  private XRiComboBox ARG3A;
  private JLabel OBJ_95;
  private JLabel OBJ_104;
  private JLabel OBJ_84;
  private XRiTextField ARG15A;
  private XRiTextField ARG2N;
  private XRiTextField ARG36N;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
