//$$david$$ ££13/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_AB extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] ARG13A_Value = { "", "N", "E", };
  private String[] ARG13A_Title = { "", "Normale", "Export" };
  private String[] ARG33A_Value = { "", "I", "D", "O", "R", "W" };
  private String[] ARG33A_Title = { "Autres", "Interne", "Direct usine", "Ouverte", "Retour", "e-Commerce" };
  private String[] PLA4A_Value = { "0", "1", "3", "4", "6", "7", };
  private String[] PLA4A_Title = { "Attente", "Validé", "Réservé", "Expédié", "Facturé", "Comptabilisé" };
  private String[] ARG4A_Value = { "0", "1", "3", "4", "6", "7", "9", };
  private String[] ARG4A_Title = { "Attente", "Validé", "Réservé", "Expédié", "Facturé", "Comptabilisé", "Tous" };
  private String[] ARG3A_Value = { "E", "D", };
  private String[] ARG3A_Title = { "Bons", "Devis" };
  
  private boolean criteresAvances = false;
  private boolean initCriteres = false;
  private boolean appele = false;
  
  // panels
  private ODialog dialog_LOG = null;
  
  /**
   * Constructeur.
   */
  public RGVM13FM_AB(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX18.setValeurs("18", "RBC");
    TIDX19.setValeurs("19", "RBC");
    TIDX17.setValeurs("17", "RBC");
    TIDX27.setValeurs("27", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX26.setValeurs("26", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX24.setValeurs("24", "RBC");
    TIDX31.setValeurs("31", "RBC");
    TIDX23.setValeurs("23", "RBC");
    TIDX22.setValeurs("22", "RBC");
    TIDX41.setValeurs("41", "RBC");
    TIDX40.setValeurs("40", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX20.setValeurs("20", "RBC");
    TIDX12.setValeurs("12", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX7.setValeurs("7", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX36.setValeurs("36", "RBC");
    TIDX2.setValeurs("2", "RBC");
    TIDX55.setValeurs("55", "RBC");
    ARG13A.setValeurs(ARG13A_Value, ARG13A_Title);
    ARG33A.setValeurs(ARG33A_Value, ARG33A_Title);
    PLA4A.setValeurs(PLA4A_Value, PLA4A_Title);
    ARG4A.setValeurs(ARG4A_Value, ARG4A_Title);
    
    VERR.setValeursSelection("1", "0");
    ARG43A.setValeursSelection("X", " ");
    ARG3A.setValeurs(ARG3A_Value, ARG3A_Title);
    DOCSPE.setValeursSelection("1", "");
    SCAN30.setValeurs("E", SCAN30_GRP);
    SCAN30_E.setValeurs("N");
    SCAN30_T.setValeurs(" ");
    SCAN33.setValeurs("E", SCAN33_GRP);
    SCAN33_E.setValeurs("N");
    SCAN33_T.setValeurs(" ");
    ARG57A.setValeursSelection("1", "  ");
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion de @GESTION@ @SOUTIT@")).trim());
    lbTRTAUT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Traitement automatique @TRTAUT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    riSousMenu_crea.setVisible(true);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Etat du bon ou du devis
    ARG4A_tous.setSelected(lexique.HostFieldGetData("ARG4A").equals("9"));
    PLA4A.setVisible(!ARG4A_tous.isSelected());
    OBJ_142.setVisible(!lexique.HostFieldGetData("ARG4A").trim().equals("9"));
    OBJ_176.setVisible(lexique.isPresent("INDNFA"));
    OBJ_43.setVisible(lexique.isPresent("INDNUM"));
    lb_requete.setVisible(interpreteurD.analyseExpression("@AFFREQ@").equals("1"));
    lbTRTAUT.setVisible(!lexique.HostFieldGetData("TRTAUT").trim().isEmpty());
    
    initCriteres = false;
    criteresAvances = lexique.HostFieldGetData("WECRA").trim().equals("N");
    switchAffichages();
    
    // Gestion bouton de sortie
    appele = lexique.HostFieldGetData("APPELE").trim().isEmpty();
    if (appele) {
      bouton_retour.setText("Sortir");
    }
    else {
      bouton_retour.setText("Retour");
    }
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    switchAffichages();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    if (appele) {
      lexique.HostScreenSendKey(this, "F3");
    }
    else {
      lexique.HostScreenSendKey(this, "F12");
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void switchAffichages() {
    // Si on a pas initialisé les critères on le fait sinon on inverse les critères
    if (initCriteres) {
      criteresAvances = !criteresAvances;
    }
    else {
      initCriteres = true;
    }
    
    TIDX10.setVisible(criteresAvances);
    ARG10A.setVisible(criteresAvances);
    TIDX12.setVisible(criteresAvances);
    ARG12A.setVisible(criteresAvances);
    OBJ_156.setVisible(criteresAvances);
    ARG28A.setVisible(criteresAvances);
    OBJ_157.setVisible(criteresAvances);
    ARG11A.setVisible(criteresAvances);
    OBJ_161.setVisible(criteresAvances);
    ARG37A.setVisible(criteresAvances);
    OBJ_162.setVisible(criteresAvances);
    ARG56A.setVisible(criteresAvances);
    TIDX26.setVisible(criteresAvances);
    ARG26A.setVisible(criteresAvances);
    TIDX17.setVisible(criteresAvances);
    TIDX18.setVisible(criteresAvances);
    TIDX19.setVisible(criteresAvances);
    ARG17A.setVisible(criteresAvances);
    ARG18A.setVisible(criteresAvances);
    ARG19A.setVisible(criteresAvances);
    TIDX27.setVisible(criteresAvances);
    ARG27A.setVisible(criteresAvances);
    OBJ_102.setVisible(criteresAvances);
    ARG38A.setVisible(criteresAvances);
    OBJ_108.setVisible(criteresAvances);
    ARG32A.setVisible(criteresAvances);
    OBJ_39.setVisible(criteresAvances);
    ARG13A.setVisible(criteresAvances);
    panel5.setVisible(criteresAvances);
    OBJ_142.setVisible(criteresAvances);
    OBJ_39.setVisible(criteresAvances);
    ARG13A.setVisible(criteresAvances);
    riSousMenu2.setVisible(criteresAvances);
    
    // Si on est en mode critères avancés
    if (criteresAvances) {
      riSousMenu_bt6.setText("Critères principaux");
      lexique.HostFieldPutData("WECRA", 0, "N");
    }
    // Si on est en mode critères simplifiés
    else {
      riSousMenu_bt6.setText("Tous les critères");
      lexique.HostFieldPutData("WECRA", 0, "S");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void ARG4A_tousActionPerformed(ActionEvent e) {
    if (ARG4A_tous.isSelected()) {
      ARG4A.setSelectedIndex(6);
    }
    else {
      ARG4A.setSelectedIndex(0);
    }
    
    OBJ_142.setVisible(!ARG4A_tous.isSelected());
    PLA4A.setVisible(!ARG4A_tous.isSelected());
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    ARG4A_tous.setSelected(ARG4A.getSelectedIndex() == 6);
    OBJ_142.setVisible(!ARG4A_tous.isSelected());
    PLA4A.setVisible(!ARG4A_tous.isSelected());
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    if (interpreteurD.analyseExpression("@AFFREQ@").equalsIgnoreCase("1")) {
      lexique.HostFieldPutData("AFFREQ", 0, " ");
      lb_requete.setVisible(false);
    }
    else {
      lexique.HostFieldPutData("AFFREQ", 0, "1");
      lb_requete.setVisible(true);
    }
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void ARG3AActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    if (dialog_LOG == null) {
      dialog_LOG = new ODialog((Window) getTopLevelAncestor(), new RGVM13FM_LOG(this));
    }
    dialog_LOG.affichePopupPerso();
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void riSousMenu_bt4ActionPerformed(ActionEvent e) {
    try {
      INDNUM.setText("999999");
      lexique.HostScreenSendKey(this, "ENTER");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void riSousMenu_bt5ActionPerformed(ActionEvent e) {
    try {
      lexique.HostCursorPut("INDNUM");
      lexique.HostScreenSendKey(this, "F4");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_41 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_176 = new JLabel();
    INDNUM = new XRiTextField();
    INDETB = new XRiTextField();
    INDSUF = new XRiTextField();
    INDNFA = new XRiTextField();
    WDAT1X = new XRiCalendrier();
    VERR = new XRiCheckBox();
    OBJ_95 = new JLabel();
    ARG3A = new XRiComboBox();
    OBJ_40 = new JLabel();
    p_tete_droite = new JPanel();
    lbTRTAUT = new JLabel();
    lb_requete = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu4 = new RiSousMenu();
    riSousMenu_bt4 = new RiSousMenu_bt();
    riSousMenu5 = new RiSousMenu();
    riSousMenu_bt5 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    ARG4A_tous = new JCheckBox();
    ARG4A = new XRiComboBox();
    TIDX2 = new XRiRadioButton();
    ARG2N = new XRiTextField();
    PLA2N = new XRiTextField();
    TIDX36 = new XRiRadioButton();
    ARG36N = new XRiTextField();
    PLA36N = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG5A = new XRiTextField();
    OBJ_65 = new JLabel();
    OBJ_142 = new JLabel();
    PLA4A = new XRiComboBox();
    ARG57A = new XRiCheckBox();
    OBJ_146 = new JLabel();
    OBJ_148 = new JLabel();
    OBJ_162 = new JLabel();
    ARG56A = new XRiTextField();
    TIDX9 = new XRiRadioButton();
    ARG9N1 = new XRiTextField();
    ARG9N2 = new XRiTextField();
    ARGDOC = new XRiTextField();
    OBJ_66 = new JLabel();
    TIDX6 = new XRiRadioButton();
    TIDX7 = new XRiRadioButton();
    ARG7A = new XRiTextField();
    ARG6A = new XRiTextField();
    OBJ_155 = new JLabel();
    OBJ_151 = new JLabel();
    ARG29A = new XRiTextField();
    ARG8A = new XRiTextField();
    TIDX10 = new XRiRadioButton();
    ARG10A = new XRiTextField();
    TIDX12 = new XRiRadioButton();
    ARG12A = new XRiTextField();
    ARG28A = new XRiTextField();
    ARG37A = new XRiTextField();
    OBJ_156 = new JLabel();
    OBJ_157 = new JLabel();
    ARG11A = new XRiTextField();
    OBJ_161 = new JLabel();
    panel6 = new JPanel();
    WNOM = new XRiTextField();
    WCDP = new XRiTextField();
    WTEL = new XRiTextField();
    ARG39A = new XRiTextField();
    OBJ_149 = new JLabel();
    OBJ_147 = new JLabel();
    OBJ_144 = new JLabel();
    OBJ_152 = new JLabel();
    WCLK = new XRiTextField();
    OBJ_150 = new JLabel();
    panel2 = new JPanel();
    ARG33A = new XRiComboBox();
    ARG13A = new XRiComboBox();
    OBJ_177 = new JLabel();
    OBJ_39 = new JLabel();
    SCAN33 = new XRiRadioButton();
    SCAN33_E = new XRiRadioButton();
    SCAN33_T = new XRiRadioButton();
    panel5 = new JPanel();
    TIDX20 = new XRiRadioButton();
    ARG20D = new XRiCalendrier();
    PLA20D = new XRiCalendrier();
    TIDX21 = new XRiRadioButton();
    ARG21D = new XRiCalendrier();
    PLA21D = new XRiCalendrier();
    TIDX40 = new XRiRadioButton();
    ARG40D = new XRiCalendrier();
    PLA40D = new XRiCalendrier();
    TIDX41 = new XRiRadioButton();
    ARG41D = new XRiCalendrier();
    PLA41D = new XRiCalendrier();
    TIDX22 = new XRiRadioButton();
    ARG22D = new XRiCalendrier();
    PLA22D = new XRiCalendrier();
    TIDX23 = new XRiRadioButton();
    ARG23D = new XRiCalendrier();
    PLA23D = new XRiCalendrier();
    TIDX31 = new XRiRadioButton();
    ARG31N = new XRiTextField();
    PLA31N = new XRiTextField();
    TIDX24 = new XRiRadioButton();
    ARG24D = new XRiTextField();
    TIDX14 = new XRiRadioButton();
    ARG14A = new XRiTextField();
    OBJ_153 = new JLabel();
    OBJ_154 = new JLabel();
    label1 = new JLabel();
    ARG43A = new XRiCheckBox();
    TIDX55 = new XRiRadioButton();
    ARG55N = new XRiTextField();
    label2 = new JLabel();
    PLA55N = new XRiTextField();
    DOCSPE = new XRiCheckBox();
    OBJ_160 = new JLabel();
    ARG30A = new XRiTextField();
    SCAN30 = new XRiRadioButton();
    SCAN30_E = new XRiRadioButton();
    SCAN30_T = new XRiRadioButton();
    panel4 = new JPanel();
    TIDX27 = new XRiRadioButton();
    OBJ_108 = new JLabel();
    OBJ_102 = new JLabel();
    TIDX17 = new XRiRadioButton();
    ARG17A = new XRiTextField();
    TIDX19 = new XRiRadioButton();
    ARG19A = new XRiTextField();
    ARG38A = new XRiTextField();
    TIDX18 = new XRiRadioButton();
    ARG18A = new XRiTextField();
    ARG27A = new XRiTextField();
    ARG32A = new XRiTextField();
    TIDX15 = new XRiRadioButton();
    TIDX16 = new XRiRadioButton();
    ARG16A = new XRiTextField();
    ARG15A = new XRiTextField();
    TIDX26 = new XRiRadioButton();
    ARG26A = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    ARG10A2 = new XRiComboBox();
    SCAN33_GRP = new ButtonGroup();
    SCAN30_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(1125, 705));
    setPreferredSize(new Dimension(1105, 695));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de @GESTION@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_41 ----
          OBJ_41.setText("Etablissement");
          OBJ_41.setName("OBJ_41");

          //---- OBJ_43 ----
          OBJ_43.setText("Num\u00e9ro");
          OBJ_43.setName("OBJ_43");

          //---- OBJ_176 ----
          OBJ_176.setText("Facture");
          OBJ_176.setName("OBJ_176");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(null);
          INDETB.setName("INDETB");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setName("INDSUF");

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(null);
          INDNFA.setName("INDNFA");

          //---- WDAT1X ----
          WDAT1X.setName("WDAT1X");

          //---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(null);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");

          //---- OBJ_95 ----
          OBJ_95.setText("Type");
          OBJ_95.setName("OBJ_95");

          //---- ARG3A ----
          ARG3A.setModel(new DefaultComboBoxModel(new String[] {
            "Bons",
            "Devis"
          }));
          ARG3A.setComponentPopupMenu(BTD);
          ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG3A.setName("ARG3A");
          ARG3A.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ARG3AActionPerformed(e);
            }
          });

          //---- OBJ_40 ----
          OBJ_40.setText("Traitement");
          OBJ_40.setName("OBJ_40");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_176, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(65, 65, 65)
                    .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                    .addGap(272, 272, 272))
                  .addGroup(GroupLayout.Alignment.TRAILING, p_tete_gaucheLayout.createSequentialGroup()
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(OBJ_40, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                    .addGap(32, 32, 32)
                    .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 49, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(188, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_176, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_40)
                  .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lbTRTAUT ----
          lbTRTAUT.setText("Traitement automatique @TRTAUT@");
          lbTRTAUT.setHorizontalAlignment(SwingConstants.RIGHT);
          lbTRTAUT.setName("lbTRTAUT");
          p_tete_droite.add(lbTRTAUT);

          //---- lb_requete ----
          lb_requete.setText("Affichage requ\u00eate SQL");
          lb_requete.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_requete.setFont(lb_requete.getFont().deriveFont(lb_requete.getFont().getStyle() | Font.BOLD));
          lb_requete.setName("lb_requete");
          p_tete_droite.add(lb_requete);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Crit\u00e8res");
              riSousMenu_bt6.setToolTipText("changer le mode de recherche");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");

              //---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Crit\u00e8res logistiques");
              riSousMenu_bt2.setToolTipText("Crit\u00e8res de recherche li\u00e9s \u00e0 la logistique");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Visualisation requ\u00eate");
              riSousMenu_bt1.setToolTipText("Visualisation requ\u00eate SQL");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");

              //---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Traitement automatique");
              riSousMenu_bt3.setToolTipText("Traitement de fin de bon automatique");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);

            //======== riSousMenu4 ========
            {
              riSousMenu4.setName("riSousMenu4");

              //---- riSousMenu_bt4 ----
              riSousMenu_bt4.setText("Dernier document");
              riSousMenu_bt4.setToolTipText("Ouvrir le dernier document consult\u00e9");
              riSousMenu_bt4.setName("riSousMenu_bt4");
              riSousMenu_bt4.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt4ActionPerformed(e);
                }
              });
              riSousMenu4.add(riSousMenu_bt4);
            }
            menus_haut.add(riSousMenu4);

            //======== riSousMenu5 ========
            {
              riSousMenu5.setName("riSousMenu5");

              //---- riSousMenu_bt5 ----
              riSousMenu_bt5.setText("Vos derniers documents");
              riSousMenu_bt5.setToolTipText("Liste des derniers documents consult\u00e9s");
              riSousMenu_bt5.setName("riSousMenu_bt5");
              riSousMenu_bt5.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt5ActionPerformed(e);
                }
              });
              riSousMenu5.add(riSousMenu_bt5);
            }
            menus_haut.add(riSousMenu5);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setAutoscrolls(true);
        p_centrage.setMinimumSize(new Dimension(1200, 610));
        p_centrage.setPreferredSize(new Dimension(1200, 610));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(950, 620));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(925, 620));
          p_contenu.setMaximumSize(new Dimension(925, 610));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {451, 420, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {180, 84, 198, 184, 0, 0, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0, 0.9, 1.0E-4};

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder(""));
            panel3.setOpaque(false);
            panel3.setMinimumSize(new Dimension(521, 210));
            panel3.setPreferredSize(new Dimension(521, 460));
            panel3.setName("panel3");
            panel3.setLayout(new GridBagLayout());
            ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0, 73, 63, 35, 0, 30, 64, 0, 0};
            ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {21, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 25, 0, 0};
            ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

            //---- ARG4A_tous ----
            ARG4A_tous.setText("");
            ARG4A_tous.setToolTipText("Tous");
            ARG4A_tous.setComponentPopupMenu(BTD);
            ARG4A_tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A_tous.setMinimumSize(new Dimension(18, 28));
            ARG4A_tous.setName("ARG4A_tous");
            ARG4A_tous.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ARG4A_tousActionPerformed(e);
              }
            });
            panel3.add(ARG4A_tous, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(null);
            ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A.setPreferredSize(new Dimension(107, 32));
            ARG4A.setName("ARG4A");
            ARG4A.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ARG4AActionPerformed(e);
              }
            });
            panel3.add(ARG4A, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX2 ----
            TIDX2.setText("Num\u00e9ro de bon");
            TIDX2.setComponentPopupMenu(BTD);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setToolTipText("Tri sur cet argument");
            TIDX2.setName("TIDX2");
            panel3.add(TIDX2, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG2N ----
            ARG2N.setComponentPopupMenu(BTD2);
            ARG2N.setMinimumSize(new Dimension(70, 28));
            ARG2N.setPreferredSize(new Dimension(70, 28));
            ARG2N.setName("ARG2N");
            panel3.add(ARG2N, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA2N ----
            PLA2N.setComponentPopupMenu(BTD2);
            PLA2N.setMinimumSize(new Dimension(70, 28));
            PLA2N.setPreferredSize(new Dimension(70, 28));
            PLA2N.setName("PLA2N");
            panel3.add(PLA2N, new GridBagConstraints(5, 2, 3, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX36 ----
            TIDX36.setText("Num\u00e9ro de facture");
            TIDX36.setComponentPopupMenu(BTD);
            TIDX36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX36.setToolTipText("Tri sur cet argument");
            TIDX36.setName("TIDX36");
            panel3.add(TIDX36, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG36N ----
            ARG36N.setComponentPopupMenu(BTD2);
            ARG36N.setMinimumSize(new Dimension(70, 28));
            ARG36N.setPreferredSize(new Dimension(70, 28));
            ARG36N.setName("ARG36N");
            panel3.add(ARG36N, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA36N ----
            PLA36N.setComponentPopupMenu(BTD2);
            PLA36N.setMinimumSize(new Dimension(70, 28));
            PLA36N.setPreferredSize(new Dimension(70, 28));
            PLA36N.setName("PLA36N");
            panel3.add(PLA36N, new GridBagConstraints(5, 3, 3, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX5 ----
            TIDX5.setText("Magasin");
            TIDX5.setComponentPopupMenu(BTD);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setToolTipText("Tri sur cet argument");
            TIDX5.setName("TIDX5");
            panel3.add(TIDX5, new GridBagConstraints(0, 4, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD);
            ARG5A.setPreferredSize(new Dimension(40, 28));
            ARG5A.setMinimumSize(new Dimension(40, 28));
            ARG5A.setName("ARG5A");
            panel3.add(ARG5A, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_65 ----
            OBJ_65.setText("Etats des bons");
            OBJ_65.setMinimumSize(new Dimension(83, 28));
            OBJ_65.setName("OBJ_65");
            panel3.add(OBJ_65, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_142 ----
            OBJ_142.setText("\u00e0");
            OBJ_142.setName("OBJ_142");
            panel3.add(OBJ_142, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA4A ----
            PLA4A.setPreferredSize(new Dimension(67, 32));
            PLA4A.setName("PLA4A");
            panel3.add(PLA4A, new GridBagConstraints(5, 0, 3, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG57A ----
            ARG57A.setText("Reliquat");
            ARG57A.setMaximumSize(new Dimension(80, 30));
            ARG57A.setMinimumSize(new Dimension(80, 30));
            ARG57A.setPreferredSize(new Dimension(80, 30));
            ARG57A.setName("ARG57A");
            panel3.add(ARG57A, new GridBagConstraints(8, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- OBJ_146 ----
            OBJ_146.setText("\u00e0");
            OBJ_146.setName("OBJ_146");
            panel3.add(OBJ_146, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_148 ----
            OBJ_148.setText("\u00e0");
            OBJ_148.setName("OBJ_148");
            panel3.add(OBJ_148, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_162 ----
            OBJ_162.setText("Libell\u00e9 saisi sur ligne");
            OBJ_162.setName("OBJ_162");
            panel3.add(OBJ_162, new GridBagConstraints(0, 12, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG56A ----
            ARG56A.setComponentPopupMenu(BTD2);
            ARG56A.setMinimumSize(new Dimension(260, 28));
            ARG56A.setPreferredSize(new Dimension(260, 28));
            ARG56A.setName("ARG56A");
            panel3.add(ARG56A, new GridBagConstraints(2, 12, 6, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX9 ----
            TIDX9.setText("Num\u00e9ro du client livr\u00e9");
            TIDX9.setComponentPopupMenu(BTD);
            TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX9.setToolTipText("Tri sur cet argument");
            TIDX9.setName("TIDX9");
            panel3.add(TIDX9, new GridBagConstraints(0, 5, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG9N1 ----
            ARG9N1.setComponentPopupMenu(BTD2);
            ARG9N1.setHorizontalAlignment(SwingConstants.RIGHT);
            ARG9N1.setMinimumSize(new Dimension(60, 28));
            ARG9N1.setPreferredSize(new Dimension(60, 28));
            ARG9N1.setName("ARG9N1");
            panel3.add(ARG9N1, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG9N2 ----
            ARG9N2.setComponentPopupMenu(BTD2);
            ARG9N2.setMinimumSize(new Dimension(35, 28));
            ARG9N2.setPreferredSize(new Dimension(35, 28));
            ARG9N2.setName("ARG9N2");
            panel3.add(ARG9N2, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARGDOC ----
            ARGDOC.setComponentPopupMenu(null);
            ARGDOC.setMinimumSize(new Dimension(260, 28));
            ARGDOC.setPreferredSize(new Dimension(260, 28));
            ARGDOC.setName("ARGDOC");
            panel3.add(ARGDOC, new GridBagConstraints(2, 1, 6, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_66 ----
            OBJ_66.setText("Recherche document");
            OBJ_66.setName("OBJ_66");
            panel3.add(OBJ_66, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX6 ----
            TIDX6.setText("Vendeur");
            TIDX6.setComponentPopupMenu(BTD);
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setToolTipText("Tri sur cet argument");
            TIDX6.setName("TIDX6");
            panel3.add(TIDX6, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX7 ----
            TIDX7.setText("Repr\u00e9sentant 1");
            TIDX7.setComponentPopupMenu(BTD);
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setToolTipText("Tri sur cet argument");
            TIDX7.setName("TIDX7");
            panel3.add(TIDX7, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG7A ----
            ARG7A.setComponentPopupMenu(BTD);
            ARG7A.setPreferredSize(new Dimension(35, 28));
            ARG7A.setMinimumSize(new Dimension(35, 28));
            ARG7A.setName("ARG7A");
            panel3.add(ARG7A, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG6A ----
            ARG6A.setComponentPopupMenu(BTD);
            ARG6A.setPreferredSize(new Dimension(40, 28));
            ARG6A.setMinimumSize(new Dimension(40, 28));
            ARG6A.setName("ARG6A");
            panel3.add(ARG6A, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_155 ----
            OBJ_155.setText("Repr\u00e9sentant 2");
            OBJ_155.setName("OBJ_155");
            panel3.add(OBJ_155, new GridBagConstraints(3, 7, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_151 ----
            OBJ_151.setText("Pr\u00e9parateur");
            OBJ_151.setName("OBJ_151");
            panel3.add(OBJ_151, new GridBagConstraints(3, 6, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG29A ----
            ARG29A.setComponentPopupMenu(BTD);
            ARG29A.setMinimumSize(new Dimension(40, 28));
            ARG29A.setPreferredSize(new Dimension(40, 28));
            ARG29A.setName("ARG29A");
            panel3.add(ARG29A, new GridBagConstraints(5, 6, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG8A ----
            ARG8A.setComponentPopupMenu(BTD);
            ARG8A.setPreferredSize(new Dimension(35, 28));
            ARG8A.setMinimumSize(new Dimension(35, 28));
            ARG8A.setName("ARG8A");
            panel3.add(ARG8A, new GridBagConstraints(5, 7, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX10 ----
            TIDX10.setText("Mode d'exp\u00e9dition");
            TIDX10.setComponentPopupMenu(BTD);
            TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX10.setToolTipText("Tri sur cet argument");
            TIDX10.setName("TIDX10");
            panel3.add(TIDX10, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG10A ----
            ARG10A.setComponentPopupMenu(BTD);
            ARG10A.setPreferredSize(new Dimension(35, 28));
            ARG10A.setMinimumSize(new Dimension(35, 28));
            ARG10A.setName("ARG10A");
            panel3.add(ARG10A, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX12 ----
            TIDX12.setText("Zone g\u00e9ographique");
            TIDX12.setComponentPopupMenu(BTD);
            TIDX12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX12.setToolTipText("Tri sur cet argument");
            TIDX12.setName("TIDX12");
            panel3.add(TIDX12, new GridBagConstraints(0, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG12A ----
            ARG12A.setComponentPopupMenu(BTD);
            ARG12A.setMinimumSize(new Dimension(60, 28));
            ARG12A.setPreferredSize(new Dimension(60, 28));
            ARG12A.setName("ARG12A");
            panel3.add(ARG12A, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG28A ----
            ARG28A.setComponentPopupMenu(BTD2);
            ARG28A.setPreferredSize(new Dimension(60, 28));
            ARG28A.setMinimumSize(new Dimension(60, 28));
            ARG28A.setName("ARG28A");
            panel3.add(ARG28A, new GridBagConstraints(2, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG37A ----
            ARG37A.setComponentPopupMenu(BTD2);
            ARG37A.setPreferredSize(new Dimension(220, 28));
            ARG37A.setMinimumSize(new Dimension(220, 28));
            ARG37A.setName("ARG37A");
            panel3.add(ARG37A, new GridBagConstraints(2, 11, 6, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_156 ----
            OBJ_156.setText("Regroupement transport");
            OBJ_156.setName("OBJ_156");
            panel3.add(OBJ_156, new GridBagConstraints(0, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_157 ----
            OBJ_157.setText("Transporteur");
            OBJ_157.setName("OBJ_157");
            panel3.add(OBJ_157, new GridBagConstraints(3, 10, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG11A ----
            ARG11A.setComponentPopupMenu(BTD);
            ARG11A.setMinimumSize(new Dimension(35, 28));
            ARG11A.setPreferredSize(new Dimension(35, 28));
            ARG11A.setName("ARG11A");
            panel3.add(ARG11A, new GridBagConstraints(5, 10, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_161 ----
            OBJ_161.setText("Article");
            OBJ_161.setName("OBJ_161");
            panel3.add(OBJ_161, new GridBagConstraints(0, 11, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));
          }
          p_contenu.add(panel3, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(5, 5, 5, 5), 0, 0));

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder("Recherche client"));
            panel6.setOpaque(false);
            panel6.setMinimumSize(new Dimension(443, 160));
            panel6.setName("panel6");
            panel6.setLayout(new GridBagLayout());
            ((GridBagLayout)panel6.getLayout()).columnWidths = new int[] {141, 187, 77, 0};
            ((GridBagLayout)panel6.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)panel6.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel6.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

            //---- WNOM ----
            WNOM.setComponentPopupMenu(BTD2);
            WNOM.setPreferredSize(new Dimension(160, 26));
            WNOM.setMinimumSize(new Dimension(160, 26));
            WNOM.setName("WNOM");
            panel6.add(WNOM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- WCDP ----
            WCDP.setComponentPopupMenu(BTD2);
            WCDP.setMinimumSize(new Dimension(55, 26));
            WCDP.setPreferredSize(new Dimension(55, 26));
            WCDP.setName("WCDP");
            panel6.add(WCDP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- WTEL ----
            WTEL.setComponentPopupMenu(BTD2);
            WTEL.setPreferredSize(new Dimension(120, 26));
            WTEL.setMinimumSize(new Dimension(120, 26));
            WTEL.setName("WTEL");
            panel6.add(WTEL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- ARG39A ----
            ARG39A.setComponentPopupMenu(BTD2);
            ARG39A.setPreferredSize(new Dimension(160, 26));
            ARG39A.setMinimumSize(new Dimension(160, 26));
            ARG39A.setName("ARG39A");
            panel6.add(ARG39A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_149 ----
            OBJ_149.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
            OBJ_149.setName("OBJ_149");
            panel6.add(OBJ_149, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_147 ----
            OBJ_147.setText("Code postal");
            OBJ_147.setName("OBJ_147");
            panel6.add(OBJ_147, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_144 ----
            OBJ_144.setText("Nom");
            OBJ_144.setName("OBJ_144");
            panel6.add(OBJ_144, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_152 ----
            OBJ_152.setText("Mot de classement");
            OBJ_152.setName("OBJ_152");
            panel6.add(OBJ_152, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- WCLK ----
            WCLK.setComponentPopupMenu(BTD2);
            WCLK.setPreferredSize(new Dimension(160, 26));
            WCLK.setMinimumSize(new Dimension(160, 26));
            WCLK.setName("WCLK");
            panel6.add(WCLK, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_150 ----
            OBJ_150.setText("Nom saisi");
            OBJ_150.setName("OBJ_150");
            panel6.add(OBJ_150, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel6, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setBorder(new TitledBorder(""));
            panel2.setMinimumSize(new Dimension(433, 50));
            panel2.setPreferredSize(new Dimension(433, 80));
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {108, 139, 0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0, 1.0E-4};

            //---- ARG33A ----
            ARG33A.setComponentPopupMenu(null);
            ARG33A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG33A.setName("ARG33A");
            panel2.add(ARG33A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG13A ----
            ARG13A.setComponentPopupMenu(null);
            ARG13A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG13A.setName("ARG13A");
            panel2.add(ARG13A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_177 ----
            OBJ_177.setText("Type commande");
            OBJ_177.setName("OBJ_177");
            panel2.add(OBJ_177, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_39 ----
            OBJ_39.setText("Facturation");
            OBJ_39.setName("OBJ_39");
            panel2.add(OBJ_39, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- SCAN33 ----
            SCAN33.setText("Inclus");
            SCAN33.setComponentPopupMenu(null);
            SCAN33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN33.setName("SCAN33");
            panel2.add(SCAN33, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- SCAN33_E ----
            SCAN33_E.setText("Exclu");
            SCAN33_E.setComponentPopupMenu(null);
            SCAN33_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN33_E.setName("SCAN33_E");
            panel2.add(SCAN33_E, new GridBagConstraints(3, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- SCAN33_T ----
            SCAN33_T.setText("Tous");
            SCAN33_T.setComponentPopupMenu(null);
            SCAN33_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN33_T.setName("SCAN33_T");
            panel2.add(SCAN33_T, new GridBagConstraints(4, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          p_contenu.add(panel2, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder(""));
            panel5.setOpaque(false);
            panel5.setMinimumSize(new Dimension(433, 160));
            panel5.setPreferredSize(new Dimension(433, 160));
            panel5.setName("panel5");
            panel5.setLayout(null);

            //---- TIDX20 ----
            TIDX20.setText("Date de cr\u00e9ation");
            TIDX20.setComponentPopupMenu(BTD);
            TIDX20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX20.setToolTipText("Tri sur cet argument");
            TIDX20.setName("TIDX20");
            panel5.add(TIDX20);
            TIDX20.setBounds(20, 24, 122, 20);

            //---- ARG20D ----
            ARG20D.setComponentPopupMenu(BTD2);
            ARG20D.setName("ARG20D");
            panel5.add(ARG20D);
            ARG20D.setBounds(160, 20, 105, ARG20D.getPreferredSize().height);

            //---- PLA20D ----
            PLA20D.setComponentPopupMenu(BTD2);
            PLA20D.setName("PLA20D");
            panel5.add(PLA20D);
            PLA20D.setBounds(285, 20, 105, PLA20D.getPreferredSize().height);

            //---- TIDX21 ----
            TIDX21.setText("Validation");
            TIDX21.setComponentPopupMenu(BTD);
            TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX21.setToolTipText("Tri sur cet argument");
            TIDX21.setName("TIDX21");
            panel5.add(TIDX21);
            TIDX21.setBounds(20, 53, 122, 20);

            //---- ARG21D ----
            ARG21D.setComponentPopupMenu(BTD2);
            ARG21D.setName("ARG21D");
            panel5.add(ARG21D);
            ARG21D.setBounds(160, 49, 105, ARG21D.getPreferredSize().height);

            //---- PLA21D ----
            PLA21D.setComponentPopupMenu(BTD2);
            PLA21D.setName("PLA21D");
            panel5.add(PLA21D);
            PLA21D.setBounds(285, 49, 105, PLA21D.getPreferredSize().height);

            //---- TIDX40 ----
            TIDX40.setText("Livraison pr\u00e9vue");
            TIDX40.setComponentPopupMenu(BTD);
            TIDX40.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX40.setToolTipText("Tri sur cet argument");
            TIDX40.setName("TIDX40");
            panel5.add(TIDX40);
            TIDX40.setBounds(20, 82, 122, 20);

            //---- ARG40D ----
            ARG40D.setComponentPopupMenu(BTD2);
            ARG40D.setName("ARG40D");
            panel5.add(ARG40D);
            ARG40D.setBounds(160, 78, 105, ARG40D.getPreferredSize().height);

            //---- PLA40D ----
            PLA40D.setComponentPopupMenu(BTD2);
            PLA40D.setName("PLA40D");
            panel5.add(PLA40D);
            PLA40D.setBounds(285, 78, 105, PLA40D.getPreferredSize().height);

            //---- TIDX41 ----
            TIDX41.setText("Livraison souhait\u00e9e");
            TIDX41.setComponentPopupMenu(BTD);
            TIDX41.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX41.setToolTipText("Tri sur cet argument");
            TIDX41.setName("TIDX41");
            panel5.add(TIDX41);
            TIDX41.setBounds(20, 111, 141, 20);

            //---- ARG41D ----
            ARG41D.setComponentPopupMenu(BTD2);
            ARG41D.setName("ARG41D");
            panel5.add(ARG41D);
            ARG41D.setBounds(160, 107, 105, ARG41D.getPreferredSize().height);

            //---- PLA41D ----
            PLA41D.setComponentPopupMenu(BTD2);
            PLA41D.setName("PLA41D");
            panel5.add(PLA41D);
            PLA41D.setBounds(285, 107, 105, PLA41D.getPreferredSize().height);

            //---- TIDX22 ----
            TIDX22.setText("Exp\u00e9dition");
            TIDX22.setComponentPopupMenu(BTD);
            TIDX22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX22.setToolTipText("Tri sur cet argument");
            TIDX22.setName("TIDX22");
            panel5.add(TIDX22);
            TIDX22.setBounds(20, 140, 122, 20);

            //---- ARG22D ----
            ARG22D.setComponentPopupMenu(BTD2);
            ARG22D.setName("ARG22D");
            panel5.add(ARG22D);
            ARG22D.setBounds(160, 136, 105, ARG22D.getPreferredSize().height);

            //---- PLA22D ----
            PLA22D.setComponentPopupMenu(BTD2);
            PLA22D.setName("PLA22D");
            panel5.add(PLA22D);
            PLA22D.setBounds(285, 136, 105, PLA22D.getPreferredSize().height);

            //---- TIDX23 ----
            TIDX23.setText("Facturation");
            TIDX23.setComponentPopupMenu(BTD);
            TIDX23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX23.setToolTipText("Tri sur cet argument");
            TIDX23.setName("TIDX23");
            panel5.add(TIDX23);
            TIDX23.setBounds(20, 169, 122, 20);

            //---- ARG23D ----
            ARG23D.setComponentPopupMenu(BTD2);
            ARG23D.setName("ARG23D");
            panel5.add(ARG23D);
            ARG23D.setBounds(160, 165, 105, ARG23D.getPreferredSize().height);

            //---- PLA23D ----
            PLA23D.setComponentPopupMenu(BTD2);
            PLA23D.setName("PLA23D");
            panel5.add(PLA23D);
            PLA23D.setBounds(285, 165, 105, PLA23D.getPreferredSize().height);

            //---- TIDX31 ----
            TIDX31.setText("Montant HT");
            TIDX31.setComponentPopupMenu(BTD);
            TIDX31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX31.setToolTipText("Tri sur cet argument");
            TIDX31.setName("TIDX31");
            panel5.add(TIDX31);
            TIDX31.setBounds(20, 198, 105, 20);

            //---- ARG31N ----
            ARG31N.setComponentPopupMenu(BTD2);
            ARG31N.setName("ARG31N");
            panel5.add(ARG31N);
            ARG31N.setBounds(160, 194, 100, ARG31N.getPreferredSize().height);

            //---- PLA31N ----
            PLA31N.setComponentPopupMenu(BTD2);
            PLA31N.setName("PLA31N");
            panel5.add(PLA31N);
            PLA31N.setBounds(285, 194, 100, PLA31N.getPreferredSize().height);

            //---- TIDX24 ----
            TIDX24.setText("Facture minimum");
            TIDX24.setComponentPopupMenu(BTD);
            TIDX24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX24.setToolTipText("Tri sur cet argument");
            TIDX24.setName("TIDX24");
            panel5.add(TIDX24);
            TIDX24.setBounds(20, 256, 122, 20);

            //---- ARG24D ----
            ARG24D.setComponentPopupMenu(BTD2);
            ARG24D.setName("ARG24D");
            panel5.add(ARG24D);
            ARG24D.setBounds(160, 252, 90, ARG24D.getPreferredSize().height);

            //---- TIDX14 ----
            TIDX14.setText("Devise");
            TIDX14.setComponentPopupMenu(BTD);
            TIDX14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX14.setToolTipText("Tri sur cet argument");
            TIDX14.setName("TIDX14");
            panel5.add(TIDX14);
            TIDX14.setBounds(20, 304, 122, 20);

            //---- ARG14A ----
            ARG14A.setComponentPopupMenu(BTD);
            ARG14A.setName("ARG14A");
            panel5.add(ARG14A);
            ARG14A.setBounds(160, 300, 40, ARG14A.getPreferredSize().height);

            //---- OBJ_153 ----
            OBJ_153.setText("du");
            OBJ_153.setName("OBJ_153");
            panel5.add(OBJ_153);
            OBJ_153.setBounds(165, 5, 22, 13);

            //---- OBJ_154 ----
            OBJ_154.setText("au");
            OBJ_154.setName("OBJ_154");
            panel5.add(OBJ_154);
            OBJ_154.setBounds(285, 5, 22, 13);

            //---- label1 ----
            label1.setText("\u00e0");
            label1.setHorizontalTextPosition(SwingConstants.CENTER);
            label1.setName("label1");
            panel5.add(label1);
            label1.setBounds(265, 200, 25, label1.getPreferredSize().height);

            //---- ARG43A ----
            ARG43A.setText("Factures non r\u00e9gl\u00e9es clients comptants");
            ARG43A.setName("ARG43A");
            panel5.add(ARG43A);
            ARG43A.setBounds(160, 281, 285, ARG43A.getPreferredSize().height);

            //---- TIDX55 ----
            TIDX55.setText("Montant TTC");
            TIDX55.setComponentPopupMenu(BTD);
            TIDX55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX55.setToolTipText("Tri sur cet argument");
            TIDX55.setName("TIDX55");
            panel5.add(TIDX55);
            TIDX55.setBounds(20, 227, 105, 20);

            //---- ARG55N ----
            ARG55N.setComponentPopupMenu(BTD2);
            ARG55N.setName("ARG55N");
            panel5.add(ARG55N);
            ARG55N.setBounds(160, 223, 100, 28);

            //---- label2 ----
            label2.setText("\u00e0");
            label2.setHorizontalTextPosition(SwingConstants.CENTER);
            label2.setName("label2");
            panel5.add(label2);
            label2.setBounds(265, 229, 25, 16);

            //---- PLA55N ----
            PLA55N.setComponentPopupMenu(BTD2);
            PLA55N.setName("PLA55N");
            panel5.add(PLA55N);
            PLA55N.setBounds(285, 223, 100, 28);

            //---- DOCSPE ----
            DOCSPE.setText("Contient des articles sp\u00e9ciaux");
            DOCSPE.setName("DOCSPE");
            panel5.add(DOCSPE);
            DOCSPE.setBounds(235, 305, 195, DOCSPE.getPreferredSize().height);

            //---- OBJ_160 ----
            OBJ_160.setText("Avoir");
            OBJ_160.setName("OBJ_160");
            panel5.add(OBJ_160);
            OBJ_160.setBounds(20, 333, 125, 20);

            //---- ARG30A ----
            ARG30A.setComponentPopupMenu(BTD);
            ARG30A.setName("ARG30A");
            panel5.add(ARG30A);
            ARG30A.setBounds(160, 329, 24, ARG30A.getPreferredSize().height);

            //---- SCAN30 ----
            SCAN30.setText("Inclus");
            SCAN30.setComponentPopupMenu(null);
            SCAN30.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN30.setName("SCAN30");
            panel5.add(SCAN30);
            SCAN30.setBounds(195, 330, 70, 26);

            //---- SCAN30_E ----
            SCAN30_E.setText("Exclu");
            SCAN30_E.setComponentPopupMenu(null);
            SCAN30_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN30_E.setName("SCAN30_E");
            panel5.add(SCAN30_E);
            SCAN30_E.setBounds(277, 330, 70, 26);

            //---- SCAN30_T ----
            SCAN30_T.setText("Tous");
            SCAN30_T.setComponentPopupMenu(null);
            SCAN30_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN30_T.setName("SCAN30_T");
            panel5.add(SCAN30_T);
            SCAN30_T.setBounds(359, 330, 70, 26);
          }
          p_contenu.add(panel5, new GridBagConstraints(1, 2, 1, 2, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setMinimumSize(new Dimension(550, 85));
            panel4.setPreferredSize(new Dimension(550, 180));
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {144, 78, 102, 97, 81, 35, 0};
            ((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
            ((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

            //---- TIDX27 ----
            TIDX27.setText("Centrale");
            TIDX27.setComponentPopupMenu(BTD);
            TIDX27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX27.setToolTipText("Tri sur cet argument");
            TIDX27.setName("TIDX27");
            panel4.add(TIDX27, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_108 ----
            OBJ_108.setText("Code pr\u00e9pa");
            OBJ_108.setName("OBJ_108");
            panel4.add(OBJ_108, new GridBagConstraints(4, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_102 ----
            OBJ_102.setText("Type vente");
            OBJ_102.setName("OBJ_102");
            panel4.add(OBJ_102, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX17 ----
            TIDX17.setText("Section");
            TIDX17.setComponentPopupMenu(BTD);
            TIDX17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX17.setToolTipText("Tri sur cet argument");
            TIDX17.setName("TIDX17");
            panel4.add(TIDX17, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG17A ----
            ARG17A.setComponentPopupMenu(BTD);
            ARG17A.setPreferredSize(new Dimension(50, 28));
            ARG17A.setMinimumSize(new Dimension(50, 28));
            ARG17A.setName("ARG17A");
            panel4.add(ARG17A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX19 ----
            TIDX19.setText("Canal");
            TIDX19.setComponentPopupMenu(BTD);
            TIDX19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX19.setToolTipText("Tri sur cet argument");
            TIDX19.setName("TIDX19");
            panel4.add(TIDX19, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG19A ----
            ARG19A.setComponentPopupMenu(BTD);
            ARG19A.setMinimumSize(new Dimension(40, 28));
            ARG19A.setPreferredSize(new Dimension(40, 28));
            ARG19A.setName("ARG19A");
            panel4.add(ARG19A, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG38A ----
            ARG38A.setComponentPopupMenu(BTD);
            ARG38A.setMinimumSize(new Dimension(25, 28));
            ARG38A.setPreferredSize(new Dimension(25, 28));
            ARG38A.setName("ARG38A");
            panel4.add(ARG38A, new GridBagConstraints(5, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX18 ----
            TIDX18.setText("Affaire");
            TIDX18.setComponentPopupMenu(BTD);
            TIDX18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX18.setToolTipText("Tri sur cet argument");
            TIDX18.setName("TIDX18");
            panel4.add(TIDX18, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG18A ----
            ARG18A.setComponentPopupMenu(BTD);
            ARG18A.setPreferredSize(new Dimension(50, 28));
            ARG18A.setMinimumSize(new Dimension(50, 28));
            ARG18A.setName("ARG18A");
            panel4.add(ARG18A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG27A ----
            ARG27A.setComponentPopupMenu(BTD2);
            ARG27A.setPreferredSize(new Dimension(70, 28));
            ARG27A.setMinimumSize(new Dimension(70, 28));
            ARG27A.setName("ARG27A");
            panel4.add(ARG27A, new GridBagConstraints(3, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG32A ----
            ARG32A.setComponentPopupMenu(BTD);
            ARG32A.setMinimumSize(new Dimension(25, 28));
            ARG32A.setPreferredSize(new Dimension(25, 28));
            ARG32A.setName("ARG32A");
            panel4.add(ARG32A, new GridBagConstraints(5, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX15 ----
            TIDX15.setText("R\u00e9f\u00e9rence courte");
            TIDX15.setComponentPopupMenu(BTD);
            TIDX15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX15.setToolTipText("Tri sur cet argument");
            TIDX15.setName("TIDX15");
            panel4.add(TIDX15, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX16 ----
            TIDX16.setText("R\u00e9f\u00e9rence longue");
            TIDX16.setComponentPopupMenu(BTD);
            TIDX16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX16.setToolTipText("Tri sur cet argument");
            TIDX16.setName("TIDX16");
            panel4.add(TIDX16, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG16A ----
            ARG16A.setComponentPopupMenu(BTD2);
            ARG16A.setMinimumSize(new Dimension(160, 28));
            ARG16A.setPreferredSize(new Dimension(160, 28));
            ARG16A.setName("ARG16A");
            panel4.add(ARG16A, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG15A ----
            ARG15A.setComponentPopupMenu(BTD2);
            ARG15A.setPreferredSize(new Dimension(90, 28));
            ARG15A.setMinimumSize(new Dimension(90, 28));
            ARG15A.setName("ARG15A");
            panel4.add(ARG15A, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX26 ----
            TIDX26.setText("Commande initiale");
            TIDX26.setComponentPopupMenu(BTD);
            TIDX26.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX26.setToolTipText("Tri sur cet argument");
            TIDX26.setName("TIDX26");
            panel4.add(TIDX26, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG26A ----
            ARG26A.setComponentPopupMenu(BTD);
            ARG26A.setMinimumSize(new Dimension(110, 28));
            ARG26A.setPreferredSize(new Dimension(110, 28));
            ARG26A.setName("ARG26A");
            panel4.add(ARG26A, new GridBagConstraints(1, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));
          }
          p_contenu.add(panel4, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 5, 5, 5), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_20);
    }

    //---- ARG10A2 ----
    ARG10A2.setModel(new DefaultComboBoxModel(new String[] {
      "Tous",
      "D\u00e9sactiv\u00e9",
      "Epuis\u00e9",
      "Pr\u00e9-fin de s\u00e9rie",
      "Fin de s\u00e9rie"
    }));
    ARG10A2.setComponentPopupMenu(BTD);
    ARG10A2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG10A2.setName("ARG10A2");

    //---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX36);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX7);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX12);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX40);
    RBC_GRP.add(TIDX41);
    RBC_GRP.add(TIDX22);
    RBC_GRP.add(TIDX23);
    RBC_GRP.add(TIDX31);
    RBC_GRP.add(TIDX24);
    RBC_GRP.add(TIDX14);
    RBC_GRP.add(TIDX55);
    RBC_GRP.add(TIDX27);
    RBC_GRP.add(TIDX17);
    RBC_GRP.add(TIDX19);
    RBC_GRP.add(TIDX18);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX26);

    //---- SCAN33_GRP ----
    SCAN33_GRP.add(SCAN33);
    SCAN33_GRP.add(SCAN33_E);
    SCAN33_GRP.add(SCAN33_T);

    //---- SCAN30_GRP ----
    SCAN30_GRP.add(SCAN30);
    SCAN30_GRP.add(SCAN30_E);
    SCAN30_GRP.add(SCAN30_T);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_41;
  private JLabel OBJ_43;
  private JLabel OBJ_176;
  private XRiTextField INDNUM;
  private XRiTextField INDETB;
  private XRiTextField INDSUF;
  private XRiTextField INDNFA;
  private XRiCalendrier WDAT1X;
  private XRiCheckBox VERR;
  private JLabel OBJ_95;
  private XRiComboBox ARG3A;
  private JLabel OBJ_40;
  private JPanel p_tete_droite;
  private JLabel lbTRTAUT;
  private JLabel lb_requete;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu4;
  private RiSousMenu_bt riSousMenu_bt4;
  private RiSousMenu riSousMenu5;
  private RiSousMenu_bt riSousMenu_bt5;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private JCheckBox ARG4A_tous;
  private XRiComboBox ARG4A;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2N;
  private XRiTextField PLA2N;
  private XRiRadioButton TIDX36;
  private XRiTextField ARG36N;
  private XRiTextField PLA36N;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5A;
  private JLabel OBJ_65;
  private JLabel OBJ_142;
  private XRiComboBox PLA4A;
  private XRiCheckBox ARG57A;
  private JLabel OBJ_146;
  private JLabel OBJ_148;
  private JLabel OBJ_162;
  private XRiTextField ARG56A;
  private XRiRadioButton TIDX9;
  private XRiTextField ARG9N1;
  private XRiTextField ARG9N2;
  private XRiTextField ARGDOC;
  private JLabel OBJ_66;
  private XRiRadioButton TIDX6;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7A;
  private XRiTextField ARG6A;
  private JLabel OBJ_155;
  private JLabel OBJ_151;
  private XRiTextField ARG29A;
  private XRiTextField ARG8A;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG10A;
  private XRiRadioButton TIDX12;
  private XRiTextField ARG12A;
  private XRiTextField ARG28A;
  private XRiTextField ARG37A;
  private JLabel OBJ_156;
  private JLabel OBJ_157;
  private XRiTextField ARG11A;
  private JLabel OBJ_161;
  private JPanel panel6;
  private XRiTextField WNOM;
  private XRiTextField WCDP;
  private XRiTextField WTEL;
  private XRiTextField ARG39A;
  private JLabel OBJ_149;
  private JLabel OBJ_147;
  private JLabel OBJ_144;
  private JLabel OBJ_152;
  private XRiTextField WCLK;
  private JLabel OBJ_150;
  private JPanel panel2;
  private XRiComboBox ARG33A;
  private XRiComboBox ARG13A;
  private JLabel OBJ_177;
  private JLabel OBJ_39;
  private XRiRadioButton SCAN33;
  private XRiRadioButton SCAN33_E;
  private XRiRadioButton SCAN33_T;
  private JPanel panel5;
  private XRiRadioButton TIDX20;
  private XRiCalendrier ARG20D;
  private XRiCalendrier PLA20D;
  private XRiRadioButton TIDX21;
  private XRiCalendrier ARG21D;
  private XRiCalendrier PLA21D;
  private XRiRadioButton TIDX40;
  private XRiCalendrier ARG40D;
  private XRiCalendrier PLA40D;
  private XRiRadioButton TIDX41;
  private XRiCalendrier ARG41D;
  private XRiCalendrier PLA41D;
  private XRiRadioButton TIDX22;
  private XRiCalendrier ARG22D;
  private XRiCalendrier PLA22D;
  private XRiRadioButton TIDX23;
  private XRiCalendrier ARG23D;
  private XRiCalendrier PLA23D;
  private XRiRadioButton TIDX31;
  private XRiTextField ARG31N;
  private XRiTextField PLA31N;
  private XRiRadioButton TIDX24;
  private XRiTextField ARG24D;
  private XRiRadioButton TIDX14;
  private XRiTextField ARG14A;
  private JLabel OBJ_153;
  private JLabel OBJ_154;
  private JLabel label1;
  private XRiCheckBox ARG43A;
  private XRiRadioButton TIDX55;
  private XRiTextField ARG55N;
  private JLabel label2;
  private XRiTextField PLA55N;
  private XRiCheckBox DOCSPE;
  private JLabel OBJ_160;
  private XRiTextField ARG30A;
  private XRiRadioButton SCAN30;
  private XRiRadioButton SCAN30_E;
  private XRiRadioButton SCAN30_T;
  private JPanel panel4;
  private XRiRadioButton TIDX27;
  private JLabel OBJ_108;
  private JLabel OBJ_102;
  private XRiRadioButton TIDX17;
  private XRiTextField ARG17A;
  private XRiRadioButton TIDX19;
  private XRiTextField ARG19A;
  private XRiTextField ARG38A;
  private XRiRadioButton TIDX18;
  private XRiTextField ARG18A;
  private XRiTextField ARG27A;
  private XRiTextField ARG32A;
  private XRiRadioButton TIDX15;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG16A;
  private XRiTextField ARG15A;
  private XRiRadioButton TIDX26;
  private XRiTextField ARG26A;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_20;
  private XRiComboBox ARG10A2;
  private ButtonGroup SCAN33_GRP;
  private ButtonGroup SCAN30_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
