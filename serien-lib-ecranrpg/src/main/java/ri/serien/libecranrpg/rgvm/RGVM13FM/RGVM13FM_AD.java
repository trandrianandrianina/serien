
package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.gescom.commun.client.EnumTypeCompteClient;
import ri.serien.libcommun.gescom.commun.etablissement.IdEtablissement;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.client.snclient.SNClient;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_AD extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] PLA4A_Value = { "0", "1", "2", "3", "4", "5", "6" };
  private String[] PLA4A_Title = { "Attente", "Validé", "Envoyé", "Signé", "Val.dépassée", "Perdu", "Cloturé" };
  private String[] ARG4A_Value = { "0", "1", "2", "3", "4", "5", "6", "9" };
  private String[] ARG4A_Title = { "Attente", "Validé", "Envoyé", "Signé", "Val.dépassée", "Perdu", "Cloturé", "Tous" };
  private boolean criteresAvances = false;
  private boolean initCriteres = false;
  private String[] ARG3A_Value = { "E", "D", };
  private String[] ARG3A_Title = { "Bons", "Devis" };
  private String[] ARG33A_Value = { "", "I", "D", "O", "R", "W" };
  private String[] ARG33A_Title = { "Autres", "Interne", "Direct usine", "Ouverte", "Retour", "e-Commerce" };
  private boolean appele = false;
  
  /**
   * Constructeur.
   */
  public RGVM13FM_AD(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX27.setValeurs("27", "RBC");
    TIDX19.setValeurs("19", "RBC");
    TIDX18.setValeurs("18", "RBC");
    TIDX17.setValeurs("17", "RBC");
    TIDX34.setValeurs("34", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX31.setValeurs("31", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX36.setValeurs("36", "RBC");
    TIDX25.setValeurs("25", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX20.setValeurs("20", "RBC");
    TIDX12.setValeurs("12", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX7.setValeurs("7", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX2.setValeurs("2", "RBC");
    PLA4A.setValeurs(PLA4A_Value, PLA4A_Title);
    ARG4A.setValeurs(ARG4A_Value, ARG4A_Title);
    
    VERR.setValeursSelection("1", "0");
    ARG3A.setValeurs(ARG3A_Value, ARG3A_Title);
    
    DOCSPE.setValeursSelection("1", "");
    ARG33A.setValeurs(ARG33A_Value, ARG33A_Title);
    SCAN33.setValeurs("E", SCAN33_GRP);
    SCAN33_E.setValeurs("N");
    SCAN33_T.setValeurs(" ");
    
    // Type de fiche
    cbTypeCompteClient.addItem("Clients et Prospects");
    cbTypeCompteClient.addItem("Clients");
    cbTypeCompteClient.addItem("Prospects");
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion de @GESTION@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(riMenu_bt_V01F);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    initCriteres = false;
    criteresAvances = lexique.HostFieldGetData("WECRA").trim().equals("N");
    
    tous.setSelected(lexique.HostFieldGetData("ARG4A").equals("9"));
    
    lb_requete.setVisible(interpreteurD.analyseExpression("@AFFREQ@").equals("1"));
    
    // Client
    snClient.setSession(getSession());
    snClient.setIdEtablissement(IdEtablissement.getInstance(lexique.HostFieldGetData("INDETB")));
    snClient.setRechercheProspectAutorisee(true);
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "ARG9N1", "ARG9N2");
    
    if (lexique.HostFieldGetData("ARG58A").trim().equalsIgnoreCase("C")) {
      cbTypeCompteClient.setSelectedItem("Clients");
      snClient.forcerRechercheTypeCompte(EnumTypeCompteClient.EN_COMPTE);
    }
    else if (lexique.HostFieldGetData("ARG58A").trim().equalsIgnoreCase("P")) {
      cbTypeCompteClient.setSelectedItem("Prospects");
      snClient.forcerRechercheTypeCompte(EnumTypeCompteClient.PROSPECT);
    }
    else {
      cbTypeCompteClient.setSelectedItem("Clients et Prospects");
      snClient.forcerRechercheTypeCompte(null);
    }
    
    // Gestion bouton de sortie
    appele = lexique.HostFieldGetData("APPELE").trim().equalsIgnoreCase("");
    if (appele) {
      bouton_retour.setText("Sortir");
    }
    else {
      bouton_retour.setText("Retour");
    }
    
    // Panels
    switchAffichages();
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (tous.isSelected()) {
      lexique.HostFieldPutData("ARG4A", 0, "9");
    }
    
    snClient.renseignerChampRPG(lexique, "ARG9N1", "ARG9N2");
    
    if (cbTypeCompteClient.getSelectedItem().equals("Clients et Prospects")) {
      lexique.HostFieldPutData("ARG58A", 0, "");
    }
    else if (cbTypeCompteClient.getSelectedItem().equals("Clients")) {
      lexique.HostFieldPutData("ARG58A", 0, "C");
    }
    else if (cbTypeCompteClient.getSelectedItem().equals("Prospects")) {
      lexique.HostFieldPutData("ARG58A", 0, "P");
    }
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    if (interpreteurD.analyseExpression("@AFFREQ@").equalsIgnoreCase("1")) {
      lexique.HostFieldPutData("AFFREQ", 0, " ");
      lb_requete.setVisible(false);
    }
    else {
      lexique.HostFieldPutData("AFFREQ", 0, "1");
      lb_requete.setVisible(true);
    }
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    ARG4A.setSelectedIndex(7);
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    switchAffichages();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    if (appele) {
      lexique.HostScreenSendKey(this, "F3");
    }
    else {
      lexique.HostScreenSendKey(this, "F12");
    }
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void switchAffichages() {
    if (initCriteres) {
      criteresAvances = !criteresAvances;
    }
    else {
      initCriteres = true;
    }
    
    // visibilité des panels
    panel21.setVisible(criteresAvances);
    TIDX36.setVisible(criteresAvances);
    ARG36A.setVisible(criteresAvances);
    panel5.setVisible(criteresAvances);
    panel6.setVisible(criteresAvances);
    panel7.setVisible(criteresAvances);
    panel5.setVisible(criteresAvances);
    panel6.setVisible(criteresAvances);
    panel7.setVisible(criteresAvances);
    
    if (criteresAvances) {
      panel2.setSize(435, 385);
      panel41.setLocation(5, 40);
      panel4.setSize(435, 115);
      panel4.setLocation(15, 395);
      riSousMenu_bt9.setText("Critères principaux");
      riSousMenu_bt9.setToolTipText("Critères principaux");
      lexique.HostFieldPutData("WECRA", 0, "N");
    }
    else {
      panel2.setSize(435, 180);
      panel41.setLocation(5, 10);
      panel4.setSize(435, 90);
      panel4.setLocation(15, 200);
      riSousMenu_bt9.setText("Tous les critères");
      riSousMenu_bt9.setToolTipText("Tous les critères");
      lexique.HostFieldPutData("WECRA", 0, "S");
    }
  }
  
  private void tousActionPerformed(ActionEvent e) {
    if (tous.isSelected()) {
      ARG4A.setSelectedIndex(7);
    }
    else {
      ARG4A.setSelectedIndex(0);
    }
    PLA4A.setVisible(!tous.isSelected());
    label1.setVisible(!tous.isSelected());
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    tous.setSelected(ARG4A.getSelectedIndex() == 7);
    PLA4A.setVisible(!tous.isSelected());
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD2.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void cbTypeCompteClientItemStateChanged(ItemEvent e) {
    try {
      if (cbTypeCompteClient.getSelectedItem().equals("Clients et Prospects")) {
        snClient.forcerRechercheTypeCompte(null);
      }
      else if (cbTypeCompteClient.getSelectedItem().equals("Clients")) {
        snClient.forcerRechercheTypeCompte(EnumTypeCompteClient.EN_COMPTE);
      }
      else if (cbTypeCompteClient.getSelectedItem().equals("Prospects")) {
        snClient.forcerRechercheTypeCompte(EnumTypeCompteClient.PROSPECT);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_47 = new JLabel();
    OBJ_41 = new JLabel();
    VERR = new XRiCheckBox();
    WDAT1X = new XRiCalendrier();
    INDNUM = new XRiTextField();
    OBJ_43 = new JLabel();
    INDETB = new XRiTextField();
    OBJ_95 = new JLabel();
    ARG3A = new XRiComboBox();
    p_tete_droite = new JPanel();
    lb_requete = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    p_centrage = new SNPanel();
    p_contenu = new SNPanelContenu();
    panel2 = new JPanel();
    tous = new JCheckBox();
    ARG4A = new XRiComboBox();
    TIDX2 = new XRiRadioButton();
    ARG2N = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG5A = new XRiTextField();
    OBJ_59 = new JLabel();
    OBJ_68 = new JLabel();
    label1 = new JLabel();
    PLA4A = new XRiComboBox();
    ARGDOC = new XRiTextField();
    OBJ_66 = new JLabel();
    PLA2N = new XRiTextField();
    cbTypeCompteClient = new SNComboBox();
    snClient = new SNClient();
    panel21 = new JPanel();
    TIDX6 = new XRiRadioButton();
    ARG6A = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG7A = new XRiTextField();
    ARG8A = new XRiTextField();
    TIDX10 = new XRiRadioButton();
    ARG10A = new XRiTextField();
    TIDX12 = new XRiRadioButton();
    ARG12A = new XRiTextField();
    ARG28A = new XRiTextField();
    ARG11A = new XRiTextField();
    ARG37A = new XRiTextField();
    OBJ_106 = new JLabel();
    OBJ_94 = new JLabel();
    OBJ_109 = new JLabel();
    OBJ_137 = new JLabel();
    ARG56A = new XRiTextField();
    OBJ_162 = new JLabel();
    DOCSPE = new XRiCheckBox();
    panel4 = new JPanel();
    panel41 = new JPanel();
    TIDX15 = new XRiRadioButton();
    ARG15A = new XRiTextField();
    TIDX16 = new XRiRadioButton();
    ARG16A = new XRiTextField();
    TIDX36 = new XRiRadioButton();
    ARG36A = new XRiTextField();
    panel5 = new JPanel();
    TIDX20 = new XRiRadioButton();
    ARG20D = new XRiCalendrier();
    PLA20D = new XRiCalendrier();
    TIDX21 = new XRiRadioButton();
    ARG21D = new XRiCalendrier();
    PLA21D = new XRiCalendrier();
    TIDX25 = new XRiRadioButton();
    ARG25D = new XRiCalendrier();
    PLA25D = new XRiCalendrier();
    OBJ_85 = new JLabel();
    OBJ_86 = new JLabel();
    panel6 = new JPanel();
    TIDX31 = new XRiRadioButton();
    ARG31N = new XRiTextField();
    PLA31N = new XRiTextField();
    TIDX14 = new XRiRadioButton();
    ARG14A = new XRiTextField();
    TIDX34 = new XRiRadioButton();
    ARG34A = new XRiTextField();
    OBJ_113 = new JLabel();
    OBJ_114 = new JLabel();
    ARG44A = new XRiTextField();
    panel7 = new JPanel();
    TIDX17 = new XRiRadioButton();
    ARG17A = new XRiTextField();
    TIDX18 = new XRiRadioButton();
    ARG18A = new XRiTextField();
    TIDX19 = new XRiRadioButton();
    ARG19A = new XRiTextField();
    TIDX27 = new XRiRadioButton();
    ARG27A = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTD2 = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    ARG33A = new XRiComboBox();
    SCAN33 = new XRiRadioButton();
    SCAN33_E = new XRiRadioButton();
    SCAN33_T = new XRiRadioButton();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    ARG9N1 = new XRiTextField();
    ARG9N2 = new XRiTextField();
    panel3 = new JPanel();
    OBJ_62 = new JLabel();
    WNOM = new XRiTextField();
    OBJ_71 = new JLabel();
    WCDP = new XRiTextField();
    OBJ_75 = new JLabel();
    WTEL = new XRiTextField();
    panel31 = new JPanel();
    OBJ_138 = new JLabel();
    ARG39A = new XRiTextField();
    SCAN33_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de @GESTION@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(800, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_47 ----
          OBJ_47.setText("Traitement");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_41 ----
          OBJ_41.setText("Etablissement");
          OBJ_41.setName("OBJ_41");

          //---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(BTD);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");

          //---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(BTD);
          WDAT1X.setName("WDAT1X");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");

          //---- OBJ_43 ----
          OBJ_43.setText("Num\u00e9ro");
          OBJ_43.setName("OBJ_43");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");

          //---- OBJ_95 ----
          OBJ_95.setText("Type");
          OBJ_95.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_95.setName("OBJ_95");

          //---- ARG3A ----
          ARG3A.setComponentPopupMenu(BTD);
          ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG3A.setName("ARG3A");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(90, 90, 90)
                    .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 144, Short.MAX_VALUE)
                .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                .addGap(74, 74, 74))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                  .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_requete ----
          lb_requete.setText("Affichage requ\u00eate SQL");
          lb_requete.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_requete.setFont(lb_requete.getFont().deriveFont(lb_requete.getFont().getStyle() | Font.BOLD));
          lb_requete.setName("lb_requete");
          p_tete_droite.add(lb_requete);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setOpaque(false);
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Visualisation requ\u00eate");
              riSousMenu_bt6.setToolTipText("Visualisation de la requ\u00eate");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Tous les crit\u00e8res");
              riSousMenu_bt9.setToolTipText("Tous le crit\u00e8res");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Tous les devis");
              riSousMenu_bt7.setToolTipText("Tous les devis");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("G\u00e9rer les filtres");
              riSousMenu_bt10.setToolTipText("Tous les devis");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Sauvegarder le filtre");
              riSousMenu_bt13.setToolTipText("Tous les devis");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu11);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setOpaque(false);
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(875, 600));
          p_contenu.setMinimumSize(new Dimension(875, 600));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {435, 346};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {205, 254, 100, 56, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {1.0, 0.0, 0.0, 0.0, 1.0};

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {155, 25, 0, 0, 53, 0, 0, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 237, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0E-4};

            //---- tous ----
            tous.setText("");
            tous.setToolTipText("Tout les devis");
            tous.setComponentPopupMenu(BTD);
            tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            tous.setPreferredSize(new Dimension(20, 28));
            tous.setMinimumSize(new Dimension(20, 28));
            tous.setMaximumSize(new Dimension(20, 28));
            tous.setName("tous");
            tous.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                tousActionPerformed(e);
              }
            });
            panel2.add(tous, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(BTD);
            ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A.setPreferredSize(new Dimension(125, 28));
            ARG4A.setMinimumSize(new Dimension(125, 28));
            ARG4A.setName("ARG4A");
            ARG4A.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                ARG4AActionPerformed(e);
              }
            });
            panel2.add(ARG4A, new GridBagConstraints(2, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX2 ----
            TIDX2.setText("Num\u00e9ro de devis");
            TIDX2.setComponentPopupMenu(BTD);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setToolTipText("Tri sur cet argument");
            TIDX2.setName("TIDX2");
            panel2.add(TIDX2, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG2N ----
            ARG2N.setComponentPopupMenu(BTD2);
            ARG2N.setMinimumSize(new Dimension(70, 28));
            ARG2N.setPreferredSize(new Dimension(70, 28));
            ARG2N.setName("ARG2N");
            panel2.add(ARG2N, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX5 ----
            TIDX5.setText("Magasin");
            TIDX5.setComponentPopupMenu(BTD);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setToolTipText("Tri sur cet argument");
            TIDX5.setName("TIDX5");
            panel2.add(TIDX5, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD);
            ARG5A.setPreferredSize(new Dimension(34, 28));
            ARG5A.setMinimumSize(new Dimension(34, 28));
            ARG5A.setName("ARG5A");
            panel2.add(ARG5A, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_59 ----
            OBJ_59.setText("Etats des devis");
            OBJ_59.setName("OBJ_59");
            panel2.add(OBJ_59, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_68 ----
            OBJ_68.setText("\u00e0");
            OBJ_68.setName("OBJ_68");
            panel2.add(OBJ_68, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- label1 ----
            label1.setText("\u00e0");
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel2.add(label1, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA4A ----
            PLA4A.setComponentPopupMenu(BTD);
            PLA4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            PLA4A.setMinimumSize(new Dimension(125, 28));
            PLA4A.setPreferredSize(new Dimension(125, 28));
            PLA4A.setName("PLA4A");
            panel2.add(PLA4A, new GridBagConstraints(5, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- ARGDOC ----
            ARGDOC.setComponentPopupMenu(null);
            ARGDOC.setMinimumSize(new Dimension(210, 28));
            ARGDOC.setPreferredSize(new Dimension(210, 28));
            ARGDOC.setName("ARGDOC");
            panel2.add(ARGDOC, new GridBagConstraints(2, 1, 5, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- OBJ_66 ----
            OBJ_66.setText("Recherche document");
            OBJ_66.setName("OBJ_66");
            panel2.add(OBJ_66, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA2N ----
            PLA2N.setComponentPopupMenu(BTD2);
            PLA2N.setMinimumSize(new Dimension(70, 28));
            PLA2N.setPreferredSize(new Dimension(70, 28));
            PLA2N.setName("PLA2N");
            panel2.add(PLA2N, new GridBagConstraints(4, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- cbTypeCompteClient ----
            cbTypeCompteClient.setMinimumSize(new Dimension(200, 30));
            cbTypeCompteClient.setPreferredSize(new Dimension(200, 30));
            cbTypeCompteClient.setName("cbTypeCompteClient");
            cbTypeCompteClient.addItemListener(new ItemListener() {
              @Override
              public void itemStateChanged(ItemEvent e) {
                cbTypeCompteClientItemStateChanged(e);
              }
            });
            panel2.add(cbTypeCompteClient, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- snClient ----
            snClient.setMaximumSize(new Dimension(300, 30));
            snClient.setMinimumSize(new Dimension(300, 30));
            snClient.setPreferredSize(new Dimension(300, 30));
            snClient.setName("snClient");
            panel2.add(snClient, new GridBagConstraints(2, 4, 5, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //======== panel21 ========
            {
              panel21.setOpaque(false);
              panel21.setName("panel21");
              panel21.setLayout(new GridBagLayout());
              ((GridBagLayout)panel21.getLayout()).columnWidths = new int[] {182, 0, 110, 0, 0, 0};
              ((GridBagLayout)panel21.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 30, 0};
              ((GridBagLayout)panel21.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
              ((GridBagLayout)panel21.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

              //---- TIDX6 ----
              TIDX6.setText("Vendeur");
              TIDX6.setComponentPopupMenu(BTD);
              TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX6.setToolTipText("Tri sur cet argument");
              TIDX6.setName("TIDX6");
              panel21.add(TIDX6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG6A ----
              ARG6A.setComponentPopupMenu(BTD);
              ARG6A.setPreferredSize(new Dimension(40, 28));
              ARG6A.setMinimumSize(new Dimension(40, 28));
              ARG6A.setName("ARG6A");
              panel21.add(ARG6A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- TIDX7 ----
              TIDX7.setText("Repr\u00e9sentant 1");
              TIDX7.setComponentPopupMenu(BTD);
              TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX7.setToolTipText("Tri sur cet argument");
              TIDX7.setName("TIDX7");
              panel21.add(TIDX7, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG7A ----
              ARG7A.setComponentPopupMenu(BTD);
              ARG7A.setMinimumSize(new Dimension(34, 28));
              ARG7A.setPreferredSize(new Dimension(34, 28));
              ARG7A.setName("ARG7A");
              panel21.add(ARG7A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG8A ----
              ARG8A.setComponentPopupMenu(BTD);
              ARG8A.setMinimumSize(new Dimension(34, 28));
              ARG8A.setPreferredSize(new Dimension(34, 28));
              ARG8A.setName("ARG8A");
              panel21.add(ARG8A, new GridBagConstraints(3, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- TIDX10 ----
              TIDX10.setText("Mode d'exp\u00e9dition");
              TIDX10.setComponentPopupMenu(BTD);
              TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX10.setToolTipText("Tri sur cet argument");
              TIDX10.setName("TIDX10");
              panel21.add(TIDX10, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG10A ----
              ARG10A.setComponentPopupMenu(BTD);
              ARG10A.setMinimumSize(new Dimension(34, 28));
              ARG10A.setPreferredSize(new Dimension(34, 28));
              ARG10A.setName("ARG10A");
              panel21.add(ARG10A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- TIDX12 ----
              TIDX12.setText("Zone g\u00e9ographique");
              TIDX12.setComponentPopupMenu(BTD);
              TIDX12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX12.setToolTipText("Tri sur cet argument");
              TIDX12.setName("TIDX12");
              panel21.add(TIDX12, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG12A ----
              ARG12A.setComponentPopupMenu(BTD);
              ARG12A.setMinimumSize(new Dimension(60, 28));
              ARG12A.setPreferredSize(new Dimension(60, 28));
              ARG12A.setName("ARG12A");
              panel21.add(ARG12A, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG28A ----
              ARG28A.setComponentPopupMenu(BTD2);
              ARG28A.setMinimumSize(new Dimension(60, 28));
              ARG28A.setPreferredSize(new Dimension(60, 28));
              ARG28A.setName("ARG28A");
              panel21.add(ARG28A, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG11A ----
              ARG11A.setComponentPopupMenu(BTD);
              ARG11A.setPreferredSize(new Dimension(34, 28));
              ARG11A.setMinimumSize(new Dimension(34, 28));
              ARG11A.setName("ARG11A");
              panel21.add(ARG11A, new GridBagConstraints(3, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG37A ----
              ARG37A.setComponentPopupMenu(BTD2);
              ARG37A.setPreferredSize(new Dimension(210, 28));
              ARG37A.setMinimumSize(new Dimension(210, 28));
              ARG37A.setName("ARG37A");
              panel21.add(ARG37A, new GridBagConstraints(1, 6, 3, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- OBJ_106 ----
              OBJ_106.setText("Regroupement transport");
              OBJ_106.setName("OBJ_106");
              panel21.add(OBJ_106, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- OBJ_94 ----
              OBJ_94.setText("Repr\u00e9sentant 2");
              OBJ_94.setName("OBJ_94");
              panel21.add(OBJ_94, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- OBJ_109 ----
              OBJ_109.setText("Transporteur");
              OBJ_109.setName("OBJ_109");
              panel21.add(OBJ_109, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- OBJ_137 ----
              OBJ_137.setText("Article");
              OBJ_137.setName("OBJ_137");
              panel21.add(OBJ_137, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG56A ----
              ARG56A.setComponentPopupMenu(BTD2);
              ARG56A.setMinimumSize(new Dimension(260, 28));
              ARG56A.setPreferredSize(new Dimension(260, 28));
              ARG56A.setName("ARG56A");
              panel21.add(ARG56A, new GridBagConstraints(1, 7, 3, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- OBJ_162 ----
              OBJ_162.setText("Libell\u00e9 saisi sur ligne");
              OBJ_162.setName("OBJ_162");
              panel21.add(OBJ_162, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- DOCSPE ----
              DOCSPE.setText("Contient des articles sp\u00e9ciaux");
              DOCSPE.setName("DOCSPE");
              panel21.add(DOCSPE, new GridBagConstraints(0, 8, 2, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));
            }
            panel2.add(panel21, new GridBagConstraints(0, 7, 7, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel2, new GridBagConstraints(0, 0, 1, 3, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(5, 5, 0, 0), 0, 0));

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(new GridBagLayout());
            ((GridBagLayout)panel4.getLayout()).columnWidths = new int[] {175, 292, 0};
            ((GridBagLayout)panel4.getLayout()).rowHeights = new int[] {63, 0, 0};
            ((GridBagLayout)panel4.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel4.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //======== panel41 ========
            {
              panel41.setOpaque(false);
              panel41.setName("panel41");
              panel41.setLayout(new GridBagLayout());
              ((GridBagLayout)panel41.getLayout()).columnWidths = new int[] {175, 291, 0};
              ((GridBagLayout)panel41.getLayout()).rowHeights = new int[] {0, 0, 0};
              ((GridBagLayout)panel41.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
              ((GridBagLayout)panel41.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

              //---- TIDX15 ----
              TIDX15.setText("R\u00e9f\u00e9rence courte");
              TIDX15.setComponentPopupMenu(BTD);
              TIDX15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX15.setToolTipText("Tri sur cet argument");
              TIDX15.setName("TIDX15");
              panel41.add(TIDX15, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 5, 5), 0, 0));

              //---- ARG15A ----
              ARG15A.setComponentPopupMenu(BTD2);
              ARG15A.setPreferredSize(new Dimension(90, 28));
              ARG15A.setMinimumSize(new Dimension(90, 28));
              ARG15A.setName("ARG15A");
              panel41.add(ARG15A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 5, 0), 0, 0));

              //---- TIDX16 ----
              TIDX16.setText("R\u00e9f\u00e9rence longue");
              TIDX16.setComponentPopupMenu(BTD);
              TIDX16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              TIDX16.setToolTipText("Tri sur cet argument");
              TIDX16.setName("TIDX16");
              panel41.add(TIDX16, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 5), 0, 0));

              //---- ARG16A ----
              ARG16A.setComponentPopupMenu(BTD2);
              ARG16A.setMinimumSize(new Dimension(160, 28));
              ARG16A.setPreferredSize(new Dimension(160, 28));
              ARG16A.setName("ARG16A");
              panel41.add(ARG16A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
            }
            panel4.add(panel41, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX36 ----
            TIDX36.setText("Espoir r\u00e9alisation");
            TIDX36.setComponentPopupMenu(BTD);
            TIDX36.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX36.setToolTipText("Tri sur cet argument");
            TIDX36.setName("TIDX36");
            panel4.add(TIDX36, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG36A ----
            ARG36A.setComponentPopupMenu(BTD2);
            ARG36A.setMinimumSize(new Dimension(40, 28));
            ARG36A.setPreferredSize(new Dimension(40, 28));
            ARG36A.setName("ARG36A");
            panel4.add(ARG36A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel4, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));

          //======== panel5 ========
          {
            panel5.setBorder(new TitledBorder("Dates"));
            panel5.setOpaque(false);
            panel5.setName("panel5");
            panel5.setLayout(new GridBagLayout());
            ((GridBagLayout)panel5.getLayout()).columnWidths = new int[] {135, 123, 149, 0};
            ((GridBagLayout)panel5.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0};
            ((GridBagLayout)panel5.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel5.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- TIDX20 ----
            TIDX20.setText("Cr\u00e9ation");
            TIDX20.setComponentPopupMenu(BTD);
            TIDX20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX20.setToolTipText("Tri sur cet argument");
            TIDX20.setName("TIDX20");
            panel5.add(TIDX20, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG20D ----
            ARG20D.setComponentPopupMenu(BTD2);
            ARG20D.setPreferredSize(new Dimension(105, 28));
            ARG20D.setMinimumSize(new Dimension(105, 28));
            ARG20D.setName("ARG20D");
            panel5.add(ARG20D, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA20D ----
            PLA20D.setComponentPopupMenu(BTD2);
            PLA20D.setPreferredSize(new Dimension(105, 28));
            PLA20D.setMinimumSize(new Dimension(105, 28));
            PLA20D.setName("PLA20D");
            panel5.add(PLA20D, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX21 ----
            TIDX21.setText("Validation");
            TIDX21.setComponentPopupMenu(BTD);
            TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX21.setToolTipText("Tri sur cet argument");
            TIDX21.setName("TIDX21");
            panel5.add(TIDX21, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG21D ----
            ARG21D.setComponentPopupMenu(BTD2);
            ARG21D.setPreferredSize(new Dimension(105, 28));
            ARG21D.setMinimumSize(new Dimension(105, 28));
            ARG21D.setName("ARG21D");
            panel5.add(ARG21D, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA21D ----
            PLA21D.setComponentPopupMenu(BTD2);
            PLA21D.setPreferredSize(new Dimension(105, 28));
            PLA21D.setMinimumSize(new Dimension(105, 28));
            PLA21D.setName("PLA21D");
            panel5.add(PLA21D, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX25 ----
            TIDX25.setText("Validit\u00e9");
            TIDX25.setComponentPopupMenu(BTD);
            TIDX25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX25.setToolTipText("Tri sur cet argument");
            TIDX25.setName("TIDX25");
            panel5.add(TIDX25, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG25D ----
            ARG25D.setComponentPopupMenu(BTD2);
            ARG25D.setPreferredSize(new Dimension(105, 28));
            ARG25D.setMinimumSize(new Dimension(105, 28));
            ARG25D.setName("ARG25D");
            panel5.add(ARG25D, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- PLA25D ----
            PLA25D.setComponentPopupMenu(BTD2);
            PLA25D.setPreferredSize(new Dimension(105, 28));
            PLA25D.setMinimumSize(new Dimension(105, 28));
            PLA25D.setName("PLA25D");
            panel5.add(PLA25D, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));

            //---- OBJ_85 ----
            OBJ_85.setText("du");
            OBJ_85.setName("OBJ_85");
            panel5.add(OBJ_85, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_86 ----
            OBJ_86.setText("au");
            OBJ_86.setName("OBJ_86");
            panel5.add(OBJ_86, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          p_contenu.add(panel5, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //======== panel6 ========
          {
            panel6.setBorder(new TitledBorder(""));
            panel6.setOpaque(false);
            panel6.setName("panel6");
            panel6.setLayout(new GridBagLayout());
            ((GridBagLayout)panel6.getLayout()).columnWidths = new int[] {135, 95, 30, 84, 80, 0};
            ((GridBagLayout)panel6.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
            ((GridBagLayout)panel6.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel6.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

            //---- TIDX31 ----
            TIDX31.setText("Montant HT de");
            TIDX31.setComponentPopupMenu(BTD);
            TIDX31.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX31.setToolTipText("Tri sur cet argument");
            TIDX31.setName("TIDX31");
            panel6.add(TIDX31, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG31N ----
            ARG31N.setComponentPopupMenu(BTD2);
            ARG31N.setMinimumSize(new Dimension(75, 28));
            ARG31N.setPreferredSize(new Dimension(75, 28));
            ARG31N.setHorizontalAlignment(SwingConstants.TRAILING);
            ARG31N.setName("ARG31N");
            panel6.add(ARG31N, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- PLA31N ----
            PLA31N.setComponentPopupMenu(BTD2);
            PLA31N.setMinimumSize(new Dimension(75, 28));
            PLA31N.setPreferredSize(new Dimension(75, 28));
            PLA31N.setHorizontalAlignment(SwingConstants.TRAILING);
            PLA31N.setName("PLA31N");
            panel6.add(PLA31N, new GridBagConstraints(3, 0, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- TIDX14 ----
            TIDX14.setText("Devise");
            TIDX14.setComponentPopupMenu(BTD);
            TIDX14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX14.setToolTipText("Tri sur cet argument");
            TIDX14.setName("TIDX14");
            panel6.add(TIDX14, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- ARG14A ----
            ARG14A.setComponentPopupMenu(BTD);
            ARG14A.setPreferredSize(new Dimension(40, 28));
            ARG14A.setMinimumSize(new Dimension(40, 28));
            ARG14A.setName("ARG14A");
            panel6.add(ARG14A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- TIDX34 ----
            TIDX34.setText("Motif devis perdu");
            TIDX34.setComponentPopupMenu(BTD);
            TIDX34.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX34.setToolTipText("Tri sur cet argument");
            TIDX34.setName("TIDX34");
            panel6.add(TIDX34, new GridBagConstraints(0, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG34A ----
            ARG34A.setComponentPopupMenu(BTD);
            ARG34A.setMinimumSize(new Dimension(60, 28));
            ARG34A.setPreferredSize(new Dimension(60, 28));
            ARG34A.setName("ARG34A");
            panel6.add(ARG34A, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- OBJ_113 ----
            OBJ_113.setText("\u00e0");
            OBJ_113.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_113.setName("OBJ_113");
            panel6.add(OBJ_113, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 5), 0, 0));

            //---- OBJ_114 ----
            OBJ_114.setText("Type de devis");
            OBJ_114.setName("OBJ_114");
            panel6.add(OBJ_114, new GridBagConstraints(2, 2, 2, 1, 0.0, 0.0,
              GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG44A ----
            ARG44A.setComponentPopupMenu(BTD);
            ARG44A.setPreferredSize(new Dimension(45, 28));
            ARG44A.setMinimumSize(new Dimension(45, 28));
            ARG44A.setName("ARG44A");
            panel6.add(ARG44A, new GridBagConstraints(4, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel6, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //======== panel7 ========
          {
            panel7.setBorder(new TitledBorder(""));
            panel7.setOpaque(false);
            panel7.setName("panel7");
            panel7.setLayout(new GridBagLayout());
            ((GridBagLayout)panel7.getLayout()).columnWidths = new int[] {71, 116, 0, 157, 0, 139, 0, 114, 0};
            ((GridBagLayout)panel7.getLayout()).rowHeights = new int[] {0, 0};
            ((GridBagLayout)panel7.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel7.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

            //---- TIDX17 ----
            TIDX17.setText("Section");
            TIDX17.setComponentPopupMenu(BTD);
            TIDX17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX17.setToolTipText("Tri sur cet argument");
            TIDX17.setName("TIDX17");
            panel7.add(TIDX17, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG17A ----
            ARG17A.setComponentPopupMenu(BTD);
            ARG17A.setPreferredSize(new Dimension(50, 28));
            ARG17A.setMinimumSize(new Dimension(50, 28));
            ARG17A.setName("ARG17A");
            panel7.add(ARG17A, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- TIDX18 ----
            TIDX18.setText("Affaire");
            TIDX18.setComponentPopupMenu(BTD);
            TIDX18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX18.setToolTipText("Tri sur cet argument");
            TIDX18.setName("TIDX18");
            panel7.add(TIDX18, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG18A ----
            ARG18A.setComponentPopupMenu(BTD);
            ARG18A.setPreferredSize(new Dimension(50, 28));
            ARG18A.setMinimumSize(new Dimension(50, 28));
            ARG18A.setName("ARG18A");
            panel7.add(ARG18A, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- TIDX19 ----
            TIDX19.setText("Canal de vente");
            TIDX19.setComponentPopupMenu(BTD);
            TIDX19.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX19.setToolTipText("Tri sur cet argument");
            TIDX19.setName("TIDX19");
            panel7.add(TIDX19, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG19A ----
            ARG19A.setComponentPopupMenu(BTD);
            ARG19A.setMinimumSize(new Dimension(40, 28));
            ARG19A.setPreferredSize(new Dimension(40, 28));
            ARG19A.setName("ARG19A");
            panel7.add(ARG19A, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- TIDX27 ----
            TIDX27.setText("Centrale");
            TIDX27.setComponentPopupMenu(BTD);
            TIDX27.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX27.setToolTipText("Tri sur cet argument");
            TIDX27.setName("TIDX27");
            panel7.add(TIDX27, new GridBagConstraints(6, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- ARG27A ----
            ARG27A.setComponentPopupMenu(BTD);
            ARG27A.setMinimumSize(new Dimension(70, 28));
            ARG27A.setPreferredSize(new Dimension(70, 28));
            ARG27A.setName("ARG27A");
            panel7.add(ARG27A, new GridBagConstraints(7, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel7, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 5, 5, 5), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTD2 ========
    {
      BTD2.setName("BTD2");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD2.add(OBJ_20);
    }

    //---- ARG33A ----
    ARG33A.setComponentPopupMenu(null);
    ARG33A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG33A.setName("ARG33A");

    //---- SCAN33 ----
    SCAN33.setText("Inclus");
    SCAN33.setComponentPopupMenu(null);
    SCAN33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    SCAN33.setName("SCAN33");

    //---- SCAN33_E ----
    SCAN33_E.setText("Exclu");
    SCAN33_E.setComponentPopupMenu(null);
    SCAN33_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    SCAN33_E.setName("SCAN33_E");

    //---- SCAN33_T ----
    SCAN33_T.setText("Tous");
    SCAN33_T.setComponentPopupMenu(null);
    SCAN33_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    SCAN33_T.setName("SCAN33_T");

    //======== riSousMenu10 ========
    {
      riSousMenu10.setName("riSousMenu10");

      //---- riSousMenu_bt11 ----
      riSousMenu_bt11.setText("Tous les devis");
      riSousMenu_bt11.setToolTipText("Tous les devis");
      riSousMenu_bt11.setName("riSousMenu_bt11");
      riSousMenu_bt11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt7ActionPerformed(e);
        }
      });
      riSousMenu10.add(riSousMenu_bt11);
    }

    //---- ARG9N1 ----
    ARG9N1.setComponentPopupMenu(BTD2);
    ARG9N1.setMinimumSize(new Dimension(70, 28));
    ARG9N1.setPreferredSize(new Dimension(70, 28));
    ARG9N1.setName("ARG9N1");

    //---- ARG9N2 ----
    ARG9N2.setComponentPopupMenu(BTD2);
    ARG9N2.setMinimumSize(new Dimension(40, 28));
    ARG9N2.setPreferredSize(new Dimension(40, 28));
    ARG9N2.setName("ARG9N2");

    //======== panel3 ========
    {
      panel3.setBorder(new TitledBorder("Recherche client"));
      panel3.setOpaque(false);
      panel3.setName("panel3");
      panel3.setLayout(new GridBagLayout());
      ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {135, 223, 74, 0};
      ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
      ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
      ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

      //---- OBJ_62 ----
      OBJ_62.setText("Nom ou R.S");
      OBJ_62.setName("OBJ_62");
      panel3.add(OBJ_62, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- WNOM ----
      WNOM.setComponentPopupMenu(BTD2);
      WNOM.setMinimumSize(new Dimension(160, 28));
      WNOM.setPreferredSize(new Dimension(160, 28));
      WNOM.setName("WNOM");
      panel3.add(WNOM, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- OBJ_71 ----
      OBJ_71.setText("Code postal");
      OBJ_71.setName("OBJ_71");
      panel3.add(OBJ_71, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- WCDP ----
      WCDP.setComponentPopupMenu(BTD);
      WCDP.setPreferredSize(new Dimension(60, 28));
      WCDP.setMinimumSize(new Dimension(60, 28));
      WCDP.setName("WCDP");
      panel3.add(WCDP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- OBJ_75 ----
      OBJ_75.setText("N\u00b0 t\u00e9l\u00e9phone");
      OBJ_75.setName("OBJ_75");
      panel3.add(OBJ_75, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- WTEL ----
      WTEL.setComponentPopupMenu(BTD2);
      WTEL.setMinimumSize(new Dimension(120, 28));
      WTEL.setPreferredSize(new Dimension(120, 28));
      WTEL.setName("WTEL");
      panel3.add(WTEL, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //======== panel31 ========
      {
        panel31.setOpaque(false);
        panel31.setName("panel31");
        panel31.setLayout(new GridBagLayout());
        ((GridBagLayout)panel31.getLayout()).columnWidths = new int[] {134, 221, 0, 0};
        ((GridBagLayout)panel31.getLayout()).rowHeights = new int[] {0, 0};
        ((GridBagLayout)panel31.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};
        ((GridBagLayout)panel31.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};
      }
      panel3.add(panel31, new GridBagConstraints(0, 3, 3, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //---- OBJ_138 ----
      OBJ_138.setText("Nom saisi");
      OBJ_138.setName("OBJ_138");
      panel3.add(OBJ_138, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));

      //---- ARG39A ----
      ARG39A.setComponentPopupMenu(BTD2);
      ARG39A.setMinimumSize(new Dimension(160, 28));
      ARG39A.setPreferredSize(new Dimension(160, 28));
      ARG39A.setName("ARG39A");
      panel3.add(ARG39A, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 5), 0, 0));
    }

    //---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX7);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX12);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX36);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX25);
    RBC_GRP.add(TIDX31);
    RBC_GRP.add(TIDX14);
    RBC_GRP.add(TIDX34);
    RBC_GRP.add(TIDX17);
    RBC_GRP.add(TIDX18);
    RBC_GRP.add(TIDX19);
    RBC_GRP.add(TIDX27);

    //---- SCAN33_GRP ----
    SCAN33_GRP.add(SCAN33);
    SCAN33_GRP.add(SCAN33_E);
    SCAN33_GRP.add(SCAN33_T);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_47;
  private JLabel OBJ_41;
  private XRiCheckBox VERR;
  private XRiCalendrier WDAT1X;
  private XRiTextField INDNUM;
  private JLabel OBJ_43;
  private XRiTextField INDETB;
  private JLabel OBJ_95;
  private XRiComboBox ARG3A;
  private JPanel p_tete_droite;
  private JLabel lb_requete;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt13;
  private SNPanel p_centrage;
  private SNPanelContenu p_contenu;
  private JPanel panel2;
  private JCheckBox tous;
  private XRiComboBox ARG4A;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2N;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5A;
  private JLabel OBJ_59;
  private JLabel OBJ_68;
  private JLabel label1;
  private XRiComboBox PLA4A;
  private XRiTextField ARGDOC;
  private JLabel OBJ_66;
  private XRiTextField PLA2N;
  private SNComboBox cbTypeCompteClient;
  private SNClient snClient;
  private JPanel panel21;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG6A;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7A;
  private XRiTextField ARG8A;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG10A;
  private XRiRadioButton TIDX12;
  private XRiTextField ARG12A;
  private XRiTextField ARG28A;
  private XRiTextField ARG11A;
  private XRiTextField ARG37A;
  private JLabel OBJ_106;
  private JLabel OBJ_94;
  private JLabel OBJ_109;
  private JLabel OBJ_137;
  private XRiTextField ARG56A;
  private JLabel OBJ_162;
  private XRiCheckBox DOCSPE;
  private JPanel panel4;
  private JPanel panel41;
  private XRiRadioButton TIDX15;
  private XRiTextField ARG15A;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG16A;
  private XRiRadioButton TIDX36;
  private XRiTextField ARG36A;
  private JPanel panel5;
  private XRiRadioButton TIDX20;
  private XRiCalendrier ARG20D;
  private XRiCalendrier PLA20D;
  private XRiRadioButton TIDX21;
  private XRiCalendrier ARG21D;
  private XRiCalendrier PLA21D;
  private XRiRadioButton TIDX25;
  private XRiCalendrier ARG25D;
  private XRiCalendrier PLA25D;
  private JLabel OBJ_85;
  private JLabel OBJ_86;
  private JPanel panel6;
  private XRiRadioButton TIDX31;
  private XRiTextField ARG31N;
  private XRiTextField PLA31N;
  private XRiRadioButton TIDX14;
  private XRiTextField ARG14A;
  private XRiRadioButton TIDX34;
  private XRiTextField ARG34A;
  private JLabel OBJ_113;
  private JLabel OBJ_114;
  private XRiTextField ARG44A;
  private JPanel panel7;
  private XRiRadioButton TIDX17;
  private XRiTextField ARG17A;
  private XRiRadioButton TIDX18;
  private XRiTextField ARG18A;
  private XRiRadioButton TIDX19;
  private XRiTextField ARG19A;
  private XRiRadioButton TIDX27;
  private XRiTextField ARG27A;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTD2;
  private JMenuItem OBJ_20;
  private XRiComboBox ARG33A;
  private XRiRadioButton SCAN33;
  private XRiRadioButton SCAN33_E;
  private XRiRadioButton SCAN33_T;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt11;
  private XRiTextField ARG9N1;
  private XRiTextField ARG9N2;
  private JPanel panel3;
  private JLabel OBJ_62;
  private XRiTextField WNOM;
  private JLabel OBJ_71;
  private XRiTextField WCDP;
  private JLabel OBJ_75;
  private XRiTextField WTEL;
  private JPanel panel31;
  private JLabel OBJ_138;
  private XRiTextField ARG39A;
  private ButtonGroup SCAN33_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
