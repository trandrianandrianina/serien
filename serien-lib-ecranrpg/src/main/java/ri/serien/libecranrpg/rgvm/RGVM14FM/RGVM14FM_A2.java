
package ri.serien.libecranrpg.rgvm.RGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM14FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] ARG17A_Value = { "", "+", };
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private boolean suivi = false;
  private boolean avoirPartiel = false;
  
  public RGVM14FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ARG17A.setValeurs(ARG17A_Value, null);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, null, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
    SCAN17.setValeurs("N", SCAN17_GRP);
    SCAN17_INCLUS.setValeurs("E");
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNUM@")).trim());
    INDNFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNFA@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    INDSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDSUF@")).trim());
    E1RCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1RCC@")).trim());
    E1NCC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@E1NCC@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WNOM@")).trim());
    WA1LIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WA1LIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    avoirPartiel = lexique.HostFieldGetData("TITRE").trim().equalsIgnoreCase("Avoir partiel");
    panel1.setVisible(!avoirPartiel);
    if (avoirPartiel) {
      WTP01.setComponentPopupMenu(popupMenu3);
      p_contenu.setPreferredSize(new Dimension(720, 340));
    }
    INDNUM.setVisible(lexique.isTrue("N85"));
    INDSUF.setVisible(lexique.isTrue("N85"));
    INDNFA.setVisible(lexique.isTrue("85"));
    SCAN17_INCLUS.setVisible(lexique.HostFieldGetData("ARG17A").equalsIgnoreCase("+"));
    SCAN17.setVisible(lexique.HostFieldGetData("ARG17A").equalsIgnoreCase("+"));
    // Test du mode suivi de commande
    suivi = (lexique.HostFieldGetData("SUIVICMD").trim().equalsIgnoreCase("1"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
    riSousMenu6.setVisible(!suivi);
    riSousMenu7.setVisible(!suivi);
    riSousMenu8.setVisible(!suivi);
    riSousMenu9.setVisible(!suivi);
    riSousMenu10.setVisible(suivi);
    lbSuiviCmd.setVisible(suivi);
    ARG16N1.setEnabled(!suivi);
    ARG16N2.setEnabled(!suivi);
    
    if (suivi) {
      WTP01.setComponentPopupMenu(popupMenu2);
    }
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    if (avoirPartiel) {
      lexique.HostScreenSendKey(this, "F7");
    }
    else {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_46ActionPerformed() {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_27ActionPerformed() {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_28ActionPerformed() {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_29ActionPerformed() {
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_30ActionPerformed() {
    WTP01.setValeurTop("!");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_31ActionPerformed() {
    WTP01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed() {
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed() {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed() {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_35ActionPerformed() {
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed() {
    WTP01.setValeurTop("Z");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed() {
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_38ActionPerformed() {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_39ActionPerformed() {
    WTP01.setValeurTop("K");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_40ActionPerformed() {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_41ActionPerformed() {
    
  }
  
  private void OBJ_42ActionPerformed() {
    WTP01.setValeurTop("é");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_43ActionPerformed() {
    WTP01.setValeurTop("ê");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_44ActionPerformed() {
    WTP01.setValeurTop("ë");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_45ActionPerformed() {
    WTP01.setValeurTop("È");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_47ActionPerformed() {
    WTP01.setValeurTop("|");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      if (avoirPartiel) {
        String ligneActuelle = lexique.HostFieldGetData(
            "LD" + ((WTP01.getSelectedRow() + 1) < 10 ? "0" + (WTP01.getSelectedRow() + 1) : (WTP01.getSelectedRow() + 1)));
        if (ligneActuelle != null && !ligneActuelle.trim().isEmpty()) {
          if ((ligneActuelle.substring(ligneActuelle.length() - 1).equals("+"))) {
            WTP01.setValeurTop("-");
            
          }
          else {
            WTP01.setValeurTop("+");
            
          }
        }
      }
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem4ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem5ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("B");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem6ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem7ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem8ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    INDNUM = new RiZoneSortie();
    INDNFA = new RiZoneSortie();
    OBJ_51 = new JLabel();
    WETB = new RiZoneSortie();
    INDSUF = new RiZoneSortie();
    OBJ_52 = new JLabel();
    E1RCC = new RiZoneSortie();
    E1NCC = new RiZoneSortie();
    p_tete_droite = new JPanel();
    lbSuiviCmd = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ARG16N1 = new XRiTextField();
    OBJ_61 = new JLabel();
    ARG16N2 = new XRiTextField();
    OBJ_64 = new JLabel();
    ARG3A = new XRiTextField();
    ARG17A = new XRiComboBox();
    SCAN17 = new XRiRadioButton();
    SCAN17_INCLUS = new XRiRadioButton();
    OBJ_66 = new JLabel();
    WNOM = new RiZoneSortie();
    WA1LIB = new RiZoneSortie();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    OBJ_46 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_27 = new JMenuItem();
    OBJ_28 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_35 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    OBJ_38 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_47 = new JMenuItem();
    BT_ChgSoc = new JButton();
    OBJ_58 = new JLabel();
    popupMenu1 = new JPopupMenu();
    menuItem4 = new JMenuItem();
    menuItem3 = new JMenuItem();
    popupMenu2 = new JPopupMenu();
    menuItem6 = new JMenuItem();
    menuItem7 = new JMenuItem();
    menuItem8 = new JMenuItem();
    menuItem5 = new JMenuItem();
    popupMenu3 = new JPopupMenu();
    menuItem1 = new JMenuItem();
    menuItem2 = new JMenuItem();
    menuItem9 = new JMenuItem();
    SCAN17_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Lignes du document");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(950, 32));
          p_tete_gauche.setMinimumSize(new Dimension(950, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_49 ----
          OBJ_49.setText("Etablissement");
          OBJ_49.setName("OBJ_49");

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setOpaque(false);
          INDNUM.setText("@INDNUM@");
          INDNUM.setHorizontalAlignment(SwingConstants.RIGHT);
          INDNUM.setName("INDNUM");

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(null);
          INDNFA.setOpaque(false);
          INDNFA.setText("@INDNFA@");
          INDNFA.setHorizontalAlignment(SwingConstants.RIGHT);
          INDNFA.setName("INDNFA");

          //---- OBJ_51 ----
          OBJ_51.setText("Num\u00e9ro");
          OBJ_51.setName("OBJ_51");

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setOpaque(false);
          INDSUF.setText("@INDSUF@");
          INDSUF.setName("INDSUF");

          //---- OBJ_52 ----
          OBJ_52.setText("R\u00e9f\u00e9rences de commande");
          OBJ_52.setName("OBJ_52");

          //---- E1RCC ----
          E1RCC.setComponentPopupMenu(null);
          E1RCC.setOpaque(false);
          E1RCC.setText("@E1RCC@");
          E1RCC.setHorizontalAlignment(SwingConstants.LEADING);
          E1RCC.setHorizontalTextPosition(SwingConstants.LEADING);
          E1RCC.setName("E1RCC");

          //---- E1NCC ----
          E1NCC.setComponentPopupMenu(null);
          E1NCC.setOpaque(false);
          E1NCC.setText("@E1NCC@");
          E1NCC.setHorizontalAlignment(SwingConstants.LEADING);
          E1NCC.setHorizontalTextPosition(SwingConstants.LEADING);
          E1NCC.setName("E1NCC");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                  .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1RCC, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(E1NCC, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addComponent(E1RCC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(E1NCC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(20, 0));
          p_tete_droite.setMinimumSize(new Dimension(20, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);

        //---- lbSuiviCmd ----
        lbSuiviCmd.setText("Suivi de commande");
        lbSuiviCmd.setFont(lbSuiviCmd.getFont().deriveFont(lbSuiviCmd.getFont().getStyle() | Font.BOLD));
        lbSuiviCmd.setName("lbSuiviCmd");
        barre_tete.add(lbSuiviCmd);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Totalisation");
              riSousMenu_bt6.setToolTipText("Totalisation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Transfert tableur");
              riSousMenu_bt7.setToolTipText("Transfert de la liste obtenue vers le tableur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Stock.  personnalisation");
              riSousMenu_bt8.setToolTipText("Stockage de la personnalisation programme");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Recherche bon, facture");
              riSousMenu_bt9.setToolTipText("Recherche bon ou facture");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Autres vues");
              riSousMenu_bt10.setToolTipText("Autres vues");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1020, 500));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1020, 500));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(new GridBagLayout());
          ((GridBagLayout)p_contenu.getLayout()).columnWidths = new int[] {1012, 0};
          ((GridBagLayout)p_contenu.getLayout()).rowHeights = new int[] {50, 165, 325, 0};
          ((GridBagLayout)p_contenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
          ((GridBagLayout)p_contenu.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- ARG16N1 ----
            ARG16N1.setComponentPopupMenu(null);
            ARG16N1.setHorizontalAlignment(SwingConstants.RIGHT);
            ARG16N1.setName("ARG16N1");
            panel1.add(ARG16N1);
            ARG16N1.setBounds(60, 20, 70, ARG16N1.getPreferredSize().height);

            //---- OBJ_61 ----
            OBJ_61.setText("Client");
            OBJ_61.setName("OBJ_61");
            panel1.add(OBJ_61);
            OBJ_61.setBounds(15, 24, 45, 20);

            //---- ARG16N2 ----
            ARG16N2.setComponentPopupMenu(null);
            ARG16N2.setHorizontalAlignment(SwingConstants.RIGHT);
            ARG16N2.setName("ARG16N2");
            panel1.add(ARG16N2);
            ARG16N2.setBounds(130, 20, 40, ARG16N2.getPreferredSize().height);

            //---- OBJ_64 ----
            OBJ_64.setText("Article");
            OBJ_64.setName("OBJ_64");
            panel1.add(OBJ_64);
            OBJ_64.setBounds(15, 61, 40, 20);

            //---- ARG3A ----
            ARG3A.setComponentPopupMenu(null);
            ARG3A.setName("ARG3A");
            panel1.add(ARG3A);
            ARG3A.setBounds(60, 57, 210, ARG3A.getPreferredSize().height);

            //---- ARG17A ----
            ARG17A.setModel(new DefaultComboBoxModel(new String[] {
              "Tous",
              "S\u00e9lectionn\u00e9s"
            }));
            ARG17A.setComponentPopupMenu(null);
            ARG17A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG17A.setName("ARG17A");
            panel1.add(ARG17A);
            ARG17A.setBounds(60, 94, 110, ARG17A.getPreferredSize().height);

            //---- SCAN17 ----
            SCAN17.setText("Exclus");
            SCAN17.setComponentPopupMenu(BTD);
            SCAN17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN17.setName("SCAN17");
            panel1.add(SCAN17);
            SCAN17.setBounds(202, 97, 68, 20);

            //---- SCAN17_INCLUS ----
            SCAN17_INCLUS.setText("Inclus");
            SCAN17_INCLUS.setComponentPopupMenu(BTD);
            SCAN17_INCLUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN17_INCLUS.setName("SCAN17_INCLUS");
            panel1.add(SCAN17_INCLUS);
            SCAN17_INCLUS.setBounds(280, 97, 68, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("Etat");
            OBJ_66.setName("OBJ_66");
            panel1.add(OBJ_66);
            OBJ_66.setBounds(25, 97, 26, 20);

            //---- WNOM ----
            WNOM.setText("@WNOM@");
            WNOM.setName("WNOM");
            panel1.add(WNOM);
            WNOM.setBounds(180, 22, 310, WNOM.getPreferredSize().height);

            //---- WA1LIB ----
            WA1LIB.setText("@WA1LIB@");
            WA1LIB.setName("WA1LIB");
            panel1.add(WA1LIB);
            WA1LIB.setBounds(280, 59, 510, WA1LIB.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(new GridBagLayout());
            ((GridBagLayout)panel2.getLayout()).columnWidths = new int[] {970, 28, 0};
            ((GridBagLayout)panel2.getLayout()).rowHeights = new int[] {137, 132, 0};
            ((GridBagLayout)panel2.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
            ((GridBagLayout)panel2.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setMinimumSize(new Dimension(965, 250));
              SCROLLPANE_LIST.setPreferredSize(new Dimension(965, 250));
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setFont(new Font("Courier New", Font.PLAIN, 11));
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setMinimumSize(new Dimension(970, 245));
              WTP01.setPreferredSize(new Dimension(970, 245));
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 2, 1.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 5), 0, 0));

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setMaximumSize(new Dimension(28, 135));
            BT_PGUP.setMinimumSize(new Dimension(28, 135));
            BT_PGUP.setPreferredSize(new Dimension(28, 135));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.NORTHWEST, GridBagConstraints.NONE,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setMaximumSize(new Dimension(28, 135));
            BT_PGDOWN.setMinimumSize(new Dimension(28, 135));
            BT_PGDOWN.setPreferredSize(new Dimension(28, 135));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.SOUTHWEST, GridBagConstraints.NONE,
              new Insets(0, 0, 0, 0), 0, 0));
          }
          p_contenu.add(panel2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choix");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- OBJ_18 ----
      OBJ_18.setText("Affichage");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_19 ----
      OBJ_19.setText("Extraction");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_20 ----
      OBJ_20.setText("Duplication");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Acc\u00e9s aux lignes de pied");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);

      //---- OBJ_22 ----
      OBJ_22.setText("Acc\u00e9s au choix des options");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);

      //---- OBJ_23 ----
      OBJ_23.setText("Acc\u00e9s aux achats");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      BTD.add(OBJ_23);

      //---- OBJ_46 ----
      OBJ_46.setText("Acc\u00e9s au colisage");
      OBJ_46.setName("OBJ_46");
      OBJ_46.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_46ActionPerformed();
        }
      });
      BTD.add(OBJ_46);

      //---- OBJ_24 ----
      OBJ_24.setText("Acc\u00e9s au stocks");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_27 ----
      OBJ_27.setText("Affectation de commande");
      OBJ_27.setName("OBJ_27");
      OBJ_27.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_27ActionPerformed();
        }
      });
      BTD.add(OBJ_27);

      //---- OBJ_28 ----
      OBJ_28.setText("Reliquats");
      OBJ_28.setName("OBJ_28");
      OBJ_28.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_28ActionPerformed();
        }
      });
      BTD.add(OBJ_28);

      //---- OBJ_29 ----
      OBJ_29.setText("Extraction quantit\u00e9s disponibles");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed();
        }
      });
      BTD.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Autorisation");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed();
        }
      });
      BTD.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("Calcul cl\u00e9 d'autorisation");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed();
        }
      });
      BTD.add(OBJ_31);

      //---- OBJ_32 ----
      OBJ_32.setText("Historique de bon");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed();
        }
      });
      BTD.add(OBJ_32);

      //---- OBJ_33 ----
      OBJ_33.setText("Duplication avec changement de tiers");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed();
        }
      });
      BTD.add(OBJ_33);

      //---- OBJ_34 ----
      OBJ_34.setText("Duplication avec changement de type de facturation");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed();
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_35 ----
      OBJ_35.setText("Edition");
      OBJ_35.setName("OBJ_35");
      OBJ_35.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed();
        }
      });
      BTD.add(OBJ_35);

      //---- OBJ_36 ----
      OBJ_36.setText("Exportation vers tableur");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed();
        }
      });
      BTD.add(OBJ_36);

      //---- OBJ_37 ----
      OBJ_37.setText("Modification ent\u00eate apr\u00e8s facturation");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed();
        }
      });
      BTD.add(OBJ_37);

      //---- OBJ_38 ----
      OBJ_38.setText("D\u00e9v\u00e9rouillage");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed();
        }
      });
      BTD.add(OBJ_38);

      //---- OBJ_39 ----
      OBJ_39.setText("Options client");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed();
        }
      });
      BTD.add(OBJ_39);

      //---- OBJ_40 ----
      OBJ_40.setText("Rechiffrer");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed();
        }
      });
      BTD.add(OBJ_40);

      //---- OBJ_41 ----
      OBJ_41.setText("Tri\u00e9 par...");
      OBJ_41.setName("OBJ_41");
      OBJ_41.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_41ActionPerformed();
        }
      });
      BTD.add(OBJ_41);

      //---- OBJ_42 ----
      OBJ_42.setText("Affichage affectation adresse de stock");
      OBJ_42.setName("OBJ_42");
      OBJ_42.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_42ActionPerformed();
        }
      });
      BTD.add(OBJ_42);

      //---- OBJ_43 ----
      OBJ_43.setText("D\u00e9tail pr\u00e9paration");
      OBJ_43.setName("OBJ_43");
      OBJ_43.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_43ActionPerformed();
        }
      });
      BTD.add(OBJ_43);

      //---- OBJ_44 ----
      OBJ_44.setText("D\u00e9tail emballage");
      OBJ_44.setName("OBJ_44");
      OBJ_44.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_44ActionPerformed();
        }
      });
      BTD.add(OBJ_44);

      //---- OBJ_45 ----
      OBJ_45.setText("D\u00e9tail exp\u00e9dition");
      OBJ_45.setName("OBJ_45");
      OBJ_45.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_45ActionPerformed();
        }
      });
      BTD.add(OBJ_45);

      //---- OBJ_47 ----
      OBJ_47.setText("Annulation de la commande");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_47ActionPerformed();
        }
      });
      BTD.add(OBJ_47);
    }

    //---- BT_ChgSoc ----
    BT_ChgSoc.setText("");
    BT_ChgSoc.setToolTipText("Changement d'\u00e9tablissement");
    BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ChgSoc.setName("BT_ChgSoc");
    BT_ChgSoc.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ChgSocActionPerformed(e);
      }
    });

    //---- OBJ_58 ----
    OBJ_58.setText("R\u00e9sultat de la recherche");
    OBJ_58.setName("OBJ_58");

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- menuItem4 ----
      menuItem4.setText("Options article");
      menuItem4.setName("menuItem4");
      menuItem4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem4ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem4);
    }

    //---- menuItem3 ----
    menuItem3.setText("Affichage bon");
    menuItem3.setName("menuItem3");
    menuItem3.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        menuItem3ActionPerformed(e);
      }
    });

    //======== popupMenu2 ========
    {
      popupMenu2.setName("popupMenu2");

      //---- menuItem6 ----
      menuItem6.setText("Options article");
      menuItem6.setName("menuItem6");
      menuItem6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem6ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem6);

      //---- menuItem7 ----
      menuItem7.setText("Attendu/r\u00e9serv\u00e9");
      menuItem7.setName("menuItem7");
      menuItem7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem7ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem7);

      //---- menuItem8 ----
      menuItem8.setText("Achat li\u00e9");
      menuItem8.setName("menuItem8");
      menuItem8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem8ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem8);
    }

    //---- menuItem5 ----
    menuItem5.setText("Affichage du bon");
    menuItem5.setName("menuItem5");
    menuItem5.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        menuItem5ActionPerformed(e);
      }
    });

    //======== popupMenu3 ========
    {
      popupMenu3.setName("popupMenu3");

      //---- menuItem1 ----
      menuItem1.setText("S\u00e9lectionner");
      menuItem1.setName("menuItem1");
      menuItem1.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem1ActionPerformed(e);
        }
      });
      popupMenu3.add(menuItem1);

      //---- menuItem2 ----
      menuItem2.setText("D\u00e9selectionner");
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      popupMenu3.add(menuItem2);

      //---- menuItem9 ----
      menuItem9.setText("Options article");
      menuItem9.setName("menuItem9");
      menuItem9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem6ActionPerformed(e);
        }
      });
      popupMenu3.add(menuItem9);
    }

    //---- SCAN17_GRP ----
    SCAN17_GRP.add(SCAN17);
    SCAN17_GRP.add(SCAN17_INCLUS);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private RiZoneSortie INDNUM;
  private RiZoneSortie INDNFA;
  private JLabel OBJ_51;
  private RiZoneSortie WETB;
  private RiZoneSortie INDSUF;
  private JLabel OBJ_52;
  private RiZoneSortie E1RCC;
  private RiZoneSortie E1NCC;
  private JPanel p_tete_droite;
  private JLabel lbSuiviCmd;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField ARG16N1;
  private JLabel OBJ_61;
  private XRiTextField ARG16N2;
  private JLabel OBJ_64;
  private XRiTextField ARG3A;
  private XRiComboBox ARG17A;
  private XRiRadioButton SCAN17;
  private XRiRadioButton SCAN17_INCLUS;
  private JLabel OBJ_66;
  private RiZoneSortie WNOM;
  private RiZoneSortie WA1LIB;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_23;
  private JMenuItem OBJ_46;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_27;
  private JMenuItem OBJ_28;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_35;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_37;
  private JMenuItem OBJ_38;
  private JMenuItem OBJ_39;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_47;
  private JButton BT_ChgSoc;
  private JLabel OBJ_58;
  private JPopupMenu popupMenu1;
  private JMenuItem menuItem4;
  private JMenuItem menuItem3;
  private JPopupMenu popupMenu2;
  private JMenuItem menuItem6;
  private JMenuItem menuItem7;
  private JMenuItem menuItem8;
  private JMenuItem menuItem5;
  private JPopupMenu popupMenu3;
  private JMenuItem menuItem1;
  private JMenuItem menuItem2;
  private JMenuItem menuItem9;
  private ButtonGroup SCAN17_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
