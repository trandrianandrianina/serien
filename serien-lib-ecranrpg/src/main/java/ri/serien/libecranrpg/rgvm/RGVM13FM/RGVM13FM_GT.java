
package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_GT extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public RGVM13FM_GT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Préparation logistique");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TOT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    NBBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBBON@")).trim());
    VOLT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@VOLT@")).trim());
    PDST.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@PDST@")).trim());
    VOLT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LGMX@")).trim());
    M2PT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@M2PT@")).trim());
    UNTQ1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
    UNTQ9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTQ1@")).trim());
    UNTL9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@UNTL1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_7 = new JLabel();
    DLMINX = new XRiCalendrier();
    DLMAXX = new XRiCalendrier();
    OBJ_8 = new JLabel();
    OBJ_10 = new JLabel();
    TOT1 = new RiZoneSortie();
    NBBON = new RiZoneSortie();
    OBJ_12 = new JLabel();
    VOLT = new RiZoneSortie();
    OBJ_13 = new JLabel();
    OBJ_14 = new JLabel();
    PDST = new RiZoneSortie();
    OBJ_15 = new JLabel();
    VOLT3 = new RiZoneSortie();
    OBJ_16 = new JLabel();
    M2PT = new RiZoneSortie();
    separator1 = compFactory.createSeparator("Unit\u00e9s de transport");
    UNTQ1 = new RiZoneSortie();
    UNTL1 = new RiZoneSortie();
    UNTQ2 = new RiZoneSortie();
    UNTL2 = new RiZoneSortie();
    UNTQ3 = new RiZoneSortie();
    UNTL3 = new RiZoneSortie();
    UNTQ4 = new RiZoneSortie();
    UNTL4 = new RiZoneSortie();
    UNTQ5 = new RiZoneSortie();
    UNTL5 = new RiZoneSortie();
    UNTQ6 = new RiZoneSortie();
    UNTL6 = new RiZoneSortie();
    UNTQ7 = new RiZoneSortie();
    UNTL7 = new RiZoneSortie();
    UNTQ8 = new RiZoneSortie();
    UNTL8 = new RiZoneSortie();
    UNTQ9 = new RiZoneSortie();
    UNTL9 = new RiZoneSortie();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(670, 435));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Synth\u00e8se des commandes s\u00e9lectionn\u00e9es"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_7 ----
          OBJ_7.setText("Livraisons du");
          OBJ_7.setName("OBJ_7");
          panel1.add(OBJ_7);
          OBJ_7.setBounds(20, 45, 120, 28);

          //---- DLMINX ----
          DLMINX.setName("DLMINX");
          panel1.add(DLMINX);
          DLMINX.setBounds(150, 45, 105, DLMINX.getPreferredSize().height);

          //---- DLMAXX ----
          DLMAXX.setName("DLMAXX");
          panel1.add(DLMAXX);
          DLMAXX.setBounds(355, 45, 105, DLMAXX.getPreferredSize().height);

          //---- OBJ_8 ----
          OBJ_8.setText("au");
          OBJ_8.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_8.setName("OBJ_8");
          panel1.add(OBJ_8);
          OBJ_8.setBounds(240, 45, 115, 28);

          //---- OBJ_10 ----
          OBJ_10.setText("Chiffre d'affaires HT");
          OBJ_10.setName("OBJ_10");
          panel1.add(OBJ_10);
          OBJ_10.setBounds(20, 81, 120, 28);

          //---- TOT1 ----
          TOT1.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT1.setText("@TOT1@");
          TOT1.setName("TOT1");
          panel1.add(TOT1);
          TOT1.setBounds(150, 83, 120, TOT1.getPreferredSize().height);

          //---- NBBON ----
          NBBON.setText("@NBBON@");
          NBBON.setHorizontalAlignment(SwingConstants.RIGHT);
          NBBON.setName("NBBON");
          panel1.add(NBBON);
          NBBON.setBounds(150, 117, 70, NBBON.getPreferredSize().height);

          //---- OBJ_12 ----
          OBJ_12.setText("Nombre de bons");
          OBJ_12.setName("OBJ_12");
          panel1.add(OBJ_12);
          OBJ_12.setBounds(20, 115, 120, 28);

          //---- VOLT ----
          VOLT.setText("@VOLT@");
          VOLT.setHorizontalAlignment(SwingConstants.RIGHT);
          VOLT.setName("VOLT");
          panel1.add(VOLT);
          VOLT.setBounds(150, 151, 80, VOLT.getPreferredSize().height);

          //---- OBJ_13 ----
          OBJ_13.setText("Volume total");
          OBJ_13.setName("OBJ_13");
          panel1.add(OBJ_13);
          OBJ_13.setBounds(20, 149, 120, 28);

          //---- OBJ_14 ----
          OBJ_14.setText("Poids total");
          OBJ_14.setName("OBJ_14");
          panel1.add(OBJ_14);
          OBJ_14.setBounds(265, 149, 95, 28);

          //---- PDST ----
          PDST.setText("@PDST@");
          PDST.setHorizontalAlignment(SwingConstants.RIGHT);
          PDST.setName("PDST");
          panel1.add(PDST);
          PDST.setBounds(365, 151, 80, PDST.getPreferredSize().height);

          //---- OBJ_15 ----
          OBJ_15.setText("Longueur maximum");
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(20, 183, 120, 28);

          //---- VOLT3 ----
          VOLT3.setText("@LGMX@");
          VOLT3.setHorizontalAlignment(SwingConstants.RIGHT);
          VOLT3.setName("VOLT3");
          panel1.add(VOLT3);
          VOLT3.setBounds(150, 185, 80, VOLT3.getPreferredSize().height);

          //---- OBJ_16 ----
          OBJ_16.setText("m\u00b2 au plancher");
          OBJ_16.setName("OBJ_16");
          panel1.add(OBJ_16);
          OBJ_16.setBounds(265, 183, 95, 28);

          //---- M2PT ----
          M2PT.setText("@M2PT@");
          M2PT.setHorizontalAlignment(SwingConstants.RIGHT);
          M2PT.setName("M2PT");
          panel1.add(M2PT);
          M2PT.setBounds(365, 185, 80, M2PT.getPreferredSize().height);

          //---- separator1 ----
          separator1.setName("separator1");
          panel1.add(separator1);
          separator1.setBounds(15, 230, 440, separator1.getPreferredSize().height);

          //---- UNTQ1 ----
          UNTQ1.setText("@UNTQ1@");
          UNTQ1.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ1.setName("UNTQ1");
          panel1.add(UNTQ1);
          UNTQ1.setBounds(20, 260, 40, UNTQ1.getPreferredSize().height);

          //---- UNTL1 ----
          UNTL1.setText("@UNTL1@");
          UNTL1.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL1.setName("UNTL1");
          panel1.add(UNTL1);
          UNTL1.setBounds(65, 260, 160, UNTL1.getPreferredSize().height);

          //---- UNTQ2 ----
          UNTQ2.setText("@UNTQ1@");
          UNTQ2.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ2.setName("UNTQ2");
          panel1.add(UNTQ2);
          UNTQ2.setBounds(20, 285, 40, UNTQ2.getPreferredSize().height);

          //---- UNTL2 ----
          UNTL2.setText("@UNTL1@");
          UNTL2.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL2.setName("UNTL2");
          panel1.add(UNTL2);
          UNTL2.setBounds(65, 285, 160, UNTL2.getPreferredSize().height);

          //---- UNTQ3 ----
          UNTQ3.setText("@UNTQ1@");
          UNTQ3.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ3.setName("UNTQ3");
          panel1.add(UNTQ3);
          UNTQ3.setBounds(20, 310, 40, UNTQ3.getPreferredSize().height);

          //---- UNTL3 ----
          UNTL3.setText("@UNTL1@");
          UNTL3.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL3.setName("UNTL3");
          panel1.add(UNTL3);
          UNTL3.setBounds(65, 310, 160, UNTL3.getPreferredSize().height);

          //---- UNTQ4 ----
          UNTQ4.setText("@UNTQ1@");
          UNTQ4.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ4.setName("UNTQ4");
          panel1.add(UNTQ4);
          UNTQ4.setBounds(20, 335, 40, UNTQ4.getPreferredSize().height);

          //---- UNTL4 ----
          UNTL4.setText("@UNTL1@");
          UNTL4.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL4.setName("UNTL4");
          panel1.add(UNTL4);
          UNTL4.setBounds(65, 335, 160, UNTL4.getPreferredSize().height);

          //---- UNTQ5 ----
          UNTQ5.setText("@UNTQ1@");
          UNTQ5.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ5.setName("UNTQ5");
          panel1.add(UNTQ5);
          UNTQ5.setBounds(20, 360, 40, UNTQ5.getPreferredSize().height);

          //---- UNTL5 ----
          UNTL5.setText("@UNTL1@");
          UNTL5.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL5.setName("UNTL5");
          panel1.add(UNTL5);
          UNTL5.setBounds(65, 360, 160, UNTL5.getPreferredSize().height);

          //---- UNTQ6 ----
          UNTQ6.setText("@UNTQ1@");
          UNTQ6.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ6.setName("UNTQ6");
          panel1.add(UNTQ6);
          UNTQ6.setBounds(240, 260, 40, UNTQ6.getPreferredSize().height);

          //---- UNTL6 ----
          UNTL6.setText("@UNTL1@");
          UNTL6.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL6.setName("UNTL6");
          panel1.add(UNTL6);
          UNTL6.setBounds(285, 260, 160, UNTL6.getPreferredSize().height);

          //---- UNTQ7 ----
          UNTQ7.setText("@UNTQ1@");
          UNTQ7.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ7.setName("UNTQ7");
          panel1.add(UNTQ7);
          UNTQ7.setBounds(240, 285, 40, UNTQ7.getPreferredSize().height);

          //---- UNTL7 ----
          UNTL7.setText("@UNTL1@");
          UNTL7.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL7.setName("UNTL7");
          panel1.add(UNTL7);
          UNTL7.setBounds(285, 285, 160, UNTL7.getPreferredSize().height);

          //---- UNTQ8 ----
          UNTQ8.setText("@UNTQ1@");
          UNTQ8.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ8.setName("UNTQ8");
          panel1.add(UNTQ8);
          UNTQ8.setBounds(240, 310, 40, UNTQ8.getPreferredSize().height);

          //---- UNTL8 ----
          UNTL8.setText("@UNTL1@");
          UNTL8.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL8.setName("UNTL8");
          panel1.add(UNTL8);
          UNTL8.setBounds(285, 310, 160, UNTL8.getPreferredSize().height);

          //---- UNTQ9 ----
          UNTQ9.setText("@UNTQ1@");
          UNTQ9.setHorizontalAlignment(SwingConstants.LEADING);
          UNTQ9.setName("UNTQ9");
          panel1.add(UNTQ9);
          UNTQ9.setBounds(240, 335, 40, UNTQ9.getPreferredSize().height);

          //---- UNTL9 ----
          UNTL9.setText("@UNTL1@");
          UNTL9.setHorizontalAlignment(SwingConstants.LEADING);
          UNTL9.setName("UNTL9");
          panel1.add(UNTL9);
          UNTL9.setBounds(285, 335, 160, UNTL9.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 480, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 413, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_7;
  private XRiCalendrier DLMINX;
  private XRiCalendrier DLMAXX;
  private JLabel OBJ_8;
  private JLabel OBJ_10;
  private RiZoneSortie TOT1;
  private RiZoneSortie NBBON;
  private JLabel OBJ_12;
  private RiZoneSortie VOLT;
  private JLabel OBJ_13;
  private JLabel OBJ_14;
  private RiZoneSortie PDST;
  private JLabel OBJ_15;
  private RiZoneSortie VOLT3;
  private JLabel OBJ_16;
  private RiZoneSortie M2PT;
  private JComponent separator1;
  private RiZoneSortie UNTQ1;
  private RiZoneSortie UNTL1;
  private RiZoneSortie UNTQ2;
  private RiZoneSortie UNTL2;
  private RiZoneSortie UNTQ3;
  private RiZoneSortie UNTL3;
  private RiZoneSortie UNTQ4;
  private RiZoneSortie UNTL4;
  private RiZoneSortie UNTQ5;
  private RiZoneSortie UNTL5;
  private RiZoneSortie UNTQ6;
  private RiZoneSortie UNTL6;
  private RiZoneSortie UNTQ7;
  private RiZoneSortie UNTL7;
  private RiZoneSortie UNTQ8;
  private RiZoneSortie UNTL8;
  private RiZoneSortie UNTQ9;
  private RiZoneSortie UNTL9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
