
package ri.serien.libecranrpg.rgvm.RGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGVM03FM_A3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  // Combobox TYPCLI
  private String[] TYPCLI_Value = { "", "1", "3", "4", };
  
  /**
   * Constructeur.
   */
  public RGVM03FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    TIDX5.setValeurs("6", "RBC");
    TIDX13.setValeurs("13", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX22.setValeurs("22", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX2.setValeurs("2", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX4.setValeurs("5", "RBC");
    TIDX3.setValeurs("3", "RBC");
    TIDX8.setValeurs("8", "RBC");
    TYPCLI.setValeurs(TYPCLI_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Recherches multi-critères clients"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX8 = new XRiRadioButton();
    TIDX3 = new XRiRadioButton();
    TIDX4 = new XRiRadioButton();
    TIDX9 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX21 = new XRiRadioButton();
    TIDX22 = new XRiRadioButton();
    TIDX10 = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    ARG3A = new XRiTextField();
    ARG8A = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG21D = new XRiTextField();
    ARG22D = new XRiTextField();
    ARG2N = new XRiTextField();
    ARG5N = new XRiTextField();
    ARG10A = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG9A = new XRiTextField();
    ARG13A = new XRiTextField();
    barre_tete = new JMenuBar();
    P_Infos = new JPanel();
    TYPCLI = new XRiComboBox();
    OBJ_70 = new JLabel();
    INDCLI = new XRiTextField();
    OBJ_72 = new JLabel();
    INDETB = new XRiTextField();
    INDLIV = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    BTDA = new JPopupMenu();
    OBJ_20 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(630, 375));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");

          //---- TIDX8 ----
          TIDX8.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
          TIDX8.setComponentPopupMenu(BTD);
          TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX8.setName("TIDX8");

          //---- TIDX3 ----
          TIDX3.setText("Code alphab\u00e9tique");
          TIDX3.setComponentPopupMenu(BTD);
          TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX3.setName("TIDX3");

          //---- TIDX4 ----
          TIDX4.setText("Code postal");
          TIDX4.setComponentPopupMenu(BTD);
          TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX4.setName("TIDX4");

          //---- TIDX9 ----
          TIDX9.setText("Repr\u00e9sentants");
          TIDX9.setComponentPopupMenu(BTD);
          TIDX9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX9.setName("TIDX9");

          //---- TIDX2 ----
          TIDX2.setText("Num\u00e9ro client");
          TIDX2.setComponentPopupMenu(BTD);
          TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX2.setName("TIDX2");

          //---- TIDX21 ----
          TIDX21.setText("Date de cr\u00e9ation");
          TIDX21.setComponentPopupMenu(BTD);
          TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX21.setName("TIDX21");

          //---- TIDX22 ----
          TIDX22.setText("Date derni\u00e8re visite");
          TIDX22.setComponentPopupMenu(BTD);
          TIDX22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX22.setName("TIDX22");

          //---- TIDX10 ----
          TIDX10.setText("Zone g\u00e9ographique");
          TIDX10.setComponentPopupMenu(BTD);
          TIDX10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX10.setName("TIDX10");

          //---- TIDX13 ----
          TIDX13.setText("Magasin");
          TIDX13.setComponentPopupMenu(BTD);
          TIDX13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX13.setName("TIDX13");

          //---- ARG3A ----
          ARG3A.setComponentPopupMenu(BTD);
          ARG3A.setName("ARG3A");

          //---- ARG8A ----
          ARG8A.setComponentPopupMenu(BTD);
          ARG8A.setName("ARG8A");

          //---- TIDX5 ----
          TIDX5.setText("Cat\u00e9gorie");
          TIDX5.setComponentPopupMenu(BTD);
          TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          TIDX5.setName("TIDX5");

          //---- ARG21D ----
          ARG21D.setComponentPopupMenu(BTD);
          ARG21D.setName("ARG21D");

          //---- ARG22D ----
          ARG22D.setComponentPopupMenu(BTD);
          ARG22D.setName("ARG22D");

          //---- ARG2N ----
          ARG2N.setComponentPopupMenu(BTD);
          ARG2N.setName("ARG2N");

          //---- ARG5N ----
          ARG5N.setComponentPopupMenu(BTD);
          ARG5N.setName("ARG5N");

          //---- ARG10A ----
          ARG10A.setComponentPopupMenu(BTD);
          ARG10A.setName("ARG10A");

          //---- ARG6A ----
          ARG6A.setComponentPopupMenu(BTD);
          ARG6A.setName("ARG6A");

          //---- ARG9A ----
          ARG9A.setComponentPopupMenu(BTD);
          ARG9A.setName("ARG9A");

          //---- ARG13A ----
          ARG13A.setComponentPopupMenu(BTD);
          ARG13A.setName("ARG13A");

          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(14, 14, 14)
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX8, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGap(25, 25, 25)
                    .addComponent(ARG8A, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG5N, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX9, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG9A, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 84, GroupLayout.PREFERRED_SIZE)
                    .addGap(101, 101, 101)
                    .addComponent(ARG6A, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG2N, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX21, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG21D, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX22, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG22D, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX10, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG10A, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addComponent(TIDX13, GroupLayout.PREFERRED_SIZE, 158, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27)
                    .addComponent(ARG13A, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)))
                .addGap(75, 75, 75))
          );
          panel1Layout.setVerticalGroup(
            panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(panel1Layout.createParallelGroup()
                  .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX8, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG8A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX4, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG5N, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG9A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX5, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG6A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG2N, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX21, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG21D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX22, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG22D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX10, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG10A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel1Layout.createParallelGroup()
                  .addGroup(panel1Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(TIDX13, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ARG13A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
          );
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 417, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(33, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(30, Short.MAX_VALUE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== barre_tete ========
    {
      barre_tete.setPreferredSize(new Dimension(111, 30));
      barre_tete.setName("barre_tete");

      //======== P_Infos ========
      {
        P_Infos.setBorder(null);
        P_Infos.setMinimumSize(new Dimension(66, 37));
        P_Infos.setPreferredSize(new Dimension(66, 40));
        P_Infos.setOpaque(false);
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- TYPCLI ----
        TYPCLI.setModel(new DefaultComboBoxModel(new String[] {
          "Tous",
          "Grand public",
          "VPC",
          "Donneur d'ordre"
        }));
        TYPCLI.setToolTipText("type de client");
        TYPCLI.setComponentPopupMenu(BTD);
        TYPCLI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        TYPCLI.setName("TYPCLI");
        P_Infos.add(TYPCLI);
        TYPCLI.setBounds(305, 0, 195, TYPCLI.getPreferredSize().height);

        //---- OBJ_70 ----
        OBJ_70.setText("Etablissement");
        OBJ_70.setName("OBJ_70");
        P_Infos.add(OBJ_70);
        OBJ_70.setBounds(5, 5, 102, 20);

        //---- INDCLI ----
        INDCLI.setComponentPopupMenu(BTD);
        INDCLI.setName("INDCLI");
        P_Infos.add(INDCLI);
        INDCLI.setBounds(210, 0, 60, INDCLI.getPreferredSize().height);

        //---- OBJ_72 ----
        OBJ_72.setText("Client");
        OBJ_72.setName("OBJ_72");
        P_Infos.add(OBJ_72);
        OBJ_72.setBounds(165, 5, 49, 18);

        //---- INDETB ----
        INDETB.setComponentPopupMenu(BTD);
        INDETB.setName("INDETB");
        P_Infos.add(INDETB);
        INDETB.setBounds(95, 0, 40, INDETB.getPreferredSize().height);

        //---- INDLIV ----
        INDLIV.setToolTipText("Suffixe de livraison");
        INDLIV.setComponentPopupMenu(BTD);
        INDLIV.setName("INDLIV");
        P_Infos.add(INDLIV);
        INDLIV.setBounds(270, 0, 24, INDLIV.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      barre_tete.add(P_Infos);
    }
    add(barre_tete, BorderLayout.NORTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }

    //======== BTDA ========
    {
      BTDA.setName("BTDA");

      //---- OBJ_20 ----
      OBJ_20.setText("Aide en ligne");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTDA.add(OBJ_20);
    }

    //---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX8);
    RBC_GRP.add(TIDX3);
    RBC_GRP.add(TIDX4);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX22);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX13);
    RBC_GRP.add(TIDX5);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX4;
  private XRiRadioButton TIDX9;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX21;
  private XRiRadioButton TIDX22;
  private XRiRadioButton TIDX10;
  private XRiRadioButton TIDX13;
  private XRiTextField ARG3A;
  private XRiTextField ARG8A;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG21D;
  private XRiTextField ARG22D;
  private XRiTextField ARG2N;
  private XRiTextField ARG5N;
  private XRiTextField ARG10A;
  private XRiTextField ARG6A;
  private XRiTextField ARG9A;
  private XRiTextField ARG13A;
  private JMenuBar barre_tete;
  private JPanel P_Infos;
  private XRiComboBox TYPCLI;
  private JLabel OBJ_70;
  private XRiTextField INDCLI;
  private JLabel OBJ_72;
  private XRiTextField INDETB;
  private XRiTextField INDLIV;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  private JPopupMenu BTDA;
  private JMenuItem OBJ_20;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
