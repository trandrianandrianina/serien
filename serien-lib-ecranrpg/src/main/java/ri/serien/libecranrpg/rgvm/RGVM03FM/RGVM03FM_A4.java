
package ri.serien.libecranrpg.rgvm.RGVM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGVM03FM_A4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TITG", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 1050, };
  
  private String[] ARG28A_Value = { " ", "1", "3", "4", "5" };
  private String[] ARG19N_Value = { "0", "0", "1", "2", "5", "6", "9" };
  private String[] ARG44X_Value = { " ", "P", "*" };
  
  private boolean afficherListeResultat = false;
  private boolean isS31 = false;
  private boolean isCharge = false;
  
  /**
   * Constructeur.
   */
  public RGVM03FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    // Il faudra décommenter des lignes dans le RPG lors de l'activation de cette partie
    TIDX22.setValeurs("22", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX10.setValeurs("10", "RBC");
    TIDX27.setValeurs("27", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX14.setValeurs("14", "RBC");
    TIDX12.setValeurs("12", "RBC");
    TIDX11.setValeurs("11", "RBC");
    TIDX20.setValeurs("20", "RBC");
    TIDX7.setValeurs("7", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX24.setValeurs("24", "RBC");
    TIDX26.setValeurs("26", "RBC");
    TIDX18.setValeurs("18", "RBC");
    TIDX9.setValeurs("9", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX13.setValeurs("13", "RBC");
    TIDX2.setValeurs("2", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX8.setValeurs("8", "RBC");
    TIDX3.setValeurs("3", "RBC");
    
    SCAN19.setValeurs(" ", SCAN19_GRP);
    SCAN19_C.setValeurs("N");
    ARG28A.setValeurs(ARG28A_Value, null);
    ARG19N.setValeurs(ARG19N_Value, null);
    ARG44X.setValeurs(ARG44X_Value, null);
    
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    
    
    OBJ_DEBL.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    OBJ_FINL.setIcon(lexique.chargerImage("images/pfin20.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt1.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Recherche client @LIBPG@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WTI1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI1@")).trim());
    WTI2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI2@")).trim());
    WTI3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI3@")).trim());
    WTI4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI4@")).trim());
    WTI5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WTI5@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // Permet de gérer les modes d'affichage
    setDiverses();
    
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    isS31 = lexique.HostFieldGetData("SET031").equals("9");
    isCharge = true;
    
    // Titre
    if (lexique.HostFieldGetData("LIBPG").trim().isEmpty()) {
      p_bpresentation.setText("Gestion des clients");
    }
    else {
      p_bpresentation.setText("Recherche multi-critères de clients");
    }
    
    // Composant établissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    // Composant établissement duplication
    snEtablissementDup.setSession(getSession());
    snEtablissementDup.charger(false);
    snEtablissementDup.setSelectionParChampRPG(lexique, "WIETB3");
    
    // Gestion de l'affichage des critères de recherche
    afficherListeResultat = lexique.HostFieldGetData("FMTS").trim().endsWith("A2");
    boolean afficherTousLesCriteres = lexique.isTrue("N61");
    pnlAutresCriteres.setVisible(afficherTousLesCriteres);
    pnlSelectionEtat.setVisible(afficherTousLesCriteres);
    pnlZonesPersonnalisees.setVisible(afficherTousLesCriteres);
    if (afficherTousLesCriteres) {
      riSousMenu_bt6.setToolTipText("Critères principaux");
      riSousMenu_bt6.setText("Critères principaux");
    }
    else {
      riSousMenu_bt6.setToolTipText("Tous les critères");
      riSousMenu_bt6.setText("Tous les critères");
    }
    
    // Gestion d'affichage de la liste des résultats de la recherche
    if (afficherListeResultat) {
      Integer nombreEnregistrement = Constantes.convertirTexteEnInteger(lexique.HostFieldGetData("NBRERL"));
      String titreListe = "Résultat de la recherche : " + nombreEnregistrement + " lignes";
      if (nombreEnregistrement.intValue() == 0) {
        titreListe = "Résultat de la recherche : 0 ligne";
      }
      OBJ_PLIST.setBorder(new TitledBorder(titreListe));
    }
    OBJ_PLIST.setVisible(afficherListeResultat);
    
    barre_tete_dup.setVisible(lexique.isTrue("56"));
    snEtablissementDup.setVisible(lexique.isTrue("56"));
    WICLI3.setVisible(lexique.isTrue("56"));
    WILIV3.setVisible(lexique.isTrue("56"));
    
    if (SCAN19_C.isSelected()) {
      riSousMenu_bt10.setText("Désactivés inclus");
      riSousMenu_bt10.setToolTipText("Clients désactivés inclus");
    }
    else {
      riSousMenu_bt10.setText("Désactivés exclus");
      riSousMenu_bt10.setToolTipText("Clients désactivés exclus");
    }
    
    lb_requete.setVisible(interpreteurD.analyseExpression("@AFFREQ@").equals("1"));
    
    // Click doit différent si on est sur les clients de passage
    if (lexique.HostFieldGetData("ARG44X").trim().equalsIgnoreCase("A")) {
      WTP01.setComponentPopupMenu(BTD4);
    }
    else {
      WTP01.setComponentPopupMenu(BTD2);
    }
    
    menuItem5.setVisible(isS31);
    menuItem6.setVisible(isS31);
    
    if (ARG19N.getSelectedIndex() == 0 && (SCAN19.isSelected() || SCAN19_C.isSelected())) {
      ARG19N.setSelectedIndex(1);
    }
    SCAN19.setEnabled(ARG19N.getSelectedIndex() != 0);
    SCAN19_C.setEnabled(ARG19N.getSelectedIndex() != 0);
    
    p_bpresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
    
    // Rafraichissement de l'écran pour prendre en compte certaines modifications graphiques
    repaint();
    revalidate();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    snEtablissementDup.renseignerChampRPG(lexique, "WIETB3");
    
    if (!SCAN19.isSelected() && !SCAN19_C.isSelected()) {
      lexique.HostFieldPutData("SCAN19", 0, "");
      lexique.HostFieldPutData("ARG19N", 0, "");
    }
  }
  
  private void OBJ_DEBLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_FINLActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void inviteActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void aideActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD3.getInvoker().getName());
  }
  
  private void choisirActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void modifierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void supprimerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void interrogerActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void statistiquesActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("?");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void deverActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void specifActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("S");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void histoActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    afficherListeResultat = false;
    // Inversion de l'indicateur suite au clic
    // 61=on : mode critères principaux
    // 61=off : mode tous les critères
    if (lexique.isTrue("61")) {
      lexique.SetOff(61);
    }
    else {
      lexique.SetOn(61);
    }
    
    // Détermination du mode en cours afin d'effectuer les transformations adaptées
    boolean afficherTousLesCriteres = lexique.isTrue("N61");
    // Afficher tous les critères
    if (afficherTousLesCriteres) {
      pnlAutresCriteres.setVisible(true);
      pnlSelectionEtat.setVisible(true);
      pnlZonesPersonnalisees.setVisible(true);
      // Modification du bouton du menu
      riSousMenu_bt6.setToolTipText("Critères principaux");
      riSousMenu_bt6.setText("Critères principaux");
    }
    // Afficher les critères principaux
    else {
      pnlAutresCriteres.setVisible(false);
      pnlSelectionEtat.setVisible(false);
      pnlZonesPersonnalisees.setVisible(false);
      // Modification du bouton du menu
      riSousMenu_bt6.setToolTipText("Tous les critères");
      riSousMenu_bt6.setText("Tous les critères");
    }
    
    // Rafraichissement de l'écran pour prendre en compte certaines modifications graphiques
    repaint();
    revalidate();
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    if (interpreteurD.analyseExpression("@AFFREQ@").equalsIgnoreCase("1")) {
      lexique.HostFieldPutData("AFFREQ", 0, " ");
      lb_requete.setVisible(false);
    }
    else {
      lexique.HostFieldPutData("AFFREQ", 0, "1");
      lb_requete.setVisible(true);
    }
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void riSousMenu_bt2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void menuItem1ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD3.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void menuItem4ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CPdevisActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CPcomActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CPlistecomActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void CPficheActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem5ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("P");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem6ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("U");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void ARG19NActionPerformed(ActionEvent e) {
    if (isCharge) {
      if (ARG19N.getSelectedIndex() == 0) {
        SCAN19_GRP.clearSelection();
      }
      else if (!SCAN19.isSelected() && !SCAN19_C.isSelected()) {
        SCAN19_C.setSelected(true);
      }
      SCAN19.setEnabled(ARG19N.getSelectedIndex() != 0);
      SCAN19_C.setEnabled(ARG19N.getSelectedIndex() != 0);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      if (lexique.getMode() == Lexical.MODE_DUPLICATION) {
        return;
      }
      snEtablissement.renseignerChampRPG(lexique, "INDETB");
      lexique.HostScreenSendKey(this, "Enter");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    P_Infos = new JPanel();
    OBJECT_3 = new JLabel();
    OBJECT_4 = new JLabel();
    INDCLI = new XRiTextField();
    INDLIV = new XRiTextField();
    ARG44X = new XRiComboBox();
    OBJECT_6 = new JLabel();
    snEtablissement = new SNEtablissement();
    p_tete_droite = new JPanel();
    lb_requete = new JLabel();
    barre_tete_dup = new JMenuBar();
    p_tete_gauche2 = new JPanel();
    OBJ_41_OBJ_42 = new JLabel();
    WICLI3 = new XRiTextField();
    WILIV3 = new XRiTextField();
    OBJECT_5 = new JLabel();
    snEtablissementDup = new SNEtablissement();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riMenu1 = new RiMenu();
    riMenu_bt1 = new RiMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    riSousMenu2 = new RiSousMenu();
    riSousMenu_bt2 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    panel1 = new JPanel();
    pnlCriteresPrincipaux = new JPanel();
    ARG3A = new XRiTextField();
    ARG8A = new XRiTextField();
    ARG4A = new XRiTextField();
    ARG5N = new XRiTextField();
    ARG2N = new XRiTextField();
    TIDX3 = new XRiRadioButton();
    TIDX8 = new XRiRadioButton();
    TIDX5 = new XRiRadioButton();
    TIDX2 = new XRiRadioButton();
    TIDX13 = new XRiRadioButton();
    ARG13A = new XRiTextField();
    OBJECT_13 = new JLabel();
    ARG31A = new XRiTextField();
    ARG6A = new XRiTextField();
    ARG33A = new XRiTextField();
    ARG34A = new XRiTextField();
    ARG35A = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    OBJECT_19 = new JLabel();
    TIDX9 = new XRiRadioButton();
    ARG9A = new XRiTextField();
    ARG23A = new XRiTextField();
    TIDX21 = new XRiRadioButton();
    ARG21D = new XRiCalendrier();
    pnlAutresCriteres = new JPanel();
    TIDX18 = new XRiRadioButton();
    ARG18A = new XRiTextField();
    TIDX26 = new XRiRadioButton();
    ARG26N = new XRiTextField();
    TIDX24 = new XRiRadioButton();
    ARG24A = new XRiTextField();
    ARG25A = new XRiTextField();
    TIDX15 = new XRiRadioButton();
    ARG15A = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG7A = new XRiTextField();
    TIDX20 = new XRiRadioButton();
    ARG20A = new XRiTextField();
    TIDX11 = new XRiRadioButton();
    ARG11A = new XRiTextField();
    TIDX12 = new XRiRadioButton();
    ARG12A = new XRiTextField();
    TIDX14 = new XRiRadioButton();
    ARG14A = new XRiTextField();
    TIDX16 = new XRiRadioButton();
    ARG16A = new XRiTextField();
    ARG17A = new XRiTextField();
    TIDX27 = new XRiRadioButton();
    ARG27N = new XRiTextField();
    ARG37A = new XRiTextField();
    ARG38A = new XRiTextField();
    OBJECT_20 = new JLabel();
    label2 = new JLabel();
    ARG32A = new XRiTextField();
    label1 = new JLabel();
    ARG30A = new XRiTextField();
    OBJECT_11 = new JLabel();
    TIDX10 = new XRiRadioButton();
    ARG10A = new XRiTextField();
    pnlSelectionEtat = new JPanel();
    SCAN19 = new XRiRadioButton();
    SCAN19_C = new XRiRadioButton();
    ARG19N = new XRiComboBox();
    pnlZonesPersonnalisees = new JPanel();
    WTI1 = new JLabel();
    WTI2 = new JLabel();
    WTI3 = new JLabel();
    WTI4 = new JLabel();
    WTI5 = new JLabel();
    ARG39A = new XRiTextField();
    ARG40A = new XRiTextField();
    ARG41A = new XRiTextField();
    ARG42A = new XRiTextField();
    ARG43A = new XRiTextField();
    OBJECT_10 = new JPanel();
    OBJECT_18 = new JLabel();
    OBJECT_15 = new JLabel();
    OBJECT_17 = new JLabel();
    WDEV = new XRiTextField();
    WBON = new XRiTextField();
    WNFA = new XRiTextField();
    OBJ_PLIST = new JPanel();
    panel3 = new JPanel();
    SCROLLPANE__LIST_ = new JScrollPane();
    WTP01 = new XRiTable();
    OBJ_DEBL = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    OBJ_FINL = new JButton();
    BTD = new JPopupMenu();
    invite = new JMenuItem();
    aide = new JMenuItem();
    BTD2 = new JPopupMenu();
    choisir = new JMenuItem();
    modifier = new JMenuItem();
    supprimer = new JMenuItem();
    menuItem4 = new JMenuItem();
    menuItem5 = new JMenuItem();
    menuItem6 = new JMenuItem();
    interroger = new JMenuItem();
    statistiques = new JMenuItem();
    dever = new JMenuItem();
    specif = new JMenuItem();
    histo = new JMenuItem();
    menuItem3 = new JMenuItem();
    BTD3 = new JPopupMenu();
    aide2 = new JMenuItem();
    ARG28A = new XRiComboBox();
    TIDX22 = new XRiRadioButton();
    ARG22D = new XRiCalendrier();
    BTD4 = new JPopupMenu();
    CPdevis = new JMenuItem();
    CPcom = new JMenuItem();
    CPlistecom = new JMenuItem();
    CPfiche = new JMenuItem();
    SCAN19_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1050, 640));
    setPreferredSize(new Dimension(1050, 640));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Recherche client @LIBPG@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(750, 28));
          p_tete_gauche.setMinimumSize(new Dimension(750, 28));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
          
          // ======== P_Infos ========
          {
            P_Infos.setBorder(null);
            P_Infos.setMinimumSize(new Dimension(930, 30));
            P_Infos.setOpaque(false);
            P_Infos.setPreferredSize(new Dimension(930, 30));
            P_Infos.setMaximumSize(new Dimension(930, 30));
            P_Infos.setName("P_Infos");
            P_Infos.setLayout(null);
            
            // ---- OBJECT_3 ----
            OBJECT_3.setText("Etablissement");
            OBJECT_3.setName("OBJECT_3");
            P_Infos.add(OBJECT_3);
            OBJECT_3.setBounds(7, 4, 85, 22);
            
            // ---- OBJECT_4 ----
            OBJECT_4.setText("Client");
            OBJECT_4.setName("OBJECT_4");
            P_Infos.add(OBJECT_4);
            OBJECT_4.setBounds(405, 5, 42, 22);
            
            // ---- INDCLI ----
            INDCLI.setName("INDCLI");
            P_Infos.add(INDCLI);
            INDCLI.setBounds(445, 0, 60, INDCLI.getPreferredSize().height);
            
            // ---- INDLIV ----
            INDLIV.setName("INDLIV");
            P_Infos.add(INDLIV);
            INDLIV.setBounds(505, 0, 36, INDLIV.getPreferredSize().height);
            
            // ---- ARG44X ----
            ARG44X.setModel(new DefaultComboBoxModel(new String[] { "Clients", "Prospects", "Clients et prospects" }));
            ARG44X.setToolTipText("choisissez un type de recherche");
            ARG44X.setName("ARG44X");
            P_Infos.add(ARG44X);
            ARG44X.setBounds(new Rectangle(new Point(689, 2), ARG44X.getPreferredSize()));
            
            // ---- OBJECT_6 ----
            OBJECT_6.setText("Type de recherche");
            OBJECT_6.setName("OBJECT_6");
            P_Infos.add(OBJECT_6);
            OBJECT_6.setBounds(569, 4, 115, 22);
            
            // ---- snEtablissement ----
            snEtablissement.setPreferredSize(new Dimension(300, 30));
            snEtablissement.setMinimumSize(new Dimension(300, 30));
            snEtablissement.setMaximumSize(new Dimension(500, 30));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            P_Infos.add(snEtablissement);
            snEtablissement.setBounds(new Rectangle(new Point(100, 0), snEtablissement.getPreferredSize()));
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < P_Infos.getComponentCount(); i++) {
                Rectangle bounds = P_Infos.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Infos.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Infos.setMinimumSize(preferredSize);
              P_Infos.setPreferredSize(preferredSize);
            }
          }
          p_tete_gauche.add(P_Infos);
          P_Infos.setBounds(0, 0, 895, P_Infos.getPreferredSize().height);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
          
          // ---- lb_requete ----
          lb_requete.setText("Affichage requ\u00eate SQL");
          lb_requete.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_requete.setFont(lb_requete.getFont().deriveFont(lb_requete.getFont().getStyle() | Font.BOLD));
          lb_requete.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_requete.setName("lb_requete");
          p_tete_droite.add(lb_requete);
          lb_requete.setBounds(125, 0, 300, 30);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
      
      // ======== barre_tete_dup ========
      {
        barre_tete_dup.setMinimumSize(new Dimension(111, 34));
        barre_tete_dup.setPreferredSize(new Dimension(111, 34));
        barre_tete_dup.setName("barre_tete_dup");
        
        // ======== p_tete_gauche2 ========
        {
          p_tete_gauche2.setOpaque(false);
          p_tete_gauche2.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche2.setPreferredSize(new Dimension(700, 30));
          p_tete_gauche2.setMinimumSize(new Dimension(700, 30));
          p_tete_gauche2.setName("p_tete_gauche2");
          p_tete_gauche2.setLayout(null);
          
          // ---- OBJ_41_OBJ_42 ----
          OBJ_41_OBJ_42.setText("Duplication de");
          OBJ_41_OBJ_42.setName("OBJ_41_OBJ_42");
          p_tete_gauche2.add(OBJ_41_OBJ_42);
          OBJ_41_OBJ_42.setBounds(7, 5, 90, 20);
          
          // ---- WICLI3 ----
          WICLI3.setToolTipText("Par duplication");
          WICLI3.setName("WICLI3");
          p_tete_gauche2.add(WICLI3);
          WICLI3.setBounds(445, 1, 60, WICLI3.getPreferredSize().height);
          
          // ---- WILIV3 ----
          WILIV3.setToolTipText("Par duplication");
          WILIV3.setName("WILIV3");
          p_tete_gauche2.add(WILIV3);
          WILIV3.setBounds(505, 1, 36, WILIV3.getPreferredSize().height);
          
          // ---- OBJECT_5 ----
          OBJECT_5.setText("Client");
          OBJECT_5.setName("OBJECT_5");
          p_tete_gauche2.add(OBJECT_5);
          OBJECT_5.setBounds(405, 4, 42, 22);
          
          // ---- snEtablissementDup ----
          snEtablissementDup.setPreferredSize(new Dimension(300, 30));
          snEtablissementDup.setMinimumSize(new Dimension(300, 30));
          snEtablissementDup.setMaximumSize(new Dimension(500, 30));
          snEtablissementDup.setName("snEtablissementDup");
          snEtablissementDup.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          p_tete_gauche2.add(snEtablissementDup);
          snEtablissementDup.setBounds(new Rectangle(new Point(100, 0), snEtablissementDup.getPreferredSize()));
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_tete_gauche2.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche2.setMinimumSize(preferredSize);
            p_tete_gauche2.setPreferredSize(preferredSize);
          }
        }
        barre_tete_dup.add(p_tete_gauche2);
      }
      p_nord.add(barre_tete_dup);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 340));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Tous les crit\u00e8res");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setToolTipText("Autres vues");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Restrict. type extension");
              riSousMenu_bt9.setToolTipText("Restriction type extension");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Clients d\u00e9sactiv\u00e9s");
              riSousMenu_bt10.setToolTipText("Clients d\u00e9sactiv\u00e9s (inclus ou exclus)");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riMenu1 ========
            {
              riMenu1.setName("riMenu1");
              
              // ---- riMenu_bt1 ----
              riMenu_bt1.setText("Outils");
              riMenu_bt1.setName("riMenu_bt1");
              riMenu1.add(riMenu_bt1);
            }
            menus_haut.add(riMenu1);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Transfert vers tableur");
              riSousMenu_bt3.setToolTipText("Transfert vers tableur");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
            
            // ======== riSousMenu2 ========
            {
              riSousMenu2.setName("riSousMenu2");
              
              // ---- riSousMenu_bt2 ----
              riSousMenu_bt2.setText("Totalisation");
              riSousMenu_bt2.setToolTipText("Totalisation");
              riSousMenu_bt2.setName("riSousMenu_bt2");
              riSousMenu_bt2.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt2ActionPerformed(e);
                }
              });
              riSousMenu2.add(riSousMenu_bt2);
            }
            menus_haut.add(riSousMenu2);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Visualisation requ\u00eate");
              riSousMenu_bt7.setToolTipText("Visualisation requ\u00eate");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new BorderLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(1000, 580));
          p_contenu.setMaximumSize(new Dimension(1000, 580));
          p_contenu.setName("p_contenu");
          
          // ======== P_Centre ========
          {
            P_Centre.setMinimumSize(new Dimension(0, 0));
            P_Centre.setEnabled(false);
            P_Centre.setPreferredSize(new Dimension(860, 564));
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(new GridBagLayout());
            ((GridBagLayout) P_Centre.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) P_Centre.getLayout()).rowHeights = new int[] { 0, 0 };
            ((GridBagLayout) P_Centre.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
            ((GridBagLayout) P_Centre.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
            
            // ======== panel1 ========
            {
              panel1.setMinimumSize(new Dimension(850, 0));
              panel1.setPreferredSize(new Dimension(850, 540));
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(new GridBagLayout());
              ((GridBagLayout) panel1.getLayout()).columnWidths = new int[] { 761, 0, 0 };
              ((GridBagLayout) panel1.getLayout()).rowHeights = new int[] { 175, 0, 0, 0, 0 };
              ((GridBagLayout) panel1.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
              ((GridBagLayout) panel1.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0, 1.0E-4 };
              
              // ======== pnlCriteresPrincipaux ========
              {
                pnlCriteresPrincipaux.setBorder(new TitledBorder(null, "Crit\u00e8res principaux", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION));
                pnlCriteresPrincipaux.setOpaque(false);
                pnlCriteresPrincipaux.setMinimumSize(new Dimension(765, 165));
                pnlCriteresPrincipaux.setPreferredSize(new Dimension(765, 165));
                pnlCriteresPrincipaux.setName("pnlCriteresPrincipaux");
                pnlCriteresPrincipaux.setLayout(null);
                
                // ---- ARG3A ----
                ARG3A.setComponentPopupMenu(BTD3);
                ARG3A.setName("ARG3A");
                pnlCriteresPrincipaux.add(ARG3A);
                ARG3A.setBounds(200, 30, 132, ARG3A.getPreferredSize().height);
                
                // ---- ARG8A ----
                ARG8A.setComponentPopupMenu(BTD3);
                ARG8A.setName("ARG8A");
                pnlCriteresPrincipaux.add(ARG8A);
                ARG8A.setBounds(200, 80, 120, ARG8A.getPreferredSize().height);
                
                // ---- ARG4A ----
                ARG4A.setComponentPopupMenu(BTD);
                ARG4A.setName("ARG4A");
                pnlCriteresPrincipaux.add(ARG4A);
                ARG4A.setBounds(570, 55, 40, ARG4A.getPreferredSize().height);
                
                // ---- ARG5N ----
                ARG5N.setComponentPopupMenu(BTD3);
                ARG5N.setName("ARG5N");
                pnlCriteresPrincipaux.add(ARG5N);
                ARG5N.setBounds(610, 55, 60, ARG5N.getPreferredSize().height);
                
                // ---- ARG2N ----
                ARG2N.setComponentPopupMenu(BTD3);
                ARG2N.setName("ARG2N");
                pnlCriteresPrincipaux.add(ARG2N);
                ARG2N.setBounds(200, 55, 70, ARG2N.getPreferredSize().height);
                
                // ---- TIDX3 ----
                TIDX3.setText("Code alphab\u00e9tique");
                TIDX3.setName("TIDX3");
                pnlCriteresPrincipaux.add(TIDX3);
                TIDX3.setBounds(25, 35, 158, TIDX3.getPreferredSize().height);
                
                // ---- TIDX8 ----
                TIDX8.setText("Num\u00e9ro de t\u00e9l\u00e9phone");
                TIDX8.setName("TIDX8");
                pnlCriteresPrincipaux.add(TIDX8);
                TIDX8.setBounds(25, 85, 160, TIDX8.getPreferredSize().height);
                
                // ---- TIDX5 ----
                TIDX5.setText("Code pays / code postal");
                TIDX5.setName("TIDX5");
                pnlCriteresPrincipaux.add(TIDX5);
                TIDX5.setBounds(410, 60, 165, TIDX5.getPreferredSize().height);
                
                // ---- TIDX2 ----
                TIDX2.setText("Num\u00e9ro client");
                TIDX2.setName("TIDX2");
                pnlCriteresPrincipaux.add(TIDX2);
                TIDX2.setBounds(25, 60, 135, TIDX2.getPreferredSize().height);
                
                // ---- TIDX13 ----
                TIDX13.setText("Magasin");
                TIDX13.setName("TIDX13");
                pnlCriteresPrincipaux.add(TIDX13);
                TIDX13.setBounds(410, 85, 145, TIDX13.getPreferredSize().height);
                
                // ---- ARG13A ----
                ARG13A.setComponentPopupMenu(BTD);
                ARG13A.setName("ARG13A");
                pnlCriteresPrincipaux.add(ARG13A);
                ARG13A.setBounds(570, 80, 34, ARG13A.getPreferredSize().height);
                
                // ---- OBJECT_13 ----
                OBJECT_13.setText("Recherche sur ville");
                OBJECT_13.setName("OBJECT_13");
                pnlCriteresPrincipaux.add(OBJECT_13);
                OBJECT_13.setBounds(432, 34, 133, 20);
                
                // ---- ARG31A ----
                ARG31A.setComponentPopupMenu(BTD3);
                ARG31A.setName("ARG31A");
                pnlCriteresPrincipaux.add(ARG31A);
                ARG31A.setBounds(570, 30, 160, ARG31A.getPreferredSize().height);
                
                // ---- ARG6A ----
                ARG6A.setComponentPopupMenu(BTD);
                ARG6A.setName("ARG6A");
                pnlCriteresPrincipaux.add(ARG6A);
                ARG6A.setBounds(570, 105, 40, ARG6A.getPreferredSize().height);
                
                // ---- ARG33A ----
                ARG33A.setComponentPopupMenu(BTD);
                ARG33A.setName("ARG33A");
                pnlCriteresPrincipaux.add(ARG33A);
                ARG33A.setBounds(675, 105, 20, ARG33A.getPreferredSize().height);
                
                // ---- ARG34A ----
                ARG34A.setComponentPopupMenu(BTD);
                ARG34A.setName("ARG34A");
                pnlCriteresPrincipaux.add(ARG34A);
                ARG34A.setBounds(695, 105, 30, ARG34A.getPreferredSize().height);
                
                // ---- ARG35A ----
                ARG35A.setComponentPopupMenu(BTD);
                ARG35A.setName("ARG35A");
                pnlCriteresPrincipaux.add(ARG35A);
                ARG35A.setBounds(725, 105, 30, ARG35A.getPreferredSize().height);
                
                // ---- TIDX6 ----
                TIDX6.setText("Cat\u00e9gorie");
                TIDX6.setName("TIDX6");
                pnlCriteresPrincipaux.add(TIDX6);
                TIDX6.setBounds(410, 110, 125, TIDX6.getPreferredSize().height);
                
                // ---- OBJECT_19 ----
                OBJECT_19.setText("Classes");
                OBJECT_19.setName("OBJECT_19");
                pnlCriteresPrincipaux.add(OBJECT_19);
                OBJECT_19.setBounds(620, 110, 55, 19);
                
                // ---- TIDX9 ----
                TIDX9.setText("Repr\u00e9sentants");
                TIDX9.setName("TIDX9");
                pnlCriteresPrincipaux.add(TIDX9);
                TIDX9.setBounds(25, 110, 158, TIDX9.getPreferredSize().height);
                
                // ---- ARG9A ----
                ARG9A.setComponentPopupMenu(BTD);
                ARG9A.setName("ARG9A");
                pnlCriteresPrincipaux.add(ARG9A);
                ARG9A.setBounds(200, 105, 34, ARG9A.getPreferredSize().height);
                
                // ---- ARG23A ----
                ARG23A.setComponentPopupMenu(BTD);
                ARG23A.setName("ARG23A");
                pnlCriteresPrincipaux.add(ARG23A);
                ARG23A.setBounds(235, 105, 34, ARG23A.getPreferredSize().height);
                
                // ---- TIDX21 ----
                TIDX21.setText("Date de cr\u00e9ation");
                TIDX21.setName("TIDX21");
                pnlCriteresPrincipaux.add(TIDX21);
                TIDX21.setBounds(25, 135, 145, 18);
                
                // ---- ARG21D ----
                ARG21D.setComponentPopupMenu(BTD3);
                ARG21D.setName("ARG21D");
                pnlCriteresPrincipaux.add(ARG21D);
                ARG21D.setBounds(200, 130, 105, 28);
              }
              panel1.add(pnlCriteresPrincipaux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH,
                  GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlAutresCriteres ========
              {
                pnlAutresCriteres.setBorder(
                    new TitledBorder(null, "Autres crit\u00e8res", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION));
                pnlAutresCriteres.setOpaque(false);
                pnlAutresCriteres.setMinimumSize(new Dimension(765, 260));
                pnlAutresCriteres.setPreferredSize(new Dimension(765, 260));
                pnlAutresCriteres.setName("pnlAutresCriteres");
                pnlAutresCriteres.setLayout(null);
                
                // ---- TIDX18 ----
                TIDX18.setText("Code affaire");
                TIDX18.setName("TIDX18");
                pnlAutresCriteres.add(TIDX18);
                TIDX18.setBounds(25, 34, 175, 20);
                
                // ---- ARG18A ----
                ARG18A.setComponentPopupMenu(BTD);
                ARG18A.setName("ARG18A");
                pnlAutresCriteres.add(ARG18A);
                ARG18A.setBounds(200, 30, 60, ARG18A.getPreferredSize().height);
                
                // ---- TIDX26 ----
                TIDX26.setText("Num\u00e9ro du client factur\u00e9");
                TIDX26.setName("TIDX26");
                pnlAutresCriteres.add(TIDX26);
                TIDX26.setBounds(25, 59, 175, 20);
                
                // ---- ARG26N ----
                ARG26N.setComponentPopupMenu(BTD3);
                ARG26N.setName("ARG26N");
                pnlAutresCriteres.add(ARG26N);
                ARG26N.setBounds(200, 55, 70, ARG26N.getPreferredSize().height);
                
                // ---- TIDX24 ----
                TIDX24.setText("N\u00b0 Ident.(Pays/num\u00e9ro)");
                TIDX24.setName("TIDX24");
                pnlAutresCriteres.add(TIDX24);
                TIDX24.setBounds(25, 84, 175, 20);
                
                // ---- ARG24A ----
                ARG24A.setName("ARG24A");
                pnlAutresCriteres.add(ARG24A);
                ARG24A.setBounds(200, 80, 34, ARG24A.getPreferredSize().height);
                
                // ---- ARG25A ----
                ARG25A.setComponentPopupMenu(BTD3);
                ARG25A.setName("ARG25A");
                pnlAutresCriteres.add(ARG25A);
                ARG25A.setBounds(235, 80, 100, ARG25A.getPreferredSize().height);
                
                // ---- TIDX15 ----
                TIDX15.setText("Condition de vente");
                TIDX15.setName("TIDX15");
                pnlAutresCriteres.add(TIDX15);
                TIDX15.setBounds(25, 109, 175, 20);
                
                // ---- ARG15A ----
                ARG15A.setComponentPopupMenu(BTD);
                ARG15A.setName("ARG15A");
                pnlAutresCriteres.add(ARG15A);
                ARG15A.setBounds(200, 105, 70, ARG15A.getPreferredSize().height);
                
                // ---- TIDX7 ----
                TIDX7.setText("Code NAF");
                TIDX7.setName("TIDX7");
                pnlAutresCriteres.add(TIDX7);
                TIDX7.setBounds(25, 134, 175, 20);
                
                // ---- ARG7A ----
                ARG7A.setComponentPopupMenu(BTD3);
                ARG7A.setName("ARG7A");
                pnlAutresCriteres.add(ARG7A);
                ARG7A.setBounds(200, 130, 60, ARG7A.getPreferredSize().height);
                
                // ---- TIDX20 ----
                TIDX20.setText("Canal de vente");
                TIDX20.setName("TIDX20");
                pnlAutresCriteres.add(TIDX20);
                TIDX20.setBounds(25, 159, 175, 20);
                
                // ---- ARG20A ----
                ARG20A.setComponentPopupMenu(BTD);
                ARG20A.setName("ARG20A");
                pnlAutresCriteres.add(ARG20A);
                ARG20A.setBounds(200, 155, 40, ARG20A.getPreferredSize().height);
                
                // ---- TIDX11 ----
                TIDX11.setText("Mode d'exp\u00e9dition");
                TIDX11.setName("TIDX11");
                pnlAutresCriteres.add(TIDX11);
                TIDX11.setBounds(25, 184, 175, 20);
                
                // ---- ARG11A ----
                ARG11A.setComponentPopupMenu(BTD);
                ARG11A.setName("ARG11A");
                pnlAutresCriteres.add(ARG11A);
                ARG11A.setBounds(200, 180, 34, ARG11A.getPreferredSize().height);
                
                // ---- TIDX12 ----
                TIDX12.setText("Code transporteur");
                TIDX12.setName("TIDX12");
                pnlAutresCriteres.add(TIDX12);
                TIDX12.setBounds(410, 34, 155, 20);
                
                // ---- ARG12A ----
                ARG12A.setComponentPopupMenu(BTD);
                ARG12A.setName("ARG12A");
                pnlAutresCriteres.add(ARG12A);
                ARG12A.setBounds(570, 30, 34, ARG12A.getPreferredSize().height);
                
                // ---- TIDX14 ----
                TIDX14.setText("Code devise");
                TIDX14.setName("TIDX14");
                pnlAutresCriteres.add(TIDX14);
                TIDX14.setBounds(410, 59, 160, 20);
                
                // ---- ARG14A ----
                ARG14A.setComponentPopupMenu(BTD);
                ARG14A.setName("ARG14A");
                pnlAutresCriteres.add(ARG14A);
                ARG14A.setBounds(570, 55, 40, ARG14A.getPreferredSize().height);
                
                // ---- TIDX16 ----
                TIDX16.setText("R\u00e9glement / \u00e9ch\u00e9ance");
                TIDX16.setName("TIDX16");
                pnlAutresCriteres.add(TIDX16);
                TIDX16.setBounds(410, 84, 155, 20);
                
                // ---- ARG16A ----
                ARG16A.setComponentPopupMenu(BTD);
                ARG16A.setName("ARG16A");
                pnlAutresCriteres.add(ARG16A);
                ARG16A.setBounds(570, 80, 34, ARG16A.getPreferredSize().height);
                
                // ---- ARG17A ----
                ARG17A.setComponentPopupMenu(BTD);
                ARG17A.setName("ARG17A");
                pnlAutresCriteres.add(ARG17A);
                ARG17A.setBounds(605, 80, 34, ARG17A.getPreferredSize().height);
                
                // ---- TIDX27 ----
                TIDX27.setText("N\u00b0 centrale principale");
                TIDX27.setName("TIDX27");
                pnlAutresCriteres.add(TIDX27);
                TIDX27.setBounds(410, 109, 155, 20);
                
                // ---- ARG27N ----
                ARG27N.setComponentPopupMenu(BTD3);
                ARG27N.setName("ARG27N");
                pnlAutresCriteres.add(ARG27N);
                ARG27N.setBounds(570, 105, 70, ARG27N.getPreferredSize().height);
                
                // ---- ARG37A ----
                ARG37A.setComponentPopupMenu(BTD3);
                ARG37A.setName("ARG37A");
                pnlAutresCriteres.add(ARG37A);
                ARG37A.setBounds(570, 130, 160, ARG37A.getPreferredSize().height);
                
                // ---- ARG38A ----
                ARG38A.setComponentPopupMenu(BTD3);
                ARG38A.setName("ARG38A");
                pnlAutresCriteres.add(ARG38A);
                ARG38A.setBounds(570, 155, 160, ARG38A.getPreferredSize().height);
                
                // ---- OBJECT_20 ----
                OBJECT_20.setText("Recherche localit\u00e9");
                OBJECT_20.setName("OBJECT_20");
                pnlAutresCriteres.add(OBJECT_20);
                OBJECT_20.setBounds(432, 159, 133, 20);
                
                // ---- label2 ----
                label2.setText("Recherche rue");
                label2.setName("label2");
                pnlAutresCriteres.add(label2);
                label2.setBounds(432, 135, 98, 19);
                
                // ---- ARG32A ----
                ARG32A.setComponentPopupMenu(BTD3);
                ARG32A.setName("ARG32A");
                pnlAutresCriteres.add(ARG32A);
                ARG32A.setBounds(570, 180, 132, ARG32A.getPreferredSize().height);
                
                // ---- label1 ----
                label1.setText("Compl\u00e9ment de nom");
                label1.setName("label1");
                pnlAutresCriteres.add(label1);
                label1.setBounds(432, 184, 143, 20);
                
                // ---- ARG30A ----
                ARG30A.setComponentPopupMenu(BTD3);
                ARG30A.setName("ARG30A");
                pnlAutresCriteres.add(ARG30A);
                ARG30A.setBounds(570, 205, 132, ARG30A.getPreferredSize().height);
                
                // ---- OBJECT_11 ----
                OBJECT_11.setText("Recherche phon\u00e9tique");
                OBJECT_11.setName("OBJECT_11");
                pnlAutresCriteres.add(OBJECT_11);
                OBJECT_11.setBounds(432, 210, 140, 19);
                
                // ---- TIDX10 ----
                TIDX10.setText("Zone g\u00e9ographique");
                TIDX10.setName("TIDX10");
                pnlAutresCriteres.add(TIDX10);
                TIDX10.setBounds(25, 210, 145, TIDX10.getPreferredSize().height);
                
                // ---- ARG10A ----
                ARG10A.setComponentPopupMenu(BTD);
                ARG10A.setName("ARG10A");
                pnlAutresCriteres.add(ARG10A);
                ARG10A.setBounds(200, 205, 60, ARG10A.getPreferredSize().height);
              }
              panel1.add(pnlAutresCriteres, new GridBagConstraints(0, 1, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
              
              // ======== pnlSelectionEtat ========
              {
                pnlSelectionEtat.setBorder(new TitledBorder(null, "S\u00e9lection d'\u00e9tat", TitledBorder.DEFAULT_JUSTIFICATION,
                    TitledBorder.DEFAULT_POSITION));
                pnlSelectionEtat.setOpaque(false);
                pnlSelectionEtat.setPreferredSize(new Dimension(224, 140));
                pnlSelectionEtat.setMinimumSize(new Dimension(224, 140));
                pnlSelectionEtat.setName("pnlSelectionEtat");
                pnlSelectionEtat.setLayout(null);
                
                // ---- SCAN19 ----
                SCAN19.setText("Uniquement cet \u00e9tat");
                SCAN19.setName("SCAN19");
                pnlSelectionEtat.add(SCAN19);
                SCAN19.setBounds(15, 65, 185, 20);
                
                // ---- SCAN19_C ----
                SCAN19_C.setText("Sans cet \u00e9tat");
                SCAN19_C.setName("SCAN19_C");
                pnlSelectionEtat.add(SCAN19_C);
                SCAN19_C.setBounds(15, 90, 155, 20);
                
                // ---- ARG19N ----
                ARG19N.setModel(new DefaultComboBoxModel(new String[] { "Afficher tous les \u00e9tats", "Client actif", "Alerte en rouge",
                    "Client interdit devis autoris\u00e9", "Acompte obligatoire", "Paiement \u00e0 la commande",
                    "Client d\u00e9sactiv\u00e9" }));
                ARG19N.setName("ARG19N");
                ARG19N.addActionListener(new ActionListener() {
                  @Override
                  public void actionPerformed(ActionEvent e) {
                    ARG19NActionPerformed(e);
                  }
                });
                pnlSelectionEtat.add(ARG19N);
                ARG19N.setBounds(15, 30, 195, ARG19N.getPreferredSize().height);
              }
              panel1.add(pnlSelectionEtat, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== pnlZonesPersonnalisees ========
              {
                pnlZonesPersonnalisees.setBorder(new TitledBorder("Zones personnalis\u00e9es"));
                pnlZonesPersonnalisees.setMinimumSize(new Dimension(232, 120));
                pnlZonesPersonnalisees.setOpaque(false);
                pnlZonesPersonnalisees.setPreferredSize(new Dimension(218, 120));
                pnlZonesPersonnalisees.setName("pnlZonesPersonnalisees");
                pnlZonesPersonnalisees.setLayout(null);
                
                // ---- WTI1 ----
                WTI1.setText("@WTI1@");
                WTI1.setName("WTI1");
                pnlZonesPersonnalisees.add(WTI1);
                WTI1.setBounds(10, 35, 30, WTI1.getPreferredSize().height);
                
                // ---- WTI2 ----
                WTI2.setText("@WTI2@");
                WTI2.setName("WTI2");
                pnlZonesPersonnalisees.add(WTI2);
                WTI2.setBounds(50, 35, 30, WTI2.getPreferredSize().height);
                
                // ---- WTI3 ----
                WTI3.setText("@WTI3@");
                WTI3.setName("WTI3");
                pnlZonesPersonnalisees.add(WTI3);
                WTI3.setBounds(90, 35, 30, WTI3.getPreferredSize().height);
                
                // ---- WTI4 ----
                WTI4.setText("@WTI4@");
                WTI4.setName("WTI4");
                pnlZonesPersonnalisees.add(WTI4);
                WTI4.setBounds(130, 35, 30, WTI4.getPreferredSize().height);
                
                // ---- WTI5 ----
                WTI5.setText("@WTI5@");
                WTI5.setName("WTI5");
                pnlZonesPersonnalisees.add(WTI5);
                WTI5.setBounds(170, 35, 30, WTI5.getPreferredSize().height);
                
                // ---- ARG39A ----
                ARG39A.setEnabled(false);
                ARG39A.setComponentPopupMenu(BTD);
                ARG39A.setName("ARG39A");
                pnlZonesPersonnalisees.add(ARG39A);
                ARG39A.setBounds(10, 60, 34, ARG39A.getPreferredSize().height);
                
                // ---- ARG40A ----
                ARG40A.setEnabled(false);
                ARG40A.setComponentPopupMenu(BTD);
                ARG40A.setName("ARG40A");
                pnlZonesPersonnalisees.add(ARG40A);
                ARG40A.setBounds(50, 60, 34, ARG40A.getPreferredSize().height);
                
                // ---- ARG41A ----
                ARG41A.setEnabled(false);
                ARG41A.setComponentPopupMenu(BTD);
                ARG41A.setName("ARG41A");
                pnlZonesPersonnalisees.add(ARG41A);
                ARG41A.setBounds(90, 60, 34, ARG41A.getPreferredSize().height);
                
                // ---- ARG42A ----
                ARG42A.setEnabled(false);
                ARG42A.setComponentPopupMenu(BTD);
                ARG42A.setName("ARG42A");
                pnlZonesPersonnalisees.add(ARG42A);
                ARG42A.setBounds(130, 60, 34, ARG42A.getPreferredSize().height);
                
                // ---- ARG43A ----
                ARG43A.setEnabled(false);
                ARG43A.setComponentPopupMenu(BTD);
                ARG43A.setName("ARG43A");
                pnlZonesPersonnalisees.add(ARG43A);
                ARG43A.setBounds(170, 60, 34, ARG43A.getPreferredSize().height);
              }
              panel1.add(pnlZonesPersonnalisees, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== OBJECT_10 ========
              {
                OBJECT_10.setBorder(
                    new TitledBorder(null, "Acc\u00e8s direct", TitledBorder.DEFAULT_JUSTIFICATION, TitledBorder.DEFAULT_POSITION));
                OBJECT_10.setMinimumSize(new Dimension(252, 165));
                OBJECT_10.setOpaque(false);
                OBJECT_10.setPreferredSize(new Dimension(252, 165));
                OBJECT_10.setName("OBJECT_10");
                OBJECT_10.setLayout(null);
                
                // ---- OBJECT_18 ----
                OBJECT_18.setText("Num\u00e9ro de facture");
                OBJECT_18.setName("OBJECT_18");
                OBJECT_10.add(OBJECT_18);
                OBJECT_18.setBounds(15, 110, 113, 20);
                
                // ---- OBJECT_15 ----
                OBJECT_15.setText("Num\u00e9ro de devis");
                OBJECT_15.setName("OBJECT_15");
                OBJECT_10.add(OBJECT_15);
                OBJECT_15.setBounds(15, 34, 106, 20);
                
                // ---- OBJECT_17 ----
                OBJECT_17.setText("Num\u00e9ro de bon");
                OBJECT_17.setName("OBJECT_17");
                OBJECT_10.add(OBJECT_17);
                OBJECT_17.setBounds(15, 72, 96, 20);
                
                // ---- WDEV ----
                WDEV.setComponentPopupMenu(BTD3);
                WDEV.setName("WDEV");
                OBJECT_10.add(WDEV);
                WDEV.setBounds(140, 30, 60, WDEV.getPreferredSize().height);
                
                // ---- WBON ----
                WBON.setComponentPopupMenu(BTD3);
                WBON.setName("WBON");
                OBJECT_10.add(WBON);
                WBON.setBounds(140, 68, 60, WBON.getPreferredSize().height);
                
                // ---- WNFA ----
                WNFA.setComponentPopupMenu(BTD3);
                WNFA.setName("WNFA");
                OBJECT_10.add(WNFA);
                WNFA.setBounds(140, 106, 68, WNFA.getPreferredSize().height);
              }
              panel1.add(OBJECT_10, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
                  new Insets(0, 0, 5, 0), 0, 0));
              
              // ======== OBJ_PLIST ========
              {
                OBJ_PLIST.setPreferredSize(new Dimension(850, 10));
                OBJ_PLIST.setMinimumSize(new Dimension(850, 0));
                OBJ_PLIST.setOpaque(false);
                OBJ_PLIST.setBorder(new TitledBorder("R\u00e9sultat de la recherche: @NBRERL@"));
                OBJ_PLIST.setName("OBJ_PLIST");
                OBJ_PLIST.setLayout(new GridBagLayout());
                ((GridBagLayout) OBJ_PLIST.getLayout()).columnWidths = new int[] { 0, 0 };
                ((GridBagLayout) OBJ_PLIST.getLayout()).rowHeights = new int[] { 50, 0 };
                ((GridBagLayout) OBJ_PLIST.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
                ((GridBagLayout) OBJ_PLIST.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
                
                // ======== panel3 ========
                {
                  panel3.setOpaque(false);
                  panel3.setMinimumSize(new Dimension(943, 50));
                  panel3.setPreferredSize(new Dimension(943, 270));
                  panel3.setName("panel3");
                  panel3.setLayout(new GridBagLayout());
                  ((GridBagLayout) panel3.getLayout()).columnWidths = new int[] { 0, 28, 0 };
                  ((GridBagLayout) panel3.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
                  ((GridBagLayout) panel3.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
                  ((GridBagLayout) panel3.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
                  
                  // ======== SCROLLPANE__LIST_ ========
                  {
                    SCROLLPANE__LIST_.setMinimumSize(new Dimension(900, 40));
                    SCROLLPANE__LIST_.setPreferredSize(new Dimension(900, 265));
                    SCROLLPANE__LIST_.setAutoscrolls(true);
                    SCROLLPANE__LIST_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
                    SCROLLPANE__LIST_.setName("SCROLLPANE__LIST_");
                    
                    // ---- WTP01 ----
                    WTP01.setPreferredScrollableViewportSize(new Dimension(900, 300));
                    WTP01.setComponentPopupMenu(BTD2);
                    WTP01.setMinimumSize(new Dimension(900, 40));
                    WTP01.setPreferredSize(new Dimension(1050, 300));
                    WTP01.setMaximumSize(new Dimension(1050, 300));
                    WTP01.setName("WTP01");
                    WTP01.addMouseListener(new MouseAdapter() {
                      @Override
                      public void mouseClicked(MouseEvent e) {
                        WTP01MouseClicked(e);
                      }
                    });
                    SCROLLPANE__LIST_.setViewportView(WTP01);
                  }
                  panel3.add(SCROLLPANE__LIST_, new GridBagConstraints(0, 0, 1, 4, 0.0, 0.0, GridBagConstraints.CENTER,
                      GridBagConstraints.BOTH, new Insets(10, 10, 10, 5), 0, 0));
                  
                  // ---- OBJ_DEBL ----
                  OBJ_DEBL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_DEBL.setMaximumSize(new Dimension(28, 32));
                  OBJ_DEBL.setMinimumSize(new Dimension(28, 28));
                  OBJ_DEBL.setPreferredSize(new Dimension(28, 32));
                  OBJ_DEBL.setName("OBJ_DEBL");
                  OBJ_DEBL.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      OBJ_DEBLActionPerformed(e);
                    }
                  });
                  panel3.add(OBJ_DEBL, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                      new Insets(10, 0, 5, 0), 0, 0));
                  
                  // ---- BT_PGUP ----
                  BT_PGUP.setText("");
                  BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  BT_PGUP.setMaximumSize(new Dimension(28, 85));
                  BT_PGUP.setMinimumSize(new Dimension(28, 35));
                  BT_PGUP.setPreferredSize(new Dimension(28, 85));
                  BT_PGUP.setName("BT_PGUP");
                  panel3.add(BT_PGUP, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                      new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- BT_PGDOWN ----
                  BT_PGDOWN.setText("");
                  BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  BT_PGDOWN.setMaximumSize(new Dimension(28, 85));
                  BT_PGDOWN.setMinimumSize(new Dimension(28, 35));
                  BT_PGDOWN.setPreferredSize(new Dimension(28, 85));
                  BT_PGDOWN.setName("BT_PGDOWN");
                  panel3.add(BT_PGDOWN, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                      new Insets(0, 0, 5, 0), 0, 0));
                  
                  // ---- OBJ_FINL ----
                  OBJ_FINL.setText("");
                  OBJ_FINL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
                  OBJ_FINL.setMaximumSize(new Dimension(28, 32));
                  OBJ_FINL.setMinimumSize(new Dimension(28, 28));
                  OBJ_FINL.setPreferredSize(new Dimension(28, 32));
                  OBJ_FINL.setName("OBJ_FINL");
                  OBJ_FINL.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      OBJ_FINLActionPerformed(e);
                    }
                  });
                  panel3.add(OBJ_FINL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                      new Insets(0, 0, 10, 0), 0, 0));
                }
                OBJ_PLIST.add(panel3, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
              }
              panel1.add(OBJ_PLIST, new GridBagConstraints(0, 3, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            P_Centre.add(panel1, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
              p_contenuLayout.createParallelGroup().addComponent(P_Centre, GroupLayout.DEFAULT_SIZE, 1018, Short.MAX_VALUE));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup().addComponent(P_Centre, GroupLayout.DEFAULT_SIZE,
              GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE));
        }
        p_centrage.add(p_contenu, BorderLayout.CENTER);
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- invite ----
      invite.setText("Choix possibles");
      invite.setName("invite");
      invite.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          inviteActionPerformed(e);
        }
      });
      BTD.add(invite);
      
      // ---- aide ----
      aide.setText("Aide en ligne");
      aide.setName("aide");
      aide.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD.add(aide);
    }
    
    // ======== BTD2 ========
    {
      BTD2.setName("BTD2");
      
      // ---- choisir ----
      choisir.setText("Choisir");
      choisir.setName("choisir");
      choisir.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          choisirActionPerformed(e);
        }
      });
      BTD2.add(choisir);
      
      // ---- modifier ----
      modifier.setText("Modifier");
      modifier.setName("modifier");
      modifier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          modifierActionPerformed(e);
        }
      });
      BTD2.add(modifier);
      
      // ---- supprimer ----
      supprimer.setText("Annuler");
      supprimer.setName("supprimer");
      supprimer.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          supprimerActionPerformed(e);
        }
      });
      BTD2.add(supprimer);
      
      // ---- menuItem4 ----
      menuItem4.setText("Encours comptable");
      menuItem4.setName("menuItem4");
      menuItem4.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem4ActionPerformed(e);
        }
      });
      BTD2.add(menuItem4);
      
      // ---- menuItem5 ----
      menuItem5.setText("D\u00e9blocage de l'encours");
      menuItem5.setName("menuItem5");
      menuItem5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem5ActionPerformed(e);
        }
      });
      BTD2.add(menuItem5);
      
      // ---- menuItem6 ----
      menuItem6.setText("Historique d\u00e9blocage de l'encours");
      menuItem6.setName("menuItem6");
      menuItem6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem6ActionPerformed(e);
        }
      });
      BTD2.add(menuItem6);
      
      // ---- interroger ----
      interroger.setText("Interroger");
      interroger.setName("interroger");
      interroger.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          interrogerActionPerformed(e);
        }
      });
      BTD2.add(interroger);
      
      // ---- statistiques ----
      statistiques.setText("Options");
      statistiques.setName("statistiques");
      statistiques.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          statistiquesActionPerformed(e);
        }
      });
      BTD2.add(statistiques);
      
      // ---- dever ----
      dever.setText("D\u00e9verrouiller");
      dever.setName("dever");
      dever.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          deverActionPerformed(e);
        }
      });
      BTD2.add(dever);
      
      // ---- specif ----
      specif.setText("Application sp\u00e9cifique");
      specif.setName("specif");
      specif.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          specifActionPerformed(e);
        }
      });
      BTD2.add(specif);
      
      // ---- histo ----
      histo.setText("Historique de modifications");
      histo.setName("histo");
      histo.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          histoActionPerformed(e);
        }
      });
      BTD2.add(histo);
      
      // ---- menuItem3 ----
      menuItem3.setText("Tri\u00e9 par");
      menuItem3.setName("menuItem3");
      menuItem3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem3ActionPerformed(e);
        }
      });
      BTD2.add(menuItem3);
    }
    
    // ======== BTD3 ========
    {
      BTD3.setName("BTD3");
      
      // ---- aide2 ----
      aide2.setText("Aide en ligne");
      aide2.setName("aide2");
      aide2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          aideActionPerformed(e);
        }
      });
      BTD3.add(aide2);
    }
    
    // ---- ARG28A ----
    ARG28A.setModel(new DefaultComboBoxModel(new String[] { "Tous", "Grand public", "VPC", "Donneur d'ordre", "Client + centrale" }));
    ARG28A.setName("ARG28A");
    
    // ---- TIDX22 ----
    TIDX22.setText("Date derni\u00e8re visite");
    TIDX22.setName("TIDX22");
    
    // ---- ARG22D ----
    ARG22D.setComponentPopupMenu(BTD3);
    ARG22D.setName("ARG22D");
    
    // ======== BTD4 ========
    {
      BTD4.setName("BTD4");
      
      // ---- CPdevis ----
      CPdevis.setText("Cr\u00e9er un devis");
      CPdevis.setName("CPdevis");
      CPdevis.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CPdevisActionPerformed(e);
        }
      });
      BTD4.add(CPdevis);
      
      // ---- CPcom ----
      CPcom.setText("Cr\u00e9er une commande");
      CPcom.setName("CPcom");
      CPcom.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CPcomActionPerformed(e);
        }
      });
      BTD4.add(CPcom);
      
      // ---- CPlistecom ----
      CPlistecom.setText("Liste des commandes ");
      CPlistecom.setName("CPlistecom");
      CPlistecom.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CPlistecomActionPerformed(e);
        }
      });
      BTD4.add(CPlistecom);
      
      // ---- CPfiche ----
      CPfiche.setText("Cr\u00e9er une fiche client");
      CPfiche.setName("CPfiche");
      CPfiche.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CPficheActionPerformed(e);
        }
      });
      BTD4.add(CPfiche);
    }
    
    // ---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX3);
    RBC_GRP.add(TIDX8);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX13);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX9);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX18);
    RBC_GRP.add(TIDX26);
    RBC_GRP.add(TIDX24);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX7);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX11);
    RBC_GRP.add(TIDX12);
    RBC_GRP.add(TIDX14);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX27);
    RBC_GRP.add(TIDX10);
    RBC_GRP.add(TIDX22);
    
    // ---- SCAN19_GRP ----
    SCAN19_GRP.add(SCAN19);
    SCAN19_GRP.add(SCAN19_C);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel P_Infos;
  private JLabel OBJECT_3;
  private JLabel OBJECT_4;
  private XRiTextField INDCLI;
  private XRiTextField INDLIV;
  private XRiComboBox ARG44X;
  private JLabel OBJECT_6;
  private SNEtablissement snEtablissement;
  private JPanel p_tete_droite;
  private JLabel lb_requete;
  private JMenuBar barre_tete_dup;
  private JPanel p_tete_gauche2;
  private JLabel OBJ_41_OBJ_42;
  private XRiTextField WICLI3;
  private XRiTextField WILIV3;
  private JLabel OBJECT_5;
  private SNEtablissement snEtablissementDup;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiMenu riMenu1;
  private RiMenu_bt riMenu_bt1;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private RiSousMenu riSousMenu2;
  private RiSousMenu_bt riSousMenu_bt2;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JPanel panel1;
  private JPanel pnlCriteresPrincipaux;
  private XRiTextField ARG3A;
  private XRiTextField ARG8A;
  private XRiTextField ARG4A;
  private XRiTextField ARG5N;
  private XRiTextField ARG2N;
  private XRiRadioButton TIDX3;
  private XRiRadioButton TIDX8;
  private XRiRadioButton TIDX5;
  private XRiRadioButton TIDX2;
  private XRiRadioButton TIDX13;
  private XRiTextField ARG13A;
  private JLabel OBJECT_13;
  private XRiTextField ARG31A;
  private XRiTextField ARG6A;
  private XRiTextField ARG33A;
  private XRiTextField ARG34A;
  private XRiTextField ARG35A;
  private XRiRadioButton TIDX6;
  private JLabel OBJECT_19;
  private XRiRadioButton TIDX9;
  private XRiTextField ARG9A;
  private XRiTextField ARG23A;
  private XRiRadioButton TIDX21;
  private XRiCalendrier ARG21D;
  private JPanel pnlAutresCriteres;
  private XRiRadioButton TIDX18;
  private XRiTextField ARG18A;
  private XRiRadioButton TIDX26;
  private XRiTextField ARG26N;
  private XRiRadioButton TIDX24;
  private XRiTextField ARG24A;
  private XRiTextField ARG25A;
  private XRiRadioButton TIDX15;
  private XRiTextField ARG15A;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7A;
  private XRiRadioButton TIDX20;
  private XRiTextField ARG20A;
  private XRiRadioButton TIDX11;
  private XRiTextField ARG11A;
  private XRiRadioButton TIDX12;
  private XRiTextField ARG12A;
  private XRiRadioButton TIDX14;
  private XRiTextField ARG14A;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG16A;
  private XRiTextField ARG17A;
  private XRiRadioButton TIDX27;
  private XRiTextField ARG27N;
  private XRiTextField ARG37A;
  private XRiTextField ARG38A;
  private JLabel OBJECT_20;
  private JLabel label2;
  private XRiTextField ARG32A;
  private JLabel label1;
  private XRiTextField ARG30A;
  private JLabel OBJECT_11;
  private XRiRadioButton TIDX10;
  private XRiTextField ARG10A;
  private JPanel pnlSelectionEtat;
  private XRiRadioButton SCAN19;
  private XRiRadioButton SCAN19_C;
  private XRiComboBox ARG19N;
  private JPanel pnlZonesPersonnalisees;
  private JLabel WTI1;
  private JLabel WTI2;
  private JLabel WTI3;
  private JLabel WTI4;
  private JLabel WTI5;
  private XRiTextField ARG39A;
  private XRiTextField ARG40A;
  private XRiTextField ARG41A;
  private XRiTextField ARG42A;
  private XRiTextField ARG43A;
  private JPanel OBJECT_10;
  private JLabel OBJECT_18;
  private JLabel OBJECT_15;
  private JLabel OBJECT_17;
  private XRiTextField WDEV;
  private XRiTextField WBON;
  private XRiTextField WNFA;
  private JPanel OBJ_PLIST;
  private JPanel panel3;
  private JScrollPane SCROLLPANE__LIST_;
  private XRiTable WTP01;
  private JButton OBJ_DEBL;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton OBJ_FINL;
  private JPopupMenu BTD;
  private JMenuItem invite;
  private JMenuItem aide;
  private JPopupMenu BTD2;
  private JMenuItem choisir;
  private JMenuItem modifier;
  private JMenuItem supprimer;
  private JMenuItem menuItem4;
  private JMenuItem menuItem5;
  private JMenuItem menuItem6;
  private JMenuItem interroger;
  private JMenuItem statistiques;
  private JMenuItem dever;
  private JMenuItem specif;
  private JMenuItem histo;
  private JMenuItem menuItem3;
  private JPopupMenu BTD3;
  private JMenuItem aide2;
  private XRiComboBox ARG28A;
  private XRiRadioButton TIDX22;
  private XRiCalendrier ARG22D;
  private JPopupMenu BTD4;
  private JMenuItem CPdevis;
  private JMenuItem CPcom;
  private JMenuItem CPlistecom;
  private JMenuItem CPfiche;
  private ButtonGroup SCAN19_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
