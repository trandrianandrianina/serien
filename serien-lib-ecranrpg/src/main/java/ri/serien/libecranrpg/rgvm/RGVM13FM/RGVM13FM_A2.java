//$$david$$ ££12/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String TITRE_RECHERCHE = "Documents de ventes correspondant à votre recherche";
  private static final String TITRE_DEBUT = " (vous êtes au début)";
  private static final String TITRE_FIN = " (vous êtes à la fin)";
  
  // Variables
  private String[] ARG3A_Value = { "E", "D", };
  private String[] ARG3A_Title = { "Bons", "Devis" };
  private String[] ARG4A_bons_Value = { "0", "1", "3", "4", "6", "7", "9", };
  private String[] ARG4A_bons_Title = { "Attente", "Validé", "Réservé", "Expédié", "Facturé", "Comptabilisé", "Tous" };
  private String[] ARG4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "9" };
  private String[] ARG4A_devis_Title = { "Attente", "Validé", "Envoyé", "Signé", "Validité dépassée", "Perdu", "Cloturé", "Tous" };
  private String[] PLA4A_devis_Value = { "0", "1", "2", "3", "4", "5", "6", "7" };
  private String[] PLA4A_devis_Title = { "Attente", "Validé", "Envoyé", "Signé", "Validité dépassée", "Perdu", "Cloturé", "Tous" };
  private String[] PLA4A_bons_Value = { "0", "1", "3", "4", "6", "7", };
  private String[] PLA4A_bons_Title = { "Attente", "Validé", "Réservé", "Expédié", "Facturé", "Comptabilisé" };
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private int[] _WTP01_Width = { 650, };
  // private Color[][] _LIST_Text_Color=new Color[15][1];
  // private Color[][] _LIST_Fond_Color=null;
  private boolean isUnBon = false;
  private boolean isUnDevis = false;
  private boolean isFacture = false;
  private boolean isVerrou = false;
  private boolean ecranDevis = false;
  private boolean ecranChantier = false;
  private boolean isTousCriteres = false;
  private String[] ARG33A_Value = { "", "I", "D", "O", "R", "W" };
  private String[] ARG33A_Title = { "Autres", "Interne", "Direct usine", "Ouverte", "Retour", "e-Commerce" };
  
  /**
   * Constructeur.
   */
  public RGVM13FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Mettre le titre par défaut
    lbTitreResultat.setText(TITRE_RECHERCHE);
    
    // Aspect des boutons de pagination car pour une raison inconnue ce coce n'est pas généré automatiquement pour les 2 boutons
    BT_PGUP.setMaximumSize(new Dimension(28, 12));
    BT_PGUP.setMinimumSize(new Dimension(28, 12));
    BT_PGUP.setPreferredSize(new Dimension(28, 12));
    BT_PGDOWN.setMaximumSize(new Dimension(28, 12));
    BT_PGDOWN.setMinimumSize(new Dimension(28, 12));
    BT_PGDOWN.setPreferredSize(new Dimension(28, 12));
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    VERR.setValeursSelection("1", "0");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    ARG3A.setValeurs(ARG3A_Value, ARG3A_Title);
    ARG33A.setValeurs(ARG33A_Value, ARG33A_Title);
    SCAN33.setValeurs("E", SCAN33_GRP);
    SCAN33_E.setValeurs("N");
    SCAN33_T.setValeurs(" ");
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion de @GESTION@ @SOUTIT@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    lbTRTAUT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Traitement automatique @TRTAUT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    lib_client.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMCLI@")).trim());
  }
  
  @Override
  public void setData() {
    // Pour l'init des Combo ARG4A et PLA4A
    // Mode bons verrouillés
    isVerrou = lexique.HostFieldGetData("VERR").equalsIgnoreCase("1");
    // Mode bon ou mode devis
    ecranDevis = lexique.HostFieldGetData("GESTION").trim().equalsIgnoreCase("devis client");
    ecranChantier = lexique.HostFieldGetData("GESTION").trim().equalsIgnoreCase("Chantiers");
    switchMode(lexique.HostFieldGetData("ARG3A"));
    
    super.setData();
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    riSousMenu_crea.setVisible(true);
    riSousMenu11.setVisible(lexique.isTrue("81"));
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Gestion du texte sur la position de la recherche
    if (lexique.isTrue("47")) {
      lbTitreResultat.setText(TITRE_RECHERCHE + TITRE_DEBUT);
    }
    else if (lexique.isTrue("48")) {
      lbTitreResultat.setText(TITRE_RECHERCHE + TITRE_FIN);
    }
    else {
      lbTitreResultat.setText(TITRE_RECHERCHE);
    }
    
    // Mode bons verrouillés
    isFacture = lexique.isTrue("75");
    
    // Vue commande initiale
    String arg3a = lexique.HostFieldGetData("ARG3A").trim();
    miCommandeInitiale.setVisible(arg3a.equalsIgnoreCase("E"));
    
    // Mode bon ou mode devis
    riSousMenu9.setEnabled(!arg3a.equalsIgnoreCase("D"));
    OBJ_68.setVisible(!arg3a.equalsIgnoreCase("D"));
    
    tous.setSelected(lexique.HostFieldGetData("ARG4A").equals("9"));
    
    if (lexique.isTrue("93")) {
      WTP01.setComponentPopupMenu(popupMenu2);
    }
    
    // Bouton valider
    navig_valid.setVisible(lexique.isTrue("N93"));
    
    INDSUF.setVisible(lexique.isPresent("INDSUF"));
    WETB.setEnabled(lexique.isPresent("WETB"));
    ARG9N2.setEnabled(lexique.isPresent("ARG9N2"));
    WSUIS.setEnabled(lexique.isPresent("WSUIS"));
    WP2.setEnabled(lexique.isPresent("WP2"));
    INDNUM.setVisible(lexique.isPresent("INDSUF"));
    ARG9N1.setEnabled(lexique.isPresent("ARG9N1"));
    ARG36N.setEnabled(lexique.isPresent("ARG36N"));
    ARG2N.setVisible(lexique.isPresent("ARG2N"));
    // ARG15A.setEnabled(lexique.isPresent("ARG15A"));
    isTousCriteres = lexique.isTrue("N61");
    lbTRTAUT.setVisible(!lexique.HostFieldGetData("TRTAUT").trim().isEmpty());
    OBJ_95.setVisible(isTousCriteres);
    if (isFacture) {
      OBJ_73.setText("Facture");
    }
    else {
      OBJ_73.setText("Numéro");
    }
    
    lib_client.setVisible(!lexique.HostFieldGetData("NOMCLI").trim().isEmpty());
    
    // Mode sélection multi-lignes
    if (lexique.isTrue("93")) {
      riSousMenu_bt13.setText("Selection unique");
    }
    else {
      riSousMenu_bt13.setText("Selection multiple");
    }
    
    // Facture
    OBJ_36.setVisible(isFacture);
    OBJ_39.setVisible(!isFacture);
    menuItem3.setVisible(!isFacture);
    menuItem5.setVisible(isFacture);
    menuItem7.setVisible(isFacture);
    menuItem8.setVisible(isFacture);
    menuItem9.setVisible(isFacture);
    
    if (lexique.HostFieldGetData("SET171").trim().equals("3")) {
      miAutoriserReglementDiffere.setVisible(true);
      miAfficherHistoriqueDemandesDifferes.setVisible(true);
    }
    else {
      miAutoriserReglementDiffere.setVisible(false);
      miAfficherHistoriqueDemandesDifferes.setVisible(false);
    }
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void switchMode(String valeur) {
    int modeComplexe = 0;
    isUnBon = valeur.trim().equals("E");
    isUnDevis = valeur.trim().equals("D");
    
    if (isUnBon) {
      OBJ_93.setText("Etats des bons");
      OBJ_21.setText("Fin de bon");
      // Liste des bons en gestion des devis (point de menu)
      if (ecranDevis) {
        modeComplexe = 3;
        // Liste des bons en gestion des bons (point de menu)
      }
      else {
        modeComplexe = 4;
      }
      ARG4A.setValeurs(ARG4A_bons_Value, ARG4A_bons_Title);
      PLA4A.setValeurs(PLA4A_bons_Value, PLA4A_bons_Title);
    }
    else if (isUnDevis) {
      OBJ_93.setText("Etats des devis");
      OBJ_21.setText("Fin de devis");
      // Liste des devis en gestion des devis (point de menu)
      if (ecranDevis) {
        modeComplexe = 1;
        // Liste des devis en gestion des bons (point de menu)
      }
      else {
        modeComplexe = 2;
      }
      ARG4A.setValeurs(ARG4A_devis_Value, ARG4A_devis_Title);
      PLA4A.setValeurs(PLA4A_devis_Value, PLA4A_devis_Title);
    }
    
    // Options de liste
    
    // Dupliquer les bons et les devis
    trans_bons.setVisible((isUnDevis && !ecranDevis) || (ecranDevis && isUnBon));
    if (isUnDevis && !ecranDevis) {
      trans_bons.setText("Dupliquer devis en bon");
    }
    if (ecranDevis && isUnBon) {
      trans_bons.setText("Dupliquer bon en devis");
    }
    
    OBJ_67.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_52.setVisible((modeComplexe == 1 || ecranChantier) && !isVerrou);
    OBJ_39.setVisible(isUnDevis && !ecranDevis);
    TRIAGE2.setVisible(isUnDevis && !ecranDevis);
    CHOISIR.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_18.setVisible(modeComplexe == 4 && !isVerrou);
    menuItem6.setVisible(modeComplexe == 4 && !isVerrou);
    
    OBJ_19.setVisible(!isVerrou);
    menuItem2.setVisible(modeComplexe == 4 && !isVerrou);
    OBJ_31.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    
    // Mode verrouillage
    OBJ_34.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_20.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_21.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_37.setVisible(isVerrou);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F2");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miAutoriserReglementDiffereActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("$");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Z");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("K");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      // si on est en mode selection multiple
      if (lexique.isTrue("93")) {
        // si la ligne est déjà toppée
        if (isSelectionnee(WTP01.getSelectedRow())) {
          WTP01.setValeurTop("-");
        }
        else {
          WTP01.setValeurTop("+");
        }
        
        lexique.HostScreenSendKey(this, "Enter");
      }
      else {
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
  }
  
  /**
   * Vérifier que la ligne a été sélectionnée dans l'application (non graphiquement)
   * C'est pourri mais j'ai rien trouvé de mieux que ce bouzin
   */
  private boolean isSelectionnee(int ligne) {
    boolean retour = false;
    switch (ligne) {
      case 0:
        retour = lexique.HostFieldGetData("BG01").equals("YL");
        break;
      case 1:
        retour = lexique.HostFieldGetData("BG02").equals("YL");
        break;
      case 2:
        retour = lexique.HostFieldGetData("BG03").equals("YL");
        break;
      case 3:
        retour = lexique.HostFieldGetData("BG04").equals("YL");
        break;
      case 4:
        retour = lexique.HostFieldGetData("BG05").equals("YL");
        break;
      case 5:
        retour = lexique.HostFieldGetData("BG06").equals("YL");
        break;
      case 6:
        retour = lexique.HostFieldGetData("BG07").equals("YL");
        break;
      case 7:
        retour = lexique.HostFieldGetData("BG08").equals("YL");
        break;
      case 8:
        retour = lexique.HostFieldGetData("BG09").equals("YL");
        break;
      case 9:
        retour = lexique.HostFieldGetData("BG10").equals("YL");
        break;
      case 10:
        retour = lexique.HostFieldGetData("BG11").equals("YL");
        break;
      case 11:
        retour = lexique.HostFieldGetData("BG12").equals("YL");
        break;
      case 12:
        retour = lexique.HostFieldGetData("BG13").equals("YL");
        break;
      case 13:
        retour = lexique.HostFieldGetData("BG14").equals("YL");
        break;
      case 14:
        retour = lexique.HostFieldGetData("BG15").equals("YL");
        break;
      
      default:
        retour = false;
        break;
    }
    return retour;
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_129ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    popupMenu1.show(WTP01, 100, 50);
  }
  
  private void tousActionPerformed(ActionEvent e) {
    if (tous.isSelected()) {
      if (isUnBon) {
        ARG4A.setSelectedIndex(6);
      }
      else {
        ARG4A.setSelectedIndex(7);
      }
    }
    else {
      ARG4A.setSelectedIndex(0);
    }
  }
  
  private void ARG3AActionPerformed(ActionEvent e) {
    // switchMode(ARG3A.getSelectedIndex());
    switchMode(lexique.HostFieldGetData("ARG3A"));
  }
  
  private void VERRActionPerformed(ActionEvent e) {
    WTP01.clearSelection();
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void ARG4AActionPerformed(ActionEvent e) {
    if (isUnDevis) {
      tous.setSelected(ARG4A.getSelectedIndex() == 7);
    }
    else {
      tous.setSelected(ARG4A.getSelectedIndex() == 6);
    }
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, ">", "Enter");
    WTP01.setValeurTop(">");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void trans_bonsActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "3", "Enter");
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void menuItem6ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "X", "Enter");
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "A", "Enter");
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem5ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem7ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "À", "Enter");
    WTP01.setValeurTop("À");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem8ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "Â", "Enter");
    WTP01.setValeurTop("Â");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem9ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("V");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem80ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem81ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem82ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem10ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Ç");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    
    WTP01.setValeurTop("&");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void mimiAfficherHistoriqueDemandesDifferesActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("£");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("É");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Ê");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Ë");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("È");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21");
  }
  
  private void trans_chantierActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("C");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miCommandeInitialeActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("O");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_81 = new JLabel();
    OBJ_70 = new JLabel();
    WDAT1X = new XRiCalendrier();
    VERR = new XRiCheckBox();
    INDNUM = new XRiTextField();
    OBJ_73 = new JLabel();
    WETB = new RiZoneSortie();
    INDSUF = new XRiTextField();
    INDNFA = new XRiTextField();
    OBJ_95 = new JLabel();
    ARG3A = new XRiComboBox();
    p_tete_droite = new JPanel();
    lbTRTAUT = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu3 = new RiSousMenu();
    riSousMenu_bt3 = new RiSousMenu_bt();
    pnlContenu = new SNPanelContenu();
    pnlCritere = new SNPanelTitre();
    ARG4A = new XRiComboBox();
    PLA4A = new XRiComboBox();
    lib_client = new RiZoneSortie();
    OBJ_104 = new JLabel();
    OBJ_84 = new JLabel();
    OBJ_93 = new JLabel();
    ARG15A = new XRiTextField();
    ARG2N = new XRiTextField();
    ARG36N = new XRiTextField();
    ARG9N1 = new XRiTextField();
    OBJ_87 = new JLabel();
    ARG9N2 = new XRiTextField();
    OBJ_99 = new JLabel();
    tous = new JCheckBox();
    ARGDOC = new XRiTextField();
    OBJ_66 = new JLabel();
    ARG33A = new XRiComboBox();
    SCAN33 = new XRiRadioButton();
    SCAN33_E = new XRiRadioButton();
    SCAN33_T = new XRiRadioButton();
    lbTitreResultat = new SNLabelTitre();
    pnlResultat = new SNPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlBoutonTableau = new JPanel();
    BT_DEB = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_FIN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    trans_bons = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_67 = new JMenuItem();
    OBJ_68 = new JMenuItem();
    miCommandeInitiale = new JMenuItem();
    OBJ_18 = new JMenuItem();
    menuItem6 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_52 = new JMenuItem();
    OBJ_53 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    menuItem5 = new JMenuItem();
    menuItem10 = new JMenuItem();
    trans_chantier = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    miAutoriserReglementDiffere = new JMenuItem();
    miAfficherHistoriqueDemandesDifferes = new JMenuItem();
    OBJ_38 = new JMenuItem();
    menuItem2 = new JMenuItem();
    WSUIS = new XRiTextField();
    WP2 = new XRiTextField();
    popupMenu1 = new JPopupMenu();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    menuItem3 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    menuItem9 = new JMenuItem();
    menuItem7 = new JMenuItem();
    menuItem8 = new JMenuItem();
    OBJ_47 = new JMenuItem();
    OBJ_48 = new JMenuItem();
    OBJ_49 = new JMenuItem();
    OBJ_50 = new JMenuItem();
    TRIAGE2 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    menuItem4 = new JMenuItem();
    TRIAGE = new JMenuItem();
    popupMenu2 = new JPopupMenu();
    menuItem80 = new JMenuItem();
    menuItem81 = new JMenuItem();
    menuItem82 = new JMenuItem();
    SCAN33_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1024, 660));
    setPreferredSize(new Dimension(1024, 660));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Gestion de @GESTION@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_81 ----
          OBJ_81.setText("Traitement");
          OBJ_81.setName("OBJ_81");
          
          // ---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");
          
          // ---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setName("WDAT1X");
          
          // ---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(BTD);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");
          VERR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              VERRActionPerformed(e);
            }
          });
          
          // ---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");
          
          // ---- OBJ_73 ----
          OBJ_73.setText("Num\u00e9ro");
          OBJ_73.setName("OBJ_73");
          
          // ---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          
          // ---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          INDSUF.setName("INDSUF");
          
          // ---- INDNFA ----
          INDNFA.setComponentPopupMenu(null);
          INDNFA.setName("INDNFA");
          
          // ---- OBJ_95 ----
          OBJ_95.setText("Type");
          OBJ_95.setHorizontalAlignment(SwingConstants.LEFT);
          OBJ_95.setName("OBJ_95");
          
          // ---- ARG3A ----
          ARG3A.setComponentPopupMenu(BTD);
          ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG3A.setName("ARG3A");
          ARG3A.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ARG3AActionPerformed(e);
            }
          });
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(WETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, 58, GroupLayout.PREFERRED_SIZE)
                      .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, 66, GroupLayout.PREFERRED_SIZE)
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(65, 65, 65).addComponent(INDSUF,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
                  .addGap(20, 20, 20).addComponent(VERR, GroupLayout.PREFERRED_SIZE, 98, GroupLayout.PREFERRED_SIZE).addGap(59, 59, 59)
                  .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                  .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE).addGap(37, 37, 37)
                  .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 55, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2).addComponent(WETB, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_73, GroupLayout.PREFERRED_SIZE, 20,
                  GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNUM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDSUF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(VERR, GroupLayout.PREFERRED_SIZE, 16, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_95, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ARG3A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_81, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(WDAT1X, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
        
        // ---- lbTRTAUT ----
        lbTRTAUT.setText("Traitement automatique @TRTAUT@");
        lbTRTAUT.setHorizontalAlignment(SwingConstants.RIGHT);
        lbTRTAUT.setName("lbTRTAUT");
        barre_tete.add(lbTRTAUT);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setOpaque(false);
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");
              
              // ---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Totalisation");
              riSousMenu_bt6.setToolTipText("Totalisation");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Exportation tableur");
              riSousMenu_bt7.setToolTipText("Transfert de la liste obtenue vers le tableur");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Personnalisation");
              riSousMenu_bt8.setToolTipText("Stockage de la personnalisation programme");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Bon ou facture");
              riSousMenu_bt9.setToolTipText("Recherche bon ou facture");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Regroupement de bons");
              riSousMenu_bt11.setToolTipText("Regroupement de commandes");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");
              
              // ---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Pr\u00e9paration logistique");
              riSousMenu_bt12.setToolTipText("Pr\u00e9paration logistique");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
            
            // ======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");
              
              // ---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("S\u00e9lection");
              riSousMenu_bt13.setToolTipText("On/Off de la s\u00e9lection multi-bons");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Traitement s\u00e9lection");
              riSousMenu_bt10.setToolTipText("Traitement sur la s\u00e9lection");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu3 ========
            {
              riSousMenu3.setName("riSousMenu3");
              
              // ---- riSousMenu_bt3 ----
              riSousMenu_bt3.setText("Traitement automatique");
              riSousMenu_bt3.setToolTipText("Traitement de fin de bon automatique");
              riSousMenu_bt3.setName("riSousMenu_bt3");
              riSousMenu_bt3.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt3ActionPerformed(e);
                }
              });
              riSousMenu3.add(riSousMenu_bt3);
            }
            menus_haut.add(riSousMenu3);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== pnlContenu ========
      {
        pnlContenu.setName("pnlContenu");
        pnlContenu.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0, 1.0E-4 };
        
        // ======== pnlCritere ========
        {
          pnlCritere.setBorder(new TitledBorder("Crit\u00e8res de recherche"));
          pnlCritere.setOpaque(false);
          pnlCritere.setMinimumSize(new Dimension(500, 150));
          pnlCritere.setPreferredSize(new Dimension(500, 150));
          pnlCritere.setName("pnlCritere");
          pnlCritere.setLayout(null);
          
          // ---- ARG4A ----
          ARG4A.setComponentPopupMenu(BTD);
          ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG4A.setName("ARG4A");
          ARG4A.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              ARG4AActionPerformed(e);
            }
          });
          pnlCritere.add(ARG4A);
          ARG4A.setBounds(150, 60, 135, ARG4A.getPreferredSize().height);
          
          // ---- PLA4A ----
          PLA4A.setComponentPopupMenu(BTD);
          PLA4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PLA4A.setName("PLA4A");
          pnlCritere.add(PLA4A);
          PLA4A.setBounds(305, 60, 140, PLA4A.getPreferredSize().height);
          
          // ---- lib_client ----
          lib_client.setText("@NOMCLI@");
          lib_client.setName("lib_client");
          pnlCritere.add(lib_client);
          lib_client.setBounds(375, 30, 241, lib_client.getPreferredSize().height);
          
          // ---- OBJ_104 ----
          OBJ_104.setText("R\u00e9f\u00e9rence courte");
          OBJ_104.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_104.setName("OBJ_104");
          pnlCritere.add(OBJ_104);
          OBJ_104.setBounds(620, 30, 135, 26);
          
          // ---- OBJ_84 ----
          OBJ_84.setText("Num\u00e9ro d\u00e9but");
          OBJ_84.setName("OBJ_84");
          pnlCritere.add(OBJ_84);
          OBJ_84.setBounds(20, 30, 105, 20);
          
          // ---- OBJ_93 ----
          OBJ_93.setText("Etats des bons");
          OBJ_93.setName("OBJ_93");
          pnlCritere.add(OBJ_93);
          OBJ_93.setBounds(20, 65, 96, 20);
          
          // ---- ARG15A ----
          ARG15A.setComponentPopupMenu(null);
          ARG15A.setToolTipText("Recherche sur r\u00e9f\u00e9rence client, r\u00e9f\u00e9rence longue et commande initiale");
          ARG15A.setName("ARG15A");
          pnlCritere.add(ARG15A);
          ARG15A.setBounds(760, 30, 90, ARG15A.getPreferredSize().height);
          
          // ---- ARG2N ----
          ARG2N.setComponentPopupMenu(null);
          ARG2N.setName("ARG2N");
          pnlCritere.add(ARG2N);
          ARG2N.setBounds(125, 30, 70, ARG2N.getPreferredSize().height);
          
          // ---- ARG36N ----
          ARG36N.setComponentPopupMenu(null);
          ARG36N.setName("ARG36N");
          pnlCritere.add(ARG36N);
          ARG36N.setBounds(125, 30, 80, ARG36N.getPreferredSize().height);
          
          // ---- ARG9N1 ----
          ARG9N1.setComponentPopupMenu(null);
          ARG9N1.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG9N1.setName("ARG9N1");
          pnlCritere.add(ARG9N1);
          ARG9N1.setBounds(270, 30, 58, ARG9N1.getPreferredSize().height);
          
          // ---- OBJ_87 ----
          OBJ_87.setText("Client");
          OBJ_87.setName("OBJ_87");
          pnlCritere.add(OBJ_87);
          OBJ_87.setBounds(220, 30, 45, 28);
          
          // ---- ARG9N2 ----
          ARG9N2.setComponentPopupMenu(null);
          ARG9N2.setName("ARG9N2");
          pnlCritere.add(ARG9N2);
          ARG9N2.setBounds(330, 30, 40, ARG9N2.getPreferredSize().height);
          
          // ---- OBJ_99 ----
          OBJ_99.setText("\u00e0");
          OBJ_99.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_99.setName("OBJ_99");
          pnlCritere.add(OBJ_99);
          OBJ_99.setBounds(285, 65, 20, 20);
          
          // ---- tous ----
          tous.setText("");
          tous.setToolTipText("Tous");
          tous.setComponentPopupMenu(BTD);
          tous.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tous.setName("tous");
          tous.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tousActionPerformed(e);
            }
          });
          pnlCritere.add(tous);
          tous.setBounds(125, 65, 20, 20);
          
          // ---- ARGDOC ----
          ARGDOC.setComponentPopupMenu(null);
          ARGDOC.setName("ARGDOC");
          pnlCritere.add(ARGDOC);
          ARGDOC.setBounds(760, 60, 210, ARGDOC.getPreferredSize().height);
          
          // ---- OBJ_66 ----
          OBJ_66.setText("Recherche document");
          OBJ_66.setHorizontalAlignment(SwingConstants.TRAILING);
          OBJ_66.setName("OBJ_66");
          pnlCritere.add(OBJ_66);
          OBJ_66.setBounds(570, 65, 185, 20);
          
          // ---- ARG33A ----
          ARG33A.setComponentPopupMenu(null);
          ARG33A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG33A.setName("ARG33A");
          pnlCritere.add(ARG33A);
          ARG33A.setBounds(20, 95, 130, ARG33A.getPreferredSize().height);
          
          // ---- SCAN33 ----
          SCAN33.setText("Inclus");
          SCAN33.setComponentPopupMenu(null);
          SCAN33.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCAN33.setName("SCAN33");
          pnlCritere.add(SCAN33);
          SCAN33.setBounds(160, 100, 70, 20);
          
          // ---- SCAN33_E ----
          SCAN33_E.setText("Exclu");
          SCAN33_E.setComponentPopupMenu(null);
          SCAN33_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCAN33_E.setName("SCAN33_E");
          pnlCritere.add(SCAN33_E);
          SCAN33_E.setBounds(235, 100, 70, 20);
          
          // ---- SCAN33_T ----
          SCAN33_T.setText("Tous");
          SCAN33_T.setComponentPopupMenu(null);
          SCAN33_T.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          SCAN33_T.setName("SCAN33_T");
          pnlCritere.add(SCAN33_T);
          SCAN33_T.setBounds(310, 100, 70, 20);
        }
        pnlContenu.add(pnlCritere, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTitreResultat ----
        lbTitreResultat.setText("[TITRE_RECHERCHE]");
        lbTitreResultat.setName("lbTitreResultat");
        pnlContenu.add(lbTitreResultat, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ======== pnlResultat ========
        {
          pnlResultat.setOpaque(false);
          pnlResultat.setName("pnlResultat");
          pnlResultat.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlResultat.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlResultat.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlResultat.getLayout()).columnWeights = new double[] { 1.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlResultat.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== SCROLLPANE_LIST ========
          {
            SCROLLPANE_LIST.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST.setPreferredSize(new Dimension(400, 270));
            SCROLLPANE_LIST.setMinimumSize(new Dimension(400, 270));
            SCROLLPANE_LIST.setName("SCROLLPANE_LIST");
            
            // ---- WTP01 ----
            WTP01.setComponentPopupMenu(BTD);
            WTP01.setName("WTP01");
            WTP01.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                WTP01MouseClicked(e);
              }
            });
            SCROLLPANE_LIST.setViewportView(WTP01);
          }
          pnlResultat.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ======== pnlBoutonTableau ========
          {
            pnlBoutonTableau.setOpaque(false);
            pnlBoutonTableau.setName("pnlBoutonTableau");
            pnlBoutonTableau.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlBoutonTableau.getLayout()).columnWidths = new int[] { 0, 0 };
            ((GridBagLayout) pnlBoutonTableau.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlBoutonTableau.getLayout()).columnWeights = new double[] { 0.0, 1.0E-4 };
            ((GridBagLayout) pnlBoutonTableau.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setMaximumSize(new Dimension(28, 28));
            BT_DEB.setMinimumSize(new Dimension(28, 28));
            BT_DEB.setPreferredSize(new Dimension(28, 28));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_111ActionPerformed(e);
              }
            });
            pnlBoutonTableau.add(BT_DEB, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.NORTHWEST,
                GridBagConstraints.NONE, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            pnlBoutonTableau.add(BT_PGUP, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.5, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            pnlBoutonTableau.add(BT_PGDOWN, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.5, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setPreferredSize(new Dimension(28, 28));
            BT_FIN.setMinimumSize(new Dimension(28, 28));
            BT_FIN.setMaximumSize(new Dimension(28, 28));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_129ActionPerformed(e);
              }
            });
            pnlBoutonTableau.add(BT_FIN, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.SOUTHWEST,
                GridBagConstraints.NONE, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlResultat.add(pnlBoutonTableau, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlContenu.add(pnlResultat, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.NORTH, GridBagConstraints.HORIZONTAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      p_sud.add(pnlContenu, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
      
      // ---- trans_bons ----
      trans_bons.setText("Cr\u00e9er la commande client");
      trans_bons.setName("trans_bons");
      trans_bons.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          trans_bonsActionPerformed(e);
        }
      });
      BTD.add(trans_bons);
      
      // ---- OBJ_40 ----
      OBJ_40.setText("Afficher");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      BTD.add(OBJ_40);
      
      // ---- OBJ_34 ----
      OBJ_34.setText("Edition");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);
      
      // ---- OBJ_67 ----
      OBJ_67.setText("Exportation tableur");
      OBJ_67.setName("OBJ_67");
      OBJ_67.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_67ActionPerformed(e);
        }
      });
      BTD.add(OBJ_67);
      
      // ---- OBJ_68 ----
      OBJ_68.setText("Suivi de commande");
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });
      BTD.add(OBJ_68);
      
      // ---- miCommandeInitiale ----
      miCommandeInitiale.setText("Commande initiale");
      miCommandeInitiale.setName("miCommandeInitiale");
      miCommandeInitiale.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miCommandeInitialeActionPerformed(e);
        }
      });
      BTD.add(miCommandeInitiale);
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Extraire");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- menuItem6 ----
      menuItem6.setText("Extraction quantit\u00e9s disponibles");
      menuItem6.setName("menuItem6");
      menuItem6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem6ActionPerformed(e);
        }
      });
      BTD.add(menuItem6);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Dupliquer");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_52 ----
      OBJ_52.setText("Duplication avec changement de tiers");
      OBJ_52.setName("OBJ_52");
      OBJ_52.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD.add(OBJ_52);
      
      // ---- OBJ_53 ----
      OBJ_53.setText("Duplication avec changement de type de facturation");
      OBJ_53.setName("OBJ_53");
      OBJ_53.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_53ActionPerformed(e);
        }
      });
      BTD.add(OBJ_53);
      
      // ---- OBJ_37 ----
      OBJ_37.setText("D\u00e9verrouillage");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);
      
      // ---- menuItem5 ----
      menuItem5.setText("Avoir");
      menuItem5.setName("menuItem5");
      menuItem5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem5ActionPerformed(e);
        }
      });
      BTD.add(menuItem5);
      
      // ---- menuItem10 ----
      menuItem10.setText("Cr\u00e9ation avec cette adresse");
      menuItem10.setName("menuItem10");
      menuItem10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem10ActionPerformed(e);
        }
      });
      BTD.add(menuItem10);
      
      // ---- trans_chantier ----
      trans_chantier.setText("Cr\u00e9er un chantier \u00e0 partir de ce devis");
      trans_chantier.setName("trans_chantier");
      trans_chantier.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          trans_chantierActionPerformed(e);
        }
      });
      BTD.add(trans_chantier);
      BTD.addSeparator();
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Acc\u00e8s aux lignes de pied");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Fin de bon");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      BTD.addSeparator();
      
      // ---- OBJ_31 ----
      OBJ_31.setText("Historique");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);
      
      // ---- miAutoriserReglementDiffere ----
      miAutoriserReglementDiffere.setText("Autoriser le r\u00e8glement diff\u00e9r\u00e9");
      miAutoriserReglementDiffere.setName("miAutoriserReglementDiffere");
      miAutoriserReglementDiffere.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAutoriserReglementDiffereActionPerformed(e);
        }
      });
      BTD.add(miAutoriserReglementDiffere);
      
      // ---- miAfficherHistoriqueDemandesDifferes ----
      miAfficherHistoriqueDemandesDifferes.setText("Historique des demandes de diff\u00e9r\u00e9s");
      miAfficherHistoriqueDemandesDifferes.setName("miAfficherHistoriqueDemandesDifferes");
      miAfficherHistoriqueDemandesDifferes.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          mimiAfficherHistoriqueDemandesDifferesActionPerformed(e);
        }
      });
      BTD.add(miAfficherHistoriqueDemandesDifferes);
      
      // ---- OBJ_38 ----
      OBJ_38.setText("Options client");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      BTD.add(OBJ_38);
      
      // ---- menuItem2 ----
      menuItem2.setText("+ Options avanc\u00e9es ");
      menuItem2.setComponentPopupMenu(popupMenu1);
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD.add(menuItem2);
      BTD.addSeparator();
    }
    
    // ---- WSUIS ----
    WSUIS.setComponentPopupMenu(BTD);
    WSUIS.setName("WSUIS");
    
    // ---- WP2 ----
    WP2.setComponentPopupMenu(BTD);
    WP2.setName("WP2");
    
    // ======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");
      
      // ---- OBJ_32 ----
      OBJ_32.setText("Duplication avec changement de tiers");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_32);
      
      // ---- OBJ_33 ----
      OBJ_33.setText("Duplication avec changement de type de facturation");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_33);
      
      // ---- OBJ_36 ----
      OBJ_36.setText("Modification Ent\u00eate apr\u00e8s facturation");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_36);
      
      // ---- OBJ_39 ----
      OBJ_39.setText("Rechiffrer");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_39);
      
      // ---- menuItem3 ----
      menuItem3.setText("Regroupement de commande");
      menuItem3.setName("menuItem3");
      menuItem3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem3ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem3);
      
      // ---- OBJ_23 ----
      OBJ_23.setText("Acc\u00e8s au colisage");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_23);
      
      // ---- menuItem9 ----
      menuItem9.setText("Avoir global en valeur");
      menuItem9.setName("menuItem9");
      menuItem9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem9ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem9);
      
      // ---- menuItem7 ----
      menuItem7.setText("Avoir partiel");
      menuItem7.setName("menuItem7");
      menuItem7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem7ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem7);
      
      // ---- menuItem8 ----
      menuItem8.setText("Avoir partiel en valeur");
      menuItem8.setName("menuItem8");
      menuItem8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem8ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem8);
      
      // ---- OBJ_47 ----
      OBJ_47.setText("Saisie des adresses de stockage pour le bon");
      OBJ_47.setName("OBJ_47");
      OBJ_47.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_47ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_47);
      
      // ---- OBJ_48 ----
      OBJ_48.setText("Affichage du d\u00e9tail de la pr\u00e9paration");
      OBJ_48.setName("OBJ_48");
      OBJ_48.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_48ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_48);
      
      // ---- OBJ_49 ----
      OBJ_49.setText("Affichage du d\u00e9tail de l'emballage");
      OBJ_49.setName("OBJ_49");
      OBJ_49.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_49ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_49);
      
      // ---- OBJ_50 ----
      OBJ_50.setText("Affichage du d\u00e9tail de l'exp\u00e9dition");
      OBJ_50.setName("OBJ_50");
      OBJ_50.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_50ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_50);
    }
    
    // ---- TRIAGE2 ----
    TRIAGE2.setText("Tri\u00e9 par ...");
    TRIAGE2.setName("TRIAGE2");
    
    // ---- OBJ_22 ----
    OBJ_22.setText("Acc\u00e8s aux achats");
    OBJ_22.setName("OBJ_22");
    OBJ_22.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_22ActionPerformed(e);
      }
    });
    
    // ---- OBJ_24 ----
    OBJ_24.setText("Adresse de stock");
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_24ActionPerformed(e);
      }
    });
    
    // ---- OBJ_41 ----
    OBJ_41.setText("Affichage affectation adresse de stock");
    OBJ_41.setName("OBJ_41");
    OBJ_41.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_41ActionPerformed(e);
      }
    });
    
    // ---- OBJ_25 ----
    OBJ_25.setText("Affectation de commande");
    OBJ_25.setName("OBJ_25");
    OBJ_25.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_25ActionPerformed(e);
      }
    });
    
    // ---- OBJ_42 ----
    OBJ_42.setText("D\u00e9tail pr\u00e9paration");
    OBJ_42.setName("OBJ_42");
    OBJ_42.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_42ActionPerformed(e);
      }
    });
    
    // ---- OBJ_43 ----
    OBJ_43.setText("D\u00e9tail emballage");
    OBJ_43.setName("OBJ_43");
    OBJ_43.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_43ActionPerformed(e);
      }
    });
    
    // ---- OBJ_44 ----
    OBJ_44.setText("D\u00e9tail exp\u00e9dition");
    OBJ_44.setName("OBJ_44");
    OBJ_44.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_44ActionPerformed(e);
      }
    });
    
    // ---- OBJ_45 ----
    OBJ_45.setText("Annulation de commande");
    OBJ_45.setName("OBJ_45");
    OBJ_45.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_45ActionPerformed(e);
      }
    });
    
    // ---- OBJ_29 ----
    OBJ_29.setText("Autorisation");
    OBJ_29.setName("OBJ_29");
    OBJ_29.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_29ActionPerformed(e);
      }
    });
    
    // ---- OBJ_30 ----
    OBJ_30.setText("Calcul cl\u00e9 d'autorisation");
    OBJ_30.setName("OBJ_30");
    OBJ_30.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_30ActionPerformed(e);
      }
    });
    
    // ---- menuItem4 ----
    menuItem4.setText("Reliquats");
    menuItem4.setName("menuItem4");
    
    // ---- TRIAGE ----
    TRIAGE.setText("Tri\u00e9 par ...");
    TRIAGE.setName("TRIAGE");
    
    // ======== popupMenu2 ========
    {
      popupMenu2.setName("popupMenu2");
      
      // ---- menuItem80 ----
      menuItem80.setText("S\u00e9lectionner");
      menuItem80.setName("menuItem80");
      menuItem80.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem80ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem80);
      
      // ---- menuItem81 ----
      menuItem81.setText("D\u00e9-s\u00e9lectionner");
      menuItem81.setName("menuItem81");
      menuItem81.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem81ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem81);
      
      // ---- menuItem82 ----
      menuItem82.setText("Afficher");
      menuItem82.setName("menuItem82");
      menuItem82.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem82ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem82);
    }
    
    // ---- SCAN33_GRP ----
    SCAN33_GRP.add(SCAN33);
    SCAN33_GRP.add(SCAN33_E);
    SCAN33_GRP.add(SCAN33_T);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_81;
  private JLabel OBJ_70;
  private XRiCalendrier WDAT1X;
  private XRiCheckBox VERR;
  private XRiTextField INDNUM;
  private JLabel OBJ_73;
  private RiZoneSortie WETB;
  private XRiTextField INDSUF;
  private XRiTextField INDNFA;
  private JLabel OBJ_95;
  private XRiComboBox ARG3A;
  private JPanel p_tete_droite;
  private JLabel lbTRTAUT;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu3;
  private RiSousMenu_bt riSousMenu_bt3;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlCritere;
  private XRiComboBox ARG4A;
  private XRiComboBox PLA4A;
  private RiZoneSortie lib_client;
  private JLabel OBJ_104;
  private JLabel OBJ_84;
  private JLabel OBJ_93;
  private XRiTextField ARG15A;
  private XRiTextField ARG2N;
  private XRiTextField ARG36N;
  private XRiTextField ARG9N1;
  private JLabel OBJ_87;
  private XRiTextField ARG9N2;
  private JLabel OBJ_99;
  private JCheckBox tous;
  private XRiTextField ARGDOC;
  private JLabel OBJ_66;
  private XRiComboBox ARG33A;
  private XRiRadioButton SCAN33;
  private XRiRadioButton SCAN33_E;
  private XRiRadioButton SCAN33_T;
  private SNLabelTitre lbTitreResultat;
  private SNPanel pnlResultat;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JPanel pnlBoutonTableau;
  private JButton BT_DEB;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_FIN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem trans_bons;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_67;
  private JMenuItem OBJ_68;
  private JMenuItem miCommandeInitiale;
  private JMenuItem OBJ_18;
  private JMenuItem menuItem6;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_52;
  private JMenuItem OBJ_53;
  private JMenuItem OBJ_37;
  private JMenuItem menuItem5;
  private JMenuItem menuItem10;
  private JMenuItem trans_chantier;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_31;
  private JMenuItem miAutoriserReglementDiffere;
  private JMenuItem miAfficherHistoriqueDemandesDifferes;
  private JMenuItem OBJ_38;
  private JMenuItem menuItem2;
  private XRiTextField WSUIS;
  private XRiTextField WP2;
  private JPopupMenu popupMenu1;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_39;
  private JMenuItem menuItem3;
  private JMenuItem OBJ_23;
  private JMenuItem menuItem9;
  private JMenuItem menuItem7;
  private JMenuItem menuItem8;
  private JMenuItem OBJ_47;
  private JMenuItem OBJ_48;
  private JMenuItem OBJ_49;
  private JMenuItem OBJ_50;
  private JMenuItem TRIAGE2;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem menuItem4;
  private JMenuItem TRIAGE;
  private JPopupMenu popupMenu2;
  private JMenuItem menuItem80;
  private JMenuItem menuItem81;
  private JMenuItem menuItem82;
  private ButtonGroup SCAN33_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
