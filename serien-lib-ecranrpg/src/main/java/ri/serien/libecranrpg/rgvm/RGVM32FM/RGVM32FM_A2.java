
package ri.serien.libecranrpg.rgvm.RGVM32FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM32FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] INDCAT_Value = { "", "C", "p", "c", "k", "g", };
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "TIT1", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", },
      { "LD08", }, { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 538, };
  
  public RGVM32FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    xRiBarreBouton.ajouterBouton(EnumBouton.CONTINUER, true);
    xRiBarreBouton.ajouterBouton(EnumBouton.RETOURNER_RECHERCHE, true);
    xRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    INDCAT.setValeurs(INDCAT_Value, null, false);
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Active les bouton liée au V01F
    xRiBarreBouton.rafraichir(lexique);
    
    snEtablissement.setVisible(lexique.isPresent("WETB"));
    INDCNV.setEnabled(lexique.isPresent("INDCNV"));
    snArticle.setEnabled(lexique.isPresent("INDRAT"));
    pnlCreation.setVisible(lexique.isTrue("51"));
    snClient.setVisible(false);
    snPays.setVisible(false);
    snZoneGeographique.setVisible(false);
    snCategorieClient.setVisible(false);
    snCanalDeVente.setVisible(false);
    lbArgument.setVisible(snClient.isVisible() || snPays.isVisible() || snZoneGeographique.isVisible() || snCategorieClient.isVisible()
        || snCanalDeVente.isVisible());
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    chargerComposantArticle();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Article
    snArticle.renseignerChampRPG(lexique, "INDRAT");
    
    // Récupère la valeur sélectionnée dans la combobox
    String typeArg = String.valueOf(INDCAT.getSelectedItem());
    if (typeArg instanceof String) {
      // Renvoie au RPG
      if (typeArg.equals("Client")) {
        String codeClient = Constantes.convertirIntegerEnTexte(snClient.getIdSelection().getNumero(), 6);
        lexique.HostFieldPutData("INDCNV", 0, codeClient);
      }
      else if (typeArg.equals("Pays")) {
        snPays.renseignerChampRPG(lexique, "INDCNV");
      }
      else if (typeArg.equals("Zone géographique")) {
        snZoneGeographique.renseignerChampRPG(lexique, "INDCNV");
      }
      else if (typeArg.equals("Catégorie client")) {
        snCategorieClient.renseignerChampRPG(lexique, "INDCNV");
      }
      else if (typeArg.equals("Canal de vente")) {
        snCanalDeVente.renseignerChampRPG(lexique, "INDCNV");
      }
      else {
        
      }
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  // Charger composant article
  private void chargerComposantArticle() {
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "INDRAT");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantArticle();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      
      if (pSNBouton.isBouton(EnumBouton.CONTINUER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.RETOURNER_RECHERCHE)) {
        lexique.HostScreenSendKey(this, "F12");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // Affichage du composant correspondant à l'argument
  private void INDCATActionPerformed(ActionEvent e) {
    // Récupère l'objet sélectionné dans la combobox et le cast en String
    String typeArg = String.valueOf(INDCAT.getSelectedItem());
    if (typeArg instanceof String) {
      // Affiche le composant client et le charge
      // Change le texte du libellé argument à "Client"
      if (typeArg.equals("Client")) {
        snClient.setVisible(true);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Client");
        chargerComposantClient();
      }
      else if (typeArg.equals("Pays")) {
        // Affiche le composant pays et le charge
        // Change le texte du libellé argument à "Pays"
        snClient.setVisible(false);
        snPays.setVisible(true);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Pays");
        chargerComposantPays();
      }
      else if (typeArg.equals("Zone géographique")) {
        // Affiche le composant zone géographique et le charge
        // Change le texte du libellé argument à "Zone géographique"
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(true);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Zone géographique");
        chargerComposantZoneGeographique();
      }
      else if (typeArg.equals("Catégorie client")) {
        // Affiche le composant catégorie client et le charge
        // Change le texte du libellé argument à "Catégorie client"
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(true);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Catégorie client");
        chargerComposantCategorieClient();
      }
      else if (typeArg.equals("Canal de vente")) {
        // Affiche le composant canal de vente et le charge
        // Change le texte du libellé argument à "Canal de vente"
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(true);
        
        lbArgument.setText("Canal de vente");
        chargerComposantCanalDeVente();
      }
      else {
        // Masque tus les composants
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
      }
      // Affiche le libellé argument en fonction de l'affichage des composants de sélection
      lbArgument.setVisible(snClient.isVisible() || snPays.isVisible() || snZoneGeographique.isVisible() || snCategorieClient.isVisible()
          || snCanalDeVente.isVisible());
    }
  }
  
  // Charger composants de sélection d'argument
  private void chargerComposantClient() {
    snClient.setSession(getSession());
    snClient.setIdEtablissement(snEtablissement.getIdSelection());
    snClient.charger(false);
    snClient.setSelectionParChampRPG(lexique, "INDCNV");
  }
  
  private void chargerComposantPays() {
    snPays.setSession(getSession());
    snPays.setIdEtablissement(snEtablissement.getIdSelection());
    snPays.charger(false);
    snPays.setSelectionParChampRPG(lexique, "INDCNV");
  }
  
  private void chargerComposantZoneGeographique() {
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(snEtablissement.getIdSelection());
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelectionParChampRPG(lexique, "INDCNV");
  }
  
  private void chargerComposantCategorieClient() {
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClient.charger(false);
    snCategorieClient.setSelectionParChampRPG(lexique, "INDCNV");
  }
  
  private void chargerComposantCanalDeVente() {
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(snEtablissement.getIdSelection());
    snCanalDeVente.charger(false);
    snCanalDeVente.setSelectionParChampRPG(lexique, "INDCNV");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlEtablissementCreation = new SNPanelTitre();
    pnlEtablissement = new SNPanel();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlCreation = new SNPanel();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbTypeArgument = new SNLabelChamp();
    INDCAT = new XRiComboBox();
    lbArgument = new SNLabelChamp();
    pnlArgument = new SNPanel();
    snCanalDeVente = new SNCanalDeVente();
    snCategorieClient = new SNCategorieClient();
    snClient = new SNClientPrincipal();
    snPays = new SNPays();
    snZoneGeographique = new SNZoneGeographique();
    pnlResultatRecherche = new SNPanelTitre();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    pnlBoutonPageUpDown = new SNPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    xRiBarreBouton = new XRiBarreBouton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    INDCNV = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //---- bpPresentation ----
    bpPresentation.setText("Autorisation de vente");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);

    //======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout)pnlContenu.getLayout()).columnWidths = new int[] {0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).rowHeights = new int[] {0, 0, 0};
      ((GridBagLayout)pnlContenu.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
      ((GridBagLayout)pnlContenu.getLayout()).rowWeights = new double[] {0.0, 1.0, 1.0E-4};

      //======== pnlEtablissementCreation ========
      {
        pnlEtablissementCreation.setTitre("Etablissement en cours");
        pnlEtablissementCreation.setName("pnlEtablissementCreation");
        pnlEtablissementCreation.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlEtablissementCreation.getLayout()).columnWidths = new int[] {0, 0};
        ((GridBagLayout)pnlEtablissementCreation.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlEtablissementCreation.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
        ((GridBagLayout)pnlEtablissementCreation.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== pnlEtablissement ========
        {
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowHeights = new int[] {0, 0};
          ((GridBagLayout)pnlEtablissement.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlEtablissement.getLayout()).rowWeights = new double[] {0.0, 1.0E-4};

          //---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //---- snEtablissement ----
          snEtablissement.setPreferredSize(new Dimension(500, 30));
          snEtablissement.setMinimumSize(new Dimension(500, 30));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlEtablissementCreation.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));

        //======== pnlCreation ========
        {
          pnlCreation.setName("pnlCreation");
          pnlCreation.setLayout(new GridBagLayout());
          ((GridBagLayout)pnlCreation.getLayout()).columnWidths = new int[] {0, 0, 0};
          ((GridBagLayout)pnlCreation.getLayout()).rowHeights = new int[] {0, 0, 0, 0};
          ((GridBagLayout)pnlCreation.getLayout()).columnWeights = new double[] {0.0, 1.0, 1.0E-4};
          ((GridBagLayout)pnlCreation.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 1.0E-4};

          //---- lbArticle ----
          lbArticle.setText("Article");
          lbArticle.setName("lbArticle");
          pnlCreation.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- snArticle ----
          snArticle.setPreferredSize(new Dimension(500, 30));
          snArticle.setMinimumSize(new Dimension(500, 30));
          snArticle.setName("snArticle");
          pnlCreation.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbTypeArgument ----
          lbTypeArgument.setText("Type d'argument");
          lbTypeArgument.setName("lbTypeArgument");
          pnlCreation.add(lbTypeArgument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

          //---- INDCAT ----
          INDCAT.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "Client",
            "Pays",
            "Cat\u00e9gorie client",
            "Canal de vente",
            "Zone g\u00e9ographique"
          }));
          INDCAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          INDCAT.setFont(new Font("sansserif", Font.PLAIN, 14));
          INDCAT.setPreferredSize(new Dimension(500, 30));
          INDCAT.setMinimumSize(new Dimension(500, 30));
          INDCAT.setName("INDCAT");
          INDCAT.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              INDCATActionPerformed(e);
            }
          });
          pnlCreation.add(INDCAT, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));

          //---- lbArgument ----
          lbArgument.setText("Argument");
          lbArgument.setName("lbArgument");
          pnlCreation.add(lbArgument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

          //======== pnlArgument ========
          {
            pnlArgument.setName("pnlArgument");
            pnlArgument.setLayout(new GridBagLayout());
            ((GridBagLayout)pnlArgument.getLayout()).columnWidths = new int[] {0, 0};
            ((GridBagLayout)pnlArgument.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
            ((GridBagLayout)pnlArgument.getLayout()).columnWeights = new double[] {1.0, 1.0E-4};
            ((GridBagLayout)pnlArgument.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

            //---- snCanalDeVente ----
            snCanalDeVente.setPreferredSize(new Dimension(500, 30));
            snCanalDeVente.setMinimumSize(new Dimension(500, 30));
            snCanalDeVente.setName("snCanalDeVente");
            pnlArgument.add(snCanalDeVente, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snCategorieClient ----
            snCategorieClient.setPreferredSize(new Dimension(500, 30));
            snCategorieClient.setMinimumSize(new Dimension(500, 30));
            snCategorieClient.setName("snCategorieClient");
            pnlArgument.add(snCategorieClient, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snClient ----
            snClient.setPreferredSize(new Dimension(500, 30));
            snClient.setMinimumSize(new Dimension(500, 30));
            snClient.setName("snClient");
            pnlArgument.add(snClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snPays ----
            snPays.setPreferredSize(new Dimension(500, 30));
            snPays.setMinimumSize(new Dimension(500, 30));
            snPays.setName("snPays");
            pnlArgument.add(snPays, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

            //---- snZoneGeographique ----
            snZoneGeographique.setPreferredSize(new Dimension(500, 30));
            snZoneGeographique.setMinimumSize(new Dimension(500, 30));
            snZoneGeographique.setName("snZoneGeographique");
            pnlArgument.add(snZoneGeographique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          }
          pnlCreation.add(pnlArgument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlEtablissementCreation.add(pnlCreation, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissementCreation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 5, 0), 0, 0));

      //======== pnlResultatRecherche ========
      {
        pnlResultatRecherche.setTitre("R\u00e9sultat de la recherche");
        pnlResultatRecherche.setPreferredSize(new Dimension(501, 364));
        pnlResultatRecherche.setName("pnlResultatRecherche");
        pnlResultatRecherche.setLayout(new GridBagLayout());
        ((GridBagLayout)pnlResultatRecherche.getLayout()).columnWidths = new int[] {0, 0, 0};
        ((GridBagLayout)pnlResultatRecherche.getLayout()).rowHeights = new int[] {0, 0, 0};
        ((GridBagLayout)pnlResultatRecherche.getLayout()).columnWeights = new double[] {1.0, 0.0, 1.0E-4};
        ((GridBagLayout)pnlResultatRecherche.getLayout()).rowWeights = new double[] {0.0, 0.0, 1.0E-4};

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- WTP01 ----
          WTP01.setComponentPopupMenu(BTD);
          WTP01.setPreferredScrollableViewportSize(new Dimension(450, 240));
          WTP01.setName("WTP01");
          WTP01.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
              WTP01MouseClicked(e);
            }
          });
          SCROLLPANE_LIST.setViewportView(WTP01);
        }
        pnlResultatRecherche.add(SCROLLPANE_LIST, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 5), 0, 0));

        //======== pnlBoutonPageUpDown ========
        {
          pnlBoutonPageUpDown.setMinimumSize(new Dimension(28, 29));
          pnlBoutonPageUpDown.setPreferredSize(new Dimension(28, 29));
          pnlBoutonPageUpDown.setName("pnlBoutonPageUpDown");
          pnlBoutonPageUpDown.setLayout(new GridLayout(2, 1, 5, 5));

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setPreferredSize(new Dimension(20, 12));
          BT_PGUP.setMinimumSize(new Dimension(20, 12));
          BT_PGUP.setMaximumSize(new Dimension(20, 12));
          BT_PGUP.setName("BT_PGUP");
          pnlBoutonPageUpDown.add(BT_PGUP);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setToolTipText("Page suivante");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          pnlBoutonPageUpDown.add(BT_PGDOWN);
        }
        pnlResultatRecherche.add(pnlBoutonPageUpDown, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.EAST, GridBagConstraints.VERTICAL,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlResultatRecherche, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
        new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);

    //---- xRiBarreBouton ----
    xRiBarreBouton.setName("xRiBarreBouton");
    add(xRiBarreBouton, BorderLayout.SOUTH);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //---- INDCNV ----
    INDCNV.setComponentPopupMenu(BTD);
    INDCNV.setFont(new Font("sansserif", Font.PLAIN, 14));
    INDCNV.setPreferredSize(new Dimension(80, 30));
    INDCNV.setMinimumSize(new Dimension(80, 30));
    INDCNV.setName("INDCNV");
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissementCreation;
  private SNPanel pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanel pnlCreation;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbTypeArgument;
  private XRiComboBox INDCAT;
  private SNLabelChamp lbArgument;
  private SNPanel pnlArgument;
  private SNCanalDeVente snCanalDeVente;
  private SNCategorieClient snCategorieClient;
  private SNClientPrincipal snClient;
  private SNPays snPays;
  private SNZoneGeographique snZoneGeographique;
  private SNPanelTitre pnlResultatRecherche;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private SNPanel pnlBoutonPageUpDown;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiBarreBouton xRiBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private XRiTextField INDCNV;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
