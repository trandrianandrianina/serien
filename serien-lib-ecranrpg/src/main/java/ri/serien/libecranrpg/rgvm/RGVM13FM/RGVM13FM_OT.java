
package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_OT extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public RGVM13FM_OT(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Titre
    setTitle("Totalisation liée à la demande");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    NBBON.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NBBON@")).trim());
    TOT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT1@")).trim());
    TOT3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT3@")).trim());
    TOT4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT4@")).trim());
    TOT5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT5@")).trim());
    TOT6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT6@")).trim());
    TOT7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOT7@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_18.setVisible(lexique.isPresent("TOT7"));
    TOT7.setVisible(lexique.isPresent("TOT7"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_7 = new JLabel();
    NBBON = new RiZoneSortie();
    OBJ_9 = new JLabel();
    TOT1 = new RiZoneSortie();
    TOT3 = new RiZoneSortie();
    OBJ_11 = new JLabel();
    OBJ_13 = new JLabel();
    TOT4 = new RiZoneSortie();
    TOT5 = new RiZoneSortie();
    OBJ_15 = new JLabel();
    OBJ_17 = new JLabel();
    TOT6 = new RiZoneSortie();
    TOT7 = new RiZoneSortie();
    OBJ_18 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(500, 260));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder(""));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- OBJ_7 ----
          OBJ_7.setText("Nombre de bons");
          OBJ_7.setName("OBJ_7");
          panel1.add(OBJ_7);
          OBJ_7.setBounds(15, 18, 135, 20);

          //---- NBBON ----
          NBBON.setText("@NBBON@");
          NBBON.setHorizontalAlignment(SwingConstants.RIGHT);
          NBBON.setName("NBBON");
          panel1.add(NBBON);
          NBBON.setBounds(210, 16, 70, NBBON.getPreferredSize().height);

          //---- OBJ_9 ----
          OBJ_9.setText("Chiffre d'affaires H.T");
          OBJ_9.setName("OBJ_9");
          panel1.add(OBJ_9);
          OBJ_9.setBounds(15, 47, 135, 20);

          //---- TOT1 ----
          TOT1.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT1.setText("@TOT1@");
          TOT1.setName("TOT1");
          panel1.add(TOT1);
          TOT1.setBounds(160, 45, 120, TOT1.getPreferredSize().height);

          //---- TOT3 ----
          TOT3.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT3.setText("@TOT3@");
          TOT3.setName("TOT3");
          panel1.add(TOT3);
          TOT3.setBounds(160, 74, 120, TOT3.getPreferredSize().height);

          //---- OBJ_11 ----
          OBJ_11.setText("Chiffre d'affaires TTC");
          OBJ_11.setName("OBJ_11");
          panel1.add(OBJ_11);
          OBJ_11.setBounds(15, 76, 135, 20);

          //---- OBJ_13 ----
          OBJ_13.setText("Prix de revient");
          OBJ_13.setName("OBJ_13");
          panel1.add(OBJ_13);
          OBJ_13.setBounds(15, 105, 135, 20);

          //---- TOT4 ----
          TOT4.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT4.setText("@TOT4@");
          TOT4.setName("TOT4");
          panel1.add(TOT4);
          TOT4.setBounds(160, 103, 120, TOT4.getPreferredSize().height);

          //---- TOT5 ----
          TOT5.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT5.setText("@TOT5@");
          TOT5.setName("TOT5");
          panel1.add(TOT5);
          TOT5.setBounds(160, 132, 120, TOT5.getPreferredSize().height);

          //---- OBJ_15 ----
          OBJ_15.setText("Marge r\u00e9elle");
          OBJ_15.setName("OBJ_15");
          panel1.add(OBJ_15);
          OBJ_15.setBounds(15, 134, 135, 20);

          //---- OBJ_17 ----
          OBJ_17.setText("Marge r\u00e9elle en %");
          OBJ_17.setName("OBJ_17");
          panel1.add(OBJ_17);
          OBJ_17.setBounds(15, 163, 135, 20);

          //---- TOT6 ----
          TOT6.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT6.setText("@TOT6@");
          TOT6.setName("TOT6");
          panel1.add(TOT6);
          TOT6.setBounds(160, 161, 120, TOT6.getPreferredSize().height);

          //---- TOT7 ----
          TOT7.setHorizontalAlignment(SwingConstants.RIGHT);
          TOT7.setText("@TOT7@");
          TOT7.setName("TOT7");
          panel1.add(TOT7);
          TOT7.setBounds(160, 190, 120, TOT7.getPreferredSize().height);

          //---- OBJ_18 ----
          OBJ_18.setText("Montant non r\u00e9gl\u00e9");
          OBJ_18.setName("OBJ_18");
          panel1.add(OBJ_18);
          OBJ_18.setBounds(15, 192, 135, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(15, 15, 15)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 298, GroupLayout.PREFERRED_SIZE)
              .addContainerGap(17, Short.MAX_VALUE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 239, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_7;
  private RiZoneSortie NBBON;
  private JLabel OBJ_9;
  private RiZoneSortie TOT1;
  private RiZoneSortie TOT3;
  private JLabel OBJ_11;
  private JLabel OBJ_13;
  private RiZoneSortie TOT4;
  private RiZoneSortie TOT5;
  private JLabel OBJ_15;
  private JLabel OBJ_17;
  private RiZoneSortie TOT6;
  private RiZoneSortie TOT7;
  private JLabel OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
