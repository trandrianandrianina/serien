
package ri.serien.libecranrpg.rgvm.RGVM32FM;

import java.awt.BorderLayout;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.article.snarticle.SNArticle;
import ri.serien.libswing.composant.metier.referentiel.client.sncategorieclient.SNCategorieClient;
import ri.serien.libswing.composant.metier.referentiel.client.snclientprincipal.SNClientPrincipal;
import ri.serien.libswing.composant.metier.referentiel.commun.snpays.SNPays;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.transport.snzonegeographique.SNZoneGeographique;
import ri.serien.libswing.composant.metier.vente.documentvente.sncanaldevente.SNCanalDeVente;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiBarreBouton;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM32FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ARG3A_Value = { "", "C", "p", "c", "k", "g", };
  
  public RGVM32FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    // Barre des Boutons
    xRiBarreBouton.ajouterBouton(EnumBouton.RECHERCHER, true);
    xRiBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    xRiBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
    
    ARG3A.setValeurs(ARG3A_Value, null, false);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    snEtablissement.setVisible(lexique.isPresent("INDETB"));
    snArticle.setEnabled(lexique.isPresent("ARG4A"));
    snClient.setVisible(false);
    snPays.setVisible(false);
    snZoneGeographique.setVisible(false);
    snCategorieClient.setVisible(false);
    snCanalDeVente.setVisible(false);
    lbArgument.setVisible(snClient.isVisible() || snPays.isVisible() || snZoneGeographique.isVisible() || snCategorieClient.isVisible()
        || snCanalDeVente.isVisible());
    ARG3A.setSelectedIndex(0);
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("INDETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "INDETB");
    
    chargerComposantArticle();
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "INDETB");
    
    // Article
    snArticle.renseignerChampRPG(lexique, "ARG4A");
    
    // Arguments
    // Récupère la valeur contenue dans la combobox
    String typeArg = String.valueOf(ARG3A.getSelectedItem());
    if (typeArg instanceof String) {
      // Utilise le composant Client pour envoyer des données au RPG
      if (typeArg.equals("Client")) {
        // Formate le numero client pour qu'il soit composé de 6 caractères
        if (snClient.getIdSelection() != null) {
          String codeClient = Constantes.convertirIntegerEnTexte(snClient.getIdSelection().getNumero(), 6);
          lexique.HostFieldPutData("ARG2A", 0, codeClient);
        }
      }
      // Utilise le composant Pays pour envoyer des données au RPG
      else if (typeArg.equals("Pays")) {
        snPays.renseignerChampRPG(lexique, "ARG2A");
      }
      // Utilise le composant Zone géographique pour envoyer des données au RPG
      else if (typeArg.equals("Zone géographique")) {
        snZoneGeographique.renseignerChampRPG(lexique, "ARG2A");
      }
      // Utilise le composant Catégorie client pour envyer des données au RPG
      else if (typeArg.equals("Catégorie client")) {
        snCategorieClient.renseignerChampRPG(lexique, "ARG2A");
      }
      // Utilise le composant Canal de vente pour envoyer des données au RPG
      else if (typeArg.equals("Canal de vente")) {
        snCanalDeVente.renseignerChampRPG(lexique, "ARG2A");
      }
      else {
        lexique.HostFieldPutData("ARG2A", 0, " ");
      }
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    xRiBarreBouton.isTraiterClickBouton(pSNBouton);
    try {
      
      if (pSNBouton.isBouton(EnumBouton.RECHERCHER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // Charger le composant article
  private void chargerComposantArticle() {
    snArticle.setSession(getSession());
    snArticle.setIdEtablissement(snEtablissement.getIdSelection());
    snArticle.charger(false);
    snArticle.setSelectionParChampRPG(lexique, "ARG4A");
  }
  
  // Actualiser les composants en cas de changement d'établissment
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantArticle();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  // Afficher le composant argument en fonction du type d'argument sélectionné
  // Le composant affiché est toujours initialisé à null pour éviter les "inexistant en cas de retour à la recherche depuis un autre
  // critère)
  private void ARG3AActionPerformed(ActionEvent e) {
    
    String typeArg = String.valueOf(ARG3A.getSelectedItem());
    if (typeArg instanceof String) {
      if (typeArg.equals("Client")) {
        snClient.setVisible(true);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Client");
        chargerComposantClient();
      }
      else if (typeArg.equals("Pays")) {
        snClient.setVisible(false);
        snPays.setVisible(true);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Pays");
        chargerComposantPays();
      }
      else if (typeArg.equals("Zone géographique")) {
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(true);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Zone géographique");
        chargerComposantZoneGeographique();
      }
      else if (typeArg.equals("Catégorie client")) {
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(true);
        snCanalDeVente.setVisible(false);
        
        lbArgument.setText("Catégorie client");
        chargerComposantCategorieClient();
      }
      else if (typeArg.equals("Canal de vente")) {
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(true);
        
        lbArgument.setText("Canal de vente");
        chargerComposantCanalDeVente();
      }
      else {
        snClient.setVisible(false);
        snPays.setVisible(false);
        snZoneGeographique.setVisible(false);
        snCategorieClient.setVisible(false);
        snCanalDeVente.setVisible(false);
      }
      lbArgument.setVisible(snClient.isVisible() || snPays.isVisible() || snZoneGeographique.isVisible() || snCategorieClient.isVisible()
          || snCanalDeVente.isVisible());
    }
  }
  
  // Charger composants de sélection d'argument
  private void chargerComposantClient() {
    snClient.setSession(getSession());
    snClient.setIdEtablissement(snEtablissement.getIdSelection());
    snClient.charger(false);
    snClient.setSelection(null);
  }
  
  private void chargerComposantPays() {
    snPays.setSession(getSession());
    snPays.setIdEtablissement(snEtablissement.getIdSelection());
    snPays.setTousAutorise(true);
    snPays.charger(false);
    snPays.setSelection(null);
  }
  
  private void chargerComposantZoneGeographique() {
    snZoneGeographique.setSession(getSession());
    snZoneGeographique.setIdEtablissement(snEtablissement.getIdSelection());
    snZoneGeographique.setTousAutorise(true);
    snZoneGeographique.charger(false);
    snZoneGeographique.setSelection(null);
  }
  
  private void chargerComposantCategorieClient() {
    snCategorieClient.setSession(getSession());
    snCategorieClient.setIdEtablissement(snEtablissement.getIdSelection());
    snCategorieClient.setTousAutorise(true);
    snCategorieClient.charger(false);
    snCategorieClient.setSelection(null);
  }
  
  private void chargerComposantCanalDeVente() {
    snCanalDeVente.setSession(getSession());
    snCanalDeVente.setIdEtablissement(snEtablissement.getIdSelection());
    snCanalDeVente.setTousAutorise(true);
    snCanalDeVente.charger(false);
    snCanalDeVente.setSelection(null);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlEtablissement = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    pnlRecherche = new SNPanelTitre();
    lbArticle = new SNLabelChamp();
    snArticle = new SNArticle();
    lbTypeArgument = new SNLabelChamp();
    ARG3A = new XRiComboBox();
    lbArgument = new SNLabelChamp();
    pnlArgument = new SNPanel();
    snPays = new SNPays();
    snCategorieClient = new SNCategorieClient();
    snCanalDeVente = new SNCanalDeVente();
    snZoneGeographique = new SNZoneGeographique();
    snClient = new SNClientPrincipal();
    xRiBarreBouton = new XRiBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Autorisation de ventes");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlEtablissement ========
      {
        pnlEtablissement.setTitre("Etablissement");
        pnlEtablissement.setName("pnlEtablissement");
        pnlEtablissement.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbEtablissement ----
        lbEtablissement.setText("Etablissement en cours");
        lbEtablissement.setName("lbEtablissement");
        pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- snEtablissement ----
        snEtablissement.setPreferredSize(new Dimension(500, 30));
        snEtablissement.setMinimumSize(new Dimension(500, 30));
        snEtablissement.setName("snEtablissement");
        pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
            GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlEtablissement,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlRecherche ========
      {
        pnlRecherche.setTitre("Recherche multi-crit\u00e8res");
        pnlRecherche.setName("pnlRecherche");
        pnlRecherche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlRecherche.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlRecherche.getLayout()).rowHeights = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlRecherche.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlRecherche.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbArticle ----
        lbArticle.setText("Article");
        lbArticle.setName("lbArticle");
        pnlRecherche.add(lbArticle, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- snArticle ----
        snArticle.setPreferredSize(new Dimension(500, 30));
        snArticle.setMinimumSize(new Dimension(500, 30));
        snArticle.setName("snArticle");
        pnlRecherche.add(snArticle, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTypeArgument ----
        lbTypeArgument.setText("Type d'argument");
        lbTypeArgument.setName("lbTypeArgument");
        pnlRecherche.add(lbTypeArgument, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ARG3A ----
        ARG3A.setModel(new DefaultComboBoxModel(
            new String[] { "Aucun", "Client", "Pays", "Cat\u00e9gorie client", "Canal de vente", "Zone g\u00e9ographique" }));
        ARG3A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ARG3A.setPreferredSize(new Dimension(500, 30));
        ARG3A.setMinimumSize(new Dimension(500, 30));
        ARG3A.setMaximumSize(new Dimension(32767, 30));
        ARG3A.setFont(new Font("sansserif", Font.PLAIN, 14));
        ARG3A.setName("ARG3A");
        ARG3A.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            ARG3AActionPerformed(e);
          }
        });
        pnlRecherche.add(ARG3A, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbArgument ----
        lbArgument.setText("lbArgument");
        lbArgument.setName("lbArgument");
        pnlRecherche.add(lbArgument, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ======== pnlArgument ========
        {
          pnlArgument.setName("pnlArgument");
          pnlArgument.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlArgument.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlArgument.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlArgument.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlArgument.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          
          // ---- snPays ----
          snPays.setPreferredSize(new Dimension(500, 30));
          snPays.setMinimumSize(new Dimension(500, 30));
          snPays.setName("snPays");
          pnlArgument.add(snPays, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snCategorieClient ----
          snCategorieClient.setPreferredSize(new Dimension(500, 30));
          snCategorieClient.setMinimumSize(new Dimension(500, 30));
          snCategorieClient.setName("snCategorieClient");
          pnlArgument.add(snCategorieClient, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snCanalDeVente ----
          snCanalDeVente.setPreferredSize(new Dimension(500, 30));
          snCanalDeVente.setMinimumSize(new Dimension(500, 30));
          snCanalDeVente.setName("snCanalDeVente");
          pnlArgument.add(snCanalDeVente, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snZoneGeographique ----
          snZoneGeographique.setPreferredSize(new Dimension(500, 30));
          snZoneGeographique.setMinimumSize(new Dimension(500, 30));
          snZoneGeographique.setName("snZoneGeographique");
          pnlArgument.add(snZoneGeographique, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- snClient ----
          snClient.setPreferredSize(new Dimension(500, 30));
          snClient.setMinimumSize(new Dimension(500, 30));
          snClient.setName("snClient");
          pnlArgument.add(snClient, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlRecherche.add(pnlArgument, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlRecherche,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- xRiBarreBouton ----
    xRiBarreBouton.setName("xRiBarreBouton");
    add(xRiBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanelTitre pnlEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNPanelTitre pnlRecherche;
  private SNLabelChamp lbArticle;
  private SNArticle snArticle;
  private SNLabelChamp lbTypeArgument;
  private XRiComboBox ARG3A;
  private SNLabelChamp lbArgument;
  private SNPanel pnlArgument;
  private SNPays snPays;
  private SNCategorieClient snCategorieClient;
  private SNCanalDeVente snCanalDeVente;
  private SNZoneGeographique snZoneGeographique;
  private SNClientPrincipal snClient;
  private XRiBarreBouton xRiBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
