
package ri.serien.libecranrpg.rgvm.RGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM07FM_AC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public RGVM07FM_AC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_59.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RA1LIB@")).trim());
    OBJ_57.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGRLIB@")).trim());
    OBJ_58.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFALIB@")).trim());
    OBJ_37.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGALIB@")).trim());
    OBJ_42.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFRNOM@")).trim());
    OBJ_55.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SFALIB@")).trim());
    OBJ_56.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RTALIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    WGRP.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("G"));
    WFAM.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("F"));
    OBJ_43.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("A"));
    WSFA.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("S"));
    WCTA.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("T"));
    OBJ_47.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("T"));
    OBJ_49.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("F"));
    WFRS.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("2"));
    WFRS.setEnabled(lexique.isPresent("WFRS"));
    OBJ_48.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("G"));
    OBJ_45.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("A"));
    WRGA.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("1"));
    WRGA.setEnabled(lexique.isPresent("WRGA"));
    OBJ_50.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("S"));
    OBJ_35.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("1"));
    OBJ_40.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("2"));
    WART.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("A"));
    WARTA.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("A"));
    OBJ_42.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("2") & lexique.isPresent("RFRNOM"));
    OBJ_37.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("1") & lexique.isPresent("RGALIB"));
    OBJ_58.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("F"));
    OBJ_57.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("G"));
    OBJ_56.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("T") & lexique.isPresent("RTALIB"));
    OBJ_55.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("S") & lexique.isPresent("SFALIB"));
    OBJ_59.setVisible(lexique.HostFieldGetData("ARG4A").equalsIgnoreCase("A"));
    
    // TODO Icones
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("RATTACHEMENT"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WARTA = new XRiTextField();
    WART = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_43 = new JLabel();
    WRGA = new XRiTextField();
    WFRS = new XRiTextField();
    WCTA = new XRiTextField();
    WSFA = new XRiTextField();
    WFAM = new XRiTextField();
    WGRP = new XRiTextField();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_59 = new RiZoneSortie();
    OBJ_57 = new RiZoneSortie();
    OBJ_58 = new RiZoneSortie();
    OBJ_37 = new RiZoneSortie();
    OBJ_50 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_42 = new RiZoneSortie();
    OBJ_55 = new RiZoneSortie();
    OBJ_56 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(1000, 500));
    setPreferredSize(new Dimension(1000, 500));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Rattachement");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mode expert/assistant");
              riSousMenu_bt6.setToolTipText("Mode expert / Mode assistant");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 300));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Recherche de condition"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WARTA ----
            WARTA.setComponentPopupMenu(BTD);
            WARTA.setName("WARTA");
            panel1.add(WARTA);
            WARTA.setBounds(514, 38, 210, WARTA.getPreferredSize().height);

            //---- WART ----
            WART.setComponentPopupMenu(BTD);
            WART.setName("WART");
            panel1.add(WART);
            WART.setBounds(149, 38, 210, WART.getPreferredSize().height);

            //---- OBJ_45 ----
            OBJ_45.setText("Classement 1");
            OBJ_45.setName("OBJ_45");
            panel1.add(OBJ_45);
            OBJ_45.setBounds(419, 42, 96, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("Article");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(14, 42, 86, 20);

            //---- WRGA ----
            WRGA.setComponentPopupMenu(BTD);
            WRGA.setName("WRGA");
            panel1.add(WRGA);
            WRGA.setBounds(149, 72, 110, WRGA.getPreferredSize().height);

            //---- WFRS ----
            WFRS.setComponentPopupMenu(BTD);
            WFRS.setName("WFRS");
            panel1.add(WFRS);
            WFRS.setBounds(149, 72, 80, WFRS.getPreferredSize().height);

            //---- WCTA ----
            WCTA.setComponentPopupMenu(BTD);
            WCTA.setName("WCTA");
            panel1.add(WCTA);
            WCTA.setBounds(149, 72, 60, WCTA.getPreferredSize().height);

            //---- WSFA ----
            WSFA.setComponentPopupMenu(BTD);
            WSFA.setName("WSFA");
            panel1.add(WSFA);
            WSFA.setBounds(149, 72, 60, WSFA.getPreferredSize().height);

            //---- WFAM ----
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setName("WFAM");
            panel1.add(WFAM);
            WFAM.setBounds(149, 72, 40, WFAM.getPreferredSize().height);

            //---- WGRP ----
            WGRP.setComponentPopupMenu(BTD);
            WGRP.setName("WGRP");
            panel1.add(WGRP);
            WGRP.setBounds(149, 72, 20, WGRP.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("Groupe");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(14, 76, 70, 20);

            //---- OBJ_49 ----
            OBJ_49.setText("Famille");
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(14, 76, 69, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("Tarif");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(14, 76, 52, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("@RA1LIB@");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(149, 74, 285, OBJ_59.getPreferredSize().height);

            //---- OBJ_57 ----
            OBJ_57.setText("@RGRLIB@");
            OBJ_57.setName("OBJ_57");
            panel1.add(OBJ_57);
            OBJ_57.setBounds(265, 74, 277, OBJ_57.getPreferredSize().height);

            //---- OBJ_58 ----
            OBJ_58.setText("@RFALIB@");
            OBJ_58.setName("OBJ_58");
            panel1.add(OBJ_58);
            OBJ_58.setBounds(265, 74, 277, OBJ_58.getPreferredSize().height);

            //---- OBJ_37 ----
            OBJ_37.setText("@RGALIB@");
            OBJ_37.setName("OBJ_37");
            panel1.add(OBJ_37);
            OBJ_37.setBounds(265, 74, 231, OBJ_37.getPreferredSize().height);

            //---- OBJ_50 ----
            OBJ_50.setText("Sous-Famille");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(14, 76, 118, 20);

            //---- OBJ_40 ----
            OBJ_40.setText("Code Fournisseur");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(14, 76, 143, 20);

            //---- OBJ_35 ----
            OBJ_35.setText("Regroupement achat");
            OBJ_35.setName("OBJ_35");
            panel1.add(OBJ_35);
            OBJ_35.setBounds(14, 76, 135, 20);

            //---- OBJ_42 ----
            OBJ_42.setText("@RFRNOM@");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(265, 74, 231, OBJ_42.getPreferredSize().height);

            //---- OBJ_55 ----
            OBJ_55.setText("@SFALIB@");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(265, 74, 277, OBJ_55.getPreferredSize().height);

            //---- OBJ_56 ----
            OBJ_56.setText("@RTALIB@");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(265, 74, 277, OBJ_56.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 816, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(52, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(74, 74, 74)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(79, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WARTA;
  private XRiTextField WART;
  private JLabel OBJ_45;
  private JLabel OBJ_43;
  private XRiTextField WRGA;
  private XRiTextField WFRS;
  private XRiTextField WCTA;
  private XRiTextField WSFA;
  private XRiTextField WFAM;
  private XRiTextField WGRP;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private JLabel OBJ_47;
  private RiZoneSortie OBJ_59;
  private RiZoneSortie OBJ_57;
  private RiZoneSortie OBJ_58;
  private RiZoneSortie OBJ_37;
  private JLabel OBJ_50;
  private JLabel OBJ_40;
  private JLabel OBJ_35;
  private RiZoneSortie OBJ_42;
  private RiZoneSortie OBJ_55;
  private RiZoneSortie OBJ_56;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
