
package ri.serien.libecranrpg.rgvm.RGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM07FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ARG2A_Value = { "?", " ", "Q", "D", "F", "H", "A" }; // catégorie de condition
  private String[] WTCN_Value = { "", "!", "C", }; // type de condition
  private String[] ARG4A_Value = { "", "A", "T", "F", "S", "G", "*", "R", "L", "1", "2", "3", "4", "5" }; // type de ratachement
  
  public RGVM07FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    ARG2A.setValeurs(ARG2A_Value, null);
    // type CNV client ou cnv
    RBC.setValeurs("1", RBC_GRP);
    RBC_B.setValeurs("2");
    
    // rattachement
    RBR.setValeurs("1", RBR_GRP);
    RBR_B.setValeurs("2");
    RBR_C.setValeurs("3");
    RBR_D.setValeurs("6");
    RBR_E.setValeurs("4");
    RBR_ra.setValeurs("7");
    RBR_fr.setValeurs("8");
    
    SCAN2.setValeurs("E", SCAN2_GRP);
    SCAN2_B.setValeurs("N");
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RCNLIB@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RCLNOM@")).trim());
    OBJ_86.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGRLIB@")).trim());
    OBJ_88.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFALIB@")).trim());
    OBJ_91.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SFALIB@")).trim());
    OBJ_93.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RTALIB@")).trim());
    OBJ_83.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RA1LIB@")).trim());
    OBJ_107.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RGALIB@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RFRNOM@")).trim());
    OBJ_105.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DVLIB@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_83.setVisible(lexique.HostFieldGetData("RBR").equalsIgnoreCase("1") & lexique.isPresent("RA1LIB"));
    
    OBJ_72.setVisible(lexique.isPresent("RCLNOM"));
    OBJ_107.setVisible(lexique.isPresent("RGALIB"));
    OBJ_76.setVisible(lexique.isPresent("RFRNOM"));
    OBJ_105.setVisible(lexique.isPresent("DVLIB"));
    OBJ_93.setVisible(lexique.isPresent("RTALIB"));
    OBJ_91.setVisible(lexique.isPresent("SFALIB"));
    OBJ_88.setVisible(lexique.isPresent("RFALIB"));
    OBJ_86.setVisible(lexique.isPresent("RGRLIB"));
    OBJ_69.setVisible(lexique.isPresent("RCNLIB"));
    // ARG2A.setVisible( lexique.isPresent("ARG2A"));
    // ARG2A.setEnabled( lexique.isPresent("ARG2A"));
    // ARG4A.setVisible( lexique.isPresent("ARG4A"));
    OBJ_99.setVisible(lexique.isPresent("ARG10A"));
    
    //
    WCNV.setEnabled(RBC.isSelected());
    WCLI.setEnabled(RBC_B.isSelected());
    WCLIA.setEnabled(RBC_B.isSelected());
    
    // ***
    WART.setEnabled(RBR.isSelected());
    WARTA.setEnabled(RBR.isSelected());
    WGRP.setEnabled(RBR_B.isSelected());
    WFAM.setEnabled(RBR_C.isSelected());
    WSFA.setEnabled(RBR_D.isSelected());
    WCTA.setEnabled(RBR_E.isSelected());
    WRGA.setEnabled(RBR_ra.isSelected());
    WFRS.setEnabled(RBR_fr.isSelected());
    
    ARG2A.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SCAN2.setVisible(ARG2A.getSelectedIndex() != 0);
        SCAN2_B.setVisible(ARG2A.getSelectedIndex() != 0);
        if (ARG2A.getSelectedIndex() == 0) {
          lexique.HostFieldPutData("SCAN2", 0, "E");
        }
      }
    });
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - RECHERCHE CNV"));
    
    

    p_bpresentation.setCodeEtablissement(ARG1A.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(ARG1A.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_68MouseClicked(MouseEvent e) {
    
    WCNV.setEnabled(RBC.isSelected());
    WCLI.setEnabled(RBC_B.isSelected());
    WCLIA.setEnabled(RBC_B.isSelected());
    
    WART.setEnabled(RBR.isSelected());
    WARTA.setEnabled(RBR.isSelected());
    WGRP.setEnabled(RBR_B.isSelected());
    WFAM.setEnabled(RBR_C.isSelected());
    WSFA.setEnabled(RBR_D.isSelected());
    WCTA.setEnabled(RBR_E.isSelected());
    WRGA.setEnabled(RBR_ra.isSelected());
    WFRS.setEnabled(RBR_fr.isSelected());
    
  }
  
  private void conditionActionPerformed(ActionEvent e) {
    WCNV.setEnabled(RBC.isSelected());
    WCLI.setEnabled(RBC_B.isSelected());
    WCLIA.setEnabled(RBC_B.isSelected());
  }
  
  private void OBJ_68(MouseEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_39 = new JLabel();
    OBJ_58 = new JLabel();
    ARG1A = new XRiTextField();
    ARG2A = new XRiComboBox();
    SCAN2 = new XRiRadioButton();
    SCAN2_B = new XRiRadioButton();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    OBJ_69 = new RiZoneSortie();
    OBJ_72 = new RiZoneSortie();
    OBJ_74 = new JLabel();
    WCLIA = new XRiTextField();
    RBC = new XRiRadioButton();
    RBC_B = new XRiRadioButton();
    WCNV = new XRiTextField();
    WCLI = new XRiTextField();
    panel2 = new JPanel();
    OBJ_86 = new RiZoneSortie();
    OBJ_88 = new RiZoneSortie();
    OBJ_91 = new RiZoneSortie();
    OBJ_93 = new RiZoneSortie();
    RBR_D = new XRiRadioButton();
    RBR_B = new XRiRadioButton();
    RBR_C = new XRiRadioButton();
    RBR = new XRiRadioButton();
    WSFA = new XRiTextField();
    WCTA = new XRiTextField();
    RBR_E = new XRiRadioButton();
    WFAM = new XRiTextField();
    WGRP = new XRiTextField();
    panel6 = new JPanel();
    OBJ_82 = new JLabel();
    OBJ_84 = new JLabel();
    WART = new XRiTextField();
    WARTA = new XRiTextField();
    OBJ_83 = new RiZoneSortie();
    RBR_ra = new XRiRadioButton();
    OBJ_107 = new RiZoneSortie();
    WRGA = new XRiTextField();
    RBR_fr = new XRiRadioButton();
    WFRS = new XRiTextField();
    OBJ_76 = new RiZoneSortie();
    panel1 = new JPanel();
    OBJ_99 = new JLabel();
    OBJ_101 = new JLabel();
    ARG10A = new XRiTextField();
    ARG7D = new XRiCalendrier();
    OBJ_102 = new JLabel();
    ARG7F = new XRiCalendrier();
    OBJ_104 = new JLabel();
    ARG8A = new XRiTextField();
    OBJ_105 = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_96 = new JLabel();
    OBJ_78 = new JLabel();
    OBJ_65 = new JLabel();
    OBJ_62 = new JLabel();
    V06FO = new XRiTextField();
    SCAN2_GRP = new ButtonGroup();
    RBC_GRP = new ButtonGroup();
    RBR_GRP = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(1100, 800));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Recherche condition de ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 40));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_39 ----
          OBJ_39.setText("Etablissement");
          OBJ_39.setPreferredSize(new Dimension(95, 20));
          OBJ_39.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_39.setName("OBJ_39");
          
          // ---- OBJ_58 ----
          OBJ_58.setText("Cat\u00e9gorie");
          OBJ_58.setPreferredSize(new Dimension(62, 20));
          OBJ_58.setFont(new Font("sansserif", Font.PLAIN, 14));
          OBJ_58.setName("OBJ_58");
          
          // ---- ARG1A ----
          ARG1A.setComponentPopupMenu(BTD);
          ARG1A.setName("ARG1A");
          
          // ---- ARG2A ----
          ARG2A.setModel(new DefaultComboBoxModel(new String[] { "Toutes cat\u00e9gories", "Conditions normales",
              "Conditions quantitatives", "D\u00e9rogations", "Conditions flash", "Chantiers", "Affaires" }));
          ARG2A.setComponentPopupMenu(BTD);
          ARG2A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ARG2A.setFont(new Font("sansserif", Font.PLAIN, 14));
          ARG2A.setName("ARG2A");
          
          // ---- SCAN2 ----
          SCAN2.setText("Inclus");
          SCAN2.setName("SCAN2");
          
          // ---- SCAN2_B ----
          SCAN2_B.setText("Exclus");
          SCAN2_B.setName("SCAN2_B");
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(ARG1A, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(100, 100, 100)
                  .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(ARG2A, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(70, 70, 70).addComponent(SCAN2_B,
                          GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                      .addComponent(SCAN2, GroupLayout.PREFERRED_SIZE, 75, GroupLayout.PREFERRED_SIZE))
                  .addContainerGap()));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(6, 6, 6).addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 18,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(1, 1, 1).addComponent(ARG1A, GroupLayout.PREFERRED_SIZE,
                  GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(2, 2, 2)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                      .addComponent(OBJ_58, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(ARG2A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              .addComponent(SCAN2_B, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE)
              .addComponent(SCAN2, GroupLayout.PREFERRED_SIZE, 26, GroupLayout.PREFERRED_SIZE));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(980, 600));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setMinimumSize(new Dimension(980, 600));
          p_contenu.setFont(new Font("sansserif", Font.PLAIN, 14));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);
          
          // ======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Condition"));
            panel3.setOpaque(false);
            panel3.setFont(new Font("sansserif", Font.PLAIN, 14));
            panel3.setName("panel3");
            panel3.setLayout(null);
            
            // ---- OBJ_69 ----
            OBJ_69.setText("@RCNLIB@");
            OBJ_69.setName("OBJ_69");
            panel3.add(OBJ_69);
            OBJ_69.setBounds(276, 30, 277, OBJ_69.getPreferredSize().height);
            
            // ---- OBJ_72 ----
            OBJ_72.setText("@RCLNOM@");
            OBJ_72.setName("OBJ_72");
            panel3.add(OBJ_72);
            OBJ_72.setBounds(276, 59, 218, OBJ_72.getPreferredSize().height);
            
            // ---- OBJ_74 ----
            OBJ_74.setText("Recherche ");
            OBJ_74.setName("OBJ_74");
            panel3.add(OBJ_74);
            OBJ_74.setBounds(505, 60, 75, 18);
            
            // ---- WCLIA ----
            WCLIA.setComponentPopupMenu(BTD);
            WCLIA.setName("WCLIA");
            panel3.add(WCLIA);
            WCLIA.setBounds(580, 55, 210, WCLIA.getPreferredSize().height);
            
            // ---- RBC ----
            RBC.setText("Param\u00e8tre 'CN'");
            RBC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBC.setName("RBC");
            RBC.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            panel3.add(RBC);
            RBC.setBounds(29, 32, 122, 20);
            
            // ---- RBC_B ----
            RBC_B.setText("Client");
            RBC_B.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBC_B.setName("RBC_B");
            RBC_B.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            panel3.add(RBC_B);
            RBC_B.setBounds(29, 61, 111, 20);
            
            // ---- WCNV ----
            WCNV.setComponentPopupMenu(BTD);
            WCNV.setName("WCNV");
            panel3.add(WCNV);
            WCNV.setBounds(206, 28, 60, WCNV.getPreferredSize().height);
            
            // ---- WCLI ----
            WCLI.setComponentPopupMenu(BTD);
            WCLI.setName("WCLI");
            panel3.add(WCLI);
            WCLI.setBounds(206, 57, 60, WCLI.getPreferredSize().height);
            
            { // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel3);
          panel3.setBounds(15, 10, 940, 105);
          
          // ======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Rattachement"));
            panel2.setOpaque(false);
            panel2.setFont(new Font("sansserif", Font.PLAIN, 14));
            panel2.setName("panel2");
            
            // ---- OBJ_86 ----
            OBJ_86.setText("@RGRLIB@");
            OBJ_86.setName("OBJ_86");
            
            // ---- OBJ_88 ----
            OBJ_88.setText("@RFALIB@");
            OBJ_88.setName("OBJ_88");
            
            // ---- OBJ_91 ----
            OBJ_91.setText("@SFALIB@");
            OBJ_91.setName("OBJ_91");
            
            // ---- OBJ_93 ----
            OBJ_93.setText("@RTALIB@");
            OBJ_93.setName("OBJ_93");
            
            // ---- RBR_D ----
            RBR_D.setText("Sous-Famille");
            RBR_D.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBR_D.setName("RBR_D");
            RBR_D.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- RBR_B ----
            RBR_B.setText("Groupe");
            RBR_B.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBR_B.setName("RBR_B");
            RBR_B.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- RBR_C ----
            RBR_C.setText("Famille");
            RBR_C.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBR_C.setName("RBR_C");
            RBR_C.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- RBR ----
            RBR.setText("Article");
            RBR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBR.setName("RBR");
            RBR.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- WSFA ----
            WSFA.setComponentPopupMenu(BTD);
            WSFA.setName("WSFA");
            
            // ---- WCTA ----
            WCTA.setComponentPopupMenu(BTD);
            WCTA.setName("WCTA");
            
            // ---- RBR_E ----
            RBR_E.setText("Tarif");
            RBR_E.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            RBR_E.setName("RBR_E");
            RBR_E.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- WFAM ----
            WFAM.setComponentPopupMenu(BTD);
            WFAM.setName("WFAM");
            
            // ---- WGRP ----
            WGRP.setComponentPopupMenu(BTD);
            WGRP.setName("WGRP");
            
            // ======== panel6 ========
            {
              panel6.setBorder(new BevelBorder(BevelBorder.LOWERED));
              panel6.setOpaque(false);
              panel6.setPreferredSize(new Dimension(814, 33));
              panel6.setName("panel6");
              
              // ---- OBJ_82 ----
              OBJ_82.setText("Code");
              OBJ_82.setName("OBJ_82");
              
              // ---- OBJ_84 ----
              OBJ_84.setText("Recherche");
              OBJ_84.setName("OBJ_84");
              
              // ---- WART ----
              WART.setComponentPopupMenu(BTD);
              WART.setName("WART");
              
              // ---- WARTA ----
              WARTA.setComponentPopupMenu(BTD);
              WARTA.setName("WARTA");
              
              // ---- OBJ_83 ----
              OBJ_83.setText("@RA1LIB@");
              OBJ_83.setName("OBJ_83");
              
              GroupLayout panel6Layout = new GroupLayout(panel6);
              panel6.setLayout(panel6Layout);
              panel6Layout.setHorizontalGroup(panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup().addGap(29, 29, 29)
                      .addGroup(panel6Layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                          .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, 185, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel6Layout.createSequentialGroup()
                              .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                              .addComponent(WART, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(11, 11, 11)
                              .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 71, GroupLayout.PREFERRED_SIZE)
                              .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                              .addComponent(WARTA, GroupLayout.PREFERRED_SIZE, 210, GroupLayout.PREFERRED_SIZE).addGap(92, 92, 92)))
                      .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)));
              panel6Layout.setVerticalGroup(panel6Layout.createParallelGroup()
                  .addGroup(panel6Layout.createSequentialGroup()
                      .addGroup(panel6Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                          .addComponent(WART, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_84, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addComponent(WARTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(OBJ_82, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                      .addComponent(OBJ_83, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)));
            }
            
            // ---- RBR_ra ----
            RBR_ra.setText("Regroupement achats");
            RBR_ra.setName("RBR_ra");
            RBR_ra.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- OBJ_107 ----
            OBJ_107.setText("@RGALIB@");
            OBJ_107.setName("OBJ_107");
            
            // ---- WRGA ----
            WRGA.setComponentPopupMenu(BTD);
            WRGA.setName("WRGA");
            WRGA.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- RBR_fr ----
            RBR_fr.setText("Fournisseur");
            RBR_fr.setName("RBR_fr");
            RBR_fr.addMouseListener(new MouseAdapter() {
              @Override
              public void mouseClicked(MouseEvent e) {
                OBJ_68MouseClicked(e);
              }
            });
            
            // ---- WFRS ----
            WFRS.setComponentPopupMenu(BTD);
            WFRS.setName("WFRS");
            
            // ---- OBJ_76 ----
            OBJ_76.setText("@RFRNOM@");
            OBJ_76.setName("OBJ_76");
            
            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup().addGap(12, 12, 12)
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(RBR_C, GroupLayout.PREFERRED_SIZE, 69, GroupLayout.PREFERRED_SIZE).addGap(111, 111, 111)
                            .addComponent(WFAM, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(25, 25, 25)
                            .addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(RBR_D, GroupLayout.PREFERRED_SIZE, 104, GroupLayout.PREFERRED_SIZE).addGap(76, 76, 76)
                            .addComponent(WSFA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(RBR_E, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE).addGap(128, 128, 128)
                            .addComponent(WCTA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                            .addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(RBR_B, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(110, 110, 110)
                            .addComponent(WGRP, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(45, 45, 45)
                            .addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addGroup(panel2Layout.createParallelGroup()
                                .addComponent(RBR_ra, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                                .addComponent(RBR_fr, GroupLayout.PREFERRED_SIZE, 125, GroupLayout.PREFERRED_SIZE))
                            .addGroup(panel2Layout.createParallelGroup()
                                .addComponent(WFRS, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                .addComponent(WRGA, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(panel2Layout.createParallelGroup()
                                .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE)
                                .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE)))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addComponent(RBR, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE).addGap(28, 28, 28)
                            .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE)))
                    .addContainerGap(129, Short.MAX_VALUE)));
            panel2Layout.setVerticalGroup(panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2)
                            .addComponent(panel6, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(panel2Layout.createSequentialGroup().addGap(28, 28, 28)
                            .addComponent(RBR, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                    .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(WGRP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_86, GroupLayout.PREFERRED_SIZE,
                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(GroupLayout.Alignment.TRAILING,
                            panel2Layout.createSequentialGroup()
                                .addComponent(RBR_B, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)))
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4)
                            .addComponent(RBR_C, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(4, 4, 4))
                        .addComponent(WFAM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_88, GroupLayout.PREFERRED_SIZE,
                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4)
                            .addComponent(RBR_D, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(4, 4, 4))
                        .addComponent(WSFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_91, GroupLayout.PREFERRED_SIZE,
                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4)
                            .addComponent(RBR_E, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(4, 4, 4))
                        .addComponent(WCTA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_93, GroupLayout.PREFERRED_SIZE,
                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panel2Layout.createParallelGroup()
                        .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4)
                            .addComponent(RBR_ra, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE).addGap(12, 12, 12)
                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(RBR_fr, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(WFRS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addContainerGap(34, Short.MAX_VALUE))
                        .addGroup(panel2Layout.createSequentialGroup()
                            .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(WRGA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(OBJ_107, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(OBJ_76, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(37, Short.MAX_VALUE)))));
          }
          p_contenu.add(panel2);
          panel2.setBounds(15, 130, 936, 295);
          
          // ======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Divers"));
            panel1.setOpaque(false);
            panel1.setFont(new Font("sansserif", Font.PLAIN, 14));
            panel1.setName("panel1");
            
            // ---- OBJ_99 ----
            OBJ_99.setText("R\u00e9f\u00e9rence");
            OBJ_99.setName("OBJ_99");
            
            // ---- OBJ_101 ----
            OBJ_101.setText("Dates de validit\u00e9 entre");
            OBJ_101.setName("OBJ_101");
            
            // ---- ARG10A ----
            ARG10A.setComponentPopupMenu(BTD);
            ARG10A.setName("ARG10A");
            
            // ---- ARG7D ----
            ARG7D.setName("ARG7D");
            
            // ---- OBJ_102 ----
            OBJ_102.setText("et");
            OBJ_102.setName("OBJ_102");
            
            // ---- ARG7F ----
            ARG7F.setName("ARG7F");
            
            // ---- OBJ_104 ----
            OBJ_104.setText("Devise");
            OBJ_104.setName("OBJ_104");
            
            // ---- ARG8A ----
            ARG8A.setComponentPopupMenu(BTD);
            ARG8A.setName("ARG8A");
            
            // ---- OBJ_105 ----
            OBJ_105.setText("@DVLIB@");
            OBJ_105.setName("OBJ_105");
            
            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addContainerGap()
                    .addGroup(panel1Layout.createParallelGroup().addComponent(OBJ_99)
                        .addGroup(panel1Layout.createParallelGroup().addComponent(OBJ_101, GroupLayout.Alignment.TRAILING)
                            .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(ARG8A, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, 277, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                            .addComponent(ARG7D, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 14, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(ARG7F, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
                        .addComponent(ARG10A, GroupLayout.PREFERRED_SIZE, 242, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(17, Short.MAX_VALUE)));
            panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup().addContainerGap().addGroup(panel1Layout.createParallelGroup()
                    .addComponent(OBJ_99, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addComponent(ARG10A,
                        GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(4, 4, 4)
                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(ARG7D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_102, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ARG7F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_101, GroupLayout.PREFERRED_SIZE, 27, GroupLayout.PREFERRED_SIZE))
                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                    .addGroup(panel1Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                        .addComponent(OBJ_104, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                        .addComponent(ARG8A, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_105, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(0, 12, Short.MAX_VALUE)));
          }
          p_contenu.add(panel1);
          panel1.setBounds(15, 430, 505, 160);
          
          { // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 2, 1, 2, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Aide en ligne");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
    }
    
    // ---- OBJ_96 ----
    OBJ_96.setText("Divers");
    OBJ_96.setName("OBJ_96");
    
    // ---- OBJ_78 ----
    OBJ_78.setText("Rattachement");
    OBJ_78.setName("OBJ_78");
    
    // ---- OBJ_65 ----
    OBJ_65.setText("Condition");
    OBJ_65.setName("OBJ_65");
    
    // ---- OBJ_62 ----
    OBJ_62.setText("Recherche multi-crit\u00e8res");
    OBJ_62.setName("OBJ_62");
    
    // ---- V06FO ----
    V06FO.setComponentPopupMenu(BTD);
    V06FO.setName("V06FO");
    
    // ---- SCAN2_GRP ----
    SCAN2_GRP.add(SCAN2);
    SCAN2_GRP.add(SCAN2_B);
    
    // ---- RBC_GRP ----
    RBC_GRP.add(RBC);
    RBC_GRP.add(RBC_B);
    
    // ---- RBR_GRP ----
    RBR_GRP.add(RBR_D);
    RBR_GRP.add(RBR_B);
    RBR_GRP.add(RBR_C);
    RBR_GRP.add(RBR);
    RBR_GRP.add(RBR_E);
    RBR_GRP.add(RBR_ra);
    RBR_GRP.add(RBR_fr);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_39;
  private JLabel OBJ_58;
  private XRiTextField ARG1A;
  private XRiComboBox ARG2A;
  private XRiRadioButton SCAN2;
  private XRiRadioButton SCAN2_B;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel3;
  private RiZoneSortie OBJ_69;
  private RiZoneSortie OBJ_72;
  private JLabel OBJ_74;
  private XRiTextField WCLIA;
  private XRiRadioButton RBC;
  private XRiRadioButton RBC_B;
  private XRiTextField WCNV;
  private XRiTextField WCLI;
  private JPanel panel2;
  private RiZoneSortie OBJ_86;
  private RiZoneSortie OBJ_88;
  private RiZoneSortie OBJ_91;
  private RiZoneSortie OBJ_93;
  private XRiRadioButton RBR_D;
  private XRiRadioButton RBR_B;
  private XRiRadioButton RBR_C;
  private XRiRadioButton RBR;
  private XRiTextField WSFA;
  private XRiTextField WCTA;
  private XRiRadioButton RBR_E;
  private XRiTextField WFAM;
  private XRiTextField WGRP;
  private JPanel panel6;
  private JLabel OBJ_82;
  private JLabel OBJ_84;
  private XRiTextField WART;
  private XRiTextField WARTA;
  private RiZoneSortie OBJ_83;
  private XRiRadioButton RBR_ra;
  private RiZoneSortie OBJ_107;
  private XRiTextField WRGA;
  private XRiRadioButton RBR_fr;
  private XRiTextField WFRS;
  private RiZoneSortie OBJ_76;
  private JPanel panel1;
  private JLabel OBJ_99;
  private JLabel OBJ_101;
  private XRiTextField ARG10A;
  private XRiCalendrier ARG7D;
  private JLabel OBJ_102;
  private XRiCalendrier ARG7F;
  private JLabel OBJ_104;
  private XRiTextField ARG8A;
  private RiZoneSortie OBJ_105;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_16;
  private JLabel OBJ_96;
  private JLabel OBJ_78;
  private JLabel OBJ_65;
  private JLabel OBJ_62;
  private XRiTextField V06FO;
  private ButtonGroup SCAN2_GRP;
  private ButtonGroup RBC_GRP;
  private ButtonGroup RBR_GRP;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
