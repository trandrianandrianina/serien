//$$david$$ ££12/01/11££ -> tests et modifs

package ri.serien.libecranrpg.rgvm.RGVM13FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JToggleButton;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_A3 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LDG01", }, { "LDG02", }, { "LDG03", }, { "LDG04", }, { "LDG05", }, { "LDG06", }, { "LDG07", },
      { "LDG08", }, { "LDG09", }, { "LDG10", }, { "LDG11", }, { "LDG12", }, { "LDG13", }, { "LDG14", }, { "LDG15", }, };
  private int[] _WTP01_Width = { 650, };
  private boolean isUnBon = false;
  private boolean isUnDevis = false;
  private boolean isFacture = false;
  private boolean isVerrou = false;
  private boolean ecranDevis = false;
  private boolean isTousCriteres = false;
  private boolean appele = false;
  
  /**
   * Constructeur.
   */
  public RGVM13FM_A3(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    VERR.setValeursSelection("1", "0");
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, Constantes.CORRESPONDANCE_COULEURS);
    TOUTETAT.setValeursSelection("1", " ");
    WCOMPTE.setValeursSelection("1", " ");
    
    
    BT_FIN.setIcon(lexique.chargerImage("images/pfin20.png", true));
    BT_DEB.setIcon(lexique.chargerImage("images/pdeb20.png", true));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Gestion de @GESTION@ @SOUTIT@")).trim());
    WETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    WOBS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WOBS@")).trim());
    WATN.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WATN@")).trim());
    WEPOS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPOS@")).trim());
    WEPLF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEPLF@")).trim());
    WEDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WEDEP@")).trim());
  }
  
  @Override
  public void setData() {
    // Pour l'init des Combo ARG4A et PLA4A
    // Mode bons verrouillés
    isVerrou = lexique.HostFieldGetData("VERR").equals("1");
    // Mode bon ou mode devis
    ecranDevis = lexique.HostFieldGetData("GESTION").trim().equalsIgnoreCase("devis client");
    switchMode(lexique.HostFieldGetData("ARG3A"));
    
    super.setData();
    
    String typdoc = lexique.HostFieldGetData("TYPDOC").trim();
    tbt_Devis.setSelected(typdoc.equalsIgnoreCase("DEV"));
    tbt_Commande.setSelected(typdoc.equalsIgnoreCase("CMD"));
    tbt_Livraison.setSelected(typdoc.equalsIgnoreCase("EXP"));
    tbt_Facture.setSelected(typdoc.equalsIgnoreCase("FAC"));
    
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    riSousMenu_crea.setVisible(true);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    OBJ_68.setVisible(!typdoc.equalsIgnoreCase("DEV"));
    
    if (!lexique.HostFieldGetData("NOMCLI").trim().isEmpty()) {
      ((TitledBorder) panel2.getBorder())
          .setTitle(((TitledBorder) panel2.getBorder()).getTitle() + " pour " + lexique.HostFieldGetData("NOMCLI").trim() + " (tél. : "
              + lexique.HostFieldGetData("TELCLI").trim() + " - " + lexique.HostFieldGetData("VILCLI").trim() + ")");
      ((TitledBorder) panel2.getBorder()).setTitleFont(new Font("fonts/VeraMono.ttf", Font.BOLD, 13));
    }
    
    // Gestion du texte sur la position de la recherche
    if (lexique.isTrue("47")) {
      ((TitledBorder) panel2.getBorder()).setTitle(((TitledBorder) panel2.getBorder()).getTitle() + " - Vous êtes au début");
    }
    else if (lexique.isTrue("48")) {
      ((TitledBorder) panel2.getBorder()).setTitle(((TitledBorder) panel2.getBorder()).getTitle() + " - Vous êtes à la fin");
    }
    
    WCOMPTE.setVisible(!WNOM.getText().trim().isEmpty());
    
    // Apparence des boutons
    // Pas de client en compte pas de livraison
    tbt_Livraison.setEnabled(!lexique.HostFieldGetData("ARG9N1").trim().isEmpty());
    
    // Si documents en cours...
    if (!lexique.HostFieldGetData("NBCH").trim().isEmpty()) {
      tbt_Chantier.setForeground(Constantes.COULEUR_LISTE_COMMENTAIRE);
      if (lexique.HostFieldGetData("NBCH").trim().equals("1")) {
        tbt_Chantier.setToolTipText("1 chantier en cours");
      }
      else {
        tbt_Chantier.setToolTipText(lexique.HostFieldGetData("NBCH") + " chantiers en cours");
      }
    }
    else {
      tbt_Chantier.setForeground(Color.BLACK);
      tbt_Chantier.setToolTipText("");
    }
    if (!lexique.HostFieldGetData("NBDEV").trim().isEmpty()) {
      tbt_Devis.setForeground(Constantes.COULEUR_LISTE_COMMENTAIRE);
      tbt_Devis.setToolTipText(lexique.HostFieldGetData("NBDEV") + " devis en cours de validité");
    }
    else {
      tbt_Devis.setForeground(Color.BLACK);
      tbt_Devis.setToolTipText("");
    }
    if (!lexique.HostFieldGetData("NBCMD").trim().isEmpty()) {
      tbt_Commande.setForeground(Constantes.COULEUR_LISTE_COMMENTAIRE);
      if (lexique.HostFieldGetData("NBCMD").trim().equals("1")) {
        tbt_Commande.setToolTipText("1 commande non livrée");
      }
      else {
        tbt_Commande.setToolTipText(lexique.HostFieldGetData("NBCMD") + " commandes non livrées");
      }
    }
    else {
      tbt_Commande.setForeground(Color.BLACK);
      tbt_Commande.setToolTipText("");
    }
    if (!lexique.HostFieldGetData("NBBL").trim().isEmpty()) {
      tbt_Livraison.setForeground(Constantes.COULEUR_LISTE_COMMENTAIRE);
      if (lexique.HostFieldGetData("NBBL").trim().equals("1")) {
        tbt_Livraison.setToolTipText("1 livraison non facturée");
      }
      else {
        tbt_Livraison.setToolTipText(lexique.HostFieldGetData("NBBL") + " livraisons non facturées");
      }
    }
    else {
      tbt_Livraison.setForeground(Color.BLACK);
      tbt_Livraison.setToolTipText("");
    }
    if (!lexique.HostFieldGetData("NBFACNR").trim().isEmpty()) {
      tbt_Facture.setForeground(Constantes.COULEUR_LISTE_COMMENTAIRE);
      if (lexique.HostFieldGetData("NBFACNR").trim().equals("1")) {
        tbt_Facture.setToolTipText("1 facture non réglée");
      }
      else {
        tbt_Facture.setToolTipText(lexique.HostFieldGetData("NBFACNR") + " factures non réglées");
      }
    }
    else {
      tbt_Facture.setForeground(Color.BLACK);
      tbt_Facture.setToolTipText("");
    }
    
    if (typdoc.equals("DEV")) {
      TOUTETAT.setToolTipText("Tous les devis quelques soient leurs états");
    }
    else if (typdoc.equals("CMD")) {
      TOUTETAT.setToolTipText("Tous les bons quelques soient leurs états");
    }
    
    // Mode bons verrouillés
    isFacture = lexique.isTrue("75");
    
    // Mode bon ou mode devis
    riSousMenu9.setEnabled(!lexique.HostFieldGetData("ARG3A").equalsIgnoreCase("D"));
    
    if (lexique.isTrue("93")) {
      WTP01.setComponentPopupMenu(popupMenu2);
    }
    
    // Bouton valider
    navig_valid.setVisible(lexique.isTrue("N93"));
    
    INDSUF.setVisible(lexique.isPresent("INDSUF"));
    WETB.setEnabled(lexique.isPresent("WETB"));
    ARG9N2.setEnabled(lexique.isPresent("ARG9N2"));
    WSUIS.setEnabled(lexique.isPresent("WSUIS"));
    WP2.setEnabled(lexique.isPresent("WP2"));
    INDNUM.setVisible(lexique.isPresent("INDSUF"));
    ARG9N1.setEnabled(lexique.isPresent("ARG9N1"));
    ARG36N.setEnabled(lexique.isPresent("ARG36N"));
    ARG2N.setVisible(lexique.isPresent("ARG2N"));
    ARG15A.setEnabled(lexique.isPresent("ARG15A"));
    isTousCriteres = lexique.isTrue("N61");
    WOBS.setVisible(lexique.isPresent("WOBS"));
    OBJ_84.setVisible(lexique.isPresent("WOBS"));
    WATN.setVisible(lexique.isPresent("WATN"));
    OBJ_86.setVisible(lexique.isPresent("WATN"));
    WEPOS.setVisible(lexique.isPresent("WEPOS"));
    OBJ_89.setVisible(lexique.isPresent("WEPOS"));
    WEPLF.setVisible(lexique.isPresent("WEPLF"));
    OBJ_90.setVisible(lexique.isPresent("WEPLF"));
    WEDEP.setVisible(lexique.isPresent("WEDEP"));
    OBJ_91.setVisible(lexique.isPresent("WEDEP"));
    
    if (isFacture) {
      OBJ_73.setText("Facture");
    }
    else {
      OBJ_73.setText("Numéro");
    }
    
    // Gestion bouton de sortie
    appele = lexique.HostFieldGetData("APPELE").trim().equalsIgnoreCase("");
    if (appele) {
      bouton_retour.setText("Sortir");
    }
    else {
      bouton_retour.setText("Retour");
    }
    
    // Facture
    OBJ_36.setVisible(isFacture);
    OBJ_39.setVisible(!isFacture);
    menuItem3.setVisible(!isFacture);
    menuItem5.setVisible(isFacture);
    menuItem7.setVisible(isFacture);
    menuItem8.setVisible(isFacture);
    menuItem9.setVisible(isFacture);
    
    miAutoriserReglementDiffere.setVisible(lexique.HostFieldGetData("SET171").trim().equals("3"));
    
    p_bpresentation.setCodeEtablissement(WETB.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void switchMode(String valeur) {
    int modeComplexe = 0;
    
    if (lexique.HostFieldGetData("TYPDOC").trim().equalsIgnoreCase("DEV")) {
      isUnBon = false;
      isUnDevis = true;
    }
    else {
      isUnBon = true;
      isUnDevis = false;
    }
    
    if (isUnBon) {
      OBJ_21.setText("Fin de bon");
      // Liste des bons en gestion des devis (point de menu)
      if (ecranDevis) {
        modeComplexe = 3;
        // Liste des bons en gestion des bons (point de menu)
      }
      else {
        modeComplexe = 4;
      }
    }
    else if (isUnDevis) {
      OBJ_21.setText("Fin de devis");
      // Liste des devis en gestion des devis (point de menu)
      if (ecranDevis) {
        modeComplexe = 1;
        // Liste des devis en gestion des bons (point de menu)
      }
      else {
        modeComplexe = 2;
      }
    }
    
    // Options de liste
    
    // Dupliquer les bons et les devis
    trans_bons.setVisible((isUnDevis && !ecranDevis) || (ecranDevis && isUnBon));
    if (isUnDevis && !ecranDevis) {
      trans_bons.setText("Dupliquer devis en bon");
    }
    if (ecranDevis && isUnBon) {
      trans_bons.setText("Dupliquer bon en devis");
    }
    
    OBJ_67.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_52.setVisible(modeComplexe == 1 && !isVerrou);
    OBJ_39.setVisible(isUnDevis && !ecranDevis);
    TRIAGE2.setVisible(isUnDevis && !ecranDevis);
    CHOISIR.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_18.setVisible(modeComplexe == 4 && !isVerrou);
    menuItem6.setVisible(modeComplexe == 4 && !isVerrou);
    
    OBJ_19.setVisible((modeComplexe == 1 || modeComplexe == 2 || modeComplexe == 4) && !isVerrou);
    menuItem2.setVisible(modeComplexe == 4 && !isVerrou);
    OBJ_31.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    
    // Mode verrouillage
    OBJ_20.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_21.setVisible((modeComplexe == 1 || modeComplexe == 4) && !isVerrou);
    OBJ_37.setVisible(isVerrou);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    if (appele) {
      lexique.HostScreenSendKey(this, "F3");
    }
    else {
      lexique.HostScreenSendKey(this, "F12");
    }
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_34ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("E");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("3");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("5");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("6");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("7");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_23ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("8");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("9");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("H");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_33ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("F");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void miAutoriserReglementDiffereActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("$");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_36ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("M");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_37ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_38ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("K");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_39ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("R");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_41ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_42ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_43ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    if (WTP01.doubleClicSelection(e)) {
      if (lexique.isTrue("93")) {
        WTP01.setValeurTop("+");
        lexique.HostScreenSendKey(this, "Enter");
      }
      else {
        lexique.HostScreenSendKey(this, "Enter");
      }
    }
  }
  
  private void OBJ_111ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_129ActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("WSUIS", 0, "F");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void menuItem2ActionPerformed(ActionEvent e) {
    popupMenu1.show(WTP01, 100, 50);
  }
  
  private void VERRActionPerformed(ActionEvent e) {
    WTP01.clearSelection();
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void menuItem3ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop(">");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void trans_bonsActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void riSousMenu_bt1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void menuItem6ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("X");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_40ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem5ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem7ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("À");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem8ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Â");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem9ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("V");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem80ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem81ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem82ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("A");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void menuItem10ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("Ç");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    WTP01.setValeurTop("&");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void TOUTETATActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void tbt_DevisActionPerformed(ActionEvent e) {
    if (!lexique.HostFieldGetData("TYPDOC").equals("DEV")) {
      lexique.HostFieldPutData("TYPDOC", 0, "DEV");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void tbt_CommandeActionPerformed(ActionEvent e) {
    if (!lexique.HostFieldGetData("TYPDOC").equals("CMD")) {
      lexique.HostFieldPutData("TYPDOC", 0, "CMD");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void tbt_LivraisonActionPerformed(ActionEvent e) {
    if (!lexique.HostFieldGetData("TYPDOC").equals("EXP")) {
      lexique.HostFieldPutData("TYPDOC", 0, "EXP");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void tbt_FactureActionPerformed(ActionEvent e) {
    if (!lexique.HostFieldGetData("TYPDOC").equals("FAC")) {
      lexique.HostFieldPutData("TYPDOC", 0, "FAC");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void WNOMKeyPressed(KeyEvent e) {
    WCOMPTE.setVisible(!WNOM.getText().trim().isEmpty());
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void tbt_ChantierActionPerformed(ActionEvent e) {
    if (!lexique.HostFieldGetData("TYPDOC").equals("*CH")) {
      lexique.HostFieldPutData("TYPDOC", 0, "*CH");
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void OBJ_35ActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_81 = new JLabel();
    OBJ_70 = new JLabel();
    WDAT1X = new XRiCalendrier();
    VERR = new XRiCheckBox();
    INDNUM = new XRiTextField();
    OBJ_73 = new JLabel();
    WETB = new RiZoneSortie();
    INDSUF = new XRiTextField();
    INDNFA = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu1 = new RiSousMenu();
    riSousMenu_bt1 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WNOM = new XRiTextField();
    OBJ_104 = new JLabel();
    ARG15A = new XRiTextField();
    ARG9N1 = new XRiTextField();
    OBJ_87 = new JLabel();
    ARG9N2 = new XRiTextField();
    WCOMPTE = new XRiCheckBox();
    TOUTETAT = new XRiCheckBox();
    WOBS = new RiZoneSortie();
    OBJ_84 = new JLabel();
    OBJ_86 = new JLabel();
    WATN = new RiZoneSortie();
    panel2 = new JPanel();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BT_DEB = new JButton();
    BT_FIN = new JButton();
    tbt_Devis = new JToggleButton();
    tbt_Commande = new JToggleButton();
    tbt_Livraison = new JToggleButton();
    tbt_Facture = new JToggleButton();
    tbt_Chantier = new JToggleButton();
    panel4 = new JPanel();
    WEPOS = new RiZoneSortie();
    WEPLF = new RiZoneSortie();
    WEDEP = new RiZoneSortie();
    OBJ_89 = new JLabel();
    OBJ_90 = new JLabel();
    OBJ_91 = new JLabel();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    trans_bons = new JMenuItem();
    OBJ_40 = new JMenuItem();
    OBJ_34 = new JMenuItem();
    OBJ_67 = new JMenuItem();
    OBJ_68 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    menuItem6 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_52 = new JMenuItem();
    OBJ_37 = new JMenuItem();
    menuItem5 = new JMenuItem();
    menuItem10 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    miAutoriserReglementDiffere = new JMenuItem();
    OBJ_38 = new JMenuItem();
    menuItem2 = new JMenuItem();
    WSUIS = new XRiTextField();
    WP2 = new XRiTextField();
    popupMenu1 = new JPopupMenu();
    OBJ_32 = new JMenuItem();
    OBJ_33 = new JMenuItem();
    OBJ_36 = new JMenuItem();
    OBJ_39 = new JMenuItem();
    menuItem3 = new JMenuItem();
    OBJ_23 = new JMenuItem();
    menuItem9 = new JMenuItem();
    menuItem7 = new JMenuItem();
    menuItem8 = new JMenuItem();
    TRIAGE2 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_24 = new JMenuItem();
    OBJ_41 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    OBJ_42 = new JMenuItem();
    OBJ_43 = new JMenuItem();
    OBJ_44 = new JMenuItem();
    OBJ_45 = new JMenuItem();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    menuItem4 = new JMenuItem();
    TRIAGE = new JMenuItem();
    popupMenu2 = new JPopupMenu();
    menuItem80 = new JMenuItem();
    menuItem81 = new JMenuItem();
    menuItem82 = new JMenuItem();
    ARG2N = new XRiTextField();
    ARG36N = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(1024, 660));
    setPreferredSize(new Dimension(1024, 660));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Gestion de @GESTION@ @SOUTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(850, 32));
          p_tete_gauche.setMinimumSize(new Dimension(850, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_81 ----
          OBJ_81.setText("Traitement");
          OBJ_81.setName("OBJ_81");
          p_tete_gauche.add(OBJ_81);
          OBJ_81.setBounds(462, 2, 75, 24);

          //---- OBJ_70 ----
          OBJ_70.setText("Etablissement");
          OBJ_70.setName("OBJ_70");
          p_tete_gauche.add(OBJ_70);
          OBJ_70.setBounds(5, 2, 90, 24);

          //---- WDAT1X ----
          WDAT1X.setComponentPopupMenu(null);
          WDAT1X.setName("WDAT1X");
          p_tete_gauche.add(WDAT1X);
          WDAT1X.setBounds(549, 0, 105, WDAT1X.getPreferredSize().height);

          //---- VERR ----
          VERR.setText("Verrouill\u00e9s");
          VERR.setToolTipText("Liste avec ou sans les bons verrouill\u00e9s");
          VERR.setComponentPopupMenu(BTD);
          VERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          VERR.setName("VERR");
          VERR.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              VERRActionPerformed(e);
            }
          });
          p_tete_gauche.add(VERR);
          VERR.setBounds(305, 2, 98, 24);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(null);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(200, 0, 58, INDNUM.getPreferredSize().height);

          //---- OBJ_73 ----
          OBJ_73.setText("Num\u00e9ro");
          OBJ_73.setName("OBJ_73");
          p_tete_gauche.add(OBJ_73);
          OBJ_73.setBounds(150, 2, 50, 24);

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setOpaque(false);
          WETB.setText("@WETB@");
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(98, 2, 40, WETB.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(null);
          INDSUF.setHorizontalAlignment(SwingConstants.RIGHT);
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(265, 0, 20, INDSUF.getPreferredSize().height);

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(null);
          INDNFA.setName("INDNFA");
          p_tete_gauche.add(INDNFA);
          INDNFA.setBounds(200, 0, 66, INDNFA.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_tete_gauche.getComponentCount(); i++) {
              Rectangle bounds = p_tete_gauche.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_tete_gauche.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_tete_gauche.setMinimumSize(preferredSize);
            p_tete_gauche.setPreferredSize(preferredSize);
          }
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setToolTipText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu1 ========
            {
              riSousMenu1.setName("riSousMenu1");

              //---- riSousMenu_bt1 ----
              riSousMenu_bt1.setText("Autres vues");
              riSousMenu_bt1.setName("riSousMenu_bt1");
              riSousMenu_bt1.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt1ActionPerformed(e);
                }
              });
              riSousMenu1.add(riSousMenu_bt1);
            }
            menus_haut.add(riSousMenu1);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Bon ou facture");
              riSousMenu_bt9.setToolTipText("Recherche bon ou facture");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Recherche");
              riSousMenu_bt12.setToolTipText("Crit\u00e8res de recherche");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setPreferredSize(new Dimension(1240, 575));
        p_centrage.setMinimumSize(new Dimension(850, 605));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        ((GridBagLayout)p_centrage.getLayout()).columnWidths = new int[] {1050};

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 550));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setForeground(Color.black);
          p_contenu.setMinimumSize(new Dimension(800, 560));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Filtres de recherche"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WNOM ----
            WNOM.setName("WNOM");
            WNOM.addKeyListener(new KeyAdapter() {
              @Override
              public void keyPressed(KeyEvent e) {
                WNOMKeyPressed(e);
              }
            });
            panel1.add(WNOM);
            WNOM.setBounds(165, 30, 160, 28);

            //---- OBJ_104 ----
            OBJ_104.setText("Recherche sur r\u00e9f\u00e9rences");
            OBJ_104.setHorizontalAlignment(SwingConstants.LEFT);
            OBJ_104.setName("OBJ_104");
            panel1.add(OBJ_104);
            OBJ_104.setBounds(15, 59, 150, 28);

            //---- ARG15A ----
            ARG15A.setComponentPopupMenu(null);
            ARG15A.setToolTipText("Recherche sur r\u00e9f\u00e9rence client, r\u00e9f\u00e9rence longue et commande initiale");
            ARG15A.setName("ARG15A");
            panel1.add(ARG15A);
            ARG15A.setBounds(165, 59, 90, ARG15A.getPreferredSize().height);

            //---- ARG9N1 ----
            ARG9N1.setComponentPopupMenu(null);
            ARG9N1.setHorizontalAlignment(SwingConstants.RIGHT);
            ARG9N1.setName("ARG9N1");
            panel1.add(ARG9N1);
            ARG9N1.setBounds(60, 30, 58, ARG9N1.getPreferredSize().height);

            //---- OBJ_87 ----
            OBJ_87.setText("Client");
            OBJ_87.setName("OBJ_87");
            panel1.add(OBJ_87);
            OBJ_87.setBounds(15, 30, 45, 28);

            //---- ARG9N2 ----
            ARG9N2.setComponentPopupMenu(null);
            ARG9N2.setName("ARG9N2");
            panel1.add(ARG9N2);
            ARG9N2.setBounds(115, 30, 40, ARG9N2.getPreferredSize().height);

            //---- WCOMPTE ----
            WCOMPTE.setText("client en compte");
            WCOMPTE.setName("WCOMPTE");
            panel1.add(WCOMPTE);
            WCOMPTE.setBounds(330, 30, 155, 28);

            //---- TOUTETAT ----
            TOUTETAT.setText("Historique");
            TOUTETAT.setBorder(new SoftBevelBorder(SoftBevelBorder.LOWERED));
            TOUTETAT.setBackground(new Color(153, 153, 153));
            TOUTETAT.setForeground(Color.black);
            TOUTETAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TOUTETAT.setName("TOUTETAT");
            TOUTETAT.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                TOUTETATActionPerformed(e);
              }
            });
            panel1.add(TOUTETAT);
            TOUTETAT.setBounds(15, 90, 175, 28);

            //---- WOBS ----
            WOBS.setText("@WOBS@");
            WOBS.setName("WOBS");
            panel1.add(WOBS);
            WOBS.setBounds(575, 32, 180, WOBS.getPreferredSize().height);

            //---- OBJ_84 ----
            OBJ_84.setText("Obs.");
            OBJ_84.setName("OBJ_84");
            panel1.add(OBJ_84);
            OBJ_84.setBounds(495, 32, 75, 24);

            //---- OBJ_86 ----
            OBJ_86.setText("Attention");
            OBJ_86.setName("OBJ_86");
            panel1.add(OBJ_86);
            OBJ_86.setBounds(495, 61, 75, 24);

            //---- WATN ----
            WATN.setText("@WATN@");
            WATN.setName("WATN");
            panel1.add(WATN);
            WATN.setBounds(575, 61, 180, WATN.getPreferredSize().height);
          }
          p_contenu.add(panel1);
          panel1.setBounds(13, 75, 772, 130);

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("R\u00e9sultat de la recherche"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel2.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(15, 35, 960, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel2.add(BT_PGUP);
            BT_PGUP.setBounds(980, 60, 25, 95);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel2.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(980, 175, 25, 95);

            //---- BT_DEB ----
            BT_DEB.setText("");
            BT_DEB.setToolTipText("D\u00e9but de la liste");
            BT_DEB.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_DEB.setName("BT_DEB");
            BT_DEB.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_111ActionPerformed(e);
              }
            });
            panel2.add(BT_DEB);
            BT_DEB.setBounds(980, 30, 25, 30);

            //---- BT_FIN ----
            BT_FIN.setText("");
            BT_FIN.setToolTipText("Fin de la liste");
            BT_FIN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_FIN.setName("BT_FIN");
            BT_FIN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_129ActionPerformed(e);
              }
            });
            panel2.add(BT_FIN);
            BT_FIN.setBounds(980, 270, 25, 30);
          }
          p_contenu.add(panel2);
          panel2.setBounds(11, 205, 1024, 330);

          //---- tbt_Devis ----
          tbt_Devis.setText("DEVIS");
          tbt_Devis.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tbt_Devis.setName("tbt_Devis");
          tbt_Devis.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tbt_DevisActionPerformed(e);
            }
          });
          p_contenu.add(tbt_Devis);
          tbt_Devis.setBounds(223, 15, 186, 53);

          //---- tbt_Commande ----
          tbt_Commande.setText("COMMANDES");
          tbt_Commande.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tbt_Commande.setName("tbt_Commande");
          tbt_Commande.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tbt_CommandeActionPerformed(e);
            }
          });
          p_contenu.add(tbt_Commande);
          tbt_Commande.setBounds(432, 15, 185, 53);

          //---- tbt_Livraison ----
          tbt_Livraison.setText("LIVRAISONS OU RETRAITS");
          tbt_Livraison.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tbt_Livraison.setName("tbt_Livraison");
          tbt_Livraison.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tbt_LivraisonActionPerformed(e);
            }
          });
          p_contenu.add(tbt_Livraison);
          tbt_Livraison.setBounds(640, 15, 184, 53);

          //---- tbt_Facture ----
          tbt_Facture.setText("FACTURES");
          tbt_Facture.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tbt_Facture.setName("tbt_Facture");
          tbt_Facture.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tbt_FactureActionPerformed(e);
            }
          });
          p_contenu.add(tbt_Facture);
          tbt_Facture.setBounds(847, 15, 188, 53);

          //---- tbt_Chantier ----
          tbt_Chantier.setText("CHANTIERS");
          tbt_Chantier.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          tbt_Chantier.setName("tbt_Chantier");
          tbt_Chantier.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              tbt_ChantierActionPerformed(e);
            }
          });
          p_contenu.add(tbt_Chantier);
          tbt_Chantier.setBounds(13, 15, 187, 53);

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("Encours client"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- WEPOS ----
            WEPOS.setText("@WEPOS@");
            WEPOS.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPOS.setName("WEPOS");
            panel4.add(WEPOS);
            WEPOS.setBounds(140, 32, 70, WEPOS.getPreferredSize().height);

            //---- WEPLF ----
            WEPLF.setText("@WEPLF@");
            WEPLF.setHorizontalAlignment(SwingConstants.RIGHT);
            WEPLF.setName("WEPLF");
            panel4.add(WEPLF);
            WEPLF.setBounds(140, 61, 70, WEPLF.getPreferredSize().height);

            //---- WEDEP ----
            WEDEP.setText("@WEDEP@");
            WEDEP.setHorizontalAlignment(SwingConstants.RIGHT);
            WEDEP.setForeground(new Color(171, 8, 8));
            WEDEP.setName("WEDEP");
            panel4.add(WEDEP);
            WEDEP.setBounds(140, 90, 70, WEDEP.getPreferredSize().height);

            //---- OBJ_89 ----
            OBJ_89.setText("Position");
            OBJ_89.setName("OBJ_89");
            panel4.add(OBJ_89);
            OBJ_89.setBounds(20, 35, 100, 24);

            //---- OBJ_90 ----
            OBJ_90.setText("Plafond");
            OBJ_90.setName("OBJ_90");
            panel4.add(OBJ_90);
            OBJ_90.setBounds(20, 61, 100, 24);

            //---- OBJ_91 ----
            OBJ_91.setText("D\u00e9passement");
            OBJ_91.setForeground(new Color(171, 8, 8));
            OBJ_91.setName("OBJ_91");
            panel4.add(OBJ_91);
            OBJ_91.setBounds(20, 90, 100, 24);
          }
          p_contenu.add(panel4);
          panel4.setBounds(790, 75, 245, 130);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);

      //---- trans_bons ----
      trans_bons.setText("Cr\u00e9er la commande client");
      trans_bons.setName("trans_bons");
      trans_bons.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          trans_bonsActionPerformed(e);
        }
      });
      BTD.add(trans_bons);

      //---- OBJ_40 ----
      OBJ_40.setText("Afficher");
      OBJ_40.setName("OBJ_40");
      OBJ_40.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_40ActionPerformed(e);
        }
      });
      BTD.add(OBJ_40);

      //---- OBJ_34 ----
      OBJ_34.setText("Edition");
      OBJ_34.setName("OBJ_34");
      OBJ_34.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_34ActionPerformed(e);
        }
      });
      BTD.add(OBJ_34);

      //---- OBJ_67 ----
      OBJ_67.setText("Exportation tableur");
      OBJ_67.setName("OBJ_67");
      OBJ_67.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_35ActionPerformed(e);
        }
      });
      BTD.add(OBJ_67);

      //---- OBJ_68 ----
      OBJ_68.setText("Suivi de commande");
      OBJ_68.setName("OBJ_68");
      OBJ_68.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_68ActionPerformed(e);
        }
      });
      BTD.add(OBJ_68);

      //---- OBJ_18 ----
      OBJ_18.setText("Extraire");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- menuItem6 ----
      menuItem6.setText("Extraction quantit\u00e9s disponibles");
      menuItem6.setName("menuItem6");
      menuItem6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem6ActionPerformed(e);
        }
      });
      BTD.add(menuItem6);

      //---- OBJ_19 ----
      OBJ_19.setText("Dupliquer");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);

      //---- OBJ_52 ----
      OBJ_52.setText("Duplication avec changement de tiers");
      OBJ_52.setName("OBJ_52");
      OBJ_52.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BTD.add(OBJ_52);

      //---- OBJ_37 ----
      OBJ_37.setText("D\u00e9verrouillage");
      OBJ_37.setName("OBJ_37");
      OBJ_37.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_37ActionPerformed(e);
        }
      });
      BTD.add(OBJ_37);

      //---- menuItem5 ----
      menuItem5.setText("Avoir");
      menuItem5.setName("menuItem5");
      menuItem5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem5ActionPerformed(e);
        }
      });
      BTD.add(menuItem5);

      //---- menuItem10 ----
      menuItem10.setText("Cr\u00e9ation avec cette adresse");
      menuItem10.setName("menuItem10");
      menuItem10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem10ActionPerformed(e);
        }
      });
      BTD.add(menuItem10);
      BTD.addSeparator();

      //---- OBJ_20 ----
      OBJ_20.setText("Acc\u00e8s aux lignes de pied");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_21 ----
      OBJ_21.setText("Fin de bon");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
      BTD.addSeparator();

      //---- OBJ_31 ----
      OBJ_31.setText("Historique");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BTD.add(OBJ_31);

      //---- miAutoriserReglementDiffere ----
      miAutoriserReglementDiffere.setText("Autoriser le r\u00e8glement diff\u00e9r\u00e9");
      miAutoriserReglementDiffere.setName("miAutoriserReglementDiffere");
      miAutoriserReglementDiffere.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miAutoriserReglementDiffereActionPerformed(e);
        }
      });
      BTD.add(miAutoriserReglementDiffere);

      //---- OBJ_38 ----
      OBJ_38.setText("Options client");
      OBJ_38.setName("OBJ_38");
      OBJ_38.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_38ActionPerformed(e);
        }
      });
      BTD.add(OBJ_38);

      //---- menuItem2 ----
      menuItem2.setText("+ Options avanc\u00e9es ");
      menuItem2.setComponentPopupMenu(popupMenu1);
      menuItem2.setName("menuItem2");
      menuItem2.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem2ActionPerformed(e);
        }
      });
      BTD.add(menuItem2);
      BTD.addSeparator();
    }

    //---- WSUIS ----
    WSUIS.setComponentPopupMenu(BTD);
    WSUIS.setName("WSUIS");

    //---- WP2 ----
    WP2.setComponentPopupMenu(BTD);
    WP2.setName("WP2");

    //======== popupMenu1 ========
    {
      popupMenu1.setName("popupMenu1");

      //---- OBJ_32 ----
      OBJ_32.setText("Duplication avec changement de tiers");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_32);

      //---- OBJ_33 ----
      OBJ_33.setText("Duplication avec changement de type de facturation");
      OBJ_33.setName("OBJ_33");
      OBJ_33.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_33ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_33);

      //---- OBJ_36 ----
      OBJ_36.setText("Modification Ent\u00eate apr\u00e8s facturation");
      OBJ_36.setName("OBJ_36");
      OBJ_36.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_36ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_36);

      //---- OBJ_39 ----
      OBJ_39.setText("Rechiffrer");
      OBJ_39.setName("OBJ_39");
      OBJ_39.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_39ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_39);

      //---- menuItem3 ----
      menuItem3.setText("Regroupement de commande");
      menuItem3.setName("menuItem3");
      menuItem3.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem3ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem3);

      //---- OBJ_23 ----
      OBJ_23.setText("Acc\u00e8s au colisage");
      OBJ_23.setName("OBJ_23");
      OBJ_23.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_23ActionPerformed(e);
        }
      });
      popupMenu1.add(OBJ_23);

      //---- menuItem9 ----
      menuItem9.setText("Avoir global en valeur");
      menuItem9.setName("menuItem9");
      menuItem9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem9ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem9);

      //---- menuItem7 ----
      menuItem7.setText("Avoir partiel");
      menuItem7.setName("menuItem7");
      menuItem7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem7ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem7);

      //---- menuItem8 ----
      menuItem8.setText("Avoir partiel en valeur");
      menuItem8.setName("menuItem8");
      menuItem8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem8ActionPerformed(e);
        }
      });
      popupMenu1.add(menuItem8);
    }

    //---- TRIAGE2 ----
    TRIAGE2.setText("Tri\u00e9 par ...");
    TRIAGE2.setName("TRIAGE2");

    //---- OBJ_22 ----
    OBJ_22.setText("Acc\u00e8s aux achats");
    OBJ_22.setName("OBJ_22");
    OBJ_22.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_22ActionPerformed(e);
      }
    });

    //---- OBJ_24 ----
    OBJ_24.setText("Adresse de stock");
    OBJ_24.setName("OBJ_24");
    OBJ_24.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_24ActionPerformed(e);
      }
    });

    //---- OBJ_41 ----
    OBJ_41.setText("Affichage affectation adresse de stock");
    OBJ_41.setName("OBJ_41");
    OBJ_41.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_41ActionPerformed(e);
      }
    });

    //---- OBJ_25 ----
    OBJ_25.setText("Affectation de commande");
    OBJ_25.setName("OBJ_25");
    OBJ_25.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_25ActionPerformed(e);
      }
    });

    //---- OBJ_42 ----
    OBJ_42.setText("D\u00e9tail pr\u00e9paration");
    OBJ_42.setName("OBJ_42");
    OBJ_42.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_42ActionPerformed(e);
      }
    });

    //---- OBJ_43 ----
    OBJ_43.setText("D\u00e9tail emballage");
    OBJ_43.setName("OBJ_43");
    OBJ_43.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_43ActionPerformed(e);
      }
    });

    //---- OBJ_44 ----
    OBJ_44.setText("D\u00e9tail exp\u00e9dition");
    OBJ_44.setName("OBJ_44");
    OBJ_44.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_44ActionPerformed(e);
      }
    });

    //---- OBJ_45 ----
    OBJ_45.setText("Annulation de commande");
    OBJ_45.setName("OBJ_45");
    OBJ_45.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_45ActionPerformed(e);
      }
    });

    //---- OBJ_29 ----
    OBJ_29.setText("Autorisation");
    OBJ_29.setName("OBJ_29");
    OBJ_29.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_29ActionPerformed(e);
      }
    });

    //---- OBJ_30 ----
    OBJ_30.setText("Calcul cl\u00e9 d'autorisation");
    OBJ_30.setName("OBJ_30");
    OBJ_30.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_30ActionPerformed(e);
      }
    });

    //---- menuItem4 ----
    menuItem4.setText("Reliquats");
    menuItem4.setName("menuItem4");

    //---- TRIAGE ----
    TRIAGE.setText("Tri\u00e9 par ...");
    TRIAGE.setName("TRIAGE");

    //======== popupMenu2 ========
    {
      popupMenu2.setName("popupMenu2");

      //---- menuItem80 ----
      menuItem80.setText("S\u00e9lectionner");
      menuItem80.setName("menuItem80");
      menuItem80.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem80ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem80);

      //---- menuItem81 ----
      menuItem81.setText("D\u00e9-s\u00e9lectionner");
      menuItem81.setName("menuItem81");
      menuItem81.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem81ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem81);

      //---- menuItem82 ----
      menuItem82.setText("Afficher");
      menuItem82.setName("menuItem82");
      menuItem82.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          menuItem82ActionPerformed(e);
        }
      });
      popupMenu2.add(menuItem82);
    }

    //---- ARG2N ----
    ARG2N.setComponentPopupMenu(null);
    ARG2N.setName("ARG2N");

    //---- ARG36N ----
    ARG36N.setComponentPopupMenu(null);
    ARG36N.setName("ARG36N");

    //---- buttonGroup1 ----
    ButtonGroup buttonGroup1 = new ButtonGroup();
    buttonGroup1.add(tbt_Devis);
    buttonGroup1.add(tbt_Commande);
    buttonGroup1.add(tbt_Livraison);
    buttonGroup1.add(tbt_Facture);
    buttonGroup1.add(tbt_Chantier);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_81;
  private JLabel OBJ_70;
  private XRiCalendrier WDAT1X;
  private XRiCheckBox VERR;
  private XRiTextField INDNUM;
  private JLabel OBJ_73;
  private RiZoneSortie WETB;
  private XRiTextField INDSUF;
  private XRiTextField INDNFA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu1;
  private RiSousMenu_bt riSousMenu_bt1;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField WNOM;
  private JLabel OBJ_104;
  private XRiTextField ARG15A;
  private XRiTextField ARG9N1;
  private JLabel OBJ_87;
  private XRiTextField ARG9N2;
  private XRiCheckBox WCOMPTE;
  private XRiCheckBox TOUTETAT;
  private RiZoneSortie WOBS;
  private JLabel OBJ_84;
  private JLabel OBJ_86;
  private RiZoneSortie WATN;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JButton BT_DEB;
  private JButton BT_FIN;
  private JToggleButton tbt_Devis;
  private JToggleButton tbt_Commande;
  private JToggleButton tbt_Livraison;
  private JToggleButton tbt_Facture;
  private JToggleButton tbt_Chantier;
  private JPanel panel4;
  private RiZoneSortie WEPOS;
  private RiZoneSortie WEPLF;
  private RiZoneSortie WEDEP;
  private JLabel OBJ_89;
  private JLabel OBJ_90;
  private JLabel OBJ_91;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JMenuItem trans_bons;
  private JMenuItem OBJ_40;
  private JMenuItem OBJ_34;
  private JMenuItem OBJ_67;
  private JMenuItem OBJ_68;
  private JMenuItem OBJ_18;
  private JMenuItem menuItem6;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_52;
  private JMenuItem OBJ_37;
  private JMenuItem menuItem5;
  private JMenuItem menuItem10;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_21;
  private JMenuItem OBJ_31;
  private JMenuItem miAutoriserReglementDiffere;
  private JMenuItem OBJ_38;
  private JMenuItem menuItem2;
  private XRiTextField WSUIS;
  private XRiTextField WP2;
  private JPopupMenu popupMenu1;
  private JMenuItem OBJ_32;
  private JMenuItem OBJ_33;
  private JMenuItem OBJ_36;
  private JMenuItem OBJ_39;
  private JMenuItem menuItem3;
  private JMenuItem OBJ_23;
  private JMenuItem menuItem9;
  private JMenuItem menuItem7;
  private JMenuItem menuItem8;
  private JMenuItem TRIAGE2;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_41;
  private JMenuItem OBJ_25;
  private JMenuItem OBJ_42;
  private JMenuItem OBJ_43;
  private JMenuItem OBJ_44;
  private JMenuItem OBJ_45;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem menuItem4;
  private JMenuItem TRIAGE;
  private JPopupMenu popupMenu2;
  private JMenuItem menuItem80;
  private JMenuItem menuItem81;
  private JMenuItem menuItem82;
  private XRiTextField ARG2N;
  private XRiTextField ARG36N;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
