
package ri.serien.libecranrpg.rgvm.RGVM14FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class RGVM14FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  
  private String[] ARG17A_Value = { "", "+", };
  
  public RGVM14FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    TIDX25.setValeurs("25", "RBC");
    TIDX24.setValeurs("24", "RBC");
    TIDX23.setValeurs("23", "RBC");
    TIDX22.setValeurs("22", "RBC");
    TIDX21.setValeurs("21", "RBC");
    TIDX20.setValeurs("20", "RBC");
    TIDX15.setValeurs("15", "RBC");
    TIDX16.setValeurs("16", "RBC");
    TIDX8.setValeurs("8", "RBC");
    TIDX7.setValeurs("7", "RBC");
    TIDX6.setValeurs("6", "RBC");
    TIDX5.setValeurs("5", "RBC");
    TIDX4.setValeurs("4", "RBC");
    TIDX3.setValeurs("3", "RBC");
    TIDX2.setValeurs("2", "RBC");
    ARG17A.setValeurs(ARG17A_Value, null);
    SCAN15.setValeursSelection("S", " ");
    
    SCAN17.setValeurs("N", SCAN17_GRP);
    SCAN17_INCLUS.setValeurs("E");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    // SCAN3.setSelected(lexique.HostFieldGetData("SCAN3").equalsIgnoreCase("S"));
    // SCAN15.setSelected(lexique.HostFieldGetData("SCAN15").equalsIgnoreCase("S"));
    // OBJ_72.setSelected(lexique.HostFieldGetData("SCAN17").equalsIgnoreCase("E"));
    // OBJ_71.setSelected(lexique.HostFieldGetData("SCAN17").equalsIgnoreCase("N"));
    // TIDX3.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("3"));
    // TIDX5.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("5"));
    // TIDX24.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("24"));
    // TIDX2.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("2"));
    // TIDX25.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("25"));
    // TIDX7.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("7"));
    // TIDX21.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("21"));
    // TIDX20.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("20"));
    // TIDX22.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("22"));
    // TIDX23.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("23"));
    // TIDX4.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("4"));
    // TIDX15.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("15"));
    // TIDX16.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("16"));
    // TIDX8.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("8"));
    // TIDX6.setSelected(lexique.HostFieldGetData("RBC").equalsIgnoreCase("6"));
    
    panel3.setVisible(lexique.isTrue("N92"));
    TIDX21.setVisible(lexique.isTrue("N92"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (SCAN3.isSelected())
    // lexique.HostFieldPutData("SCAN3", 0, "S");
    // else
    // lexique.HostFieldPutData("SCAN3", 0, " ");
    // if (SCAN15.isSelected())
    // lexique.HostFieldPutData("SCAN15", 0, "S");
    // else
    // lexique.HostFieldPutData("SCAN15", 0, " ");
    // if (OBJ_72.isSelected())
    // lexique.HostFieldPutData("SCAN17", 0, "E");
    // if (OBJ_71.isSelected())
    // lexique.HostFieldPutData("SCAN17", 0, "N");
    // if (TIDX3.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "3");
    // if (TIDX5.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "5");
    // if (TIDX24.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "24");
    // if (TIDX2.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "2");
    // if (TIDX25.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "25");
    // if (TIDX7.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "7");
    // if (TIDX21.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "21");
    // if (TIDX20.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "20");
    // if (TIDX22.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "22");
    // if (TIDX23.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "23");
    // if (TIDX4.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "4");
    // if (TIDX15.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "15");
    // if (TIDX16.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "16");
    // if (TIDX8.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "8");
    // if (TIDX6.isSelected())
    // lexique.HostFieldPutData("RBC", 0, "6");
    // lexique.HostFieldPutData("ARG17A", 0, ARG17A_Value[ARG17A.getSelectedIndex()]);
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_30 = new JLabel();
    INDNUM = new XRiTextField();
    OBJ_31 = new JLabel();
    INDETB = new XRiTextField();
    INDSUF = new XRiTextField();
    INDNFA = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    TIDX2 = new XRiRadioButton();
    ARG2N = new XRiTextField();
    TIDX3 = new XRiRadioButton();
    ARG3A = new XRiTextField();
    TIDX4 = new XRiRadioButton();
    ARG4A = new XRiTextField();
    ARG4B = new XRiTextField();
    TIDX5 = new XRiRadioButton();
    ARG5A = new XRiTextField();
    TIDX6 = new XRiRadioButton();
    ARG6A = new XRiTextField();
    TIDX7 = new XRiRadioButton();
    ARG7A = new XRiTextField();
    TIDX8 = new XRiRadioButton();
    ARG8A = new XRiTextField();
    TIDX16 = new XRiRadioButton();
    ARG16N2 = new XRiTextField();
    ARG16N1 = new XRiTextField();
    panel4 = new JPanel();
    OBJ_74 = new JLabel();
    ARG9A = new XRiTextField();
    OBJ_75 = new JLabel();
    FLD011 = new XRiTextField();
    OBJ_76 = new JLabel();
    ARG17A = new XRiComboBox();
    SCAN17 = new XRiRadioButton();
    SCAN17_INCLUS = new XRiRadioButton();
    ARG13A = new XRiTextField();
    OBJ_80 = new JLabel();
    ARG12A = new XRiTextField();
    OBJ_78 = new JLabel();
    FLD012 = new XRiTextField();
    OBJ_77 = new JLabel();
    OBJ_82 = new JLabel();
    ARG14A = new XRiTextField();
    TIDX15 = new XRiRadioButton();
    ARG15A = new XRiTextField();
    SCAN15 = new XRiCheckBox();
    panel2 = new JPanel();
    TIDX20 = new XRiRadioButton();
    TIDX21 = new XRiRadioButton();
    OBJ_48 = new JLabel();
    OBJ_49 = new JLabel();
    ARG20D = new XRiCalendrier();
    PLA20D = new XRiCalendrier();
    ARG21D = new XRiCalendrier();
    PLA21D = new XRiCalendrier();
    panel3 = new JPanel();
    TIDX22 = new XRiRadioButton();
    ARG22N = new XRiTextField();
    PLA22N = new XRiTextField();
    PLA23N = new XRiTextField();
    ARG23N = new XRiTextField();
    TIDX23 = new XRiRadioButton();
    TIDX24 = new XRiRadioButton();
    ARG24N = new XRiTextField();
    PLA24N = new XRiTextField();
    PLA25N = new XRiTextField();
    ARG25N = new XRiTextField();
    TIDX25 = new XRiRadioButton();
    OBJ_64 = new JLabel();
    OBJ_63 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    OBJ_44 = new JLabel();
    OBJ_60 = new JLabel();
    OBJ_36 = new JPanel();
    OBJ_35 = new JTabbedPane();
    SCAN17_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_30 ----
          OBJ_30.setText("Etablissement");
          OBJ_30.setName("OBJ_30");
          p_tete_gauche.add(OBJ_30);
          OBJ_30.setBounds(5, 5, 95, 20);

          //---- INDNUM ----
          INDNUM.setComponentPopupMenu(BTD);
          INDNUM.setName("INDNUM");
          p_tete_gauche.add(INDNUM);
          INDNUM.setBounds(215, 0, 58, INDNUM.getPreferredSize().height);

          //---- OBJ_31 ----
          OBJ_31.setText("Num\u00e9ro");
          OBJ_31.setName("OBJ_31");
          p_tete_gauche.add(OBJ_31);
          OBJ_31.setBounds(160, 5, 50, 20);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(105, 1, 40, INDETB.getPreferredSize().height);

          //---- INDSUF ----
          INDSUF.setComponentPopupMenu(BTD);
          INDSUF.setName("INDSUF");
          p_tete_gauche.add(INDSUF);
          INDSUF.setBounds(275, 0, 27, INDSUF.getPreferredSize().height);

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(BTD);
          INDNFA.setName("INDNFA");
          p_tete_gauche.add(INDNFA);
          INDNFA.setBounds(340, 0, 66, INDNFA.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Stock. personnalisation");
              riSousMenu_bt6.setToolTipText("Stockage de la personnalisation programme");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(870, 470));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- TIDX2 ----
            TIDX2.setText("Num\u00e9ro");
            TIDX2.setComponentPopupMenu(BTD);
            TIDX2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX2.setName("TIDX2");
            panel1.add(TIDX2);
            TIDX2.setBounds(15, 19, 125, 20);

            //---- ARG2N ----
            ARG2N.setComponentPopupMenu(BTD);
            ARG2N.setName("ARG2N");
            panel1.add(ARG2N);
            ARG2N.setBounds(140, 15, 70, ARG2N.getPreferredSize().height);

            //---- TIDX3 ----
            TIDX3.setText("Article");
            TIDX3.setComponentPopupMenu(BTD);
            TIDX3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX3.setName("TIDX3");
            panel1.add(TIDX3);
            TIDX3.setBounds(15, 49, 125, 20);

            //---- ARG3A ----
            ARG3A.setComponentPopupMenu(BTD);
            ARG3A.setName("ARG3A");
            panel1.add(ARG3A);
            ARG3A.setBounds(140, 45, 210, ARG3A.getPreferredSize().height);

            //---- TIDX4 ----
            TIDX4.setText("Fournisseur");
            TIDX4.setComponentPopupMenu(BTD);
            TIDX4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX4.setName("TIDX4");
            panel1.add(TIDX4);
            TIDX4.setBounds(15, 79, 125, 20);

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(BTD);
            ARG4A.setName("ARG4A");
            panel1.add(ARG4A);
            ARG4A.setBounds(140, 75, 20, ARG4A.getPreferredSize().height);

            //---- ARG4B ----
            ARG4B.setComponentPopupMenu(BTD);
            ARG4B.setName("ARG4B");
            panel1.add(ARG4B);
            ARG4B.setBounds(160, 75, 70, ARG4B.getPreferredSize().height);

            //---- TIDX5 ----
            TIDX5.setText("Famille");
            TIDX5.setComponentPopupMenu(BTD);
            TIDX5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX5.setName("TIDX5");
            panel1.add(TIDX5);
            TIDX5.setBounds(15, 109, 125, 20);

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD);
            ARG5A.setName("ARG5A");
            panel1.add(ARG5A);
            ARG5A.setBounds(140, 105, 40, ARG5A.getPreferredSize().height);

            //---- TIDX6 ----
            TIDX6.setText("Unit\u00e9 de vente");
            TIDX6.setComponentPopupMenu(BTD);
            TIDX6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX6.setName("TIDX6");
            panel1.add(TIDX6);
            TIDX6.setBounds(15, 139, 125, 20);

            //---- ARG6A ----
            ARG6A.setComponentPopupMenu(BTD);
            ARG6A.setName("ARG6A");
            panel1.add(ARG6A);
            ARG6A.setBounds(140, 135, 40, ARG6A.getPreferredSize().height);

            //---- TIDX7 ----
            TIDX7.setText("Magasin");
            TIDX7.setComponentPopupMenu(BTD);
            TIDX7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX7.setName("TIDX7");
            panel1.add(TIDX7);
            TIDX7.setBounds(15, 169, 125, 20);

            //---- ARG7A ----
            ARG7A.setComponentPopupMenu(BTD);
            ARG7A.setName("ARG7A");
            panel1.add(ARG7A);
            ARG7A.setBounds(140, 165, 30, ARG7A.getPreferredSize().height);

            //---- TIDX8 ----
            TIDX8.setText("Repr\u00e9sentant");
            TIDX8.setComponentPopupMenu(BTD);
            TIDX8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX8.setName("TIDX8");
            panel1.add(TIDX8);
            TIDX8.setBounds(15, 199, 125, 20);

            //---- ARG8A ----
            ARG8A.setComponentPopupMenu(BTD);
            ARG8A.setName("ARG8A");
            panel1.add(ARG8A);
            ARG8A.setBounds(140, 195, 30, ARG8A.getPreferredSize().height);

            //---- TIDX16 ----
            TIDX16.setText("Num\u00e9ro Client");
            TIDX16.setComponentPopupMenu(BTD);
            TIDX16.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX16.setName("TIDX16");
            panel1.add(TIDX16);
            TIDX16.setBounds(15, 229, 125, 20);

            //---- ARG16N2 ----
            ARG16N2.setComponentPopupMenu(BTD);
            ARG16N2.setName("ARG16N2");
            panel1.add(ARG16N2);
            ARG16N2.setBounds(200, 225, 40, ARG16N2.getPreferredSize().height);

            //---- ARG16N1 ----
            ARG16N1.setComponentPopupMenu(BTD);
            ARG16N1.setName("ARG16N1");
            panel1.add(ARG16N1);
            ARG16N1.setBounds(140, 225, 60, ARG16N1.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          //======== panel4 ========
          {
            panel4.setBorder(new TitledBorder(""));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);

            //---- OBJ_74 ----
            OBJ_74.setText("Avoir");
            OBJ_74.setName("OBJ_74");
            panel4.add(OBJ_74);
            OBJ_74.setBounds(15, 19, 45, 20);

            //---- ARG9A ----
            ARG9A.setComponentPopupMenu(BTD);
            ARG9A.setName("ARG9A");
            panel4.add(ARG9A);
            ARG9A.setBounds(60, 15, 20, ARG9A.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("Valeur");
            OBJ_75.setName("OBJ_75");
            panel4.add(OBJ_75);
            OBJ_75.setBounds(90, 19, 45, 20);

            //---- FLD011 ----
            FLD011.setComponentPopupMenu(BTD);
            FLD011.setName("FLD011");
            panel4.add(FLD011);
            FLD011.setBounds(135, 15, 20, FLD011.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Etat");
            OBJ_76.setName("OBJ_76");
            panel4.add(OBJ_76);
            OBJ_76.setBounds(165, 19, 35, 20);

            //---- ARG17A ----
            ARG17A.setModel(new DefaultComboBoxModel(new String[] {
              "Tous",
              "S\u00e9lectionn\u00e9s"
            }));
            ARG17A.setComponentPopupMenu(BTD);
            ARG17A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG17A.setName("ARG17A");
            panel4.add(ARG17A);
            ARG17A.setBounds(200, 16, 91, ARG17A.getPreferredSize().height);

            //---- SCAN17 ----
            SCAN17.setText("Exclus");
            SCAN17.setComponentPopupMenu(BTD);
            SCAN17.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN17.setName("SCAN17");
            panel4.add(SCAN17);
            SCAN17.setBounds(315, 19, 80, 20);

            //---- SCAN17_INCLUS ----
            SCAN17_INCLUS.setText("Inclus");
            SCAN17_INCLUS.setComponentPopupMenu(BTD);
            SCAN17_INCLUS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN17_INCLUS.setName("SCAN17_INCLUS");
            panel4.add(SCAN17_INCLUS);
            SCAN17_INCLUS.setBounds(315, 49, 75, 20);

            //---- ARG13A ----
            ARG13A.setComponentPopupMenu(BTD);
            ARG13A.setName("ARG13A");
            panel4.add(ARG13A);
            ARG13A.setBounds(200, 45, 20, ARG13A.getPreferredSize().height);

            //---- OBJ_80 ----
            OBJ_80.setText("GBA");
            OBJ_80.setName("OBJ_80");
            panel4.add(OBJ_80);
            OBJ_80.setBounds(165, 49, 35, 20);

            //---- ARG12A ----
            ARG12A.setComponentPopupMenu(BTD);
            ARG12A.setName("ARG12A");
            panel4.add(ARG12A);
            ARG12A.setBounds(135, 45, 20, ARG12A.getPreferredSize().height);

            //---- OBJ_78 ----
            OBJ_78.setText("Rgrp");
            OBJ_78.setName("OBJ_78");
            panel4.add(OBJ_78);
            OBJ_78.setBounds(90, 49, 45, 20);

            //---- FLD012 ----
            FLD012.setComponentPopupMenu(BTD);
            FLD012.setName("FLD012");
            panel4.add(FLD012);
            FLD012.setBounds(60, 45, 20, FLD012.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("Gratuit");
            OBJ_77.setName("OBJ_77");
            panel4.add(OBJ_77);
            OBJ_77.setBounds(15, 49, 45, 20);

            //---- OBJ_82 ----
            OBJ_82.setText("Nomenclature");
            OBJ_82.setName("OBJ_82");
            panel4.add(OBJ_82);
            OBJ_82.setBounds(15, 79, 110, 20);

            //---- ARG14A ----
            ARG14A.setComponentPopupMenu(BTD);
            ARG14A.setName("ARG14A");
            panel4.add(ARG14A);
            ARG14A.setBounds(135, 75, 20, ARG14A.getPreferredSize().height);

            //---- TIDX15 ----
            TIDX15.setText("Classement 2");
            TIDX15.setComponentPopupMenu(BTD);
            TIDX15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX15.setName("TIDX15");
            panel4.add(TIDX15);
            TIDX15.setBounds(15, 109, 109, 20);

            //---- ARG15A ----
            ARG15A.setComponentPopupMenu(BTD);
            ARG15A.setName("ARG15A");
            panel4.add(ARG15A);
            ARG15A.setBounds(135, 105, 210, ARG15A.getPreferredSize().height);

            //---- SCAN15 ----
            SCAN15.setText("");
            SCAN15.setToolTipText("Scan");
            SCAN15.setComponentPopupMenu(BTD);
            SCAN15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            SCAN15.setName("SCAN15");
            panel4.add(SCAN15);
            SCAN15.setBounds(355, 109, 24, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Dates"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- TIDX20 ----
            TIDX20.setText("Livraison pr\u00e9vue");
            TIDX20.setComponentPopupMenu(BTD);
            TIDX20.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX20.setName("TIDX20");
            panel2.add(TIDX20);
            TIDX20.setBounds(15, 59, 125, 20);

            //---- TIDX21 ----
            TIDX21.setText("Livr. souhait\u00e9e");
            TIDX21.setComponentPopupMenu(BTD);
            TIDX21.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX21.setName("TIDX21");
            panel2.add(TIDX21);
            TIDX21.setBounds(15, 89, 125, 20);

            //---- OBJ_48 ----
            OBJ_48.setText("Du");
            OBJ_48.setName("OBJ_48");
            panel2.add(OBJ_48);
            OBJ_48.setBounds(145, 35, 22, 13);

            //---- OBJ_49 ----
            OBJ_49.setText("au");
            OBJ_49.setName("OBJ_49");
            panel2.add(OBJ_49);
            OBJ_49.setBounds(265, 35, 22, 13);

            //---- ARG20D ----
            ARG20D.setName("ARG20D");
            panel2.add(ARG20D);
            ARG20D.setBounds(140, 55, 105, ARG20D.getPreferredSize().height);

            //---- PLA20D ----
            PLA20D.setName("PLA20D");
            panel2.add(PLA20D);
            PLA20D.setBounds(260, 55, 105, PLA20D.getPreferredSize().height);

            //---- ARG21D ----
            ARG21D.setName("ARG21D");
            panel2.add(ARG21D);
            ARG21D.setBounds(140, 85, 105, ARG21D.getPreferredSize().height);

            //---- PLA21D ----
            PLA21D.setName("PLA21D");
            panel2.add(PLA21D);
            PLA21D.setBounds(260, 85, 105, PLA21D.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Divers"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- TIDX22 ----
            TIDX22.setText("Prix VC HT");
            TIDX22.setComponentPopupMenu(BTD);
            TIDX22.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX22.setName("TIDX22");
            panel3.add(TIDX22);
            TIDX22.setBounds(25, 59, 105, 20);

            //---- ARG22N ----
            ARG22N.setComponentPopupMenu(BTD);
            ARG22N.setName("ARG22N");
            panel3.add(ARG22N);
            ARG22N.setBounds(135, 55, 90, ARG22N.getPreferredSize().height);

            //---- PLA22N ----
            PLA22N.setComponentPopupMenu(BTD);
            PLA22N.setName("PLA22N");
            panel3.add(PLA22N);
            PLA22N.setBounds(255, 55, 90, PLA22N.getPreferredSize().height);

            //---- PLA23N ----
            PLA23N.setComponentPopupMenu(BTD);
            PLA23N.setName("PLA23N");
            panel3.add(PLA23N);
            PLA23N.setBounds(255, 85, 90, PLA23N.getPreferredSize().height);

            //---- ARG23N ----
            ARG23N.setComponentPopupMenu(BTD);
            ARG23N.setName("ARG23N");
            panel3.add(ARG23N);
            ARG23N.setBounds(135, 85, 90, ARG23N.getPreferredSize().height);

            //---- TIDX23 ----
            TIDX23.setText("Montant HT");
            TIDX23.setComponentPopupMenu(BTD);
            TIDX23.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX23.setName("TIDX23");
            panel3.add(TIDX23);
            TIDX23.setBounds(25, 89, 105, 20);

            //---- TIDX24 ----
            TIDX24.setText("Remise");
            TIDX24.setComponentPopupMenu(BTD);
            TIDX24.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX24.setName("TIDX24");
            panel3.add(TIDX24);
            TIDX24.setBounds(25, 119, 105, 20);

            //---- ARG24N ----
            ARG24N.setComponentPopupMenu(BTD);
            ARG24N.setName("ARG24N");
            panel3.add(ARG24N);
            ARG24N.setBounds(135, 115, 40, ARG24N.getPreferredSize().height);

            //---- PLA24N ----
            PLA24N.setComponentPopupMenu(BTD);
            PLA24N.setName("PLA24N");
            panel3.add(PLA24N);
            PLA24N.setBounds(255, 115, 34, PLA24N.getPreferredSize().height);

            //---- PLA25N ----
            PLA25N.setComponentPopupMenu(BTD);
            PLA25N.setName("PLA25N");
            panel3.add(PLA25N);
            PLA25N.setBounds(255, 145, 74, PLA25N.getPreferredSize().height);

            //---- ARG25N ----
            ARG25N.setComponentPopupMenu(BTD);
            ARG25N.setName("ARG25N");
            panel3.add(ARG25N);
            ARG25N.setBounds(135, 145, 90, ARG25N.getPreferredSize().height);

            //---- TIDX25 ----
            TIDX25.setText("Quantit\u00e9");
            TIDX25.setComponentPopupMenu(BTD);
            TIDX25.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            TIDX25.setName("TIDX25");
            panel3.add(TIDX25);
            TIDX25.setBounds(25, 149, 105, 20);

            //---- OBJ_64 ----
            OBJ_64.setText("\u00e0");
            OBJ_64.setName("OBJ_64");
            panel3.add(OBJ_64);
            OBJ_64.setBounds(260, 35, 22, 13);

            //---- OBJ_63 ----
            OBJ_63.setText("De");
            OBJ_63.setName("OBJ_63");
            panel3.add(OBJ_63);
            OBJ_63.setBounds(140, 35, 22, 13);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 405, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 270, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 135, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                    .addGap(10, 10, 10)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 245, GroupLayout.PREFERRED_SIZE))))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //---- OBJ_44 ----
    OBJ_44.setText("Dates");
    OBJ_44.setName("OBJ_44");

    //---- OBJ_60 ----
    OBJ_60.setText("Divers");
    OBJ_60.setName("OBJ_60");

    //======== OBJ_36 ========
    {
      OBJ_36.setName("OBJ_36");
      OBJ_36.setLayout(null);

      //======== OBJ_35 ========
      {
        OBJ_35.setName("OBJ_35");
      }
      OBJ_36.add(OBJ_35);
      OBJ_35.setBounds(-10, 10, 715, 468);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < OBJ_36.getComponentCount(); i++) {
          Rectangle bounds = OBJ_36.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = OBJ_36.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        OBJ_36.setMinimumSize(preferredSize);
        OBJ_36.setPreferredSize(preferredSize);
      }
    }

    //---- RBC_GRP ----
    ButtonGroup RBC_GRP = new ButtonGroup();
    RBC_GRP.add(TIDX2);
    RBC_GRP.add(TIDX3);
    RBC_GRP.add(TIDX4);
    RBC_GRP.add(TIDX5);
    RBC_GRP.add(TIDX6);
    RBC_GRP.add(TIDX7);
    RBC_GRP.add(TIDX8);
    RBC_GRP.add(TIDX16);
    RBC_GRP.add(TIDX15);
    RBC_GRP.add(TIDX20);
    RBC_GRP.add(TIDX21);
    RBC_GRP.add(TIDX22);
    RBC_GRP.add(TIDX23);
    RBC_GRP.add(TIDX24);
    RBC_GRP.add(TIDX25);

    //---- SCAN17_GRP ----
    SCAN17_GRP.add(SCAN17);
    SCAN17_GRP.add(SCAN17_INCLUS);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_30;
  private XRiTextField INDNUM;
  private JLabel OBJ_31;
  private XRiTextField INDETB;
  private XRiTextField INDSUF;
  private XRiTextField INDNFA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton TIDX2;
  private XRiTextField ARG2N;
  private XRiRadioButton TIDX3;
  private XRiTextField ARG3A;
  private XRiRadioButton TIDX4;
  private XRiTextField ARG4A;
  private XRiTextField ARG4B;
  private XRiRadioButton TIDX5;
  private XRiTextField ARG5A;
  private XRiRadioButton TIDX6;
  private XRiTextField ARG6A;
  private XRiRadioButton TIDX7;
  private XRiTextField ARG7A;
  private XRiRadioButton TIDX8;
  private XRiTextField ARG8A;
  private XRiRadioButton TIDX16;
  private XRiTextField ARG16N2;
  private XRiTextField ARG16N1;
  private JPanel panel4;
  private JLabel OBJ_74;
  private XRiTextField ARG9A;
  private JLabel OBJ_75;
  private XRiTextField FLD011;
  private JLabel OBJ_76;
  private XRiComboBox ARG17A;
  private XRiRadioButton SCAN17;
  private XRiRadioButton SCAN17_INCLUS;
  private XRiTextField ARG13A;
  private JLabel OBJ_80;
  private XRiTextField ARG12A;
  private JLabel OBJ_78;
  private XRiTextField FLD012;
  private JLabel OBJ_77;
  private JLabel OBJ_82;
  private XRiTextField ARG14A;
  private XRiRadioButton TIDX15;
  private XRiTextField ARG15A;
  private XRiCheckBox SCAN15;
  private JPanel panel2;
  private XRiRadioButton TIDX20;
  private XRiRadioButton TIDX21;
  private JLabel OBJ_48;
  private JLabel OBJ_49;
  private XRiCalendrier ARG20D;
  private XRiCalendrier PLA20D;
  private XRiCalendrier ARG21D;
  private XRiCalendrier PLA21D;
  private JPanel panel3;
  private XRiRadioButton TIDX22;
  private XRiTextField ARG22N;
  private XRiTextField PLA22N;
  private XRiTextField PLA23N;
  private XRiTextField ARG23N;
  private XRiRadioButton TIDX23;
  private XRiRadioButton TIDX24;
  private XRiTextField ARG24N;
  private XRiTextField PLA24N;
  private XRiTextField PLA25N;
  private XRiTextField ARG25N;
  private XRiRadioButton TIDX25;
  private JLabel OBJ_64;
  private JLabel OBJ_63;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JLabel OBJ_44;
  private JLabel OBJ_60;
  private JPanel OBJ_36;
  private JTabbedPane OBJ_35;
  private ButtonGroup SCAN17_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
