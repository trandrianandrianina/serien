
package ri.serien.libecranrpg.rgvm.RGVM13FM;
// MON PACKAGE

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class RGVM13FM_LOG extends SNPanelEcranRPG implements ioFrame {
  // Variables
  
  /**
   * Constructeur.
   */
  public RGVM13FM_LOG(SNPanelEcranRPG parent) {
    super(parent);
    initComponents();
    setVersionLook(2);
    
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    initDiverses();
    
    // Titre
    setTitle("Critères de recherche logistique");
    
    
    bouton_valider.setIcon(lexique.chargerImage("images/valider_h.png", true));
    bouton_retour.setIcon(lexique.chargerImage("images/retour_p.png", true));
    
    setModal(true);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    this.setVisible(true);
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  public void valider() {
    getData();
    lexique.HostScreenSendKey(this, "Enter");
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    closePopupLinkWithBuffer(true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    valider();
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel3 = new JPanel();
    label4 = new JLabel();
    ARG47N = new XRiTextField();
    PLA47N = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    ARG48N = new XRiTextField();
    PLA48N = new XRiTextField();
    label5 = new JLabel();
    ARG49N = new XRiTextField();
    PLA49N = new XRiTextField();
    label6 = new JLabel();
    ARG50N = new XRiTextField();
    PLA50N = new XRiTextField();
    label7 = new JLabel();
    ARG51N = new XRiTextField();
    PLA51N = new XRiTextField();
    label8 = new JLabel();
    ARG52A = new XRiTextField();
    PLA52A = new XRiTextField();
    label9 = new JLabel();
    ARG53A = new XRiTextField();
    PLA53A = new XRiTextField();
    label10 = new JLabel();
    label11 = new JLabel();
    ARG54N = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(980, 200));
    setPreferredSize(new Dimension(595, 350));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(955, 215));
      p_principal.setMinimumSize(new Dimension(955, 215));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel3 ========
        {
          panel3.setBorder(new TitledBorder(""));
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);

          //---- label4 ----
          label4.setText("Ordre / tourn\u00e9e");
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(20, 80, 170, 28);

          //---- ARG47N ----
          ARG47N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG47N.setName("ARG47N");
          panel3.add(ARG47N);
          ARG47N.setBounds(190, 80, 60, ARG47N.getPreferredSize().height);

          //---- PLA47N ----
          PLA47N.setHorizontalAlignment(SwingConstants.RIGHT);
          PLA47N.setName("PLA47N");
          panel3.add(PLA47N);
          PLA47N.setBounds(280, 80, 60, PLA47N.getPreferredSize().height);

          //---- label1 ----
          label1.setText("De");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(190, 55, 60, 28);

          //---- label2 ----
          label2.setText("\u00e0");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(280, 55, 60, 28);

          //---- ARG48N ----
          ARG48N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG48N.setName("ARG48N");
          panel3.add(ARG48N);
          ARG48N.setBounds(190, 113, 60, ARG48N.getPreferredSize().height);

          //---- PLA48N ----
          PLA48N.setHorizontalAlignment(SwingConstants.RIGHT);
          PLA48N.setName("PLA48N");
          panel3.add(PLA48N);
          PLA48N.setBounds(280, 113, 60, PLA48N.getPreferredSize().height);

          //---- label5 ----
          label5.setText("Volume maximum");
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(20, 113, 170, 28);

          //---- ARG49N ----
          ARG49N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG49N.setName("ARG49N");
          panel3.add(ARG49N);
          ARG49N.setBounds(190, 146, 60, ARG49N.getPreferredSize().height);

          //---- PLA49N ----
          PLA49N.setHorizontalAlignment(SwingConstants.RIGHT);
          PLA49N.setName("PLA49N");
          panel3.add(PLA49N);
          PLA49N.setBounds(280, 146, 60, PLA49N.getPreferredSize().height);

          //---- label6 ----
          label6.setText("Poids maximum");
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(20, 146, 170, 28);

          //---- ARG50N ----
          ARG50N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG50N.setName("ARG50N");
          panel3.add(ARG50N);
          ARG50N.setBounds(190, 179, 60, ARG50N.getPreferredSize().height);

          //---- PLA50N ----
          PLA50N.setHorizontalAlignment(SwingConstants.RIGHT);
          PLA50N.setName("PLA50N");
          panel3.add(PLA50N);
          PLA50N.setBounds(280, 179, 60, PLA50N.getPreferredSize().height);

          //---- label7 ----
          label7.setText("m\u00b2 au plancher maximum");
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(20, 179, 170, 28);

          //---- ARG51N ----
          ARG51N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG51N.setName("ARG51N");
          panel3.add(ARG51N);
          ARG51N.setBounds(190, 212, 60, ARG51N.getPreferredSize().height);

          //---- PLA51N ----
          PLA51N.setHorizontalAlignment(SwingConstants.RIGHT);
          PLA51N.setName("PLA51N");
          panel3.add(PLA51N);
          PLA51N.setBounds(280, 212, 60, PLA51N.getPreferredSize().height);

          //---- label8 ----
          label8.setText("Longueur maximum");
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(20, 212, 170, 28);

          //---- ARG52A ----
          ARG52A.setComponentPopupMenu(BTD);
          ARG52A.setName("ARG52A");
          panel3.add(ARG52A);
          ARG52A.setBounds(190, 245, 40, ARG52A.getPreferredSize().height);

          //---- PLA52A ----
          PLA52A.setComponentPopupMenu(BTD);
          PLA52A.setName("PLA52A");
          panel3.add(PLA52A);
          PLA52A.setBounds(280, 245, 40, PLA52A.getPreferredSize().height);

          //---- label9 ----
          label9.setText("Cat\u00e9gorie");
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(20, 245, 170, 28);

          //---- ARG53A ----
          ARG53A.setName("ARG53A");
          panel3.add(ARG53A);
          ARG53A.setBounds(190, 278, 24, ARG53A.getPreferredSize().height);

          //---- PLA53A ----
          PLA53A.setName("PLA53A");
          panel3.add(PLA53A);
          PLA53A.setBounds(280, 278, 24, PLA53A.getPreferredSize().height);

          //---- label10 ----
          label10.setText("Code s\u00e9lection");
          label10.setName("label10");
          panel3.add(label10);
          label10.setBounds(20, 278, 170, 28);

          //---- label11 ----
          label11.setText("N\u00b0de pr\u00e9paration ou tourn\u00e9e");
          label11.setName("label11");
          panel3.add(label11);
          label11.setBounds(20, 15, 170, 28);

          //---- ARG54N ----
          ARG54N.setHorizontalAlignment(SwingConstants.RIGHT);
          ARG54N.setName("ARG54N");
          panel3.add(ARG54N);
          ARG54N.setBounds(190, 15, 60, ARG54N.getPreferredSize().height);
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 405, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(panel3, GroupLayout.DEFAULT_SIZE, 328, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel3;
  private JLabel label4;
  private XRiTextField ARG47N;
  private XRiTextField PLA47N;
  private JLabel label1;
  private JLabel label2;
  private XRiTextField ARG48N;
  private XRiTextField PLA48N;
  private JLabel label5;
  private XRiTextField ARG49N;
  private XRiTextField PLA49N;
  private JLabel label6;
  private XRiTextField ARG50N;
  private XRiTextField PLA50N;
  private JLabel label7;
  private XRiTextField ARG51N;
  private XRiTextField PLA51N;
  private JLabel label8;
  private XRiTextField ARG52A;
  private XRiTextField PLA52A;
  private JLabel label9;
  private XRiTextField ARG53A;
  private XRiTextField PLA53A;
  private JLabel label10;
  private JLabel label11;
  private XRiTextField ARG54N;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  // JFormDesigner - End of variables declaration //GEN-END:variables
  
}
