
package ri.serien.libecranrpg.rgvm.RGVM07FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiRadioButton;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;
import ri.serien.libswing.moteur.interpreteur.Lexical;

/**
 * @author Stéphane Vénéri
 */
public class RGVM07FM_A0 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ARG2A_Value = { "?", " ", "Q", "D", "F", "N", "H", "A" };
  private String[] ARG4A_Value = { "A", "T", "F", "S", "G", "*", "R", "L", "1", "2", };
  private String[] ARG4A_Text = { "Article", "Tarif", "Famille", "Sous-famille", "Groupe", "Emboîtage ou général", "Ensemble d'articles",
      "Num de ligne (rang)", "Regroupement achat", "Fournisseur", };
  private String[] ARG4A2_Value = { "A", "T", "3", "4", "5", "*", "R", "L", "1", "2", };
  
  public RGVM07FM_A0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    ARG2A.setValeurs(ARG2A_Value, null);
    ARG4A.setValeurs(ARG4A_Value, ARG4A_Text);
    WTCN.setValeurs("C", WTCN_GRP);
    WTCN_CN.setValeurs(" ");
    SCAN2.setValeurs("E", SCAN2_GRP);
    SCAN2_B.setValeurs("N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    if (lexique.HostFieldGetData("WTCN").trim().equalsIgnoreCase("")) {
      WTCN_CN.setSelected(true);
    }
    
    ARG2A.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        SCAN2.setVisible(ARG2A.getSelectedIndex() != 0);
        SCAN2_B.setVisible(ARG2A.getSelectedIndex() != 0);
        if (ARG2A.getSelectedIndex() == 0) {
          lexique.HostFieldPutData("SCAN2", 0, "E");
        }
      }
    });
    
    SCAN2.setVisible(lexique.getMode() != Lexical.MODE_CREATION);
    SCAN2_B.setVisible(lexique.getMode() != Lexical.MODE_CREATION);
    
    if (lexique.HostFieldGetData("WTCN").equalsIgnoreCase("C")) {
      OBJ_57.setText("Code client");
    }
    else {
      OBJ_57.setText("Code condition");
    }
    
    // Titre
    setTitle("FM@LOCGRP/+1/@ - CONDITIONS DE VENTES");
    
    

    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    if (ARG4A2.getSelectedIndex() > 0) {
      lexique.HostFieldPutData("ARG4A", 0, ARG4A2_Value[ARG4A2.getSelectedIndex()]);
    }
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void WTCN_OBJ_49ActionPerformed(ActionEvent e) {
    OBJ_57.setText("Code client");
  }
  
  private void WTCN1ActionPerformed(ActionEvent e) {
    OBJ_57.setText("Code condition");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WTCN = new XRiRadioButton();
    WTCN_CN = new XRiRadioButton();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_46 = new JLabel();
    ARG4A = new XRiComboBox();
    OBJ_47 = new JLabel();
    ARG5A = new XRiTextField();
    xTitledSeparator2 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    OBJ_57 = new JLabel();
    ARG3A = new XRiTextField();
    ARG2A = new XRiComboBox();
    OBJ_48 = new JLabel();
    SCAN2 = new XRiRadioButton();
    SCAN2_B = new XRiRadioButton();
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_54 = new JLabel();
    ARG4A2 = new JComboBox();
    WTCN_GRP = new ButtonGroup();
    SCAN2_GRP = new ButtonGroup();

    //======== this ========
    setMinimumSize(new Dimension(900, 680));
    setPreferredSize(new Dimension(900, 680));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Conditions de ventes");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(null);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Suppression");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mode expert/assistant");
              riSousMenu_bt6.setToolTipText("Mode expert / Mode assistant");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(510, 400));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Cr\u00e9ation d'une condition"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WTCN ----
            WTCN.setText("Conditions client");
            WTCN.setComponentPopupMenu(BTD);
            WTCN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTCN.setName("WTCN");
            WTCN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTCN_OBJ_49ActionPerformed(e);
              }
            });
            panel1.add(WTCN);
            WTCN.setBounds(39, 61, 122, 26);

            //---- WTCN_CN ----
            WTCN_CN.setText("Conditions param\u00e8tre 'CN'");
            WTCN_CN.setComponentPopupMenu(BTD);
            WTCN_CN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WTCN_CN.setName("WTCN_CN");
            WTCN_CN.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                WTCN1ActionPerformed(e);
              }
            });
            panel1.add(WTCN_CN);
            WTCN_CN.setBounds(39, 91, 181, 26);

            //---- xTitledSeparator1 ----
            xTitledSeparator1.setTitle("");
            xTitledSeparator1.setName("xTitledSeparator1");
            panel1.add(xTitledSeparator1);
            xTitledSeparator1.setBounds(20, 161, 425, xTitledSeparator1.getPreferredSize().height);

            //---- OBJ_46 ----
            OBJ_46.setText("Type de rattachement");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(34, 191, 135, 26);

            //---- ARG4A ----
            ARG4A.setComponentPopupMenu(BTD);
            ARG4A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG4A.setName("ARG4A");
            panel1.add(ARG4A);
            ARG4A.setBounds(229, 191, 210, ARG4A.getPreferredSize().height);

            //---- OBJ_47 ----
            OBJ_47.setText("Rattachement \u00e0");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(34, 232, 104, 26);

            //---- ARG5A ----
            ARG5A.setComponentPopupMenu(BTD);
            ARG5A.setName("ARG5A");
            panel1.add(ARG5A);
            ARG5A.setBounds(229, 231, 210, ARG5A.getPreferredSize().height);

            //---- xTitledSeparator2 ----
            xTitledSeparator2.setTitle("");
            xTitledSeparator2.setName("xTitledSeparator2");
            panel1.add(xTitledSeparator2);
            xTitledSeparator2.setBounds(20, 271, 425, xTitledSeparator2.getPreferredSize().height);

            //======== P_SEL0 ========
            {
              P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
              P_SEL0.setOpaque(false);
              P_SEL0.setName("P_SEL0");
              P_SEL0.setLayout(null);

              //---- OBJ_57 ----
              OBJ_57.setText("Code client");
              OBJ_57.setName("OBJ_57");
              P_SEL0.add(OBJ_57);
              OBJ_57.setBounds(15, 10, 100, 28);

              //---- ARG3A ----
              ARG3A.setComponentPopupMenu(BTD);
              ARG3A.setHorizontalAlignment(SwingConstants.RIGHT);
              ARG3A.setName("ARG3A");
              P_SEL0.add(ARG3A);
              ARG3A.setBounds(120, 10, 58, ARG3A.getPreferredSize().height);
            }
            panel1.add(P_SEL0);
            P_SEL0.setBounds(229, 61, 210, 50);

            //---- ARG2A ----
            ARG2A.setModel(new DefaultComboBoxModel(new String[] {
              "Toutes cat\u00e9gories",
              "Conditions normales",
              "Conditions quantitatives",
              "D\u00e9rogations",
              "Conditions flash",
              "Conditions n\u00e9goci\u00e9es",
              "Chantiers",
              "Affaires"
            }));
            ARG2A.setComponentPopupMenu(BTD);
            ARG2A.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            ARG2A.setName("ARG2A");
            panel1.add(ARG2A);
            ARG2A.setBounds(229, 285, 210, ARG2A.getPreferredSize().height);

            //---- OBJ_48 ----
            OBJ_48.setText("Cat\u00e9gorie");
            OBJ_48.setName("OBJ_48");
            panel1.add(OBJ_48);
            OBJ_48.setBounds(38, 285, 80, 26);

            //---- SCAN2 ----
            SCAN2.setText("Inclus");
            SCAN2.setName("SCAN2");
            panel1.add(SCAN2);
            SCAN2.setBounds(229, 320, 71, 26);

            //---- SCAN2_B ----
            SCAN2_B.setText("Exclus");
            SCAN2_B.setName("SCAN2_B");
            panel1.add(SCAN2_B);
            SCAN2_B.setBounds(305, 320, 75, 26);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 484, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 372, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Annuler");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }

    //---- OBJ_54 ----
    OBJ_54.setText("Cr\u00e9ation d'une condition");
    OBJ_54.setName("OBJ_54");

    //---- ARG4A2 ----
    ARG4A2.setComponentPopupMenu(BTD);
    ARG4A2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    ARG4A2.setName("ARG4A2");

    //---- WTCN_GRP ----
    WTCN_GRP.add(WTCN);
    WTCN_GRP.add(WTCN_CN);

    //---- SCAN2_GRP ----
    SCAN2_GRP.add(SCAN2);
    SCAN2_GRP.add(SCAN2_B);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiRadioButton WTCN;
  private XRiRadioButton WTCN_CN;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel OBJ_46;
  private XRiComboBox ARG4A;
  private JLabel OBJ_47;
  private XRiTextField ARG5A;
  private JXTitledSeparator xTitledSeparator2;
  private JPanel P_SEL0;
  private JLabel OBJ_57;
  private XRiTextField ARG3A;
  private XRiComboBox ARG2A;
  private JLabel OBJ_48;
  private XRiRadioButton SCAN2;
  private XRiRadioButton SCAN2_B;
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  private JLabel OBJ_54;
  private JComboBox ARG4A2;
  private ButtonGroup WTCN_GRP;
  private ButtonGroup SCAN2_GRP;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
