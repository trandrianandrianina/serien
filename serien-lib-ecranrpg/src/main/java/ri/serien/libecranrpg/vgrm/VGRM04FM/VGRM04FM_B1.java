
package ri.serien.libecranrpg.vgrm.VGRM04FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGRM04FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] GLT02_Value = { "M", "C", };
  private String[] GLT01_Value = { "M", "C", };
  
  public VGRM04FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    GLT02.setValeurs(GLT02_Value, null);
    GLT01.setValeurs(GLT01_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDGRI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDGRI@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    GRPO10.setVisible(lexique.isPresent("GRPO10"));
    GRLI10.setVisible(lexique.isPresent("GRLI10"));
    GRPO09.setVisible(lexique.isPresent("GRPO09"));
    GRLI09.setVisible(lexique.isPresent("GRLI09"));
    GRPO08.setVisible(lexique.isPresent("GRPO08"));
    GRLI08.setVisible(lexique.isPresent("GRLI08"));
    GRPO07.setVisible(lexique.isPresent("GRPO07"));
    GRLI07.setVisible(lexique.isPresent("GRLI07"));
    GRPO06.setVisible(lexique.isPresent("GRPO06"));
    GRLI06.setVisible(lexique.isPresent("GRLI06"));
    GRPO05.setEnabled(lexique.isPresent("GRPO05"));
    GRLI05.setVisible(lexique.isPresent("GRLI05"));
    GRPO04.setVisible(lexique.isPresent("GRPO04"));
    GRLI04.setVisible(lexique.isPresent("GRLI04"));
    GRPO03.setVisible(lexique.isPresent("GRPO03"));
    GRLI03.setVisible(lexique.isPresent("GRLI03"));
    GRPO02.setVisible(lexique.isPresent("GRPO02"));
    GRLI02.setVisible(lexique.isPresent("GRLI02"));
    GRPO01.setVisible(lexique.isPresent("GRPO01"));
    GRLI01.setVisible(lexique.isPresent("GRLI01"));
    GLIB.setEnabled(lexique.isPresent("GLIB"));


    OBJ_43.setVisible(lexique.isPresent("GLT01"));
    OBJ_47.setVisible(lexique.isPresent("GLT02"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("GLT02", 0, GLT02_Value[GLT02.getSelectedIndex()]);
    // lexique.HostFieldPutData("GLT01", 0, GLT01_Value[GLT01.getSelectedIndex()]);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_28 = new JLabel();
    INDGRI = new RiZoneSortie();
    OBJ_32 = new JLabel();
    INDETB = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    GLT01 = new XRiComboBox();
    GLT02 = new XRiComboBox();
    GLIB = new XRiTextField();
    OBJ_43 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_46 = new JLabel();
    OBJ_39 = new JLabel();
    GRLI01 = new XRiTextField();
    GRPO01 = new XRiTextField();
    GRLI02 = new XRiTextField();
    GRPO02 = new XRiTextField();
    GRLI03 = new XRiTextField();
    GRPO03 = new XRiTextField();
    GRLI04 = new XRiTextField();
    GRPO04 = new XRiTextField();
    GRLI05 = new XRiTextField();
    GRPO05 = new XRiTextField();
    GRLI06 = new XRiTextField();
    GRPO06 = new XRiTextField();
    GRLI07 = new XRiTextField();
    GRPO07 = new XRiTextField();
    GRLI08 = new XRiTextField();
    GRPO08 = new XRiTextField();
    GRLI09 = new XRiTextField();
    GRPO09 = new XRiTextField();
    GRLI10 = new XRiTextField();
    GRPO10 = new XRiTextField();
    label1 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Grille de commissions");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_28 ----
          OBJ_28.setText("Etablissement");
          OBJ_28.setName("OBJ_28");
          p_tete_gauche.add(OBJ_28);
          OBJ_28.setBounds(5, 3, 95, 18);

          //---- INDGRI ----
          INDGRI.setComponentPopupMenu(BTD);
          INDGRI.setOpaque(false);
          INDGRI.setText("@INDGRI@");
          INDGRI.setName("INDGRI");
          p_tete_gauche.add(INDGRI);
          INDGRI.setBounds(215, 0, 60, INDGRI.getPreferredSize().height);

          //---- OBJ_32 ----
          OBJ_32.setText("Grille");
          OBJ_32.setName("OBJ_32");
          p_tete_gauche.add(OBJ_32);
          OBJ_32.setBounds(170, 3, 45, 18);

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");
          p_tete_gauche.add(INDETB);
          INDETB.setBounds(100, 0, 40, INDETB.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 250));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(510, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Tableau r\u00e9capitulatif"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- GLT01 ----
            GLT01.setModel(new DefaultComboBoxModel(new String[] {
              "Marge",
              "Chiffre d'affaire"
            }));
            GLT01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GLT01.setName("GLT01");
            panel1.add(GLT01);
            GLT01.setBounds(295, 111, 102, GLT01.getPreferredSize().height);

            //---- GLT02 ----
            GLT02.setModel(new DefaultComboBoxModel(new String[] {
              "Marge",
              "Chiffre d'affaire"
            }));
            GLT02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            GLT02.setName("GLT02");
            panel1.add(GLT02);
            GLT02.setBounds(295, 141, 102, GLT02.getPreferredSize().height);

            //---- GLIB ----
            GLIB.setComponentPopupMenu(BTD);
            GLIB.setName("GLIB");
            panel1.add(GLIB);
            GLIB.setBounds(85, 40, 310, GLIB.getPreferredSize().height);

            //---- OBJ_43 ----
            OBJ_43.setText("Ventes au prix de base");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(25, 115, 150, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("Ventes sup\u00e9rieures au prix de base");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(25, 144, 235, 20);

            //---- OBJ_46 ----
            OBJ_46.setText("% commission");
            OBJ_46.setName("OBJ_46");
            panel1.add(OBJ_46);
            OBJ_46.setBounds(215, 85, 109, 20);

            //---- OBJ_39 ----
            OBJ_39.setText("Libell\u00e9");
            OBJ_39.setName("OBJ_39");
            panel1.add(OBJ_39);
            OBJ_39.setBounds(25, 44, 55, 20);

            //---- GRLI01 ----
            GRLI01.setComponentPopupMenu(BTD);
            GRLI01.setName("GRLI01");
            panel1.add(GRLI01);
            GRLI01.setBounds(85, 110, 50, GRLI01.getPreferredSize().height);

            //---- GRPO01 ----
            GRPO01.setComponentPopupMenu(BTD);
            GRPO01.setName("GRPO01");
            panel1.add(GRPO01);
            GRPO01.setBounds(235, 110, 50, GRPO01.getPreferredSize().height);

            //---- GRLI02 ----
            GRLI02.setComponentPopupMenu(BTD);
            GRLI02.setName("GRLI02");
            panel1.add(GRLI02);
            GRLI02.setBounds(85, 140, 50, GRLI02.getPreferredSize().height);

            //---- GRPO02 ----
            GRPO02.setComponentPopupMenu(BTD);
            GRPO02.setName("GRPO02");
            panel1.add(GRPO02);
            GRPO02.setBounds(235, 140, 50, GRPO02.getPreferredSize().height);

            //---- GRLI03 ----
            GRLI03.setComponentPopupMenu(BTD);
            GRLI03.setName("GRLI03");
            panel1.add(GRLI03);
            GRLI03.setBounds(85, 170, 50, GRLI03.getPreferredSize().height);

            //---- GRPO03 ----
            GRPO03.setComponentPopupMenu(BTD);
            GRPO03.setName("GRPO03");
            panel1.add(GRPO03);
            GRPO03.setBounds(235, 170, 50, GRPO03.getPreferredSize().height);

            //---- GRLI04 ----
            GRLI04.setComponentPopupMenu(BTD);
            GRLI04.setName("GRLI04");
            panel1.add(GRLI04);
            GRLI04.setBounds(85, 200, 50, GRLI04.getPreferredSize().height);

            //---- GRPO04 ----
            GRPO04.setComponentPopupMenu(BTD);
            GRPO04.setName("GRPO04");
            panel1.add(GRPO04);
            GRPO04.setBounds(235, 200, 50, GRPO04.getPreferredSize().height);

            //---- GRLI05 ----
            GRLI05.setComponentPopupMenu(BTD);
            GRLI05.setName("GRLI05");
            panel1.add(GRLI05);
            GRLI05.setBounds(85, 230, 50, GRLI05.getPreferredSize().height);

            //---- GRPO05 ----
            GRPO05.setComponentPopupMenu(BTD);
            GRPO05.setName("GRPO05");
            panel1.add(GRPO05);
            GRPO05.setBounds(235, 230, 50, GRPO05.getPreferredSize().height);

            //---- GRLI06 ----
            GRLI06.setComponentPopupMenu(BTD);
            GRLI06.setName("GRLI06");
            panel1.add(GRLI06);
            GRLI06.setBounds(85, 260, 50, GRLI06.getPreferredSize().height);

            //---- GRPO06 ----
            GRPO06.setComponentPopupMenu(BTD);
            GRPO06.setName("GRPO06");
            panel1.add(GRPO06);
            GRPO06.setBounds(235, 260, 50, GRPO06.getPreferredSize().height);

            //---- GRLI07 ----
            GRLI07.setComponentPopupMenu(BTD);
            GRLI07.setName("GRLI07");
            panel1.add(GRLI07);
            GRLI07.setBounds(85, 290, 50, GRLI07.getPreferredSize().height);

            //---- GRPO07 ----
            GRPO07.setComponentPopupMenu(BTD);
            GRPO07.setName("GRPO07");
            panel1.add(GRPO07);
            GRPO07.setBounds(235, 290, 50, GRPO07.getPreferredSize().height);

            //---- GRLI08 ----
            GRLI08.setComponentPopupMenu(BTD);
            GRLI08.setName("GRLI08");
            panel1.add(GRLI08);
            GRLI08.setBounds(85, 320, 50, GRLI08.getPreferredSize().height);

            //---- GRPO08 ----
            GRPO08.setComponentPopupMenu(BTD);
            GRPO08.setName("GRPO08");
            panel1.add(GRPO08);
            GRPO08.setBounds(235, 320, 50, GRPO08.getPreferredSize().height);

            //---- GRLI09 ----
            GRLI09.setComponentPopupMenu(BTD);
            GRLI09.setName("GRLI09");
            panel1.add(GRLI09);
            GRLI09.setBounds(85, 350, 50, GRLI09.getPreferredSize().height);

            //---- GRPO09 ----
            GRPO09.setComponentPopupMenu(BTD);
            GRPO09.setName("GRPO09");
            panel1.add(GRPO09);
            GRPO09.setBounds(235, 350, 50, GRPO09.getPreferredSize().height);

            //---- GRLI10 ----
            GRLI10.setComponentPopupMenu(BTD);
            GRLI10.setName("GRLI10");
            panel1.add(GRLI10);
            GRLI10.setBounds(85, 380, 50, GRLI10.getPreferredSize().height);

            //---- GRPO10 ----
            GRPO10.setComponentPopupMenu(BTD);
            GRPO10.setName("GRPO10");
            panel1.add(GRPO10);
            GRPO10.setBounds(235, 380, 50, GRPO10.getPreferredSize().height);

            //---- label1 ----
            label1.setText("% de remises jusqu'\u00e0");
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(25, 85, 180, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 431, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(39, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 438, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(47, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_28;
  private RiZoneSortie INDGRI;
  private JLabel OBJ_32;
  private RiZoneSortie INDETB;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiComboBox GLT01;
  private XRiComboBox GLT02;
  private XRiTextField GLIB;
  private JLabel OBJ_43;
  private JLabel OBJ_47;
  private JLabel OBJ_46;
  private JLabel OBJ_39;
  private XRiTextField GRLI01;
  private XRiTextField GRPO01;
  private XRiTextField GRLI02;
  private XRiTextField GRPO02;
  private XRiTextField GRLI03;
  private XRiTextField GRPO03;
  private XRiTextField GRLI04;
  private XRiTextField GRPO04;
  private XRiTextField GRLI05;
  private XRiTextField GRPO05;
  private XRiTextField GRLI06;
  private XRiTextField GRPO06;
  private XRiTextField GRLI07;
  private XRiTextField GRPO07;
  private XRiTextField GRLI08;
  private XRiTextField GRPO08;
  private XRiTextField GRLI09;
  private XRiTextField GRPO09;
  private XRiTextField GRLI10;
  private XRiTextField GRPO10;
  private JLabel label1;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
