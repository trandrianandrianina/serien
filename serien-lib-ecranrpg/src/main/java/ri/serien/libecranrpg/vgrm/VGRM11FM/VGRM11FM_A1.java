
package ri.serien.libecranrpg.vgrm.VGRM11FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGRM11FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] C1TRA_Value = { "", "A", "G", "F", "T", "R" };
  
  public VGRM11FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    C1TRA.setValeurs(C1TRA_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ELIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELIB@")).trim());
    C1RAT.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRAT@")).trim());
    LRP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRP1@")).trim());
    LRP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRP2@")).trim());
    LRP3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LRP3@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Rattachement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    ELIB = new RiZoneSortie();
    C1PC11 = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_45 = new JLabel();
    C1TRA = new XRiComboBox();
    OBJ_46 = new JLabel();
    C1RAT = new XRiTextField();
    C1PC21 = new XRiTextField();
    C1GR11 = new XRiTextField();
    C1GR21 = new XRiTextField();
    OBJ_22 = new JLabel();
    LRP1 = new RiZoneSortie();
    LRP2 = new RiZoneSortie();
    LRP3 = new RiZoneSortie();
    C1TB1 = new XRiTextField();
    OBJ_23 = new JLabel();
    C1PC31 = new XRiTextField();
    C1GR31 = new XRiTextField();
    C1TB2 = new XRiTextField();
    C1TB3 = new XRiTextField();
    OBJ_24 = new JLabel();
    C1PC12 = new XRiTextField();
    C1PC22 = new XRiTextField();
    C1PC32 = new XRiTextField();
    C1GR32 = new XRiTextField();
    C1GR22 = new XRiTextField();
    C1GR12 = new XRiTextField();
    OBJ_25 = new JLabel();
    C1RP1 = new XRiTextField();
    OBJ_26 = new JLabel();
    C1RP2 = new XRiTextField();
    C1RP3 = new XRiTextField();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(855, 300));
    setPreferredSize(new Dimension(855, 300));
    setMaximumSize(new Dimension(855, 300));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- ELIB ----
          ELIB.setText("@ELIB@");
          ELIB.setName("ELIB");
          p_recup.add(ELIB);
          ELIB.setBounds(187, 80, 310, ELIB.getPreferredSize().height);

          //---- C1PC11 ----
          C1PC11.setComponentPopupMenu(BTD);
          C1PC11.setName("C1PC11");
          p_recup.add(C1PC11);
          C1PC11.setBounds(450, 168, 60, C1PC11.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Pourcentage");
          OBJ_21.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_21.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(443, 145, 75, 24);

          //---- OBJ_45 ----
          OBJ_45.setText("Type de rattachement");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(26, 19, 149, 28);

          //---- C1TRA ----
          C1TRA.setModel(new DefaultComboBoxModel(new String[] {
            "Tous",
            "Article",
            "Groupe",
            "Famille",
            "Tarif",
            "Rattachement CNV"
          }));
          C1TRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          C1TRA.setName("C1TRA");
          p_recup.add(C1TRA);
          C1TRA.setBounds(187, 20, 210, C1TRA.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Rattachement");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(26, 49, 149, 28);

          //---- C1RAT ----
          C1RAT.setToolTipText("@LIBRAT@");
          C1RAT.setComponentPopupMenu(BTD);
          C1RAT.setName("C1RAT");
          p_recup.add(C1RAT);
          C1RAT.setBounds(187, 49, 210, C1RAT.getPreferredSize().height);

          //---- C1PC21 ----
          C1PC21.setComponentPopupMenu(BTD);
          C1PC21.setName("C1PC21");
          p_recup.add(C1PC21);
          C1PC21.setBounds(450, 198, 60, C1PC21.getPreferredSize().height);

          //---- C1GR11 ----
          C1GR11.setComponentPopupMenu(BTD);
          C1GR11.setName("C1GR11");
          p_recup.add(C1GR11);
          C1GR11.setBounds(525, 168, 60, C1GR11.getPreferredSize().height);

          //---- C1GR21 ----
          C1GR21.setComponentPopupMenu(BTD);
          C1GR21.setName("C1GR21");
          p_recup.add(C1GR21);
          C1GR21.setBounds(525, 198, 60, C1GR21.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Grille");
          OBJ_22.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_22.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(523, 145, 65, 24);

          //---- LRP1 ----
          LRP1.setText("@LRP1@");
          LRP1.setName("LRP1");
          p_recup.add(LRP1);
          LRP1.setBounds(75, 170, 310, LRP1.getPreferredSize().height);

          //---- LRP2 ----
          LRP2.setText("@LRP2@");
          LRP2.setName("LRP2");
          p_recup.add(LRP2);
          LRP2.setBounds(75, 200, 310, LRP2.getPreferredSize().height);

          //---- LRP3 ----
          LRP3.setText("@LRP3@");
          LRP3.setName("LRP3");
          p_recup.add(LRP3);
          LRP3.setBounds(75, 230, 310, LRP3.getPreferredSize().height);

          //---- C1TB1 ----
          C1TB1.setComponentPopupMenu(BTD);
          C1TB1.setName("C1TB1");
          p_recup.add(C1TB1);
          C1TB1.setBounds(400, 168, 35, C1TB1.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("Base");
          OBJ_23.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(400, 145, 30, 24);

          //---- C1PC31 ----
          C1PC31.setComponentPopupMenu(BTD);
          C1PC31.setName("C1PC31");
          p_recup.add(C1PC31);
          C1PC31.setBounds(450, 228, 60, C1PC31.getPreferredSize().height);

          //---- C1GR31 ----
          C1GR31.setComponentPopupMenu(BTD);
          C1GR31.setName("C1GR31");
          p_recup.add(C1GR31);
          C1GR31.setBounds(525, 228, 60, C1GR31.getPreferredSize().height);

          //---- C1TB2 ----
          C1TB2.setComponentPopupMenu(BTD);
          C1TB2.setName("C1TB2");
          p_recup.add(C1TB2);
          C1TB2.setBounds(400, 198, 35, C1TB2.getPreferredSize().height);

          //---- C1TB3 ----
          C1TB3.setComponentPopupMenu(BTD);
          C1TB3.setName("C1TB3");
          p_recup.add(C1TB3);
          C1TB3.setBounds(400, 228, 35, C1TB3.getPreferredSize().height);

          //---- OBJ_24 ----
          OBJ_24.setText("Pourcentage");
          OBJ_24.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_24.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(598, 145, 75, 24);

          //---- C1PC12 ----
          C1PC12.setComponentPopupMenu(BTD);
          C1PC12.setName("C1PC12");
          p_recup.add(C1PC12);
          C1PC12.setBounds(605, 168, 60, C1PC12.getPreferredSize().height);

          //---- C1PC22 ----
          C1PC22.setComponentPopupMenu(BTD);
          C1PC22.setName("C1PC22");
          p_recup.add(C1PC22);
          C1PC22.setBounds(605, 198, 60, C1PC22.getPreferredSize().height);

          //---- C1PC32 ----
          C1PC32.setComponentPopupMenu(BTD);
          C1PC32.setName("C1PC32");
          p_recup.add(C1PC32);
          C1PC32.setBounds(605, 228, 60, C1PC32.getPreferredSize().height);

          //---- C1GR32 ----
          C1GR32.setComponentPopupMenu(BTD);
          C1GR32.setName("C1GR32");
          p_recup.add(C1GR32);
          C1GR32.setBounds(680, 228, 60, C1GR32.getPreferredSize().height);

          //---- C1GR22 ----
          C1GR22.setComponentPopupMenu(BTD);
          C1GR22.setName("C1GR22");
          p_recup.add(C1GR22);
          C1GR22.setBounds(680, 198, 60, C1GR22.getPreferredSize().height);

          //---- C1GR12 ----
          C1GR12.setComponentPopupMenu(BTD);
          C1GR12.setName("C1GR12");
          p_recup.add(C1GR12);
          C1GR12.setBounds(680, 168, 60, C1GR12.getPreferredSize().height);

          //---- OBJ_25 ----
          OBJ_25.setText("Grille");
          OBJ_25.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_25.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_25.setName("OBJ_25");
          p_recup.add(OBJ_25);
          OBJ_25.setBounds(678, 145, 65, 24);

          //---- C1RP1 ----
          C1RP1.setComponentPopupMenu(BTD);
          C1RP1.setName("C1RP1");
          p_recup.add(C1RP1);
          C1RP1.setBounds(25, 168, 35, C1RP1.getPreferredSize().height);

          //---- OBJ_26 ----
          OBJ_26.setText("Repr\u00e9sentant");
          OBJ_26.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_26.setName("OBJ_26");
          p_recup.add(OBJ_26);
          OBJ_26.setBounds(25, 140, 165, 24);

          //---- C1RP2 ----
          C1RP2.setComponentPopupMenu(BTD);
          C1RP2.setName("C1RP2");
          p_recup.add(C1RP2);
          C1RP2.setBounds(25, 198, 35, C1RP2.getPreferredSize().height);

          //---- C1RP3 ----
          C1RP3.setComponentPopupMenu(BTD);
          C1RP3.setName("C1RP3");
          p_recup.add(C1RP3);
          C1RP3.setBounds(25, 228, 35, C1RP3.getPreferredSize().height);

          //---- OBJ_27 ----
          OBJ_27.setText("Objectifs non atteint");
          OBJ_27.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_27.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_27.setFont(OBJ_27.getFont().deriveFont(OBJ_27.getFont().getStyle() | Font.BOLD));
          OBJ_27.setName("OBJ_27");
          p_recup.add(OBJ_27);
          OBJ_27.setBounds(450, 125, 130, 24);

          //---- OBJ_28 ----
          OBJ_28.setText("Objectifs atteint");
          OBJ_28.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_28.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_28.setFont(OBJ_28.getFont().deriveFont(OBJ_28.getFont().getStyle() | Font.BOLD));
          OBJ_28.setName("OBJ_28");
          p_recup.add(OBJ_28);
          OBJ_28.setBounds(605, 125, 130, 24);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 771, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 274, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie ELIB;
  private XRiTextField C1PC11;
  private JLabel OBJ_21;
  private JLabel OBJ_45;
  private XRiComboBox C1TRA;
  private JLabel OBJ_46;
  private XRiTextField C1RAT;
  private XRiTextField C1PC21;
  private XRiTextField C1GR11;
  private XRiTextField C1GR21;
  private JLabel OBJ_22;
  private RiZoneSortie LRP1;
  private RiZoneSortie LRP2;
  private RiZoneSortie LRP3;
  private XRiTextField C1TB1;
  private JLabel OBJ_23;
  private XRiTextField C1PC31;
  private XRiTextField C1GR31;
  private XRiTextField C1TB2;
  private XRiTextField C1TB3;
  private JLabel OBJ_24;
  private XRiTextField C1PC12;
  private XRiTextField C1PC22;
  private XRiTextField C1PC32;
  private XRiTextField C1GR32;
  private XRiTextField C1GR22;
  private XRiTextField C1GR12;
  private JLabel OBJ_25;
  private XRiTextField C1RP1;
  private JLabel OBJ_26;
  private XRiTextField C1RP2;
  private XRiTextField C1RP3;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
