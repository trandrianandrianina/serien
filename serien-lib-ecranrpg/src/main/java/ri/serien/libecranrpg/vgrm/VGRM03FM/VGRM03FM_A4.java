
package ri.serien.libecranrpg.vgrm.VGRM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGRM03FM_A4 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _WTP01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11",
      "WTP12", "WTP13", "WTP14", "WTP15", };
  private String[] _WTP01_Title = { "HLD01", };
  private String[][] _WTP01_Data = { { "LD01", }, { "LD02", }, { "LD03", }, { "LD04", }, { "LD05", }, { "LD06", }, { "LD07", }, { "LD08", },
      { "LD09", }, { "LD10", }, { "LD11", }, { "LD12", }, { "LD13", }, { "LD14", }, { "LD15", }, };
  private int[] _WTP01_Width = { 63, };
  
  public VGRM03FM_A4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTP01.setAspectTable(_WTP01_Top, _WTP01_Title, _WTP01_Data, _WTP01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    NOMRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RPLIB@")).trim());
    CODRP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDREP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( LIST, LIST.get_LIST_Title_Data_Brut(), _WTP01_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    OBJ_37.setVisible(lexique.HostFieldGetData("G15").equalsIgnoreCase("*G"));
    OBJ_36.setVisible(lexique.HostFieldGetData("G14").equalsIgnoreCase("*G"));
    OBJ_35.setVisible(lexique.HostFieldGetData("G13").equalsIgnoreCase("*G"));
    OBJ_34.setVisible(lexique.HostFieldGetData("G12").equalsIgnoreCase("*G"));
    OBJ_33.setVisible(lexique.HostFieldGetData("G11").equalsIgnoreCase("*G"));
    OBJ_32.setVisible(lexique.HostFieldGetData("G10").equalsIgnoreCase("*G"));
    OBJ_31.setVisible(lexique.HostFieldGetData("G09").equalsIgnoreCase("*G"));
    OBJ_30.setVisible(lexique.HostFieldGetData("G08").equalsIgnoreCase("*G"));
    OBJ_29.setVisible(lexique.HostFieldGetData("G07").equalsIgnoreCase("*G"));
    OBJ_28.setVisible(lexique.HostFieldGetData("G06").equalsIgnoreCase("*G"));
    OBJ_27.setVisible(lexique.HostFieldGetData("G05").equalsIgnoreCase("*G"));
    OBJ_26.setVisible(lexique.HostFieldGetData("G04").equalsIgnoreCase("*G"));
    OBJ_24.setVisible(lexique.HostFieldGetData("G02").equalsIgnoreCase("*G"));
    OBJ_25.setVisible(lexique.HostFieldGetData("G03").equalsIgnoreCase("*G"));
    
    OBJ_37.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_36.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_35.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_34.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_33.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_32.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_31.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_30.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_29.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_28.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_27.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_26.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_24.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_25.setIcon(lexique.chargerImage("images/violet.gif", true));
    OBJ_58.setIcon(lexique.chargerImage("images/w95mbx02.gif", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void CHOISIRActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTP01_Top, "1", "Enter");
    WTP01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTP01_Top, "1", "Enter",e);
    if (WTP01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    NOMRP = new RiZoneSortie();
    CODRP = new RiZoneSortie();
    label1 = new JLabel();
    WCLI = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    INDNFA = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_49 = new JLabel();
    OBJ_50 = new JLabel();
    ARG1M = new XRiTextField();
    ARG1A = new XRiTextField();
    ARG2M = new XRiTextField();
    ARG2A = new XRiTextField();
    SCROLLPANE_LIST = new JScrollPane();
    WTP01 = new XRiTable();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    CHOISIR = new JMenuItem();
    OBJ_53 = new JButton();
    OBJ_39 = new JButton();
    OBJ_58 = new JButton();
    OBJ_25 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    OBJ_32 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_34 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_36 = new JLabel();
    OBJ_37 = new JLabel();
    OBJ_24 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(920, 550));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Commissionnement repr\u00e9sentant");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(880, 32));
          p_tete_gauche.setMinimumSize(new Dimension(880, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- NOMRP ----
          NOMRP.setOpaque(false);
          NOMRP.setText("@RPLIB@");
          NOMRP.setName("NOMRP");

          //---- CODRP ----
          CODRP.setOpaque(false);
          CODRP.setText("@INDREP@");
          CODRP.setName("CODRP");

          //---- label1 ----
          label1.setText("Repr\u00e9sentant");
          label1.setName("label1");

          //---- WCLI ----
          WCLI.setComponentPopupMenu(BTD);
          WCLI.setName("WCLI");

          //---- label2 ----
          label2.setText("Num\u00e9ro client");
          label2.setName("label2");

          //---- label3 ----
          label3.setText("Num\u00e9ro de facture");
          label3.setName("label3");

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(BTD);
          INDNFA.setName("INDNFA");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(CODRP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(NOMRP, GroupLayout.PREFERRED_SIZE, 310, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WCLI, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(label3, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(115, 115, 115)
                    .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE))))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label1, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(CODRP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(NOMRP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label2, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WCLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(label3, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 320));
            menus_haut.setPreferredSize(new Dimension(160, 320));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(720, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder(""));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_49 ----
            OBJ_49.setText("Date de facuration");
            OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_49.setName("OBJ_49");
            panel1.add(OBJ_49);
            OBJ_49.setBounds(25, 37, 125, 25);

            //---- OBJ_50 ----
            OBJ_50.setText("Date de r\u00e8glement");
            OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(225, 37, 125, 25);

            //---- ARG1M ----
            ARG1M.setComponentPopupMenu(BTD);
            ARG1M.setName("ARG1M");
            panel1.add(ARG1M);
            ARG1M.setBounds(145, 35, 26, ARG1M.getPreferredSize().height);

            //---- ARG1A ----
            ARG1A.setComponentPopupMenu(BTD);
            ARG1A.setName("ARG1A");
            panel1.add(ARG1A);
            ARG1A.setBounds(175, 35, 26, ARG1A.getPreferredSize().height);

            //---- ARG2M ----
            ARG2M.setComponentPopupMenu(BTD);
            ARG2M.setName("ARG2M");
            panel1.add(ARG2M);
            ARG2M.setBounds(345, 35, 26, ARG2M.getPreferredSize().height);

            //---- ARG2A ----
            ARG2A.setComponentPopupMenu(BTD);
            ARG2A.setName("ARG2A");
            panel1.add(ARG2A);
            ARG2A.setBounds(375, 35, 26, ARG2A.getPreferredSize().height);

            //======== SCROLLPANE_LIST ========
            {
              SCROLLPANE_LIST.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

              //---- WTP01 ----
              WTP01.setComponentPopupMenu(BTD);
              WTP01.setName("WTP01");
              WTP01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST.setViewportView(WTP01);
            }
            panel1.add(SCROLLPANE_LIST);
            SCROLLPANE_LIST.setBounds(20, 75, 577, 270);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(600, 80, 25, 120);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(600, 220, 25, 120);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 645, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 385, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- CHOISIR ----
      CHOISIR.setText("Choisir");
      CHOISIR.setName("CHOISIR");
      CHOISIR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          CHOISIRActionPerformed(e);
        }
      });
      BTD.add(CHOISIR);
    }

    //---- OBJ_53 ----
    OBJ_53.setText("Recherche de facture");
    OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_53.setName("OBJ_53");

    //---- OBJ_39 ----
    OBJ_39.setText("Liste des factures commissionn\u00e9es");
    OBJ_39.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_39.setName("OBJ_39");

    //---- OBJ_58 ----
    OBJ_58.setText("");
    OBJ_58.setToolTipText("Aide");
    OBJ_58.setComponentPopupMenu(BTD);
    OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_58.setName("OBJ_58");

    //---- OBJ_25 ----
    OBJ_25.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_25.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_25.setName("OBJ_25");

    //---- OBJ_26 ----
    OBJ_26.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_26.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_26.setName("OBJ_26");

    //---- OBJ_27 ----
    OBJ_27.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_27.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_27.setName("OBJ_27");

    //---- OBJ_28 ----
    OBJ_28.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_28.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_28.setName("OBJ_28");

    //---- OBJ_29 ----
    OBJ_29.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_29.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_29.setName("OBJ_29");

    //---- OBJ_30 ----
    OBJ_30.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_30.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_30.setName("OBJ_30");

    //---- OBJ_31 ----
    OBJ_31.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_31.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_31.setName("OBJ_31");

    //---- OBJ_32 ----
    OBJ_32.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_32.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_32.setName("OBJ_32");

    //---- OBJ_33 ----
    OBJ_33.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_33.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_33.setName("OBJ_33");

    //---- OBJ_34 ----
    OBJ_34.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_34.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_34.setName("OBJ_34");

    //---- OBJ_35 ----
    OBJ_35.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_35.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_35.setName("OBJ_35");

    //---- OBJ_36 ----
    OBJ_36.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_36.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_36.setName("OBJ_36");

    //---- OBJ_37 ----
    OBJ_37.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_37.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_37.setName("OBJ_37");

    //---- OBJ_24 ----
    OBJ_24.setIcon(new ImageIcon("images/violet.gif"));
    OBJ_24.setToolTipText("G\u00e9n\u00e9r\u00e9e");
    OBJ_24.setName("OBJ_24");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie NOMRP;
  private RiZoneSortie CODRP;
  private JLabel label1;
  private XRiTextField WCLI;
  private JLabel label2;
  private JLabel label3;
  private XRiTextField INDNFA;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_49;
  private JLabel OBJ_50;
  private XRiTextField ARG1M;
  private XRiTextField ARG1A;
  private XRiTextField ARG2M;
  private XRiTextField ARG2A;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable WTP01;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem CHOISIR;
  private JButton OBJ_53;
  private JButton OBJ_39;
  private JButton OBJ_58;
  private JLabel OBJ_25;
  private JLabel OBJ_26;
  private JLabel OBJ_27;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private JLabel OBJ_32;
  private JLabel OBJ_33;
  private JLabel OBJ_34;
  private JLabel OBJ_35;
  private JLabel OBJ_36;
  private JLabel OBJ_37;
  private JLabel OBJ_24;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
