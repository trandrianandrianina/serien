
package ri.serien.libecranrpg.vgrm.VGRM03FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGRM03FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGRM03FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    INDNFA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDNFA@")).trim());
    INDETB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDETB@")).trim());
    INDREP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INDREP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_65.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLNOM@")).trim());
    CLTEL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CLTEL@")).trim());
    OBJ_78.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CRLIB@")).trim());
    OBJ_25.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SEUI@")).trim());
    OBJ_26.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ACTI@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SURC@")).trim());
    CRCNT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CRCNT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    
    
    CRIN2.setVisible(lexique.isPresent("CRIN2"));
    CRCLIL.setEnabled(lexique.isPresent("CRCLIL"));
    OBJ_76.setVisible(lexique.isPresent("CRCNT"));
    // CRDTRX.setEnabled( lexique.isPresent("CRDTRX"));
    CRCLIP.setEnabled(lexique.isPresent("CRCLIP"));
    CRCNT.setVisible(lexique.isPresent("CRCNT"));
    // CRDDPX.setEnabled( lexique.isPresent("CRDDPX"));
    // CRDTFX.setEnabled( lexique.isPresent("CRDTFX"));
    OBJ_54.setVisible(lexique.isPresent("G"));
    CRMDP.setEnabled(lexique.isPresent("CRMDP"));
    CRMTP.setEnabled(lexique.isPresent("CRMTP"));
    CRMTC.setEnabled(lexique.isPresent("CRMTC"));
    CRMTR.setEnabled(lexique.isPresent("CRMTR"));
    CRMTF.setEnabled(lexique.isPresent("CRMTF"));
    CLTEL.setEnabled(lexique.isPresent("CLTEL"));
    OBJ_81.setVisible(lexique.isPresent("SURC"));
    OBJ_26.setVisible(lexique.isPresent("ACTI"));
    OBJ_25.setVisible(lexique.isPresent("SEUI"));
    OBJ_78.setVisible(lexique.isPresent("CRLIB"));
    OBJ_65.setVisible(lexique.isPresent("CLNOM"));
    CROBS.setEnabled(lexique.isPresent("CROBS"));
    
    p_bpresentation.setCodeEtablissement(INDETB.getText());
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_54 = new JLabel();
    OBJ_48 = new JLabel();
    INDNFA = new RiZoneSortie();
    INDETB = new RiZoneSortie();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    INDREP = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_65 = new RiZoneSortie();
    CLTEL = new RiZoneSortie();
    OBJ_70 = new JLabel();
    OBJ_59 = new JLabel();
    OBJ_66 = new JLabel();
    CRMTF = new XRiTextField();
    CRMTR = new XRiTextField();
    CRDTFX = new XRiCalendrier();
    OBJ_68 = new JLabel();
    OBJ_72 = new JLabel();
    CRCLIP = new XRiTextField();
    CRDTRX = new XRiCalendrier();
    CRCLIL = new XRiTextField();
    xTitledPanel2 = new JXTitledPanel();
    OBJ_78 = new RiZoneSortie();
    OBJ_79 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_85 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_81 = new RiZoneSortie();
    OBJ_82 = new JLabel();
    CRMTC = new XRiTextField();
    CRMTP = new XRiTextField();
    CRMDP = new XRiTextField();
    CRDDPX = new XRiCalendrier();
    CRCNT = new RiZoneSortie();
    OBJ_76 = new JLabel();
    CRIN2 = new XRiTextField();
    OBJ_87 = new JLabel();
    xTitledPanel3 = new JXTitledPanel();
    CROBS = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_20 = new JMenuItem();
    OBJ_19 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(890, 640));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Commissionnement repr\u00e9sentant");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_54 ----
          OBJ_54.setText("G\u00e9n\u00e9r\u00e9e");
          OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() | Font.BOLD));
          OBJ_54.setName("OBJ_54");

          //---- OBJ_48 ----
          OBJ_48.setText("Num\u00e9ro facture");
          OBJ_48.setName("OBJ_48");

          //---- INDNFA ----
          INDNFA.setComponentPopupMenu(BTD);
          INDNFA.setOpaque(false);
          INDNFA.setText("@INDNFA@");
          INDNFA.setName("INDNFA");

          //---- INDETB ----
          INDETB.setComponentPopupMenu(BTD);
          INDETB.setOpaque(false);
          INDETB.setText("@INDETB@");
          INDETB.setName("INDETB");

          //---- OBJ_46 ----
          OBJ_46.setText("Etablissement");
          OBJ_46.setName("OBJ_46");

          //---- OBJ_47 ----
          OBJ_47.setText("Repr\u00e9sentant");
          OBJ_47.setName("OBJ_47");

          //---- INDREP ----
          INDREP.setComponentPopupMenu(BTD);
          INDREP.setOpaque(false);
          INDREP.setText("@INDREP@");
          INDREP.setName("INDREP");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(95, 95, 95)
                    .addComponent(INDREP, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                .addGap(26, 26, 26)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 95, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 72, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(INDETB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDREP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(INDNFA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_54, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("M\u00e9mo. position curseur");
              riSousMenu_bt6.setToolTipText("On/Off m\u00e9morisation position curseur en modification");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(690, 540));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Facture client");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            xTitledPanel1ContentContainer.setLayout(null);

            //---- OBJ_65 ----
            OBJ_65.setText("@CLNOM@");
            OBJ_65.setName("OBJ_65");
            xTitledPanel1ContentContainer.add(OBJ_65);
            OBJ_65.setBounds(115, 27, 265, OBJ_65.getPreferredSize().height);

            //---- CLTEL ----
            CLTEL.setComponentPopupMenu(BTD);
            CLTEL.setText("@CLTEL@");
            CLTEL.setName("CLTEL");
            xTitledPanel1ContentContainer.add(CLTEL);
            CLTEL.setBounds(470, 27, 150, CLTEL.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("Mois r\u00e8glement");
            OBJ_70.setName("OBJ_70");
            xTitledPanel1ContentContainer.add(OBJ_70);
            OBJ_70.setBounds(16, 89, 102, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("T\u00e9lephone");
            OBJ_59.setName("OBJ_59");
            xTitledPanel1ContentContainer.add(OBJ_59);
            OBJ_59.setBounds(395, 29, 75, 20);

            //---- OBJ_66 ----
            OBJ_66.setText("Date facture");
            OBJ_66.setName("OBJ_66");
            xTitledPanel1ContentContainer.add(OBJ_66);
            OBJ_66.setBounds(16, 59, 81, 20);

            //---- CRMTF ----
            CRMTF.setComponentPopupMenu(BTD);
            CRMTF.setName("CRMTF");
            xTitledPanel1ContentContainer.add(CRMTF);
            CRMTF.setBounds(470, 55, 98, CRMTF.getPreferredSize().height);

            //---- CRMTR ----
            CRMTR.setComponentPopupMenu(BTD);
            CRMTR.setName("CRMTR");
            xTitledPanel1ContentContainer.add(CRMTR);
            CRMTR.setBounds(470, 85, 98, CRMTR.getPreferredSize().height);

            //---- CRDTFX ----
            CRDTFX.setComponentPopupMenu(BTD);
            CRDTFX.setName("CRDTFX");
            xTitledPanel1ContentContainer.add(CRDTFX);
            CRDTFX.setBounds(115, 55, 105, CRDTFX.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("Montant");
            OBJ_68.setName("OBJ_68");
            xTitledPanel1ContentContainer.add(OBJ_68);
            OBJ_68.setBounds(395, 59, 75, 20);

            //---- OBJ_72 ----
            OBJ_72.setText("Montant");
            OBJ_72.setName("OBJ_72");
            xTitledPanel1ContentContainer.add(OBJ_72);
            OBJ_72.setBounds(395, 89, 75, 20);

            //---- CRCLIP ----
            CRCLIP.setComponentPopupMenu(BTD);
            CRCLIP.setName("CRCLIP");
            xTitledPanel1ContentContainer.add(CRCLIP);
            CRCLIP.setBounds(15, 25, 58, CRCLIP.getPreferredSize().height);

            //---- CRDTRX ----
            CRDTRX.setComponentPopupMenu(BTD);
            CRDTRX.setTypeSaisie(6);
            CRDTRX.setName("CRDTRX");
            xTitledPanel1ContentContainer.add(CRDTRX);
            CRDTRX.setBounds(115, 85, 90, CRDTRX.getPreferredSize().height);

            //---- CRCLIL ----
            CRCLIL.setComponentPopupMenu(BTD);
            CRCLIL.setName("CRCLIL");
            xTitledPanel1ContentContainer.add(CRCLIL);
            CRCLIL.setBounds(75, 25, 34, CRCLIL.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel1ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel2 ========
          {
            xTitledPanel2.setTitle("Commission");
            xTitledPanel2.setName("xTitledPanel2");
            Container xTitledPanel2ContentContainer = xTitledPanel2.getContentContainer();
            xTitledPanel2ContentContainer.setLayout(null);

            //---- OBJ_78 ----
            OBJ_78.setText("@CRLIB@");
            OBJ_78.setName("OBJ_78");
            xTitledPanel2ContentContainer.add(OBJ_78);
            OBJ_78.setBounds(120, 27, 243, OBJ_78.getPreferredSize().height);

            //---- OBJ_79 ----
            OBJ_79.setText("Montant total de la commission");
            OBJ_79.setName("OBJ_79");
            xTitledPanel2ContentContainer.add(OBJ_79);
            OBJ_79.setBounds(15, 59, 195, 20);

            //---- OBJ_25 ----
            OBJ_25.setText("@SEUI@");
            OBJ_25.setName("OBJ_25");
            xTitledPanel2ContentContainer.add(OBJ_25);
            OBJ_25.setBounds(400, 29, 183, 20);

            //---- OBJ_85 ----
            OBJ_85.setText("Montant du dernier paiement");
            OBJ_85.setName("OBJ_85");
            xTitledPanel2ContentContainer.add(OBJ_85);
            OBJ_85.setBounds(15, 119, 195, 20);

            //---- OBJ_26 ----
            OBJ_26.setText("@ACTI@");
            OBJ_26.setName("OBJ_26");
            xTitledPanel2ContentContainer.add(OBJ_26);
            OBJ_26.setBounds(400, 29, 183, 20);

            //---- OBJ_81 ----
            OBJ_81.setText("@SURC@");
            OBJ_81.setName("OBJ_81");
            xTitledPanel2ContentContainer.add(OBJ_81);
            OBJ_81.setBounds(330, 57, 153, OBJ_81.getPreferredSize().height);

            //---- OBJ_82 ----
            OBJ_82.setText("Montant total pay\u00e9");
            OBJ_82.setName("OBJ_82");
            xTitledPanel2ContentContainer.add(OBJ_82);
            OBJ_82.setBounds(15, 89, 195, 20);

            //---- CRMTC ----
            CRMTC.setComponentPopupMenu(BTD);
            CRMTC.setName("CRMTC");
            xTitledPanel2ContentContainer.add(CRMTC);
            CRMTC.setBounds(225, 55, 98, CRMTC.getPreferredSize().height);

            //---- CRMTP ----
            CRMTP.setComponentPopupMenu(BTD);
            CRMTP.setName("CRMTP");
            xTitledPanel2ContentContainer.add(CRMTP);
            CRMTP.setBounds(225, 85, 98, CRMTP.getPreferredSize().height);

            //---- CRMDP ----
            CRMDP.setComponentPopupMenu(BTD);
            CRMDP.setName("CRMDP");
            xTitledPanel2ContentContainer.add(CRMDP);
            CRMDP.setBounds(225, 115, 98, CRMDP.getPreferredSize().height);

            //---- CRDDPX ----
            CRDDPX.setComponentPopupMenu(BTD);
            CRDDPX.setName("CRDDPX");
            xTitledPanel2ContentContainer.add(CRDDPX);
            CRDDPX.setBounds(378, 115, 105, CRDDPX.getPreferredSize().height);

            //---- CRCNT ----
            CRCNT.setComponentPopupMenu(BTD);
            CRCNT.setText("@CRCNT@");
            CRCNT.setName("CRCNT");
            xTitledPanel2ContentContainer.add(CRCNT);
            CRCNT.setBounds(55, 27, 60, CRCNT.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("Type");
            OBJ_76.setName("OBJ_76");
            xTitledPanel2ContentContainer.add(OBJ_76);
            OBJ_76.setBounds(15, 29, 36, 20);

            //---- CRIN2 ----
            CRIN2.setComponentPopupMenu(BTD);
            CRIN2.setName("CRIN2");
            xTitledPanel2ContentContainer.add(CRIN2);
            CRIN2.setBounds(370, 25, 20, CRIN2.getPreferredSize().height);

            //---- OBJ_87 ----
            OBJ_87.setText("le");
            OBJ_87.setHorizontalAlignment(SwingConstants.CENTER);
            OBJ_87.setName("OBJ_87");
            xTitledPanel2ContentContainer.add(OBJ_87);
            OBJ_87.setBounds(345, 119, 25, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel2ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel2ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel2ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel2ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel2ContentContainer.setPreferredSize(preferredSize);
            }
          }

          //======== xTitledPanel3 ========
          {
            xTitledPanel3.setTitle("Observations");
            xTitledPanel3.setName("xTitledPanel3");
            Container xTitledPanel3ContentContainer = xTitledPanel3.getContentContainer();
            xTitledPanel3ContentContainer.setLayout(null);

            //---- CROBS ----
            CROBS.setComponentPopupMenu(BTD);
            CROBS.setName("CROBS");
            xTitledPanel3ContentContainer.add(CROBS);
            CROBS.setBounds(15, 25, 550, CROBS.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < xTitledPanel3ContentContainer.getComponentCount(); i++) {
                Rectangle bounds = xTitledPanel3ContentContainer.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = xTitledPanel3ContentContainer.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              xTitledPanel3ContentContainer.setMinimumSize(preferredSize);
              xTitledPanel3ContentContainer.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(24, 24, 24)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE)
                  .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 640, GroupLayout.PREFERRED_SIZE)))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 160, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(xTitledPanel2, GroupLayout.PREFERRED_SIZE, 190, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(xTitledPanel3, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_20 ----
      OBJ_20.setText("Choix possibles");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);

      //---- OBJ_19 ----
      OBJ_19.setText("Aide en ligne");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_54;
  private JLabel OBJ_48;
  private RiZoneSortie INDNFA;
  private RiZoneSortie INDETB;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private RiZoneSortie INDREP;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledPanel xTitledPanel1;
  private RiZoneSortie OBJ_65;
  private RiZoneSortie CLTEL;
  private JLabel OBJ_70;
  private JLabel OBJ_59;
  private JLabel OBJ_66;
  private XRiTextField CRMTF;
  private XRiTextField CRMTR;
  private XRiCalendrier CRDTFX;
  private JLabel OBJ_68;
  private JLabel OBJ_72;
  private XRiTextField CRCLIP;
  private XRiCalendrier CRDTRX;
  private XRiTextField CRCLIL;
  private JXTitledPanel xTitledPanel2;
  private RiZoneSortie OBJ_78;
  private JLabel OBJ_79;
  private JLabel OBJ_25;
  private JLabel OBJ_85;
  private JLabel OBJ_26;
  private RiZoneSortie OBJ_81;
  private JLabel OBJ_82;
  private XRiTextField CRMTC;
  private XRiTextField CRMTP;
  private XRiTextField CRMDP;
  private XRiCalendrier CRDDPX;
  private RiZoneSortie CRCNT;
  private JLabel OBJ_76;
  private XRiTextField CRIN2;
  private JLabel OBJ_87;
  private JXTitledPanel xTitledPanel3;
  private XRiTextField CROBS;
  private JPopupMenu BTD;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_19;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
