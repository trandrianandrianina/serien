
package ri.serien.libecranrpg.vgrm.VGRM10FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGRM10FM_A1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] PCTRA_Value = { "", "A", "G", "F", "T", "R" };
  private String[] PCTBC_Value = { "", "1", "2" };
  
  public VGRM10FM_A1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    PCTRA.setValeurs(PCTRA_Value, null);
    PCTBC.setValeurs(PCTBC_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ELIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ELIB@")).trim());
    PCRAT.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBRAT@")).trim());
    LIB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@")).trim());
    LIB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Rattachement"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    ELIB = new RiZoneSortie();
    PCCOM1 = new XRiTextField();
    OBJ_21 = new JLabel();
    OBJ_45 = new JLabel();
    PCTRA = new XRiComboBox();
    OBJ_46 = new JLabel();
    PCRAT = new XRiTextField();
    PCCOM2 = new XRiTextField();
    PCGR1 = new XRiTextField();
    PCGR2 = new XRiTextField();
    OBJ_22 = new JLabel();
    PCDD1X = new XRiCalendrier();
    PCDD2X = new XRiCalendrier();
    PCDF1X = new XRiCalendrier();
    PCDF2X = new XRiCalendrier();
    LIB1 = new RiZoneSortie();
    LIB2 = new RiZoneSortie();
    OBJ_23 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_47 = new JLabel();
    PCTBC = new XRiComboBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(855, 280));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== p_recup ========
        {
          p_recup.setBorder(new TitledBorder(""));
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //---- ELIB ----
          ELIB.setText("@ELIB@");
          ELIB.setName("ELIB");
          p_recup.add(ELIB);
          ELIB.setBounds(187, 80, 310, ELIB.getPreferredSize().height);

          //---- PCCOM1 ----
          PCCOM1.setComponentPopupMenu(BTD);
          PCCOM1.setName("PCCOM1");
          p_recup.add(PCCOM1);
          PCCOM1.setBounds(26, 145, 60, PCCOM1.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setText("Pourcentage   ou");
          OBJ_21.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_21.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_21.setName("OBJ_21");
          p_recup.add(OBJ_21);
          OBJ_21.setBounds(12, 121, 108, 24);

          //---- OBJ_45 ----
          OBJ_45.setText("Type de rattachement");
          OBJ_45.setName("OBJ_45");
          p_recup.add(OBJ_45);
          OBJ_45.setBounds(26, 19, 149, 28);

          //---- PCTRA ----
          PCTRA.setModel(new DefaultComboBoxModel(new String[] {
            "Tous",
            "Article",
            "Groupe",
            "Famille",
            "Tarif",
            "Rattachement CNV"
          }));
          PCTRA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PCTRA.setName("PCTRA");
          p_recup.add(PCTRA);
          PCTRA.setBounds(187, 20, 210, PCTRA.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("Rattachement");
          OBJ_46.setName("OBJ_46");
          p_recup.add(OBJ_46);
          OBJ_46.setBounds(26, 49, 149, 28);

          //---- PCRAT ----
          PCRAT.setToolTipText("@LIBRAT@");
          PCRAT.setComponentPopupMenu(BTD);
          PCRAT.setName("PCRAT");
          p_recup.add(PCRAT);
          PCRAT.setBounds(187, 49, 210, PCRAT.getPreferredSize().height);

          //---- PCCOM2 ----
          PCCOM2.setComponentPopupMenu(BTD);
          PCCOM2.setName("PCCOM2");
          p_recup.add(PCCOM2);
          PCCOM2.setBounds(26, 175, 60, PCCOM2.getPreferredSize().height);

          //---- PCGR1 ----
          PCGR1.setComponentPopupMenu(BTD);
          PCGR1.setName("PCGR1");
          p_recup.add(PCGR1);
          PCGR1.setBounds(110, 145, 60, PCGR1.getPreferredSize().height);

          //---- PCGR2 ----
          PCGR2.setComponentPopupMenu(BTD);
          PCGR2.setName("PCGR2");
          p_recup.add(PCGR2);
          PCGR2.setBounds(110, 175, 60, PCGR2.getPreferredSize().height);

          //---- OBJ_22 ----
          OBJ_22.setText("Grille");
          OBJ_22.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_22.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_22.setName("OBJ_22");
          p_recup.add(OBJ_22);
          OBJ_22.setBounds(108, 121, 65, 24);

          //---- PCDD1X ----
          PCDD1X.setName("PCDD1X");
          p_recup.add(PCDD1X);
          PCDD1X.setBounds(187, 145, 105, PCDD1X.getPreferredSize().height);

          //---- PCDD2X ----
          PCDD2X.setName("PCDD2X");
          p_recup.add(PCDD2X);
          PCDD2X.setBounds(187, 175, 105, PCDD2X.getPreferredSize().height);

          //---- PCDF1X ----
          PCDF1X.setName("PCDF1X");
          p_recup.add(PCDF1X);
          PCDF1X.setBounds(309, 145, 105, PCDF1X.getPreferredSize().height);

          //---- PCDF2X ----
          PCDF2X.setName("PCDF2X");
          p_recup.add(PCDF2X);
          PCDF2X.setBounds(309, 175, 105, PCDF2X.getPreferredSize().height);

          //---- LIB1 ----
          LIB1.setText("@LIB1@");
          LIB1.setName("LIB1");
          p_recup.add(LIB1);
          LIB1.setBounds(431, 147, 210, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setText("@LIB2@");
          LIB2.setName("LIB2");
          p_recup.add(LIB2);
          LIB2.setBounds(431, 177, 210, LIB2.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("D\u00e9but");
          OBJ_23.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_23.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_23.setName("OBJ_23");
          p_recup.add(OBJ_23);
          OBJ_23.setBounds(187, 121, 63, 24);

          //---- OBJ_24 ----
          OBJ_24.setText("Fin");
          OBJ_24.setHorizontalTextPosition(SwingConstants.CENTER);
          OBJ_24.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_24.setName("OBJ_24");
          p_recup.add(OBJ_24);
          OBJ_24.setBounds(309, 121, 61, 24);

          //---- OBJ_47 ----
          OBJ_47.setText("Base commission");
          OBJ_47.setName("OBJ_47");
          p_recup.add(OBJ_47);
          OBJ_47.setBounds(25, 210, 145, 28);

          //---- PCTBC ----
          PCTBC.setModel(new DefaultComboBoxModel(new String[] {
            "Sur chiffre d'affaires",
            "Sur marge",
            "Sur chiffre d'affaires hors droits (alcools)"
          }));
          PCTBC.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          PCTBC.setName("PCTBC");
          p_recup.add(PCTBC);
          PCTBC.setBounds(187, 211, 210, PCTBC.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_recup.getComponentCount(); i++) {
              Rectangle bounds = p_recup.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_recup.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_recup.setMinimumSize(preferredSize);
            p_recup.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 673, Short.MAX_VALUE)
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addContainerGap()
              .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 268, Short.MAX_VALUE)
              .addContainerGap())
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private RiZoneSortie ELIB;
  private XRiTextField PCCOM1;
  private JLabel OBJ_21;
  private JLabel OBJ_45;
  private XRiComboBox PCTRA;
  private JLabel OBJ_46;
  private XRiTextField PCRAT;
  private XRiTextField PCCOM2;
  private XRiTextField PCGR1;
  private XRiTextField PCGR2;
  private JLabel OBJ_22;
  private XRiCalendrier PCDD1X;
  private XRiCalendrier PCDD2X;
  private XRiCalendrier PCDF1X;
  private XRiCalendrier PCDF2X;
  private RiZoneSortie LIB1;
  private RiZoneSortie LIB2;
  private JLabel OBJ_23;
  private JLabel OBJ_24;
  private JLabel OBJ_47;
  private XRiComboBox PCTBC;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
