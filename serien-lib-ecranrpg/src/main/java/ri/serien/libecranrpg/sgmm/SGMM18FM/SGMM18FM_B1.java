
package ri.serien.libecranrpg.sgmm.SGMM18FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SGMM18FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] ETAT_Value = { "*", "", "1", "2", "3", "4", "5", "6", "7", "8", };
  
  public SGMM18FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    ETAT.setValeurs(ETAT_Value, null);
    WTOUD.setValeursSelection("**", "  ");
    WTOUA.setValeursSelection("**", "  ");
    WTOUI.setValeursSelection("**", "  ");
    DETMAT.setValeursSelection("O", "N");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    z_dgnom_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    z_wencx_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    z_etablissement_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WETB@")).trim());
    INLIBD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INLIBD@")).trim());
    INLIBF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@INLIBF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    ATEFIN.setEnabled(lexique.isPresent("ATEFIN"));
    ATEDEB.setEnabled(lexique.isPresent("ATEDEB"));
    // DATFIN.setEnabled(lexique.isPresent("DATFIN"));
    // DATDEB.setEnabled(lexique.isPresent("DATDEB"));
    // WTOUD.setVisible(lexique.isPresent("WTOUD"));
    // WTOUD.setEnabled(lexique.isPresent("WTOUD"));
    // WTOUD.setSelected(lexique.HostFieldGetData("WTOUD").equalsIgnoreCase("**"));
    // WTOUA.setVisible(lexique.isPresent("WTOUA"));
    // WTOUA.setEnabled(lexique.isPresent("WTOUA"));
    // WTOUA.setSelected(lexique.HostFieldGetData("WTOUA").equalsIgnoreCase("**"));
    // WTOUI.setSelected(lexique.HostFieldGetData("WTOUI").equalsIgnoreCase("**"));
    // DETMAT.setVisible(lexique.isPresent("DETMAT"));
    // DETMAT.setEnabled(lexique.isPresent("DETMAT"));
    // DETMAT.setSelected(lexique.HostFieldGetData("DETMAT").equalsIgnoreCase("O"));
    OBJ_46.setVisible(lexique.isPresent("ETAT"));
    OBJ_23.setVisible(lexique.isPresent("ETAT"));
    OBJ_22.setVisible(lexique.isPresent("ETAT"));
    // ETAT.setSelectedIndex(getIndice("ETAT", ETAT_Value));
    // ETAT.setVisible(lexique.isPresent("ETAT"));
    OBJ_24.setVisible(lexique.isPresent("ETAT"));
    P_SEL0.setVisible(!lexique.HostFieldGetData("WTOUI").trim().equalsIgnoreCase("**"));
    
    

    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (WTOUD.isSelected())
    // lexique.HostFieldPutData("WTOUD", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUD", 0, " ");
    // if (WTOUA.isSelected())
    // lexique.HostFieldPutData("WTOUA", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUA", 0, " ");
    // if (WTOUI.isSelected())
    // lexique.HostFieldPutData("WTOUI", 0, "**");
    // else
    // lexique.HostFieldPutData("WTOUI", 0, " ");
    // if (DETMAT.isSelected())
    // lexique.HostFieldPutData("DETMAT", 0, "O");
    // else
    // lexique.HostFieldPutData("DETMAT", 0, "N");
    // lexique.HostFieldPutData("ETAT", 0, ETAT_Value[ETAT.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void bouton_etablissementActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "sgmm18"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTOUActionPerformed(ActionEvent e) {
    P_SEL0.setVisible(!P_SEL0.isVisible());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY
    // //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    sep_etablissement = new JXTitledSeparator();
    z_dgnom_ = new RiZoneSortie();
    z_wencx_ = new RiZoneSortie();
    z_etablissement_ = new RiZoneSortie();
    bouton_etablissement = new SNBoutonRecherche();
    OBJ_26 = new JXTitledSeparator();
    P_SEL0 = new JPanel();
    INLIBD = new JLabel();
    INLIBF = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_50 = new JLabel();
    INTDEB = new XRiTextField();
    INTFIN = new XRiTextField();
    OBJ_24 = new JXTitledSeparator();
    ETAT = new XRiComboBox();
    OBJ_22 = new JXTitledSeparator();
    OBJ_23 = new JXTitledSeparator();
    OBJ_46 = new JLabel();
    DETMAT = new XRiCheckBox();
    WTOUI = new XRiCheckBox();
    WTOUA = new XRiCheckBox();
    WTOUD = new XRiCheckBox();
    OBJ_47 = new JLabel();
    OBJ_48 = new JLabel();
    OBJ_51 = new JLabel();
    OBJ_25 = new JLabel();
    ATEDEB = new XRiTextField();
    ATEFIN = new XRiTextField();
    OBJ_49 = new JLabel();
    OBJ_52 = new JLabel();
    DATDEB = new XRiCalendrier();
    DATFIN = new XRiCalendrier();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 620));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(760, 520));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //---- sep_etablissement ----
          sep_etablissement.setTitle("Etablissement s\u00e9lectionn\u00e9");
          sep_etablissement.setName("sep_etablissement");

          //---- z_dgnom_ ----
          z_dgnom_.setText("@DGNOM@");
          z_dgnom_.setName("z_dgnom_");

          //---- z_wencx_ ----
          z_wencx_.setText("@WENCX@");
          z_wencx_.setName("z_wencx_");

          //---- z_etablissement_ ----
          z_etablissement_.setComponentPopupMenu(null);
          z_etablissement_.setText("@WETB@");
          z_etablissement_.setName("z_etablissement_");

          //---- bouton_etablissement ----
          bouton_etablissement.setToolTipText("Changement d'\u00e9tablissement");
          bouton_etablissement.setName("bouton_etablissement");
          bouton_etablissement.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              bouton_etablissementActionPerformed(e);
            }
          });

          //---- OBJ_26 ----
          OBJ_26.setTitle("Plage d'intervenants");
          OBJ_26.setName("OBJ_26");

          //======== P_SEL0 ========
          {
            P_SEL0.setBorder(new BevelBorder(BevelBorder.LOWERED));
            P_SEL0.setOpaque(false);
            P_SEL0.setName("P_SEL0");
            P_SEL0.setLayout(null);

            //---- INLIBD ----
            INLIBD.setText("@INLIBD@");
            INLIBD.setName("INLIBD");
            P_SEL0.add(INLIBD);
            INLIBD.setBounds(235, 14, 316, 20);

            //---- INLIBF ----
            INLIBF.setText("@INLIBF@");
            INLIBF.setName("INLIBF");
            P_SEL0.add(INLIBF);
            INLIBF.setBounds(235, 44, 316, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("Code de d\u00e9but");
            OBJ_53.setName("OBJ_53");
            P_SEL0.add(OBJ_53);
            OBJ_53.setBounds(15, 14, 93, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("Code de fin");
            OBJ_50.setName("OBJ_50");
            P_SEL0.add(OBJ_50);
            OBJ_50.setBounds(15, 44, 72, 20);

            //---- INTDEB ----
            INTDEB.setComponentPopupMenu(BTD);
            INTDEB.setName("INTDEB");
            P_SEL0.add(INTDEB);
            INTDEB.setBounds(145, 10, 40, INTDEB.getPreferredSize().height);

            //---- INTFIN ----
            INTFIN.setComponentPopupMenu(BTD);
            INTFIN.setName("INTFIN");
            P_SEL0.add(INTFIN);
            INTFIN.setBounds(145, 40, 40, INTFIN.getPreferredSize().height);
          }

          //---- OBJ_24 ----
          OBJ_24.setTitle("");
          OBJ_24.setName("OBJ_24");

          //---- ETAT ----
          ETAT.setModel(new DefaultComboBoxModel(new String[] {
            "Tous les \u00e9tats",
            "En attente",
            "Devis \u00e0 faire",
            "Devis exp\u00e9di\u00e9",
            "Devis accept\u00e9",
            "Devis refus\u00e9",
            "A faire",
            "Planifi\u00e9",
            "A facturer",
            "Termin\u00e9"
          }));
          ETAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          ETAT.setName("ETAT");

          //---- OBJ_22 ----
          OBJ_22.setTitle("");
          OBJ_22.setName("OBJ_22");

          //---- OBJ_23 ----
          OBJ_23.setTitle("");
          OBJ_23.setName("OBJ_23");

          //---- OBJ_46 ----
          OBJ_46.setText("Etat des r\u00e9parations \u00e0 \u00e9diter");
          OBJ_46.setName("OBJ_46");

          //---- DETMAT ----
          DETMAT.setText("Edition du d\u00e9tail mat\u00e9riel");
          DETMAT.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          DETMAT.setName("DETMAT");

          //---- WTOUI ----
          WTOUI.setText("S\u00e9lection compl\u00e8te");
          WTOUI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUI.setName("WTOUI");
          WTOUI.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              WTOUActionPerformed(e);
            }
          });

          //---- WTOUA ----
          WTOUA.setText("S\u00e9lection compl\u00e8te");
          WTOUA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUA.setName("WTOUA");

          //---- WTOUD ----
          WTOUD.setText("S\u00e9lection compl\u00e8te");
          WTOUD.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          WTOUD.setName("WTOUD");

          //---- OBJ_47 ----
          OBJ_47.setText("Code Atelier");
          OBJ_47.setName("OBJ_47");

          //---- OBJ_48 ----
          OBJ_48.setText("D\u00e9but");
          OBJ_48.setName("OBJ_48");

          //---- OBJ_51 ----
          OBJ_51.setText("D\u00e9but");
          OBJ_51.setName("OBJ_51");

          //---- OBJ_25 ----
          OBJ_25.setText("Date");
          OBJ_25.setName("OBJ_25");

          //---- ATEDEB ----
          ATEDEB.setComponentPopupMenu(BTD);
          ATEDEB.setName("ATEDEB");

          //---- ATEFIN ----
          ATEFIN.setComponentPopupMenu(BTD);
          ATEFIN.setName("ATEFIN");

          //---- OBJ_49 ----
          OBJ_49.setText("Fin");
          OBJ_49.setName("OBJ_49");

          //---- OBJ_52 ----
          OBJ_52.setText("Fin");
          OBJ_52.setName("OBJ_52");

          //---- DATDEB ----
          DATDEB.setName("DATDEB");

          //---- DATFIN ----
          DATFIN.setName("DATFIN");

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(52, 52, 52)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)
                  .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE)))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(WTOUI, GroupLayout.PREFERRED_SIZE, 144, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(29, 29, 29)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 590, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 78, GroupLayout.PREFERRED_SIZE)
                .addGap(47, 47, 47)
                .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(ATEDEB, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE)
                .addGap(116, 116, 116)
                .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(ATEFIN, GroupLayout.PREFERRED_SIZE, 34, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 33, GroupLayout.PREFERRED_SIZE)
                .addGap(92, 92, 92)
                .addComponent(WTOUD, GroupLayout.PREFERRED_SIZE, 141, GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45)
                .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 183, GroupLayout.PREFERRED_SIZE)
                .addGap(127, 127, 127)
                .addComponent(ETAT, GroupLayout.PREFERRED_SIZE, 129, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, 685, GroupLayout.PREFERRED_SIZE))
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(49, 49, 49)
                .addComponent(DETMAT, GroupLayout.PREFERRED_SIZE, 177, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(sep_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(17, 17, 17)
                    .addComponent(z_etablissement_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(15, 15, 15)
                    .addComponent(bouton_etablissement, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addComponent(z_dgnom_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addGap(6, 6, 6)
                    .addComponent(z_wencx_, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21)
                .addComponent(OBJ_26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(WTOUI, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(P_SEL0, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20)
                .addComponent(OBJ_24, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(19, 19, 19)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_47, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(WTOUA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_48, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ATEDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_49, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ATEFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(2, 2, 2)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_25, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(WTOUD, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_51, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATDEB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_52, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(DATFIN, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addComponent(OBJ_22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addGroup(p_contenuLayout.createSequentialGroup()
                    .addGap(3, 3, 3)
                    .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                  .addComponent(ETAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addComponent(OBJ_23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(DETMAT, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY
  // //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator sep_etablissement;
  private RiZoneSortie z_dgnom_;
  private RiZoneSortie z_wencx_;
  private RiZoneSortie z_etablissement_;
  private SNBoutonRecherche bouton_etablissement;
  private JXTitledSeparator OBJ_26;
  private JPanel P_SEL0;
  private JLabel INLIBD;
  private JLabel INLIBF;
  private JLabel OBJ_53;
  private JLabel OBJ_50;
  private XRiTextField INTDEB;
  private XRiTextField INTFIN;
  private JXTitledSeparator OBJ_24;
  private XRiComboBox ETAT;
  private JXTitledSeparator OBJ_22;
  private JXTitledSeparator OBJ_23;
  private JLabel OBJ_46;
  private XRiCheckBox DETMAT;
  private XRiCheckBox WTOUI;
  private XRiCheckBox WTOUA;
  private XRiCheckBox WTOUD;
  private JLabel OBJ_47;
  private JLabel OBJ_48;
  private JLabel OBJ_51;
  private JLabel OBJ_25;
  private XRiTextField ATEDEB;
  private XRiTextField ATEFIN;
  private JLabel OBJ_49;
  private JLabel OBJ_52;
  private XRiCalendrier DATDEB;
  private XRiCalendrier DATFIN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
