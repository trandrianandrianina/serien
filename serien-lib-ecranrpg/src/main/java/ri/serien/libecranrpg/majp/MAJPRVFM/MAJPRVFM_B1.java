
package ri.serien.libecranrpg.majp.MAJPRVFM;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.util.ArrayList;

import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * [GVM638] Gestion des ventes -> Opérations et éditions diverses -> Purge / reprise sur fichiers -> MàJ prix de revient / lignes bons
 * Titre : MàJ prix de revient / lignes bons
 */
public class MAJPRVFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private int indexSelectionne = 0;
  
  public MAJPRVFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    initDiverses();
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverse après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Indicateur
    boolean isParametragePlanification = lexique.isTrue("97");
    
    // Visibilité
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
  }
  
  @Override
  public void getData() {
    super.getData();
    snEtablissement.renseignerChampRPG(lexique, "WETB");
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      snEtablissement.renseignerChampRPG(lexique, "WETB");
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlDroite = new SNPanel();
    pnlInformationFacturation = new SNPanelTitre();
    lbNumeroBon = new SNLabelChamp();
    WNUMD = new XRiTextField();
    WSUFD = new XRiTextField();
    lbA = new SNLabelChamp();
    WNUMF = new XRiTextField();
    WSUFF = new XRiTextField();
    pnlGauche = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfPeriodeEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriodeEnCours = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Mise \u00e0 jour du prix de revient sur les lignes de bons");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridLayout());
      
      // ======== pnlDroite ========
      {
        pnlDroite.setName("pnlDroite");
        pnlDroite.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ======== pnlInformationFacturation ========
        {
          pnlInformationFacturation.setTitre("S\u00e9lection");
          pnlInformationFacturation.setName("pnlInformationFacturation");
          pnlInformationFacturation.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlInformationFacturation.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0, 0 };
          ((GridBagLayout) pnlInformationFacturation.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlInformationFacturation.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
          ((GridBagLayout) pnlInformationFacturation.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ---- lbNumeroBon ----
          lbNumeroBon.setText("Num\u00e9ros de bons de");
          lbNumeroBon.setFont(new Font("sansserif", Font.PLAIN, 14));
          lbNumeroBon.setName("lbNumeroBon");
          pnlInformationFacturation.add(lbNumeroBon, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WNUMD ----
          WNUMD.setMinimumSize(new Dimension(70, 30));
          WNUMD.setPreferredSize(new Dimension(70, 30));
          WNUMD.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNUMD.setName("WNUMD");
          pnlInformationFacturation.add(WNUMD, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WSUFD ----
          WSUFD.setMinimumSize(new Dimension(30, 30));
          WSUFD.setPreferredSize(new Dimension(30, 30));
          WSUFD.setFont(new Font("sansserif", Font.PLAIN, 14));
          WSUFD.setName("WSUFD");
          pnlInformationFacturation.add(WSUFD, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- lbA ----
          lbA.setText("\u00e0");
          lbA.setMaximumSize(new Dimension(30, 30));
          lbA.setMinimumSize(new Dimension(30, 30));
          lbA.setPreferredSize(new Dimension(30, 30));
          lbA.setHorizontalTextPosition(SwingConstants.CENTER);
          lbA.setHorizontalAlignment(SwingConstants.CENTER);
          lbA.setName("lbA");
          pnlInformationFacturation.add(lbA, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WNUMF ----
          WNUMF.setMinimumSize(new Dimension(70, 30));
          WNUMF.setPreferredSize(new Dimension(70, 30));
          WNUMF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WNUMF.setName("WNUMF");
          pnlInformationFacturation.add(WNUMF, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          
          // ---- WSUFF ----
          WSUFF.setMinimumSize(new Dimension(30, 30));
          WSUFF.setPreferredSize(new Dimension(30, 30));
          WSUFF.setFont(new Font("sansserif", Font.PLAIN, 14));
          WSUFF.setName("WSUFF");
          pnlInformationFacturation.add(WSUFF, new GridBagConstraints(5, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlDroite.add(pnlInformationFacturation, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      }
      pnlContenu.add(pnlDroite);
      
      // ======== pnlGauche ========
      {
        pnlGauche.setName("pnlGauche");
        pnlGauche.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
        ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ======== pnlEtablissement ========
        {
          pnlEtablissement.setTitre("Etablissement");
          pnlEtablissement.setName("pnlEtablissement");
          pnlEtablissement.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
          ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
          ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
          
          // ---- tfPeriodeEnCours ----
          tfPeriodeEnCours.setText("@WENCX@");
          tfPeriodeEnCours.setEnabled(false);
          tfPeriodeEnCours.setPreferredSize(new Dimension(260, 30));
          tfPeriodeEnCours.setMinimumSize(new Dimension(260, 30));
          tfPeriodeEnCours.setMaximumSize(new Dimension(260, 30));
          tfPeriodeEnCours.setName("tfPeriodeEnCours");
          pnlEtablissement.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
              GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          
          // ---- snEtablissement ----
          snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
          snEtablissement.setName("snEtablissement");
          snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
            @Override
            public void valueChanged(SNComposantEvent e) {
              snEtablissementValueChanged(e);
            }
          });
          pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
          
          // ---- lbEtablissement ----
          lbEtablissement.setText("Etablissement en cours");
          lbEtablissement.setName("lbEtablissement");
          pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
          
          // ---- lbPeriodeEnCours ----
          lbPeriodeEnCours.setText("P\u00e9riode en cours");
          lbPeriodeEnCours.setName("lbPeriodeEnCours");
          pnlEtablissement.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
        }
        pnlGauche.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlGauche);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlInformationFacturation;
  private SNLabelChamp lbNumeroBon;
  private XRiTextField WNUMD;
  private XRiTextField WSUFD;
  private SNLabelChamp lbA;
  private XRiTextField WNUMF;
  private XRiTextField WSUFF;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfPeriodeEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNBarreBouton snBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}

class TypeFac {
  String nom;
  String valeur;
  int index;
  
  public TypeFac(String n, String v, int i) {
    nom = n;
    valeur = v;
    index = i;
  }
  
  @Override
  public String toString() {
    return nom;
  }
  
}
