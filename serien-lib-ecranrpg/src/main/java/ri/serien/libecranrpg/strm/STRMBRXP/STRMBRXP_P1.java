
package ri.serien.libecranrpg.strm.STRMBRXP;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class STRMBRXP_P1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public STRMBRXP_P1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    WERR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WERR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    WERR.setVisible(lexique.isTrue("19"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_presentation = new JPanel();
    p_logo = new JPanel();
    logoEtb = new JLabel();
    p_titre = new JPanel();
    titre = new JLabel();
    fond_titre = new JLabel();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OUTQ = new XRiTextField();
    AGENCE = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label5 = new JLabel();
    WERR = new RiZoneSortie();
    panel2 = new JPanel();
    label7 = new JLabel();
    label8 = new JLabel();
    NUMDEB = new XRiTextField();
    NUMFIN = new XRiTextField();
    NUMFIC = new XRiTextField();
    label9 = new JLabel();
    label4 = new JLabel();
    label6 = new JLabel();
    label10 = new JLabel();
    ADSIP = new XRiTextField();
    USRPWD = new XRiTextField();
    USRPRF = new XRiTextField();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //======== p_presentation ========
      {
        p_presentation.setPreferredSize(new Dimension(1020, 55));
        p_presentation.setMinimumSize(new Dimension(950, 55));
        p_presentation.setMaximumSize(new Dimension(32767, 55));
        p_presentation.setBackground(Color.white);
        p_presentation.setAutoscrolls(true);
        p_presentation.setFocusable(false);
        p_presentation.setName("p_presentation");
        p_presentation.setLayout(new BorderLayout());

        //======== p_logo ========
        {
          p_logo.setMinimumSize(new Dimension(120, 55));
          p_logo.setPreferredSize(new Dimension(120, 55));
          p_logo.setBackground(new Color(238, 239, 241));
          p_logo.setName("p_logo");
          p_logo.setLayout(null);

          //---- logoEtb ----
          logoEtb.setBorder(null);
          logoEtb.setName("logoEtb");
          p_logo.add(logoEtb);
          logoEtb.setBounds(70, 5, 45, 45);
        }
        p_presentation.add(p_logo, BorderLayout.EAST);

        //======== p_titre ========
        {
          p_titre.setBackground(new Color(238, 239, 241));
          p_titre.setPreferredSize(new Dimension(930, 0));
          p_titre.setMinimumSize(new Dimension(930, 0));
          p_titre.setName("p_titre");
          p_titre.setLayout(null);

          //---- titre ----
          titre.setBackground(new Color(198, 198, 200));
          titre.setText("Configuration PRISME");
          titre.setHorizontalAlignment(SwingConstants.LEFT);
          titre.setFont(titre.getFont().deriveFont(titre.getFont().getStyle() | Font.BOLD, titre.getFont().getSize() + 6f));
          titre.setForeground(Color.darkGray);
          titre.setName("titre");
          p_titre.add(titre);
          titre.setBounds(10, 0, 600, 55);

          //---- fond_titre ----
          fond_titre.setFont(new Font("Arial", Font.BOLD, 18));
          fond_titre.setForeground(Color.white);
          fond_titre.setName("fond_titre");
          p_titre.add(fond_titre);
          fond_titre.setBounds(0, 0, 900, 55);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_titre.getComponentCount(); i++) {
              Rectangle bounds = p_titre.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_titre.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_titre.setMinimumSize(preferredSize);
            p_titre.setPreferredSize(preferredSize);
          }
        }
        p_presentation.add(p_titre, BorderLayout.CENTER);
      }
      p_nord.add(p_presentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OUTQ ----
            OUTQ.setName("OUTQ");
            panel1.add(OUTQ);
            OUTQ.setBounds(580, 146, 110, OUTQ.getPreferredSize().height);

            //---- AGENCE ----
            AGENCE.setName("AGENCE");
            panel1.add(AGENCE);
            AGENCE.setBounds(580, 191, 110, AGENCE.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Vous venez de demander la configuration pour");
            label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD, label1.getFont().getSize() + 3f));
            label1.setHorizontalAlignment(SwingConstants.CENTER);
            label1.setName("label1");
            panel1.add(label1);
            label1.setBounds(10, 25, 860, 30);

            //---- label2 ----
            label2.setText(" les bordereaux de transport PRISME");
            label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 3f));
            label2.setHorizontalAlignment(SwingConstants.CENTER);
            label2.setName("label2");
            panel1.add(label2);
            label2.setBounds(10, 60, 860, 30);

            //---- label3 ----
            label3.setText("Donnez le nom de la file d'attente de sortie li\u00e9e \u00e0 l'imprimante");
            label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 3f));
            label3.setName("label3");
            panel1.add(label3);
            label3.setBounds(10, 145, 860, 30);

            //---- label5 ----
            label5.setText("Code \u00e9meteur");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getSize() + 3f));
            label5.setName("label5");
            panel1.add(label5);
            label5.setBounds(10, 190, 555, 30);

            //---- WERR ----
            WERR.setText("@WERR@");
            WERR.setForeground(Color.red);
            WERR.setHorizontalAlignment(SwingConstants.CENTER);
            WERR.setName("WERR");
            panel1.add(WERR);
            WERR.setBounds(10, 525, 860, WERR.getPreferredSize().height);

            //======== panel2 ========
            {
              panel2.setBorder(new TitledBorder("Transfert du fichier hebdomadaire des exp\u00e9ditions"));
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- label7 ----
              label7.setText("Plage de num\u00e9ros de fichiers atttribu\u00e9s par PRISME");
              label7.setFont(label7.getFont().deriveFont(label7.getFont().getSize() + 3f));
              label7.setName("label7");
              panel2.add(label7);
              label7.setBounds(25, 35, 520, 30);

              //---- label8 ----
              label8.setText("Num\u00e9ro en cours");
              label8.setFont(label8.getFont().deriveFont(label8.getFont().getSize() + 3f));
              label8.setName("label8");
              panel2.add(label8);
              label8.setBounds(25, 75, 520, 30);

              //---- NUMDEB ----
              NUMDEB.setName("NUMDEB");
              panel2.add(NUMDEB);
              NUMDEB.setBounds(570, 36, 70, NUMDEB.getPreferredSize().height);

              //---- NUMFIN ----
              NUMFIN.setName("NUMFIN");
              panel2.add(NUMFIN);
              NUMFIN.setBounds(660, 36, 70, NUMFIN.getPreferredSize().height);

              //---- NUMFIC ----
              NUMFIC.setName("NUMFIC");
              panel2.add(NUMFIC);
              NUMFIC.setBounds(570, 76, 70, NUMFIC.getPreferredSize().height);

              //---- label9 ----
              label9.setText("/");
              label9.setFont(label9.getFont().deriveFont(label9.getFont().getSize() + 3f));
              label9.setHorizontalAlignment(SwingConstants.CENTER);
              label9.setName("label9");
              panel2.add(label9);
              label9.setBounds(640, 35, 20, 30);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(10, 245, 860, 130);

            //---- label4 ----
            label4.setText("Adresse IP");
            label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() + 3f));
            label4.setName("label4");
            panel1.add(label4);
            label4.setBounds(10, 395, 510, 30);

            //---- label6 ----
            label6.setText("Profil");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getSize() + 3f));
            label6.setName("label6");
            panel1.add(label6);
            label6.setBounds(10, 435, 510, 30);

            //---- label10 ----
            label10.setText("Mot de passe");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getSize() + 3f));
            label10.setName("label10");
            panel1.add(label10);
            label10.setBounds(10, 475, 510, 30);

            //---- ADSIP ----
            ADSIP.setName("ADSIP");
            panel1.add(ADSIP);
            ADSIP.setBounds(580, 396, 160, ADSIP.getPreferredSize().height);

            //---- USRPWD ----
            USRPWD.setName("USRPWD");
            panel1.add(USRPWD);
            USRPWD.setBounds(580, 476, 110, USRPWD.getPreferredSize().height);

            //---- USRPRF ----
            USRPRF.setName("USRPRF");
            panel1.add(USRPRF);
            USRPRF.setBounds(580, 436, 110, USRPRF.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_contenu.add(panel1);
          panel1.setBounds(10, 10, 880, 560);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private JPanel p_presentation;
  private JPanel p_logo;
  private JLabel logoEtb;
  private JPanel p_titre;
  private JLabel titre;
  private JLabel fond_titre;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField OUTQ;
  private XRiTextField AGENCE;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label5;
  private RiZoneSortie WERR;
  private JPanel panel2;
  private JLabel label7;
  private JLabel label8;
  private XRiTextField NUMDEB;
  private XRiTextField NUMFIN;
  private XRiTextField NUMFIC;
  private JLabel label9;
  private JLabel label4;
  private JLabel label6;
  private JLabel label10;
  private XRiTextField ADSIP;
  private XRiTextField USRPWD;
  private XRiTextField USRPRF;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
