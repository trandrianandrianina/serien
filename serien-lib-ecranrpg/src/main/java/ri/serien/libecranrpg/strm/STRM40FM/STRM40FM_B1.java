
package ri.serien.libecranrpg.strm.STRM40FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.referentiel.fournisseur.snfournisseur.SNFournisseur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.combobox.SNComboBox;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.radiobouton.SNRadioButton;
import ri.serien.libswing.composant.primitif.saisie.SNNombreEntier;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Emmanuel MARCQ
 */
public class STRM40FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] WSTAT_Value = { "ATT", "VAL", };
  private boolean modePlanning = false;
  
  public STRM40FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    
    WSTAT.setValeurs(WSTAT_Value, null);
    
    // Barre des boutons
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    bpPresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lbLOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    tfEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@"));
    
    // Message planning
    modePlanning = lexique.isTrue("96");
    lbPlanning.setVisible(modePlanning);
    rbDate.setVisible(modePlanning);
    rbDateParametree.setVisible(modePlanning);
    pnlDateParametree.setVisible(modePlanning);
    lbDate.setVisible(!modePlanning);
    if (modePlanning) {
      Message message = Message.getMessageImportant("Vous êtes en paramétrage de la mise au planning pour cette demande.");
      lbPlanning.setMessage(message);
      cbDatePlanning.removeAllItems();
      cbDatePlanning.addItem("");
      cbDatePlanning.addItem("Date du jour");
      cbDatePlanning.addItem("Début du mois en cours");
      cbDatePlanning.addItem("Début du mois précédent");
      cbDatePlanning.addItem("Fin du mois précédent");
      cbDatePlanning.addItem("Date du jour + nn jours");
      if (cbDatePlanning.getSelectedItem().equals("Date du jour + nn jours")) {
        lbJours.setVisible(true);
        tfNombreJours.setVisible(true);
      }
      else {
        lbJours.setVisible(false);
        tfNombreJours.setVisible(false);
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).startsWith("*")) {
        rbDate.setSelected(false);
        rbDateParametree.setSelected(true);
      }
      else {
        rbDate.setSelected(true);
        rbDateParametree.setSelected(false);
      }
      if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).equals("*DAT")) {
        cbDatePlanning.setSelectedItem("Date du jour");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).equals("*DME")) {
        cbDatePlanning.setSelectedItem("Début du mois en cours");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).equals("*DMP")) {
        cbDatePlanning.setSelectedItem("Début du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).equals("*FMP")) {
        cbDatePlanning.setSelectedItem("Fin du mois précédent");
      }
      else if (Constantes.normerTexte(lexique.HostFieldGetData("WDATM")).startsWith("*DAT")) {
        cbDatePlanning.setSelectedItem("Date du jour + nn jours");
      }
    }
    
    // Etablissement
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Magasin
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "WMAG");
    
    // Fournisseur de début
    snFournisseurDebut.setSession(getSession());
    snFournisseurDebut.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurDebut.charger(false);
    snFournisseurDebut.setSelectionParChampRPG(lexique, "WFRSD");
    
    // Fournisseur de fin
    snFournisseurFin.setSession(getSession());
    snFournisseurFin.setIdEtablissement(snEtablissement.getIdSelection());
    snFournisseurFin.charger(false);
    snFournisseurFin.setSelectionParChampRPG(lexique, "WFRSF");
    
    // Icones
    bpPresentation.setCodeEtablissement(snEtablissement.getCodeSelection());
  }
  
  @Override
  public void getData() {
    super.getData();
    
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    snMagasin.renseignerChampRPG(lexique, "WMAG");
    snFournisseurDebut.renseignerChampRPG(lexique, "WFRSD");
    snFournisseurFin.renseignerChampRPG(lexique, "WFRSF");
    if (modePlanning) {
      switch (cbDatePlanning.getSelectedIndex()) {
        case 1:
          lexique.HostFieldPutData("WDATM", 0, "*DAT");
          break;
        case 2:
          lexique.HostFieldPutData("WDATM", 0, "*DME");
          break;
        case 3:
          lexique.HostFieldPutData("WDATM", 0, "*DMP");
          break;
        case 4:
          lexique.HostFieldPutData("WDATM", 0, "*FMP");
          break;
        case 5:
          lexique.HostFieldPutData("WDATM", 0, "*DAT" + tfNombreJours.getText());
          break;
        
        default:
          break;
      }
    }
  }
  
  private void modifierParametrageDate() {
    if (rbDate.isSelected()) {
      WDATM.setEnabled(true);
      WDATM.setDate(null);
      lbJours.setEnabled(false);
      tfNombreJours.setEnabled(false);
      cbDatePlanning.setSelectedIndex(0);
      cbDatePlanning.setEnabled(false);
    }
    else if (rbDateParametree.isSelected()) {
      lexique.HostFieldPutData("WDATM", 0, "");
      WDATM.setDate(null);
      WDATM.setEnabled(false);
      lbJours.setEnabled(true);
      tfNombreJours.setEnabled(true);
      cbDatePlanning.setEnabled(true);
    }
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    try {
      lexique.HostScreenSendKey(this, "F5");
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void cbDatePlanningItemStateChanged(ItemEvent e) {
    try {
      if (cbDatePlanning.getSelectedItem() != null && cbDatePlanning.getSelectedItem().equals("Date du jour + nn jours")) {
        lbJours.setVisible(true);
        tfNombreJours.setVisible(true);
      }
      else {
        lbJours.setVisible(false);
        tfNombreJours.setVisible(false);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void lbDateItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void lbDateParametreeItemStateChanged(ItemEvent e) {
    try {
      modifierParametrageDate();
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlMessage = new SNPanel();
    lbPlanning = new SNMessage();
    lbLOCTP = new SNLabelTitre();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlCritereDeSelection = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbFournisseurDebut = new SNLabelChamp();
    snFournisseurDebut = new SNFournisseur();
    lbFournisseurFin = new SNLabelChamp();
    snFournisseurFin = new SNFournisseur();
    lbStatut = new SNLabelChamp();
    WSTAT = new XRiComboBox();
    pnlLibelleDate = new SNPanel();
    rbDate = new SNRadioButton();
    lbDate = new SNLabelChamp();
    WDATM = new XRiCalendrier();
    rbDateParametree = new SNRadioButton();
    pnlDateParametree = new SNPanel();
    cbDatePlanning = new SNComboBox();
    lbJours = new SNLabelChamp();
    tfNombreJours = new SNNombreEntier();
    pnlDroite = new SNPanel();
    pnlEtablissement = new SNPanelTitre();
    tfEnCours = new SNTexte();
    snEtablissement = new SNEtablissement();
    lbEtablissement = new SNLabelChamp();
    lbPeriode = new SNLabelChamp();
    snBarreBouton = new SNBarreBouton();
    buttonGroup1 = new ButtonGroup();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(915, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("@TITPG1@ @TITPG2@");
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new BorderLayout());
      
      // ======== pnlMessage ========
      {
        pnlMessage.setName("pnlMessage");
        pnlMessage.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlMessage.getLayout()).columnWidths = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).rowHeights = new int[] { 0, 0 };
        ((GridBagLayout) pnlMessage.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
        ((GridBagLayout) pnlMessage.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
        
        // ---- lbPlanning ----
        lbPlanning.setText("Label Planning");
        lbPlanning.setName("lbPlanning");
        pnlMessage.add(lbPlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbLOCTP ----
        lbLOCTP.setText("@LOCTP@");
        lbLOCTP.setMinimumSize(new Dimension(120, 30));
        lbLOCTP.setPreferredSize(new Dimension(120, 30));
        lbLOCTP.setHorizontalTextPosition(SwingConstants.RIGHT);
        lbLOCTP.setName("lbLOCTP");
        pnlMessage.add(lbLOCTP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlContenu.add(pnlMessage, BorderLayout.NORTH);
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlCritereDeSelection ========
          {
            pnlCritereDeSelection.setTitre("Crit\u00e8res de s\u00e9lection");
            pnlCritereDeSelection.setName("pnlCritereDeSelection");
            pnlCritereDeSelection.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlCritereDeSelection.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setName("lbMagasin");
            pnlCritereDeSelection.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setFont(new Font("sansserif", Font.PLAIN, 14));
            snMagasin.setName("snMagasin");
            pnlCritereDeSelection.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurDebut ----
            lbFournisseurDebut.setText("Fournisseur de d\u00e9but");
            lbFournisseurDebut.setName("lbFournisseurDebut");
            pnlCritereDeSelection.add(lbFournisseurDebut, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurDebut ----
            snFournisseurDebut.setName("snFournisseurDebut");
            pnlCritereDeSelection.add(snFournisseurDebut, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbFournisseurFin ----
            lbFournisseurFin.setText("Fournisseur de fin");
            lbFournisseurFin.setName("lbFournisseurFin");
            pnlCritereDeSelection.add(lbFournisseurFin, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snFournisseurFin ----
            snFournisseurFin.setName("snFournisseurFin");
            pnlCritereDeSelection.add(snFournisseurFin, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbStatut ----
            lbStatut.setText("Statut \u00e0 \u00e9diter");
            lbStatut.setName("lbStatut");
            pnlCritereDeSelection.add(lbStatut, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WSTAT ----
            WSTAT.setFont(new Font("sansserif", Font.PLAIN, 14));
            WSTAT.setBackground(Color.white);
            WSTAT.setPreferredSize(new Dimension(150, 30));
            WSTAT.setMinimumSize(new Dimension(150, 30));
            WSTAT.setMaximumSize(new Dimension(200, 30));
            WSTAT.setModel(new DefaultComboBoxModel<>(new String[] { "En attente", "Valid\u00e9" }));
            WSTAT.setName("WSTAT");
            pnlCritereDeSelection.add(WSTAT, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ======== pnlLibelleDate ========
            {
              pnlLibelleDate.setName("pnlLibelleDate");
              pnlLibelleDate.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).columnWeights = new double[] { 1.0, 1.0, 1.0E-4 };
              ((GridBagLayout) pnlLibelleDate.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- rbDate ----
              rbDate.setText("Date minimum pour envoi");
              rbDate.setPreferredSize(new Dimension(200, 30));
              rbDate.setMinimumSize(new Dimension(200, 30));
              rbDate.setMaximumSize(new Dimension(250, 30));
              rbDate.setName("rbDate");
              rbDate.addItemListener(e -> lbDateItemStateChanged(e));
              pnlLibelleDate.add(rbDate, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbDate ----
              lbDate.setText("Date minimum pour envoi");
              lbDate.setMaximumSize(new Dimension(200, 30));
              lbDate.setMinimumSize(new Dimension(200, 30));
              lbDate.setPreferredSize(new Dimension(200, 30));
              lbDate.setName("lbDate");
              pnlLibelleDate.add(lbDate, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                  new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlLibelleDate, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- WDATM ----
            WDATM.setMinimumSize(new Dimension(115, 30));
            WDATM.setMaximumSize(new Dimension(115, 30));
            WDATM.setPreferredSize(new Dimension(115, 30));
            WDATM.setFont(new Font("sansserif", Font.PLAIN, 14));
            WDATM.setName("WDATM");
            pnlCritereDeSelection.add(WDATM, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- rbDateParametree ----
            rbDateParametree.setText("ou date param\u00e9tr\u00e9e");
            rbDateParametree.setPreferredSize(new Dimension(200, 30));
            rbDateParametree.setMinimumSize(new Dimension(200, 30));
            rbDateParametree.setMaximumSize(new Dimension(250, 30));
            rbDateParametree.setName("rbDateParametree");
            rbDateParametree.addItemListener(e -> lbDateParametreeItemStateChanged(e));
            pnlCritereDeSelection.add(rbDateParametree, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ======== pnlDateParametree ========
            {
              pnlDateParametree.setName("pnlDateParametree");
              pnlDateParametree.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlDateParametree.getLayout()).columnWeights = new double[] { 1.0, 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlDateParametree.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- cbDatePlanning ----
              cbDatePlanning.setName("cbDatePlanning");
              cbDatePlanning.addItemListener(e -> cbDatePlanningItemStateChanged(e));
              pnlDateParametree.add(cbDatePlanning, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- lbJours ----
              lbJours.setText(" jours");
              lbJours.setMaximumSize(new Dimension(50, 30));
              lbJours.setPreferredSize(new Dimension(50, 30));
              lbJours.setMinimumSize(new Dimension(50, 30));
              lbJours.setName("lbJours");
              pnlDateParametree.add(lbJours, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfNombreJours ----
              tfNombreJours.setMaximumSize(new Dimension(50, 30));
              tfNombreJours.setMinimumSize(new Dimension(50, 30));
              tfNombreJours.setPreferredSize(new Dimension(50, 30));
              tfNombreJours.setName("tfNombreJours");
              pnlDateParametree.add(tfNombreJours, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlCritereDeSelection.add(pnlDateParametree, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlCritereDeSelection, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissement ========
          {
            pnlEtablissement.setTitre("Etablissement");
            pnlEtablissement.setName("pnlEtablissement");
            pnlEtablissement.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissement.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissement.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- tfEnCours ----
            tfEnCours.setText("@WENCX@");
            tfEnCours.setEnabled(false);
            tfEnCours.setMinimumSize(new Dimension(260, 30));
            tfEnCours.setPreferredSize(new Dimension(260, 30));
            tfEnCours.setName("tfEnCours");
            pnlEtablissement.add(tfEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(e -> snEtablissementValueChanged(e));
            pnlEtablissement.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissement.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- lbPeriode ----
            lbPeriode.setText("P\u00e9riode en cours");
            lbPeriode.setName("lbPeriode");
            pnlEtablissement.add(lbPeriode, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
          }
          pnlDroite.add(pnlEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne, BorderLayout.CENTER);
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ---- buttonGroup1 ----
    buttonGroup1.add(rbDate);
    buttonGroup1.add(rbDateParametree);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlMessage;
  private SNMessage lbPlanning;
  private SNLabelTitre lbLOCTP;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlCritereDeSelection;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbFournisseurDebut;
  private SNFournisseur snFournisseurDebut;
  private SNLabelChamp lbFournisseurFin;
  private SNFournisseur snFournisseurFin;
  private SNLabelChamp lbStatut;
  private XRiComboBox WSTAT;
  private SNPanel pnlLibelleDate;
  private SNRadioButton rbDate;
  private SNLabelChamp lbDate;
  private XRiCalendrier WDATM;
  private SNRadioButton rbDateParametree;
  private SNPanel pnlDateParametree;
  private SNComboBox cbDatePlanning;
  private SNLabelChamp lbJours;
  private SNNombreEntier tfNombreJours;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissement;
  private SNTexte tfEnCours;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbEtablissement;
  private SNLabelChamp lbPeriode;
  private SNBarreBouton snBarreBouton;
  private ButtonGroup buttonGroup1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
