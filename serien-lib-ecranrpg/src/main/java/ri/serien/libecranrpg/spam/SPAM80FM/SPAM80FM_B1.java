
package ri.serien.libecranrpg.spam.SPAM80FM;
// Nom Fichier: i_SPAM80FM_FMTB1_FMTF1_414.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SPAM80FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SPAM80FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_173_OBJ_173.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    PCT67X.setEnabled(lexique.isPresent("PCT67X"));
    PCT66X.setEnabled(lexique.isPresent("PCT66X"));
    PCT65X.setEnabled(lexique.isPresent("PCT65X"));
    PCT64X.setEnabled(lexique.isPresent("PCT64X"));
    PCT63X.setEnabled(lexique.isPresent("PCT63X"));
    PCT62X.setEnabled(lexique.isPresent("PCT62X"));
    PCT61X.setEnabled(lexique.isPresent("PCT61X"));
    PCT60X.setEnabled(lexique.isPresent("PCT60X"));
    PCT17X.setEnabled(lexique.isPresent("PCT17X"));
    PCT59X.setEnabled(lexique.isPresent("PCT59X"));
    PCT16X.setEnabled(lexique.isPresent("PCT16X"));
    PCT58X.setEnabled(lexique.isPresent("PCT58X"));
    PCT15X.setEnabled(lexique.isPresent("PCT15X"));
    PCT57X.setEnabled(lexique.isPresent("PCT57X"));
    PCT14X.setEnabled(lexique.isPresent("PCT14X"));
    PCT56X.setEnabled(lexique.isPresent("PCT56X"));
    PCT13X.setEnabled(lexique.isPresent("PCT13X"));
    PCT12X.setEnabled(lexique.isPresent("PCT12X"));
    PCT55X.setEnabled(lexique.isPresent("PCT55X"));
    PCT11X.setEnabled(lexique.isPresent("PCT11X"));
    PCT54X.setEnabled(lexique.isPresent("PCT54X"));
    PCT10X.setEnabled(lexique.isPresent("PCT10X"));
    PCT53X.setEnabled(lexique.isPresent("PCT53X"));
    PCT09X.setEnabled(lexique.isPresent("PCT09X"));
    PCT52X.setEnabled(lexique.isPresent("PCT52X"));
    PCT25X.setEnabled(lexique.isPresent("PCT25X"));
    PCT08X.setEnabled(lexique.isPresent("PCT08X"));
    PCT51X.setEnabled(lexique.isPresent("PCT51X"));
    PCT24X.setEnabled(lexique.isPresent("PCT24X"));
    PCT07X.setEnabled(lexique.isPresent("PCT07X"));
    PCT23X.setEnabled(lexique.isPresent("PCT23X"));
    PCT06X.setEnabled(lexique.isPresent("PCT06X"));
    PCT22X.setEnabled(lexique.isPresent("PCT22X"));
    PCT05X.setEnabled(lexique.isPresent("PCT05X"));
    PCT21X.setEnabled(lexique.isPresent("PCT21X"));
    PCT04X.setEnabled(lexique.isPresent("PCT04X"));
    PCT20X.setEnabled(lexique.isPresent("PCT20X"));
    PCT03X.setEnabled(lexique.isPresent("PCT03X"));
    PCT19X.setEnabled(lexique.isPresent("PCT19X"));
    PCT02X.setEnabled(lexique.isPresent("PCT02X"));
    PCT18X.setEnabled(lexique.isPresent("PCT18X"));
    PCT01X.setEnabled(lexique.isPresent("PCT01X"));
    OBJ_173_OBJ_173.setVisible(lexique.isPresent("NOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - FICHE PERSONNEL"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_6ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "spam80"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_6 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    OBJ_173_OBJ_173 = new JLabel();
    OBJ_31_OBJ_31 = new JLabel();
    OBJ_32_OBJ_32 = new JLabel();
    PCETB = new XRiTextField();
    PCMATR = new XRiTextField();
    P_Centre = new JPanel();
    LIV60 = new XRiTextField();
    LIV65 = new XRiTextField();
    LIV01 = new XRiTextField();
    LIV18 = new XRiTextField();
    LIV02 = new XRiTextField();
    LIV19 = new XRiTextField();
    LIV03 = new XRiTextField();
    LIV20 = new XRiTextField();
    LIV04 = new XRiTextField();
    LIV21 = new XRiTextField();
    LIV05 = new XRiTextField();
    LIV22 = new XRiTextField();
    LIV06 = new XRiTextField();
    LIV23 = new XRiTextField();
    LIV07 = new XRiTextField();
    LIV24 = new XRiTextField();
    LIV51 = new XRiTextField();
    LIV08 = new XRiTextField();
    LIV25 = new XRiTextField();
    LIV52 = new XRiTextField();
    LIV09 = new XRiTextField();
    LIV53 = new XRiTextField();
    LIV10 = new XRiTextField();
    LIV54 = new XRiTextField();
    LIV11 = new XRiTextField();
    LIV55 = new XRiTextField();
    LIV12 = new XRiTextField();
    LIV13 = new XRiTextField();
    LIV56 = new XRiTextField();
    LIV14 = new XRiTextField();
    LIV57 = new XRiTextField();
    LIV15 = new XRiTextField();
    LIV58 = new XRiTextField();
    LIV16 = new XRiTextField();
    LIV59 = new XRiTextField();
    LIV17 = new XRiTextField();
    LIV61 = new XRiTextField();
    LIV62 = new XRiTextField();
    LIV63 = new XRiTextField();
    LIV64 = new XRiTextField();
    LIV66 = new XRiTextField();
    LIV67 = new XRiTextField();
    PCT01X = new XRiTextField();
    PCT18X = new XRiTextField();
    PCT02X = new XRiTextField();
    PCT19X = new XRiTextField();
    PCT03X = new XRiTextField();
    PCT20X = new XRiTextField();
    PCT04X = new XRiTextField();
    PCT21X = new XRiTextField();
    PCT05X = new XRiTextField();
    PCT22X = new XRiTextField();
    PCT06X = new XRiTextField();
    PCT23X = new XRiTextField();
    PCT07X = new XRiTextField();
    PCT24X = new XRiTextField();
    PCT51X = new XRiTextField();
    PCT08X = new XRiTextField();
    PCT25X = new XRiTextField();
    PCT52X = new XRiTextField();
    PCT09X = new XRiTextField();
    PCT53X = new XRiTextField();
    PCT10X = new XRiTextField();
    PCT54X = new XRiTextField();
    PCT11X = new XRiTextField();
    PCT55X = new XRiTextField();
    PCT12X = new XRiTextField();
    PCT13X = new XRiTextField();
    PCT56X = new XRiTextField();
    PCT14X = new XRiTextField();
    PCT57X = new XRiTextField();
    PCT15X = new XRiTextField();
    PCT58X = new XRiTextField();
    PCT16X = new XRiTextField();
    PCT59X = new XRiTextField();
    PCT17X = new XRiTextField();
    PCT60X = new XRiTextField();
    PCT61X = new XRiTextField();
    PCT62X = new XRiTextField();
    PCT63X = new XRiTextField();
    PCT64X = new XRiTextField();
    PCT65X = new XRiTextField();
    PCT66X = new XRiTextField();
    PCT67X = new XRiTextField();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    OBJ_56_OBJ_56 = new JLabel();
    OBJ_57_OBJ_57 = new JLabel();
    OBJ_62_OBJ_62 = new JLabel();
    OBJ_63_OBJ_63 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_77_OBJ_77 = new JLabel();
    OBJ_78_OBJ_78 = new JLabel();
    OBJ_85_OBJ_85 = new JLabel();
    OBJ_86_OBJ_86 = new JLabel();
    OBJ_91_OBJ_91 = new JLabel();
    OBJ_92_OBJ_92 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_112_OBJ_112 = new JLabel();
    OBJ_113_OBJ_113 = new JLabel();
    OBJ_119_OBJ_119 = new JLabel();
    OBJ_120_OBJ_120 = new JLabel();
    OBJ_126_OBJ_126 = new JLabel();
    OBJ_127_OBJ_127 = new JLabel();
    OBJ_132_OBJ_132 = new JLabel();
    OBJ_133_OBJ_133 = new JLabel();
    OBJ_139_OBJ_139 = new JLabel();
    OBJ_140_OBJ_140 = new JLabel();
    OBJ_144_OBJ_144 = new JLabel();
    OBJ_145_OBJ_145 = new JLabel();
    OBJ_148_OBJ_148 = new JLabel();
    OBJ_149_OBJ_149 = new JLabel();
    OBJ_152_OBJ_152 = new JLabel();
    OBJ_153_OBJ_153 = new JLabel();
    OBJ_155_OBJ_155 = new JLabel();
    OBJ_156_OBJ_156 = new JLabel();
    OBJ_158_OBJ_158 = new JLabel();
    OBJ_159_OBJ_159 = new JLabel();
    OBJ_161_OBJ_161 = new JLabel();
    OBJ_162_OBJ_162 = new JLabel();
    OBJ_164_OBJ_164 = new JLabel();
    OBJ_165_OBJ_165 = new JLabel();
    OBJ_172_OBJ_172 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    xTitledSeparator2 = new JXTitledSeparator();
    LIV2 = new XRiTextField();
    LIV26 = new XRiTextField();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_6 ----
      OBJ_6.setText("R\u00e9afficher");
      OBJ_6.setName("OBJ_6");
      OBJ_6.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_6ActionPerformed(e);
        }
      });
      CMD.add(OBJ_6);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("FICHE PERSONNEL");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });
        P_Infos.add(bt_Fonctions);
        bt_Fonctions.setBounds(883, 5, 115, bt_Fonctions.getPreferredSize().height);

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");
        P_Infos.add(l_V01F);
        l_V01F.setBounds(21, 9, 80, 20);

        //---- OBJ_173_OBJ_173 ----
        OBJ_173_OBJ_173.setText("@NOM@");
        OBJ_173_OBJ_173.setName("OBJ_173_OBJ_173");
        P_Infos.add(OBJ_173_OBJ_173);
        OBJ_173_OBJ_173.setBounds(365, 10, 285, 16);

        //---- OBJ_31_OBJ_31 ----
        OBJ_31_OBJ_31.setText("Etablissement");
        OBJ_31_OBJ_31.setName("OBJ_31_OBJ_31");
        P_Infos.add(OBJ_31_OBJ_31);
        OBJ_31_OBJ_31.setBounds(95, 10, 93, 16);

        //---- OBJ_32_OBJ_32 ----
        OBJ_32_OBJ_32.setText("Matricule");
        OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
        P_Infos.add(OBJ_32_OBJ_32);
        OBJ_32_OBJ_32.setBounds(245, 10, 63, 16);

        //---- PCETB ----
        PCETB.setName("PCETB");
        P_Infos.add(PCETB);
        PCETB.setBounds(195, 5, 40, PCETB.getPreferredSize().height);

        //---- PCMATR ----
        PCMATR.setName("PCMATR");
        P_Infos.add(PCMATR);
        PCMATR.setBounds(320, 5, 36, PCMATR.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //---- LIV60 ----
      LIV60.setName("LIV60");
      P_Centre.add(LIV60);
      LIV60.setBounds(375, 355, 172, LIV60.getPreferredSize().height);

      //---- LIV65 ----
      LIV65.setName("LIV65");
      P_Centre.add(LIV65);
      LIV65.setBounds(375, 480, 172, LIV65.getPreferredSize().height);

      //---- LIV01 ----
      LIV01.setName("LIV01");
      P_Centre.add(LIV01);
      LIV01.setBounds(55, 30, 172, LIV01.getPreferredSize().height);

      //---- LIV18 ----
      LIV18.setName("LIV18");
      P_Centre.add(LIV18);
      LIV18.setBounds(55, 455, 172, LIV18.getPreferredSize().height);

      //---- LIV02 ----
      LIV02.setName("LIV02");
      P_Centre.add(LIV02);
      LIV02.setBounds(55, 55, 172, LIV02.getPreferredSize().height);

      //---- LIV19 ----
      LIV19.setName("LIV19");
      P_Centre.add(LIV19);
      LIV19.setBounds(55, 480, 172, LIV19.getPreferredSize().height);

      //---- LIV03 ----
      LIV03.setName("LIV03");
      P_Centre.add(LIV03);
      LIV03.setBounds(55, 80, 172, LIV03.getPreferredSize().height);

      //---- LIV20 ----
      LIV20.setName("LIV20");
      P_Centre.add(LIV20);
      LIV20.setBounds(55, 505, 172, LIV20.getPreferredSize().height);

      //---- LIV04 ----
      LIV04.setName("LIV04");
      P_Centre.add(LIV04);
      LIV04.setBounds(55, 105, 172, LIV04.getPreferredSize().height);

      //---- LIV21 ----
      LIV21.setName("LIV21");
      P_Centre.add(LIV21);
      LIV21.setBounds(55, 530, 172, LIV21.getPreferredSize().height);

      //---- LIV05 ----
      LIV05.setName("LIV05");
      P_Centre.add(LIV05);
      LIV05.setBounds(55, 130, 172, LIV05.getPreferredSize().height);

      //---- LIV22 ----
      LIV22.setName("LIV22");
      P_Centre.add(LIV22);
      LIV22.setBounds(375, 30, 172, LIV22.getPreferredSize().height);

      //---- LIV06 ----
      LIV06.setName("LIV06");
      P_Centre.add(LIV06);
      LIV06.setBounds(55, 155, 172, LIV06.getPreferredSize().height);

      //---- LIV23 ----
      LIV23.setName("LIV23");
      P_Centre.add(LIV23);
      LIV23.setBounds(375, 55, 172, LIV23.getPreferredSize().height);

      //---- LIV07 ----
      LIV07.setName("LIV07");
      P_Centre.add(LIV07);
      LIV07.setBounds(55, 180, 172, LIV07.getPreferredSize().height);

      //---- LIV24 ----
      LIV24.setName("LIV24");
      P_Centre.add(LIV24);
      LIV24.setBounds(375, 80, 172, LIV24.getPreferredSize().height);

      //---- LIV51 ----
      LIV51.setName("LIV51");
      P_Centre.add(LIV51);
      LIV51.setBounds(375, 130, 172, LIV51.getPreferredSize().height);

      //---- LIV08 ----
      LIV08.setName("LIV08");
      P_Centre.add(LIV08);
      LIV08.setBounds(55, 205, 172, LIV08.getPreferredSize().height);

      //---- LIV25 ----
      LIV25.setName("LIV25");
      P_Centre.add(LIV25);
      LIV25.setBounds(375, 105, 172, LIV25.getPreferredSize().height);

      //---- LIV52 ----
      LIV52.setName("LIV52");
      P_Centre.add(LIV52);
      LIV52.setBounds(375, 155, 172, LIV52.getPreferredSize().height);

      //---- LIV09 ----
      LIV09.setName("LIV09");
      P_Centre.add(LIV09);
      LIV09.setBounds(55, 230, 172, LIV09.getPreferredSize().height);

      //---- LIV53 ----
      LIV53.setName("LIV53");
      P_Centre.add(LIV53);
      LIV53.setBounds(375, 180, 172, LIV53.getPreferredSize().height);

      //---- LIV10 ----
      LIV10.setName("LIV10");
      P_Centre.add(LIV10);
      LIV10.setBounds(55, 255, 172, LIV10.getPreferredSize().height);

      //---- LIV54 ----
      LIV54.setName("LIV54");
      P_Centre.add(LIV54);
      LIV54.setBounds(375, 205, 172, LIV54.getPreferredSize().height);

      //---- LIV11 ----
      LIV11.setName("LIV11");
      P_Centre.add(LIV11);
      LIV11.setBounds(55, 280, 172, LIV11.getPreferredSize().height);

      //---- LIV55 ----
      LIV55.setName("LIV55");
      P_Centre.add(LIV55);
      LIV55.setBounds(375, 230, 172, LIV55.getPreferredSize().height);

      //---- LIV12 ----
      LIV12.setName("LIV12");
      P_Centre.add(LIV12);
      LIV12.setBounds(55, 305, 172, LIV12.getPreferredSize().height);

      //---- LIV13 ----
      LIV13.setName("LIV13");
      P_Centre.add(LIV13);
      LIV13.setBounds(55, 330, 172, LIV13.getPreferredSize().height);

      //---- LIV56 ----
      LIV56.setName("LIV56");
      P_Centre.add(LIV56);
      LIV56.setBounds(375, 255, 172, LIV56.getPreferredSize().height);

      //---- LIV14 ----
      LIV14.setName("LIV14");
      P_Centre.add(LIV14);
      LIV14.setBounds(55, 355, 172, LIV14.getPreferredSize().height);

      //---- LIV57 ----
      LIV57.setName("LIV57");
      P_Centre.add(LIV57);
      LIV57.setBounds(375, 280, 172, LIV57.getPreferredSize().height);

      //---- LIV15 ----
      LIV15.setName("LIV15");
      P_Centre.add(LIV15);
      LIV15.setBounds(55, 380, 172, LIV15.getPreferredSize().height);

      //---- LIV58 ----
      LIV58.setName("LIV58");
      P_Centre.add(LIV58);
      LIV58.setBounds(375, 305, 172, LIV58.getPreferredSize().height);

      //---- LIV16 ----
      LIV16.setName("LIV16");
      P_Centre.add(LIV16);
      LIV16.setBounds(55, 405, 172, LIV16.getPreferredSize().height);

      //---- LIV59 ----
      LIV59.setName("LIV59");
      P_Centre.add(LIV59);
      LIV59.setBounds(375, 330, 172, LIV59.getPreferredSize().height);

      //---- LIV17 ----
      LIV17.setName("LIV17");
      P_Centre.add(LIV17);
      LIV17.setBounds(55, 430, 172, LIV17.getPreferredSize().height);

      //---- LIV61 ----
      LIV61.setName("LIV61");
      P_Centre.add(LIV61);
      LIV61.setBounds(375, 380, 172, LIV61.getPreferredSize().height);

      //---- LIV62 ----
      LIV62.setName("LIV62");
      P_Centre.add(LIV62);
      LIV62.setBounds(375, 405, 172, LIV62.getPreferredSize().height);

      //---- LIV63 ----
      LIV63.setName("LIV63");
      P_Centre.add(LIV63);
      LIV63.setBounds(375, 430, 172, LIV63.getPreferredSize().height);

      //---- LIV64 ----
      LIV64.setName("LIV64");
      P_Centre.add(LIV64);
      LIV64.setBounds(375, 455, 172, LIV64.getPreferredSize().height);

      //---- LIV66 ----
      LIV66.setName("LIV66");
      P_Centre.add(LIV66);
      LIV66.setBounds(375, 505, 172, LIV66.getPreferredSize().height);

      //---- LIV67 ----
      LIV67.setName("LIV67");
      P_Centre.add(LIV67);
      LIV67.setBounds(375, 530, 172, LIV67.getPreferredSize().height);

      //---- PCT01X ----
      PCT01X.setComponentPopupMenu(BTD);
      PCT01X.setName("PCT01X");
      P_Centre.add(PCT01X);
      PCT01X.setBounds(230, 30, 110, PCT01X.getPreferredSize().height);

      //---- PCT18X ----
      PCT18X.setComponentPopupMenu(BTD);
      PCT18X.setName("PCT18X");
      P_Centre.add(PCT18X);
      PCT18X.setBounds(230, 455, 110, PCT18X.getPreferredSize().height);

      //---- PCT02X ----
      PCT02X.setComponentPopupMenu(BTD);
      PCT02X.setName("PCT02X");
      P_Centre.add(PCT02X);
      PCT02X.setBounds(230, 55, 110, PCT02X.getPreferredSize().height);

      //---- PCT19X ----
      PCT19X.setComponentPopupMenu(BTD);
      PCT19X.setName("PCT19X");
      P_Centre.add(PCT19X);
      PCT19X.setBounds(230, 480, 110, PCT19X.getPreferredSize().height);

      //---- PCT03X ----
      PCT03X.setComponentPopupMenu(BTD);
      PCT03X.setName("PCT03X");
      P_Centre.add(PCT03X);
      PCT03X.setBounds(230, 80, 110, PCT03X.getPreferredSize().height);

      //---- PCT20X ----
      PCT20X.setComponentPopupMenu(BTD);
      PCT20X.setName("PCT20X");
      P_Centre.add(PCT20X);
      PCT20X.setBounds(230, 505, 110, PCT20X.getPreferredSize().height);

      //---- PCT04X ----
      PCT04X.setComponentPopupMenu(BTD);
      PCT04X.setName("PCT04X");
      P_Centre.add(PCT04X);
      PCT04X.setBounds(230, 105, 110, PCT04X.getPreferredSize().height);

      //---- PCT21X ----
      PCT21X.setComponentPopupMenu(BTD);
      PCT21X.setName("PCT21X");
      P_Centre.add(PCT21X);
      PCT21X.setBounds(230, 530, 110, PCT21X.getPreferredSize().height);

      //---- PCT05X ----
      PCT05X.setComponentPopupMenu(BTD);
      PCT05X.setName("PCT05X");
      P_Centre.add(PCT05X);
      PCT05X.setBounds(230, 130, 110, PCT05X.getPreferredSize().height);

      //---- PCT22X ----
      PCT22X.setComponentPopupMenu(BTD);
      PCT22X.setName("PCT22X");
      P_Centre.add(PCT22X);
      PCT22X.setBounds(555, 30, 110, PCT22X.getPreferredSize().height);

      //---- PCT06X ----
      PCT06X.setComponentPopupMenu(BTD);
      PCT06X.setName("PCT06X");
      P_Centre.add(PCT06X);
      PCT06X.setBounds(230, 155, 110, PCT06X.getPreferredSize().height);

      //---- PCT23X ----
      PCT23X.setComponentPopupMenu(BTD);
      PCT23X.setName("PCT23X");
      P_Centre.add(PCT23X);
      PCT23X.setBounds(555, 55, 110, PCT23X.getPreferredSize().height);

      //---- PCT07X ----
      PCT07X.setComponentPopupMenu(BTD);
      PCT07X.setName("PCT07X");
      P_Centre.add(PCT07X);
      PCT07X.setBounds(230, 180, 110, PCT07X.getPreferredSize().height);

      //---- PCT24X ----
      PCT24X.setComponentPopupMenu(BTD);
      PCT24X.setName("PCT24X");
      P_Centre.add(PCT24X);
      PCT24X.setBounds(555, 80, 110, PCT24X.getPreferredSize().height);

      //---- PCT51X ----
      PCT51X.setComponentPopupMenu(BTD);
      PCT51X.setName("PCT51X");
      P_Centre.add(PCT51X);
      PCT51X.setBounds(555, 130, 110, PCT51X.getPreferredSize().height);

      //---- PCT08X ----
      PCT08X.setComponentPopupMenu(BTD);
      PCT08X.setName("PCT08X");
      P_Centre.add(PCT08X);
      PCT08X.setBounds(230, 205, 110, PCT08X.getPreferredSize().height);

      //---- PCT25X ----
      PCT25X.setComponentPopupMenu(BTD);
      PCT25X.setName("PCT25X");
      P_Centre.add(PCT25X);
      PCT25X.setBounds(555, 105, 110, PCT25X.getPreferredSize().height);

      //---- PCT52X ----
      PCT52X.setComponentPopupMenu(BTD);
      PCT52X.setName("PCT52X");
      P_Centre.add(PCT52X);
      PCT52X.setBounds(555, 155, 110, PCT52X.getPreferredSize().height);

      //---- PCT09X ----
      PCT09X.setComponentPopupMenu(BTD);
      PCT09X.setName("PCT09X");
      P_Centre.add(PCT09X);
      PCT09X.setBounds(230, 230, 110, PCT09X.getPreferredSize().height);

      //---- PCT53X ----
      PCT53X.setComponentPopupMenu(BTD);
      PCT53X.setName("PCT53X");
      P_Centre.add(PCT53X);
      PCT53X.setBounds(555, 180, 110, PCT53X.getPreferredSize().height);

      //---- PCT10X ----
      PCT10X.setComponentPopupMenu(BTD);
      PCT10X.setName("PCT10X");
      P_Centre.add(PCT10X);
      PCT10X.setBounds(230, 255, 110, PCT10X.getPreferredSize().height);

      //---- PCT54X ----
      PCT54X.setComponentPopupMenu(BTD);
      PCT54X.setName("PCT54X");
      P_Centre.add(PCT54X);
      PCT54X.setBounds(555, 205, 110, PCT54X.getPreferredSize().height);

      //---- PCT11X ----
      PCT11X.setComponentPopupMenu(BTD);
      PCT11X.setName("PCT11X");
      P_Centre.add(PCT11X);
      PCT11X.setBounds(230, 280, 110, PCT11X.getPreferredSize().height);

      //---- PCT55X ----
      PCT55X.setComponentPopupMenu(BTD);
      PCT55X.setName("PCT55X");
      P_Centre.add(PCT55X);
      PCT55X.setBounds(555, 230, 110, PCT55X.getPreferredSize().height);

      //---- PCT12X ----
      PCT12X.setComponentPopupMenu(BTD);
      PCT12X.setName("PCT12X");
      P_Centre.add(PCT12X);
      PCT12X.setBounds(230, 305, 110, PCT12X.getPreferredSize().height);

      //---- PCT13X ----
      PCT13X.setComponentPopupMenu(BTD);
      PCT13X.setName("PCT13X");
      P_Centre.add(PCT13X);
      PCT13X.setBounds(230, 330, 110, PCT13X.getPreferredSize().height);

      //---- PCT56X ----
      PCT56X.setComponentPopupMenu(BTD);
      PCT56X.setName("PCT56X");
      P_Centre.add(PCT56X);
      PCT56X.setBounds(555, 255, 110, PCT56X.getPreferredSize().height);

      //---- PCT14X ----
      PCT14X.setComponentPopupMenu(BTD);
      PCT14X.setName("PCT14X");
      P_Centre.add(PCT14X);
      PCT14X.setBounds(230, 355, 110, PCT14X.getPreferredSize().height);

      //---- PCT57X ----
      PCT57X.setComponentPopupMenu(BTD);
      PCT57X.setName("PCT57X");
      P_Centre.add(PCT57X);
      PCT57X.setBounds(555, 280, 110, PCT57X.getPreferredSize().height);

      //---- PCT15X ----
      PCT15X.setComponentPopupMenu(BTD);
      PCT15X.setName("PCT15X");
      P_Centre.add(PCT15X);
      PCT15X.setBounds(230, 380, 110, PCT15X.getPreferredSize().height);

      //---- PCT58X ----
      PCT58X.setComponentPopupMenu(BTD);
      PCT58X.setName("PCT58X");
      P_Centre.add(PCT58X);
      PCT58X.setBounds(555, 305, 110, PCT58X.getPreferredSize().height);

      //---- PCT16X ----
      PCT16X.setComponentPopupMenu(BTD);
      PCT16X.setName("PCT16X");
      P_Centre.add(PCT16X);
      PCT16X.setBounds(230, 405, 110, PCT16X.getPreferredSize().height);

      //---- PCT59X ----
      PCT59X.setComponentPopupMenu(BTD);
      PCT59X.setName("PCT59X");
      P_Centre.add(PCT59X);
      PCT59X.setBounds(555, 330, 110, PCT59X.getPreferredSize().height);

      //---- PCT17X ----
      PCT17X.setComponentPopupMenu(BTD);
      PCT17X.setName("PCT17X");
      P_Centre.add(PCT17X);
      PCT17X.setBounds(230, 430, 110, PCT17X.getPreferredSize().height);

      //---- PCT60X ----
      PCT60X.setComponentPopupMenu(BTD);
      PCT60X.setName("PCT60X");
      P_Centre.add(PCT60X);
      PCT60X.setBounds(555, 355, 110, PCT60X.getPreferredSize().height);

      //---- PCT61X ----
      PCT61X.setComponentPopupMenu(BTD);
      PCT61X.setName("PCT61X");
      P_Centre.add(PCT61X);
      PCT61X.setBounds(555, 380, 110, PCT61X.getPreferredSize().height);

      //---- PCT62X ----
      PCT62X.setComponentPopupMenu(BTD);
      PCT62X.setName("PCT62X");
      P_Centre.add(PCT62X);
      PCT62X.setBounds(555, 405, 110, PCT62X.getPreferredSize().height);

      //---- PCT63X ----
      PCT63X.setComponentPopupMenu(BTD);
      PCT63X.setName("PCT63X");
      P_Centre.add(PCT63X);
      PCT63X.setBounds(555, 430, 110, PCT63X.getPreferredSize().height);

      //---- PCT64X ----
      PCT64X.setComponentPopupMenu(BTD);
      PCT64X.setName("PCT64X");
      P_Centre.add(PCT64X);
      PCT64X.setBounds(555, 455, 110, PCT64X.getPreferredSize().height);

      //---- PCT65X ----
      PCT65X.setComponentPopupMenu(BTD);
      PCT65X.setName("PCT65X");
      P_Centre.add(PCT65X);
      PCT65X.setBounds(555, 480, 110, PCT65X.getPreferredSize().height);

      //---- PCT66X ----
      PCT66X.setComponentPopupMenu(BTD);
      PCT66X.setName("PCT66X");
      P_Centre.add(PCT66X);
      PCT66X.setBounds(555, 505, 110, PCT66X.getPreferredSize().height);

      //---- PCT67X ----
      PCT67X.setComponentPopupMenu(BTD);
      PCT67X.setName("PCT67X");
      P_Centre.add(PCT67X);
      PCT67X.setBounds(555, 530, 110, PCT67X.getPreferredSize().height);

      //---- OBJ_48_OBJ_48 ----
      OBJ_48_OBJ_48.setText("01");
      OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
      P_Centre.add(OBJ_48_OBJ_48);
      OBJ_48_OBJ_48.setBounds(30, 35, 19, 18);

      //---- OBJ_49_OBJ_49 ----
      OBJ_49_OBJ_49.setText("22");
      OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
      P_Centre.add(OBJ_49_OBJ_49);
      OBJ_49_OBJ_49.setBounds(350, 35, 19, 18);

      //---- OBJ_56_OBJ_56 ----
      OBJ_56_OBJ_56.setText("02");
      OBJ_56_OBJ_56.setName("OBJ_56_OBJ_56");
      P_Centre.add(OBJ_56_OBJ_56);
      OBJ_56_OBJ_56.setBounds(30, 60, 19, 18);

      //---- OBJ_57_OBJ_57 ----
      OBJ_57_OBJ_57.setText("23");
      OBJ_57_OBJ_57.setName("OBJ_57_OBJ_57");
      P_Centre.add(OBJ_57_OBJ_57);
      OBJ_57_OBJ_57.setBounds(350, 60, 19, 18);

      //---- OBJ_62_OBJ_62 ----
      OBJ_62_OBJ_62.setText("03");
      OBJ_62_OBJ_62.setName("OBJ_62_OBJ_62");
      P_Centre.add(OBJ_62_OBJ_62);
      OBJ_62_OBJ_62.setBounds(30, 85, 19, 18);

      //---- OBJ_63_OBJ_63 ----
      OBJ_63_OBJ_63.setText("24");
      OBJ_63_OBJ_63.setName("OBJ_63_OBJ_63");
      P_Centre.add(OBJ_63_OBJ_63);
      OBJ_63_OBJ_63.setBounds(350, 85, 19, 18);

      //---- OBJ_68_OBJ_68 ----
      OBJ_68_OBJ_68.setText("04");
      OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
      P_Centre.add(OBJ_68_OBJ_68);
      OBJ_68_OBJ_68.setBounds(30, 110, 19, 18);

      //---- OBJ_77_OBJ_77 ----
      OBJ_77_OBJ_77.setText("05");
      OBJ_77_OBJ_77.setName("OBJ_77_OBJ_77");
      P_Centre.add(OBJ_77_OBJ_77);
      OBJ_77_OBJ_77.setBounds(30, 135, 19, 18);

      //---- OBJ_78_OBJ_78 ----
      OBJ_78_OBJ_78.setText("25");
      OBJ_78_OBJ_78.setName("OBJ_78_OBJ_78");
      P_Centre.add(OBJ_78_OBJ_78);
      OBJ_78_OBJ_78.setBounds(350, 110, 19, 18);

      //---- OBJ_85_OBJ_85 ----
      OBJ_85_OBJ_85.setText("06");
      OBJ_85_OBJ_85.setName("OBJ_85_OBJ_85");
      P_Centre.add(OBJ_85_OBJ_85);
      OBJ_85_OBJ_85.setBounds(30, 160, 19, 18);

      //---- OBJ_86_OBJ_86 ----
      OBJ_86_OBJ_86.setText("52");
      OBJ_86_OBJ_86.setName("OBJ_86_OBJ_86");
      P_Centre.add(OBJ_86_OBJ_86);
      OBJ_86_OBJ_86.setBounds(350, 160, 19, 18);

      //---- OBJ_91_OBJ_91 ----
      OBJ_91_OBJ_91.setText("07");
      OBJ_91_OBJ_91.setName("OBJ_91_OBJ_91");
      P_Centre.add(OBJ_91_OBJ_91);
      OBJ_91_OBJ_91.setBounds(30, 185, 19, 18);

      //---- OBJ_92_OBJ_92 ----
      OBJ_92_OBJ_92.setText("53");
      OBJ_92_OBJ_92.setName("OBJ_92_OBJ_92");
      P_Centre.add(OBJ_92_OBJ_92);
      OBJ_92_OBJ_92.setBounds(350, 185, 19, 18);

      //---- OBJ_97_OBJ_97 ----
      OBJ_97_OBJ_97.setText("08");
      OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
      P_Centre.add(OBJ_97_OBJ_97);
      OBJ_97_OBJ_97.setBounds(30, 210, 19, 18);

      //---- OBJ_98_OBJ_98 ----
      OBJ_98_OBJ_98.setText("54");
      OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
      P_Centre.add(OBJ_98_OBJ_98);
      OBJ_98_OBJ_98.setBounds(350, 210, 19, 18);

      //---- OBJ_104_OBJ_104 ----
      OBJ_104_OBJ_104.setText("09");
      OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
      P_Centre.add(OBJ_104_OBJ_104);
      OBJ_104_OBJ_104.setBounds(30, 230, 19, 28);

      //---- OBJ_105_OBJ_105 ----
      OBJ_105_OBJ_105.setText("55");
      OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
      P_Centre.add(OBJ_105_OBJ_105);
      OBJ_105_OBJ_105.setBounds(350, 230, 19, 28);

      //---- OBJ_112_OBJ_112 ----
      OBJ_112_OBJ_112.setText("10");
      OBJ_112_OBJ_112.setName("OBJ_112_OBJ_112");
      P_Centre.add(OBJ_112_OBJ_112);
      OBJ_112_OBJ_112.setBounds(30, 260, 19, 18);

      //---- OBJ_113_OBJ_113 ----
      OBJ_113_OBJ_113.setText("56");
      OBJ_113_OBJ_113.setName("OBJ_113_OBJ_113");
      P_Centre.add(OBJ_113_OBJ_113);
      OBJ_113_OBJ_113.setBounds(350, 260, 19, 18);

      //---- OBJ_119_OBJ_119 ----
      OBJ_119_OBJ_119.setText("11");
      OBJ_119_OBJ_119.setName("OBJ_119_OBJ_119");
      P_Centre.add(OBJ_119_OBJ_119);
      OBJ_119_OBJ_119.setBounds(30, 285, 19, 18);

      //---- OBJ_120_OBJ_120 ----
      OBJ_120_OBJ_120.setText("57");
      OBJ_120_OBJ_120.setName("OBJ_120_OBJ_120");
      P_Centre.add(OBJ_120_OBJ_120);
      OBJ_120_OBJ_120.setBounds(350, 285, 19, 18);

      //---- OBJ_126_OBJ_126 ----
      OBJ_126_OBJ_126.setText("12");
      OBJ_126_OBJ_126.setName("OBJ_126_OBJ_126");
      P_Centre.add(OBJ_126_OBJ_126);
      OBJ_126_OBJ_126.setBounds(30, 310, 19, 18);

      //---- OBJ_127_OBJ_127 ----
      OBJ_127_OBJ_127.setText("58");
      OBJ_127_OBJ_127.setName("OBJ_127_OBJ_127");
      P_Centre.add(OBJ_127_OBJ_127);
      OBJ_127_OBJ_127.setBounds(350, 310, 19, 18);

      //---- OBJ_132_OBJ_132 ----
      OBJ_132_OBJ_132.setText("13");
      OBJ_132_OBJ_132.setName("OBJ_132_OBJ_132");
      P_Centre.add(OBJ_132_OBJ_132);
      OBJ_132_OBJ_132.setBounds(30, 335, 19, 18);

      //---- OBJ_133_OBJ_133 ----
      OBJ_133_OBJ_133.setText("59");
      OBJ_133_OBJ_133.setName("OBJ_133_OBJ_133");
      P_Centre.add(OBJ_133_OBJ_133);
      OBJ_133_OBJ_133.setBounds(350, 335, 19, 18);

      //---- OBJ_139_OBJ_139 ----
      OBJ_139_OBJ_139.setText("14");
      OBJ_139_OBJ_139.setName("OBJ_139_OBJ_139");
      P_Centre.add(OBJ_139_OBJ_139);
      OBJ_139_OBJ_139.setBounds(30, 360, 19, 18);

      //---- OBJ_140_OBJ_140 ----
      OBJ_140_OBJ_140.setText("60");
      OBJ_140_OBJ_140.setName("OBJ_140_OBJ_140");
      P_Centre.add(OBJ_140_OBJ_140);
      OBJ_140_OBJ_140.setBounds(350, 360, 19, 18);

      //---- OBJ_144_OBJ_144 ----
      OBJ_144_OBJ_144.setText("15");
      OBJ_144_OBJ_144.setName("OBJ_144_OBJ_144");
      P_Centre.add(OBJ_144_OBJ_144);
      OBJ_144_OBJ_144.setBounds(30, 385, 19, 18);

      //---- OBJ_145_OBJ_145 ----
      OBJ_145_OBJ_145.setText("61");
      OBJ_145_OBJ_145.setName("OBJ_145_OBJ_145");
      P_Centre.add(OBJ_145_OBJ_145);
      OBJ_145_OBJ_145.setBounds(350, 385, 19, 18);

      //---- OBJ_148_OBJ_148 ----
      OBJ_148_OBJ_148.setText("16");
      OBJ_148_OBJ_148.setName("OBJ_148_OBJ_148");
      P_Centre.add(OBJ_148_OBJ_148);
      OBJ_148_OBJ_148.setBounds(30, 410, 19, 18);

      //---- OBJ_149_OBJ_149 ----
      OBJ_149_OBJ_149.setText("62");
      OBJ_149_OBJ_149.setName("OBJ_149_OBJ_149");
      P_Centre.add(OBJ_149_OBJ_149);
      OBJ_149_OBJ_149.setBounds(350, 410, 19, 18);

      //---- OBJ_152_OBJ_152 ----
      OBJ_152_OBJ_152.setText("17");
      OBJ_152_OBJ_152.setName("OBJ_152_OBJ_152");
      P_Centre.add(OBJ_152_OBJ_152);
      OBJ_152_OBJ_152.setBounds(30, 435, 19, 18);

      //---- OBJ_153_OBJ_153 ----
      OBJ_153_OBJ_153.setText("63");
      OBJ_153_OBJ_153.setName("OBJ_153_OBJ_153");
      P_Centre.add(OBJ_153_OBJ_153);
      OBJ_153_OBJ_153.setBounds(350, 435, 19, 18);

      //---- OBJ_155_OBJ_155 ----
      OBJ_155_OBJ_155.setText("18");
      OBJ_155_OBJ_155.setName("OBJ_155_OBJ_155");
      P_Centre.add(OBJ_155_OBJ_155);
      OBJ_155_OBJ_155.setBounds(30, 460, 19, 18);

      //---- OBJ_156_OBJ_156 ----
      OBJ_156_OBJ_156.setText("64");
      OBJ_156_OBJ_156.setName("OBJ_156_OBJ_156");
      P_Centre.add(OBJ_156_OBJ_156);
      OBJ_156_OBJ_156.setBounds(350, 460, 19, 18);

      //---- OBJ_158_OBJ_158 ----
      OBJ_158_OBJ_158.setText("19");
      OBJ_158_OBJ_158.setName("OBJ_158_OBJ_158");
      P_Centre.add(OBJ_158_OBJ_158);
      OBJ_158_OBJ_158.setBounds(30, 485, 19, 18);

      //---- OBJ_159_OBJ_159 ----
      OBJ_159_OBJ_159.setText("65");
      OBJ_159_OBJ_159.setName("OBJ_159_OBJ_159");
      P_Centre.add(OBJ_159_OBJ_159);
      OBJ_159_OBJ_159.setBounds(350, 485, 19, 18);

      //---- OBJ_161_OBJ_161 ----
      OBJ_161_OBJ_161.setText("20");
      OBJ_161_OBJ_161.setName("OBJ_161_OBJ_161");
      P_Centre.add(OBJ_161_OBJ_161);
      OBJ_161_OBJ_161.setBounds(30, 510, 19, 18);

      //---- OBJ_162_OBJ_162 ----
      OBJ_162_OBJ_162.setText("66");
      OBJ_162_OBJ_162.setName("OBJ_162_OBJ_162");
      P_Centre.add(OBJ_162_OBJ_162);
      OBJ_162_OBJ_162.setBounds(350, 510, 19, 18);

      //---- OBJ_164_OBJ_164 ----
      OBJ_164_OBJ_164.setText("21");
      OBJ_164_OBJ_164.setName("OBJ_164_OBJ_164");
      P_Centre.add(OBJ_164_OBJ_164);
      OBJ_164_OBJ_164.setBounds(30, 535, 19, 18);

      //---- OBJ_165_OBJ_165 ----
      OBJ_165_OBJ_165.setText("67");
      OBJ_165_OBJ_165.setName("OBJ_165_OBJ_165");
      P_Centre.add(OBJ_165_OBJ_165);
      OBJ_165_OBJ_165.setBounds(350, 535, 19, 18);

      //---- OBJ_172_OBJ_172 ----
      OBJ_172_OBJ_172.setText("51");
      OBJ_172_OBJ_172.setName("OBJ_172_OBJ_172");
      P_Centre.add(OBJ_172_OBJ_172);
      OBJ_172_OBJ_172.setBounds(350, 135, 19, 18);

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Affichage des compteurs (postes PEC)");
      xTitledSeparator1.setName("xTitledSeparator1");
      P_Centre.add(xTitledSeparator1);
      xTitledSeparator1.setBounds(15, 5, 960, xTitledSeparator1.getPreferredSize().height);

      //---- xTitledSeparator2 ----
      xTitledSeparator2.setTitle("Modification des compteurs (postes PEC)");
      xTitledSeparator2.setName("xTitledSeparator2");
      P_Centre.add(xTitledSeparator2);
      xTitledSeparator2.setBounds(15, 20, 960, xTitledSeparator2.getPreferredSize().height);

      //---- LIV2 ----
      LIV2.setName("LIV2");
      P_Centre.add(LIV2);
      LIV2.setBounds(55, 30, 172, LIV2.getPreferredSize().height);

      //---- LIV26 ----
      LIV26.setName("LIV26");
      P_Centre.add(LIV26);
      LIV26.setBounds(375, 30, 144, LIV26.getPreferredSize().height);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_6;
  private JMenuItem OBJ_7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JLabel OBJ_173_OBJ_173;
  private JLabel OBJ_31_OBJ_31;
  private JLabel OBJ_32_OBJ_32;
  private XRiTextField PCETB;
  private XRiTextField PCMATR;
  private JPanel P_Centre;
  private XRiTextField LIV60;
  private XRiTextField LIV65;
  private XRiTextField LIV01;
  private XRiTextField LIV18;
  private XRiTextField LIV02;
  private XRiTextField LIV19;
  private XRiTextField LIV03;
  private XRiTextField LIV20;
  private XRiTextField LIV04;
  private XRiTextField LIV21;
  private XRiTextField LIV05;
  private XRiTextField LIV22;
  private XRiTextField LIV06;
  private XRiTextField LIV23;
  private XRiTextField LIV07;
  private XRiTextField LIV24;
  private XRiTextField LIV51;
  private XRiTextField LIV08;
  private XRiTextField LIV25;
  private XRiTextField LIV52;
  private XRiTextField LIV09;
  private XRiTextField LIV53;
  private XRiTextField LIV10;
  private XRiTextField LIV54;
  private XRiTextField LIV11;
  private XRiTextField LIV55;
  private XRiTextField LIV12;
  private XRiTextField LIV13;
  private XRiTextField LIV56;
  private XRiTextField LIV14;
  private XRiTextField LIV57;
  private XRiTextField LIV15;
  private XRiTextField LIV58;
  private XRiTextField LIV16;
  private XRiTextField LIV59;
  private XRiTextField LIV17;
  private XRiTextField LIV61;
  private XRiTextField LIV62;
  private XRiTextField LIV63;
  private XRiTextField LIV64;
  private XRiTextField LIV66;
  private XRiTextField LIV67;
  private XRiTextField PCT01X;
  private XRiTextField PCT18X;
  private XRiTextField PCT02X;
  private XRiTextField PCT19X;
  private XRiTextField PCT03X;
  private XRiTextField PCT20X;
  private XRiTextField PCT04X;
  private XRiTextField PCT21X;
  private XRiTextField PCT05X;
  private XRiTextField PCT22X;
  private XRiTextField PCT06X;
  private XRiTextField PCT23X;
  private XRiTextField PCT07X;
  private XRiTextField PCT24X;
  private XRiTextField PCT51X;
  private XRiTextField PCT08X;
  private XRiTextField PCT25X;
  private XRiTextField PCT52X;
  private XRiTextField PCT09X;
  private XRiTextField PCT53X;
  private XRiTextField PCT10X;
  private XRiTextField PCT54X;
  private XRiTextField PCT11X;
  private XRiTextField PCT55X;
  private XRiTextField PCT12X;
  private XRiTextField PCT13X;
  private XRiTextField PCT56X;
  private XRiTextField PCT14X;
  private XRiTextField PCT57X;
  private XRiTextField PCT15X;
  private XRiTextField PCT58X;
  private XRiTextField PCT16X;
  private XRiTextField PCT59X;
  private XRiTextField PCT17X;
  private XRiTextField PCT60X;
  private XRiTextField PCT61X;
  private XRiTextField PCT62X;
  private XRiTextField PCT63X;
  private XRiTextField PCT64X;
  private XRiTextField PCT65X;
  private XRiTextField PCT66X;
  private XRiTextField PCT67X;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_49_OBJ_49;
  private JLabel OBJ_56_OBJ_56;
  private JLabel OBJ_57_OBJ_57;
  private JLabel OBJ_62_OBJ_62;
  private JLabel OBJ_63_OBJ_63;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_77_OBJ_77;
  private JLabel OBJ_78_OBJ_78;
  private JLabel OBJ_85_OBJ_85;
  private JLabel OBJ_86_OBJ_86;
  private JLabel OBJ_91_OBJ_91;
  private JLabel OBJ_92_OBJ_92;
  private JLabel OBJ_97_OBJ_97;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_112_OBJ_112;
  private JLabel OBJ_113_OBJ_113;
  private JLabel OBJ_119_OBJ_119;
  private JLabel OBJ_120_OBJ_120;
  private JLabel OBJ_126_OBJ_126;
  private JLabel OBJ_127_OBJ_127;
  private JLabel OBJ_132_OBJ_132;
  private JLabel OBJ_133_OBJ_133;
  private JLabel OBJ_139_OBJ_139;
  private JLabel OBJ_140_OBJ_140;
  private JLabel OBJ_144_OBJ_144;
  private JLabel OBJ_145_OBJ_145;
  private JLabel OBJ_148_OBJ_148;
  private JLabel OBJ_149_OBJ_149;
  private JLabel OBJ_152_OBJ_152;
  private JLabel OBJ_153_OBJ_153;
  private JLabel OBJ_155_OBJ_155;
  private JLabel OBJ_156_OBJ_156;
  private JLabel OBJ_158_OBJ_158;
  private JLabel OBJ_159_OBJ_159;
  private JLabel OBJ_161_OBJ_161;
  private JLabel OBJ_162_OBJ_162;
  private JLabel OBJ_164_OBJ_164;
  private JLabel OBJ_165_OBJ_165;
  private JLabel OBJ_172_OBJ_172;
  private JXTitledSeparator xTitledSeparator1;
  private JXTitledSeparator xTitledSeparator2;
  private XRiTextField LIV2;
  private XRiTextField LIV26;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
