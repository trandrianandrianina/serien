
package ri.serien.libecranrpg.spam.SPAM80FM;
// Nom Fichier: i_SPAM80FM_FMTB2_FMTF1_424.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.BevelBorder;

import com.jgoodies.forms.factories.FormFactory;
import com.jgoodies.forms.layout.CellConstraints;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.FormSpec;
import com.jgoodies.forms.layout.RowSpec;
import com.jgoodies.forms.layout.Sizes;
import org.jdesktop.swingx.JXHeader;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SPAM80FM_B2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SPAM80FM_B2(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    xH_Titre.setDescription(lexique.TranslationTable(interpreteurD.analyseExpression("FM@LOCGRP/+1/@")).trim());
    l_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_147_OBJ_147.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOM@")).trim());
    V03F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V03F@")).trim());
    BT_V07F6.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+7=@")).trim());
    BT_V07F5.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+6=@")).trim());
    BT_V07F4.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+5=@")).trim());
    BT_V07F3.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+4=@")).trim());
    BT_V07F2.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+3=@")).trim());
    BT_V07F1.setToolTipText(lexique.TranslationTable(interpreteurD.analyseExpression("@V07F/+2=@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    PCT84X.setEnabled(lexique.isPresent("PCT84X"));
    PCT83X.setEnabled(lexique.isPresent("PCT83X"));
    PCT82X.setEnabled(lexique.isPresent("PCT82X"));
    PCT81X.setEnabled(lexique.isPresent("PCT81X"));
    PCT98X.setEnabled(lexique.isPresent("PCT98X"));
    PCT80X.setEnabled(lexique.isPresent("PCT80X"));
    PCT79X.setEnabled(lexique.isPresent("PCT79X"));
    PCT97X.setEnabled(lexique.isPresent("PCT97X"));
    PCT78X.setEnabled(lexique.isPresent("PCT78X"));
    PCT96X.setEnabled(lexique.isPresent("PCT96X"));
    PCT77X.setEnabled(lexique.isPresent("PCT77X"));
    PCT95X.setEnabled(lexique.isPresent("PCT95X"));
    PCT76X.setEnabled(lexique.isPresent("PCT76X"));
    PCT94X.setEnabled(lexique.isPresent("PCT94X"));
    PCT92X.setEnabled(lexique.isPresent("PCT92X"));
    PCT75X.setEnabled(lexique.isPresent("PCT75X"));
    PCT93X.setEnabled(lexique.isPresent("PCT93X"));
    PCT91X.setEnabled(lexique.isPresent("PCT91X"));
    PCT74X.setEnabled(lexique.isPresent("PCT74X"));
    PCT90X.setEnabled(lexique.isPresent("PCT90X"));
    PCT73X.setEnabled(lexique.isPresent("PCT73X"));
    PCT89X.setEnabled(lexique.isPresent("PCT89X"));
    PCT72X.setEnabled(lexique.isPresent("PCT72X"));
    PCT88X.setEnabled(lexique.isPresent("PCT88X"));
    PCT71X.setEnabled(lexique.isPresent("PCT71X"));
    PCT87X.setEnabled(lexique.isPresent("PCT87X"));
    PCT70X.setEnabled(lexique.isPresent("PCT70X"));
    PCT86X.setEnabled(lexique.isPresent("PCT86X"));
    PCT69X.setEnabled(lexique.isPresent("PCT69X"));
    PCT85X.setEnabled(lexique.isPresent("PCT85X"));
    PCT68X.setEnabled(lexique.isPresent("PCT68X"));
    OBJ_147_OBJ_147.setVisible(lexique.isPresent("NOM"));
    if (lexique.isTrue("19")) {
      BT_ERR.setVisible(true);
      V03F.setForeground(Color.RED);
    }
    else {
      BT_ERR.setVisible(false);
      V03F.setForeground((Color) lexique.getSystemDefaultColor("Label.foreground"));
    }
    
    // TODO Icones
    BT_ERR.setIcon(lexique.chargerImage("images/erreur.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    xH_Titre.setIcon(lexique.chargerImage("images/logo32.png", true));
    
    // V07F
    lexique.setVisibilityButton(BT_V07F6, "@V07F/+7=@");
    lexique.setVisibilityButton(BT_V07F5, "@V07F/+6=@");
    lexique.setVisibilityButton(BT_V07F4, "@V07F/+5=@");
    lexique.setVisibilityButton(BT_V07F3, "@V07F/+4=@");
    lexique.setVisibilityButton(BT_V07F2, "@V07F/+3=@");
    lexique.setVisibilityButton(BT_V07F1, "@V07F/+2=@");
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - FICHE PERSONNEL"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F3"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F5"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F5", false);
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F8"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F12"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F13"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F14"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F14", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F15"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F16"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F18"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // prg = "vpam"
    // ScriptCall("G_AIDE")
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="F4"
    // ScriptCall("G_TOUCHE")
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void bt_FonctionsActionPerformed(ActionEvent e) {
    CMD.show(bt_Fonctions, 0, bt_Fonctions.getHeight());
  }
  
  private void BT_ERRActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F1", false);
  }
  
  private void BT_V07F6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-7=@", false);
  }
  
  private void BT_V07F5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-6=@", false);
  }
  
  private void BT_V07F4ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-5=@", false);
  }
  
  private void BT_V07F3ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-4=@", false);
  }
  
  private void BT_V07F2ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-3=@", false);
  }
  
  private void BT_V07F1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "@V07F/-2=@", false);
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    CMD = new JPopupMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    OBJ_9 = new JMenuItem();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    OBJ_12 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_17 = new JMenuItem();
    OBJ_18 = new JMenuItem();
    P_Haut = new JPanel();
    xH_Titre = new JXHeader();
    P_Infos = new JPanel();
    bt_Fonctions = new JButton();
    l_V01F = new JLabel();
    OBJ_147_OBJ_147 = new JLabel();
    OBJ_48_OBJ_48 = new JLabel();
    OBJ_49_OBJ_49 = new JLabel();
    PCETB = new XRiTextField();
    PCMATR = new XRiTextField();
    P_Centre = new JPanel();
    LIV68 = new XRiTextField();
    LIV85 = new XRiTextField();
    LIV69 = new XRiTextField();
    LIV86 = new XRiTextField();
    LIV70 = new XRiTextField();
    LIV87 = new XRiTextField();
    LIV71 = new XRiTextField();
    LIV88 = new XRiTextField();
    LIV72 = new XRiTextField();
    LIV89 = new XRiTextField();
    LIV73 = new XRiTextField();
    LIV90 = new XRiTextField();
    LIV74 = new XRiTextField();
    LIV91 = new XRiTextField();
    LIV93 = new XRiTextField();
    LIV75 = new XRiTextField();
    LIV92 = new XRiTextField();
    LIV94 = new XRiTextField();
    LIV76 = new XRiTextField();
    LIV95 = new XRiTextField();
    LIV77 = new XRiTextField();
    LIV96 = new XRiTextField();
    LIV78 = new XRiTextField();
    LIV97 = new XRiTextField();
    LIV79 = new XRiTextField();
    LIV80 = new XRiTextField();
    LIV98 = new XRiTextField();
    LIV81 = new XRiTextField();
    LIV82 = new XRiTextField();
    LIV83 = new XRiTextField();
    LIV84 = new XRiTextField();
    PCT68X = new XRiTextField();
    PCT85X = new XRiTextField();
    PCT69X = new XRiTextField();
    PCT86X = new XRiTextField();
    PCT70X = new XRiTextField();
    PCT87X = new XRiTextField();
    PCT71X = new XRiTextField();
    PCT88X = new XRiTextField();
    PCT72X = new XRiTextField();
    PCT89X = new XRiTextField();
    PCT73X = new XRiTextField();
    PCT90X = new XRiTextField();
    PCT74X = new XRiTextField();
    PCT91X = new XRiTextField();
    PCT93X = new XRiTextField();
    PCT75X = new XRiTextField();
    PCT92X = new XRiTextField();
    PCT94X = new XRiTextField();
    PCT76X = new XRiTextField();
    PCT95X = new XRiTextField();
    PCT77X = new XRiTextField();
    PCT96X = new XRiTextField();
    PCT78X = new XRiTextField();
    PCT97X = new XRiTextField();
    PCT79X = new XRiTextField();
    PCT80X = new XRiTextField();
    PCT98X = new XRiTextField();
    PCT81X = new XRiTextField();
    PCT82X = new XRiTextField();
    PCT83X = new XRiTextField();
    PCT84X = new XRiTextField();
    OBJ_60_OBJ_60 = new JLabel();
    OBJ_61_OBJ_61 = new JLabel();
    OBJ_68_OBJ_68 = new JLabel();
    OBJ_69_OBJ_69 = new JLabel();
    OBJ_74_OBJ_74 = new JLabel();
    OBJ_75_OBJ_75 = new JLabel();
    OBJ_80_OBJ_80 = new JLabel();
    OBJ_89_OBJ_89 = new JLabel();
    OBJ_90_OBJ_90 = new JLabel();
    OBJ_97_OBJ_97 = new JLabel();
    OBJ_98_OBJ_98 = new JLabel();
    OBJ_103_OBJ_103 = new JLabel();
    OBJ_104_OBJ_104 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    OBJ_110_OBJ_110 = new JLabel();
    OBJ_115_OBJ_115 = new JLabel();
    OBJ_116_OBJ_116 = new JLabel();
    OBJ_123_OBJ_123 = new JLabel();
    OBJ_124_OBJ_124 = new JLabel();
    OBJ_129_OBJ_129 = new JLabel();
    OBJ_132_OBJ_132 = new JLabel();
    OBJ_135_OBJ_135 = new JLabel();
    OBJ_138_OBJ_138 = new JLabel();
    OBJ_139_OBJ_139 = new JLabel();
    OBJ_140_OBJ_140 = new JLabel();
    OBJ_141_OBJ_141 = new JLabel();
    OBJ_142_OBJ_142 = new JLabel();
    OBJ_143_OBJ_143 = new JLabel();
    OBJ_144_OBJ_144 = new JLabel();
    OBJ_145_OBJ_145 = new JLabel();
    OBJ_146_OBJ_146 = new JLabel();
    xTitledSeparator1 = new JXTitledSeparator();
    P_Bas = new JPanel();
    BT_ERR = new JButton();
    V03F = new JLabel();
    P_Boutons = new JPanel();
    BT_V07F6 = new JButton();
    BT_V07F5 = new JButton();
    BT_V07F4 = new JButton();
    BT_V07F3 = new JButton();
    BT_V07F2 = new JButton();
    BT_V07F1 = new JButton();
    BT_ENTER = new JButton();
    CellConstraints cc = new CellConstraints();

    //======== CMD ========
    {
      CMD.setName("CMD");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      CMD.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("R\u00e9afficher");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      CMD.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Recherche multi-crit\u00e8res");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      CMD.add(OBJ_8);

      //---- OBJ_9 ----
      OBJ_9.setText("Annuler");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      CMD.add(OBJ_9);
      CMD.addSeparator();

      //---- OBJ_10 ----
      OBJ_10.setText("Cr\u00e9ation");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      CMD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Modification");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      CMD.add(OBJ_11);

      //---- OBJ_12 ----
      OBJ_12.setText("Interrogation");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      CMD.add(OBJ_12);

      //---- OBJ_13 ----
      OBJ_13.setText("Annulation");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      CMD.add(OBJ_13);

      //---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      CMD.add(OBJ_14);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);

      //---- OBJ_18 ----
      OBJ_18.setText("Invite");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
    }

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(950, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== P_Haut ========
    {
      P_Haut.setName("P_Haut");
      P_Haut.setLayout(new VerticalLayout());

      //---- xH_Titre ----
      xH_Titre.setTitle("FICHE PERSONNEL");
      xH_Titre.setIcon(null);
      xH_Titre.setTitleFont(new Font("Arial", Font.BOLD, 18));
      xH_Titre.setTitleForeground(Color.gray);
      xH_Titre.setDescriptionForeground(Color.gray);
      xH_Titre.setDescription("FM@LOCGRP/+1/@");
      xH_Titre.setName("xH_Titre");
      P_Haut.add(xH_Titre);

      //======== P_Infos ========
      {
        P_Infos.setBorder(new BevelBorder(BevelBorder.RAISED));
        P_Infos.setMinimumSize(new Dimension(66, 22));
        P_Infos.setName("P_Infos");
        P_Infos.setLayout(null);

        //---- bt_Fonctions ----
        bt_Fonctions.setText("Fonctions");
        bt_Fonctions.setName("bt_Fonctions");
        bt_Fonctions.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            bt_FonctionsActionPerformed(e);
          }
        });
        P_Infos.add(bt_Fonctions);
        bt_Fonctions.setBounds(883, 5, 115, bt_Fonctions.getPreferredSize().height);

        //---- l_V01F ----
        l_V01F.setText("@V01F@");
        l_V01F.setFont(new Font("sansserif", Font.BOLD, 12));
        l_V01F.setName("l_V01F");
        P_Infos.add(l_V01F);
        l_V01F.setBounds(21, 9, 80, 20);

        //---- OBJ_147_OBJ_147 ----
        OBJ_147_OBJ_147.setText("@NOM@");
        OBJ_147_OBJ_147.setName("OBJ_147_OBJ_147");
        P_Infos.add(OBJ_147_OBJ_147);
        OBJ_147_OBJ_147.setBounds(375, 10, 285, 16);

        //---- OBJ_48_OBJ_48 ----
        OBJ_48_OBJ_48.setText("Etablissement");
        OBJ_48_OBJ_48.setName("OBJ_48_OBJ_48");
        P_Infos.add(OBJ_48_OBJ_48);
        OBJ_48_OBJ_48.setBounds(105, 10, 93, 16);

        //---- OBJ_49_OBJ_49 ----
        OBJ_49_OBJ_49.setText("Matricule");
        OBJ_49_OBJ_49.setName("OBJ_49_OBJ_49");
        P_Infos.add(OBJ_49_OBJ_49);
        OBJ_49_OBJ_49.setBounds(255, 10, 63, 16);

        //---- PCETB ----
        PCETB.setName("PCETB");
        P_Infos.add(PCETB);
        PCETB.setBounds(205, 5, 40, PCETB.getPreferredSize().height);

        //---- PCMATR ----
        PCMATR.setName("PCMATR");
        P_Infos.add(PCMATR);
        PCMATR.setBounds(325, 5, 36, PCMATR.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < P_Infos.getComponentCount(); i++) {
            Rectangle bounds = P_Infos.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = P_Infos.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          P_Infos.setMinimumSize(preferredSize);
          P_Infos.setPreferredSize(preferredSize);
        }
      }
      P_Haut.add(P_Infos);
    }
    add(P_Haut, BorderLayout.NORTH);

    //======== P_Centre ========
    {
      P_Centre.setName("P_Centre");
      P_Centre.setLayout(null);

      //---- LIV68 ----
      LIV68.setName("LIV68");
      P_Centre.add(LIV68);
      LIV68.setBounds(55, 30, 172, LIV68.getPreferredSize().height);

      //---- LIV85 ----
      LIV85.setName("LIV85");
      P_Centre.add(LIV85);
      LIV85.setBounds(55, 455, 172, LIV85.getPreferredSize().height);

      //---- LIV69 ----
      LIV69.setName("LIV69");
      P_Centre.add(LIV69);
      LIV69.setBounds(55, 55, 172, LIV69.getPreferredSize().height);

      //---- LIV86 ----
      LIV86.setName("LIV86");
      P_Centre.add(LIV86);
      LIV86.setBounds(55, 480, 172, LIV86.getPreferredSize().height);

      //---- LIV70 ----
      LIV70.setName("LIV70");
      P_Centre.add(LIV70);
      LIV70.setBounds(55, 80, 172, LIV70.getPreferredSize().height);

      //---- LIV87 ----
      LIV87.setName("LIV87");
      P_Centre.add(LIV87);
      LIV87.setBounds(55, 505, 172, LIV87.getPreferredSize().height);

      //---- LIV71 ----
      LIV71.setName("LIV71");
      P_Centre.add(LIV71);
      LIV71.setBounds(55, 105, 172, LIV71.getPreferredSize().height);

      //---- LIV88 ----
      LIV88.setName("LIV88");
      P_Centre.add(LIV88);
      LIV88.setBounds(55, 530, 172, LIV88.getPreferredSize().height);

      //---- LIV72 ----
      LIV72.setName("LIV72");
      P_Centre.add(LIV72);
      LIV72.setBounds(55, 130, 172, LIV72.getPreferredSize().height);

      //---- LIV89 ----
      LIV89.setName("LIV89");
      P_Centre.add(LIV89);
      LIV89.setBounds(380, 30, 172, LIV89.getPreferredSize().height);

      //---- LIV73 ----
      LIV73.setName("LIV73");
      P_Centre.add(LIV73);
      LIV73.setBounds(55, 155, 172, LIV73.getPreferredSize().height);

      //---- LIV90 ----
      LIV90.setName("LIV90");
      P_Centre.add(LIV90);
      LIV90.setBounds(380, 55, 172, LIV90.getPreferredSize().height);

      //---- LIV74 ----
      LIV74.setName("LIV74");
      P_Centre.add(LIV74);
      LIV74.setBounds(55, 180, 172, LIV74.getPreferredSize().height);

      //---- LIV91 ----
      LIV91.setName("LIV91");
      P_Centre.add(LIV91);
      LIV91.setBounds(380, 80, 172, LIV91.getPreferredSize().height);

      //---- LIV93 ----
      LIV93.setName("LIV93");
      P_Centre.add(LIV93);
      LIV93.setBounds(380, 130, 172, LIV93.getPreferredSize().height);

      //---- LIV75 ----
      LIV75.setName("LIV75");
      P_Centre.add(LIV75);
      LIV75.setBounds(55, 205, 172, LIV75.getPreferredSize().height);

      //---- LIV92 ----
      LIV92.setName("LIV92");
      P_Centre.add(LIV92);
      LIV92.setBounds(380, 105, 172, LIV92.getPreferredSize().height);

      //---- LIV94 ----
      LIV94.setName("LIV94");
      P_Centre.add(LIV94);
      LIV94.setBounds(380, 155, 172, LIV94.getPreferredSize().height);

      //---- LIV76 ----
      LIV76.setName("LIV76");
      P_Centre.add(LIV76);
      LIV76.setBounds(55, 230, 172, LIV76.getPreferredSize().height);

      //---- LIV95 ----
      LIV95.setName("LIV95");
      P_Centre.add(LIV95);
      LIV95.setBounds(380, 180, 172, LIV95.getPreferredSize().height);

      //---- LIV77 ----
      LIV77.setName("LIV77");
      P_Centre.add(LIV77);
      LIV77.setBounds(55, 255, 172, LIV77.getPreferredSize().height);

      //---- LIV96 ----
      LIV96.setName("LIV96");
      P_Centre.add(LIV96);
      LIV96.setBounds(380, 205, 172, LIV96.getPreferredSize().height);

      //---- LIV78 ----
      LIV78.setName("LIV78");
      P_Centre.add(LIV78);
      LIV78.setBounds(55, 280, 172, LIV78.getPreferredSize().height);

      //---- LIV97 ----
      LIV97.setName("LIV97");
      P_Centre.add(LIV97);
      LIV97.setBounds(380, 230, 172, LIV97.getPreferredSize().height);

      //---- LIV79 ----
      LIV79.setName("LIV79");
      P_Centre.add(LIV79);
      LIV79.setBounds(55, 305, 172, LIV79.getPreferredSize().height);

      //---- LIV80 ----
      LIV80.setName("LIV80");
      P_Centre.add(LIV80);
      LIV80.setBounds(55, 330, 172, LIV80.getPreferredSize().height);

      //---- LIV98 ----
      LIV98.setName("LIV98");
      P_Centre.add(LIV98);
      LIV98.setBounds(380, 255, 172, LIV98.getPreferredSize().height);

      //---- LIV81 ----
      LIV81.setName("LIV81");
      P_Centre.add(LIV81);
      LIV81.setBounds(55, 355, 172, LIV81.getPreferredSize().height);

      //---- LIV82 ----
      LIV82.setName("LIV82");
      P_Centre.add(LIV82);
      LIV82.setBounds(55, 380, 172, LIV82.getPreferredSize().height);

      //---- LIV83 ----
      LIV83.setName("LIV83");
      P_Centre.add(LIV83);
      LIV83.setBounds(55, 405, 172, LIV83.getPreferredSize().height);

      //---- LIV84 ----
      LIV84.setName("LIV84");
      P_Centre.add(LIV84);
      LIV84.setBounds(55, 430, 172, LIV84.getPreferredSize().height);

      //---- PCT68X ----
      PCT68X.setComponentPopupMenu(BTD);
      PCT68X.setName("PCT68X");
      P_Centre.add(PCT68X);
      PCT68X.setBounds(230, 30, 110, PCT68X.getPreferredSize().height);

      //---- PCT85X ----
      PCT85X.setComponentPopupMenu(BTD);
      PCT85X.setName("PCT85X");
      P_Centre.add(PCT85X);
      PCT85X.setBounds(230, 455, 110, PCT85X.getPreferredSize().height);

      //---- PCT69X ----
      PCT69X.setComponentPopupMenu(BTD);
      PCT69X.setName("PCT69X");
      P_Centre.add(PCT69X);
      PCT69X.setBounds(230, 55, 110, PCT69X.getPreferredSize().height);

      //---- PCT86X ----
      PCT86X.setComponentPopupMenu(BTD);
      PCT86X.setName("PCT86X");
      P_Centre.add(PCT86X);
      PCT86X.setBounds(230, 480, 110, PCT86X.getPreferredSize().height);

      //---- PCT70X ----
      PCT70X.setComponentPopupMenu(BTD);
      PCT70X.setName("PCT70X");
      P_Centre.add(PCT70X);
      PCT70X.setBounds(230, 80, 110, PCT70X.getPreferredSize().height);

      //---- PCT87X ----
      PCT87X.setComponentPopupMenu(BTD);
      PCT87X.setName("PCT87X");
      P_Centre.add(PCT87X);
      PCT87X.setBounds(230, 505, 110, PCT87X.getPreferredSize().height);

      //---- PCT71X ----
      PCT71X.setComponentPopupMenu(BTD);
      PCT71X.setName("PCT71X");
      P_Centre.add(PCT71X);
      PCT71X.setBounds(230, 105, 110, PCT71X.getPreferredSize().height);

      //---- PCT88X ----
      PCT88X.setComponentPopupMenu(BTD);
      PCT88X.setName("PCT88X");
      P_Centre.add(PCT88X);
      PCT88X.setBounds(230, 530, 110, PCT88X.getPreferredSize().height);

      //---- PCT72X ----
      PCT72X.setComponentPopupMenu(BTD);
      PCT72X.setName("PCT72X");
      P_Centre.add(PCT72X);
      PCT72X.setBounds(230, 130, 110, PCT72X.getPreferredSize().height);

      //---- PCT89X ----
      PCT89X.setComponentPopupMenu(BTD);
      PCT89X.setName("PCT89X");
      P_Centre.add(PCT89X);
      PCT89X.setBounds(560, 30, 110, PCT89X.getPreferredSize().height);

      //---- PCT73X ----
      PCT73X.setComponentPopupMenu(BTD);
      PCT73X.setName("PCT73X");
      P_Centre.add(PCT73X);
      PCT73X.setBounds(230, 155, 110, PCT73X.getPreferredSize().height);

      //---- PCT90X ----
      PCT90X.setComponentPopupMenu(BTD);
      PCT90X.setName("PCT90X");
      P_Centre.add(PCT90X);
      PCT90X.setBounds(560, 55, 110, PCT90X.getPreferredSize().height);

      //---- PCT74X ----
      PCT74X.setComponentPopupMenu(BTD);
      PCT74X.setName("PCT74X");
      P_Centre.add(PCT74X);
      PCT74X.setBounds(230, 180, 110, PCT74X.getPreferredSize().height);

      //---- PCT91X ----
      PCT91X.setComponentPopupMenu(BTD);
      PCT91X.setName("PCT91X");
      P_Centre.add(PCT91X);
      PCT91X.setBounds(560, 80, 110, PCT91X.getPreferredSize().height);

      //---- PCT93X ----
      PCT93X.setComponentPopupMenu(BTD);
      PCT93X.setName("PCT93X");
      P_Centre.add(PCT93X);
      PCT93X.setBounds(560, 130, 110, PCT93X.getPreferredSize().height);

      //---- PCT75X ----
      PCT75X.setComponentPopupMenu(BTD);
      PCT75X.setName("PCT75X");
      P_Centre.add(PCT75X);
      PCT75X.setBounds(230, 205, 110, PCT75X.getPreferredSize().height);

      //---- PCT92X ----
      PCT92X.setComponentPopupMenu(BTD);
      PCT92X.setName("PCT92X");
      P_Centre.add(PCT92X);
      PCT92X.setBounds(560, 105, 110, PCT92X.getPreferredSize().height);

      //---- PCT94X ----
      PCT94X.setComponentPopupMenu(BTD);
      PCT94X.setName("PCT94X");
      P_Centre.add(PCT94X);
      PCT94X.setBounds(560, 155, 110, PCT94X.getPreferredSize().height);

      //---- PCT76X ----
      PCT76X.setComponentPopupMenu(BTD);
      PCT76X.setName("PCT76X");
      P_Centre.add(PCT76X);
      PCT76X.setBounds(230, 230, 110, PCT76X.getPreferredSize().height);

      //---- PCT95X ----
      PCT95X.setComponentPopupMenu(BTD);
      PCT95X.setName("PCT95X");
      P_Centre.add(PCT95X);
      PCT95X.setBounds(560, 180, 110, PCT95X.getPreferredSize().height);

      //---- PCT77X ----
      PCT77X.setComponentPopupMenu(BTD);
      PCT77X.setName("PCT77X");
      P_Centre.add(PCT77X);
      PCT77X.setBounds(230, 255, 110, PCT77X.getPreferredSize().height);

      //---- PCT96X ----
      PCT96X.setComponentPopupMenu(BTD);
      PCT96X.setName("PCT96X");
      P_Centre.add(PCT96X);
      PCT96X.setBounds(560, 205, 110, PCT96X.getPreferredSize().height);

      //---- PCT78X ----
      PCT78X.setComponentPopupMenu(BTD);
      PCT78X.setName("PCT78X");
      P_Centre.add(PCT78X);
      PCT78X.setBounds(230, 280, 110, PCT78X.getPreferredSize().height);

      //---- PCT97X ----
      PCT97X.setComponentPopupMenu(BTD);
      PCT97X.setName("PCT97X");
      P_Centre.add(PCT97X);
      PCT97X.setBounds(560, 230, 110, PCT97X.getPreferredSize().height);

      //---- PCT79X ----
      PCT79X.setComponentPopupMenu(BTD);
      PCT79X.setName("PCT79X");
      P_Centre.add(PCT79X);
      PCT79X.setBounds(230, 305, 110, PCT79X.getPreferredSize().height);

      //---- PCT80X ----
      PCT80X.setComponentPopupMenu(BTD);
      PCT80X.setName("PCT80X");
      P_Centre.add(PCT80X);
      PCT80X.setBounds(230, 330, 110, PCT80X.getPreferredSize().height);

      //---- PCT98X ----
      PCT98X.setComponentPopupMenu(BTD);
      PCT98X.setName("PCT98X");
      P_Centre.add(PCT98X);
      PCT98X.setBounds(560, 255, 110, PCT98X.getPreferredSize().height);

      //---- PCT81X ----
      PCT81X.setComponentPopupMenu(BTD);
      PCT81X.setName("PCT81X");
      P_Centre.add(PCT81X);
      PCT81X.setBounds(230, 355, 110, PCT81X.getPreferredSize().height);

      //---- PCT82X ----
      PCT82X.setComponentPopupMenu(BTD);
      PCT82X.setName("PCT82X");
      P_Centre.add(PCT82X);
      PCT82X.setBounds(230, 380, 110, PCT82X.getPreferredSize().height);

      //---- PCT83X ----
      PCT83X.setComponentPopupMenu(BTD);
      PCT83X.setName("PCT83X");
      P_Centre.add(PCT83X);
      PCT83X.setBounds(230, 405, 110, PCT83X.getPreferredSize().height);

      //---- PCT84X ----
      PCT84X.setComponentPopupMenu(BTD);
      PCT84X.setName("PCT84X");
      P_Centre.add(PCT84X);
      PCT84X.setBounds(230, 430, 110, PCT84X.getPreferredSize().height);

      //---- OBJ_60_OBJ_60 ----
      OBJ_60_OBJ_60.setText("68");
      OBJ_60_OBJ_60.setName("OBJ_60_OBJ_60");
      P_Centre.add(OBJ_60_OBJ_60);
      OBJ_60_OBJ_60.setBounds(30, 35, 19, 18);

      //---- OBJ_61_OBJ_61 ----
      OBJ_61_OBJ_61.setText("89");
      OBJ_61_OBJ_61.setName("OBJ_61_OBJ_61");
      P_Centre.add(OBJ_61_OBJ_61);
      OBJ_61_OBJ_61.setBounds(355, 35, 19, 18);

      //---- OBJ_68_OBJ_68 ----
      OBJ_68_OBJ_68.setText("69");
      OBJ_68_OBJ_68.setName("OBJ_68_OBJ_68");
      P_Centre.add(OBJ_68_OBJ_68);
      OBJ_68_OBJ_68.setBounds(30, 60, 19, 18);

      //---- OBJ_69_OBJ_69 ----
      OBJ_69_OBJ_69.setText("90");
      OBJ_69_OBJ_69.setName("OBJ_69_OBJ_69");
      P_Centre.add(OBJ_69_OBJ_69);
      OBJ_69_OBJ_69.setBounds(355, 60, 19, 18);

      //---- OBJ_74_OBJ_74 ----
      OBJ_74_OBJ_74.setText("70");
      OBJ_74_OBJ_74.setName("OBJ_74_OBJ_74");
      P_Centre.add(OBJ_74_OBJ_74);
      OBJ_74_OBJ_74.setBounds(30, 85, 19, 18);

      //---- OBJ_75_OBJ_75 ----
      OBJ_75_OBJ_75.setText("91");
      OBJ_75_OBJ_75.setName("OBJ_75_OBJ_75");
      P_Centre.add(OBJ_75_OBJ_75);
      OBJ_75_OBJ_75.setBounds(355, 85, 19, 18);

      //---- OBJ_80_OBJ_80 ----
      OBJ_80_OBJ_80.setText("71");
      OBJ_80_OBJ_80.setName("OBJ_80_OBJ_80");
      P_Centre.add(OBJ_80_OBJ_80);
      OBJ_80_OBJ_80.setBounds(30, 110, 19, 18);

      //---- OBJ_89_OBJ_89 ----
      OBJ_89_OBJ_89.setText("72");
      OBJ_89_OBJ_89.setName("OBJ_89_OBJ_89");
      P_Centre.add(OBJ_89_OBJ_89);
      OBJ_89_OBJ_89.setBounds(30, 135, 19, 18);

      //---- OBJ_90_OBJ_90 ----
      OBJ_90_OBJ_90.setText("92");
      OBJ_90_OBJ_90.setName("OBJ_90_OBJ_90");
      P_Centre.add(OBJ_90_OBJ_90);
      OBJ_90_OBJ_90.setBounds(355, 110, 19, 18);

      //---- OBJ_97_OBJ_97 ----
      OBJ_97_OBJ_97.setText("73");
      OBJ_97_OBJ_97.setName("OBJ_97_OBJ_97");
      P_Centre.add(OBJ_97_OBJ_97);
      OBJ_97_OBJ_97.setBounds(30, 160, 19, 18);

      //---- OBJ_98_OBJ_98 ----
      OBJ_98_OBJ_98.setText("94");
      OBJ_98_OBJ_98.setName("OBJ_98_OBJ_98");
      P_Centre.add(OBJ_98_OBJ_98);
      OBJ_98_OBJ_98.setBounds(355, 160, 19, 18);

      //---- OBJ_103_OBJ_103 ----
      OBJ_103_OBJ_103.setText("74");
      OBJ_103_OBJ_103.setName("OBJ_103_OBJ_103");
      P_Centre.add(OBJ_103_OBJ_103);
      OBJ_103_OBJ_103.setBounds(30, 185, 19, 18);

      //---- OBJ_104_OBJ_104 ----
      OBJ_104_OBJ_104.setText("95");
      OBJ_104_OBJ_104.setName("OBJ_104_OBJ_104");
      P_Centre.add(OBJ_104_OBJ_104);
      OBJ_104_OBJ_104.setBounds(355, 185, 19, 18);

      //---- OBJ_109_OBJ_109 ----
      OBJ_109_OBJ_109.setText("75");
      OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");
      P_Centre.add(OBJ_109_OBJ_109);
      OBJ_109_OBJ_109.setBounds(30, 210, 19, 18);

      //---- OBJ_110_OBJ_110 ----
      OBJ_110_OBJ_110.setText("96");
      OBJ_110_OBJ_110.setName("OBJ_110_OBJ_110");
      P_Centre.add(OBJ_110_OBJ_110);
      OBJ_110_OBJ_110.setBounds(355, 210, 19, 18);

      //---- OBJ_115_OBJ_115 ----
      OBJ_115_OBJ_115.setText("76");
      OBJ_115_OBJ_115.setName("OBJ_115_OBJ_115");
      P_Centre.add(OBJ_115_OBJ_115);
      OBJ_115_OBJ_115.setBounds(30, 235, 19, 18);

      //---- OBJ_116_OBJ_116 ----
      OBJ_116_OBJ_116.setText("97");
      OBJ_116_OBJ_116.setName("OBJ_116_OBJ_116");
      P_Centre.add(OBJ_116_OBJ_116);
      OBJ_116_OBJ_116.setBounds(355, 235, 19, 18);

      //---- OBJ_123_OBJ_123 ----
      OBJ_123_OBJ_123.setText("77");
      OBJ_123_OBJ_123.setName("OBJ_123_OBJ_123");
      P_Centre.add(OBJ_123_OBJ_123);
      OBJ_123_OBJ_123.setBounds(30, 260, 19, 18);

      //---- OBJ_124_OBJ_124 ----
      OBJ_124_OBJ_124.setText("98");
      OBJ_124_OBJ_124.setName("OBJ_124_OBJ_124");
      P_Centre.add(OBJ_124_OBJ_124);
      OBJ_124_OBJ_124.setBounds(355, 260, 19, 18);

      //---- OBJ_129_OBJ_129 ----
      OBJ_129_OBJ_129.setText("78");
      OBJ_129_OBJ_129.setName("OBJ_129_OBJ_129");
      P_Centre.add(OBJ_129_OBJ_129);
      OBJ_129_OBJ_129.setBounds(30, 285, 19, 18);

      //---- OBJ_132_OBJ_132 ----
      OBJ_132_OBJ_132.setText("79");
      OBJ_132_OBJ_132.setName("OBJ_132_OBJ_132");
      P_Centre.add(OBJ_132_OBJ_132);
      OBJ_132_OBJ_132.setBounds(30, 310, 19, 18);

      //---- OBJ_135_OBJ_135 ----
      OBJ_135_OBJ_135.setText("80");
      OBJ_135_OBJ_135.setName("OBJ_135_OBJ_135");
      P_Centre.add(OBJ_135_OBJ_135);
      OBJ_135_OBJ_135.setBounds(30, 335, 19, 18);

      //---- OBJ_138_OBJ_138 ----
      OBJ_138_OBJ_138.setText("81");
      OBJ_138_OBJ_138.setName("OBJ_138_OBJ_138");
      P_Centre.add(OBJ_138_OBJ_138);
      OBJ_138_OBJ_138.setBounds(30, 360, 19, 18);

      //---- OBJ_139_OBJ_139 ----
      OBJ_139_OBJ_139.setText("82");
      OBJ_139_OBJ_139.setName("OBJ_139_OBJ_139");
      P_Centre.add(OBJ_139_OBJ_139);
      OBJ_139_OBJ_139.setBounds(30, 385, 19, 18);

      //---- OBJ_140_OBJ_140 ----
      OBJ_140_OBJ_140.setText("83");
      OBJ_140_OBJ_140.setName("OBJ_140_OBJ_140");
      P_Centre.add(OBJ_140_OBJ_140);
      OBJ_140_OBJ_140.setBounds(30, 410, 19, 18);

      //---- OBJ_141_OBJ_141 ----
      OBJ_141_OBJ_141.setText("84");
      OBJ_141_OBJ_141.setName("OBJ_141_OBJ_141");
      P_Centre.add(OBJ_141_OBJ_141);
      OBJ_141_OBJ_141.setBounds(30, 435, 19, 18);

      //---- OBJ_142_OBJ_142 ----
      OBJ_142_OBJ_142.setText("85");
      OBJ_142_OBJ_142.setName("OBJ_142_OBJ_142");
      P_Centre.add(OBJ_142_OBJ_142);
      OBJ_142_OBJ_142.setBounds(30, 460, 19, 18);

      //---- OBJ_143_OBJ_143 ----
      OBJ_143_OBJ_143.setText("86");
      OBJ_143_OBJ_143.setName("OBJ_143_OBJ_143");
      P_Centre.add(OBJ_143_OBJ_143);
      OBJ_143_OBJ_143.setBounds(30, 485, 19, 18);

      //---- OBJ_144_OBJ_144 ----
      OBJ_144_OBJ_144.setText("87");
      OBJ_144_OBJ_144.setName("OBJ_144_OBJ_144");
      P_Centre.add(OBJ_144_OBJ_144);
      OBJ_144_OBJ_144.setBounds(30, 510, 19, 18);

      //---- OBJ_145_OBJ_145 ----
      OBJ_145_OBJ_145.setText("88");
      OBJ_145_OBJ_145.setName("OBJ_145_OBJ_145");
      P_Centre.add(OBJ_145_OBJ_145);
      OBJ_145_OBJ_145.setBounds(30, 535, 19, 18);

      //---- OBJ_146_OBJ_146 ----
      OBJ_146_OBJ_146.setText("93");
      OBJ_146_OBJ_146.setName("OBJ_146_OBJ_146");
      P_Centre.add(OBJ_146_OBJ_146);
      OBJ_146_OBJ_146.setBounds(355, 135, 19, 18);

      //---- xTitledSeparator1 ----
      xTitledSeparator1.setTitle("Affichage des compteurs (postes PEC)");
      xTitledSeparator1.setName("xTitledSeparator1");
      P_Centre.add(xTitledSeparator1);
      xTitledSeparator1.setBounds(20, 10, 970, xTitledSeparator1.getPreferredSize().height);

      {
        // compute preferred size
        Dimension preferredSize = new Dimension();
        for(int i = 0; i < P_Centre.getComponentCount(); i++) {
          Rectangle bounds = P_Centre.getComponent(i).getBounds();
          preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
          preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
        }
        Insets insets = P_Centre.getInsets();
        preferredSize.width += insets.right;
        preferredSize.height += insets.bottom;
        P_Centre.setMinimumSize(preferredSize);
        P_Centre.setPreferredSize(preferredSize);
      }
    }
    add(P_Centre, BorderLayout.CENTER);

    //======== P_Bas ========
    {
      P_Bas.setName("P_Bas");
      P_Bas.setLayout(new FormLayout(
        new ColumnSpec[] {
          FormFactory.DEFAULT_COLSPEC,
          FormFactory.LABEL_COMPONENT_GAP_COLSPEC,
          new ColumnSpec(Sizes.dluX(200)),
          new ColumnSpec(Sizes.DLUX2),
          new ColumnSpec(ColumnSpec.RIGHT, Sizes.dluX(241), FormSpec.DEFAULT_GROW),
          new ColumnSpec(Sizes.DLUX7)
        },
        RowSpec.decodeSpecs("fill:default")));

      //---- BT_ERR ----
      BT_ERR.setPreferredSize(new Dimension(32, 32));
      BT_ERR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
      BT_ERR.setName("BT_ERR");
      BT_ERR.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          BT_ERRActionPerformed(e);
        }
      });
      P_Bas.add(BT_ERR, cc.xy(1, 1));

      //---- V03F ----
      V03F.setText("@V03F@");
      V03F.setFont(V03F.getFont().deriveFont(V03F.getFont().getStyle() | Font.BOLD));
      V03F.setName("V03F");
      P_Bas.add(V03F, cc.xy(3, 1));

      //======== P_Boutons ========
      {
        P_Boutons.setName("P_Boutons");
        P_Boutons.setLayout(new FlowLayout(FlowLayout.RIGHT));

        //---- BT_V07F6 ----
        BT_V07F6.setToolTipText("@V07F/+7=@");
        BT_V07F6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F6.setName("BT_V07F6");
        BT_V07F6.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F6ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F6);

        //---- BT_V07F5 ----
        BT_V07F5.setToolTipText("@V07F/+6=@");
        BT_V07F5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F5.setName("BT_V07F5");
        BT_V07F5.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F5ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F5);

        //---- BT_V07F4 ----
        BT_V07F4.setToolTipText("@V07F/+5=@");
        BT_V07F4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F4.setName("BT_V07F4");
        BT_V07F4.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F4ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F4);

        //---- BT_V07F3 ----
        BT_V07F3.setToolTipText("@V07F/+4=@");
        BT_V07F3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F3.setName("BT_V07F3");
        BT_V07F3.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F3ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F3);

        //---- BT_V07F2 ----
        BT_V07F2.setSelectedIcon(null);
        BT_V07F2.setToolTipText("@V07F/+3=@");
        BT_V07F2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F2.setName("BT_V07F2");
        BT_V07F2.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F2ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F2);

        //---- BT_V07F1 ----
        BT_V07F1.setToolTipText("@V07F/+2=@");
        BT_V07F1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_V07F1.setName("BT_V07F1");
        BT_V07F1.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_V07F1ActionPerformed(e);
          }
        });
        P_Boutons.add(BT_V07F1);

        //---- BT_ENTER ----
        BT_ENTER.setToolTipText("Validation");
        BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_ENTER.setName("BT_ENTER");
        BT_ENTER.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            BT_ENTERActionPerformed();
          }
        });
        P_Boutons.add(BT_ENTER);
      }
      P_Bas.add(P_Boutons, cc.xy(5, 1));
    }
    add(P_Bas, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }



  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPopupMenu CMD;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JMenuItem OBJ_9;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_14;
  private JPopupMenu BTD;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_18;
  private JPanel P_Haut;
  private JXHeader xH_Titre;
  private JPanel P_Infos;
  private JButton bt_Fonctions;
  private JLabel l_V01F;
  private JLabel OBJ_147_OBJ_147;
  private JLabel OBJ_48_OBJ_48;
  private JLabel OBJ_49_OBJ_49;
  private XRiTextField PCETB;
  private XRiTextField PCMATR;
  private JPanel P_Centre;
  private XRiTextField LIV68;
  private XRiTextField LIV85;
  private XRiTextField LIV69;
  private XRiTextField LIV86;
  private XRiTextField LIV70;
  private XRiTextField LIV87;
  private XRiTextField LIV71;
  private XRiTextField LIV88;
  private XRiTextField LIV72;
  private XRiTextField LIV89;
  private XRiTextField LIV73;
  private XRiTextField LIV90;
  private XRiTextField LIV74;
  private XRiTextField LIV91;
  private XRiTextField LIV93;
  private XRiTextField LIV75;
  private XRiTextField LIV92;
  private XRiTextField LIV94;
  private XRiTextField LIV76;
  private XRiTextField LIV95;
  private XRiTextField LIV77;
  private XRiTextField LIV96;
  private XRiTextField LIV78;
  private XRiTextField LIV97;
  private XRiTextField LIV79;
  private XRiTextField LIV80;
  private XRiTextField LIV98;
  private XRiTextField LIV81;
  private XRiTextField LIV82;
  private XRiTextField LIV83;
  private XRiTextField LIV84;
  private XRiTextField PCT68X;
  private XRiTextField PCT85X;
  private XRiTextField PCT69X;
  private XRiTextField PCT86X;
  private XRiTextField PCT70X;
  private XRiTextField PCT87X;
  private XRiTextField PCT71X;
  private XRiTextField PCT88X;
  private XRiTextField PCT72X;
  private XRiTextField PCT89X;
  private XRiTextField PCT73X;
  private XRiTextField PCT90X;
  private XRiTextField PCT74X;
  private XRiTextField PCT91X;
  private XRiTextField PCT93X;
  private XRiTextField PCT75X;
  private XRiTextField PCT92X;
  private XRiTextField PCT94X;
  private XRiTextField PCT76X;
  private XRiTextField PCT95X;
  private XRiTextField PCT77X;
  private XRiTextField PCT96X;
  private XRiTextField PCT78X;
  private XRiTextField PCT97X;
  private XRiTextField PCT79X;
  private XRiTextField PCT80X;
  private XRiTextField PCT98X;
  private XRiTextField PCT81X;
  private XRiTextField PCT82X;
  private XRiTextField PCT83X;
  private XRiTextField PCT84X;
  private JLabel OBJ_60_OBJ_60;
  private JLabel OBJ_61_OBJ_61;
  private JLabel OBJ_68_OBJ_68;
  private JLabel OBJ_69_OBJ_69;
  private JLabel OBJ_74_OBJ_74;
  private JLabel OBJ_75_OBJ_75;
  private JLabel OBJ_80_OBJ_80;
  private JLabel OBJ_89_OBJ_89;
  private JLabel OBJ_90_OBJ_90;
  private JLabel OBJ_97_OBJ_97;
  private JLabel OBJ_98_OBJ_98;
  private JLabel OBJ_103_OBJ_103;
  private JLabel OBJ_104_OBJ_104;
  private JLabel OBJ_109_OBJ_109;
  private JLabel OBJ_110_OBJ_110;
  private JLabel OBJ_115_OBJ_115;
  private JLabel OBJ_116_OBJ_116;
  private JLabel OBJ_123_OBJ_123;
  private JLabel OBJ_124_OBJ_124;
  private JLabel OBJ_129_OBJ_129;
  private JLabel OBJ_132_OBJ_132;
  private JLabel OBJ_135_OBJ_135;
  private JLabel OBJ_138_OBJ_138;
  private JLabel OBJ_139_OBJ_139;
  private JLabel OBJ_140_OBJ_140;
  private JLabel OBJ_141_OBJ_141;
  private JLabel OBJ_142_OBJ_142;
  private JLabel OBJ_143_OBJ_143;
  private JLabel OBJ_144_OBJ_144;
  private JLabel OBJ_145_OBJ_145;
  private JLabel OBJ_146_OBJ_146;
  private JXTitledSeparator xTitledSeparator1;
  private JPanel P_Bas;
  private JButton BT_ERR;
  private JLabel V03F;
  private JPanel P_Boutons;
  private JButton BT_V07F6;
  private JButton BT_V07F5;
  private JButton BT_V07F4;
  private JButton BT_V07F3;
  private JButton BT_V07F2;
  private JButton BT_V07F1;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
