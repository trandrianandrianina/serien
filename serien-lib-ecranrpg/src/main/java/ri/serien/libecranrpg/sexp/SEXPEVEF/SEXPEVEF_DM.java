
package ri.serien.libecranrpg.sexp.SEXPEVEF;
// Nom Fichier: pop_SEXPEVFM_FMTDWX_213.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/*
 * Created by JFormDesigner on Fri Aug 07 16:04:46 CEST 2009
 */

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVEF_DM extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVEF_DM(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_RETOUR);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
    
    // TODO Icones
    BT_RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_18_OBJ_18.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_19_OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // En fonction du contenu de REPONS on force le DefaultButton (c'est le RPG qui pilote, on ne l'oubli pas)
    if (lexique.getHostField("REPONW").getValeurToBuffer().equals("OUI")) {
      setDefaultButton(BT_ENTER);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Evènement Série N"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostFieldPutData("REPONW", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_RETOURActionPerformed() {
    lexique.HostFieldPutData("REPONW", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_18_OBJ_18 = new JLabel();
    OBJ_19_OBJ_19 = new JLabel();
    BT_ENTER = new JButton();
    BT_RETOUR = new JButton();
    NOMBRE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setPreferredSize(new Dimension(440, 220));
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(null);

    //---- OBJ_18_OBJ_18 ----
    OBJ_18_OBJ_18.setText("@EVLB1@");
    OBJ_18_OBJ_18.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_18_OBJ_18.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_18_OBJ_18.setForeground(Color.white);
    OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");
    add(OBJ_18_OBJ_18);
    OBJ_18_OBJ_18.setBounds(0, 5, 440, 26);

    //---- OBJ_19_OBJ_19 ----
    OBJ_19_OBJ_19.setText("@EVLB2@");
    OBJ_19_OBJ_19.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_19_OBJ_19.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_19_OBJ_19.setForeground(Color.white);
    OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
    add(OBJ_19_OBJ_19);
    OBJ_19_OBJ_19.setBounds(0, 55, 440, 26);

    //---- BT_ENTER ----
    BT_ENTER.setText("Valider");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setMargin(new Insets(0, -15, 0, 0));
    BT_ENTER.setPreferredSize(new Dimension(110, 19));
    BT_ENTER.setMinimumSize(new Dimension(110, 1));
    BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
    BT_ENTER.setIconTextGap(25);
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed();
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(78, 135, 285, 40);

    //---- BT_RETOUR ----
    BT_RETOUR.setText(" Retour");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed();
      }
    });
    add(BT_RETOUR);
    BT_RETOUR.setBounds(78, 175, 285, 40);

    //---- NOMBRE ----
    NOMBRE.setName("NOMBRE");
    add(NOMBRE);
    NOMBRE.setBounds(185, 90, 68, NOMBRE.getPreferredSize().height);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_18_OBJ_18;
  private JLabel OBJ_19_OBJ_19;
  private JButton BT_ENTER;
  private JButton BT_RETOUR;
  private XRiTextField NOMBRE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
