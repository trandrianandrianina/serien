
package ri.serien.libecranrpg.sexp.SEXPCAFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPCAFM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXPCAFM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    CHO7.setValeursSelection("X", " ");
    CHO6.setValeursSelection("X", " ");
    CHO5.setValeursSelection("X", " ");
    CHO4.setValeursSelection("X", " ");
    CHO3.setValeursSelection("X", " ");
    CHO2.setValeursSelection("X", " ");
    CHO1.setValeursSelection("X", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    l_LOCTP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
    OBJ_32.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    OBJ_33.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    DEJJS.setVisible(lexique.isPresent("DEJJS"));
    WETB.setVisible(lexique.isPresent("WETB"));
    // CHO1.setVisible( lexique.isPresent("CHO1"));
    // CHO1.setSelected(lexique.HostFieldGetData("CHO1").equalsIgnoreCase("X"));
    // CHO4.setVisible( lexique.isPresent("CHO4"));
    // CHO4.setSelected(lexique.HostFieldGetData("CHO4").equalsIgnoreCase("X"));
    // CHO2.setVisible( lexique.isPresent("CHO2"));
    // CHO2.setSelected(lexique.HostFieldGetData("CHO2").equalsIgnoreCase("X"));
    // CHO6.setVisible( lexique.isPresent("CHO6"));
    // CHO6.setSelected(lexique.HostFieldGetData("CHO6").equalsIgnoreCase("X"));
    // CHO3.setVisible( lexique.isPresent("CHO3"));
    // CHO3.setSelected(lexique.HostFieldGetData("CHO3").equalsIgnoreCase("X"));
    // CHO5.setVisible( lexique.isPresent("CHO5"));
    // CHO5.setSelected(lexique.HostFieldGetData("CHO5").equalsIgnoreCase("X"));
    // DEDAS.setVisible( lexique.isPresent("DEDAS"));
    // CHO7.setVisible( lexique.isPresent("CHO7"));
    // CHO7.setSelected(lexique.HostFieldGetData("CHO7").equalsIgnoreCase("X"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @TITPG1@ @TITPG2@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (CHO1.isSelected())
    // lexique.HostFieldPutData("CHO1", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO1", 0, " ");
    // if (CHO4.isSelected())
    // lexique.HostFieldPutData("CHO4", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO4", 0, " ");
    // if (CHO2.isSelected())
    // lexique.HostFieldPutData("CHO2", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO2", 0, " ");
    // if (CHO6.isSelected())
    // lexique.HostFieldPutData("CHO6", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO6", 0, " ");
    // if (CHO3.isSelected())
    // lexique.HostFieldPutData("CHO3", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO3", 0, " ");
    // if (CHO5.isSelected())
    // lexique.HostFieldPutData("CHO5", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO5", 0, " ");
    // if (CHO7.isSelected())
    // lexique.HostFieldPutData("CHO7", 0, "X");
    // else
    // lexique.HostFieldPutData("CHO7", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void riBoutonRecherche1ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    l_LOCTP = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    OBJ_27 = new JXTitledSeparator();
    OBJ_32 = new RiZoneSortie();
    OBJ_33 = new RiZoneSortie();
    WETB = new XRiTextField();
    riBoutonRecherche1 = new SNBoutonRecherche();
    OBJ_20 = new JXTitledSeparator();
    OBJ_21 = new JXTitledSeparator();
    OBJ_24_OBJ_24 = new JLabel();
    DEJJS = new XRiTextField();
    DEDAS = new XRiCalendrier();
    CHO1 = new XRiCheckBox();
    CHO2 = new XRiCheckBox();
    CHO3 = new XRiCheckBox();
    CHO4 = new XRiCheckBox();
    CHO5 = new XRiCheckBox();
    CHO6 = new XRiCheckBox();
    CHO7 = new XRiCheckBox();
    BTD = new JPopupMenu();
    OBJ_13 = new JMenuItem();
    OBJ_12 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- l_LOCTP ----
          l_LOCTP.setText("@LOCTP@");
          l_LOCTP.setFont(new Font("sansserif", Font.BOLD, 12));
          l_LOCTP.setPreferredSize(new Dimension(120, 20));
          l_LOCTP.setHorizontalTextPosition(SwingConstants.LEADING);
          l_LOCTP.setHorizontalAlignment(SwingConstants.RIGHT);
          l_LOCTP.setName("l_LOCTP");
          p_tete_droite.add(l_LOCTP);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(600, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          p_contenu.setLayout(null);

          //---- OBJ_27 ----
          OBJ_27.setTitle("Etablissement s\u00e9lectionn\u00e9");
          OBJ_27.setName("OBJ_27");
          p_contenu.add(OBJ_27);
          OBJ_27.setBounds(35, 25, 535, 19);

          //---- OBJ_32 ----
          OBJ_32.setText("@DGNOM@");
          OBJ_32.setName("OBJ_32");
          p_contenu.add(OBJ_32);
          OBJ_32.setBounds(205, 55, 260, OBJ_32.getPreferredSize().height);

          //---- OBJ_33 ----
          OBJ_33.setText("@WENCX@");
          OBJ_33.setName("OBJ_33");
          p_contenu.add(OBJ_33);
          OBJ_33.setBounds(205, 85, 260, OBJ_33.getPreferredSize().height);

          //---- WETB ----
          WETB.setComponentPopupMenu(null);
          WETB.setName("WETB");
          p_contenu.add(WETB);
          WETB.setBounds(50, 70, 40, WETB.getPreferredSize().height);

          //---- riBoutonRecherche1 ----
          riBoutonRecherche1.setToolTipText("Changement d'\u00e9tablissement");
          riBoutonRecherche1.setName("riBoutonRecherche1");
          riBoutonRecherche1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              riBoutonRecherche1ActionPerformed(e);
            }
          });
          p_contenu.add(riBoutonRecherche1);
          riBoutonRecherche1.setBounds(new Rectangle(new Point(95, 70), riBoutonRecherche1.getPreferredSize()));

          //---- OBJ_20 ----
          OBJ_20.setTitle("Jours ch\u00f4m\u00e9s");
          OBJ_20.setName("OBJ_20");
          p_contenu.add(OBJ_20);
          OBJ_20.setBounds(35, 190, 535, OBJ_20.getPreferredSize().height);

          //---- OBJ_21 ----
          OBJ_21.setTitle("");
          OBJ_21.setName("OBJ_21");
          p_contenu.add(OBJ_21);
          OBJ_21.setBounds(35, 130, 535, OBJ_21.getPreferredSize().height);

          //---- OBJ_24_OBJ_24 ----
          OBJ_24_OBJ_24.setText("Donnez le 1er jour de la semaine 01");
          OBJ_24_OBJ_24.setName("OBJ_24_OBJ_24");
          p_contenu.add(OBJ_24_OBJ_24);
          OBJ_24_OBJ_24.setBounds(50, 154, 240, 20);

          //---- DEJJS ----
          DEJJS.setComponentPopupMenu(null);
          DEJJS.setName("DEJJS");
          p_contenu.add(DEJJS);
          DEJJS.setBounds(305, 150, 35, DEJJS.getPreferredSize().height);

          //---- DEDAS ----
          DEDAS.setName("DEDAS");
          p_contenu.add(DEDAS);
          DEDAS.setBounds(360, 150, 105, DEDAS.getPreferredSize().height);

          //---- CHO1 ----
          CHO1.setText("Lundi");
          CHO1.setComponentPopupMenu(BTD);
          CHO1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO1.setName("CHO1");
          p_contenu.add(CHO1);
          CHO1.setBounds(50, 225, 155, 20);

          //---- CHO2 ----
          CHO2.setText("Mardi");
          CHO2.setComponentPopupMenu(BTD);
          CHO2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO2.setName("CHO2");
          p_contenu.add(CHO2);
          CHO2.setBounds(50, 254, 155, 20);

          //---- CHO3 ----
          CHO3.setText("Mercredi");
          CHO3.setComponentPopupMenu(BTD);
          CHO3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO3.setName("CHO3");
          p_contenu.add(CHO3);
          CHO3.setBounds(50, 283, 155, 20);

          //---- CHO4 ----
          CHO4.setText("Jeudi");
          CHO4.setComponentPopupMenu(BTD);
          CHO4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO4.setName("CHO4");
          p_contenu.add(CHO4);
          CHO4.setBounds(50, 312, 155, 20);

          //---- CHO5 ----
          CHO5.setText("Vendredi");
          CHO5.setComponentPopupMenu(BTD);
          CHO5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO5.setName("CHO5");
          p_contenu.add(CHO5);
          CHO5.setBounds(305, 225, 155, 20);

          //---- CHO6 ----
          CHO6.setText("Samedi");
          CHO6.setComponentPopupMenu(BTD);
          CHO6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO6.setName("CHO6");
          p_contenu.add(CHO6);
          CHO6.setBounds(305, 254, 155, 20);

          //---- CHO7 ----
          CHO7.setText("Dimanche");
          CHO7.setComponentPopupMenu(BTD);
          CHO7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          CHO7.setName("CHO7");
          p_contenu.add(CHO7);
          CHO7.setBounds(305, 283, 155, 20);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < p_contenu.getComponentCount(); i++) {
              Rectangle bounds = p_contenu.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = p_contenu.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            p_contenu.setMinimumSize(preferredSize);
            p_contenu.setPreferredSize(preferredSize);
          }
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_13 ----
      OBJ_13.setText("Choix possibles");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);

      //---- OBJ_12 ----
      OBJ_12.setText("Aide en ligne");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel l_LOCTP;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_27;
  private RiZoneSortie OBJ_32;
  private RiZoneSortie OBJ_33;
  private XRiTextField WETB;
  private SNBoutonRecherche riBoutonRecherche1;
  private JXTitledSeparator OBJ_20;
  private JXTitledSeparator OBJ_21;
  private JLabel OBJ_24_OBJ_24;
  private XRiTextField DEJJS;
  private XRiCalendrier DEDAS;
  private XRiCheckBox CHO1;
  private XRiCheckBox CHO2;
  private XRiCheckBox CHO3;
  private XRiCheckBox CHO4;
  private XRiCheckBox CHO5;
  private XRiCheckBox CHO6;
  private XRiCheckBox CHO7;
  private JPopupMenu BTD;
  private JMenuItem OBJ_13;
  private JMenuItem OBJ_12;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
