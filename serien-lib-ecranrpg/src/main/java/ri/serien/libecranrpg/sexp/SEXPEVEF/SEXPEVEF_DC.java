
package ri.serien.libecranrpg.sexp.SEXPEVEF;
// Nom Fichier: pop_SEXPEVEF_FMTDC_174.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVEF_DC extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVEF_DC(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_NON);
    
    
    BT_OUI.setIcon(lexique.chargerImage("images/OK_p.png", true));
    BT_NON.setIcon(lexique.chargerImage("images/retour_p.png", true));
    // BT_RETOUR.setIcon(lexique.getImage("images/retour_p.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_7_OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_8_OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // En fonction du contenu de REPONS on force le DefaultButton (c'est le RPG qui pilote, on ne l'oubli pas)
    if (lexique.getHostField("REPONS").getValeurToBuffer().equals("OUI")) {
      setDefaultButton(BT_OUI);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@*TIME@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void BT_OUIActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPONS", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void BT_NONActionPerformed(ActionEvent e) {
    lexique.HostFieldPutData("REPONS", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void BT_RETOURActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_7_OBJ_7 = new JLabel();
    OBJ_8_OBJ_8 = new JLabel();
    BT_OUI = new JButton();
    BT_NON = new JButton();
    BT_RETOUR = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setPreferredSize(new Dimension(360, 185));
    setName("this");
    setLayout(null);

    //---- OBJ_7_OBJ_7 ----
    OBJ_7_OBJ_7.setText("@EVLB1@");
    OBJ_7_OBJ_7.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_7_OBJ_7.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_7_OBJ_7.setForeground(Color.white);
    OBJ_7_OBJ_7.setName("OBJ_7_OBJ_7");
    add(OBJ_7_OBJ_7);
    OBJ_7_OBJ_7.setBounds(0, 0, 360, 24);

    //---- OBJ_8_OBJ_8 ----
    OBJ_8_OBJ_8.setText("@EVLB2@");
    OBJ_8_OBJ_8.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_8_OBJ_8.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_8_OBJ_8.setForeground(Color.white);
    OBJ_8_OBJ_8.setName("OBJ_8_OBJ_8");
    add(OBJ_8_OBJ_8);
    OBJ_8_OBJ_8.setBounds(0, 25, 360, 24);

    //---- BT_OUI ----
    BT_OUI.setText("OUI");
    BT_OUI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_OUI.setMargin(new Insets(0, -15, 0, 0));
    BT_OUI.setPreferredSize(new Dimension(110, 19));
    BT_OUI.setMinimumSize(new Dimension(110, 1));
    BT_OUI.setFont(BT_OUI.getFont().deriveFont(BT_OUI.getFont().getStyle() | Font.BOLD, BT_OUI.getFont().getSize() + 2f));
    BT_OUI.setIconTextGap(25);
    BT_OUI.setName("BT_OUI");
    BT_OUI.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_OUIActionPerformed(e);
      }
    });
    add(BT_OUI);
    BT_OUI.setBounds(70, 60, 220, 40);

    //---- BT_NON ----
    BT_NON.setText("NON");
    BT_NON.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_NON.setMargin(new Insets(0, -15, 0, 0));
    BT_NON.setPreferredSize(new Dimension(120, 19));
    BT_NON.setMinimumSize(new Dimension(120, 1));
    BT_NON.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_NON.setIconTextGap(25);
    BT_NON.setName("BT_NON");
    BT_NON.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_NONActionPerformed(e);
      }
    });
    add(BT_NON);
    BT_NON.setBounds(70, 105, 220, 40);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }

    //---- BT_RETOUR ----
    BT_RETOUR.setText("Retour");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed(e);
      }
    });
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_7_OBJ_7;
  private JLabel OBJ_8_OBJ_8;
  private JButton BT_OUI;
  private JButton BT_NON;
  private JButton BT_RETOUR;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
