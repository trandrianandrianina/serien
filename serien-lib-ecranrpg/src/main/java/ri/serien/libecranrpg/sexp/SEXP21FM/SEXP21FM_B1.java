
package ri.serien.libecranrpg.sexp.SEXP21FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.commun.message.Message;
import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.label.SNLabelTitre;
import ri.serien.libswing.composant.primitif.message.SNMessage;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SEXP21FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private String[] ACTSPL_Value = { "1", "2", };
  private String[] ACTSPL_Title = { "Suspendu", "Supprimé", };
  private String[] DENSITE_Value = { "1", "2", "3", "4", "5", };
  private String[] DENSITE_Title = { "Très clair", "Clair", "Normal", "Foncé", "Très foncé", };
  private static final String BOUTON_AFFICHER_DETAIL_TECHNIQUE = "Afficher détails techniques";
  private static final String BOUTON_MASQUER_DETAIL_TECHNIQUE = "Masquer détails techniques";
  private static final String BOUTON_AFFICHER_DOCUMENTS_LIEES = "Afficher documents liés";
  private static final String BOUTON_GENERER_PDF = "Générer le PDF";
  // Modes d'impression
  private static final char MODE_IMPRESSION_IMPRESSION = 'I';
  private static final char MODE_IMPRESSION_FICHIER = 'F';
  private static final char MODE_IMPRESSION_ATTENTE = 'A';
  private static final char MODE_IMPRESSION_TERMINEE = 'T';
  private char modeImpression = ' ';
  private char precedent_modeImpression = ' ';
  private Dimension dimensionMinimalFenetre = new Dimension(870, 100);
  Message messageLabelGeneration = null;
  
  public SEXP21FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // configuration de la barre de bouton
    sNBarreBouton.ajouterBouton(BOUTON_AFFICHER_DETAIL_TECHNIQUE, 't', true);
    sNBarreBouton.ajouterBouton(BOUTON_MASQUER_DETAIL_TECHNIQUE, 'm', false);
    sNBarreBouton.ajouterBouton(BOUTON_AFFICHER_DOCUMENTS_LIEES, 'l', true);
    sNBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    sNBarreBouton.ajouterBouton(EnumBouton.FERMER, true);
    sNBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      // Traitement des actions bouton
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterClicBouton(pSNBouton);
      }
    });
    setDialog(true);
    // Ajout
    initDiverses();
    cbDensite.setValeurs(DENSITE_Value, DENSITE_Title);
    cbACTSPL.setValeurs(ACTSPL_Value, ACTSPL_Title);
    ckDuplex.setValeursSelection("1", " ");
    ckNoirblc.setValeursSelection("1", " ");
    ckDraft.setValeursSelection("1", " ");
    NOMFIC.setListeCaracteresNonAutorises(Constantes.caracteresNonAutorisesPourFichier);
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    zsImprimante.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@AEIMU@")).trim());
    tfLGIMP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LGIMP@")).trim());
    zsSpool.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@SPOOL@")).trim());
    zsFileAttente.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@OUTQSRC@")).trim());
    zsWindows.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMDOSW@")).trim());
    zsIFS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NOMDOS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    lbGeneration.setVisible(false);
    gererLesErreurs("19");
    
    tfLGIMP.setVisible(lexique.isPresent("LGIMP"));
    zsSpool.setVisible(lexique.isPresent("SPOOL"));
    zsImprimante.setVisible(lexique.isPresent("AEIMU"));
    zsIFS.setVisible(lexique.isPresent("NOMDOS"));
    zsWindows.setVisible(lexique.isPresent("NOMDOSW"));
    zsFileAttente.setVisible(lexique.isPresent("OUTQSRC"));
    OUTQIMP.setVisible(lexique.isPresent("OUTQIMP"));
    cbDensite.setEnabled(ckDraft.isSelected());
    lbDensité.setEnabled(!ckDraft.isSelected());
    precedent_modeImpression = modeImpression;
    modeImpression = lexique.HostFieldGetData("MODEIMP").charAt(0);
    
    // Permet de réactiver le bouton valider si l'utilisateur ressaisie une demande
    if (modeImpression != MODE_IMPRESSION_TERMINEE) {
      sNBarreBouton.activerBouton(EnumBouton.VALIDER, true);
    }
    // Impression directe depuis l'AS/400
    if (modeImpression == MODE_IMPRESSION_IMPRESSION) {
      pnlImpression.setVisible(true);
      pnlFichier.setVisible(false);
    }
    // Génération du PDF
    else if (modeImpression == MODE_IMPRESSION_FICHIER) {
      pnlImpression.setVisible(false);
      pnlFichier.setVisible(true);
    }
    if (modeImpression == MODE_IMPRESSION_ATTENTE) {
      messageLabelGeneration = messageLabelGeneration.getMessageNormal("Génération en cours. Veuillez patienter.");
      lbGeneration.setMessage(messageLabelGeneration);
      lbGeneration.setVisible(true);
    }
    else if (modeImpression == MODE_IMPRESSION_TERMINEE) {
      if (precedent_modeImpression == MODE_IMPRESSION_FICHIER) {
        messageLabelGeneration = messageLabelGeneration.getMessageNormal("Transfert du fichier.");
        lbGeneration.setMessage(messageLabelGeneration);
        lbGeneration.setVisible(true);
        afficheFichier();
        messageLabelGeneration = messageLabelGeneration.getMessageNormal("Transfert terminé.");
      }
      else {
        messageLabelGeneration = messageLabelGeneration.getMessageNormal("Génération terminé.");
      }
      lbGeneration.setMessage(messageLabelGeneration);
      lbGeneration.setVisible(true);
      sNBarreBouton.activerBouton(EnumBouton.VALIDER, false);
    }
    
    pnlSpool.setVisible(false);
    // Titre
    setTitle(interpreteurD.analyseExpression("Informations complémentaires"));
    redimensionnerPanel();
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void btTraiterClicBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(BOUTON_AFFICHER_DETAIL_TECHNIQUE)) {
        pnlSpool.setVisible(!pnlSpool.isVisible());
        redimensionnerPanel();
        sNBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL_TECHNIQUE, false);
        sNBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL_TECHNIQUE, true);
        lbGeneration.setVisible(false);
      }
      else if (pSNBouton.isBouton(BOUTON_MASQUER_DETAIL_TECHNIQUE)) {
        pnlSpool.setVisible(!pnlSpool.isVisible());
        redimensionnerPanel();
        sNBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL_TECHNIQUE, true);
        sNBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL_TECHNIQUE, false);
        lbGeneration.setVisible(false);
      }
      else if (pSNBouton.isBouton(BOUTON_AFFICHER_DOCUMENTS_LIEES)) {
        messageLabelGeneration = messageLabelGeneration.getMessageImportant(
            "Vous ne pouvez pas consulter les documents liés car ils ne sont pas paramétrés sur votre bibliothèque.");
        // Si aucun documents liées n'est présent, lbGeneration deviendra un message d'erreur
        try {
          lexique.ouvrirExplorer(interpreteurD.analyseExpression("@NOMDOSW@").trim());
        }
        catch (Exception e) {
          lbGeneration.setMessage(messageLabelGeneration);
          lbGeneration.setVisible(true);
          redimensionnerPanel();
        }
      }
      else if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        verificationAffichageTechnique();
        // Vérifie si le nom donne au fichier PDF n'est pas vide
        if (NOMFIC.getText().isEmpty() == true) {
          messageLabelGeneration = messageLabelGeneration.getMessageImportant("Veuillez donner un nom de fichier.");
          lbGeneration.setMessage(messageLabelGeneration);
          lbGeneration.setVisible(true);
          return;
        }
        lexique.HostScreenSendKey(this, "F7");
      }
      else if (pSNBouton.isBouton(EnumBouton.FERMER)) {
        reinitialisePanel();
        lexique.HostScreenSendKey(this, "F3");
        verificationAffichageTechnique();
        // Permet de réactiver le bouton car on le désactiver apres une premiere edition
        // Voir if ((pre_modeimp == 'F') && (modeimp == 'T'))
        sNBarreBouton.activerBouton(EnumBouton.VALIDER, true);
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void reinitialisePanel() {
    // On réinitialise les variables qui définissent l'état
    modeImpression = ' ';
    precedent_modeImpression = ' ';
  }
  
  private void redimensionnerPanel() {
    // Permet d'initialiser la taille du panel this
    this.setSize(dimensionMinimalFenetre);
    this.setPreferredSize(dimensionMinimalFenetre);
    // Si pnlSpool est visible, vas mettre la taille de celui-ci dans le this
    if (pnlSpool.isVisible()) {
      this.setPreferredSize(
          new Dimension(this.getPreferredSize().width, this.getPreferredSize().height + pnlSpool.getPreferredSize().height + 10));
    }
    // Si pnlImpression est visible, vas mettre la taille de celui-ci dans le this
    if (pnlImpression.isVisible()) {
      this.setPreferredSize(
          new Dimension(this.getPreferredSize().width, this.getPreferredSize().height + pnlImpression.getPreferredSize().height + 10));
    }
    // Si pnlFichier est visible, vas mettre la taille de celui-ci dans le this
    if (pnlFichier.isVisible()) {
      this.setPreferredSize(
          new Dimension(this.getPreferredSize().width, this.getPreferredSize().height + pnlFichier.getPreferredSize().height + 10));
    }
    // Si lbGeneration est visible vas mettre la taille de celui ci dans le this
    if (lbGeneration.isVisible()) {
      this.setPreferredSize(
          new Dimension(this.getPreferredSize().width, this.getPreferredSize().height + lbGeneration.getPreferredSize().height + 10));
    }
    // Vas modifier la taille du JDialogue afin qu'il soit en accord avec le this
    if (dialogue != null) {
      dialogue.setSize(this.getPreferredSize());
      dialogue.setPreferredSize(this.getPreferredSize());
    }
    validate();
    repaint();
  }
  
  // -- Méthodes Evènementielles ---------------------------------------------
  private void DRAFTA(ActionEvent e) {
    cbDensite.setEnabled(ckDraft.isSelected());
    lbDensité.setEnabled(ckDraft.isSelected());
  }
  
  // Permet d'éviter que la touche masquer s'affiche si le pnlSpool est présent
  private void verificationAffichageTechnique() {
    if (pnlSpool.isVisible()) {
      sNBarreBouton.activerBouton(BOUTON_AFFICHER_DETAIL_TECHNIQUE, true);
      sNBarreBouton.activerBouton(BOUTON_MASQUER_DETAIL_TECHNIQUE, false);
    }
  }
  
  /**
   * Affichage du fichier PDF
   */
  private void afficheFichier() {
    getParent().setCursor(new Cursor(Cursor.WAIT_CURSOR));
    String fichier = interpreteurD.analyseExpression("@NOMDOS@");
    String extension = interpreteurD.analyseExpression("@EXTDOC@").toLowerCase();
    if (fichier.endsWith("/")) {
      fichier = fichier + NOMFIC.getText().trim() + '.' + extension;
    }
    else {
      fichier = fichier + '/' + NOMFIC.getText().trim() + '.' + extension;
    }
    if (extension.equals("")) {
      JOptionPane.showMessageDialog(this, "Le fichier " + fichier + " ne peut être récupéré car les données sont incorrectes.",
          "Erreur sur SEXP21FM - afficheFichier()", JOptionPane.ERROR_MESSAGE);
    }
    fichier = lexique.RecuperationFichier(fichier);
    lexique.AfficheDocument(fichier);
    getParent().setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
  }
  
  private void pnlSpoolComponentShown(ComponentEvent e) {
    int hauteur = this.getHeight();
    this.setPreferredSize(getPreferredSize());
  }
  
  private void pnlSpoolComponentHidden(ComponentEvent e) {
    int hauteur = this.getHeight();
    this.setSize(getPreferredSize());
  }
  
  // Permet lors de l'édition d'un PDF, d'utiliser la touche entrée pour valider
  private void appuieToucheEntree(KeyEvent e) {
    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
      verificationAffichageTechnique();
      // remplacement des blancs par des underscore car certains gestionnaires de PDF ne supportent pas les noms de fichiers avec espaces
      NOMFIC.setText(NOMFIC.getText().trim().replaceAll(" ", "_"));
      // Vérifie si le nom donne au fichier PDF n'est pas vide
      if (NOMFIC.getText().isEmpty() == true) {
        messageLabelGeneration = messageLabelGeneration.getMessageImportant("Veuillez donner un nom de fichier.");
        lbGeneration.setMessage(messageLabelGeneration);
        lbGeneration.setVisible(true);
        return;
      }
      lexique.HostScreenSendKey(this, "F7");
    }
  }
  
  private void thisKeyPressed(KeyEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void DRAFTActionPerformed(ActionEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void panel1ComponentShown(ComponentEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void panel1ComponentHidden(ComponentEvent e) {
    try {
      // TODO Saisissez votre code
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  /**
   * 
   * Evènement sur changement de focus sur le nom de fichier
   * Remplacement des blancs par des underscore car certains gestionnaires de PDF ne supportent pas les noms de fichiers avec espaces.
   * 
   * @param e
   */
  private void NOMFICFocusLost(FocusEvent e) {
    try {
      NOMFIC.setText(NOMFIC.getText().trim().replaceAll(" ", "_"));
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    pnlprincipal = new SNPanelContenu();
    pnlFichier = new SNPanelTitre();
    lbTitre = new SNLabelTitre();
    lbNomFichier = new SNLabelChamp();
    NOMFIC = new XRiTextField();
    lbExtension = new SNLabelChamp();
    pnlImpression = new SNPanelTitre();
    ckDraft = new XRiCheckBox();
    lbNbrCopie = new SNLabelChamp();
    ckNoirblc = new XRiCheckBox();
    lbTiroirEntrée = new SNLabelChamp();
    lbDensité = new SNLabelChamp();
    NBRCPY = new XRiTextField();
    TIROIRE = new XRiTextField();
    OUTQIMP = new XRiTextField();
    lbOUTQ = new SNLabelChamp();
    lbImprimante = new SNLabelChamp();
    zsImprimante = new SNTexte();
    lbLangageImprimante = new SNLabelChamp();
    tfLGIMP = new SNTexte();
    ckDuplex = new XRiCheckBox();
    cbDensite = new XRiComboBox();
    lbTiroirSortie = new SNLabelChamp();
    TIROIRS = new XRiTextField();
    pnlSpool = new SNPanelTitre();
    lbDoitEtre = new SNLabelChamp();
    cbACTSPL = new XRiComboBox();
    zsSpool = new SNTexte();
    lbFileAttente = new SNLabelChamp();
    zsFileAttente = new SNTexte();
    lbSpool = new SNLabelChamp();
    lbWindows = new SNLabelChamp();
    zsWindows = new SNTexte();
    zsIFS = new SNTexte();
    lbIFS = new SNLabelChamp();
    lbGeneration = new SNMessage();
    sNBarreBouton = new SNBarreBouton();
    
    // ======== this ========
    setMinimumSize(new Dimension(870, 515));
    setPreferredSize(new Dimension(870, 515));
    setMaximumSize(new Dimension(870, 515));
    setInheritsPopupMenu(true);
    setBackground(new Color(239, 239, 222));
    setName("this");
    addKeyListener(new KeyAdapter() {
      @Override
      public void keyPressed(KeyEvent e) {
        thisKeyPressed(e);
      }
    });
    setLayout(new BorderLayout());
    
    // ======== pnlprincipal ========
    {
      pnlprincipal.setName("pnlprincipal");
      pnlprincipal.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlprincipal.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlprincipal.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
      ((GridBagLayout) pnlprincipal.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlprincipal.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
      
      // ======== pnlFichier ========
      {
        pnlFichier.setTitre("Document li\u00e9");
        pnlFichier.setName("pnlFichier");
        pnlFichier.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlFichier.getLayout()).columnWidths = new int[] { 0, 0, 0, 0 };
        ((GridBagLayout) pnlFichier.getLayout()).rowHeights = new int[] { 0, 0, 0 };
        ((GridBagLayout) pnlFichier.getLayout()).columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlFichier.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
        
        // ---- lbTitre ----
        lbTitre.setText("L'\u00e9dition est export\u00e9e dans les documents li\u00e9s avec le nom de fichier saisi ci-dessous.");
        lbTitre.setName("lbTitre");
        pnlFichier.add(lbTitre, new GridBagConstraints(0, 0, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbNomFichier ----
        lbNomFichier.setText("Nom du fichier");
        lbNomFichier.setName("lbNomFichier");
        pnlFichier.add(lbNomFichier, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- NOMFIC ----
        NOMFIC.setFont(new Font("sansserif", Font.PLAIN, 14));
        NOMFIC.setName("NOMFIC");
        NOMFIC.addKeyListener(new KeyAdapter() {
          @Override
          public void keyPressed(KeyEvent e) {
            appuieToucheEntree(e);
          }
        });
        NOMFIC.addFocusListener(new FocusAdapter() {
          @Override
          public void focusLost(FocusEvent e) {
            NOMFICFocusLost(e);
          }
        });
        pnlFichier.add(NOMFIC, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbExtension ----
        lbExtension.setText("(sans extension)");
        lbExtension.setHorizontalAlignment(SwingConstants.LEADING);
        lbExtension.setName("lbExtension");
        pnlFichier.add(lbExtension, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlprincipal.add(pnlFichier,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlImpression ========
      {
        pnlImpression.setTitre("Options d'imprimantes");
        pnlImpression.setName("pnlImpression");
        pnlImpression.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlImpression.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlImpression.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlImpression.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlImpression.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- ckDraft ----
        ckDraft.setText("Economie d'encre");
        ckDraft.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckDraft.setFont(new Font("sansserif", Font.PLAIN, 14));
        ckDraft.setName("ckDraft");
        ckDraft.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
            DRAFTActionPerformed(e);
          }
        });
        pnlImpression.add(ckDraft, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbNbrCopie ----
        lbNbrCopie.setText("Nombre copie");
        lbNbrCopie.setName("lbNbrCopie");
        pnlImpression.add(lbNbrCopie, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ckNoirblc ----
        ckNoirblc.setText("Noir & blanc");
        ckNoirblc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckNoirblc.setFont(new Font("sansserif", Font.PLAIN, 14));
        ckNoirblc.setName("ckNoirblc");
        pnlImpression.add(ckNoirblc, new GridBagConstraints(3, 2, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTiroirEntrée ----
        lbTiroirEntrée.setText("Tiroir d'entr\u00e9e");
        lbTiroirEntrée.setName("lbTiroirEntr\u00e9e");
        pnlImpression.add(lbTiroirEntrée, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- lbDensité ----
        lbDensité.setText("Densit\u00e9");
        lbDensité.setName("lbDensit\u00e9");
        pnlImpression.add(lbDensité, new GridBagConstraints(3, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- NBRCPY ----
        NBRCPY.setFont(new Font("sansserif", Font.PLAIN, 14));
        NBRCPY.setName("NBRCPY");
        pnlImpression.add(NBRCPY, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- TIROIRE ----
        TIROIRE.setFont(new Font("sansserif", Font.PLAIN, 14));
        TIROIRE.setName("TIROIRE");
        pnlImpression.add(TIROIRE, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- OUTQIMP ----
        OUTQIMP.setFont(new Font("sansserif", Font.PLAIN, 14));
        OUTQIMP.setName("OUTQIMP");
        pnlImpression.add(OUTQIMP, new GridBagConstraints(1, 1, 4, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbOUTQ ----
        lbOUTQ.setText("File de sortie (OUTQ)");
        lbOUTQ.setName("lbOUTQ");
        pnlImpression.add(lbOUTQ, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbImprimante ----
        lbImprimante.setText("Imprimante");
        lbImprimante.setName("lbImprimante");
        pnlImpression.add(lbImprimante, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsImprimante ----
        zsImprimante.setText("@AEIMU@");
        zsImprimante.setEditable(false);
        zsImprimante.setName("zsImprimante");
        pnlImpression.add(zsImprimante, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbLangageImprimante ----
        lbLangageImprimante.setText("Langage imprimante");
        lbLangageImprimante.setName("lbLangageImprimante");
        pnlImpression.add(lbLangageImprimante, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
            GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- tfLGIMP ----
        tfLGIMP.setText("@LGIMP@");
        tfLGIMP.setName("tfLGIMP");
        pnlImpression.add(tfLGIMP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- ckDuplex ----
        ckDuplex.setText("Recto-verso");
        ckDuplex.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        ckDuplex.setFont(new Font("sansserif", Font.PLAIN, 14));
        ckDuplex.setName("ckDuplex");
        pnlImpression.add(ckDuplex, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbDensite ----
        cbDensite.setModel(
            new DefaultComboBoxModel(new String[] { "Tr\u00e8s clair", "Clair", "Normal", "Fonc\u00e9", "Tr\u00e8s fonc\u00e9" }));
        cbDensite.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cbDensite.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbDensite.setName("cbDensite");
        pnlImpression.add(cbDensite, new GridBagConstraints(4, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbTiroirSortie ----
        lbTiroirSortie.setText("Tiroir de sortie");
        lbTiroirSortie.setName("lbTiroirSortie");
        pnlImpression.add(lbTiroirSortie, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
        
        // ---- TIROIRS ----
        TIROIRS.setForeground(Color.black);
        TIROIRS.setName("TIROIRS");
        pnlImpression.add(TIROIRS, new GridBagConstraints(3, 4, 2, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
      }
      pnlprincipal.add(pnlImpression,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== pnlSpool ========
      {
        pnlSpool.setTitre("D\u00e9tails techniques");
        pnlSpool.setName("pnlSpool");
        pnlSpool.addComponentListener(new ComponentAdapter() {
          @Override
          public void componentHidden(ComponentEvent e) {
            panel1ComponentHidden(e);
          }
          
          @Override
          public void componentShown(ComponentEvent e) {
            panel1ComponentShown(e);
          }
        });
        pnlSpool.setLayout(new GridBagLayout());
        ((GridBagLayout) pnlSpool.getLayout()).columnWidths = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSpool.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
        ((GridBagLayout) pnlSpool.getLayout()).columnWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        ((GridBagLayout) pnlSpool.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
        
        // ---- lbDoitEtre ----
        lbDoitEtre.setText("doit \u00eatre");
        lbDoitEtre.setName("lbDoitEtre");
        pnlSpool.add(lbDoitEtre, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- cbACTSPL ----
        cbACTSPL.setModel(new DefaultComboBoxModel(new String[] { "Suspendu", "Supprim\u00e9" }));
        cbACTSPL.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        cbACTSPL.setFont(new Font("sansserif", Font.PLAIN, 14));
        cbACTSPL.setName("cbACTSPL");
        pnlSpool.add(cbACTSPL, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- zsSpool ----
        zsSpool.setText("@SPOOL@");
        zsSpool.setName("zsSpool");
        pnlSpool.add(zsSpool, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbFileAttente ----
        lbFileAttente.setText("File attente");
        lbFileAttente.setName("lbFileAttente");
        pnlSpool.add(lbFileAttente, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsFileAttente ----
        zsFileAttente.setText("@OUTQSRC@");
        zsFileAttente.setName("zsFileAttente");
        pnlSpool.add(zsFileAttente, new GridBagConstraints(1, 1, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- lbSpool ----
        lbSpool.setText("Le spool");
        lbSpool.setName("lbSpool");
        pnlSpool.add(lbSpool, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- lbWindows ----
        lbWindows.setText("Dossier windows");
        lbWindows.setName("lbWindows");
        pnlSpool.add(lbWindows, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
        
        // ---- zsWindows ----
        zsWindows.setText("@NOMDOSW@");
        zsWindows.setName("zsWindows");
        pnlSpool.add(zsWindows, new GridBagConstraints(1, 2, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
        
        // ---- zsIFS ----
        zsIFS.setText("@NOMDOS@");
        zsIFS.setName("zsIFS");
        pnlSpool.add(zsIFS, new GridBagConstraints(1, 3, 3, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
        
        // ---- lbIFS ----
        lbIFS.setText("Dossier dans IFS");
        lbIFS.setName("lbIFS");
        pnlSpool.add(lbIFS, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));
      }
      pnlprincipal.add(pnlSpool,
          new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ---- lbGeneration ----
      lbGeneration.setText("Label G\u00e9n\u00e9ration");
      lbGeneration.setPreferredSize(new Dimension(150, 25));
      lbGeneration.setMinimumSize(new Dimension(150, 25));
      lbGeneration.setMaximumSize(new Dimension(150, 25));
      lbGeneration.setName("lbGeneration");
      pnlprincipal.add(lbGeneration,
          new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
    }
    add(pnlprincipal, BorderLayout.CENTER);
    
    // ---- sNBarreBouton ----
    sNBarreBouton.setMaximumSize(new Dimension(0, 65));
    sNBarreBouton.setName("sNBarreBouton");
    add(sNBarreBouton, BorderLayout.SOUTH);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNPanelContenu pnlprincipal;
  private SNPanelTitre pnlFichier;
  private SNLabelTitre lbTitre;
  private SNLabelChamp lbNomFichier;
  private XRiTextField NOMFIC;
  private SNLabelChamp lbExtension;
  private SNPanelTitre pnlImpression;
  private XRiCheckBox ckDraft;
  private SNLabelChamp lbNbrCopie;
  private XRiCheckBox ckNoirblc;
  private SNLabelChamp lbTiroirEntrée;
  private SNLabelChamp lbDensité;
  private XRiTextField NBRCPY;
  private XRiTextField TIROIRE;
  private XRiTextField OUTQIMP;
  private SNLabelChamp lbOUTQ;
  private SNLabelChamp lbImprimante;
  private SNTexte zsImprimante;
  private SNLabelChamp lbLangageImprimante;
  private SNTexte tfLGIMP;
  private XRiCheckBox ckDuplex;
  private XRiComboBox cbDensite;
  private SNLabelChamp lbTiroirSortie;
  private XRiTextField TIROIRS;
  private SNPanelTitre pnlSpool;
  private SNLabelChamp lbDoitEtre;
  private XRiComboBox cbACTSPL;
  private SNTexte zsSpool;
  private SNLabelChamp lbFileAttente;
  private SNTexte zsFileAttente;
  private SNLabelChamp lbSpool;
  private SNLabelChamp lbWindows;
  private SNTexte zsWindows;
  private SNTexte zsIFS;
  private SNLabelChamp lbIFS;
  private SNMessage lbGeneration;
  private SNBarreBouton sNBarreBouton;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
