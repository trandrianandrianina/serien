
package ri.serien.libecranrpg.sexp.SEXPEVFM;
// Nom Fichier: pop_SEXPEVFM_FMTAS_FMTF1_173.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVFM_AS extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXPEVFM_AS(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EV998@")).trim());
    OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVPGM@")).trim());
    OBJ_10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*USER@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EV999@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    OBJ_14.setIcon(lexique.chargerImage("images/msgbox04.gif", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK.png", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_13 = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_14 = new JLabel();
    OBJ_15 = new JLabel();
    OBJ_9 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_6 = new JLabel();
    OBJ_5 = new JLabel();
    OBJ_10 = new JLabel();
    OBJ_11 = new JLabel();
    OBJ_7 = new JLabel();
    BT_ENTER = new JButton();

    //======== this ========
    setBackground(new Color(239, 239, 222));
    setName("this");

    //======== OBJ_13 ========
    {
      OBJ_13.setBorder(new TitledBorder(" "));
      OBJ_13.setBackground(new Color(239, 239, 222));
      OBJ_13.setName("OBJ_13");

      //---- OBJ_18 ----
      OBJ_18.setText("SERIE M    ASSISTANCE");
      OBJ_18.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_18.setFont(OBJ_18.getFont().deriveFont(OBJ_18.getFont().getSize() + 3f));
      OBJ_18.setName("OBJ_18");

      //---- OBJ_14 ----
      OBJ_14.setIcon(new ImageIcon("images/msgbox04.gif"));
      OBJ_14.setName("OBJ_14");

      //---- OBJ_15 ----
      OBJ_15.setText("Faites une impression \u00e9cran et veuillez prendre contact avec");
      OBJ_15.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_15.setFont(OBJ_15.getFont().deriveFont(OBJ_15.getFont().getSize() + 3f));
      OBJ_15.setName("OBJ_15");

      GroupLayout OBJ_13Layout = new GroupLayout(OBJ_13);
      OBJ_13.setLayout(OBJ_13Layout);
      OBJ_13Layout.setHorizontalGroup(
        OBJ_13Layout.createParallelGroup()
          .addGroup(OBJ_13Layout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE)
            .addGap(18, 18, 18)
            .addGroup(OBJ_13Layout.createParallelGroup()
              .addComponent(OBJ_15, GroupLayout.PREFERRED_SIZE, 396, GroupLayout.PREFERRED_SIZE)
              .addGroup(OBJ_13Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 396, GroupLayout.PREFERRED_SIZE))))
      );
      OBJ_13Layout.setVerticalGroup(
        OBJ_13Layout.createParallelGroup()
          .addGroup(OBJ_13Layout.createSequentialGroup()
            .addGap(28, 28, 28)
            .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 43, GroupLayout.PREFERRED_SIZE))
          .addGroup(OBJ_13Layout.createSequentialGroup()
            .addGap(12, 12, 12)
            .addComponent(OBJ_15)
            .addGap(13, 13, 13)
            .addComponent(OBJ_18))
      );
    }

    //---- OBJ_9 ----
    OBJ_9.setText("@EVLB1@");
    OBJ_9.setName("OBJ_9");

    //---- OBJ_12 ----
    OBJ_12.setText("@EVLB2@");
    OBJ_12.setName("OBJ_12");

    //---- OBJ_8 ----
    OBJ_8.setText("@EV998@");
    OBJ_8.setName("OBJ_8");

    //---- OBJ_6 ----
    OBJ_6.setText("ANOMALIE SYSTEME");
    OBJ_6.setFont(new Font("sansserif", Font.BOLD, 16));
    OBJ_6.setForeground(new Color(255, 0, 51));
    OBJ_6.setName("OBJ_6");

    //---- OBJ_5 ----
    OBJ_5.setText("@EVPGM@");
    OBJ_5.setName("OBJ_5");

    //---- OBJ_10 ----
    OBJ_10.setText("@*USER@");
    OBJ_10.setName("OBJ_10");

    //---- OBJ_11 ----
    OBJ_11.setText("@*TIME@");
    OBJ_11.setName("OBJ_11");

    //---- OBJ_7 ----
    OBJ_7.setText("@EV999@");
    OBJ_7.setFont(new Font("sansserif", Font.BOLD, 16));
    OBJ_7.setForeground(new Color(255, 0, 51));
    OBJ_7.setName("OBJ_7");

    //---- BT_ENTER ----
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setMargin(new Insets(0, -15, 0, 0));
    BT_ENTER.setPreferredSize(new Dimension(110, 19));
    BT_ENTER.setMinimumSize(new Dimension(110, 1));
    BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
    BT_ENTER.setIconTextGap(25);
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed();
      }
    });

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(18, 18, 18)
              .addComponent(OBJ_5, GroupLayout.PREFERRED_SIZE, 109, GroupLayout.PREFERRED_SIZE)
              .addGap(3, 3, 3)
              .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 175, GroupLayout.PREFERRED_SIZE)
              .addGap(1, 1, 1)
              .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 64, GroupLayout.PREFERRED_SIZE)
              .addGap(0, 0, 0)
              .addComponent(OBJ_8, GroupLayout.PREFERRED_SIZE, 179, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(18, 18, 18)
              .addComponent(OBJ_9, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)
              .addGap(87, 87, 87)
              .addComponent(OBJ_10, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(18, 18, 18)
              .addComponent(OBJ_12, GroupLayout.PREFERRED_SIZE, 333, GroupLayout.PREFERRED_SIZE)
              .addGap(87, 87, 87)
              .addComponent(OBJ_11, GroupLayout.PREFERRED_SIZE, 111, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(12, 12, 12)
              .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
              .addGap(489, 489, 489)
              .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 51, GroupLayout.PREFERRED_SIZE)))
          .addContainerGap(9, Short.MAX_VALUE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
            .addComponent(OBJ_7, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
            .addGroup(layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addGroup(layout.createParallelGroup()
                .addComponent(OBJ_5)
                .addComponent(OBJ_8))))
          .addGap(6, 6, 6)
          .addGroup(layout.createParallelGroup()
            .addComponent(OBJ_9)
            .addComponent(OBJ_10))
          .addGap(6, 6, 6)
          .addGroup(layout.createParallelGroup()
            .addGroup(layout.createSequentialGroup()
              .addGap(2, 2, 2)
              .addComponent(OBJ_12))
            .addComponent(OBJ_11))
          .addGap(13, 13, 13)
          .addComponent(OBJ_13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
          .addGap(3, 3, 3)
          .addComponent(BT_ENTER, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
          .addContainerGap())
    );
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel OBJ_13;
  private JLabel OBJ_18;
  private JLabel OBJ_14;
  private JLabel OBJ_15;
  private JLabel OBJ_9;
  private JLabel OBJ_12;
  private JLabel OBJ_8;
  private JLabel OBJ_6;
  private JLabel OBJ_5;
  private JLabel OBJ_10;
  private JLabel OBJ_11;
  private JLabel OBJ_7;
  private JButton BT_ENTER;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
