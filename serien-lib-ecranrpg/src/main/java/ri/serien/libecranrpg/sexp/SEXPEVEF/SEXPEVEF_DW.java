
package ri.serien.libecranrpg.sexp.SEXPEVEF;
// Nom Fichier: pop_SEXPEVEF_FMTDW_208.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVEF_DW extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVEF_DW(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_RETOUR);
    
    // Menu Command
    // setMenuCommand(OBJ_4);
    
    
    BT_RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_19_OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_20_OBJ_20.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    OBJ_23_OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // En fonction du contenu de REPONS on force le DefaultButton (c'est le RPG qui pilote, on ne l'oubli pas)
    if (lexique.getHostField("REPONW").getValeurToBuffer().equals("OUI")) {
      setDefaultButton(BT_ENTER);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Evènement Série N"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_5ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void OBJ_8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "Attention");
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostFieldPutData("REPONW", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void BT_RETOURActionPerformed() {
    lexique.HostFieldPutData("REPONW", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_19_OBJ_19 = new JLabel();
    OBJ_20_OBJ_20 = new JLabel();
    DATDEB = new XRiCalendrier();
    OBJ_17_OBJ_17 = new JLabel();
    OBJ_23_OBJ_23 = new JLabel();
    BT_ENTER = new JButton();
    BT_RETOUR = new JButton();
    OBJ_4 = new JMenu();
    OBJ_5 = new JMenuItem();
    OBJ_7 = new JMenuItem();
    OBJ_8 = new JMenuItem();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setMinimumSize(new Dimension(410, 190));
    setPreferredSize(new Dimension(410, 190));
    setName("this");
    setLayout(null);

    //---- OBJ_19_OBJ_19 ----
    OBJ_19_OBJ_19.setText("@EVLB1@");
    OBJ_19_OBJ_19.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_19_OBJ_19.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_19_OBJ_19.setForeground(Color.white);
    OBJ_19_OBJ_19.setName("OBJ_19_OBJ_19");
    add(OBJ_19_OBJ_19);
    OBJ_19_OBJ_19.setBounds(0, 5, 410, 26);

    //---- OBJ_20_OBJ_20 ----
    OBJ_20_OBJ_20.setText("@EVLB2@");
    OBJ_20_OBJ_20.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_20_OBJ_20.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_20_OBJ_20.setForeground(Color.white);
    OBJ_20_OBJ_20.setName("OBJ_20_OBJ_20");
    add(OBJ_20_OBJ_20);
    OBJ_20_OBJ_20.setBounds(0, 30, 410, 26);

    //---- DATDEB ----
    DATDEB.setComponentPopupMenu(BTD);
    DATDEB.setName("DATDEB");
    add(DATDEB);
    DATDEB.setBounds(155, 70, 125, DATDEB.getPreferredSize().height);

    //---- OBJ_17_OBJ_17 ----
    OBJ_17_OBJ_17.setText("Date");
    OBJ_17_OBJ_17.setForeground(Color.white);
    OBJ_17_OBJ_17.setName("OBJ_17_OBJ_17");
    add(OBJ_17_OBJ_17);
    OBJ_17_OBJ_17.setBounds(65, 74, 82, 20);

    //---- OBJ_23_OBJ_23 ----
    OBJ_23_OBJ_23.setText("@*TIME@");
    OBJ_23_OBJ_23.setForeground(Color.white);
    OBJ_23_OBJ_23.setName("OBJ_23_OBJ_23");
    add(OBJ_23_OBJ_23);
    OBJ_23_OBJ_23.setBounds(300, 75, 70, 18);

    //---- BT_ENTER ----
    BT_ENTER.setText("Valider");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setMargin(new Insets(0, -15, 0, 0));
    BT_ENTER.setPreferredSize(new Dimension(110, 19));
    BT_ENTER.setMinimumSize(new Dimension(110, 1));
    BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
    BT_ENTER.setIconTextGap(25);
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed();
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(65, 105, 285, 40);

    //---- BT_RETOUR ----
    BT_RETOUR.setText(" Retour");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed();
      }
    });
    add(BT_RETOUR);
    BT_RETOUR.setBounds(65, 145, 285, 40);

    {
      // compute preferred size
      Dimension preferredSize = new Dimension();
      for(int i = 0; i < getComponentCount(); i++) {
        Rectangle bounds = getComponent(i).getBounds();
        preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
        preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
      }
      Insets insets = getInsets();
      preferredSize.width += insets.right;
      preferredSize.height += insets.bottom;
      setMinimumSize(preferredSize);
      setPreferredSize(preferredSize);
    }

    //======== OBJ_4 ========
    {
      OBJ_4.setText("Commandes");
      OBJ_4.setName("OBJ_4");

      //---- OBJ_5 ----
      OBJ_5.setText("Fin de Travail");
      OBJ_5.setName("OBJ_5");
      OBJ_5.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_5ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_5);

      //---- OBJ_7 ----
      OBJ_7.setText("Annuler");
      OBJ_7.setName("OBJ_7");
      OBJ_7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_7ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_7);

      //---- OBJ_8 ----
      OBJ_8.setText("Exploitation");
      OBJ_8.setName("OBJ_8");
      OBJ_8.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_8ActionPerformed(e);
        }
      });
      OBJ_4.add(OBJ_8);
    }

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Aide en ligne");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_11 ----
      OBJ_11.setText("Invite");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_19_OBJ_19;
  private JLabel OBJ_20_OBJ_20;
  private XRiCalendrier DATDEB;
  private JLabel OBJ_17_OBJ_17;
  private JLabel OBJ_23_OBJ_23;
  private JButton BT_ENTER;
  private JButton BT_RETOUR;
  private JMenu OBJ_4;
  private JMenuItem OBJ_5;
  private JMenuItem OBJ_7;
  private JMenuItem OBJ_8;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
