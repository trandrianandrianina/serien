/*
 * Created by JFormDesigner on Sat May 03 15:54:27 CEST 2008
 */

package ri.serien.libecranrpg.sexp.SEXPEVEF;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stephane VENERI
 */
public class SEXPEVEF_D0 extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVEF_D0(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Bouton par défaut
    setDefaultButton(BT_RETOUR);
    
    
    BT_RETOUR.setIcon(lexique.chargerImage("images/retour_p.png", true));
    BT_ENTER.setIcon(lexique.chargerImage("images/OK_p.png", true));
    // BT_LOT.setIcon(lexique.getImage("images/lot1.gif", true));
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVL42@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // En fonction du contenu de REPONS on force le DefaultButton (c'est le RPG qui pilote, on ne l'oubli pas)
    if (lexique.getHostField("REPONS").getValeurToBuffer().equals("OUI")) {
      setDefaultButton(BT_ENTER);
    }
    
    // Traitement spécifique pour le traitement par lot/soumission
    // Pas forcément bo mais évite la modif de 250000 batchs
    String chaine = interpreteurD.analyseExpression("@EVL42@");
    if (chaine != null) {
      if (chaine.toLowerCase().startsWith("traitement")) {
        Lot_CHK.setSelected(true);
        label1.setText("Demande enregistrée");
      }
    }
    
    setTitle(interpreteurD.analyseExpression("@EVL41@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostFieldPutData("REPONS", 0, "OUI");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void BT_RETOURActionPerformed() {
    lexique.HostFieldPutData("REPONS", 0, "NON");
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void chk_LotActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6");
    lexique.ClosePanel();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJECT_3 = new JLabel();
    label1 = new JLabel();
    BT_ENTER = new JButton();
    BT_RETOUR = new JButton();
    Lot_CHK = new JCheckBox();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setPreferredSize(new Dimension(360, 200));
    setName("this");
    setLayout(null);

    //---- OBJECT_3 ----
    OBJECT_3.setText("Veuillez confirmer");
    OBJECT_3.setHorizontalAlignment(SwingConstants.CENTER);
    OBJECT_3.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJECT_3.setForeground(Color.white);
    OBJECT_3.setName("OBJECT_3");
    add(OBJECT_3);
    OBJECT_3.setBounds(62, 30, 230, 25);

    //---- label1 ----
    label1.setText("@EVL42@");
    label1.setHorizontalAlignment(SwingConstants.CENTER);
    label1.setFont(new Font("sansserif", Font.PLAIN, 16));
    label1.setForeground(Color.white);
    label1.setName("label1");
    add(label1);
    label1.setBounds(62, 8, 230, 25);

    //---- BT_ENTER ----
    BT_ENTER.setText("Valider");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setMargin(new Insets(0, -15, 0, 0));
    BT_ENTER.setPreferredSize(new Dimension(110, 19));
    BT_ENTER.setMinimumSize(new Dimension(110, 1));
    BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
    BT_ENTER.setIconTextGap(25);
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed();
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(35, 70, 285, 40);

    //---- BT_RETOUR ----
    BT_RETOUR.setText(" Retour");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed();
      }
    });
    add(BT_RETOUR);
    BT_RETOUR.setBounds(35, 110, 285, 40);

    //---- Lot_CHK ----
    Lot_CHK.setText("Travail en arri\u00e8re-plan");
    Lot_CHK.setForeground(Color.white);
    Lot_CHK.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    Lot_CHK.setName("Lot_CHK");
    Lot_CHK.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        chk_LotActionPerformed(e);
      }
    });
    add(Lot_CHK);
    Lot_CHK.setBounds(40, 165, 285, Lot_CHK.getPreferredSize().height);

    setPreferredSize(new Dimension(360, 200));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJECT_3;
  private JLabel label1;
  private JButton BT_ENTER;
  private JButton BT_RETOUR;
  private JCheckBox Lot_CHK;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
