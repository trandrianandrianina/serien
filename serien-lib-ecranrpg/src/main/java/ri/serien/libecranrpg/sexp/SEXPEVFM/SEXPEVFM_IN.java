
package ri.serien.libecranrpg.sexp.SEXPEVFM;
// Nom Fichier: pop_SEXPEVFM_FMTIN_FMTF1_178.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVFM_IN extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXPEVFM_IN(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_9);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@*TIME@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_7 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_9 = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(null);

    //---- OBJ_7 ----
    OBJ_7.setText("@EVLB1@");
    OBJ_7.setFont(new Font("sansserif", Font.BOLD, 14));
    OBJ_7.setForeground(Color.white);
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(10, 35, 395, 22);

    //---- OBJ_8 ----
    OBJ_8.setText("@EVLB2@");
    OBJ_8.setFont(new Font("sansserif", Font.BOLD, 14));
    OBJ_8.setForeground(Color.white);
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(10, 60, 395, 22);

    //---- OBJ_9 ----
    OBJ_9.setText("Retour");
    OBJ_9.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_9.setFont(new Font("sansserif", Font.BOLD, 14));
    OBJ_9.setName("OBJ_9");
    OBJ_9.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_9ActionPerformed(e);
      }
    });
    add(OBJ_9);
    OBJ_9.setBounds(95, 105, 220, 40);

    setPreferredSize(new Dimension(414, 150));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_7;
  private JLabel OBJ_8;
  private JButton OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
