
package ri.serien.libecranrpg.sexp.SEXPH6FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPH6FM_B0 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] XCETA_Value = { "", "A", "H", "R", "E", "F", "C", "*", };
  private String[] XCETA_Title = { "", "Attente", "Homologué", "Réservé", "Expédié", "Facturé", "Comptabilisé", "Tous", };
  
  public SEXPH6FM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    XCETA.setValeurs(XCETA_Value, XCETA_Title);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ01@")).trim());
    OBJ_69.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ02@")).trim());
    OBJ_70.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ03@")).trim());
    OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ04@")).trim());
    OBJ_72.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ05@")).trim());
    OBJ_73.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ06@")).trim());
    OBJ_74.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ07@")).trim());
    OBJ_75.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ08@")).trim());
    OBJ_76.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ09@")).trim());
    OBJ_77.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLZ10@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // XCETA.setSelectedIndex(getIndice("XCETA", XCETA_Value));
    XCTYP.setVisible(lexique.isPresent("XCTYP"));
    WETB.setVisible(lexique.isPresent("WETB"));
    XCZ10.setVisible(lexique.isPresent("XCZ10"));
    XCZ09.setVisible(lexique.isPresent("XCZ09"));
    XCZ08.setVisible(lexique.isPresent("XCZ08"));
    XCZ07.setVisible(lexique.isPresent("XCZ07"));
    XCZ06.setVisible(lexique.isPresent("XCZ06"));
    XCZ05.setVisible(lexique.isPresent("XCZ05"));
    XCZ04.setVisible(lexique.isPresent("XCZ04"));
    XCZ03.setVisible(lexique.isPresent("XCZ03"));
    XCZ02.setVisible(lexique.isPresent("XCZ02"));
    XCZ01.setVisible(lexique.isPresent("XCZ01"));
    OBJ_77.setVisible(lexique.isPresent("XCLZ10"));
    OBJ_76.setVisible(lexique.isPresent("XCLZ09"));
    OBJ_75.setVisible(lexique.isPresent("XCLZ08"));
    OBJ_74.setVisible(lexique.isPresent("XCLZ07"));
    OBJ_73.setVisible(lexique.isPresent("XCLZ06"));
    OBJ_72.setVisible(lexique.isPresent("XCLZ05"));
    OBJ_71.setVisible(lexique.isPresent("XCLZ04"));
    OBJ_70.setVisible(lexique.isPresent("XCLZ03"));
    OBJ_69.setVisible(lexique.isPresent("XCLZ02"));
    OBJ_68.setVisible(lexique.isPresent("XCLZ01"));
    // XCETA.setEnabled( lexique.isPresent("XCETA"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @(TITLE)@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("XCETA", 0, XCETA_Value[XCETA.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_49 = new JLabel();
    WETB = new XRiTextField();
    OBJ_50 = new JLabel();
    XCTYP = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_55 = new JLabel();
    XCETA = new XRiComboBox();
    OBJ_68 = new RiZoneSortie();
    OBJ_69 = new RiZoneSortie();
    OBJ_70 = new RiZoneSortie();
    OBJ_71 = new RiZoneSortie();
    OBJ_72 = new RiZoneSortie();
    OBJ_73 = new RiZoneSortie();
    OBJ_74 = new RiZoneSortie();
    OBJ_75 = new RiZoneSortie();
    OBJ_76 = new RiZoneSortie();
    OBJ_77 = new RiZoneSortie();
    XCZ01 = new XRiTextField();
    XCZ02 = new XRiTextField();
    XCZ03 = new XRiTextField();
    XCZ04 = new XRiTextField();
    XCZ05 = new XRiTextField();
    XCZ06 = new XRiTextField();
    XCZ07 = new XRiTextField();
    XCZ08 = new XRiTextField();
    XCZ09 = new XRiTextField();
    XCZ10 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_49 ----
          OBJ_49.setText("Etablissement");
          OBJ_49.setToolTipText("Module");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(0, 5, 95, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(100, 0, 40, WETB.getPreferredSize().height);

          //---- OBJ_50 ----
          OBJ_50.setText("Fichier");
          OBJ_50.setToolTipText("Module");
          OBJ_50.setName("OBJ_50");
          p_tete_gauche.add(OBJ_50);
          OBJ_50.setBounds(180, 5, 52, 20);

          //---- XCTYP ----
          XCTYP.setComponentPopupMenu(BTD);
          XCTYP.setName("XCTYP");
          p_tete_gauche.add(XCTYP);
          XCTYP.setBounds(240, 0, 40, XCTYP.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(500, 450));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Zones historis\u00e9es \u00e0 contr\u00f4ler"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_55 ----
            OBJ_55.setText("Etat du bon \u00e0 partir de");
            OBJ_55.setName("OBJ_55");
            panel1.add(OBJ_55);
            OBJ_55.setBounds(50, 35, 139, 20);

            //---- XCETA ----
            XCETA.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            XCETA.setName("XCETA");
            panel1.add(XCETA);
            XCETA.setBounds(215, 32, 84, XCETA.getPreferredSize().height);

            //---- OBJ_68 ----
            OBJ_68.setText("@XCLZ01@");
            OBJ_68.setName("OBJ_68");
            panel1.add(OBJ_68);
            OBJ_68.setBounds(135, 70, 278, OBJ_68.getPreferredSize().height);

            //---- OBJ_69 ----
            OBJ_69.setText("@XCLZ02@");
            OBJ_69.setName("OBJ_69");
            panel1.add(OBJ_69);
            OBJ_69.setBounds(135, 101, 278, OBJ_69.getPreferredSize().height);

            //---- OBJ_70 ----
            OBJ_70.setText("@XCLZ03@");
            OBJ_70.setName("OBJ_70");
            panel1.add(OBJ_70);
            OBJ_70.setBounds(135, 132, 278, OBJ_70.getPreferredSize().height);

            //---- OBJ_71 ----
            OBJ_71.setText("@XCLZ04@");
            OBJ_71.setName("OBJ_71");
            panel1.add(OBJ_71);
            OBJ_71.setBounds(135, 163, 278, OBJ_71.getPreferredSize().height);

            //---- OBJ_72 ----
            OBJ_72.setText("@XCLZ05@");
            OBJ_72.setName("OBJ_72");
            panel1.add(OBJ_72);
            OBJ_72.setBounds(135, 194, 278, OBJ_72.getPreferredSize().height);

            //---- OBJ_73 ----
            OBJ_73.setText("@XCLZ06@");
            OBJ_73.setName("OBJ_73");
            panel1.add(OBJ_73);
            OBJ_73.setBounds(135, 225, 278, OBJ_73.getPreferredSize().height);

            //---- OBJ_74 ----
            OBJ_74.setText("@XCLZ07@");
            OBJ_74.setName("OBJ_74");
            panel1.add(OBJ_74);
            OBJ_74.setBounds(135, 256, 278, OBJ_74.getPreferredSize().height);

            //---- OBJ_75 ----
            OBJ_75.setText("@XCLZ08@");
            OBJ_75.setName("OBJ_75");
            panel1.add(OBJ_75);
            OBJ_75.setBounds(135, 287, 278, OBJ_75.getPreferredSize().height);

            //---- OBJ_76 ----
            OBJ_76.setText("@XCLZ09@");
            OBJ_76.setName("OBJ_76");
            panel1.add(OBJ_76);
            OBJ_76.setBounds(135, 318, 278, OBJ_76.getPreferredSize().height);

            //---- OBJ_77 ----
            OBJ_77.setText("@XCLZ10@");
            OBJ_77.setName("OBJ_77");
            panel1.add(OBJ_77);
            OBJ_77.setBounds(135, 349, 278, OBJ_77.getPreferredSize().height);

            //---- XCZ01 ----
            XCZ01.setComponentPopupMenu(BTD);
            XCZ01.setName("XCZ01");
            panel1.add(XCZ01);
            XCZ01.setBounds(50, 68, 70, XCZ01.getPreferredSize().height);

            //---- XCZ02 ----
            XCZ02.setComponentPopupMenu(BTD);
            XCZ02.setName("XCZ02");
            panel1.add(XCZ02);
            XCZ02.setBounds(50, 99, 70, XCZ02.getPreferredSize().height);

            //---- XCZ03 ----
            XCZ03.setComponentPopupMenu(BTD);
            XCZ03.setName("XCZ03");
            panel1.add(XCZ03);
            XCZ03.setBounds(50, 130, 70, XCZ03.getPreferredSize().height);

            //---- XCZ04 ----
            XCZ04.setComponentPopupMenu(BTD);
            XCZ04.setName("XCZ04");
            panel1.add(XCZ04);
            XCZ04.setBounds(50, 161, 70, XCZ04.getPreferredSize().height);

            //---- XCZ05 ----
            XCZ05.setComponentPopupMenu(BTD);
            XCZ05.setName("XCZ05");
            panel1.add(XCZ05);
            XCZ05.setBounds(50, 192, 70, XCZ05.getPreferredSize().height);

            //---- XCZ06 ----
            XCZ06.setComponentPopupMenu(BTD);
            XCZ06.setName("XCZ06");
            panel1.add(XCZ06);
            XCZ06.setBounds(50, 223, 70, XCZ06.getPreferredSize().height);

            //---- XCZ07 ----
            XCZ07.setComponentPopupMenu(BTD);
            XCZ07.setName("XCZ07");
            panel1.add(XCZ07);
            XCZ07.setBounds(50, 254, 70, XCZ07.getPreferredSize().height);

            //---- XCZ08 ----
            XCZ08.setComponentPopupMenu(BTD);
            XCZ08.setName("XCZ08");
            panel1.add(XCZ08);
            XCZ08.setBounds(50, 285, 70, XCZ08.getPreferredSize().height);

            //---- XCZ09 ----
            XCZ09.setComponentPopupMenu(BTD);
            XCZ09.setName("XCZ09");
            panel1.add(XCZ09);
            XCZ09.setBounds(50, 316, 70, XCZ09.getPreferredSize().height);

            //---- XCZ10 ----
            XCZ10.setComponentPopupMenu(BTD);
            XCZ10.setName("XCZ10");
            panel1.add(XCZ10);
            XCZ10.setBounds(50, 347, 70, XCZ10.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 474, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 422, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_49;
  private XRiTextField WETB;
  private JLabel OBJ_50;
  private XRiTextField XCTYP;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_55;
  private XRiComboBox XCETA;
  private RiZoneSortie OBJ_68;
  private RiZoneSortie OBJ_69;
  private RiZoneSortie OBJ_70;
  private RiZoneSortie OBJ_71;
  private RiZoneSortie OBJ_72;
  private RiZoneSortie OBJ_73;
  private RiZoneSortie OBJ_74;
  private RiZoneSortie OBJ_75;
  private RiZoneSortie OBJ_76;
  private RiZoneSortie OBJ_77;
  private XRiTextField XCZ01;
  private XRiTextField XCZ02;
  private XRiTextField XCZ03;
  private XRiTextField XCZ04;
  private XRiTextField XCZ05;
  private XRiTextField XCZ06;
  private XRiTextField XCZ07;
  private XRiTextField XCZ08;
  private XRiTextField XCZ09;
  private XRiTextField XCZ10;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
