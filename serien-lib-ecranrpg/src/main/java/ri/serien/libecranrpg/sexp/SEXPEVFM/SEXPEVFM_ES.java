/*
 * Created by JFormDesigner on Wed Oct 01 11:34:46 CEST 2008
 */

package ri.serien.libecranrpg.sexp.SEXPEVFM;

import java.awt.Color;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVFM_ES extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVFM_ES(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    EVLB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*USER@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVMNU@")).trim());
    CML2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CML2@")).trim());
    CML1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CML1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_6 = new JPanel();
    OBJ_7 = new JLabel();
    EVLB1 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_11 = new JLabel();
    CML2 = new JLabel();
    CML1 = new JLabel();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");

    //======== OBJ_6 ========
    {
      OBJ_6.setOpaque(false);
      OBJ_6.setName("OBJ_6");
      OBJ_6.setLayout(null);

      //---- OBJ_7 ----
      OBJ_7.setText("@EVLB2@");
      OBJ_7.setHorizontalAlignment(SwingConstants.CENTER);
      OBJ_7.setFont(OBJ_7.getFont().deriveFont(OBJ_7.getFont().getStyle() | Font.BOLD, OBJ_7.getFont().getSize() + 2f));
      OBJ_7.setForeground(Color.white);
      OBJ_7.setName("OBJ_7");
      OBJ_6.add(OBJ_7);
      OBJ_7.setBounds(5, 90, 390, 24);

      //---- EVLB1 ----
      EVLB1.setText("@EVLB1@");
      EVLB1.setHorizontalAlignment(SwingConstants.CENTER);
      EVLB1.setFont(EVLB1.getFont().deriveFont(EVLB1.getFont().getStyle() | Font.BOLD, EVLB1.getFont().getSize() + 2f));
      EVLB1.setForeground(Color.white);
      EVLB1.setName("EVLB1");
      OBJ_6.add(EVLB1);
      EVLB1.setBounds(5, 60, 390, 24);

      //---- OBJ_12 ----
      OBJ_12.setText("@*USER@");
      OBJ_12.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_12.setForeground(Color.white);
      OBJ_12.setName("OBJ_12");
      OBJ_6.add(OBJ_12);
      OBJ_12.setBounds(246, 14, 144, 24);

      //---- OBJ_11 ----
      OBJ_11.setText("@EVMNU@");
      OBJ_11.setForeground(Color.white);
      OBJ_11.setName("OBJ_11");
      OBJ_6.add(OBJ_11);
      OBJ_11.setBounds(5, 14, 88, 24);
    }

    //---- CML2 ----
    CML2.setText("@CML2@");
    CML2.setForeground(Color.white);
    CML2.setName("CML2");

    //---- CML1 ----
    CML1.setText("@CML1@");
    CML1.setForeground(Color.white);
    CML1.setName("CML1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(7, 7, 7)
          .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 398, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(5, 5, 5)
          .addComponent(CML2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addComponent(CML1, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(28, 28, 28)
          .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 132, GroupLayout.PREFERRED_SIZE)
          .addGap(10, 10, 10)
          .addGroup(layout.createParallelGroup()
            .addComponent(CML2)
            .addComponent(CML1)))
    );
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel OBJ_6;
  private JLabel OBJ_7;
  private JLabel EVLB1;
  private JLabel OBJ_12;
  private JLabel OBJ_11;
  private JLabel CML2;
  private JLabel CML1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
