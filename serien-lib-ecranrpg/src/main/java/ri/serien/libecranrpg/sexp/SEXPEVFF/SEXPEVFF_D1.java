
package ri.serien.libecranrpg.sexp.SEXPEVFF;
// Nom Fichier: pop_SEXPEVFF_FMTD1_175.java

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVFF_D1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXPEVFF_D1(ArrayList<?> param) {
    super(param);
    initComponents();
    // setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(BT_ENTER);
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    OBJ_9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB3R@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@*TIME@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void BT_ENTERActionPerformed() {
    lexique.HostFieldPutData("REPONS", 0, "OUI");
    lexique.HostScreenSendKey(this, "Enter", true);
  }
  
  private void BT_RETOURActionPerformed() {
    lexique.HostFieldPutData("REPONS", 0, "NON");
    lexique.HostScreenSendKey(this, "Enter", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    OBJ_7 = new JLabel();
    OBJ_8 = new JLabel();
    OBJ_9 = new JLabel();
    BT_ENTER = new JButton();
    BT_RETOUR = new JButton();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(null);

    //---- OBJ_7 ----
    OBJ_7.setText("@EVLB1@");
    OBJ_7.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_7.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_7.setForeground(Color.white);
    OBJ_7.setName("OBJ_7");
    add(OBJ_7);
    OBJ_7.setBounds(0, 25, 400, 25);

    //---- OBJ_8 ----
    OBJ_8.setText("@EVLB2@");
    OBJ_8.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_8.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_8.setForeground(Color.white);
    OBJ_8.setName("OBJ_8");
    add(OBJ_8);
    OBJ_8.setBounds(0, 50, 400, 25);

    //---- OBJ_9 ----
    OBJ_9.setText("@EVLB3R@");
    OBJ_9.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_9.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_9.setForeground(Color.white);
    OBJ_9.setName("OBJ_9");
    add(OBJ_9);
    OBJ_9.setBounds(0, 75, 400, 25);

    //---- BT_ENTER ----
    BT_ENTER.setText("OUI");
    BT_ENTER.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_ENTER.setMargin(new Insets(0, -15, 0, 0));
    BT_ENTER.setPreferredSize(new Dimension(110, 19));
    BT_ENTER.setMinimumSize(new Dimension(110, 1));
    BT_ENTER.setFont(BT_ENTER.getFont().deriveFont(BT_ENTER.getFont().getStyle() | Font.BOLD, BT_ENTER.getFont().getSize() + 2f));
    BT_ENTER.setIconTextGap(25);
    BT_ENTER.setName("BT_ENTER");
    BT_ENTER.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_ENTERActionPerformed();
      }
    });
    add(BT_ENTER);
    BT_ENTER.setBounds(60, 105, 285, 40);

    //---- BT_RETOUR ----
    BT_RETOUR.setText("NON");
    BT_RETOUR.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    BT_RETOUR.setMargin(new Insets(0, -15, 0, 0));
    BT_RETOUR.setPreferredSize(new Dimension(120, 19));
    BT_RETOUR.setMinimumSize(new Dimension(120, 1));
    BT_RETOUR.setFont(new Font("sansserif", Font.BOLD, 14));
    BT_RETOUR.setIconTextGap(25);
    BT_RETOUR.setName("BT_RETOUR");
    BT_RETOUR.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        BT_RETOURActionPerformed();
      }
    });
    add(BT_RETOUR);
    BT_RETOUR.setBounds(60, 145, 285, 40);

    setPreferredSize(new Dimension(400, 190));
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JLabel OBJ_7;
  private JLabel OBJ_8;
  private JLabel OBJ_9;
  private JButton BT_ENTER;
  private JButton BT_RETOUR;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
