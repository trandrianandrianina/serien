/*
 * Copyright (C) Résolution Informatique - Tout droits réservés.
 * Les copies non autorisées de ce fichier, quel que soit le média, sont strictements interdites.
 */

package ri.serien.libecranrpg.sexp.SEXPAGPF;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JTabbedPane;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.dialoguestandard.erreur.DialogueErreur;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snetablissement.SNEtablissement;
import ri.serien.libswing.composant.metier.referentiel.etablissement.snmagasin.SNMagasin;
import ri.serien.libswing.composant.metier.vente.documentvente.snvendeur.SNVendeur;
import ri.serien.libswing.composant.primitif.barrebouton.SNBarreBouton;
import ri.serien.libswing.composant.primitif.barrebouton.SNBoutonListener;
import ri.serien.libswing.composant.primitif.bouton.EnumBouton;
import ri.serien.libswing.composant.primitif.bouton.SNBouton;
import ri.serien.libswing.composant.primitif.label.SNLabelChamp;
import ri.serien.libswing.composant.primitif.panel.SNPanel;
import ri.serien.libswing.composant.primitif.panel.SNPanelContenu;
import ri.serien.libswing.composant.primitif.panel.SNPanelTitre;
import ri.serien.libswing.composant.primitif.saisie.SNAdresseMail;
import ri.serien.libswing.composant.primitif.saisie.SNTexte;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.composant.InterfaceSNComposantListener;
import ri.serien.libswing.moteur.composant.SNComposantEvent;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPAGPF_B1 extends SNPanelEcranRPG implements ioFrame {
  
  private static final String BOUTON_PLANNING = "Mise au planning";
  
  public SEXPAGPF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Ajout
    initDiverses();
    L2EXE.setValeursSelection("1", " ");
    L3EXE.setValeursSelection("1", " ");
    snBarreBouton.ajouterBouton(EnumBouton.VALIDER, true);
    snBarreBouton.ajouterBouton(EnumBouton.QUITTER, true);
    // - Ajouter un bouton personnalisé
    snBarreBouton.ajouterBouton(BOUTON_PLANNING, 'p', true);
    snBarreBouton.ajouterBoutonListener(new SNBoutonListener() {
      @Override
      public void traiterClicBouton(SNBouton pSNBouton) {
        btTraiterCliqueBouton(pSNBouton);
      }
    });
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    tfPeriodeEnCours.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WENCX@")).trim());
    tfCodePlanningImport.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L2PLNS@")).trim());
    tfCodePlanningExport.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@L3PLNS@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    // Visibilité
    tfPeriodeEnCours.setVisible(!lexique.HostFieldGetData("WENCX").isEmpty());
    lbPeriodeEnCours.setVisible(tfPeriodeEnCours.isVisible());
    tfCodePlanningImport.setVisible(lexique.isTrue("70"));
    tfCodePlanningExport.setVisible(lexique.isTrue("71"));
    
    // Logo
    bpPresentation.setCodeEtablissement(lexique.HostFieldGetData("WETB"));
    
    // Initialisation de l'Etablissment
    snEtablissement.setSession(getSession());
    snEtablissement.charger(false);
    snEtablissement.setSelectionParChampRPG(lexique, "WETB");
    
    // Charger composants
    chargerComposantMagasin();
    chargerComposantVendeur();
    chargerComposantMail();
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // Etablissement
    snEtablissement.renseignerChampRPG(lexique, "WETB");
    
    // Magasin
    snMagasin.renseignerChampRPG(lexique, "L2MAG");
    
    // Vendeur
    snVendeur.renseignerChampRPG(lexique, "L2VDE");
    
    // mail
    lexique.HostFieldPutData("L1MAIL", 0, snAdresseMail.getText());
  }
  
  private void btTraiterCliqueBouton(SNBouton pSNBouton) {
    try {
      if (pSNBouton.isBouton(EnumBouton.VALIDER)) {
        lexique.HostScreenSendKey(this, "ENTER");
      }
      else if (pSNBouton.isBouton(EnumBouton.QUITTER)) {
        lexique.HostScreenSendKey(this, "F3");
      }
      else if (pSNBouton.isBouton(BOUTON_PLANNING)) {
        lexique.HostScreenSendKey(this, "F9");
      }
    }
    catch (Exception exception) {
      DialogueErreur.afficher(exception);
    }
  }
  
  // charger composant par fonction
  private void chargerComposantMagasin() {
    snMagasin.setSession(getSession());
    snMagasin.setIdEtablissement(snEtablissement.getIdSelection());
    snMagasin.charger(false);
    snMagasin.setSelectionParChampRPG(lexique, "L2MAG");
  }
  
  private void chargerComposantVendeur() {
    snVendeur.setSession(getSession());
    snVendeur.setIdEtablissement(snEtablissement.getIdSelection());
    snVendeur.charger(false);
    snVendeur.setSelectionParChampRPG(lexique, "L2VDE");
  }
  
  private void chargerComposantMail() {
    snAdresseMail.setText(lexique.HostFieldGetData("L1MAIL"));
  }
  
  private void miMisePlanningActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F9");
  }
  
  private void snEtablissementValueChanged(SNComposantEvent e) {
    chargerComposantMagasin();
    chargerComposantVendeur();
    lexique.HostScreenSendKey(this, "F5");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    bpPresentation = new SNBandeauTitre();
    pnlContenu = new SNPanelContenu();
    pnlColonne = new SNPanel();
    pnlGauche = new SNPanel();
    pnlParametreGeneraux = new SNPanelTitre();
    lbGencodeSociete = new SNLabelChamp();
    L1GCS = new XRiTextField();
    lbNomRepertoireEDI = new SNLabelChamp();
    L1EDI = new XRiTextField();
    lbCheminEDI = new SNLabelChamp();
    L1DOS = new XRiTextField();
    lbMail = new SNLabelChamp();
    snAdresseMail = new SNAdresseMail();
    pnlDroite = new SNPanel();
    pnlEtablissementSelectionne = new SNPanelTitre();
    lbEtablissement = new SNLabelChamp();
    snEtablissement = new SNEtablissement();
    lbPeriodeEnCours = new SNLabelChamp();
    tfPeriodeEnCours = new SNTexte();
    btpParametrageTraitement = new JTabbedPane();
    pnlImportCommandes = new SNPanel();
    pnlParametrageImportCommande = new SNPanelTitre();
    lbMagasin = new SNLabelChamp();
    snMagasin = new SNMagasin();
    lbVendeur = new SNLabelChamp();
    snVendeur = new SNVendeur();
    lbSousRepertoireImportCommande = new SNLabelChamp();
    L2SRP = new XRiTextField();
    lbLabelFichierImportCommande = new SNLabelChamp();
    L2LBL = new XRiTextField();
    lbCodeFTPImportCommande = new SNLabelChamp();
    L2FTP = new XRiTextField();
    lbDelaiExecutionImportCommande = new SNLabelChamp();
    L2DLY = new XRiTextField();
    lbHeureArretImportCommande = new SNLabelChamp();
    L2ARR = new XRiTextField();
    lbCodePlanningImportCommande = new SNLabelChamp();
    pnlCodePlanningImport = new SNPanel();
    L2PLN = new XRiTextField();
    tfCodePlanningImport = new SNTexte();
    lbExecutionDirecteImportCommande = new SNLabelChamp();
    L2EXE = new XRiCheckBox();
    pnlExportFacture = new SNPanel();
    pnlParametrageExportFacture = new SNPanelTitre();
    lbSousRepertoireExportFacture = new SNLabelChamp();
    L3SRP = new XRiTextField();
    lbLabelFichierExportFacture = new SNLabelChamp();
    L3LBL = new XRiTextField();
    lbCodeFTPExportFacture = new SNLabelChamp();
    L3FTP = new XRiTextField();
    lbDelaiExecutionExportFacture = new SNLabelChamp();
    L3DLY = new XRiTextField();
    lbHeureArretExportFacture = new SNLabelChamp();
    L3ARR = new XRiTextField();
    lbCodePlanningExportFacture = new SNLabelChamp();
    pnlCodePlanningExport = new SNPanel();
    L3PLN = new XRiTextField();
    tfCodePlanningExport = new SNTexte();
    lbExecutionDirecteExportFacture = new SNLabelChamp();
    L3EXE = new XRiCheckBox();
    snBarreBouton = new SNBarreBouton();
    BTD = new JPopupMenu();
    miMisePlanning = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(1190, 700));
    setPreferredSize(new Dimension(1190, 700));
    setMaximumSize(new Dimension(1190, 700));
    setName("this");
    setLayout(new BorderLayout());
    
    // ---- bpPresentation ----
    bpPresentation.setText("Param\u00e9trage des \u00e9changes EDI avec @GP");
    bpPresentation.setCapitaliserPremiereLettre(false);
    bpPresentation.setName("bpPresentation");
    add(bpPresentation, BorderLayout.NORTH);
    
    // ======== pnlContenu ========
    {
      pnlContenu.setName("pnlContenu");
      pnlContenu.setLayout(new GridBagLayout());
      ((GridBagLayout) pnlContenu.getLayout()).columnWidths = new int[] { 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).rowHeights = new int[] { 0, 0, 0 };
      ((GridBagLayout) pnlContenu.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
      ((GridBagLayout) pnlContenu.getLayout()).rowWeights = new double[] { 0.0, 1.0, 1.0E-4 };
      
      // ======== pnlColonne ========
      {
        pnlColonne.setName("pnlColonne");
        pnlColonne.setLayout(new GridLayout());
        
        // ======== pnlGauche ========
        {
          pnlGauche.setName("pnlGauche");
          pnlGauche.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlGauche.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlGauche.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlGauche.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlParametreGeneraux ========
          {
            pnlParametreGeneraux.setTitre("Param\u00e8tres g\u00e9n\u00e9raux");
            pnlParametreGeneraux.setName("pnlParametreGeneraux");
            pnlParametreGeneraux.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlParametreGeneraux.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlParametreGeneraux.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlParametreGeneraux.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlParametreGeneraux.getLayout()).rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbGencodeSociete ----
            lbGencodeSociete.setText("Gencode soci\u00e9t\u00e9");
            lbGencodeSociete.setMaximumSize(new Dimension(200, 30));
            lbGencodeSociete.setPreferredSize(new Dimension(200, 30));
            lbGencodeSociete.setMinimumSize(new Dimension(200, 30));
            lbGencodeSociete.setName("lbGencodeSociete");
            pnlParametreGeneraux.add(lbGencodeSociete, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L1GCS ----
            L1GCS.setFont(new Font("sansserif", Font.PLAIN, 14));
            L1GCS.setMaximumSize(new Dimension(140, 30));
            L1GCS.setMinimumSize(new Dimension(140, 30));
            L1GCS.setPreferredSize(new Dimension(140, 30));
            L1GCS.setName("L1GCS");
            pnlParametreGeneraux.add(L1GCS, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbNomRepertoireEDI ----
            lbNomRepertoireEDI.setText("Nom r\u00e9pertoire EDI");
            lbNomRepertoireEDI.setMaximumSize(new Dimension(200, 30));
            lbNomRepertoireEDI.setMinimumSize(new Dimension(200, 30));
            lbNomRepertoireEDI.setPreferredSize(new Dimension(200, 30));
            lbNomRepertoireEDI.setVerticalAlignment(SwingConstants.CENTER);
            lbNomRepertoireEDI.setName("lbNomRepertoireEDI");
            pnlParametreGeneraux.add(lbNomRepertoireEDI, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L1EDI ----
            L1EDI.setFont(new Font("sansserif", Font.PLAIN, 14));
            L1EDI.setMaximumSize(new Dimension(140, 30));
            L1EDI.setMinimumSize(new Dimension(140, 30));
            L1EDI.setPreferredSize(new Dimension(140, 30));
            L1EDI.setName("L1EDI");
            pnlParametreGeneraux.add(L1EDI, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCheminEDI ----
            lbCheminEDI.setText("Chemin r\u00e9pertoire EDI");
            lbCheminEDI.setMaximumSize(new Dimension(200, 30));
            lbCheminEDI.setMinimumSize(new Dimension(200, 30));
            lbCheminEDI.setPreferredSize(new Dimension(200, 30));
            lbCheminEDI.setVerticalAlignment(SwingConstants.CENTER);
            lbCheminEDI.setName("lbCheminEDI");
            pnlParametreGeneraux.add(lbCheminEDI, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L1DOS ----
            L1DOS.setFont(new Font("sansserif", Font.PLAIN, 14));
            L1DOS.setMaximumSize(new Dimension(400, 30));
            L1DOS.setMinimumSize(new Dimension(400, 30));
            L1DOS.setRequestFocusEnabled(false);
            L1DOS.setPreferredSize(new Dimension(400, 30));
            L1DOS.setEditable(false);
            L1DOS.setEnabled(false);
            L1DOS.setName("L1DOS");
            pnlParametreGeneraux.add(L1DOS, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbMail ----
            lbMail.setText("E-mail pour notifications");
            lbMail.setMaximumSize(new Dimension(200, 30));
            lbMail.setMinimumSize(new Dimension(200, 30));
            lbMail.setPreferredSize(new Dimension(200, 30));
            lbMail.setVerticalAlignment(SwingConstants.CENTER);
            lbMail.setName("lbMail");
            pnlParametreGeneraux.add(lbMail, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- snAdresseMail ----
            snAdresseMail.setName("snAdresseMail");
            pnlParametreGeneraux.add(snAdresseMail, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlGauche.add(pnlParametreGeneraux, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlGauche);
        
        // ======== pnlDroite ========
        {
          pnlDroite.setName("pnlDroite");
          pnlDroite.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlDroite.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlDroite.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlDroite.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
          
          // ======== pnlEtablissementSelectionne ========
          {
            pnlEtablissementSelectionne.setTitre("Etablissement");
            pnlEtablissementSelectionne.setName("pnlEtablissementSelectionne");
            pnlEtablissementSelectionne.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowHeights = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlEtablissementSelectionne.getLayout()).rowWeights = new double[] { 0.0, 0.0, 1.0E-4 };
            
            // ---- lbEtablissement ----
            lbEtablissement.setText("Etablissement en cours");
            lbEtablissement.setName("lbEtablissement");
            pnlEtablissementSelectionne.add(lbEtablissement, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snEtablissement ----
            snEtablissement.setFont(new Font("sansserif", Font.PLAIN, 14));
            snEtablissement.setName("snEtablissement");
            snEtablissement.addSNComposantListener(new InterfaceSNComposantListener() {
              @Override
              public void valueChanged(SNComposantEvent e) {
                snEtablissementValueChanged(e);
              }
            });
            pnlEtablissementSelectionne.add(snEtablissement, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbPeriodeEnCours ----
            lbPeriodeEnCours.setText("P\u00e9riode en cours");
            lbPeriodeEnCours.setName("lbPeriodeEnCours");
            pnlEtablissementSelectionne.add(lbPeriodeEnCours, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- tfPeriodeEnCours ----
            tfPeriodeEnCours.setText("@WENCX@");
            tfPeriodeEnCours.setMaximumSize(new Dimension(272, 20));
            tfPeriodeEnCours.setEditable(false);
            tfPeriodeEnCours.setPreferredSize(new Dimension(272, 30));
            tfPeriodeEnCours.setMinimumSize(new Dimension(272, 30));
            tfPeriodeEnCours.setEnabled(false);
            tfPeriodeEnCours.setName("tfPeriodeEnCours");
            pnlEtablissementSelectionne.add(tfPeriodeEnCours, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlDroite.add(pnlEtablissementSelectionne, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        pnlColonne.add(pnlDroite);
      }
      pnlContenu.add(pnlColonne,
          new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
      
      // ======== btpParametrageTraitement ========
      {
        btpParametrageTraitement.setFont(new Font("sansserif", Font.PLAIN, 14));
        btpParametrageTraitement.setName("btpParametrageTraitement");
        
        // ======== pnlImportCommandes ========
        {
          pnlImportCommandes.setName("pnlImportCommandes");
          pnlImportCommandes.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlImportCommandes.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlImportCommandes.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlImportCommandes.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlImportCommandes.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== pnlParametrageImportCommande ========
          {
            pnlParametrageImportCommande.setName("pnlParametrageImportCommande");
            pnlParametrageImportCommande.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlParametrageImportCommande.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlParametrageImportCommande.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlParametrageImportCommande.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlParametrageImportCommande.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbMagasin ----
            lbMagasin.setText("Magasin");
            lbMagasin.setMinimumSize(new Dimension(202, 30));
            lbMagasin.setPreferredSize(new Dimension(202, 30));
            lbMagasin.setMaximumSize(new Dimension(202, 30));
            lbMagasin.setName("lbMagasin");
            pnlParametrageImportCommande.add(lbMagasin, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snMagasin ----
            snMagasin.setName("snMagasin");
            pnlParametrageImportCommande.add(snMagasin, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbVendeur ----
            lbVendeur.setText("Vendeur");
            lbVendeur.setMaximumSize(new Dimension(202, 30));
            lbVendeur.setPreferredSize(new Dimension(202, 30));
            lbVendeur.setMinimumSize(new Dimension(202, 30));
            lbVendeur.setName("lbVendeur");
            pnlParametrageImportCommande.add(lbVendeur, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- snVendeur ----
            snVendeur.setName("snVendeur");
            pnlParametrageImportCommande.add(snVendeur, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbSousRepertoireImportCommande ----
            lbSousRepertoireImportCommande.setText("Sous-r\u00e9pertoire ");
            lbSousRepertoireImportCommande.setMaximumSize(new Dimension(202, 30));
            lbSousRepertoireImportCommande.setPreferredSize(new Dimension(202, 30));
            lbSousRepertoireImportCommande.setMinimumSize(new Dimension(202, 30));
            lbSousRepertoireImportCommande.setName("lbSousRepertoireImportCommande");
            pnlParametrageImportCommande.add(lbSousRepertoireImportCommande, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L2SRP ----
            L2SRP.setFont(new Font("sansserif", Font.PLAIN, 14));
            L2SRP.setMaximumSize(new Dimension(140, 30));
            L2SRP.setMinimumSize(new Dimension(140, 30));
            L2SRP.setPreferredSize(new Dimension(140, 30));
            L2SRP.setName("L2SRP");
            pnlParametrageImportCommande.add(L2SRP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLabelFichierImportCommande ----
            lbLabelFichierImportCommande.setText("Label fichier");
            lbLabelFichierImportCommande.setMaximumSize(new Dimension(202, 30));
            lbLabelFichierImportCommande.setPreferredSize(new Dimension(202, 30));
            lbLabelFichierImportCommande.setMinimumSize(new Dimension(202, 30));
            lbLabelFichierImportCommande.setName("lbLabelFichierImportCommande");
            pnlParametrageImportCommande.add(lbLabelFichierImportCommande, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L2LBL ----
            L2LBL.setMaximumSize(new Dimension(140, 30));
            L2LBL.setMinimumSize(new Dimension(140, 30));
            L2LBL.setPreferredSize(new Dimension(140, 30));
            L2LBL.setName("L2LBL");
            pnlParametrageImportCommande.add(L2LBL, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFTPImportCommande ----
            lbCodeFTPImportCommande.setText("Code FTP");
            lbCodeFTPImportCommande.setMaximumSize(new Dimension(202, 30));
            lbCodeFTPImportCommande.setPreferredSize(new Dimension(202, 30));
            lbCodeFTPImportCommande.setMinimumSize(new Dimension(202, 30));
            lbCodeFTPImportCommande.setName("lbCodeFTPImportCommande");
            pnlParametrageImportCommande.add(lbCodeFTPImportCommande, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L2FTP ----
            L2FTP.setMaximumSize(new Dimension(72, 30));
            L2FTP.setMinimumSize(new Dimension(72, 30));
            L2FTP.setPreferredSize(new Dimension(72, 30));
            L2FTP.setName("L2FTP");
            pnlParametrageImportCommande.add(L2FTP, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDelaiExecutionImportCommande ----
            lbDelaiExecutionImportCommande.setText("D\u00e9lai d'ex\u00e9cution");
            lbDelaiExecutionImportCommande.setMaximumSize(new Dimension(202, 30));
            lbDelaiExecutionImportCommande.setPreferredSize(new Dimension(202, 30));
            lbDelaiExecutionImportCommande.setMinimumSize(new Dimension(202, 30));
            lbDelaiExecutionImportCommande.setName("lbDelaiExecutionImportCommande");
            pnlParametrageImportCommande.add(lbDelaiExecutionImportCommande, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L2DLY ----
            L2DLY.setMaximumSize(new Dimension(60, 30));
            L2DLY.setMinimumSize(new Dimension(60, 30));
            L2DLY.setPreferredSize(new Dimension(60, 30));
            L2DLY.setName("L2DLY");
            pnlParametrageImportCommande.add(L2DLY, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbHeureArretImportCommande ----
            lbHeureArretImportCommande.setText("Heure d'arr\u00eat");
            lbHeureArretImportCommande.setMaximumSize(new Dimension(202, 30));
            lbHeureArretImportCommande.setPreferredSize(new Dimension(202, 30));
            lbHeureArretImportCommande.setMinimumSize(new Dimension(202, 30));
            lbHeureArretImportCommande.setName("lbHeureArretImportCommande");
            pnlParametrageImportCommande.add(lbHeureArretImportCommande, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L2ARR ----
            L2ARR.setMaximumSize(new Dimension(70, 30));
            L2ARR.setMinimumSize(new Dimension(70, 30));
            L2ARR.setPreferredSize(new Dimension(70, 30));
            L2ARR.setName("L2ARR");
            pnlParametrageImportCommande.add(L2ARR, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodePlanningImportCommande ----
            lbCodePlanningImportCommande.setText("Code planning");
            lbCodePlanningImportCommande.setMaximumSize(new Dimension(202, 30));
            lbCodePlanningImportCommande.setPreferredSize(new Dimension(202, 30));
            lbCodePlanningImportCommande.setMinimumSize(new Dimension(202, 30));
            lbCodePlanningImportCommande.setName("lbCodePlanningImportCommande");
            pnlParametrageImportCommande.add(lbCodePlanningImportCommande, new GridBagConstraints(0, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCodePlanningImport ========
            {
              pnlCodePlanningImport.setName("pnlCodePlanningImport");
              pnlCodePlanningImport.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCodePlanningImport.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCodePlanningImport.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCodePlanningImport.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlCodePlanningImport.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- L2PLN ----
              L2PLN.setFont(new Font("sansserif", Font.PLAIN, 14));
              L2PLN.setMaximumSize(new Dimension(140, 30));
              L2PLN.setMinimumSize(new Dimension(140, 30));
              L2PLN.setPreferredSize(new Dimension(140, 30));
              L2PLN.setComponentPopupMenu(BTD);
              L2PLN.setName("L2PLN");
              pnlCodePlanningImport.add(L2PLN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfCodePlanningImport ----
              tfCodePlanningImport.setMinimumSize(new Dimension(300, 30));
              tfCodePlanningImport.setPreferredSize(new Dimension(300, 30));
              tfCodePlanningImport.setText("@L2PLNS@");
              tfCodePlanningImport.setEditable(false);
              tfCodePlanningImport.setEnabled(false);
              tfCodePlanningImport.setName("tfCodePlanningImport");
              pnlCodePlanningImport.add(tfCodePlanningImport, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlParametrageImportCommande.add(pnlCodePlanningImport, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbExecutionDirecteImportCommande ----
            lbExecutionDirecteImportCommande.setText("Ex\u00e9cution directe");
            lbExecutionDirecteImportCommande.setMaximumSize(new Dimension(202, 30));
            lbExecutionDirecteImportCommande.setPreferredSize(new Dimension(202, 30));
            lbExecutionDirecteImportCommande.setMinimumSize(new Dimension(202, 30));
            lbExecutionDirecteImportCommande.setName("lbExecutionDirecteImportCommande");
            pnlParametrageImportCommande.add(lbExecutionDirecteImportCommande, new GridBagConstraints(0, 8, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- L2EXE ----
            L2EXE.setFont(new Font("sansserif", Font.PLAIN, 14));
            L2EXE.setName("L2EXE");
            pnlParametrageImportCommande.add(L2EXE, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlImportCommandes.add(pnlParametrageImportCommande, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        btpParametrageTraitement.addTab("Import des commandes", pnlImportCommandes);
        
        // ======== pnlExportFacture ========
        {
          pnlExportFacture.setName("pnlExportFacture");
          pnlExportFacture.setLayout(new GridBagLayout());
          ((GridBagLayout) pnlExportFacture.getLayout()).columnWidths = new int[] { 0, 0 };
          ((GridBagLayout) pnlExportFacture.getLayout()).rowHeights = new int[] { 0, 0 };
          ((GridBagLayout) pnlExportFacture.getLayout()).columnWeights = new double[] { 1.0, 1.0E-4 };
          ((GridBagLayout) pnlExportFacture.getLayout()).rowWeights = new double[] { 1.0, 1.0E-4 };
          
          // ======== pnlParametrageExportFacture ========
          {
            pnlParametrageExportFacture.setName("pnlParametrageExportFacture");
            pnlParametrageExportFacture.setLayout(new GridBagLayout());
            ((GridBagLayout) pnlParametrageExportFacture.getLayout()).columnWidths = new int[] { 0, 0, 0 };
            ((GridBagLayout) pnlParametrageExportFacture.getLayout()).rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0 };
            ((GridBagLayout) pnlParametrageExportFacture.getLayout()).columnWeights = new double[] { 0.0, 1.0, 1.0E-4 };
            ((GridBagLayout) pnlParametrageExportFacture.getLayout()).rowWeights =
                new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4 };
            
            // ---- lbSousRepertoireExportFacture ----
            lbSousRepertoireExportFacture.setText("Sous-r\u00e9pertoire ");
            lbSousRepertoireExportFacture.setMaximumSize(new Dimension(202, 30));
            lbSousRepertoireExportFacture.setPreferredSize(new Dimension(202, 30));
            lbSousRepertoireExportFacture.setMinimumSize(new Dimension(202, 30));
            lbSousRepertoireExportFacture.setName("lbSousRepertoireExportFacture");
            pnlParametrageExportFacture.add(lbSousRepertoireExportFacture, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L3SRP ----
            L3SRP.setFont(new Font("sansserif", Font.PLAIN, 14));
            L3SRP.setMaximumSize(new Dimension(140, 30));
            L3SRP.setMinimumSize(new Dimension(140, 30));
            L3SRP.setPreferredSize(new Dimension(140, 30));
            L3SRP.setName("L3SRP");
            pnlParametrageExportFacture.add(L3SRP, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbLabelFichierExportFacture ----
            lbLabelFichierExportFacture.setText("Label fichier");
            lbLabelFichierExportFacture.setMaximumSize(new Dimension(202, 30));
            lbLabelFichierExportFacture.setPreferredSize(new Dimension(202, 30));
            lbLabelFichierExportFacture.setMinimumSize(new Dimension(202, 30));
            lbLabelFichierExportFacture.setName("lbLabelFichierExportFacture");
            pnlParametrageExportFacture.add(lbLabelFichierExportFacture, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L3LBL ----
            L3LBL.setMaximumSize(new Dimension(140, 30));
            L3LBL.setMinimumSize(new Dimension(140, 30));
            L3LBL.setPreferredSize(new Dimension(140, 30));
            L3LBL.setName("L3LBL");
            pnlParametrageExportFacture.add(L3LBL, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodeFTPExportFacture ----
            lbCodeFTPExportFacture.setText("Code FTP");
            lbCodeFTPExportFacture.setMaximumSize(new Dimension(202, 30));
            lbCodeFTPExportFacture.setPreferredSize(new Dimension(202, 30));
            lbCodeFTPExportFacture.setMinimumSize(new Dimension(202, 30));
            lbCodeFTPExportFacture.setName("lbCodeFTPExportFacture");
            pnlParametrageExportFacture.add(lbCodeFTPExportFacture, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L3FTP ----
            L3FTP.setMaximumSize(new Dimension(72, 30));
            L3FTP.setMinimumSize(new Dimension(72, 30));
            L3FTP.setPreferredSize(new Dimension(72, 30));
            L3FTP.setName("L3FTP");
            pnlParametrageExportFacture.add(L3FTP, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbDelaiExecutionExportFacture ----
            lbDelaiExecutionExportFacture.setText("D\u00e9lai d'ex\u00e9cution");
            lbDelaiExecutionExportFacture.setMaximumSize(new Dimension(202, 30));
            lbDelaiExecutionExportFacture.setPreferredSize(new Dimension(202, 30));
            lbDelaiExecutionExportFacture.setMinimumSize(new Dimension(202, 30));
            lbDelaiExecutionExportFacture.setName("lbDelaiExecutionExportFacture");
            pnlParametrageExportFacture.add(lbDelaiExecutionExportFacture, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L3DLY ----
            L3DLY.setMaximumSize(new Dimension(60, 30));
            L3DLY.setMinimumSize(new Dimension(60, 30));
            L3DLY.setPreferredSize(new Dimension(60, 30));
            L3DLY.setName("L3DLY");
            pnlParametrageExportFacture.add(L3DLY, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbHeureArretExportFacture ----
            lbHeureArretExportFacture.setText("Heure d'arr\u00eat");
            lbHeureArretExportFacture.setMaximumSize(new Dimension(202, 30));
            lbHeureArretExportFacture.setPreferredSize(new Dimension(202, 30));
            lbHeureArretExportFacture.setMinimumSize(new Dimension(202, 30));
            lbHeureArretExportFacture.setName("lbHeureArretExportFacture");
            pnlParametrageExportFacture.add(lbHeureArretExportFacture, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ---- L3ARR ----
            L3ARR.setMaximumSize(new Dimension(70, 30));
            L3ARR.setMinimumSize(new Dimension(70, 30));
            L3ARR.setPreferredSize(new Dimension(70, 30));
            L3ARR.setName("L3ARR");
            pnlParametrageExportFacture.add(L3ARR, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbCodePlanningExportFacture ----
            lbCodePlanningExportFacture.setText("Code planning");
            lbCodePlanningExportFacture.setMaximumSize(new Dimension(202, 30));
            lbCodePlanningExportFacture.setPreferredSize(new Dimension(202, 30));
            lbCodePlanningExportFacture.setMinimumSize(new Dimension(202, 30));
            lbCodePlanningExportFacture.setName("lbCodePlanningExportFacture");
            pnlParametrageExportFacture.add(lbCodePlanningExportFacture, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 5, 5), 0, 0));
            
            // ======== pnlCodePlanningExport ========
            {
              pnlCodePlanningExport.setName("pnlCodePlanningExport");
              pnlCodePlanningExport.setLayout(new GridBagLayout());
              ((GridBagLayout) pnlCodePlanningExport.getLayout()).columnWidths = new int[] { 0, 0, 0 };
              ((GridBagLayout) pnlCodePlanningExport.getLayout()).rowHeights = new int[] { 0, 0 };
              ((GridBagLayout) pnlCodePlanningExport.getLayout()).columnWeights = new double[] { 0.0, 0.0, 1.0E-4 };
              ((GridBagLayout) pnlCodePlanningExport.getLayout()).rowWeights = new double[] { 0.0, 1.0E-4 };
              
              // ---- L3PLN ----
              L3PLN.setFont(new Font("sansserif", Font.PLAIN, 14));
              L3PLN.setMaximumSize(new Dimension(140, 30));
              L3PLN.setMinimumSize(new Dimension(140, 30));
              L3PLN.setPreferredSize(new Dimension(140, 30));
              L3PLN.setComponentPopupMenu(BTD);
              L3PLN.setName("L3PLN");
              pnlCodePlanningExport.add(L3PLN, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
              
              // ---- tfCodePlanningExport ----
              tfCodePlanningExport.setPreferredSize(new Dimension(300, 30));
              tfCodePlanningExport.setMinimumSize(new Dimension(300, 30));
              tfCodePlanningExport.setEditable(false);
              tfCodePlanningExport.setEnabled(false);
              tfCodePlanningExport.setText("@L3PLNS@");
              tfCodePlanningExport.setName("tfCodePlanningExport");
              pnlCodePlanningExport.add(tfCodePlanningExport, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                  GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
            }
            pnlParametrageExportFacture.add(pnlCodePlanningExport, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
                GridBagConstraints.BOTH, new Insets(0, 0, 5, 0), 0, 0));
            
            // ---- lbExecutionDirecteExportFacture ----
            lbExecutionDirecteExportFacture.setText("Ex\u00e9cution directe");
            lbExecutionDirecteExportFacture.setMaximumSize(new Dimension(202, 30));
            lbExecutionDirecteExportFacture.setPreferredSize(new Dimension(202, 30));
            lbExecutionDirecteExportFacture.setMinimumSize(new Dimension(202, 30));
            lbExecutionDirecteExportFacture.setName("lbExecutionDirecteExportFacture");
            pnlParametrageExportFacture.add(lbExecutionDirecteExportFacture, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 0, 0, 5), 0, 0));
            
            // ---- L3EXE ----
            L3EXE.setFont(new Font("sansserif", Font.PLAIN, 14));
            L3EXE.setPreferredSize(new Dimension(18, 30));
            L3EXE.setMinimumSize(new Dimension(18, 30));
            L3EXE.setMaximumSize(new Dimension(18, 30));
            L3EXE.setName("L3EXE");
            pnlParametrageExportFacture.add(L3EXE, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0, GridBagConstraints.WEST,
                GridBagConstraints.VERTICAL, new Insets(0, 0, 0, 0), 0, 0));
          }
          pnlExportFacture.add(pnlParametrageExportFacture, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER,
              GridBagConstraints.BOTH, new Insets(0, 0, 0, 0), 0, 0));
        }
        btpParametrageTraitement.addTab("Export des factures", pnlExportFacture);
      }
      pnlContenu.add(btpParametrageTraitement,
          new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));
    }
    add(pnlContenu, BorderLayout.CENTER);
    
    // ---- snBarreBouton ----
    snBarreBouton.setName("snBarreBouton");
    add(snBarreBouton, BorderLayout.SOUTH);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- miMisePlanning ----
      miMisePlanning.setText("Mise au planning");
      miMisePlanning.setMaximumSize(new Dimension(120, 30));
      miMisePlanning.setPreferredSize(new Dimension(120, 30));
      miMisePlanning.setMinimumSize(new Dimension(120, 30));
      miMisePlanning.setFont(new Font("sansserif", Font.PLAIN, 14));
      miMisePlanning.setName("miMisePlanning");
      miMisePlanning.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          miMisePlanningActionPerformed(e);
        }
      });
      BTD.add(miMisePlanning);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private SNBandeauTitre bpPresentation;
  private SNPanelContenu pnlContenu;
  private SNPanel pnlColonne;
  private SNPanel pnlGauche;
  private SNPanelTitre pnlParametreGeneraux;
  private SNLabelChamp lbGencodeSociete;
  private XRiTextField L1GCS;
  private SNLabelChamp lbNomRepertoireEDI;
  private XRiTextField L1EDI;
  private SNLabelChamp lbCheminEDI;
  private XRiTextField L1DOS;
  private SNLabelChamp lbMail;
  private SNAdresseMail snAdresseMail;
  private SNPanel pnlDroite;
  private SNPanelTitre pnlEtablissementSelectionne;
  private SNLabelChamp lbEtablissement;
  private SNEtablissement snEtablissement;
  private SNLabelChamp lbPeriodeEnCours;
  private SNTexte tfPeriodeEnCours;
  private JTabbedPane btpParametrageTraitement;
  private SNPanel pnlImportCommandes;
  private SNPanelTitre pnlParametrageImportCommande;
  private SNLabelChamp lbMagasin;
  private SNMagasin snMagasin;
  private SNLabelChamp lbVendeur;
  private SNVendeur snVendeur;
  private SNLabelChamp lbSousRepertoireImportCommande;
  private XRiTextField L2SRP;
  private SNLabelChamp lbLabelFichierImportCommande;
  private XRiTextField L2LBL;
  private SNLabelChamp lbCodeFTPImportCommande;
  private XRiTextField L2FTP;
  private SNLabelChamp lbDelaiExecutionImportCommande;
  private XRiTextField L2DLY;
  private SNLabelChamp lbHeureArretImportCommande;
  private XRiTextField L2ARR;
  private SNLabelChamp lbCodePlanningImportCommande;
  private SNPanel pnlCodePlanningImport;
  private XRiTextField L2PLN;
  private SNTexte tfCodePlanningImport;
  private SNLabelChamp lbExecutionDirecteImportCommande;
  private XRiCheckBox L2EXE;
  private SNPanel pnlExportFacture;
  private SNPanelTitre pnlParametrageExportFacture;
  private SNLabelChamp lbSousRepertoireExportFacture;
  private XRiTextField L3SRP;
  private SNLabelChamp lbLabelFichierExportFacture;
  private XRiTextField L3LBL;
  private SNLabelChamp lbCodeFTPExportFacture;
  private XRiTextField L3FTP;
  private SNLabelChamp lbDelaiExecutionExportFacture;
  private XRiTextField L3DLY;
  private SNLabelChamp lbHeureArretExportFacture;
  private XRiTextField L3ARR;
  private SNLabelChamp lbCodePlanningExportFacture;
  private SNPanel pnlCodePlanningExport;
  private XRiTextField L3PLN;
  private SNTexte tfCodePlanningExport;
  private SNLabelChamp lbExecutionDirecteExportFacture;
  private XRiCheckBox L3EXE;
  private SNBarreBouton snBarreBouton;
  private JPopupMenu BTD;
  private JMenuItem miMisePlanning;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
