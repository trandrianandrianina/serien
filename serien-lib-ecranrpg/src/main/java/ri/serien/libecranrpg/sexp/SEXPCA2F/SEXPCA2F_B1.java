
package ri.serien.libecranrpg.sexp.SEXPCA2F;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPCA2F_B1 extends SNPanelEcranRPG implements ioFrame {
  
  // private String[] _LIST_Top = null;
  private String[] _JS11_Title = { "", "", "", "", };
  private String[][] _JS11_Data = { { "JS11", "DS11", "CS11", "LS11", }, { "JS12", "DS12", "CS12", "LS12", },
      { "JS13", "DS13", "CS13", "LS13", }, { "JS14", "DS14", "CS14", "LS14", }, { "JS15", "DS15", "CS15", "LS15", },
      { "JS16", "DS16", "CS16", "LS16", }, { "JS17", "DS17", "CS17", "LS17", }, };
  private int[] _JS11_Width = { 60, 61, 15, 36, };
  private String[] _JS61_Title = { "", "", "", "", };
  private String[][] _JS61_Data = { { "JS61", "DS61", "CS61", "LS61", }, { "JS62", "DS62", "CS62", "LS62", },
      { "JS63", "DS63", "CS63", "LS63", }, { "JS64", "DS64", "CS64", "LS64", }, { "JS65", "DS65", "CS65", "LS65", },
      { "JS66", "DS66", "CS66", "LS66", }, { "JS67", "DS67", "CS67", "LS67", }, };
  private int[] _JS61_Width = { 60, 61, 15, 36, };
  private String[] _JS51_Title = { "", "", "", "", };
  private String[][] _JS51_Data = { { "JS51", "DS51", "CS51", "LS51", }, { "JS52", "DS52", "CS52", "LS52", },
      { "JS53", "DS53", "CS53", "LS53", }, { "JS54", "DS54", "CS54", "LS54", }, { "JS55", "DS55", "CS55", "LS55", },
      { "JS56", "DS56", "CS56", "LS56", }, { "JS57", "DS57", "CS57", "LS57", }, };
  private int[] _JS51_Width = { 60, 61, 15, 36, };
  private String[] _JS41_Title = { "", "", "", "", };
  private String[][] _JS41_Data = { { "JS41", "DS41", "CS41", "LS41", }, { "JS42", "DS42", "CS42", "LS42", },
      { "JS43", "DS43", "CS43", "LS43", }, { "JS44", "DS44", "CS44", "LS44", }, { "JS45", "DS45", "CS45", "LS45", },
      { "JS46", "DS46", "CS46", "LS46", }, { "JS47", "DS47", "CS47", "LS47", }, };
  private int[] _JS41_Width = { 60, 61, 15, 36, };
  private String[] _JS31_Title = { "", "", "", "", };
  private String[][] _JS31_Data = { { "JS31", "DS31", "CS31", "LS31", }, { "JS32", "DS32", "CS32", "LS32", },
      { "JS33", "DS33", "CS33", "LS33", }, { "JS34", "DS34", "CS34", "LS34", }, { "JS35", "DS35", "CS35", "LS35", },
      { "JS36", "DS36", "CS36", "LS36", }, { "JS37", "DS07", "CS37", "LS37", }, };
  private int[] _JS31_Width = { 60, 61, 15, 36, };
  private String[] _JS21_Title = { "", "", "", "", };
  private String[][] _JS21_Data = { { "JS21", "DS21", "CS21", "LS21", }, { "JS22", "DS22", "CS22", "LS22", },
      { "JS23", "DS23", "CS23", "LS23", }, { "JS24", "DS24", "CS24", "LS24", }, { "JS25", "DS25", "CS25", "LS25", },
      { "JS26", "DS26", "CS26", "LS26", }, { "JS27", "DS07", "CS27", "LS27", }, };
  private int[] _JS21_Width = { 60, 61, 15, 36, };
  // private String[] _LIST2_Top = null;
  // private String[] _LIST3_Top = null;
  // private String[] _LIST4_Top = null;
  // private String[] _LIST5_Top = null;
  // private String[] _LIST6_Top = null;
  
  public SEXPCA2F_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // setDialog(true);
    
    // Ajout
    initDiverses();
    // JS11.setAspectTable(null, _JS11_Title, _JS11_Data, _JS11_Width, false, null, null, null, null);
    // JS61.setAspectTable(null, _JS61_Title, _JS61_Data, _JS61_Width, false, null, null, null, null);
    // JS51.setAspectTable(null, _JS51_Title, _JS51_Data, _JS51_Width, false, null, null, null, null);
    // JS41.setAspectTable(null, _JS41_Title, _JS41_Data, _JS41_Width, false, null, null, null, null);
    // JS31.setAspectTable(null, _JS31_Title, _JS31_Data, _JS31_Width, false, null, null, null, null);
    // JS21.setAspectTable(null, _JS21_Title, _JS21_Data, _JS21_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( JS11, JS11.get_JS11_Title_Data_Brut(), _JS11_Top);
    // majTable( JS21, JS21.get_LIST_Title_Data_Brut(), _JS21_Top);
    // majTable( JS31, JS31.get_LIST_Title_Data_Brut(), _JS31_Top);
    // majTable( JS41, JS41.get_LIST_Title_Data_Brut(), _JS41_Top);
    // majTable( JS51, JS51.get_LIST_Title_Data_Brut(), _JS51_Top);
    // majTable( JS61, JS61.get_LIST_Title_Data_Brut(), _JS61_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @(TITLE)@"));
    
    

    
    
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void BT_PGUPActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGUP", false);
  }
  
  private void BT_PGDOWNActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "PGDOWN", false);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    OBJ_37 = new JLabel();
    P_PnlOpts = new JPanel();
    OBJ_64 = new JButton();
    OBJ_65 = new JButton();
    DEANR = new XRiTextField();
    WNSE = new XRiTextField();
    label1 = new JLabel();
    WETB = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 0));
          p_tete_gauche.setMinimumSize(new Dimension(700, 0));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("?B1.bouton_valider.toolTipText?");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("?B1.bouton_retour.toolTipText?");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(820, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== p_recup ========
          {
            p_recup.setOpaque(false);
            p_recup.setName("p_recup");
            p_recup.setLayout(null);

            //---- OBJ_37 ----
            OBJ_37.setText("Num\u00e9ro 1\u00e8re semaine / ann\u00e9e");
            OBJ_37.setName("OBJ_37");
            p_recup.add(OBJ_37);
            OBJ_37.setBounds(20, 50, 195, 20);

            //======== P_PnlOpts ========
            {
              P_PnlOpts.setName("P_PnlOpts");
              P_PnlOpts.setLayout(null);

              //---- OBJ_64 ----
              OBJ_64.setText("?B1.OBJ_64.text?");
              OBJ_64.setToolTipText("?B1.OBJ_64.toolTipText?");
              OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_64.setName("OBJ_64");
              P_PnlOpts.add(OBJ_64);
              OBJ_64.setBounds(5, 6, 40, 40);

              //---- OBJ_65 ----
              OBJ_65.setText("?B1.OBJ_65.text?");
              OBJ_65.setToolTipText("?B1.OBJ_65.toolTipText?");
              OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              OBJ_65.setName("OBJ_65");
              P_PnlOpts.add(OBJ_65);
              OBJ_65.setBounds(5, 60, 40, 40);
            }
            p_recup.add(P_PnlOpts);
            P_PnlOpts.setBounds(1036, 15, 55, 516);

            //---- DEANR ----
            DEANR.setComponentPopupMenu(BTD);
            DEANR.setName("DEANR");
            p_recup.add(DEANR);
            DEANR.setBounds(255, 45, 54, DEANR.getPreferredSize().height);

            //---- WNSE ----
            WNSE.setComponentPopupMenu(BTD);
            WNSE.setName("WNSE");
            p_recup.add(WNSE);
            WNSE.setBounds(220, 45, 30, WNSE.getPreferredSize().height);

            //---- label1 ----
            label1.setText("Code Etablissement");
            label1.setName("label1");
            p_recup.add(label1);
            label1.setBounds(20, 12, 140, 20);

            //---- WETB ----
            WETB.setName("WETB");
            p_recup.add(WETB);
            WETB.setBounds(170, 8, 40, WETB.getPreferredSize().height);
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(p_recup, GroupLayout.DEFAULT_SIZE, 787, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(p_recup, GroupLayout.PREFERRED_SIZE, 419, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(20, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JLabel OBJ_37;
  private JPanel P_PnlOpts;
  private JButton OBJ_64;
  private JButton OBJ_65;
  private XRiTextField DEANR;
  private XRiTextField WNSE;
  private JLabel label1;
  private XRiTextField WETB;
  private JPopupMenu BTD;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
