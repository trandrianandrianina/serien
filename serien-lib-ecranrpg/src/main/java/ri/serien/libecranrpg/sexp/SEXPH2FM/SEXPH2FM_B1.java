
package ri.serien.libecranrpg.sexp.SEXPH2FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPH2FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _XSL01_Top = null;
  private String[] _XSL01_Title = { "XCTIT2", };
  private String[][] _XSL01_Data = { { "XSL01", }, { "XSL02", }, { "XSL03", }, { "XSL04", }, { "XSL05", }, { "XSL06", }, { "XSL07", },
      { "XSL08", }, { "XSL09", }, { "XSL10", }, { "XSL11", }, { "XSL12", }, { "XSL13", }, { "XSL14", }, { "XSL15", } };
  private int[] _XSL01_Width = { 300, 200, 200 };
  
  public SEXPH2FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    
    XSL01.setAspectTable(_XSL01_Top, _XSL01_Title, _XSL01_Data, _XSL01_Width, true, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Avant @XCAV@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("Après @XCAP@")).trim());
    XCUSR1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCUSR1@")).trim());
    XCUSR2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCUSR2@")).trim());
    XCTIM1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCTIM1@")).trim());
    XCTIM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCTIM2@")).trim());
    XCACT1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCACT1@")).trim());
    XCACT2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCACT2@")).trim());
    XCREP1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCREP1@")).trim());
    XCREP2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCREP2@")).trim());
    XCCLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCCLI@")).trim());
    XCLIV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCLIV@")).trim());
    XCART.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCART@")).trim());
    XCCOL.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCCOL@")).trim());
    XCFRS.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCFRS@")).trim());
    XCCAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCCAT@")).trim());
    XCCNV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCCNV@")).trim());
    XCTRA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCTRA@")).trim());
    XCRAT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCRAT@")).trim());
    XCQTE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCQTE@")).trim());
    XCNUM2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCNUM@")).trim());
    XCSUF2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCSUF@")).trim());
    XCNUM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCNUM@")).trim());
    XCSUF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCSUF@")).trim());
    XCART2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCART2@")).trim());
    XCNLI.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@XCNLI@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    panel1.setVisible(lexique.isTrue("20"));
    panel4.setVisible(lexique.isTrue("21"));
    panel5.setVisible(lexique.isTrue("22"));
    panel6.setVisible(lexique.isTrue("23"));
    panel7.setVisible(lexique.isTrue("24"));
    panel8.setVisible(lexique.isTrue("25"));
    panel9.setVisible(lexique.isTrue("26"));
    
    riSousMenu6.setVisible(lexique.isTrue("N17"));
    riSousMenu7.setVisible(lexique.isTrue("N18"));
    
    // Heures
    String xctim1 = lexique.HostFieldGetData("XCTIM1");
    String xctim2 = lexique.HostFieldGetData("XCTIM2");
    
    if (!xctim1.equals(null)) {
      if (xctim1.length() == 5) {
        xctim1 = '0' + xctim1;
        XCTIM1.setText(xctim1.substring(0, 2) + ":" + xctim1.substring(2, 4) + ":" + xctim1.substring(4, 6));
      }
    }
    if (!xctim2.equals(null)) {
      if (xctim2.length() == 5) {
        xctim2 = '0' + xctim2;
        XCTIM2.setText(xctim2.substring(0, 2) + ":" + xctim2.substring(2, 4) + ":" + xctim2.substring(4, 6));
      }
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3");
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4");
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTP01MouseClicked(MouseEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_54 = new JLabel();
    WETB = new XRiTextField();
    OBJ_55 = new JLabel();
    XCFILE = new XRiTextField();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    XCUSR1 = new RiZoneSortie();
    XCUSR2 = new RiZoneSortie();
    XCDAT1 = new XRiCalendrier();
    XCDAT2 = new XRiCalendrier();
    XCTIM1 = new RiZoneSortie();
    XCTIM2 = new RiZoneSortie();
    XCACT1 = new RiZoneSortie();
    XCACT2 = new RiZoneSortie();
    XCREP1 = new RiZoneSortie();
    XCREP2 = new RiZoneSortie();
    panel3 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    SCROLLPANE__LIST_ = new JScrollPane();
    XSL01 = new XRiTable();
    panel10 = new JPanel();
    panel4 = new JPanel();
    etiquette2 = new JLabel();
    XCCLI = new RiZoneSortie();
    label1 = new JLabel();
    XCLIV = new RiZoneSortie();
    panel1 = new JPanel();
    etiquette01 = new JLabel();
    XCART = new RiZoneSortie();
    panel5 = new JPanel();
    etiquette3 = new JLabel();
    XCCOL = new RiZoneSortie();
    label2 = new JLabel();
    XCFRS = new RiZoneSortie();
    panel6 = new JPanel();
    etiquette4 = new JLabel();
    XCCAT = new RiZoneSortie();
    etiquette5 = new JLabel();
    XCCNV = new RiZoneSortie();
    etiquette6 = new JLabel();
    XCTRA = new RiZoneSortie();
    XCRAT = new RiZoneSortie();
    etiquette7 = new JLabel();
    etiquette8 = new JLabel();
    XCQTE = new RiZoneSortie();
    panel9 = new JPanel();
    etiquette12 = new JLabel();
    XCNUM2 = new RiZoneSortie();
    label4 = new JLabel();
    XCSUF2 = new RiZoneSortie();
    panel7 = new JPanel();
    etiquette9 = new JLabel();
    XCNUM = new RiZoneSortie();
    label3 = new JLabel();
    XCSUF = new RiZoneSortie();
    panel8 = new JPanel();
    etiquette10 = new JLabel();
    XCART2 = new RiZoneSortie();
    etiquette11 = new JLabel();
    XCNLI = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_54 ----
          OBJ_54.setText("Etablissement");
          OBJ_54.setToolTipText("Module");
          OBJ_54.setName("OBJ_54");
          p_tete_gauche.add(OBJ_54);
          OBJ_54.setBounds(5, 5, 95, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(115, 0, 40, WETB.getPreferredSize().height);

          //---- OBJ_55 ----
          OBJ_55.setText("Fichier");
          OBJ_55.setToolTipText("Module");
          OBJ_55.setName("OBJ_55");
          p_tete_gauche.add(OBJ_55);
          OBJ_55.setBounds(180, 5, 52, 20);

          //---- XCFILE ----
          XCFILE.setComponentPopupMenu(BTD);
          XCFILE.setName("XCFILE");
          p_tete_gauche.add(XCFILE);
          XCFILE.setBounds(245, 0, 110, XCFILE.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Pr\u00e9c\u00e9dent");
              riSousMenu_bt6.setToolTipText("Pr\u00e9c\u00e9dent (F7)");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Suivant");
              riSousMenu_bt7.setToolTipText("Suivant (F8)");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setBorder(new TitledBorder(""));
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- label5 ----
            label5.setText("Utilisateur");
            label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
            label5.setName("label5");
            panel2.add(label5);
            label5.setBounds(175, 20, 114, 25);

            //---- label6 ----
            label6.setText("Avant @XCAV@");
            label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
            label6.setName("label6");
            panel2.add(label6);
            label6.setBounds(26, 43, 100, 24);

            //---- label7 ----
            label7.setText("Apr\u00e8s @XCAP@");
            label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
            label7.setName("label7");
            panel2.add(label7);
            label7.setBounds(26, 76, 100, 24);

            //---- label8 ----
            label8.setText("Date");
            label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
            label8.setName("label8");
            panel2.add(label8);
            label8.setBounds(313, 20, 110, 25);

            //---- label9 ----
            label9.setText("Heure");
            label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
            label9.setName("label9");
            panel2.add(label9);
            label9.setBounds(447, 20, 84, 25);

            //---- label10 ----
            label10.setText("Action");
            label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
            label10.setName("label10");
            panel2.add(label10);
            label10.setBounds(555, 20, 114, 25);

            //---- XCUSR1 ----
            XCUSR1.setText("@XCUSR1@");
            XCUSR1.setName("XCUSR1");
            panel2.add(XCUSR1);
            XCUSR1.setBounds(175, 43, 114, XCUSR1.getPreferredSize().height);

            //---- XCUSR2 ----
            XCUSR2.setText("@XCUSR2@");
            XCUSR2.setName("XCUSR2");
            panel2.add(XCUSR2);
            XCUSR2.setBounds(175, 76, 114, XCUSR2.getPreferredSize().height);

            //---- XCDAT1 ----
            XCDAT1.setName("XCDAT1");
            panel2.add(XCDAT1);
            XCDAT1.setBounds(313, 41, 110, XCDAT1.getPreferredSize().height);

            //---- XCDAT2 ----
            XCDAT2.setName("XCDAT2");
            panel2.add(XCDAT2);
            XCDAT2.setBounds(313, 74, 110, XCDAT2.getPreferredSize().height);

            //---- XCTIM1 ----
            XCTIM1.setText("@XCTIM1@");
            XCTIM1.setName("XCTIM1");
            panel2.add(XCTIM1);
            XCTIM1.setBounds(447, 43, 84, XCTIM1.getPreferredSize().height);

            //---- XCTIM2 ----
            XCTIM2.setText("@XCTIM2@");
            XCTIM2.setName("XCTIM2");
            panel2.add(XCTIM2);
            XCTIM2.setBounds(447, 76, 84, XCTIM2.getPreferredSize().height);

            //---- XCACT1 ----
            XCACT1.setText("@XCACT1@");
            XCACT1.setName("XCACT1");
            panel2.add(XCACT1);
            XCACT1.setBounds(555, 43, 114, XCACT1.getPreferredSize().height);

            //---- XCACT2 ----
            XCACT2.setText("@XCACT2@");
            XCACT2.setName("XCACT2");
            panel2.add(XCACT2);
            XCACT2.setBounds(555, 76, 114, XCACT2.getPreferredSize().height);

            //---- XCREP1 ----
            XCREP1.setText("@XCREP1@");
            XCREP1.setName("XCREP1");
            panel2.add(XCREP1);
            XCREP1.setBounds(693, 43, 114, XCREP1.getPreferredSize().height);

            //---- XCREP2 ----
            XCREP2.setText("@XCREP2@");
            XCREP2.setName("XCREP2");
            panel2.add(XCREP2);
            XCREP2.setBounds(693, 76, 114, XCREP2.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setOpaque(false);
            panel3.setBorder(new TitledBorder(""));
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel3.add(BT_PGUP);
            BT_PGUP.setBounds(845, 25, 25, 125);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel3.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(845, 165, 25, 125);

            //======== SCROLLPANE__LIST_ ========
            {
              SCROLLPANE__LIST_.setMinimumSize(new Dimension(900, 0));
              SCROLLPANE__LIST_.setPreferredSize(new Dimension(900, 324));
              SCROLLPANE__LIST_.setAutoscrolls(true);
              SCROLLPANE__LIST_.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);
              SCROLLPANE__LIST_.setName("SCROLLPANE__LIST_");

              //---- XSL01 ----
              XSL01.setPreferredScrollableViewportSize(new Dimension(900, 300));
              XSL01.setComponentPopupMenu(BTD);
              XSL01.setMinimumSize(new Dimension(900, 0));
              XSL01.setPreferredSize(new Dimension(1050, 240));
              XSL01.setName("XSL01");
              XSL01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTP01MouseClicked(e);
                }
              });
              SCROLLPANE__LIST_.setViewportView(XSL01);
            }
            panel3.add(SCROLLPANE__LIST_);
            SCROLLPANE__LIST_.setBounds(15, 25, 825, 267);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          //======== panel10 ========
          {
            panel10.setBorder(new TitledBorder(""));
            panel10.setOpaque(false);
            panel10.setName("panel10");
            panel10.setLayout(null);

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- etiquette2 ----
              etiquette2.setText("Client");
              etiquette2.setFont(etiquette2.getFont().deriveFont(etiquette2.getFont().getSize() + 3f));
              etiquette2.setName("etiquette2");
              panel4.add(etiquette2);
              etiquette2.setBounds(40, 15, 160, 25);

              //---- XCCLI ----
              XCCLI.setText("@XCCLI@");
              XCCLI.setHorizontalAlignment(SwingConstants.RIGHT);
              XCCLI.setName("XCCLI");
              panel4.add(XCCLI);
              XCCLI.setBounds(210, 15, 60, XCCLI.getPreferredSize().height);

              //---- label1 ----
              label1.setText("/");
              label1.setHorizontalAlignment(SwingConstants.CENTER);
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
              label1.setName("label1");
              panel4.add(label1);
              label1.setBounds(270, 15, 20, 24);

              //---- XCLIV ----
              XCLIV.setText("@XCLIV@");
              XCLIV.setHorizontalAlignment(SwingConstants.RIGHT);
              XCLIV.setName("XCLIV");
              panel4.add(XCLIV);
              XCLIV.setBounds(290, 15, 40, XCLIV.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel4);
            panel4.setBounds(10, 15, 855, 59);

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- etiquette01 ----
              etiquette01.setText("Article");
              etiquette01.setFont(etiquette01.getFont().deriveFont(etiquette01.getFont().getSize() + 3f));
              etiquette01.setName("etiquette01");
              panel1.add(etiquette01);
              etiquette01.setBounds(40, 15, 160, 25);

              //---- XCART ----
              XCART.setText("@XCART@");
              XCART.setName("XCART");
              panel1.add(XCART);
              XCART.setBounds(210, 15, 210, XCART.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel1);
            panel1.setBounds(10, 15, 855, 59);

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- etiquette3 ----
              etiquette3.setText("Fournisseur");
              etiquette3.setFont(etiquette3.getFont().deriveFont(etiquette3.getFont().getSize() + 3f));
              etiquette3.setName("etiquette3");
              panel5.add(etiquette3);
              etiquette3.setBounds(40, 15, 160, 25);

              //---- XCCOL ----
              XCCOL.setText("@XCCOL@");
              XCCOL.setName("XCCOL");
              panel5.add(XCCOL);
              XCCOL.setBounds(210, 15, 24, XCCOL.getPreferredSize().height);

              //---- label2 ----
              label2.setText("/");
              label2.setHorizontalAlignment(SwingConstants.CENTER);
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getSize() + 3f));
              label2.setName("label2");
              panel5.add(label2);
              label2.setBounds(235, 15, 20, 24);

              //---- XCFRS ----
              XCFRS.setText("@XCFRS@");
              XCFRS.setName("XCFRS");
              panel5.add(XCFRS);
              XCFRS.setBounds(255, 15, 60, XCFRS.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel5);
            panel5.setBounds(10, 15, 855, 59);

            //======== panel6 ========
            {
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- etiquette4 ----
              etiquette4.setText("Cat\u00e9gorie");
              etiquette4.setFont(etiquette4.getFont().deriveFont(etiquette4.getFont().getSize() + 3f));
              etiquette4.setName("etiquette4");
              panel6.add(etiquette4);
              etiquette4.setBounds(40, 15, 95, 25);

              //---- XCCAT ----
              XCCAT.setText("@XCCAT@");
              XCCAT.setName("XCCAT");
              panel6.add(XCCAT);
              XCCAT.setBounds(140, 15, 24, XCCAT.getPreferredSize().height);

              //---- etiquette5 ----
              etiquette5.setText("Code");
              etiquette5.setFont(etiquette5.getFont().deriveFont(etiquette5.getFont().getSize() + 3f));
              etiquette5.setName("etiquette5");
              panel6.add(etiquette5);
              etiquette5.setBounds(210, 15, 45, 25);

              //---- XCCNV ----
              XCCNV.setText("@XCCNV@");
              XCCNV.setName("XCCNV");
              panel6.add(XCCNV);
              XCCNV.setBounds(255, 15, 60, XCCNV.getPreferredSize().height);

              //---- etiquette6 ----
              etiquette6.setText("Type");
              etiquette6.setFont(etiquette6.getFont().deriveFont(etiquette6.getFont().getSize() + 3f));
              etiquette6.setName("etiquette6");
              panel6.add(etiquette6);
              etiquette6.setBounds(350, 15, 65, 25);

              //---- XCTRA ----
              XCTRA.setText("@XCTRA@");
              XCTRA.setName("XCTRA");
              panel6.add(XCTRA);
              XCTRA.setBounds(420, 15, 24, XCTRA.getPreferredSize().height);

              //---- XCRAT ----
              XCRAT.setText("@XCRAT@");
              XCRAT.setName("XCRAT");
              panel6.add(XCRAT);
              XCRAT.setBounds(520, 15, 210, XCRAT.getPreferredSize().height);

              //---- etiquette7 ----
              etiquette7.setText("Rat");
              etiquette7.setFont(etiquette7.getFont().deriveFont(etiquette7.getFont().getSize() + 3f));
              etiquette7.setName("etiquette7");
              panel6.add(etiquette7);
              etiquette7.setBounds(470, 15, 45, 25);

              //---- etiquette8 ----
              etiquette8.setText("Quantit\u00e9");
              etiquette8.setFont(etiquette8.getFont().deriveFont(etiquette8.getFont().getSize() + 3f));
              etiquette8.setName("etiquette8");
              panel6.add(etiquette8);
              etiquette8.setBounds(745, 15, 65, 25);

              //---- XCQTE ----
              XCQTE.setText("@XCQTE@");
              XCQTE.setName("XCQTE");
              panel6.add(XCQTE);
              XCQTE.setBounds(810, 15, 60, XCQTE.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel6);
            panel6.setBounds(10, 15, 855, 59);

            //======== panel9 ========
            {
              panel9.setOpaque(false);
              panel9.setName("panel9");
              panel9.setLayout(null);

              //---- etiquette12 ----
              etiquette12.setText("Bon d'achat");
              etiquette12.setFont(etiquette12.getFont().deriveFont(etiquette12.getFont().getSize() + 3f));
              etiquette12.setName("etiquette12");
              panel9.add(etiquette12);
              etiquette12.setBounds(40, 15, 160, 25);

              //---- XCNUM2 ----
              XCNUM2.setText("@XCNUM@");
              XCNUM2.setName("XCNUM2");
              panel9.add(XCNUM2);
              XCNUM2.setBounds(210, 15, 60, XCNUM2.getPreferredSize().height);

              //---- label4 ----
              label4.setText("/");
              label4.setHorizontalAlignment(SwingConstants.CENTER);
              label4.setFont(label4.getFont().deriveFont(label4.getFont().getSize() + 3f));
              label4.setName("label4");
              panel9.add(label4);
              label4.setBounds(270, 15, 20, 24);

              //---- XCSUF2 ----
              XCSUF2.setText("@XCSUF@");
              XCSUF2.setName("XCSUF2");
              panel9.add(XCSUF2);
              XCSUF2.setBounds(290, 15, 24, XCSUF2.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel9.getComponentCount(); i++) {
                  Rectangle bounds = panel9.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel9.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel9.setMinimumSize(preferredSize);
                panel9.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel9);
            panel9.setBounds(10, 15, 855, 59);

            //======== panel7 ========
            {
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- etiquette9 ----
              etiquette9.setText("Bon de vente");
              etiquette9.setFont(etiquette9.getFont().deriveFont(etiquette9.getFont().getSize() + 3f));
              etiquette9.setName("etiquette9");
              panel7.add(etiquette9);
              etiquette9.setBounds(40, 15, 160, 25);

              //---- XCNUM ----
              XCNUM.setText("@XCNUM@");
              XCNUM.setName("XCNUM");
              panel7.add(XCNUM);
              XCNUM.setBounds(210, 15, 60, XCNUM.getPreferredSize().height);

              //---- label3 ----
              label3.setText("/");
              label3.setHorizontalAlignment(SwingConstants.CENTER);
              label3.setFont(label3.getFont().deriveFont(label3.getFont().getSize() + 3f));
              label3.setName("label3");
              panel7.add(label3);
              label3.setBounds(270, 15, 20, 24);

              //---- XCSUF ----
              XCSUF.setText("@XCSUF@");
              XCSUF.setName("XCSUF");
              panel7.add(XCSUF);
              XCSUF.setBounds(290, 15, 24, XCSUF.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel7);
            panel7.setBounds(10, 15, 855, 59);

            //======== panel8 ========
            {
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- etiquette10 ----
              etiquette10.setText("Article");
              etiquette10.setFont(etiquette10.getFont().deriveFont(etiquette10.getFont().getSize() + 3f));
              etiquette10.setName("etiquette10");
              panel8.add(etiquette10);
              etiquette10.setBounds(40, 15, 160, 25);

              //---- XCART2 ----
              XCART2.setText("@XCART2@");
              XCART2.setName("XCART2");
              panel8.add(XCART2);
              XCART2.setBounds(210, 15, 210, XCART2.getPreferredSize().height);

              //---- etiquette11 ----
              etiquette11.setText("Ligne");
              etiquette11.setFont(etiquette11.getFont().deriveFont(etiquette11.getFont().getSize() + 3f));
              etiquette11.setName("etiquette11");
              panel8.add(etiquette11);
              etiquette11.setBounds(470, 15, 65, 25);

              //---- XCNLI ----
              XCNLI.setText("@XCNLI@");
              XCNLI.setName("XCNLI");
              panel8.add(XCNLI);
              XCNLI.setBounds(540, 15, 34, XCNLI.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }
            panel10.add(panel8);
            panel8.setBounds(10, 15, 855, 59);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel10.getComponentCount(); i++) {
                Rectangle bounds = panel10.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel10.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel10.setMinimumSize(preferredSize);
              panel10.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup()
                  .addComponent(panel3, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(panel10, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel10, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, 129, Short.MAX_VALUE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 315, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_54;
  private XRiTextField WETB;
  private JLabel OBJ_55;
  private XRiTextField XCFILE;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private RiZoneSortie XCUSR1;
  private RiZoneSortie XCUSR2;
  private XRiCalendrier XCDAT1;
  private XRiCalendrier XCDAT2;
  private RiZoneSortie XCTIM1;
  private RiZoneSortie XCTIM2;
  private RiZoneSortie XCACT1;
  private RiZoneSortie XCACT2;
  private RiZoneSortie XCREP1;
  private RiZoneSortie XCREP2;
  private JPanel panel3;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JScrollPane SCROLLPANE__LIST_;
  private XRiTable XSL01;
  private JPanel panel10;
  private JPanel panel4;
  private JLabel etiquette2;
  private RiZoneSortie XCCLI;
  private JLabel label1;
  private RiZoneSortie XCLIV;
  private JPanel panel1;
  private JLabel etiquette01;
  private RiZoneSortie XCART;
  private JPanel panel5;
  private JLabel etiquette3;
  private RiZoneSortie XCCOL;
  private JLabel label2;
  private RiZoneSortie XCFRS;
  private JPanel panel6;
  private JLabel etiquette4;
  private RiZoneSortie XCCAT;
  private JLabel etiquette5;
  private RiZoneSortie XCCNV;
  private JLabel etiquette6;
  private RiZoneSortie XCTRA;
  private RiZoneSortie XCRAT;
  private JLabel etiquette7;
  private JLabel etiquette8;
  private RiZoneSortie XCQTE;
  private JPanel panel9;
  private JLabel etiquette12;
  private RiZoneSortie XCNUM2;
  private JLabel label4;
  private RiZoneSortie XCSUF2;
  private JPanel panel7;
  private JLabel etiquette9;
  private RiZoneSortie XCNUM;
  private JLabel label3;
  private RiZoneSortie XCSUF;
  private JPanel panel8;
  private JLabel etiquette10;
  private RiZoneSortie XCART2;
  private JLabel etiquette11;
  private RiZoneSortie XCNLI;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
