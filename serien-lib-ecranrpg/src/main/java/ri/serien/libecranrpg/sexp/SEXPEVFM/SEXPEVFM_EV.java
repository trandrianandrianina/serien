/*
 * Created by JFormDesigner on Wed Oct 01 11:34:46 CEST 2008
 */

package ri.serien.libecranrpg.sexp.SEXPEVFM;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVFM_EV extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVFM_EV(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    setDefaultButton(OBJ_14);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    EVLB2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    EVLB1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*USER@")).trim());
    OBJ_11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVMNU@")).trim());
    CML2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CML2@")).trim());
    CML1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@CML1@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    // Traitement spécifique pour le traitement par lot/soumission
    // Pas forcément bo mais évite la modif de 250000 batchs
    String chaine = interpreteurD.analyseExpression("@EVLB2@");
    if (chaine != null) {
      if (chaine.toLowerCase().startsWith("traitement par")) {
        EVLB2.setText("Demande enregistrée");
      }
    }
    
    // TODO Icones
    OBJ_14.setIcon(lexique.chargerImage("images/OK_p.png", true));
    
    // si popup de programme non GXé présentation plus propre avec image
    if (lexique.HostFieldGetData("EVLB2").equalsIgnoreCase("PAS GX")) {
      EVLB1.setText("Programme " + lexique.HostFieldGetData("EVLB1").trim());
      EVLB1.setBounds(130, 38, 250, 24);
      EVLB2.setBounds(130, 70, 250, 24);
      EVLB2.setText("en cours de développement");
      label1.setIcon(lexique.chargerImage("images/logoSerieN.png", true));
    }
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
    // lexique.ClosePanel();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_6 = new JPanel();
    EVLB2 = new JLabel();
    EVLB1 = new JLabel();
    OBJ_12 = new JLabel();
    OBJ_11 = new JLabel();
    label1 = new JLabel();
    OBJ_14 = new JButton();
    CML2 = new JLabel();
    CML1 = new JLabel();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");

    //======== OBJ_6 ========
    {
      OBJ_6.setOpaque(false);
      OBJ_6.setName("OBJ_6");
      OBJ_6.setLayout(null);

      //---- EVLB2 ----
      EVLB2.setText("@EVLB2@");
      EVLB2.setHorizontalAlignment(SwingConstants.CENTER);
      EVLB2.setFont(new Font("sansserif", Font.BOLD, 14));
      EVLB2.setForeground(Color.white);
      EVLB2.setName("EVLB2");
      OBJ_6.add(EVLB2);
      EVLB2.setBounds(0, 70, 380, 24);

      //---- EVLB1 ----
      EVLB1.setText("@EVLB1@");
      EVLB1.setHorizontalAlignment(SwingConstants.CENTER);
      EVLB1.setFont(new Font("sansserif", Font.BOLD, 14));
      EVLB1.setForeground(Color.white);
      EVLB1.setName("EVLB1");
      OBJ_6.add(EVLB1);
      EVLB1.setBounds(0, 38, 380, 24);

      //---- OBJ_12 ----
      OBJ_12.setText("@*USER@");
      OBJ_12.setHorizontalAlignment(SwingConstants.RIGHT);
      OBJ_12.setForeground(Color.white);
      OBJ_12.setName("OBJ_12");
      OBJ_6.add(OBJ_12);
      OBJ_12.setBounds(215, 0, 144, 24);

      //---- OBJ_11 ----
      OBJ_11.setText("@EVMNU@");
      OBJ_11.setForeground(Color.white);
      OBJ_11.setName("OBJ_11");
      OBJ_6.add(OBJ_11);
      OBJ_11.setBounds(15, 0, 88, 24);

      //---- label1 ----
      label1.setHorizontalAlignment(SwingConstants.CENTER);
      label1.setName("label1");
      OBJ_6.add(label1);
      label1.setBounds(10, 0, 120, 120);
    }

    //---- OBJ_14 ----
    OBJ_14.setToolTipText("Ok");
    OBJ_14.setText("Ok");
    OBJ_14.setFont(new Font("sansserif", Font.BOLD, 14));
    OBJ_14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_14.setName("OBJ_14");
    OBJ_14.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_14ActionPerformed(e);
      }
    });

    //---- CML2 ----
    CML2.setText("@CML2@");
    CML2.setForeground(Color.white);
    CML2.setName("CML2");

    //---- CML1 ----
    CML1.setText("@CML1@");
    CML1.setForeground(Color.white);
    CML1.setName("CML1");

    GroupLayout layout = new GroupLayout(this);
    setLayout(layout);
    layout.setHorizontalGroup(
      layout.createParallelGroup()
        .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE)
        .addGroup(layout.createSequentialGroup()
          .addGap(15, 15, 15)
          .addComponent(CML1, GroupLayout.PREFERRED_SIZE, 107, GroupLayout.PREFERRED_SIZE)
          .addGap(28, 28, 28)
          .addComponent(CML2, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
        .addGroup(layout.createSequentialGroup()
          .addGap(85, 85, 85)
          .addComponent(OBJ_14, GroupLayout.PREFERRED_SIZE, 220, GroupLayout.PREFERRED_SIZE))
    );
    layout.setVerticalGroup(
      layout.createParallelGroup()
        .addGroup(layout.createSequentialGroup()
          .addGap(20, 20, 20)
          .addComponent(OBJ_6, GroupLayout.PREFERRED_SIZE, 120, GroupLayout.PREFERRED_SIZE)
          .addGap(5, 5, 5)
          .addGroup(layout.createParallelGroup()
            .addComponent(CML1, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE)
            .addComponent(CML2, GroupLayout.PREFERRED_SIZE, 21, GroupLayout.PREFERRED_SIZE))
          .addGap(14, 14, 14)
          .addComponent(OBJ_14))
    );
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel OBJ_6;
  private JLabel EVLB2;
  private JLabel EVLB1;
  private JLabel OBJ_12;
  private JLabel OBJ_11;
  private JLabel label1;
  private JButton OBJ_14;
  private JLabel CML2;
  private JLabel CML1;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
