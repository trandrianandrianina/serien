
package ri.serien.libecranrpg.sexp.SEXPH5FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import com.jgoodies.forms.factories.DefaultComponentFactory;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class SEXPH5FM_B0 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public SEXPH5FM_B0(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // TODO setData spécifiques...
    panel1.setVisible(lexique.isTrue("N61"));
    panel2.setVisible(lexique.isTrue("N62"));
    panel3.setVisible(lexique.isTrue("N63"));
    panel4.setVisible(lexique.isTrue("N64"));
    panel5.setVisible(lexique.isTrue("N65"));
    panel6.setVisible(lexique.isTrue("N66"));
    panel7.setVisible(lexique.isTrue("N67"));
    panel8.setVisible(lexique.isTrue("N68"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", true);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel9 = new JPanel();
    panel1 = new JPanel();
    separator1 = compFactory.createSeparator("Articles");
    OBJ_95 = new JLabel();
    OBJ_77 = new JLabel();
    XRART = new XRiTextField();
    panel2 = new JPanel();
    separator2 = compFactory.createSeparator("Client");
    OBJ_96 = new JLabel();
    OBJ_92 = new JLabel();
    XRCLI = new XRiTextField();
    OBJ_91 = new JLabel();
    XRLIV = new XRiTextField();
    panel3 = new JPanel();
    separator3 = compFactory.createSeparator("Fournisseur");
    OBJ_97 = new JLabel();
    OBJ_93 = new JLabel();
    XRCOL = new XRiTextField();
    XRFRS = new XRiTextField();
    panel4 = new JPanel();
    separator4 = compFactory.createSeparator("Conditions de vente");
    OBJ_98 = new JLabel();
    OBJ_94 = new JLabel();
    XRCAT = new XRiTextField();
    OBJ_81 = new JLabel();
    XRCNV = new XRiTextField();
    OBJ_82 = new JLabel();
    XRTRA = new XRiTextField();
    OBJ_83 = new JLabel();
    XRRAT = new XRiTextField();
    OBJ_84 = new JLabel();
    XRQTE = new XRiTextField();
    panel5 = new JPanel();
    separator5 = compFactory.createSeparator("Ent\u00eates de bons de vente");
    OBJ_99 = new JLabel();
    OBJ_100 = new JLabel();
    XRNUM1 = new XRiTextField();
    XRSUF1 = new XRiTextField();
    panel6 = new JPanel();
    separator6 = compFactory.createSeparator("Lignes de bon de vente");
    OBJ_101 = new JLabel();
    OBJ_102 = new JLabel();
    XRNUM2 = new XRiTextField();
    XRSUF2 = new XRiTextField();
    panel7 = new JPanel();
    separator7 = compFactory.createSeparator("Ent\u00eates de bons d'achat");
    OBJ_103 = new JLabel();
    OBJ_104 = new JLabel();
    XRNUM3 = new XRiTextField();
    XRSUF3 = new XRiTextField();
    panel8 = new JPanel();
    separator8 = compFactory.createSeparator("Lignes de bons d'achat");
    OBJ_105 = new JLabel();
    OBJ_106 = new JLabel();
    XRNUM4 = new XRiTextField();
    XRSUF4 = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Param\u00e8tre PF");
              riSousMenu_bt6.setToolTipText("Acc\u00e8s au param\u00e8tre PF");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Zones historis\u00e9es");
              riSousMenu_bt7.setToolTipText("Zones historis\u00e9es");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 605));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel9 ========
          {
            panel9.setPreferredSize(new Dimension(900, 600));
            panel9.setMinimumSize(new Dimension(900, 600));
            panel9.setMaximumSize(new Dimension(900, 600));
            panel9.setOpaque(false);
            panel9.setName("panel9");

            //======== panel1 ========
            {
              panel1.setOpaque(false);
              panel1.setName("panel1");
              panel1.setLayout(null);

              //---- separator1 ----
              separator1.setName("separator1");
              panel1.add(separator1);
              separator1.setBounds(5, 5, 825, separator1.getPreferredSize().height);

              //---- OBJ_95 ----
              OBJ_95.setText("Fichier : PGVMARTM");
              OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getSize() + 3f));
              OBJ_95.setName("OBJ_95");
              panel1.add(OBJ_95);
              OBJ_95.setBounds(20, 35, 170, 21);

              //---- OBJ_77 ----
              OBJ_77.setText("Code article");
              OBJ_77.setFont(OBJ_77.getFont().deriveFont(OBJ_77.getFont().getSize() + 3f));
              OBJ_77.setName("OBJ_77");
              panel1.add(OBJ_77);
              OBJ_77.setBounds(235, 35, 125, 21);

              //---- XRART ----
              XRART.setComponentPopupMenu(BTD);
              XRART.setName("XRART");
              panel1.add(XRART);
              XRART.setBounds(390, 31, 166, XRART.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel1.getComponentCount(); i++) {
                  Rectangle bounds = panel1.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel1.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel1.setMinimumSize(preferredSize);
                panel1.setPreferredSize(preferredSize);
              }
            }

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- separator2 ----
              separator2.setName("separator2");
              panel2.add(separator2);
              separator2.setBounds(5, 5, 825, separator2.getPreferredSize().height);

              //---- OBJ_96 ----
              OBJ_96.setText("Fichier : PGVMCLIM");
              OBJ_96.setFont(OBJ_96.getFont().deriveFont(OBJ_96.getFont().getSize() + 3f));
              OBJ_96.setName("OBJ_96");
              panel2.add(OBJ_96);
              OBJ_96.setBounds(20, 35, 170, 21);

              //---- OBJ_92 ----
              OBJ_92.setText("Code client");
              OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getSize() + 3f));
              OBJ_92.setName("OBJ_92");
              panel2.add(OBJ_92);
              OBJ_92.setBounds(235, 35, 125, 21);

              //---- XRCLI ----
              XRCLI.setComponentPopupMenu(BTD);
              XRCLI.setName("XRCLI");
              panel2.add(XRCLI);
              XRCLI.setBounds(390, 31, 60, XRCLI.getPreferredSize().height);

              //---- OBJ_91 ----
              OBJ_91.setText("/");
              OBJ_91.setFont(OBJ_91.getFont().deriveFont(OBJ_91.getFont().getSize() + 3f));
              OBJ_91.setHorizontalAlignment(SwingConstants.CENTER);
              OBJ_91.setName("OBJ_91");
              panel2.add(OBJ_91);
              OBJ_91.setBounds(450, 35, 20, OBJ_91.getPreferredSize().height);

              //---- XRLIV ----
              XRLIV.setComponentPopupMenu(BTD);
              XRLIV.setName("XRLIV");
              panel2.add(XRLIV);
              XRLIV.setBounds(470, 31, 24, XRLIV.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }

            //======== panel3 ========
            {
              panel3.setOpaque(false);
              panel3.setName("panel3");
              panel3.setLayout(null);

              //---- separator3 ----
              separator3.setName("separator3");
              panel3.add(separator3);
              separator3.setBounds(5, 5, 825, separator3.getPreferredSize().height);

              //---- OBJ_97 ----
              OBJ_97.setText("Fichier : PGVMFRSM");
              OBJ_97.setFont(OBJ_97.getFont().deriveFont(OBJ_97.getFont().getSize() + 3f));
              OBJ_97.setName("OBJ_97");
              panel3.add(OBJ_97);
              OBJ_97.setBounds(20, 35, 170, 21);

              //---- OBJ_93 ----
              OBJ_93.setText("Code fournisseur");
              OBJ_93.setFont(OBJ_93.getFont().deriveFont(OBJ_93.getFont().getSize() + 3f));
              OBJ_93.setName("OBJ_93");
              panel3.add(OBJ_93);
              OBJ_93.setBounds(235, 35, 125, 21);

              //---- XRCOL ----
              XRCOL.setComponentPopupMenu(BTD);
              XRCOL.setName("XRCOL");
              panel3.add(XRCOL);
              XRCOL.setBounds(390, 31, 24, XRCOL.getPreferredSize().height);

              //---- XRFRS ----
              XRFRS.setComponentPopupMenu(BTD);
              XRFRS.setName("XRFRS");
              panel3.add(XRFRS);
              XRFRS.setBounds(420, 31, 60, XRFRS.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel3.getComponentCount(); i++) {
                  Rectangle bounds = panel3.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel3.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel3.setMinimumSize(preferredSize);
                panel3.setPreferredSize(preferredSize);
              }
            }

            //======== panel4 ========
            {
              panel4.setOpaque(false);
              panel4.setName("panel4");
              panel4.setLayout(null);

              //---- separator4 ----
              separator4.setName("separator4");
              panel4.add(separator4);
              separator4.setBounds(5, 5, 825, separator4.getPreferredSize().height);

              //---- OBJ_98 ----
              OBJ_98.setText("Fichier : PGVMCNVM");
              OBJ_98.setFont(OBJ_98.getFont().deriveFont(OBJ_98.getFont().getSize() + 3f));
              OBJ_98.setName("OBJ_98");
              panel4.add(OBJ_98);
              OBJ_98.setBounds(20, 35, 170, 21);

              //---- OBJ_94 ----
              OBJ_94.setText("Cat\u00e9gorie");
              OBJ_94.setFont(OBJ_94.getFont().deriveFont(OBJ_94.getFont().getSize() + 3f));
              OBJ_94.setName("OBJ_94");
              panel4.add(OBJ_94);
              OBJ_94.setBounds(235, 35, 75, 21);

              //---- XRCAT ----
              XRCAT.setComponentPopupMenu(BTD);
              XRCAT.setName("XRCAT");
              panel4.add(XRCAT);
              XRCAT.setBounds(310, 31, 24, XRCAT.getPreferredSize().height);

              //---- OBJ_81 ----
              OBJ_81.setText("Code");
              OBJ_81.setFont(OBJ_81.getFont().deriveFont(OBJ_81.getFont().getSize() + 3f));
              OBJ_81.setName("OBJ_81");
              panel4.add(OBJ_81);
              OBJ_81.setBounds(345, 35, 45, OBJ_81.getPreferredSize().height);

              //---- XRCNV ----
              XRCNV.setComponentPopupMenu(BTD);
              XRCNV.setName("XRCNV");
              panel4.add(XRCNV);
              XRCNV.setBounds(390, 31, 44, XRCNV.getPreferredSize().height);

              //---- OBJ_82 ----
              OBJ_82.setText("Type");
              OBJ_82.setFont(OBJ_82.getFont().deriveFont(OBJ_82.getFont().getSize() + 3f));
              OBJ_82.setName("OBJ_82");
              panel4.add(OBJ_82);
              OBJ_82.setBounds(450, 35, 40, OBJ_82.getPreferredSize().height);

              //---- XRTRA ----
              XRTRA.setComponentPopupMenu(BTD);
              XRTRA.setName("XRTRA");
              panel4.add(XRTRA);
              XRTRA.setBounds(490, 31, 24, XRTRA.getPreferredSize().height);

              //---- OBJ_83 ----
              OBJ_83.setText("Rat");
              OBJ_83.setFont(OBJ_83.getFont().deriveFont(OBJ_83.getFont().getSize() + 3f));
              OBJ_83.setName("OBJ_83");
              panel4.add(OBJ_83);
              OBJ_83.setBounds(535, 35, 30, OBJ_83.getPreferredSize().height);

              //---- XRRAT ----
              XRRAT.setComponentPopupMenu(BTD);
              XRRAT.setName("XRRAT");
              panel4.add(XRRAT);
              XRRAT.setBounds(575, 31, 160, XRRAT.getPreferredSize().height);

              //---- OBJ_84 ----
              OBJ_84.setText("Qt\u00e9");
              OBJ_84.setFont(OBJ_84.getFont().deriveFont(OBJ_84.getFont().getSize() + 3f));
              OBJ_84.setName("OBJ_84");
              panel4.add(OBJ_84);
              OBJ_84.setBounds(750, 35, 30, OBJ_84.getPreferredSize().height);

              //---- XRQTE ----
              XRQTE.setComponentPopupMenu(BTD);
              XRQTE.setName("XRQTE");
              panel4.add(XRQTE);
              XRQTE.setBounds(790, 31, 44, XRQTE.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel4.getComponentCount(); i++) {
                  Rectangle bounds = panel4.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel4.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel4.setMinimumSize(preferredSize);
                panel4.setPreferredSize(preferredSize);
              }
            }

            //======== panel5 ========
            {
              panel5.setOpaque(false);
              panel5.setName("panel5");
              panel5.setLayout(null);

              //---- separator5 ----
              separator5.setName("separator5");
              panel5.add(separator5);
              separator5.setBounds(5, 5, 825, separator5.getPreferredSize().height);

              //---- OBJ_99 ----
              OBJ_99.setText("Fichier : PGVMEBCM");
              OBJ_99.setFont(OBJ_99.getFont().deriveFont(OBJ_99.getFont().getSize() + 3f));
              OBJ_99.setName("OBJ_99");
              panel5.add(OBJ_99);
              OBJ_99.setBounds(20, 35, 170, 21);

              //---- OBJ_100 ----
              OBJ_100.setText("Num\u00e9ro de bon");
              OBJ_100.setFont(OBJ_100.getFont().deriveFont(OBJ_100.getFont().getSize() + 3f));
              OBJ_100.setName("OBJ_100");
              panel5.add(OBJ_100);
              OBJ_100.setBounds(235, 35, 125, 21);

              //---- XRNUM1 ----
              XRNUM1.setComponentPopupMenu(BTD);
              XRNUM1.setName("XRNUM1");
              panel5.add(XRNUM1);
              XRNUM1.setBounds(390, 31, 60, XRNUM1.getPreferredSize().height);

              //---- XRSUF1 ----
              XRSUF1.setComponentPopupMenu(BTD);
              XRSUF1.setName("XRSUF1");
              panel5.add(XRSUF1);
              XRSUF1.setBounds(455, 31, 24, XRSUF1.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel5.getComponentCount(); i++) {
                  Rectangle bounds = panel5.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel5.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel5.setMinimumSize(preferredSize);
                panel5.setPreferredSize(preferredSize);
              }
            }

            //======== panel6 ========
            {
              panel6.setOpaque(false);
              panel6.setName("panel6");
              panel6.setLayout(null);

              //---- separator6 ----
              separator6.setName("separator6");
              panel6.add(separator6);
              separator6.setBounds(5, 5, 825, separator6.getPreferredSize().height);

              //---- OBJ_101 ----
              OBJ_101.setText("Fichier : PGVMLBCM");
              OBJ_101.setFont(OBJ_101.getFont().deriveFont(OBJ_101.getFont().getSize() + 3f));
              OBJ_101.setName("OBJ_101");
              panel6.add(OBJ_101);
              OBJ_101.setBounds(20, 35, 170, 21);

              //---- OBJ_102 ----
              OBJ_102.setText("Num\u00e9ro de bon");
              OBJ_102.setFont(OBJ_102.getFont().deriveFont(OBJ_102.getFont().getSize() + 3f));
              OBJ_102.setName("OBJ_102");
              panel6.add(OBJ_102);
              OBJ_102.setBounds(235, 35, 125, 21);

              //---- XRNUM2 ----
              XRNUM2.setComponentPopupMenu(BTD);
              XRNUM2.setName("XRNUM2");
              panel6.add(XRNUM2);
              XRNUM2.setBounds(390, 31, 60, XRNUM2.getPreferredSize().height);

              //---- XRSUF2 ----
              XRSUF2.setComponentPopupMenu(BTD);
              XRSUF2.setName("XRSUF2");
              panel6.add(XRSUF2);
              XRSUF2.setBounds(455, 31, 24, XRSUF2.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel6.getComponentCount(); i++) {
                  Rectangle bounds = panel6.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel6.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel6.setMinimumSize(preferredSize);
                panel6.setPreferredSize(preferredSize);
              }
            }

            //======== panel7 ========
            {
              panel7.setOpaque(false);
              panel7.setName("panel7");
              panel7.setLayout(null);

              //---- separator7 ----
              separator7.setName("separator7");
              panel7.add(separator7);
              separator7.setBounds(5, 5, 825, separator7.getPreferredSize().height);

              //---- OBJ_103 ----
              OBJ_103.setText("Fichier : PGVMEBFM");
              OBJ_103.setFont(OBJ_103.getFont().deriveFont(OBJ_103.getFont().getSize() + 3f));
              OBJ_103.setName("OBJ_103");
              panel7.add(OBJ_103);
              OBJ_103.setBounds(20, 35, 170, 21);

              //---- OBJ_104 ----
              OBJ_104.setText("Num\u00e9ro de bon");
              OBJ_104.setFont(OBJ_104.getFont().deriveFont(OBJ_104.getFont().getSize() + 3f));
              OBJ_104.setName("OBJ_104");
              panel7.add(OBJ_104);
              OBJ_104.setBounds(235, 35, 125, 21);

              //---- XRNUM3 ----
              XRNUM3.setComponentPopupMenu(BTD);
              XRNUM3.setName("XRNUM3");
              panel7.add(XRNUM3);
              XRNUM3.setBounds(390, 31, 60, XRNUM3.getPreferredSize().height);

              //---- XRSUF3 ----
              XRSUF3.setComponentPopupMenu(BTD);
              XRSUF3.setName("XRSUF3");
              panel7.add(XRSUF3);
              XRSUF3.setBounds(455, 31, 24, XRSUF3.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel7.getComponentCount(); i++) {
                  Rectangle bounds = panel7.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel7.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel7.setMinimumSize(preferredSize);
                panel7.setPreferredSize(preferredSize);
              }
            }

            //======== panel8 ========
            {
              panel8.setOpaque(false);
              panel8.setName("panel8");
              panel8.setLayout(null);

              //---- separator8 ----
              separator8.setName("separator8");
              panel8.add(separator8);
              separator8.setBounds(5, 5, 825, separator8.getPreferredSize().height);

              //---- OBJ_105 ----
              OBJ_105.setText("Fichier : PGVMLBFM");
              OBJ_105.setFont(OBJ_105.getFont().deriveFont(OBJ_105.getFont().getSize() + 3f));
              OBJ_105.setName("OBJ_105");
              panel8.add(OBJ_105);
              OBJ_105.setBounds(20, 35, 170, 21);

              //---- OBJ_106 ----
              OBJ_106.setText("Num\u00e9ro de bon");
              OBJ_106.setFont(OBJ_106.getFont().deriveFont(OBJ_106.getFont().getSize() + 3f));
              OBJ_106.setName("OBJ_106");
              panel8.add(OBJ_106);
              OBJ_106.setBounds(235, 35, 125, 21);

              //---- XRNUM4 ----
              XRNUM4.setComponentPopupMenu(BTD);
              XRNUM4.setName("XRNUM4");
              panel8.add(XRNUM4);
              XRNUM4.setBounds(390, 31, 60, XRNUM4.getPreferredSize().height);

              //---- XRSUF4 ----
              XRSUF4.setComponentPopupMenu(BTD);
              XRSUF4.setName("XRSUF4");
              panel8.add(XRSUF4);
              XRSUF4.setBounds(455, 31, 24, XRSUF4.getPreferredSize().height);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel8.getComponentCount(); i++) {
                  Rectangle bounds = panel8.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel8.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel8.setMinimumSize(preferredSize);
                panel8.setPreferredSize(preferredSize);
              }
            }

            GroupLayout panel9Layout = new GroupLayout(panel9);
            panel9.setLayout(panel9Layout);
            panel9Layout.setHorizontalGroup(
              panel9Layout.createParallelGroup()
                .addGroup(panel9Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel9Layout.createParallelGroup()
                    .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)
                    .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 840, GroupLayout.PREFERRED_SIZE)))
            );
            panel9Layout.setVerticalGroup(
              panel9Layout.createParallelGroup()
                .addGroup(panel9Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel5, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel6, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel7, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addComponent(panel8, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(panel9, GroupLayout.PREFERRED_SIZE, 871, GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel9, GroupLayout.PREFERRED_SIZE, 566, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(9, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel9;
  private JPanel panel1;
  private JComponent separator1;
  private JLabel OBJ_95;
  private JLabel OBJ_77;
  private XRiTextField XRART;
  private JPanel panel2;
  private JComponent separator2;
  private JLabel OBJ_96;
  private JLabel OBJ_92;
  private XRiTextField XRCLI;
  private JLabel OBJ_91;
  private XRiTextField XRLIV;
  private JPanel panel3;
  private JComponent separator3;
  private JLabel OBJ_97;
  private JLabel OBJ_93;
  private XRiTextField XRCOL;
  private XRiTextField XRFRS;
  private JPanel panel4;
  private JComponent separator4;
  private JLabel OBJ_98;
  private JLabel OBJ_94;
  private XRiTextField XRCAT;
  private JLabel OBJ_81;
  private XRiTextField XRCNV;
  private JLabel OBJ_82;
  private XRiTextField XRTRA;
  private JLabel OBJ_83;
  private XRiTextField XRRAT;
  private JLabel OBJ_84;
  private XRiTextField XRQTE;
  private JPanel panel5;
  private JComponent separator5;
  private JLabel OBJ_99;
  private JLabel OBJ_100;
  private XRiTextField XRNUM1;
  private XRiTextField XRSUF1;
  private JPanel panel6;
  private JComponent separator6;
  private JLabel OBJ_101;
  private JLabel OBJ_102;
  private XRiTextField XRNUM2;
  private XRiTextField XRSUF2;
  private JPanel panel7;
  private JComponent separator7;
  private JLabel OBJ_103;
  private JLabel OBJ_104;
  private XRiTextField XRNUM3;
  private XRiTextField XRSUF3;
  private JPanel panel8;
  private JComponent separator8;
  private JLabel OBJ_105;
  private JLabel OBJ_106;
  private XRiTextField XRNUM4;
  private XRiTextField XRSUF4;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
