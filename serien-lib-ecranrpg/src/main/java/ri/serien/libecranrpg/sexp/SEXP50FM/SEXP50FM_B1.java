
package ri.serien.libecranrpg.sexp.SEXP50FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXP50FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXP50FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    REC5.setValeursSelection("1", " ");
    REC6.setValeursSelection("1", " ");
    REC4.setValeursSelection("1", " ");
    REC3.setValeursSelection("1", " ");
    REC2.setValeursSelection("1", " ");
    REC1.setValeursSelection("1", " ");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TITPG1@ @TITPG2@")).trim());
    lb_loctp_.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTP@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    FIN6.setEnabled(lexique.isPresent("FIN6"));
    DEB6.setEnabled(lexique.isPresent("DEB6"));
    FIN5.setEnabled(lexique.isPresent("FIN5"));
    DEB5.setEnabled(lexique.isPresent("DEB5"));
    FIN4.setEnabled(lexique.isPresent("FIN4"));
    DEB4.setEnabled(lexique.isPresent("DEB4"));
    FIN3.setEnabled(lexique.isPresent("FIN3"));
    DEB3.setEnabled(lexique.isPresent("DEB3"));
    FIN2.setEnabled(lexique.isPresent("FIN2"));
    DEB2.setEnabled(lexique.isPresent("DEB2"));
    FIN1.setEnabled(lexique.isPresent("FIN1"));
    DEB1.setEnabled(lexique.isPresent("DEB1"));
    // REC5.setSelected(lexique.HostFieldGetData("REC5").equalsIgnoreCase("1"));
    // REC6.setSelected(lexique.HostFieldGetData("REC6").equalsIgnoreCase("1"));
    // REC4.setSelected(lexique.HostFieldGetData("REC4").equalsIgnoreCase("1"));
    // REC3.setSelected(lexique.HostFieldGetData("REC3").equalsIgnoreCase("1"));
    // REC2.setSelected(lexique.HostFieldGetData("REC2").equalsIgnoreCase("1"));
    // REC1.setSelected(lexique.HostFieldGetData("REC1").equalsIgnoreCase("1"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (REC5.isSelected())
    // lexique.HostFieldPutData("REC5", 0, "1");
    // else
    // lexique.HostFieldPutData("REC5", 0, " ");
    // if (REC6.isSelected())
    // lexique.HostFieldPutData("REC6", 0, "1");
    // else
    // lexique.HostFieldPutData("REC6", 0, " ");
    // if (REC4.isSelected())
    // lexique.HostFieldPutData("REC4", 0, "1");
    // else
    // lexique.HostFieldPutData("REC4", 0, " ");
    // if (REC3.isSelected())
    // lexique.HostFieldPutData("REC3", 0, "1");
    // else
    // lexique.HostFieldPutData("REC3", 0, " ");
    // if (REC2.isSelected())
    // lexique.HostFieldPutData("REC2", 0, "1");
    // else
    // lexique.HostFieldPutData("REC2", 0, " ");
    // if (REC1.isSelected())
    // lexique.HostFieldPutData("REC1", 0, "1");
    // else
    // lexique.HostFieldPutData("REC1", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    p_tete_droite = new JPanel();
    lb_loctp_ = new JLabel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_40 = new JLabel();
    REC1 = new XRiCheckBox();
    REC2 = new XRiCheckBox();
    REC3 = new XRiCheckBox();
    REC4 = new XRiCheckBox();
    REC6 = new XRiCheckBox();
    REC5 = new XRiCheckBox();
    OBJ_41 = new JLabel();
    DEB1 = new XRiTextField();
    FIN1 = new XRiTextField();
    DEB2 = new XRiTextField();
    FIN2 = new XRiTextField();
    DEB3 = new XRiTextField();
    FIN3 = new XRiTextField();
    DEB4 = new XRiTextField();
    FIN4 = new XRiTextField();
    DEB5 = new XRiTextField();
    FIN5 = new XRiTextField();
    DEB6 = new XRiTextField();
    FIN6 = new XRiTextField();
    OBJ_42 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_47 = new JLabel();
    OBJ_50 = new JLabel();
    OBJ_53 = new JLabel();
    OBJ_56 = new JLabel();
    OBJ_59 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@TITPG1@ @TITPG2@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));

          //---- lb_loctp_ ----
          lb_loctp_.setText("@LOCTP@");
          lb_loctp_.setMinimumSize(new Dimension(120, 22));
          lb_loctp_.setPreferredSize(new Dimension(120, 22));
          lb_loctp_.setFont(lb_loctp_.getFont().deriveFont(lb_loctp_.getFont().getStyle() | Font.BOLD));
          lb_loctp_.setHorizontalTextPosition(SwingConstants.RIGHT);
          lb_loctp_.setHorizontalAlignment(SwingConstants.RIGHT);
          lb_loctp_.setName("lb_loctp_");
          p_tete_droite.add(lb_loctp_);
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(460, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Groupe(s) de fichiers \u00e0 traiter"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- OBJ_40 ----
            OBJ_40.setText("Num\u00e9ro de version");
            OBJ_40.setName("OBJ_40");
            panel1.add(OBJ_40);
            OBJ_40.setBounds(180, 35, 117, 20);

            //---- REC1 ----
            REC1.setText("PCGM");
            REC1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC1.setName("REC1");
            panel1.add(REC1);
            REC1.setBounds(60, 89, 70, 20);

            //---- REC2 ----
            REC2.setText("PSEM");
            REC2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC2.setName("REC2");
            panel1.add(REC2);
            REC2.setBounds(60, 119, 70, 20);

            //---- REC3 ----
            REC3.setText("PGEM");
            REC3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC3.setName("REC3");
            panel1.add(REC3);
            REC3.setBounds(60, 149, 70, 20);

            //---- REC4 ----
            REC4.setText("PGIM");
            REC4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC4.setName("REC4");
            panel1.add(REC4);
            REC4.setBounds(60, 179, 70, 20);

            //---- REC6 ----
            REC6.setText("PPAM");
            REC6.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC6.setName("REC6");
            panel1.add(REC6);
            REC6.setBounds(60, 239, 70, 20);

            //---- REC5 ----
            REC5.setText("PGVM");
            REC5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            REC5.setName("REC5");
            panel1.add(REC5);
            REC5.setBounds(60, 209, 70, 20);

            //---- OBJ_41 ----
            OBJ_41.setText("D\u00e9but");
            OBJ_41.setName("OBJ_41");
            panel1.add(OBJ_41);
            OBJ_41.setBounds(180, 64, 39, 20);

            //---- DEB1 ----
            DEB1.setComponentPopupMenu(BTD);
            DEB1.setName("DEB1");
            panel1.add(DEB1);
            DEB1.setBounds(180, 85, 34, DEB1.getPreferredSize().height);

            //---- FIN1 ----
            FIN1.setComponentPopupMenu(BTD);
            FIN1.setName("FIN1");
            panel1.add(FIN1);
            FIN1.setBounds(275, 85, 34, FIN1.getPreferredSize().height);

            //---- DEB2 ----
            DEB2.setComponentPopupMenu(BTD);
            DEB2.setName("DEB2");
            panel1.add(DEB2);
            DEB2.setBounds(180, 115, 34, DEB2.getPreferredSize().height);

            //---- FIN2 ----
            FIN2.setComponentPopupMenu(BTD);
            FIN2.setName("FIN2");
            panel1.add(FIN2);
            FIN2.setBounds(275, 115, 34, FIN2.getPreferredSize().height);

            //---- DEB3 ----
            DEB3.setComponentPopupMenu(BTD);
            DEB3.setName("DEB3");
            panel1.add(DEB3);
            DEB3.setBounds(180, 145, 34, DEB3.getPreferredSize().height);

            //---- FIN3 ----
            FIN3.setComponentPopupMenu(BTD);
            FIN3.setName("FIN3");
            panel1.add(FIN3);
            FIN3.setBounds(275, 145, 34, FIN3.getPreferredSize().height);

            //---- DEB4 ----
            DEB4.setComponentPopupMenu(BTD);
            DEB4.setName("DEB4");
            panel1.add(DEB4);
            DEB4.setBounds(180, 175, 34, DEB4.getPreferredSize().height);

            //---- FIN4 ----
            FIN4.setComponentPopupMenu(BTD);
            FIN4.setName("FIN4");
            panel1.add(FIN4);
            FIN4.setBounds(275, 175, 34, FIN4.getPreferredSize().height);

            //---- DEB5 ----
            DEB5.setComponentPopupMenu(BTD);
            DEB5.setName("DEB5");
            panel1.add(DEB5);
            DEB5.setBounds(180, 205, 34, DEB5.getPreferredSize().height);

            //---- FIN5 ----
            FIN5.setComponentPopupMenu(BTD);
            FIN5.setName("FIN5");
            panel1.add(FIN5);
            FIN5.setBounds(275, 205, 34, FIN5.getPreferredSize().height);

            //---- DEB6 ----
            DEB6.setComponentPopupMenu(BTD);
            DEB6.setName("DEB6");
            panel1.add(DEB6);
            DEB6.setBounds(180, 235, 34, DEB6.getPreferredSize().height);

            //---- FIN6 ----
            FIN6.setComponentPopupMenu(BTD);
            FIN6.setName("FIN6");
            panel1.add(FIN6);
            FIN6.setBounds(275, 235, 34, FIN6.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Fin");
            OBJ_42.setName("OBJ_42");
            panel1.add(OBJ_42);
            OBJ_42.setBounds(277, 64, 30, 20);

            //---- OBJ_43 ----
            OBJ_43.setText("1-");
            OBJ_43.setName("OBJ_43");
            panel1.add(OBJ_43);
            OBJ_43.setBounds(35, 89, 20, 20);

            //---- OBJ_47 ----
            OBJ_47.setText("2-");
            OBJ_47.setName("OBJ_47");
            panel1.add(OBJ_47);
            OBJ_47.setBounds(35, 119, 20, 20);

            //---- OBJ_50 ----
            OBJ_50.setText("3-");
            OBJ_50.setName("OBJ_50");
            panel1.add(OBJ_50);
            OBJ_50.setBounds(35, 149, 20, 20);

            //---- OBJ_53 ----
            OBJ_53.setText("4-");
            OBJ_53.setName("OBJ_53");
            panel1.add(OBJ_53);
            OBJ_53.setBounds(35, 179, 20, 20);

            //---- OBJ_56 ----
            OBJ_56.setText("5-");
            OBJ_56.setName("OBJ_56");
            panel1.add(OBJ_56);
            OBJ_56.setBounds(35, 209, 20, 20);

            //---- OBJ_59 ----
            OBJ_59.setText("6-");
            OBJ_59.setName("OBJ_59");
            panel1.add(OBJ_59);
            OBJ_59.setBounds(35, 239, 20, 20);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 350, GroupLayout.PREFERRED_SIZE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 295, GroupLayout.PREFERRED_SIZE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JPanel p_tete_droite;
  private JLabel lb_loctp_;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_40;
  private XRiCheckBox REC1;
  private XRiCheckBox REC2;
  private XRiCheckBox REC3;
  private XRiCheckBox REC4;
  private XRiCheckBox REC6;
  private XRiCheckBox REC5;
  private JLabel OBJ_41;
  private XRiTextField DEB1;
  private XRiTextField FIN1;
  private XRiTextField DEB2;
  private XRiTextField FIN2;
  private XRiTextField DEB3;
  private XRiTextField FIN3;
  private XRiTextField DEB4;
  private XRiTextField FIN4;
  private XRiTextField DEB5;
  private XRiTextField FIN5;
  private XRiTextField DEB6;
  private XRiTextField FIN6;
  private JLabel OBJ_42;
  private JLabel OBJ_43;
  private JLabel OBJ_47;
  private JLabel OBJ_50;
  private JLabel OBJ_53;
  private JLabel OBJ_56;
  private JLabel OBJ_59;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
