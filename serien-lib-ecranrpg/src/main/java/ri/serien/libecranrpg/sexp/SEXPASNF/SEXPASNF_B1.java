
package ri.serien.libecranrpg.sexp.SEXPASNF;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;

/**
 * @author Stéphane Vénéri
 */
public class SEXPASNF_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public SEXPASNF_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_16.setBorder(new TitledBorder(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB1@")).trim()));
    OBJ_19.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@NPAGE@")).trim());
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIB2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    
    
    // R1.setVisible(interpreteurD.analyseExpression("@OPT01@").equalsIgnoreCase("1"));
    // R1.setEnabled( lexique.isPresent("REPONS"));
    // R2.setVisible(interpreteurD.analyseExpression("@OPT02@").equalsIgnoreCase("1"));
    // R2.setEnabled( lexique.isPresent("REPONS"));
    R3.setVisible(interpreteurD.analyseExpression("@OPT03@").equalsIgnoreCase("1"));
    // R3.setEnabled( lexique.isPresent("REPONS"));
    OBJ_15.setVisible(interpreteurD.analyseExpression("@OPT08@").equalsIgnoreCase("1"));
    R8.setVisible(interpreteurD.analyseExpression("@OPT05@").equalsIgnoreCase("1"));
    R5.setVisible(interpreteurD.analyseExpression("@OPT05@").equalsIgnoreCase("1"));
    // R6.setVisible(interpreteurD.analyseExpression("@OPT06@").equalsIgnoreCase("1"));
    R7.setVisible(interpreteurD.analyseExpression("@OPT07@").equalsIgnoreCase("1"));
    R4.setVisible(interpreteurD.analyseExpression("@OPT04@").equalsIgnoreCase("1"));
    OBJ_15.setVisible(interpreteurD.analyseExpression("@OPT08@").equalsIgnoreCase("1"));
    
    // TODO Icones
    OBJ_18.setIcon(lexique.chargerImage("images/edition.gif", true));
    // R6.setIcon(lexique.getImage("images/editcf.gif", true));
    R5.setIcon(lexique.chargerImage("images/pdf.gif", true));
    R7.setIcon(lexique.chargerImage("images/envoyer.gif", true));
    R4.setIcon(lexique.chargerImage("images/word.gif", true));
    R3.setIcon(lexique.chargerImage("images/mater1.gif", true));
    R2.setIcon(lexique.chargerImage("images/impri1.gif", true));
    R1.setIcon(lexique.chargerImage("images/vue1.gif", true));
    OBJ_15.setIcon(lexique.chargerImage("images/pdf.gif", true));
    R8.setIcon(lexique.chargerImage("images/barcode.gif", true));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("EDITION TERMINEE..."));
    
    

    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt12ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void riSousMenu_bt13ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    // lexique.ViewerDocDirect("@PARCH@", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riSousMenu12 = new RiSousMenu();
    riSousMenu_bt12 = new RiSousMenu_bt();
    riSousMenu13 = new RiSousMenu();
    riSousMenu_bt13 = new RiSousMenu_bt();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_16 = new JPanel();
    OBJ_18 = new JLabel();
    OBJ_19 = new RiZoneSortie();
    label1 = new RiZoneSortie();
    OBJ_15 = new JButton();
    R7 = new JButton();
    R8 = new JButton();
    R5 = new JButton();
    R4 = new JButton();
    R3 = new JButton();
    R2 = new JButton();
    R1 = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(725, 240));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 250));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Visu. du fichier d'\u00e9dition");
              riSousMenu_bt6.setToolTipText("Visualisation du fichier d'\u00e9dition");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Lib\u00e9ration");
              riSousMenu_bt7.setToolTipText("<HTML>Lib\u00e9ration en vue de<BR>l'impression du fichier d'\u00e9dition</HTML>");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Choix imprimante");
              riSousMenu_bt8.setToolTipText("<HTML>Choix de l'imprimante et<BR>de la file d'attente d'impression</HTML>");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Exporter vers \u00e9diteur");
              riSousMenu_bt9.setToolTipText("Exporter le fichier d'\u00e9dition vers \u00e9diteur");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Exporter vers PDF");
              riSousMenu_bt10.setToolTipText("Exporter le fichier d'\u00e9dition vers PDF");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Edition des \u00e9tiquettes");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riSousMenu12 ========
            {
              riSousMenu12.setName("riSousMenu12");

              //---- riSousMenu_bt12 ----
              riSousMenu_bt12.setText("Edition + fond de page");
              riSousMenu_bt12.setToolTipText("Edition avec fond de page");
              riSousMenu_bt12.setName("riSousMenu_bt12");
              riSousMenu_bt12.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt12ActionPerformed(e);
                }
              });
              riSousMenu12.add(riSousMenu_bt12);
            }
            menus_haut.add(riSousMenu12);

            //======== riSousMenu13 ========
            {
              riSousMenu13.setName("riSousMenu13");

              //---- riSousMenu_bt13 ----
              riSousMenu_bt13.setText("Envoi par mail du PDF");
              riSousMenu_bt13.setToolTipText("Envoi par mail du PDF");
              riSousMenu_bt13.setName("riSousMenu_bt13");
              riSousMenu_bt13.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt13ActionPerformed(e);
                }
              });
              riSousMenu13.add(riSousMenu_bt13);
            }
            menus_haut.add(riSousMenu13);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.CENTER);

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== OBJ_16 ========
        {
          OBJ_16.setBorder(new TitledBorder("@LIB1@"));
          OBJ_16.setOpaque(false);
          OBJ_16.setName("OBJ_16");
          OBJ_16.setLayout(null);

          //---- OBJ_18 ----
          OBJ_18.setText("");
          OBJ_18.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_18.setName("OBJ_18");
          OBJ_16.add(OBJ_18);
          OBJ_18.setBounds(26, 43, 48, 48);

          //---- OBJ_19 ----
          OBJ_19.setText("@NPAGE@");
          OBJ_19.setFont(OBJ_19.getFont().deriveFont(OBJ_19.getFont().getSize() + 3f));
          OBJ_19.setName("OBJ_19");
          OBJ_16.add(OBJ_19);
          OBJ_19.setBounds(360, 45, 56, OBJ_19.getPreferredSize().height);

          //---- label1 ----
          label1.setText("@LIB2@");
          label1.setFont(label1.getFont().deriveFont(label1.getFont().getSize() + 3f));
          label1.setName("label1");
          OBJ_16.add(label1);
          label1.setBounds(35, 45, 318, label1.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < OBJ_16.getComponentCount(); i++) {
              Rectangle bounds = OBJ_16.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = OBJ_16.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            OBJ_16.setMinimumSize(preferredSize);
            OBJ_16.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(OBJ_16);
        OBJ_16.setBounds(45, 55, 460, OBJ_16.getPreferredSize().height);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //---- OBJ_15 ----
    OBJ_15.setText("");
    OBJ_15.setToolTipText("Exporter le fichier d'\u00e9dition vers Acrobat Reader avec SIM 2.0");
    OBJ_15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    OBJ_15.setName("OBJ_15");
    OBJ_15.addActionListener(new ActionListener() {
      @Override
      public void actionPerformed(ActionEvent e) {
        OBJ_15ActionPerformed(e);
      }
    });

    //---- R7 ----
    R7.setText("");
    R7.setToolTipText("Envoi par mail du document PDF");
    R7.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R7.setName("R7");

    //---- R8 ----
    R8.setText("");
    R8.setToolTipText("Edition des \u00e9tiquettes");
    R8.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R8.setName("R8");

    //---- R5 ----
    R5.setText("");
    R5.setToolTipText("Exporter le fichier d'\u00e9dition vers Acrobat Reader");
    R5.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R5.setName("R5");

    //---- R4 ----
    R4.setText("");
    R4.setToolTipText("Exporter le fichier d'\u00e9dition vers Word");
    R4.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R4.setName("R4");

    //---- R3 ----
    R3.setText("");
    R3.setToolTipText("<HTML>Choix de l'imprimante et<BR>de la file d'attente d'impression</HTML>");
    R3.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R3.setName("R3");

    //---- R2 ----
    R2.setText("");
    R2.setToolTipText("<HTML>Lib\u00e9ration en vue de<BR>l'impression du fichier d'\u00e9dition</HTML>");
    R2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R2.setName("R2");

    //---- R1 ----
    R1.setText("");
    R1.setToolTipText("Visualisation du fichier d'\u00e9dition");
    R1.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
    R1.setName("R1");
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiSousMenu riSousMenu12;
  private RiSousMenu_bt riSousMenu_bt12;
  private RiSousMenu riSousMenu13;
  private RiSousMenu_bt riSousMenu_bt13;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel OBJ_16;
  private JLabel OBJ_18;
  private RiZoneSortie OBJ_19;
  private RiZoneSortie label1;
  private JButton OBJ_15;
  private JButton R7;
  private JButton R8;
  private JButton R5;
  private JButton R4;
  private JButton R3;
  private JButton R2;
  private JButton R1;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
