
package ri.serien.libecranrpg.sexp.SEXPH4FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCheckBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class SEXPH4FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _XSS01_Top = { "XSS01", "XSS02", "XSS03", "XSS04", "XSS05", "XSS06", "XSS07", "XSS08", "XSS09", "XSS10", "XSS11",
      "XSS12", "XSS13", "XSS14", "XSS15", };
  private String[] _XSS01_Title = { "TITCOL", };
  private String[][] _XSS01_Data = { { "XSL01" }, { "XSL02" }, { "XSL03" }, { "XSL04" }, { "XSL05" }, { "XSL06" }, { "XSL07" }, { "XSL08" },
      { "XSL09" }, { "XSL10" }, { "XSL11" }, { "XSL12" }, { "XSL13" }, { "XSL14" }, { "XSL15" } };
  private int[] _XSS01_Width = { 50, };
  
  public SEXPH4FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    XSH15.setValeursSelection("1", " ");
    XSH14.setValeursSelection("1", " ");
    XSH13.setValeursSelection("1", " ");
    XSH12.setValeursSelection("1", " ");
    XSH11.setValeursSelection("1", " ");
    XSH10.setValeursSelection("1", " ");
    XSH09.setValeursSelection("1", " ");
    XSH08.setValeursSelection("1", " ");
    XSH07.setValeursSelection("1", " ");
    XSH06.setValeursSelection("1", " ");
    XSH05.setValeursSelection("1", " ");
    XSH04.setValeursSelection("1", " ");
    XSH03.setValeursSelection("1", " ");
    XSH02.setValeursSelection("1", " ");
    XSH01.setValeursSelection("1", " ");
    XSL01.setAspectTable(_XSS01_Top, _XSS01_Title, _XSS01_Data, _XSS01_Width, false, null, null, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    // XSH15.setVisible( lexique.isPresent("XSH15"));
    // XSH15.setSelected(lexique.HostFieldGetData("XSH15").equalsIgnoreCase("1"));
    // XSH14.setVisible( lexique.isPresent("XSH14"));
    // XSH14.setSelected(lexique.HostFieldGetData("XSH14").equalsIgnoreCase("1"));
    // XSH13.setVisible( lexique.isPresent("XSH13"));
    // XSH13.setSelected(lexique.HostFieldGetData("XSH13").equalsIgnoreCase("1"));
    // XSH12.setVisible( lexique.isPresent("XSH12"));
    // XSH12.setSelected(lexique.HostFieldGetData("XSH12").equalsIgnoreCase("1"));
    // XSH11.setVisible( lexique.isPresent("XSH11"));
    // XSH11.setSelected(lexique.HostFieldGetData("XSH11").equalsIgnoreCase("1"));
    // XSH10.setVisible( lexique.isPresent("XSH10"));
    // XSH10.setSelected(lexique.HostFieldGetData("XSH10").equalsIgnoreCase("1"));
    // XSH09.setVisible( lexique.isPresent("XSH09"));
    // XSH09.setSelected(lexique.HostFieldGetData("XSH09").equalsIgnoreCase("1"));
    // XSH08.setVisible( lexique.isPresent("XSH08"));
    // XSH08.setSelected(lexique.HostFieldGetData("XSH08").equalsIgnoreCase("1"));
    // XSH07.setVisible( lexique.isPresent("XSH07"));
    // XSH07.setSelected(lexique.HostFieldGetData("XSH07").equalsIgnoreCase("1"));
    // XSH06.setVisible( lexique.isPresent("XSH06"));
    // XSH06.setSelected(lexique.HostFieldGetData("XSH06").equalsIgnoreCase("1"));
    // XSH05.setVisible( lexique.isPresent("XSH05"));
    // XSH05.setSelected(lexique.HostFieldGetData("XSH05").equalsIgnoreCase("1"));
    // XSH04.setVisible( lexique.isPresent("XSH04"));
    // XSH04.setSelected(lexique.HostFieldGetData("XSH04").equalsIgnoreCase("1"));
    // XSH03.setVisible( lexique.isPresent("XSH03"));
    // XSH03.setSelected(lexique.HostFieldGetData("XSH03").equalsIgnoreCase("1"));
    // XSH02.setVisible( lexique.isPresent("XSH02"));
    // XSH02.setSelected(lexique.HostFieldGetData("XSH02").equalsIgnoreCase("1"));
    // XSH01.setVisible( lexique.isPresent("XSH01"));
    // XSH01.setSelected(lexique.HostFieldGetData("XSH01").equalsIgnoreCase("1"));
    XCTYP.setVisible(lexique.isPresent("XCTYP"));
    WETB.setVisible(lexique.isPresent("WETB"));
    XCFILE.setVisible(lexique.isPresent("XCFILE"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ -  @TITPG1@ @(TITLE)@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WETB.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WETB.getText()));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // if (XSH15.isSelected())
    // lexique.HostFieldPutData("XSH15", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH15", 0, " ");
    // if (XSH14.isSelected())
    // lexique.HostFieldPutData("XSH14", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH14", 0, " ");
    // if (XSH13.isSelected())
    // lexique.HostFieldPutData("XSH13", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH13", 0, " ");
    // if (XSH12.isSelected())
    // lexique.HostFieldPutData("XSH12", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH12", 0, " ");
    // if (XSH11.isSelected())
    // lexique.HostFieldPutData("XSH11", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH11", 0, " ");
    // if (XSH10.isSelected())
    // lexique.HostFieldPutData("XSH10", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH10", 0, " ");
    // if (XSH09.isSelected())
    // lexique.HostFieldPutData("XSH09", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH09", 0, " ");
    // if (XSH08.isSelected())
    // lexique.HostFieldPutData("XSH08", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH08", 0, " ");
    // if (XSH07.isSelected())
    // lexique.HostFieldPutData("XSH07", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH07", 0, " ");
    // if (XSH06.isSelected())
    // lexique.HostFieldPutData("XSH06", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH06", 0, " ");
    // if (XSH05.isSelected())
    // lexique.HostFieldPutData("XSH05", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH05", 0, " ");
    // if (XSH04.isSelected())
    // lexique.HostFieldPutData("XSH04", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH04", 0, " ");
    // if (XSH03.isSelected())
    // lexique.HostFieldPutData("XSH03", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH03", 0, " ");
    // if (XSH02.isSelected())
    // lexique.HostFieldPutData("XSH02", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH02", 0, " ");
    // if (XSH01.isSelected())
    // lexique.HostFieldPutData("XSH01", 0, "1");
    // else
    // lexique.HostFieldPutData("XSH01", 0, " ");
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_48 = new JLabel();
    WETB = new XRiTextField();
    OBJ_49 = new JLabel();
    XCTYP = new XRiTextField();
    XCFILE = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    panel2 = new JPanel();
    XSH01 = new XRiCheckBox();
    XSH02 = new XRiCheckBox();
    XSH03 = new XRiCheckBox();
    XSH04 = new XRiCheckBox();
    XSH05 = new XRiCheckBox();
    XSH06 = new XRiCheckBox();
    XSH07 = new XRiCheckBox();
    XSH08 = new XRiCheckBox();
    XSH09 = new XRiCheckBox();
    XSH10 = new XRiCheckBox();
    XSH11 = new XRiCheckBox();
    XSH12 = new XRiCheckBox();
    XSH13 = new XRiCheckBox();
    XSH14 = new XRiCheckBox();
    XSH15 = new XRiCheckBox();
    scrollPane1 = new JScrollPane();
    XSL01 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_17 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Choix des champs \u00e0 historiser");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          p_tete_gauche.setLayout(null);

          //---- OBJ_48 ----
          OBJ_48.setText("Etablissement");
          OBJ_48.setToolTipText("Module");
          OBJ_48.setName("OBJ_48");
          p_tete_gauche.add(OBJ_48);
          OBJ_48.setBounds(0, 5, 95, 20);

          //---- WETB ----
          WETB.setComponentPopupMenu(BTD);
          WETB.setName("WETB");
          p_tete_gauche.add(WETB);
          WETB.setBounds(100, 0, 40, WETB.getPreferredSize().height);

          //---- OBJ_49 ----
          OBJ_49.setText("Fichier");
          OBJ_49.setToolTipText("Module");
          OBJ_49.setName("OBJ_49");
          p_tete_gauche.add(OBJ_49);
          OBJ_49.setBounds(160, 5, 52, 20);

          //---- XCTYP ----
          XCTYP.setComponentPopupMenu(BTD);
          XCTYP.setName("XCTYP");
          p_tete_gauche.add(XCTYP);
          XCTYP.setBounds(230, 0, 40, XCTYP.getPreferredSize().height);

          //---- XCFILE ----
          XCFILE.setComponentPopupMenu(BTD);
          XCFILE.setName("XCFILE");
          p_tete_gauche.add(XCFILE);
          XCFILE.setBounds(275, 0, 40, XCFILE.getPreferredSize().height);
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 380));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setToolTipText("Page pr\u00e9c\u00e9dente");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            panel1.add(BT_PGUP);
            BT_PGUP.setBounds(825, 10, 25, 150);

            //---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setToolTipText("Page suivante");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            panel1.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(825, 190, 25, 150);

            //======== panel2 ========
            {
              panel2.setOpaque(false);
              panel2.setName("panel2");
              panel2.setLayout(null);

              //---- XSH01 ----
              XSH01.setText("");
              XSH01.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH01.setName("XSH01");
              panel2.add(XSH01);
              XSH01.setBounds(10, 30, 20, 20);

              //---- XSH02 ----
              XSH02.setText("");
              XSH02.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH02.setName("XSH02");
              panel2.add(XSH02);
              XSH02.setBounds(10, 50, 20, 20);

              //---- XSH03 ----
              XSH03.setText("");
              XSH03.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH03.setName("XSH03");
              panel2.add(XSH03);
              XSH03.setBounds(10, 70, 20, 20);

              //---- XSH04 ----
              XSH04.setText("");
              XSH04.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH04.setName("XSH04");
              panel2.add(XSH04);
              XSH04.setBounds(10, 90, 20, 20);

              //---- XSH05 ----
              XSH05.setText("");
              XSH05.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH05.setName("XSH05");
              panel2.add(XSH05);
              XSH05.setBounds(10, 110, 20, 20);

              //---- XSH06 ----
              XSH06.setText("");
              XSH06.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH06.setName("XSH06");
              panel2.add(XSH06);
              XSH06.setBounds(10, 130, 20, 20);

              //---- XSH07 ----
              XSH07.setText("");
              XSH07.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH07.setName("XSH07");
              panel2.add(XSH07);
              XSH07.setBounds(10, 150, 20, 20);

              //---- XSH08 ----
              XSH08.setText("");
              XSH08.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH08.setName("XSH08");
              panel2.add(XSH08);
              XSH08.setBounds(10, 170, 20, 20);

              //---- XSH09 ----
              XSH09.setText("");
              XSH09.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH09.setName("XSH09");
              panel2.add(XSH09);
              XSH09.setBounds(10, 190, 20, 20);

              //---- XSH10 ----
              XSH10.setText("");
              XSH10.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH10.setName("XSH10");
              panel2.add(XSH10);
              XSH10.setBounds(10, 210, 20, 20);

              //---- XSH11 ----
              XSH11.setText("");
              XSH11.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH11.setName("XSH11");
              panel2.add(XSH11);
              XSH11.setBounds(10, 230, 20, 20);

              //---- XSH12 ----
              XSH12.setText("");
              XSH12.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH12.setName("XSH12");
              panel2.add(XSH12);
              XSH12.setBounds(10, 250, 20, 20);

              //---- XSH13 ----
              XSH13.setText("");
              XSH13.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH13.setName("XSH13");
              panel2.add(XSH13);
              XSH13.setBounds(10, 270, 20, 20);

              //---- XSH14 ----
              XSH14.setText("");
              XSH14.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH14.setName("XSH14");
              panel2.add(XSH14);
              XSH14.setBounds(10, 290, 20, 20);

              //---- XSH15 ----
              XSH15.setText("");
              XSH15.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
              XSH15.setName("XSH15");
              panel2.add(XSH15);
              XSH15.setBounds(10, 310, 20, 20);

              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < panel2.getComponentCount(); i++) {
                  Rectangle bounds = panel2.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = panel2.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                panel2.setMinimumSize(preferredSize);
                panel2.setPreferredSize(preferredSize);
              }
            }
            panel1.add(panel2);
            panel2.setBounds(500, 0, 35, 350);

            //======== scrollPane1 ========
            {
              scrollPane1.setName("scrollPane1");

              //---- XSL01 ----
              XSL01.setOpaque(false);
              XSL01.setSelectionForeground(Color.white);
              XSL01.setRowHeight(20);
              XSL01.setRowSelectionAllowed(false);
              XSL01.setName("XSL01");
              scrollPane1.setViewportView(XSL01);
            }
            panel1.add(scrollPane1);
            scrollPane1.setBounds(40, 10, 775, 330);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 552, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_18 ----
      OBJ_18.setText("Choix possibles");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);

      //---- OBJ_17 ----
      OBJ_17.setText("Aide en ligne");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_48;
  private XRiTextField WETB;
  private JLabel OBJ_49;
  private XRiTextField XCTYP;
  private XRiTextField XCFILE;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPanel panel2;
  private XRiCheckBox XSH01;
  private XRiCheckBox XSH02;
  private XRiCheckBox XSH03;
  private XRiCheckBox XSH04;
  private XRiCheckBox XSH05;
  private XRiCheckBox XSH06;
  private XRiCheckBox XSH07;
  private XRiCheckBox XSH08;
  private XRiCheckBox XSH09;
  private XRiCheckBox XSH10;
  private XRiCheckBox XSH11;
  private XRiCheckBox XSH12;
  private XRiCheckBox XSH13;
  private XRiCheckBox XSH14;
  private XRiCheckBox XSH15;
  private JScrollPane scrollPane1;
  private XRiTable XSL01;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_17;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
