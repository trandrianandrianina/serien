
package ri.serien.libecranrpg.sexp.SEXPERFM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class SEXPERFM_ER extends SNPanelEcranRPG implements ioFrame {
  // Variables
  // private String[] _LIST_Top=null;
  private String[] _I5001_Title = { "", "", "", };
  private String[][] _I5001_Data = { { "I5001", "S5001", "L5001", }, { "I5002", "S5002", "L5002", }, { "I5003", "S5003", "L5003", },
      { "I5004", "S5004", "L5004", }, { "I5005", "S5005", "L5005", }, { "I5006", "S5006", "L5006", }, { "I5007", "S5007", "L5007", },
      { "I5008", "S5008", "L5008", }, };
  private int[] _I5001_Width = { 41, 40, 400, };
  
  /**
   * Constructeur.
   */
  public SEXPERFM_ER(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_retour);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    I5001.setAspectTable(null, _I5001_Title, _I5001_Data, _I5001_Width, false, null, null, null, null);
    
    // Titre
    setTitle("SERIE N - ERREURS");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WX2XA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WX2XA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable( I5001, I5001.get_I5001_Title_Data_Brut(), _I5001_Top);
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    // gererLesErreurs("19");
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    OBJ_18_OBJ_18 = new JXTitledSeparator();
    SCROLLPANE_LIST = new JScrollPane();
    I5001 = new XRiTable();
    OBJ_21_OBJ_21 = new JLabel();
    WX2XA = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(720, 230));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //---- OBJ_18_OBJ_18 ----
        OBJ_18_OBJ_18.setTitle("Signification des erreurs");
        OBJ_18_OBJ_18.setName("OBJ_18_OBJ_18");

        //======== SCROLLPANE_LIST ========
        {
          SCROLLPANE_LIST.setName("SCROLLPANE_LIST");

          //---- I5001 ----
          I5001.setName("I5001");
          SCROLLPANE_LIST.setViewportView(I5001);
        }

        //---- OBJ_21_OBJ_21 ----
        OBJ_21_OBJ_21.setText("Erreurs");
        OBJ_21_OBJ_21.setName("OBJ_21_OBJ_21");

        //---- WX2XA ----
        WX2XA.setBackground(new Color(214, 217, 223));
        WX2XA.setForeground(Color.red);
        WX2XA.setText("@WX2XA@");
        WX2XA.setName("WX2XA");

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_21_OBJ_21, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                  .addGap(7, 7, 7)
                  .addComponent(WX2XA, GroupLayout.PREFERRED_SIZE, 275, GroupLayout.PREFERRED_SIZE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addComponent(OBJ_18_OBJ_18, GroupLayout.DEFAULT_SIZE, 534, Short.MAX_VALUE))
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(20, 20, 20)
                  .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 488, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 11, Short.MAX_VALUE)
                  .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                    .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))))
              .addContainerGap())
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(10, 10, 10)
              .addComponent(OBJ_18_OBJ_18, GroupLayout.PREFERRED_SIZE, 19, GroupLayout.PREFERRED_SIZE)
              .addGap(6, 6, 6)
              .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addComponent(BT_PGUP, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                  .addComponent(BT_PGDOWN, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                .addComponent(SCROLLPANE_LIST, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE))
              .addGap(13, 13, 13)
              .addGroup(p_contenuLayout.createParallelGroup()
                .addGroup(p_contenuLayout.createSequentialGroup()
                  .addGap(2, 2, 2)
                  .addComponent(OBJ_21_OBJ_21))
                .addComponent(WX2XA, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JXTitledSeparator OBJ_18_OBJ_18;
  private JScrollPane SCROLLPANE_LIST;
  private XRiTable I5001;
  private JLabel OBJ_21_OBJ_21;
  private JLabel WX2XA;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
