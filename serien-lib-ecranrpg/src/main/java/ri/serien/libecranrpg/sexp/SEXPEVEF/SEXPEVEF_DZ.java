
package ri.serien.libecranrpg.sexp.SEXPEVEF;
// Nom Fichier: pop_SEXPEVEF_FMTDZ_176.java

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.SwingConstants;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class SEXPEVEF_DZ extends SNPanelEcranRPG implements ioFrame {
  
  
  public SEXPEVEF_DZ(ArrayList<?> param) {
    super(param);
    initComponents();
    setDialog(true);
    
    // Ajout
    initDiverses();
    
    // Bouton par défaut
    
    // Menu Command
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_7_OBJ_7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB1@")).trim());
    OBJ_8_OBJ_8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@EVLB2@")).trim());
    OBJ_5_OBJ_5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@*TIME@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    
    OBJ_5_OBJ_5.setVisible(lexique.isPresent("*TIME"));
    // EV998.setVisible( lexique.isPresent("EV998"));
    OBJ_8_OBJ_8.setVisible(lexique.isPresent("EVLB2"));
    OBJ_7_OBJ_7.setVisible(lexique.isPresent("EVLB1"));
    
    // TODO Icones
    
    // V07F
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@TITPG1@"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    OBJ_7_OBJ_7 = new JLabel();
    OBJ_8_OBJ_8 = new JLabel();
    EV998 = new XRiTextField();
    OBJ_5_OBJ_5 = new JLabel();
    OBJ_9_OBJ_9 = new JLabel();

    //======== this ========
    setBackground(new Color(90, 90, 90));
    setName("this");
    setLayout(null);

    //---- OBJ_7_OBJ_7 ----
    OBJ_7_OBJ_7.setText("@EVLB1@");
    OBJ_7_OBJ_7.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_7_OBJ_7.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_7_OBJ_7.setForeground(Color.white);
    OBJ_7_OBJ_7.setName("OBJ_7_OBJ_7");
    add(OBJ_7_OBJ_7);
    OBJ_7_OBJ_7.setBounds(0, 35, 400, 20);

    //---- OBJ_8_OBJ_8 ----
    OBJ_8_OBJ_8.setText("@EVLB2@");
    OBJ_8_OBJ_8.setFont(new Font("sansserif", Font.PLAIN, 16));
    OBJ_8_OBJ_8.setHorizontalAlignment(SwingConstants.CENTER);
    OBJ_8_OBJ_8.setForeground(Color.white);
    OBJ_8_OBJ_8.setName("OBJ_8_OBJ_8");
    add(OBJ_8_OBJ_8);
    OBJ_8_OBJ_8.setBounds(0, 60, 400, 20);

    //---- EV998 ----
    EV998.setName("EV998");
    add(EV998);
    EV998.setBounds(145, 105, 196, EV998.getPreferredSize().height);

    //---- OBJ_5_OBJ_5 ----
    OBJ_5_OBJ_5.setText("@*TIME@");
    OBJ_5_OBJ_5.setForeground(Color.white);
    OBJ_5_OBJ_5.setName("OBJ_5_OBJ_5");
    add(OBJ_5_OBJ_5);
    OBJ_5_OBJ_5.setBounds(320, 5, 75, 20);

    //---- OBJ_9_OBJ_9 ----
    OBJ_9_OBJ_9.setText("R\u00e9ponse :");
    OBJ_9_OBJ_9.setForeground(Color.white);
    OBJ_9_OBJ_9.setName("OBJ_9_OBJ_9");
    add(OBJ_9_OBJ_9);
    OBJ_9_OBJ_9.setBounds(60, 110, 63, 20);

    setPreferredSize(new Dimension(399, 146));
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JLabel OBJ_7_OBJ_7;
  private JLabel OBJ_8_OBJ_8;
  private XRiTextField EV998;
  private JLabel OBJ_5_OBJ_5;
  private JLabel OBJ_9_OBJ_9;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
