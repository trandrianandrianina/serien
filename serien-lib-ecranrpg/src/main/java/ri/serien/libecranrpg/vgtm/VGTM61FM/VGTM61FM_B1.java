
package ri.serien.libecranrpg.vgtm.VGTM61FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonRecherche;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

public class VGTM61FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
   
  // TODO declarations classe spécifiques...
  
  public VGTM61FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // TODO constructeur spécifiques...
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    DGNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // permet de gérer les modes d'affichage
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    // TODO setData spécifiques...
    
    

    
    p_bpresentation.setCodeEtablissement(WSOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WSOC.getText()));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    // TODO getDATA spécifiques
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void BT_ChgSocActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_18 = new JLabel();
    WSOC = new XRiTextField();
    BT_ChgSoc = new SNBoutonRecherche();
    DGNOM = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label17 = new JLabel();
    label1 = new JLabel();
    label18 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label19 = new JLabel();
    LIB01 = new XRiTextField();
    LIB02 = new XRiTextField();
    LIB03 = new XRiTextField();
    LIB04 = new XRiTextField();
    LIB05 = new XRiTextField();
    LIB06 = new XRiTextField();
    LIB07 = new XRiTextField();
    LIB08 = new XRiTextField();
    LIB09 = new XRiTextField();
    LIB10 = new XRiTextField();
    LIB11 = new XRiTextField();
    LIB12 = new XRiTextField();
    LIB13 = new XRiTextField();
    LIB14 = new XRiTextField();
    LIB15 = new XRiTextField();
    LIB16 = new XRiTextField();
    label20 = new JLabel();
    CPD01 = new XRiTextField();
    CPD02 = new XRiTextField();
    CPD03 = new XRiTextField();
    CPD04 = new XRiTextField();
    CPD05 = new XRiTextField();
    CPD06 = new XRiTextField();
    CPD07 = new XRiTextField();
    CPD08 = new XRiTextField();
    CPD09 = new XRiTextField();
    CPD10 = new XRiTextField();
    CPD11 = new XRiTextField();
    CPD12 = new XRiTextField();
    CPD13 = new XRiTextField();
    CPD14 = new XRiTextField();
    CPD15 = new XRiTextField();
    CPD16 = new XRiTextField();
    label21 = new JLabel();
    label22 = new JLabel();
    CPF01 = new XRiTextField();
    CPF02 = new XRiTextField();
    CPF03 = new XRiTextField();
    CPF04 = new XRiTextField();
    CPF05 = new XRiTextField();
    CPF06 = new XRiTextField();
    CPF07 = new XRiTextField();
    CPF08 = new XRiTextField();
    CPF09 = new XRiTextField();
    CPF10 = new XRiTextField();
    CPF11 = new XRiTextField();
    CPF12 = new XRiTextField();
    CPF13 = new XRiTextField();
    CPF14 = new XRiTextField();
    CPF15 = new XRiTextField();
    CPF16 = new XRiTextField();
    label23 = new JLabel();
    COD01 = new XRiTextField();
    COD02 = new XRiTextField();
    COD03 = new XRiTextField();
    COD04 = new XRiTextField();
    COD05 = new XRiTextField();
    COD06 = new XRiTextField();
    COD07 = new XRiTextField();
    COD08 = new XRiTextField();
    COD09 = new XRiTextField();
    COD10 = new XRiTextField();
    COD11 = new XRiTextField();
    COD12 = new XRiTextField();
    COD13 = new XRiTextField();
    COD14 = new XRiTextField();
    COD15 = new XRiTextField();
    COD16 = new XRiTextField();
    LIC01D = new XRiTextField();
    LIC02D = new XRiTextField();
    LIC03D = new XRiTextField();
    LIC04D = new XRiTextField();
    LIC05D = new XRiTextField();
    LIC06D = new XRiTextField();
    LIC07D = new XRiTextField();
    LIC08D = new XRiTextField();
    LIC09D = new XRiTextField();
    LIC10D = new XRiTextField();
    LIC11D = new XRiTextField();
    LIC12D = new XRiTextField();
    LIC13D = new XRiTextField();
    LIC14D = new XRiTextField();
    LIC15D = new XRiTextField();
    LIC16D = new XRiTextField();
    LIC01F = new XRiTextField();
    LIC02F = new XRiTextField();
    LIC03F = new XRiTextField();
    LIC04F = new XRiTextField();
    LIC05F = new XRiTextField();
    LIC06F = new XRiTextField();
    LIC07F = new XRiTextField();
    LIC08F = new XRiTextField();
    LIC09F = new XRiTextField();
    LIC10F = new XRiTextField();
    LIC11F = new XRiTextField();
    LIC12F = new XRiTextField();
    LIC13F = new XRiTextField();
    LIC14F = new XRiTextField();
    LIC15F = new XRiTextField();
    LIC16F = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Personnalistaion du tableau de tr\u00e9sorerie");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_18 ----
          OBJ_18.setText("Soci\u00e9t\u00e9");
          OBJ_18.setName("OBJ_18");

          //---- WSOC ----
          WSOC.setName("WSOC");

          //---- BT_ChgSoc ----
          BT_ChgSoc.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_ChgSoc.setName("BT_ChgSoc");
          BT_ChgSoc.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BT_ChgSocActionPerformed(e);
              BT_ChgSocActionPerformed(e);
              BT_ChgSocActionPerformed(e);
            }
          });

          //---- DGNOM ----
          DGNOM.setOpaque(false);
          DGNOM.setText("@DGNOM@");
          DGNOM.setName("DGNOM");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE)
                .addGap(3, 3, 3)
                .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, 260, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_18, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(BT_ChgSoc, GroupLayout.PREFERRED_SIZE, 29, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(930, 480));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel1 ========
          {
            panel1.setBorder(new BevelBorder(BevelBorder.LOWERED));
            panel1.setOpaque(false);
            panel1.setName("panel1");

            //---- label17 ----
            label17.setText("N\u00b0");
            label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
            label17.setName("label17");

            //---- label1 ----
            label1.setText("01");
            label1.setName("label1");

            //---- label18 ----
            label18.setText("Libell\u00e9");
            label18.setFont(label18.getFont().deriveFont(label18.getFont().getStyle() | Font.BOLD));
            label18.setHorizontalAlignment(SwingConstants.CENTER);
            label18.setName("label18");

            //---- label2 ----
            label2.setText("02");
            label2.setName("label2");

            //---- label3 ----
            label3.setText("03");
            label3.setName("label3");

            //---- label4 ----
            label4.setText("04");
            label4.setName("label4");

            //---- label5 ----
            label5.setText("05");
            label5.setName("label5");

            //---- label6 ----
            label6.setText("06");
            label6.setName("label6");

            //---- label7 ----
            label7.setText("07");
            label7.setName("label7");

            //---- label8 ----
            label8.setText("08");
            label8.setName("label8");

            //---- label9 ----
            label9.setText("09");
            label9.setName("label9");

            //---- label10 ----
            label10.setText("10");
            label10.setName("label10");

            //---- label11 ----
            label11.setText("11");
            label11.setName("label11");

            //---- label12 ----
            label12.setText("12");
            label12.setName("label12");

            //---- label13 ----
            label13.setText("13");
            label13.setName("label13");

            //---- label14 ----
            label14.setText("14");
            label14.setName("label14");

            //---- label15 ----
            label15.setText("15");
            label15.setName("label15");

            //---- label16 ----
            label16.setText("16");
            label16.setName("label16");

            //---- label19 ----
            label19.setText("Compte");
            label19.setFont(label19.getFont().deriveFont(label19.getFont().getStyle() | Font.BOLD));
            label19.setHorizontalAlignment(SwingConstants.CENTER);
            label19.setName("label19");

            //---- LIB01 ----
            LIB01.setName("LIB01");

            //---- LIB02 ----
            LIB02.setName("LIB02");

            //---- LIB03 ----
            LIB03.setName("LIB03");

            //---- LIB04 ----
            LIB04.setName("LIB04");

            //---- LIB05 ----
            LIB05.setName("LIB05");

            //---- LIB06 ----
            LIB06.setName("LIB06");

            //---- LIB07 ----
            LIB07.setName("LIB07");

            //---- LIB08 ----
            LIB08.setName("LIB08");

            //---- LIB09 ----
            LIB09.setName("LIB09");

            //---- LIB10 ----
            LIB10.setName("LIB10");

            //---- LIB11 ----
            LIB11.setName("LIB11");

            //---- LIB12 ----
            LIB12.setName("LIB12");

            //---- LIB13 ----
            LIB13.setName("LIB13");

            //---- LIB14 ----
            LIB14.setName("LIB14");

            //---- LIB15 ----
            LIB15.setName("LIB15");

            //---- LIB16 ----
            LIB16.setName("LIB16");

            //---- label20 ----
            label20.setText("D\u00e9but");
            label20.setFont(label20.getFont().deriveFont(label20.getFont().getStyle() | Font.BOLD));
            label20.setHorizontalAlignment(SwingConstants.CENTER);
            label20.setName("label20");

            //---- CPD01 ----
            CPD01.setComponentPopupMenu(BTD);
            CPD01.setName("CPD01");

            //---- CPD02 ----
            CPD02.setComponentPopupMenu(BTD);
            CPD02.setName("CPD02");

            //---- CPD03 ----
            CPD03.setComponentPopupMenu(BTD);
            CPD03.setName("CPD03");

            //---- CPD04 ----
            CPD04.setComponentPopupMenu(BTD);
            CPD04.setName("CPD04");

            //---- CPD05 ----
            CPD05.setComponentPopupMenu(BTD);
            CPD05.setName("CPD05");

            //---- CPD06 ----
            CPD06.setComponentPopupMenu(BTD);
            CPD06.setName("CPD06");

            //---- CPD07 ----
            CPD07.setComponentPopupMenu(BTD);
            CPD07.setName("CPD07");

            //---- CPD08 ----
            CPD08.setComponentPopupMenu(BTD);
            CPD08.setName("CPD08");

            //---- CPD09 ----
            CPD09.setComponentPopupMenu(BTD);
            CPD09.setName("CPD09");

            //---- CPD10 ----
            CPD10.setComponentPopupMenu(BTD);
            CPD10.setName("CPD10");

            //---- CPD11 ----
            CPD11.setComponentPopupMenu(BTD);
            CPD11.setName("CPD11");

            //---- CPD12 ----
            CPD12.setComponentPopupMenu(BTD);
            CPD12.setName("CPD12");

            //---- CPD13 ----
            CPD13.setComponentPopupMenu(BTD);
            CPD13.setName("CPD13");

            //---- CPD14 ----
            CPD14.setComponentPopupMenu(BTD);
            CPD14.setName("CPD14");

            //---- CPD15 ----
            CPD15.setComponentPopupMenu(BTD);
            CPD15.setName("CPD15");

            //---- CPD16 ----
            CPD16.setComponentPopupMenu(BTD);
            CPD16.setName("CPD16");

            //---- label21 ----
            label21.setText("Compte");
            label21.setFont(label21.getFont().deriveFont(label21.getFont().getStyle() | Font.BOLD));
            label21.setHorizontalAlignment(SwingConstants.CENTER);
            label21.setName("label21");

            //---- label22 ----
            label22.setText("Fin");
            label22.setFont(label22.getFont().deriveFont(label22.getFont().getStyle() | Font.BOLD));
            label22.setHorizontalAlignment(SwingConstants.CENTER);
            label22.setName("label22");

            //---- CPF01 ----
            CPF01.setComponentPopupMenu(BTD);
            CPF01.setName("CPF01");

            //---- CPF02 ----
            CPF02.setComponentPopupMenu(BTD);
            CPF02.setName("CPF02");

            //---- CPF03 ----
            CPF03.setComponentPopupMenu(BTD);
            CPF03.setName("CPF03");

            //---- CPF04 ----
            CPF04.setComponentPopupMenu(BTD);
            CPF04.setName("CPF04");

            //---- CPF05 ----
            CPF05.setComponentPopupMenu(BTD);
            CPF05.setName("CPF05");

            //---- CPF06 ----
            CPF06.setComponentPopupMenu(BTD);
            CPF06.setName("CPF06");

            //---- CPF07 ----
            CPF07.setComponentPopupMenu(BTD);
            CPF07.setName("CPF07");

            //---- CPF08 ----
            CPF08.setComponentPopupMenu(BTD);
            CPF08.setName("CPF08");

            //---- CPF09 ----
            CPF09.setComponentPopupMenu(BTD);
            CPF09.setName("CPF09");

            //---- CPF10 ----
            CPF10.setComponentPopupMenu(BTD);
            CPF10.setName("CPF10");

            //---- CPF11 ----
            CPF11.setComponentPopupMenu(BTD);
            CPF11.setName("CPF11");

            //---- CPF12 ----
            CPF12.setComponentPopupMenu(BTD);
            CPF12.setName("CPF12");

            //---- CPF13 ----
            CPF13.setComponentPopupMenu(BTD);
            CPF13.setName("CPF13");

            //---- CPF14 ----
            CPF14.setComponentPopupMenu(BTD);
            CPF14.setName("CPF14");

            //---- CPF15 ----
            CPF15.setComponentPopupMenu(BTD);
            CPF15.setName("CPF15");

            //---- CPF16 ----
            CPF16.setComponentPopupMenu(BTD);
            CPF16.setName("CPF16");

            //---- label23 ----
            label23.setText("Code");
            label23.setFont(label23.getFont().deriveFont(label23.getFont().getStyle() | Font.BOLD));
            label23.setHorizontalAlignment(SwingConstants.CENTER);
            label23.setName("label23");

            //---- COD01 ----
            COD01.setName("COD01");

            //---- COD02 ----
            COD02.setName("COD02");

            //---- COD03 ----
            COD03.setName("COD03");

            //---- COD04 ----
            COD04.setName("COD04");

            //---- COD05 ----
            COD05.setName("COD05");

            //---- COD06 ----
            COD06.setName("COD06");

            //---- COD07 ----
            COD07.setName("COD07");

            //---- COD08 ----
            COD08.setName("COD08");

            //---- COD09 ----
            COD09.setName("COD09");

            //---- COD10 ----
            COD10.setName("COD10");

            //---- COD11 ----
            COD11.setName("COD11");

            //---- COD12 ----
            COD12.setName("COD12");

            //---- COD13 ----
            COD13.setName("COD13");

            //---- COD14 ----
            COD14.setName("COD14");

            //---- COD15 ----
            COD15.setName("COD15");

            //---- COD16 ----
            COD16.setName("COD16");

            //---- LIC01D ----
            LIC01D.setEditable(false);
            LIC01D.setName("LIC01D");

            //---- LIC02D ----
            LIC02D.setEditable(false);
            LIC02D.setName("LIC02D");

            //---- LIC03D ----
            LIC03D.setEditable(false);
            LIC03D.setName("LIC03D");

            //---- LIC04D ----
            LIC04D.setEditable(false);
            LIC04D.setName("LIC04D");

            //---- LIC05D ----
            LIC05D.setEditable(false);
            LIC05D.setName("LIC05D");

            //---- LIC06D ----
            LIC06D.setEditable(false);
            LIC06D.setName("LIC06D");

            //---- LIC07D ----
            LIC07D.setEditable(false);
            LIC07D.setName("LIC07D");

            //---- LIC08D ----
            LIC08D.setEditable(false);
            LIC08D.setName("LIC08D");

            //---- LIC09D ----
            LIC09D.setEditable(false);
            LIC09D.setName("LIC09D");

            //---- LIC10D ----
            LIC10D.setEditable(false);
            LIC10D.setName("LIC10D");

            //---- LIC11D ----
            LIC11D.setEditable(false);
            LIC11D.setName("LIC11D");

            //---- LIC12D ----
            LIC12D.setEditable(false);
            LIC12D.setName("LIC12D");

            //---- LIC13D ----
            LIC13D.setEditable(false);
            LIC13D.setName("LIC13D");

            //---- LIC14D ----
            LIC14D.setEditable(false);
            LIC14D.setName("LIC14D");

            //---- LIC15D ----
            LIC15D.setEditable(false);
            LIC15D.setName("LIC15D");

            //---- LIC16D ----
            LIC16D.setEditable(false);
            LIC16D.setName("LIC16D");

            //---- LIC01F ----
            LIC01F.setEditable(false);
            LIC01F.setName("LIC01F");

            //---- LIC02F ----
            LIC02F.setEditable(false);
            LIC02F.setName("LIC02F");

            //---- LIC03F ----
            LIC03F.setEditable(false);
            LIC03F.setName("LIC03F");

            //---- LIC04F ----
            LIC04F.setEditable(false);
            LIC04F.setName("LIC04F");

            //---- LIC05F ----
            LIC05F.setEditable(false);
            LIC05F.setName("LIC05F");

            //---- LIC06F ----
            LIC06F.setEditable(false);
            LIC06F.setName("LIC06F");

            //---- LIC07F ----
            LIC07F.setEditable(false);
            LIC07F.setName("LIC07F");

            //---- LIC08F ----
            LIC08F.setEditable(false);
            LIC08F.setName("LIC08F");

            //---- LIC09F ----
            LIC09F.setEditable(false);
            LIC09F.setName("LIC09F");

            //---- LIC10F ----
            LIC10F.setEditable(false);
            LIC10F.setName("LIC10F");

            //---- LIC11F ----
            LIC11F.setEditable(false);
            LIC11F.setName("LIC11F");

            //---- LIC12F ----
            LIC12F.setEditable(false);
            LIC12F.setName("LIC12F");

            //---- LIC13F ----
            LIC13F.setEditable(false);
            LIC13F.setName("LIC13F");

            //---- LIC14F ----
            LIC14F.setEditable(false);
            LIC14F.setName("LIC14F");

            //---- LIC15F ----
            LIC15F.setEditable(false);
            LIC15F.setName("LIC15F");

            //---- LIC16F ----
            LIC16F.setEditable(false);
            LIC16F.setName("LIC16F");

            GroupLayout panel1Layout = new GroupLayout(panel1);
            panel1.setLayout(panel1Layout);
            panel1Layout.setHorizontalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(15, 15, 15)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(label17, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addComponent(label18, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(label19, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(label20, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(label21, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(label22, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                      .addGap(5, 5, 5)
                      .addComponent(label23, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label1)
                        .addComponent(label2, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label3, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label4, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label5, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label6, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label7, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label8, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label9, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label10, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label11, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label12, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label13, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label14, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label15, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label16, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
                      .addGap(20, 20, 20)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB16, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIB15, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(CPD01, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD02, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD04, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD08, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD06, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD07, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD05, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD09, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD12, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD11, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD10, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD13, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD03, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD15, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD16, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPD14, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(LIC01D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC06D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC07D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC04D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC02D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC12D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC10D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC09D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC03D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC11D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC08D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC05D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC13D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC14D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC16D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC15D, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(CPF01, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF12, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF09, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF05, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF14, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF07, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF08, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF03, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF04, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF06, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF11, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF13, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF16, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF15, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF10, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(CPF02, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(LIC03F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC04F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC05F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC08F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC06F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC02F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC01F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC07F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC13F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC14F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC10F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC16F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC15F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC12F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC11F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE)
                        .addComponent(LIC09F, GroupLayout.PREFERRED_SIZE, 180, GroupLayout.PREFERRED_SIZE))
                      .addGap(5, 5, 5)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(COD01, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD04, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD03, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD02, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD10, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD15, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD06, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD14, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD07, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD11, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD16, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD08, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD12, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD09, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD13, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                        .addComponent(COD05, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)))))
            );
            panel1Layout.setVerticalGroup(
              panel1Layout.createParallelGroup()
                .addGroup(panel1Layout.createSequentialGroup()
                  .addGap(10, 10, 10)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addComponent(label17)
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(8, 8, 8)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(label18, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label19, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label20, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label21, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label22, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE)
                        .addComponent(label23, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE))))
                  .addGap(2, 2, 2)
                  .addGroup(panel1Layout.createParallelGroup()
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(6, 6, 6)
                      .addComponent(label1)
                      .addGap(9, 9, 9)
                      .addComponent(label2)
                      .addGap(9, 9, 9)
                      .addComponent(label3)
                      .addGap(9, 9, 9)
                      .addComponent(label4)
                      .addGap(9, 9, 9)
                      .addComponent(label5)
                      .addGap(9, 9, 9)
                      .addComponent(label6)
                      .addGap(9, 9, 9)
                      .addComponent(label7)
                      .addGap(9, 9, 9)
                      .addComponent(label8)
                      .addGap(9, 9, 9)
                      .addComponent(label9)
                      .addGap(9, 9, 9)
                      .addComponent(label10)
                      .addGap(9, 9, 9)
                      .addComponent(label11)
                      .addGap(9, 9, 9)
                      .addComponent(label12)
                      .addGap(9, 9, 9)
                      .addComponent(label13)
                      .addGap(9, 9, 9)
                      .addComponent(label14)
                      .addGap(9, 9, 9)
                      .addComponent(label15)
                      .addGap(9, 9, 9)
                      .addComponent(label16))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIB03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIB04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(325, 325, 325)
                          .addComponent(LIB14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(LIB11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIB01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(LIB09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(LIB10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIB05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(LIB08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIB06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(LIB13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIB07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIB02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(LIB12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addComponent(LIB16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(350, 350, 350)
                      .addComponent(LIB15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(CPD01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(CPD02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(CPD04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(CPD08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(CPD06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(CPD07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(CPD05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(CPD09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(CPD12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(CPD11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(CPD10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(CPD13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(CPD03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(CPD15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(CPD16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(325, 325, 325)
                      .addComponent(CPD14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(LIC01D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIC06D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIC07D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIC04D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIC02D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(LIC12D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(LIC10D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(LIC09D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIC03D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(LIC11D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(LIC08D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIC05D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(LIC13D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(325, 325, 325)
                          .addComponent(LIC14D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addComponent(LIC16D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(350, 350, 350)
                      .addComponent(LIC15D, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addComponent(CPF01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGap(22, 22, 22)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(CPF12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(CPF09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(CPF05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(275, 275, 275)
                          .addComponent(CPF14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(CPF07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(CPF08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(CPF03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(CPF04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(CPF06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(CPF11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(CPF13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(325, 325, 325)
                          .addComponent(CPF16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(300, 300, 300)
                          .addComponent(CPF15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(CPF10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(25, 25, 25)
                      .addComponent(CPF02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIC03F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIC04F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIC05F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(LIC08F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIC06F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIC02F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIC01F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIC07F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(LIC13F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(LIC14F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(LIC10F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(LIC16F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(LIC15F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(LIC12F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(LIC11F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(200, 200, 200)
                      .addComponent(LIC09F, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGroup(panel1Layout.createParallelGroup()
                        .addComponent(COD01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(COD04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(COD03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(COD02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                      .addGap(22, 22, 22)
                      .addGroup(panel1Layout.createParallelGroup()
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(100, 100, 100)
                          .addComponent(COD10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(225, 225, 225)
                          .addComponent(COD15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addComponent(COD06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(200, 200, 200)
                          .addComponent(COD14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(25, 25, 25)
                          .addComponent(COD07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(125, 125, 125)
                          .addComponent(COD11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(250, 250, 250)
                          .addComponent(COD16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(50, 50, 50)
                          .addComponent(COD08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(150, 150, 150)
                          .addComponent(COD12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(75, 75, 75)
                          .addComponent(COD09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                        .addGroup(panel1Layout.createSequentialGroup()
                          .addGap(175, 175, 175)
                          .addComponent(COD13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                    .addGroup(panel1Layout.createSequentialGroup()
                      .addGap(100, 100, 100)
                      .addComponent(COD05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(30, 30, 30))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);

      //---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_18;
  private XRiTextField WSOC;
  private SNBoutonRecherche BT_ChgSoc;
  private RiZoneSortie DGNOM;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label17;
  private JLabel label1;
  private JLabel label18;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label19;
  private XRiTextField LIB01;
  private XRiTextField LIB02;
  private XRiTextField LIB03;
  private XRiTextField LIB04;
  private XRiTextField LIB05;
  private XRiTextField LIB06;
  private XRiTextField LIB07;
  private XRiTextField LIB08;
  private XRiTextField LIB09;
  private XRiTextField LIB10;
  private XRiTextField LIB11;
  private XRiTextField LIB12;
  private XRiTextField LIB13;
  private XRiTextField LIB14;
  private XRiTextField LIB15;
  private XRiTextField LIB16;
  private JLabel label20;
  private XRiTextField CPD01;
  private XRiTextField CPD02;
  private XRiTextField CPD03;
  private XRiTextField CPD04;
  private XRiTextField CPD05;
  private XRiTextField CPD06;
  private XRiTextField CPD07;
  private XRiTextField CPD08;
  private XRiTextField CPD09;
  private XRiTextField CPD10;
  private XRiTextField CPD11;
  private XRiTextField CPD12;
  private XRiTextField CPD13;
  private XRiTextField CPD14;
  private XRiTextField CPD15;
  private XRiTextField CPD16;
  private JLabel label21;
  private JLabel label22;
  private XRiTextField CPF01;
  private XRiTextField CPF02;
  private XRiTextField CPF03;
  private XRiTextField CPF04;
  private XRiTextField CPF05;
  private XRiTextField CPF06;
  private XRiTextField CPF07;
  private XRiTextField CPF08;
  private XRiTextField CPF09;
  private XRiTextField CPF10;
  private XRiTextField CPF11;
  private XRiTextField CPF12;
  private XRiTextField CPF13;
  private XRiTextField CPF14;
  private XRiTextField CPF15;
  private XRiTextField CPF16;
  private JLabel label23;
  private XRiTextField COD01;
  private XRiTextField COD02;
  private XRiTextField COD03;
  private XRiTextField COD04;
  private XRiTextField COD05;
  private XRiTextField COD06;
  private XRiTextField COD07;
  private XRiTextField COD08;
  private XRiTextField COD09;
  private XRiTextField COD10;
  private XRiTextField COD11;
  private XRiTextField COD12;
  private XRiTextField COD13;
  private XRiTextField COD14;
  private XRiTextField COD15;
  private XRiTextField COD16;
  private XRiTextField LIC01D;
  private XRiTextField LIC02D;
  private XRiTextField LIC03D;
  private XRiTextField LIC04D;
  private XRiTextField LIC05D;
  private XRiTextField LIC06D;
  private XRiTextField LIC07D;
  private XRiTextField LIC08D;
  private XRiTextField LIC09D;
  private XRiTextField LIC10D;
  private XRiTextField LIC11D;
  private XRiTextField LIC12D;
  private XRiTextField LIC13D;
  private XRiTextField LIC14D;
  private XRiTextField LIC15D;
  private XRiTextField LIC16D;
  private XRiTextField LIC01F;
  private XRiTextField LIC02F;
  private XRiTextField LIC03F;
  private XRiTextField LIC04F;
  private XRiTextField LIC05F;
  private XRiTextField LIC06F;
  private XRiTextField LIC07F;
  private XRiTextField LIC08F;
  private XRiTextField LIC09F;
  private XRiTextField LIC10F;
  private XRiTextField LIC11F;
  private XRiTextField LIC12F;
  private XRiTextField LIC13F;
  private XRiTextField LIC14F;
  private XRiTextField LIC15F;
  private XRiTextField LIC16F;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
