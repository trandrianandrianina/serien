
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Container;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.JXTitledSeparator;
import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.ODialog;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.autonome.recherchejournaux.GfxRechercheJournaux;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_B1 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _WTX01_Top = { "WTX01", "WTX02", "WTX03", "WTX04", "WTX05", "WTX06", "WTX07", "WTX08", "WTX09", "WTX10", "WTX11",
      "WTX12", "WTX13", "WTX14", "WTX15", "WTX16", };
  private String[] _WTX01_Title = { "HLD01", };
  private String[][] _WTX01_Data = { { "LR01", }, { "LR02", }, { "LR03", }, { "LR04", }, { "LR05", }, { "LR06", }, { "LR07", },
      { "LR08", }, { "LR09", }, { "LR10", }, { "LR11", }, { "LR12", }, { "LR13", }, { "LR14", }, { "LR15", }, { "LR16", }, };
  private int[] _WTX01_Width = { 22, };
  public ODialog dialog_JO = null;
  
  /**
   * Constructeur.
   */
  public VGTM71FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTX01.setAspectTable(_WTX01_Top, _WTX01_Title, _WTX01_Data, _WTX01_Width, false, null, null, null, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation
        .setText(lexique.TranslationTable(interpreteurD.analyseExpression("Saisie des écritures de trésorerie : @LOCTIT@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_80.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIB@")).trim());
    OBJ_82.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOAGB@")).trim());
    OBJ_81.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@JOLIBR@")).trim());
    OBJ_92.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT1@")).trim());
    OBJ_95.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TIT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    WCOD.setVisible(lexique.isPresent("WCOD"));
    L1NLI.setVisible(lexique.isPresent("L1NLI"));
    L1NCGX.setEnabled(lexique.isPresent("L1NCGX"));
    L1NCA.setEnabled(lexique.isPresent("L1NCA"));
    WC1NFA.setEnabled(lexique.isPresent("WC1NFA"));
    WC1MTT.setEnabled(lexique.isPresent("WC1MTT"));
    WMTCP.setVisible(lexique.isTrue("85"));
    WMTCR.setVisible(lexique.isTrue("84"));
    WRCHA.setEnabled(lexique.isPresent("WRCHA"));
    OBJ_81.setVisible(lexique.isPresent("JOLIBR"));
    OBJ_95.setVisible(WMTCP.isVisible());
    OBJ_92.setVisible(WMTCR.isVisible());
    OBJ_82.setVisible(lexique.isPresent("JOAGB"));
    OBJ_80.setVisible(lexique.isPresent("JOLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@LOCTIT@"));
    p_bpresentation.setCodeEtablissement(E1SOC.getText());
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F15", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_18ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "1", "Enter");
    WTX01.setValeurTop("1");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_19ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "2", "Enter");
    WTX01.setValeurTop("2");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_20ActionPerformed(ActionEvent e) {
    // lexique.validSelection(LIST, _WTX01_Top, "4", "Enter");
    WTX01.setValeurTop("4");
    lexique.HostScreenSendKey(this, "Enter");
  }
  
  private void OBJ_22ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_21ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void WTX01MouseClicked(MouseEvent e) {
    // lexique.doubleClicSelection(LIST, _WTX01_Top, "1", "Enter", e);
    if (WTX01.doubleClicSelection(e)) {
      lexique.HostScreenSendKey(this, "Enter");
    }
  }
  
  private void BTN_JOActionPerformed(ActionEvent e) {
    lexique.addVariableGlobale("CODE_SOC", E1SOC.getText());
    lexique.addVariableGlobale("ZONE_JO", "E1CJO");
    if (dialog_JO == null) {
      dialog_JO = new ODialog((Window) getTopLevelAncestor(), new GfxRechercheJournaux(this));
    }
    dialog_JO.affichePopupPerso();
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_53 = new JLabel();
    E1SOC = new XRiTextField();
    E1CJO = new XRiTextField();
    OBJ_55 = new JLabel();
    E1CFO = new XRiTextField();
    OBJ_56 = new JLabel();
    E1DTEX = new XRiTextField();
    BTN_JO2 = new JButton();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    P_Centre = new JPanel();
    SCROLLPANE_LIST4 = new JScrollPane();
    WTX01 = new XRiTable();
    OBJ_80 = new RiZoneSortie();
    OBJ_82 = new RiZoneSortie();
    OBJ_81 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    OBJ_90 = new JLabel();
    OBJ_101 = new JLabel();
    OBJ_91 = new JLabel();
    OBJ_92 = new JLabel();
    WMTCR = new XRiTextField();
    WCOD = new XRiTextField();
    L1NLI = new XRiTextField();
    L1NCGX = new XRiTextField();
    L1NCA = new XRiTextField();
    OBJ_95 = new JLabel();
    WMTCP = new XRiTextField();
    xTitledSeparator1 = new JXTitledSeparator();
    OBJ_98 = new JLabel();
    WRCHA = new XRiTextField();
    OBJ_99 = new JLabel();
    WC1NFA = new XRiTextField();
    OBJ_100 = new JLabel();
    WC1MTT = new XRiTextField();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_18 = new JMenuItem();
    OBJ_19 = new JMenuItem();
    OBJ_20 = new JMenuItem();
    OBJ_22 = new JMenuItem();
    OBJ_21 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());
      
      // ---- p_bpresentation ----
      p_bpresentation.setText("Saisie des \u00e9critures de tr\u00e9sorerie : @LOCTIT@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);
      
      // ======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");
        
        // ======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");
          
          // ---- OBJ_53 ----
          OBJ_53.setText("Soci\u00e9t\u00e9");
          OBJ_53.setName("OBJ_53");
          
          // ---- E1SOC ----
          E1SOC.setComponentPopupMenu(null);
          E1SOC.setName("E1SOC");
          
          // ---- E1CJO ----
          E1CJO.setComponentPopupMenu(null);
          E1CJO.setName("E1CJO");
          
          // ---- OBJ_55 ----
          OBJ_55.setText("Folio");
          OBJ_55.setName("OBJ_55");
          
          // ---- E1CFO ----
          E1CFO.setComponentPopupMenu(null);
          E1CFO.setName("E1CFO");
          
          // ---- OBJ_56 ----
          OBJ_56.setText("Date");
          OBJ_56.setName("OBJ_56");
          
          // ---- E1DTEX ----
          E1DTEX.setComponentPopupMenu(null);
          E1DTEX.setName("E1DTEX");
          
          // ---- BTN_JO2 ----
          BTN_JO2.setText("Journals");
          BTN_JO2.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BTN_JO2.setToolTipText("Recherche d'un journal");
          BTN_JO2.setName("BTN_JO2");
          BTN_JO2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              BTN_JOActionPerformed(e);
            }
          });
          
          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(5, 5, 5)
                  .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                  .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(23, 23, 23)
                  .addComponent(BTN_JO2, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                  .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                  .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(40, 40, 40)
                  .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE).addGap(0, 0, 0)
                  .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE).addGap(204, 204, 204)));
          p_tete_gaucheLayout.setVerticalGroup(p_tete_gaucheLayout.createParallelGroup()
              .addComponent(E1SOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                  .addComponent(E1CJO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addComponent(BTN_JO2))
              .addComponent(E1CFO, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(E1DTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup().addGap(4, 4, 4)
                  .addGroup(p_tete_gaucheLayout.createParallelGroup()
                      .addComponent(OBJ_53, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_55, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                      .addComponent(OBJ_56, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))));
        }
        barre_tete.add(p_tete_gauche);
        
        // ======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);
    
    // ======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Modification indicatif folio");
              riSousMenu_bt6.setToolTipText("Modification de l'indicatif du folio");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Options de fin de folio");
              riSousMenu_bt7.setToolTipText("Options de fin de folio");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Curseur sur le jour");
              riSousMenu_bt8.setToolTipText("Mise En/Hors fonction curseur positionn\u00e9 sur le jour");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Affectations r\u00e8glements");
              riSousMenu_bt9.setToolTipText("Mise En/Hors fonction des affectations  de r\u00e8glements");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Recherche d'\u00e9critures");
              riSousMenu_bt10.setToolTipText("Recherche d'\u00e9critures et retour sur l'en-t\u00eate...");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Demande d'acceptation");
              riSousMenu_bt11
                  .setToolTipText("Demande d'acceptation diff\u00e9r\u00e9e des r\u00e8glements (Si le param\u00e8tre EP existe)");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);
      
      // ======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());
        
        // ======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 560));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");
          
          // ======== P_Centre ========
          {
            P_Centre.setOpaque(false);
            P_Centre.setName("P_Centre");
            P_Centre.setLayout(null);
            
            // ======== SCROLLPANE_LIST4 ========
            {
              SCROLLPANE_LIST4.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST4.setName("SCROLLPANE_LIST4");
              
              // ---- WTX01 ----
              WTX01.setComponentPopupMenu(BTD);
              WTX01.setName("WTX01");
              WTX01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTX01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST4.setViewportView(WTX01);
            }
            P_Centre.add(SCROLLPANE_LIST4);
            SCROLLPANE_LIST4.setBounds(20, 45, 655, 285);
            
            // ---- OBJ_80 ----
            OBJ_80.setText("@JOLIB@");
            OBJ_80.setName("OBJ_80");
            P_Centre.add(OBJ_80);
            OBJ_80.setBounds(20, 15, 232, OBJ_80.getPreferredSize().height);
            
            // ---- OBJ_82 ----
            OBJ_82.setText("@JOAGB@");
            OBJ_82.setHorizontalAlignment(SwingConstants.RIGHT);
            OBJ_82.setName("OBJ_82");
            P_Centre.add(OBJ_82);
            OBJ_82.setBounds(422, 15, 253, OBJ_82.getPreferredSize().height);
            
            // ---- OBJ_81 ----
            OBJ_81.setText("@JOLIBR@");
            OBJ_81.setName("OBJ_81");
            P_Centre.add(OBJ_81);
            OBJ_81.setBounds(20, 20, 190, 20);
            
            // ======== xTitledPanel1 ========
            {
              xTitledPanel1.setBorder(new DropShadowBorder());
              xTitledPanel1.setName("xTitledPanel1");
              Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
              xTitledPanel1ContentContainer.setLayout(null);
              
              // ---- OBJ_90 ----
              OBJ_90.setText("C");
              OBJ_90.setName("OBJ_90");
              xTitledPanel1ContentContainer.add(OBJ_90);
              OBJ_90.setBounds(15, 14, 15, 18);
              
              // ---- OBJ_101 ----
              OBJ_101.setText("N\u00b0lig.");
              OBJ_101.setName("OBJ_101");
              xTitledPanel1ContentContainer.add(OBJ_101);
              OBJ_101.setBounds(45, 14, 34, 18);
              
              // ---- OBJ_91 ----
              OBJ_91.setText("Num\u00e9ro compte");
              OBJ_91.setName("OBJ_91");
              xTitledPanel1ContentContainer.add(OBJ_91);
              OBJ_91.setBounds(90, 14, 112, 18);
              
              // ---- OBJ_92 ----
              OBJ_92.setText("@TIT1@");
              OBJ_92.setFont(OBJ_92.getFont().deriveFont(OBJ_92.getFont().getStyle() | Font.BOLD));
              OBJ_92.setName("OBJ_92");
              xTitledPanel1ContentContainer.add(OBJ_92);
              OBJ_92.setBounds(295, 14, 195, 20);
              
              // ---- WMTCR ----
              WMTCR.setComponentPopupMenu(BTD);
              WMTCR.setFont(WMTCR.getFont().deriveFont(WMTCR.getFont().getStyle() | Font.BOLD));
              WMTCR.setName("WMTCR");
              xTitledPanel1ContentContainer.add(WMTCR);
              WMTCR.setBounds(495, 10, 140, WMTCR.getPreferredSize().height);
              
              // ---- WCOD ----
              WCOD.setComponentPopupMenu(BTD);
              WCOD.setName("WCOD");
              xTitledPanel1ContentContainer.add(WCOD);
              WCOD.setBounds(15, 40, 20, WCOD.getPreferredSize().height);
              
              // ---- L1NLI ----
              L1NLI.setComponentPopupMenu(BTD);
              L1NLI.setName("L1NLI");
              xTitledPanel1ContentContainer.add(L1NLI);
              L1NLI.setBounds(45, 40, 44, L1NLI.getPreferredSize().height);
              
              // ---- L1NCGX ----
              L1NCGX.setComponentPopupMenu(BTD);
              L1NCGX.setName("L1NCGX");
              xTitledPanel1ContentContainer.add(L1NCGX);
              L1NCGX.setBounds(90, 40, 60, L1NCGX.getPreferredSize().height);
              
              // ---- L1NCA ----
              L1NCA.setComponentPopupMenu(BTD);
              L1NCA.setName("L1NCA");
              xTitledPanel1ContentContainer.add(L1NCA);
              L1NCA.setBounds(155, 40, 60, L1NCA.getPreferredSize().height);
              
              // ---- OBJ_95 ----
              OBJ_95.setText("@TIT2@");
              OBJ_95.setFont(OBJ_95.getFont().deriveFont(OBJ_95.getFont().getStyle() | Font.BOLD));
              OBJ_95.setName("OBJ_95");
              xTitledPanel1ContentContainer.add(OBJ_95);
              OBJ_95.setBounds(295, 44, 195, 20);
              
              // ---- WMTCP ----
              WMTCP.setComponentPopupMenu(BTD);
              WMTCP.setFont(WMTCP.getFont().deriveFont(WMTCP.getFont().getStyle() | Font.BOLD));
              WMTCP.setName("WMTCP");
              xTitledPanel1ContentContainer.add(WMTCP);
              WMTCP.setBounds(495, 40, 140, WMTCP.getPreferredSize().height);
              
              // ---- xTitledSeparator1 ----
              xTitledSeparator1.setTitle("Recherche par");
              xTitledSeparator1.setName("xTitledSeparator1");
              xTitledPanel1ContentContainer.add(xTitledSeparator1);
              xTitledSeparator1.setBounds(10, 75, 635, xTitledSeparator1.getPreferredSize().height);
              
              // ---- OBJ_98 ----
              OBJ_98.setText("Nom de compte");
              OBJ_98.setName("OBJ_98");
              xTitledPanel1ContentContainer.add(OBJ_98);
              OBJ_98.setBounds(15, 100, 99, 18);
              
              // ---- WRCHA ----
              WRCHA.setComponentPopupMenu(BTD);
              WRCHA.setName("WRCHA");
              xTitledPanel1ContentContainer.add(WRCHA);
              WRCHA.setBounds(125, 95, 116, WRCHA.getPreferredSize().height);
              
              // ---- OBJ_99 ----
              OBJ_99.setText("Num\u00e9ro de facture");
              OBJ_99.setName("OBJ_99");
              xTitledPanel1ContentContainer.add(OBJ_99);
              OBJ_99.setBounds(250, 100, 114, 18);
              
              // ---- WC1NFA ----
              WC1NFA.setComponentPopupMenu(BTD);
              WC1NFA.setName("WC1NFA");
              xTitledPanel1ContentContainer.add(WC1NFA);
              WC1NFA.setBounds(360, 95, 68, WC1NFA.getPreferredSize().height);
              
              // ---- OBJ_100 ----
              OBJ_100.setText("Montant");
              OBJ_100.setName("OBJ_100");
              xTitledPanel1ContentContainer.add(OBJ_100);
              OBJ_100.setBounds(445, 100, 51, 18);
              
              // ---- WC1MTT ----
              WC1MTT.setComponentPopupMenu(BTD);
              WC1MTT.setName("WC1MTT");
              xTitledPanel1ContentContainer.add(WC1MTT);
              WC1MTT.setBounds(511, 95, 124, WC1MTT.getPreferredSize().height);
              
              {
                // compute preferred size
                Dimension preferredSize = new Dimension();
                for (int i = 0; i < xTitledPanel1ContentContainer.getComponentCount(); i++) {
                  Rectangle bounds = xTitledPanel1ContentContainer.getComponent(i).getBounds();
                  preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                  preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = xTitledPanel1ContentContainer.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                xTitledPanel1ContentContainer.setMinimumSize(preferredSize);
                xTitledPanel1ContentContainer.setPreferredSize(preferredSize);
              }
            }
            P_Centre.add(xTitledPanel1);
            xTitledPanel1.setBounds(20, 345, 690, 170);
            
            // ---- BT_PGUP ----
            BT_PGUP.setText("");
            BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGUP.setName("BT_PGUP");
            P_Centre.add(BT_PGUP);
            BT_PGUP.setBounds(685, 45, 25, 130);
            
            // ---- BT_PGDOWN ----
            BT_PGDOWN.setText("");
            BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            BT_PGDOWN.setName("BT_PGDOWN");
            P_Centre.add(BT_PGDOWN);
            BT_PGDOWN.setBounds(685, 200, 25, 130);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < P_Centre.getComponentCount(); i++) {
                Rectangle bounds = P_Centre.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = P_Centre.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              P_Centre.setMinimumSize(preferredSize);
              P_Centre.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 723, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(13, Short.MAX_VALUE)));
          p_contenuLayout.setVerticalGroup(p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup().addContainerGap()
                  .addComponent(P_Centre, GroupLayout.PREFERRED_SIZE, 535, GroupLayout.PREFERRED_SIZE)
                  .addContainerGap(10, Short.MAX_VALUE)));
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0, GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setComponentPopupMenu(null);
      BTD.setName("BTD");
      
      // ---- OBJ_18 ----
      OBJ_18.setText("Choisir");
      OBJ_18.setName("OBJ_18");
      OBJ_18.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_18ActionPerformed(e);
        }
      });
      BTD.add(OBJ_18);
      
      // ---- OBJ_19 ----
      OBJ_19.setText("Modifier");
      OBJ_19.setName("OBJ_19");
      OBJ_19.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_19ActionPerformed(e);
        }
      });
      BTD.add(OBJ_19);
      
      // ---- OBJ_20 ----
      OBJ_20.setText("Annuler");
      OBJ_20.setName("OBJ_20");
      OBJ_20.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_20ActionPerformed(e);
        }
      });
      BTD.add(OBJ_20);
      BTD.addSeparator();
      
      // ---- OBJ_22 ----
      OBJ_22.setText("Choix possibles");
      OBJ_22.setName("OBJ_22");
      OBJ_22.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_22ActionPerformed(e);
        }
      });
      BTD.add(OBJ_22);
      
      // ---- OBJ_21 ----
      OBJ_21.setText("Aide en ligne");
      OBJ_21.setName("OBJ_21");
      OBJ_21.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_21ActionPerformed(e);
        }
      });
      BTD.add(OBJ_21);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_53;
  private XRiTextField E1SOC;
  private XRiTextField E1CJO;
  private JLabel OBJ_55;
  private XRiTextField E1CFO;
  private JLabel OBJ_56;
  private XRiTextField E1DTEX;
  private JButton BTN_JO2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel P_Centre;
  private JScrollPane SCROLLPANE_LIST4;
  private XRiTable WTX01;
  private RiZoneSortie OBJ_80;
  private RiZoneSortie OBJ_82;
  private JLabel OBJ_81;
  private JXTitledPanel xTitledPanel1;
  private JLabel OBJ_90;
  private JLabel OBJ_101;
  private JLabel OBJ_91;
  private JLabel OBJ_92;
  private XRiTextField WMTCR;
  private XRiTextField WCOD;
  private XRiTextField L1NLI;
  private XRiTextField L1NCGX;
  private XRiTextField L1NCA;
  private JLabel OBJ_95;
  private XRiTextField WMTCP;
  private JXTitledSeparator xTitledSeparator1;
  private JLabel OBJ_98;
  private XRiTextField WRCHA;
  private JLabel OBJ_99;
  private XRiTextField WC1NFA;
  private JLabel OBJ_100;
  private XRiTextField WC1MTT;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_18;
  private JMenuItem OBJ_19;
  private JMenuItem OBJ_20;
  private JMenuItem OBJ_22;
  private JMenuItem OBJ_21;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
