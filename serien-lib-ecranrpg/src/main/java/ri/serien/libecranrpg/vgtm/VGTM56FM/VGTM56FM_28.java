
package ri.serien.libecranrpg.vgtm.VGTM56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.BevelBorder;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM56FM_28 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _WTW01_Top =
      { "WTW01", "WTW02", "WTW03", "WTW04", "WTW05", "WTW06", "WTW07", "WTW08", "WTW09", "WTW10", "WTW11", "WTW12", "WTW13", "WTW14", };
  private String[] _WTW01_Title = { "HLDNN", };
  private String[][] _WTW01_Data = { { "LW01", }, { "LW02", }, { "LW03", }, { "LW04", }, { "LW05", }, { "LW06", }, { "LW07", }, { "LW08", },
      { "LW09", }, { "LW10", }, { "LW11", }, { "LW12", }, { "LW13", }, { "LW14", }, };
  private int[] _WTW01_Width = { 541, };
  private Color[][] _WTW01_Text_Color = new Color[14][1];
  
  private String[] _ZTW01_Top =
      { "ZTW01", "ZTW02", "ZTW03", "ZTW04", "ZTW05", "ZTW06", "ZTW07", "ZTW08", "ZTW09", "ZTW10", "ZTW11", "ZTW12", "ZTW13", "ZTW14" };
  private String[][] _ZTW01_Data = { { "LZ01", }, { "LZ02", }, { "LZ03", }, { "LZ04", }, { "LZ05", }, { "LZ06", }, { "LZ07", }, { "LZ08", },
      { "LZ09", }, { "LZ10", }, { "LZ11", }, { "LZ12", }, { "LZ13", }, { "LZ14", }, };
  private int[] _ZTW01_Width = { 541, };
  private Color[][] _ZTW01_Text_Color = new Color[14][1];
  
  private Color bleu = Constantes.COULEUR_LISTE_LETTRAGE;
  
  public VGTM56FM_28(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    WTW01.setAspectTable(_WTW01_Top, _WTW01_Title, _WTW01_Data, _WTW01_Width, false, null, _WTW01_Text_Color, null, null);
    ZTW01.setAspectTable(_ZTW01_Top, null, _ZTW01_Data, _ZTW01_Width, false, null, _ZTW01_Text_Color, null, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WZ2B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZ2B@")).trim());
    WZ1B.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WZ1B@")).trim());
    HMTTDB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HMTTDB@")).trim());
    HMTTCR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@HMTTCR@")).trim());
    BMTTCR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BMTTCR@")).trim());
    BMTTDB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@BMTTDB@")).trim());
    RMTTD.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@RMTTD@")).trim());
  }
  
  @Override
  public void setData() {
    // Couleur de la liste WTW01 INDICATEURS de 21 à 34
    for (int i = 0; i < 14; i++) {
      if (lexique.isTrue(String.valueOf(i + 21))) {
        _WTW01_Text_Color[i][0] = bleu;
      }
      else {
        _WTW01_Text_Color[i][0] = Color.BLACK;
      }
    }
    
    // Couleur de la liste LZ01 INDICATEURS de 53 à 66
    for (int i = 0; i < 14; i++) {
      if (lexique.isTrue(String.valueOf(i + 53))) {
        _ZTW01_Text_Color[i][0] = bleu;
      }
      else {
        _ZTW01_Text_Color[i][0] = Color.BLACK;
      }
    }
    
    super.setData();
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    

    
    


    
    
    panel4.setVisible(lexique.isTrue("88"));
    OBJ_68.setVisible(lexique.isTrue("88"));
    OBJ_65.setVisible(lexique.isTrue("88"));
    BMTTCR.setVisible(lexique.isTrue("88"));
    BMTTDB.setVisible(lexique.isTrue("88"));
    HMTTCR.setVisible(lexique.isTrue("88"));
    HMTTDB.setVisible(lexique.isTrue("88"));
    riSousMenu14.setEnabled(lexique.HostFieldGetData("ZIRCH").equalsIgnoreCase("1"));
    
    WZ1B.setEnabled(lexique.isPresent("WZ1B"));
    
    OBJ_70.setVisible(lexique.isTrue("(87) AND (88)"));
    RMTTD.setVisible(lexique.isTrue("(87) AND (88)"));
    
    WZ2B.setEnabled(lexique.isPresent("WZ2B"));
    
    // TODO Icones
    OBJ_62.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_71.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    OBJ_63.setIcon(lexique.chargerImage("images/pgup20.png", true));
    OBJ_72.setIcon(lexique.chargerImage("images/pgdwn20.png", true));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt4.setIcon(lexique.chargerImage("images/fonctions.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F19", false);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F20", false);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F21", false);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F10", false);
  }
  
  private void riSousMenu_bt18ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16", false);
  }
  
  private void riSousMenu_bt19ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F17", false);
  }
  
  private void riSousMenu_bt20ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", false);
  }
  
  private void riSousMenu_bt21ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    // si équilibré on fait entrée
    if (lexique.HostFieldGetData("RMTTD").equalsIgnoreCase("")
        && ((!lexique.HostFieldGetData("HMTTDB").equalsIgnoreCase("")) || (!lexique.HostFieldGetData("HMTTCR").equalsIgnoreCase(""))
            || (!lexique.HostFieldGetData("BMTTDB").equalsIgnoreCase("")) || (!lexique.HostFieldGetData("BMTTCR").equalsIgnoreCase("")))) {
      lexique.HostScreenSendKey(this, "ENTER");
      // sinon on fait F3
    }
    else {
      lexique.HostScreenSendKey(this, "F3");
    }
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_24ActionPerformed(ActionEvent e) {
    WTW01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_25ActionPerformed(ActionEvent e) {
    WTW01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_26ActionPerformed(ActionEvent e) {
    WTW01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_29ActionPerformed(ActionEvent e) {
    ZTW01.setValeurTop("+");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_30ActionPerformed(ActionEvent e) {
    ZTW01.setValeurTop("-");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_31ActionPerformed(ActionEvent e) {
    ZTW01.setValeurTop("D");
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void OBJ_32ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BT2.getInvoker().getName());
  }
  
  private void WTW01MouseClicked(MouseEvent e) {
    if (WTW01.doubleClicSelection(e)) {
      if (lexique.isTrue(String.valueOf(WTW01.getSelectedRow() + 21))) {
        WTW01.setValeurTop("-");
      }
      else {
        WTW01.setValeurTop("+");
      }
      lexique.HostScreenSendKey(this, "ENTER");
    }
    
  }
  
  private void ZTW01MouseClicked(MouseEvent e) {
    if (ZTW01.doubleClicSelection(e)) {
      if (lexique.isTrue(String.valueOf(ZTW01.getSelectedRow() + 53))) {
        ZTW01.setValeurTop("-");
      }
      else {
        ZTW01.setValeurTop("+");
      }
      
      lexique.HostScreenSendKey(this, "ENTER");
    }
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 2);
    lexique.HostScreenSendKey(this, "PGUP");
    
  }
  
  private void OBJ_71ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(4, 2);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 2);
    lexique.HostScreenSendKey(this, "PGUP");
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 2);
    lexique.HostScreenSendKey(this, "PGDOWN");
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_80 = new JLabel();
    WISOC = new XRiTextField();
    WIJOL = new XRiTextField();
    OBJ_79 = new JLabel();
    WIDAT = new XRiTextField();
    WIREF = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu4 = new RiMenu();
    riMenu_bt4 = new RiMenu_bt();
    riSousMenu18 = new RiSousMenu();
    riSousMenu_bt18 = new RiSousMenu_bt();
    riSousMenu19 = new RiSousMenu();
    riSousMenu_bt19 = new RiSousMenu_bt();
    riSousMenu20 = new RiSousMenu();
    riSousMenu_bt20 = new RiSousMenu_bt();
    riSousMenu21 = new RiSousMenu();
    riSousMenu_bt21 = new RiSousMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    SCROLLPANE_LIST8 = new JScrollPane();
    WTW01 = new XRiTable();
    WZ2B = new RiZoneSortie();
    WZ1B = new RiZoneSortie();
    SCROLLPANE_LIST9 = new JScrollPane();
    ZTW01 = new XRiTable();
    OBJ_62 = new JButton();
    OBJ_71 = new JButton();
    OBJ_63 = new JButton();
    OBJ_72 = new JButton();
    panel4 = new JPanel();
    OBJ_65 = new JLabel();
    HMTTDB = new RiZoneSortie();
    HMTTCR = new RiZoneSortie();
    BMTTCR = new RiZoneSortie();
    BMTTDB = new RiZoneSortie();
    OBJ_70 = new JLabel();
    OBJ_68 = new JLabel();
    RMTTD = new RiZoneSortie();
    label1 = new JLabel();
    label2 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_24 = new JMenuItem();
    OBJ_25 = new JMenuItem();
    menuItem1 = new JMenuItem();
    OBJ_26 = new JMenuItem();
    BT2 = new JPopupMenu();
    OBJ_29 = new JMenuItem();
    OBJ_30 = new JMenuItem();
    OBJ_31 = new JMenuItem();
    OBJ_32 = new JMenuItem();
    WZ3B = new XRiTextField();
    WZ4B = new XRiTextField();
    WZ5B = new XRiTextField();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Rapprochement bancaire");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 40));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_80 ----
          OBJ_80.setText("Soci\u00e9t\u00e9");
          OBJ_80.setName("OBJ_80");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(null);
          WISOC.setName("WISOC");

          //---- WIJOL ----
          WIJOL.setComponentPopupMenu(null);
          WIJOL.setName("WIJOL");

          //---- OBJ_79 ----
          OBJ_79.setText("du");
          OBJ_79.setName("OBJ_79");

          //---- WIDAT ----
          WIDAT.setComponentPopupMenu(null);
          WIDAT.setName("WIDAT");

          //---- WIREF ----
          WIREF.setComponentPopupMenu(null);
          WIREF.setName("WIREF");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WIJOL, GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WIDAT, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(44, 44, 44)
                .addComponent(WIREF, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIJOL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDAT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIREF, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_80, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_79, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Recherche d'\u00e9critures");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");

              //---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Changement date limite");
              riSousMenu_bt15.setToolTipText("Changement de la date limite de rapprochement");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);

            //======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");

              //---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Rappro. par montants");
              riSousMenu_bt8.setToolTipText("Demande de rapprochement par montants");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);

            //======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");

              //---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Ecritures comptables");
              riSousMenu_bt9.setToolTipText("Acc\u00e8s \u00e0 la saisie des \u00e9critures comptables");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);

            //======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");

              //---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Saisie tr\u00e9sorerie");
              riSousMenu_bt10.setToolTipText("Acc\u00e8s \u00e0 la saisie de tr\u00e9sorerie (mouvements divers)");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);

            //======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");

              //---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("Saisie r\u00e9duite \u00e9critures");
              riSousMenu_bt11.setToolTipText("Saisie r\u00e9duite des \u00e9critures comptables");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);

            //======== riMenu4 ========
            {
              riMenu4.setName("riMenu4");

              //---- riMenu_bt4 ----
              riMenu_bt4.setText("Affichage");
              riMenu_bt4.setName("riMenu_bt4");
              riMenu4.add(riMenu_bt4);
            }
            menus_haut.add(riMenu4);

            //======== riSousMenu18 ========
            {
              riSousMenu18.setName("riSousMenu18");

              //---- riSousMenu_bt18 ----
              riSousMenu_bt18.setText("Ecritures par dates");
              riSousMenu_bt18.setToolTipText("Affichage des \u00e9critures ( en bas) par dates");
              riSousMenu_bt18.setName("riSousMenu_bt18");
              riSousMenu_bt18.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt18ActionPerformed(e);
                }
              });
              riSousMenu18.add(riSousMenu_bt18);
            }
            menus_haut.add(riSousMenu18);

            //======== riSousMenu19 ========
            {
              riSousMenu19.setName("riSousMenu19");

              //---- riSousMenu_bt19 ----
              riSousMenu_bt19.setText("Ecritures par montants");
              riSousMenu_bt19.setToolTipText("Affichage des \u00e9critures (en bas) par montants");
              riSousMenu_bt19.setName("riSousMenu_bt19");
              riSousMenu_bt19.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt19ActionPerformed(e);
                }
              });
              riSousMenu19.add(riSousMenu_bt19);
            }
            menus_haut.add(riSousMenu19);

            //======== riSousMenu20 ========
            {
              riSousMenu20.setName("riSousMenu20");

              //---- riSousMenu_bt20 ----
              riSousMenu_bt20.setText("Ecritures par pi\u00e8ces");
              riSousMenu_bt20.setToolTipText("Affichage des \u00e9critures (en bas) par pi\u00e8ces");
              riSousMenu_bt20.setName("riSousMenu_bt20");
              riSousMenu_bt20.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt20ActionPerformed(e);
                }
              });
              riSousMenu20.add(riSousMenu_bt20);
            }
            menus_haut.add(riSousMenu20);

            //======== riSousMenu21 ========
            {
              riSousMenu21.setName("riSousMenu21");

              //---- riSousMenu_bt21 ----
              riSousMenu_bt21.setText("Ecritures rapproch\u00e9es");
              riSousMenu_bt21.setToolTipText("On / Off Affichage des \u00e9critures rapproch\u00e9es");
              riSousMenu_bt21.setName("riSousMenu_bt21");
              riSousMenu_bt21.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt21ActionPerformed(e);
                }
              });
              riSousMenu21.add(riSousMenu_bt21);
            }
            menus_haut.add(riSousMenu21);

            //======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");

              //---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Lib\u00e9ll\u00e9s compl\u00e9mentaires");
              riSousMenu_bt14.setToolTipText("Affichage des lib\u00e9ll\u00e9s compl\u00e9mentaires");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(1000, 580));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder(""));
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //======== SCROLLPANE_LIST8 ========
            {
              SCROLLPANE_LIST8.setComponentPopupMenu(BTD);
              SCROLLPANE_LIST8.setName("SCROLLPANE_LIST8");

              //---- WTW01 ----
              WTW01.setPreferredSize(new Dimension(541, 224));
              WTW01.setComponentPopupMenu(BTD);
              WTW01.setName("WTW01");
              WTW01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  WTW01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST8.setViewportView(WTW01);
            }

            //---- WZ2B ----
            WZ2B.setText("@WZ2B@");
            WZ2B.setName("WZ2B");

            //---- WZ1B ----
            WZ1B.setText("@WZ1B@");
            WZ1B.setName("WZ1B");

            //======== SCROLLPANE_LIST9 ========
            {
              SCROLLPANE_LIST9.setComponentPopupMenu(BT2);
              SCROLLPANE_LIST9.setName("SCROLLPANE_LIST9");

              //---- ZTW01 ----
              ZTW01.setComponentPopupMenu(BT2);
              ZTW01.setName("ZTW01");
              ZTW01.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  ZTW01MouseClicked(e);
                }
              });
              SCROLLPANE_LIST9.setViewportView(ZTW01);
            }

            //---- OBJ_62 ----
            OBJ_62.setText("");
            OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_62.setName("OBJ_62");
            OBJ_62.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_62ActionPerformed(e);
              }
            });

            //---- OBJ_71 ----
            OBJ_71.setText("");
            OBJ_71.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_71.setName("OBJ_71");
            OBJ_71.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_71ActionPerformed(e);
              }
            });

            //---- OBJ_63 ----
            OBJ_63.setText("");
            OBJ_63.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_63.setName("OBJ_63");
            OBJ_63.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_63ActionPerformed(e);
              }
            });

            //---- OBJ_72 ----
            OBJ_72.setText("");
            OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_72.setName("OBJ_72");
            OBJ_72.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_72ActionPerformed(e);
              }
            });

            //======== panel4 ========
            {
              panel4.setBorder(new TitledBorder("Equilibre"));
              panel4.setOpaque(false);
              panel4.setName("panel4");

              //---- OBJ_65 ----
              OBJ_65.setText("Bordereau");
              OBJ_65.setName("OBJ_65");

              //---- HMTTDB ----
              HMTTDB.setBorder(new BevelBorder(BevelBorder.LOWERED));
              HMTTDB.setText("@HMTTDB@");
              HMTTDB.setHorizontalAlignment(SwingConstants.RIGHT);
              HMTTDB.setName("HMTTDB");

              //---- HMTTCR ----
              HMTTCR.setBorder(new BevelBorder(BevelBorder.LOWERED));
              HMTTCR.setHorizontalAlignment(SwingConstants.RIGHT);
              HMTTCR.setText("@HMTTCR@");
              HMTTCR.setName("HMTTCR");

              //---- BMTTCR ----
              BMTTCR.setBorder(new BevelBorder(BevelBorder.LOWERED));
              BMTTCR.setHorizontalAlignment(SwingConstants.RIGHT);
              BMTTCR.setText("@BMTTCR@");
              BMTTCR.setName("BMTTCR");

              //---- BMTTDB ----
              BMTTDB.setBorder(new BevelBorder(BevelBorder.LOWERED));
              BMTTDB.setHorizontalAlignment(SwingConstants.RIGHT);
              BMTTDB.setText("@BMTTDB@");
              BMTTDB.setName("BMTTDB");

              //---- OBJ_70 ----
              OBJ_70.setText("Diff\u00e9rence");
              OBJ_70.setName("OBJ_70");

              //---- OBJ_68 ----
              OBJ_68.setText("Ecritures");
              OBJ_68.setName("OBJ_68");

              //---- RMTTD ----
              RMTTD.setBorder(new BevelBorder(BevelBorder.LOWERED));
              RMTTD.setHorizontalAlignment(SwingConstants.RIGHT);
              RMTTD.setText("@RMTTD@");
              RMTTD.setForeground(new Color(255, 0, 51));
              RMTTD.setFont(RMTTD.getFont().deriveFont(RMTTD.getFont().getStyle() | Font.BOLD));
              RMTTD.setName("RMTTD");

              //---- label1 ----
              label1.setText("d\u00e9bit");
              label1.setHorizontalAlignment(SwingConstants.CENTER);
              label1.setFont(label1.getFont().deriveFont(label1.getFont().getStyle() | Font.BOLD));
              label1.setName("label1");

              //---- label2 ----
              label2.setText("cr\u00e9dit");
              label2.setHorizontalAlignment(SwingConstants.CENTER);
              label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
              label2.setName("label2");

              GroupLayout panel4Layout = new GroupLayout(panel4);
              panel4.setLayout(panel4Layout);
              panel4Layout.setHorizontalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(74, 74, 74)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                    .addGap(3, 3, 3)
                    .addComponent(HMTTDB, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(HMTTCR, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                    .addGap(5, 5, 5)
                    .addComponent(BMTTDB, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE)
                    .addGap(7, 7, 7)
                    .addComponent(BMTTCR, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(4, 4, 4)
                    .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 85, GroupLayout.PREFERRED_SIZE)
                    .addGap(100, 100, 100)
                    .addComponent(RMTTD, GroupLayout.PREFERRED_SIZE, 108, GroupLayout.PREFERRED_SIZE))
              );
              panel4Layout.setVerticalGroup(
                panel4Layout.createParallelGroup()
                  .addGroup(panel4Layout.createSequentialGroup()
                    .addGap(20, 20, 20)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addComponent(label1)
                      .addComponent(label2))
                    .addGap(4, 4, 4)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_65, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(HMTTDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(HMTTCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(11, 11, 11)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(BMTTDB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(BMTTCR, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                    .addGap(11, 11, 11)
                    .addGroup(panel4Layout.createParallelGroup()
                      .addGroup(panel4Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(OBJ_70, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(RMTTD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
              );
            }

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(14, 14, 14)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(SCROLLPANE_LIST8, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addComponent(WZ1B, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)
                      .addGap(10, 10, 10)
                      .addComponent(WZ2B, GroupLayout.PREFERRED_SIZE, 390, GroupLayout.PREFERRED_SIZE))
                    .addComponent(SCROLLPANE_LIST9, GroupLayout.PREFERRED_SIZE, 575, GroupLayout.PREFERRED_SIZE))
                  .addGap(5, 5, 5)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE)
                    .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                  .addGap(10, 10, 10)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addComponent(SCROLLPANE_LIST8, GroupLayout.PREFERRED_SIZE, 255, GroupLayout.PREFERRED_SIZE)
                  .addGap(5, 5, 5)
                  .addGroup(panel2Layout.createParallelGroup()
                    .addComponent(WZ1B, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                    .addComponent(WZ2B, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                  .addGap(6, 6, 6)
                  .addComponent(SCROLLPANE_LIST9, GroupLayout.PREFERRED_SIZE, 240, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(9, 9, 9)
                  .addComponent(OBJ_62, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(20, 20, 20)
                  .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 115, GroupLayout.PREFERRED_SIZE)
                  .addGap(40, 40, 40)
                  .addComponent(OBJ_63, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE)
                  .addGap(15, 15, 15)
                  .addComponent(OBJ_72, GroupLayout.PREFERRED_SIZE, 110, GroupLayout.PREFERRED_SIZE))
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGap(189, 189, 189)
                  .addComponent(panel4, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
            );
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_24 ----
      OBJ_24.setText("S\u00e9lection");
      OBJ_24.setName("OBJ_24");
      OBJ_24.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_24ActionPerformed(e);
        }
      });
      BTD.add(OBJ_24);

      //---- OBJ_25 ----
      OBJ_25.setText("Annuler la s\u00e9lection");
      OBJ_25.setName("OBJ_25");
      OBJ_25.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_25ActionPerformed(e);
        }
      });
      BTD.add(OBJ_25);

      //---- menuItem1 ----
      menuItem1.setText("D\u00e9coupage/\u00e9critures");
      menuItem1.setName("menuItem1");
      BTD.add(menuItem1);
      BTD.addSeparator();

      //---- OBJ_26 ----
      OBJ_26.setText("Aide en ligne");
      OBJ_26.setName("OBJ_26");
      OBJ_26.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_26ActionPerformed(e);
        }
      });
      BTD.add(OBJ_26);
    }

    //======== BT2 ========
    {
      BT2.setName("BT2");

      //---- OBJ_29 ----
      OBJ_29.setText("S\u00e9lection");
      OBJ_29.setName("OBJ_29");
      OBJ_29.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_29ActionPerformed(e);
        }
      });
      BT2.add(OBJ_29);

      //---- OBJ_30 ----
      OBJ_30.setText("Annuler la s\u00e9lection");
      OBJ_30.setName("OBJ_30");
      OBJ_30.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_30ActionPerformed(e);
        }
      });
      BT2.add(OBJ_30);

      //---- OBJ_31 ----
      OBJ_31.setText("D\u00e9coupage/\u00e9critures");
      OBJ_31.setName("OBJ_31");
      OBJ_31.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_31ActionPerformed(e);
        }
      });
      BT2.add(OBJ_31);
      BT2.addSeparator();

      //---- OBJ_32 ----
      OBJ_32.setText("Aide en ligne");
      OBJ_32.setName("OBJ_32");
      OBJ_32.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_32ActionPerformed(e);
        }
      });
      BT2.add(OBJ_32);
    }

    //---- WZ3B ----
    WZ3B.setName("WZ3B");

    //---- WZ4B ----
    WZ4B.setName("WZ4B");

    //---- WZ5B ----
    WZ5B.setName("WZ5B");

    //======== riSousMenu7 ========
    {
      riSousMenu7.setName("riSousMenu7");

      //---- riSousMenu_bt7 ----
      riSousMenu_bt7.setText("Edition \u00e9tat rappro.");
      riSousMenu_bt7.setToolTipText("Edition de l'\u00e9tat de rapprochement");
      riSousMenu_bt7.setName("riSousMenu_bt7");
      riSousMenu_bt7.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          riSousMenu_bt7ActionPerformed(e);
        }
      });
      riSousMenu7.add(riSousMenu_bt7);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_80;
  private XRiTextField WISOC;
  private XRiTextField WIJOL;
  private JLabel OBJ_79;
  private XRiTextField WIDAT;
  private XRiTextField WIREF;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu4;
  private RiMenu_bt riMenu_bt4;
  private RiSousMenu riSousMenu18;
  private RiSousMenu_bt riSousMenu_bt18;
  private RiSousMenu riSousMenu19;
  private RiSousMenu_bt riSousMenu_bt19;
  private RiSousMenu riSousMenu20;
  private RiSousMenu_bt riSousMenu_bt20;
  private RiSousMenu riSousMenu21;
  private RiSousMenu_bt riSousMenu_bt21;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JScrollPane SCROLLPANE_LIST8;
  private XRiTable WTW01;
  private RiZoneSortie WZ2B;
  private RiZoneSortie WZ1B;
  private JScrollPane SCROLLPANE_LIST9;
  private XRiTable ZTW01;
  private JButton OBJ_62;
  private JButton OBJ_71;
  private JButton OBJ_63;
  private JButton OBJ_72;
  private JPanel panel4;
  private JLabel OBJ_65;
  private RiZoneSortie HMTTDB;
  private RiZoneSortie HMTTCR;
  private RiZoneSortie BMTTCR;
  private RiZoneSortie BMTTDB;
  private JLabel OBJ_70;
  private JLabel OBJ_68;
  private RiZoneSortie RMTTD;
  private JLabel label1;
  private JLabel label2;
  private JPopupMenu BTD;
  private JMenuItem OBJ_24;
  private JMenuItem OBJ_25;
  private JMenuItem menuItem1;
  private JMenuItem OBJ_26;
  private JPopupMenu BT2;
  private JMenuItem OBJ_29;
  private JMenuItem OBJ_30;
  private JMenuItem OBJ_31;
  private JMenuItem OBJ_32;
  private XRiTextField WZ3B;
  private XRiTextField WZ4B;
  private XRiTextField WZ5B;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
