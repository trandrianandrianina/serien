
package ri.serien.libecranrpg.vgtm.VGTM57FM;
// Nom Fichier: pop_VCGM54FM_FMTB3_FMTF1_595.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGTM57FM_L4 extends SNPanelEcranRPG implements ioFrame {
  
   
  private String[] _LA01_Top =
      { "LA01", "LA02", "LA03", "LA04", "LA05", "LA06", "LA07", "LA08", "LA09", "LA10", "LA11", "LA12", "LA13", "LA14", "LA15", "LA16", };
  private String[] _LA01_Title = { "Date  N°Pièce Libellé        Mt Facture Mtt. Réglé    Solde   Aff", };
  private String[][] _LA01_Data = { { "LA01", }, { "LA02", }, { "LA03", }, { "LA04", }, { "LA05", }, { "LA06", }, { "LA07", }, { "LA08", },
      { "LA09", }, { "LA10", }, { "LA11", }, { "LA12", }, { "LA13", }, { "LA14", }, { "LA15", }, { "LA16", }, };
  private int[] _LA01_Width = { 474, };
  
  public VGTM57FM_L4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LA01.setAspectTable(_LA01_Top, _LA01_Title, _LA01_Data, _LA01_Width, false, null, null, null, null);
    
    setTitle("Règlements");
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDIFE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIFE@")).trim());
    TOTAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTAFF@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA02@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA03@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA04@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA05@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA06@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA07@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA08@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA09@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA10@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA11@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA12@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA13@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA14@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA15@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA16@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Ajoute à la liste des oData les variables non liées directement à un composant graphique
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LA01_Top);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    
    OBJ_75.setVisible(interpreteurD.analyseExpression("@LA16@").contains(">"));
    OBJ_74.setVisible(interpreteurD.analyseExpression("@LA15@").contains(">"));
    OBJ_73.setVisible(interpreteurD.analyseExpression("@LA14@").contains(">"));
    OBJ_72.setVisible(interpreteurD.analyseExpression("@LA13@").contains(">"));
    OBJ_71.setVisible(interpreteurD.analyseExpression("@LA12@").contains(">"));
    OBJ_70.setVisible(interpreteurD.analyseExpression("@LA11@").contains(">"));
    OBJ_69.setVisible(interpreteurD.analyseExpression("@LA10@").contains(">"));
    OBJ_68.setVisible(interpreteurD.analyseExpression("@LA09@").contains(">"));
    OBJ_67.setVisible(interpreteurD.analyseExpression("@LA08@").contains(">"));
    OBJ_66.setVisible(interpreteurD.analyseExpression("@LA07@").contains(">"));
    OBJ_65.setVisible(interpreteurD.analyseExpression("@LA06@").contains(">"));
    OBJ_64.setVisible(interpreteurD.analyseExpression("@LA05@").contains(">"));
    OBJ_63.setVisible(interpreteurD.analyseExpression("@LA04@").contains(">"));
    OBJ_62.setVisible(interpreteurD.analyseExpression("@LA03@").contains(">"));
    OBJ_61.setVisible(interpreteurD.analyseExpression("@LA02@").contains(">"));
    OBJ_60.setVisible(interpreteurD.analyseExpression("@LA01@").contains(">"));
    
    // texte des boutons de duplication
    if (MTA01.getText().trim().equals("")) {
      OBJ_60.setText(">");
    }
    else {
      OBJ_60.setText("<");
    }
    if (MTA02.getText().trim().equals("")) {
      OBJ_61.setText(">");
    }
    else {
      OBJ_61.setText("<");
    }
    if (MTA03.getText().trim().equals("")) {
      OBJ_62.setText(">");
    }
    else {
      OBJ_62.setText("<");
    }
    if (MTA04.getText().trim().equals("")) {
      OBJ_63.setText(">");
    }
    else {
      OBJ_63.setText("<");
    }
    if (MTA05.getText().trim().equals("")) {
      OBJ_64.setText(">");
    }
    else {
      OBJ_64.setText("<");
    }
    if (MTA06.getText().trim().equals("")) {
      OBJ_65.setText(">");
    }
    else {
      OBJ_65.setText("<");
    }
    if (MTA07.getText().trim().equals("")) {
      OBJ_66.setText(">");
    }
    else {
      OBJ_66.setText("<");
    }
    if (MTA08.getText().trim().equals("")) {
      OBJ_67.setText(">");
    }
    else {
      OBJ_67.setText("<");
    }
    if (MTA09.getText().trim().equals("")) {
      OBJ_68.setText(">");
    }
    else {
      OBJ_68.setText("<");
    }
    if (MTA10.getText().trim().equals("")) {
      OBJ_69.setText(">");
    }
    else {
      OBJ_69.setText("<");
    }
    if (MTA11.getText().trim().equals("")) {
      OBJ_70.setText(">");
    }
    else {
      OBJ_70.setText("<");
    }
    if (MTA12.getText().trim().equals("")) {
      OBJ_71.setText(">");
    }
    else {
      OBJ_71.setText("<");
    }
    if (MTA13.getText().trim().equals("")) {
      OBJ_72.setText(">");
    }
    else {
      OBJ_72.setText("<");
    }
    if (MTA14.getText().trim().equals("")) {
      OBJ_73.setText(">");
    }
    else {
      OBJ_73.setText("<");
    }
    if (MTA15.getText().trim().equals("")) {
      OBJ_74.setText(">");
    }
    else {
      OBJ_74.setText("<");
    }
    if (MTA16.getText().trim().equals("")) {
      OBJ_75.setText(">");
    }
    else {
      OBJ_75.setText("<");
    }
    
    WDIFE.setVisible(lexique.isTrue("31"));
    OBJ_690.setVisible(WDIFE.isVisible());
    
    if (lexique.isTrue("80")) {
      riSousMenu_bt9.setText("Fin d'affection");
      riSousMenu_bt9.setToolTipText("Fin d'affection");
    }
    else {
      riSousMenu_bt9.setText("Acceptation différence");
      riSousMenu_bt9.setToolTipText("Acceptation différence");
    }
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_60ActionPerformed(ActionEvent e) {
    if (MTA01.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA01");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_60.setText("<");
    }
    else {
      MTA01.setText("");
      OBJ_60.setText(">");
    }
  }
  
  private void OBJ_61ActionPerformed(ActionEvent e) {
    if (MTA02.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA02");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_61.setText("<");
    }
    else {
      MTA02.setText("");
      OBJ_61.setText(">");
    }
  }
  
  private void OBJ_62ActionPerformed(ActionEvent e) {
    if (MTA03.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA03");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_62.setText("<");
    }
    else {
      MTA03.setText("");
      OBJ_62.setText(">");
    }
  }
  
  private void OBJ_63ActionPerformed(ActionEvent e) {
    if (MTA04.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA04");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_63.setText("<");
    }
    else {
      MTA04.setText("");
      OBJ_63.setText(">");
    }
  }
  
  private void OBJ_64ActionPerformed(ActionEvent e) {
    if (MTA05.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA05");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_64.setText("<");
    }
    else {
      MTA05.setText("");
      OBJ_64.setText(">");
    }
  }
  
  private void OBJ_65ActionPerformed(ActionEvent e) {
    if (MTA06.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA06");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_65.setText("<");
    }
    else {
      MTA06.setText("");
      OBJ_65.setText(">");
    }
  }
  
  private void OBJ_66ActionPerformed(ActionEvent e) {
    if (MTA07.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA07");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_66.setText("<");
    }
    else {
      MTA07.setText("");
      OBJ_66.setText(">");
    }
  }
  
  private void OBJ_67ActionPerformed(ActionEvent e) {
    if (MTA08.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA08");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_67.setText("<");
    }
    else {
      MTA08.setText("");
      OBJ_67.setText(">");
    }
  }
  
  private void OBJ_68ActionPerformed(ActionEvent e) {
    if (MTA09.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA09");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_68.setText("<");
    }
    else {
      MTA09.setText("");
      OBJ_68.setText(">");
    }
  }
  
  private void OBJ_69ActionPerformed(ActionEvent e) {
    if (MTA10.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA10");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_69.setText("<");
    }
    else {
      MTA10.setText("");
      OBJ_69.setText(">");
    }
  }
  
  private void OBJ_70ActionPerformed(ActionEvent e) {
    if (MTA11.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA11");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_70.setText("<");
    }
    else {
      MTA11.setText("");
      OBJ_70.setText(">");
    }
  }
  
  private void OBJ_71ActionPerformed(ActionEvent e) {
    if (MTA12.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA12");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_71.setText("<");
    }
    else {
      MTA12.setText("");
      OBJ_71.setText(">");
    }
  }
  
  private void OBJ_72ActionPerformed(ActionEvent e) {
    if (MTA13.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA13");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_72.setText("<");
    }
    else {
      MTA13.setText("");
      OBJ_72.setText(">");
    }
  }
  
  private void OBJ_73ActionPerformed(ActionEvent e) {
    if (MTA14.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA14");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_73.setText("<");
    }
    else {
      MTA14.setText("");
      OBJ_73.setText(">");
    }
  }
  
  private void OBJ_74ActionPerformed(ActionEvent e) {
    if (MTA15.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA15");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_74.setText("<");
    }
    else {
      MTA15.setText("");
      OBJ_74.setText(">");
    }
  }
  
  private void OBJ_75ActionPerformed(ActionEvent e) {
    if (MTA16.getText().trim().equals("")) {
      lexique.HostCursorPut("MTA16");
      lexique.HostScreenSendKey(this, "F4", true);
      OBJ_75.setText("<");
    }
    else {
      MTA16.setText("");
      OBJ_75.setText(">");
    }
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    // Script original (à supprimer)
    // touche="DUPLICATE"
    // ScriptCall("G_TOUCHE")
    lexique.HostScreenSendKey(this, "DUPLICATE", true);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER", true);
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", true);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11", true);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    WDIFE = new RiZoneSortie();
    TOTAFF = new RiZoneSortie();
    OBJ_690 = new JLabel();
    SCROLLPANE_LIST3 = new JScrollPane();
    LA01 = new XRiTable();
    MTA01 = new XRiTextField();
    MTA02 = new XRiTextField();
    MTA03 = new XRiTextField();
    MTA04 = new XRiTextField();
    MTA05 = new XRiTextField();
    MTA06 = new XRiTextField();
    MTA07 = new XRiTextField();
    MTA08 = new XRiTextField();
    MTA09 = new XRiTextField();
    MTA10 = new XRiTextField();
    MTA16 = new XRiTextField();
    MTA14 = new XRiTextField();
    MTA15 = new XRiTextField();
    MTA13 = new XRiTextField();
    MTA12 = new XRiTextField();
    MTA11 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label1 = new JLabel();
    label18 = new JLabel();
    panel2 = new JPanel();
    OBJ_60 = new JButton();
    OBJ_61 = new JButton();
    OBJ_62 = new JButton();
    OBJ_63 = new JButton();
    OBJ_64 = new JButton();
    OBJ_65 = new JButton();
    OBJ_66 = new JButton();
    OBJ_67 = new JButton();
    OBJ_68 = new JButton();
    OBJ_69 = new JButton();
    OBJ_70 = new JButton();
    OBJ_71 = new JButton();
    OBJ_72 = new JButton();
    OBJ_73 = new JButton();
    OBJ_74 = new JButton();
    OBJ_75 = new JButton();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    BTD = new JPopupMenu();
    OBJ_15 = new JMenuItem();
    OBJ_16 = new JMenuItem();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(945, 440));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu_V01F ========
          {
            riMenu_V01F.setMinimumSize(new Dimension(104, 50));
            riMenu_V01F.setPreferredSize(new Dimension(170, 50));
            riMenu_V01F.setMaximumSize(new Dimension(104, 50));
            riMenu_V01F.setName("riMenu_V01F");

            //---- riMenu_bt_V01F ----
            riMenu_bt_V01F.setText("Affectation");
            riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
            riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
            riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
            riMenu_bt_V01F.setName("riMenu_bt_V01F");
            riMenu_V01F.add(riMenu_bt_V01F);
          }
          menus_haut.add(riMenu_V01F);

          //======== riSousMenu_consult ========
          {
            riSousMenu_consult.setName("riSousMenu_consult");

            //---- riSousMenu_bt_consult ----
            riSousMenu_bt_consult.setText("Consultation");
            riSousMenu_bt_consult.setToolTipText("Consultation");
            riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
            riSousMenu_consult.add(riSousMenu_bt_consult);
          }
          menus_haut.add(riSousMenu_consult);

          //======== riSousMenu_modif ========
          {
            riSousMenu_modif.setName("riSousMenu_modif");

            //---- riSousMenu_bt_modif ----
            riSousMenu_bt_modif.setText("Modification");
            riSousMenu_bt_modif.setToolTipText("Modification");
            riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
            riSousMenu_modif.add(riSousMenu_bt_modif);
          }
          menus_haut.add(riSousMenu_modif);

          //======== riSousMenu_crea ========
          {
            riSousMenu_crea.setName("riSousMenu_crea");

            //---- riSousMenu_bt_crea ----
            riSousMenu_bt_crea.setText("Cr\u00e9ation");
            riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
            riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
            riSousMenu_crea.add(riSousMenu_bt_crea);
          }
          menus_haut.add(riSousMenu_crea);

          //======== riSousMenu_suppr ========
          {
            riSousMenu_suppr.setName("riSousMenu_suppr");

            //---- riSousMenu_bt_suppr ----
            riSousMenu_bt_suppr.setText("Annulation");
            riSousMenu_bt_suppr.setToolTipText("Annulation");
            riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
            riSousMenu_suppr.add(riSousMenu_bt_suppr);
          }
          menus_haut.add(riSousMenu_suppr);

          //======== riSousMenuF_dupli ========
          {
            riSousMenuF_dupli.setName("riSousMenuF_dupli");

            //---- riSousMenu_bt_dupli ----
            riSousMenu_bt_dupli.setText("Duplication");
            riSousMenu_bt_dupli.setToolTipText("Duplication");
            riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
            riSousMenuF_dupli.add(riSousMenu_bt_dupli);
          }
          menus_haut.add(riSousMenuF_dupli);

          //======== riSousMenu_rappel ========
          {
            riSousMenu_rappel.setName("riSousMenu_rappel");

            //---- riSousMenu_bt_rappel ----
            riSousMenu_bt_rappel.setText("Rappel");
            riSousMenu_bt_rappel.setToolTipText("Rappel");
            riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
            riSousMenu_rappel.add(riSousMenu_bt_rappel);
          }
          menus_haut.add(riSousMenu_rappel);

          //======== riSousMenu_reac ========
          {
            riSousMenu_reac.setName("riSousMenu_reac");

            //---- riSousMenu_bt_reac ----
            riSousMenu_bt_reac.setText("R\u00e9activation");
            riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
            riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
            riSousMenu_reac.add(riSousMenu_bt_reac);
          }
          menus_haut.add(riSousMenu_reac);

          //======== riSousMenu_destr ========
          {
            riSousMenu_destr.setName("riSousMenu_destr");

            //---- riSousMenu_bt_destr ----
            riSousMenu_bt_destr.setText("Suppression");
            riSousMenu_bt_destr.setToolTipText("Suppression");
            riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
            riSousMenu_destr.add(riSousMenu_bt_destr);
          }
          menus_haut.add(riSousMenu_destr);

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");

            //---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Diff\u00e9rence");
            riSousMenu_bt9.setToolTipText("Diff\u00e9rence");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //---- WDIFE ----
        WDIFE.setText("@WDIFE@");
        WDIFE.setHorizontalAlignment(SwingConstants.RIGHT);
        WDIFE.setName("WDIFE");
        p_contenu.add(WDIFE);
        WDIFE.setBounds(495, 385, 100, WDIFE.getPreferredSize().height);

        //---- TOTAFF ----
        TOTAFF.setText("@TOTAFF@");
        TOTAFF.setHorizontalAlignment(SwingConstants.RIGHT);
        TOTAFF.setName("TOTAFF");
        p_contenu.add(TOTAFF);
        TOTAFF.setBounds(600, 385, 100, TOTAFF.getPreferredSize().height);

        //---- OBJ_690 ----
        OBJ_690.setText("Ecart");
        OBJ_690.setName("OBJ_690");
        p_contenu.add(OBJ_690);
        OBJ_690.setBounds(435, 387, 60, 20);

        //======== SCROLLPANE_LIST3 ========
        {
          SCROLLPANE_LIST3.setComponentPopupMenu(BTD);
          SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

          //---- LA01 ----
          LA01.setRowHeight(20);
          LA01.setRowSelectionAllowed(false);
          LA01.setName("LA01");
          SCROLLPANE_LIST3.setViewportView(LA01);
        }
        p_contenu.add(SCROLLPANE_LIST3);
        SCROLLPANE_LIST3.setBounds(20, 20, 515, 350);

        //---- MTA01 ----
        MTA01.setName("MTA01");
        p_contenu.add(MTA01);
        MTA01.setBounds(600, 43, 100, 25);

        //---- MTA02 ----
        MTA02.setName("MTA02");
        p_contenu.add(MTA02);
        MTA02.setBounds(600, 63, 100, 25);

        //---- MTA03 ----
        MTA03.setName("MTA03");
        p_contenu.add(MTA03);
        MTA03.setBounds(600, 83, 100, 25);

        //---- MTA04 ----
        MTA04.setName("MTA04");
        p_contenu.add(MTA04);
        MTA04.setBounds(600, 103, 100, 25);

        //---- MTA05 ----
        MTA05.setName("MTA05");
        p_contenu.add(MTA05);
        MTA05.setBounds(600, 123, 100, 25);

        //---- MTA06 ----
        MTA06.setName("MTA06");
        p_contenu.add(MTA06);
        MTA06.setBounds(600, 143, 100, 25);

        //---- MTA07 ----
        MTA07.setName("MTA07");
        p_contenu.add(MTA07);
        MTA07.setBounds(600, 163, 100, 25);

        //---- MTA08 ----
        MTA08.setName("MTA08");
        p_contenu.add(MTA08);
        MTA08.setBounds(600, 183, 100, 25);

        //---- MTA09 ----
        MTA09.setName("MTA09");
        p_contenu.add(MTA09);
        MTA09.setBounds(600, 203, 100, 25);

        //---- MTA10 ----
        MTA10.setName("MTA10");
        p_contenu.add(MTA10);
        MTA10.setBounds(600, 223, 100, 25);

        //---- MTA16 ----
        MTA16.setName("MTA16");
        p_contenu.add(MTA16);
        MTA16.setBounds(600, 343, 100, 25);

        //---- MTA14 ----
        MTA14.setName("MTA14");
        p_contenu.add(MTA14);
        MTA14.setBounds(600, 303, 100, 25);

        //---- MTA15 ----
        MTA15.setName("MTA15");
        p_contenu.add(MTA15);
        MTA15.setBounds(600, 323, 100, 25);

        //---- MTA13 ----
        MTA13.setName("MTA13");
        p_contenu.add(MTA13);
        MTA13.setBounds(600, 283, 100, 25);

        //---- MTA12 ----
        MTA12.setName("MTA12");
        p_contenu.add(MTA12);
        MTA12.setBounds(600, 263, 100, 25);

        //---- MTA11 ----
        MTA11.setName("MTA11");
        p_contenu.add(MTA11);
        MTA11.setBounds(600, 243, 100, 25);

        //---- label2 ----
        label2.setText("@ERA01@");
        label2.setHorizontalAlignment(SwingConstants.CENTER);
        label2.setForeground(new Color(204, 0, 0));
        label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
        label2.setName("label2");
        p_contenu.add(label2);
        label2.setBounds(705, 40, 20, 25);

        //---- label3 ----
        label3.setText("@ERA02@");
        label3.setHorizontalAlignment(SwingConstants.CENTER);
        label3.setForeground(new Color(204, 0, 0));
        label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
        label3.setName("label3");
        p_contenu.add(label3);
        label3.setBounds(705, 60, 20, 25);

        //---- label4 ----
        label4.setText("@ERA03@");
        label4.setHorizontalAlignment(SwingConstants.CENTER);
        label4.setForeground(new Color(204, 0, 0));
        label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
        label4.setName("label4");
        p_contenu.add(label4);
        label4.setBounds(705, 80, 20, 25);

        //---- label5 ----
        label5.setText("@ERA04@");
        label5.setHorizontalAlignment(SwingConstants.CENTER);
        label5.setForeground(new Color(204, 0, 0));
        label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
        label5.setName("label5");
        p_contenu.add(label5);
        label5.setBounds(705, 103, 20, 25);

        //---- label6 ----
        label6.setText("@ERA05@");
        label6.setHorizontalAlignment(SwingConstants.CENTER);
        label6.setForeground(new Color(204, 0, 0));
        label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
        label6.setName("label6");
        p_contenu.add(label6);
        label6.setBounds(705, 123, 20, 25);

        //---- label7 ----
        label7.setText("@ERA06@");
        label7.setHorizontalAlignment(SwingConstants.CENTER);
        label7.setForeground(new Color(204, 0, 0));
        label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
        label7.setName("label7");
        p_contenu.add(label7);
        label7.setBounds(705, 143, 20, 25);

        //---- label8 ----
        label8.setText("@ERA07@");
        label8.setHorizontalAlignment(SwingConstants.CENTER);
        label8.setForeground(new Color(204, 0, 0));
        label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
        label8.setName("label8");
        p_contenu.add(label8);
        label8.setBounds(705, 163, 20, 25);

        //---- label9 ----
        label9.setText("@ERA08@");
        label9.setHorizontalAlignment(SwingConstants.CENTER);
        label9.setForeground(new Color(204, 0, 0));
        label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
        label9.setName("label9");
        p_contenu.add(label9);
        label9.setBounds(705, 183, 20, 25);

        //---- label10 ----
        label10.setText("@ERA09@");
        label10.setHorizontalAlignment(SwingConstants.CENTER);
        label10.setForeground(new Color(204, 0, 0));
        label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
        label10.setName("label10");
        p_contenu.add(label10);
        label10.setBounds(705, 203, 20, 25);

        //---- label11 ----
        label11.setText("@ERA10@");
        label11.setHorizontalAlignment(SwingConstants.CENTER);
        label11.setForeground(new Color(204, 0, 0));
        label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
        label11.setName("label11");
        p_contenu.add(label11);
        label11.setBounds(705, 223, 20, 25);

        //---- label12 ----
        label12.setText("@ERA11@");
        label12.setHorizontalAlignment(SwingConstants.CENTER);
        label12.setForeground(new Color(204, 0, 0));
        label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
        label12.setName("label12");
        p_contenu.add(label12);
        label12.setBounds(705, 243, 20, 25);

        //---- label13 ----
        label13.setText("@ERA12@");
        label13.setHorizontalAlignment(SwingConstants.CENTER);
        label13.setForeground(new Color(204, 0, 0));
        label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
        label13.setName("label13");
        p_contenu.add(label13);
        label13.setBounds(705, 263, 20, 25);

        //---- label14 ----
        label14.setText("@ERA13@");
        label14.setHorizontalAlignment(SwingConstants.CENTER);
        label14.setForeground(new Color(204, 0, 0));
        label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
        label14.setName("label14");
        p_contenu.add(label14);
        label14.setBounds(705, 283, 20, 25);

        //---- label15 ----
        label15.setText("@ERA14@");
        label15.setHorizontalAlignment(SwingConstants.CENTER);
        label15.setForeground(new Color(204, 0, 0));
        label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
        label15.setName("label15");
        p_contenu.add(label15);
        label15.setBounds(705, 303, 20, 25);

        //---- label16 ----
        label16.setText("@ERA15@");
        label16.setHorizontalAlignment(SwingConstants.CENTER);
        label16.setForeground(new Color(204, 0, 0));
        label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
        label16.setName("label16");
        p_contenu.add(label16);
        label16.setBounds(705, 323, 20, 25);

        //---- label17 ----
        label17.setText("@ERA16@");
        label17.setHorizontalAlignment(SwingConstants.CENTER);
        label17.setForeground(new Color(204, 0, 0));
        label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
        label17.setName("label17");
        p_contenu.add(label17);
        label17.setBounds(705, 343, 20, 25);

        //---- label1 ----
        label1.setText("Montant affect\u00e9");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setName("label1");
        p_contenu.add(label1);
        label1.setBounds(595, 15, 105, 25);

        //---- label18 ----
        label18.setText("E");
        label18.setName("label18");
        p_contenu.add(label18);
        label18.setBounds(new Rectangle(new Point(710, 20), label18.getPreferredSize()));

        //======== panel2 ========
        {
          panel2.setOpaque(false);
          panel2.setName("panel2");
          panel2.setLayout(null);

          //---- OBJ_60 ----
          OBJ_60.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD));
          OBJ_60.setName("OBJ_60");
          OBJ_60.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_60ActionPerformed(e);
            }
          });
          panel2.add(OBJ_60);
          OBJ_60.setBounds(5, 10, 54, 25);

          //---- OBJ_61 ----
          OBJ_61.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_61.setFont(OBJ_61.getFont().deriveFont(OBJ_61.getFont().getStyle() | Font.BOLD));
          OBJ_61.setName("OBJ_61");
          OBJ_61.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_61ActionPerformed(e);
            }
          });
          panel2.add(OBJ_61);
          OBJ_61.setBounds(5, 30, 54, 25);

          //---- OBJ_62 ----
          OBJ_62.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_62.setFont(OBJ_62.getFont().deriveFont(OBJ_62.getFont().getStyle() | Font.BOLD));
          OBJ_62.setName("OBJ_62");
          OBJ_62.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_62ActionPerformed(e);
            }
          });
          panel2.add(OBJ_62);
          OBJ_62.setBounds(5, 50, 54, 25);

          //---- OBJ_63 ----
          OBJ_63.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_63.setFont(OBJ_63.getFont().deriveFont(OBJ_63.getFont().getStyle() | Font.BOLD));
          OBJ_63.setName("OBJ_63");
          OBJ_63.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_63ActionPerformed(e);
            }
          });
          panel2.add(OBJ_63);
          OBJ_63.setBounds(5, 70, 54, 25);

          //---- OBJ_64 ----
          OBJ_64.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_64.setFont(OBJ_64.getFont().deriveFont(OBJ_64.getFont().getStyle() | Font.BOLD));
          OBJ_64.setName("OBJ_64");
          OBJ_64.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_64ActionPerformed(e);
            }
          });
          panel2.add(OBJ_64);
          OBJ_64.setBounds(5, 90, 54, 25);

          //---- OBJ_65 ----
          OBJ_65.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_65.setFont(OBJ_65.getFont().deriveFont(OBJ_65.getFont().getStyle() | Font.BOLD));
          OBJ_65.setName("OBJ_65");
          OBJ_65.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_65ActionPerformed(e);
            }
          });
          panel2.add(OBJ_65);
          OBJ_65.setBounds(5, 110, 54, 25);

          //---- OBJ_66 ----
          OBJ_66.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_66.setFont(OBJ_66.getFont().deriveFont(OBJ_66.getFont().getStyle() | Font.BOLD));
          OBJ_66.setName("OBJ_66");
          OBJ_66.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_66ActionPerformed(e);
            }
          });
          panel2.add(OBJ_66);
          OBJ_66.setBounds(5, 130, 54, 25);

          //---- OBJ_67 ----
          OBJ_67.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_67.setFont(OBJ_67.getFont().deriveFont(OBJ_67.getFont().getStyle() | Font.BOLD));
          OBJ_67.setName("OBJ_67");
          OBJ_67.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_67ActionPerformed(e);
            }
          });
          panel2.add(OBJ_67);
          OBJ_67.setBounds(5, 150, 54, 25);

          //---- OBJ_68 ----
          OBJ_68.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_68.setFont(OBJ_68.getFont().deriveFont(OBJ_68.getFont().getStyle() | Font.BOLD));
          OBJ_68.setName("OBJ_68");
          OBJ_68.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_68ActionPerformed(e);
            }
          });
          panel2.add(OBJ_68);
          OBJ_68.setBounds(5, 170, 54, 25);

          //---- OBJ_69 ----
          OBJ_69.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_69.setFont(OBJ_69.getFont().deriveFont(OBJ_69.getFont().getStyle() | Font.BOLD));
          OBJ_69.setName("OBJ_69");
          OBJ_69.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_69ActionPerformed(e);
            }
          });
          panel2.add(OBJ_69);
          OBJ_69.setBounds(5, 190, 54, 25);

          //---- OBJ_70 ----
          OBJ_70.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_70.setFont(OBJ_70.getFont().deriveFont(OBJ_70.getFont().getStyle() | Font.BOLD));
          OBJ_70.setName("OBJ_70");
          OBJ_70.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_70ActionPerformed(e);
            }
          });
          panel2.add(OBJ_70);
          OBJ_70.setBounds(5, 210, 54, 25);

          //---- OBJ_71 ----
          OBJ_71.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_71.setFont(OBJ_71.getFont().deriveFont(OBJ_71.getFont().getStyle() | Font.BOLD));
          OBJ_71.setName("OBJ_71");
          OBJ_71.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_71ActionPerformed(e);
            }
          });
          panel2.add(OBJ_71);
          OBJ_71.setBounds(5, 230, 54, 25);

          //---- OBJ_72 ----
          OBJ_72.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_72.setFont(OBJ_72.getFont().deriveFont(OBJ_72.getFont().getStyle() | Font.BOLD));
          OBJ_72.setName("OBJ_72");
          OBJ_72.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_72ActionPerformed(e);
            }
          });
          panel2.add(OBJ_72);
          OBJ_72.setBounds(5, 250, 54, 25);

          //---- OBJ_73 ----
          OBJ_73.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_73.setFont(OBJ_73.getFont().deriveFont(OBJ_73.getFont().getStyle() | Font.BOLD));
          OBJ_73.setName("OBJ_73");
          OBJ_73.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_73ActionPerformed(e);
            }
          });
          panel2.add(OBJ_73);
          OBJ_73.setBounds(5, 270, 54, 25);

          //---- OBJ_74 ----
          OBJ_74.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_74.setFont(OBJ_74.getFont().deriveFont(OBJ_74.getFont().getStyle() | Font.BOLD));
          OBJ_74.setName("OBJ_74");
          OBJ_74.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_74ActionPerformed(e);
            }
          });
          panel2.add(OBJ_74);
          OBJ_74.setBounds(5, 290, 54, 25);

          //---- OBJ_75 ----
          OBJ_75.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_75.setFont(OBJ_75.getFont().deriveFont(OBJ_75.getFont().getStyle() | Font.BOLD));
          OBJ_75.setName("OBJ_75");
          OBJ_75.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_75ActionPerformed(e);
            }
          });
          panel2.add(OBJ_75);
          OBJ_75.setBounds(5, 310, 54, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel2.getComponentCount(); i++) {
              Rectangle bounds = panel2.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel2.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel2.setMinimumSize(preferredSize);
            panel2.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel2);
        panel2.setBounds(540, 33, 60, 350);

        //---- BT_PGUP ----
        BT_PGUP.setText("");
        BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGUP.setName("BT_PGUP");
        p_contenu.add(BT_PGUP);
        BT_PGUP.setBounds(735, 20, 25, 150);

        //---- BT_PGDOWN ----
        BT_PGDOWN.setText("");
        BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        BT_PGDOWN.setName("BT_PGDOWN");
        p_contenu.add(BT_PGDOWN);
        BT_PGDOWN.setBounds(735, 220, 25, 150);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_15 ----
      OBJ_15.setText("Choix possibles");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);

      //---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private RiZoneSortie WDIFE;
  private RiZoneSortie TOTAFF;
  private JLabel OBJ_690;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable LA01;
  private XRiTextField MTA01;
  private XRiTextField MTA02;
  private XRiTextField MTA03;
  private XRiTextField MTA04;
  private XRiTextField MTA05;
  private XRiTextField MTA06;
  private XRiTextField MTA07;
  private XRiTextField MTA08;
  private XRiTextField MTA09;
  private XRiTextField MTA10;
  private XRiTextField MTA16;
  private XRiTextField MTA14;
  private XRiTextField MTA15;
  private XRiTextField MTA13;
  private XRiTextField MTA12;
  private XRiTextField MTA11;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label1;
  private JLabel label18;
  private JPanel panel2;
  private JButton OBJ_60;
  private JButton OBJ_61;
  private JButton OBJ_62;
  private JButton OBJ_63;
  private JButton OBJ_64;
  private JButton OBJ_65;
  private JButton OBJ_66;
  private JButton OBJ_67;
  private JButton OBJ_68;
  private JButton OBJ_69;
  private JButton OBJ_70;
  private JButton OBJ_71;
  private JButton OBJ_72;
  private JButton OBJ_73;
  private JButton OBJ_74;
  private JButton OBJ_75;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private JPopupMenu BTD;
  private JMenuItem OBJ_15;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
