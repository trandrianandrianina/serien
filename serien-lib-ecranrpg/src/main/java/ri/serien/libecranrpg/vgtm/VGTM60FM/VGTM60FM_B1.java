
package ri.serien.libecranrpg.vgtm.VGTM60FM;
// Nom Fichier: i_VGTM60FM_FMTB1_FMTF1_67.java

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.Constantes;
import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonLeger;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM60FM_B1 extends SNPanelEcranRPG implements ioFrame {
  
  // private String[] _LIST_Top=null;
  private String[] _LD011_Title = { "Rubriques", "PER01", "PER02", "PER03", "PER04", "PER05", "PER06", };
  private String[][] _LD011_Data = { { "LD011", "LD012", "LD013", "LD014", "LD015", "LD016", "LD017", },
      { "LD021", "LD022", "LD023", "LD024", "LD025", "LD026", "LD027", },
      { "LD031", "LD032", "LD033", "LD034", "LD035", "LD036", "LD037", },
      { "LD041", "LD042", "LD043", "LD044", "LD045", "LD046", "LD047", },
      { "LD051", "LD052", "LD053", "LD054", "LD055", "LD056", "LD057", },
      { "LD061", "LD062", "LD063", "LD064", "LD065", "LD066", "LD067", },
      { "LD071", "LD072", "LD073", "LD074", "LD075", "LD076", "LD077", },
      { "LD081", "LD082", "LD083", "LD084", "LD085", "LD086", "LD087", },
      { "LD091", "LD092", "LD093", "LD094", "LD095", "LD096", "LD097", },
      { "LD101", "LD102", "LD103", "LD104", "LD105", "LD106", "LD107", },
      { "LD111", "LD112", "LD113", "LD114", "LD115", "LD116", "LD117", },
      { "LD121", "LD122", "LD123", "LD124", "LD125", "LD126", "LD127", },
      { "LD131", "LD132", "LD133", "LD134", "LD135", "LD136", "LD137", },
      { "LD141", "LD142", "LD143", "LD144", "LD145", "LD146", "LD147", },
      { "LD151", "LD152", "LD153", "LD154", "LD155", "LD156", "LD157", },
      { "LD161", "LD162", "LD163", "LD164", "LD165", "LD166", "LD167", },
      { "LD171", "LD172", "LD173", "LD174", "LD175", "LD176", "LD177", },
      { "LD181", "LD182", "LD183", "LD184", "LD185", "LD186", "LD187", }, };
  private int[] _LD011_Width = { 133, 70, 70, 70, 70, 70, 70, };
  private Color[][] _LD011_Text_Color = new Color[18][7];
  private Color[][] _LD011_Fond_Color = null;
  private int[] _LD011_Justification = { SwingConstants.LEFT, SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT,
      SwingConstants.RIGHT, SwingConstants.RIGHT, SwingConstants.RIGHT };
  
  public VGTM60FM_B1(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    // setDialog(true);
    
    // Ajout
    initDiverses();
    LD011.setAspectTable(null, _LD011_Title, _LD011_Data, _LD011_Width, false, _LD011_Justification, _LD011_Text_Color, _LD011_Fond_Color,
        null);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WSOC.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WSOC@")).trim());
    DGNOM.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@DGNOM@")).trim());
    LIBDEV.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEV@")).trim());
    LIBDEP.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBDEP@")).trim());
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    // majTable(LIST, LIST.get_LIST_Title_Data_Brut(), _LIST_Top);
    
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_44.setIcon(lexique.chargerImage("images/arriere.gif", true));
    OBJ_46.setIcon(lexique.chargerImage("images/avant.gif", true));
    
    // ++++++++++++++++++++++++++ COULEURS LISTE ++++++++++++++++++++++++++++++++++++++++
    // Couleurs des lignes de la liste (suivant indicateurs de 61 à 76)
    for (int i = 0; i < _LD011_Text_Color.length; i++) {
      if (lexique.isTrue(String.valueOf(i + 61))) {
        _LD011_Text_Color[i][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][1] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][2] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][3] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][4] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][5] = Constantes.COULEUR_LISTE_COMMENTAIRE;
        _LD011_Text_Color[i][6] = Constantes.COULEUR_LISTE_COMMENTAIRE;
      }
      else {
        _LD011_Text_Color[i][0] = Color.BLACK;
        _LD011_Text_Color[i][1] = Color.BLACK;
        _LD011_Text_Color[i][2] = Color.BLACK;
        _LD011_Text_Color[i][3] = Color.BLACK;
        _LD011_Text_Color[i][4] = Color.BLACK;
        _LD011_Text_Color[i][5] = Color.BLACK;
        _LD011_Text_Color[i][6] = Color.BLACK;
      }
    }
    
    _LD011_Text_Color[16][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][1] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][2] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][3] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][4] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][5] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[16][6] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    
    _LD011_Text_Color[17][0] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][1] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][2] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][3] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][4] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][5] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    _LD011_Text_Color[17][6] = Constantes.COULEUR_LISTE_COMMENTAIRE;
    // ++++++++++++++++++++++++++ COULEURS LISTE ++++++++++++++++++++++++++++++++++++++++
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Tableau de trésorerie"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", false);
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F13", false);
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F3", false);
  }
  
  private void LD011MouseClicked(MouseEvent e) {
    String nomCellule = "LD" + ((LD011.getSelectedRow() + 1) < 10 ? "0" + (LD011.getSelectedRow() + 1) : (LD011.getSelectedRow() + 1))
        + (LD011.getSelectedColumn() + 1);
    lexique.HostCursorPut(nomCellule);
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    WSOC = new RiZoneSortie();
    OBJ_40_OBJ_40 = new JLabel();
    DGNOM = new RiZoneSortie();
    LIBDEV = new RiZoneSortie();
    LIBDEP = new RiZoneSortie();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_44 = new SNBoutonLeger();
    OBJ_43_OBJ_43 = new JLabel();
    WI = new XRiComboBox();
    OBJ_46 = new SNBoutonLeger();
    panel1 = new JPanel();
    SCROLLPANE_OBJ_19 = new JScrollPane();
    LD011 = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("Tableau de tr\u00e9sorerie");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(700, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- WSOC ----
          WSOC.setOpaque(false);
          WSOC.setText("@WSOC@");
          WSOC.setName("WSOC");

          //---- OBJ_40_OBJ_40 ----
          OBJ_40_OBJ_40.setText("Soci\u00e9t\u00e9");
          OBJ_40_OBJ_40.setName("OBJ_40_OBJ_40");

          //---- DGNOM ----
          DGNOM.setOpaque(false);
          DGNOM.setText("@DGNOM@");
          DGNOM.setName("DGNOM");

          //---- LIBDEV ----
          LIBDEV.setOpaque(false);
          LIBDEV.setText("@LIBDEV@");
          LIBDEV.setName("LIBDEV");

          //---- LIBDEP ----
          LIBDEP.setOpaque(false);
          LIBDEP.setText("@LIBDEP@");
          LIBDEP.setForeground(new Color(204, 0, 0));
          LIBDEP.setName("LIBDEP");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                    .addGap(60, 60, 60)
                    .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                  .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 62, GroupLayout.PREFERRED_SIZE))
                .addGap(5, 5, 5)
                .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, 214, GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(LIBDEV, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(LIBDEP, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(180, Short.MAX_VALUE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WSOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(3, 3, 3)
                .addComponent(OBJ_40_OBJ_40, GroupLayout.PREFERRED_SIZE, 18, GroupLayout.PREFERRED_SIZE))
              .addComponent(DGNOM, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                .addComponent(LIBDEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addComponent(LIBDEP, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");

              //---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);

            //======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");

              //---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);

            //======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");

              //---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);

            //======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");

              //---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);

            //======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");

              //---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);

            //======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");

              //---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);

            //======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");

              //---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);

            //======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");

              //---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);

            //======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");

              //---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Mise \u00e0 jour de l'affichage");
              riSousMenu_bt6.setToolTipText("Mise \u00e0 jour de l'affichage");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);

            //======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");

              //---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("R\u00e9g\u00e9neration du tableau ");
              riSousMenu_bt7.setToolTipText("R\u00e9g\u00e9neration du tableau de tr\u00e9sorerie");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(900, 460));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setOpaque(false);
            panel2.setName("panel2");

            //---- OBJ_44 ----
            OBJ_44.setText("");
            OBJ_44.setToolTipText("P\u00e9riodes pr\u00e9c\u00e9dentes");
            OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_44.setName("OBJ_44");
            OBJ_44.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_44ActionPerformed(e);
              }
            });

            //---- OBJ_43_OBJ_43 ----
            OBJ_43_OBJ_43.setText("Tranche");
            OBJ_43_OBJ_43.setName("OBJ_43_OBJ_43");

            //---- WI ----
            WI.setModel(new DefaultComboBoxModel(new String[] {
              "1",
              "5",
              "10",
              "15",
              "30"
            }));
            WI.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            WI.setName("WI");

            //---- OBJ_46 ----
            OBJ_46.setText("");
            OBJ_46.setToolTipText("P\u00e9riodes suivantes");
            OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_46.setName("OBJ_46");
            OBJ_46.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_46ActionPerformed(e);
              }
            });

            GroupLayout panel2Layout = new GroupLayout(panel2);
            panel2.setLayout(panel2Layout);
            panel2Layout.setHorizontalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addComponent(OBJ_44, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE)
                  .addGap(180, 180, 180)
                  .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 54, GroupLayout.PREFERRED_SIZE)
                  .addGap(1, 1, 1)
                  .addComponent(WI, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                  .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 199, Short.MAX_VALUE)
                  .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, 195, GroupLayout.PREFERRED_SIZE))
            );
            panel2Layout.setVerticalGroup(
              panel2Layout.createParallelGroup()
                .addGroup(panel2Layout.createSequentialGroup()
                  .addGroup(panel2Layout.createParallelGroup()
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(12, 12, 12)
                      .addComponent(OBJ_43_OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel2Layout.createSequentialGroup()
                      .addGap(9, 9, 9)
                      .addGroup(panel2Layout.createParallelGroup()
                        .addComponent(WI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addComponent(OBJ_46, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                  .addGap(1, 1, 1))
                .addComponent(OBJ_44, GroupLayout.Alignment.TRAILING, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
            );
          }

          //======== panel1 ========
          {
            panel1.setBorder(new TitledBorder("Tr\u00e9sorerie"));
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //======== SCROLLPANE_OBJ_19 ========
            {
              SCROLLPANE_OBJ_19.setName("SCROLLPANE_OBJ_19");

              //---- LD011 ----
              LD011.setShowHorizontalLines(true);
              LD011.setShowVerticalLines(true);
              LD011.setCellSelectionEnabled(true);
              LD011.setName("LD011");
              LD011.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                  LD011MouseClicked(e);
                }
              });
              SCROLLPANE_OBJ_19.setViewportView(LD011);
            }
            panel1.add(SCROLLPANE_OBJ_19);
            SCROLLPANE_OBJ_19.setBounds(30, 35, 810, 320);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel1, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 874, Short.MAX_VALUE)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(12, 12, 12))
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel1, GroupLayout.DEFAULT_SIZE, 385, Short.MAX_VALUE)
                .addContainerGap())
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Aide en ligne");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private RiZoneSortie WSOC;
  private JLabel OBJ_40_OBJ_40;
  private RiZoneSortie DGNOM;
  private RiZoneSortie LIBDEV;
  private RiZoneSortie LIBDEP;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private SNBoutonLeger OBJ_44;
  private JLabel OBJ_43_OBJ_43;
  private XRiComboBox WI;
  private SNBoutonLeger OBJ_46;
  private JPanel panel1;
  private JScrollPane SCROLLPANE_OBJ_19;
  private XRiTable LD011;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
