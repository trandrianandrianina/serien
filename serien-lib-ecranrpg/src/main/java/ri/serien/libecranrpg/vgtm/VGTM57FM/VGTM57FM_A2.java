
package ri.serien.libecranrpg.vgtm.VGTM57FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composant.primitif.bouton.SNBoutonDetail;
import ri.serien.libswing.composant.primitif.panel.SNPanelDegradeGris;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.moteur.graphique.SNBandeauTitre;

/**
 * @author Stéphane Vénéri
 */
public class VGTM57FM_A2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] LCRBAO_Value = { "", "1", "4", };
  
  public VGTM57FM_A2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    // Ajout
    initDiverses();
    LCRBAO.setValeurs(LCRBAO_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    p_bpresentation.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCTIT@ @TITBRD@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("78");
    


    
    
    OBJ_40.setVisible(lexique.isPresent("WA2RGL"));
    OBJ_42.setVisible(lexique.isPresent("WA2ECH"));
    OBJ_44.setVisible(lexique.isPresent("WA2RTI"));
    OBJ_47.setVisible(lexique.isPresent("WA2RTI"));
    OBJ_46.setVisible(lexique.isPresent("WA2NRO"));
    OBJ_106_OBJ_106.setVisible(lexique.isPresent("WA2ETB"));
    OBJ_107_OBJ_107.setVisible(lexique.isPresent("WA2ETB"));
    OBJ_108_OBJ_108.setVisible(lexique.isPresent("WA2ETB"));
    OBJ_109_OBJ_109.setVisible(lexique.isPresent("WA2ETB"));
    OBJ_105_OBJ_107.setVisible(lexique.isPresent("WA2CLI"));
    OBJ_105_OBJ_106.setVisible(lexique.isPresent("WA2VIL"));
    OBJ_133.setVisible(lexique.isPresent("WA2BQE"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - @LOCTIT@ @TITBRD@"));
    
    

    
    p_bpresentation.setCodeEtablissement(WISOC.getText());
    // logoEtb.setIcon(ManagerSessionClient.getInstance().getLogoImage(WISOC.getText()));
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    // TODO add your code here
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void OBJ_133ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut("WA2BQ2");
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_nord = new JPanel();
    p_bpresentation = new SNBandeauTitre();
    barre_tete = new JMenuBar();
    p_tete_gauche = new JPanel();
    OBJ_37 = new JLabel();
    WISOC = new XRiTextField();
    OBJ_39 = new JLabel();
    WINCGX = new XRiTextField();
    OBJ_41 = new JLabel();
    WINCA = new XRiTextField();
    OBJ_43 = new JLabel();
    WIDTEX = new XRiTextField();
    OBJ_45 = new JLabel();
    WICJO2 = new XRiTextField();
    p_tete_droite = new JPanel();
    p_sud = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_centrage = new SNPanelDegradeGris();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    OBJ_31 = new JLabel();
    A1LIB = new XRiTextField();
    A1CPL = new XRiTextField();
    A1RUE = new XRiTextField();
    A1LOC = new XRiTextField();
    A1CDP = new XRiTextField();
    A1VIL = new XRiTextField();
    OBJ_38 = new JLabel();
    WA2MTT = new XRiTextField();
    LCRBAO = new XRiComboBox();
    OBJ_40 = new JLabel();
    WA2RGL = new XRiTextField();
    OBJ_42 = new JLabel();
    WA2ECH = new XRiCalendrier();
    OBJ_44 = new JLabel();
    OBJ_46 = new JLabel();
    WA2NRO = new XRiTextField();
    WA2RTI = new XRiTextField();
    OBJ_47 = new JLabel();
    panel3 = new JPanel();
    WA2RIE = new XRiTextField();
    WA2BQ2 = new XRiTextField();
    OBJ_105_OBJ_105 = new JLabel();
    OBJ_108_OBJ_108 = new JLabel();
    WA2CPT = new XRiTextField();
    OBJ_107_OBJ_107 = new JLabel();
    WA2ETB = new XRiTextField();
    WA2GUI = new XRiTextField();
    OBJ_106_OBJ_106 = new JLabel();
    OBJ_109_OBJ_109 = new JLabel();
    WA2RIB = new XRiTextField();
    WA2VIL = new XRiTextField();
    OBJ_105_OBJ_106 = new JLabel();
    WA2CLI = new XRiTextField();
    OBJ_105_OBJ_107 = new JLabel();
    OBJ_133 = new SNBoutonDetail();
    WA2BQE = new XRiTextField();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(900, 600));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_nord ========
    {
      p_nord.setName("p_nord");
      p_nord.setLayout(new VerticalLayout());

      //---- p_bpresentation ----
      p_bpresentation.setText("@LOCTIT@ @TITBRD@");
      p_bpresentation.setName("p_bpresentation");
      p_nord.add(p_bpresentation);

      //======== barre_tete ========
      {
        barre_tete.setMinimumSize(new Dimension(111, 34));
        barre_tete.setPreferredSize(new Dimension(111, 34));
        barre_tete.setName("barre_tete");

        //======== p_tete_gauche ========
        {
          p_tete_gauche.setOpaque(false);
          p_tete_gauche.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
          p_tete_gauche.setPreferredSize(new Dimension(900, 32));
          p_tete_gauche.setMinimumSize(new Dimension(700, 32));
          p_tete_gauche.setName("p_tete_gauche");

          //---- OBJ_37 ----
          OBJ_37.setText("Soci\u00e9t\u00e9");
          OBJ_37.setName("OBJ_37");

          //---- WISOC ----
          WISOC.setComponentPopupMenu(BTD);
          WISOC.setName("WISOC");

          //---- OBJ_39 ----
          OBJ_39.setText("Compte");
          OBJ_39.setName("OBJ_39");

          //---- WINCGX ----
          WINCGX.setComponentPopupMenu(BTD);
          WINCGX.setName("WINCGX");

          //---- OBJ_41 ----
          OBJ_41.setText("Tiers");
          OBJ_41.setName("OBJ_41");

          //---- WINCA ----
          WINCA.setComponentPopupMenu(BTD);
          WINCA.setName("WINCA");

          //---- OBJ_43 ----
          OBJ_43.setText("Date");
          OBJ_43.setName("OBJ_43");

          //---- WIDTEX ----
          WIDTEX.setComponentPopupMenu(BTD);
          WIDTEX.setName("WIDTEX");

          //---- OBJ_45 ----
          OBJ_45.setText("Banque");
          OBJ_45.setName("OBJ_45");

          //---- WICJO2 ----
          WICJO2.setComponentPopupMenu(BTD);
          WICJO2.setName("WICJO2");

          GroupLayout p_tete_gaucheLayout = new GroupLayout(p_tete_gauche);
          p_tete_gauche.setLayout(p_tete_gaucheLayout);
          p_tete_gaucheLayout.setHorizontalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(5, 5, 5)
                .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WINCGX, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(WINCA, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                .addGap(30, 30, 30)
                .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, 76, GroupLayout.PREFERRED_SIZE)
                .addGap(34, 34, 34)
                .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 65, GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addComponent(WICJO2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE))
          );
          p_tete_gaucheLayout.setVerticalGroup(
            p_tete_gaucheLayout.createParallelGroup()
              .addComponent(WISOC, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WINCGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WINCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WIDTEX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addComponent(WICJO2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
              .addGroup(p_tete_gaucheLayout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(p_tete_gaucheLayout.createParallelGroup()
                  .addComponent(OBJ_37, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_39, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_41, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_43, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                  .addComponent(OBJ_45, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)))
          );
        }
        barre_tete.add(p_tete_gauche);

        //======== p_tete_droite ========
        {
          p_tete_droite.setOpaque(false);
          p_tete_droite.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
          p_tete_droite.setPreferredSize(new Dimension(150, 0));
          p_tete_droite.setMinimumSize(new Dimension(150, 0));
          p_tete_droite.setName("p_tete_droite");
          p_tete_droite.setLayout(new FlowLayout(FlowLayout.RIGHT));
        }
        barre_tete.add(p_tete_droite);
      }
      p_nord.add(barre_tete);
    }
    add(p_nord, BorderLayout.NORTH);

    //======== p_sud ========
    {
      p_sud.setName("p_sud");
      p_sud.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 520));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");

          //======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());

            //======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");

              //---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);

            //======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");

              //---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Affectations r\u00e9glements");
              riSousMenu_bt6.setToolTipText("Mise en/hors fonction des affectations de r\u00e9glements");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_sud.add(p_menus, BorderLayout.EAST);

      //======== p_centrage ========
      {
        p_centrage.setBackground(new Color(198, 198, 200));
        p_centrage.setName("p_centrage");
        p_centrage.setLayout(new GridBagLayout());

        //======== p_contenu ========
        {
          p_contenu.setPreferredSize(new Dimension(750, 420));
          p_contenu.setBorder(new LineBorder(Color.darkGray));
          p_contenu.setBackground(new Color(239, 239, 222));
          p_contenu.setName("p_contenu");

          //======== panel2 ========
          {
            panel2.setBorder(new TitledBorder("Identification tiers"));
            panel2.setOpaque(false);
            panel2.setName("panel2");
            panel2.setLayout(null);

            //---- OBJ_31 ----
            OBJ_31.setText("Nom du client");
            OBJ_31.setName("OBJ_31");
            panel2.add(OBJ_31);
            OBJ_31.setBounds(25, 32, 87, 20);

            //---- A1LIB ----
            A1LIB.setName("A1LIB");
            panel2.add(A1LIB);
            A1LIB.setBounds(25, 57, 310, A1LIB.getPreferredSize().height);

            //---- A1CPL ----
            A1CPL.setName("A1CPL");
            panel2.add(A1CPL);
            A1CPL.setBounds(25, 89, 310, A1CPL.getPreferredSize().height);

            //---- A1RUE ----
            A1RUE.setName("A1RUE");
            panel2.add(A1RUE);
            A1RUE.setBounds(25, 121, 310, A1RUE.getPreferredSize().height);

            //---- A1LOC ----
            A1LOC.setName("A1LOC");
            panel2.add(A1LOC);
            A1LOC.setBounds(25, 153, 310, A1LOC.getPreferredSize().height);

            //---- A1CDP ----
            A1CDP.setName("A1CDP");
            panel2.add(A1CDP);
            A1CDP.setBounds(25, 185, 60, A1CDP.getPreferredSize().height);

            //---- A1VIL ----
            A1VIL.setName("A1VIL");
            panel2.add(A1VIL);
            A1VIL.setBounds(90, 185, 245, A1VIL.getPreferredSize().height);

            //---- OBJ_38 ----
            OBJ_38.setText("Montant du r\u00e9glement");
            OBJ_38.setName("OBJ_38");
            panel2.add(OBJ_38);
            OBJ_38.setBounds(400, 61, 132, 20);

            //---- WA2MTT ----
            WA2MTT.setComponentPopupMenu(BTD);
            WA2MTT.setHorizontalAlignment(SwingConstants.RIGHT);
            WA2MTT.setName("WA2MTT");
            panel2.add(WA2MTT);
            WA2MTT.setBounds(570, 57, 110, WA2MTT.getPreferredSize().height);

            //---- LCRBAO ----
            LCRBAO.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "LCR",
              "BAO"
            }));
            LCRBAO.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LCRBAO.setName("LCRBAO");
            panel2.add(LCRBAO);
            LCRBAO.setBounds(605, 90, 64, LCRBAO.getPreferredSize().height);

            //---- OBJ_40 ----
            OBJ_40.setText("R\u00e9glement");
            OBJ_40.setName("OBJ_40");
            panel2.add(OBJ_40);
            OBJ_40.setBounds(400, 94, 80, 18);

            //---- WA2RGL ----
            WA2RGL.setComponentPopupMenu(BTD);
            WA2RGL.setName("WA2RGL");
            panel2.add(WA2RGL);
            WA2RGL.setBounds(570, 89, 30, WA2RGL.getPreferredSize().height);

            //---- OBJ_42 ----
            OBJ_42.setText("Ech\u00e9ance");
            OBJ_42.setName("OBJ_42");
            panel2.add(OBJ_42);
            OBJ_42.setBounds(400, 126, 64, 18);

            //---- WA2ECH ----
            WA2ECH.setComponentPopupMenu(BTD);
            WA2ECH.setName("WA2ECH");
            panel2.add(WA2ECH);
            WA2ECH.setBounds(570, 121, 105, WA2ECH.getPreferredSize().height);

            //---- OBJ_44 ----
            OBJ_44.setText("R\u00e9f.");
            OBJ_44.setName("OBJ_44");
            panel2.add(OBJ_44);
            OBJ_44.setBounds(400, 165, 40, 18);

            //---- OBJ_46 ----
            OBJ_46.setText("Bordereau");
            OBJ_46.setName("OBJ_46");
            panel2.add(OBJ_46);
            OBJ_46.setBounds(570, 165, 105, 18);

            //---- WA2NRO ----
            WA2NRO.setComponentPopupMenu(BTD);
            WA2NRO.setName("WA2NRO");
            panel2.add(WA2NRO);
            WA2NRO.setBounds(570, 185, 110, WA2NRO.getPreferredSize().height);

            //---- WA2RTI ----
            WA2RTI.setComponentPopupMenu(BTD);
            WA2RTI.setName("WA2RTI");
            panel2.add(WA2RTI);
            WA2RTI.setBounds(400, 185, 110, WA2RTI.getPreferredSize().height);

            //---- OBJ_47 ----
            OBJ_47.setText("Tir\u00e9");
            OBJ_47.setName("OBJ_47");
            panel2.add(OBJ_47);
            OBJ_47.setBounds(470, 165, 40, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel2.getComponentCount(); i++) {
                Rectangle bounds = panel2.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel2.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel2.setMinimumSize(preferredSize);
              panel2.setPreferredSize(preferredSize);
            }
          }

          //======== panel3 ========
          {
            panel3.setBorder(new TitledBorder("Banque"));
            panel3.setOpaque(false);
            panel3.setName("panel3");
            panel3.setLayout(null);

            //---- WA2RIE ----
            WA2RIE.setComponentPopupMenu(BTD);
            WA2RIE.setName("WA2RIE");
            panel3.add(WA2RIE);
            WA2RIE.setBounds(400, 85, 285, WA2RIE.getPreferredSize().height);

            //---- WA2BQ2 ----
            WA2BQ2.setComponentPopupMenu(BTD);
            WA2BQ2.setName("WA2BQ2");
            panel3.add(WA2BQ2);
            WA2BQ2.setBounds(25, 55, 80, WA2BQ2.getPreferredSize().height);

            //---- OBJ_105_OBJ_105 ----
            OBJ_105_OBJ_105.setText("Banque");
            OBJ_105_OBJ_105.setName("OBJ_105_OBJ_105");
            panel3.add(OBJ_105_OBJ_105);
            OBJ_105_OBJ_105.setBounds(25, 35, 80, 20);

            //---- OBJ_108_OBJ_108 ----
            OBJ_108_OBJ_108.setText("N\u00b0 Compte");
            OBJ_108_OBJ_108.setName("OBJ_108_OBJ_108");
            panel3.add(OBJ_108_OBJ_108);
            OBJ_108_OBJ_108.setBounds(535, 35, 97, 20);

            //---- WA2CPT ----
            WA2CPT.setComponentPopupMenu(BTD);
            WA2CPT.setName("WA2CPT");
            panel3.add(WA2CPT);
            WA2CPT.setBounds(535, 55, 100, WA2CPT.getPreferredSize().height);

            //---- OBJ_107_OBJ_107 ----
            OBJ_107_OBJ_107.setText("N\u00b0Guichet");
            OBJ_107_OBJ_107.setName("OBJ_107_OBJ_107");
            panel3.add(OBJ_107_OBJ_107);
            OBJ_107_OBJ_107.setBounds(465, 35, 63, 20);

            //---- WA2ETB ----
            WA2ETB.setComponentPopupMenu(BTD);
            WA2ETB.setName("WA2ETB");
            panel3.add(WA2ETB);
            WA2ETB.setBounds(400, 55, 52, WA2ETB.getPreferredSize().height);

            //---- WA2GUI ----
            WA2GUI.setComponentPopupMenu(BTD);
            WA2GUI.setName("WA2GUI");
            panel3.add(WA2GUI);
            WA2GUI.setBounds(470, 55, 52, WA2GUI.getPreferredSize().height);

            //---- OBJ_106_OBJ_106 ----
            OBJ_106_OBJ_106.setText("N\u00b0 Etb");
            OBJ_106_OBJ_106.setName("OBJ_106_OBJ_106");
            panel3.add(OBJ_106_OBJ_106);
            OBJ_106_OBJ_106.setBounds(400, 35, 40, 20);

            //---- OBJ_109_OBJ_109 ----
            OBJ_109_OBJ_109.setText("Cl\u00e9");
            OBJ_109_OBJ_109.setName("OBJ_109_OBJ_109");
            panel3.add(OBJ_109_OBJ_109);
            OBJ_109_OBJ_109.setBounds(655, 35, 24, 20);

            //---- WA2RIB ----
            WA2RIB.setComponentPopupMenu(BTD);
            WA2RIB.setName("WA2RIB");
            panel3.add(WA2RIB);
            WA2RIB.setBounds(655, 55, 30, WA2RIB.getPreferredSize().height);

            //---- WA2VIL ----
            WA2VIL.setComponentPopupMenu(BTD);
            WA2VIL.setName("WA2VIL");
            panel3.add(WA2VIL);
            WA2VIL.setBounds(120, 55, 100, WA2VIL.getPreferredSize().height);

            //---- OBJ_105_OBJ_106 ----
            OBJ_105_OBJ_106.setText("Ville");
            OBJ_105_OBJ_106.setName("OBJ_105_OBJ_106");
            panel3.add(OBJ_105_OBJ_106);
            OBJ_105_OBJ_106.setBounds(120, 35, 80, 20);

            //---- WA2CLI ----
            WA2CLI.setComponentPopupMenu(BTD);
            WA2CLI.setName("WA2CLI");
            panel3.add(WA2CLI);
            WA2CLI.setBounds(235, 55, 100, WA2CLI.getPreferredSize().height);

            //---- OBJ_105_OBJ_107 ----
            OBJ_105_OBJ_107.setText("Centrale");
            OBJ_105_OBJ_107.setName("OBJ_105_OBJ_107");
            panel3.add(OBJ_105_OBJ_107);
            OBJ_105_OBJ_107.setBounds(235, 35, 80, 20);

            //---- OBJ_133 ----
            OBJ_133.setText("");
            OBJ_133.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            OBJ_133.setName("OBJ_133");
            OBJ_133.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                OBJ_133ActionPerformed(e);
              }
            });
            panel3.add(OBJ_133);
            OBJ_133.setBounds(225, 55, 30, 28);

            //---- WA2BQE ----
            WA2BQE.setComponentPopupMenu(BTD);
            WA2BQE.setName("WA2BQE");
            panel3.add(WA2BQE);
            WA2BQE.setBounds(25, 55, 194, WA2BQE.getPreferredSize().height);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel3.getComponentCount(); i++) {
                Rectangle bounds = panel3.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel3.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel3.setMinimumSize(preferredSize);
              panel3.setPreferredSize(preferredSize);
            }
          }

          GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
          p_contenu.setLayout(p_contenuLayout);
          p_contenuLayout.setHorizontalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(GroupLayout.Alignment.TRAILING, p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(p_contenuLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                  .addComponent(panel2, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE)
                  .addComponent(panel3, GroupLayout.Alignment.LEADING, GroupLayout.DEFAULT_SIZE, 724, Short.MAX_VALUE))
                .addContainerGap())
          );
          p_contenuLayout.setVerticalGroup(
            p_contenuLayout.createParallelGroup()
              .addGroup(p_contenuLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panel2, GroupLayout.PREFERRED_SIZE, 249, GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panel3, GroupLayout.PREFERRED_SIZE, 136, GroupLayout.PREFERRED_SIZE)
                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
          );
        }
        p_centrage.add(p_contenu, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
          GridBagConstraints.CENTER, GridBagConstraints.BOTH,
          new Insets(0, 0, 5, 0), 0, 0));
      }
      p_sud.add(p_centrage, BorderLayout.CENTER);
    }
    add(p_sud, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_nord;
  private SNBandeauTitre p_bpresentation;
  private JMenuBar barre_tete;
  private JPanel p_tete_gauche;
  private JLabel OBJ_37;
  private XRiTextField WISOC;
  private JLabel OBJ_39;
  private XRiTextField WINCGX;
  private JLabel OBJ_41;
  private XRiTextField WINCA;
  private JLabel OBJ_43;
  private XRiTextField WIDTEX;
  private JLabel OBJ_45;
  private XRiTextField WICJO2;
  private JPanel p_tete_droite;
  private JPanel p_sud;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private SNPanelDegradeGris p_centrage;
  private JPanel p_contenu;
  private JPanel panel2;
  private JLabel OBJ_31;
  private XRiTextField A1LIB;
  private XRiTextField A1CPL;
  private XRiTextField A1RUE;
  private XRiTextField A1LOC;
  private XRiTextField A1CDP;
  private XRiTextField A1VIL;
  private JLabel OBJ_38;
  private XRiTextField WA2MTT;
  private XRiComboBox LCRBAO;
  private JLabel OBJ_40;
  private XRiTextField WA2RGL;
  private JLabel OBJ_42;
  private XRiCalendrier WA2ECH;
  private JLabel OBJ_44;
  private JLabel OBJ_46;
  private XRiTextField WA2NRO;
  private XRiTextField WA2RTI;
  private JLabel OBJ_47;
  private JPanel panel3;
  private XRiTextField WA2RIE;
  private XRiTextField WA2BQ2;
  private JLabel OBJ_105_OBJ_105;
  private JLabel OBJ_108_OBJ_108;
  private XRiTextField WA2CPT;
  private JLabel OBJ_107_OBJ_107;
  private XRiTextField WA2ETB;
  private XRiTextField WA2GUI;
  private JLabel OBJ_106_OBJ_106;
  private JLabel OBJ_109_OBJ_109;
  private XRiTextField WA2RIB;
  private XRiTextField WA2VIL;
  private JLabel OBJ_105_OBJ_106;
  private XRiTextField WA2CLI;
  private JLabel OBJ_105_OBJ_107;
  private SNBoutonDetail OBJ_133;
  private XRiTextField WA2BQE;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
