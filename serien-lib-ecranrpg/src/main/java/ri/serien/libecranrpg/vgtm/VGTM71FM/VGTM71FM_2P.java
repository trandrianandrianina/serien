
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.LayoutStyle;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_2P extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] L1SNS_Value = { "", "D", "C", };
  
  /**
   * Constructeur.
   */
  public VGTM71FM_2P(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    riMenu_bt3.setIcon(lexique.chargerImage("images/outils.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    riMenu_bt_V01F.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@V01F@")).trim());
    OBJ_71.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1SNS@")).trim());
    OBJ_68.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
    OBJ_28.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LOCCOM@")).trim());
    TTVA.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TTVA@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    // On baisse l'indicateur d'erreur car la gestion boucle dans ce cas...
    lexique.SetOff(61);
    
    OBJ_71.setVisible(lexique.isPresent("W1SNS"));
    WCHGX.setVisible(lexique.isTrue("75"));
    L1DEV.setVisible(lexique.isTrue("75"));
    L1MTD.setVisible(lexique.isTrue("75"));
    OBJ_28.setVisible(lexique.isPresent("LIBMTT"));
    L1QTE.setVisible(lexique.isTrue("76"));
    OBJ_62.setVisible(lexique.isTrue("75"));
    OBJ_63.setVisible(lexique.isTrue("76"));
    OBJ_64.setVisible(lexique.isTrue("75"));
    
    OBJ_68.setVisible(lexique.isTrue("77"));
    if (lexique.isTrue("79")) {
      OBJ_68.setForeground(Color.RED);
    }
    else {
      OBJ_68.setForeground(Color.BLACK);
    }
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@V01F@"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(20, 13);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(18, 11);
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8", true);
  }
  
  private void riSousMenu_bt10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", true);
  }
  
  private void riSousMenu_bt11ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void riSousMenu_bt14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F18", true);
  }
  
  private void riSousMenu_bt15ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", true);
  }
  
  private void OBJ_16ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", true);
  }
  
  private void OBJ_17ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_15ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    scroll_droite = new JScrollPane();
    menus_haut = new JPanel();
    riMenu_V01F = new RiMenu();
    riMenu_bt_V01F = new RiMenu_bt();
    riSousMenu_consult = new RiSousMenu();
    riSousMenu_bt_consult = new RiSousMenu_bt();
    riSousMenu_modif = new RiSousMenu();
    riSousMenu_bt_modif = new RiSousMenu_bt();
    riSousMenu_crea = new RiSousMenu();
    riSousMenu_bt_crea = new RiSousMenu_bt();
    riSousMenu_suppr = new RiSousMenu();
    riSousMenu_bt_suppr = new RiSousMenu_bt();
    riSousMenuF_dupli = new RiSousMenu();
    riSousMenu_bt_dupli = new RiSousMenu_bt();
    riSousMenu_rappel = new RiSousMenu();
    riSousMenu_bt_rappel = new RiSousMenu_bt();
    riSousMenu_reac = new RiSousMenu();
    riSousMenu_bt_reac = new RiSousMenu_bt();
    riSousMenu_destr = new RiSousMenu();
    riSousMenu_bt_destr = new RiSousMenu_bt();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    riSousMenu10 = new RiSousMenu();
    riSousMenu_bt10 = new RiSousMenu_bt();
    riSousMenu11 = new RiSousMenu();
    riSousMenu_bt11 = new RiSousMenu_bt();
    riMenu3 = new RiMenu();
    riMenu_bt3 = new RiMenu_bt();
    riSousMenu14 = new RiSousMenu();
    riSousMenu_bt14 = new RiSousMenu_bt();
    riSousMenu15 = new RiSousMenu();
    riSousMenu_bt15 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel2 = new JPanel();
    WCOD = new XRiTextField();
    L1NLI = new XRiTextField();
    L1NCG = new XRiTextField();
    L1NCA = new XRiTextField();
    LIBCPT = new XRiTextField();
    USOLDE = new XRiTextField();
    OBJ_67 = new JLabel();
    OBJ_71 = new RiZoneSortie();
    OBJ_68 = new JLabel();
    panel3 = new JPanel();
    L1DTJ = new XRiTextField();
    L1SNS = new XRiComboBox();
    L1CLB = new XRiTextField();
    L1MTT = new XRiTextField();
    OBJ_28 = new JLabel();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    L1PCE = new XRiTextField();
    L1DEV = new XRiTextField();
    WCHGX = new XRiTextField();
    L1MTD = new XRiTextField();
    L1QTE = new XRiTextField();
    OBJ_31 = new JLabel();
    OBJ_64 = new JLabel();
    OBJ_62 = new JLabel();
    OBJ_35 = new JLabel();
    OBJ_63 = new JLabel();
    panel1 = new JPanel();
    L1SAN = new XRiTextField();
    L1ACT = new XRiTextField();
    L1NAT = new XRiTextField();
    OBJ_34 = new JLabel();
    OBJ_33 = new JLabel();
    OBJ_32 = new JLabel();
    L1LIB1 = new XRiTextField();
    L1LIB2 = new XRiTextField();
    label5 = new JLabel();
    panel4 = new JPanel();
    L1IN6 = new XRiTextField();
    OBJ_53 = new JLabel();
    TTVA = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_16 = new JMenuItem();
    OBJ_17 = new JMenuItem();
    OBJ_15 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(1185, 255));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== scroll_droite ========
        {
          scroll_droite.setBackground(new Color(238, 239, 241));
          scroll_droite.setPreferredSize(new Dimension(16, 145));
          scroll_droite.setBorder(null);
          scroll_droite.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
          scroll_droite.setName("scroll_droite");
          
          // ======== menus_haut ========
          {
            menus_haut.setMinimumSize(new Dimension(160, 520));
            menus_haut.setPreferredSize(new Dimension(160, 520));
            menus_haut.setBackground(new Color(238, 239, 241));
            menus_haut.setAutoscrolls(true);
            menus_haut.setName("menus_haut");
            menus_haut.setLayout(new VerticalLayout());
            
            // ======== riMenu_V01F ========
            {
              riMenu_V01F.setMinimumSize(new Dimension(104, 50));
              riMenu_V01F.setPreferredSize(new Dimension(170, 50));
              riMenu_V01F.setMaximumSize(new Dimension(104, 50));
              riMenu_V01F.setName("riMenu_V01F");
              
              // ---- riMenu_bt_V01F ----
              riMenu_bt_V01F.setText("@V01F@");
              riMenu_bt_V01F.setPreferredSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMinimumSize(new Dimension(167, 50));
              riMenu_bt_V01F.setMaximumSize(new Dimension(170, 50));
              riMenu_bt_V01F.setFont(riMenu_bt_V01F.getFont().deriveFont(riMenu_bt_V01F.getFont().getSize() + 2f));
              riMenu_bt_V01F.setName("riMenu_bt_V01F");
              riMenu_V01F.add(riMenu_bt_V01F);
            }
            menus_haut.add(riMenu_V01F);
            
            // ======== riSousMenu_consult ========
            {
              riSousMenu_consult.setName("riSousMenu_consult");
              
              // ---- riSousMenu_bt_consult ----
              riSousMenu_bt_consult.setText("Consultation");
              riSousMenu_bt_consult.setToolTipText("Consultation");
              riSousMenu_bt_consult.setName("riSousMenu_bt_consult");
              riSousMenu_consult.add(riSousMenu_bt_consult);
            }
            menus_haut.add(riSousMenu_consult);
            
            // ======== riSousMenu_modif ========
            {
              riSousMenu_modif.setName("riSousMenu_modif");
              
              // ---- riSousMenu_bt_modif ----
              riSousMenu_bt_modif.setText("Modification");
              riSousMenu_bt_modif.setToolTipText("Modification");
              riSousMenu_bt_modif.setName("riSousMenu_bt_modif");
              riSousMenu_modif.add(riSousMenu_bt_modif);
            }
            menus_haut.add(riSousMenu_modif);
            
            // ======== riSousMenu_crea ========
            {
              riSousMenu_crea.setName("riSousMenu_crea");
              
              // ---- riSousMenu_bt_crea ----
              riSousMenu_bt_crea.setText("Cr\u00e9ation");
              riSousMenu_bt_crea.setToolTipText("Cr\u00e9ation");
              riSousMenu_bt_crea.setName("riSousMenu_bt_crea");
              riSousMenu_crea.add(riSousMenu_bt_crea);
            }
            menus_haut.add(riSousMenu_crea);
            
            // ======== riSousMenu_suppr ========
            {
              riSousMenu_suppr.setName("riSousMenu_suppr");
              
              // ---- riSousMenu_bt_suppr ----
              riSousMenu_bt_suppr.setText("Annulation");
              riSousMenu_bt_suppr.setToolTipText("Annulation");
              riSousMenu_bt_suppr.setName("riSousMenu_bt_suppr");
              riSousMenu_suppr.add(riSousMenu_bt_suppr);
            }
            menus_haut.add(riSousMenu_suppr);
            
            // ======== riSousMenuF_dupli ========
            {
              riSousMenuF_dupli.setName("riSousMenuF_dupli");
              
              // ---- riSousMenu_bt_dupli ----
              riSousMenu_bt_dupli.setText("Duplication");
              riSousMenu_bt_dupli.setToolTipText("Duplication");
              riSousMenu_bt_dupli.setName("riSousMenu_bt_dupli");
              riSousMenuF_dupli.add(riSousMenu_bt_dupli);
            }
            menus_haut.add(riSousMenuF_dupli);
            
            // ======== riSousMenu_rappel ========
            {
              riSousMenu_rappel.setName("riSousMenu_rappel");
              
              // ---- riSousMenu_bt_rappel ----
              riSousMenu_bt_rappel.setText("Rappel");
              riSousMenu_bt_rappel.setToolTipText("Rappel");
              riSousMenu_bt_rappel.setName("riSousMenu_bt_rappel");
              riSousMenu_rappel.add(riSousMenu_bt_rappel);
            }
            menus_haut.add(riSousMenu_rappel);
            
            // ======== riSousMenu_reac ========
            {
              riSousMenu_reac.setName("riSousMenu_reac");
              
              // ---- riSousMenu_bt_reac ----
              riSousMenu_bt_reac.setText("R\u00e9activation");
              riSousMenu_bt_reac.setToolTipText("R\u00e9activation");
              riSousMenu_bt_reac.setName("riSousMenu_bt_reac");
              riSousMenu_reac.add(riSousMenu_bt_reac);
            }
            menus_haut.add(riSousMenu_reac);
            
            // ======== riSousMenu_destr ========
            {
              riSousMenu_destr.setName("riSousMenu_destr");
              
              // ---- riSousMenu_bt_destr ----
              riSousMenu_bt_destr.setText("Suppression");
              riSousMenu_bt_destr.setToolTipText("Suppression");
              riSousMenu_bt_destr.setName("riSousMenu_bt_destr");
              riSousMenu_destr.add(riSousMenu_bt_destr);
            }
            menus_haut.add(riSousMenu_destr);
            
            // ======== riMenu2 ========
            {
              riMenu2.setName("riMenu2");
              
              // ---- riMenu_bt2 ----
              riMenu_bt2.setText("Options");
              riMenu_bt2.setName("riMenu_bt2");
              riMenu2.add(riMenu_bt2);
            }
            menus_haut.add(riMenu2);
            
            // ======== riSousMenu6 ========
            {
              riSousMenu6.setName("riSousMenu6");
              
              // ---- riSousMenu_bt6 ----
              riSousMenu_bt6.setText("Vue des comptes");
              riSousMenu_bt6.setToolTipText("Vue des comptes");
              riSousMenu_bt6.setName("riSousMenu_bt6");
              riSousMenu_bt6.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt6ActionPerformed(e);
                }
              });
              riSousMenu6.add(riSousMenu_bt6);
            }
            menus_haut.add(riSousMenu6);
            
            // ======== riSousMenu7 ========
            {
              riSousMenu7.setName("riSousMenu7");
              
              // ---- riSousMenu_bt7 ----
              riSousMenu_bt7.setText("Plan comptable");
              riSousMenu_bt7.setToolTipText("Plan comptable");
              riSousMenu_bt7.setName("riSousMenu_bt7");
              riSousMenu_bt7.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt7ActionPerformed(e);
                }
              });
              riSousMenu7.add(riSousMenu_bt7);
            }
            menus_haut.add(riSousMenu7);
            
            // ======== riSousMenu8 ========
            {
              riSousMenu8.setName("riSousMenu8");
              
              // ---- riSousMenu_bt8 ----
              riSousMenu_bt8.setText("Curseur sur le jour");
              riSousMenu_bt8.setToolTipText("Mise En/Hors fonction curseur positionn\u00e9 sur le jour");
              riSousMenu_bt8.setName("riSousMenu_bt8");
              riSousMenu_bt8.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt8ActionPerformed(e);
                }
              });
              riSousMenu8.add(riSousMenu_bt8);
            }
            menus_haut.add(riSousMenu8);
            
            // ======== riSousMenu9 ========
            {
              riSousMenu9.setName("riSousMenu9");
              
              // ---- riSousMenu_bt9 ----
              riSousMenu_bt9.setText("Saisie zones libell\u00e9s");
              riSousMenu_bt9.setToolTipText("Saisie des zones : code libell\u00e9  et  libell\u00e9");
              riSousMenu_bt9.setName("riSousMenu_bt9");
              riSousMenu_bt9.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt9ActionPerformed(e);
                }
              });
              riSousMenu9.add(riSousMenu_bt9);
            }
            menus_haut.add(riSousMenu9);
            
            // ======== riSousMenu10 ========
            {
              riSousMenu10.setName("riSousMenu10");
              
              // ---- riSousMenu_bt10 ----
              riSousMenu_bt10.setText("Affectations r\u00e8glements");
              riSousMenu_bt10.setToolTipText("Mise En/Hors fonction des affectations de r\u00e8glements");
              riSousMenu_bt10.setName("riSousMenu_bt10");
              riSousMenu_bt10.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt10ActionPerformed(e);
                }
              });
              riSousMenu10.add(riSousMenu_bt10);
            }
            menus_haut.add(riSousMenu10);
            
            // ======== riSousMenu11 ========
            {
              riSousMenu11.setName("riSousMenu11");
              
              // ---- riSousMenu_bt11 ----
              riSousMenu_bt11.setText("For\u00e7age en devises");
              riSousMenu_bt11.setToolTipText("For\u00e7age en saisie sur comptes en devises");
              riSousMenu_bt11.setName("riSousMenu_bt11");
              riSousMenu_bt11.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt11ActionPerformed(e);
                }
              });
              riSousMenu11.add(riSousMenu_bt11);
            }
            menus_haut.add(riSousMenu11);
            
            // ======== riMenu3 ========
            {
              riMenu3.setName("riMenu3");
              
              // ---- riMenu_bt3 ----
              riMenu_bt3.setText("Outils");
              riMenu_bt3.setName("riMenu_bt3");
              riMenu3.add(riMenu_bt3);
            }
            menus_haut.add(riMenu3);
            
            // ======== riSousMenu14 ========
            {
              riSousMenu14.setName("riSousMenu14");
              
              // ---- riSousMenu_bt14 ----
              riSousMenu_bt14.setText("Documents li\u00e9s");
              riSousMenu_bt14.setToolTipText("Documents li\u00e9s");
              riSousMenu_bt14.setName("riSousMenu_bt14");
              riSousMenu_bt14.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt14ActionPerformed(e);
                }
              });
              riSousMenu14.add(riSousMenu_bt14);
            }
            menus_haut.add(riSousMenu14);
            
            // ======== riSousMenu15 ========
            {
              riSousMenu15.setName("riSousMenu15");
              
              // ---- riSousMenu_bt15 ----
              riSousMenu_bt15.setText("Bloc-notes");
              riSousMenu_bt15.setToolTipText("Bloc-notes");
              riSousMenu_bt15.setName("riSousMenu_bt15");
              riSousMenu_bt15.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                  riSousMenu_bt15ActionPerformed(e);
                }
              });
              riSousMenu15.add(riSousMenu_bt15);
            }
            menus_haut.add(riSousMenu15);
          }
          scroll_droite.setViewportView(menus_haut);
        }
        p_menus.add(scroll_droite, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel2 ========
        {
          panel2.setBorder(new DropShadowBorder());
          panel2.setOpaque(false);
          panel2.setName("panel2");
          
          // ---- WCOD ----
          WCOD.setComponentPopupMenu(BTD);
          WCOD.setName("WCOD");
          
          // ---- L1NLI ----
          L1NLI.setComponentPopupMenu(BTD);
          L1NLI.setName("L1NLI");
          
          // ---- L1NCG ----
          L1NCG.setComponentPopupMenu(BTD);
          L1NCG.setName("L1NCG");
          
          // ---- L1NCA ----
          L1NCA.setComponentPopupMenu(BTD);
          L1NCA.setName("L1NCA");
          
          // ---- LIBCPT ----
          LIBCPT.setEditable(false);
          LIBCPT.setName("LIBCPT");
          
          // ---- USOLDE ----
          USOLDE.setComponentPopupMenu(BTD);
          USOLDE.setName("USOLDE");
          
          // ---- OBJ_67 ----
          OBJ_67.setText("Solde");
          OBJ_67.setName("OBJ_67");
          
          // ---- OBJ_71 ----
          OBJ_71.setText("@W1SNS@");
          OBJ_71.setName("OBJ_71");
          
          // ---- OBJ_68 ----
          OBJ_68.setText("@W1ATT@");
          OBJ_68.setForeground(Color.red);
          OBJ_68.setName("OBJ_68");
          
          GroupLayout panel2Layout = new GroupLayout(panel2);
          panel2.setLayout(panel2Layout);
          panel2Layout.setHorizontalGroup(panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup()
                  .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15)
                          .addComponent(WCOD, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(5, 5, 5)
                          .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, 44, GroupLayout.PREFERRED_SIZE).addGap(18, 18, 18)
                          .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                          .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                          .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE,
                          310, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(17, 17, 17)
                          .addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE).addGap(19, 19, 19)
                          .addComponent(USOLDE, GroupLayout.PREFERRED_SIZE, 116, GroupLayout.PREFERRED_SIZE).addGap(14, 14, 14)
                          .addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE))
                      .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE,
                          310, GroupLayout.PREFERRED_SIZE)))
                  .addGap(10, 10, 10)));
          panel2Layout.setVerticalGroup(panel2Layout.createParallelGroup()
              .addGroup(panel2Layout.createSequentialGroup().addGap(15, 15, 15)
                  .addGroup(panel2Layout.createParallelGroup()
                      .addComponent(WCOD, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addComponent(L1NLI, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                          .addComponent(L1NCG, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1NCA, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(2, 2, 2).addComponent(LIBCPT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                  .addGap(93, 93, 93)
                  .addGroup(panel2Layout.createParallelGroup()
                      .addGroup(panel2Layout.createSequentialGroup().addGap(4, 4, 4).addComponent(OBJ_67, GroupLayout.PREFERRED_SIZE, 20,
                          GroupLayout.PREFERRED_SIZE))
                      .addComponent(USOLDE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel2Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_71, GroupLayout.PREFERRED_SIZE,
                          GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)))
                  .addGap(6, 6, 6).addComponent(OBJ_68, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)));
        }
        p_contenu.add(panel2);
        panel2.setBounds(10, 10, 340, 235);
        
        // ======== panel3 ========
        {
          panel3.setBorder(new DropShadowBorder());
          panel3.setOpaque(false);
          panel3.setName("panel3");
          
          // ---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setName("L1DTJ");
          
          // ---- L1SNS ----
          L1SNS.setModel(new DefaultComboBoxModel(new String[] { "  ", "D\u00e9bit", "Cr\u00e9dit" }));
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          
          // ---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          
          // ---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          
          // ---- OBJ_28 ----
          OBJ_28.setText("@LIBMTT@");
          OBJ_28.setName("OBJ_28");
          
          // ---- OBJ_29 ----
          OBJ_29.setText("Sens");
          OBJ_29.setName("OBJ_29");
          
          // ---- OBJ_30 ----
          OBJ_30.setText("C");
          OBJ_30.setHorizontalAlignment(SwingConstants.CENTER);
          OBJ_30.setName("OBJ_30");
          
          // ---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          
          // ---- L1DEV ----
          L1DEV.setName("L1DEV");
          
          // ---- WCHGX ----
          WCHGX.setName("WCHGX");
          
          // ---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");
          
          // ---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          
          // ---- OBJ_31 ----
          OBJ_31.setText("N\u00b0pi\u00e8ce");
          OBJ_31.setName("OBJ_31");
          
          // ---- OBJ_64 ----
          OBJ_64.setText("Devise");
          OBJ_64.setName("OBJ_64");
          
          // ---- OBJ_62 ----
          OBJ_62.setText("Taux");
          OBJ_62.setName("OBJ_62");
          
          // ---- OBJ_35 ----
          OBJ_35.setText("le");
          OBJ_35.setName("OBJ_35");
          
          // ---- OBJ_63 ----
          OBJ_63.setText("Quantit\u00e9");
          OBJ_63.setName("OBJ_63");
          
          // ======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);
            
            // ---- L1SAN ----
            L1SAN.setComponentPopupMenu(BTD);
            L1SAN.setName("L1SAN");
            panel1.add(L1SAN);
            L1SAN.setBounds(115, 30, 50, L1SAN.getPreferredSize().height);
            
            // ---- L1ACT ----
            L1ACT.setComponentPopupMenu(BTD);
            L1ACT.setName("L1ACT");
            panel1.add(L1ACT);
            L1ACT.setBounds(170, 30, 60, L1ACT.getPreferredSize().height);
            
            // ---- L1NAT ----
            L1NAT.setComponentPopupMenu(BTD);
            L1NAT.setName("L1NAT");
            panel1.add(L1NAT);
            L1NAT.setBounds(235, 30, 60, L1NAT.getPreferredSize().height);
            
            // ---- OBJ_34 ----
            OBJ_34.setText("Nature");
            OBJ_34.setName("OBJ_34");
            panel1.add(OBJ_34);
            OBJ_34.setBounds(240, 5, 43, OBJ_34.getPreferredSize().height);
            
            // ---- OBJ_33 ----
            OBJ_33.setText("Affaire");
            OBJ_33.setName("OBJ_33");
            panel1.add(OBJ_33);
            OBJ_33.setBounds(180, 5, 42, OBJ_33.getPreferredSize().height);
            
            // ---- OBJ_32 ----
            OBJ_32.setText("Section");
            OBJ_32.setName("OBJ_32");
            panel1.add(OBJ_32);
            OBJ_32.setBounds(115, 5, 45, OBJ_32.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          
          // ---- L1LIB1 ----
          L1LIB1.setName("L1LIB1");
          
          // ---- L1LIB2 ----
          L1LIB2.setName("L1LIB2");
          
          // ---- label5 ----
          label5.setText("@LOCCOM@");
          label5.setName("label5");
          
          // ======== panel4 ========
          {
            panel4.setBorder(new TitledBorder("TVA"));
            panel4.setOpaque(false);
            panel4.setName("panel4");
            panel4.setLayout(null);
            
            // ---- L1IN6 ----
            L1IN6.setName("L1IN6");
            panel4.add(L1IN6);
            L1IN6.setBounds(20, 30, 24, L1IN6.getPreferredSize().height);
            
            // ---- OBJ_53 ----
            OBJ_53.setText("Taux");
            OBJ_53.setName("OBJ_53");
            panel4.add(OBJ_53);
            OBJ_53.setBounds(55, 35, 45, 19);
            
            // ---- TTVA ----
            TTVA.setText("@TTVA@");
            TTVA.setHorizontalAlignment(SwingConstants.RIGHT);
            TTVA.setName("TTVA");
            panel4.add(TTVA);
            TTVA.setBounds(100, 32, 44, TTVA.getPreferredSize().height);
            
            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for (int i = 0; i < panel4.getComponentCount(); i++) {
                Rectangle bounds = panel4.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel4.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel4.setMinimumSize(preferredSize);
              panel4.setPreferredSize(preferredSize);
            }
          }
          
          GroupLayout panel3Layout = new GroupLayout(panel3);
          panel3.setLayout(panel3Layout);
          panel3Layout
              .setHorizontalGroup(
                  panel3Layout.createParallelGroup()
                      .addGroup(
                          panel3Layout.createSequentialGroup().addGap(10, 10, 10)
                              .addGroup(panel3Layout.createParallelGroup().addGroup(panel3Layout.createSequentialGroup()
                                  .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 15, GroupLayout.PREFERRED_SIZE).addGap(24, 24, 24)
                                  .addComponent(OBJ_29, GroupLayout.PREFERRED_SIZE, 32, GroupLayout.PREFERRED_SIZE).addGap(54, 54, 54)
                                  .addComponent(OBJ_30, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                                  .addComponent(OBJ_28, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE).addGap(6, 6, 6)
                                  .addComponent(label5, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE).addGap(125, 125, 125)
                                  .addComponent(OBJ_31, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                                  .addGroup(panel3Layout.createSequentialGroup()
                                      .addComponent(L1DTJ, GroupLayout.PREFERRED_SIZE, 28, GroupLayout.PREFERRED_SIZE).addGap(11, 11, 11)
                                      .addComponent(L1SNS, GroupLayout.PREFERRED_SIZE, 73, GroupLayout.PREFERRED_SIZE).addGap(13, 13, 13)
                                      .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                                      .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, 94, GroupLayout.PREFERRED_SIZE).addGap(6, 6, 6)
                                      .addComponent(L1LIB1, GroupLayout.PREFERRED_SIZE, 145, GroupLayout.PREFERRED_SIZE)
                                      .addGap(35, 35, 35).addComponent(L1LIB2, GroupLayout.PREFERRED_SIZE, 80, GroupLayout.PREFERRED_SIZE)
                                      .addGap(10, 10, 10).addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, 68, GroupLayout.PREFERRED_SIZE))
                                  .addGroup(panel3Layout.createSequentialGroup().addGroup(panel3Layout.createParallelGroup()
                                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 46, GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel3Layout.createSequentialGroup().addGap(43, 43, 43).addComponent(OBJ_62,
                                          GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE))
                                      .addGroup(panel3Layout.createSequentialGroup()
                                          .addComponent(L1DEV, GroupLayout.PREFERRED_SIZE, 40, GroupLayout.PREFERRED_SIZE).addGap(1, 1, 1)
                                          .addComponent(WCHGX, GroupLayout.PREFERRED_SIZE, 90, GroupLayout.PREFERRED_SIZE)))
                                      .addGap(1, 1, 1).addComponent(L1MTD, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                      .addGap(1, 1, 1)
                                      .addGroup(panel3Layout.createParallelGroup()
                                          .addComponent(L1QTE, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                                          .addGroup(panel3Layout.createSequentialGroup().addGap(92, 92, 92).addComponent(panel1,
                                              GroupLayout.PREFERRED_SIZE, 300, GroupLayout.PREFERRED_SIZE))
                                          .addGroup(panel3Layout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_63,
                                              GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)))))
                              .addGap(10, 10, 10))
                      .addGroup(GroupLayout.Alignment.TRAILING, panel3Layout.createSequentialGroup().addContainerGap()
                          .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 170, GroupLayout.PREFERRED_SIZE).addContainerGap()));
          panel3Layout
              .setVerticalGroup(panel3Layout.createParallelGroup()
                  .addGroup(panel3Layout.createSequentialGroup().addGap(20, 20, 20)
                      .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(OBJ_35, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                          .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_29))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_30))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_28))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(label5))
                          .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_31)))
                      .addGap(4, 4, 4)
                      .addGroup(panel3Layout.createParallelGroup()
                          .addComponent(L1DTJ, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1SNS, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1CLB, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1MTT, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1LIB1, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1LIB2, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                          .addComponent(L1PCE, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                      .addGap(2, 2, 2)
                      .addGroup(
                          panel3Layout.createParallelGroup()
                              .addGroup(panel3Layout
                                  .createSequentialGroup().addGap(5, 5, 5)
                                  .addGroup(panel3Layout.createParallelGroup()
                                      .addComponent(OBJ_64, GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE)
                                      .addGroup(panel3Layout.createSequentialGroup().addGap(2, 2, 2).addComponent(OBJ_62)))
                                  .addGap(5, 5, 5)
                                  .addGroup(panel3Layout.createParallelGroup()
                                      .addComponent(L1DEV, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)
                                      .addComponent(WCHGX, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                          GroupLayout.PREFERRED_SIZE)))
                              .addGroup(panel3Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(L1MTD,
                                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addGroup(panel3Layout.createSequentialGroup().addGap(30, 30, 30).addComponent(L1QTE,
                                  GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                              .addGroup(panel3Layout.createSequentialGroup().addGap(7, 7, 7).addComponent(OBJ_63)))
                      .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                      .addComponent(panel4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addContainerGap()));
        }
        p_contenu.add(panel3);
        panel3.setBounds(355, 10, 650, 235);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_16 ----
      OBJ_16.setText("Duplication");
      OBJ_16.setName("OBJ_16");
      OBJ_16.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_16ActionPerformed(e);
        }
      });
      BTD.add(OBJ_16);
      
      // ---- OBJ_17 ----
      OBJ_17.setText("Choix possibles");
      OBJ_17.setName("OBJ_17");
      OBJ_17.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_17ActionPerformed(e);
        }
      });
      BTD.add(OBJ_17);
      
      // ---- OBJ_15 ----
      OBJ_15.setText("Aide en ligne");
      OBJ_15.setName("OBJ_15");
      OBJ_15.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_15ActionPerformed(e);
        }
      });
      BTD.add(OBJ_15);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JScrollPane scroll_droite;
  private JPanel menus_haut;
  private RiMenu riMenu_V01F;
  private RiMenu_bt riMenu_bt_V01F;
  private RiSousMenu riSousMenu_consult;
  private RiSousMenu_bt riSousMenu_bt_consult;
  private RiSousMenu riSousMenu_modif;
  private RiSousMenu_bt riSousMenu_bt_modif;
  private RiSousMenu riSousMenu_crea;
  private RiSousMenu_bt riSousMenu_bt_crea;
  private RiSousMenu riSousMenu_suppr;
  private RiSousMenu_bt riSousMenu_bt_suppr;
  private RiSousMenu riSousMenuF_dupli;
  private RiSousMenu_bt riSousMenu_bt_dupli;
  private RiSousMenu riSousMenu_rappel;
  private RiSousMenu_bt riSousMenu_bt_rappel;
  private RiSousMenu riSousMenu_reac;
  private RiSousMenu_bt riSousMenu_bt_reac;
  private RiSousMenu riSousMenu_destr;
  private RiSousMenu_bt riSousMenu_bt_destr;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private RiSousMenu riSousMenu10;
  private RiSousMenu_bt riSousMenu_bt10;
  private RiSousMenu riSousMenu11;
  private RiSousMenu_bt riSousMenu_bt11;
  private RiMenu riMenu3;
  private RiMenu_bt riMenu_bt3;
  private RiSousMenu riSousMenu14;
  private RiSousMenu_bt riSousMenu_bt14;
  private RiSousMenu riSousMenu15;
  private RiSousMenu_bt riSousMenu_bt15;
  private JPanel p_contenu;
  private JPanel panel2;
  private XRiTextField WCOD;
  private XRiTextField L1NLI;
  private XRiTextField L1NCG;
  private XRiTextField L1NCA;
  private XRiTextField LIBCPT;
  private XRiTextField USOLDE;
  private JLabel OBJ_67;
  private RiZoneSortie OBJ_71;
  private JLabel OBJ_68;
  private JPanel panel3;
  private XRiTextField L1DTJ;
  private XRiComboBox L1SNS;
  private XRiTextField L1CLB;
  private XRiTextField L1MTT;
  private JLabel OBJ_28;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private XRiTextField L1PCE;
  private XRiTextField L1DEV;
  private XRiTextField WCHGX;
  private XRiTextField L1MTD;
  private XRiTextField L1QTE;
  private JLabel OBJ_31;
  private JLabel OBJ_64;
  private JLabel OBJ_62;
  private JLabel OBJ_35;
  private JLabel OBJ_63;
  private JPanel panel1;
  private XRiTextField L1SAN;
  private XRiTextField L1ACT;
  private XRiTextField L1NAT;
  private JLabel OBJ_34;
  private JLabel OBJ_33;
  private JLabel OBJ_32;
  private XRiTextField L1LIB1;
  private XRiTextField L1LIB2;
  private JLabel label5;
  private JPanel panel4;
  private XRiTextField L1IN6;
  private JLabel OBJ_53;
  private RiZoneSortie TTVA;
  private JPopupMenu BTD;
  private JMenuItem OBJ_16;
  private JMenuItem OBJ_17;
  private JMenuItem OBJ_15;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
