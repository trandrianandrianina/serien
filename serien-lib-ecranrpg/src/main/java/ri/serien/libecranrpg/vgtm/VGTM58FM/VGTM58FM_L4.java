
package ri.serien.libecranrpg.vgtm.VGTM58FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Emmanuel MARCQ
 */
public class VGTM58FM_L4 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] _LA01_Top = { "WTP01", "WTP02", "WTP03", "WTP04", "WTP05", "WTP06", "WTP07", "WTP08", "WTP09", "WTP10", "WTP11", "WTP12",
      "WTP13", "WTP14", "WTP15", "WT16" };
  private String[] _LA01_Title = { "Date  N°Pièce Libellé        Mt Facture Mtt. Réglé    Solde   Aff", };
  private String[][] _LA01_Data = { { "LA01", "MTA01", }, { "LA02", "MTA02", }, { "LA03", "MTA03", }, { "LA04", "MTA04", },
      { "LA05", "MTA05", }, { "LA06", "MTA06", }, { "LA07", "MTA07", }, { "LA08", "MTA08", }, { "LA09", "MTA09", }, { "LA10", "MTA10", },
      { "LA11", "MTA11", }, { "LA12", "MTA12", }, { "LA13", "MTA13", }, { "LA14", "MTA14", }, { "LA15", "MTA15", }, { "LA16", "MTA16", }, };
  private int[] _LA01_Width = { 459, 81, };
  
  public VGTM58FM_L4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    LA01.setAspectTable(_LA01_Top, _LA01_Title, _LA01_Data, _LA01_Width, false, null, null, null, null);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    WDIFE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIFE@")).trim());
    TOTAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTAFF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    OBJ_690.setVisible(lexique.isPresent("WDIFE"));
    
    // TODO Icones
    
    // Titre
    setTitle(interpreteurD.analyseExpression("FM@LOCGRP/+1/@ - Recherche factures rattachées N° centrale"));
    
    

    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F6", false);
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    WDIFE = new RiZoneSortie();
    TOTAFF = new RiZoneSortie();
    OBJ_690 = new JLabel();
    SCROLLPANE_LIST3 = new JScrollPane();
    LA01 = new XRiTable();
    MTA01 = new XRiTextField();
    MTA02 = new XRiTextField();
    MTA03 = new XRiTextField();
    MTA04 = new XRiTextField();
    MTA05 = new XRiTextField();
    MTA06 = new XRiTextField();
    MTA07 = new XRiTextField();
    MTA08 = new XRiTextField();
    MTA09 = new XRiTextField();
    MTA10 = new XRiTextField();
    MTA16 = new XRiTextField();
    MTA14 = new XRiTextField();
    MTA15 = new XRiTextField();
    MTA13 = new XRiTextField();
    MTA12 = new XRiTextField();
    MTA11 = new XRiTextField();
    label1 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(875, 445));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setPreferredSize(new Dimension(940, 445));
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setComponentPopupMenu(null);
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setComponentPopupMenu(null);
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setComponentPopupMenu(null);
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setComponentPopupMenu(null);
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setComponentPopupMenu(null);
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("On/Off Saisie montant");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Confirmation s\u00e9lection");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);

          //======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");

            //---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Vue 2");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setPreferredSize(new Dimension(940, 445));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- WDIFE ----
          WDIFE.setText("@WDIFE@");
          WDIFE.setHorizontalAlignment(SwingConstants.RIGHT);
          WDIFE.setName("WDIFE");
          panel1.add(WDIFE);
          WDIFE.setBounds(new Rectangle(new Point(430, 385), WDIFE.getPreferredSize()));

          //---- TOTAFF ----
          TOTAFF.setText("@TOTAFF@");
          TOTAFF.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTAFF.setName("TOTAFF");
          panel1.add(TOTAFF);
          TOTAFF.setBounds(new Rectangle(new Point(535, 385), TOTAFF.getPreferredSize()));

          //---- OBJ_690 ----
          OBJ_690.setText("Ecart");
          OBJ_690.setName("OBJ_690");
          panel1.add(OBJ_690);
          OBJ_690.setBounds(370, 385, 60, 20);

          //======== SCROLLPANE_LIST3 ========
          {
            SCROLLPANE_LIST3.setName("SCROLLPANE_LIST3");

            //---- LA01 ----
            LA01.setRowHeight(20);
            LA01.setRowSelectionAllowed(false);
            LA01.setName("LA01");
            SCROLLPANE_LIST3.setViewportView(LA01);
          }
          panel1.add(SCROLLPANE_LIST3);
          SCROLLPANE_LIST3.setBounds(20, 20, 515, 350);

          //---- MTA01 ----
          MTA01.setName("MTA01");
          panel1.add(MTA01);
          MTA01.setBounds(535, 45, 100, 25);

          //---- MTA02 ----
          MTA02.setName("MTA02");
          panel1.add(MTA02);
          MTA02.setBounds(535, 65, 100, 25);

          //---- MTA03 ----
          MTA03.setName("MTA03");
          panel1.add(MTA03);
          MTA03.setBounds(535, 85, 100, 25);

          //---- MTA04 ----
          MTA04.setName("MTA04");
          panel1.add(MTA04);
          MTA04.setBounds(535, 105, 100, 25);

          //---- MTA05 ----
          MTA05.setName("MTA05");
          panel1.add(MTA05);
          MTA05.setBounds(535, 125, 100, 25);

          //---- MTA06 ----
          MTA06.setName("MTA06");
          panel1.add(MTA06);
          MTA06.setBounds(535, 145, 100, 25);

          //---- MTA07 ----
          MTA07.setName("MTA07");
          panel1.add(MTA07);
          MTA07.setBounds(535, 165, 100, 25);

          //---- MTA08 ----
          MTA08.setName("MTA08");
          panel1.add(MTA08);
          MTA08.setBounds(535, 185, 100, 25);

          //---- MTA09 ----
          MTA09.setName("MTA09");
          panel1.add(MTA09);
          MTA09.setBounds(535, 205, 100, 25);

          //---- MTA10 ----
          MTA10.setName("MTA10");
          panel1.add(MTA10);
          MTA10.setBounds(535, 225, 100, 25);

          //---- MTA16 ----
          MTA16.setName("MTA16");
          panel1.add(MTA16);
          MTA16.setBounds(535, 345, 100, 25);

          //---- MTA14 ----
          MTA14.setName("MTA14");
          panel1.add(MTA14);
          MTA14.setBounds(535, 305, 100, 25);

          //---- MTA15 ----
          MTA15.setName("MTA15");
          panel1.add(MTA15);
          MTA15.setBounds(535, 325, 100, 25);

          //---- MTA13 ----
          MTA13.setName("MTA13");
          panel1.add(MTA13);
          MTA13.setBounds(535, 285, 100, 25);

          //---- MTA12 ----
          MTA12.setName("MTA12");
          panel1.add(MTA12);
          MTA12.setBounds(535, 265, 100, 25);

          //---- MTA11 ----
          MTA11.setName("MTA11");
          panel1.add(MTA11);
          MTA11.setBounds(535, 245, 100, 25);

          //---- label1 ----
          label1.setText("Montant affect\u00e9");
          label1.setHorizontalAlignment(SwingConstants.CENTER);
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(530, 20, 105, 25);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(645, 20, 25, 150);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(645, 220, 25, 150);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 685, 425);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie WDIFE;
  private RiZoneSortie TOTAFF;
  private JLabel OBJ_690;
  private JScrollPane SCROLLPANE_LIST3;
  private XRiTable LA01;
  private XRiTextField MTA01;
  private XRiTextField MTA02;
  private XRiTextField MTA03;
  private XRiTextField MTA04;
  private XRiTextField MTA05;
  private XRiTextField MTA06;
  private XRiTextField MTA07;
  private XRiTextField MTA08;
  private XRiTextField MTA09;
  private XRiTextField MTA10;
  private XRiTextField MTA16;
  private XRiTextField MTA14;
  private XRiTextField MTA15;
  private XRiTextField MTA13;
  private XRiTextField MTA12;
  private XRiTextField MTA11;
  private JLabel label1;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
