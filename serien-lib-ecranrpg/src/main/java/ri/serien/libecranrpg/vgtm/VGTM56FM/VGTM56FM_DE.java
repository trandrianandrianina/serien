
package ri.serien.libecranrpg.vgtm.VGTM56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM56FM_DE extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGTM56FM_DE(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    ORILIB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORILIB@")).trim());
    ORIPCE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORIPCE@")).trim());
    ORITDB.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORITDB@")).trim());
    ORITCR.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ORITCR@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    // Titre
    setTitle(interpreteurD.analyseExpression("Découpage d'écriture"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    ORILIB = new RiZoneSortie();
    ORIPCE = new RiZoneSortie();
    ORITDB = new RiZoneSortie();
    ORITCR = new RiZoneSortie();
    LIB1 = new XRiTextField();
    LIB2 = new XRiTextField();
    LIB3 = new XRiTextField();
    LIB4 = new XRiTextField();
    LIB5 = new XRiTextField();
    PCE1 = new JTextField();
    PCE2 = new JTextField();
    PCE3 = new JTextField();
    PCE4 = new JTextField();
    LIB6 = new XRiTextField();
    LIB7 = new XRiTextField();
    PCE5 = new JTextField();
    PCE6 = new JTextField();
    PCE7 = new JTextField();
    TDB1 = new XRiTextField();
    TDB2 = new XRiTextField();
    TDB3 = new XRiTextField();
    TDB4 = new XRiTextField();
    TDB5 = new XRiTextField();
    TDB6 = new XRiTextField();
    TDB7 = new XRiTextField();
    TCR1 = new XRiTextField();
    TCR2 = new XRiTextField();
    TCR3 = new XRiTextField();
    TCR4 = new XRiTextField();
    TCR5 = new XRiTextField();
    TCR6 = new XRiTextField();
    TCR7 = new XRiTextField();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(835, 370));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Ecriture \u00e0 d\u00e9couper"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- ORILIB ----
          ORILIB.setText("@ORILIB@");
          ORILIB.setName("ORILIB");
          panel1.add(ORILIB);
          ORILIB.setBounds(25, 45, 270, ORILIB.getPreferredSize().height);

          //---- ORIPCE ----
          ORIPCE.setText("@ORIPCE@");
          ORIPCE.setHorizontalAlignment(SwingConstants.RIGHT);
          ORIPCE.setName("ORIPCE");
          panel1.add(ORIPCE);
          ORIPCE.setBounds(306, 45, 66, ORIPCE.getPreferredSize().height);

          //---- ORITDB ----
          ORITDB.setText("@ORITDB@");
          ORITDB.setHorizontalAlignment(SwingConstants.RIGHT);
          ORITDB.setName("ORITDB");
          panel1.add(ORITDB);
          ORITDB.setBounds(385, 45, 100, ORITDB.getPreferredSize().height);

          //---- ORITCR ----
          ORITCR.setText("@ORITCR@");
          ORITCR.setHorizontalAlignment(SwingConstants.RIGHT);
          ORITCR.setName("ORITCR");
          panel1.add(ORITCR);
          ORITCR.setBounds(495, 45, 100, 24);

          //---- LIB1 ----
          LIB1.setName("LIB1");
          panel1.add(LIB1);
          LIB1.setBounds(25, 93, 270, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setName("LIB2");
          panel1.add(LIB2);
          LIB2.setBounds(25, 123, 270, LIB2.getPreferredSize().height);

          //---- LIB3 ----
          LIB3.setName("LIB3");
          panel1.add(LIB3);
          LIB3.setBounds(25, 153, 270, LIB3.getPreferredSize().height);

          //---- LIB4 ----
          LIB4.setName("LIB4");
          panel1.add(LIB4);
          LIB4.setBounds(25, 183, 270, LIB4.getPreferredSize().height);

          //---- LIB5 ----
          LIB5.setName("LIB5");
          panel1.add(LIB5);
          LIB5.setBounds(25, 213, 270, LIB5.getPreferredSize().height);

          //---- PCE1 ----
          PCE1.setName("PCE1");
          panel1.add(PCE1);
          PCE1.setBounds(305, 93, 68, PCE1.getPreferredSize().height);

          //---- PCE2 ----
          PCE2.setName("PCE2");
          panel1.add(PCE2);
          PCE2.setBounds(305, 123, 68, 28);

          //---- PCE3 ----
          PCE3.setName("PCE3");
          panel1.add(PCE3);
          PCE3.setBounds(305, 153, 68, 28);

          //---- PCE4 ----
          PCE4.setName("PCE4");
          panel1.add(PCE4);
          PCE4.setBounds(305, 183, 68, 28);

          //---- LIB6 ----
          LIB6.setName("LIB6");
          panel1.add(LIB6);
          LIB6.setBounds(25, 243, 270, LIB6.getPreferredSize().height);

          //---- LIB7 ----
          LIB7.setName("LIB7");
          panel1.add(LIB7);
          LIB7.setBounds(25, 273, 270, LIB7.getPreferredSize().height);

          //---- PCE5 ----
          PCE5.setName("PCE5");
          panel1.add(PCE5);
          PCE5.setBounds(305, 213, 68, 28);

          //---- PCE6 ----
          PCE6.setName("PCE6");
          panel1.add(PCE6);
          PCE6.setBounds(305, 243, 68, 28);

          //---- PCE7 ----
          PCE7.setName("PCE7");
          panel1.add(PCE7);
          PCE7.setBounds(305, 273, 68, 28);

          //---- TDB1 ----
          TDB1.setName("TDB1");
          panel1.add(TDB1);
          TDB1.setBounds(384, 93, 100, TDB1.getPreferredSize().height);

          //---- TDB2 ----
          TDB2.setName("TDB2");
          panel1.add(TDB2);
          TDB2.setBounds(383, 125, 100, 28);

          //---- TDB3 ----
          TDB3.setName("TDB3");
          panel1.add(TDB3);
          TDB3.setBounds(383, 155, 100, 28);

          //---- TDB4 ----
          TDB4.setName("TDB4");
          panel1.add(TDB4);
          TDB4.setBounds(383, 185, 100, 28);

          //---- TDB5 ----
          TDB5.setName("TDB5");
          panel1.add(TDB5);
          TDB5.setBounds(383, 215, 100, 28);

          //---- TDB6 ----
          TDB6.setName("TDB6");
          panel1.add(TDB6);
          TDB6.setBounds(383, 245, 100, 28);

          //---- TDB7 ----
          TDB7.setName("TDB7");
          panel1.add(TDB7);
          TDB7.setBounds(383, 275, 100, 28);

          //---- TCR1 ----
          TCR1.setName("TCR1");
          panel1.add(TCR1);
          TCR1.setBounds(495, 93, 100, 28);

          //---- TCR2 ----
          TCR2.setName("TCR2");
          panel1.add(TCR2);
          TCR2.setBounds(495, 125, 100, 28);

          //---- TCR3 ----
          TCR3.setName("TCR3");
          panel1.add(TCR3);
          TCR3.setBounds(495, 155, 100, 28);

          //---- TCR4 ----
          TCR4.setName("TCR4");
          panel1.add(TCR4);
          TCR4.setBounds(495, 185, 100, 28);

          //---- TCR5 ----
          TCR5.setName("TCR5");
          panel1.add(TCR5);
          TCR5.setBounds(495, 215, 100, 28);

          //---- TCR6 ----
          TCR6.setName("TCR6");
          panel1.add(TCR6);
          TCR6.setBounds(495, 245, 100, 28);

          //---- TCR7 ----
          TCR7.setName("TCR7");
          panel1.add(TCR7);
          TCR7.setBounds(495, 275, 100, 28);

          //---- label1 ----
          label1.setText("Nature de l'op\u00e9ration");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(30, 70, 260, 25);

          //---- label2 ----
          label2.setText("N\u00b0 pi\u00e8ce");
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(310, 70, 60, 25);

          //---- label3 ----
          label3.setText("D\u00e9bit");
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(390, 70, 90, 25);

          //---- label4 ----
          label4.setText("Cr\u00e9dit");
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(500, 70, 90, 25);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }

        GroupLayout p_contenuLayout = new GroupLayout(p_contenu);
        p_contenu.setLayout(p_contenuLayout);
        p_contenuLayout.setHorizontalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 625, GroupLayout.PREFERRED_SIZE))
        );
        p_contenuLayout.setVerticalGroup(
          p_contenuLayout.createParallelGroup()
            .addGroup(p_contenuLayout.createSequentialGroup()
              .addGap(20, 20, 20)
              .addComponent(panel1, GroupLayout.PREFERRED_SIZE, 325, GroupLayout.PREFERRED_SIZE))
        );
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel panel1;
  private RiZoneSortie ORILIB;
  private RiZoneSortie ORIPCE;
  private RiZoneSortie ORITDB;
  private RiZoneSortie ORITCR;
  private XRiTextField LIB1;
  private XRiTextField LIB2;
  private XRiTextField LIB3;
  private XRiTextField LIB4;
  private XRiTextField LIB5;
  private JTextField PCE1;
  private JTextField PCE2;
  private JTextField PCE3;
  private JTextField PCE4;
  private XRiTextField LIB6;
  private XRiTextField LIB7;
  private JTextField PCE5;
  private JTextField PCE6;
  private JTextField PCE7;
  private XRiTextField TDB1;
  private XRiTextField TDB2;
  private XRiTextField TDB3;
  private XRiTextField TDB4;
  private XRiTextField TDB5;
  private XRiTextField TDB6;
  private XRiTextField TDB7;
  private XRiTextField TCR1;
  private XRiTextField TCR2;
  private XRiTextField TCR3;
  private XRiTextField TCR4;
  private XRiTextField TCR5;
  private XRiTextField TCR6;
  private XRiTextField TCR7;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
