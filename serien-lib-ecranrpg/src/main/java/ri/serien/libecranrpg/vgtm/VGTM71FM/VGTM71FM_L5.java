
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.GroupLayout;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.JXTitledPanel;
import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_L5 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _WDERLD_Title = { "HLD02", };
  private String[][] _WDERLD_Data = { { "WDERLD", }, };
  private int[] _WDERLD_Width = { 75, };
  
  /**
   * Constructeur.
   */
  public VGTM71FM_L5(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    WDERLD.setAspectTable(null, _WDERLD_Title, _WDERLD_Data, _WDERLD_Width, true, null, null, null, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    label1.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMTT@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT1@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBMT2@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    WECVTL.setVisible(lexique.isTrue("78"));
    OBJ_30_OBJ_30.setVisible(lexique.isTrue("78"));
    OBJ_32_OBJ_32.setVisible(lexique.isTrue("78"));
    WMTVTL.setVisible(lexique.isTrue("78"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F7", true);
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", true);
  }
  
  private void OBJ_11ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    OBJ_30_OBJ_30 = new JLabel();
    WMTVTL = new XRiTextField();
    WECVTL = new XRiTextField();
    OBJ_32_OBJ_32 = new JLabel();
    xTitledPanel1 = new JXTitledPanel();
    label1 = new JLabel();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    WVM01 = new XRiTextField();
    WVS01 = new XRiTextField();
    WVA01 = new XRiTextField();
    WVM02 = new XRiTextField();
    WVS02 = new XRiTextField();
    WVA02 = new XRiTextField();
    WVM03 = new XRiTextField();
    WVS03 = new XRiTextField();
    WVA03 = new XRiTextField();
    WVM04 = new XRiTextField();
    WVS04 = new XRiTextField();
    WVA04 = new XRiTextField();
    WVM05 = new XRiTextField();
    WVS05 = new XRiTextField();
    WVA05 = new XRiTextField();
    WVM06 = new XRiTextField();
    WVS06 = new XRiTextField();
    WVA06 = new XRiTextField();
    WVM07 = new XRiTextField();
    WVS07 = new XRiTextField();
    WVA07 = new XRiTextField();
    WVM08 = new XRiTextField();
    WVS08 = new XRiTextField();
    WVA08 = new XRiTextField();
    WVM09 = new XRiTextField();
    WVS09 = new XRiTextField();
    WVA09 = new XRiTextField();
    WVM10 = new XRiTextField();
    WVS10 = new XRiTextField();
    WVA10 = new XRiTextField();
    WVM11 = new XRiTextField();
    WVS11 = new XRiTextField();
    WVA11 = new XRiTextField();
    WVM12 = new XRiTextField();
    WVS12 = new XRiTextField();
    WVA12 = new XRiTextField();
    WVM13 = new XRiTextField();
    WVS13 = new XRiTextField();
    WVA13 = new XRiTextField();
    WVM14 = new XRiTextField();
    WVS14 = new XRiTextField();
    WVA14 = new XRiTextField();
    WVM15 = new XRiTextField();
    WVS15 = new XRiTextField();
    WVA15 = new XRiTextField();
    WVM16 = new XRiTextField();
    WVS16 = new XRiTextField();
    WVA16 = new XRiTextField();
    WVM17 = new XRiTextField();
    WVS17 = new XRiTextField();
    WVA17 = new XRiTextField();
    WVM18 = new XRiTextField();
    WVS18 = new XRiTextField();
    WVA18 = new XRiTextField();
    WVM19 = new XRiTextField();
    WVS19 = new XRiTextField();
    WVA19 = new XRiTextField();
    WVM20 = new XRiTextField();
    WVS20 = new XRiTextField();
    WVA20 = new XRiTextField();
    WVM21 = new XRiTextField();
    WVS21 = new XRiTextField();
    WVA21 = new XRiTextField();
    WVM22 = new XRiTextField();
    WVS22 = new XRiTextField();
    WVA22 = new XRiTextField();
    WVM23 = new XRiTextField();
    WVS23 = new XRiTextField();
    WVA23 = new XRiTextField();
    WVM24 = new XRiTextField();
    WVS24 = new XRiTextField();
    WVA24 = new XRiTextField();
    WVM25 = new XRiTextField();
    WVS25 = new XRiTextField();
    WVA25 = new XRiTextField();
    WVM26 = new XRiTextField();
    WVS26 = new XRiTextField();
    WVA26 = new XRiTextField();
    WVM27 = new XRiTextField();
    WVS27 = new XRiTextField();
    WVA27 = new XRiTextField();
    WVM28 = new XRiTextField();
    WVS28 = new XRiTextField();
    WVA28 = new XRiTextField();
    WVM29 = new XRiTextField();
    WVS29 = new XRiTextField();
    WVA29 = new XRiTextField();
    WVM30 = new XRiTextField();
    WVS30 = new XRiTextField();
    WVA30 = new XRiTextField();
    WVM31 = new XRiTextField();
    WVS31 = new XRiTextField();
    WVA31 = new XRiTextField();
    WVM32 = new XRiTextField();
    WVS32 = new XRiTextField();
    WVA32 = new XRiTextField();
    WVM33 = new XRiTextField();
    WVS33 = new XRiTextField();
    WVA33 = new XRiTextField();
    WVM34 = new XRiTextField();
    WVS34 = new XRiTextField();
    WVA34 = new XRiTextField();
    WVM35 = new XRiTextField();
    WVS35 = new XRiTextField();
    WVA35 = new XRiTextField();
    WVM36 = new XRiTextField();
    WVS36 = new XRiTextField();
    WVA36 = new XRiTextField();
    WVM37 = new XRiTextField();
    WVS37 = new XRiTextField();
    WVA37 = new XRiTextField();
    WVM38 = new XRiTextField();
    WVS38 = new XRiTextField();
    WVA38 = new XRiTextField();
    WVM39 = new XRiTextField();
    WVS39 = new XRiTextField();
    WVA39 = new XRiTextField();
    WVM40 = new XRiTextField();
    WVS40 = new XRiTextField();
    WVA40 = new XRiTextField();
    WVM41 = new XRiTextField();
    WVS41 = new XRiTextField();
    WVA41 = new XRiTextField();
    WVM42 = new XRiTextField();
    WVS42 = new XRiTextField();
    WVA42 = new XRiTextField();
    scrollPane1 = new JScrollPane();
    WDERLD = new XRiTable();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_11 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(925, 590));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("curseur sur le jour");
            riSousMenu_bt6.setToolTipText("Mise En/Hors fonction curseur positionn\u00e9 sur le jour");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          
          // ---- OBJ_30_OBJ_30 ----
          OBJ_30_OBJ_30.setText("Montant ventillation");
          OBJ_30_OBJ_30.setName("OBJ_30_OBJ_30");
          
          // ---- WMTVTL ----
          WMTVTL.setName("WMTVTL");
          
          // ---- WECVTL ----
          WECVTL.setComponentPopupMenu(BTD);
          WECVTL.setFont(WECVTL.getFont().deriveFont(WECVTL.getFont().getStyle() | Font.BOLD));
          WECVTL.setForeground(Color.red);
          WECVTL.setName("WECVTL");
          
          // ---- OBJ_32_OBJ_32 ----
          OBJ_32_OBJ_32.setText("Ecart");
          OBJ_32_OBJ_32.setFont(OBJ_32_OBJ_32.getFont().deriveFont(OBJ_32_OBJ_32.getFont().getStyle() | Font.BOLD));
          OBJ_32_OBJ_32.setName("OBJ_32_OBJ_32");
          
          // ======== xTitledPanel1 ========
          {
            xTitledPanel1.setTitle("Votre s\u00e9lection");
            xTitledPanel1.setName("xTitledPanel1");
            Container xTitledPanel1ContentContainer = xTitledPanel1.getContentContainer();
            
            // ---- label1 ----
            label1.setText("@LIBMTT@");
            label1.setName("label1");
            
            // ---- label2 ----
            label2.setText("Sect");
            label2.setName("label2");
            
            // ---- label3 ----
            label3.setText("Aff");
            label3.setName("label3");
            
            // ---- label4 ----
            label4.setText("@LIBMT1@");
            label4.setName("label4");
            
            // ---- label5 ----
            label5.setText("Sect");
            label5.setName("label5");
            
            // ---- label6 ----
            label6.setText("Aff");
            label6.setName("label6");
            
            // ---- label7 ----
            label7.setText("@LIBMT2@");
            label7.setName("label7");
            
            // ---- label8 ----
            label8.setText("Sect");
            label8.setName("label8");
            
            // ---- label9 ----
            label9.setText("Aff");
            label9.setName("label9");
            
            // ---- WVM01 ----
            WVM01.setName("WVM01");
            
            // ---- WVS01 ----
            WVS01.setComponentPopupMenu(BTD);
            WVS01.setName("WVS01");
            
            // ---- WVA01 ----
            WVA01.setName("WVA01");
            
            // ---- WVM02 ----
            WVM02.setName("WVM02");
            
            // ---- WVS02 ----
            WVS02.setComponentPopupMenu(BTD);
            WVS02.setName("WVS02");
            
            // ---- WVA02 ----
            WVA02.setName("WVA02");
            
            // ---- WVM03 ----
            WVM03.setName("WVM03");
            
            // ---- WVS03 ----
            WVS03.setComponentPopupMenu(BTD);
            WVS03.setName("WVS03");
            
            // ---- WVA03 ----
            WVA03.setName("WVA03");
            
            // ---- WVM04 ----
            WVM04.setName("WVM04");
            
            // ---- WVS04 ----
            WVS04.setComponentPopupMenu(BTD);
            WVS04.setName("WVS04");
            
            // ---- WVA04 ----
            WVA04.setName("WVA04");
            
            // ---- WVM05 ----
            WVM05.setName("WVM05");
            
            // ---- WVS05 ----
            WVS05.setComponentPopupMenu(BTD);
            WVS05.setName("WVS05");
            
            // ---- WVA05 ----
            WVA05.setName("WVA05");
            
            // ---- WVM06 ----
            WVM06.setName("WVM06");
            
            // ---- WVS06 ----
            WVS06.setComponentPopupMenu(BTD);
            WVS06.setName("WVS06");
            
            // ---- WVA06 ----
            WVA06.setName("WVA06");
            
            // ---- WVM07 ----
            WVM07.setName("WVM07");
            
            // ---- WVS07 ----
            WVS07.setComponentPopupMenu(BTD);
            WVS07.setName("WVS07");
            
            // ---- WVA07 ----
            WVA07.setName("WVA07");
            
            // ---- WVM08 ----
            WVM08.setName("WVM08");
            
            // ---- WVS08 ----
            WVS08.setComponentPopupMenu(BTD);
            WVS08.setName("WVS08");
            
            // ---- WVA08 ----
            WVA08.setName("WVA08");
            
            // ---- WVM09 ----
            WVM09.setName("WVM09");
            
            // ---- WVS09 ----
            WVS09.setComponentPopupMenu(BTD);
            WVS09.setName("WVS09");
            
            // ---- WVA09 ----
            WVA09.setName("WVA09");
            
            // ---- WVM10 ----
            WVM10.setName("WVM10");
            
            // ---- WVS10 ----
            WVS10.setComponentPopupMenu(BTD);
            WVS10.setName("WVS10");
            
            // ---- WVA10 ----
            WVA10.setName("WVA10");
            
            // ---- WVM11 ----
            WVM11.setName("WVM11");
            
            // ---- WVS11 ----
            WVS11.setComponentPopupMenu(BTD);
            WVS11.setName("WVS11");
            
            // ---- WVA11 ----
            WVA11.setName("WVA11");
            
            // ---- WVM12 ----
            WVM12.setName("WVM12");
            
            // ---- WVS12 ----
            WVS12.setComponentPopupMenu(BTD);
            WVS12.setName("WVS12");
            
            // ---- WVA12 ----
            WVA12.setName("WVA12");
            
            // ---- WVM13 ----
            WVM13.setName("WVM13");
            
            // ---- WVS13 ----
            WVS13.setComponentPopupMenu(BTD);
            WVS13.setName("WVS13");
            
            // ---- WVA13 ----
            WVA13.setName("WVA13");
            
            // ---- WVM14 ----
            WVM14.setName("WVM14");
            
            // ---- WVS14 ----
            WVS14.setComponentPopupMenu(BTD);
            WVS14.setName("WVS14");
            
            // ---- WVA14 ----
            WVA14.setName("WVA14");
            
            // ---- WVM15 ----
            WVM15.setName("WVM15");
            
            // ---- WVS15 ----
            WVS15.setComponentPopupMenu(BTD);
            WVS15.setName("WVS15");
            
            // ---- WVA15 ----
            WVA15.setName("WVA15");
            
            // ---- WVM16 ----
            WVM16.setName("WVM16");
            
            // ---- WVS16 ----
            WVS16.setComponentPopupMenu(BTD);
            WVS16.setName("WVS16");
            
            // ---- WVA16 ----
            WVA16.setName("WVA16");
            
            // ---- WVM17 ----
            WVM17.setName("WVM17");
            
            // ---- WVS17 ----
            WVS17.setComponentPopupMenu(BTD);
            WVS17.setName("WVS17");
            
            // ---- WVA17 ----
            WVA17.setName("WVA17");
            
            // ---- WVM18 ----
            WVM18.setName("WVM18");
            
            // ---- WVS18 ----
            WVS18.setComponentPopupMenu(BTD);
            WVS18.setName("WVS18");
            
            // ---- WVA18 ----
            WVA18.setName("WVA18");
            
            // ---- WVM19 ----
            WVM19.setName("WVM19");
            
            // ---- WVS19 ----
            WVS19.setComponentPopupMenu(BTD);
            WVS19.setName("WVS19");
            
            // ---- WVA19 ----
            WVA19.setName("WVA19");
            
            // ---- WVM20 ----
            WVM20.setName("WVM20");
            
            // ---- WVS20 ----
            WVS20.setComponentPopupMenu(BTD);
            WVS20.setName("WVS20");
            
            // ---- WVA20 ----
            WVA20.setName("WVA20");
            
            // ---- WVM21 ----
            WVM21.setName("WVM21");
            
            // ---- WVS21 ----
            WVS21.setComponentPopupMenu(BTD);
            WVS21.setName("WVS21");
            
            // ---- WVA21 ----
            WVA21.setName("WVA21");
            
            // ---- WVM22 ----
            WVM22.setName("WVM22");
            
            // ---- WVS22 ----
            WVS22.setComponentPopupMenu(BTD);
            WVS22.setName("WVS22");
            
            // ---- WVA22 ----
            WVA22.setName("WVA22");
            
            // ---- WVM23 ----
            WVM23.setName("WVM23");
            
            // ---- WVS23 ----
            WVS23.setComponentPopupMenu(BTD);
            WVS23.setName("WVS23");
            
            // ---- WVA23 ----
            WVA23.setName("WVA23");
            
            // ---- WVM24 ----
            WVM24.setName("WVM24");
            
            // ---- WVS24 ----
            WVS24.setComponentPopupMenu(BTD);
            WVS24.setName("WVS24");
            
            // ---- WVA24 ----
            WVA24.setName("WVA24");
            
            // ---- WVM25 ----
            WVM25.setName("WVM25");
            
            // ---- WVS25 ----
            WVS25.setComponentPopupMenu(BTD);
            WVS25.setName("WVS25");
            
            // ---- WVA25 ----
            WVA25.setName("WVA25");
            
            // ---- WVM26 ----
            WVM26.setName("WVM26");
            
            // ---- WVS26 ----
            WVS26.setComponentPopupMenu(BTD);
            WVS26.setName("WVS26");
            
            // ---- WVA26 ----
            WVA26.setName("WVA26");
            
            // ---- WVM27 ----
            WVM27.setName("WVM27");
            
            // ---- WVS27 ----
            WVS27.setComponentPopupMenu(BTD);
            WVS27.setName("WVS27");
            
            // ---- WVA27 ----
            WVA27.setName("WVA27");
            
            // ---- WVM28 ----
            WVM28.setName("WVM28");
            
            // ---- WVS28 ----
            WVS28.setComponentPopupMenu(BTD);
            WVS28.setName("WVS28");
            
            // ---- WVA28 ----
            WVA28.setName("WVA28");
            
            // ---- WVM29 ----
            WVM29.setName("WVM29");
            
            // ---- WVS29 ----
            WVS29.setComponentPopupMenu(BTD);
            WVS29.setName("WVS29");
            
            // ---- WVA29 ----
            WVA29.setName("WVA29");
            
            // ---- WVM30 ----
            WVM30.setName("WVM30");
            
            // ---- WVS30 ----
            WVS30.setComponentPopupMenu(BTD);
            WVS30.setName("WVS30");
            
            // ---- WVA30 ----
            WVA30.setName("WVA30");
            
            // ---- WVM31 ----
            WVM31.setName("WVM31");
            
            // ---- WVS31 ----
            WVS31.setComponentPopupMenu(BTD);
            WVS31.setName("WVS31");
            
            // ---- WVA31 ----
            WVA31.setName("WVA31");
            
            // ---- WVM32 ----
            WVM32.setName("WVM32");
            
            // ---- WVS32 ----
            WVS32.setComponentPopupMenu(BTD);
            WVS32.setName("WVS32");
            
            // ---- WVA32 ----
            WVA32.setName("WVA32");
            
            // ---- WVM33 ----
            WVM33.setName("WVM33");
            
            // ---- WVS33 ----
            WVS33.setComponentPopupMenu(BTD);
            WVS33.setName("WVS33");
            
            // ---- WVA33 ----
            WVA33.setName("WVA33");
            
            // ---- WVM34 ----
            WVM34.setName("WVM34");
            
            // ---- WVS34 ----
            WVS34.setComponentPopupMenu(BTD);
            WVS34.setName("WVS34");
            
            // ---- WVA34 ----
            WVA34.setName("WVA34");
            
            // ---- WVM35 ----
            WVM35.setName("WVM35");
            
            // ---- WVS35 ----
            WVS35.setComponentPopupMenu(BTD);
            WVS35.setName("WVS35");
            
            // ---- WVA35 ----
            WVA35.setName("WVA35");
            
            // ---- WVM36 ----
            WVM36.setName("WVM36");
            
            // ---- WVS36 ----
            WVS36.setComponentPopupMenu(BTD);
            WVS36.setName("WVS36");
            
            // ---- WVA36 ----
            WVA36.setName("WVA36");
            
            // ---- WVM37 ----
            WVM37.setName("WVM37");
            
            // ---- WVS37 ----
            WVS37.setComponentPopupMenu(BTD);
            WVS37.setName("WVS37");
            
            // ---- WVA37 ----
            WVA37.setName("WVA37");
            
            // ---- WVM38 ----
            WVM38.setName("WVM38");
            
            // ---- WVS38 ----
            WVS38.setComponentPopupMenu(BTD);
            WVS38.setName("WVS38");
            
            // ---- WVA38 ----
            WVA38.setName("WVA38");
            
            // ---- WVM39 ----
            WVM39.setName("WVM39");
            
            // ---- WVS39 ----
            WVS39.setComponentPopupMenu(BTD);
            WVS39.setName("WVS39");
            
            // ---- WVA39 ----
            WVA39.setName("WVA39");
            
            // ---- WVM40 ----
            WVM40.setName("WVM40");
            
            // ---- WVS40 ----
            WVS40.setComponentPopupMenu(BTD);
            WVS40.setName("WVS40");
            
            // ---- WVA40 ----
            WVA40.setName("WVA40");
            
            // ---- WVM41 ----
            WVM41.setName("WVM41");
            
            // ---- WVS41 ----
            WVS41.setComponentPopupMenu(BTD);
            WVS41.setName("WVS41");
            
            // ---- WVA41 ----
            WVA41.setName("WVA41");
            
            // ---- WVM42 ----
            WVM42.setName("WVM42");
            
            // ---- WVS42 ----
            WVS42.setComponentPopupMenu(BTD);
            WVS42.setName("WVS42");
            
            // ---- WVA42 ----
            WVA42.setName("WVA42");
            
            GroupLayout xTitledPanel1ContentContainerLayout = new GroupLayout(xTitledPanel1ContentContainer);
            xTitledPanel1ContentContainer.setLayout(xTitledPanel1ContentContainerLayout);
            xTitledPanel1ContentContainerLayout.setHorizontalGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(21, 21, 21)
                    .addComponent(label1, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                    .addComponent(label2, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                    .addComponent(label3, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE).addGap(50, 50, 50)
                    .addComponent(label4, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                    .addComponent(label5, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                    .addComponent(label6, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE).addGap(50, 50, 50)
                    .addComponent(label7, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                    .addComponent(label8, GroupLayout.PREFERRED_SIZE, 30, GroupLayout.PREFERRED_SIZE).addGap(20, 20, 20)
                    .addComponent(label9, GroupLayout.PREFERRED_SIZE, 25, GroupLayout.PREFERRED_SIZE))
                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(16, 16, 16)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVM10, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM13, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM31, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM19, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM16, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM04, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM25, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM07, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM28, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM22, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM34, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM40, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM01, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM37, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVS16, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS07, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS19, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS13, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS28, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS22, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS25, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS04, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS01, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS10, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS40, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS34, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS37, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS31, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVA04, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA01, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA37, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA34, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA28, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA31, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA13, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA10, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA16, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA40, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA22, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA19, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA25, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA07, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGap(15, 15, 15)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVM05, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM02, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM08, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM41, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM29, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM32, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM17, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM38, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM14, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM35, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM26, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM23, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM20, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM11, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVS02, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS08, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS17, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS05, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS14, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS11, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS20, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS29, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS41, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS35, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS26, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS38, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS32, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS23, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVA11, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA05, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA08, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA02, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA14, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA35, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA20, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA23, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA29, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA32, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA38, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA26, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA41, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA17, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))
                    .addGap(15, 15, 15)
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVM12, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM18, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM21, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM15, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM24, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM09, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM03, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM06, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM30, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM33, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM36, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM39, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM42, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVM27, GroupLayout.PREFERRED_SIZE, 100, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVS18, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS12, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS30, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS24, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS03, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS27, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS06, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS21, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS09, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS15, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS36, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS39, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS42, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVS33, GroupLayout.PREFERRED_SIZE, 50, GroupLayout.PREFERRED_SIZE))
                    .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addComponent(WVA03, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA06, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA15, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA36, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA21, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA30, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA18, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA39, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA12, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA33, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA27, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA42, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA24, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE)
                        .addComponent(WVA09, GroupLayout.PREFERRED_SIZE, 60, GroupLayout.PREFERRED_SIZE))));
            xTitledPanel1ContentContainerLayout
                .setVerticalGroup(
                    xTitledPanel1ContentContainerLayout.createParallelGroup()
                        .addGroup(
                            xTitledPanel1ContentContainerLayout
                                .createSequentialGroup().addGap(16, 16,
                                    16)
                                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup().addComponent(label1)
                                    .addComponent(label2).addComponent(
                                        label3)
                                    .addComponent(label4).addComponent(
                                        label5)
                                    .addComponent(label6).addComponent(label7).addComponent(label8).addComponent(label9))
                                .addGap(4, 4, 4).addGroup(
                                    xTitledPanel1ContentContainerLayout.createParallelGroup()
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout
                                                .createSequentialGroup().addGap(25, 25,
                                                    25)
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(50, 50, 50).addComponent(WVM10, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(75, 75, 75).addComponent(WVM13, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(225, 225, 225).addComponent(WVM31, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(125, 125, 125).addComponent(WVM19, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(100, 100, 100).addComponent(WVM16, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addComponent(WVM04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE)
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(175, 175, 175).addComponent(WVM25, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(25, 25, 25).addComponent(WVM07, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(200, 200, 200).addComponent(WVM28, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(150, 150, 150).addComponent(WVM22, GroupLayout.PREFERRED_SIZE,
                                                                GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                            .addGap(250, 250, 250)
                                                            .addComponent(WVM34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE)))
                                                .addGap(22, 22, 22).addComponent(WVM40, GroupLayout.PREFERRED_SIZE,
                                                    GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                        .addComponent(WVM01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                            GroupLayout.PREFERRED_SIZE)
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(300, 300, 300)
                                                .addComponent(
                                                    WVM37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(125, 125, 125).addComponent(WVS16, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVS07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(150, 150, 150).addComponent(WVS19,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100).addComponent(WVS13,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(225, 225, 225).addComponent(WVS28,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(175, 175, 175).addComponent(WVS22,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(200, 200, 200).addComponent(WVS25,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVS04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVS01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVS10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVS40, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVS34, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVS37, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                250, 250, 250).addComponent(WVS31, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVA04, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(
                                                    WVA01, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(225, 225, 225).addComponent(WVA37, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(200, 200, 200).addComponent(WVA34,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(150, 150, 150).addComponent(WVA28,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(175, 175, 175).addComponent(WVA31,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVA13, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVA10, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                        .addComponent(WVA16, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(250, 250, 250).addComponent(WVA40,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100).addComponent(
                                                        WVA22, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVA19, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(125, 125, 125)
                                                        .addComponent(WVA25, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(xTitledPanel1ContentContainerLayout
                                            .createSequentialGroup().addGap(50, 50, 50)
                                            .addComponent(WVA07, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGroup(
                                            xTitledPanel1ContentContainerLayout.createParallelGroup().addGroup(
                                                xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVM05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(
                                                    WVM02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                        50, 50, 50)
                                                        .addComponent(WVM08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(225, 225, 225).addComponent(WVM41,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(125, 125, 125).addComponent(WVM29,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(150, 150, 150).addComponent(WVM32,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVM17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(200, 200, 200).addComponent(WVM38,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVM14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(175, 175, 175)
                                                    .addComponent(WVM35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(100, 100, 100)
                                                        .addComponent(WVM26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVM23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVM20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                            .addComponent(WVM11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addComponent(WVS02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVS08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(125, 125, 125).addComponent(WVS17,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVS05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100).addComponent(WVS14,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVS11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(150, 150, 150)
                                                    .addComponent(WVS20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22).addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVS29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(125, 125, 125)
                                                        .addComponent(WVS41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVS35, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVS26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100)
                                                    .addComponent(WVS38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVS32, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                175, 175, 175).addComponent(WVS23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGroup(
                                            xTitledPanel1ContentContainerLayout.createParallelGroup().addGroup(
                                                xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVA11, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVA05, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVA08, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(
                                                    WVA02, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                        100, 100, 100)
                                                        .addComponent(WVA14, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(125, 125, 125).addComponent(WVA35, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVA20, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                        .addComponent(WVA23, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVA29, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100).addComponent(WVA32,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(150, 150, 150).addComponent(
                                                        WVA38, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVA26, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                325, 325, 325).addComponent(WVA41, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(125, 125, 125)
                                            .addComponent(WVA17, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVM12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(125, 125, 125).addComponent(WVM18,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(
                                                    xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(150, 150, 150)
                                                        .addComponent(WVM21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(100, 100, 100).addComponent(WVM15,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(175, 175, 175).addComponent(WVM24,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVM09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVM03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVM06, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addComponent(WVM30, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(25, 25, 25)
                                                    .addComponent(WVM33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVM36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVM39, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout
                                                .createSequentialGroup().addGap(325, 325, 325).addComponent(
                                                    WVM42, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(200, 200, 200)
                                                .addComponent(WVM27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(125, 125, 125).addComponent(WVS18, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(75, 75, 75).addComponent(WVS12, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(225, 225, 225).addComponent(WVS30, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(175, 175, 175).addComponent(WVS24, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addComponent(WVS03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(
                                                        xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(200, 200, 200)
                                                            .addComponent(WVS27, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout
                                                        .createSequentialGroup().addGap(25, 25, 25).addComponent(WVS06,
                                                            GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                            GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(
                                                        xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(150, 150, 150)
                                                            .addComponent(WVS21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(50, 50, 50).addComponent(WVS09, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(
                                                        xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(100, 100, 100)
                                                            .addComponent(WVS15, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                                GroupLayout.PREFERRED_SIZE)))
                                                .addGap(22, 22, 22)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                    .addComponent(WVS36, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(25, 25, 25).addComponent(WVS39, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                    .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                        .addGap(50, 50, 50).addComponent(WVS42, GroupLayout.PREFERRED_SIZE,
                                                            GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(
                                            xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(
                                                250, 250, 250).addComponent(WVS33, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addComponent(WVA03, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(25, 25, 25).addComponent(WVA06,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE)))
                                            .addGap(22, 22, 22)
                                            .addGroup(xTitledPanel1ContentContainerLayout.createParallelGroup()
                                                .addGroup(xTitledPanel1ContentContainerLayout
                                                    .createSequentialGroup().addGap(25, 25, 25).addComponent(WVA15,
                                                        GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(200, 200, 200).addComponent(WVA36, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(75, 75, 75)
                                                    .addComponent(WVA21, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(150, 150, 150).addComponent(WVA30, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                                    .addComponent(WVA18, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                        GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(225, 225, 225).addComponent(WVA39, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addComponent(WVA12, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                    GroupLayout.PREFERRED_SIZE)
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(175, 175, 175).addComponent(WVA33, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(125, 125, 125).addComponent(WVA27, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(250, 250, 250).addComponent(WVA42, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
                                                .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup()
                                                    .addGap(100, 100, 100).addComponent(WVA24, GroupLayout.PREFERRED_SIZE,
                                                        GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))))
                                        .addGroup(xTitledPanel1ContentContainerLayout.createSequentialGroup().addGap(50, 50, 50)
                                            .addComponent(WVA09, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE,
                                                GroupLayout.PREFERRED_SIZE)))));
          }
          
          // ======== scrollPane1 ========
          {
            scrollPane1.setName("scrollPane1");
            
            // ---- WDERLD ----
            WDERLD.setName("WDERLD");
            scrollPane1.setViewportView(WDERLD);
          }
          
          GroupLayout panel1Layout = new GroupLayout(panel1);
          panel1.setLayout(panel1Layout);
          panel1Layout.setHorizontalGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 705,
                  GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup().addGap(15, 15, 15).addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE,
                  705, GroupLayout.PREFERRED_SIZE))
              .addGroup(panel1Layout.createSequentialGroup().addGap(30, 30, 30)
                  .addComponent(OBJ_30_OBJ_30, GroupLayout.PREFERRED_SIZE, 117, GroupLayout.PREFERRED_SIZE).addGap(30, 30, 30)
                  .addComponent(WMTVTL, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE).addGap(80, 80, 80)
                  .addComponent(OBJ_32_OBJ_32, GroupLayout.PREFERRED_SIZE, 52, GroupLayout.PREFERRED_SIZE).addGap(3, 3, 3)
                  .addComponent(WECVTL, GroupLayout.PREFERRED_SIZE, 105, GroupLayout.PREFERRED_SIZE)));
          panel1Layout.setVerticalGroup(panel1Layout.createParallelGroup()
              .addGroup(panel1Layout.createSequentialGroup().addGap(20, 20, 20)
                  .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 45, GroupLayout.PREFERRED_SIZE).addGap(10, 10, 10)
                  .addComponent(xTitledPanel1, GroupLayout.PREFERRED_SIZE, 440, GroupLayout.PREFERRED_SIZE).addGap(15, 15, 15)
                  .addGroup(panel1Layout.createParallelGroup()
                      .addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_30_OBJ_30,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WMTVTL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                      .addGroup(panel1Layout.createSequentialGroup().addGap(5, 5, 5).addComponent(OBJ_32_OBJ_32,
                          GroupLayout.PREFERRED_SIZE, 20, GroupLayout.PREFERRED_SIZE))
                      .addComponent(WECVTL, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))));
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 735, 570);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("Choix possibles");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_11 ----
      OBJ_11.setText("Aide en ligne");
      OBJ_11.setName("OBJ_11");
      OBJ_11.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_11ActionPerformed(e);
        }
      });
      BTD.add(OBJ_11);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel OBJ_30_OBJ_30;
  private XRiTextField WMTVTL;
  private XRiTextField WECVTL;
  private JLabel OBJ_32_OBJ_32;
  private JXTitledPanel xTitledPanel1;
  private JLabel label1;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private XRiTextField WVM01;
  private XRiTextField WVS01;
  private XRiTextField WVA01;
  private XRiTextField WVM02;
  private XRiTextField WVS02;
  private XRiTextField WVA02;
  private XRiTextField WVM03;
  private XRiTextField WVS03;
  private XRiTextField WVA03;
  private XRiTextField WVM04;
  private XRiTextField WVS04;
  private XRiTextField WVA04;
  private XRiTextField WVM05;
  private XRiTextField WVS05;
  private XRiTextField WVA05;
  private XRiTextField WVM06;
  private XRiTextField WVS06;
  private XRiTextField WVA06;
  private XRiTextField WVM07;
  private XRiTextField WVS07;
  private XRiTextField WVA07;
  private XRiTextField WVM08;
  private XRiTextField WVS08;
  private XRiTextField WVA08;
  private XRiTextField WVM09;
  private XRiTextField WVS09;
  private XRiTextField WVA09;
  private XRiTextField WVM10;
  private XRiTextField WVS10;
  private XRiTextField WVA10;
  private XRiTextField WVM11;
  private XRiTextField WVS11;
  private XRiTextField WVA11;
  private XRiTextField WVM12;
  private XRiTextField WVS12;
  private XRiTextField WVA12;
  private XRiTextField WVM13;
  private XRiTextField WVS13;
  private XRiTextField WVA13;
  private XRiTextField WVM14;
  private XRiTextField WVS14;
  private XRiTextField WVA14;
  private XRiTextField WVM15;
  private XRiTextField WVS15;
  private XRiTextField WVA15;
  private XRiTextField WVM16;
  private XRiTextField WVS16;
  private XRiTextField WVA16;
  private XRiTextField WVM17;
  private XRiTextField WVS17;
  private XRiTextField WVA17;
  private XRiTextField WVM18;
  private XRiTextField WVS18;
  private XRiTextField WVA18;
  private XRiTextField WVM19;
  private XRiTextField WVS19;
  private XRiTextField WVA19;
  private XRiTextField WVM20;
  private XRiTextField WVS20;
  private XRiTextField WVA20;
  private XRiTextField WVM21;
  private XRiTextField WVS21;
  private XRiTextField WVA21;
  private XRiTextField WVM22;
  private XRiTextField WVS22;
  private XRiTextField WVA22;
  private XRiTextField WVM23;
  private XRiTextField WVS23;
  private XRiTextField WVA23;
  private XRiTextField WVM24;
  private XRiTextField WVS24;
  private XRiTextField WVA24;
  private XRiTextField WVM25;
  private XRiTextField WVS25;
  private XRiTextField WVA25;
  private XRiTextField WVM26;
  private XRiTextField WVS26;
  private XRiTextField WVA26;
  private XRiTextField WVM27;
  private XRiTextField WVS27;
  private XRiTextField WVA27;
  private XRiTextField WVM28;
  private XRiTextField WVS28;
  private XRiTextField WVA28;
  private XRiTextField WVM29;
  private XRiTextField WVS29;
  private XRiTextField WVA29;
  private XRiTextField WVM30;
  private XRiTextField WVS30;
  private XRiTextField WVA30;
  private XRiTextField WVM31;
  private XRiTextField WVS31;
  private XRiTextField WVA31;
  private XRiTextField WVM32;
  private XRiTextField WVS32;
  private XRiTextField WVA32;
  private XRiTextField WVM33;
  private XRiTextField WVS33;
  private XRiTextField WVA33;
  private XRiTextField WVM34;
  private XRiTextField WVS34;
  private XRiTextField WVA34;
  private XRiTextField WVM35;
  private XRiTextField WVS35;
  private XRiTextField WVA35;
  private XRiTextField WVM36;
  private XRiTextField WVS36;
  private XRiTextField WVA36;
  private XRiTextField WVM37;
  private XRiTextField WVS37;
  private XRiTextField WVA37;
  private XRiTextField WVM38;
  private XRiTextField WVS38;
  private XRiTextField WVA38;
  private XRiTextField WVM39;
  private XRiTextField WVS39;
  private XRiTextField WVA39;
  private XRiTextField WVM40;
  private XRiTextField WVS40;
  private XRiTextField WVA40;
  private XRiTextField WVM41;
  private XRiTextField WVS41;
  private XRiTextField WVA41;
  private XRiTextField WVM42;
  private XRiTextField WVS42;
  private XRiTextField WVA42;
  private JScrollPane scrollPane1;
  private XRiTable WDERLD;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_11;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
