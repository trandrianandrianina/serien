
package ri.serien.libecranrpg.vgtm.VGTM55FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;
import org.jdesktop.swingx.border.DropShadowBorder;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM55FM_L2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] LBSNS_Value = { "", "C", "D", };
  
  public VGTM55FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LBSNS.setValeurs(LBSNS_Value, null);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    


    
    
    // LBSNS.setSelectedIndex(getIndice("LBSNS", LBSNS_Value));
    WCOD.setEnabled(lexique.isPresent("WCOD"));
    LBNLI.setEnabled(lexique.isPresent("LBNLI"));
    LBPCE.setEnabled(lexique.isPresent("LBPCE"));
    // DATVAL.setEnabled( lexique.isPresent("DATVAL"));
    DATOPE.setEnabled(lexique.isPresent("DATOPE"));
    LBMTT.setEnabled(lexique.isPresent("LBMTT"));
    // LBSNS.setVisible( lexique.isPresent("LBSNS"));
    // LBSNS.setEnabled( lexique.isPresent("LBSNS"));
    LBLIBC.setEnabled(lexique.isPresent("LBLIBC"));
    LBLIB.setEnabled(lexique.isPresent("LBLIB"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("@V01F@"));
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    // lexique.HostFieldPutData("LBSNS", 0, LBSNS_Value[LBSNS.getSelectedIndex()]);
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void OBJ_10ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE", true);
  }
  
  private void OBJ_9ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    p_contenu = new JPanel();
    p_recup = new JPanel();
    panel1 = new JPanel();
    WCOD = new XRiTextField();
    LBNLI = new XRiTextField();
    DATOPE = new XRiTextField();
    LBLIB = new XRiTextField();
    LBPCE = new XRiTextField();
    LBMTT = new XRiTextField();
    LBSNS = new XRiComboBox();
    DATVAL = new XRiCalendrier();
    LBLIBC = new XRiTextField();
    OBJ_23 = new JLabel();
    OBJ_20 = new JLabel();
    OBJ_25 = new JLabel();
    OBJ_24 = new JLabel();
    OBJ_27 = new JLabel();
    OBJ_17 = new JLabel();
    OBJ_26 = new JLabel();
    OBJ_16 = new JLabel();
    BTD = new JPopupMenu();
    OBJ_10 = new JMenuItem();
    OBJ_9 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(930, 165));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== p_recup ========
        {
          p_recup.setMinimumSize(new Dimension(300, 200));
          p_recup.setPreferredSize(new Dimension(804, 305));
          p_recup.setBorder(new DropShadowBorder());
          p_recup.setOpaque(false);
          p_recup.setName("p_recup");
          p_recup.setLayout(null);

          //======== panel1 ========
          {
            panel1.setOpaque(false);
            panel1.setName("panel1");
            panel1.setLayout(null);

            //---- WCOD ----
            WCOD.setName("WCOD");
            panel1.add(WCOD);
            WCOD.setBounds(10, 25, 20, WCOD.getPreferredSize().height);

            //---- LBNLI ----
            LBNLI.setName("LBNLI");
            panel1.add(LBNLI);
            LBNLI.setBounds(40, 25, 44, LBNLI.getPreferredSize().height);

            //---- DATOPE ----
            DATOPE.setName("DATOPE");
            panel1.add(DATOPE);
            DATOPE.setBounds(250, 25, 66, DATOPE.getPreferredSize().height);

            //---- LBLIB ----
            LBLIB.setName("LBLIB");
            panel1.add(LBLIB);
            LBLIB.setBounds(10, 75, 305, LBLIB.getPreferredSize().height);

            //---- LBPCE ----
            LBPCE.setName("LBPCE");
            panel1.add(LBPCE);
            LBPCE.setBounds(375, 105, 68, LBPCE.getPreferredSize().height);

            //---- LBMTT ----
            LBMTT.setName("LBMTT");
            panel1.add(LBMTT);
            LBMTT.setBounds(445, 105, 108, LBMTT.getPreferredSize().height);

            //---- LBSNS ----
            LBSNS.setModel(new DefaultComboBoxModel(new String[] {
              "",
              "Cr\u00e9dit",
              "D\u00e9bit"
            }));
            LBSNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            LBSNS.setName("LBSNS");
            panel1.add(LBSNS);
            LBSNS.setBounds(555, 105, 75, LBSNS.getPreferredSize().height);

            //---- DATVAL ----
            DATVAL.setName("DATVAL");
            panel1.add(DATVAL);
            DATVAL.setBounds(630, 105, 105, DATVAL.getPreferredSize().height);

            //---- LBLIBC ----
            LBLIBC.setName("LBLIBC");
            panel1.add(LBLIBC);
            LBLIBC.setBounds(10, 105, 305, LBLIBC.getPreferredSize().height);

            //---- OBJ_23 ----
            OBJ_23.setText("Nature");
            OBJ_23.setName("OBJ_23");
            panel1.add(OBJ_23);
            OBJ_23.setBounds(10, 55, 147, 18);

            //---- OBJ_20 ----
            OBJ_20.setText("Date de l'op\u00e9ration");
            OBJ_20.setName("OBJ_20");
            panel1.add(OBJ_20);
            OBJ_20.setBounds(135, 29, 117, 20);

            //---- OBJ_25 ----
            OBJ_25.setText("Montant");
            OBJ_25.setName("OBJ_25");
            panel1.add(OBJ_25);
            OBJ_25.setBounds(445, 80, 69, 18);

            //---- OBJ_24 ----
            OBJ_24.setText("N\u00b0pi\u00e8ce");
            OBJ_24.setName("OBJ_24");
            panel1.add(OBJ_24);
            OBJ_24.setBounds(375, 80, 52, 18);

            //---- OBJ_27 ----
            OBJ_27.setText("Date de valeur");
            OBJ_27.setName("OBJ_27");
            panel1.add(OBJ_27);
            OBJ_27.setBounds(630, 80, 81, 18);

            //---- OBJ_17 ----
            OBJ_17.setText("N\u00b0lig");
            OBJ_17.setName("OBJ_17");
            panel1.add(OBJ_17);
            OBJ_17.setBounds(40, 5, 36, 18);

            //---- OBJ_26 ----
            OBJ_26.setText("Sens");
            OBJ_26.setName("OBJ_26");
            panel1.add(OBJ_26);
            OBJ_26.setBounds(555, 80, 34, 18);

            //---- OBJ_16 ----
            OBJ_16.setText("C");
            OBJ_16.setName("OBJ_16");
            panel1.add(OBJ_16);
            OBJ_16.setBounds(15, 5, 12, 18);

            {
              // compute preferred size
              Dimension preferredSize = new Dimension();
              for(int i = 0; i < panel1.getComponentCount(); i++) {
                Rectangle bounds = panel1.getComponent(i).getBounds();
                preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
              }
              Insets insets = panel1.getInsets();
              preferredSize.width += insets.right;
              preferredSize.height += insets.bottom;
              panel1.setMinimumSize(preferredSize);
              panel1.setPreferredSize(preferredSize);
            }
          }
          p_recup.add(panel1);
          panel1.setBounds(0, 0, 740, 145);
        }
        p_contenu.add(p_recup);
        p_recup.setBounds(5, 5, 750, 155);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_10 ----
      OBJ_10.setText("Duplication");
      OBJ_10.setName("OBJ_10");
      OBJ_10.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_10ActionPerformed(e);
        }
      });
      BTD.add(OBJ_10);

      //---- OBJ_9 ----
      OBJ_9.setText("Aide en ligne");
      OBJ_9.setName("OBJ_9");
      OBJ_9.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_9ActionPerformed(e);
        }
      });
      BTD.add(OBJ_9);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel p_contenu;
  private JPanel p_recup;
  private JPanel panel1;
  private XRiTextField WCOD;
  private XRiTextField LBNLI;
  private XRiTextField DATOPE;
  private XRiTextField LBLIB;
  private XRiTextField LBPCE;
  private XRiTextField LBMTT;
  private XRiComboBox LBSNS;
  private XRiCalendrier DATVAL;
  private XRiTextField LBLIBC;
  private JLabel OBJ_23;
  private JLabel OBJ_20;
  private JLabel OBJ_25;
  private JLabel OBJ_24;
  private JLabel OBJ_27;
  private JLabel OBJ_17;
  private JLabel OBJ_26;
  private JLabel OBJ_16;
  private JPopupMenu BTD;
  private JMenuItem OBJ_10;
  private JMenuItem OBJ_9;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
