
package ri.serien.libecranrpg.vgtm.VGTM71FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;
import ri.serien.libswing.composantrpg.lexical.table.XRiTable;

/**
 * @author Stéphane Vénéri
 */
public class VGTM71FM_L4 extends SNPanelEcranRPG implements ioFrame {
  // Variables
  private String[] _LA01_Title = { "HLD65", };
  private String[][] _LA01_Data = { { "LA01", }, { "LA02", }, { "LA03", }, { "LA04", }, { "LA05", }, { "LA06", }, { "LA07", },
      { "LA08", }, { "LA09", }, { "LA10", }, { "LA11", }, { "LA12", }, { "LA13", }, { "LA14", }, { "LA15", }, { "LA16", }, };
  private int[] _LA01_Width = { 450, };
  private String[] _WDERLD_Title = { "HLD02", };
  private String[][] _WDERLD_Data = { { "WDERLD", }, };
  private int[] _WDERLD_Width = { 75, };
  
  /**
   * Constructeur.
   */
  public VGTM71FM_L4(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    LA01.setAspectTable(null, _LA01_Title, _LA01_Data, _LA01_Width, false, null, null, null, null);
    WDERLD.setAspectTable(null, _WDERLD_Title, _WDERLD_Data, _WDERLD_Width, true, null, null, null, null);
    
    
    riMenu_bt2.setIcon(lexique.chargerImage("images/options.png", true));
  }
  
  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    TOTAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@TOTAFF@")).trim());
    WDIFE.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@WDIFE@")).trim());
    label2.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA01@")).trim());
    label3.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA02@")).trim());
    label4.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA03@")).trim());
    label5.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA04@")).trim());
    label6.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA05@")).trim());
    label7.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA06@")).trim());
    label8.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA07@")).trim());
    label9.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA08@")).trim());
    label10.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA09@")).trim());
    label11.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA10@")).trim());
    label12.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA11@")).trim());
    label13.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA12@")).trim());
    label14.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA13@")).trim());
    label15.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA14@")).trim());
    label16.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA15@")).trim());
    label17.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@ERA16@")).trim());
    MSGAFF.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@MSGAFF@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("61");
    
    OBJ_59.setEnabled(interpreteurD.analyseExpression("@LA16@").contains(">"));
    OBJ_59.setText(changerBouton(MTA16, OBJ_59));
    OBJ_58.setEnabled(interpreteurD.analyseExpression("@LA15@").contains(">"));
    OBJ_58.setText(changerBouton(MTA15, OBJ_58));
    OBJ_57.setEnabled(interpreteurD.analyseExpression("@LA14@").contains(">"));
    OBJ_57.setText(changerBouton(MTA14, OBJ_57));
    OBJ_56.setEnabled(interpreteurD.analyseExpression("@LA13@").contains(">"));
    OBJ_56.setText(changerBouton(MTA13, OBJ_56));
    OBJ_55.setEnabled(interpreteurD.analyseExpression("@LA12@").contains(">"));
    OBJ_55.setText(changerBouton(MTA12, OBJ_55));
    OBJ_54.setEnabled(interpreteurD.analyseExpression("@LA11@").contains(">"));
    OBJ_54.setText(changerBouton(MTA11, OBJ_54));
    OBJ_53.setEnabled(interpreteurD.analyseExpression("@LA10@").contains(">"));
    OBJ_53.setText(changerBouton(MTA10, OBJ_53));
    OBJ_52.setEnabled(interpreteurD.analyseExpression("@LA09@").contains(">"));
    OBJ_52.setText(changerBouton(MTA09, OBJ_52));
    OBJ_51.setEnabled(interpreteurD.analyseExpression("@LA08@").contains(">"));
    OBJ_51.setText(changerBouton(MTA08, OBJ_51));
    OBJ_50.setEnabled(interpreteurD.analyseExpression("@LA07@").contains(">"));
    OBJ_50.setText(changerBouton(MTA07, OBJ_50));
    OBJ_49.setEnabled(interpreteurD.analyseExpression("@LA06@").contains(">"));
    OBJ_49.setText(changerBouton(MTA06, OBJ_49));
    OBJ_48.setEnabled(interpreteurD.analyseExpression("@LA05@").contains(">"));
    OBJ_48.setText(changerBouton(MTA05, OBJ_48));
    OBJ_47.setEnabled(interpreteurD.analyseExpression("@LA04@").contains(">"));
    OBJ_47.setText(changerBouton(MTA04, OBJ_47));
    OBJ_46.setEnabled(interpreteurD.analyseExpression("@LA03@").contains(">"));
    OBJ_46.setText(changerBouton(MTA03, OBJ_46));
    OBJ_45.setEnabled(interpreteurD.analyseExpression("@LA02@").contains(">"));
    OBJ_45.setText(changerBouton(MTA02, OBJ_45));
    OBJ_44.setEnabled(interpreteurD.analyseExpression("@LA01@").contains(">"));
    OBJ_44.setText(changerBouton(MTA01, OBJ_44));
    WDIFE.setVisible(lexique.isTrue("31"));
    OBJ_60.setVisible(WDIFE.isVisible());
    label18.setVisible(lexique.isTrue("61"));
    
    // Titre
    setTitle(interpreteurD.analyseExpression("Affectation"));
  }
  
  @Override
  public void getData() {
    super.getData();
  }
  
  private void dupliquerValeur(JButton bouton, XRiTextField zone, String variable) {
    if (bouton.getText().equals(">")) {
      zone.setText(lexique.HostFieldGetData(variable).substring(50, 61).trim());
      bouton.setText("<");
    }
    else {
      zone.setText("");
      bouton.setText(">");
    }
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private String changerBouton(XRiTextField zone, JButton button) {
    zone.setEnabled(button.isEnabled());
    if (zone.getText().equals("")) {
      return ">";
    }
    else {
      return "<";
    }
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12");
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F23");
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F8");
  }
  
  private void riSousMenu_bt8ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F11");
  }
  
  private void riSousMenu_bt9ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F24");
  }
  
  private void OBJ_44ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_44, MTA01, "LA01");
  }
  
  private void OBJ_45ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_45, MTA02, "LA02");
  }
  
  private void OBJ_46ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_46, MTA03, "LA03");
  }
  
  private void OBJ_47ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_47, MTA04, "LA04");
  }
  
  private void OBJ_48ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_48, MTA05, "LA05");
  }
  
  private void OBJ_49ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_49, MTA06, "LA06");
  }
  
  private void OBJ_50ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_50, MTA07, "LA07");
  }
  
  private void OBJ_51ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_51, MTA08, "LA08");
  }
  
  private void OBJ_52ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_52, MTA09, "LA09");
  }
  
  private void OBJ_53ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_53, MTA10, "LA10");
  }
  
  private void OBJ_54ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_54, MTA11, "LA11");
  }
  
  private void OBJ_55ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_55, MTA12, "LA12");
  }
  
  private void OBJ_56ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_56, MTA13, "LA13");
  }
  
  private void OBJ_57ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_57, MTA14, "LA14");
  }
  
  private void OBJ_58ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_58, MTA15, "LA15");
  }
  
  private void OBJ_59ActionPerformed(ActionEvent e) {
    dupliquerValeur(OBJ_59, MTA16, "LA16");
  }
  
  private void OBJ_12ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F16");
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "DUPLICATE");
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    riSousMenu8 = new RiSousMenu();
    riSousMenu_bt8 = new RiSousMenu_bt();
    riSousMenu9 = new RiSousMenu();
    riSousMenu_bt9 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    scrollPane1 = new JScrollPane();
    WDERLD = new XRiTable();
    panel3 = new JPanel();
    label19 = new JLabel();
    TOTAFF = new RiZoneSortie();
    WDIFE = new RiZoneSortie();
    OBJ_60 = new JLabel();
    OBJ_44 = new JButton();
    OBJ_45 = new JButton();
    OBJ_46 = new JButton();
    OBJ_47 = new JButton();
    OBJ_48 = new JButton();
    OBJ_49 = new JButton();
    OBJ_50 = new JButton();
    OBJ_51 = new JButton();
    OBJ_52 = new JButton();
    OBJ_53 = new JButton();
    OBJ_54 = new JButton();
    OBJ_55 = new JButton();
    OBJ_56 = new JButton();
    OBJ_57 = new JButton();
    OBJ_58 = new JButton();
    OBJ_59 = new JButton();
    SCROLLPANE_LIST18 = new JScrollPane();
    LA01 = new XRiTable();
    MTA01 = new XRiTextField();
    MTA02 = new XRiTextField();
    MTA03 = new XRiTextField();
    MTA04 = new XRiTextField();
    MTA05 = new XRiTextField();
    MTA06 = new XRiTextField();
    MTA07 = new XRiTextField();
    MTA08 = new XRiTextField();
    MTA09 = new XRiTextField();
    MTA10 = new XRiTextField();
    MTA11 = new XRiTextField();
    MTA12 = new XRiTextField();
    MTA13 = new XRiTextField();
    MTA14 = new XRiTextField();
    MTA15 = new XRiTextField();
    MTA16 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();
    label6 = new JLabel();
    label7 = new JLabel();
    label8 = new JLabel();
    label9 = new JLabel();
    label10 = new JLabel();
    label11 = new JLabel();
    label12 = new JLabel();
    label13 = new JLabel();
    label14 = new JLabel();
    label15 = new JLabel();
    label16 = new JLabel();
    label17 = new JLabel();
    label1 = new JLabel();
    label18 = new JLabel();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    MSGAFF = new RiZoneSortie();
    BTD = new JPopupMenu();
    OBJ_12 = new JMenuItem();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();
    
    // ======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(960, 495));
    setName("this");
    setLayout(new BorderLayout());
    
    // ======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());
      
      // ======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());
        
        // ======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());
          
          // ======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");
            
            // ---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);
          
          // ======== navig_valid ========
          {
            navig_valid.setName("navig_valid");
            
            // ---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);
          
          // ======== navig_retour ========
          {
            navig_retour.setName("navig_retour");
            
            // ---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);
        
        // ======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
          
          // ======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");
            
            // ---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);
          
          // ======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");
            
            // ---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("S\u00e9lection globale");
            riSousMenu_bt6.setToolTipText("S\u00e9lection globale");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);
          
          // ======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");
            
            // ---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("S\u00e9lection d'une \u00e9ch\u00e9ance");
            riSousMenu_bt7.setToolTipText("S\u00e9lection d'une \u00e9ch\u00e9ance");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
          
          // ======== riSousMenu8 ========
          {
            riSousMenu8.setName("riSousMenu8");
            
            // ---- riSousMenu_bt8 ----
            riSousMenu_bt8.setText("Acceptation \u00e9cart");
            riSousMenu_bt8
                .setToolTipText("Demande d'acceptation diff\u00e9r\u00e9e des r\u00e8glements (Si le param\u00e8tre EP existe)");
            riSousMenu_bt8.setName("riSousMenu_bt8");
            riSousMenu_bt8.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt8ActionPerformed(e);
              }
            });
            riSousMenu8.add(riSousMenu_bt8);
          }
          menus_haut.add(riSousMenu8);
          
          // ======== riSousMenu9 ========
          {
            riSousMenu9.setName("riSousMenu9");
            
            // ---- riSousMenu_bt9 ----
            riSousMenu_bt9.setText("Affichage \u00e9cr. \u00e0 r\u00e9gler");
            riSousMenu_bt9.setToolTipText("Changement d'affichage des \u00e9critures \u00e0 r\u00e9gler");
            riSousMenu_bt9.setName("riSousMenu_bt9");
            riSousMenu_bt9.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt9ActionPerformed(e);
              }
            });
            riSousMenu9.add(riSousMenu_bt9);
          }
          menus_haut.add(riSousMenu9);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);
      
      // ======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);
        
        // ======== scrollPane1 ========
        {
          scrollPane1.setName("scrollPane1");
          
          // ---- WDERLD ----
          WDERLD.setName("WDERLD");
          scrollPane1.setViewportView(WDERLD);
        }
        p_contenu.add(scrollPane1);
        scrollPane1.setBounds(20, 25, 612, 45);
        
        // ======== panel3 ========
        {
          panel3.setOpaque(false);
          panel3.setName("panel3");
          panel3.setLayout(null);
          
          // ---- label19 ----
          label19.setText("Affect.");
          label19.setHorizontalAlignment(SwingConstants.CENTER);
          label19.setName("label19");
          panel3.add(label19);
          label19.setBounds(520, 15, 54, 20);
          
          // ---- TOTAFF ----
          TOTAFF.setComponentPopupMenu(BTD);
          TOTAFF.setFont(TOTAFF.getFont().deriveFont(TOTAFF.getFont().getStyle() | Font.BOLD));
          TOTAFF.setForeground(Color.red);
          TOTAFF.setText("@TOTAFF@");
          TOTAFF.setHorizontalAlignment(SwingConstants.RIGHT);
          TOTAFF.setName("TOTAFF");
          panel3.add(TOTAFF);
          TOTAFF.setBounds(585, 375, 100, TOTAFF.getPreferredSize().height);
          
          // ---- WDIFE ----
          WDIFE.setFont(WDIFE.getFont().deriveFont(WDIFE.getFont().getStyle() | Font.BOLD));
          WDIFE.setText("@WDIFE@");
          WDIFE.setHorizontalAlignment(SwingConstants.RIGHT);
          WDIFE.setName("WDIFE");
          panel3.add(WDIFE);
          WDIFE.setBounds(426, 375, 84, WDIFE.getPreferredSize().height);
          
          // ---- OBJ_60 ----
          OBJ_60.setText("Ecart");
          OBJ_60.setHorizontalAlignment(SwingConstants.RIGHT);
          OBJ_60.setFont(OBJ_60.getFont().deriveFont(OBJ_60.getFont().getStyle() | Font.BOLD, OBJ_60.getFont().getSize() + 1f));
          OBJ_60.setName("OBJ_60");
          panel3.add(OBJ_60);
          OBJ_60.setBounds(330, 377, 90, 20);
          
          // ---- OBJ_44 ----
          OBJ_44.setToolTipText("Duplication");
          OBJ_44.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_44.setText(">");
          OBJ_44.setFont(OBJ_44.getFont().deriveFont(OBJ_44.getFont().getStyle() | Font.BOLD));
          OBJ_44.setName("OBJ_44");
          OBJ_44.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_44ActionPerformed(e);
            }
          });
          panel3.add(OBJ_44);
          OBJ_44.setBounds(520, 35, 54, 22);
          
          // ---- OBJ_45 ----
          OBJ_45.setToolTipText("Duplication");
          OBJ_45.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_45.setText(">");
          OBJ_45.setFont(OBJ_45.getFont().deriveFont(OBJ_45.getFont().getStyle() | Font.BOLD));
          OBJ_45.setName("OBJ_45");
          OBJ_45.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_45ActionPerformed(e);
            }
          });
          panel3.add(OBJ_45);
          OBJ_45.setBounds(520, 55, 54, 22);
          
          // ---- OBJ_46 ----
          OBJ_46.setToolTipText("Duplication");
          OBJ_46.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_46.setText(">");
          OBJ_46.setFont(OBJ_46.getFont().deriveFont(OBJ_46.getFont().getStyle() | Font.BOLD));
          OBJ_46.setName("OBJ_46");
          OBJ_46.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_46ActionPerformed(e);
            }
          });
          panel3.add(OBJ_46);
          OBJ_46.setBounds(520, 75, 54, 22);
          
          // ---- OBJ_47 ----
          OBJ_47.setToolTipText("Duplication");
          OBJ_47.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_47.setText(">");
          OBJ_47.setFont(OBJ_47.getFont().deriveFont(OBJ_47.getFont().getStyle() | Font.BOLD));
          OBJ_47.setName("OBJ_47");
          OBJ_47.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_47ActionPerformed(e);
            }
          });
          panel3.add(OBJ_47);
          OBJ_47.setBounds(520, 95, 54, 22);
          
          // ---- OBJ_48 ----
          OBJ_48.setToolTipText("Duplication");
          OBJ_48.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_48.setText(">");
          OBJ_48.setFont(OBJ_48.getFont().deriveFont(OBJ_48.getFont().getStyle() | Font.BOLD));
          OBJ_48.setName("OBJ_48");
          OBJ_48.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_48ActionPerformed(e);
            }
          });
          panel3.add(OBJ_48);
          OBJ_48.setBounds(520, 115, 54, 22);
          
          // ---- OBJ_49 ----
          OBJ_49.setToolTipText("Duplication");
          OBJ_49.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_49.setText(">");
          OBJ_49.setFont(OBJ_49.getFont().deriveFont(OBJ_49.getFont().getStyle() | Font.BOLD));
          OBJ_49.setName("OBJ_49");
          OBJ_49.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_49ActionPerformed(e);
            }
          });
          panel3.add(OBJ_49);
          OBJ_49.setBounds(520, 135, 54, 22);
          
          // ---- OBJ_50 ----
          OBJ_50.setToolTipText("Duplication");
          OBJ_50.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_50.setText(">");
          OBJ_50.setFont(OBJ_50.getFont().deriveFont(OBJ_50.getFont().getStyle() | Font.BOLD));
          OBJ_50.setName("OBJ_50");
          OBJ_50.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_50ActionPerformed(e);
            }
          });
          panel3.add(OBJ_50);
          OBJ_50.setBounds(520, 155, 54, 22);
          
          // ---- OBJ_51 ----
          OBJ_51.setToolTipText("Duplication");
          OBJ_51.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_51.setText(">");
          OBJ_51.setFont(OBJ_51.getFont().deriveFont(OBJ_51.getFont().getStyle() | Font.BOLD));
          OBJ_51.setName("OBJ_51");
          OBJ_51.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_51ActionPerformed(e);
            }
          });
          panel3.add(OBJ_51);
          OBJ_51.setBounds(520, 175, 54, 22);
          
          // ---- OBJ_52 ----
          OBJ_52.setToolTipText("Duplication");
          OBJ_52.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_52.setText(">");
          OBJ_52.setFont(OBJ_52.getFont().deriveFont(OBJ_52.getFont().getStyle() | Font.BOLD));
          OBJ_52.setName("OBJ_52");
          OBJ_52.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_52ActionPerformed(e);
            }
          });
          panel3.add(OBJ_52);
          OBJ_52.setBounds(520, 195, 54, 22);
          
          // ---- OBJ_53 ----
          OBJ_53.setToolTipText("Duplication");
          OBJ_53.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_53.setText(">");
          OBJ_53.setFont(OBJ_53.getFont().deriveFont(OBJ_53.getFont().getStyle() | Font.BOLD));
          OBJ_53.setName("OBJ_53");
          OBJ_53.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_53ActionPerformed(e);
            }
          });
          panel3.add(OBJ_53);
          OBJ_53.setBounds(520, 215, 54, 22);
          
          // ---- OBJ_54 ----
          OBJ_54.setToolTipText("Duplication");
          OBJ_54.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_54.setText(">");
          OBJ_54.setFont(OBJ_54.getFont().deriveFont(OBJ_54.getFont().getStyle() | Font.BOLD));
          OBJ_54.setName("OBJ_54");
          OBJ_54.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_54ActionPerformed(e);
            }
          });
          panel3.add(OBJ_54);
          OBJ_54.setBounds(520, 235, 54, 22);
          
          // ---- OBJ_55 ----
          OBJ_55.setToolTipText("Duplication");
          OBJ_55.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_55.setText(">");
          OBJ_55.setFont(OBJ_55.getFont().deriveFont(OBJ_55.getFont().getStyle() | Font.BOLD));
          OBJ_55.setName("OBJ_55");
          OBJ_55.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_55ActionPerformed(e);
            }
          });
          panel3.add(OBJ_55);
          OBJ_55.setBounds(520, 255, 54, 22);
          
          // ---- OBJ_56 ----
          OBJ_56.setToolTipText("Duplication");
          OBJ_56.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_56.setText(">");
          OBJ_56.setFont(OBJ_56.getFont().deriveFont(OBJ_56.getFont().getStyle() | Font.BOLD));
          OBJ_56.setName("OBJ_56");
          OBJ_56.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_56ActionPerformed(e);
            }
          });
          panel3.add(OBJ_56);
          OBJ_56.setBounds(520, 275, 54, 22);
          
          // ---- OBJ_57 ----
          OBJ_57.setToolTipText("Duplication");
          OBJ_57.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_57.setText(">");
          OBJ_57.setFont(OBJ_57.getFont().deriveFont(OBJ_57.getFont().getStyle() | Font.BOLD));
          OBJ_57.setName("OBJ_57");
          OBJ_57.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_57ActionPerformed(e);
            }
          });
          panel3.add(OBJ_57);
          OBJ_57.setBounds(520, 295, 54, 22);
          
          // ---- OBJ_58 ----
          OBJ_58.setToolTipText("Duplication");
          OBJ_58.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_58.setText(">");
          OBJ_58.setFont(OBJ_58.getFont().deriveFont(OBJ_58.getFont().getStyle() | Font.BOLD));
          OBJ_58.setName("OBJ_58");
          OBJ_58.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_58ActionPerformed(e);
            }
          });
          panel3.add(OBJ_58);
          OBJ_58.setBounds(520, 315, 54, 22);
          
          // ---- OBJ_59 ----
          OBJ_59.setToolTipText("Duplication");
          OBJ_59.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          OBJ_59.setText(">");
          OBJ_59.setFont(OBJ_59.getFont().deriveFont(OBJ_59.getFont().getStyle() | Font.BOLD));
          OBJ_59.setName("OBJ_59");
          OBJ_59.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              OBJ_59ActionPerformed(e);
            }
          });
          panel3.add(OBJ_59);
          OBJ_59.setBounds(520, 335, 54, 22);
          
          // ======== SCROLLPANE_LIST18 ========
          {
            SCROLLPANE_LIST18.setComponentPopupMenu(BTD);
            SCROLLPANE_LIST18.setName("SCROLLPANE_LIST18");
            
            // ---- LA01 ----
            LA01.setRowHeight(20);
            LA01.setName("LA01");
            SCROLLPANE_LIST18.setViewportView(LA01);
          }
          panel3.add(SCROLLPANE_LIST18);
          SCROLLPANE_LIST18.setBounds(5, 10, 505, 350);
          
          // ---- MTA01 ----
          MTA01.setName("MTA01");
          panel3.add(MTA01);
          MTA01.setBounds(585, 35, 100, 23);
          
          // ---- MTA02 ----
          MTA02.setName("MTA02");
          panel3.add(MTA02);
          MTA02.setBounds(585, 55, 100, 23);
          
          // ---- MTA03 ----
          MTA03.setName("MTA03");
          panel3.add(MTA03);
          MTA03.setBounds(585, 75, 100, 23);
          
          // ---- MTA04 ----
          MTA04.setName("MTA04");
          panel3.add(MTA04);
          MTA04.setBounds(585, 95, 100, 23);
          
          // ---- MTA05 ----
          MTA05.setName("MTA05");
          panel3.add(MTA05);
          MTA05.setBounds(585, 115, 100, 23);
          
          // ---- MTA06 ----
          MTA06.setName("MTA06");
          panel3.add(MTA06);
          MTA06.setBounds(585, 135, 100, 23);
          
          // ---- MTA07 ----
          MTA07.setName("MTA07");
          panel3.add(MTA07);
          MTA07.setBounds(585, 155, 100, 23);
          
          // ---- MTA08 ----
          MTA08.setName("MTA08");
          panel3.add(MTA08);
          MTA08.setBounds(585, 175, 100, 23);
          
          // ---- MTA09 ----
          MTA09.setName("MTA09");
          panel3.add(MTA09);
          MTA09.setBounds(585, 195, 100, 23);
          
          // ---- MTA10 ----
          MTA10.setName("MTA10");
          panel3.add(MTA10);
          MTA10.setBounds(585, 215, 100, 23);
          
          // ---- MTA11 ----
          MTA11.setName("MTA11");
          panel3.add(MTA11);
          MTA11.setBounds(585, 235, 100, 23);
          
          // ---- MTA12 ----
          MTA12.setName("MTA12");
          panel3.add(MTA12);
          MTA12.setBounds(585, 255, 100, 23);
          
          // ---- MTA13 ----
          MTA13.setName("MTA13");
          panel3.add(MTA13);
          MTA13.setBounds(585, 275, 100, 23);
          
          // ---- MTA14 ----
          MTA14.setName("MTA14");
          panel3.add(MTA14);
          MTA14.setBounds(585, 295, 100, 23);
          
          // ---- MTA15 ----
          MTA15.setName("MTA15");
          panel3.add(MTA15);
          MTA15.setBounds(585, 315, 100, 23);
          
          // ---- MTA16 ----
          MTA16.setName("MTA16");
          panel3.add(MTA16);
          MTA16.setBounds(585, 335, 100, 23);
          
          // ---- label2 ----
          label2.setText("@ERA01@");
          label2.setHorizontalAlignment(SwingConstants.CENTER);
          label2.setForeground(new Color(204, 0, 0));
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD));
          label2.setName("label2");
          panel3.add(label2);
          label2.setBounds(690, 35, 20, 23);
          
          // ---- label3 ----
          label3.setText("@ERA02@");
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setForeground(new Color(204, 0, 0));
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD));
          label3.setName("label3");
          panel3.add(label3);
          label3.setBounds(690, 55, 20, 23);
          
          // ---- label4 ----
          label4.setText("@ERA03@");
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setForeground(new Color(204, 0, 0));
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD));
          label4.setName("label4");
          panel3.add(label4);
          label4.setBounds(690, 75, 20, 23);
          
          // ---- label5 ----
          label5.setText("@ERA04@");
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setForeground(new Color(204, 0, 0));
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD));
          label5.setName("label5");
          panel3.add(label5);
          label5.setBounds(690, 95, 20, 23);
          
          // ---- label6 ----
          label6.setText("@ERA05@");
          label6.setHorizontalAlignment(SwingConstants.CENTER);
          label6.setForeground(new Color(204, 0, 0));
          label6.setFont(label6.getFont().deriveFont(label6.getFont().getStyle() | Font.BOLD));
          label6.setName("label6");
          panel3.add(label6);
          label6.setBounds(690, 115, 20, 23);
          
          // ---- label7 ----
          label7.setText("@ERA06@");
          label7.setHorizontalAlignment(SwingConstants.CENTER);
          label7.setForeground(new Color(204, 0, 0));
          label7.setFont(label7.getFont().deriveFont(label7.getFont().getStyle() | Font.BOLD));
          label7.setName("label7");
          panel3.add(label7);
          label7.setBounds(690, 135, 20, 23);
          
          // ---- label8 ----
          label8.setText("@ERA07@");
          label8.setHorizontalAlignment(SwingConstants.CENTER);
          label8.setForeground(new Color(204, 0, 0));
          label8.setFont(label8.getFont().deriveFont(label8.getFont().getStyle() | Font.BOLD));
          label8.setName("label8");
          panel3.add(label8);
          label8.setBounds(690, 155, 20, 23);
          
          // ---- label9 ----
          label9.setText("@ERA08@");
          label9.setHorizontalAlignment(SwingConstants.CENTER);
          label9.setForeground(new Color(204, 0, 0));
          label9.setFont(label9.getFont().deriveFont(label9.getFont().getStyle() | Font.BOLD));
          label9.setName("label9");
          panel3.add(label9);
          label9.setBounds(690, 175, 20, 23);
          
          // ---- label10 ----
          label10.setText("@ERA09@");
          label10.setHorizontalAlignment(SwingConstants.CENTER);
          label10.setForeground(new Color(204, 0, 0));
          label10.setFont(label10.getFont().deriveFont(label10.getFont().getStyle() | Font.BOLD));
          label10.setName("label10");
          panel3.add(label10);
          label10.setBounds(690, 195, 20, 23);
          
          // ---- label11 ----
          label11.setText("@ERA10@");
          label11.setHorizontalAlignment(SwingConstants.CENTER);
          label11.setForeground(new Color(204, 0, 0));
          label11.setFont(label11.getFont().deriveFont(label11.getFont().getStyle() | Font.BOLD));
          label11.setName("label11");
          panel3.add(label11);
          label11.setBounds(690, 215, 20, 23);
          
          // ---- label12 ----
          label12.setText("@ERA11@");
          label12.setHorizontalAlignment(SwingConstants.CENTER);
          label12.setForeground(new Color(204, 0, 0));
          label12.setFont(label12.getFont().deriveFont(label12.getFont().getStyle() | Font.BOLD));
          label12.setName("label12");
          panel3.add(label12);
          label12.setBounds(690, 235, 20, 23);
          
          // ---- label13 ----
          label13.setText("@ERA12@");
          label13.setHorizontalAlignment(SwingConstants.CENTER);
          label13.setForeground(new Color(204, 0, 0));
          label13.setFont(label13.getFont().deriveFont(label13.getFont().getStyle() | Font.BOLD));
          label13.setName("label13");
          panel3.add(label13);
          label13.setBounds(690, 255, 20, 23);
          
          // ---- label14 ----
          label14.setText("@ERA13@");
          label14.setHorizontalAlignment(SwingConstants.CENTER);
          label14.setForeground(new Color(204, 0, 0));
          label14.setFont(label14.getFont().deriveFont(label14.getFont().getStyle() | Font.BOLD));
          label14.setName("label14");
          panel3.add(label14);
          label14.setBounds(690, 275, 20, 23);
          
          // ---- label15 ----
          label15.setText("@ERA14@");
          label15.setHorizontalAlignment(SwingConstants.CENTER);
          label15.setForeground(new Color(204, 0, 0));
          label15.setFont(label15.getFont().deriveFont(label15.getFont().getStyle() | Font.BOLD));
          label15.setName("label15");
          panel3.add(label15);
          label15.setBounds(690, 295, 20, 23);
          
          // ---- label16 ----
          label16.setText("@ERA15@");
          label16.setHorizontalAlignment(SwingConstants.CENTER);
          label16.setForeground(new Color(204, 0, 0));
          label16.setFont(label16.getFont().deriveFont(label16.getFont().getStyle() | Font.BOLD));
          label16.setName("label16");
          panel3.add(label16);
          label16.setBounds(690, 315, 20, 23);
          
          // ---- label17 ----
          label17.setText("@ERA16@");
          label17.setHorizontalAlignment(SwingConstants.CENTER);
          label17.setForeground(new Color(204, 0, 0));
          label17.setFont(label17.getFont().deriveFont(label17.getFont().getStyle() | Font.BOLD));
          label17.setName("label17");
          panel3.add(label17);
          label17.setBounds(690, 335, 20, 23);
          
          // ---- label1 ----
          label1.setText("Montant affect\u00e9");
          label1.setHorizontalAlignment(SwingConstants.LEFT);
          label1.setName("label1");
          panel3.add(label1);
          label1.setBounds(585, 15, 100, 20);
          
          // ---- label18 ----
          label18.setText("E");
          label18.setName("label18");
          panel3.add(label18);
          label18.setBounds(695, 15, 15, 20);
          
          // ---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel3.add(BT_PGUP);
          BT_PGUP.setBounds(725, 35, 25, 160);
          
          // ---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel3.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(725, 200, 25, 160);
          
          // ---- MSGAFF ----
          MSGAFF.setComponentPopupMenu(BTD);
          MSGAFF.setFont(MSGAFF.getFont().deriveFont(MSGAFF.getFont().getStyle() | Font.BOLD));
          MSGAFF.setForeground(Color.red);
          MSGAFF.setText("@MSGAFF@");
          MSGAFF.setBorder(null);
          MSGAFF.setBackground(new Color(238, 238, 210));
          MSGAFF.setName("MSGAFF");
          panel3.add(MSGAFF);
          MSGAFF.setBounds(5, 380, 335, MSGAFF.getPreferredSize().height);
          
          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for (int i = 0; i < panel3.getComponentCount(); i++) {
              Rectangle bounds = panel3.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel3.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel3.setMinimumSize(preferredSize);
            panel3.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel3);
        panel3.setBounds(15, 75, 765, 410);
        
        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for (int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    
    // ======== BTD ========
    {
      BTD.setName("BTD");
      
      // ---- OBJ_12 ----
      OBJ_12.setText("D\u00e9coupage \u00e9criture");
      OBJ_12.setName("OBJ_12");
      OBJ_12.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_12ActionPerformed(e);
        }
      });
      BTD.add(OBJ_12);
      
      // ---- OBJ_14 ----
      OBJ_14.setText("Duplication");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);
      
      // ---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization //GEN-END:initComponents
  }
  
  // JFormDesigner - Variables declaration - DO NOT MODIFY //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private RiSousMenu riSousMenu8;
  private RiSousMenu_bt riSousMenu_bt8;
  private RiSousMenu riSousMenu9;
  private RiSousMenu_bt riSousMenu_bt9;
  private JPanel p_contenu;
  private JScrollPane scrollPane1;
  private XRiTable WDERLD;
  private JPanel panel3;
  private JLabel label19;
  private RiZoneSortie TOTAFF;
  private RiZoneSortie WDIFE;
  private JLabel OBJ_60;
  private JButton OBJ_44;
  private JButton OBJ_45;
  private JButton OBJ_46;
  private JButton OBJ_47;
  private JButton OBJ_48;
  private JButton OBJ_49;
  private JButton OBJ_50;
  private JButton OBJ_51;
  private JButton OBJ_52;
  private JButton OBJ_53;
  private JButton OBJ_54;
  private JButton OBJ_55;
  private JButton OBJ_56;
  private JButton OBJ_57;
  private JButton OBJ_58;
  private JButton OBJ_59;
  private JScrollPane SCROLLPANE_LIST18;
  private XRiTable LA01;
  private XRiTextField MTA01;
  private XRiTextField MTA02;
  private XRiTextField MTA03;
  private XRiTextField MTA04;
  private XRiTextField MTA05;
  private XRiTextField MTA06;
  private XRiTextField MTA07;
  private XRiTextField MTA08;
  private XRiTextField MTA09;
  private XRiTextField MTA10;
  private XRiTextField MTA11;
  private XRiTextField MTA12;
  private XRiTextField MTA13;
  private XRiTextField MTA14;
  private XRiTextField MTA15;
  private XRiTextField MTA16;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  private JLabel label6;
  private JLabel label7;
  private JLabel label8;
  private JLabel label9;
  private JLabel label10;
  private JLabel label11;
  private JLabel label12;
  private JLabel label13;
  private JLabel label14;
  private JLabel label15;
  private JLabel label16;
  private JLabel label17;
  private JLabel label1;
  private JLabel label18;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private RiZoneSortie MSGAFF;
  private JPopupMenu BTD;
  private JMenuItem OBJ_12;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration //GEN-END:variables
}
