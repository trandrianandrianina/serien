
package ri.serien.libecranrpg.vgtm.VGTM57FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu;
import ri.serien.libswing.composantrpg.lexical.RiSousMenu_bt;
import ri.serien.libswing.composantrpg.lexical.RiZoneSortie;
import ri.serien.libswing.composantrpg.lexical.XRiComboBox;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Emmanuel MARCQ
 */
public class VGTM57FM_L2 extends SNPanelEcranRPG implements ioFrame {
  
   
  
  private String[] L1SNS_Value = { "", "D", "C", };
  private String[] L1SNS_text = { "", "Débit", "Crédit", };
  
  public VGTM57FM_L2(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    
    // Bouton par défaut
    setDefaultButton(bouton_valider);
    
    setDialog(true);
    
    // Ajout
    initDiverses();
    L1SNS.setValeurs(L1SNS_Value, L1SNS_text);
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
    OBJ_23.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@LIBCPT@")).trim());
    W1ATT.setText(lexique.TranslationTable(interpreteurD.analyseExpression("@W1ATT@")).trim());
  }
  
  @Override
  public void setData() {
    super.setData();
    
    // Fonctions diverses après initialisation des données
    setDiverses();
    
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    
    // Gestion auto des erreurs
    gererLesErreurs("19");
    


    
    
    
    OBJ_49.setVisible(lexique.isPresent("L1DEV"));
    OBJ_37.setVisible(lexique.isPresent("LIBMTT"));
    OBJ_23.setVisible(lexique.isPresent("LIBCPT"));
    OBJ_29.setVisible(lexique.isPresent("WCHGX"));
    OBJ_30.setVisible(lexique.isPresent("L1MTD"));
    OBJ_31.setVisible(lexique.isPresent("L1QTE"));
    OBJ_42.setVisible(lexique.isPresent("L1SAN"));
    OBJ_43.setVisible(lexique.isPresent("L1SAN"));
    OBJ_44.setVisible(lexique.isPresent("L1SAN"));
    
    // TODO Icones
    
  }
  
  @Override
  public void getData() {
    super.getData();
    
    
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void riSousMenu_bt6ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F9", false);
  }
  
  private void riSousMenu_bt7ActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F22", false);
  }
  
  private void OBJ_14ActionPerformed(ActionEvent e) {
    lexique.HostCursorPut(BTD.getInvoker().getName());
    lexique.HostScreenSendKey(this, "F4", false);
  }
  
  private void OBJ_13ActionPerformed(ActionEvent e) {
    lexique.WatchHelp(BTD.getInvoker().getName());
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    riMenu2 = new RiMenu();
    riMenu_bt2 = new RiMenu_bt();
    riSousMenu6 = new RiSousMenu();
    riSousMenu_bt6 = new RiSousMenu_bt();
    riSousMenu7 = new RiSousMenu();
    riSousMenu_bt7 = new RiSousMenu_bt();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    L1NCG = new XRiTextField();
    L1NCA = new XRiTextField();
    L1NLI = new XRiTextField();
    WCOD = new XRiTextField();
    OBJ_37 = new JLabel();
    OBJ_40 = new JLabel();
    OBJ_41 = new JLabel();
    OBJ_42 = new JLabel();
    OBJ_44 = new JLabel();
    OBJ_43 = new JLabel();
    OBJ_38 = new JLabel();
    OBJ_39 = new JLabel();
    L1SNS = new XRiComboBox();
    L1LIB = new XRiTextField();
    L1MTT = new XRiTextField();
    L1PCE = new XRiTextField();
    L1SAN = new XRiTextField();
    L1ACT = new XRiTextField();
    L1NAT = new XRiTextField();
    L1DTJ = new XRiTextField();
    L1CLB = new XRiTextField();
    OBJ_45 = new JLabel();
    OBJ_49 = new JLabel();
    L1DEV = new XRiTextField();
    OBJ_29 = new JLabel();
    OBJ_30 = new JLabel();
    OBJ_31 = new JLabel();
    WCHGX = new XRiTextField();
    L1MTD = new XRiTextField();
    L1QTE = new XRiTextField();
    OBJ_23 = new RiZoneSortie();
    OBJ_46 = new JLabel();
    OBJ_47 = new JLabel();
    W1ATT = new JLabel();
    BTD = new JPopupMenu();
    OBJ_14 = new JMenuItem();
    OBJ_13 = new JMenuItem();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(860, 210));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());

          //======== riMenu2 ========
          {
            riMenu2.setName("riMenu2");

            //---- riMenu_bt2 ----
            riMenu_bt2.setText("Options");
            riMenu_bt2.setName("riMenu_bt2");
            riMenu2.add(riMenu_bt2);
          }
          menus_haut.add(riMenu2);

          //======== riSousMenu6 ========
          {
            riSousMenu6.setName("riSousMenu6");

            //---- riSousMenu_bt6 ----
            riSousMenu_bt6.setText("Affectations r\u00e9glements");
            riSousMenu_bt6.setToolTipText("Mise en/hors fonction des affectations de r\u00e9glements");
            riSousMenu_bt6.setName("riSousMenu_bt6");
            riSousMenu_bt6.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt6ActionPerformed(e);
              }
            });
            riSousMenu6.add(riSousMenu_bt6);
          }
          menus_haut.add(riSousMenu6);

          //======== riSousMenu7 ========
          {
            riSousMenu7.setName("riSousMenu7");

            //---- riSousMenu_bt7 ----
            riSousMenu_bt7.setText("Bloc-notes sur \u00e9criture");
            riSousMenu_bt7.setToolTipText("Bloc-notes sur \u00e9criture");
            riSousMenu_bt7.setName("riSousMenu_bt7");
            riSousMenu_bt7.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                riSousMenu_bt7ActionPerformed(e);
              }
            });
            riSousMenu7.add(riSousMenu_bt7);
          }
          menus_haut.add(riSousMenu7);
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- L1NCG ----
          L1NCG.setName("L1NCG");
          panel1.add(L1NCG);
          L1NCG.setBounds(205, 20, 58, L1NCG.getPreferredSize().height);

          //---- L1NCA ----
          L1NCA.setName("L1NCA");
          panel1.add(L1NCA);
          L1NCA.setBounds(265, 20, 58, L1NCA.getPreferredSize().height);

          //---- L1NLI ----
          L1NLI.setName("L1NLI");
          panel1.add(L1NLI);
          L1NLI.setBounds(100, 20, 42, L1NLI.getPreferredSize().height);

          //---- WCOD ----
          WCOD.setName("WCOD");
          panel1.add(WCOD);
          WCOD.setBounds(30, 20, 24, WCOD.getPreferredSize().height);

          //---- OBJ_37 ----
          OBJ_37.setText("Montant");
          OBJ_37.setName("OBJ_37");
          panel1.add(OBJ_37);
          OBJ_37.setBounds(65, 55, 99, 28);

          //---- OBJ_40 ----
          OBJ_40.setText("Libell\u00e9");
          OBJ_40.setName("OBJ_40");
          panel1.add(OBJ_40);
          OBJ_40.setBounds(65, 115, 61, 28);

          //---- OBJ_41 ----
          OBJ_41.setText("N\u00b0Pi\u00e8ce");
          OBJ_41.setName("OBJ_41");
          panel1.add(OBJ_41);
          OBJ_41.setBounds(335, 115, 52, 28);

          //---- OBJ_42 ----
          OBJ_42.setText("Section");
          OBJ_42.setName("OBJ_42");
          panel1.add(OBJ_42);
          OBJ_42.setBounds(430, 115, 49, 28);

          //---- OBJ_44 ----
          OBJ_44.setText("Nature");
          OBJ_44.setName("OBJ_44");
          panel1.add(OBJ_44);
          OBJ_44.setBounds(554, 115, 43, 28);

          //---- OBJ_43 ----
          OBJ_43.setText("Affaire");
          OBJ_43.setName("OBJ_43");
          panel1.add(OBJ_43);
          OBJ_43.setBounds(487, 115, 37, 28);

          //---- OBJ_38 ----
          OBJ_38.setText("Sens");
          OBJ_38.setName("OBJ_38");
          panel1.add(OBJ_38);
          OBJ_38.setBounds(205, 55, 31, 28);

          //---- OBJ_39 ----
          OBJ_39.setText("C");
          OBJ_39.setName("OBJ_39");
          panel1.add(OBJ_39);
          OBJ_39.setBounds(10, 140, 12, 28);

          //---- L1SNS ----
          L1SNS.setModel(new DefaultComboBoxModel(new String[] {
            " ",
            "D\u00e9bit",
            "Cr\u00e9dit"
          }));
          L1SNS.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          L1SNS.setName("L1SNS");
          panel1.add(L1SNS);
          L1SNS.setBounds(205, 80, 75, L1SNS.getPreferredSize().height);

          //---- L1LIB ----
          L1LIB.setComponentPopupMenu(BTD);
          L1LIB.setName("L1LIB");
          panel1.add(L1LIB);
          L1LIB.setBounds(65, 140, 270, L1LIB.getPreferredSize().height);

          //---- L1MTT ----
          L1MTT.setComponentPopupMenu(BTD);
          L1MTT.setName("L1MTT");
          panel1.add(L1MTT);
          L1MTT.setBounds(65, 80, 114, L1MTT.getPreferredSize().height);

          //---- L1PCE ----
          L1PCE.setComponentPopupMenu(BTD);
          L1PCE.setName("L1PCE");
          panel1.add(L1PCE);
          L1PCE.setBounds(335, 140, 80, L1PCE.getPreferredSize().height);

          //---- L1SAN ----
          L1SAN.setComponentPopupMenu(BTD);
          L1SAN.setName("L1SAN");
          panel1.add(L1SAN);
          L1SAN.setBounds(430, 140, 54, L1SAN.getPreferredSize().height);

          //---- L1ACT ----
          L1ACT.setComponentPopupMenu(BTD);
          L1ACT.setName("L1ACT");
          panel1.add(L1ACT);
          L1ACT.setBounds(487, 140, 64, L1ACT.getPreferredSize().height);

          //---- L1NAT ----
          L1NAT.setComponentPopupMenu(BTD);
          L1NAT.setName("L1NAT");
          panel1.add(L1NAT);
          L1NAT.setBounds(554, 140, 64, L1NAT.getPreferredSize().height);

          //---- L1DTJ ----
          L1DTJ.setComponentPopupMenu(BTD);
          L1DTJ.setName("L1DTJ");
          panel1.add(L1DTJ);
          L1DTJ.setBounds(30, 80, 30, L1DTJ.getPreferredSize().height);

          //---- L1CLB ----
          L1CLB.setComponentPopupMenu(BTD);
          L1CLB.setName("L1CLB");
          panel1.add(L1CLB);
          L1CLB.setBounds(30, 140, 24, L1CLB.getPreferredSize().height);

          //---- OBJ_45 ----
          OBJ_45.setText("le");
          OBJ_45.setName("OBJ_45");
          panel1.add(OBJ_45);
          OBJ_45.setBounds(10, 85, 18, 20);

          //---- OBJ_49 ----
          OBJ_49.setText("Devise");
          OBJ_49.setName("OBJ_49");
          panel1.add(OBJ_49);
          OBJ_49.setBounds(280, 55, 43, 28);

          //---- L1DEV ----
          L1DEV.setName("L1DEV");
          panel1.add(L1DEV);
          L1DEV.setBounds(283, 80, 40, L1DEV.getPreferredSize().height);

          //---- OBJ_29 ----
          OBJ_29.setText("Taux");
          OBJ_29.setName("OBJ_29");
          panel1.add(OBJ_29);
          OBJ_29.setBounds(335, 55, 76, 28);

          //---- OBJ_30 ----
          OBJ_30.setText("Montant en devise");
          OBJ_30.setName("OBJ_30");
          panel1.add(OBJ_30);
          OBJ_30.setBounds(430, 55, 104, 28);

          //---- OBJ_31 ----
          OBJ_31.setText("Quantit\u00e9");
          OBJ_31.setName("OBJ_31");
          panel1.add(OBJ_31);
          OBJ_31.setBounds(554, 55, 52, 28);

          //---- WCHGX ----
          WCHGX.setName("WCHGX");
          panel1.add(WCHGX);
          WCHGX.setBounds(335, 80, 76, WCHGX.getPreferredSize().height);

          //---- L1MTD ----
          L1MTD.setComponentPopupMenu(BTD);
          L1MTD.setName("L1MTD");
          panel1.add(L1MTD);
          L1MTD.setBounds(430, 80, 104, L1MTD.getPreferredSize().height);

          //---- L1QTE ----
          L1QTE.setComponentPopupMenu(BTD);
          L1QTE.setName("L1QTE");
          panel1.add(L1QTE);
          L1QTE.setBounds(554, 80, 104, L1QTE.getPreferredSize().height);

          //---- OBJ_23 ----
          OBJ_23.setText("@LIBCPT@");
          OBJ_23.setName("OBJ_23");
          panel1.add(OBJ_23);
          OBJ_23.setBounds(335, 22, 232, OBJ_23.getPreferredSize().height);

          //---- OBJ_46 ----
          OBJ_46.setText("ligne");
          OBJ_46.setName("OBJ_46");
          panel1.add(OBJ_46);
          OBJ_46.setBounds(65, 20, 30, 28);

          //---- OBJ_47 ----
          OBJ_47.setText("Compte");
          OBJ_47.setName("OBJ_47");
          panel1.add(OBJ_47);
          OBJ_47.setBounds(150, 20, 50, 28);

          //---- W1ATT ----
          W1ATT.setText("@W1ATT@");
          W1ATT.setHorizontalAlignment(SwingConstants.CENTER);
          W1ATT.setName("W1ATT");
          panel1.add(W1ATT);
          W1ATT.setBounds(10, 175, 655, W1ATT.getPreferredSize().height);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(5, 5, 675, 200);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);

    //======== BTD ========
    {
      BTD.setName("BTD");

      //---- OBJ_14 ----
      OBJ_14.setText("Choix possibles");
      OBJ_14.setName("OBJ_14");
      OBJ_14.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_14ActionPerformed(e);
        }
      });
      BTD.add(OBJ_14);

      //---- OBJ_13 ----
      OBJ_13.setText("Aide en ligne");
      OBJ_13.setName("OBJ_13");
      OBJ_13.addActionListener(new ActionListener() {
        @Override
        public void actionPerformed(ActionEvent e) {
          OBJ_13ActionPerformed(e);
        }
      });
      BTD.add(OBJ_13);
    }
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private RiMenu riMenu2;
  private RiMenu_bt riMenu_bt2;
  private RiSousMenu riSousMenu6;
  private RiSousMenu_bt riSousMenu_bt6;
  private RiSousMenu riSousMenu7;
  private RiSousMenu_bt riSousMenu_bt7;
  private JPanel p_contenu;
  private JPanel panel1;
  private XRiTextField L1NCG;
  private XRiTextField L1NCA;
  private XRiTextField L1NLI;
  private XRiTextField WCOD;
  private JLabel OBJ_37;
  private JLabel OBJ_40;
  private JLabel OBJ_41;
  private JLabel OBJ_42;
  private JLabel OBJ_44;
  private JLabel OBJ_43;
  private JLabel OBJ_38;
  private JLabel OBJ_39;
  private XRiComboBox L1SNS;
  private XRiTextField L1LIB;
  private XRiTextField L1MTT;
  private XRiTextField L1PCE;
  private XRiTextField L1SAN;
  private XRiTextField L1ACT;
  private XRiTextField L1NAT;
  private XRiTextField L1DTJ;
  private XRiTextField L1CLB;
  private JLabel OBJ_45;
  private JLabel OBJ_49;
  private XRiTextField L1DEV;
  private JLabel OBJ_29;
  private JLabel OBJ_30;
  private JLabel OBJ_31;
  private XRiTextField WCHGX;
  private XRiTextField L1MTD;
  private XRiTextField L1QTE;
  private RiZoneSortie OBJ_23;
  private JLabel OBJ_46;
  private JLabel OBJ_47;
  private JLabel W1ATT;
  private JPopupMenu BTD;
  private JMenuItem OBJ_14;
  private JMenuItem OBJ_13;
  // JFormDesigner - End of variables declaration  //GEN-END:variables
}
