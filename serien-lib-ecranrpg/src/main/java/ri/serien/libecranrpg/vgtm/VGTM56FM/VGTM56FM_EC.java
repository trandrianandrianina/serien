
package ri.serien.libecranrpg.vgtm.VGTM56FM;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Insets;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;

import org.jdesktop.swingx.VerticalLayout;

import ri.serien.libcommun.outils.composants.ioFrame;
import ri.serien.libswing.composantrpg.autonome.SNPanelEcranRPG;
import ri.serien.libswing.composantrpg.lexical.RiMenu;
import ri.serien.libswing.composantrpg.lexical.RiMenu_bt;
import ri.serien.libswing.composantrpg.lexical.XRiCalendrier;
import ri.serien.libswing.composantrpg.lexical.XRiTextField;

/**
 * @author Stéphane Vénéri
 */
public class VGTM56FM_EC extends SNPanelEcranRPG implements ioFrame {
  
   
  
  public VGTM56FM_EC(ArrayList<?> param) {
    super(param);
    initComponents();
    setVersionLook(2);
    setDefaultButton(bouton_valider);
    setDialog(true);
    initDiverses();
    
  }

  /**
   * Penser à ajouter les composants contenant des libellés variables du type @XXX@.
   * Cette mise à jour est obligatoirement manuelle.
   */
  @Override
  public void mettreAJourVariableLibelle() {
  }
  
  @Override
  public void setData() {
    super.setData();
    setDiverses();
    // Permet d'afficher aucun sous-menu
    gererAffichageMenus(null);
    gererLesErreurs("19");
    
    
    setTitle(interpreteurD.analyseExpression("Rapprochement bancaire"));
  }
  
  @Override
  public void getData() {
    super.getData();
    
  }
  
  private void bouton_validerActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "ENTER");
  }
  
  private void bouton_retourActionPerformed(ActionEvent e) {
    lexique.HostScreenSendKey(this, "F12", false);
  }
  
  private void initComponents() {
    // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
    p_principal = new JPanel();
    p_menus = new JPanel();
    menus_bas = new JPanel();
    navig_erreurs = new RiMenu();
    bouton_erreurs = new RiMenu_bt();
    navig_valid = new RiMenu();
    bouton_valider = new RiMenu_bt();
    navig_retour = new RiMenu();
    bouton_retour = new RiMenu_bt();
    menus_haut = new JPanel();
    p_contenu = new JPanel();
    panel1 = new JPanel();
    label1 = new JLabel();
    ECDAT = new XRiCalendrier();
    BT_PGUP = new JButton();
    BT_PGDOWN = new JButton();
    LIB1 = new XRiTextField();
    LIB2 = new XRiTextField();
    LIB3 = new XRiTextField();
    LIB4 = new XRiTextField();
    LIB5 = new XRiTextField();
    LIB6 = new XRiTextField();
    LIB7 = new XRiTextField();
    PCE1 = new XRiTextField();
    PCE2 = new XRiTextField();
    PCE3 = new XRiTextField();
    PCE4 = new XRiTextField();
    PCE5 = new XRiTextField();
    PCE6 = new XRiTextField();
    PCE7 = new XRiTextField();
    TDB1 = new XRiTextField();
    TDB2 = new XRiTextField();
    TDB3 = new XRiTextField();
    TDB4 = new XRiTextField();
    TDB5 = new XRiTextField();
    TDB6 = new XRiTextField();
    TDB7 = new XRiTextField();
    TCR1 = new XRiTextField();
    TCR2 = new XRiTextField();
    TCR3 = new XRiTextField();
    TCR4 = new XRiTextField();
    TCR5 = new XRiTextField();
    TCR6 = new XRiTextField();
    TCR7 = new XRiTextField();
    label2 = new JLabel();
    label3 = new JLabel();
    label4 = new JLabel();
    label5 = new JLabel();

    //======== this ========
    setMinimumSize(new Dimension(0, 0));
    setPreferredSize(new Dimension(820, 315));
    setName("this");
    setLayout(new BorderLayout());

    //======== p_principal ========
    {
      p_principal.setName("p_principal");
      p_principal.setLayout(new BorderLayout());

      //======== p_menus ========
      {
        p_menus.setPreferredSize(new Dimension(170, 0));
        p_menus.setMinimumSize(new Dimension(170, 0));
        p_menus.setBackground(new Color(238, 239, 241));
        p_menus.setBorder(LineBorder.createGrayLineBorder());
        p_menus.setName("p_menus");
        p_menus.setLayout(new BorderLayout());

        //======== menus_bas ========
        {
          menus_bas.setOpaque(false);
          menus_bas.setBackground(new Color(238, 239, 241));
          menus_bas.setName("menus_bas");
          menus_bas.setLayout(new VerticalLayout());

          //======== navig_erreurs ========
          {
            navig_erreurs.setName("navig_erreurs");

            //---- bouton_erreurs ----
            bouton_erreurs.setText("Erreurs");
            bouton_erreurs.setToolTipText("Erreurs");
            bouton_erreurs.setName("bouton_erreurs");
            navig_erreurs.add(bouton_erreurs);
          }
          menus_bas.add(navig_erreurs);

          //======== navig_valid ========
          {
            navig_valid.setName("navig_valid");

            //---- bouton_valider ----
            bouton_valider.setText("Valider");
            bouton_valider.setToolTipText("Valider");
            bouton_valider.setName("bouton_valider");
            bouton_valider.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_validerActionPerformed(e);
              }
            });
            navig_valid.add(bouton_valider);
          }
          menus_bas.add(navig_valid);

          //======== navig_retour ========
          {
            navig_retour.setName("navig_retour");

            //---- bouton_retour ----
            bouton_retour.setText("Retour");
            bouton_retour.setToolTipText("Retour");
            bouton_retour.setName("bouton_retour");
            bouton_retour.addActionListener(new ActionListener() {
              @Override
              public void actionPerformed(ActionEvent e) {
                bouton_retourActionPerformed(e);
              }
            });
            navig_retour.add(bouton_retour);
          }
          menus_bas.add(navig_retour);
        }
        p_menus.add(menus_bas, BorderLayout.SOUTH);

        //======== menus_haut ========
        {
          menus_haut.setMinimumSize(new Dimension(160, 520));
          menus_haut.setPreferredSize(new Dimension(160, 520));
          menus_haut.setBackground(new Color(238, 239, 241));
          menus_haut.setAutoscrolls(true);
          menus_haut.setName("menus_haut");
          menus_haut.setLayout(new VerticalLayout());
        }
        p_menus.add(menus_haut, BorderLayout.NORTH);
      }
      p_principal.add(p_menus, BorderLayout.EAST);

      //======== p_contenu ========
      {
        p_contenu.setBackground(new Color(238, 238, 210));
        p_contenu.setName("p_contenu");
        p_contenu.setLayout(null);

        //======== panel1 ========
        {
          panel1.setBorder(new TitledBorder("Saisie d'\u00e9critures pour ajustement du premier rapprochement"));
          panel1.setOpaque(false);
          panel1.setName("panel1");
          panel1.setLayout(null);

          //---- label1 ----
          label1.setText("Date d'\u00e9critures");
          label1.setName("label1");
          panel1.add(label1);
          label1.setBounds(15, 38, 115, 20);

          //---- ECDAT ----
          ECDAT.setName("ECDAT");
          panel1.add(ECDAT);
          ECDAT.setBounds(145, 34, 105, ECDAT.getPreferredSize().height);

          //---- BT_PGUP ----
          BT_PGUP.setText("");
          BT_PGUP.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGUP.setName("BT_PGUP");
          panel1.add(BT_PGUP);
          BT_PGUP.setBounds(570, 90, 25, 80);

          //---- BT_PGDOWN ----
          BT_PGDOWN.setText("");
          BT_PGDOWN.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
          BT_PGDOWN.setName("BT_PGDOWN");
          panel1.add(BT_PGDOWN);
          BT_PGDOWN.setBounds(570, 188, 25, 80);

          //---- LIB1 ----
          LIB1.setName("LIB1");
          panel1.add(LIB1);
          LIB1.setBounds(15, 90, 274, LIB1.getPreferredSize().height);

          //---- LIB2 ----
          LIB2.setName("LIB2");
          panel1.add(LIB2);
          LIB2.setBounds(15, 115, 274, LIB2.getPreferredSize().height);

          //---- LIB3 ----
          LIB3.setName("LIB3");
          panel1.add(LIB3);
          LIB3.setBounds(15, 140, 274, LIB3.getPreferredSize().height);

          //---- LIB4 ----
          LIB4.setName("LIB4");
          panel1.add(LIB4);
          LIB4.setBounds(15, 165, 274, LIB4.getPreferredSize().height);

          //---- LIB5 ----
          LIB5.setName("LIB5");
          panel1.add(LIB5);
          LIB5.setBounds(15, 190, 274, LIB5.getPreferredSize().height);

          //---- LIB6 ----
          LIB6.setName("LIB6");
          panel1.add(LIB6);
          LIB6.setBounds(15, 215, 274, LIB6.getPreferredSize().height);

          //---- LIB7 ----
          LIB7.setName("LIB7");
          panel1.add(LIB7);
          LIB7.setBounds(15, 240, 274, LIB7.getPreferredSize().height);

          //---- PCE1 ----
          PCE1.setName("PCE1");
          panel1.add(PCE1);
          PCE1.setBounds(290, 90, 68, PCE1.getPreferredSize().height);

          //---- PCE2 ----
          PCE2.setName("PCE2");
          panel1.add(PCE2);
          PCE2.setBounds(290, 115, 68, PCE2.getPreferredSize().height);

          //---- PCE3 ----
          PCE3.setName("PCE3");
          panel1.add(PCE3);
          PCE3.setBounds(290, 140, 68, PCE3.getPreferredSize().height);

          //---- PCE4 ----
          PCE4.setName("PCE4");
          panel1.add(PCE4);
          PCE4.setBounds(290, 165, 68, PCE4.getPreferredSize().height);

          //---- PCE5 ----
          PCE5.setName("PCE5");
          panel1.add(PCE5);
          PCE5.setBounds(290, 190, 68, PCE5.getPreferredSize().height);

          //---- PCE6 ----
          PCE6.setName("PCE6");
          panel1.add(PCE6);
          PCE6.setBounds(290, 215, 68, PCE6.getPreferredSize().height);

          //---- PCE7 ----
          PCE7.setName("PCE7");
          panel1.add(PCE7);
          PCE7.setBounds(290, 240, 68, PCE7.getPreferredSize().height);

          //---- TDB1 ----
          TDB1.setName("TDB1");
          panel1.add(TDB1);
          TDB1.setBounds(360, 90, 100, TDB1.getPreferredSize().height);

          //---- TDB2 ----
          TDB2.setName("TDB2");
          panel1.add(TDB2);
          TDB2.setBounds(360, 115, 100, TDB2.getPreferredSize().height);

          //---- TDB3 ----
          TDB3.setName("TDB3");
          panel1.add(TDB3);
          TDB3.setBounds(360, 140, 100, TDB3.getPreferredSize().height);

          //---- TDB4 ----
          TDB4.setName("TDB4");
          panel1.add(TDB4);
          TDB4.setBounds(360, 165, 100, TDB4.getPreferredSize().height);

          //---- TDB5 ----
          TDB5.setName("TDB5");
          panel1.add(TDB5);
          TDB5.setBounds(360, 190, 100, TDB5.getPreferredSize().height);

          //---- TDB6 ----
          TDB6.setName("TDB6");
          panel1.add(TDB6);
          TDB6.setBounds(360, 215, 100, TDB6.getPreferredSize().height);

          //---- TDB7 ----
          TDB7.setName("TDB7");
          panel1.add(TDB7);
          TDB7.setBounds(360, 240, 100, TDB7.getPreferredSize().height);

          //---- TCR1 ----
          TCR1.setName("TCR1");
          panel1.add(TCR1);
          TCR1.setBounds(460, 90, 100, TCR1.getPreferredSize().height);

          //---- TCR2 ----
          TCR2.setName("TCR2");
          panel1.add(TCR2);
          TCR2.setBounds(460, 115, 100, TCR2.getPreferredSize().height);

          //---- TCR3 ----
          TCR3.setName("TCR3");
          panel1.add(TCR3);
          TCR3.setBounds(460, 140, 100, TCR3.getPreferredSize().height);

          //---- TCR4 ----
          TCR4.setName("TCR4");
          panel1.add(TCR4);
          TCR4.setBounds(460, 165, 100, TCR4.getPreferredSize().height);

          //---- TCR5 ----
          TCR5.setName("TCR5");
          panel1.add(TCR5);
          TCR5.setBounds(460, 190, 100, TCR5.getPreferredSize().height);

          //---- TCR6 ----
          TCR6.setName("TCR6");
          panel1.add(TCR6);
          TCR6.setBounds(460, 215, 100, TCR6.getPreferredSize().height);

          //---- TCR7 ----
          TCR7.setName("TCR7");
          panel1.add(TCR7);
          TCR7.setBounds(460, 240, 100, TCR7.getPreferredSize().height);

          //---- label2 ----
          label2.setText("Nature de l'op\u00e9ration");
          label2.setFont(label2.getFont().deriveFont(label2.getFont().getStyle() | Font.BOLD, label2.getFont().getSize() + 1f));
          label2.setName("label2");
          panel1.add(label2);
          label2.setBounds(15, 65, 274, 23);

          //---- label3 ----
          label3.setText("N\u00b0 pi\u00e8ce");
          label3.setFont(label3.getFont().deriveFont(label3.getFont().getStyle() | Font.BOLD, label3.getFont().getSize() + 1f));
          label3.setHorizontalAlignment(SwingConstants.CENTER);
          label3.setName("label3");
          panel1.add(label3);
          label3.setBounds(290, 65, 68, 23);

          //---- label4 ----
          label4.setText("D\u00e9bit");
          label4.setFont(label4.getFont().deriveFont(label4.getFont().getStyle() | Font.BOLD, label4.getFont().getSize() + 1f));
          label4.setHorizontalAlignment(SwingConstants.CENTER);
          label4.setName("label4");
          panel1.add(label4);
          label4.setBounds(360, 65, 100, 23);

          //---- label5 ----
          label5.setText("Cr\u00e9dit");
          label5.setFont(label5.getFont().deriveFont(label5.getFont().getStyle() | Font.BOLD, label5.getFont().getSize() + 1f));
          label5.setHorizontalAlignment(SwingConstants.CENTER);
          label5.setName("label5");
          panel1.add(label5);
          label5.setBounds(460, 65, 100, 23);

          {
            // compute preferred size
            Dimension preferredSize = new Dimension();
            for(int i = 0; i < panel1.getComponentCount(); i++) {
              Rectangle bounds = panel1.getComponent(i).getBounds();
              preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
              preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
            }
            Insets insets = panel1.getInsets();
            preferredSize.width += insets.right;
            preferredSize.height += insets.bottom;
            panel1.setMinimumSize(preferredSize);
            panel1.setPreferredSize(preferredSize);
          }
        }
        p_contenu.add(panel1);
        panel1.setBounds(10, 10, 625, 290);

        {
          // compute preferred size
          Dimension preferredSize = new Dimension();
          for(int i = 0; i < p_contenu.getComponentCount(); i++) {
            Rectangle bounds = p_contenu.getComponent(i).getBounds();
            preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
            preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
          }
          Insets insets = p_contenu.getInsets();
          preferredSize.width += insets.right;
          preferredSize.height += insets.bottom;
          p_contenu.setMinimumSize(preferredSize);
          p_contenu.setPreferredSize(preferredSize);
        }
      }
      p_principal.add(p_contenu, BorderLayout.CENTER);
    }
    add(p_principal, BorderLayout.CENTER);
    // JFormDesigner - End of component initialization  //GEN-END:initComponents
  }


  // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
  private JPanel p_principal;
  private JPanel p_menus;
  private JPanel menus_bas;
  private RiMenu navig_erreurs;
  private RiMenu_bt bouton_erreurs;
  private RiMenu navig_valid;
  private RiMenu_bt bouton_valider;
  private RiMenu navig_retour;
  private RiMenu_bt bouton_retour;
  private JPanel menus_haut;
  private JPanel p_contenu;
  private JPanel panel1;
  private JLabel label1;
  private XRiCalendrier ECDAT;
  private JButton BT_PGUP;
  private JButton BT_PGDOWN;
  private XRiTextField LIB1;
  private XRiTextField LIB2;
  private XRiTextField LIB3;
  private XRiTextField LIB4;
  private XRiTextField LIB5;
  private XRiTextField LIB6;
  private XRiTextField LIB7;
  private XRiTextField PCE1;
  private XRiTextField PCE2;
  private XRiTextField PCE3;
  private XRiTextField PCE4;
  private XRiTextField PCE5;
  private XRiTextField PCE6;
  private XRiTextField PCE7;
  private XRiTextField TDB1;
  private XRiTextField TDB2;
  private XRiTextField TDB3;
  private XRiTextField TDB4;
  private XRiTextField TDB5;
  private XRiTextField TDB6;
  private XRiTextField TDB7;
  private XRiTextField TCR1;
  private XRiTextField TCR2;
  private XRiTextField TCR3;
  private XRiTextField TCR4;
  private XRiTextField TCR5;
  private XRiTextField TCR6;
  private XRiTextField TCR7;
  private JLabel label2;
  private JLabel label3;
  private JLabel label4;
  private JLabel label5;
  // JFormDesigner - End of variables declaration  //GEN-END:variables

}
